---
title: Let's talk about statues, heritage, and history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VL5O_RbHhxI) |
| Published | 2020/06/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Many statues are not historically significant or accurate, but rather mythologize and create a false narrative about American leaders.
- Statues in the South were often erected to send a message of dominance and segregation.
- American mythology, perpetuated by these statues, fails to address the country's problems and can lead to unrest.
- Beau mentions the example of Albert Pike, a Confederate officer whose statues downplay his controversial past.
- While mythologizing historical figures is okay to an extent, it must not be confused with history.
- Thomas Jefferson, despite his flaws, played a significant role in American history and should be remembered accurately.
- The U.S. was founded on flawed principles, but its strength lies in the ability to change and evolve.
- Preserving statues that are being taken down can serve as a reminder of a pivotal moment in American history.
- Beau stresses the importance of embracing change and moving forward as a nation.
- He advocates for a focus on real American history rather than mythological narratives.

### Quotes

- "These statues aren't about history."
- "Our most important heritage is the machinery for change."
- "It's time to move forward."
- "You're the person holding it back."
- "We should probably focus more on American history, real history, rather than mythology."

### Oneliner

Many statues mythologize rather than depict history, hindering progress; real American history is about embracing change and moving forward.

### Audience

History enthusiasts, activists

### On-the-ground actions from transcript

- Preserve statues being taken down for historical significance (suggested)
- Focus on teaching real American history in educational settings (implied)

### Whats missing in summary

Beau's engaging delivery and passion for authentic American history are best experienced in the full transcript.

### Tags

#History #AmericanHeritage #Statues #Change #Mythology


## Transcript
Well howdy there internet people, it's Beau again.
So today I want to respond to a question I've gotten a couple times, and basically the question
goes like this, Beau, you're obviously a huge history person.
How can you possibly be okay with these statues being torn down?
They're erasing history.
They're destroying our history.
No they are not.
No they are not.
Most of these statues, the overwhelming majority of them, are not historically significant
in any way, shape, or form, nor are they historically accurate in any way, shape, or form.
They are not part of history.
They are there to mythologize.
They are there to create a mythology about the leaders of the United States.
Those major historical figures.
They're there to rehabilitate their image in some cases.
They're there to honor them.
So they're only teaching one side of the story.
And then when you talk about those in the South, the statues down here in the South,
most of them weren't even put up for that reason.
Most of them were put up at a specific point in history, and they really were put up as
a way of sending a message to a certain group of Americans that basically boiled down to,
y'all better stay in your place.
Yeah, I'm totally cool with those being taken down.
I have no problem with it.
One of the issues that the U.S. has is that most Americans know American mythology, not
American history.
And many of these statues help create that, because they only tell the one side of the
story.
They only tell the good parts.
And that leads people to believe that the U.S. doesn't have problems.
And failure to recognize those problems leads to, I don't know, cities burning.
We should probably work on that.
A good example, an interesting example, is the one that got taken down in D.C. recently.
Albert Pike.
This is one I've seen myself a number of times.
I am fairly certain that the plaques there, they mention his public works.
They mention his fraternal organizations.
They mention all the good things that he's done.
They don't mention that he was a Confederate officer that resigned under some pretty unique
circumstances.
They don't mention that most civil rights organizations believed that he was a member
of that group with the white sheets.
That's not mentioned.
So it's not history.
It's a memorial.
And on some levels, that's okay too.
It's okay to mythologize to a certain level, but you can't confuse it with history.
Every nation has a mythology.
That's fine.
But it's not history.
The two things aren't the same.
Somebody I think that would be worthy of mythologizing, because they were really important in the
founding of this country, somebody that I'm okay with statues of, Thomas Jefferson.
Now if you want to make it a historical thing, you would have to include the fact that while
he completely knew that slavery was wrong, immoral, unethical, all of these things, and
he knew that it was going to cause problems in the United States, he referred to it as
a hideous blot.
He didn't like free all his slaves though.
Historical figures are flawed.
They are.
But if we ignore that they are flawed, we get this idea that the U.S. was founded without
sin.
The U.S. was founded on some pretty messed up stuff.
The thing that makes the U.S. unique is that the founders knew they were flawed, and they
included the machinery for change.
That's one of our most important heritages as Americans.
The machinery for change.
People say that all the time.
Well that's in the Constitution.
You can't change that.
Article 5 says we can.
One of the more important pieces of American history is the machinery for change.
The idea of including more and more people under that umbrella of freedom.
The idea that all men are created equal.
And yeah, when it was founded, all men had a really narrow definition, but it expanded
because of that machinery for change.
That's our heritage.
That's the good heritage.
Personally, I think a lot of these statues should be preserved.
They should be taken down and preserved.
Those that were taken down the way they've been taken down the last few weeks should
be preserved as is.
Because those will be historic.
Because it's that defining moment when the American people are moving forward.
It's another piece, another gear in that machine of change.
So yeah, they should be preserved out of sight.
Because one day they'll probably be in museums.
Because this is the time it happened.
It's important.
We need to forget about the idea that the U.S. can't change.
We need to drop that concept.
Our whole history is the U.S. changing.
And it's time for another one.
It's time to move forward.
These statues aren't about history.
So if you're making that argument, you're wrong.
These statues aren't really about heritage.
Because our most important heritage is the machinery for change.
So if you're making that argument, not just are you wrong, you are standing in the way
of the most important heritage this country has.
And that's moving forward.
You're the person holding it back.
We should probably focus more on American history, real history, rather than mythology.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}