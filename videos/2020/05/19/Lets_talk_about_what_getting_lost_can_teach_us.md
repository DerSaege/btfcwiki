---
title: Let's talk about what getting lost can teach us....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tXn6eKsy4Ks) |
| Published | 2020/05/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the idea of getting lost and what it can teach us as a country and a world.
- In search and rescue, the first thing instructors teach is that lost individuals tend to do nothing to help themselves.
- There is a strong impulse to keep moving once someone is lost, which Beau attributes to various factors such as evolutionary instincts or optimism.
- Beau advises that if you get lost, the best course of action is to sit down for 20-30 minutes to analyze the situation and come up with a plan.
- Even skilled individuals like Ralph Bagnold, a desert explorer, have felt the urge to keep moving when lost.
- Moving without a plan can lead to misguided decisions, like following the sun as a navigation strategy.
- Beau draws parallels between being physically lost and the state of the country and world, noting that many people recognize the current system is not ideal but continue to move without a clear plan.
- He urges for a collective pause to come up with a plan and choose a course for the future that prioritizes cooperation over competition.
- Beau stresses the importance of making a conscious choice to advocate for cooperation and shift towards a more sustainable path.
- He concludes by underlining the critical need to address the state of being lost at a national, global, and species level, advocating for proactive change.

### Quotes

- "We're lost. We're lost as a country. We are lost as a world. We're lost as a species."
- "We need to come up with that course, and we need to get moving."
- "This is not sustainable. It's not going to last."
- "If we would sit down and come up with a plan, we'd be able to chart a course."
- "We need to make the conscious choice to start advocating for that."

### Oneliner

Beau introduces the concept of getting lost, paralleling it with the directionless state of the country and world, urging for a collective pause to plan a cooperative and sustainable future.

### Audience

Global citizens

### On-the-ground actions from transcript

- Pause collectively to analyze current situations and challenges, then develop a plan for the future (exemplified)
- Advocate for cooperation over competition in all aspects of society (exemplified)

### Whats missing in summary

The full transcript provides a thought-provoking analogy between getting physically lost and the directionless state of society, urging for proactive planning and advocacy for cooperation to overcome challenges.

### Tags

#Lost #Cooperation #Sustainability #CollectiveAction #Advocacy


## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to talk about getting lost.
And what getting lost can teach us as a country
and as a world.
You know, if you study search and rescue,
one of the very first things they will tell you,
your instructors will tell you, that the lost person
will always consistently do one thing and one thing only,
nothing to help themselves.
There's an overwhelming desire, once you get lost,
to keep moving, to keep moving.
I've read about it for years, trying to figure out what it is
and pin it down.
Some people chalk it up to an evolutionary thing,
where if you don't know where you're at,
you must be in danger, therefore you keep moving.
You have this drive to keep moving.
Some people say that it's the optimist in people.
Right over that hill, that's where I need to be.
So they keep moving.
If you don't know, that's wrong.
OK, sit down.
If you get lost out in the woods, sit down.
Just sit down.
20, 30 minutes, if you're skilled at navigation,
you'll be able to analyze the problem.
And you can work out a plan.
If you're not, odds are, if you've
done everything else you're supposed
to as far as letting people know that you're
going into the woods and that kind of stuff,
somebody will find you.
Somebody with a plan will find you.
Because those search patterns that they use,
they kind of work best if you're not moving.
But that's not what people do.
Even skilled people.
There's a guy named Ralph Bagnold, a British guy,
desert explorer, founded the British Long Range Desert
Patrol thing.
Man knew his way around the desert.
He got lost.
And he even said that he was just overcome with the desire
to keep moving.
The fact that he didn't is the reason we know the story.
The thing is, when you start moving
and you don't have a plan, you come up
with things that sound like a plan.
Like, I'm going to follow the sun.
Yeah, that's cool if you start at midday.
That'll keep you in the same direction.
But if not, well, you'll go right for a little bit.
And then you'll go left, unless it's directly
overhead when it passes.
Then you could end up back in the spot where you started.
A lot of the little tricks you hear about,
they don't actually work the way people think they do.
So that's where we're at as a country and as a world, too.
We're lost.
Most people know that the system that we have
is not what they want.
But we keep moving.
We know it's wrong, but we keep moving.
Sometimes we go right.
Sometimes we go left.
But we keep moving.
If we would sit down and come up with a plan,
we'd be able to chart a course.
And we'd be able to get there.
But we don't.
We keep moving.
We keep arguing over the same mostly irrelevant points.
Most people know that this isn't what we want.
Now, here's the thing.
We've sat down.
Right now, we're sitting down.
Everybody's paused.
We can come up with a plan.
Now is when you need to decide what kind of course you want.
Where do you want to end up?
Do you want to end up on a route where, once again, everybody's
in competition?
Where everybody's on the brink?
Where anything can bring us to our knees?
Or do you want to get on a route that's filled with cooperation?
If you know anything about survival,
cooperation's normally the right way to go.
We need to make the conscious choice
to start advocating for that.
We need to make the conscious choice
to get on a course that puts us where we want to be,
not just a little bit to the left of where we were.
We need to get moving because we are lost.
We need to come up with that course,
and we need to get moving.
We are lost.
We are lost as a country.
We are lost as a world.
We're lost as a species.
This is not sustainable.
It's not going to last.
And we've got to figure a way out.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}