---
title: Let's talk about Eric Trump's comments last night....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DQ8uG6OghyE) |
| Published | 2020/05/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses Eric Trump's statement on Democrats shutting everything down to prevent President Trump from having rallies.
- Questions the level of coordination and sacrifice needed to pull off such a feat.
- Points out that if President Trump wants to hold a rally, he has the authority and resources to do so.
- Criticizes President Trump's dismissal of the pandemic and belief that it will disappear magically after the election.
- Raises concerns about the danger and recklessness of downplaying the seriousness of the pandemic.
- Challenges the notion of magic solving the current crisis and stresses the importance of leadership.
- Expresses worry about the potential consequences of President Trump's handling of the pandemic.
- Calls for focus on leadership rather than relying on magic to address the ongoing crisis.
- Suggests that President Trump's campaign team should prioritize saving lives over rally attendance.
- Concludes by sharing thoughts and wishing the audience a good day.

### Quotes

- "Leadership is not magic."
- "I'd be less worried about his ability to put 50,000 people in an arena every couple months, and more worried about his ability to put 50,000 people in the ground every month."
- "If he wants to hold a rally, he can. Nobody's gonna stop him."
- "I'd say it's reckless."
- "He went on to say that after the election, this [pandemic] would magically disappear."

### Oneliner

Beau challenges Eric Trump's claims, questions pandemic response, and stresses the importance of leadership during crisis.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Call for responsible and transparent leadership in times of crisis (implied)
- Prioritize public health and safety over political rallies (implied)

### Whats missing in summary

The transcript delves into the manipulation of information and the potential consequences of dismissing a serious public health crisis.

### Tags

#EricTrump #PandemicResponse #Leadership #PoliticalRallies #PublicHealth


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're gonna talk about
what Eric Trump said last night
on that network that sometimes passes for news.
He started off the highlight reel
by saying that the Democrats did this.
They got everything shut down.
So Biden could stay at home
and not embarrass himself in public.
And while they were doing that,
they simultaneously deprived the president
of his greatest asset,
which is his ability to fill an arena
with 50,000 people every couple months.
The Democrats did that.
Shut everything down,
along with half the world,
to stop daddy from having a rally.
I hope he understands that.
It sounds a little bit off.
But let's just say that it's true for a second.
Let's entertain this idea.
The Democrats and half the world got together,
shut down the economies,
hundreds of billions of dollars lost,
all to stop daddy from having rallies.
I would suggest that that level of coordination
and that level of sacrifice
just demonstrates exactly how bad a president he is.
I don't know that I would be pointing that out.
I would also like to say
that he's the president of the United States.
If he wants to hold a rally, he can.
Nobody's gonna stop him.
He could do it all at the White House,
do it at Lafayette, across the street.
Go ahead.
Pack your voters in there.
Have them stand real close to each other.
And that's actually why I'm irritated.
He went on to say that after the election,
this would magically disappear.
Once again, implying that it's not real on that network,
a network whose audience is known for being,
let's just say, easily led.
I would suggest that that's dangerous.
I'd say it's reckless.
But I'm curious, what kind of magic?
The same kind of magic
that was supposed to make it magically disappear last month?
Or is it the new kind of magic
that's gonna get these vaccines by the end of the year?
Or is it that ever-elusive third kind of magic
that may appear after the election?
Leadership.
I'm hoping it's that.
Leadership is not magic.
It can be displayed by anybody,
except for this president, apparently.
If I was on President Trump's campaign team,
I'd be less worried about his ability
to put 50,000 people in an arena every couple months,
and more worried about his ability
to put 50,000 people in the ground every month.
Because that's what the rest of us are...
that's what we're looking at.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}