---
title: Let's talk with Trae Crowder the liberal redneck....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=a3SRXCCwdeI) |
| Published | 2020/05/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introducing Trey Crowder, the liberal redneck comedian with substance behind the jokes.
- Trey explains his journey from standup comedian to creating videos with a political twist.
- Trey's inspiration to start making videos came from witnessing a viral far-right preacher video.
- The success of Trey's videos led him to quit his day job and pursue comedy full-time.
- Trey's authenticity and genuine upbringing in a rural, poverty-stricken part of Tennessee influence his content.
- Beau and Trey bond over the experience of hiding their accents and eventually embracing them.
- They address stereotypes about Southerners and the importance of debunking misconceptions.
- Trey talks about his book, "The Liberal Redneck Manifesto: Dragging Dixie Out of the Dark," co-written with his friends.
- Despite the success of the book, Trey feels disconnected from that period of his life, which was a whirlwind of change.
- Beau and Trey touch on how Trey's comedic work has had political ramifications and sparked meaningful dialogues.
- Trey shares his mixed feelings about being seen as an activist rather than solely a comedian and the expectations that come with it.
- They dive into Trey's intent behind his work, focusing on challenging stereotypes and showcasing the diversity in the South.
- Trey stresses the importance of not assuming extreme viewpoints in dialogues with others to foster better relationships and understanding.
- Trey talks about the challenges of the current standup comedy scene due to the pandemic and focuses on his upcoming projects.
- Despite uncertainties, Trey remains optimistic about progress and encourages everyone to soldier on.

### Quotes

"Write what you know and that's pretty much all I've ever done."  
"People should be aware that the image of Southerners isn't accurate at all."  
"I always wanted to be a comedian. Having said that, I think it's cool if my work reaches people and starts a conversation."

### Oneliner

Beau chats with Trey Crowder, the liberal redneck comedian whose authentic content challenges stereotypes and sparks meaningful dialogues, fostering understanding and showcasing the diverse perspectives in the South.

### Audience
Comedy fans, Southern residents

### On-the-ground actions from transcript

- Listen openly and without assumptions to bridge divides (implied)
- Challenge stereotypes and misconceptions about Southerners (implied)

### What's missing in summary
The full transcript provides an in-depth look at Trey Crowder's journey from standup comedian to creating impactful political content, showcasing the importance of authenticity and challenging stereotypes. Viewing the full transcript allows for a deeper understanding of Trey's motivations and the evolution of his work.  

### Tags
#Comedy #South #Authenticity #PoliticalContent #ChallengingStereotypes #Progress


## Transcript
All right, well, howdy there, internet people.
It's Bo again.
And today we have the liberal redneck,
a comedian of sorts, I guess.
Somebody that when my wife told me about,
she's like, hey, you need to watch this guy.
He's the liberal redneck.
And I'm like, yeah, I've seen the act before.
Not really that concerned.
And then I tune in and it's a little bit more than that.
There's some substance behind the jokes
and we're here to talk to Trey Crowder.
How you doing today?
I'm doing all right, Bo.
Thanks for having me.
I hate to start on a technical question, but I'm going to.
I could see you a minute ago and now I can.
Is that what was supposed to happen?
Well, you shouldn't be able to see me.
Should, I should be able to?
You should.
Yeah, I can't.
I did, as soon as you started recording,
your picture went away.
And now it's just a little icon
that has your initials on it.
It says BF.
I don't see your picture at all.
I mean, I can handle that,
but I didn't know if it would mess anything up for you,
but I can't see it.
Nah, I mean, we can go without my picture.
Everybody's seeing me.
All right.
Okay.
Like, the audio sounds okay though, right?
Yeah, yeah, I can hear.
All right.
Okay, so how did this start?
I mean, did you just decide that,
hey, I'm gonna mask some political content and comedy
and go from there?
Where did this start again?
Well, I'm actually like,
I was a comedian before, a standup comedian.
I'm primarily a, if you ask me,
if I'm filling out a form,
as for what your job, what your occupation is,
I write down writer, comedian,
and I always wanted, pretty much always,
ever since I was a kid, wanted to be a comedian.
And as soon as I got old enough to even start thinking
about what types of comedy I would probably do,
all of my favorite comedians when I was growing up
were the people that were like social commentators,
people that talked about things that were happening
in society, like actual issues and stuff.
So my biggest, my number one favorite,
and the guy who was the biggest influence on me probably
was Chris Rock, but like Chris Rock, Dave Chappelle,
George Carlin, Richard Pryor,
all of those types of comics were always my favorites.
So when I first started doing standup in 2010,
I immediately was doing,
I would talk about what I considered like, you know,
real things, like I would talk about racism or homophobia,
or I'd talk about the Bible or whatever on stage
from the very beginning.
And then as far as the videos go,
I had this standup bit that I did on stage
that was basically what the videos ended up being.
It was all about, the premise of it was,
I hate how every time you ever see somebody
that looks and sounds like me on TV,
it's always the same shit.
It's the same Bible thumping troglodyte, you know,
with a misspelled sign about how Jesus hates big government
or whatever, it's always the same thing.
So I was like, I need to,
I'm gonna start fighting fire with fire.
I'm gonna start going in public and being just as loud
and crazy and redneck as they are,
but I'm gonna say a bunch of my, you know,
liberal stances to balance the scales.
And then I would just say a bunch of liberal shit
in a really redneck fashion.
And I was doing that on stage.
And I talked to people, other comedian friends
and stuff at the time,
and balanced the idea around of like making videos
based around that bit or that character.
And everybody I ever talked to was always like,
yeah, that's a good idea.
You should definitely do that.
But I still just kept,
I kept not doing it because I thought,
oh, I need to save up money and buy a fancy camera.
I need to learn how to edit and all this stuff.
It seemed like a barrier to entry for whatever reason.
And then one day in April, 2016,
when North Carolina passed the HB2 law,
I saw a video that was going viral on Facebook
on the far right.
It was like people I went to high school with
were sharing it.
That's how I saw it.
And it was this preacher in North Carolina
who was just standing in the woods for some reason,
standing beside his,
just standing beside his big Dodge Ram,
just holding his phone and just hollering at it,
preaching fire and brimstone about the evils of,
you know, perverts in the bathrooms with our little girls
and all this stuff.
And not a dick joke inside.
Wasn't funny at all.
Like, it wasn't even trying to be funny.
Wasn't even trying to be funny,
just straight up fire and brimstone.
And it had 15 million views.
And I saw that and I was like,
it's like a light bulb went off.
I realized like, oh man,
if this type of thing is what I'm trying to make fun of
or satirize, you know, and it is,
then I don't need any fancy equipment.
I don't need it to look fancy.
As a matter of fact, that would be a mistake.
Like what I need to do is do it exactly the way
this guy does it.
Just go out back and holler at my phone.
And that's the style, you know?
And as soon as I had that realization,
I made the first Live or Redneck video
like two or three days after that.
And the first one got like 70,000 views or something.
And I was over the moon about it.
I was like, okay, I guess I'm onto something.
I'll keep doing it.
And the second one that I made was about the HB2 law.
And it went like crazy viral.
Ended up getting like around 30 million views
on Facebook or something like that.
And just like took off insanely.
And my like life and my comedy career
and all that stuff literally changed overnight
as a result of that.
I still had a day job at the time.
All that allowed me to quit my day job
and go into comedy full-time and go on tour.
And that's what I've been doing for the past four years.
So that's kind of how it got started.
And I'd like to think that that North Carolina preacher,
someday, somehow, one way or another,
if he knows about me at all or ever sees any of my stuff,
I hope he knows that like, this is all his fault.
Or I guess it's God's fault.
I don't know, you know, depending on how he looks at it.
But you know, mysterious ways, right?
But yeah, it was the direct influence on me.
But as far as the content of it, it's just,
I mean, it all comes from a very authentic place.
Like I grew up, as long as I've ever had
any political opinions at all,
they've always been progressive.
And I grew up poor in a rural,
poverty-stricken part of Tennessee,
a very stereotypically redneck area.
So like, I've just always kind of been that guy.
So like, really, I'm just right.
And they say, write what you know.
And I mean, that's pretty much all I've ever done.
Like it comes to me, I come by it all very honest, for sure.
Like I didn't really have to sit and plan
or contrive any of that.
It just, you know, it comes from a very genuine place for me.
Yeah, it comes through.
I can see that.
And it's funny because one of the things that I watched
was talking about people hiding their accents
when they leave, you know?
And that's something I did for a long, long time.
And this channel actually, you know,
didn't see any success until I actually,
kind of the same thing,
started as me making fun of myself
for the fact that I hid it for so long,
once people found out about it.
And then it, yeah, it kind of blew up.
But you also bring up a point in one of them
where you're talking about the percentages, you know,
because there is that image.
Anybody that has a beard, wears a ball cap,
sounds remotely like we are hard right,
like ultra conservative bigots, blah, blah, blah, blah.
But the percentage of votes doesn't really show that.
And you brought that up.
And it's something that I think,
because I have a lot of international viewers,
I think it's something that people should be aware of
is that that image isn't accurate, like not at all.
Right.
I mean, just liberal Southerners tend to be more quiet
about their beliefs because it isn't the majority viewpoint.
Right.
Yeah, yeah.
You know, when all that first happened,
I got so many messages.
I met so many people in person who said verbally to me,
some version of like,
oh, you're, you know, you're like seeing a unicorn, right?
Or like, oh, I didn't know,
I didn't know that people like you existed.
So obviously this is all coming from people
like outside the South, but like, no exaggeration,
people that literally believed
that every single white Southerner, you know,
that at least was Southern enough that had the accent
that you could tell they're from the South,
but every single one of them was like, you know,
a Christian conservative or whatever.
And like, to me, that's so absurd
that anyone could actually think that,
but like people really did,
and people still do really think that.
Not everybody, I live in California now,
plenty of people do not look at it that way,
or plenty of people just don't give a shit
one way or another, but it's very much a real thing.
And I was aware of that for a long time
before I ever even started comedy.
I knew that was a thing, and it always annoyed me
because again, I've never fit that stereotype
in that way ever, but I've always sounded
and everything like I would.
So it's something that I've always been like sensitive to
and annoyed by, so, you know, try to set people straight.
Yeah, and I actually think it's a service.
And again, yeah, I was the same way.
I hid my accent, I was a journalist,
I was drinking one night and it came out
and they were like, oh my God, that's me.
But-
Well, you had to probably, right?
Like, cause here's the thing,
I never did hide or get rid of my accent,
but I don't at all like blame people who do
or hold it against them because I understand
the reasons why you would.
And I know that like in a lot of careers
or industries or whatever,
and I'm assuming journalism is probably one of them,
like, you know, you almost have to, you know?
But I just, I always had such a chip on my shoulder
about it ever since I was a kid,
because we had a lake in my hometown
and people from the North had boats on that lake
and would come down there in the summer.
So it was like a small little tourist town
and all the tourists were from up North
and like everybody in my town knew,
like the dynamic was, you know,
we were all the dumb ass backwards, local yokels to them.
You know, they all look down their noses at us
and stuff like that.
And so like, I've never not been aware of that perception,
but instead of like trying to change it,
it made me, you know, like the red, the red ass in me,
you know, like, we're just like,
fuck them, they don't know me.
You know what I mean?
Like, just cause I talk this way, don't mean shit.
They can kiss my ass.
Like, that was my attitude about it always.
Like, I ain't gotta change my accent
just for these motherfuckers.
Like, and I've just, that was always my sort of stance
as it were.
And you know, I'm very glad that I didn't
because I would be remiss, it would be disingenuous for me
not to acknowledge that my accent has been a huge part
of all this for me and my whole thing, like without a doubt.
So, you know, ended up working out,
but I don't blame anybody that goes the other way.
Oh yeah.
And that's, I've had the same experience.
I mean, that's the funny part about it is
it started as a joke,
but now I basically do the same thing I did as a journalist
just in my shop, you know, right there.
Yeah.
It's unique because people don't know
that there is a wide diversity of thought in the South.
Right, right.
So tell us about the book.
Don't you have a book?
Yeah.
So two of my best friends that I knew
from doing standup already before all this,
Corey Forrester and Drew Morgan,
whenever we all had the same manager
before the videos went viral,
we'd all been doing stuff together
and had been good friends for a long time
before the videos went viral.
And so after they did, immediately we were like,
hey, let's go on tour.
We always wanted to go on tour.
And it seemed like an opportunity to,
so that started pretty quickly.
And around the same time, very, very early on,
like we got contacted by a literary agent
who was like, I think we could sell a book.
And then we did, and it all happened very quickly.
And this is 2016 and they wanted,
the agent and the publisher and everybody
very much wanted this book to be out before the election,
just because it was like gonna have political themes to it.
And so what that meant was we had an insane timeline,
unlike, it's very unusual for a book to have,
it was like six to eight weeks or something
for the bulk of the draft of it,
which necessitated the three of us
sort of co-writing it together in order to meet that.
So the three of us wrote this book,
it's the Liberal Redneck Manifesto,
colon, Dragging Dixie Out of the Dark.
I'm gonna be honest with you,
we did not come up with that title.
I've always had mixed feelings at best about that title.
That was the publisher's call, but it is what it is.
But either way, what the book basically is,
is it's just an analysis and a topic by topic breakdown
of like the stereotypes and perceptions.
And then on the other hand,
the actual realities of what people think about the South
and what the South is.
So in each chapter, it's broken down by chapter,
each chapter is a different topic.
So there's like a chapter on guns,
on gun culture in the South,
there's a chapter on race and race relations in the South.
And then there's more lighthearted Southern subjects too,
like there's one on food and there's one on music,
but then you got, there's one on religion,
there's one on Southern women,
like the whole Southern bell thing,
what that's really about.
There's one on opioids.
So it really like runs the gamut,
but it's all the things that we thought were like
the sort of cultural touchstones
or however you wanna put it of the South.
And it was, on one hand,
what do people think that it's like
and that we are like where this is concerned
versus what is it really like?
And also, what could we do better?
What needs to change?
That kind of thing.
We all three of us are very happy,
again, that was 2016.
So I mean, it's been,
four years ago, we wrote that book
and it feels like a lifetime ago.
It seems like another person did it almost.
But having said that, we were definitely,
we've always been proud of how it turned out
and feel good about it.
And it got, it was very well received.
People seem to like it.
It got good reviews and all that type of thing.
So I mean, I feel good about the book,
but yeah, it also feels like something
from a past life almost.
It feels like it was so long ago.
I mean, seriously,
cause it was like,
that's the most insane period of my life,
bar none to date.
And I can't imagine that it will ever be topped.
I can't imagine there will ever be a more insane
brief window of my life
because like the tour, the book, everything,
quitting my day job,
like it was,
and it's all a blur looking back on it is my point.
And part of that blur includes writing this book.
But yeah, we're proud of it.
We feel good about it.
So let me ask you this.
I mean, your intent is comedy,
but how does it make you feel
to know that your work,
like the Charlottesville video,
that is actually gets used to reach out to people.
It does have political ramifications.
I mean, people actually use that as a tool
to reach out to others.
I mean, I'm just curious how you feel about that.
It's weird, honestly,
I go back and forth on it.
I'm kind of on two minds about it.
Cause again, I always want to be a comedian.
I never wanted to be any kind of like activist
or fucking provocateur or any kind of shit like that.
Like I just wanted to be a comedian.
Having said that, I mean, I think it's cool.
Anytime you can actually like reach somebody
or start a conversation or whatever is great.
I'm glad I have the ability to do that
because like, again, it goes both ways.
Cause on the one hand, a lot of comics get frustrated
because they feel like they can't ever be serious
about anything if you're a professional comedian.
And that's never been a problem for me.
It's totally okay for me to be serious.
People aren't put off by that.
It doesn't, like I can get away with being serious
in a way that a lot of comedians can't
because of my whole thing.
And that's an asset and that's great.
But the flip side of that is,
I'm a comic at heart and everything.
And like, man, sometimes I just want to,
you know, just make some dick jokes
or talk about my wife or whatever,
just talk about silly shit.
You know, talking about my sons,
I have two young sons, talk about them
and all the fart jokes they make or whatever.
Like just, you know, silly,
observational daily life type stuff
that doesn't have any weight to it and isn't supposed to.
I, you know, I definitely feel sometimes like,
not that I can't do that,
but that that's not what anybody wants or expects out of me.
Like, I definitely feel kind of a,
I feel like a weird obligation to like, you know,
talk about things like always.
And I don't mind it, but sometimes it would be nice,
you know, to be able to just do some silly shit for a while.
But don't get me wrong,
I'm grateful and I wouldn't change any of it.
But it is kind of a complicated dynamic, you know, for me.
Right.
Yeah, I can see that.
I mean, my viewers are used to the whole, you know,
every once in a while I will just do something
very satirical and, you know,
actually come out at a far right person
and make an entire video about it.
And it confuses newer viewers,
but eventually they catch on.
I can definitely, there is a pressure to turn out
the kind of content that the people
who normally support you.
I mean, that makes sense.
Right.
You know, for me, looking at your stuff,
it's one of those things where I actually couldn't tell
whether or not you set out to be political
or you were just funny, you know?
I wasn't really sure what the intent was.
I mean, the end result is that you have content
that can make people question what they believe.
And that's, I mean, to me, it's just.
Yeah, well, the thing that I think a lot of people
may not understand is that, like, as far as,
first of all, I don't really consider myself
to have a, like, mission or anything.
But, like, people ask me a lot about, like,
my videos changing people's minds about things and whatnot.
And normally, like, the vast majority of the time
when I get that question,
they specifically mean, like,
stereotypical rednecks in the South
changing their mind about, like, progressive issues.
That's what they're asking me.
And, like, yes, I know that that has happened.
I have talked to people, and people have told me, like,
that has happened at least a couple of times.
But it's not like that's not that common.
What is way, way, way more common
is hearing from people from outside the South
and everything who say that, like, my videos are,
influenced them and changed their mind
about, like, how they looked at
and felt about the South in general.
Like, made them realize that, oh, they're not all the same,
or the South's not a monolith or whatever.
I've gotten that kind of response, a shitload.
And, like, that's important to me.
Like, because, like I said, like,
growing up in that tourist town, like,
really, it all kind of comes back to that for me.
Like, that's what's at the core of all of it, really, for me,
is more so than any one specific political issue or whatever.
It's getting across to people or illustrating for people,
like, hey, we're not all the same, you know?
Or, like, it's, you know, the South isn't a monolith.
There is diversity.
Like you said, there's diversity of white thoughts
and opinions, too.
It's not just between, you know,
black and whites in the South or whatever.
There's, like, it's way more complicated than you realize.
And don't assume things about people
just based on where they're from, you know?
And getting all of that across is really sort of, like,
the foundational thing that I feel like most of my stuff
and most of my work comes from, really.
Both, like, stand-up bits and all that,
but also, like, I've been trying to get a TV show made
for, you know, four years now or whatnot.
I'm still trying.
And it's like, with that, it probably
won't be hardcore political in, like, a topical way.
But it will absolutely be based around trying
to show a different side of the South
than you would typically see on a TV show.
Because everybody knows, you know,
it's usually some version of the same thing.
You know, Beverly Hillbillies, blue collar, comedy tour,
something in that vein.
And just doing, just simply doing something
that is very, very Southern, but also not that.
Very authentically Southern, but coming from a different place
that people haven't seen before.
Like, just that alone is, like, is a huge goal of mine
and, you know, is very important to me.
So, really, it all kind of comes back to that for me,
more so than any one, like, progressive talking point
or anything like that.
Although, I do believe all those things.
Yeah.
Yeah, well, I mean, it seems, yeah,
I don't think anybody doubts that it's,
that you actually do that.
I don't think, I've never seen anybody say,
oh, it's just a bit, and that those aren't really your views.
But, so, and this is kind of unfair.
I wish I had warned you about this one,
because normally the people who come on are activists.
They are people who set out to do that.
I always have one question I ask everybody.
Okay.
That is, if you could give the people watching this,
one piece of advice as far as how they can change
their community and better their community,
what would it be?
I don't go into any kind of discourse
with someone who you know is on the other side of you,
automatically assuming from the very beginning
that they are on the extreme polar opposite end
of the spectrum from where you stand on it.
Because I think that people do that a lot in America
and maybe everywhere.
But like, I'll give you a specific example
from talking to my buddies back home.
I still go back home every summer and go out on the lake.
I can't this year, because the world ended and all that.
But, you know, as soon as we can go back, I will.
And see all my buddies I grew up with and everything.
And a lot of them are like me.
A lot of them are totally on the same page as me,
politically.
And then, you know, some of them are not,
but we've grown up together.
We're really good friends.
Some of them are conservatives.
One of those trips, one of my good friends comes up to me
and we're standing there drinking beers or whatever.
He walks up and he's like,
Trey, I just like, my thing is,
I just don't understand why, or he goes, he's like,
you know me, you've known me your whole life.
You know, I work full time, I'm married, I'm a family man.
I got two little girls, you know,
I never broke the law, yada, yada.
He's like, I just don't understand why you think
that I shouldn't be able to own a gun.
And I was like, I don't think that Kobe.
Like, I don't, like, I don't at all think that,
like even a little bit.
I was like, you absolutely should be able to own a gun.
I was like, all I think is that there are plenty of people
in this country, plenty of people in this country
who probably shouldn't.
And we should, you know, try to take steps to keep insane
or violent people or whatever from being able to have a gun
because there are plenty of people who shouldn't have one,
but you're not one of them.
And I said that and he was like,
oh, I mean, yeah, I agree with you about that.
He's like, definitely, there's definitely some people
that don't, you know, shouldn't have a gun or whatever.
And so we ended up realizing or finding out that like,
we actually were like this far apart, like barely,
if at all, like we were pretty much in agreement
about how we felt about guns,
but we started it from this position of like, you know,
he thinks that I wanna take everybody's guns.
And I think he wants to give every eighth grader
an assault rifle, you know what I mean?
And neither one is true.
And I think that people do that a lot.
You find out that somebody is on the other side
of an issue from you and in your mind,
you automatically put them in the extreme
when most people are not extreme in their views, just period.
And I think that that skews a lot of the conversations
and stuff from the very beginning.
They can't even get started
because people start from the wrong place.
So I feel like that's some advice I would give people.
Just try to keep that in mind, you know,
as you're going about your life and dealing with people.
And I think that it will lead to, you know,
better relationships and things actually getting done
and all of that good shit, I would hope.
Sounds good.
So what's next?
What projects do you have to plug?
What are you doing next?
Do you have a tour?
Well, probably not right now.
No, yeah, no, standup is, yeah, standup comedy is illegal.
And, you know, if we're being honest,
if we're being honest about it, I mean,
when you think about it,
live standup comedy requires a, you know,
large number of people to be put close together
in a relatively small space
and live standup comedy, let's just be frank here,
really could not be less essential, right?
So like, I think, I hope not.
I would love for the tour to kick back up
as soon as it's actually safe and all that.
But like, I personally think it's gonna be a long time
before we can do live shows again.
And that sucks, but you know, it's out of our hands.
Nothing I can do about it.
So the tour is suspended indefinitely
until, you know, until things hopefully go back to normal.
So with that in mind, and again, like I said,
I've been trying to get these TV shows made.
I've sold multiple shows and written multiple scripts
that just didn't reach the finish line, didn't get shot.
I had a couple, I had some that I was still working on.
Those are also now just, you know,
put on pause until things go back to normal.
So we have, me and the guys I mentioned earlier,
Corey and Drew, have our podcast
that we've been doing this whole time,
the Well Red, that's Well, R-E-D, Well Red podcast.
It comes out every week.
We're still doing that via Zoom.
Still, you know, making videos and stuff,
trying to do some different things now with the lockdown,
some like Zoom sketches and whatnot,
and just putting them out on the social pages.
And the only like real, like legitimate thing
I have to plug is I have a Facebook watch show
with this media company called Attention Media,
and the show is called South and Off.
And it's kind of similar to the format of the book.
It's like each little episode, like five minute episodes,
and each episode is about one particular subject,
and it's some Southern specific, you know,
topic or phenomenon or perception or whatever.
And we did one season last year,
and we just filmed on quarantine.
Like I said, this little studio in my house out here
in Burbank and filmed it all myself,
and animating it and graphics and all that.
But we're putting that out starting next week,
South and Off season two.
And so like this year, there's topics from like,
you know, the myth of the Southern Bale,
coal country, conservative versus conservation,
diversity, Southern diversity, like you mentioned earlier,
that's the subject of one of the episodes,
and Southern innovations, contributions
that the South has made to like, you know,
America as a whole.
So just, and then a few more.
So those are some of the subjects that are covered,
and those will come out weekly starting Monday,
the, what is that, the 11th, next Monday.
So South and Off season two.
And then after that, man, hell if I know, I don't know,
hopefully something will come up.
I'm just trying to keep busy,
but mostly I'm just sitting around
and trying to keep my wife and I from losing our minds
with these children who can't go to school, you know,
anymore, trying to homeschool them
and just maintain our sanity, basically.
Right, yeah.
No, I definitely, definitely can relate.
We had the same thing.
We had something planned this summer to go on tour,
and we, the whole thing just kind of fell apart.
And I'm like, well, now I don't know what I'm going to do,
which is nice on one level, you know,
because I'm still out in the country.
I don't have to go anywhere.
Yeah, I don't mind.
I honestly don't mind being at home.
You know, I love my family.
I love being at home with them,
but yeah, the uncertainty of it all
and not knowing, you know, what is going to happen,
but hell, everybody is in that position right now, right?
Nobody knows what's going to happen.
So, you know, I'll be all right.
All right, so you got any final thoughts
you want to get out there?
I don't know.
Just, I guess everybody just try to, you know,
keep your heads down and soldier on.
Like, I'm, weird people might not pick up on this
or might expect otherwise, but I actually am like,
ultimately an optimist about things,
and I always have been.
And I think part of that is because like,
I grew up in the rural South, like, look,
the rural South has still got a lot of work left to do
and a lot of progress left to be made,
but I don't care what anybody says.
Just in my lifetime,
it has changed an insane amount for the better.
I mean, you know, like I never ever would have believed
that at this point in my life,
like when I was a teenager or whatever,
that gay marriage would even be legal,
let alone like, you know,
kind of almost normalized at this point,
which is a good thing.
But I never, if you'd have told me that when I was 18,
you're not living in Salina, Tennessee,
I told you, you're out of your goddamn mind.
Like, and, but, you know, it's happened.
And people in my hometown, you know, are not,
they've come a long way.
And I know that for a fact,
and I believe that ultimately one way or another,
we will continue to progress in the right direction,
little hiccups and speed bumps aside.
So, you know, I just think everybody just hang in there
and we'll get it figured out eventually.
Sounds good.
All right, everybody, that's the show.
It's just a thought.
Y'all try to have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}