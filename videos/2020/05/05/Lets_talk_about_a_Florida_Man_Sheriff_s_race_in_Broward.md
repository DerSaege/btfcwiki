---
title: Let's talk about a Florida Man Sheriff's race in Broward....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xxeDQP6tNjA) |
| Published | 2020/05/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Florida man sheriff's race with a scandal emerges between Sheriff Israel and Sheriff Tony.
- Governor DeSantis replaced Sheriff Israel with Sheriff Tony post-Parkland incident.
- Sheriff Tony faced a scandal from his past involving self-defense during a dealer incident as a 14-year-old in Philadelphia.
- The sealed juvenile case from Sheriff Tony's past became public knowledge during the election.
- The scandal doesn't seem to affect Florida voters, who value Sheriff Tony's past actions in protecting others.
- The political strategist's move against Sheriff Tony backfired, potentially boosting his election chances.
- The race contrasts Sheriff Tony's proactive response at 14 with Sheriff Israel's handling of the Parkland incident.
- Beau finds the situation comical and quintessentially "Florida."

### Quotes

- "That's something that gives you a Florida man card right there."
- "Y'all did not think this dude's gonna win in a landslide."
- "This is the most Florida thing ever."

### Oneliner

Beau gives a Florida perspective on a scandalous sheriff's race, showcasing how past actions may sway voters in the Sunshine State.

### Audience

Florida Voters

### On-the-ground actions from transcript

- Stay prepared for hurricane season (suggested)
- Stay informed and engaged in local politics (implied)

### Whats missing in summary

Beau's humorous commentary and unique insight on Florida politics and scandals.

### Tags

#Florida #SheriffRace #Scandal #Election #FloridaMan


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about
a Florida man sheriff's race.
Kind of political campaign that could
only occur in this state.
Um, yeah, especially for a law enforcement office.
Before we get into that though,
odds are if you're watching this,
you might be from Florida.
We're moving into hurricane season.
Get prepared, stay prepared.
Don't wait for it to show up on radar.
The agencies that normally respond to emergencies
are tapped out.
There's not gonna be help coming like normal.
Okay, before we get into the meat of this,
I need to say, I'm not from Broward County.
That is not my county.
I don't have a dog in this race.
I don't know either one of these guys,
um, other than what's widely available in the press.
Never met either one of them.
I am not a particular fan of Governor DeSantis,
who is the person who yanked one of them out of office
and put the other one in.
I just want to provide an average Florida man's opinion
of this developing news story slash scandal,
scandal in any other state.
Okay, so if you're not aware,
after the incident at Parkland, at that school,
the governor yanked Sheriff Israel out of office,
installed Sheriff Tony.
Tony had a little bit of experience
dealing with this type of stuff.
Now, as Florida politics demands,
these two guys are running against each other now
in a primary down there.
And just like clockwork, as the election nears,
a scandal emerges.
And the scandal is that apparently 20-something years ago,
when Sheriff Tony was 14,
living in Philadelphia in the Badlands,
there was an incident with a dealer.
Dealer came after him and another youth.
Sheriff Tony took his dad's weapon and popped him.
That's something that gives you
a Florida man card right there.
Welcome to the state.
Okay, the case was ruled self-defense,
according to reports, and was reportedly sealed.
Sheriff Tony apparently didn't disclose the contents
of a sealed case during his hiring process.
That's the scandal.
I am far more curious as to how a sealed juvenile case
from another state managed to become public knowledge
right at the time of election.
That, to me, is far more interesting,
because I'm certain there was no corruption there whatsoever.
Yeah, that's straight Florida.
More importantly, I don't think the political strategist
behind this move thought this through,
because now the race is between the guy
who oversaw the Parkland response,
with all that that entails,
the guy widely seen as being responsible
for that response, or lack thereof.
Everybody remembers the cop waiting outside.
So the race is between him and a dude
who at 14 proved he'd act if kids were in trouble.
Y'all did not think this dude's gonna win in a landslide.
I've tried to film this like six times,
and I can't get through it without laughing.
This is the most funny thing I've ever seen.
Without laughing, this is the most Florida thing ever.
The fact that some political strategist
thought that this would be a mark against Sheriff Doughty.
People are gonna turn out in droves for him.
The guy responsible for protecting their kid.
And that's why he was put there.
He's demonstrated he will, at a very young age,
in comparison to the person who got yanked by the governor.
I'm fairly certain that this didn't get
the proper amount of thought
before they decided to move forward with it.
Again, I'm not endorsing either one of these guys.
I'm just giving a Florida view of that situation.
I don't think anybody's gonna care
that 20-something years ago, he engaged in self-defense.
This is Florida.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}