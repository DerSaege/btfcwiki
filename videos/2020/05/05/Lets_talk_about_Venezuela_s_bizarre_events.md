---
title: Let's talk about Venezuela's bizarre events...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DYk2ZjO4ewo) |
| Published | 2020/05/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A spy novel-like situation unfolded in Florida and Venezuela.
- A crew from Florida, SilverCorp USA, contracted with Venezuelan opposition groups.
- The goal was to politically realign Venezuela by overthrowing Maduro.
- Some of SilverCorp's people were captured by Venezuelan authorities.
- Beau believes the US government was not directly involved.
- SilverCorp USA received only $50,000 out of a $212 million contract.
- Photos released by Venezuelan authorities suggest SilverCorp was underfunded.
- The US government usually provides ample funding to contractors for operations.
- Companies used for secretive operations by the US government are discreet and lack overt connections to the government.
- Goudreau, the head of SilverCorp, revealed detailed information to the press, which is unusual.
- The plan was to secure Caracas and mount an insurgency against the Venezuelan government.
- Beau suggests that the situation may have been a private affair gone wrong.
- There might have been ideological motivations behind the operation due to the minimal pay.
- Beau recommends the US should maintain a hands-off approach with Venezuela to avoid further complications.
- Venezuelan intelligence seemed to have significant infiltration within dissident movements.
- Beau urges for a hands-off approach, allowing the Venezuelan people to determine their future.
- The presence of US citizens in Venezuelan custody could escalate tensions.

### Quotes

- "This is not the type of thing that normally happens within this industry."
- "I think the Venezuelan people can decide for themselves what they want."
- "It's just a thought. Y'all have a good night."

### Oneliner

Beau analyzes a bizarre spy novel-like situation in Florida and Venezuela, suggesting that the US government likely wasn't directly involved, and advocates for a hands-off approach toward Venezuela.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Monitor the situation and advocate for a peaceful resolution (implied)
- Support diplomatic efforts to de-escalate tensions between the US and Venezuela (implied)
- Stay informed about developments in Venezuela to better understand the situation (implied)

### Whats missing in summary

The full transcript provides more in-depth analysis and context on the situation unfolding between Florida, Venezuela, and the potential involvement of the US government, offering a nuanced perspective on international relations and covert operations.

### Tags

#Florida #Venezuela #USGovernment #SpyNovel #InternationalRelations


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about changes in leadership and how sometimes they don't
happen because we just got a master class in that.
We basically just had a spy novel unfold here in Florida and Venezuela.
Understand this is a developing situation.
Some of the information that I'm about to say may change as time goes on and more information
comes out.
However I think I've got a good enough handle on it to answer the burning question of whether
or not the US government was involved in this.
If you have no idea what I'm talking about right now, it's because it's buried in the
news cycle really.
In normal circumstances this would be front page news.
This would be a big deal, but it's 2020 and there's a lot going on.
So it's kind of been buried.
Rough storyline is that apparently a crew out of Florida, of course Florida, SilverCorp
USA, led by a guy named Goudreau, contracted with Venezuelan opposition groups to politically
realign that country.
Am I politically realigned?
I mean overthrow, oust Maduro.
It appears and it's reported that some of their people have been caught, captured by
Venezuelan authorities.
The question everybody wants to know is, is the US government involved in this?
I'm going to say no.
I'm going to say no.
Not because they wouldn't, they most certainly would.
In fact, they very well may have an operation going on down there right now with this goal,
but I don't think they were involved in this one.
They may have been aware of it and just kind of turned a blind eye to it because they didn't
think it would amount to anything.
And it now has amounted to an international incident.
Okay, so the guy running SilverCorp USA has apparently made statements to the press about
what's going on.
Go ahead and tell you, that's really rare.
It is not normal for a contracting company to discuss an ongoing event with the press.
That's unusual.
In that reporting, it was disclosed that they had a $212 million contract of which they
apparently received $50,000 and started working.
If you're not familiar with the industry, $50,000 is enough to get a high-end security
detail for a day.
It is not enough to begin something this significant on this scale.
Some of the photos released by Venezuelan authorities also tend to back up the idea
that they were underfunded.
They released some photos of their equipment.
It does not appear that they had a lot of financial resources.
The US government, when they are dealing with contractors, they throw money at them.
You will run out of everything before you run out of money.
They'll give it to you in cash.
The general reason for the US government to use outside people, private citizens, not
active duty military, not intelligence people, private citizens to do something like this
is to maintain plausible deniability.
Because of that, they are willing to pay a premium.
That plausible deniability also factors into some other things.
Generally, when they contract with people to do this kind of crazy stuff, something
this secretive, something this outlandish, they tend to hand those jobs to companies
that are very discreet.
They may not have a website.
If they do have a website, it certainly doesn't say, hey, we're military contractors.
It probably focuses on training or maybe even just a little blurb about crisis mitigation,
something like that.
The Silvercorp USA website is pretty descriptive about what they do and describes some controversial
services.
So it's probably not a firm the US government would contract with for this type of stuff.
The other thing is plausible deniability.
Generally companies that are handed these kind of contracts do not have USA in their
name.
The names are normally very bland.
They don't even reference anything remotely untoward.
They're normally very, very bland, very nondescript, very innocent names.
There are some exceptions to this, but normally they're huge companies that have USA in the
title or something like that, that take big public contracts and then they have a second
tier that does the crazy secret stuff.
This does not appear to be one of those companies.
All of that leads us to believe that it's probably not government backed.
It does not appear so.
The State Department denial means nothing.
The fact that they were underfunded does.
Aside from that, in the apparent interview and in the statements to the press, Goudreau
said how many people they had in Venezuela.
That is also really rare, really unusual, very bizarre.
Generally speaking, you do not want to give up troop numbers.
There is a possibility that he's attempting to engage in disinformation and say, oh, we
only had X number of people there, you caught them all and there's other people trying to
get away right now.
That's a possibility, but given everything else, I don't know how likely that is.
Aside from that, he laid out the plan and said that it was to, primary objective was
to get Maduro and secure Caracas with a few hundred people.
That's really ambitious.
And then the secondary objective would be to mount an insurgency.
I have a feeling him saying that is going to haunt him the rest of his life because
he has people in custody right now.
I am fairly certain that in Venezuela, mounting an insurgency against the Venezuelan government
is probably illegal.
He very well may have given the Venezuelan government everything that they needed.
That was probably an unwise statement.
This appears to be something that is completely private.
A private affair, people that maybe got in over their heads.
Looks like something that was hatched on the fly and started moving.
There may be an ideological motivation in it for him and his men.
The odds of a contractor taking a job this significant for what amounts to no pay, because
$50,000 sounds like a lot, but when you're dividing it up amongst people and you're
talking about travel expenses and you're talking about the risk associated with something
like this, that's nothing.
That is not even remotely close to enough.
So there may be an ideological motivation there that comes into play.
I would suggest that this is something that's probably going to be made a movie at some
point in time because it is that bizarre.
This is not the type of thing that normally happens within this industry.
There are already a few contractors saying that they don't want them to be called
contractors.
They want them to use another word that starts with an M. And that's a debate people can
have.
I would take this opportunity to suggest that the United States should be very hands-off
with Venezuela.
If people actually believe their rhetoric and they're concerned about the welfare
of the Venezuelan people, I would suggest the best thing to do would be to stop stifling
their economy.
Economies all over the world are in trouble right now.
They don't need additional problems.
I would also point out that Venezuelan intelligence apparently knew everything.
To have that kind of sourcing and that level of infiltration within a dissident movement
means you have a lot of loyalty from the people.
You don't get that kind of information without having a lot of sources, without having a
lot of people who are on your side.
And Venezuelan intelligence is actually making jokes in some of these articles about how
they had to pay for some of the meetings to take place, like fund the people meeting,
so they could find out more information.
When a group is that infiltrated, it's not even a group anymore.
It's not a dissident movement.
It's just a lightning rod.
I would suggest that we stay hands-off.
I think that the Venezuelan people can decide for themselves what they want.
I don't think this is the U.S.'s business.
I would suggest that this news story is probably going to continue for a while, because there
are a lot of things still up in the air.
At the end of the day, there are U.S. citizens in Venezuelan custody.
And that could be used as a flashpoint to provoke something that the U.S. government
has wanted to do for a long time.
But we'll see how it plays out.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}