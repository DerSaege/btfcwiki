---
title: Let's talk about passion, rhetoric, and public assemblies...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lpaj7S-Udm0) |
| Published | 2020/05/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urges to tone down passion and rhetoric in the United States.
- Acknowledges decent intentions of those supporting public assemblies.
- Stresses the right to assemble without needing justification.
- Raises concerns about economic devastation, shortages, sheltering, and authoritarian measures.
- Criticizes the rhetoric advocating for armed conflict.
- Warns of the consequences of escalating rhetoric to armed conflict.
- Points out the lack of understanding among those advocating for armed conflict.
- Emphasizes that advocating for armed conflict will disproportionately harm innocents.
- States that domestic conflict is the worst thing that can happen to a country.
- Urges for the elimination of rhetoric promoting armed conflict.
- Encourages passion in expressing views but warns against advocating for violence.
- Asserts that good ideas do not require force.

### Quotes

- "Good ideas generally do not require force."
- "Because if you use that rhetoric long enough, eventually it's going to happen."
- "There are very, very few situations that can be improved by going loud."
- "There are very, very few situations in which it [violence] will be useful."
- "They don't need our agreement."

### Oneliner

Beau urges to tone down passionate rhetoric in the US, warning against advocating armed conflict due to its disproportionate harm on innocents and stressing that good ideas do not require force.

### Audience

United States citizens

### On-the-ground actions from transcript

- Advocate for peaceful and respectful discourse in public assemblies (implied).

### Whats missing in summary

The full transcript provides detailed insights into the dangers of escalating rhetoric to armed conflict and the importance of advocating for peaceful solutions.

### Tags

#Passion #Rhetoric #UnitedStates #Advocacy #Violence #PeacefulResolution


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about passion and rhetoric in the United States and why
it's probably time to tone the rhetoric down a whole lot.
I'm saying this because I know a few of the people that support these things.
They're decent enough people.
I think they're misguided, but they're decent enough people.
There's a whole bunch of public assemblies going on.
We're going to call them that because for the most part it's what they are.
They're public assemblies.
They've got a right to do it.
Doesn't matter if you or I agree with them.
They have a right and the whole point of a right is that they don't have to justify it
to us.
They don't need our agreement.
Doesn't matter what we think of it.
And there are limitations that can be placed on that right in this situation, but we're
not going to talk about that right now.
We're just going to talk about these assemblies.
What's the concern?
They're worried that some of the measures being taken are causing economic devastation,
causing shortages, causing the denial of freedom of movement because they have to shelter.
They're losing creature comforts, like haircuts.
There's the perception of like an authoritarian vibe.
Fair enough.
Fair enough.
What's the rhetoric being employed?
Many are using the rhetoric of advocating for armed conflict.
Take as long with that as you need to.
And if you don't see where I'm going with this, you probably shouldn't be advocating
for armed conflict.
Economic devastation.
Shortages.
Sheltering.
Denial of movement.
Authoritarian measures.
Losing creature comforts.
Trust me, the avant-garde hairstylist will not be open.
If it goes loud, every problem, every concern that they have gets multiplied.
If you are new to this channel, understand, I'm not a pacifist.
I'm just somebody who doesn't like the misapplication of force.
Because normally, it's not the people who misapply the force that suffer the most.
It's the innocents.
If those are your concerns, that rhetoric should come off the table.
It should come off the table.
Because if you use that rhetoric long enough, eventually it's going to happen.
And nobody really wants that.
Nobody really wants that.
The reality is that many of the people who advocate for this type of stuff don't know
the reality of it.
This, yeah, it's annoying.
It's annoying.
It is.
These measures are annoying.
Fact.
But they are nothing in comparison.
Nothing.
Not even close.
There are very, very, very few situations that can be improved by going loud.
When you are talking about something like this, it's pretty much the worst thing that
can happen to a country and its people.
A kind of domestic conflict like that.
And in this country, it would drag on and on and on.
And the whole time, it's not the people who think it's a good idea that suffer.
It's the innocents.
It's the civilians.
The non-combatants.
This rhetoric needs to go away.
People are passionate.
Because it's the United States and this is our thing, it's what we do, right?
It's how we solve all of our problems.
Because they're passionate, that rhetoric quickly turns to advocating stuff like this.
It needs to go away.
It needs to stop.
Because eventually it's going to happen.
And most of the people who think it's a good idea, they don't really know.
They have an image from a glorification.
They don't know.
It's almost never the right move.
There are very, very few situations in which it would be useful.
This is certainly not one of them.
Be passionate about it.
You want to go out there, yeah, I think you're wrong.
I think it's a mistake.
I think for some of you, it's going to be a fatal mistake because I think you're going
to get something.
But it's your right.
You can choose to take that risk if you'd like.
But don't make that choice for other people.
Don't put them at risk because of your rhetoric.
Good ideas generally do not require force.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}