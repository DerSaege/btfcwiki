---
title: Let's talk about what Trump can learn from some old interviews....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=I2Kyo8QpxJ8) |
| Published | 2020/05/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls witnessing a series of interviews where a recruiter struggled to choose between two candidates for a job.
- The recruiter was looking for someone who was good at finding lost things.
- Both candidates had the required experience and good references but lacked direct client interaction.
- The recruiter's concern was that neither candidate had been held accountable before.
- One candidate confidently promised to do everything possible, while the other couldn't provide a clear timeline.
- The candidate who vowed to do everything was hired, showcasing the importance of under-promising and over-delivering in crisis situations.
- Beau explains how overpromising and underdelivering erodes trust during crises.
- He criticizes leaders who prioritize branding over inspiring faith and comfort in the people they serve.
- Beau points out the importance of leaders admitting when they don't know something instead of spreading misinformation.
- He stresses that true leadership involves honesty, accountability, and inspiring belief in the people.
- Beau concludes by urging individuals to step up and lead in the absence of genuine leadership in traditional positions of power.
- He calls for collective action and responsible leadership from the populace in navigating crises.

### Quotes

- "One of the basic rules of crisis mitigation is to under promise and over deliver."
- "The U.S. needs real leadership. It is probably not going to be in the Oval Office for a very long time."
- "What gets people through tough situations is leadership."
- "The American people would rather hear, 'I don't know,' than a lie."
- "It's up to all of the people who make up this country to lead it."

### Oneliner

Beau stresses the importance of under-promising, over-delivering, and honest leadership in inspiring faith during crises, advocating for collective action in the absence of genuine leadership.

### Audience

Americans, Community Members

### On-the-ground actions from transcript

- Lead with honesty and accountability in your interactions and decision-making (exemplified).
- Take responsibility for your actions and admit when you don't know something (exemplified).
- Inspire faith and trust within your community through transparent communication (exemplified).
- Step up as a leader in your community to navigate crises effectively (exemplified).

### Whats missing in summary

The full transcript provides a detailed analysis of leadership qualities needed during crises and the importance of honesty, accountability, and inspiring faith in inspiring collective action.

### Tags

#Leadership #CrisisMitigation #Honesty #Accountability #CommunityLeadership


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about a series of interviews I witnessed forever ago and
what they can teach the president about inspiring faith in times like these.
The guy I was working for was a recruiter for contracting companies, for consultant
companies.
Now I know in movies when you're talking about those guys, they're experts at everything.
In real life, they're really really good at one thing and they're okay at everything else.
He was in the process of recruiting somebody who was really good at finding stuff that
was lost.
That's what he was looking for.
Got a whole bunch of applicants and at this point I'm a flunky.
I'm calling and checking references and stuff like that.
And we get down to two good solid candidates.
Either one of them could fulfill the job.
They've got the required experience, they know what they're doing and the references
check out.
He couldn't decide between the two of them and he said he was worried because neither
one of them had ever actually been the person to deal with the client.
They'd always just been out in the field.
Neither one of them had ever been held accountable.
Now I didn't exactly understand the significance of that to be honest at the time but I understood
that he was really really good at what he did.
So I didn't ask any questions, I was just going to wait and see.
He brings them both in the same day, puts one in each meeting room, goes in, reads them
both on, tells them both about what's going on and leaves them in the meeting room with
a lot of information about what's happening.
He walks out and I'm like, what's going on?
I'm just giving him time to go through it.
Okay, so explain to me what the problem is.
Neither one of them have been accountable.
Okay, still doesn't tell me anything.
So gives them 45 minutes, an hour, something like that.
Walks back in, first room.
So how long is this going to take you?
Two weeks, three tops.
Cool.
Leaves that room, goes into the other one.
How long is it going to take you?
I don't have a clue.
I don't have a clue.
Days, weeks, months?
What's this going to take?
I don't know.
Assume I'm the client for a moment.
What are you going to tell me?
I'm going to tell you I'm going to do everything I can.
That's the one he hired.
One of the basic rules of crisis mitigation is to under promise and over deliver.
If you promise the moon and don't come up with it, people lose faith very, very quickly
because bad things are happening around them.
If you have to go from 12 to 1,000 to 30,000 to 50,000 to 80,000 and still say you're doing
a good job, people lose faith, rightfully so.
Because if you don't understand the basic communications behind dealing with your client,
and in this case, the president's client is the American people, if you don't understand
the basic communication skills, what are the odds that you understand how to actually mitigate
the problem?
You're just guessing.
Guesswork doesn't actually pan out when you're doing stuff like this.
This is, I don't know what estimate, you know, I don't know which number estimate this is,
but it's going to be wrong too.
It's going to be wrong too.
And eventually, he'll get to the point where he's right, you know, because he'll keep raising
the number and then he'll say he was right.
Because he's approaching this as if it's a business.
He's branding.
He's wanting people to talk about what he said rather than making sure that it inspires
faith.
That it makes people feel more comfortable, makes people understand that they have a leader.
You know, he said that his move saved millions of lives, shutting down airplanes.
That is a branding move.
That's something that's completely immeasurable.
Nobody can prove that false, but he can say it.
Most people know that's not true, but he can say it.
The thing is, the branding doesn't help because the bags are still getting filled.
You know, you've got one raising the estimate and the other saying that we need to forget
about it.
More than a thousand people today.
It's not over.
So his estimate as far as the number lost, it's wrong.
Just like the last ones.
His estimate as far as when there will be a ready vaccine, it's probably wrong because
he's hoping to advertise, to get you talking about it, not understanding that there are
no stocks here.
People talking about it doesn't raise the value.
The value of that office is not measured in how much people talk about it.
It's measured in how much people believe what's coming out of it.
That's the true value.
And right now, very few people believe what he says because he has a really poor track
record of being accurate.
And he refuses to take responsibility or admit when he's wrong.
But he wants to take the credit and claim that he's right.
The U.S. needs real leadership.
It is probably not going to be in the Oval Office for a very long time.
That means it's up to you.
It's up to us.
It's up to all of the people who make up this country to lead it.
Because the Senate, the House, the Oval Office, we don't have leaders there.
We don't.
None of them are showing leadership.
They're showing that they want to command, that they want to rule.
And that's fine, whatever.
But that's not what gets people through crises.
What gets people through tough situations is leadership.
Not being under somebody's command.
If there's anything that the President can learn about this situation, it's to admit
when he doesn't know something.
And it's to understand that the American people would rather hear, I don't know, than a lie.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}