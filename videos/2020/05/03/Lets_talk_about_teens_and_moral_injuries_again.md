---
title: Let's talk about teens and moral injuries again....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uyE8dby4SHk) |
| Published | 2020/05/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Teens are experiencing moral injury due to government response to the pandemic, feeling wronged and demoralized.
- Moral injury involves perpetrating, failing to prevent, or witnessing a gross transgression of morals.
- Teens face a systemic failure, not a singular act, realizing the facade of society crumbling.
- Suffering a moral injury leads to demoralization and self-handicapping behavior.
- Belief in the just world hypothesis and self-esteem are helpful in coping with moral injury.
- Teens can generate self-esteem by realizing they can be the change in a flawed system.
- Younger people are the driving force for change and can make a difference through consistent actions.
- Involvement in fighting against systemic failures requires a long-term commitment and patience.
- Government is critiqued for downplaying threats and putting families at risk for political gain.
- Younger generations are encouraged to challenge the status quo and work towards a better future.
- Beau urges teens to take action, do something daily for 28 days, and be the catalyst for positive change.

### Quotes
- "You want to change the world. You want to overcome the moral injury. You want to change the system that let you down. You can. Just got to get in the fight."
- "It's always the younger generation that actually changes the world."
- "You're more capable of changing the world than people my age."

### Oneliner
Teens facing moral injury from systemic failures urged to take daily action for positive change and overcome demoralization.

### Audience
Teens, Activists

### On-the-ground actions from transcript
- Get involved in the fight against systemic failures by doing one thing each day for 28 days (exemplified).
- Challenge the status quo through consistent actions and advocacy (implied).

### Whats missing in summary
Importance of younger generations in driving positive change and the need for long-term commitment for systemic transformation.

### Tags
#Teens #MoralInjury #SystemicFailures #YouthActivism #PositiveChange


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about teens, what's going on today, and moral injury.
We're going to do this because I got a message.
And I'm not going to read the whole thing, but enough of it for you to get the idea.
Hey Bo, I'm a teen from New York, been watching the response unfold.
Someone in my family have contracted it, been hospitalized.
I along with most of my friends are livid.
We know we have been wronged by those in government who delayed the response, downplayed the threat,
and put our families at risk.
I found one of your videos from last year talking about moral injury in teens.
That's what I'm seeing in my friends.
Fits the bill to the letter.
Could you talk about how moral injury relates to what is happening now?
Absolutely.
Absolutely.
The thing is, the advice is pretty much the same.
The situation is different, but the advice is the same.
With the exception of now, I would suggest that you wear a mask.
For those that don't know, moral injury is perpetrating, failing to prevent, or witnessing
a gross transgression of your morals.
When it gets brought up, it's normally talking about soldiers.
That's normally when the term comes up.
Soldiers who saw something horrible.
Much like PTSD though, it's not limited to soldiers.
What is happening now can definitely be a catalyst for it.
No doubt.
The thing is though, it's not an act.
You know, perpetrating, failing to prevent, or witnessing a gross transgression.
Normally we think of that as a singular act.
One thing that happened, and it has a perpetrator as in that definition.
What teens are witnessing today, and what they are realizing, is that it's not a single
thing.
And it doesn't really have a perpetrator.
It's not a person.
It's not somebody that can be hauled in front of the court.
It's a systemic failure, because they are witnessing all of the facade, the movie set
that we've built, that we say, this is America.
They're watching it come tumbling down around them.
And they are smarter than a lot of us.
They understand that it's a system that is built like that, to keep people right on the
edge.
And then when something happens, it's real easy for them to slip off.
What happens when you suffer a moral injury?
You become demoralized.
It's hard to maintain a positive outlook on things.
You start engaging in self-handicapping behavior.
You know, I'm not going to worry about that.
Not going to finish it.
Might start, but not going to finish it, because nothing matters.
I mean, look around.
It's what happens.
And the inoculation for that is belief in the just world hypothesis.
I always kind of laugh at the idea of that being helpful, because generally speaking,
not in all cases, but in most, if you have suffered a moral injury, you know the world's
not just.
The good guy doesn't always win, and the bad guy doesn't always get punished.
I always find it funny when I see that in texts.
This will help.
It's hard to believe in something you know to be false, but it is what it is.
The other thing that is helpful is self-esteem.
Self-esteem, which is something that teens in general have issues with.
How do you generate self-esteem?
For this, it's easy.
It's easy, because you know what the problem is.
You're right, it's not a person, it's a system.
System can't be hauled in front of the court, but it can be changed.
And you can be that change.
And I know, what can I do?
Younger people, right?
Everything.
It's always the younger generation that actually changes the world.
Younger people, we can advise, we can assist, we can facilitate, but it's always the younger
people that do the heavy lifting.
Really is.
It's always been that way.
So what can you do?
Get in the fight.
Get in the fight.
Do one thing each day for 28 days.
Then it is no longer something you have to think about doing.
It's a habit.
You're used to it.
It doesn't have to be anything huge.
A social media post.
Something that simple.
You know, it's not going to change the world today, but you've joined the fight.
You've gotten in the campaign, and it is a campaign, and that's something you have to
be aware of if you're younger.
Because people my age and younger, we are used to instant gratification.
We are used to instant communication, everything being available on tap whenever we want it.
This isn't like that.
This is getting involved in something that requires a long-term commitment.
Because the system is broke.
It did let you down.
It did put you at risk.
It always does.
If you're younger, your exposure to government is pretty limited, really.
The government employees you run across, most people, are what, teachers?
The firefighter who comes to the school?
Good people.
People you look up to.
That's not the whole of government.
You get higher up the chain, and it becomes exactly what you imagine it to be.
People doing math to see how many we can lose before it impacts their chances for re-election.
You're right.
You're right.
They let you down.
They.
The system.
We let you down.
Because we enabled it.
And that's why you have to get involved fighting against it now.
Because if you don't, you will eventually buy into it.
You will become one of the people who downplay the threat.
You will become a supporter of the people who put your families at risk.
And you won't even think about it.
Because if you stay inside that system, you don't step out and look at it from the outside
and realize that there could be something better.
Eventually you'll believe in it.
Eventually you'll say, this is all there is.
No it's not perfect, but it's the best we've got.
Maybe that's true, but that doesn't mean it can't be better.
It can be.
It can be improved.
And you and your friends can be the people to improve it.
It's going to be your generation.
Y'all are going to do the heavy lifting.
All we can do is help.
It's got to be you.
Because you're still outside of that system.
You haven't bought into it yet.
You don't have the sunk cost fallacy where you've put so much faith into something that
it's painful to reject it.
You don't want to waste all of that faith.
So you keep justifying the behavior.
You haven't done that yet.
You and your friends are more capable of changing the world than people my age.
Because you're more open to new ideas.
And it's going to take new ideas.
And it's going to take you guys being active.
One thing a day.
Do what you can, when you can, where you can for as long as you can.
One thing.
Every day for 28 days.
Then you won't even think about it anymore and you will continue doing it.
You want to change the world.
You want to overcome the moral injury.
You want to change the system that let you down.
You can.
Just got to get in the fight.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}