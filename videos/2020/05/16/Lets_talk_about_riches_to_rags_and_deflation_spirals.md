---
title: Let's talk about riches to rags and deflation spirals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=J3Yd8rs8Wgc) |
| Published | 2020/05/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of deflation and how it differs from inflation in terms of prices and purchasing power.
- Describes a deflation spiral where decreasing demand leads to lower prices, impacting companies' revenue and potentially causing layoffs and closures.
- Links prolonged deflation to the possibility of a Great Depression-like scenario.
- Points out factors contributing to the current deflationary trend, including reduced demand due to people staying home and tariffs affecting prices.
- Criticizes government response for not bailing out the lower-income individuals to prevent a deflationary spiral.
- Suggests that injecting money into the economy through stimulus packages is necessary to keep the economy afloat.
- Expresses skepticism about policymakers' understanding of economic issues and their ability to address the situation effectively.
- Advocates for taking action to prevent economic collapse by pumping money into the economy, even if it means increasing the national debt.
- Advises individuals to be cautious with spending, pay off debts, and keep cash on hand to prepare for potential job losses.

### Quotes

- "We're staring down the barrel of the doom loop."
- "Your main concern was the economy, and you may have provided an immortal wound."
- "But we do. Here we are."

### Oneliner

Beau explains deflation, warns of a potential spiral leading to economic crisis, criticizes government inaction, and advocates for injecting money into the economy to prevent collapse.

### Audience

Economic policymakers, concerned citizens

### On-the-ground actions from transcript
- Advocate for government policies that prioritize bailing out lower-income individuals (implied)
- Monitor economic developments and government responses closely (implied)
- Be cautious with spending, pay off debts, and keep cash on hand in preparation for potential job losses (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of deflation, its potential consequences, and the role of government policies in mitigating economic crises. Viewing the full content offers a comprehensive understanding of these complex economic concepts and their real-world implications.

### Tags

#Deflation #EconomicCrisis #GovernmentResponse #StimulusPackages #FinancialAdvice


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about deflation,
deflation spirals, riches to rag stories.
Most people know what inflation is.
That's the price of everything going up.
Your purchasing power gets weaker, okay?
As a consumer, you're used to that.
That's typically what's occurring in the United States.
Deflation is the opposite.
Prices are going down.
Your purchasing power is getting stronger.
And that sounds like a good thing.
And it is in the short term.
And when I say prices,
I don't just mean like on single items.
I'm talking about something called the consumer price index.
Picture it like an average
of the cost of almost everything.
So your purchasing power is getting stronger.
That sounds good.
And it is for a limited time.
However, if a deflationary period is prolonged,
you can enter something called a deflation spiral.
Prices go down because demand is less.
So the company is selling less of their product.
So they lower prices to try to entice buyers.
If buyers don't bite,
then the company is selling less of their products
at a lower price.
If it's less of their products at a lower price,
they have less money coming in.
If they have less money coming in short term,
they can weather it.
Long term, they still have the same liabilities.
They still have the same debt.
They still have the same stuff they have to pay.
If they can't do it by getting revenue
and having money coming in,
if they can't meet those payments,
their options are to cut cost
via layoffs, wage reductions, closures.
That transfers the burden of a deflation spiral
back to the consumer,
because now you're in the same boat.
You have less money coming in,
but you still have your car payment and your house payment,
which typically, a lot of this is psychological,
you can see, typically means you're gonna spend less money
because you're worried.
Makes sense.
You try to keep your cash on hand.
The cash isn't moving.
It's not actually in the economy.
It's sitting somewhere, the currency.
As this goes on and on, if it continues too long,
what happens?
The Great Depression.
There was a deflationary spiral in that, a doom loop.
I've read that it's kind of tied more to the change
in unemployment rather than the actual level
of unemployment.
For those that really wanna get into it
and see some neat starting points,
that's a good one to start with.
Why is it happening?
Well, demand dropped because of everybody staying home
for a lot of products.
People were staying home, they weren't making money,
so they held onto it.
They didn't get a stimulus to make them buy products online,
which would have helped keep prices up.
In addition to that, before then,
you had policies enacted by the administration
that intentionally lowered demand for certain products,
the tariffs.
The tariffs intentionally raised prices
in an attempt to keep people from buying some of them.
I know, this goes against the whole America first thing.
I got it, but it is what it is.
So what's the government response?
What can they do?
Now, it should be pointed out before we get too far
into this, that us, average people, yeah, it hurts.
It hurts.
But those of us on the bottom, well,
we're used to discomfort.
And there's only so much we can lose.
However, for those at the top, when this begins happening,
they can lose everything.
They can go from being millionaires and billionaires
to having nothing, richest rags.
Government policy, fiscal policy can't help in this.
And it probably will, because very soon,
all of those high-powered lobbyists
are gonna be going to these senators saying,
we're in trouble.
And the policy, the solution is to inject more money
into the economy, to free it up, to get it moving again.
Had the government done what was moral in the beginning
and bailed out the little people,
the money would have flowed up.
The money would have been spent.
Prices wouldn't have fallen.
They didn't do that, really.
1,200 bucks.
That's probably what's gonna happen.
That's probably what's going to happen
if this deflationary period continues
for any length of time.
They're gonna have to do another stimulus.
And it's gonna have to be a big one,
kind of like the crazy one
that everybody's making fun of now.
They're probably gonna have to do that
to keep the economy moving.
That's the policy.
As is normally the case,
had the government and those politicians
that were so concerned about feathering their own nests,
had they done what was right for the country
in the beginning, we wouldn't have this problem now.
But we do.
Here we are.
We're staring down the barrel of the doom loop.
Is it unavoidable?
No, it's not.
It could very easily be sidestepped.
Is it gonna be?
I don't know.
We have a bunch of buffoons up there, to be honest.
I don't think many of them actually understand
any of these subjects.
They just go off what their advisors tell them.
However, a lot of their rhetoric for a lot of time
has always been, well, how are we gonna pay for it all?
Right now, you're not gonna pay for anything
if you don't get that money moving.
If the money that's currently in circulation
that people are holding onto because they're nervous
doesn't start moving, you're not gonna pay for anything
because there's gonna be no taxes.
I think most people know my ideological stance
on a lot of this.
However, I am not willing to advocate
that people go into bankruptcy
just to pass some ideological purity test on the internet.
This is the system we're in,
and we're not gonna change it before this happens.
So until then, we have to play by the rules of the system
to avoid your neighbor becoming homeless.
That's probably what's gonna need to happen.
They're gonna need to pump money into the economy.
So if you start hearing just ridiculously large numbers,
understand that's why.
That is why, and there are methods
to deal with getting that money out of circulation as well
without causing huge problems.
But we'll see what they do.
Either way, it's something you need to be aware of.
It's something you need to keep an eye on.
From an economic standpoint,
From an economic standpoint,
looking at the whole economy,
I think the advice from most experts
would be to make sure you spend some money.
I'm not an expert, and I can tell you
that I would be paying off debt.
I would be not making non-essential purchases,
and I'd be keeping cash on hand,
because if this gets out of hand,
you very well may lose your job permanently.
And the longer the ups and downs of stay at home,
okay, no, we can come out, stay at home,
no, okay, we can come out,
the longer it goes on, the more likely this is to happen,
because money gets held longer.
It's not in circulation.
So all of those who were pushing for a fast reopen,
and now it looks like a lot of those places
are gonna have to close back up, congrats.
Your main concern was the economy,
and you may have provided an immortal wound.
and you may have provided an immortal wound.
Good job.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}