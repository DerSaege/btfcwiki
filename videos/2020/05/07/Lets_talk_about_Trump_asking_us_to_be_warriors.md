---
title: Let's talk about Trump asking us to be warriors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZM11nZ-tg8k) |
| Published | 2020/05/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques the President's eagerness to quickly reopen the economy without taking responsibility for the consequences.
- Points out that rushing into reopening without a proper plan could lead to more harm.
- Mentions the importance of having a safe and effective treatment or vaccine before fully stopping precautions.
- Expresses concerns about the potential dangers of not maintaining precautions and the possibility of flare-ups.
- Addresses the need for individuals to make informed decisions about when it's safe to resume normal activities.
- Emphasizes the role of leadership in mitigating risks and protecting the well-being of the people.
- Urges caution and awareness of possible supply chain interruptions that could have severe consequences.
- Encourages individuals to be proactive in monitoring the situation and being prepared for potential challenges.
- Stresses the importance of taking personal responsibility in navigating through uncertain times.
- Concludes by reminding listeners to stay vigilant and prepared for any future developments.

### Quotes

- "You're going to have to lead yourself here."
- "Lay down your life for the altar of corporate profits, I guess."
- "Being a warrior is about mitigating risk."
- "A warrior, a leader, would want to make sure that doesn't happen because that compounds everything."
- "And generally, it should be noted that when you're acting in that manner, you're not fighting against the people in front of you. You're fighting for the people behind you."

### Oneliner

Beau warns against rushing into reopening, stresses the need for caution, leadership, and individual responsibility in navigating through uncertain times.

### Audience

Individuals, concerned citizens

### On-the-ground actions from transcript

- Monitor the situation and stay informed about potential risks and developments (suggested).
- Be prepared to re-implement extreme measures such as staying home if necessary (suggested).
- Stay vigilant for possible interruptions in the supply chain and be proactive in addressing any challenges that may arise (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the current situation and offers valuable insights into the importance of caution, leadership, and individual responsibility in handling the crisis effectively.

### Tags

#COVID-19 #Leadership #Responsibility #Economy #Precautions


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So I guess today we're going to talk about phase two
of the great heroic epic of failure
that is our response to the current situation.
The President of the United States seems very intent
on reopening quickly.
He accepts no responsibility for any of it,
but he wants to encourage that.
That's what matters.
That matters because the economy is what he staked
his re-election on,
and he doesn't really care about his citizens.
He cares about getting their vote.
He said that Americans need to be warriors about this,
and that just rubbed me the wrong way, to be honest.
I am not sure that I would take Donald Trump's advice
on what constitutes a warrior.
He wants to charge ahead, be brave.
I get it. I get it.
Half a league, half a league, half a league onward, right?
Probably one of the more popular references.
One of those things that's burned into our cultural memory.
It would be worth noting that that poem,
the Charge of the Light Brigade,
is actually about an unmitigated failure.
You do not win wars by rushing in without thinking.
That's how you lose them.
We're at the point now where we could probably begin
to slowly start to reopen in a methodical, safe,
slow, observed manner,
but that's not what is being advocated really, is it?
Because, see, we have those projections,
and at this point, I'm convinced the President believes
that when you reach that number,
that's when it's supposed to stop.
This thing doesn't know.
It's not like it's sitting there thinking,
oh, I got 100,000. I'm done.
It will continue until we have an available, safe,
reliable, effective treatment or vaccine.
That's when it stops, not before then.
Until then, we're going to have to maintain precautions.
It doesn't matter how brave you want to make yourself.
It doesn't matter how you want to portray yourself
to the media.
It doesn't matter how much you whine and cry.
That's what's going to have to happen.
Get your mind around it.
Those tolls, he's starting to publicly question them,
saying that they seem inflated.
And, of course, his media allies are willing
to indulge his paranoid delusions.
Yeah, there were a couple of cases
where things were overcounted.
The fact that they were corrected and caught
means that they're looking for that.
There's a whole bunch of people who were at home
when they went.
We don't even know they're gone yet.
The odds are that the toll is actually undercounted,
not over.
This is going to go on.
And if we rush it and we get out there and everybody goes back,
we're not taking precautions, it's going to flare up again.
Right now, we have interruptions in the supply chain.
They're annoying.
They are annoying.
That's what they are.
But imagine if it's hitting multiple sectors at once
because we weren't taking precautions.
Well, then it becomes dangerous.
It's not annoying.
It's a danger.
A warrior, a leader, would want to make
sure that doesn't happen because that compounds everything.
It makes it worse.
Being a warrior is about mitigating risk.
And generally, it should be noted
that when you're acting in that manner,
you're not fighting against the people in front of you.
You're fighting for the people behind you.
And it seems at this point, the Oval Office
does not care about those behind it.
Doesn't care about those on the bottom.
Doesn't care about us common folk, us peons down here,
who actually have to take the risk.
We don't have a staff of people making sure
that everybody around us is tested
and their temperature is taken.
Just have to rush in, right?
All into the valley of death because we're
expected to be warriors for the stock market.
Lay down your life for the altar of corporate profits, I guess.
We're going to reopen slowly.
Governments are notoriously slow to respond.
You, you individually, have to keep an eye on things.
You have to determine when you feel it's safe
and when it undoubtedly is going to flare up again when
you think you need to re-exercise
those extreme measures of staying home.
Because that's probably what's going to happen.
We're going to get flare ups.
And the longer we do this with half measures,
we don't get the testing we need.
We try to rush it.
The longer it's going to last, the more damaging
it's going to be to the economy.
You're going to have to lead yourself here.
And I would take note, it is a small possibility.
We're talking 1%, 2% that there are significant interruptions
in the supply chain.
But it's something that I would be aware of
and I would keep an eye on.
Because if that happens, it could get real bad real quick.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}