---
title: Let's talk about why people believe certain theories....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KwUPRbRCutw) |
| Published | 2020/05/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why people buy into theories, including comfort in thinking there is a plan and a dislike for uncertainty.
- Mentions that people may buy into theories because it makes them feel unique and boosts self-esteem.
- Refutes the stereotype that those who believe in theories are not smart, stating that they just think differently.
- Points out that the brain craves patterns, leading to the recognition of patterns even when there's no actual evidence.
- Talks about projection as a significant factor in theories, where believers see authoritarian behavior mirrored in the groups they theorize about.
- Notes the historical trend of politicians positioning themselves as opposition to theories, often authoritarian strongmen.
- Suggests that theories themselves are not dangerous but can become so if manipulated by certain figures.
- Recommends checking out Ashley Alker's debunking of a recent documentary that relies on creating comfort, uniqueness, and pattern recognition to appeal to viewers.
- Stresses the importance of monitoring theories, particularly to prevent manipulation of believers by certain individuals.
- Beau concludes by expressing the need to be cautious about those who attempt to exploit theories for their gain.

### Quotes

- "The brain craves patterns. We look for patterns."
- "They just think in a different way."
- "They're very easily led."
- "They do induce a very healthy skepticism of government."
- "It's scary if we don't have a clear picture of what's going on."

### Oneliner

Beau explains why people buy into theories, unpacking the comfort, uniqueness, and projection factors while cautioning against potential exploitation by certain individuals.

### Audience

Skeptics, critical thinkers

### On-the-ground actions from transcript

- Verify information before sharing or believing in conspiracy theories (implied)
- Engage in healthy skepticism of government actions (implied)

### Whats missing in summary

The full transcript provides detailed insights into the psychology behind belief in theories and the potential dangers of their manipulation by certain individuals.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about theories and why people sometimes buy into theories
that may seem less than rational at times.
We've talked about a bunch of reasons in various videos as we've come across them.
I don't think we've ever done where we hit all of the main components in one video.
So we're going to do that now and I'm fairly certain that we've never talked about one
of the biggest ones, historically speaking, as far as the effects that it has.
So we're going to do that today.
Okay, so why do people buy into these theories?
One of the main reasons is comfort.
It is comforting to think that there is somebody in control, that there is a plan.
If you don't have that, there's a lot of uncertainty.
Uncertainty is generally, I mean, that makes you uncomfortable.
We don't like it as a species.
It's scary if we don't have a clear picture of what's going on.
When you look historically at times when these theories take hold, they're during periods
of uncertainty.
So even if the group, organization, whatever, is against you, against your interest, it's
comforting to think there's a plan and it's not just chaos.
So that's one reason.
Psychologists often talk about uniqueness and people, when they buy into these theories,
they feel unique.
Maybe that's true.
Maybe it's true.
They get a self-esteem boost out of the idea that they possess some knowledge that isn't
common.
So they're unique.
That may be true.
I don't know.
One thing I want to point out is that people who buy into these theories, they're often
portrayed as not smart.
That's not true.
They just think in a different way.
And different parts of their brain may be more active, and not in a bad way necessarily.
Everybody's mind craves patterns.
We look for patterns.
That's why we see things in clouds and the swirls in trees and stuff like that.
We crave patterns.
A lot of these theories are just based on a name appearing over and over again in news
cycles or it's stuff like that.
It's patterns.
There's no actual evidence linking the individual events.
It's just a pattern.
And so much like the cloud, some people can make of that what they want.
But that doesn't mean they aren't smart.
So here's one of the big ones, to me one of the most important, projection.
When you look at these theories, there's a major plot hole in all of them.
And that major plot hole is that the organization has no opposition.
There's no bad guy.
I mean, that's just lazy writing if it's a story.
There's nobody there fighting the idea that they're fighting us.
That doesn't make any sense.
When you look at these theories, the people that make up the group, the organization that
is being theorized about, these are people that have money, power.
You're talking about the most powerful people on the planet.
They have control of states, banks, the militaries.
They have power.
And they have no opposition.
Nobody's actively fighting against them.
And if you talk to the people who believe these theories, these organizations, these
groups have had control, them pulling the strings, for hundreds, if not thousands of
years.
There's a plot hole there.
The most powerful people on the planet, with no opposition, can't get their act together.
Hundreds, thousands of years.
And they can't enact their goals.
That seems really unlikely.
It seems, if you look at the behavior of a lot of social groups that are centered around
these theories, you're going to see a lot of infighting.
You're going to see a lot of jockeying for position.
You're going to see a lot of deals made behind the scenes to elevate one group over another
and promote one theory over another.
Much the way these, the groups that believe in these theories, see the people in the theories
acting.
They're kind of projecting their behavior on that.
This is actually backed up by psychological studies that show many of the people who believe
in these theories have Machiavellian tendencies and tend to be very authoritarian.
So they view society, those up at the top, those in power, as authoritarian.
They probably are.
I mean, real life, they probably are, outside of the theory.
They probably are very authoritarian.
It's kind of proven.
But that doesn't mean there's an overarching pattern.
That pattern may be created just because the brain likes to recognize patterns.
But here's the interesting thing and why this is important historically.
They have no opposition in the theories, but every once in a while, some politician arises
who tries to paint themselves and cast themselves as the opposition to this theory.
It's a good move, PR wise.
I mean, it's hard to lose to something that doesn't really exist.
So they cast themselves as the opposition to whatever the conspiracy is, whatever the
theory is.
And it energizes their base.
Historically, the candidates and politicians and political figures that have done this
are authoritarian strongmen.
Look at them, all of them.
Doesn't matter what the theory is.
That's what they are.
So what's interesting, historically speaking, is oftentimes those who possess a theory that
their enemy, their opposition, those bad people out there are authoritarians out to rule the
world and out to invade every facet of their life.
When somebody casts themselves as opposition to that, they fall in line.
And then they end up supporting the very authoritarian measures that they would like to pretend they
oppose.
This backs up what the shrinks think about it being projection, about it being that those
who believe in these theories are authoritarians without the means.
So when somebody comes along that kind of caters to what they believe, they're willing
to grant them the exact same powers that they would have opposed had it been the people
in the theory.
And I know right now people are thinking about the guy in charge here in the US right now.
Like yeah, man, that totally fits.
It fits all of them.
It fits the German guy, it fits the Italian guy, it fits the Russian guy, all of them.
Anybody who has energized their base via a theory like this.
You know, I've often said these theories aren't dangerous.
They're not.
They're not.
A lot of them are.
I don't really like trashing them too much because they do induce a very healthy skepticism
of government.
They only really become dangerous when somebody tries to use those people who believe in it.
That's when they become dangerous.
Until then, they can actually be kind of beneficial because they stimulate public discourse and
they do induce that healthy skepticism.
And that can prevent a lot of authoritarian things from taking place.
But with the recent documentary, people have asked me to do a debunking of it.
I'm not going to.
I'm going to send you to Ashley Alker, that's Ashley spelled traditionally A-L-K-E-R.
She's a doctor.
She put out a tweet, a series of tweets debunking it.
I've retweeted it.
You can find it on my Twitter if you don't want to go find hers.
And just, you can scroll through it and it's very easy to discern that the documentary
is not really a documentary.
There's a very plain agenda within it.
But it relies on creating that comfort, appealing to uniqueness, making people feel a little
smarter than maybe they are, using terminology they don't understand, but presenting it in
a way that seems to make sense, even though there may not be scientific basis for what's
being said, and relies heavily on pattern recognition.
You know, talk about Fauci a lot, but connect him to nothing but that name.
It's out there.
So that's it.
So there's a brief overview of why it's important to monitor them.
And I don't necessarily think they need to be stifled all the time, because these theories
aren't necessarily inherently bad.
There's a lot of good benefits that come from them.
We need to be more concerned about those who are attempting to use the people who believe
in those theories.
Because while they often react in a unified manner and suggest that those who don't believe
in it are sheep, or whatever the term is they want to use, if somebody can harness that
theory, they're very easily led.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}