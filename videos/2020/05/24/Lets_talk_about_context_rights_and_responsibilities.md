---
title: Let's talk about context, rights, and responsibilities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VLDSdEz7JHU) |
| Published | 2020/05/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses rights and responsibilities in a video that was condensed and edited without full context.
- Believes that keeping the overall intent and message intact despite lacking context is beneficial for promoting ideas.
- Acknowledges that the edited video lacks context, but supports the approach taken.
- Notes that the Constitution's intent was to establish a more perfect union, ensure justice, domestic tranquility, defense, welfare, and liberty.
- Emphasizes that every right comes with an inherent responsibility.
- Raises concerns about individuals quoting founders without understanding the context of their quotes.
- Questions whether people today are motivated by quotes without context or by the larger intent defined in the Constitution.
- Stresses the importance of understanding the context to avoid drawing incorrect conclusions.
- Encourages viewers to seek and appreciate context when forming opinions about historical matters.
- Reminds that the Constitution's purpose was not to cater to selfish interests but to create a better society.

### Quotes

- "Every right has an inherent responsibility."
- "Maybe if you're going to have an opinion about what somebody meant 200 years ago, you need context."
- "Context is important."
- "In order to form a more perfect union, establish justice, ensure domestic tranquility..."

### Oneliner

Beau addresses the importance of context in understanding rights and responsibilities, urging viewers to appreciate the broader intent of the Constitution.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Seek context before forming opinions (implied)
- Educate oneself on the larger intent of historical documents (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the relationship between rights, responsibilities, and context in historical contexts.

### Tags

#Rights #Responsibilities #Context #Constitution #History


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're gonna talk about rights
and responsibilities again.
We're gonna do that because now this took the original video,
they edited it, they condensed it,
and added some flair to it.
And I know, you don't have to say anything,
I know some of you had issues with the way they did it
when it first surfaced.
That's actually how I found out it existed.
I don't.
I don't have a problem with what they did.
I think they did a really good job
of keeping the overall intent,
the overall message of that video intact.
Yes, it lacks some context.
That happens when you just use quotes.
It does.
It really does.
That occurs pretty often.
But I believe that individuals or individual entities
acting in their own interests,
but keeping society in mind,
I think that promotes everybody's welfare.
I think that makes everybody better
at getting an idea out there like that.
So I don't have a problem with it.
I fully support it.
But it does lack context.
And that context isn't really provided
even in that video unedited in some cases.
This channel is an ongoing thing.
It's an ongoing discussion.
So I don't even know that they could do it easily
without pulling stuff from other videos.
So I don't have a problem with it.
But it led to some questions and some comments.
Normally, it comes in the form of a quote from a founder,
and then saying, you know,
they never talked about our responsibilities.
Rights don't have inherent responsibilities.
They only talked about our individual liberties.
Look at this.
This is what they said.
Yeah, I mean, that's true, kind of.
They, yeah, they said that.
They talked about individual liberty,
and they talked about inherent rights
and stuff like that all the time
in their letters and their speeches, constantly.
It was a very overriding theme for them.
But it lacks context.
It lacks context.
Because there's another quote that anybody
who is quoting the founders should be familiar with.
In fact, some of the people who sent these messages
have it as their profile picture.
We, the people of the United States,
in order to form a more perfect union,
establish justice, ensure domestic tranquility,
provide for the common defense, promote the general welfare,
and secure the blessings of liberty to ourselves
and our posterity, do ordain and establish
this Constitution of the United States.
When all of those guys got together,
they put their name on that.
And that was the intent behind the Constitution.
That's the lens it's supposed to be viewed through.
So let's take it apart for a second.
In order to form a more perfect union,
in order to build the society, we
establish this Constitution, right?
And in the list of reasons, in the list of things
they're setting out to do, provide for the common defense,
promote the general welfare, they
believed that if individuals or individual entities were
acting in their own interest but keeping society in mind,
it would promote the general welfare.
We, the people of the United States, collective, a society,
every right has an inherent responsibility in it.
Now, my question here is, without the context,
without viewing it through that lens, what's happening?
The same thing that was happening back then.
The pebble in your shoe, that's what most people
are concerned about, right?
What affects you directly.
Speeches and letters, when they were trying to motivate people,
that's when they talked about that.
That's when they talked about your individual rights,
because they were trying to lead people.
They were trying to motivate them,
even though they may not have the full picture.
They may not have the context.
They may not be educated enough to understand the larger view.
If today, you are motivated by the little quotes
without context, and you're not motivated by the preamble,
the thing that set out to define what they were trying to do,
which are you?
Are you one of the patriot leaders?
Or are you one of the people that they had to motivate?
Are you one of the people that they're like,
man, we just don't even have the time to explain this to them?
Let's just appeal to their, the pebble in their shoe.
Context is important.
If you ignore it or remove it, yeah,
you're going to come away with a wrong conclusion a lot of times.
It's going to happen.
So maybe, and the people who are watching this,
who saw this on NowThis, you subscribe to the channel
because you wanted context, right?
You're a curious person.
That's how you got here.
Maybe if you're going to have an opinion about what somebody
meant 200 years ago, you need context.
You need to really focus on it.
Anyway, every right has an inherent responsibility.
It's spelled out in the Constitution.
It's spelled out in the intent.
It is spelled out from the very beginning.
This is what we're setting out to do.
And it wasn't to appeal to everybody's selfishness.
It was to form a more perfect union.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}