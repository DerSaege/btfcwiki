---
title: Let's talk about a poll and debunk a theory....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6ivsETtDa5A) |
| Published | 2020/05/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses a poll revealing concerning beliefs held by Fox News viewers regarding Bill Gates and vaccines.
- Mentions the skepticism towards Bill Gates despite his early warnings about pandemics.
- Notes the historical context of conspiracy theories related to tracking individuals.
- Speculates on potential motives behind the belief that Bill Gates will use vaccines to track people.
- Points out how much personal information individuals willingly provide through technology.
- Questions the need for a chip when ample data is already accessible through various means.
- Comments on the ironic situation where individuals worried about surveillance support restrictive measures like building a wall.
- Criticizes the lack of critical thinking among those who believe in far-fetched theories.
- Expresses concern about the lack of awareness among Fox News viewers regarding existing surveillance issues.
- Concludes by reflecting on the implications of blind belief and misinformation spread by certain news networks.

### Quotes

- "The surveillance society you're worried about, it's already here."
- "If you truly believe that this is a possibility, you should be screaming to have that wall torn down more than any liberal."
- "The same people that believe there's this James Bond villain Bill Gates guy out there [...] are the same people who want to cut off their only means of escape."
- "It's like they're easily led. Like somebody else is doing the thinking for them."
- "This is not something I'm worried about. I am more worried that there's 75% of people who view it as a possibility who watch Fox News who don't understand that what they're worried about is already happening."

### Oneliner

Beau addresses concerning beliefs held by Fox News viewers about Bill Gates using vaccines to track people, questioning the need for a chip when technology already provides extensive data on individuals.

### Audience

Critical Thinkers

### On-the-ground actions from transcript

- Inform others about the implications of blind belief and the importance of critical thinking (implied).
- Advocate for transparency and accountability in media coverage to prevent misinformation (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the misinformation spread through news networks and the implications of blind belief in conspiracy theories.

### Tags

#FoxNews #BillGates #ConspiracyTheories #Surveillance #CriticalThinking


## Transcript
Well howdy there internet people, it's Beau again.
So this is going to probably be a long one.
We're going to talk about a poll that came out, a healthy dose of skepticism, why people
believe certain things, Bill Gates and Fox News.
Anytime I make fun of Fox News or their viewership, somebody shows up in the comments section
saying that I'm being unfair, that it's not a fair generalization to say that they are
easily led or that they are gullible or that they allow others to do their thinking for
them.
A recent poll by Yahoo News and YouGov kind of dampens those complaints.
The poll was basically they were asking people to give true or false, whether or not they
believed something was true or didn't believe it or they could say not sure.
The question they asked was whether or not Mr. Gates was going to use the vaccine to
give them a chip.
Fifty percent of Fox News viewers said true.
Twenty-five percent said they weren't sure.
I'd really like to know what that first fifty percent knew, what evidence they have that
the twenty-five percent doesn't.
These numbers of true or not sure, they hold steady around seventy-five percent, plus or
minus for Republicans in general and Trump voters.
Okay, let me stop here and say there is like a mountain of stuff that you can criticize
Bill Gates for.
There is.
There's a whole bunch of stuff that you can criticize him for.
This just isn't one of them.
His actions in regards to what's happening right now, he was sounding the alarm before
it ever happened.
This is his thing.
And because of that, he gets villainized in this way because his name pops up.
His name shows up in all of this different stuff related to treatment of disease.
So therefore, well, he must be behind it.
Kind of sense does that make?
I normally don't mind theories like this because I think they induce a healthy amount of skepticism
in government because government is normally who they're aimed at in some way.
However, you also need to be skeptical of the theories that get presented because, you
know, you have motive, means, and opportunity, right?
That's what you really need to figure out.
I don't even know what the motive here is.
And I know somebody surely has a list of things that this will do.
But we're going to go through the ones that I think they might believe.
But before we even get into that, I want to point out that this is just a new spin on
an old theory.
Before it was a chip, it was a barcode.
Before it was a barcode, it was a social security number.
It all stems from taking the mark, getting the number and all of that.
It's where it comes from.
Okay, so I don't know what possible motive somebody would think exists for doing this.
But let me give you some that I was able to kind of dream up.
This isn't really a straw man, but these may not actually be their positions because I
doubt they even know, to be honest.
It was something they heard and they believed.
Track movement, like the cell phone you voluntarily carry everywhere and probably answered this
poll on.
That tracks your movement.
Even if it's not active, even if they're not actively surveilling you at the moment, it
keeps track of what towers you ping off of.
And they can review that months or years later.
Most people, as far as their movement goes, today they type in the address they're going
to on their phone.
There's a record of that using the navigation system, finding out how long it will take.
All of that stuff stays.
That's all part of your digital footprint.
If that doesn't do it for you, your photos.
Your photos that you post to social media, a lot of them have metadata in it that says
where you were, when you were, all of that stuff.
And even if it doesn't, the visual clues in those photos give a pretty good picture of
where you are and what you're doing.
Well maybe it's to track your financial transactions.
Like your ATM card and your bank and all of the digital stuff that you already do.
Most people don't use cash anymore.
That record already exists.
It's already a thing.
There's no reason to chip someone.
But aside from that, your social media.
Stuff you voluntarily put out there.
It provides a complete psychological profile of you.
How you process information, what sources you like, whether or not you read an entire
article, what you post, what gets you angry, what makes you happy, how quickly you read,
all of this stuff.
Your likes, your dislikes, what celebrities you follow, how easily you're influenced,
all of that stuff.
You voluntarily turn over.
That's more than any chip could ever do.
To get that kind of information, you used to have to go through somebody's trash for
weeks.
But now it's just kind of handed out.
And then a lot of people have the home assistant.
Siri, what's the weather?
And that is an open invite to be listened to in your home.
Then you have the navigation system in your car.
The one that may or may not be built in.
In a lot of newer cars, there's already a navigation system in it.
That does a pretty good job of tracking where you go.
And then on top of that, you have CCTV.
You know, you have security cameras everywhere.
I live out in the boonies, and I actually tried it one day just as an experiment, to
see if you could get from a main road to my house without ending up on camera.
And you can't.
And I live way out there.
If you live in any form of suburban, little town, major city, you're filmed constantly.
There's no motive for this.
And who gets this information?
Tech companies, the people that are actually getting villainized in these theories.
You're the one group of people that don't need it, because you've given them more than
they could ever get from a chip.
From the motive standpoint, it doesn't even make sense.
This isn't a thing.
It's not a thing.
Now at some point in the future, sure, maybe a bank decides to help prevent identity theft
in that manner, or something like that.
I'll be honest, I probably wouldn't do it.
But the idea that they're going to hide it for surveillance purposes, that's just, it's
not even cost effective.
It's not even cost effective.
Because something like that could be disrupted very easily, and you already provide them
way more information than they would ever get from a chip.
Just doesn't make any sense.
Now there is a funny part to this, because hearing that 75% of a demographic believe
like this is a thing, that's probably pretty depressing.
So here's something humorous.
The same people that believe there's this James Bond villain Bill Gates guy out there
who's probably acting in cahoots with the government to do this crazy thing, whatever
the full theory is, the people who believe this are the same people who want to cut off
their only means of escape.
Fox News viewers are ridiculously in support of building the wall.
If this is a thing, and one day you had to run from said science fiction government,
probably not going to be able to get on a plane.
And right now you're chanting to close up your only avenue of escape.
That seems a little short sighted.
It's like you're letting somebody else do the thinking for you.
If you truly believe that this is a possibility, you should be screaming to have that wall
torn down more than any liberal.
It is.
It's like they're easily led.
Like somebody else is doing the thinking for them.
Like they're gullible.
Like they are easily manipulated.
Because those are two very contradictory things.
We live in this science fiction dystopian world, which in some ways we already do, but
you want to make sure that you can't leave it.
It doesn't even make sense.
But those at Fox News, they can run those stories about people that don't look like
you and get you to kick down.
I'm not concerned about this.
This is not something I'm worried about.
I am more worried that there's 75% of people who view it as a possibility who watch Fox
News who don't understand that what they're worried about is already happening.
Because they watch a news network, in quotes, that doesn't inform them.
That cheerleads it all on and convinces them to cheerlead for it.
The surveillance society you're worried about, it's already here.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}