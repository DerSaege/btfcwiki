---
title: Let's talk about whether or not you should vote....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HqXsYPZ-ZLE) |
| Published | 2020/05/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of voting, philosophies, Biden, and the decision to vote.
- Beau acknowledges the diverse viewpoints of his audience regarding voting for Biden.
- The dilemma of choosing between Biden and Trump is discussed, with neither candidate being a perfect choice.
- He addresses the concept of lesser of two evils voting, a common scenario in the United States.
- Beau talks about how some philosophies view voting as taboo due to the moral responsibility attached to it.
- The importance of considering the potential members of the candidates' administrations is emphasized.
- Beau delves into the struggle of individuals with radical philosophies to find reasons to vote for Biden.
- The moral dilemma faced by those with certain beliefs on voting is explored.
- Beau touches on the notion of harm reduction and voting as a form of defense for marginalized communities.
- The significance of rejecting certain harmful ideologies is discussed in relation to the upcoming election.
- Beau expresses the importance of voting based on conscience and personal beliefs.
- He refrains from endorsing any particular candidate, leaving the decision to the individual's moral judgement.

### Quotes

- "It's not unique. It feels unique but the reality is this lesser of two evils voting it's what we normally end up with in the United States."
- "People who believe in one of those philosophies, they want everybody to get a fair shake. They want everybody to have some form of representation."
- "Your conscience tells you, you understand it's not what your philosophy dictates, but these people need help."
- "Your moral obligation if you are going to participate in voting, is to vote your conscience."
- "It's sad that the base is already this lackluster in support, but a whole lot of people asked the waiter for a 100 proof bottle of truth, and the waiter brought back Trump lite."

### Oneliner

Beau addresses the dilemma of voting for Biden, the concept of harm reduction, and the importance of voting based on conscience and moral beliefs.

### Audience

Voters, Philosophers

### On-the-ground actions from transcript

- Vote based on conscience and personal beliefs (suggested)
- Support harm reduction for marginalized communities through voting (implied)

### Whats missing in summary

The full transcript provides a nuanced exploration of ethical voting dilemmas and considerations surrounding harm reduction for marginalized communities.

### Tags

#Voting #Biden #Philosophy #Ethics #HarmReduction


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about voting and philosophies and Biden and whether or
not you should vote.
We're going to do this because I've got two questions back to back and they're coming
from opposite ends of the spectrum but at heart they're really kind of the same question
even though they're framed very very differently.
At their core both of them are saying please give me a reason to vote for Biden and that
doesn't really come as a surprise.
There's not a whole lot of people watching this channel that are looking at Joe Biden
going that's my man, me and him we align on policy issues.
I'm cosigning this guy, he's going to represent me the way I want to be represented.
Really not a whole lot of people thinking like that.
There are even fewer looking at the current administration and saying we need four more
years of that.
So it's put people in a unique spot but it's not unique.
It feels unique but the reality is this lesser of two evils voting it's what we normally
end up with in the United States.
You know we got 300 million people and these are the two we wind up with.
Okay so before we get further this is a unique channel in the sense that we have a lot of
people who are coming from a place of standard US political philosophy watching this channel.
We also have a lot of people who are coming from a more radical philosophy.
It might surprise those who are coming from within the norm to know that once you get
outside of that and you get further into some of the branches some of those philosophies
voting is taboo.
It's not something you do and the reasoning behind it is when you vote you are lending
your consent to be governed and you are morally responsible for anything that your candidate
does.
So if your candidate decides to drum up support near election time, near re-election time
by doing something bad that's on you.
You're responsible for that.
That's how the philosophy works.
One of these questions is coming from that angle and one is coming from the standard
philosophy within the United States and that's the one we're going to start with.
What if in this upcoming election we start looking at the candidates as a whole as opposed
to just the candidate.
For example yes we know Joe.
We know his resume etc.
What about the people he may bring to his administration?
And the idea is hey maybe we get a secretary of education that actually knows something
about education.
Wouldn't that be nice?
This one's easy.
If you feel it is your social responsibility to vote, if you're one of those people who
is going to vote, yeah that should absolutely play into your thought there.
Because when you look at Trump and you look at Biden, you're right on character levels
and stuff like that.
One's a better speaker.
But there's not a lot of tangible differences.
So that's going to make it hard to make a decision based on that.
However, the people they would bring into their administration or the people they have
in their administration, the differences there are huge, huge, massive, massive.
So it should definitely play into your thought on that and your decision making.
Now the other one is a little bit more difficult to answer.
I've never voted for any Democrat politician as president.
I voted for Bernie in the primary as a compromise as he best represents how I feel.
I'm struggling to come up with reasons to vote for Biden.
His voting history is right of center to me.
Obviously he's better than Trump, but that's not saying much.
Do you plan to vote for Biden?
Do you think somebody with a radical philosophy, do you think it's wrong for them to vote?
Okay, so I would suggest to answer the question about do I think it's wrong for people of
those philosophies to vote.
Yeah the principles, the morals, you don't.
You don't.
That's the philosophy.
People who believe in one of those philosophies, they want everybody to get a fair shake.
They want everybody to have some form of representation.
And they want an egalitarian society.
Now if you're somebody like me, you look like me, you have the privileges that I have, you
live your life the way I do, the difference between Biden and Trump on a personal level,
how it's going to impact you directly, there's not one.
There really isn't.
Biden or Trump, neither of them are going to impact me directly.
But what about kids in camps?
Biden is probably going to get rid of the most horrible stuff going on down at the border.
If you think all of that's going to go away when Biden takes office, if Biden takes office,
it won't.
It won't.
He'll put a kinder, gentler face on it, but a whole lot of it is going to continue to
happen.
It's not all going to go away.
But the most horrific stuff would.
Harm reduction for those people.
What about people who identify differently or have a different orientation?
People who have been scapegoated by this administration?
To them, it probably matters a lot.
If there was somebody who identified differently, who was part of one of those philosophies,
I would not look down on them for voting in self-defense.
And I certainly wouldn't look down on somebody who decided to hand them their vote and vote
in favor of their interests.
The whole not voting thing, in a lot of ways, has become an ideological purity test.
And in a lot of ways, it's the end thing to do.
Why don't vote?
Okay, you're sticking to those morals, that's your philosophy, cool.
I can respect that.
I can.
I can also respect somebody who is moving in the realm of harm reduction for people
who are marginalized.
Because that also does fit within the philosophy.
If you're wanting to reduce harm, that's how you create the society that most of those
philosophies want.
So I wouldn't look down on either.
And I don't, as far as who I'm voting for, or if I'm voting, or whatever, I don't do
endorsements, I don't tell people how to vote.
I don't think that it's ethical or moral for me to do that.
I give you the best answers I can, but as far as advising on that, I can't.
It's a moral decision you have to make on your own.
That's why voting is done in secret.
The social responsibility there is to vote your conscience.
Conscience tells you not to vote.
It's what it tells you.
Your conscience tells you, you understand it's not what your philosophy dictates, but
these people need help.
I wouldn't object to that either.
I don't like the lesser of two evils voting, but it's not something I see as a horrible
thing when you're talking about people that are marginalized and targeted and stuff like
that.
Even though Biden is not my man, and I've said that from the beginning, I really think
it's important that Trump loses.
I'm not really a supporter of Biden, but I think it's very important for the country
that Trump loses, and not just because it's Trump.
That administration has targeted and marginalized a lot of people, scapegoated them, made them
the enemy.
I think it's very important for those people to see those ideas rejected.
To see Trump and Trumpism rejected at the ballot box.
I think that's important.
Because if it doesn't happen, the only thing that they can believe is that the rest of
us are okay with it.
I know that's not really an answer to your question.
Other than no, I don't think it's wrong.
I mean yeah, it violates the philosophy, but that's the philosophy, that's the goal, that's
where we want to get to.
It's not where we're at.
And right now, there are people who could benefit from your support.
Now to go back a couple weeks to answer a question I haven't got to yet, no, I don't
think that voting for a third party is acting as a spoiler.
Your obligation, your moral obligation if you are going to participate in voting, is
to vote your conscience.
That's where it ends.
That's where it ends.
It's sad that the base is already this lackluster in support, but a whole lot of people asked
the waiter for a 100 proof bottle of truth, and the waiter brought back Trump lite.
So there are a lot of questions.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}