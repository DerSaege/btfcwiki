---
title: Let's talk about what Trump can learn from John F. Kennedy and the Dunning-Kruger effect....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BINJILWlPHY) |
| Published | 2020/05/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains what President John F. Kennedy could teach President Trump, focusing on a critical lesson yet to be learned.
- Contrasts President Trump with Dr. Fauci in briefings, illustrating the Dunning-Kruger effect.
- Describes Kennedy's humility and willingness to delegate decision-making, citing an example from his Navy days.
- Recounts a story about Kennedy's visit to France, showcasing his humility and understanding of what matters to people.
- Criticizes President Trump's repeated wrong decisions and lack of importance in the current situation.
- Emphasizes the need for experts to lead and inform the public, not the President.

### Quotes

- "President Trump, you are the man who accompanies Dr. Fauci to the podium. Nothing more."
- "He was wearing his power lightly."
- "Nobody believes, no rational person believes, that Trump is going to oversee a V-shaped recovery."
- "You don't matter in those briefings."
- "Kennedy could definitely teach it."

### Oneliner

Beau explains what Kennedy could teach Trump, contrasting their leadership styles and stressing the importance of humility and expertise in decision-making.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Listen to experts and prioritize their insights in decision-making (implied)
- Advocate for leadership that values humility and expertise (implied)

### Whats missing in summary

The full transcript delves deeper into Kennedy's leadership style and contrasts it with Trump's, offering a critical analysis of their approaches to decision-making and public perception. 

### Tags

#Leadership #DecisionMaking #Experts #Humility #PoliticalAnalysis


## Transcript
Howdy there, Internet people. It's Beau again.
So today we are going to talk about what President John F. Kennedy
could teach President Trump
if he was still here.
Because there's a really important lesson
that President Trump has yet to learn
and it would benefit the entire world,
certainly the country, if he did.
And President Kennedy
understood this lesson.
He could definitely teach it. He displayed it
his entire life.
That he had this concept down.
Have you ever watched
President Trump and Dr. Fauci at that podium together?
Let's be honest.
Fauci looks like he's just had a
burlap bag taken off his head and he's about to hold up a newspaper.
He doesn't look
incredibly comfortable with some of the stuff that gets said by his counterpart.
It's a case study in the Dunning-Kruger effect.
You've got one guy
with no subject matter expertise whatsoever
who is supremely confident in his answers
that are typically wrong.
And then you've got another guy who's one of the foremost experts in the world
and he's like, well, I don't know, it's complicated.
It's complicated.
He doesn't often give these
super confident answers.
They're always qualified by, you know, if this then that
because he understands the subject more.
If you're not familiar with this concept
you might look at one of these
briefings
and
think that Trump
actually has a better grasp on it.
It's not the case.
Kennedy,
from a very, very early point in his career
before he was ever in politics,
understood
that he may not always be the best person to make the decision.
He doesn't need to be the center all the time.
When he was in the Navy
he had his boat literally ripped in half
by a Japanese ship.
And him and his crew wind up in the water.
And he's like, you know, I've got nothing to lose, guys.
But some of y'all have families.
What do y'all want to do?
And he let them decide.
And you can't chalk that up to him being weak
or indecisive. He proved his entire career that's not who he was.
It's not like he didn't have
courage or guts.
I mean, after the decision was made he went on to
tow a wounded man
as he was swimming
by a strap in his teeth.
He's definitely somebody who was always a leader.
Even if you didn't agree with him,
you have to admit he was a leader.
You see, one of my favorite Kennedy quotes
is one that doesn't get enough attention
in my view.
Because it displayed his character a little more
than people may think.
He went to France,
brought his wife with him.
And the French,
they loved her,
Jacqueline Kennedy. They just adored her, doted on her.
I mean, come on, why not?
Of course you would.
When he introduced himself there,
he said,
I am the man who accompanied Jacqueline Kennedy to Paris and I have enjoyed it.
He didn't refer to himself as the president,
leader of the free world,
any of that stuff.
Because he knew what people
were focused on.
He knew what was important to the people
that he was speaking to.
Yeah, it was a joke in a way.
He was wearing his power lightly.
And it,
yeah, I mean, it was humorous.
But at the same time, it was an acknowledgement
that he wasn't the most important person in the room.
Not as far as most people were concerned.
We're at a point
where the president,
in this situation,
has made the wrong decision
over and over and over and over again.
Nobody really cares what he has to say.
I mean, except for those that are part of his base
that just wouldn't.
He can do no wrong.
You know, and the idea that people want to hear about the economics.
Most people understand that we were on the verge of this recession because of
his economic policies before this ever happened. We've talked about it on this
channel. You can go back. There's timestamps.
Nobody believes, no rational person believes, that Trump is going to oversee
a V-shaped recovery.
It's incredibly unlikely.
When people tune in,
they want to hear from the experts. They want to hear from people that know what's
happening.
President Trump,
you are the man
who accompanies Dr. Fauci to the podium.
Nothing more.
You don't matter in those briefings.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}