---
title: Let's talk about Trump, secrets, and sharing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TvhNrXuxT9M) |
| Published | 2020/05/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the importance of sharing information during critical times.
- Criticizes the White House's messaging on sharing information about the pandemic.
- Mentions the FBI warning American researchers about potential Chinese theft of research.
- Questions the lack of sharing valuable information for the sake of profit.
- Expresses concern about prioritizing money over public health.
- Advocates for a reevaluation of society and the system's values.

### Quotes

- "In times like these, when lives literally hang in the balance, money is what matters."
- "We've got to make a buck. Information related to what amounts to a cure, we're not going to share it."
- "It doesn't matter that failing to share it may delay development."
- "The weight of the US government will back up the idea that it's okay to delay to make a few extra bucks."
- "It's probably something we want to look at."

### Oneliner

Beau criticizes the prioritization of profit over public health, questioning the lack of sharing vital information during critical times.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Advocate for transparent and open sharing of information (implied)
- Support policies that prioritize public health over profit (implied)

### Whats missing in summary

Beau's passionate delivery and nuanced arguments

### Tags

#Sharing #Information #PublicHealth #Government #Profit


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about sharing.
Something I thought most people understood, thought they learned about it at a young age
from PBS.
I guess not.
If you've been watching the news cycle, you know there's a whole bunch of stories out
about sharing.
Specifically sharing information in times like these, when it's really important to
share information.
So tonight we will talk about sharing.
Okay, so if you've missed it, the White House's media machine has been working overtime, running
around the clock, to put out the message that none of this is the President's fault, because
early on another nation may possibly have failed to share less than reliable information.
Trump's blameless.
He did nothing wrong, because China may have failed to share less than reliable information.
Key takeaway.
But the fact that they may or may not have done that doesn't really matter, because apparently
we got the information from another source, but we didn't act on it anyway, because Jared
Kushner shared the information that if our response was good, that might scare the stock
market.
And we can't have that.
But the messaging for the last couple weeks has been, it's like super important to share
information in times like these.
And yes, that's true.
It is.
But China probably should have shared even the less than reliable information, because
it could have helped piece stuff together in other places.
This is a global thing.
Yeah, I will agree with the White House's messaging there.
So now we come to today.
American researchers have been warned by the FBI that China may be attempting to steal
their research, their information, and were provided advice on how to beef up security
to make sure that doesn't happen.
If you are a diehard Trump supporter, take as much time with that as you need to.
Pause the video.
Roll it around.
I would suggest that if other nations are attempting to steal the information, we're
probably not sharing it.
In times like these, when it's super important to share information.
They're not going to crowdsource that information.
They're not going to crowdsource the research.
They're not going to try to get other points of view in on it, because we've got to monetize
it.
We've got to make a buck.
Information related to what amounts to a cure, we're not going to share it.
Because we've got to make money.
It doesn't matter that failing to share it may delay development.
And in that time that it's delayed, we're going to suffer loss.
That doesn't matter, because we've got to make some money.
We can't even say that it's not intentional.
The researchers are getting advice from the government on how to make sure we keep the
information secret.
We keep it secured.
That's probably a really good indication that we need to take a hard, hard look at our society
and our system.
In times like these, when lives literally hang in the balance, money is what matters.
And the weight of the US government will back up the idea that it's okay to delay to make
a few extra bucks.
It's probably something we want to look at.
Anyway, it's just a thought.
Now have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}