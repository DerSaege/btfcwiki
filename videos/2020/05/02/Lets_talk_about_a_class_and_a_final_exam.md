---
title: Let's talk about a class and a final exam....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=I-UUJy77Fo8) |
| Published | 2020/05/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States is at a final exam in self-governance, with a big debate on the effectiveness of lockdowns.
- Good ideas should not require force, but lockdowns and reopening pose a catch-22 situation.
- The government reopening doesn't mean rushing back to normal; social distancing and limiting exposure are still vital.
- There's no return to normal until an effective treatment or vaccine is available.
- Individuals need to lead themselves as the government lacks effective empowerment.
- Many locations reopening do not meet safety guidelines, and authorities prioritize reelection impact over public welfare.
- An educated populace with critical thinking skills is necessary to hold representatives accountable.
- People must unite and look out for each other, especially the working class.
- Leadership is lacking, and individuals must take charge to ensure their safety and well-being.

### Quotes

- "There is no return to normal until we have an effective, reliable, available treatment or vaccine."
- "You and me, we have to lead ourselves because we don't have leadership."
- "The American people can't unite behind that because they're too busy playing elephant and donkey."

### Oneliner

The US faces a final exam in self-governance amid debates on lockdown effectiveness, urging individuals to lead themselves until a treatment or vaccine is available.

### Audience

US citizens

### On-the-ground actions from transcript

- Social distance and limit exposure to slow the spread (implied)
- Develop critical thinking skills and hold representatives accountable (implied)
- Unite and look out for each other, especially the working class (implied)

### Whats missing in summary

Importance of critical thinking skills and unity in holding representatives accountable for public welfare.

### Tags

#SelfGovernance #LockdownDebate #Leadership #CriticalThinking #CommunityUnity


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about a class,
a debate, and a final exam.
Because the United States is at that final exam right now.
We've been given a course, a class, in self-governance
and how important it is.
We've received the class, and now we're
at the point of the final exam.
There's been a big debate among people of my ideological bent
the whole time this is going on.
Because we're watching with utter fascination.
One of the key beliefs is that good ideas don't require force
and that you can lead your life better than somebody in DC
or your state capital or whatever.
That's one of the core beliefs.
And obviously some of the measures, like lockdowns,
they fly in the face of that.
And this has been a debate that even
the people that are behind the scenes on this channel
have been engaged in.
Because the lockdown is force.
Good ideas don't generally require force.
However, not having the lockdown in place
means that people have to go out to go to work.
Because if they don't, they're going
to be thrown out on the street.
And that's a type of force.
It's coercion.
So there's that catch-22, because the system we live in
is very different than the ideology we want.
So that debate has raged.
And you've probably heard me say,
we don't need lockdown orders.
Because you should know enough to do it yourself.
We've reached that point.
There's a whole bunch of places reopening.
There are a whole bunch of places
where the government is now saying,
yes, it's OK to go out.
Is it?
People keep saying we want to return to normal.
I'm going to go ahead and give you a cheat sheet
for the exam.
There is no return to normal until we
have an effective, reliable, available treatment
or vaccine.
There's no return to normal until that point.
Just because the government opens everything back up
doesn't mean it's time to rush out to the mall.
You still need to social distance.
You still need to limit your exposure.
You still need to do all this stuff.
The class started when Trump said,
one day it'll disappear like a miracle.
It'll be gone.
And it kind of ended when he admitted
that if we keep it under 100,000, that's pretty good.
That's where we're at.
You have to lead yourself at this point.
Yeah, some of you are going to be forced, coerced,
into going back to work locations
that are probably unsafe, where you run a much higher
risk of being exposed, because we don't have a system that
empowers the individual.
We don't have a system with a baseline
that allows people the freedom to truly choose
without coercion.
There is always the implied force, the poverty,
that can come crashing down at any moment
if you don't play by the system's rules.
So with these locations, a lot of them that are opening up,
they don't even meet the president's week guidelines.
Have two weeks of decline.
They don't meet that.
Some of them are at their peak, and they're reopening.
They don't care.
Those in DC, those in your state capital,
generally do not care about you.
You are a number.
And what matters is, can they keep the number of lost
below the number that is going to impact the reelection?
That's it.
That's it.
Generally speaking, that's all they care about.
You have to decide where you're going to be at.
Yeah, a lot of people are now going to have to go to work.
They have to.
They're being forced to.
But that doesn't mean that you have to take other risks.
You still need to social distance.
You still need to limit your exposure.
You still need to do what you can to slow the spread.
There are going to be losses until there
is an effective, available, reliable treatment or vaccine.
You have to do what you can to make sure you're not one of them.
An educated populace is one of those things
that the United States has to develop,
a populace with critical thinking skills.
If we had that, we wouldn't be so accepting of our senators
and representatives bailing out large companies, their friends,
every time something goes wrong and leaving everybody
at the bottom twisting in the wind.
That seems pretty basic.
But the American people can't unite behind that
because they're too busy playing elephant and donkey.
We have to look out for each other.
Those of the working class, those who generally get forgotten
because we can't afford lobbyists,
have to look out for each other.
You and me, we have to lead ourselves
because we don't have leadership.
We have rulers.
And they decided that a whole lot of people
are acceptable losses.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}