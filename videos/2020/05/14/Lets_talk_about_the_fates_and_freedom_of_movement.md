---
title: Let's talk about the fates and freedom of movement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zv6kjHzAazQ) |
| Published | 2020/05/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Acknowledges the irony and humor in life, discussing freedom of movement.
- Timing is key for discussing hypocrisy effectively.
- Points out the usefulness of calling out hypocrisy in emotional, illogical situations.
- Urges people to stop likening stay-at-home orders to Gestapo tactics.
- Notes the hypocrisy of those complaining about movement restrictions now.
- Compares reactions to movement restrictions at the southern border versus current stay-at-home orders.
- Raises the issue of privilege and safety concerns.
- Questions which group, those crossing borders or those violating stay-at-home orders, has caused more harm.
- Challenges the idea of citizenship granting special rights in the context of following the law.
- Reminds people of the consequences faced by others in authoritarian situations.

### Quotes

- "I wonder what would happen if you added up those lost that were attributable to each group."
- "It's almost like if you cheer on a government when they behave in an authoritarian manner, they become more authoritarian."
- "Those people on the bottom. The working class. All over the world."

### Oneliner

Beau addresses hypocrisy in reactions to movement restrictions, challenging privilege and safety concerns, and the consequences of authoritarian behavior.

### Audience

Social justice advocates

### On-the-ground actions from transcript

- Question hypocrisy and privilege (exemplified)
- Educate others on the consequences of authoritarian behavior (exemplified)

### Whats missing in summary

The emotional impact of confronting hypocrisy and privilege in societal attitudes.

### Tags

#Hypocrisy #Privilege #SocialJustice #Authoritarianism #CommunityPolicing


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about how the fates have a sense of humor.
How karma has jokes and how life is funny sometimes.
We're also going to talk a little about freedom of movement.
Now I'll be honest, I've been waiting to do this video.
I've wanted to do it for like a month.
But with a video of this sort, timing is really important.
And I think this is the appropriate moment.
Generally, I do not like creating a position that is based in large part on pointing out someone else's hypocrisy.
I don't like doing it because number one, it's a logical fallacy.
Just because somebody doesn't live up to the standard they advocate,
doesn't actually mean their position is wrong.
The other reason is that generally, it's ineffective.
However, there are times when it can become a useful tool.
When you're dealing with those who are over emotional, not ruled by logic, drama kings.
In that case, it can help people come together.
It can help people walk a mile in somebody else's shoes.
Somebody they think they're very, very different from.
But they probably aren't.
I would like it if people would stop referring to stay-at-home orders as Gestapo-esque.
I mean, that's a little over the top, don't you think?
Once somebody has their entire family wiped out because they violated a stay-at-home order,
I mean, then that comparison would make sense. Until then, it's just a little silly.
I mean, it's not like we have people all over the country rounding folks up
and sticking them in camps because they were in the wrong spot.
Oh, wait. That was a thing, wasn't it?
That's occurring. That happens.
And oddly enough, many of the people who are out there whining and crying right now
about their freedom of movement being interrupted were cheering it on,
waving the flag, bumper stickers, social media posts,
when it applied to the imaginary line down there on the southern border.
But now that that line is in your own front yard, well, now it's different.
Now it's different. Now it's an inconvenience.
And it's not just affecting those others, those people we like to scapegoat.
Maybe those two groups of people, they're not so different.
And I know, I can already hear it.
Whoa, whoa, whoa. They just need to follow the law down there.
Yeah, you too, buddy. I don't care if you go to the store to get your stuff.
Just do it legally. Do it the right way. Get your permission slip.
Only now it's not a visa. It's a mask.
Whoa, whoa. It's not even the same thing. I just want to make a living, take care of my family.
Man, this sounds familiar.
There is one difference, one big one.
Generally speaking, those crossing the imaginary line down there on the southern border,
they were trying to get to safety.
Everybody was concerned with the one in their front yard. Well, they don't care about safety.
They don't care about it because it's never been a concern for them.
They're privileged in that manner.
Some might call their behavior reckless at times.
You know, I wonder what would happen if you added up those lost that were attributable to each group.
Which one caused more harm?
I know. Then there's the big one.
Whoa, whoa, whoa, whoa. I'm a citizen. I've got rights.
First, that's not how the Constitution works there, patriot.
But let's just pretend that it is.
The fact that you want to use the fact that you are a citizen as a shield
means that you are more obligated to the social contract you apparently hold so highly.
You are more obligated to follow the law.
Do what your betters tell you to do.
See, they supported it months ago because they never thought it would happen to them.
Because they knew where the line was, and they were on the right side of it.
It was only going to affect those others.
Those people. Don't care about them.
See, a funny thing happens when you're talking about authoritarian governments.
The line often moves.
And it did.
It's almost like if you cheer on a government when they behave in an authoritarian manner,
they become more authoritarian. Who knew? Not me. I'm shocked.
There's no difference. There never is.
Those people on the bottom. The working class. All over the world.
Those borders don't really divide them.
They have the same motivations, the same dreams, the same hopes.
The only difference is who's manipulating them from the top.
Who's telling them which is the other. It's acceptable.
It's a scapegoat.
It's really the only difference.
Yeah, I get it.
And the way you're confined.
I understand.
But I would point out you are confined at home with Netflix and your family.
Your family wasn't ripped apart and thrown into separate camps to dissuade you from making non-essential trips.
So until that happens, perhaps we can leave the Gestapo-esque rhetoric at home.
Because all it's really doing is reminding people that just a couple months ago, you were the brown shirts.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}