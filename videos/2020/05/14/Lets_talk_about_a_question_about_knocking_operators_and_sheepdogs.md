---
title: Let's talk about a question about knocking, operators, and sheepdogs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=f565WO-Oxjo) |
| Published | 2020/05/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a question about banning certain types of entries, specifically no-knock entries, in law enforcement. 
- Stating that no-knock entries shouldn't be banned outright, but are overused (98% of the time) and inherently risky.
- Explaining the appropriate circumstances for a no-knock entry: when the suspect is dangerous and likely to flee.
- Emphasizing that no-knock entries should not be used for suspects who are not dangerous, as the risk is not justified.
- Suggesting a more coordinated approach where regular cops serve the warrant while a team of operators apprehend the suspect to reduce risk.
- Criticizing the lack of training and overreliance on firepower in these operations, leading to risks for innocent civilians.
- Urging law enforcement to prioritize preparation, intelligence, surveillance, and training over dynamic entries.
- Calling for significant reforms in law enforcement practices to prevent further unnecessary loss of life due to negligence.

### Quotes

- "The risk is absorbed by the people that you're ostensibly there to protect."
- "If your guys can't consistently put their rounds into a 5x7 card under pressure, real pressure, they have no business doing this."
- "Any loss of life is firmly on the hands of the department."

### Oneliner

Addressing the overuse and risks of no-knock entries, Beau calls for significant reforms in law enforcement to prioritize training and preparation over dynamic entries, to prevent unnecessary loss of life.

### Audience

Law Enforcement, Police Officers

### On-the-ground actions from transcript

- Train law enforcement officers to prioritize preparation, intelligence, and surveillance over dynamic entries (implied)
- Implement significant reforms in law enforcement practices to prevent unnecessary loss of life due to negligence (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the risks and overuse of no-knock entries in law enforcement, urging for reforms to prioritize training and preparation to prevent unnecessary loss of life due to negligence.

### Tags

#LawEnforcement #Reforms #Training #NoKnockEntries #RiskMitigation


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a question I got.
And sheepdogs and operators.
This video is mainly for cops.
Everybody should watch it though because it's going to give you a whole lot of insight into
a very lively debate that is occurring right now.
The question was about whether or not certain types of entries should be banned.
What people are calling no-knocks.
In our current system, no, no, they shouldn't be banned.
They are an extremely useful tool in certain circumstances.
However, big however here, I don't know an exact number but I'm saying 98% of the times
it gets used is not in those circumstances.
It is way overused and it's an inherently risky thing.
Okay so when is it appropriate to do a no-knock?
When the suspect is dangerous or is dangerous and will flee.
If they're going to flee and they're not dangerous, let them.
You can get it later.
There's no reason to accept the risk for somebody who's not dangerous.
So take that one out.
That's it.
Period.
Full stop.
There are other situations in which the same tactics get used like if someone's holding
someone.
But that's a little bit different.
That's not what's being discussed right now.
I know that by current thought and doctrine, people say that it's to preserve evidence.
No that's just flat out wrong.
No.
Because when they say that what they're talking about is well the people inside might flush
it.
If the quantity is so small that it can be flushed, it's not worth doing a dynamic entry
over.
It's not to be used for every penny any person that comes along.
It's to be used for the big fish.
You guys want to be operators right?
Sills, Delta, you want to be like them.
They don't go after the little guys.
That's not when they're used.
Because it's special.
Because it's inherently risky.
So that's not the case.
So how do you do that?
You want to preserve evidence and you're worried that the people inside might flush it.
Might get rid of it.
Or it's something that can be gotten rid of in another way.
Well you have normal cops go up and serve a normal warrant when they're not home and
go inside and preserve the evidence.
At the exact same time you have your team of operators get the suspect.
I know that's hard.
That's hard.
It requires like coordination and everything.
Two teams is really complicated.
I thought you wanted to be operators.
You've got radios.
You can do this.
It's not difficult.
And there's a whole lot less risk.
There's a whole lot less risk.
And the thing is the risk because of the amount of firepower that these teams typically have
coupled with the lack of training that these teams typically have.
The risk is absorbed by the people that you're ostensibly there to protect as in that recent
incident rounds can end up in other residences.
It is not to be used all the time.
Now when you're talking about the dangerous suspect then the very first thing you have
to do is make sure they're there.
Make sure they're actually at the location you're going to.
That requires somebody on your team.
Somebody that actually is on your team.
Some $50 snitch doesn't count.
Somebody on your team has physically seen them, laid eyes on, they're inside, and they
haven't left.
If that hasn't happened and you aren't 100% certain that they're home, you can't do it.
You can't do it because by your own reasoning you're worried they're going to flee.
And if knocking on the door is going to provide them enough time to flee, imagine what happens
in the hours that occur after a botched raid.
You can't do it.
Pro tip, criminals don't normally change the address on their driver's license when they
move.
So if you're just using that, yeah, you're going to hit the wrong place a lot.
This is risky.
It's very, very dangerous.
It should not be used all the time.
And it is most dangerous for the people not involved.
I get it.
Your sheepdogs.
They're to protect the flock, the herd, from the wolf.
I like it.
It's actually a great analogy.
What happens when a sheepdog bites one of the flock?
They don't get to be a sheepdog anymore, do they?
That should probably happen.
If a team leader conducts something and it ends up at a house that's empty or they're
not at home or whatever, that person should not be employed anymore.
Because they put everybody, their team, the surrounding civilians, everybody at risk for
nothing.
This is inherently risky.
It seems like you should know that.
And then, when a situation like the recent incident occurs, people act surprised.
Because you don't have the training to go along with it.
You took a 40-hour course on room clearing.
That's not enough.
The whole reason dynamic entries are used is because they're disorienting.
They are guaranteed to trigger fight, flight, or freeze.
They won't always trigger freeze.
And if that's the case, and you're in the wrong spot, innocent people may return.
Because they don't know who you are.
They have no reasonable reason to believe that they're cops.
They're not doing anything wrong.
This is overused.
It's overused.
And I get it.
You want to be operators.
It's cool.
Talk to some of them.
Talk to some actual operators.
Ask them to break it down into percentages.
How much time do they spend kicking doors?
And how much time do they spend on preparation, intelligence, surveillance, training?
That part that you want to do, that's the final 1%.
That's like the last 7 minutes.
Everything else leading up to that is missing in most departments.
Training.
If your guys can't consistently put their rounds into a 5x7 card under pressure, real
pressure, they have no business doing this.
None.
This is something that needs to be reformed drastically.
Because otherwise the bags are going to keep getting filled.
And people are going to keep saying, we didn't know.
Yes you do.
Yes you do.
This isn't new.
It's been like this since the 80s.
Forty years to change.
To make it right.
To train.
To get your department and your teams up to speed.
And you haven't done it.
At that point, yeah.
Your department shouldn't be doing them.
At the end of the day, any loss of life, because this is supposed to be a life saving tool,
any loss of life is firmly on the hands of the department.
And if they haven't done the bare minimums that were covered here, it's not an accident.
It's negligence.
It's willful negligence.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}