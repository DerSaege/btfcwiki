---
title: Let's talk about the deeper side to shopping cart theory....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Yo8gX0sk90U) |
| Published | 2020/05/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of shopping cart theory, which examines social responsibility based on returning shopping carts.
- Comments on the deeper implications of shopping cart theory beyond just a social media gimmick.
- Explains how returning a shopping cart is a neutral act with no personal gain or punishment, making it a measure of social responsibility.
- Observes that most people are good based on the majority returning their shopping carts properly.
- Analyzes the locations where abandoned shopping carts are found, indicating varying levels of social responsibility.
- Questions the necessity of government force when education, leadership, and access could maintain order in society.
- Suggests that governments use force to maintain power rather than to ensure societal order.
- Proposes that society needs access, leadership, and education for functioning, questioning the need for violent and forceful governments.
- Points out that most people are inherently good, contradicting the narrative used by those in power to maintain control.
- Advocates for increased social responsibility and individual accountability to lead society effectively.

### Quotes

- "Most people are good."
- "That's all you need for a functioning society."
- "Shopping cart theory proves most people are good."
- "That is how you can change the world."
- "We're at a point in history where we can demand more of each other."

### Oneliner

Beau introduces shopping cart theory, showcasing how returning shopping carts serves as a measure of social responsibility and questions the need for forceful governments, advocating for increased individual accountability to lead society effectively.

### Audience

Community members

### On-the-ground actions from transcript

- Increase social responsibility within your community by taking individual responsibility for societal issues (suggested).
- Advocate for demanding more from each other in terms of social responsibility (suggested).
- Organize efforts to ensure that all shopping carts are returned to the designated areas, even for those who may not have immediate access (implied).

### Whats missing in summary

The full transcript delves into the societal implications of simple acts like returning shopping carts, challenging the need for forceful governments and advocating for increased individual responsibility in shaping a better future.

### Tags

#SocialResponsibility #CommunityEngagement #GovernmentPower #IndividualAccountability #ChangeTheWorld


## Transcript
Well howdy there internet people, it's Bo again.
So today we're gonna talk about something very deep,
very philosophical and very deep.
We're gonna talk about shopping cart theory.
Don't laugh.
It is actually way deeper than most people would think
just by looking at it.
It looks just like a gimmick on social media, and it is.
But if you follow the dominoes,
and it leads you to some unique territory, and I love it.
If you have no idea what I'm talking about
on social media right now,
people are discussing shopping cart theory.
And basically it's a little thing
that allows you to determine whether or not
you're a good or bad person
based on whether or not you return your shopping cart
to the little corral in the parking lot.
I don't know that good and bad are the right terms for it.
Think socially responsible would be a better one.
But good and bad will work for our demonstration here.
Why is it a good test for that?
Because there's no force.
If you don't do it, nobody's gonna jump out of the bushes
and grab you, right?
There's no benefit to you,
no personal gain by returning that cart.
And you lose nothing if you don't do it.
It's completely neutral.
It's just a good measure of whether or not
you will do your part
to keep things running in an orderly fashion.
It's kind of cool that way.
If you accept it as that,
what's the first thing you notice
when you go into a parking lot?
Most people are good.
Yeah, you're gonna see maybe five or six shopping carts
thrown about, but most, the overwhelming majority,
50 or 100 of them, are gonna be in those corrals.
Most people are good.
Most people will do their part.
Where do you see the shopping carts that are just left out?
Some are like, you know, near the tree
that's in the parking lot, you know,
with one wheel up on the curb
so it doesn't roll out and hit anybody's car
because even though they're not gonna wheel it
all the way back, they're still gonna be
a little responsible with it.
And then there's some that are just left
in parking lots and spaces.
And then there's some that are, you know,
just left to roll wherever.
And those are typically way out,
away from the store,
where they've stopped putting those corrals.
Almost like access has something to do with it.
The means to be socially responsible
plays into it.
Kind of like they're like,
man, that's just too far away.
I can't do it.
I'm not paying for it, whatever.
There's a reason because they don't have that access.
Why do people do it?
Why are most people good in this situation?
Well, first, it's the obvious.
They know it's a social norm.
They know, they've been educated,
they've been led.
It's a social norm to do this.
And then the second is access.
Access certainly seems to play into it.
The means to be socially responsible.
That's all you need to keep things running.
If that's all you need to maintain order,
education, leadership, and access,
why do governments use force?
That's the whole justification for them,
is that they're there to maintain order
without them, everything would just,
you know, it'd be chaos.
So why do they use force when education,
leadership, and access is all you need?
Now, access, in the case of a society,
would be more along the lines of a baseline
that nobody can fall below.
Everybody's going to have certain things.
Maybe it's housing, healthcare, food,
you know, whatever.
Whatever that society deems.
But that's the baseline.
And it's everybody's social responsibility
to make sure that nobody falls below that.
So you need access, leadership, and education.
That's it.
That's all you need for a functioning society.
Well, then why do we have this
violence-based government,
this force-based government
that every government has?
Why do we have those people in power?
Because it's not about running a society.
It's about maintaining power.
And the easiest way to get people
to give up power is to convince them
that you're protecting them from somebody.
Those others, those evil people.
Because everybody's bad and out to get you.
But shopping cart theory proves
most people are good.
Most people will do their part
if they know what their part is.
It's almost like those in power
intentionally cultivate scapegoats
so you can kick down at them.
Feel better.
You're better than those people.
They're not good.
But most people are.
In fact, I would imagine
that most of you have seen somebody
grab a cart that wasn't theirs
and now return it to the corral.
Most people are good.
Most people would even give you a hand up,
help you out,
help those that didn't have
immediate access out.
And if something falls through the cracks,
there could be some kind of
organized safety net
to make sure that
all the carts make it to the corral,
even for those who don't have access.
It could happen.
Maybe have them wear little vests.
So what's the solution to this?
Maybe it's time to demand more
from each other
instead of demanding less.
Maybe it's time to increase
social responsibility,
take individual responsibility
for all of society.
That is how we would lead ourselves.
That is how you can change the world.
Shopping cart theory proves
that most of society's fears
aren't genuine.
They're not real.
They're just things people have
come to believe
because they were socially conditioned
to believe them,
just like they were conditioned to believe
that it's their responsibility
to return that cart.
It's probably time to up the ante.
We're at a point in history
where we can demand more of each other.
And now's the time to do it
because as our government
becomes less and less effective,
we're going to have to lead ourselves.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}