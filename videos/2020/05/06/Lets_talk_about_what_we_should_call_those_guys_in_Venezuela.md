---
title: Let's talk about what we should call those guys in Venezuela....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=R-RvgdHXZyE) |
| Published | 2020/05/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring terminology for individuals in Venezuela.
- Contrasting contractors and mercenaries.
- Drawing parallels between pirates, privateers, and the Navy.
- Emphasizing the difference a permission slip makes in the terminology.
- Addressing the moral implications of actions based on legality.
- Questioning the legitimacy of the government in Venezuela.
- Proposing a unified terminology for contractors and mercenaries.
- Warning against politicizing terminology to justify immoral actions.
- Mentioning the corporatization of the industry and its impact on international relations.
- Advocating for consistency in labeling individuals based on their actions rather than permissions.
- Expressing concerns about the conflation of legality with morality in current times.

### Quotes

- "Legality and morality are not the same thing."
- "It feeds into the idea that legality is morality."
- "If you allow it to be politicized, then we're playing into the idea that the government can authorize an activity that normally is immoral."
- "You should decide on one and apply it across the board."
- "The government can legitimize behavior that you'd normally view as immoral by simply saying it's okay."

### Oneliner

Exploring the thin line between legality and morality in defining contractors and mercenaries in Venezuela.

### Audience

Philosophy enthusiasts, international relations scholars.

### On-the-ground actions from transcript

- Settle on unified terminology (suggested).
- Avoid politicizing terms for contractors and mercenaries (implied).
- Advocate for consistency in labeling based on actions (implied).

### Whats missing in summary

The full transcript provides a nuanced exploration of the ethical implications surrounding the terminology used to describe individuals involved in military operations, urging for a thoughtful consideration of the relationship between legality and morality.

### Tags

#Contractors #Mercenaries #Legality #Morality #Venezuela #InternationalRelations


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about
accepted terminology,
slang,
a philosophical concept,
pirates, and a whole bunch of other stuff,
all in an attempt
to pin down
what we should call those guys down south.
The philosophical concept we're going to discuss is something we're going to
be revisiting.
It's something I've talked about on this channel before,
but it's been more than a year
and it's worth talking about again because to be honest when it comes to
base philosophical ideas
that we discuss on this channel,
this may be the most important one to me.
So I don't mind talking about it again.
Okay,
so
those guys down south,
the ones that got caught in Venezuela,
are they contractors
or are they mercenaries?
What's the difference between a pirate and a privateer?
Virtually indistinguishable
when they're in the field,
in the ocean, whatever.
They engage in a lot of the same behavior.
What's the real difference?
One has a permission slip.
One has a letter of mark,
an authorization from a state, a government, a ruler.
It's okay to engage in this behavior that is normally wrong
as long as you're doing it for me.
That's the difference.
That's the tangible difference.
It's the difference in the terminology.
Take it a step further.
What's the difference between a pirate
and the Navy
when that Navy is engaging in operations to
disrupt opposition shipping?
Not much.
Not really.
The involvement of state,
the involvement of a government, a ruler.
St. Augustine
told a story
about Alexander the Great.
And the story is that
he caught a pirate.
And he's talking to the pirate and
he's basically like, you know, what are you doing?
You're disrupting trade.
You're taking stuff that isn't yours.
You're a thief.
And the pirate's like, what are you doing?
You're doing all of the exact same stuff.
But because I have one boat,
I'm a pirate.
Because you have a fleet,
you're an emperor.
It goes to the idea
that might makes right.
More importantly,
it goes to the idea that
legality
is morality
and it's not.
The two things aren't the same.
Because more than likely,
the mighty
are the ones that wrote the rules,
that wrote the laws.
They wrote them to benefit themselves.
They can justify their own behavior that way.
Legality and morality are not the same thing.
And a permission slip
doesn't really change
the
moral implications of what's happening.
It just changes the terminology.
Now, the discussion about these guys down there,
it really boils down to
who you believe the legitimate government is down there.
That's really what it boils down to.
Because as far as we can prove,
it was all in-house.
You know, it was contracted
by Venezuelans,
an action that occurred in Venezuela.
Now, if you believe that Maduro
is the legitimate ruler,
well then they're mercenaries.
If you believe the opposition party is the legitimate ruler,
well then they're contractors.
The objections would be the same.
Now,
I'm in the US.
The United States
doesn't recognize Maduro as the legitimate guy.
So here,
they're probably going to be viewed as contractors.
Now, one of the reasons I would suggest that this terminology be
taken back,
because there's also slang terminology within that industry,
to keep it simple, if a contractor ever refers to somebody as a mercenary,
they're a horrible human being.
It's really what it means.
It doesn't have anything to do with any of the other stuff.
But I would suggest that we settle on terminology.
Either they are all contractors or they are all mercenaries.
If you want to say that anybody who picks up a gun for money
is a mercenary, cool, fine, whatever.
That would be more beneficial to me.
Because if we allow it to be politicized,
then we're playing into the idea that the government can authorize
an activity that would normally be seen as something that's immoral
and make it okay,
just by giving a permission slip.
And I think that's a bad idea because it feeds into the idea
that legality is morality.
And that's something that we're having a lot of problems with right now
in the United States.
We have trouble understanding that those aren't the same thing.
Now, for those who would say right now,
because they are supporters of Maduro,
these guys are mercenaries, and you want to use that term to politicize it,
I would ask if you think Maduro's security,
his close-in security, if they're mercenaries.
Or the consultants that trained their intelligence guys
who caught these guys, who infiltrated the movement.
Because they're all contractors or mercenaries,
whatever you want to call them.
They work for Wagner, a company out of Russia.
This industry has changed a lot. It has become very corporatized.
It is a facet of modern international relations.
Probably not a good thing,
but that's where we're at.
If we want to curtail it,
it would be a good idea to not allow it to be politicized
in the sense of good guys are contractors and bad guys are mercenaries.
If you want to use the legal thing about whether or not they have a permission slip, fine.
But if you don't care about that, you need to use the same term for both.
I don't particularly care which term it is.
But if the permission slip doesn't change your opinion of the action,
you should decide on one and apply it across the board.
Otherwise, you're reinforcing the idea that the government can legitimize
a behavior that you would normally view as immoral by simply saying it's okay.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}