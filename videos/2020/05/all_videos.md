# All videos from May, 2020
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2020-05-30: Let's talk about a missing takeaway about news interactions this week.... (<a href="https://youtube.com/watch?v=OohKtPH0Bs4">watch</a> || <a href="/videos/2020/05/30/Lets_talk_about_a_missing_takeaway_about_news_interactions_this_week">transcript &amp; editable summary</a>)

Journalists face force from law enforcement despite not being threats, revealing a critical flaw in threat assessment within policing that needs immediate attention.

</summary>

"None of these journalists were threats. None of them could be mistaken as a threat, but they had force used against them."
"Law enforcement has a really poor record of being able to threat-assess."
"Being a cop isn't even in the top 10 [most dangerous jobs]."
"Only you can stop city fires."
"This is something that needs to be addressed."

### AI summary (High error rate! Edit errors on video page)

Journalists, correspondents, and reporters had unfavorable interactions with law enforcement this week, sparking a debate on free speech implications.
The prevailing narrative that these events will chill freedom of the press is inaccurate; journalists are not deterred and often see such incidents as cool stories.
The more critical takeaway is that these journalists were not threats but still faced force from law enforcement.
Law enforcement's poor ability to accurately threat-assess is concerning; they should not be able to determine when force is necessary if they can't discern non-threatening individuals.
Beau suggests prioritizing threat assessment training over the warrior cop mentality in law enforcement.
He challenges the perception of policing as a highly dangerous job by citing statistics showing other professions, like fishermen and roofers, face higher risks.
The accuracy of police actions, especially in determining threats, should be held to a higher standard by the public.
Beau stresses the need to address the issue promptly, urging communities to push for reforms within their local police departments.

Actions:

for community members, activists, journalists,
Push for threat assessment training in local police departments (implied)
Advocate for reforms addressing the accuracy of police actions in determining threats (implied)
</details>
<details>
<summary>
2020-05-29: Let's talk about why Trump can't break up with Twitter.... (<a href="https://youtube.com/watch?v=smw0FS6S97s">watch</a> || <a href="/videos/2020/05/29/Lets_talk_about_why_Trump_can_t_break_up_with_Twitter">transcript &amp; editable summary</a>)

The president's reliance on Twitter to instill fear and hate reveals his inability to lead effectively, jeopardizing his re-election chances.

</summary>

"It's sad that with everything going on right now, the president's primary concern is maintaining a tool to tell people what to be afraid of and who to hate."
"He can't lose it. He can't leave the platform."
"Because he knows that if he can't do that, if he can't use Twitter, his only options are to speak publicly or actually lead."

### AI summary (High error rate! Edit errors on video page)

Introduces a lighthearted topic before diving into heavier subjects of the day.
Explains the ongoing battle between the president and Twitter.
Details how Twitter fact-checked the president's misleading tweet and the subsequent executive order.
Mentions Twitter censoring the president after his reaction.
Analyzes why the president relies on Twitter to keep his base energized and why he can't leave the platform.
Criticizes the president's primary concern of using Twitter to instill fear and hate rather than leading effectively.

Actions:

for voters, concerned citizens,
Monitor the president's actions on social media and hold him accountable (implied)
Stay informed about political tactics and their implications (implied)
</details>
<details>
<summary>
2020-05-29: Let's talk about the charging decision.... (<a href="https://youtube.com/watch?v=MSOffl6tU04">watch</a> || <a href="/videos/2020/05/29/Lets_talk_about_the_charging_decision">transcript &amp; editable summary</a>)

Beau provides a critical update on the delayed arrest, questionable charges, and the need for fighting necessary battles amidst a chaotic America.

</summary>

"Why'd you let the city burn? Could have charged that on day one."
"You fight the fights that need fighting."
"We have an economy in shambles. We have a hundred thousand gone due to inaction."
"We have a broken justice system."
"I really hope that America is great now. Because I don't think I can stand for it to get much greater."

### AI summary (High error rate! Edit errors on video page)

Provides an update and a follow-up after a wild week following his cautious initial analysis.
Expresses disappointment in the delayed arrest and the choice of charges in the case.
Questions the lack of evidence regarding Mr. Floyd's intent and criticizes the charges brought against the officer.
Believes that the current charges are not sufficient and calls for fighting the necessary fights.
Raises concerns about the state of the country, including the economy, COVID-19 deaths, media suppression, and the justice system.
Expresses hope for a better America amidst the chaos.

Actions:

for activists, justice advocates,
Fight for the necessary charges and justice (implied)
Advocate for transparency and accountability in law enforcement (implied)
Stand up against injustice and inequality (implied)
</details>
<details>
<summary>
2020-05-28: Let's talk about the light at the end of the tunnel.... (<a href="https://youtube.com/watch?v=xw0bApFnwJg">watch</a> || <a href="/videos/2020/05/28/Lets_talk_about_the_light_at_the_end_of_the_tunnel">transcript &amp; editable summary</a>)

Be proud of being part of continuous societal progress; there is no definitive end point in the journey towards improvement.

</summary>

"There is no tunnel."
"Be proud you're part of it."
"Don't look for that journey, that advancement to end."
"Just understand, you're never going to get there."
"We are planting shade trees. We will never sit under."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of tunnels and addresses the overwhelming dramatic events happening currently.
He mentions receiving messages from people wondering when things will improve.
Beau talks about the common phrase "light at the end of the tunnel" and questions its effectiveness as a framing device.
Using the example of societal progress on interracial dating, Beau illustrates how change can be imperceptible over time.
He points out that despite challenges, humanity generally progresses forward on various issues.
Beau encourages embracing ongoing societal improvement efforts without waiting for a definitive end point.
He compares societal challenges to a moving train, suggesting that progress is continuous and requires constant effort.
Beau underscores the need for continuous improvement even if major issues like poverty or climate change were miraculously solved.
He acknowledges that there will always be new battles to fight and improvements to make.
Beau concludes by urging people to keep moving forward and embracing the journey of societal progress.

Actions:

for individuals advocating for societal progress,
Embrace ongoing societal improvement efforts (implied)
Keep moving forward in the fight for progress (implied)
</details>
<details>
<summary>
2020-05-27: Let's talk about the Trump, Twitter, and the First Amendment.... (<a href="https://youtube.com/watch?v=3iaFxD0N8t4">watch</a> || <a href="/videos/2020/05/27/Lets_talk_about_the_Trump_Twitter_and_the_First_Amendment">transcript &amp; editable summary</a>)

Twitter fact-checks the president, exposing his misunderstanding of free speech and the First Amendment, while Beau advocates for politicians to be fact-checked for accountability.

</summary>

"Twitter is not stifling free speech. They are exercising social responsibility and providing additional speech."
"It is embarrassing. It is appalling. It is pathetic that the President of the United States does not understand one of the most cited parts of the U.S. Constitution."
"I personally think that all politicians should be fact checked on their statements."

### AI summary (High error rate! Edit errors on video page)

Twitter fact-checked the president's tweet, and he reacted by accusing Twitter of interfering in the presidential election.
The president claimed Twitter was stifling free speech, but Beau points out that the First Amendment is meant to protect people from the government, not the other way around.
Beau criticizes the president for not understanding the First Amendment and the concept of free speech.
He suggests that politicians, including the president, should be fact-checked to prevent them from deceiving the public.
Beau defends Twitter's actions as a form of social responsibility and additional speech, not censorship.

Actions:

for social media users,
Fact-check politicians (suggested)
Support platforms exercising social responsibility (exemplified)
</details>
<details>
<summary>
2020-05-27: Let's talk about Minneapolis, George Floyd, and what's next.... (<a href="https://youtube.com/watch?v=9aZ205Muu8A">watch</a> || <a href="/videos/2020/05/27/Lets_talk_about_Minneapolis_George_Floyd_and_what_s_next">transcript &amp; editable summary</a>)

Beau breaks down police incidents for accountability, but the George Floyd case leaves him questioning and anticipating more tragedy with missing information.

</summary>

"You can't fix it if you don't know what's broken."
"This isn't confusion. This is unexplainable."
"Everybody knows that those techniques lead to that."
"We need to know what happened here."
"I think there's more information that is going to make it more wrong."

### AI summary (High error rate! Edit errors on video page)

Breaks down events involving law enforcement captured on tape, analyzing the chain of events and policies involved.
Does this for accountability and to provide closure to families of victims.
Expresses uncertainty and disbelief regarding the George Floyd incident in Minneapolis.
Urges viewers to watch a video from a year ago that outlines unjust killings by cops.
Points out the difference between previous incidents and the prolonged, unacceptable nature of Floyd's case.
Speculates on missing information that could make the situation even more tragic.
Raises questions about the officers' histories, Floyd's state of mind, and prior interactions.
Expresses discomfort with the ambiguity and unexplainable nature of the incident.
Calls for full transparency from the Minneapolis Police Department through releasing all information.
Stresses the importance of understanding every detail to prevent such incidents from recurring.

Actions:

for advocates for justice,
Contact the Minneapolis Police Department for full transparency on the George Floyd incident (implied)
Advocate for the release of all video footage, audio recordings, and radio transcripts related to the incident (implied)
</details>
<details>
<summary>
2020-05-26: Let's talk about taking over the Democratic party and thermostats.... (<a href="https://youtube.com/watch?v=mtwNc7eMczk">watch</a> || <a href="/videos/2020/05/26/Lets_talk_about_taking_over_the_Democratic_party_and_thermostats">transcript &amp; editable summary</a>)

Thermostats and crosswalk buttons as metaphors for illusory control lead Beau to advocate for seizing the power vacuum in representative democracies and focusing on lower offices for deep systemic change within the Democratic Party.

</summary>

"Voting is not an extremely effective means of civil engagement, civic engagement, but it is what it is."
"Real leadership starts at the bottom, not at the top."
"Revolution by degrees, I guess."
"It's a long fight. It takes a long time, but it's something that will work."
"Your community network, that can help influence an election."

### AI summary (High error rate! Edit errors on video page)

Thermostats in big office buildings and commercial places are often placebos, giving employees the illusion of control without actually doing anything when adjusted.
Crosswalk buttons are similar to thermostats, often not connected to anything and merely synced with traffic signals for cars.
Voting may not be the most effective means of civic engagement, but it is a tool that can be used to exploit the naturally occurring power vacuum in representative democracies.
To take over the Democratic Party, focus on lower offices like sheriff, county commission, and state legislature, as they matter more than the presidency or Senate for deep systemic change.
Real leadership starts at the bottom, with positions like sheriff and state legislature being easier to win and requiring less money and influence.
By filling these lower offices with real progressives who think similarly, they can eventually move up into positions of real leadership as the older establishment politicians age out.
Supporting politicians in attainable positions who want deep systemic change is key to altering the overall view of the democratic establishment.
The strategy involves a long fight and gradual change, revolution by degrees rather than immediate wins.
Influencing elections through community networks can help get people in office who mirror your ideals.
The approach requires patience, as real leadership and systemic change start at the bottom.

Actions:

for progressive activists,
Organize and support real progressives running for lower offices like sheriff, county commission, state legislature (exemplified)
Build a community network to influence elections and support candidates reflecting your ideals (exemplified)
</details>
<details>
<summary>
2020-05-26: Let's talk about juries, freezing, and being into it.... (<a href="https://youtube.com/watch?v=1tg8dmJ-sIc">watch</a> || <a href="/videos/2020/05/26/Lets_talk_about_juries_freezing_and_being_into_it">transcript &amp; editable summary</a>)

Beau clarifies the nuances of freeze response in acute stress situations, cautioning against misconstruing biological reactions as consent, especially in legal contexts.

</summary>

"Freezing is normal. It's a biological response."
"An absence of evidence does not imply that they were into it."
"Succumbing to a biological response should not mean that the attacker goes free."
"That is a bad faith argument that is apparently being made in courtrooms."
"Because the prosecution may not always have the right witness to come in."

### AI summary (High error rate! Edit errors on video page)

Received a message from someone in Australia about a friendly debate on fight, flight, or freeze.
Feels obligated to talk about acute stress responses in relation to juries.
Explains the three acute stress responses: fight, flight, and freeze.
Freeze is often misunderstood, but it is part of the body's biological response to perceived threats.
Freeze can occur when decision-making is overwhelmed or in extremely traumatic situations with no way out.
Defense attorneys using the absence of evidence, like lack of physical resistance, to imply consent is wrong.
Emphasizes that freezing in a traumatic situation does not imply consent or being "into it."
Warns that succumbing to a biological response like freezing should not let attackers go free.
Urges jury members to understand the nuances of acute stress responses in courtrooms.
Stresses the importance of recognizing freezing as a normal biological response.

Actions:

for jury members,
Educate jury members on the different acute stress responses and how they can manifest in traumatic situations (implied).
Advocate for proper understanding and interpretation of freeze responses in legal settings (implied).
</details>
<details>
<summary>
2020-05-25: Let's talk about whether or not you should vote.... (<a href="https://youtube.com/watch?v=HqXsYPZ-ZLE">watch</a> || <a href="/videos/2020/05/25/Lets_talk_about_whether_or_not_you_should_vote">transcript &amp; editable summary</a>)

Beau addresses the dilemma of voting for Biden, the concept of harm reduction, and the importance of voting based on conscience and moral beliefs.

</summary>

"It's not unique. It feels unique but the reality is this lesser of two evils voting it's what we normally end up with in the United States."
"People who believe in one of those philosophies, they want everybody to get a fair shake. They want everybody to have some form of representation."
"Your conscience tells you, you understand it's not what your philosophy dictates, but these people need help."
"Your moral obligation if you are going to participate in voting, is to vote your conscience."
"It's sad that the base is already this lackluster in support, but a whole lot of people asked the waiter for a 100 proof bottle of truth, and the waiter brought back Trump lite."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of voting, philosophies, Biden, and the decision to vote.
Beau acknowledges the diverse viewpoints of his audience regarding voting for Biden.
The dilemma of choosing between Biden and Trump is discussed, with neither candidate being a perfect choice.
He addresses the concept of lesser of two evils voting, a common scenario in the United States.
Beau talks about how some philosophies view voting as taboo due to the moral responsibility attached to it.
The importance of considering the potential members of the candidates' administrations is emphasized.
Beau delves into the struggle of individuals with radical philosophies to find reasons to vote for Biden.
The moral dilemma faced by those with certain beliefs on voting is explored.
Beau touches on the notion of harm reduction and voting as a form of defense for marginalized communities.
The significance of rejecting certain harmful ideologies is discussed in relation to the upcoming election.
Beau expresses the importance of voting based on conscience and personal beliefs.
He refrains from endorsing any particular candidate, leaving the decision to the individual's moral judgement.

Actions:

for voters, philosophers,
Vote based on conscience and personal beliefs (suggested)
Support harm reduction for marginalized communities through voting (implied)
</details>
<details>
<summary>
2020-05-24: Let's talk about context, rights, and responsibilities.... (<a href="https://youtube.com/watch?v=VLDSdEz7JHU">watch</a> || <a href="/videos/2020/05/24/Lets_talk_about_context_rights_and_responsibilities">transcript &amp; editable summary</a>)

Beau addresses the importance of context in understanding rights and responsibilities, urging viewers to appreciate the broader intent of the Constitution.

</summary>

"Every right has an inherent responsibility."
"Maybe if you're going to have an opinion about what somebody meant 200 years ago, you need context."
"Context is important."
"In order to form a more perfect union, establish justice, ensure domestic tranquility..."

### AI summary (High error rate! Edit errors on video page)

Addresses rights and responsibilities in a video that was condensed and edited without full context.
Believes that keeping the overall intent and message intact despite lacking context is beneficial for promoting ideas.
Acknowledges that the edited video lacks context, but supports the approach taken.
Notes that the Constitution's intent was to establish a more perfect union, ensure justice, domestic tranquility, defense, welfare, and liberty.
Emphasizes that every right comes with an inherent responsibility.
Raises concerns about individuals quoting founders without understanding the context of their quotes.
Questions whether people today are motivated by quotes without context or by the larger intent defined in the Constitution.
Stresses the importance of understanding the context to avoid drawing incorrect conclusions.
Encourages viewers to seek and appreciate context when forming opinions about historical matters.
Reminds that the Constitution's purpose was not to cater to selfish interests but to create a better society.

Actions:

for history enthusiasts,
Seek context before forming opinions (implied)
Educate oneself on the larger intent of historical documents (implied)
</details>
<details>
<summary>
2020-05-24: Let's talk about a poll and debunk a theory.... (<a href="https://youtube.com/watch?v=6ivsETtDa5A">watch</a> || <a href="/videos/2020/05/24/Lets_talk_about_a_poll_and_debunk_a_theory">transcript &amp; editable summary</a>)

Beau addresses concerning beliefs held by Fox News viewers about Bill Gates using vaccines to track people, questioning the need for a chip when technology already provides extensive data on individuals.

</summary>

"The surveillance society you're worried about, it's already here."
"If you truly believe that this is a possibility, you should be screaming to have that wall torn down more than any liberal."
"The same people that believe there's this James Bond villain Bill Gates guy out there [...] are the same people who want to cut off their only means of escape."
"It's like they're easily led. Like somebody else is doing the thinking for them."
"This is not something I'm worried about. I am more worried that there's 75% of people who view it as a possibility who watch Fox News who don't understand that what they're worried about is already happening."

### AI summary (High error rate! Edit errors on video page)

Addresses a poll revealing concerning beliefs held by Fox News viewers regarding Bill Gates and vaccines.
Mentions the skepticism towards Bill Gates despite his early warnings about pandemics.
Notes the historical context of conspiracy theories related to tracking individuals.
Speculates on potential motives behind the belief that Bill Gates will use vaccines to track people.
Points out how much personal information individuals willingly provide through technology.
Questions the need for a chip when ample data is already accessible through various means.
Comments on the ironic situation where individuals worried about surveillance support restrictive measures like building a wall.
Criticizes the lack of critical thinking among those who believe in far-fetched theories.
Expresses concern about the lack of awareness among Fox News viewers regarding existing surveillance issues.
Concludes by reflecting on the implications of blind belief and misinformation spread by certain news networks.

Actions:

for critical thinkers,
Inform others about the implications of blind belief and the importance of critical thinking (implied).
Advocate for transparency and accountability in media coverage to prevent misinformation (implied).
</details>
<details>
<summary>
2020-05-23: Let's talk about the news you watch and a message from your family.... (<a href="https://youtube.com/watch?v=9bkmEKXOHTE">watch</a> || <a href="/videos/2020/05/23/Lets_talk_about_the_news_you_watch_and_a_message_from_your_family">transcript &amp; editable summary</a>)

Beau urges viewers to take a break from biased media consumption for 28 days to mend family divides and foster new perspectives.

</summary>

"Stop consuming that information for twenty eight days. Just stop."
"This habit that is forming is literally destroying American families."
"You're destroying your families for the sake of somebody who reveled in the fact that he was supported by the uneducated."
"You need to make a choice about what's more important to you. Your family or Red Hat?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Urges viewers to watch the entire video if it was sent to them, especially if it seems directed at them.
Expresses frustration at receiving messages from young people asking how to communicate with family members influenced by biased media.
Notes the divisive tactics employed by political outlets to drive a wedge between generations, especially in the lead-up to elections.
Advises viewers to stop consuming biased information for 28 days to gain new perspectives and reduce anger.
Suggests reputable news sources like Reuters, AP, NGOs, and Human Rights Watch for more balanced reporting.
Warns against believing opinions are equivalent to facts and consuming sources that fuel anger and division.
Criticizes news outlets that prioritize commentary over objective reporting and manipulate viewers with leading questions.
Emphasizes the importance of choosing between family harmony and political affiliations.
Condemns the impact of biased media on American families and the perpetuation of anger and division.
Encourages viewers to break the cycle of consuming divisive information and observe positive changes in their outlook.

Actions:

for family members influenced by biased media,
Stop consuming biased information for 28 days (suggested)
Seek news from reputable sources like Reuters, AP, NGOs, and Human Rights Watch (suggested)
</details>
<details>
<summary>
2020-05-22: Let's talk about what happens after and force multiplication.... (<a href="https://youtube.com/watch?v=ixH29vYrB_8">watch</a> || <a href="/videos/2020/05/22/Lets_talk_about_what_happens_after_and_force_multiplication">transcript &amp; editable summary</a>)

Beau stresses the importance of planning for what comes after any revolution and advocates for decentralized power through education and new ideas.

</summary>

"If you don't have an answer to that, it means nothing."
"I don't want to be one of the people who makes peaceful revolution impossible."
"Without the new ideas to back it up, it's not a revolution. It's just a hostile takeover."

### AI summary (High error rate! Edit errors on video page)

Received a message referencing a self-defense video in Cincinnati, sparking a larger point about what comes after.
Proposes a plan where individuals are trained to teach others, creating a network of millions ready to fight for change in less than a year.
Emphasizes the importance of preparation for what happens after any potential revolution or change.
Questions the effectiveness of movements that lack a clear post-revolution vision and systems in place.
Advocates for decentralized power through education and embracing new ideas.
Acknowledges the dirty and ugly reality of real-life revolutions, contrasting with glorified perceptions.
Stresses the need to reach those clinging to old ideas before meaningful change can occur.
Urges for building community networks, establishing redundant systems, and focusing on new ideas over violence.
Shares an example of a class in Cincinnati focusing on sustainable farming, community networks, and mutual aid instead of revolutionary tactics.
Refuses to teach offensive tactics to avoid inciting unnecessary violence and hostility.

Actions:

for change advocates,
Build community networks and establish redundant systems (implied)
Focus on embracing new ideas and education (implied)
Engage in mutual aid activities (exemplified)
</details>
<details>
<summary>
2020-05-22: Let's talk about a Trump and voting by mail theory.... (<a href="https://youtube.com/watch?v=KGRpDi3g_Ig">watch</a> || <a href="/videos/2020/05/22/Lets_talk_about_a_Trump_and_voting_by_mail_theory">transcript &amp; editable summary</a>)

President Trump campaigns against expanding mail-in voting, fearing increased voter turnout from demographics less likely to support him, like the working class.

</summary>

"Voter suppression is a term for it."
"Trump doesn't represent the working class."
"He doesn't care about the average American."
"Probably don't want that expanded either."
"He just wants those old people who sit at home and watch Fox News all day."

### AI summary (High error rate! Edit errors on video page)

President Trump is campaigning against expanding mail-in voting, which could benefit the working class who may find it difficult to take time off to vote.
Trump's voter base mainly consists of older individuals, particularly those aged 65 and above, who heavily rely on mail-in voting.
Trump fears expanding voting access to younger individuals who may not support him, as his base is primarily composed of retired or financially stable individuals.
Voter suppression is a term used to describe Trump's actions to limit voting access under the guise of preventing fraud.
Trump's concerns about voter fraud are often a cover for his fear of losing elections.
Trump's administration has consistently shown a lack of concern for the average American and their voices.
Beau believes that Trump prioritizes his approval ratings over the interests of the working class.
The military has been voting by mail for decades, showcasing the hypocrisy in Trump's opposition to expanding mail-in voting.
Beau suggests that Trump's resistance to expanding voting access stems from a desire to cater to his core supporters who are older and watch Fox News regularly.

Actions:

for voters, advocates,
Contact local officials to advocate for expanded mail-in voting access (suggested)
Join organizations working to combat voter suppression (exemplified)
</details>
<details>
<summary>
2020-05-21: Let's talk about the deeper side to shopping cart theory.... (<a href="https://youtube.com/watch?v=Yo8gX0sk90U">watch</a> || <a href="/videos/2020/05/21/Lets_talk_about_the_deeper_side_to_shopping_cart_theory">transcript &amp; editable summary</a>)

Beau introduces shopping cart theory, showcasing how returning shopping carts serves as a measure of social responsibility and questions the need for forceful governments, advocating for increased individual accountability to lead society effectively.

</summary>

"Most people are good."
"That's all you need for a functioning society."
"Shopping cart theory proves most people are good."
"That is how you can change the world."
"We're at a point in history where we can demand more of each other."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of shopping cart theory, which examines social responsibility based on returning shopping carts.
Comments on the deeper implications of shopping cart theory beyond just a social media gimmick.
Explains how returning a shopping cart is a neutral act with no personal gain or punishment, making it a measure of social responsibility.
Observes that most people are good based on the majority returning their shopping carts properly.
Analyzes the locations where abandoned shopping carts are found, indicating varying levels of social responsibility.
Questions the necessity of government force when education, leadership, and access could maintain order in society.
Suggests that governments use force to maintain power rather than to ensure societal order.
Proposes that society needs access, leadership, and education for functioning, questioning the need for violent and forceful governments.
Points out that most people are inherently good, contradicting the narrative used by those in power to maintain control.
Advocates for increased social responsibility and individual accountability to lead society effectively.

Actions:

for community members,
Increase social responsibility within your community by taking individual responsibility for societal issues (suggested).
Advocate for demanding more from each other in terms of social responsibility (suggested).
Organize efforts to ensure that all shopping carts are returned to the designated areas, even for those who may not have immediate access (implied).
</details>
<details>
<summary>
2020-05-20: Let's talk about life after Trump and the low points.... (<a href="https://youtube.com/watch?v=WA_M7Eqc2H4">watch</a> || <a href="/videos/2020/05/20/Lets_talk_about_life_after_Trump_and_the_low_points">transcript &amp; editable summary</a>)

Beau shares a story of resilience amidst the worst week ever, urging collective action and systemic change to move forward from current low points towards real progress.

</summary>

"That's what's scary to me."
"We're at a low point. Fact. But we're going to get through it."
"It's not going to be up to them. It's going to be up to you and me."
"We need new ideas. We need real systemic change."
"Being a part of actually advancing."

### AI summary (High error rate! Edit errors on video page)

Describes a true story of a man named Yamaguchi, a ship designer, who experienced the worst week ever during World War II in Japan, surviving both Hiroshima and Nagasaki atomic bombings.
Yamaguchi's resilience and survival despite severe injuries stand out as an example of getting through low points.
Beau draws parallels between the recovery of Hiroshima and Nagasaki after the devastation and the current low points faced by countries and individuals worldwide.
Expresses concern over leaders prioritizing personal gains and neglecting critical issues like infrastructure, mass incarceration, and poverty alleviation during challenging times.
Criticizes the glorification of past eras like the 1950s, pointing out the flawed nostalgia for a mythological time that neglects the realities of injustice and challenges faced then.
Beau warns about the dangers of the United States behaving like a failed state due to inaction on impending crises despite knowing about them.
Emphasizes the need for collective action, new ideas, and systemic change driven by people at the grassroots level to address current challenges effectively.
Encourages individuals to be part of shaping a better future by focusing on real progress and advancement rather than regressive ideals.
Stresses the importance of learning from low points to propel society, countries, and individuals forward with momentum and unity.

Actions:

for activists, community members,
Cooperate and work together with people at the grassroots level to generate new ideas and drive real systemic change (implied).
Take part in shaping a better future by actively advancing societal, national, and global progress through collective efforts (implied).
</details>
<details>
<summary>
2020-05-19: Let's talk about what getting lost can teach us.... (<a href="https://youtube.com/watch?v=tXn6eKsy4Ks">watch</a> || <a href="/videos/2020/05/19/Lets_talk_about_what_getting_lost_can_teach_us">transcript &amp; editable summary</a>)

Beau introduces the concept of getting lost, paralleling it with the directionless state of the country and world, urging for a collective pause to plan a cooperative and sustainable future.

</summary>

"We're lost. We're lost as a country. We are lost as a world. We're lost as a species."
"We need to come up with that course, and we need to get moving."
"This is not sustainable. It's not going to last."
"If we would sit down and come up with a plan, we'd be able to chart a course."
"We need to make the conscious choice to start advocating for that."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the idea of getting lost and what it can teach us as a country and a world.
In search and rescue, the first thing instructors teach is that lost individuals tend to do nothing to help themselves.
There is a strong impulse to keep moving once someone is lost, which Beau attributes to various factors such as evolutionary instincts or optimism.
Beau advises that if you get lost, the best course of action is to sit down for 20-30 minutes to analyze the situation and come up with a plan.
Even skilled individuals like Ralph Bagnold, a desert explorer, have felt the urge to keep moving when lost.
Moving without a plan can lead to misguided decisions, like following the sun as a navigation strategy.
Beau draws parallels between being physically lost and the state of the country and world, noting that many people recognize the current system is not ideal but continue to move without a clear plan.
He urges for a collective pause to come up with a plan and choose a course for the future that prioritizes cooperation over competition.
Beau stresses the importance of making a conscious choice to advocate for cooperation and shift towards a more sustainable path.
He concludes by underlining the critical need to address the state of being lost at a national, global, and species level, advocating for proactive change.

Actions:

for global citizens,
Pause collectively to analyze current situations and challenges, then develop a plan for the future (exemplified)
Advocate for cooperation over competition in all aspects of society (exemplified)
</details>
<details>
<summary>
2020-05-19: Let's talk about keys and masks.... (<a href="https://youtube.com/watch?v=Tfor4wG-WgE">watch</a> || <a href="/videos/2020/05/19/Lets_talk_about_keys_and_masks">transcript &amp; editable summary</a>)

Governments and entities push for mask-wearing to protect others; resisting is like driving impaired, a call for social responsibility amid conflicting messages from leaders like Trump.

</summary>

"Not wearing a mask is driving while impaired."
"Exercise some social responsibility."
"You don't need an order to tell you that."
"If you could pose a risk to others, yeah, wear a mask."
"There's a reason it doesn't make any sense."

### AI summary (High error rate! Edit errors on video page)

Governments and private entities are urging people to wear masks due to the risk of harm to others.
The concept of altering behavior for the safety of others isn't new, akin to not driving impaired.
Not wearing a mask is likened to driving while impaired.
Wearing a mask is about common sense and decency in protecting others, not a violation of constitutional rights.
Beau criticizes the idea that mask-wearing is the beginning of tyranny, pointing out the illogical arguments against it.
He questions the conflicting messages from certain leaders about the seriousness of the situation and the need for precautions.
Beau warns against blindly accepting conflicting ideas from leaders, as it leads to authoritarianism and tyranny.
Trumpism is portrayed as an ideology that requires followers to accept contradictory truths.
Beau urges individuals to think critically, exercise social responsibility, and not wait for orders to do what is right.
He encourages a deep examination of the logic behind certain ideas presented by political figures.

Actions:

for general public, mask skeptics,
Wear a mask without waiting for orders (suggested)
Think critically about the logic behind certain ideas (exemplified)
</details>
<details>
<summary>
2020-05-18: Let's talk about what the founders would do and the Constitution.... (<a href="https://youtube.com/watch?v=ZaO1R5uaZIU">watch</a> || <a href="/videos/2020/05/18/Lets_talk_about_what_the_founders_would_do_and_the_Constitution">transcript &amp; editable summary</a>)

Exploring the constitutionality of stay-at-home orders, Beau debunks claims that such measures are unconstitutional by providing historical context and stressing the importance of being well-informed.

</summary>

"If you cry wolf about tyranny, when tyranny arrives, nobody's going to believe you."
"Rights come with responsibilities, and one of them is to become educated."

### AI summary (High error rate! Edit errors on video page)

Exploring the conflict around statements claiming that stay-at-home orders are unconstitutional and causing economic devastation.
Emphasizing the importance of understanding the entire Constitution, especially the 10th amendment which grants power to the states.
Clarifying that in situations like the current pandemic, restrictions on movement and assembly can be constitutional.
Providing historical context like the yellow fever outbreak in Philadelphia during George Washington's presidency to debunk the claim that the founders wouldn't have tolerated such measures.
Stressing the need for responsible exercise of rights and the importance of being well-informed to avoid misinformation.

Actions:

for citizens, constitution advocates,
Educate yourself on the Constitution and its amendments (suggested)
Advocate for responsible exercise of rights (implied)
</details>
<details>
<summary>
2020-05-17: Let's talk about Eric Trump's comments last night.... (<a href="https://youtube.com/watch?v=DQ8uG6OghyE">watch</a> || <a href="/videos/2020/05/17/Lets_talk_about_Eric_Trump_s_comments_last_night">transcript &amp; editable summary</a>)

Beau challenges Eric Trump's claims, questions pandemic response, and stresses the importance of leadership during crisis.

</summary>

"Leadership is not magic."
"I'd be less worried about his ability to put 50,000 people in an arena every couple months, and more worried about his ability to put 50,000 people in the ground every month."
"If he wants to hold a rally, he can. Nobody's gonna stop him."
"I'd say it's reckless."
"He went on to say that after the election, this [pandemic] would magically disappear."

### AI summary (High error rate! Edit errors on video page)

Addresses Eric Trump's statement on Democrats shutting everything down to prevent President Trump from having rallies.
Questions the level of coordination and sacrifice needed to pull off such a feat.
Points out that if President Trump wants to hold a rally, he has the authority and resources to do so.
Criticizes President Trump's dismissal of the pandemic and belief that it will disappear magically after the election.
Raises concerns about the danger and recklessness of downplaying the seriousness of the pandemic.
Challenges the notion of magic solving the current crisis and stresses the importance of leadership.
Expresses worry about the potential consequences of President Trump's handling of the pandemic.
Calls for focus on leadership rather than relying on magic to address the ongoing crisis.
Suggests that President Trump's campaign team should prioritize saving lives over rally attendance.
Concludes by sharing thoughts and wishing the audience a good day.

Actions:

for voters, concerned citizens,
Call for responsible and transparent leadership in times of crisis (implied)
Prioritize public health and safety over political rallies (implied)
</details>
<details>
<summary>
2020-05-16: Let's talk about riches to rags and deflation spirals.... (<a href="https://youtube.com/watch?v=J3Yd8rs8Wgc">watch</a> || <a href="/videos/2020/05/16/Lets_talk_about_riches_to_rags_and_deflation_spirals">transcript &amp; editable summary</a>)

Beau explains deflation, warns of a potential spiral leading to economic crisis, criticizes government inaction, and advocates for injecting money into the economy to prevent collapse.

</summary>

"We're staring down the barrel of the doom loop."
"Your main concern was the economy, and you may have provided an immortal wound."
"But we do. Here we are."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of deflation and how it differs from inflation in terms of prices and purchasing power.
Describes a deflation spiral where decreasing demand leads to lower prices, impacting companies' revenue and potentially causing layoffs and closures.
Links prolonged deflation to the possibility of a Great Depression-like scenario.
Points out factors contributing to the current deflationary trend, including reduced demand due to people staying home and tariffs affecting prices.
Criticizes government response for not bailing out the lower-income individuals to prevent a deflationary spiral.
Suggests that injecting money into the economy through stimulus packages is necessary to keep the economy afloat.
Expresses skepticism about policymakers' understanding of economic issues and their ability to address the situation effectively.
Advocates for taking action to prevent economic collapse by pumping money into the economy, even if it means increasing the national debt.
Advises individuals to be cautious with spending, pay off debts, and keep cash on hand to prepare for potential job losses.

Actions:

for economic policymakers, concerned citizens,
Advocate for government policies that prioritize bailing out lower-income individuals (implied)
Monitor economic developments and government responses closely (implied)
Be cautious with spending, pay off debts, and keep cash on hand in preparation for potential job losses (implied)
</details>
<details>
<summary>
2020-05-15: Let's talk about what Trump can learn from John F. Kennedy and the Dunning-Kruger effect.... (<a href="https://youtube.com/watch?v=BINJILWlPHY">watch</a> || <a href="/videos/2020/05/15/Lets_talk_about_what_Trump_can_learn_from_John_F_Kennedy_and_the_Dunning-Kruger_effect">transcript &amp; editable summary</a>)

Beau explains what Kennedy could teach Trump, contrasting their leadership styles and stressing the importance of humility and expertise in decision-making.

</summary>

"President Trump, you are the man who accompanies Dr. Fauci to the podium. Nothing more."
"He was wearing his power lightly."
"Nobody believes, no rational person believes, that Trump is going to oversee a V-shaped recovery."
"You don't matter in those briefings."
"Kennedy could definitely teach it."

### AI summary (High error rate! Edit errors on video page)

Explains what President John F. Kennedy could teach President Trump, focusing on a critical lesson yet to be learned.
Contrasts President Trump with Dr. Fauci in briefings, illustrating the Dunning-Kruger effect.
Describes Kennedy's humility and willingness to delegate decision-making, citing an example from his Navy days.
Recounts a story about Kennedy's visit to France, showcasing his humility and understanding of what matters to people.
Criticizes President Trump's repeated wrong decisions and lack of importance in the current situation.
Emphasizes the need for experts to lead and inform the public, not the President.

Actions:

for politically engaged individuals,
Listen to experts and prioritize their insights in decision-making (implied)
Advocate for leadership that values humility and expertise (implied)
</details>
<details>
<summary>
2020-05-15: Let's talk about Trump, secrets, and sharing.... (<a href="https://youtube.com/watch?v=TvhNrXuxT9M">watch</a> || <a href="/videos/2020/05/15/Lets_talk_about_Trump_secrets_and_sharing">transcript &amp; editable summary</a>)

Beau criticizes the prioritization of profit over public health, questioning the lack of sharing vital information during critical times.

</summary>

"In times like these, when lives literally hang in the balance, money is what matters."
"We've got to make a buck. Information related to what amounts to a cure, we're not going to share it."
"It doesn't matter that failing to share it may delay development."
"The weight of the US government will back up the idea that it's okay to delay to make a few extra bucks."
"It's probably something we want to look at."

### AI summary (High error rate! Edit errors on video page)

Talks about the importance of sharing information during critical times.
Criticizes the White House's messaging on sharing information about the pandemic.
Mentions the FBI warning American researchers about potential Chinese theft of research.
Questions the lack of sharing valuable information for the sake of profit.
Expresses concern about prioritizing money over public health.
Advocates for a reevaluation of society and the system's values.

Actions:

for concerned citizens,
Advocate for transparent and open sharing of information (implied)
Support policies that prioritize public health over profit (implied)
</details>
<details>
<summary>
2020-05-14: Let's talk about the fates and freedom of movement.... (<a href="https://youtube.com/watch?v=zv6kjHzAazQ">watch</a> || <a href="/videos/2020/05/14/Lets_talk_about_the_fates_and_freedom_of_movement">transcript &amp; editable summary</a>)

Beau addresses hypocrisy in reactions to movement restrictions, challenging privilege and safety concerns, and the consequences of authoritarian behavior.

</summary>

"I wonder what would happen if you added up those lost that were attributable to each group."
"It's almost like if you cheer on a government when they behave in an authoritarian manner, they become more authoritarian."
"Those people on the bottom. The working class. All over the world."

### AI summary (High error rate! Edit errors on video page)

Acknowledges the irony and humor in life, discussing freedom of movement.
Timing is key for discussing hypocrisy effectively.
Points out the usefulness of calling out hypocrisy in emotional, illogical situations.
Urges people to stop likening stay-at-home orders to Gestapo tactics.
Notes the hypocrisy of those complaining about movement restrictions now.
Compares reactions to movement restrictions at the southern border versus current stay-at-home orders.
Raises the issue of privilege and safety concerns.
Questions which group, those crossing borders or those violating stay-at-home orders, has caused more harm.
Challenges the idea of citizenship granting special rights in the context of following the law.
Reminds people of the consequences faced by others in authoritarian situations.

Actions:

for social justice advocates,
Question hypocrisy and privilege (exemplified)
Educate others on the consequences of authoritarian behavior (exemplified)
</details>
<details>
<summary>
2020-05-14: Let's talk about a question about knocking, operators, and sheepdogs.... (<a href="https://youtube.com/watch?v=f565WO-Oxjo">watch</a> || <a href="/videos/2020/05/14/Lets_talk_about_a_question_about_knocking_operators_and_sheepdogs">transcript &amp; editable summary</a>)

Addressing the overuse and risks of no-knock entries, Beau calls for significant reforms in law enforcement to prioritize training and preparation over dynamic entries, to prevent unnecessary loss of life.

</summary>

"The risk is absorbed by the people that you're ostensibly there to protect."
"If your guys can't consistently put their rounds into a 5x7 card under pressure, real pressure, they have no business doing this."
"Any loss of life is firmly on the hands of the department."

### AI summary (High error rate! Edit errors on video page)

Addressing a question about banning certain types of entries, specifically no-knock entries, in law enforcement.
Stating that no-knock entries shouldn't be banned outright, but are overused (98% of the time) and inherently risky.
Explaining the appropriate circumstances for a no-knock entry: when the suspect is dangerous and likely to flee.
Emphasizing that no-knock entries should not be used for suspects who are not dangerous, as the risk is not justified.
Suggesting a more coordinated approach where regular cops serve the warrant while a team of operators apprehend the suspect to reduce risk.
Criticizing the lack of training and overreliance on firepower in these operations, leading to risks for innocent civilians.
Urging law enforcement to prioritize preparation, intelligence, surveillance, and training over dynamic entries.
Calling for significant reforms in law enforcement practices to prevent further unnecessary loss of life due to negligence.

Actions:

for law enforcement, police officers,
Train law enforcement officers to prioritize preparation, intelligence, and surveillance over dynamic entries (implied)
Implement significant reforms in law enforcement practices to prevent unnecessary loss of life due to negligence (implied)
</details>
<details>
<summary>
2020-05-13: Let's talk about why you should be a good person.... (<a href="https://youtube.com/watch?v=ukJIGJ7Kxoo">watch</a> || <a href="/videos/2020/05/13/Lets_talk_about_why_you_should_be_a_good_person">transcript &amp; editable summary</a>)

Be a good person, prioritize character over material wealth, and avoid harmful rhetoric to maintain valuable relationships and integrity.

</summary>

"Being poor is a lack of cash, not a lack of character."
"It's better to be poor than be a racist."
"If you kick down, people see it. And they have no reason to help you."

### AI summary (High error rate! Edit errors on video page)

Emphasizes the importance of being a good person because it's the right thing to do.
Criticizes those who need self-interest or partisanship as a reason to behave nicely.
Mentions the impact of baseless accusations during Supreme Court nomination hearings and how it can forever alter relationships.
Describes a story of a woman considering adoption based on the adoptive parents' behavior on social media.
Stresses the value of character over material wealth and Easy Street living, advocating for facing challenges and defeats in life.
Points out the significance of one's words and actions on social media, which can have lasting consequences.
Argues that showing loyalty to harmful ideologies or politicians at the cost of relationships and values is not worth it.
Encourages people to think for themselves and express genuine beliefs rather than blindly following harmful rhetoric.
Asserts that abandoning principles for party loyalty can lead to losing valuable relationships and being perceived as not a good person.
Affirms a belief in the prevalence of good people in the world who value not kicking others down.

Actions:

for social media users,
Choose your words and actions carefully on social media (implied)
Prioritize character and values over material gain (implied)
Think for yourself and express genuine beliefs (implied)
</details>
<details>
<summary>
2020-05-13: Let's talk about social progress and optimism.... (<a href="https://youtube.com/watch?v=IHNEoit5enw">watch</a> || <a href="/videos/2020/05/13/Lets_talk_about_social_progress_and_optimism">transcript &amp; editable summary</a>)

Beau talks about social progress, framing it as a battle between social progressives and conservatives, with a historical certainty that social progress always wins, encouraging optimism and perseverance.

</summary>

"On a long enough timeline the social progressive always wins."
"If you are on the front lines of a socially progressive issue today, you're just fighting a holding action."
"You're just hanging on until reinforcements show up."
"It's a historical certainty."
"It's how fast and how far we want to take it."

### AI summary (High error rate! Edit errors on video page)

Talks about social progress and optimism, reflecting on being asked how to stay calm and optimistic due to his channel's content.
Frames the channel as discussing social progress rather than left and right politics, defining the real framing as social progressive versus social conservative.
Explains the anger behind social issues for progressives and conservatives, with progressives seeking to right wrongs and conservatives preferring tradition and the status quo.
Shares his ability to stay calm and optimistic by disconnecting from direct impacts of social issues due to privilege, allowing him to see the bigger picture.
Asserts that on a long enough timeline, the social progressive always wins in history, despite occasional regressive periods.
Describes the process of social progress, starting with identifying injustices, presenting solutions, and embracing by open-minded individuals who pass it on to younger generations.
Gives a real-life example of shifting perspectives on interracial marriage across different generations, illustrating how social issues can evolve and be solved over time.
Mentions the historical trend of social progress starting in the United States and getting exported to other regions.
Encourages those fighting for socially progressive causes to stay strong, as they are just holding on until reinforcements arrive, with historical certainty of eventual victory.
Advocates for advancing socially progressive ideas while limiting dependence on government force as the key to winning in the long run, acknowledging it may not happen in their lifetimes but staying optimistic about being on the winning team.

Actions:

for community advocates,
Advocate for socially progressive ideas within your community (suggested)
Educate younger generations about social justice issues (exemplified)
Support and join movements fighting for social progress (implied)
</details>
<details>
<summary>
2020-05-12: Let's talk about success and leaving your mark.... (<a href="https://youtube.com/watch?v=yc_rShm376o">watch</a> || <a href="/videos/2020/05/12/Lets_talk_about_success_and_leaving_your_mark">transcript &amp; editable summary</a>)

Beau contrasts two historical figures' approaches to leaving their mark on the world and stresses the need for a long-term commitment to create lasting change.

</summary>

"You're going to have to be in it for a while."
"There'll be little changes every day and you can make little improvements every day."
"World's not going to change tomorrow, not completely."
"To get to where you're going, where you want to go, it's a long road."
"You've chosen a pretty big battleground."

### AI summary (High error rate! Edit errors on video page)

Beau contrasts two historical figures who aimed to leave their mark on the world but had different approaches and outcomes.
One figure, from a prominent political family, faced setbacks, including being captured by pirates, and worked to prosecute corrupt government officials.
Despite facing challenges and setbacks, the figure did not inspire fear in his captors and ultimately accomplished significant achievements.
The other figure was impulsive, setting fire to the Temple of Artemis to make an immediate impact.
Beau points out the importance of understanding the long, challenging road to making a lasting change in the world.
He warns that activism is exhausting and requires a long-term commitment to see real progress.
Beau stresses the need for activists to realize that change won't happen overnight but through persistent daily efforts.
Making little improvements each day is emphasized as a way to contribute to larger change over time.

Actions:

for activists, changemakers,
Build and improve gradually (exemplified)
Make little improvements daily (exemplified)
Understand the long-term commitment required for activism (exemplified)
</details>
<details>
<summary>
2020-05-12: Let's talk about following the money and taking Sen McConnell's advice.... (<a href="https://youtube.com/watch?v=dtIOl_eFXaI">watch</a> || <a href="/videos/2020/05/12/Lets_talk_about_following_the_money_and_taking_Sen_McConnell_s_advice">transcript &amp; editable summary</a>)

Beau suggests analyzing Mitch McConnell's campaign contributions to understand his stance on key issues like the stimulus package and health care accessibility.

</summary>

"Maybe you should display a little bit of urgency, given the fact that all of the experts are saying that we're going to be going through this ups and downs for a while."
"The Senate and the House, they could give a master class on corruption to all of these smaller governments that we tend to criticize."
"I strongly suggest you do this. Go see who has contributed to your candidate, to your representative."

### AI summary (High error rate! Edit errors on video page)

Beau suggests taking Mitch McConnell's advice on the stimulus package, despite their usual differences.
McConnell has expressed no urgency in passing the next stimulus package, wanting to see the effects of previous spending first.
Beau analyzes McConnell's campaign contributions from the last five years, focusing on companies contributing significant amounts annually.
McConnell has received substantial donations from big investment groups, Altria Group (Philip Morris), and the health care industry.
Campaign contributions seem to influence McConnell's stance on health care accessibility and profitability.
Beau points out donations from Geo Group, a private prison and real estate investment company.
Companies like FedEx and UPS also contribute to McConnell, potentially influencing his policies regarding competitors like the United States Postal Service.
Beau urges viewers to research their representatives' campaign contributions on OpenSecrets.org to understand the influence of money in politics.
Beau criticizes the significant role of money in shaping policies, suggesting that the common people lack effective lobbyists in the political system.
In light of economic experts predicting ongoing struggles, Beau calls for urgency in providing stimulus to those in need.

Actions:

for voters, activists, constituents,
Research and analyze campaign contributions of your representatives on OpenSecrets.org (suggested)
Advocate for urgency in providing stimulus to those in need, contacting elected officials if necessary (implied)
</details>
<details>
<summary>
2020-05-11: Let's talk about bad kids, utopias, and Lord of the Flies.... (<a href="https://youtube.com/watch?v=Y8c0FDZPbU0">watch</a> || <a href="/videos/2020/05/11/Lets_talk_about_bad_kids_utopias_and_Lord_of_the_Flies">transcript &amp; editable summary</a>)

Labeling individuals negatively based on low expectations creates self-fulfilling prophecies, challenging society to expect the best from humanity for a utopian future.

</summary>

"If you want a utopian society, you have to expect the best from people."
"Humanity isn't innately evil. Humanity just is. It's what we make of it."
"If we want utopia, we have to build it. Step by step."
"We should probably focus more on saying that it is possible. Not that it isn't."
"We're not there yet, but we're going to get there."

### AI summary (High error rate! Edit errors on video page)

Raises the issue of labeling individuals as "bad kids" based on low expectations.
Points out how low expectations can become a self-fulfilling prophecy for individuals.
Compares society's negative perceptions of humanity to labeling children negatively.
Challenges the idea that humans are inherently selfish or lazy.
Advocates for portraying humanity in a more positive light based on evidence of positive behavior.
Mentions the importance of expecting the best from people to achieve a utopian society.
Refers to a real-life story similar to "Lord of the Flies" where kids thrived, contradicting the negative portrayal of human nature.
Stresses the need to set high expectations to encourage positive behavior in individuals and society.
Emphasizes the role of education in shaping behavior and setting standards.
Encourages being a positive example for younger generations to create a better future.

Actions:

for educators, activists, parents,
Set high expectations for individuals and society, encouraging positive behavior (implied).
Be a positive example for younger generations to foster a better future (implied).
</details>
<details>
<summary>
2020-05-10: Let's talk about why people believe certain theories.... (<a href="https://youtube.com/watch?v=KwUPRbRCutw">watch</a> || <a href="/videos/2020/05/10/Lets_talk_about_why_people_believe_certain_theories">transcript &amp; editable summary</a>)

Beau explains why people buy into theories, unpacking the comfort, uniqueness, and projection factors while cautioning against potential exploitation by certain individuals.

</summary>

"The brain craves patterns. We look for patterns."
"They just think in a different way."
"They're very easily led."
"They do induce a very healthy skepticism of government."
"It's scary if we don't have a clear picture of what's going on."

### AI summary (High error rate! Edit errors on video page)

Explains why people buy into theories, including comfort in thinking there is a plan and a dislike for uncertainty.
Mentions that people may buy into theories because it makes them feel unique and boosts self-esteem.
Refutes the stereotype that those who believe in theories are not smart, stating that they just think differently.
Points out that the brain craves patterns, leading to the recognition of patterns even when there's no actual evidence.
Talks about projection as a significant factor in theories, where believers see authoritarian behavior mirrored in the groups they theorize about.
Notes the historical trend of politicians positioning themselves as opposition to theories, often authoritarian strongmen.
Suggests that theories themselves are not dangerous but can become so if manipulated by certain figures.
Recommends checking out Ashley Alker's debunking of a recent documentary that relies on creating comfort, uniqueness, and pattern recognition to appeal to viewers.
Stresses the importance of monitoring theories, particularly to prevent manipulation of believers by certain individuals.
Beau concludes by expressing the need to be cautious about those who attempt to exploit theories for their gain.

Actions:

for skeptics, critical thinkers,
Verify information before sharing or believing in conspiracy theories (implied)
Engage in healthy skepticism of government actions (implied)
</details>
<details>
<summary>
2020-05-08: Let's talk about signals, secrets, videos, and the future.... (<a href="https://youtube.com/watch?v=tZqye2q3srE">watch</a> || <a href="/videos/2020/05/08/Lets_talk_about_signals_secrets_videos_and_the_future">transcript &amp; editable summary</a>)

Beau addresses the importance of signals, codes, and videos, debunking conspiracy theories and advocating for evolution over revolution to create a better future.

</summary>

"We don't need revolution. We need evolution."
"We can change the system at play. We can change the way the land. We can make them obsolete."
"We don't get the system we want by fighting against the past. We get it by building the future."

### AI summary (High error rate! Edit errors on video page)

Addresses the importance of signals, codes, videos, and theories in a quick and concise manner.
Mentions an event down south where individuals got caught and appeared in videos.
Talks about the shift in standards regarding making videos for propaganda purposes.
Explains the theory that information becomes obsolete quickly in today's fast-moving battlefield.
Notes that certain signals in videos can convey messages without being overtly noticed.
Refers to a Wall Street Journal article that debunked conspiracy theories due to the likelihood of people talking.
Criticizes conspiracy theories that paint the opposition as infallible and the system as unbeatable.
Advocates for evolution over revolution to change the system by creating a redundant power structure based on cooperation.
Emphasizes the importance of building the future rather than fighting against the past.
Concludes by urging for a shift towards evolution instead of revolution to create the desired system.

Actions:

for activists, advocates, change-makers,
Build a redundant power structure based on cooperation (implied)
Focus on evolving systems rather than resorting to revolution (implied)
</details>
<details>
<summary>
2020-05-08: Let's talk about numbers, the future, and the carrot and the stick.... (<a href="https://youtube.com/watch?v=PtAcxPJrk1w">watch</a> || <a href="/videos/2020/05/08/Lets_talk_about_numbers_the_future_and_the_carrot_and_the_stick">transcript &amp; editable summary</a>)

Unemployment rises, conflicts between employers and workers escalate, and consumer spending decreases as safety concerns prevail during the economic crisis.

</summary>

"You have to go out, wear a mask."
"It’s always better to be a good person."
"Enjoy the start of the depression."

### AI summary (High error rate! Edit errors on video page)

Unemployment is at almost 15%, a significant number indicating the current economic crisis.
Despite jobs reopening, not all employees are returning due to health concerns, leading to conflicts between employers and workers.
Some employers are using government tools to force employees back to work, even if it jeopardizes their health.
States like Ohio, Alabama, and others are setting up systems for employers to report employees who refuse to return to work.
People's reluctance to return to work may also lead to decreased consumer spending on non-essentials.
Small businesses that fought to reopen might face permanent closure due to reduced revenue and higher expenses.
Beau criticizes employers who betray their employees' trust by reporting them for choosing safety over work.
He suggests that consumers should be informed about businesses that mistreat their employees during these times.
The current economic situation is predicted to worsen as people struggle financially and refuse to risk their lives for profit.
Beau advocates for individuals to prioritize their safety, seek media attention if needed, and take legal action against employers who compromise their well-being.

Actions:

for employees, consumers, activists.,
Contact the media if your employer jeopardizes your safety (suggested)
Wear a mask when going out (implied)
Inform others about businesses mistreating employees (implied)
</details>
<details>
<summary>
2020-05-08: Let's talk about a ray of hope in Georgia.... (<a href="https://youtube.com/watch?v=aEjgEM7fae0">watch</a> || <a href="/videos/2020/05/08/Lets_talk_about_a_ray_of_hope_in_Georgia">transcript &amp; editable summary</a>)

Beau warns against losing focus after a ray of hope in an injustice movement, urging continued support for justice even after arrests.

</summary>

"It's a win, right? No, it's not a win. It's a ray of hope."
"Don't let that happen here."
"It's not over. That fight will continue."
"You can use that bigotry, that racism against bigots and racists."
"Don't abandon them when they finally get a ray of hope."

### AI summary (High error rate! Edit errors on video page)

Talks about the danger of losing focus after a ray of hope in an injustice movement.
Mentions the case of Ahmaud Arbery in Georgia where two individuals were arrested for aggravated assault and murder.
Emphasizes that an arrest is not a win but a ray of hope.
Warns against people stopping their calls for justice after arrests, leading to a fall apart in the case.
Urges the community to keep fighting for justice even after arrests, as the fight continues within the court system.
Encourages allies to keep supporting the cause and using privilege to fight against bigotry and racism.
Stresses the importance of not abandoning the cause when there's a ray of hope.

Actions:

for community members, allies,
Keep supporting the cause by continuing to fight for justice (exemplified)
Hold onto the megaphone and amplify the voices seeking justice (exemplified)
Use privilege to fight against bigotry and racism (exemplified)
</details>
<details>
<summary>
2020-05-07: Let's talk about Trump asking us to be warriors.... (<a href="https://youtube.com/watch?v=ZM11nZ-tg8k">watch</a> || <a href="/videos/2020/05/07/Lets_talk_about_Trump_asking_us_to_be_warriors">transcript &amp; editable summary</a>)

Beau warns against rushing into reopening, stresses the need for caution, leadership, and individual responsibility in navigating through uncertain times.

</summary>

"You're going to have to lead yourself here."
"Lay down your life for the altar of corporate profits, I guess."
"Being a warrior is about mitigating risk."
"A warrior, a leader, would want to make sure that doesn't happen because that compounds everything."
"And generally, it should be noted that when you're acting in that manner, you're not fighting against the people in front of you. You're fighting for the people behind you."

### AI summary (High error rate! Edit errors on video page)

Critiques the President's eagerness to quickly reopen the economy without taking responsibility for the consequences.
Points out that rushing into reopening without a proper plan could lead to more harm.
Mentions the importance of having a safe and effective treatment or vaccine before fully stopping precautions.
Expresses concerns about the potential dangers of not maintaining precautions and the possibility of flare-ups.
Addresses the need for individuals to make informed decisions about when it's safe to resume normal activities.
Emphasizes the role of leadership in mitigating risks and protecting the well-being of the people.
Urges caution and awareness of possible supply chain interruptions that could have severe consequences.
Encourages individuals to be proactive in monitoring the situation and being prepared for potential challenges.
Stresses the importance of taking personal responsibility in navigating through uncertain times.
Concludes by reminding listeners to stay vigilant and prepared for any future developments.

Actions:

for individuals, concerned citizens,
Monitor the situation and stay informed about potential risks and developments (suggested).
Be prepared to re-implement extreme measures such as staying home if necessary (suggested).
Stay vigilant for possible interruptions in the supply chain and be proactive in addressing any challenges that may arise (suggested).
</details>
<details>
<summary>
2020-05-06: Let's talk about what we should call those guys in Venezuela.... (<a href="https://youtube.com/watch?v=R-RvgdHXZyE">watch</a> || <a href="/videos/2020/05/06/Lets_talk_about_what_we_should_call_those_guys_in_Venezuela">transcript &amp; editable summary</a>)

Exploring the thin line between legality and morality in defining contractors and mercenaries in Venezuela.

</summary>

"Legality and morality are not the same thing."
"It feeds into the idea that legality is morality."
"If you allow it to be politicized, then we're playing into the idea that the government can authorize an activity that normally is immoral."
"You should decide on one and apply it across the board."
"The government can legitimize behavior that you'd normally view as immoral by simply saying it's okay."

### AI summary (High error rate! Edit errors on video page)

Exploring terminology for individuals in Venezuela.
Contrasting contractors and mercenaries.
Drawing parallels between pirates, privateers, and the Navy.
Emphasizing the difference a permission slip makes in the terminology.
Addressing the moral implications of actions based on legality.
Questioning the legitimacy of the government in Venezuela.
Proposing a unified terminology for contractors and mercenaries.
Warning against politicizing terminology to justify immoral actions.
Mentioning the corporatization of the industry and its impact on international relations.
Advocating for consistency in labeling individuals based on their actions rather than permissions.
Expressing concerns about the conflation of legality with morality in current times.

Actions:

for philosophy enthusiasts, international relations scholars.,
Settle on unified terminology (suggested).
Avoid politicizing terms for contractors and mercenaries (implied).
Advocate for consistency in labeling based on actions (implied).
</details>
<details>
<summary>
2020-05-05: Let's talk with Trae Crowder the liberal redneck.... (<a href="https://youtube.com/watch?v=a3SRXCCwdeI">watch</a> || <a href="/videos/2020/05/05/Lets_talk_with_Trae_Crowder_the_liberal_redneck">transcript &amp; editable summary</a>)

Beau chats with Trey Crowder, the liberal redneck comedian whose authentic content challenges stereotypes and sparks meaningful dialogues, fostering understanding and showcasing the diverse perspectives in the South.

</summary>

"Write what you know and that's pretty much all I've ever done."
"People should be aware that the image of Southerners isn't accurate at all."
"I always wanted to be a comedian. Having said that, I think it's cool if my work reaches people and starts a conversation."

### AI summary (High error rate! Edit errors on video page)

Introducing Trey Crowder, the liberal redneck comedian with substance behind the jokes.
Trey explains his journey from standup comedian to creating videos with a political twist.
Trey's inspiration to start making videos came from witnessing a viral far-right preacher video.
The success of Trey's videos led him to quit his day job and pursue comedy full-time.
Trey's authenticity and genuine upbringing in a rural, poverty-stricken part of Tennessee influence his content.
Beau and Trey bond over the experience of hiding their accents and eventually embracing them.
They address stereotypes about Southerners and the importance of debunking misconceptions.
Trey talks about his book, "The Liberal Redneck Manifesto: Dragging Dixie Out of the Dark," co-written with his friends.
Despite the success of the book, Trey feels disconnected from that period of his life, which was a whirlwind of change.
Beau and Trey touch on how Trey's comedic work has had political ramifications and sparked meaningful dialogues.
Trey shares his mixed feelings about being seen as an activist rather than solely a comedian and the expectations that come with it.
They dive into Trey's intent behind his work, focusing on challenging stereotypes and showcasing the diversity in the South.
Trey stresses the importance of not assuming extreme viewpoints in dialogues with others to foster better relationships and understanding.
Trey talks about the challenges of the current standup comedy scene due to the pandemic and focuses on his upcoming projects.
Despite uncertainties, Trey remains optimistic about progress and encourages everyone to soldier on.

Actions:

for comedy fans, southern residents,
Listen openly and without assumptions to bridge divides (implied)
Challenge stereotypes and misconceptions about Southerners (implied)
</details>
<details>
<summary>
2020-05-05: Let's talk about a Florida Man Sheriff's race in Broward.... (<a href="https://youtube.com/watch?v=xxeDQP6tNjA">watch</a> || <a href="/videos/2020/05/05/Lets_talk_about_a_Florida_Man_Sheriff_s_race_in_Broward">transcript &amp; editable summary</a>)

Beau gives a Florida perspective on a scandalous sheriff's race, showcasing how past actions may sway voters in the Sunshine State.

</summary>

"That's something that gives you a Florida man card right there."
"Y'all did not think this dude's gonna win in a landslide."
"This is the most Florida thing ever."

### AI summary (High error rate! Edit errors on video page)

Florida man sheriff's race with a scandal emerges between Sheriff Israel and Sheriff Tony.
Governor DeSantis replaced Sheriff Israel with Sheriff Tony post-Parkland incident.
Sheriff Tony faced a scandal from his past involving self-defense during a dealer incident as a 14-year-old in Philadelphia.
The sealed juvenile case from Sheriff Tony's past became public knowledge during the election.
The scandal doesn't seem to affect Florida voters, who value Sheriff Tony's past actions in protecting others.
The political strategist's move against Sheriff Tony backfired, potentially boosting his election chances.
The race contrasts Sheriff Tony's proactive response at 14 with Sheriff Israel's handling of the Parkland incident.
Beau finds the situation comical and quintessentially "Florida."

Actions:

for florida voters,
Stay prepared for hurricane season (suggested)
Stay informed and engaged in local politics (implied)
</details>
<details>
<summary>
2020-05-05: Let's talk about Venezuela's bizarre events... (<a href="https://youtube.com/watch?v=DYk2ZjO4ewo">watch</a> || <a href="/videos/2020/05/05/Lets_talk_about_Venezuela_s_bizarre_events">transcript &amp; editable summary</a>)

Beau analyzes a bizarre spy novel-like situation in Florida and Venezuela, suggesting that the US government likely wasn't directly involved, and advocates for a hands-off approach toward Venezuela.

</summary>

"This is not the type of thing that normally happens within this industry."
"I think the Venezuelan people can decide for themselves what they want."
"It's just a thought. Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

A spy novel-like situation unfolded in Florida and Venezuela.
A crew from Florida, SilverCorp USA, contracted with Venezuelan opposition groups.
The goal was to politically realign Venezuela by overthrowing Maduro.
Some of SilverCorp's people were captured by Venezuelan authorities.
Beau believes the US government was not directly involved.
SilverCorp USA received only $50,000 out of a $212 million contract.
Photos released by Venezuelan authorities suggest SilverCorp was underfunded.
The US government usually provides ample funding to contractors for operations.
Companies used for secretive operations by the US government are discreet and lack overt connections to the government.
Goudreau, the head of SilverCorp, revealed detailed information to the press, which is unusual.
The plan was to secure Caracas and mount an insurgency against the Venezuelan government.
Beau suggests that the situation may have been a private affair gone wrong.
There might have been ideological motivations behind the operation due to the minimal pay.
Beau recommends the US should maintain a hands-off approach with Venezuela to avoid further complications.
Venezuelan intelligence seemed to have significant infiltration within dissident movements.
Beau urges for a hands-off approach, allowing the Venezuelan people to determine their future.
The presence of US citizens in Venezuelan custody could escalate tensions.

Actions:

for concerned citizens,
Monitor the situation and advocate for a peaceful resolution (implied)
Support diplomatic efforts to de-escalate tensions between the US and Venezuela (implied)
Stay informed about developments in Venezuela to better understand the situation (implied)
</details>
<details>
<summary>
2020-05-04: Let's talk about what Trump can learn from some old interviews.... (<a href="https://youtube.com/watch?v=I2Kyo8QpxJ8">watch</a> || <a href="/videos/2020/05/04/Lets_talk_about_what_Trump_can_learn_from_some_old_interviews">transcript &amp; editable summary</a>)

Beau stresses the importance of under-promising, over-delivering, and honest leadership in inspiring faith during crises, advocating for collective action in the absence of genuine leadership.

</summary>

"One of the basic rules of crisis mitigation is to under promise and over deliver."
"The U.S. needs real leadership. It is probably not going to be in the Oval Office for a very long time."
"What gets people through tough situations is leadership."
"The American people would rather hear, 'I don't know,' than a lie."
"It's up to all of the people who make up this country to lead it."

### AI summary (High error rate! Edit errors on video page)

Recalls witnessing a series of interviews where a recruiter struggled to choose between two candidates for a job.
The recruiter was looking for someone who was good at finding lost things.
Both candidates had the required experience and good references but lacked direct client interaction.
The recruiter's concern was that neither candidate had been held accountable before.
One candidate confidently promised to do everything possible, while the other couldn't provide a clear timeline.
The candidate who vowed to do everything was hired, showcasing the importance of under-promising and over-delivering in crisis situations.
Beau explains how overpromising and underdelivering erodes trust during crises.
He criticizes leaders who prioritize branding over inspiring faith and comfort in the people they serve.
Beau points out the importance of leaders admitting when they don't know something instead of spreading misinformation.
He stresses that true leadership involves honesty, accountability, and inspiring belief in the people.
Beau concludes by urging individuals to step up and lead in the absence of genuine leadership in traditional positions of power.
He calls for collective action and responsible leadership from the populace in navigating crises.

Actions:

for americans, community members,
Lead with honesty and accountability in your interactions and decision-making (exemplified).
Take responsibility for your actions and admit when you don't know something (exemplified).
Inspire faith and trust within your community through transparent communication (exemplified).
Step up as a leader in your community to navigate crises effectively (exemplified).
</details>
<details>
<summary>
2020-05-04: Let's talk about passion, rhetoric, and public assemblies... (<a href="https://youtube.com/watch?v=lpaj7S-Udm0">watch</a> || <a href="/videos/2020/05/04/Lets_talk_about_passion_rhetoric_and_public_assemblies">transcript &amp; editable summary</a>)

Beau urges to tone down passionate rhetoric in the US, warning against advocating armed conflict due to its disproportionate harm on innocents and stressing that good ideas do not require force.

</summary>

"Good ideas generally do not require force."
"Because if you use that rhetoric long enough, eventually it's going to happen."
"There are very, very few situations that can be improved by going loud."
"There are very, very few situations in which it [violence] will be useful."
"They don't need our agreement."

### AI summary (High error rate! Edit errors on video page)

Urges to tone down passion and rhetoric in the United States.
Acknowledges decent intentions of those supporting public assemblies.
Stresses the right to assemble without needing justification.
Raises concerns about economic devastation, shortages, sheltering, and authoritarian measures.
Criticizes the rhetoric advocating for armed conflict.
Warns of the consequences of escalating rhetoric to armed conflict.
Points out the lack of understanding among those advocating for armed conflict.
Emphasizes that advocating for armed conflict will disproportionately harm innocents.
States that domestic conflict is the worst thing that can happen to a country.
Urges for the elimination of rhetoric promoting armed conflict.
Encourages passion in expressing views but warns against advocating for violence.
Asserts that good ideas do not require force.

Actions:

for united states citizens,
Advocate for peaceful and respectful discourse in public assemblies (implied).
</details>
<details>
<summary>
2020-05-03: Let's talk about teens and moral injuries again.... (<a href="https://youtube.com/watch?v=uyE8dby4SHk">watch</a> || <a href="/videos/2020/05/03/Lets_talk_about_teens_and_moral_injuries_again">transcript &amp; editable summary</a>)

Teens facing moral injury from systemic failures urged to take daily action for positive change and overcome demoralization.

</summary>

"You want to change the world. You want to overcome the moral injury. You want to change the system that let you down. You can. Just got to get in the fight."
"It's always the younger generation that actually changes the world."
"You're more capable of changing the world than people my age."

### AI summary (High error rate! Edit errors on video page)

Teens are experiencing moral injury due to government response to the pandemic, feeling wronged and demoralized.
Moral injury involves perpetrating, failing to prevent, or witnessing a gross transgression of morals.
Teens face a systemic failure, not a singular act, realizing the facade of society crumbling.
Suffering a moral injury leads to demoralization and self-handicapping behavior.
Belief in the just world hypothesis and self-esteem are helpful in coping with moral injury.
Teens can generate self-esteem by realizing they can be the change in a flawed system.
Younger people are the driving force for change and can make a difference through consistent actions.
Involvement in fighting against systemic failures requires a long-term commitment and patience.
Government is critiqued for downplaying threats and putting families at risk for political gain.
Younger generations are encouraged to challenge the status quo and work towards a better future.
Beau urges teens to take action, do something daily for 28 days, and be the catalyst for positive change.

Actions:

for teens, activists,
Get involved in the fight against systemic failures by doing one thing each day for 28 days (exemplified).
Challenge the status quo through consistent actions and advocacy (implied).
</details>
<details>
<summary>
2020-05-02: Let's talk about a class and a final exam.... (<a href="https://youtube.com/watch?v=I-UUJy77Fo8">watch</a> || <a href="/videos/2020/05/02/Lets_talk_about_a_class_and_a_final_exam">transcript &amp; editable summary</a>)

The US faces a final exam in self-governance amid debates on lockdown effectiveness, urging individuals to lead themselves until a treatment or vaccine is available.

</summary>

"There is no return to normal until we have an effective, reliable, available treatment or vaccine."
"You and me, we have to lead ourselves because we don't have leadership."
"The American people can't unite behind that because they're too busy playing elephant and donkey."

### AI summary (High error rate! Edit errors on video page)

The United States is at a final exam in self-governance, with a big debate on the effectiveness of lockdowns.
Good ideas should not require force, but lockdowns and reopening pose a catch-22 situation.
The government reopening doesn't mean rushing back to normal; social distancing and limiting exposure are still vital.
There's no return to normal until an effective treatment or vaccine is available.
Individuals need to lead themselves as the government lacks effective empowerment.
Many locations reopening do not meet safety guidelines, and authorities prioritize reelection impact over public welfare.
An educated populace with critical thinking skills is necessary to hold representatives accountable.
People must unite and look out for each other, especially the working class.
Leadership is lacking, and individuals must take charge to ensure their safety and well-being.

Actions:

for us citizens,
Social distance and limit exposure to slow the spread (implied)
Develop critical thinking skills and hold representatives accountable (implied)
Unite and look out for each other, especially the working class (implied)
</details>
