---
title: Let's talk about success and leaving your mark....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yc_rShm376o) |
| Published | 2020/05/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau contrasts two historical figures who aimed to leave their mark on the world but had different approaches and outcomes.
- One figure, from a prominent political family, faced setbacks, including being captured by pirates, and worked to prosecute corrupt government officials.
- Despite facing challenges and setbacks, the figure did not inspire fear in his captors and ultimately accomplished significant achievements.
- The other figure was impulsive, setting fire to the Temple of Artemis to make an immediate impact.
- Beau points out the importance of understanding the long, challenging road to making a lasting change in the world.
- He warns that activism is exhausting and requires a long-term commitment to see real progress.
- Beau stresses the need for activists to realize that change won't happen overnight but through persistent daily efforts.
- Making little improvements each day is emphasized as a way to contribute to larger change over time.

### Quotes

- "You're going to have to be in it for a while."
- "There'll be little changes every day and you can make little improvements every day."
- "World's not going to change tomorrow, not completely."
- "To get to where you're going, where you want to go, it's a long road."
- "You've chosen a pretty big battleground."

### Oneliner

Beau contrasts two historical figures' approaches to leaving their mark on the world and stresses the need for a long-term commitment to create lasting change.

### Audience

Activists, changemakers

### On-the-ground actions from transcript

- Build and improve gradually (exemplified)
- Make little improvements daily (exemplified)
- Understand the long-term commitment required for activism (exemplified)

### Whats missing in summary

The full transcript delves into historical lessons about success and making an impact by contrasting two figures from the past. Watching the full video can provide a deeper insight into the stories shared and the importance of perseverance in enacting change.

### Tags

#Activism #Change #LongTermCommitment #History #Impact


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about two people from a long, long, long, long time ago and
what they can teach us about success, leaving your mark on the world, changing the world
today.
Because they both wanted to do that.
They both wanted to leave their mark, but they went about it in very, very, very different
ways.
And they had very different outcomes.
We're going to do this because tonight, earlier tonight, I was chatting with some people that
had just become active.
And it's always great.
It's always very invigorating, a lot of renewal.
Because they're ready.
They're ready to leave their mark, to change the world.
They're ready to do it tonight.
That immediate need.
They are ready to set the world on fire.
And it is fantastic.
That idealism is great.
May not always be the best strategy though.
So the first guy, he was from a pretty prominent political family.
However they had fallen out of favor.
So he had to go into hiding.
And along the way, he was a soldier, a religious leader.
Eventually became a lawyer, kind of how he got back on his feet.
And when he was a lawyer, he was known for going after corrupt government officials and
prosecuting them.
And he loved it, which is funny in its own way.
He had a lot of setbacks along the way.
He saddled with debt, had a lot of problems with his love life.
He was once caught by pirates.
And you know, for the pirates, it's always the same.
It's always the same.
I don't have any money, you know, just let me go.
You'll never see me again.
Not this guy.
Not this guy.
Captain's about to send out the messenger to demand the price to return him.
He's like, that's not enough.
You need to ask at least twice that much from me.
Just understand when this is all over, I'm going to come back and hunt you down.
Okay.
Captain sends out the messenger and does ask for twice as much.
This guy, meanwhile, is not inspiring fear in his captors.
He's laughing, joking, working on speeches, writing poetry, not really an intimidating
figure.
Eventually the money shows up and they let him go.
And then he comes back and hunts them all down.
And he goes on from there and accomplishes a lot of stuff.
But he had a lot of setbacks and it was a very long road.
The other guy, well, he was ready.
He was ready to set the world on fire, to leave his mark.
So he did.
He torched the Temple of Artemis.
One of the many wonders of the ancient world destroyed it.
One of those people, you know.
You know their name.
You may not know those stories because the person led such a wild and interesting life
that those are the stories that get cut when people are making movies about him.
It was a long road.
The other thing is, the first guy, he built something.
Now granted he did it on the back of a lot of destruction, but he built and he improved.
The other guy, that second guy, he wanted that immediate action, that immediate change
to leave his mark and to be remembered and he wanted it now.
One person, that first person, is remembered today because he went on to have a fling with
Cleopatra.
He ran Rome.
First person Julius Caesar.
The other person?
Doesn't really matter, does it?
If you're going to change the world, you've chosen a pretty big battleground.
It's going to be a campaign.
It's going to be a long fight and you're going to have to be in it for a while.
You got to get your mind around that now.
Got to get your mind around that now because otherwise that fire that you have, it'll burn
you up, it'll consume you.
There's a reason that it's a running joke among long-time activists that activists have
a three-year shelf life because it's exhausting.
It is exhausting unless you get your mind around it in the beginning, that it's going
to be a long fight.
World's not going to change tomorrow, not completely.
There'll be little changes every day and you can make little improvements every day.
But to get to where you're going, where you want to go, it's a long road.
It is a long road.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}