---
title: Let's talk about following the money and taking Sen McConnell's advice....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dtIOl_eFXaI) |
| Published | 2020/05/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau suggests taking Mitch McConnell's advice on the stimulus package, despite their usual differences.
- McConnell has expressed no urgency in passing the next stimulus package, wanting to see the effects of previous spending first.
- Beau analyzes McConnell's campaign contributions from the last five years, focusing on companies contributing significant amounts annually.
- McConnell has received substantial donations from big investment groups, Altria Group (Philip Morris), and the health care industry.
- Campaign contributions seem to influence McConnell's stance on health care accessibility and profitability.
- Beau points out donations from Geo Group, a private prison and real estate investment company.
- Companies like FedEx and UPS also contribute to McConnell, potentially influencing his policies regarding competitors like the United States Postal Service.
- Beau urges viewers to research their representatives' campaign contributions on OpenSecrets.org to understand the influence of money in politics.
- Beau criticizes the significant role of money in shaping policies, suggesting that the common people lack effective lobbyists in the political system.
- In light of economic experts predicting ongoing struggles, Beau calls for urgency in providing stimulus to those in need.

### Quotes

- "Maybe you should display a little bit of urgency, given the fact that all of the experts are saying that we're going to be going through this ups and downs for a while."
- "The Senate and the House, they could give a master class on corruption to all of these smaller governments that we tend to criticize."
- "I strongly suggest you do this. Go see who has contributed to your candidate, to your representative."

### Oneliner

Beau suggests analyzing Mitch McConnell's campaign contributions to understand his stance on key issues like the stimulus package and health care accessibility.

### Audience

Voters, activists, constituents

### On-the-ground actions from transcript

- Research and analyze campaign contributions of your representatives on OpenSecrets.org (suggested)
- Advocate for urgency in providing stimulus to those in need, contacting elected officials if necessary (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how campaign contributions may influence political decisions, urging viewers to scrutinize the influence of money in politics for a deeper understanding.

### Tags

#MitchMcConnell #StimulusPackage #CampaignContributions #HealthCare #PoliticalInfluence


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about taking Mitch McConnell's advice.
Because he said something, it's a good idea.
It is.
I know, it seems odd.
But he did, and I think we should do it, and we're going to do it in here.
We're going to take his advice right here.
See it's hard to get a read on McConnell because half the time he's acting like Trump's intern
and just doing whatever he says, and then sometimes he breaks with him on weird issues.
It's hard to figure it out.
But he said he's not in a hurry to do the next stimulus package.
Now what he actually said, so I can be fair because I'm going to use him as an example
of something.
I don't think we have yet felt the urgency of acting immediately.
That time could develop, but I don't think it has yet.
And I can get that from his point of view.
You know, I mean all his millionaire and billionaire buddies, they've already been bailed out.
It's not like we need to worry about the little guy who is currently creeping towards one
in five being unemployed.
No urgency though.
Get around to it whenever you want to.
That's not his logic.
His logic is that we need to see what effect the money we've spent already, see the effects
of that money.
We need to see what it has caused.
Now that is good advice.
That is good advice.
Let's see what the money spent has done.
I like that.
So I went over to OpenSecrets.org.
If you're not familiar with it, it is a website that allows you to determine who owns your
politician.
Who contributes to the re-election campaign of your elected representatives who are always
acting in good faith.
And I went to McConnell's.
It's interesting.
Now I did want to be fair.
I wanted to be fair to him.
So I limited it to the last five years.
And I'm only going to talk about companies that have given him on average $10,000 or
more per year for five years, if you break it down.
No company that I'm going to mention has given him less than $60,000 over those five years.
And understand when I say company, you're going to need to look at the methodology that
OpenSecrets uses.
They track the employers of individual donors.
So that way, if Company X decides to donate a whole bunch of money to a senator, they
can't hide it by having their employees donate the money.
Okay, so when you're looking at his list, yeah, I mean you've got the normal offenders,
the people that normally contribute to senators.
You've got the big investment groups who already got their bailout.
You got NorPAC, VoteSane, you know, the normal ones.
You also have Altria Group.
Altria Group.
Nobody knows what that is, right?
That's Philip Morris.
That's Big Tobacco.
Maybe that explains why he's not in a hurry to bail out the little guy, like the farmers
in his state, who were already having trouble and now their land is going to be available
real cheap.
If only there were some big company that would probably buy it up.
He gets a lot of money from health care.
You know, one of the big standbys when you start talking about campaign contributions
is that the companies donate to candidates based on their policies and their ideological
motivations and they support them based on that.
Because it seems real likely that the health care industry and Big Tobacco are going to
support the same candidate.
Maybe it's not about that.
Maybe it's about profit motivation and maybe this is why McConnell is so intractable when
it comes to making sure that everybody gets health care.
Making it more accessible.
Because if it's accessible, well then it's got to be cheaper.
It's not as profitable now, is it?
Lot of money from health care.
A lot.
Like I mean tons.
Let's see what else we have here.
We have Geo Group.
A real estate investment company.
Private prisons is what it really is.
I think they do mental health care facilities too in an attempt to be fair.
Man, that fits right in line with McConnell.
Doesn't it?
That's strange.
I wonder if he always felt that way or if that was a shift, a change.
Somebody should look into that.
Here's an interesting one.
Both FedEx and UPS give him money.
Now that's weird.
It's not like one is going to try to obtain an edge over the other.
Not like they're trying to take out their competitor.
Unless of course that competitor is the United States Post Office.
Which McConnell has recently just abandoned.
It's almost like every single policy decision the man makes can be tracked back to this
list.
There it is.
I strongly suggest you do this.
I've talked about this site on this channel before.
Go see who has contributed to your candidate, to your representative.
And see how much you think your vote actually matters when this much money is at play.
Because see, us common folk, us peons, those at the bottom, well we really, we don't have
lobbyists.
You know who our lobbyists are supposed to be?
The people taking this money.
In this system, those representatives, those senators, they're supposed to lobby on our
behalf.
But when you're talking about that kind of money, and this kind of money, that's not
just lunch money.
This is motive with a universal adapter.
When you add this up, you are talking about millions.
We talk about corruptions in other countries.
The Senate and the House, they could give a master class on corruption to all of these
smaller governments that we tend to criticize.
Because they can do it out in the open.
Because they have the people trained to think it's normal to not ask questions, to not see
what effect the money has.
You've got a great idea, Senator.
I would suggest that getting stimulus money in the hands of the little guy, those people
in your state especially, Senator, is probably pretty important.
It is probably pretty important.
Maybe you should display a little bit of urgency, given the fact that all of the experts are
saying that we're going to be going through this ups and downs for a while.
Maybe get off your rear.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}