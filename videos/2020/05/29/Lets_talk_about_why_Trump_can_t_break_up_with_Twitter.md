---
title: Let's talk about why Trump can't break up with Twitter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=smw0FS6S97s) |
| Published | 2020/05/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introduces a lighthearted topic before diving into heavier subjects of the day.
- Explains the ongoing battle between the president and Twitter.
- Details how Twitter fact-checked the president's misleading tweet and the subsequent executive order.
- Mentions Twitter censoring the president after his reaction.
- Analyzes why the president relies on Twitter to keep his base energized and why he can't leave the platform.
- Criticizes the president's primary concern of using Twitter to instill fear and hate rather than leading effectively.

### Quotes
- "It's sad that with everything going on right now, the president's primary concern is maintaining a tool to tell people what to be afraid of and who to hate."
- "He can't lose it. He can't leave the platform."
- "Because he knows that if he can't do that, if he can't use Twitter, his only options are to speak publicly or actually lead."

### Oneliner
The president's reliance on Twitter to instill fear and hate reveals his inability to lead effectively, jeopardizing his re-election chances.

### Audience
Voters, concerned citizens

### On-the-ground actions from transcript
- Monitor the president's actions on social media and hold him accountable (implied)
- Stay informed about political tactics and their implications (implied)

### Whats missing in summary
Insight into the broader impact of the president's reliance on social media for political messaging.

### Tags
#President #Twitter #PoliticalTactics #Leadership #FearMongering


## Transcript
Well howdy there internet people, it's Beau again.
So today let's start off with something a little bit more lighthearted.
We're moving into the weekend.
There's a lot of heavy stuff going on and we will talk about it all later, but let's
get your day started off right.
Let's talk about why the president can't break up with Twitter.
Even with all of the heavy stuff that is currently occurring in the United States and all over
the world, the president has decided that his primary objective is to battle a blue
bird on the internet and he's losing gloriously to Twitter.
If you're not aware of the saga that is occurring, this little back and forth between the president
and Twitter, I will give you a quick recap.
Not too long ago, the president did what the president often does and tweeted something
that was, well, let's just call it misleading.
Twitter fact checked him.
Put a little button below it saying more information click here.
Didn't censor him, which was the president's claim.
Twitter censored me.
That didn't happen.
The tweet was always visible.
Just had additional information added to it.
He went off on a tirade over this and in the process proved that he did not understand
the first amendment, didn't really even grasp the concepts in it, and then he decided he
was going to go the executive order route.
The executive order primarily targeted section 230 of the Communications Decency Act written
back in 1996 by Chris Cox and Ron Wyden, a bipartisan thing.
Ron Wyden has referred to the president's executive order as, quote, plainly illegal.
And he's right.
But it was never meant to be enforced anyway.
It has no teeth.
The real point of that executive order was to send a message to Twitter.
I'm the president of the United States.
You can't censor me, even though they never did.
You can't do this to me.
I'm the boss.
You can't fact check me.
You can't correct me.
Me.
Don't do this to me.
I have this power.
Less than 24 hours later, Twitter actually censored him.
Now whether or not this was Twitter's administration or somebody exploiting a mass report feature,
we don't know yet.
But the president tweeted something that was admittedly disturbing and a horrible strategy,
horrible tactics in regards to his plan to restore order.
And we'll probably get to this in another video.
But Twitter covered it up.
Covered it up.
In place of that tweet, it says, this tweet violated Twitter's rules about glorifying
violence.
Twitter has determined that it may be in the public's interest for the tweet to remain
accessible.
Has a button that you can click if you want to view the stuff that he spews.
I would imagine that there is a full blown meltdown going on in the White House right
now.
Twitter called us bluff.
Because Twitter knows Trump can't leave.
He's the president of the United States.
Why would he need a platform like this?
Why would he need Twitter?
He doesn't, right?
Or does he?
Think about it.
Twitter's platform is perfect for President Trump.
If you want, you can work really hard in the limited space it allows to insert some nuance,
some substance, some subtlety, some ideas.
But you're not required to.
You don't have to.
And his voters, they don't care about substance.
If they did, they wouldn't have voted for him.
They care about little witty bits of information.
Digital sound bites that tell them who to be afraid of, who to be mad at, who's out
to get them.
His voters are ruled by anger and fear.
And Twitter allows him to exploit that.
He can't lose it.
He can't leave the platform.
He'll lose.
He'll lose the election.
He will lose the election if he leaves Twitter.
Because right now it's the only thing keeping his base energized.
Those people who only absorb information cleared by the president.
Those who are true party line thinkers.
Those who don't commit thought crime.
They need direct contact with the president.
They need those little baby bits of instruction.
Their two minutes of hate.
They need it.
And without it, well they might go to a different source and get factual information.
And that would be devastating to the president's re-election chances.
So he won't leave.
But there will be a pretty good meltdown today.
I imagine by the time you all watch this, it's probably already happened.
It's sad that with everything going on right now, the president's primary concern is maintaining
a tool to tell people what to be afraid of and who to hate.
It's really what it's about.
Because he knows that if he can't do that, if he can't use Twitter, his only options
are to speak publicly or actually lead.
Two things the president has proven he is incapable of.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}