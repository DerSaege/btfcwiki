---
title: Let's talk about the charging decision....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MSOffl6tU04) |
| Published | 2020/05/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update and a follow-up after a wild week following his cautious initial analysis.
- Expresses disappointment in the delayed arrest and the choice of charges in the case.
- Questions the lack of evidence regarding Mr. Floyd's intent and criticizes the charges brought against the officer.
- Believes that the current charges are not sufficient and calls for fighting the necessary fights.
- Raises concerns about the state of the country, including the economy, COVID-19 deaths, media suppression, and the justice system.
- Expresses hope for a better America amidst the chaos.

### Quotes

- "Why'd you let the city burn? Could have charged that on day one."
- "You fight the fights that need fighting."
- "We have an economy in shambles. We have a hundred thousand gone due to inaction."
- "We have a broken justice system."
- "I really hope that America is great now. Because I don't think I can stand for it to get much greater."

### Oneliner

Beau provides a critical update on the delayed arrest, questionable charges, and the need for fighting necessary battles amidst a chaotic America.

### Audience

Activists, Justice Advocates

### On-the-ground actions from transcript

- Fight for the necessary charges and justice (implied)
- Advocate for transparency and accountability in law enforcement (implied)
- Stand up against injustice and inequality (implied)

### Whats missing in summary

The emotional impact and urgency conveyed by Beau's words.

### Tags

#Justice #PoliceReform #Activism #SystemicChange #SocialJustice


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to go over the new stuff, do a little update, a follow up.
Because it has been a wild few days, a wild week really.
I made that initial video, my analysis.
And uh... I was very cautious,
very restrained,
very conservative with what I said.
Because I assumed that the people that would be in the decision making process
up there
would see the same thing I saw.
Because to me it was so evident.
I even convinced myself,
because I am incredibly naive,
that that's what the hold up was.
That's why they didn't move right away, you know.
That's why they didn't go and pick them up right then, because they were
getting their ducks in a row
for real charges.
And uh...
You know, in that video
I listed four things. I said that I thought it was worse
than what everybody else was saying.
And I listed four things
that I thought
would help establish that it was worse.
Three of them
have now been reported to be true.
And as it drug on,
and nothing happened,
I'm sitting there thinking,
I understand you want to have this stuff together, but you gotta hurry.
You understand stuff's on fire, right?
And uh...
Then this morning,
I saw it, you know.
He was arrested.
I didn't even have to look at the charge,
because I knew what the charge was, you know.
Good. And then I saw the charge.
Man in third degree.
Why'd you let the city burn?
Could have charged that on day one.
That seems pretty clear from the video,
from the initial video.
Seems pretty clear.
That could have been charged on day one.
See, intent,
that was the question.
Intent.
I'd like to take this moment to point out
that so far,
at this moment,
they have released exactly zero evidence
regarding Mr. Floyd's intent
to pass a vote.
None.
And that was one of them.
Not the right guy.
You know, the other
being uh...
something in the officer's history,
like a giant history of complaints,
or possible interaction between the two,
like them working at the same club.
The routes to prove intent, they exist.
They're there.
Prosecution can find them. They can do it.
But instead,
man in murder three.
And I know
there's some people saying that,
well, that's just easier.
It's going to be easier for them to convict on that. It's going to be easier for them to get that done.
And he's going to get twenty-five years. No, he will not.
No, he will not.
That's the maximum.
That is the maximum.
You do not get the maximum for your first offense
as a cop.
He's a public servant.
He just made a mistake. It was an accident.
That's what's going to be said at his sentencing.
And no doubt they will find some way to disallow
his history.
I do not believe these are the right charges.
I do not believe these are the right charges.
I understand you fight the fights you can win, but sometimes
you have to fight the fights that need fighting.
And right now the country needs this.
The country needs this.
We have an economy
in shambles.
We have a hundred thousand gone
due to inaction.
We have
reporters getting arrested on the street while the president rambles on
about his free speech on Twitter
and him egging it on up there.
We have a city in flames.
We have a broken justice system.
I really hope that America is great now.
Because I don't think I can stand for it to get much greater.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}