---
title: Let's talk about numbers, the future, and the carrot and the stick....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PtAcxPJrk1w) |
| Published | 2020/05/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Unemployment is at almost 15%, a significant number indicating the current economic crisis.
- Despite jobs reopening, not all employees are returning due to health concerns, leading to conflicts between employers and workers.
- Some employers are using government tools to force employees back to work, even if it jeopardizes their health.
- States like Ohio, Alabama, and others are setting up systems for employers to report employees who refuse to return to work.
- People's reluctance to return to work may also lead to decreased consumer spending on non-essentials.
- Small businesses that fought to reopen might face permanent closure due to reduced revenue and higher expenses.
- Beau criticizes employers who betray their employees' trust by reporting them for choosing safety over work.
- He suggests that consumers should be informed about businesses that mistreat their employees during these times.
- The current economic situation is predicted to worsen as people struggle financially and refuse to risk their lives for profit.
- Beau advocates for individuals to prioritize their safety, seek media attention if needed, and take legal action against employers who compromise their well-being.

### Quotes

- "You have to go out, wear a mask."
- "It’s always better to be a good person."
- "Enjoy the start of the depression."

### Oneliner

Unemployment rises, conflicts between employers and workers escalate, and consumer spending decreases as safety concerns prevail during the economic crisis.

### Audience

Employees, consumers, activists.

### On-the-ground actions from transcript

- Contact the media if your employer jeopardizes your safety (suggested)
- Wear a mask when going out (implied)
- Inform others about businesses mistreating employees (implied)

### Whats missing in summary

The detailed examples and insights shared by Beau provide a deeper understanding of the economic challenges faced by individuals and businesses during the current crisis.

### Tags

#Unemployment #EconomicCrisis #WorkersRights #SmallBusinesses #SafetyConcerns


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about numbers.
We're going to talk about numbers that we knew were going to be like this.
We're going to talk about things that we knew were going to happen that are now starting
to happen.
We're going to talk about what's most likely to happen next.
And a lot of this isn't good news to be honest.
But there is a ray of hope in here.
There is some good news in here, and that good news is that the average American is
smarter than most people think.
Okay so, unemployment, 15%.
Almost 15%.
I'm not an economist, but I would suggest that's probably pretty significant.
You know, I'm no expert, but I would imagine that that's probably something worth noting.
Jobs are starting to reopen.
And because of that, everybody should be returning back to work, right?
But that's not actually happening.
It's not going the way those who cried out for less government interference imagined
it would, because Americans are smarter than they gave them credit for.
They're leading themselves, because these reopenings are against the better advice.
So what happens if you're one of these employers who whined and cried about having to be closed,
and now you've reopened and not all your employees are coming in?
See that's upsetting because now you didn't want to stay closed, but you have an obligation
to stay open because a lot of retail space, commercial leases, they require you to be
open a certain number of hours.
Have the lights on, have people there.
But the employees, not all the employees want to come in.
So what do you do if you're in that situation?
If you are a want-to-be feudal lord who owns your serfs and feel that they owe their health
to you and that you can dispense with it as you please?
Well your only option is to contact your lord, the government.
And the government has one tool in its toolbox, violence.
The state of Ohio has set up a website where employers can rat out their employees who
won't return to a job they were let go from out of fear of what's going around.
If they don't come back, the employers can rat them out to the government.
Then the government will say, oh, you're committing fraud, unemployment fraud.
You have to stop your unemployment.
And if you keep getting it, well here comes that government guy, getting that cage, son.
The common folk are not doing as they were told by their bettors.
So far in Ohio, more than 1,200 people have been ratted out by their employer.
I wish I could say Ohio was the only state doing this or considering doing this.
However, Alabama, Oklahoma, Iowa, Pennsylvania, Texas, Tennessee, and South Carolina either
already have stuff running like this or have indicated that they're interested or will
have stuff running like this.
Iowa already has a tip line set up.
So now let's take this to the next level.
If people aren't willing to return to work yet, what do you think the odds are that they're
going to go out and shop for non-essentials?
Probably pretty slim.
Number one, they don't have as much money.
Number two, they know it's not safe.
They're not willing to take the risk.
That means that all of these businesses that wanted to reopen, well, the amount of money,
their revenue is going to be lower because there's going to be less people coming to
them.
However, their expenses are going to be higher because they've got to have the lights on.
They've got to have people there.
You don't have to be Warren Buffett to understand that a lot of small businesses who were, let's
just say, upset about being closed are going to end up going out of business permanently
because they fought to reopen.
I would also suggest that, this is just me talking, but if I found out that an employer
ratted out their employee over something like this, I would never do business with them
again.
I'm not saying one of those Republican boycotts were like, you get mad about something somebody
said who's loosely affiliated with the company, so you boycott them for two weeks and normally
it's a company that you never had anything to do with anyway.
No, I mean, I would never do business with your company again.
I'm pretty sure I'm not the only person that feels that way because if you do that, you're
a horrible person.
If you're going to treat your employee like that, I would imagine you would treat your
customers pretty poorly too.
I would hope that somebody's fighting to make the list of employers public.
I think that's something that the consumer needs to know.
But with all of this in mind, what we can loosely take from this is that as predicted,
the economy's not going to get better.
It's probably going to get worse because we reopened, because the little guy wasn't bailed
out because they don't have money and they are not going to do what their betters command
and put their lives at risk.
If they don't have money, they're probably not going to be able to pay rent and stuff
like that.
So all of those landlords who were fighting over the last month or two trying to get things
to reopen so their tenants could get paid, well, they're not going to get it anyway.
Now the banks are going to be less forgiving.
It's almost like cooperation would have worked better than trying to force your will onto
others.
It's almost like we didn't need a government order.
It's almost like we just needed to do what was smart.
And the average American is smarter than their bosses and government give them credit for.
They're not just going to do what their betters tell them.
They're not going to risk their lives so those people above them can make a few extra bucks.
At this point, the government's dug its heels in on this.
I don't know what the solution is because they're not going to want to bail out their
citizens because they need that stick.
The carrot of our current system is, hey, you're going to lead a semi-comfortable life.
You may, one in a billion chance, be one of those that make it out of your current class
and get to the next class.
But they've also got that stick to say, get back to work.
They're going to rely on that stick.
If that continues for too long and people remain smarter than they are expected to be,
it's probably going to go bad.
It's probably going to go bad anyway because this is going to spread.
The projections show that.
The projections have actually been pretty on the nose.
Seventy-five thousand at this point.
And there are still people talking about how it's overblown, how it wasn't a big deal.
That seventy-five thousand with everything shut down, imagine what it's going to be next
month.
Wash your hands, don't touch your face, stay at home if you can, if the government doesn't
put a gun to your head.
You have to go out, wear a mask.
And if your employer interferes with anything having to do with your safety, contact the
media, file a lawsuit, do whatever you need to.
Because all of these employers that wanted everybody to come back for their benefit,
they should certainly be providing the protection that those employees need.
And if they're not, the consumer needs to know that.
And I would imagine some of those places might go out of business permanently.
It's always better to be a good person.
Normally always works out better for you in the long run.
Anyway, it's just a thought.
Y'all have a good day.
Enjoy the start of the depression.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}