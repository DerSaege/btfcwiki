---
title: Let's talk about a ray of hope in Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aEjgEM7fae0) |
| Published | 2020/05/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the danger of losing focus after a ray of hope in an injustice movement.
- Mentions the case of Ahmaud Arbery in Georgia where two individuals were arrested for aggravated assault and murder.
- Emphasizes that an arrest is not a win but a ray of hope.
- Warns against people stopping their calls for justice after arrests, leading to a fall apart in the case.
- Urges the community to keep fighting for justice even after arrests, as the fight continues within the court system.
- Encourages allies to keep supporting the cause and using privilege to fight against bigotry and racism.
- Stresses the importance of not abandoning the cause when there's a ray of hope.

### Quotes

- "It's a win, right? No, it's not a win. It's a ray of hope."
- "Don't let that happen here."
- "It's not over. That fight will continue."
- "You can use that bigotry, that racism against bigots and racists."
- "Don't abandon them when they finally get a ray of hope."

### Oneliner

Beau warns against losing focus after a ray of hope in an injustice movement, urging continued support for justice even after arrests.

### Audience

Community members, allies

### On-the-ground actions from transcript

- Keep supporting the cause by continuing to fight for justice (exemplified)
- Hold onto the megaphone and amplify the voices seeking justice (exemplified)
- Use privilege to fight against bigotry and racism (exemplified)

### Whats missing in summary

The emotional impact and the urgency of maintaining support and pressure in seeking justice. 

### Tags

#Justice #CommunitySupport #Racism #Injustice #Allyship


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about when it is time not to celebrate.
A lot of times an injustice occurs and when that injustice occurs and people see it, they
rally to that cause.
Most times when that happens, people join a cause that seems hopeless and they want
to get involved because it's just so offensive to general humanity.
So they jump in.
And then something happens to provide a little ray of hope.
And a whole lot of people lose focus.
A whole lot of people at that point stop fighting.
They trust the system, the powers that be, to carry out what is expected of them.
That's the most dangerous point.
That's the most dangerous point because the movement that develops to fight that injustice
will lose esteem.
And that's when it can be overtaken.
That's when the powers that be can then ignore it because they don't have that public outcry
going anymore.
Now there was an incident in Georgia.
A man named Ahmaud Arbery.
Now I'm not going to go through the details of it.
If you don't know the case, look into it yourself.
A whole bunch of people rallied behind this because it was hopeless.
Prosecutors saw no grounds for arrest.
People drew attention to it.
And that attention forced government agencies within Georgia to take a look at it.
The Georgia Bureau of Investigation has announced that both McMichaels, both the people involved,
have been arrested.
Both of them charged with aggravated assault and murder.
It's a win, right?
No, it's not a win.
It's a ray of hope.
We've seen this happen a lot where the arrest occurs and everybody stops.
Everybody stops calling for justice.
And by everybody, I don't actually mean everybody.
We're going to get to that in a minute.
And it's at that moment that things just start to fall apart with the case.
The prosecution becomes less than honest in their pursuit of justice because they don't
have the eyes on them.
Don't let that happen here.
Don't let that happen here.
Now there's a whole bunch of people in that community.
They know this and they're going to keep fighting.
However if you look like me, this is the moment where a lot of people stop.
They get arrested, the court system will take care of it from there.
We did our part.
No, no, you didn't.
It's not over.
That fight will go on.
It will continue because the court system is the same system that said there were no
grounds.
It's not over.
That fight will continue.
And if you want to be the ally you're currently patting yourself on the back for being, you
need to keep supporting the cause.
You need to keep holding that megaphone out.
Because part of the reason that there weren't grounds to arrest in the beginning is part
of the reason people will listen to you if you look like me and won't necessarily listen
to them.
You can use that bigotry, that racism against bigots and racists.
It's kind of cool that way.
If you're willing to use the privilege that your skin tone gives you.
Don't abandon them when they finally get a ray of hope.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}