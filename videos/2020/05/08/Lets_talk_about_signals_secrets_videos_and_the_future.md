---
title: Let's talk about signals, secrets, videos, and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tZqye2q3srE) |
| Published | 2020/05/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the importance of signals, codes, videos, and theories in a quick and concise manner.
- Mentions an event down south where individuals got caught and appeared in videos.
- Talks about the shift in standards regarding making videos for propaganda purposes.
- Explains the theory that information becomes obsolete quickly in today's fast-moving battlefield.
- Notes that certain signals in videos can convey messages without being overtly noticed.
- Refers to a Wall Street Journal article that debunked conspiracy theories due to the likelihood of people talking.
- Criticizes conspiracy theories that paint the opposition as infallible and the system as unbeatable.
- Advocates for evolution over revolution to change the system by creating a redundant power structure based on cooperation.
- Emphasizes the importance of building the future rather than fighting against the past.
- Concludes by urging for a shift towards evolution instead of revolution to create the desired system.

### Quotes

- "We don't need revolution. We need evolution."
- "We can change the system at play. We can change the way the land. We can make them obsolete."
- "We don't get the system we want by fighting against the past. We get it by building the future."

### Oneliner

Beau addresses the importance of signals, codes, and videos, debunking conspiracy theories and advocating for evolution over revolution to create a better future.

### Audience

Activists, Advocates, Change-makers

### On-the-ground actions from transcript

- Build a redundant power structure based on cooperation (implied)
- Focus on evolving systems rather than resorting to revolution (implied)

### Whats missing in summary

The full transcript delves into the significance of information dissemination, debunking conspiracy theories, and promoting a shift towards evolution for societal change.

### Tags

#Signals #Codes #Videos #ConspiracyTheories #Evolution #Revolution


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about signals, codes,
videos, the future, theories, all kinds of stuff.
We'll do it real, real quick.
It's not gonna take long.
We're gonna do it because it's important.
The end point here is actually pretty important.
And something happened that can shed a lot of light on it.
So those guys that went down south, doing whatever that was down there, they got caught,
right?
And now they've shown up in videos.
And there's already people trashing them, saying you're not supposed to do that, you're
not supposed to provide the opposition with propaganda.
These guys, I can't believe they couldn't hold out.
Yeah that's not the standard anymore.
That hadn't been the standard for a long time.
Making videos like that is actually kind of condoned now.
The theory is that in today's world, the modern battlefield moves so quickly that the information
you have after about a day, as far as the lay of the land, it's obsolete.
Because it's changed.
The systems that exist out there on that battlefield, well they're different.
So what you can provide, with the exception of like means and methods, it's not of any
value.
So it's okay to do it.
In fact, not just is it kind of condoned, higher tier troops are actually trained how
to deliver messages, like I'm under duress, or I'm lying, in those videos, through the
use of little signals.
I've used both of those in this video so far.
It can be done without really being noticed.
And I know what you're saying, Beau, why would you talk about that right now?
There are guys down there, like, you know, making these videos.
Shouldn't talk about that right now, that's supposed to be a secret.
I would have not said anything, however, it was in the Wall Street Journal.
This is why I don't believe in conspiracy theories.
Those that require hundreds, if not thousands of people cooperating.
Somebody's always going to talk.
Somebody's always going to let the cat out of the bag.
Those theories that are just so expansive that they involve hundreds, if not thousands
of people, they generally have no evidence.
And here's the thing, the evidence would exist.
Somebody would talk.
Somebody always talks.
So I don't like them for that reason, because I've engaged in project management.
Nothing goes that smoothly, ever.
But aside from that, I don't like them because they paint the opposition as infallible.
They paint the system as the product of this giant theory.
And that makes the system unbeatable.
Because it's got the money, the power, control of the military, the state, everything.
So you can't do anything.
Sounds like a good reason to just get up and give up and accept the status quo to me.
I know what you're saying.
You're going to say, well, you know, those people, they have names and addresses.
Yeah, yeah, it sounds good and everything.
The reality is it's not what we need.
It's not what we need.
We don't need revolution.
We need evolution.
Just like on the battlefield, apt analogy, we can change the system at play.
We can change the way the land.
We can make them obsolete.
Make it so that it doesn't matter anymore because we create a redundant system, a redundant
power structure that's based on us and cooperation rather than competition and subjugation.
Makes more sense to me.
We don't need revolution.
We need to evolve.
We don't get the system we want by fighting against the past.
We get it by building the future.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}