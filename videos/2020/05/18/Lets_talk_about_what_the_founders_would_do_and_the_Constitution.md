---
title: Let's talk about what the founders would do and the Constitution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZaO1R5uaZIU) |
| Published | 2020/05/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the conflict around statements claiming that stay-at-home orders are unconstitutional and causing economic devastation.
- Emphasizing the importance of understanding the entire Constitution, especially the 10th amendment which grants power to the states.
- Clarifying that in situations like the current pandemic, restrictions on movement and assembly can be constitutional.
- Providing historical context like the yellow fever outbreak in Philadelphia during George Washington's presidency to debunk the claim that the founders wouldn't have tolerated such measures.
- Stressing the need for responsible exercise of rights and the importance of being well-informed to avoid misinformation.

### Quotes

- "If you cry wolf about tyranny, when tyranny arrives, nobody's going to believe you."
- "Rights come with responsibilities, and one of them is to become educated."

### Oneliner

Exploring the constitutionality of stay-at-home orders, Beau debunks claims that such measures are unconstitutional by providing historical context and stressing the importance of being well-informed.

### Audience

Citizens, Constitution Advocates

### On-the-ground actions from transcript

- Educate yourself on the Constitution and its amendments (suggested)
- Advocate for responsible exercise of rights (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the constitutionality of current measures, historical examples, and the importance of being informed.

### Tags

#Constitution #StayAtHome #Responsibility #Education #History


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about what the founders would do, a little bit about the
constitution, and we're probably going to hear some rain hitting the roof.
There's a lot of statements being made right now, and they all carry the same general idea.
So I want to talk about these statements that are being made, and we're going to talk about
the general idea.
I don't want to strawman anybody's opinion, so if I've got something wrong, correct me
in the comments section.
But I'm going to make an earnest attempt to explain where these statements are coming
from, and then we're going to talk about it.
The first part of them is always, I swore an oath to support and defend the constitution
of the United States, and that oath doesn't have an expiration date.
Cool, I'm with you.
Seriously.
I like dedication.
Even if it's to a cause I don't necessarily support.
Even if it's to a cause I oppose.
Nobody wants to win against somebody who's half committed.
So I'm with you there.
The next part is that the stay at home stuff, well that's unconstitutional.
It's causing a lot of economic devastation.
The science is shaky.
The experts don't really know what they're talking about.
And then all of this is followed up with, the founders never would have tolerated it.
That's the general statement that's coming out.
Okay.
I've said this before on this channel and anytime I say it people get mad, but it constantly
gets proven to be a pretty accurate statement.
When people say I support and defend the constitution, normally what they mean is I support and defend
the Bill of Rights.
And generally, most people only know the first two amendments.
There's another group that may know the fourth and fifth as well, but there are very few
that actually know the first 10 amendments to the constitution.
If you want to support and defend the constitution, you have to read it, all of it.
Oddly enough, what matters a whole lot in this case is the 10th amendment.
It is in the Bill of Rights.
And what it says, roughly, is that any power that isn't specifically given to the federal
government in the constitution is a power that is the state's.
They have that power.
If you read the constitution, what you will find out is that, by and large, the states
have authority here, not the feds.
You know, they've got stuff that happens interstate and stuff that occurs with other nations.
But what occurs inside of a state?
Not so much.
They can advise, assist, facilitate, and they should, but they can't really order anything.
That's why if you go back through these videos, you don't see me calling for Donald Trump
to order something.
You don't see that.
You see me asking for leadership, because that's what he should be doing.
It's the states that have authority here.
The next part of this is that, well, they can't override my first amendment right to
peaceably assemble, my freedom of movement.
They can't just take that away.
Yes they can.
In a situation like this, yes they can.
The Supreme Court, using the mechanisms in the constitution, which you support and defend,
said they can.
There are tons of decisions on this.
Just because something is inconvenient does not mean that it is unconstitutional.
In this situation, they do have this authority.
It's been ruled on.
Now, in Philadelphia, they once had an issue with yellow fever.
You know anything about yellow fever, you're like, what?
This isn't a joke.
It really happened.
It's weird, but it happened.
It caused a lot of economic devastation.
So much so that the banks were just like, you know what, we're going to automatically
renew everybody's notes.
Y'all don't even worry about it, because the banks were better back then.
The science was shaky.
They didn't really understand it.
They locked down the entire city on shaky science with a whole bunch of economic devastation.
Put up checkpoints, inhibited people's right to peaceably assemble and their freedom of
movement.
It happened.
The president at the time was George Washington.
It doesn't get more founder-ish than George Washington.
The idea that the founders wouldn't have tolerated this is historically inaccurate.
Not only would they, they did.
In many cases, some of them actually played a hand in this.
If you want to support and defend the Constitution, you have to know what's in it.
You have to know the whole thing.
This is not unconstitutional.
And the founders certainly would have been okay with it.
They were.
Does this mean that we shouldn't worry about expansive government?
Of course not.
Of course that's not what it means.
We absolutely should.
This scenario has played out many times in American history.
The powers are exercised and then they recede.
When this is over, we should certainly make sure that it recedes.
You want to get out there and make a statement for that?
I will be with you, probably still wearing a mask.
That's something that we should be concerned about.
But to present this as unconstitutional or something the founders wouldn't have tolerated
is wrong.
It's inaccurate.
It's not true.
And these statements, well-meaning or not, they're sending the wrong message.
If you cry wolf about tyranny, when tyranny arrives, nobody's going to believe you.
This has played out over and over again in American history.
Rights come with responsibilities, and one of them is to become educated.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}