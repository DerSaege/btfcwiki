---
title: Let's talk about bad kids, utopias, and Lord of the Flies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Y8c0FDZPbU0) |
| Published | 2020/05/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Raises the issue of labeling individuals as "bad kids" based on low expectations.
- Points out how low expectations can become a self-fulfilling prophecy for individuals.
- Compares society's negative perceptions of humanity to labeling children negatively.
- Challenges the idea that humans are inherently selfish or lazy.
- Advocates for portraying humanity in a more positive light based on evidence of positive behavior.
- Mentions the importance of expecting the best from people to achieve a utopian society.
- Refers to a real-life story similar to "Lord of the Flies" where kids thrived, contradicting the negative portrayal of human nature.
- Stresses the need to set high expectations to encourage positive behavior in individuals and society.
- Emphasizes the role of education in shaping behavior and setting standards.
- Encourages being a positive example for younger generations to create a better future.

### Quotes

- "If you want a utopian society, you have to expect the best from people."
- "Humanity isn't innately evil. Humanity just is. It's what we make of it."
- "If we want utopia, we have to build it. Step by step."
- "We should probably focus more on saying that it is possible. Not that it isn't."
- "We're not there yet, but we're going to get there."

### Oneliner

Labeling individuals negatively based on low expectations creates self-fulfilling prophecies, challenging society to expect the best from humanity for a utopian future.

### Audience

Educators, Activists, Parents

### On-the-ground actions from transcript

- Set high expectations for individuals and society, encouraging positive behavior (implied).
- Be a positive example for younger generations to foster a better future (implied).

### Whats missing in summary

The full transcript dives deeper into the impact of societal perceptions on individuals and the importance of education in shaping behavior. Watching the full transcript provides a comprehensive understanding of fostering positive change through collective optimism.

### Tags

#Utopias #HighExpectations #PositiveBehavior #Education #SocietyImprovement


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about Utopias.
Utopian philosophies.
And bad kids.
You know those kids that early on you're looking at them and you're just like, you are a bad kid.
You're not going to amount to anything.
No you don't. You don't actually know those kids.
But, we hear it.
We hear it. There are some people who the bar is set so low for
because
the expectations are low
they behave in a manner that
well it becomes a self-fulfilling prophecy.
There are kids who are told this so much
that that's the way they behave.
Nobody expects any different of them.
It's reinforced that
this is what they're going to be.
This is how it's going to turn out. There's nothing they can really do to change it.
We know that's wrong.
We know that it becomes a self-fulfilling prophecy.
We know the kids act out.
We know
that it lowers self-esteem and lowers a lot of other things
down the line.
We know that's the wrong way to do it.
So
why do we do it with all of humanity?
When somebody talks about a utopian ideal
people will often say,
that sounds great but it'll never happen.
Why not? Because humans are selfish or lazy or...
whatever.
Whatever the negative attribute is.
Humans aren't anything.
We're a tool using primates.
We aren't as cool as we think we are.
I think that we're doing a disservice
because what's happened
is that we
portray ourselves in this negative light,
all of humanity, all of society,
based on anecdotal stuff.
But we forget about all of the anecdotal stuff that
shows we're positive.
We don't use that as an example.
We use the negative stuff
and that's what goes out there.
That's what
showcases
humanity
to ourselves
and that's what we reflect.
Just like the kid who's always told he's bad.
Not going to amount to anything.
Yeah, for some people
that creates a drive.
I'll prove you wrong
and good for them, but for most that's not the effect.
I don't see
why it wouldn't be true for everybody.
If it's true for an individual,
why would it not be true
for a society made up of individuals?
Of course it is.
If you want a utopian society, you have to expect the best from people.
You have to be optimistic.
There's a really cool story about Lord of the Flies going around right now.
I'm not going to tell the whole story
because
there's an article that actually has a great
summary of it and it's a really good job.
So we're just going to roll with that. I'll put it down below.
But generally,
you know, we all know that story, Lord of the Flies, schoolboys
stranded on an island
and it goes all wrong
because humans are bad,
selfish, lazy, don't want to tend to the fire, all that stuff.
The thing is that actually happened
in real life.
Only it didn't go bad.
Kids survived, thrived
for more than a year.
I think
we can learn a lot from that story.
And you know, when you're reading it,
the researcher, the person who actually uncovered all of this,
you can tell that they get it.
That this shows that humanity isn't innately evil.
I just hope that everybody else does.
I hope that they get that
Lord of the Flies does not exist because humanity
is like that.
Lord of the Flies exists because humanity thinks humanity is like that.
That's why it resonated. That's why it became a classic.
Because it portrayed the darker aspects.
The taboo, but the fact that it is taboo
kind of shows that that's not
the norm.
That's not the way most people are.
That's the outlier.
Most people are good. Most people would be like the real life story.
I don't know that we should center our society,
our entire civilization,
based around the idea that we should really take into account those outliers.
Those people that aren't going to play by
the rules. That aren't going to show social responsibility. And I know right now
with a whole bunch of people not showing social responsibility, it seems like a weird
time to bring this up.
But I think it's the perfect time to bring it up.
Because we don't expect better of them.
It's self-fulfilling.
We don't expect better of them.
There's a lot of times
when
we're discussing
that ilk.
That we talk about how they're not educated.
That they won't get educated.
And for those that are new to the channel, when I say education I do not mean college.
You don't need a degree to be educated.
We hold them to a low standard.
So of course they behave
that way.
I think it's important
for everybody to remember that we
are the example
for those younger than us.
And if we become negative, they'll become negative.
They'll become jaded.
They'll buy into this system.
If we want utopia, we have to build it.
Step by step.
And we have to show that it's possible.
And it is.
Humanity isn't innately evil.
Humanity just is.
It's what we make of it.
And the more
we divide ourselves
between enlightened
and those others,
the more it falls apart.
Because it's much easier
to be the selfish and lazy.
You have to work
to want to have social responsibility.
It's easier.
So if we reinforce the idea that people are lazy, that people are innately evil,
that it's hard work
to become good,
which side wins?
We should probably focus more on saying that it is possible.
Not that it isn't.
We're not there yet,
but we're going to get there.
Because you're going to help us.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}