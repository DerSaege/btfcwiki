---
title: Let's talk about a missing takeaway about news interactions this week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OohKtPH0Bs4) |
| Published | 2020/05/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Journalists, correspondents, and reporters had unfavorable interactions with law enforcement this week, sparking a debate on free speech implications.
- The prevailing narrative that these events will chill freedom of the press is inaccurate; journalists are not deterred and often see such incidents as cool stories.
- The more critical takeaway is that these journalists were not threats but still faced force from law enforcement.
- Law enforcement's poor ability to accurately threat-assess is concerning; they should not be able to determine when force is necessary if they can't discern non-threatening individuals.
- Beau suggests prioritizing threat assessment training over the warrior cop mentality in law enforcement.
- He challenges the perception of policing as a highly dangerous job by citing statistics showing other professions, like fishermen and roofers, face higher risks.
- The accuracy of police actions, especially in determining threats, should be held to a higher standard by the public.
- Beau stresses the need to address the issue promptly, urging communities to push for reforms within their local police departments.

### Quotes

- "None of these journalists were threats. None of them could be mistaken as a threat, but they had force used against them."
- "Law enforcement has a really poor record of being able to threat-assess."
- "Being a cop isn't even in the top 10 [most dangerous jobs]."
- "Only you can stop city fires."
- "This is something that needs to be addressed."

### Oneliner

Journalists face force from law enforcement despite not being threats, revealing a critical flaw in threat assessment within policing that needs immediate attention.

### Audience

Community members, Activists, Journalists

### On-the-ground actions from transcript

- Push for threat assessment training in local police departments (implied)
- Advocate for reforms addressing the accuracy of police actions in determining threats (implied)

### Whats missing in summary

The full transcript provides additional context on the flawed perception of policing as highly dangerous and the need for communities to actively address issues within law enforcement.

### Tags

#Journalism #LawEnforcement #ThreatAssessment #CommunityAction #PoliceReform


## Transcript
Well, howdy there, internet people.
It's Beau again.
So, today we're gonna talk about a missing takeaway.
There's a conversation that's happening
because of a whole bunch of events,
and it's the way wrong conversation.
Because one, what's being asserted is just not true.
And two, that conversation is,
it's overshadowing a much more important takeaway.
So that's what we're gonna talk about today.
If you've missed it, a whole bunch of journalists,
correspondents, and reporters have had,
let's just call them less than favorable interactions
with the law this week.
The takeaway that's being discussed is that, well,
these actions, they're gonna have a chilling effect
on free speech.
They're gonna chill freedom of the press.
The takeaway being that the journalists, correspondents,
and reporters that ride into events like that,
well, they're gonna stop doing their jobs because of this.
Okay, that's just not true.
That is not true.
I happen to know a few journalists like that,
that go to those events, or that used to.
Now, it's not going to chill them.
It's not gonna deter them from doing their job.
They love that stuff.
That's not a traumatic event for them.
That's a cool story to tell later.
Now, if it was more severe, more prolonged, maybe.
But right now, no.
No, it's not even a worry.
I know a lot of journalists who actually
really enjoy that type of stuff.
I mean, they love it.
I'm looking at you, Ford Fisher.
But this whole conversation is overshadowing
something much more important.
None of these journalists were threats.
None of them were threats.
None of them could be mistaken as a threat,
but they had force used against them.
What is all this about?
Law enforcement wants us to just accept their determination
of when force is necessary.
But they've shown live, in some cases,
that they don't have a clue of when it is necessary.
If you can't determine that people clearly marked
as press with a camera, with mics, and everything else
are not a threat to you, then you shouldn't have a badge.
And if you used force knowing that they weren't a threat
to you, then you shouldn't have a badge.
If it was confusing and you even had a moment of thinking,
oh, well, maybe this is dangerous,
you shouldn't have a badge.
That's the important takeaway.
Law enforcement has a really poor record
of being able to threat assess.
Maybe we need more classes on threat assessment
and less on being a warrior cop.
You got to get home at the end of your shift.
Because the problem is they bought
into their own propaganda.
They're concerned about that.
They're worried.
The reality is being a cop is not that dangerous.
I know that flies in the face of everything
you get told every day.
Look it up, Bureau of Labor Statistics.
Fishermen, loggers, roofers, pizza delivery drivers
all have more dangerous jobs.
Being a cop isn't even in the top 10.
I would suggest that the fact that the majority
of this country holds their pizza delivery
driver to a higher standard in regards
to the accuracy of their order than they do law enforcement
in regards to the accuracy of their bullets
might be part of the problem, especially when they have
demonstrated live on the air in some cases
that they cannot determine what a threat is or is not.
That's a much more important takeaway.
It is much more relevant to this conversation.
Because journalists, they're going to ride into this stuff.
They love it.
And as far as I'm sure the claim is going to come out,
well, they weren't complying or something.
They're going to make something up.
These were corporate journos.
I might buy that if it was indie correspondents,
because they do have a habit of telling law enforcement to go
somewhere else.
But these people worked for CNN and local affiliates.
No.
No, that's not happening.
You could say that for somebody else.
This is something that needs to be addressed.
And it needs to be done quickly.
And this is something that everybody should be pushing
their local department for before there's a problem.
Only you can stop city fires.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}