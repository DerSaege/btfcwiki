---
title: Let's talk about why you should be a good person....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ukJIGJ7Kxoo) |
| Published | 2020/05/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Emphasizes the importance of being a good person because it's the right thing to do.
- Criticizes those who need self-interest or partisanship as a reason to behave nicely.
- Mentions the impact of baseless accusations during Supreme Court nomination hearings and how it can forever alter relationships.
- Describes a story of a woman considering adoption based on the adoptive parents' behavior on social media.
- Stresses the value of character over material wealth and Easy Street living, advocating for facing challenges and defeats in life.
- Points out the significance of one's words and actions on social media, which can have lasting consequences.
- Argues that showing loyalty to harmful ideologies or politicians at the cost of relationships and values is not worth it.
- Encourages people to think for themselves and express genuine beliefs rather than blindly following harmful rhetoric.
- Asserts that abandoning principles for party loyalty can lead to losing valuable relationships and being perceived as not a good person.
- Affirms a belief in the prevalence of good people in the world who value not kicking others down.

### Quotes
- "Being poor is a lack of cash, not a lack of character."
- "It's better to be poor than be a racist."
- "If you kick down, people see it. And they have no reason to help you."

### Oneliner
Be a good person, prioritize character over material wealth, and avoid harmful rhetoric to maintain valuable relationships and integrity.

### Audience
Social media users

### On-the-ground actions from transcript
- Choose your words and actions carefully on social media (implied)
- Prioritize character and values over material gain (implied)
- Think for yourself and express genuine beliefs (implied)

### Whats missing in summary
Beau's engaging storytelling and emphasis on personal integrity are best experienced by watching the full transcript.

### Tags
#Character #GoodPerson #SocialMedia #Integrity #Relationships


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about being a good person
and why you should be a good person if you don't wanna be.
You know, the reason you should be a good person,
the reason you shouldn't kick down
is because it's the right thing to do.
That's the reason.
Some people, because they bought into the idea
that others are like them, they need self-interest.
They need a reason to behave nicely.
When the Supreme Court nominations were going on,
when the confirmation hearings were happening,
there were a lot of people that came out of nowhere
with no evidence, no facts, nothing,
just based on partisanship in interest
of signaling to their in-group.
Well they got on social media and they called her a liar,
making it up.
I said at the time that those posts were forever
altering relationships and the people making them
don't even know it's happening.
Because the number of people, women specifically,
that have been a victim of that, it's staggering.
It is staggering.
There are people on your social media feed
who have been a victim of it.
And they saw those posts and it confirmed what they believed,
that well maybe it was the right thing not to come forward.
They weren't going to be believed anyway.
By people they know, maybe even liked.
But it altered that relationship.
And that's cool, abstract manner.
I mean that's cool.
It illustrates the point that your words can actually
change your reality.
You may not even know it.
But every once in a while, a story
emerges that brings it out of the abstract
and into the concrete.
And I read one today.
And the truth is, we don't know if this is true.
We don't.
There's no real way to fact check it.
But it very well could be true.
Every word of this could be true.
But there's no way for us to know really.
However, it's a whole lot more concrete,
because it shows a direct chain of events.
So there's a couple.
They've got four kids.
They are economically strapped.
They don't have a lot of money.
And despite using protection, it happens, trust me,
chance of pregnant again.
She makes the choice to put the child up for adoption.
And it's like a friend of a friend of a cousin
or something like that.
Somebody they don't really know.
But they're loaded.
They got money.
And the child's going to have a good life.
You know, when they go over, they've
already set up a nursery.
They're showing brochures for the private school
they're going to send the kid to.
The kid's going to have it on Easy Street.
And for somebody who has struggled,
that's probably pretty appealing.
Everything's going great.
It's a huge load off of mom.
And then something happens.
The adoptive parents, well, they friend her on Facebook.
And she says she sees posts that are bigoted.
And basically, it's intermingled with pro-Trump posts
and then something about Muslims or gays or Chinese or whoever,
all kicking down, all blaming those, those others.
And she decides not to go through with the adoption.
She made a post asking if it was the right move.
I want to go ahead and weigh in on that now,
just in case she sees it.
Being poor is a lack of cash, not a lack of character.
I would strongly suggest that character
is far more important in life because it
gets into the philosophical aspects of what
is life and all of that.
And Easy Street sounds great, but if you have no challenges,
you don't have any defeats, you have no victories.
You just exist.
Character is far more important because
with character, you can come from nothing.
You can come from the lower rungs
of the socioeconomic ladder, and you can improve your station
to some degree.
And that's worth its value.
I would suggest a mother who would make this decision based
on her morals and ethics would instill character
in her child.
And I think character is more important.
But at the end of the day, on the topic
that we're actually talking about,
these people, the adoptive parents,
may not even believe the stuff that they
posted on social media.
They're just signaling to an in-group
to show brand loyalty to some politician.
Forever altered their life.
The internet's forever.
Their post will be there forever.
Everybody's going to know.
They're going to remember.
And social media feeds will provide the receipts.
Forever altered their life.
They may never have another opportunity like that again.
So to show brand loyalty to somebody
who does not care about you and ideas that you probably
know are wrong, you gave up your family.
It's a pretty big price to pay for some likes on Facebook.
Your words are really important.
They carry a lot of weight.
They adjust the way people perceive you
and what they're willing to do with or for you.
If you kick down, people see it.
And they have no reason to help you.
They have no reason to give you the understanding that you
refuse to grant others.
Espousing socially regressive ideas is never good.
It's never good.
It never pays off because the social progressives always win.
Assuming all of this is accurate,
the mom definitely made the right choice.
It's better to be poor than be a racist.
I mean, that's an accurate summary of it.
When you're making these posts and you are signaling,
understand that there are people that are receiving the signal
and they may not be receptive to it.
Think for yourself.
Say the stuff that you truly believe.
And let the trips fall where they may.
But I refuse to believe that many of the people who are
cosigning a lot of today's rhetoric actually believe it.
They probably never even thought about it.
They're just showing their loyalty to a party
over their principles.
And that's a way to get abandoned.
It's a way to lose some of the most important things
in your life.
And that's a way to get abandoned.
And that's a way to get abandoned.
It's a way to lose some of the most important things
in your life.
Because if you're going to abandon others,
people that you actually interact with,
for somebody you've never met who does not care about you,
you're probably not a good person.
I believe there are more good people in the world than bad.
And good people like to hang out with those who don't kick down.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}