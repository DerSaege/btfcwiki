---
title: Let's talk about social progress and optimism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IHNEoit5enw) |
| Published | 2020/05/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about social progress and optimism, reflecting on being asked how to stay calm and optimistic due to his channel's content.
- Frames the channel as discussing social progress rather than left and right politics, defining the real framing as social progressive versus social conservative.
- Explains the anger behind social issues for progressives and conservatives, with progressives seeking to right wrongs and conservatives preferring tradition and the status quo.
- Shares his ability to stay calm and optimistic by disconnecting from direct impacts of social issues due to privilege, allowing him to see the bigger picture.
- Asserts that on a long enough timeline, the social progressive always wins in history, despite occasional regressive periods.
- Describes the process of social progress, starting with identifying injustices, presenting solutions, and embracing by open-minded individuals who pass it on to younger generations.
- Gives a real-life example of shifting perspectives on interracial marriage across different generations, illustrating how social issues can evolve and be solved over time.
- Mentions the historical trend of social progress starting in the United States and getting exported to other regions.
- Encourages those fighting for socially progressive causes to stay strong, as they are just holding on until reinforcements arrive, with historical certainty of eventual victory.
- Advocates for advancing socially progressive ideas while limiting dependence on government force as the key to winning in the long run, acknowledging it may not happen in their lifetimes but staying optimistic about being on the winning team.

### Quotes

- "On a long enough timeline the social progressive always wins."
- "If you are on the front lines of a socially progressive issue today, you're just fighting a holding action."
- "You're just hanging on until reinforcements show up."
- "It's a historical certainty."
- "It's how fast and how far we want to take it."

### Oneliner

Beau talks about social progress, framing it as a battle between social progressives and conservatives, with a historical certainty that social progress always wins, encouraging optimism and perseverance.

### Audience

Community advocates

### On-the-ground actions from transcript

- Advocate for socially progressive ideas within your community (suggested)
- Educate younger generations about social justice issues (exemplified)
- Support and join movements fighting for social progress (implied)

### Whats missing in summary

The full transcript provides a detailed and insightful perspective on social progress, optimism, and the long-term inevitability of social progress despite temporary setbacks.

### Tags

#SocialProgress #Optimism #Injustice #CommunityAdvocacy #HistoricalCertainty


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about social progress
and optimism.
We're gonna do that because
somebody asked me to hype her up today,
get her motivated.
And uh...
I realized I get asked a question, two questions, a lot
because of this channel. One, how do I stay calm
and how do I stay optimistic?
I was never able to articulate it before today, didn't really know how to answer
that question.
I started thinking about it.
I think part of it has to do with we
frame this channel wrong.
When people discuss this channel
they talk about left and right.
But that's not really
the right framing for most of the stuff that we talk about.
Most of the time
we're talking about social progress.
So the real framing
is social progressive
and social conservative.
That's really a more accurate framing of it.
When you're talking about social issues
people get angry.
And it makes sense because people become a social progressive most times
because they've witnessed an injustice
or maybe it's impacted them directly.
They want to right that wrong, they want to fix it.
And that wrong makes them angry.
Makes sense.
Social conservatives by definition
they like the status quo, they like tradition.
They don't like change. Change is scary, scary creates fear, fear creates
anger.
Makes complete sense.
I don't succumb to that
in part
because I can disconnect.
Most of the issues we talk about
they don't hit me directly.
I'm very privileged in that way.
And because I have that luxury
I can take a step back.
I can disconnect and because I can do that
I can see
what's going to happen.
I know who's going to win.
I don't have a crystal ball or
magic cards or anything
but I have history books.
And they're a whole lot better at telling the future.
Spoiler alert.
The social progressive
always wins.
On a long enough timeline
the social progressive will always win.
It's the overriding theme of history.
More freedom for more people.
Take steps back every once in a while.
Every once in a while you hit a regressive period.
We're in one now.
We are in one now. You can always tell when you're in one
because the people at the top
all they're trying to do and the only idea they have is to undo the stuff that
came before them.
You're in a regressive period. Fine.
So, I'm going to take a step back.
But we're going to take three forward right afterward.
On a long enough timeline
the social progressive always wins.
Works like this.
An injustice is identified
and a solution
is presented.
People recognize
an injustice. Maybe they didn't recognize it before. Maybe they ignored it.
But for whatever reason that injustice comes to the forefront
with a solution.
That idea,
that package gets embraced
by the people who are most open-minded.
Normally
it's the younger crowd.
In the beginning
that generation,
they're young at this point,
ten, twenty percent of them
embrace that idea.
You see, they teach their kids.
And their kids
have that idea.
And it spreads a little bit.
Then it happens again.
And it happens again.
By the time that generation,
where ten or twenty percent of them
embraced it,
by the time that generation becomes the older generation,
it's a majority
of people
in support of that idea
in the younger generation.
That's abstract.
Let's do it for real.
Last year there was a study
and it broke stuff down by generation, how different generations
felt about different things.
The percentage of people
who believed that different races marrying
was a good thing for society.
The silent generation,
twenty percent,
thought it was a good thing.
The boomer generation,
thirty percent.
Gen X,
forty one percent.
Millennials, Gen Z,
it's a majority.
By the time
millennials
are the older generation,
it'll be eighty, ninety percent.
It won't be a social issue anymore.
It'll be solved.
And yeah, this is just in the US.
It may be different elsewhere.
That's also how it happens historically.
It starts somewhere and it gets exported.
In the United States,
we have that
machinery for change.
So a lot of socially progressive ideas start here.
Not all, but a lot.
So with this in mind,
it may not give you any comfort
if you are
being impacted by one of these injustices today.
But you should know
that it's not that you haven't won.
It's that you haven't won yet.
It will be different for those people who come after you.
It will be easier
each time, and it doesn't matter what the issue is.
If you are on the front lines
of a socially progressive issue today,
you're just fighting a holding action.
You're just hanging on
until reinforcements show up.
And they're coming.
You know they're coming.
And you know
you're going to win.
It's a historical certainty.
When that
idea comes,
the only question we have, it's not whether or not we're going to win.
It's how fast and how far we want to take it.
It's really what it boils down to.
For me, that overriding theme,
the most freedom for the most people,
I suggest we take that all the way.
And it's easy.
All we have to do is to continue to advocate socially progressive ideas
and simultaneously limit
dependence on government.
That's it.
That's it.
As long as those ideas keep getting advanced,
and sometimes that becomes hard.
Because when you see an injustice,
a lot of times the most available tool
seems to be government.
But generally,
they change the law,
but it's thought.
Changing thought is what matters.
So that's all we have to do.
We keep advancing the socially progressive ideas
and
limit
our
use of
government force.
That's it.
That's all it takes to win.
Not going to happen in our lifetimes.
You know, to get to that ideal world,
the one I want,
I'll be long gone.
But I can stay optimistic
and stay in the fight
because I know I'm on the winning team.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}