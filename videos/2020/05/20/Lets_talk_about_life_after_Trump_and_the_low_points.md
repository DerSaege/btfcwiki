---
title: Let's talk about life after Trump and the low points....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WA_M7Eqc2H4) |
| Published | 2020/05/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes a true story of a man named Yamaguchi, a ship designer, who experienced the worst week ever during World War II in Japan, surviving both Hiroshima and Nagasaki atomic bombings.
- Yamaguchi's resilience and survival despite severe injuries stand out as an example of getting through low points.
- Beau draws parallels between the recovery of Hiroshima and Nagasaki after the devastation and the current low points faced by countries and individuals worldwide.
- Expresses concern over leaders prioritizing personal gains and neglecting critical issues like infrastructure, mass incarceration, and poverty alleviation during challenging times.
- Criticizes the glorification of past eras like the 1950s, pointing out the flawed nostalgia for a mythological time that neglects the realities of injustice and challenges faced then.
- Beau warns about the dangers of the United States behaving like a failed state due to inaction on impending crises despite knowing about them.
- Emphasizes the need for collective action, new ideas, and systemic change driven by people at the grassroots level to address current challenges effectively.
- Encourages individuals to be part of shaping a better future by focusing on real progress and advancement rather than regressive ideals.
- Stresses the importance of learning from low points to propel society, countries, and individuals forward with momentum and unity.

### Quotes
- "That's what's scary to me."
- "We're at a low point. Fact. But we're going to get through it."
- "It's not going to be up to them. It's going to be up to you and me."
- "We need new ideas. We need real systemic change."
- "Being a part of actually advancing."

### Oneliner
Beau shares a story of resilience amidst the worst week ever, urging collective action and systemic change to move forward from current low points towards real progress.

### Audience
Activists, Community Members

### On-the-ground actions from transcript
- Cooperate and work together with people at the grassroots level to generate new ideas and drive real systemic change (implied).
- Take part in shaping a better future by actively advancing societal, national, and global progress through collective efforts (implied).

### Whats missing in summary
The full transcript provides detailed insights into historical resilience, current challenges, and the need for collective action to overcome low points and drive real systemic change effectively.

### Tags
#Resilience #CollectiveAction #SystemicChange #MovingForward #GrassrootsEfforts


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about how to get through low points
because we're in one
as a country, as a world, as a species,
as individuals.
I was reading messages tonight, it was a very common theme, a lot of people are down.
So we're gonna talk about how to get through them and where we go from here.
When people talk about their worst week ever
I have a true story I like to tell people
because it kind of puts things in focus because it is
as far as I know
it actually is the worst week ever.
Nobody watching this has dealt with anything like this
and it's a true story.
There's this guy
he's a ship designer
Japan
1940s
works for Mitsubishi, builds tankers.
He's on a business trip
and he's down by the docks.
Everything's going good, he's in high spirits. Yeah he forgot something and had to go
back and get it. I think it was like a travel stamp or something.
But he's in a good mood, having a good day
going about his business, about to head back home and boom air raid.
Knocks him to the ground.
Eardrum busts.
Burned up.
Bad, he is messed up.
Takes him a full day
just to kind of recompose himself so he can get started back home because he
wants to get home now.
Now
it being the time
that era, he gets home
and he goes to work three days later.
Bandages and all.
He's sitting there talking to his bosses
telling them what happened. They're like, no way, boom air raid.
These weren't normal air raids.
His business trip took him to Hiroshima, he returned home to Nagasaki.
That's the worst week ever
of anybody.
That's
pretty much it. His name was Yamaguchi.
That was his last name.
And
that's it.
That's pretty horrible. I mean aside from the physical aspects of it, I mean
that's got to be pretty traumatic.
Because at the time nobody even knew it existed.
His bosses were telling him it was impossible
for that to have happened.
You're an engineer, calculate it.
But it did happen.
But he made it through both.
Lived to be like ninety.
Had kids.
Became pretty outspoken
when it comes to prohibiting those things.
I would imagine that week was his low point.
But he made it through it.
What about the cities?
The cities themselves?
You see those two city names in the United States,
nobody thinks of cities.
They think of rubble.
Those city names have just become an image
of destruction.
But what's the reality?
They're back.
They're pretty big cities.
They had a low point too.
And they recovered.
Just like we will.
Just like we will.
See, we're in a low point, but to me that's not the scary part.
That's not the unnerving part.
Because we'll get through that.
What's scary to me is watching our leaders, our betters,
be more concerned about feathering their own nests
or their re-election chances
than they are with dealing with the low point.
What's scary to me is watching our infrastructure crumble.
Realizing that the mass incarceration issue,
it's still there.
It's not even really being highlighted the way it should right now.
Watching people defend, actively defend,
go out of their way to defend a system
that keeps millions on the brink of poverty,
to keep them in line.
And meanwhile, almost half the country,
as they are making it worse,
chant about how they're making it great again.
That's what's scary to me.
And you know when you ask them,
when are you talking about?
When exactly are you reaching back to?
They say the 1950s.
You know, it's even in the slogan that it's regressive,
that it's dated.
But when you ask them,
they say the 1950s.
Why?
Because of an image.
An image that's wrong.
It's a lot like the image we have of those cities.
Because they have the idea of either what they experienced as kids,
or stories that they were told.
Leave it to Beaver type stuff.
The reality is that that was just a few years after that happened.
The entire country lived under the threat of that occurring at any moment.
There was injustice everywhere.
All kinds.
That's how we're going to make America great again.
We're going to reach back to that.
It's an image. It's a myth. It's not real.
It's not real.
But people think about it because they were kids,
or they're basing it on stories.
They didn't have a clear picture of it.
I'm sure at some point in the future,
I will look back on the 1980s as this great time,
and forget about the recession.
Crack.
Forget everything else that was going on at that time.
Because with age, you forget about the bad stuff.
You idolize things.
Get that image.
What's scary to me is not what's happening right now.
What's scary to me is watching the United States behave as a failed state.
Because see, what's happening right now,
we knew it was a possibility, we knew it was coming, we did nothing.
We had infrastructure issues, we knew it was coming.
There's a whole bunch of other stuff we know is coming,
and we're not acting.
We're downplaying it.
We're pretending it's not real.
Pretending that the experts don't know what they're talking about.
How has that worked for us this time?
A toll as high as Hiroshima.
We are, we're at a low point.
Fact.
But we're going to get through it.
And we need to keep our spirits up.
And get our mind around the fact that we got a lot of work to do.
We have to fix everything these people did while they were making us great again.
Because the US at this point, the behavior,
is the behavior you see in a failed state.
That's what's scary.
That's what's unnerving.
And it's not going to get fixed by people in DC.
And I know you're thinking, your favorite politician, whoever it is.
No, not really.
It's not going to be up to them.
It's going to be up to you and me.
Because we need new ideas.
We need real systemic change.
Something that works for the people on the bottom.
And that's going to take those people on the bottom, working together, cooperating,
coming up with the ideas that we need to move forward.
We are in for a long haul.
Take pleasure in the fact that you're going to be part of it.
That you will be part of setting the stage.
Not to make America great again.
Not to compare it to some myth.
But being a part of actually advancing.
Moving society, the country, the world, the species, and individuals, all moving forward.
That's what we have to look forward to.
And we're only really going to have the momentum because we went through a low point.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}