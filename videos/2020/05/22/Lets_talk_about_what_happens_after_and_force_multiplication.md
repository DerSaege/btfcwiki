---
title: Let's talk about what happens after and force multiplication....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ixH29vYrB_8) |
| Published | 2020/05/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a message referencing a self-defense video in Cincinnati, sparking a larger point about what comes after.
- Proposes a plan where individuals are trained to teach others, creating a network of millions ready to fight for change in less than a year.
- Emphasizes the importance of preparation for what happens after any potential revolution or change.
- Questions the effectiveness of movements that lack a clear post-revolution vision and systems in place.
- Advocates for decentralized power through education and embracing new ideas.
- Acknowledges the dirty and ugly reality of real-life revolutions, contrasting with glorified perceptions.
- Stresses the need to reach those clinging to old ideas before meaningful change can occur.
- Urges for building community networks, establishing redundant systems, and focusing on new ideas over violence.
- Shares an example of a class in Cincinnati focusing on sustainable farming, community networks, and mutual aid instead of revolutionary tactics.
- Refuses to teach offensive tactics to avoid inciting unnecessary violence and hostility.

### Quotes

- "If you don't have an answer to that, it means nothing."
- "I don't want to be one of the people who makes peaceful revolution impossible."
- "Without the new ideas to back it up, it's not a revolution. It's just a hostile takeover."

### Oneliner

Beau stresses the importance of planning for what comes after any revolution and advocates for decentralized power through education and new ideas.

### Audience

Change advocates

### On-the-ground actions from transcript

- Build community networks and establish redundant systems (implied)
- Focus on embracing new ideas and education (implied)
- Engage in mutual aid activities (exemplified)

### Whats missing in summary

The full transcript provides a deeper understanding of the importance of post-revolution planning and the necessity of embracing new ideas for meaningful change.

### Tags

#Revolution #CommunityEmpowerment #DecentralizedPower #Education #NewIdeas


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about what comes after.
The beginning of this may seem a little weird, just stick with me, there's a bigger point.
We're going to talk about what comes after because I got a message from somebody who
apparently saw a video of me on Facebook giving a self-defense course in Cincinnati years
ago.
I couldn't find the video, but I know the one you're talking about because I don't
give a lot of those classes.
This video made them curious and they talked to some people who knew me and one of them
said that in six weeks I could teach twelve people the skills they need to truly take
America back.
And this person was disappointed that I wasn't using this platform to do that.
Nine.
Probably nine weeks to be honest.
If you give me an additional week I can teach them to teach.
So each one of those twelve can go and teach twelve and then teach twelve.
By the fifth generation of that you have a little less than a quarter million people
in the field ready to fight.
There are twenty people in the comments section on YouTube that can do the exact same thing.
It's not really a rare skill.
There's probably an additional thousand former Eleven Bravos down there who may not be able
to teach people to teach, but they can teach a whole lot more people in those ten weeks.
If you were to add that up, less than a year, more than five million people in the field
ready to fight.
Then what?
What comes after?
After the Glorious Revolution?
Of course the troops are home by Christmas and the good guys win.
Then what?
If you don't have an answer to that, it means nothing.
It's just a bunch of waste.
If the systems aren't in place to replace the ones that exist, it means nothing.
If people aren't ready for the new ideas, aren't ready to evolve, aren't ready to go
further, aren't ready to change the system, you just get the status quo again.
It's all there is to it.
There's a whole lot of channels out there who use that kind of rhetoric a lot.
This channel doesn't.
I don't talk like that very often.
Imagine what their comment section looks like, how many people below have that skill set.
Assuming the Glorious Revolution goes great, how long is it until one of them teaches twelve?
Who teaches twelve?
Who teaches twelve?
And that just becomes the normal way our government changes.
If people are not ready for it, it means nothing.
You ask those on the right.
There's a whole bunch of people who spend a whole lot of time planning and getting ready
for that fight.
You ask them what comes after and they say, restore the republic.
I don't even know what that means.
What, we're not going to have a king?
They're going to restore the representative democracy with a federal...
What does it mean?
They don't know.
It's just a slogan.
It means nothing.
The mission has become about the fight.
And when the mission becomes about the fight, after the fight, if people aren't actually
ideologically motivated, the whole Glorious Revolution is just a scheme to put a new group
of people in power.
You get the same thing.
Different faces.
I would prefer that everybody have power.
That it become decentralized.
And the way to do that is through education.
It's through talking about new ideas.
That's what will move us forward.
And again, it's not a rare skill.
You have to wonder why it's not happening on a widespread level with all of the discontent
in this country.
Why there aren't people out there doing that?
A lot of them.
Because anybody who has that skill set, who understands it, knows that we don't want that.
Because in real life, it isn't glorious.
It is dirty.
It is nasty.
It is ugly.
It is not something anybody wants to be a part of.
In the United States, with as much stuff as is in civilian hands, it would be devastating.
It would go on forever.
It would be a lot of loss for no benefit.
Because while, yeah, there are a lot of people who are ready for those new ideas, who are
ready for a system that doesn't leave people out, there are a whole bunch of people that
are clinging to the past and clinging to old ideas.
And until those people are reached, it's all for nothing.
It is all for nothing.
All of that lost, all of that waste, would be pointless.
We have to get the systems in place first.
I would prefer that people spend their time, rather than running around the woods, I would
prefer they spend it building community networks, establishing those parallel and redundant
systems, embracing the new ideas.
That seems much more important to me.
I would also point out that that class in Cincinnati, the same day that they participated
in mine, they learned about sustainable farming, how to can, how to build mutual networks,
or how to build community networks and engage in mutual aid.
That crew that was at that class, I met them because they had a network that fed the homeless
in Cincinnati and they had issues with the cops.
They weren't out looking to start a revolution, not of the kind that's being talked about.
Without the new ideas to back it up, it's not a revolution.
It's just a hostile takeover.
I don't use that rhetoric and I will not teach anything offensive because I don't want to
convince people that it's necessary.
I don't want to be one of the people who makes peaceful revolution impossible because I know
what is inevitable if that happens and I want no part of it.
We have to have the new ideas first and if we do it right, nobody ever has to teach anybody.
Anyway it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}