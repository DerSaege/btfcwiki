---
title: Let's talk about a Trump and voting by mail theory....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KGRpDi3g_Ig) |
| Published | 2020/05/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump is campaigning against expanding mail-in voting, which could benefit the working class who may find it difficult to take time off to vote.
- Trump's voter base mainly consists of older individuals, particularly those aged 65 and above, who heavily rely on mail-in voting.
- Trump fears expanding voting access to younger individuals who may not support him, as his base is primarily composed of retired or financially stable individuals.
- Voter suppression is a term used to describe Trump's actions to limit voting access under the guise of preventing fraud.
- Trump's concerns about voter fraud are often a cover for his fear of losing elections.
- Trump's administration has consistently shown a lack of concern for the average American and their voices.
- Beau believes that Trump prioritizes his approval ratings over the interests of the working class.
- The military has been voting by mail for decades, showcasing the hypocrisy in Trump's opposition to expanding mail-in voting.
- Beau suggests that Trump's resistance to expanding voting access stems from a desire to cater to his core supporters who are older and watch Fox News regularly.

### Quotes

- "Voter suppression is a term for it."
- "Trump doesn't represent the working class."
- "He doesn't care about the average American."
- "Probably don't want that expanded either."
- "He just wants those old people who sit at home and watch Fox News all day."

### Oneliner

President Trump campaigns against expanding mail-in voting, fearing increased voter turnout from demographics less likely to support him, like the working class.

### Audience

Voters, Advocates

### On-the-ground actions from transcript

- Contact local officials to advocate for expanded mail-in voting access (suggested)
- Join organizations working to combat voter suppression (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of President Trump's stance on mail-in voting, offering insights into his motivations and the implications for different voter demographics.

### Tags

#MailInVoting #VoterSuppression #WorkingClass #TrumpAdministration #ElectionFraud


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about President Trump, mail-in voting, and the perception
of who the Republican Party represents.
Sure you've heard he has launched a campaign against expanding mail-in voting, which is
funny to me because mail-in voting would certainly help the working class, those people who have
to work, those people who can't take off, those people who are just too busy because
they're out there being job creators and being productive members of society.
Those are the people that really could benefit from it and that's apparently who the Republican
Party represents in theory.
But that may not be true.
Okay, so expanding mail-in voting theoretically would help Trump because the people who use
it most are his people, 65 and up, retired people.
See Trump didn't get a majority of any age demographic until you hit 50 years old or
older.
By 65 and up he's getting 53% of the vote.
That's pretty significant.
And in 2016 the number of people who were 60 plus who voted, it was like more than 70%,
a really high turnout.
And they use mail-in voting the most.
It's almost like if people have access to means, shopping cart corral, they will exercise
their social responsibility.
And he doesn't want them to do that.
Because his worry is that if younger people have access and can vote, well then they'll
vote against him because they will, because he doesn't represent the working class.
The people who are voting for him are significantly those who are either at a station in their
life where they can take off or they're retired.
The average blue collar worker, yeah there's some, but most know he doesn't represent their
interest.
I mean you've got those very loud, diehard fans, but they're slipping, they're dwindling.
So if more of them had the ability to vote, that'd be bad for the Republicans.
It would be bad for Trump.
So he wants to cut off access.
There's a term for it, it's called voter suppression.
And I know his diehard fans are going to say, no it's about fraud.
He's worried about fraud.
He's always worried about fraud.
In any election he thinks he's going to lose.
He made that claim here in Florida when he thought his people were going to lose.
He started rage tweeting about voter fraud and finding extra votes for Democrats and
said he was going to send his lawyers down here.
He never sent his lawyers, by the way.
They were probably too busy.
Maybe they were working on the whole Trump University thing or they were making payments
to people, I don't know.
But they never showed up.
However, FDLE, our state level Bureau of Investigation, they investigated it thoroughly and found
nothing.
Voter fraud is just what he drags out.
It's his go-to because his base is conspiratorially minded.
And if he loses, well he has to have been cheated.
And he believes that because that's how he would win if he could.
My opinion on that anyway.
The key takeaway here is that he doesn't care about the average American.
He doesn't care about their voice.
He doesn't want to hear it, which makes sense.
It's what he's proven his entire administration.
Doesn't even listen to the experts.
Had he done that, led us into a shutdown a week earlier, tens of thousands of lives would
have been saved.
He doesn't care.
He never did.
He doesn't represent working class interests.
It's not even on his agenda.
He cares about his approval numbers and how many of them that he can dupe into voting
for him a second time.
And if he can't trick them, well he's got to stop them from voting.
Easiest way to do that is cut off their means to exercise their social responsibility under
this system.
That's my theory.
I know there are a lot of other ones.
But I would also point out, all of you true patriots, that the military has been voting
by mail for decades.
For decades.
Probably don't want that expanded either, because most of the troops have seen how much
he's weakened our national defense, how he's curtailed our ability to force project, how
he has sold out the country.
Make America great again, right?
Anyway, that's my theory on it.
I could be wrong.
I don't think I am.
I think it is about worrying that if he expands access to voting and more people vote, there
will be more people who don't like him who vote.
And right now, he just wants those old people who sit at home and watch Fox News all day.
That's who he wants to have access.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}