---
title: Let's talk about the Trump, Twitter, and the First Amendment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3iaFxD0N8t4) |
| Published | 2020/05/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Twitter fact-checked the president's tweet, and he reacted by accusing Twitter of interfering in the presidential election.
- The president claimed Twitter was stifling free speech, but Beau points out that the First Amendment is meant to protect people from the government, not the other way around.
- Beau criticizes the president for not understanding the First Amendment and the concept of free speech.
- He suggests that politicians, including the president, should be fact-checked to prevent them from deceiving the public.
- Beau defends Twitter's actions as a form of social responsibility and additional speech, not censorship.

### Quotes

- "Twitter is not stifling free speech. They are exercising social responsibility and providing additional speech."
- "It is embarrassing. It is appalling. It is pathetic that the President of the United States does not understand one of the most cited parts of the U.S. Constitution."
- "I personally think that all politicians should be fact checked on their statements."

### Oneliner

Twitter fact-checks the president, exposing his misunderstanding of free speech and the First Amendment, while Beau advocates for politicians to be fact-checked for accountability.

### Audience

Social media users

### On-the-ground actions from transcript

- Fact-check politicians (suggested)
- Support platforms exercising social responsibility (exemplified)

### Whats missing in summary

The full transcript provides a detailed breakdown of the controversy surrounding Twitter fact-checking the president's tweet and the importance of understanding the First Amendment and free speech.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the freedom of speech, the first amendment, the president
of the United States, and Twitter.
If you don't know, Twitter has had issue with less than reliable, less than accurate information
being spread on its platform.
As part of its campaign to exercise a little bit of social responsibility, it has begun
fact-checking popular tweets.
Yeah.
So one of the president's tweets was fact-checked, and he is bigly mad.
He took to Twitter, the platform he believes he's censored on, to tell everybody that he's
censored on the platform so everybody can read it on the platform.
Shortly after he was fact-checked, in almost real time.
But that was fast.
He says, Twitter is now interfering in the 2020 presidential election.
They are saying my statement on mail-in ballots, which will lead to massive corruption and
fraud is incorrect, based on fact-checking by fake news CNN and the Amazon Washington
Post.
He goes on in another tweet.
Twitter is completely stifling free speech, all caps, and I, as president, as president,
remember that, will not allow it to happen.
That's hilarious.
I mean, this is one of the funniest things that's ever happened on Twitter.
It's also one of the most embarrassing things that's ever come out of the White House.
First, a primer on the First Amendment, which is what enshrines free speech in the United
States, it's there to protect the people, private entities, from the government.
Guess which one you are, Mr. President?
You really don't have free speech on the same level as everybody else, especially once you
started using your Twitter as a method of government communication.
As you do when you say, Twitter is completely stifling free speech, and I, as president,
will not allow it to happen.
Yeah, you will.
You will allow it to happen because you don't have a choice.
You don't have a choice.
You are the government entity.
You can't censor somebody.
You can't stop them.
They were nice enough to give you a platform.
They're worried about misinformation.
So you make your tweet, they don't censor it, and they add a fact check.
That's their free speech.
You as a government entity cannot stop that.
That's a violation of the First Amendment.
It is embarrassing.
It is appalling.
It is pathetic that the President of the United States does not understand one of the most
cited parts of the U.S. Constitution, a document that he swore an oath to support and defend.
He's clearly never read it.
I personally think that all politicians should be fact checked on their statements.
It would make it a whole lot harder for them to dupe their voters.
You know, dupe them into believing that you'll make America great, that you understand what
this country is about, because you clearly don't.
Twitter is not stifling free speech.
They are exercising social responsibility and providing additional speech.
They didn't censor you, and even if they did, they could.
They don't owe you anything.
You, however, a public servant, do owe them something.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}