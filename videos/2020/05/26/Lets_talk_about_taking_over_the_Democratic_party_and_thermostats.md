---
title: Let's talk about taking over the Democratic party and thermostats....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mtwNc7eMczk) |
| Published | 2020/05/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Thermostats in big office buildings and commercial places are often placebos, giving employees the illusion of control without actually doing anything when adjusted.
- Crosswalk buttons are similar to thermostats, often not connected to anything and merely synced with traffic signals for cars.
- Voting may not be the most effective means of civic engagement, but it is a tool that can be used to exploit the naturally occurring power vacuum in representative democracies.
- To take over the Democratic Party, focus on lower offices like sheriff, county commission, and state legislature, as they matter more than the presidency or Senate for deep systemic change.
- Real leadership starts at the bottom, with positions like sheriff and state legislature being easier to win and requiring less money and influence.
- By filling these lower offices with real progressives who think similarly, they can eventually move up into positions of real leadership as the older establishment politicians age out.
- Supporting politicians in attainable positions who want deep systemic change is key to altering the overall view of the democratic establishment.
- The strategy involves a long fight and gradual change, revolution by degrees rather than immediate wins.
- Influencing elections through community networks can help get people in office who mirror your ideals.
- The approach requires patience, as real leadership and systemic change start at the bottom.

### Quotes

- "Voting is not an extremely effective means of civil engagement, civic engagement, but it is what it is."
- "Real leadership starts at the bottom, not at the top."
- "Revolution by degrees, I guess."
- "It's a long fight. It takes a long time, but it's something that will work."
- "Your community network, that can help influence an election."

### Oneliner

Thermostats and crosswalk buttons as metaphors for illusory control lead Beau to advocate for seizing the power vacuum in representative democracies and focusing on lower offices for deep systemic change within the Democratic Party.

### Audience

Progressive activists

### On-the-ground actions from transcript

- Organize and support real progressives running for lower offices like sheriff, county commission, state legislature (exemplified)
- Build a community network to influence elections and support candidates reflecting your ideals (exemplified)

### Whats missing in summary

The full transcript provides detailed insights on leveraging voting and focusing on attainable positions to drive deep systemic change within the Democratic Party.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about thermostats
and how to take over the Democratic Party.
Thermostats, like the one in your office building.
When you're talking about in big office buildings
and commercial places, the thermostat that you
as an employee have access to,
most times it doesn't do anything.
It's placebo, it's there to give you the illusion
of control, doesn't actually do anything though.
You move it from 70 to 80,
it doesn't actually make it 80 degrees.
Some of them are even so advanced
that they have white noise generators
that make you think the air conditioning has kicked on.
Crosswalks, a lot of crosswalks are the same way.
You push the button, it's not even connected to anything.
The crosswalk is just synced up
with the traffic signals for cars.
The illusion of control.
I got a message.
Beau, you, Chomsky, and a whole bunch of other people
seem to believe that voting is the least effective means
of civil engagement.
To be honest, you have convinced me of this.
However, none of you people who are attracted
to their mothers, not his term,
have ever laid out a plan to change that.
Could you, for one second, be Mick instead of Beau?
Pretend that you are advising people in some,
this is not a nice term, don't use this.
Pretend that you were advising people
in some developing world country
on how to realign their government without violence.
In short, how do we take over the Democratic Party?
Buddy.
Wow.
I like it, okay, mask up, buddy.
In the United States, in most representative democracies,
you have a naturally occurring power vacuum
that you could take advantage of.
Yeah, voting is not an extremely effective means
of civil engagement, civic engagement,
but it is what it is.
It's a tool that a lot of people can use.
This is transferable to other countries.
That power vacuum exists.
Exploit it.
Who ends up in the leadership
of the democratic establishment?
Those who have been there the longest.
Those with seniority, that's kind of how it works.
So for the moment, if you're a voter,
forget about the presidency,
forget about the Senate, really.
Sure, vote for harm reduction, whatever,
but those aren't as important as you might think.
I mean, they seem important right now,
but long-term, if you want deep systemic change,
it's gonna take a while.
And like anything else,
real leadership starts at the bottom, not at the top.
All those races that nobody really seems to care about much,
your sheriff, county commission, state legislature,
the House of Representatives,
those matter more than you think.
Those elections, they're easier to win,
they require less money, less influence.
They're attainable goals, right?
If you were to fill those slots
with people who are aligned with you,
real progressives who think the way you do,
eventually, as the Bidens and the Pelosi's,
when they age out,
those people get sucked up into positions of real leadership.
That's the way it works.
If you want deep systemic change,
you have to throw some meat into that vacuum
that is aligned with you.
Right now, there's a whole bunch of people
pinning their hopes on the Bernies and the AOCs,
and that's all fine and good.
But just like that conversation about social progressives,
they're the first ones.
They're the first ones, they're the advanced team,
and they're fighting a holding action
waiting for reinforcements.
If you don't give them some allies,
they'll never get into a position of real leadership.
They'll become combat ineffective
because they can't accomplish anything.
Eventually, they'll get voted out
because they can't accomplish anything.
You've got to focus on those lower offices.
Those are the ones that end up mattering
because they will slowly alter the overall view
of the democratic establishment.
Now, those thermostats, you're right.
They don't change anything.
However, in the more advanced systems,
if you move it from 70 to 80, it sends a signal.
It gets averaged with all the sensors and everything else.
It won't move it to 80 degrees,
but it might move it to 72.
Revolution by degrees, I guess.
I don't know that anybody's ever articulated this plan,
but go back and look at the politicians
I've had on this channel, the Perlmans, the Batars,
those real progressives who want deep systemic change
who are running for positions of attainable offices.
If you want to use this method, if you want to use voting,
that's how you do it.
Again, it's a long fight.
It takes a long time, but it's something that will work.
It's not as glamorous.
It's not an immediate win,
but real leadership starts at the bottom.
So your community network,
that can help influence an election.
And then that person, as long as they make it up
without being corrupted,
you can get people in office that reflect your ideals.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}