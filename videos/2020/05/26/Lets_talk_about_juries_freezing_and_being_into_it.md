---
title: Let's talk about juries, freezing, and being into it....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1tg8dmJ-sIc) |
| Published | 2020/05/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a message from someone in Australia about a friendly debate on fight, flight, or freeze.
- Feels obligated to talk about acute stress responses in relation to juries.
- Explains the three acute stress responses: fight, flight, and freeze.
- Freeze is often misunderstood, but it is part of the body's biological response to perceived threats.
- Freeze can occur when decision-making is overwhelmed or in extremely traumatic situations with no way out.
- Defense attorneys using the absence of evidence, like lack of physical resistance, to imply consent is wrong.
- Emphasizes that freezing in a traumatic situation does not imply consent or being "into it."
- Warns that succumbing to a biological response like freezing should not let attackers go free.
- Urges jury members to understand the nuances of acute stress responses in courtrooms.
- Stresses the importance of recognizing freezing as a normal biological response.

### Quotes

- "Freezing is normal. It's a biological response."
- "An absence of evidence does not imply that they were into it."
- "Succumbing to a biological response should not mean that the attacker goes free."
- "That is a bad faith argument that is apparently being made in courtrooms."
- "Because the prosecution may not always have the right witness to come in."

### Oneliner

Beau clarifies the nuances of freeze response in acute stress situations, cautioning against misconstruing biological reactions as consent, especially in legal contexts.

### Audience
Jury Members

### On-the-ground actions from transcript

- Educate jury members on the different acute stress responses and how they can manifest in traumatic situations (implied).
- Advocate for proper understanding and interpretation of freeze responses in legal settings (implied).

### Whats missing in summary
The full transcript provides a comprehensive explanation of freeze response and its implications in legal contexts. Viewing the full transcript will give a detailed understanding of acute stress responses and the importance of accurate interpretation in courtrooms.

### Tags
#Juries #AcuteStressResponses #LegalSystem #FreezeResponse #BiologicalReactions


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about juries
and freezing and apparently being into it.
We're gonna do this because I got a message
from somebody in Australia who was having
like a friendly debate and one of my videos
about fight, flight, or freeze,
I guess helped in the debate somehow.
There was something else in the message though
that I didn't know was occurring
and after hearing that it was,
I kind of feel obligated to talk about something
and clarify something about acute stress responses.
You know, most people, and if you're eligible
to serve on a jury, please watch this whole thing.
Most people are very familiar with fight or flight.
They know what that is, okay.
Freeze, not so much.
It's a little bit more ambiguous.
They all come from the same place.
You know, people chalk it up to,
oh well freeze means you're scared.
Well, so does fight or flight.
You've perceived a threat, a danger of some kind,
and you get a biological response.
It gives you adrenaline, it gives you pain inhibitors,
all kinds of stuff to help you if you have to fight.
To help you if you have to run, you know.
That's what it's there for.
Freeze is also part of this set of acute stress responses.
The people who really study this have three reasons
that freeze exists that I'm aware of.
There may be more.
I'll be honest, one of them I don't really buy,
but people who know a whole lot more about it than I do,
some of them believe it.
So maybe take my opinion with a grain of salt.
And that's that it's like playing possum.
Playing dead, hoping the threat will go away.
I don't actually think that's part of it,
but there are a whole lot of people who do,
and who, like that is their area of study.
The two that I'm certain of are a little different.
One is that basically your decision making process
is getting overwhelmed.
You've perceived this threat,
and there's so much information coming in,
you don't know if you can flee safely,
or if you can win the fight.
So it's fight or flight on pause.
You know, your body is preparing,
and trying to figure out what to do.
You're trying to process the information,
and you get stuck in an OODA loop.
Observe, orient, decide, act.
I just realized I've never done a video on OODA loops.
That's coming soon.
But, so you get stuck.
Your decision making process just breaks down.
It may actually be hampered by the amount of adrenaline,
because you're pulling in so much extra information
that you would normally discard.
And it makes making the decision harder.
Now that's one reason.
Another separate reason is if the event is traumatic,
and there really is no way out.
Not that you can't figure out what you want to do,
but you cannot flee safely,
and you cannot fight off the threat.
Something bad is gonna happen.
And you freeze.
You disassociate.
Accept your fate, kind of thing.
The pain inhibitors that allow you to ignore
the thorns cutting through your skin,
the thorns cutting your feet as you run, help there too.
Because something bad is gonna happen to you,
it helps dull that pain.
So in an extremely traumatic situation,
freezing is normal.
It's something that's gonna happen,
especially if there's no way out.
Or you can't perceive a way out.
Freezing is normal.
It's a biological response.
And yeah, you can train and train and train,
and you can help overcome it,
but you really can't control it.
You can help manage it,
but it's still going to happen,
even if you're very well trained.
Okay, so why am I talking about all of this?
Because in the message,
it was disclosed that there are defense attorneys
who are, well, she didn't have my client's skin
under her fingernails.
She didn't cry out.
And using this as a way to imply,
well, she was really into it
and just changed her mind later.
Or he was really into it and just changed his mind later.
That's wrong.
That's wrong.
An absence of evidence.
It does not imply that they were into it.
That's just wrong.
In fact, if they completely froze
because it was so traumatic,
because their client is guilty,
that's exactly what they would do.
They wouldn't have skin under their fingernails
because they wouldn't have fought back.
They wouldn't have cried out out of fear.
They would have disassociated.
I just think that's really important
because that is a bad faith argument
that is apparently being made in courtrooms.
That if you're gonna be on a jury, you need to know that.
That because the prosecution may not always have
the right witness to come in.
This is a biological response.
Succumbing to a biological response
should not mean that the attacker goes free.
Failing to fight back in a sufficient manner, I guess,
I don't even know what that is,
that could be simply succumbing to freeze
in a biological response that we are hardwired for.
So I felt obligated to put that information out there.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}