---
title: Let's talk about the light at the end of the tunnel....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xw0bApFnwJg) |
| Published | 2020/05/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of tunnels and addresses the overwhelming dramatic events happening currently.
- He mentions receiving messages from people wondering when things will improve.
- Beau talks about the common phrase "light at the end of the tunnel" and questions its effectiveness as a framing device.
- Using the example of societal progress on interracial dating, Beau illustrates how change can be imperceptible over time.
- He points out that despite challenges, humanity generally progresses forward on various issues.
- Beau encourages embracing ongoing societal improvement efforts without waiting for a definitive end point.
- He compares societal challenges to a moving train, suggesting that progress is continuous and requires constant effort.
- Beau underscores the need for continuous improvement even if major issues like poverty or climate change were miraculously solved.
- He acknowledges that there will always be new battles to fight and improvements to make.
- Beau concludes by urging people to keep moving forward and embracing the journey of societal progress.

### Quotes

- "There is no tunnel."
- "Be proud you're part of it."
- "Don't look for that journey, that advancement to end."
- "Just understand, you're never going to get there."
- "We are planting shade trees. We will never sit under."

### Oneliner

Be proud of being part of continuous societal progress; there is no definitive end point in the journey towards improvement.

### Audience

Individuals advocating for societal progress

### On-the-ground actions from transcript

- Embrace ongoing societal improvement efforts (implied)
- Keep moving forward in the fight for progress (implied)

### Whats missing in summary

Beau's engaging and thought-provoking delivery

### Tags

#SocietalProgress #ContinuousImprovement #EmbraceTheJourney #Activism #Humanity


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about tunnels.
There's a lot of dramatic stuff going on right now.
A lot of it is overwhelming.
That is on full display in my inbox.
A lot of the messages that carried that theme, people wondering when it's going to get better,
and more than a couple of people used the phrase, light at the end of the tunnel.
And that's a way to look at it.
We're in a tunnel, it's dark, eventually we're going to get to that end point.
I don't know that that's a good way to frame it.
I think it may be self-defeating.
I don't even know that it's accurate, because we come out of those tunnels all the time.
On certain issues, we reach the light at the end of the tunnel, but we don't...
It happens so slowly that it's imperceptible.
You won't be able to do this if you're younger, but think of something that's changed within
your life.
For me, growing up, white and black dating, still pretty taboo.
Not something that happened very often.
And those that did it, well, they got side-eyed.
Today, not really an issue, is it?
It's not an issue.
Not a big one.
At some point in my life, that changed.
Can't tell you the month, can't tell you the year, but at some point on that issue, we
reached a light at the end of the tunnel.
Don't know when, though.
And that happens all the time.
All the time.
You think of most issues today, as dark as the tunnel may seem, compare them to 500 years
ago, 400, 300, 200, 50, 20.
Most issues are better.
Most issues are better.
Because that's who we are as a species.
What humanity is, we drive forward.
Most of us.
There are those regressives that have to be drug kicking and screaming into the future.
But take pride that you're one of those dragging them.
If you're waiting to get to the light at the end of the tunnel, you're never going to get
there.
You're never going to get there.
If we're going to stick with this analogy, the light, it's a train, and it's moving away
from you just as fast as you go.
You will never get there, because that's who we are as a species.
Let's say tomorrow poverty ends.
It's over.
All over the world.
Everybody's out of poverty.
What happens?
We just sit down and chill?
No, of course not.
We have to raise everybody's standard of living.
If everybody has health care tomorrow, then what?
Is the problem solved?
No.
We need new treatments.
We need to become more efficient.
Climate change issues solved tomorrow.
Then what?
We have to undo the damage that was already done.
And then, we're done with that.
We should make another planet habitable.
It's who we are.
We will drive forward.
There is no light at the end of the tunnel, because there is no tunnel.
It's not what it is.
It's just us.
It's humanity.
We're moving forward.
And it takes time.
It takes time.
And there are those people who try to drag us back.
And we've got them by the ankle.
And their fingernails are just dug into the ground.
And we're pulling them along.
That's where we're at, though.
We can sit here and end each other over skin tone, language, what books we read.
We can make Mars habitable.
There will always be another fight.
There will always be something that needs improvement.
Don't wait for the end of the tunnel.
Don't look for that.
Don't look for that journey, that advancement to end.
Be proud you're part of it.
Embrace the fight, because it's going to go on your entire life.
The issues that we have today, as soon as they're solved, there will be other things
that we want to fix.
And that's good.
That's good.
That's how it should be.
There is no tunnel.
At times it seems dark.
And at times it is.
But it's just us moving forward.
It's us as a species.
The fact that you want to get to the light at the end of the tunnel means you're moving
the right direction.
You want to get out of the darkness.
You want to propel us into a society that is truly beneficial for everyone.
Just understand, you're never going to get there.
And that's good.
It's not a bad thing.
We will always be improving.
There will always be another battle.
It will go on your entire life.
We are planting shade trees.
We will never sit under.
And it's great.
Don't get down.
Just keep moving.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}