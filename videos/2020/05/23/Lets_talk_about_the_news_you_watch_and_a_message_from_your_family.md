---
title: Let's talk about the news you watch and a message from your family....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9bkmEKXOHTE) |
| Published | 2020/05/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urges viewers to watch the entire video if it was sent to them, especially if it seems directed at them.
- Expresses frustration at receiving messages from young people asking how to communicate with family members influenced by biased media.
- Notes the divisive tactics employed by political outlets to drive a wedge between generations, especially in the lead-up to elections.
- Advises viewers to stop consuming biased information for 28 days to gain new perspectives and reduce anger.
- Suggests reputable news sources like Reuters, AP, NGOs, and Human Rights Watch for more balanced reporting.
- Warns against believing opinions are equivalent to facts and consuming sources that fuel anger and division.
- Criticizes news outlets that prioritize commentary over objective reporting and manipulate viewers with leading questions.
- Emphasizes the importance of choosing between family harmony and political affiliations.
- Condemns the impact of biased media on American families and the perpetuation of anger and division.
- Encourages viewers to break the cycle of consuming divisive information and observe positive changes in their outlook.

### Quotes

- "Stop consuming that information for twenty eight days. Just stop."
- "This habit that is forming is literally destroying American families."
- "You're destroying your families for the sake of somebody who reveled in the fact that he was supported by the uneducated."
- "You need to make a choice about what's more important to you. Your family or Red Hat?"
- "Y'all have a good day."

### Oneliner

Beau urges viewers to take a break from biased media consumption for 28 days to mend family divides and foster new perspectives.

### Audience

Family members influenced by biased media

### On-the-ground actions from transcript

- Stop consuming biased information for 28 days (suggested)
- Seek news from reputable sources like Reuters, AP, NGOs, and Human Rights Watch (suggested)

### Whats missing in summary

The full video provides deeper insights into the damaging effects of biased media consumption on families and society, advocating for a conscious choice between divisive politics and family unity.

### Tags

#MediaConsumption #FamilyUnity #PoliticalBias #ReputableSources #CommunityHarmony


## Transcript
Well, howdy there, Internet people. It's Beau again.
So, let me start off by saying
that if somebody has sent you this video,
watch the whole thing.
If somebody tweeted it to you, sent it to you over social media,
put it on their feed in hopes that you may see it so they don't have to have a
confrontation.
Watch the whole thing.
If this feels like it's directed to you,
it probably is.
This video is being made
because I am so tired
of getting messages from your kids, in most cases,
asking how to reach out to you.
I get at least two messages a week
that can be summed up with
News Outlet X
has torn my family apart.
I don't even know my uncle, father, mother, grandfather anymore.
They've fallen down
this echo chamber.
It's a very common thing
and it's going to become more
intense
the closer we get to the election
because
the president
did not get a majority of votes
until you reach age fifty and older.
It is in the interest
of outlets
that are political in nature,
that have a political bias,
that support the president
to drive a wedge between you and your family.
They're not looking at it that way.
They're looking at it
as appealing to
their demographic
and
playing up the generation gap
so that they can secure
those voters
for the president.
Stop consuming that information for twenty eight days.
Just stop.
You know the outlets I'm talking about,
those that provide two minutes of description of an event
and then fifteen minutes of commentary to make sure that you think the right way about it.
Those that will
ask rhetorical questions
in a leading manner
to get you
to believe something that
may or may not be true.
Those outlets that
maybe they can't even call themselves news
in some countries.
We've hit the point in the United States where people
believe that their opinion
is as good as a fact
and it's not.
It isn't.
This
habit
that is forming
is literally destroying American families,
which is funny coming from the Family Values Party.
It's happening. Now I'm not saying go watch CNN.
You know, I get it, Clinton News Network and all of that.
I understand.
That's fine.
Reuters,
AP,
stuff like that.
Don't like them? Go to some NGOs. Go to some non-government organizations.
If you want to keep up on international news,
the Human Rights Watch is a really good one.
Yeah, it's all framed through
examination of human rights issues.
However,
the background they provide
on current events is fantastic.
Steer clear. I mean, even use the BBC.
Fine. Give you something, right?
Can't go cold turkey.
Use the BBC.
But drop the rest of it.
Just for twenty-eight days
and see if things don't change.
If you don't look at the world in a different way.
If you're not constantly angry.
If you're not
just filled
with rage
at people you've never met
and who have done nothing to you.
And I know,
I mean, watch those outlets
because they're the ones that tell the truth.
They're the only ones that tell the truth. Anybody who claims to have a monopoly on the truth
is a con man.
You're destroying your families
for the sake of some
body who
reveled in the fact
that he was supported by the uneducated.
Eventually
this president will leave office.
But all of the damage that he's done
on the national level and the international level that you don't get to hear about
because you're in those echo chambers
and the damage he's done to your family,
it's still going to be there.
I would love
to go a week
without getting a message like that
because it is horrifying.
And I would suggest that it is way more
than just the normal
shift
that occurs between generations.
The younger generations are always more liberal, you
were more liberal than your parents more than likely.
They embrace new ideas faster.
The problem is
a lot of the demographics that are watching these channels and consuming
these outlets,
they're not staying put
with their ideas. They're going backwards.
So that gap widens.
You need to make a choice
about what's more important to you.
Your family
or Red Hat?
The stupid slogan.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}