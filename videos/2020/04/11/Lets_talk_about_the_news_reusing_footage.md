---
title: Let's talk about the news reusing footage....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QHFTbZj66Ik) |
| Published | 2020/04/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the cottage industry within news media and journalism involving the use of stock photos and footage.
- Describes his experience as a photojournalist selling photos to news outlets.
- Mentions that media outlets rely heavily on freelancers for visual content.
- Addresses the misconception that using stock footage means an event didn't happen.
- Points out that major news networks use old footage to fill visual space, not as evidence of a conspiracy.
- Suggests that labeling stock or file footage is an ethical practice that is often overlooked.
- Notes that some people misconstrue laziness in news management as evidence of fake news.
- Acknowledges the consumer demand for immediacy driving the use of stock footage.
- Encourages freelancers to push for ethical standards in labeling their work.
- Concludes by reminding viewers to understand how news media operates.

### Quotes

- "It's not evidence of a conspiracy. It's evidence of the consumer's desire to constantly have visual input."
- "This isn't evidence of a conspiracy. It's not. This is evidence of bad news management."
- "It's not true. But that's not the case. It just means that your news outlet's lazy."
- "Try to hold these major outlets to an ethical standard."
- "That alone is not evidence of anything other than a misunderstanding about how news media works."

### Oneliner

Beau explains the use of stock photos and footage in news media, dispelling misconceptions and advocating for ethical standards in labeling.

### Audience

News Consumers

### On-the-ground actions from transcript

- Push for news outlets to ethically label stock or file footage (suggested).
- Hold major news networks accountable to ethical standards in visual content usage (exemplified).

### Whats missing in summary

The full transcript provides a comprehensive breakdown of how the news media industry uses stock footage and photos, urging viewers to be discerning and informed consumers of news.

### Tags

#NewsMedia #StockFootage #Journalism #EthicalStandards #Misconceptions


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about a cottage industry
that exists within the news media, within journalism.
It's an industry that I'm sure most people know exists,
but they may not understand how it works.
And because they don't understand how it works,
sometimes they add two and two and come up with 17.
So we're going to talk about it.
This was an industry I was a part of
for a few years.
For those that don't know, I was a journalist.
I started as a photojournalist.
This applies to not just photojournalists,
but also videographers.
This same scenario.
It's done a little bit different, but not much.
So I would go out to an event or whatever,
a newsworthy event, and I would take photos.
And then those photos would be sold or licensed
to various agencies, news outlets.
And they would use them.
Now, I think everybody knows this happens.
What you may not know is that I haven't photographed
a protest involving the group Anonymous in, I don't know,
five years.
They're still using the photos.
File photos, stock photography, file footage,
it's incredibly common.
It's used all the time.
However, there are those people who see that.
And they say, well, that photo actually happened in DC.
And they're talking about a protest that happened in LA.
And for some reason, they make the assumption
that that means the LA protest didn't happen.
It's not how it works.
Media outlets today don't have bureaus the way they used to.
They don't have little offices all over the world.
They count on freelancers.
And if they can't get a freelancer there,
if somebody doesn't show up and then offer to sell them
the photos, they use stock footage.
They use stock photography.
It's not evidence of a conspiracy.
It's evidence of the consumer's desire
to constantly have visual input.
When you're talking about footage,
now this is not an industry I had anything to do with.
But Ford Fisher from News to Share,
this is how he makes his living.
We've interviewed him on this channel.
The, when you're talking about news networks,
major news networks on TV, they have
to fill 24 hours of visual space.
You can only look at somebody talking for so long.
You need that other input.
So they will use old footage to talk about a current event.
This isn't evidence of a conspiracy.
It's not.
This is evidence of bad news management.
It should be labeled.
It should be labeled.
Ethically, it should be labeled as stock or file.
But it isn't always, because we have that desire
for immediacy, that immediacy drives it.
Sometimes it gets left out.
This isn't a huge thing.
This is an industry that has existed
for a very, very long time.
It used to be more, agencies used to be more consistent.
News outlets used to be more consistent with labeling it
as file or stock.
They don't do it as much anymore,
because they assume the consumer knows that that's the case.
It's something that people should keep in mind,
because I'm seeing a lot of, let's just say,
theories that are centered on this fact.
People are saying, well, this hospital isn't overrun
in Wichita or wherever.
Yeah, but the footage showed that it was.
The footage is clearly from New York,
but that's the footage they showed when they were
talking about a different city.
So therefore, it's fake.
It's not true.
But that's not the case.
It just means that your news outlet's lazy.
They didn't label it.
They didn't go film it for themselves,
because it's a whole lot cheaper to buy a photo or buy footage
than it is to send out your own crew.
A lot of these news outlets don't even
have their own crews anymore.
They count on the freelancers.
They count on people that are just out there doing it.
I would suggest that freelancers,
and I can't really say too much about this,
because I only did it one out of the years
that I was doing it, the very last one,
put it in their license that it gets labeled.
Try to hold these major outlets to an ethical standard.
But that's rare.
And again, I only did it the last year.
So it's not uncommon for this to happen.
That alone is not evidence of anything
other than a misunderstanding about how news media works.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}