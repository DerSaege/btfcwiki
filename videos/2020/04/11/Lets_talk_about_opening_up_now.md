---
title: Let's talk about opening up now....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gm6lnVCVFZM) |
| Published | 2020/04/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People are advocating to open the U.S. back up despite numbers indicating it's a bad idea.
- Loyalty to the president is chosen over the well-being of neighbors.
- Authoritarian measures by local governments wouldn't be necessary if there was proper leadership.
- Lack of leadership from the president results in confusion among the American people.
- Understanding basic science and history should guide individuals to take necessary precautions without police intervention.
- Referencing the 1918 pandemic, Beau warns against easing restrictions too soon.
- Advocating to reopen for economic benefit disregards risking friends and family.
- The economic impact on the 99% should not be overlooked for the benefit of the 1%.
- Lack of financial support from the government could lead to a collapse of the entire economic system.
- Beau urges people to stop listening to the president and prioritize safety over economic interests.

### Quotes

- "You shouldn't need a cop for this. You don't need a cop. You need a science book and a history book."
- "Easing up early now is wrong. It's the wrong move. We're winning, but we haven't won."
- "If you think opening the US right now, opening everything back up, going back to business as usual is a good idea, you need to get a science book."
- "He wants to be a leader. Then he has to lead."
- "We have to do the bare minimum to keep each other safe and not sell each other out because our betters have told us to die for their profits."

### Oneliner

Advocate for science, history, and community safety over premature reopening for economic gain, prioritizing collective well-being.

### Audience

Community members, advocates

### On-the-ground actions from transcript

- Stay at home, wash hands, avoid touching face to keep each other safe (exemplified)
- Advocate for financial support from the government to prevent economic collapse (suggested)
- Educate others on the importance of science and history in making informed decisions (implied)

### Whats missing in summary

In watching the full transcript, the emotional impact and urgency conveyed by Beau's message are best captured.

### Tags

#CommunitySafety #EconomicImpact #Leadership #Science #History


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, it's the weekend.
It's the weekend the president said he wanted to open everything up.
And like clockwork, there are people out there stroking his ego, showing that dear leader
can't be wrong, advocating to open the U.S. back up right now, when all, all numbers suggest
that's a really, really bad idea.
But that's what it takes right now.
You have to show your loyalty to the man in the red hat, rather than to your neighbor.
There are people who are upset with the authoritarian measures that some local governments are taking.
I get it.
I get it.
Those shouldn't be happening.
Those shouldn't be happening because they shouldn't be necessary.
The reason they're happening is because the president has shown no leadership.
He's holding press conferences that last hours and still hasn't educated the American people
on the basic facts that they need.
If you understood the science and the history behind this, you wouldn't need a cop to tell
you to stay home.
You wouldn't need a cop to tell you to take the minor precautions.
The minimum stuff you can do, you would know it's a good idea on your own.
You shouldn't need a cop for this.
You don't need a cop.
You need a science book and a history book.
The science, it's actually really easy, but people get weird when you start talking about
science.
So we're just going to go to history.
1918 was the last time we faced something like this.
And just like now, there were people who wanted to ease up early, before we had won.
I want you to go back and look at the spikes that occurred in those locations that eased
up early right afterward.
Right after they did it.
History is a very powerful teacher, if you listen.
Easing up early now is wrong.
It's the wrong move.
We're winning, but we haven't won.
This is working.
It's working far better than anybody expected.
But we have to keep it up.
We have to keep it up.
And meanwhile, you have those who have just written off a bunch of people.
We're doing all of this for 1% of the population, and we're going through all of this trouble?
I feel that way about the whole system.
You are advocating to put your friends and your family at risk for the economic benefit
of 1%.
I know, those of us down here on the bottom, us peons, us commoners, we have our own economic
issues that we have to deal with.
Fact.
But as you worry about how you're going to pay your rent, your landlord's worried about
how he's going to pay his mortgage.
As you worry about not getting your paycheck, the owner of that company, she's worrying
about not having any profits.
That 1%, they have political connections.
They have political clout.
And they are going to understand that if in a consumer-driven economy, the consumers don't
have any money, it all comes crashing down around their ears.
The last stimulus was a joke.
It was ineffective.
If the government does not put money in people's hands now, the whole system comes crashing
down around their ears.
If you think opening the US right now, opening everything back up, going back to business
as usual is a good idea, you need to get a science book.
You need to get a history book and you need to read.
You need to stop listening to the guy in the red hat because he cares about his ego and
nothing else.
He does not care about you.
He never did.
The only reason he took action was because he realized that at some point, the numbers
of those who didn't make it were going to impact his approval ratings.
That's it.
That's all he cares about.
If he cared about you, he'd be holding those press conferences, educating the American
people on how to keep each other safe.
He wants to be a leader.
Then he has to lead.
He has to set the example, but he's not doing that.
If we had real leaders, real people who would advise and influence the populace, the authoritarian
measures wouldn't be happening.
We're going to talk more about those because there are some that are being advocated that
are wrong.
Right now, what matters is we have to stay at home.
We have to wash our hands.
We have to not touch our face.
We have to do the bare minimum to keep each other safe and not sell each other out because
our betters have told us to die for their profits.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}