---
title: Let's talk about clarifying the President's authority....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Z_ykY_FSe00) |
| Published | 2020/04/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The president provided inaccurate information about how the government functions.
- Operating under the assumption that the president is wrong when speaking about basic civics is safe.
- The president's tweets about reopening states were incorrect; governors have authority in their states.
- The federal government can remove federal guidance without consulting anyone.
- Local authorities, like county commissions or city councils, have more authority than the president in certain matters.
- The idea of a nationwide reopening is not realistic; there will be rolling changes and areas under stay-at-home orders.
- Social distancing should still be in effect even if stay-at-home orders are lifted.
- Reopening cities may lead to spikes in cases and a return to stay-at-home orders.
- It's vital to understand that the situation may last a year and a half until there's a vaccine or effective treatment.
- Precautions like wearing masks, washing hands, and minimizing trips are still necessary.

### Quotes

- "Operate under the assumption that whatever he says, the exact opposite is true."
- "Your local county commission or your local mayor, your local city council has more authority than the president."
- "There's not gonna be this grand, overwhelming victory where we all just march out into the streets and hold a parade."
- "You're not just protecting yourself, you're protecting others."
- "It's probably gonna last a year and a half, maybe, until there's a vaccine, or at least until there's a really solid treatment."

### Oneliner

Operating under the assumption that the president is wrong about government functions is safe, and local authorities hold more power than the president; expect rolling changes and continued precautions as the situation may last a year and a half.

### Audience

Citizens, Local Officials

### On-the-ground actions from transcript

- Keep practicing social distancing and wearing masks (implied).
- Continue to wash hands frequently and avoid touching your face (implied).
- Minimize trips outside and follow local health guidelines (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the inaccuracies in the president's statements regarding government functions and offers insights into the long-term implications and precautions necessary during the ongoing situation.

### Tags

#GovernmentFunctions #PresidentialStatements #COVID19 #Precautions #LocalAuthorities


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're gonna talk about clarifying the president
because the president took to Twitter
to provide Americans with accurate information
about how their government functions.
And it needs to be clarified because he was wrong.
Look, I understand it's trying times, okay?
And I understand you're looking for information.
When the president began speaking or tweeting
about how the government itself functions, basic civics,
just operate under the assumption that he's wrong.
Operate under the assumption that whatever he says,
the exact opposite is true.
If you do that, you'll be safe
because he does not understand the basic concepts
of the constitution, the basic concepts of our government.
He really doesn't.
Okay, so what did he tweet in his first tweet?
Because he took up two to be this wrong.
He said, for the purpose of creating conflict and confusion,
some in the fake news media are saying
that it is the governor's decision to open up the states,
not that of the president of the United States
and the federal government.
Let it be fully understood that this is incorrect.
No, it's not.
Okay, second tweet.
It is the decision of the president
and for many good reasons.
With that being said, the administration
and I are working closely with the governors
and this will continue.
A decision by me in conjunction with the governors
and input from others will be made shortly.
Okay, no, that's not how this works.
It's not how any of this works.
That's not how the United States works.
It's not how it's ever worked.
The president can remove any federal guidance,
any federal orders that he has enacted
without consulting anybody.
He has that authority.
However, if tomorrow the president says,
stay at home orders are gone, they don't exist anymore.
And the governor of New York says, yeah, they do.
They exist in New York.
We have a federal system.
That's how it works.
To take it a step further,
to truly undermine his self-importance
and to demonstrate how little authority he actually has
in this, if the president says it doesn't exist
and my county commission in my small little county
where I live says that there's a stay at home order,
there is in this county.
When it comes to this, your local county commission
or your local mayor, your local city council
has more authority than the president.
That's how this works.
This entire diatribe is, well, to use his words,
let it be fully understood that this is incorrect.
I know it's easy to pick on Trump
with his limited understanding of the subject matter at hand
and his limited understanding
of how the government functions.
But this is a worry for me
because I wanna get people's mind around the fact
that there is not gonna be a reopen from sea to shining sea.
That's not gonna happen.
That's not in the cards.
The economy is not gonna come back with a bang.
Even once there is widespread return to normalcy,
there are still gonna be areas
that are under stay at home order
and it's gonna be rolling.
It's gonna change locations.
The idea behind the stay at home orders
was to make sure that we could flatten the curve,
that we didn't overwhelm the medical infrastructure.
That was the whole point.
Once we get it to where we're below that
and we can keep the spread below that level,
then it's okay to return to normal kind of.
Now, if you're at risk,
you still need to take a whole lot of precautions.
The stay at home order may be gone,
but social distancing should certainly
still remain in effect.
And then what's gonna happen is,
let's say, for example, Tupelo,
pick a city, doesn't matter.
They reopen on this limited basis.
And then it starts to get out of control there.
And it can, once again,
overwhelm the medical infrastructure.
That curve starts to climb.
They go back to a stay at home order.
That's how this is gonna work.
There's not gonna be this grand,
you know, overwhelming victory
where we all just march out into the streets
and hold a parade.
That's proven to be a really bad idea historically
when you're dealing with something like this.
See Philadelphia.
We need to wrap our minds around that.
We need to get ready for a long haul.
This is probably gonna last a year and a half, maybe,
until there's a vaccine,
or at least until there's a really solid treatment.
There are a couple of treatments out there
that are showing real promise,
not the one the president keeps touting.
Um, but we'll get there.
And understand we have, in many areas,
successfully flattened the curve.
So you can start to expect to see
a little bit of a return to normal.
But social distancing is still gonna be in effect.
You should still wear a mask.
You should still wash your hands and not touch your face.
You should still keep your trips to a minimum.
You're not just protecting yourself,
you're protecting others.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}