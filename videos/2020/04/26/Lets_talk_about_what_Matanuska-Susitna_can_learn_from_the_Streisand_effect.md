---
title: Let's talk about what Matanuska-Susitna can learn from the Streisand effect....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1wQy2YbsRok) |
| Published | 2020/04/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Barbara Streisand's situation serves as a lesson for the Matanuska Susitna School District in Alaska.
- Streisand fought to keep photos of her house private, which only piqued people's curiosity.
- Beau was asked for reading recommendations on a livestream and suggests books that the school district has banned.
- The school district banned teaching books like The Great Gatsby, Invisible Man, Catch-22, The Things They Carried, and I Know Why the Caged Bird Sings due to uncomfortable themes.
- Beau argues that literature with uncomfortable themes helps individuals distinguish facts from fiction and think critically.
- Exposure to such literature can prevent uninformed statements from people in power, like Lysol's recent statement.
- Beau stresses the importance of preparing students for life's discomforts through literature.
- Complaints about the banned books often stem from religious groups.
- Beau challenges the censorship, pointing out that uncomfortable themes exist in the Bible as well.
- He warns that once censorship starts, more books will follow.
- Beau offers support to librarians in the district who may need additional copies of the banned books.
- He encourages everyone to read these banned books, stating that they are significant in understanding alternative facts and censorship.
- Fiction, according to Beau, often leads to truth, making banned books invaluable.
- Beau concludes by encouraging his audience to have a good day.

### Quotes

- "Banned books are the best books."
- "Fiction is what gets you to truth."
- "Literature like this that covers uncomfortable themes, it creates a population that can distinguish facts from fiction."
- "Your book is next. The way it always works."
- "To make you a better person."

### Oneliner

Barbara Streisand's privacy fight teaches a lesson; banned books are vital for critical thinking and truth, challenging censorship.

### Audience

Educators, librarians, book lovers

### On-the-ground actions from transcript

- Support librarians in the school district by providing additional copies of banned books (suggested)
- Reach out via Twitter, Facebook, or email to assist in ensuring the availability of necessary texts (suggested)

### Whats missing in summary

The emotional impact of censorship on education and critical thinking is best experienced through the full transcript.


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about what Barbara Streisand can teach the Matanuska Susitna
School District in Alaska.
There were photos taken of her house.
She didn't want them public.
So she fought to keep them quiet.
All that did was make people curious about them.
Increased interest in them.
Made more people want to see them.
Last night on a live stream, I was asked a question I get asked a lot.
I was asked for reading recommendations.
I never have a good answer.
Because I read a lot and I think most books have something in them that can enrich your
life.
That's the purpose of literature.
To expose you to different things.
Got a list now, thanks to that school district.
If you have not read The Great Gatsby, Invisible Man, Catch-22, The Things They Carried, or
I Know Why the Caged Bird Sings, please start there.
They are good books.
All of them.
This school district has banned instruction of them.
Can't be taught about them in their schools.
Because they have uncomfortable themes.
Yeah they do.
They do.
That's the purpose of literature.
To expose you to different things.
To make you a better person.
Arguably that's the job of the school too.
Literature like this that covers uncomfortable themes, it creates a population that can distinguish
facts from fiction.
That can critically think for themselves.
If we had a population who had been exposed to literature like this, read it, took it
to heart, had some instruction on it, I'm willing to bet that Lysol wouldn't have to
make a statement because some self-important ignorant government entity said something.
Said something they know nothing about.
It is interesting to note that apparently some of the school board members who made
this decision haven't even read some of the books.
But they have uncomfortable themes.
Life is uncomfortable.
I would suggest that it would be wise to prepare students for it before they see it first hand.
Allow them to experience it safely in a classroom setting, in a book, inside that four inch
space inside their skull.
Before they see it with their own eyes.
That's kind of the school's job, to educate, to inform.
Now they got complaints, they got challenges.
These complaints typically come from those of a certain religious bent.
I challenge anyone to present an uncomfortable theme that is present in one of these five
books that is not also in the Bible.
Should that be the next one censored?
That's how this works.
When you open the door to this kind of censorship, your book is next.
Your book is next.
The way it always works.
That's not slippery slope, it is cause and effect.
You have set a precedent for banning material based on these themes.
All of which are present in the Bible.
Given the fact that the students in this school district are going to hear that the school
board doesn't want them reading this, I would suggest that it's possible librarians at these
schools are going to have a run on these books.
I understand that the administration may not be willing to purchase additional copies because
of what the district has said.
If you are a librarian in this school district and you need additional copies of any of these
books, I am 100% certain the people from last night's live stream would have no issue with
the money from that live stream going to make sure that those texts that you need magically
appear.
If you are a librarian and you need these books, reach out, Twitter, Facebook, email,
I don't care how.
Get me a message where they need to be.
They'll be there.
And I would strongly suggest everybody read these.
These are all really good.
These are all important works.
These are all things that everybody needs to understand.
Especially those who are entering a world of alternative facts.
Entering a world where government entities are trying to censor truth.
And yeah, this is fiction.
But a lot of times fiction is what gets you to truth.
Banned books are the best books.
They always will be.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}