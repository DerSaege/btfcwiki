# All videos from April, 2020
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2020-04-30: Let's talk about a question from my 5-year-old and a construction project.... (<a href="https://youtube.com/watch?v=2NV-Sihjwvw">watch</a> || <a href="/videos/2020/04/30/Lets_talk_about_a_question_from_my_5-year-old_and_a_construction_project">transcript &amp; editable summary</a>)

Beau shares his 5-year-old's unexpected question about building a wall to memorialize the current crisis, criticizing political elites and stressing the need for informed decision-making and personal agency amidst a lack of effective leadership.

</summary>

"If you are in public office, your words have consequences."
"You have to lead yourself. You have to make a decision on what you're going to do."
"Nobody can make it for you. Nobody can advise you on it."
"This needs to be built. And it needs to be tied to the leadership."
"You can't run a country on hope. On a guess."

### AI summary (High error rate! Edit errors on video page)

Beau shares a surprising question from his 5-year-old about building a wall, inspired by a visit to the Vietnam memorial in DC.
The idea of building a wall to memorialize the lives lost during the current crisis is discussed, with inscriptions of quotes from leaders.
The narrative criticizes political elites for their disconnectedness from the struggles of ordinary workers, especially in vital jobs like meatpacking.
There's a call for a monument tied to leadership to serve as a reminder of the consequences of their decisions, particularly in handling the pandemic.
The importance of basing decisions on reliable information rather than hope or personal desires is emphasized.
Criticism is directed at how economic considerations have influenced decision-making, benefiting the wealthy at the expense of the average worker.
Beau stresses the need for individual agency and decision-making in the absence of effective leadership during the crisis.
The discourse touches on the profit motives behind certain actions and products during this challenging time.

Actions:

for citizens, decision-makers,
Build memorials tied to leadership to remind them of the consequences of their decisions (suggested)
Base decisions on reliable information rather than hope or personal desires (implied)
Support policies that prioritize the well-being of workers over economic considerations (implied)
</details>
<details>
<summary>
2020-04-29: Let's talk about what a naming tradition can teach us about listening.... (<a href="https://youtube.com/watch?v=tRKzhKrxIIg">watch</a> || <a href="/videos/2020/04/29/Lets_talk_about_what_a_naming_tradition_can_teach_us_about_listening">transcript &amp; editable summary</a>)

Understanding naming traditions in the military sheds light on honoring rather than insulting, urging consideration of intent and perspectives in debates.

</summary>

"Intent is really important in life."
"Was never meant to be an insult."
"We may be closer to a resolution than we imagine if we actually listen while we talk."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of intent in life and how it should be measured, even though the effect is usually more significant.
Talks about the naming traditions in the US military for ships, tanks, aircraft, drones, and helicopters, and how they are usually named after things that embody importance or honor.
Addresses the naming tradition of helicopters in the Army, generally named after native groups or leaders, and clarifies that it was never meant to mock natives.
Shares the historical context behind naming helicopters after native groups, like the Sioux, and how it was meant to acknowledge their speed, surprise, and violence of action from horseback.
Emphasizes that the naming traditions are intended as a huge honor when it comes to people, not as insults.
Mentions that not all natives are against this naming tradition and that some see it as a significant honor.
Suggests that the opinion on whether this tradition should continue should be left to those impacted by it.
Encourages understanding the perspective of others in a debate or discussion and listening while talking to potentially reach a resolution.

Actions:

for military members, activists, historians,
Respect and honor the perspectives of those impacted by naming traditions in the military (implied)
Engage in open, respectful dialogues to understand differing viewpoints (implied)
</details>
<details>
<summary>
2020-04-29: Let's talk about a supply chain talking point.... (<a href="https://youtube.com/watch?v=LxTmhbFLoYA">watch</a> || <a href="/videos/2020/04/29/Lets_talk_about_a_supply_chain_talking_point">transcript &amp; editable summary</a>)

Beau debunks the absurdity of isolationist supply chain fixes, advocating for decentralization and community resilience over nationalist ideologies.

</summary>

"If you have one, you have none."
"A nationalist is just a low ambition globalist."
"We can't rely on governments to make sure that we're all okay."
"It's present in a lot of other videos but I've never really gone into it in this way."
"It's very clear that we haven't learned anything from this."

### AI summary (High error rate! Edit errors on video page)

Criticizes the idea of bringing all production back home to fix the supply chain, labeling it absurd and a talking point pushed by those in power.
Explains that the current supply chain issues are due to global problems and increased demand, not evil foreigners as suggested by the talking point.
Argues that decentralization and distributed networks usually improve systems, pointing out the flaws in isolationist approaches.
Notes that localized supply chains can lead to vulnerabilities, as seen in the struggling meat packing industry with ripple effects on ranchers.
Mentions the predicted shortages that have now occurred due to lack of preparation and blames the need for scapegoats on those in power.
Compares globalists who want one jurisdiction for the world to nationalist governments, stating that the difference is only in scale.
Advocates for redundancy, decentralization, and distribution of networks as beneficial, contrasting this with the weakening effects of isolationism and nationalism.
Emphasizes the importance of strong community networks over relying on state and federal governments for protection.
Concludes by warning that not learning from past mistakes will result in more vulnerabilities and situations like the current supply chain issues.

Actions:

for community members, activists,
Build strong community networks for resilience (implied)
Advocate for decentralized systems in your community (implied)
Challenge nationalist ideologies with facts and logic (implied)
</details>
<details>
<summary>
2020-04-28: Let's talk about why you have insurance and a savings account.... (<a href="https://youtube.com/watch?v=6r3cg3QZ_go">watch</a> || <a href="/videos/2020/04/28/Lets_talk_about_why_you_have_insurance_and_a_savings_account">transcript &amp; editable summary</a>)

Beau reframes emergency preparedness as insurance and a savings account, urging preparation for extreme scenarios while criticizing the government's emergency response.

</summary>

"It's a savings account of tangible assets that you use in your daily life."
"You don't have to deal with the effects of whatever the emergency is in most cases. You're having to deal with a bunch of panicked people."
"Being prepared can only help."
"Their track record for dealing with emergencies is not good."
"You have to lead yourself."

### AI summary (High error rate! Edit errors on video page)

Reframes emergency preparedness as insurance and a savings account.
Urges to prepare for extreme scenarios as it also prepares for more likely ones.
Compares emergency preparedness to having homeowner's insurance.
Suggests that being prepared can help in unforeseen circumstances like job loss.
Points out the common occurrence of disasters in the United States.
Emphasizes the peace of mind that preparedness brings.
Encourages preparedness to stay ahead of panic buying and panicked people during emergencies.
Uses a mountain lion analogy to illustrate staying one step ahead.
Mentions the importance of having 30 days' worth of food for emergencies.
Compares preparing for emergencies to putting on your mask first on an airplane before helping others.
Criticizes the government's track record in dealing with emergencies.
Advocates for being self-reliant in times of emergencies.

Actions:

for preppers and community-minded individuals.,
Stock up on 30 days' worth of food and essentials to prepare for emergencies (exemplified).
Create a bug-out bag with necessary supplies for potential evacuations (exemplified).
Take the initiative to lead yourself and be prepared for emergencies (implied).
</details>
<details>
<summary>
2020-04-28: Let's talk about Small YouTuber Support.... (<a href="https://youtube.com/watch?v=fDDeHirzU3k">watch</a> || <a href="/videos/2020/04/28/Lets_talk_about_Small_YouTuber_Support">transcript &amp; editable summary</a>)

Beau addresses the need for genuine engagement over mere numbers in supporting small YouTubers, urging creators to describe their content to attract interested subscribers.

</summary>

"You want people interested in your content more than subscribers."
"It's better to have 100 subscribers who are truly interested in your content than 1,000 who aren't going to go watch it."
"Describe your channel. Short little sentence, tell us what it is."
"Hopefully you can pick up subscribers who are actually interested in the type of content you put out."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Addresses the issue of small YouTubers needing support and recognition.
Notes the surge of new channels emerging during the current times when people are at home.
Encourages small YouTubers to describe their channel content rather than just seeking subscribers.
Advises that having genuinely interested subscribers is more beneficial than a large number of disinterested ones.
Stresses the importance of having an active, engaged audience for YouTube's algorithm.
Offers to support and showcase new channels to his active community.
Mentions a previous video with advice for new YouTubers, particularly those focused on news content.
Teases upcoming content about preparedness and the importance of being ready.

Actions:

for small content creators,
Describe your channel content clearly in the comment section when reaching out to support other small YouTubers (suggested).
Engage actively with new channels and creators by watching their content and providing feedback (implied).
</details>
<details>
<summary>
2020-04-28: Let's talk about Land O'Lakes, symbols, and the symbol-minded.... (<a href="https://youtube.com/watch?v=BupotrJw5xw">watch</a> || <a href="/videos/2020/04/28/Lets_talk_about_Land_O_Lakes_symbols_and_the_symbol-minded">transcript &amp; editable summary</a>)

Beau criticizes the manipulation through symbols, urging people to think critically beyond loyalty to symbols in a society easily led by manipulative powers.

</summary>

"Symbols are meant to evoke emotion and create loyalty without requiring critical thought."
"We should probably think beyond symbols."
"They care about using symbols to manipulate you. They don't care about you."
"We are headed into tough times. We are going to have to critically think."
"Butter is a divisive topic in this country, not because butter is that important, but because of the symbol."

### AI summary (High error rate! Edit errors on video page)

Land O'Lakes removed a drawing of a native woman from their packaging, causing distress among Americans.
Symbols are meant to evoke emotion and loyalty without requiring critical thought.
Symbols, like corporate logos or flags, are designed to elicit immediate reactions.
Beau criticizes the use of symbols to manipulate individuals who do not want to think critically.
Political memes often aim to provoke emotional responses rather than inspire critical thinking.
Americans expressed outrage over the removal of a drawing of a native woman from butter packaging, while real issues, like missing native women, are overlooked.
Beau points out the danger of valuing symbols more than what they are meant to represent.
Many individuals prioritize symbols, like national flags, over the true values they stand for.
Beau argues that critical thinking is necessary for self-governance and making informed decisions.
Symbols like political party logos often overshadow individuals' true beliefs and critical thinking.
The focus on symbols in American society makes people easily manipulated and exploited by those in power.

Actions:

for citizens, critical thinkers,
Question the symbols around you and their true significance (suggested)
Foster a culture of critical thinking and questioning in your community (implied)
</details>
<details>
<summary>
2020-04-27: Let's talk about preparing for the next time (Part 1).... (<a href="https://youtube.com/watch?v=BALZGFZ_yxs">watch</a> || <a href="/videos/2020/04/27/Lets_talk_about_preparing_for_the_next_time_Part_1">transcript &amp; editable summary</a>)

Be prepared for future crises by investing in essentials like medical kits, water containers, and tools for under $600.

</summary>

"They want to be ready for the next event. There will be another one, there will always be another one."
"Preparing ahead of time, stockpiling stuff, getting ready. However, that requires space."
"You can kit somebody up completely for that."
"You have to actually go and read this stuff because it doesn't apply to all medicine."
"You don't have to have them around the whole time for taking up space."

### AI summary (High error rate! Edit errors on video page)

Encourages preparedness after recent events, stressing the inevitability of future crises.
Acknowledges the challenge of preparing for multiple people with varying needs and limited space.
Recommends purchasing a pre-packed bag for efficiency when prepping for a larger group.
Emphasizes the importance of a comprehensive medical kit beyond basic first aid supplies.
Advises obtaining medication from the dollar store due to affordability and variety.
Suggests researching the Shelf Life Extension Program for medicine potency over time.
Recommends an Everyday Carry (EDC) kit for basic essentials always on hand.
Advocates for investing in durable, affordable knives for practical use.
Mentions the relevance of a fishing kit and storing seeds for sustainability.
Proposes collapsible water containers for compact water storage in small living spaces.

Actions:

for preparedness enthusiasts,
Purchase a pre-packed bag for efficiency in emergency preparedness (implied).
Obtain a comprehensive medical kit beyond basic first aid supplies (implied).
Research the Shelf Life Extension Program for medicine potency over time (implied).
Invest in durable, affordable knives for practical use (implied).
Store seeds for long-term sustainability (implied).
</details>
<details>
<summary>
2020-04-27: Let's talk about preparing for next time (Part 2).... (<a href="https://youtube.com/watch?v=tKoD9hZPbUE">watch</a> || <a href="/videos/2020/04/27/Lets_talk_about_preparing_for_next_time_Part_2">transcript &amp; editable summary</a>)

Beau provides detailed advice on stocking up on food supplies, additional emergency items, and free resources, stressing the importance of being prepared for future crises.

</summary>

"It's a good idea to be prepared."
"Your goal here is to just ride out the initial wave of craziness."
"You're going to have to take care of it yourself."
"It's probably better to look homeless than look like somebody in the military."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Advises viewers to watch part one before continuing with the current video for context.
Provides detailed information on stocking up on food supplies for emergencies, including canned goods and freeze-dried options.
Mentions the scarcity of certain emergency food supplies and the high prices on the secondary market.
Talks about the nutritional value and drawbacks of MREs (Meals Ready to Eat).
Suggests basic food options for being prepared, costing around $600.
Recommends additional items like an e-tool (foldable shovel) and a ham radio for emergencies.
Shares free resources such as downloading survival and medical manuals from archive.org.
Warns against looking too militaristic while prepping and suggests blending in during emergencies.
Emphasizes the importance of being prepared for future crises and not relying solely on government responses.
Clarifies that he is not endorsing any specific brands and encourages having at least thirty days worth of food stocked up.

Actions:

for preppers and those interested in emergency preparedness.,
Stock up on canned goods, freeze-dried foods, dried rice, and beans for emergency food supplies (suggested).
Purchase additional items like an e-tool (foldable shovel) and a ham radio for emergencies (suggested).
Download survival and medical manuals from archive.org for free (suggested).
</details>
<details>
<summary>
2020-04-26: Let's talk about what Matanuska-Susitna can learn from the Streisand effect.... (<a href="https://youtube.com/watch?v=1wQy2YbsRok">watch</a> || <a href="/videos/2020/04/26/Lets_talk_about_what_Matanuska-Susitna_can_learn_from_the_Streisand_effect">transcript &amp; editable summary</a>)

Barbara Streisand's privacy fight teaches a lesson; banned books are vital for critical thinking and truth, challenging censorship.

</summary>

"Banned books are the best books."
"Fiction is what gets you to truth."
"Literature like this that covers uncomfortable themes, it creates a population that can distinguish facts from fiction."
"Your book is next. The way it always works."
"To make you a better person."

### AI summary (High error rate! Edit errors on video page)

Barbara Streisand's situation serves as a lesson for the Matanuska Susitna School District in Alaska.
Streisand fought to keep photos of her house private, which only piqued people's curiosity.
Beau was asked for reading recommendations on a livestream and suggests books that the school district has banned.
The school district banned teaching books like The Great Gatsby, Invisible Man, Catch-22, The Things They Carried, and I Know Why the Caged Bird Sings due to uncomfortable themes.
Beau argues that literature with uncomfortable themes helps individuals distinguish facts from fiction and think critically.
Exposure to such literature can prevent uninformed statements from people in power, like Lysol's recent statement.
Beau stresses the importance of preparing students for life's discomforts through literature.
Complaints about the banned books often stem from religious groups.
Beau challenges the censorship, pointing out that uncomfortable themes exist in the Bible as well.
He warns that once censorship starts, more books will follow.
Beau offers support to librarians in the district who may need additional copies of the banned books.
He encourages everyone to read these banned books, stating that they are significant in understanding alternative facts and censorship.
Fiction, according to Beau, often leads to truth, making banned books invaluable.
Beau concludes by encouraging his audience to have a good day.

Actions:

for educators, librarians, book lovers,
Support librarians in the school district by providing additional copies of banned books (suggested)
Reach out via Twitter, Facebook, or email to assist in ensuring the availability of necessary texts (suggested)
</details>
<details>
<summary>
2020-04-25: Let's talk with Deep Goat about Republicans admitting Russian interference... (<a href="https://youtube.com/watch?v=rife6Cempk0">watch</a> || <a href="/videos/2020/04/25/Lets_talk_with_Deep_Goat_about_Republicans_admitting_Russian_interference">transcript &amp; editable summary</a>)

Republicans confirm Russian interference but avoid proving collusion, leaving unanswered questions on motives and impacts, indicating potential future threats.

</summary>

"We need to know why. We need to know what Russian intelligence knew about the President that the voters don't."
"At the end of the day, whether or not that campaign by the Russians had a huge effect or not, they won."
"Maybe he doesn't even know he's being used."

### AI summary (High error rate! Edit errors on video page)

Republicans in the Senate acknowledged Russian interference in the 2016 election but didn't prove collusion, stating it's a law enforcement matter, not intelligence community's duty.
Intelligence community focuses on determining intent and predicting the future, rather than catching someone in the act.
The big unanswered question is why the Russian campaign shifted from undermining the election to supporting Trump.
There's anticipation for the counterintelligence report to reveal how the US countered the Russian campaign and what Russian intelligence knew about Trump.
Despite initial claims of a hoax, politicians now agree that the Russian interference occurred.
The success of Russia's campaign to influence the election, even marginally, sets a dangerous precedent for future attempts.
Concerns arise about whether Trump is unwittingly manipulated by Russia, given his lack of international savvy.
The intelligence community's reluctance to reveal failures may hinder full transparency on the matter.
Regardless of the impact, Russia's successful election interference demonstrates their ability and potential for future attempts.
The alignment of Russia's interests with those of Trump raises suspicions about potential influence on his foreign policy decisions.

Actions:

for voters, policymakers, analysts,
Demand transparency and accountability from elected officials regarding foreign interference (implied)
Stay informed and engaged in understanding foreign policy decisions and their implications (implied)
</details>
<details>
<summary>
2020-04-25: Let's talk about which studies to believe.... (<a href="https://youtube.com/watch?v=PeNnbEdY4yQ">watch</a> || <a href="/videos/2020/04/25/Lets_talk_about_which_studies_to_believe">transcript &amp; editable summary</a>)

Beau outlines his method for selecting trustworthy studies, stressing the importance of peer-review, sample size, methodology, and common-sense reasoning in evaluating research findings.

</summary>

"A larger sample size is almost always better."
"Studies aren't there to fully answer a question. They're there to provide more information so you can combine it with other stuff to draw a conclusion."
"I ask myself if there's another reason for the apparent conclusion. A common-sense reason."
"If there is another reason, a common sense reason, the study has to be spectacular for me to put any stock in it at all."
"That study alone doesn't do it for me."

### AI summary (High error rate! Edit errors on video page)

Explains the steps he takes to choose a study to trust, prompted by a question from a follower.
Emphasizes the importance of peer-reviewed studies and the significance of sample size in research.
Mentions the methodology of the study and how participants were recruited as vital factors.
Suggests that studies are not meant to provide definitive answers but to contribute to a broader understanding.
Raises the point of understanding what exactly a study is measuring to avoid skewed results.
Provides an example of how studies can be manipulated to fit a narrative, using a scenario of changing bar closing times.
Advocates for looking beyond the apparent conclusion of a study and considering common-sense reasons for the outcomes.
Gives an example of a study suggesting a causal relationship between women owning horses and living longer, offering practical explanations for this correlation.
Critically analyzes a current study on air pollution and its link to health outcomes, questioning the significance of the findings based on population distribution.
Concludes by sharing his approach of seeking common-sense explanations before putting faith in a study.

Actions:

for researchers, students, academics,
Verify the peer-review status of studies before accepting their conclusions (implied)
Scrutinize sample sizes in research to ensure representativeness (implied)
Question the methodology and participant recruitment of studies for comprehensive evaluation (implied)
Look beyond apparent conclusions and seek common-sense explanations for research outcomes (implied)
</details>
<details>
<summary>
2020-04-24: Let's talk about alphas, wolves, Trump, and myths.... (<a href="https://youtube.com/watch?v=eefKb6EPMzc">watch</a> || <a href="/videos/2020/04/24/Lets_talk_about_alphas_wolves_Trump_and_myths">transcript &amp; editable summary</a>)

Beau dismantles the myth of the alpha male, advocating for a nurturing, protective form of masculinity over aggression and dominance.

</summary>

"The idea, the myth that exists in popular culture about an alpha, that came from a study of wolves."
"I think American masculinity would be better served trying to be a free alpha rather than a terrified animal in a cage."
"It's protecting and allowing their community to prosper."
"An alpha who had to think. A real one."
"If that's the definition of an American alpha, if that's the pinnacle of American masculinity, we're probably in real trouble."

### AI summary (High error rate! Edit errors on video page)

Explains the misconception of the term "alpha" in relation to masculinity, stemming from a study of wolves in captivity.
Points out that the myth of the alpha as aggressive and dominating has been debunked, but still persists in popular culture.
Contrasts the behavior of wolves in captivity (terrified, aggressive) with those in the wild (nurturing, protective).
Suggests that American masculinity should aim to be like a free alpha in the wild, nurturing and protective of their community.
Talks about toxic masculinity and how it doesn't mean all masculinity is bad, but certain behaviors are toxic.
Mentions that the term "toxic masculinity" originated from men's rights activists concerned about the hypermasculine traits overshadowing nurturing aspects.
Emphasizes the importance of being a real alpha, nurturing and protecting the community, instead of focusing on competition and aggression.
Criticizes the notion of President Trump as an alpha, citing his lack of responsibility, inability to lead, and reliance on privilege.
Encourages a shift from the myth of the aggressive alpha to embracing a more nurturing and cooperative approach to masculinity.
Concludes by urging for a reevaluation of what it means to be an alpha and American masculinity.

Actions:

for men, masculinity advocates,
Challenge traditional notions of masculinity by promoting nurturing and protective behaviors in your community (exemplified)
Educate others on the misconceptions surrounding the term "alpha" and toxic masculinity (suggested)
</details>
<details>
<summary>
2020-04-24: Let's talk about a Twitter Q and A (Part 5).... (<a href="https://youtube.com/watch?v=OMZ57LxjxFc">watch</a> || <a href="/videos/2020/04/24/Lets_talk_about_a_Twitter_Q_and_A_Part_5">transcript &amp; editable summary</a>)

Be prepared for disasters with limited resources, focus on policies over candidates, encourage diverse tactics against corruption, and maintain hope amidst uncertainties.

</summary>

"I support policies and ideas."
"Generally speaking, Americans do not try to put themselves in anybody else's shoes, much less foreigners."
"Hope is a dangerous thing, and I think everybody should remain dangerous, stay full of hope."
"I don't try to create little ideological foot soldiers."
"The world is constantly changing. We shouldn't want it to be static. We should want it to change."

### AI summary (High error rate! Edit errors on video page)

Unexpectedly faced with numerous questions on various topics.
Declines to show the shop due to lack of presentability.
Advises to be prepared for disasters with limited resources.
Stresses supporting policies over individual candidates.
Acknowledges the detrimental impact of both Republicans and Democrats.
Calls for a diversity of tactics in combating government corruption.
Emphasizes focusing on accuracy in developing journalistic skills.
Views the elimination of political parties and campaign finance reform as positive steps.
Shares perspective on unlearning racist and sexist undertones in Southern culture.
Expresses concerns about the long-lasting impact of Trump-appointed federal judges.
Questions the lack of consideration among Americans for their global image.
Recommends consuming information from various sources to find truth.
Encourages maintaining hope despite challenges and uncertainties.
Advocates for exposing children to diverse political views and promoting critical thinking.
Urges people to start growing their own food amidst disruptions in supply chains.

Actions:

for individuals, parents, citizens,
Start growing your own food to prepare for potential disruptions in supply chains (implied).
Expose children to diverse political views and encourage critical thinking (implied).
</details>
<details>
<summary>
2020-04-24: Let's talk about a Twitter Q and A (Part 4).... (<a href="https://youtube.com/watch?v=0-0y4PWOZy0">watch</a> || <a href="/videos/2020/04/24/Lets_talk_about_a_Twitter_Q_and_A_Part_4">transcript &amp; editable summary</a>)

Beau suggests steps to rehabilitate news media perception, advocates for countering dangerous rhetoric, and shares insights on the agricultural sector, political systems, accountability, and capitalism.

</summary>

"Mock them, debunk them, move on."
"There has to be a counter to it."
"Free market capitalism can only exist without a government."

### AI summary (High error rate! Edit errors on video page)

Provides a quick question and answer from Twitter for those who can't attend a live stream.
Suggests steps to rehabilitate news media perception, focusing on reducing bias and increasing balance.
Mentions the importance of journalistic practices like labeling speculation and file footage correctly.
Talks about the need for a counter to outlets with dangerous rhetoric and the importance of mocking and debunking them.
Addresses the impact of current events on the agricultural world, mentioning the surplus due to restaurant closures.
Expresses views on the potential need for a bailout for farmers or corporate ownership in the agricultural sector.
Shares thoughts on adopting a parliamentary system or eliminating political parties altogether.
Explains the concept of a fifth column in relation to opening gates for independent voices in media.
Comments on the challenges of fair representation in government amidst corruption and corporate influence.
Speculates on the lack of public accountability for Trump's actions post his presidency.
Talks about the value of participation in decentralized platforms like a symptom tracker during the absence of a coherent federal response.
Compares free market capitalism with cronyism and suggests that corruption occurs once the government intervenes.

Actions:

for media consumers,
Counter dangerous rhetoric by mocking and debunking inflammatory outlets (implied).
Support independent voices in media by seeking alternative news sources (implied).
Participate in decentralized platforms for information-sharing and collaboration (implied).
</details>
<details>
<summary>
2020-04-24: Let's talk about a Twitter Q and A (Part 3).... (<a href="https://youtube.com/watch?v=XL636MSKeb4">watch</a> || <a href="/videos/2020/04/24/Lets_talk_about_a_Twitter_Q_and_A_Part_3">transcript &amp; editable summary</a>)

Beau responds to various questions in a Twitter Q&A, discussing topics like general strikes, sustainability, stress management, and the implications of a military coup, advocating for peaceful solutions and individual coping mechanisms.

</summary>

"If you don't run your life, somebody else will."
"Good ideas generally don't require force."
"There is no simple way to deal with stress."

### AI summary (High error rate! Edit errors on video page)

Responding to questions in a Twitter Q&A, sharing off-the-cuff responses to various topics like general strikes and stateless societies.
Expressing support for strikes as effective tools, suggesting setting up parallel institutions to defeat existing government structures.
Explaining why some people still support Trump: sunk cost fallacy and being in echo chambers that shield them from differing viewpoints.
Contemplating how a stateless society with a highly educated populace might handle situations like the current one of staying home and taking precautions.
Advocating against armed conflict and discussing the potential benefits of a general strike as a form of peaceful protest.
Sharing thoughts on Earthships and sustainable living, acknowledging the political motivations behind certain architectural choices.
Providing advice on dealing with stress, acknowledging its universality and suggesting finding individual coping mechanisms.
Addressing the logistics and implications of a military coup in the United States, expressing doubts about its likelihood and discussing potential outcomes.
Describing personal methods of de-stressing, including working in the yard and occasional workouts.

Actions:

for community members,
Set up parallel institutions to provide better services and undermine government control (implied)
Encourage education and critical thinking in the community to foster a highly educated populace (implied)
Support peaceful forms of protest like general strikes as effective tools for change (implied)
Promote sustainable living practices and environmental awareness in local communities (implied)
Advocate for peaceful conflict resolution and individual stress management techniques (implied)
</details>
<details>
<summary>
2020-04-23: Let's talk about a Twitter Q and A (part 1 of 5) (<a href="https://youtube.com/watch?v=vxfNTnoe1kw">watch</a> || <a href="/videos/2020/04/23/Lets_talk_about_a_Twitter_Q_and_A_part_1_of_5">transcript &amp; editable summary</a>)

Beau conducts a Q&A addressing various topics from anti-intellectualism in media to charging for firearms training, sharing insights on languages, influential figures, public safety scenarios, and declining a book about rescue operations.

</summary>

"Everybody has skills to contribute, and I'm happy that I have found my place in that."
"If you put all cops in prison and set all prisoners free, will the streets be more or less safe."
"I enjoy the fight, I do."

### AI summary (High error rate! Edit errors on video page)

Conducting a Q&A session to answer questions from Twitter due to viewers' inability to attend live streams.
Explains the use of GIFs of women to express emotions, attributing it to women being more emotive and clear in their expressions.
Comments on the trend of anti-intellectualism in right-wing media in the United States, where opinions are valued over facts.
Addresses the issue of charging for firearms training due to increasing demand, considering safety training free but charging for proficiency.
Talks about managing Patreon messages and the unexpected growth of his platform with limited infrastructure.
Shares his proficiency in multiple languages that have become less fluent over time due to lack of practice.
Mentions influential figures in his life, politically, philosophically, and economically, including Emma Goldman, Sophie Scholl, Hunter S. Thompson, Thomas Paine, and Thomas Jefferson.
Responds to questions about Nostradamus and the concept of putting all cops in prison and setting prisoners free for public safety.
Expresses his thoughts on North Korea, authoritarian governments, and the potential scenarios if Kim doesn't recover.
Humorously declines an offer to be someone's dad and explains why he wouldn't write a book about past rescue operations.

Actions:

for creators and activists,
Reach out to Beau for firearms training (suggested)
Support creators with limited infrastructure like Beau on platforms like Patreon (implied)
</details>
<details>
<summary>
2020-04-23: Let's talk about a Twitter Q and A (Part 2)..... (<a href="https://youtube.com/watch?v=6wJSugxRF5A">watch</a> || <a href="/videos/2020/04/23/Lets_talk_about_a_Twitter_Q_and_A_Part_2">transcript &amp; editable summary</a>)

Beau addresses questions on moving away from consumer capitalism, warns about fascism, and criticizes governmental responses amidst other diverse topics.

</summary>

"We're definitely going to move away from the consumer capitalism that we have today."
"I try not to get into it, to be honest, because once you start talking about that, that's all people want to talk about."
"If they really just don't get it, they don't get it."
"We didn't act. We have teams within DOD. We have the CDC. We have entire agencies devoted to this."
"I think it's outdated before it's ever in the field."

### AI summary (High error rate! Edit errors on video page)

Conducting a Twitter Q&A to reach those who can't attend live streams.
Addressing questions about moving away from capitalism and consumer culture.
Believes the nation will have to move away from consumer capitalism due to its unsustainability.
Suggests ways to transition, like growing your own food and reducing consumption.
Warns about the potential rise of a fascistic government in the U.S.
Prefers not to focus on his colorful backstory but on current issues.
Advocates for focusing on those who understand the severity of COVID-19 rather than skeptics.
Surprised by the lack of proactive response to the pandemic from government agencies.
Criticizes the F-35 program and questions the necessity of fighter pilots in modern warfare.
Doubts the Republican Senate's ability to stand up to Trump.
Expresses support for the Kennedy half dollar from 1976 as his favorite U.S. coin design.
Shares insights on the role of the CIA in focusing on human intelligence and determining intent.
Criticizes Governor DeSantis's handling of safety measures in Florida theme parks.
Supports Gaia theory and the idea of earth as a self-regulating entity.
Explains the term "operator" in the context of special operations or intelligence communities.

Actions:

for socially conscious individuals.,
Stay informed and follow health guidelines to combat COVID-19 (implied).
Advocate for sustainable practices like growing your own food and reducing consumption (implied).
Support agencies focusing on human intelligence to determine intent (implied).
Stay critical of government actions and hold leaders accountable (implied).
</details>
<details>
<summary>
2020-04-22: Let's talk about satire, irony, parody, and a goat.... (<a href="https://youtube.com/watch?v=WpKPlSwgK4A">watch</a> || <a href="/videos/2020/04/22/Lets_talk_about_satire_irony_parody_and_a_goat">transcript &amp; editable summary</a>)

Beau explains satire, irony, and parody, using humor to guide viewers toward truth while fostering community through running jokes and gags.

</summary>

"Sometimes a little bit of fiction can help people get to truth."
"Little bit of humor and levity, it's a good thing every once in a while."
"It's a cheap trick, but it works."
"Everybody else gets a laugh, and I help a couple people."
"The goal of this channel is to spread information but also to build a community."

### AI summary (High error rate! Edit errors on video page)

Explains satire, irony, and parody in video content.
Despite not typically discussing his process, Beau felt compelled to address the topic due to viewer inquiries.
Long-running jokes and gags on the channel were sidelined for vital health-related information dissemination.
Satire was sparingly used on the channel, but recent subscriber growth introduced new viewers unfamiliar with Beau's style.
Defines satire as using humor, irony, exaggeration, or ridicule to criticize societal issues, mainly in politics.
Beau's satire heavily relies on exaggeration and irony, evident in his content.
Satire's challenge lies in being indistinguishable from genuine opinions, known as Poe's Law.
Beau employs humor to lighten the heavy topics covered on the channel.
Fictional elements in Beau's content aim to guide viewers towards truth amidst statistical information.
Strategic titling and appearance aid Beau in reaching diverse audiences and challenging their perspectives.
Visual cues like attire and props add layers to Beau's content, including Easter eggs and hidden jokes.
Introduces a parody character, Deep Goat, as part of the channel's behind-the-scenes humor.
Beau aims to foster community through running jokes and gags on the channel.
Ultimately, Beau's goal is to build a community that extends beyond online interactions.

Actions:

for content creators,
Analyze and incorporate satire, irony, and parody elements in content creation to reach diverse audiences and challenge perspectives (suggested).
Utilize humor to lighten heavy topics and foster community engagement (implied).
Incorporate visual cues and Easter eggs to enhance content and encourage audience interaction (implied).
</details>
<details>
<summary>
2020-04-21: Let's talk about what a Mandarin and Cameron Diaz can teach us.... (<a href="https://youtube.com/watch?v=-FIw-PPsg7s">watch</a> || <a href="/videos/2020/04/21/Lets_talk_about_what_a_Mandarin_and_Cameron_Diaz_can_teach_us">transcript &amp; editable summary</a>)

Beau explains the moral lessons from the Mandarin and "Button Button," urging cooperation and selflessness in the current crisis as a dress rehearsal for greater challenges like climate change.

</summary>

"We may not be being judged in the same manner, but we're being tested."
"We've got to stop letting those in power divide us."
"This is a dress rehearsal for some of the stuff we have coming down the pike if we don't change."
"You should probably do your part. You should probably cooperate."
"There's going to be a whole lot of people that aren't around anymore to tell you you're wrong."

### AI summary (High error rate! Edit errors on video page)

Analyzes the Mandarin parable and its derivative, "Button Button," which is the basis for the movie "The Box."
Explores the universal truth behind these stories about moral dilemmas.
Mentions the current situation we are in and the need for moral reflection.
Talks about being judged and tested in the face of cooperation and selflessness.
Urges people to stop allowing those in power to divide and manipulate us.
Emphasizes the importance of cooperation, especially in times of crisis.
Compares the current situation to a dress rehearsal for larger challenges like climate change.
Encourages individuals to take responsibility and cooperate for the greater good.
Raises questions about personal accountability and ethical decision-making.
Suggests that staying home and doing our part is vital in times of crisis.

Actions:

for individuals, community members,
Cooperate with others in your community by following health guidelines and staying at home (implied).
Take personal responsibility by making ethical decisions that prioritize the well-being of others (implied).
Refrain from actions that put vulnerable individuals at risk, even if it seems distant or less impactful to you (implied).
</details>
<details>
<summary>
2020-04-21: Let's talk about Trump growing into the role of leader.... (<a href="https://youtube.com/watch?v=rJNYl2nByKs">watch</a> || <a href="/videos/2020/04/21/Lets_talk_about_Trump_growing_into_the_role_of_leader">transcript &amp; editable summary</a>)

Trump is seen as growing into a world leader by closing borders, framing it as protecting American jobs, and empowering governors to take on leadership roles in response to the crisis.

</summary>

"He's becoming the leader we all knew he was."
"It was making sure that those governors could be seen as true leaders."
"Trump is a real leader, and he's finally showing it."

### AI summary (High error rate! Edit errors on video page)

Trump is being seen as growing into the role of a world leader by closing borders to immigration, branding it as protecting American jobs.
The decision to close borders is seen as a move to protect against the severity of the situation and encourage people to stay home.
Trump's base understands there are no jobs to protect and that his branding is a way for him to demonstrate leadership.
The real motivation behind closing borders is to show leadership and protect both Americans and foreign workers.
Trump had to frame his decision in a certain way due to his international image as a "complete buffoon."
Beau suggests that Trump is trying to empower governors to become leaders in their own right by taking actions such as reopening states cautiously.
Trump's actions are viewed as an attempt to show leadership and create a sense of control in a chaotic situation.
Despite criticisms of Trump's handling of the situation, Beau believes in giving him a second chance to prove himself as a leader.
Beau compares Trump's actions to those of a true leader who empowers others and sacrifices for the greater good.
He acknowledges the skepticism towards Trump's motives but ultimately believes in rallying behind the overall plan.

Actions:

for political observers,
Rally behind leaders who demonstrate true leadership (implied)
Empower governors to take on leadership roles (implied)
</details>
<details>
<summary>
2020-04-20: Let's talk about operators, the tacticool, and public gatherings.... (<a href="https://youtube.com/watch?v=NjE24GK3ijU">watch</a> || <a href="/videos/2020/04/20/Lets_talk_about_operators_the_tacticool_and_public_gatherings">transcript &amp; editable summary</a>)

Beau stresses the importance of education, risk mitigation, and following health guidelines, urging individuals to make informed decisions without relying on government orders.

</summary>

"Real warriors are educated."
"They're about mitigating risk."
"Knowledge is power. Education is power."
"There shouldn't have to be government orders telling you to do this."
"Shouldn't have to have an order, but because you know enough to stay home on your own."

### AI summary (High error rate! Edit errors on video page)

Talks about the presence of individuals pretending to be operators in public gatherings.
Emphasizes the importance of education and expertise in real warriors.
Mentions the focus on saving lives and mitigating risk in high-speed operators' actions.
Stresses the significance of listening to subject matter experts rather than propaganda.
Expresses the belief that operators prioritize risk mitigation, which includes wearing a mask during public gatherings.
Addresses the issue of people undermining efforts to save lives during the pandemic by not taking necessary precautions.
Criticizes the political show and lack of precautions in public gatherings.
Mentions the concept of herd immunity and its flaws in the context of the current situation.
Encourages staying at home, washing hands, and wearing masks when necessary to combat the spread of the virus.
Advocates for being educated and making informed decisions without the need for government orders.

Actions:

for community members,
Stay at home, wash hands, and wear a mask when going out for essentials (implied)
Strive to be educated and listen to subject matter experts (exemplified)
</details>
<details>
<summary>
2020-04-19: Let's talk about rights and responsibilities.... (<a href="https://youtube.com/watch?v=2rXfJdZTDJA">watch</a> || <a href="/videos/2020/04/19/Lets_talk_about_rights_and_responsibilities">transcript &amp; editable summary</a>)

Beau delves into the intertwined nature of rights and responsibilities, exposing the selfishness of prioritizing one over the other, urging for a balance that truly benefits society.

</summary>

"If you are not concerned about your responsibilities and you're only concerned about your rights, you're just selfish."
"Don't use them for that."
"It just reeks of privilege."
"You have the right to free speech. It's in the First Amendment. Why was that put in there? Was it so anybody could say any silly little thing that popped into their head? No, of course not."
"But you can't make the sacrifice of staying home and watching Netflix to protect your community."

### AI summary (High error rate! Edit errors on video page)

Addresses the concept of rights and responsibilities, particularly in the context of the U.S. Constitution's Bill of Rights.
Emphasizes the importance of understanding the inherent responsibilities that come with exercising rights.
Gives examples like the right to free speech and assembly, linking them to the responsibility to ensure those rights are exercised in a way that benefits society.
Talks about the Second Amendment and the responsibility it entails in protecting the community.
Criticizes those who claim to be ready to defend rights but fail to fulfill basic responsibilities.
Raises the idea that ultimate freedom comes with shared responsibility for everything.
Points out the selfishness of focusing only on rights without acknowledging responsibilities.
Challenges the notion of using certain groups as shields for personal struggles.
Calls for consistent support and care for all, not just in times of crisis that impact the middle class.
Condemns the privilege and transparency of those who prioritize rights over obligations to society.

Actions:

for rights and responsibilities activists,
Ensure that your exercise of rights is accompanied by a sense of responsibility towards society (implied).
Advocate for policies that support living wages and access to healthcare for all individuals (implied).
</details>
<details>
<summary>
2020-04-19: Let's talk about UBI, McDonald's, prisons, and radical ideas.... (<a href="https://youtube.com/watch?v=qmaLRP65c0o">watch</a> || <a href="/videos/2020/04/19/Lets_talk_about_UBI_McDonald_s_prisons_and_radical_ideas">transcript &amp; editable summary</a>)

Societal change, driven by income inequality, challenges stigmas and comforts, showing human nature's pursuit of improvement.

</summary>

"People always want to better themselves, better their situation, better society, most people."
"Human nature provides the carrot."
"Even in one of the most depressing environments in the world, human nature still drives for improvement."

### AI summary (High error rate! Edit errors on video page)

Societal change is imminent and historically triggered by events like the current one, potentially related to income inequality.
Many seek an egalitarian society but may not know exactly what they are looking for due to the flaws in the current system.
Reading social media arguments led Beau to reconsider his opinion, even changing it based on a counter-argument against Universal Basic Income (UBI).
Beau challenges the idea that people wouldn't work at minimum wage jobs like McDonald's if their basic needs were met, pointing out the societal stigma attached to such jobs due to low pay.
He questions the belief that comfort is the enemy of radical thought, acknowledging that even in times of comfort, people can still strive for radical change.
Beau argues that radical thought and mutual aid, like making masks during the current crisis, can occur even when people are not pushed to extreme discomfort.
Drawing from experiences in prison, Beau illustrates that human nature drives individuals to seek improvement even in challenging environments where basic needs are met.
The current system is deemed broken by Beau, leading him to reject a return to the status quo and advocate for something different, although unsure if UBI is the solution.

Actions:

for activists, policymakers, advocates,
Engage in mutual aid efforts like making masks for hospitals to support the community (exemplified)
Advocate for systemic changes to address income inequality and societal flaws (implied)
</details>
<details>
<summary>
2020-04-18: Let's talk about origin stories, blame, and irrelevant debates.... (<a href="https://youtube.com/watch?v=ly0Q5Cj8pNM">watch</a> || <a href="/videos/2020/04/18/Lets_talk_about_origin_stories_blame_and_irrelevant_debates">transcript &amp; editable summary</a>)

Beau stresses the importance of focusing on saving lives and finding solutions rather than assigning blame in the ongoing fight against the pandemic.

</summary>

"I don't care."
"We have to worry more about saving lives than saving political careers at this point."
"It's going to go on. I think we should focus on that."
"Right now, rather than trying to find a scapegoat, we need to find a solution."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Every villain needs an origin story, and the current worldwide villain's origin is debated between a market and a lab.
Despite the origin debate, Beau doesn't care because it doesn't change the need for mitigation efforts.
Beau believes that focusing on blame is a distraction from saving lives and improving international health security.
Antibodies don't guarantee immunity, and people shouldn't let their guard down based on that belief.
Beau stresses the importance of prioritizing immediate needs in the ongoing fight against the pandemic over assigning blame.
Security standards in labs need to be raised to prevent future leaks, but finding a solution should be the priority over finding a scapegoat.

Actions:

for global citizens,
Increase security standards at BSL-4 facilities (implied)
Support the World Health Organization for effective oversight (implied)
Prioritize saving lives over political interests (implied)
</details>
<details>
<summary>
2020-04-17: Let's talk about sunk costs and being wrong.... (<a href="https://youtube.com/watch?v=Yssxf8QZ0Z4">watch</a> || <a href="/videos/2020/04/17/Lets_talk_about_sunk_costs_and_being_wrong">transcript &amp; editable summary</a>)

Be open to change, accept being wrong, and beware the sunk cost fallacy in thought, business, and politics for better leadership and decision-making.

</summary>

"It's OK to be wrong."
"New information should always change your opinion."
"We need responsive leadership."

### AI summary (High error rate! Edit errors on video page)

Talks about the importance of being okay with being wrong and the sunk cost fallacy.
Mentions seeing the sunk cost fallacy being related to thought for the first time.
Explains that a sunk cost is an expense in business that won't be recouped.
Describes the sunk cost fallacy in the context of buying equipment that doesn't work as expected.
Emphasizes the need to cut losses and move on rather than trying to salvage a bad investment.
Draws parallels between the sunk cost fallacy in business decisions and beliefs or ideas.
Points out that people often struggle to admit they're wrong and cling to incorrect beliefs.
Stresses the importance of being open to changing opinions based on new information.
Shares examples of how the sunk cost fallacy plays out in politics, specifically referencing supporters of a certain political figure.
Asserts the need for responsive and articulate leadership, differentiating it from what he sees in current leadership.

Actions:

for individuals, voters, citizens,
Challenge and rethink your beliefs based on new information (implied).
Advocate for responsive and articulate leadership in your community (implied).
Encourage others to be open to changing opinions when new information arises (implied).
</details>
<details>
<summary>
2020-04-17: Let's talk about reopening America and what a gate can teach us.... (<a href="https://youtube.com/watch?v=4KQUJHlt-Mg">watch</a> || <a href="/videos/2020/04/17/Lets_talk_about_reopening_America_and_what_a_gate_can_teach_us">transcript &amp; editable summary</a>)

Beau discovers theft at a construction site, uncovering the importance of true expertise and not judging by appearances.

</summary>

"Don't judge people by their appearances. You'll be wrong a lot."
"Just because something appears to be secure, doesn't mean that it is."
"Follow the experts that actually are experts in the field you need."

### AI summary (High error rate! Edit errors on video page)

Receives a call to conduct a security survey on a high rise construction site, despite it not being his area of expertise.
Agrees to visit the construction site in the panhandle as a favor for a friend, even though it's a long drive.
Security consultants are usually expensive, but Beau is intrigued due to the ongoing mystery at the construction site.
Arrives at the meeting spot assuming he's meeting a real estate developer but is surprised by a blue-collar guy in casual attire.
Discovers that heavy construction equipment has been going missing and suspects the security guards.
Realizes that the gate in the fence, supposedly locked for months, has a lock that shows no signs of rust, indicating foul play.
Determines that someone cleverly replaced the lock, leading to the thefts.
Declines payment for his services and learns that the project manager owns the restaurant and the high rise building.
Emphasizes not to judge people by appearances, the importance of true expertise, and the need to look beyond surface appearances of security.
Warns against following advice from pseudo-experts like TV personalities who lack real credentials.

Actions:

for security professionals, truth seekers.,
Support blue-collar workers in your community (implied).
Consult real experts in fields requiring specialized knowledge (implied).
Double-check security measures in your surroundings (implied).
</details>
<details>
<summary>
2020-04-16: Let's talk about reopening the economy.... (<a href="https://youtube.com/watch?v=Hhf9ktqzxgY">watch</a> || <a href="/videos/2020/04/16/Lets_talk_about_reopening_the_economy">transcript &amp; editable summary</a>)

Reopening the economy prematurely without consumer confidence will lead to economic collapse, prioritize public safety over politics.

</summary>

"The economy will get back on track when you have confidence. Not before."
"If you want to destroy this economy, support these politicians."
"The government should spend its time trying to do its job, rather than trying to spin its way out of this."
"If you're done with the US, if you're just ready to bankrupt everybody, let's reopen now."
"Wash your hands. If you have to go out, wear a mask. Don't touch your face."

### AI summary (High error rate! Edit errors on video page)

Dates for reopening the economy like May 1st or June 15th are irrelevant as the power lies with individuals, not politicians.
The economy will only recover when people have confidence, not when the government reopens prematurely.
Reopening too soon without consumer confidence will lead to a severe economic depression.
Premature reopening can devastate small businesses with no consumer spending.
Politicians pushing for immediate reopening are prioritizing their own interests over public safety.
Hotspots for the virus are not just big cities but also smaller areas like Albany, Georgia, Idaho, and Mississippi.
The pandemic will only end with a reliable treatment or vaccine, not based on political wishes.
Trusting experts and following guidelines is more effective than protesting and spreading misinformation.
Rushing to reopen the economy without proper precautions will lead to more spikes and loss of public confidence.
Government relief for workers is vital until the situation is under control to prevent economic collapse.

Actions:

for individuals, public,
Stay at home, follow guidelines for safety (implied)
Wear a mask if you have to go out (implied)
Wash hands regularly to prevent the spread of the virus (implied)
</details>
<details>
<summary>
2020-04-15: Let's talk with Andy Ratto about mutual aid.... (<a href="https://youtube.com/watch?v=vgTam8Lbw98">watch</a> || <a href="/videos/2020/04/15/Lets_talk_with_Andy_Ratto_about_mutual_aid">transcript &amp; editable summary</a>)

Be prepared to tackle housing issues, connect with existing resources, and contribute actively to mutual aid efforts to address community needs during the ongoing pandemic and beyond.

</summary>

"We need to be prepared around housing issues that come up for people that can't pay rent."
"Everyone has something to contribute, and everyone has the opportunity to make use of that."
"We all have something to contribute, and we all are gonna face struggles and challenges."

### AI summary (High error rate! Edit errors on video page)

Introducing Andy Rado, a lead organizer of New York City United Against Coronavirus, providing information and resources for mutual aid projects in response to the ongoing pandemic.
Andy Rado explains that the project involves a variety of resources from government agencies, nonprofits, new groups, and individuals to address the extreme needs exacerbated by the coronavirus situation.
Mutual aid projects aim to address existing problems like food insecurity, housing insecurity, and lack of access to medical resources through community organizing and resource distribution.
Andy Rado shares his involvement in direct mutual aid organizing in Bed-Stuy, Brooklyn, and his role in disseminating vital information on available resources via social media and other communication channels.
The importance of not duplicating existing resources and instead collaborating with established organizations to meet community needs effectively is emphasized.
Advice is given for those looking to set up their own mutual aid network, suggesting starting by researching existing resources and partnering with established organizations.
The focus is on meeting immediate needs while also considering the long-term societal and governmental shifts necessary to address systemic issues heightened by the pandemic.
Beau and Andy Rado stress the importance of community connections, mutual support, and grassroots organizing in responding to crises at a local level.
The need for continued community engagement beyond the pandemic, addressing ongoing issues like job loss, housing insecurity, and access to healthcare, is discussed.
Andy Rado encourages individuals to contribute to mutual aid efforts by volunteering time, donating resources, or reaching out for help as part of a participatory and inclusive process.

Actions:

for community members, volunteers,
Join existing mutual aid organizations in your community to contribute time or resources (suggested).
Partner with established nonprofits or new groups to address local needs effectively (implied).
</details>
<details>
<summary>
2020-04-15: Let's talk about about dancing nurses and Matt Walsh.... (<a href="https://youtube.com/watch?v=c2QCL7BpYEQ">watch</a> || <a href="/videos/2020/04/15/Lets_talk_about_about_dancing_nurses_and_Matt_Walsh">transcript &amp; editable summary</a>)

Beau defends nurses' dancing as a morale booster and mental preparation for their challenging work while critiquing those like Matt Walsh who fail to understand their high-stress jobs.

</summary>

"Dancing before battle, that dates back to prehistory."
"Nurses, docs, dance, do primal chanting, paint your face, whatever it takes for you to pregame and get in the mindset to walk into that room and do your job, do it."
"If you have a platform right now, you have an obligation to do your part to lead people, to keep them safe."

### AI summary (High error rate! Edit errors on video page)

Nurses dancing has sparked controversy, with Matt Walsh criticizing their choice to dance instead of working.
Beau shares a story about Joe Bear, who once danced before a high-stakes mission in a combat zone.
Dancing before battle has historical roots in boosting morale and psychological preparedness.
Nurses dancing may serve a similar purpose, mentally preparing themselves for their challenging work.
Posting dance videos online may help raise morale and show solidarity with those in more strict hospital environments.
Walsh's criticism of healthcare workers causing economic issues is refuted by Beau.
Staying home aims to prevent hospitals from being overrun, not because they already are.
Walsh's lack of understanding of high-stress jobs is evident in his comments about nurses.
Right-wing pundits like Walsh are looking for scapegoats instead of taking responsibility for their actions.
Beau stresses the importance of supporting healthcare workers and ensuring their mental readiness for their critical roles.

Actions:

for healthcare advocates,
Support healthcare workers by showing appreciation for their efforts and understanding the challenges they face (implied)
Educate others on the importance of mental preparedness for high-stress jobs, like those in healthcare (implied)
Advocate for proper support and resources for frontline workers to ensure their well-being (implied)
</details>
<details>
<summary>
2020-04-14: Let's talk about your brain, theories, and authority.... (<a href="https://youtube.com/watch?v=tTzzoA6VFCw">watch</a> || <a href="/videos/2020/04/14/Lets_talk_about_your_brain_theories_and_authority">transcript &amp; editable summary</a>)

Beau explains theories seeking order from chaos, questions new authoritarian rule claims, and argues that current coercion hints at an existing authoritarian system.

</summary>

"Your brain is seeking out a pattern. It's trying to make sense of it."
"The main plot point of these theories doesn't make sense."
"The authoritarian rule people who subscribe to these theories are worried about, it's already here."
"If you want freedom, you have to exist in a system that allows it."
"If somebody can coerce you into going to risk yourself for their economic benefit mainly, and you get the crumbs, you are existing in Orwell's future."

### AI summary (High error rate! Edit errors on video page)

Explains how the brain seeks patterns to make sense of things like clouds resembling bunnies or faces.
Talks about the motivation behind various theories - the desire to bring order out of chaos for comfort.
Points out the common agreement among theories about ushering in a new authoritarian rule.
Criticizes a theory that suggests the normalization of masks and gloves is a tactic for control.
Questions the feasibility of theories that undermine the government's basis and ability to protect its citizens.
Mentions how the enforcement class may be reluctant to exercise control due to undermining methods.
Talks about the impact on military readiness and the restriction of social gatherings under authoritarian rule theories.
Argues that chaos does not benefit those in power in the long term.
Suggests a more plausible theory that an authoritarian system already exists, unnoticed by many.
Addresses the coercion within the current system that limits individual autonomy and agency.
Points out that many advocating against authoritarian rule are already living under its grasp.
Notes that the government has had powers to enforce measures like stay-at-home orders for a long time.
Asserts that surveillance measures should be opposed, especially due to government data handling concerns.
Emphasizes the importance of existing in a system that allows freedom and autonomy.

Actions:

for activists, skeptics, truth-seekers,
Advocate for transparency in government surveillance practices (implied)
Educate others about the potential coercion within existing systems (implied)
Support policies that enhance individual autonomy and agency (implied)
</details>
<details>
<summary>
2020-04-14: Let's talk about the President, varying responses, and an old video.... (<a href="https://youtube.com/watch?v=tRLWOyBZLRE">watch</a> || <a href="/videos/2020/04/14/Lets_talk_about_the_President_varying_responses_and_an_old_video">transcript &amp; editable summary</a>)

Beau criticizes the president for lack of leadership during the pandemic, urging people to prioritize public health over the economy and make informed decisions.

</summary>

"His decisions caused this. And his decisions are prolonging it."
"If you want leadership, you're going to have to do it."
"You are expendable. You have to make the decision."
"The shortest path to getting the economy back on track is everybody staying home for a month."
"It's got to be everybody."

### AI summary (High error rate! Edit errors on video page)

Criticizes the president for refusing to lead during the COVID-19 crisis.
Points out the president's attempts to divert blame and not take responsibility for the situation.
Mentions how the president disregarded advice from experts and failed to provide unified leadership, resulting in a prolonged crisis.
Advocates for staying home as a solution to the pandemic, with government support for individuals.
Calls for strong leadership to guide the country through the crisis and prioritize human life over the economy.
Emphasizes the importance of unified action to effectively combat the pandemic.
Expresses disappointment in the lack of leadership and decision-making at the national level.
Urges individuals to make informed decisions and not rely solely on government directives.
Warns against half measures and stresses the need for a comprehensive approach to battling the pandemic.
Encourages everyone to prioritize public health and safety above all else.

Actions:

for americans,
Stay home for a month to help get the economy back on track (implied).
Prioritize public health over economic concerns (implied).
Make informed decisions regarding personal safety and well-being (implied).
</details>
<details>
<summary>
2020-04-13: Let's talk about clarifying the President's authority.... (<a href="https://youtube.com/watch?v=Z_ykY_FSe00">watch</a> || <a href="/videos/2020/04/13/Lets_talk_about_clarifying_the_President_s_authority">transcript &amp; editable summary</a>)

Operating under the assumption that the president is wrong about government functions is safe, and local authorities hold more power than the president; expect rolling changes and continued precautions as the situation may last a year and a half.

</summary>

"Operate under the assumption that whatever he says, the exact opposite is true."
"Your local county commission or your local mayor, your local city council has more authority than the president."
"There's not gonna be this grand, overwhelming victory where we all just march out into the streets and hold a parade."
"You're not just protecting yourself, you're protecting others."
"It's probably gonna last a year and a half, maybe, until there's a vaccine, or at least until there's a really solid treatment."

### AI summary (High error rate! Edit errors on video page)

The president provided inaccurate information about how the government functions.
Operating under the assumption that the president is wrong when speaking about basic civics is safe.
The president's tweets about reopening states were incorrect; governors have authority in their states.
The federal government can remove federal guidance without consulting anyone.
Local authorities, like county commissions or city councils, have more authority than the president in certain matters.
The idea of a nationwide reopening is not realistic; there will be rolling changes and areas under stay-at-home orders.
Social distancing should still be in effect even if stay-at-home orders are lifted.
Reopening cities may lead to spikes in cases and a return to stay-at-home orders.
It's vital to understand that the situation may last a year and a half until there's a vaccine or effective treatment.
Precautions like wearing masks, washing hands, and minimizing trips are still necessary.

Actions:

for citizens, local officials,
Keep practicing social distancing and wearing masks (implied).
Continue to wash hands frequently and avoid touching your face (implied).
Minimize trips outside and follow local health guidelines (implied).
</details>
<details>
<summary>
2020-04-12: Let's talk about why Dr. Fauci wants you.... (<a href="https://youtube.com/watch?v=Euadz5isg9o">watch</a> || <a href="/videos/2020/04/12/Lets_talk_about_why_Dr_Fauci_wants_you">transcript &amp; editable summary</a>)

Beau encourages volunteering for a study tracking virus spread through antibodies, stressing the importance of participation for a clearer picture of the situation.

</summary>

"If you're up to it, send the email."
"Knowing how far it has spread among people who didn't have severe symptoms is really important."

### AI summary (High error rate! Edit errors on video page)

Public service announcement about volunteering for a study led by Dr. Fosse with 9,999 other people.
Goal is to track the spread of the virus among those who may not have known they had it by analyzing antibodies in blood.
Volunteers must be 18 or older, currently healthy, not showing symptoms, and not tested positive before.
The study involves a teleconference patient exam and blood analysis for antibodies.
Information gathered will help understand the spread and assist in mitigation efforts.
Volunteers can email clinicalstudiesunit@NIH.gov to participate.
Beau acknowledges trust issues but stresses the importance of this study for a clearer picture.
Results may reveal how far the virus has spread unnoticed and guide future containment efforts.
Participation is voluntary, no pressure to join if uncomfortable.
Sending an email expressing interest can lead to more information about the study.

Actions:

for potential volunteers,
Email clinicalstudiesunit@NIH.gov expressing interest in the study (suggested)
</details>
<details>
<summary>
2020-04-12: Let's talk about rewriting history.... (<a href="https://youtube.com/watch?v=zw0HErsYqX4">watch</a> || <a href="/videos/2020/04/12/Lets_talk_about_rewriting_history">transcript &amp; editable summary</a>)

History is constantly rewritten as new information emerges, challenging American mythology and reminding us of the importance of rewriting history for a positive future.

</summary>

"History is a living thing."
"People are flawed. People make mistakes."
"Rewriting history is incredibly important."
"Every time we get a new piece of information, every time the hindsight picture becomes more clear, we have to rewrite history."
"We need to be aware of the notes so we don't make the same mistakes again."

### AI summary (High error rate! Edit errors on video page)

History is constantly rewritten as we gather more information and gain a clearer picture of what happened.
The idea of rewriting history is not inherently negative; it is a necessary process as history is a living thing.
Some individuals resist rewriting history because they have given up on learning or are not interested in challenging American mythology.
Historical figures, like Columbus and Pocahontas, are often portrayed inaccurately in American mythology.
Rewriting history is vital for creating a positive future and avoiding repeating past mistakes.

Actions:

for history enthusiasts, educators, activists,
Challenge and question the established historical narratives (implied)
Educate yourself on accurate historical accounts and encourage others to do the same (implied)
Advocate for the inclusion of diverse perspectives in historical education (implied)
</details>
<details>
<summary>
2020-04-11: Let's talk about the news reusing footage.... (<a href="https://youtube.com/watch?v=QHFTbZj66Ik">watch</a> || <a href="/videos/2020/04/11/Lets_talk_about_the_news_reusing_footage">transcript &amp; editable summary</a>)

Beau explains the use of stock photos and footage in news media, dispelling misconceptions and advocating for ethical standards in labeling.

</summary>

"It's not evidence of a conspiracy. It's evidence of the consumer's desire to constantly have visual input."
"This isn't evidence of a conspiracy. It's not. This is evidence of bad news management."
"It's not true. But that's not the case. It just means that your news outlet's lazy."
"Try to hold these major outlets to an ethical standard."
"That alone is not evidence of anything other than a misunderstanding about how news media works."

### AI summary (High error rate! Edit errors on video page)

Explains the cottage industry within news media and journalism involving the use of stock photos and footage.
Describes his experience as a photojournalist selling photos to news outlets.
Mentions that media outlets rely heavily on freelancers for visual content.
Addresses the misconception that using stock footage means an event didn't happen.
Points out that major news networks use old footage to fill visual space, not as evidence of a conspiracy.
Suggests that labeling stock or file footage is an ethical practice that is often overlooked.
Notes that some people misconstrue laziness in news management as evidence of fake news.
Acknowledges the consumer demand for immediacy driving the use of stock footage.
Encourages freelancers to push for ethical standards in labeling their work.
Concludes by reminding viewers to understand how news media operates.

Actions:

for news consumers,
Push for news outlets to ethically label stock or file footage (suggested).
Hold major news networks accountable to ethical standards in visual content usage (exemplified).
</details>
<details>
<summary>
2020-04-11: Let's talk about opening up now.... (<a href="https://youtube.com/watch?v=gm6lnVCVFZM">watch</a> || <a href="/videos/2020/04/11/Lets_talk_about_opening_up_now">transcript &amp; editable summary</a>)

Advocate for science, history, and community safety over premature reopening for economic gain, prioritizing collective well-being.

</summary>

"You shouldn't need a cop for this. You don't need a cop. You need a science book and a history book."
"Easing up early now is wrong. It's the wrong move. We're winning, but we haven't won."
"If you think opening the US right now, opening everything back up, going back to business as usual is a good idea, you need to get a science book."
"He wants to be a leader. Then he has to lead."
"We have to do the bare minimum to keep each other safe and not sell each other out because our betters have told us to die for their profits."

### AI summary (High error rate! Edit errors on video page)

People are advocating to open the U.S. back up despite numbers indicating it's a bad idea.
Loyalty to the president is chosen over the well-being of neighbors.
Authoritarian measures by local governments wouldn't be necessary if there was proper leadership.
Lack of leadership from the president results in confusion among the American people.
Understanding basic science and history should guide individuals to take necessary precautions without police intervention.
Referencing the 1918 pandemic, Beau warns against easing restrictions too soon.
Advocating to reopen for economic benefit disregards risking friends and family.
The economic impact on the 99% should not be overlooked for the benefit of the 1%.
Lack of financial support from the government could lead to a collapse of the entire economic system.
Beau urges people to stop listening to the president and prioritize safety over economic interests.

Actions:

for community members, advocates,
Stay at home, wash hands, avoid touching face to keep each other safe (exemplified)
Advocate for financial support from the government to prevent economic collapse (suggested)
Educate others on the importance of science and history in making informed decisions (implied)
</details>
<details>
<summary>
2020-04-10: Let's talk about what we can learn from Arlington.... (<a href="https://youtube.com/watch?v=_1zlQPcabjg">watch</a> || <a href="/videos/2020/04/10/Lets_talk_about_what_we_can_learn_from_Arlington">transcript &amp; editable summary</a>)

Beau delves into the history of Arlington National Cemetery, proposes repurposing golf courses as visible sites for victims, and criticizes federal incompetence during the crisis.

</summary>

"We need to not overlook the issues related to unclaimed bodies and the proximity to major problems."
"Those golf courses, to me, they're perfect."
"They do not care about you."

### AI summary (High error rate! Edit errors on video page)

Explains the history of Arlington National Cemetery, which was established in the 1860s after the Battle of the Wilderness.
Mentions that Arlington was chosen for the cemetery due to its aesthetics, being already cleared, high ground to avoid flooding, and because it was Robert E. Lee's home.
Points out that Robert E. Lee's decision to fight against the Union led to his home becoming a national cemetery, serving as a political statement.
Draws a parallel between Arlington and Hart Island in New York, where unclaimed individuals are laid to rest.
Expresses the need to not overlook the issues related to unclaimed bodies and the proximity to major problems.
Suggests repurposing golf courses as visible sites for victims, contrasting with the hidden nature of Hart Island.
Criticizes the current president for lacking basic information about antibiotics and viruses.
Praises governors for their leadership in managing the crisis and combating federal incompetence.
Emphasizes the importance of public reminders and visible sites as a lesson for the future.
Advocates for repurposing golf courses as reminders of the current crisis.

Actions:

for public citizens,
Repurpose golf courses as visible sites for victims (suggested)
Advocate for public reminders and visible memorials (implied)
</details>
<details>
<summary>
2020-04-10: Let's talk about reopening Florida's schools.... (<a href="https://youtube.com/watch?v=FekR5HbW1k4">watch</a> || <a href="/videos/2020/04/10/Lets_talk_about_reopening_Florida_s_schools">transcript &amp; editable summary</a>)

Governor DeSantis' push to reopen schools in Florida disregards expert advice and risks educators' lives for political gain.

</summary>

"There's no reason to put these people at risk."
"I for one will yank my kids out of school."
"Our teachers' lives are not something the governor of Florida should be able to cash in."

### AI summary (High error rate! Edit errors on video page)

Governor DeSantis of Florida is considering reopening schools, despite experts advising against it.
DeSantis previously kept beaches open when experts recommended closing them, leading to negative consequences in South Florida.
DeSantis lacks understanding of the subject matter, evident from his actions such as cross-contaminating during a meeting.
Kids can catch COVID-19 and spread it, impacting not just children but also teachers, janitorial staff, and older family members they come in contact with.
Reopening schools solely to appease political figures like President Trump is illogical and risks the lives of educators.
Beau plans to remove his kids from school if they reopen, urging others to do the same to protect teachers from unnecessary risk.

Actions:

for florida residents, parents, educators,
Remove kids from school if reopened (exemplified)
Advocate for teachers' safety (exemplified)
Refuse to send children to school if deemed unsafe (exemplified)
</details>
<details>
<summary>
2020-04-09: Let's talk about talking points, rhetoric, and being lucky.... (<a href="https://youtube.com/watch?v=hfW0LSiAMoQ">watch</a> || <a href="/videos/2020/04/09/Lets_talk_about_talking_points_rhetoric_and_being_lucky">transcript &amp; editable summary</a>)

Beau addresses nationalism, advocates for global unity, and criticizes lackluster leadership during a time of crisis, stressing the importance of international cooperation in fighting against the current situation.

</summary>

"We are in this together."
"It doesn't care what color your passport is."
"This thing, it does not care about your borders."
"Do something worthy of being proud of."
"We are all in this together."

### AI summary (High error rate! Edit errors on video page)

Addresses the concept of nationalism and how it is being invoked in current rhetoric.
Points out the dangers of nationalism, especially during times of crisis.
Emphasizes the importance of global cooperation in fighting against the current situation.
Mentions the international support received in the form of medical supplies from various countries.
Advocates for pushing back against nationalist rhetoric and focusing on global unity.
Stresses the need for immediate cooperation and support across borders.
Warns about the ineffectiveness of trying to contain the situation by locking down borders.
Calls for proactive and worthy actions to be taken, rather than just invoking nationalism.
Criticizes lackluster leadership and the damage caused to the country's reputation.
Encourages staying at home, practicing good hygiene, and unity in the global fight against the situation.

Actions:

for global citizens,
Reach out to international organizations or individuals to offer support and aid (suggested)
Advocate for global cooperation and unity in fighting against the situation (implied)
Practice good hygiene, stay at home, and avoid touching your face (exemplified)
</details>
<details>
<summary>
2020-04-09: Let's talk about numbers, models, and confirmation bias.... (<a href="https://youtube.com/watch?v=Yht4551HDQk">watch</a> || <a href="/videos/2020/04/09/Lets_talk_about_numbers_models_and_confirmation_bias">transcript &amp; editable summary</a>)

Confirmation bias fuels skepticism in conservative circles about the severity of the pandemic, urging continued vigilance to prevent a resurgence.

</summary>

"Had this been done sooner, those numbers would be even lower."
"Half measures are half effective."
"We have to remain vigilant."
"It's more important than ever to push back against a false narrative like this."
"And if we don't, those models, they're just going to get longer."

### AI summary (High error rate! Edit errors on video page)

Confirmation bias occurs when individuals seek out evidence that supports their theory and ignore anything that contradicts it.
The conservative movement in the United States is currently suffering from confirmation bias, believing that the COVID-19 situation was overblown.
Some individuals argue that the lower numbers and revised models are proof that the pandemic was exaggerated by the media.
The lack of leadership in the country has led to governors taking on the role of acting president to combat the crisis.
Had certain actions been taken earlier, the impact of the pandemic could have been significantly reduced.
Despite the decreasing numbers, it's vital to remain vigilant and push back against false narratives to prevent a resurgence.
The importance of continuing precautions until a treatment is developed is emphasized to avoid a relapse.
There is a concern that if people believe the pandemic was overhyped and start resuming normal activities too soon, there could be a resurgence of cases.
Beau stresses the need for vigilance to prevent a second wave of infections and the importance of not letting our guard down prematurely.
Remaining cautious and pushing back against misinformation is key to ensuring the situation does not worsen.

Actions:

for public health advocates,
Remain vigilant and continue to follow safety guidelines to prevent a resurgence (implied)
Push back against misinformation and false narratives surrounding the pandemic (implied)
</details>
<details>
<summary>
2020-04-09: Let's talk about Bernie supporters and what could be.... (<a href="https://youtube.com/watch?v=pqYVdvauW6A">watch</a> || <a href="/videos/2020/04/09/Lets_talk_about_Bernie_supporters_and_what_could_be">transcript &amp; editable summary</a>)

Bernie supporters urged to build worker co-ops, organize, and seize the current pause in the economy to create systemic change - it's time to lead ourselves with action, not just votes.

</summary>

"Build your worker co-op. Organize. Educate."
"This is the time to do it."
"It's a wide cross-section. All it takes is organization."
"Not with your vote, but with your action."
"It's time to lead ourselves."

### AI summary (High error rate! Edit errors on video page)

Urges Bernie supporters to take action post-election.
Encourages building worker co-ops and organizing.
Suggests utilizing the current economic pause to make systemic changes.
Points out the availability of cheap retail and restaurant spaces post-pandemic.
Emphasizes the need for workers to lead themselves and create change.
Advocates for action over waiting for political change.
Stresses the importance of organizing and educating co-workers.
Mentions the possibility of creating alternatives to existing systems.
Advises taking advantage of the chaos for positive change.
Calls for demonstrating that alternative ideas can work practically.

Actions:

for bernie supporters,
Build worker co-ops, organize, and educate co-workers to create systemic change (exemplified)
Take advantage of available cheap retail or restaurant spaces to start new ventures (implied)
Create alternatives to existing systems, like a worker-friendly Uber Eats competitor (implied)
Lead by example and show practical success of alternative ideas through action (exemplified)
</details>
<details>
<summary>
2020-04-08: Let's talk with Ti.... (<a href="https://youtube.com/watch?v=8BZ0QsapPg8">watch</a> || <a href="/videos/2020/04/08/Lets_talk_with_Ti">transcript &amp; editable summary</a>)

Beau connects with a content creator discussing their shift from beauty to social issues, political frustrations, pandemic profiteering, amplifying marginalized voices, and the importance of compassion in a challenging world.

</summary>

"Compassion is not for the weak. Empathy is for the strong."
"I'm fighting for a lot of the things that my ancestors were fighting for."

### AI summary (High error rate! Edit errors on video page)

Came across a video that kept appearing in his feed about hair growth tips that led to connecting with tonight's guest.
They dive into the guest's evolution from beauty and fashion content to discussing social issues like racism and white privilege.
The guest felt compelled to speak out on politics, particularly about Biden, due to frustrations with the DNC and mainstream media.
Despite risking subscribers, the guest used their platform to express dissatisfaction with the political landscape.
The guest hosts Saturday night live streams during quarantine, discussing pandemic profiteering and supporting local businesses.
The ongoing theme of the guest's content revolves around addressing privilege, inequity, and amplifying marginalized voices.
Both Beau and the guest touch on the responsibility of using their platforms to reach and impact their audience.
The guest shares insights into staying at home during the pandemic and their background as a writer for digital content.
They touch on the importance of voting and the challenges faced during primary elections amid voter suppression concerns.
The guest expresses their perspective on the upcoming election, advocating for voting ethics and sharing their views on Biden and third-party options.

Actions:

for content creators, social justice advocates,
Support local businesses to counter pandemic profiteering (implied)
Exercise compassion and empathy in interactions and activism (implied)
Take steps to address privilege and inequity in your community (implied)
</details>
<details>
<summary>
2020-04-07: Let's talk about stress and getting out of the house virtually.... (<a href="https://youtube.com/watch?v=kFhXb6LmMyY">watch</a> || <a href="/videos/2020/04/07/Lets_talk_about_stress_and_getting_out_of_the_house_virtually">transcript &amp; editable summary</a>)

Beau provides tips for managing stress, suggests engaging in activities at home, and addresses concerns about shortness of breath and anxiety during challenging times.

</summary>

"There's no reason to stress yourself out."
"There are going to be mental health ramifications to this."
"Y'all try to have a good day."

### AI summary (High error rate! Edit errors on video page)

Provides tips for managing stress during challenging times.
Suggests engaging in activities at home to alleviate boredom and cabin fever.
Mentions Zooniverse as a platform for participating in crowd-sourced research projects.
Talks about the variety of projects on Zooniverse, not just space-related ones.
Encourages helping scientists with galaxy identification or exploring old manuscripts.
Emphasizes the value of learning and contributing to society through these activities.
Acknowledges the surplus time people have now and the drive for self-improvement.
Advises against putting excessive pressure on oneself during this period.
Introduces virtualmuseums.io for taking virtual tours of museums worldwide.
Recommends virtual museum tours as a captivating activity, especially for kids.
Points out the abundance of tools available now for exploration.
Addresses questions about shortness of breath as a symptom and managing anxiety.
Suggests differentiating between anxiety-related symptoms and actual health concerns.
Recommends seeking coping mechanisms and managing anxiety through available resources.
Acknowledges the mental health implications of the current situation and the importance of addressing them promptly.

Actions:

for individuals seeking stress management tips and coping mechanisms.,
Participate in crowd-sourced research projects on platforms like Zooniverse (implied).
Take virtual tours of museums on virtualmuseums.io (implied).
Seek out videos on coping mechanisms and anxiety management (implied).
</details>
<details>
<summary>
2020-04-07: Let's talk about hospitals refusing to allow their staff to bring in supplies.... (<a href="https://youtube.com/watch?v=2bP2bKVyv5M">watch</a> || <a href="/videos/2020/04/07/Lets_talk_about_hospitals_refusing_to_allow_their_staff_to_bring_in_supplies">transcript &amp; editable summary</a>)

Hospitals denying staff protective equipment prioritize profit over lives, risking safety and facing accountability from donors.

</summary>

"If they can get the equipment, and I don't care how they get it, if they can get it, let them use it."
"You refuse to allow your staff to protect themselves."
"You had money, too, and you chose to spend that money on bonuses for the administrative staff."
"We will be going out of our way to choke off funding until those people responsible for this decision are no longer employed."

### AI summary (High error rate! Edit errors on video page)

Hospitals in metropolitan areas are refusing to let staff bring in needed protective equipment that was donated or scrounged up.
Reasons provided include concerns about not everyone getting the equipment, it being counterfeit, and unauthorized.
Beau points out the flaw in these reasons, urging hospitals to prioritize those dealing with high-risk patients.
He notes that even if the equipment is counterfeit, it's better than nothing, especially considering the current shortage.
The focus on bottom line and PR seems to be the driving force behind these refusals, with money being spent on bonuses rather than necessary supplies.
Beau vows to contact private donors of hospitals that deny staff protection, aiming to cut off funding until those responsible are no longer employed.

Actions:

for hospital staff, donors,
Contact private donors of hospitals denying staff protection (implied)
</details>
<details>
<summary>
2020-04-06: Let's talk about what we've learned.... (<a href="https://youtube.com/watch?v=H7jYp72j2nk">watch</a> || <a href="/videos/2020/04/06/Lets_talk_about_what_we_ve_learned">transcript &amp; editable summary</a>)

Beau challenges the belief in government protection, urging individuals to prepare for emergencies and prioritize their safety over governmental support.

</summary>

"The reality is it's not really good at its job."
"Government is a very large, unwieldy tool."
"You have to protect yourself."
"From where I'm sitting, that very question damages the faith."
"It doesn't matter how well prepared you are."

### AI summary (High error rate! Edit errors on video page)

Challenges the belief in governments being effective at protecting citizens.
Governments sometimes cut teams and measures meant for protection due to cost.
Urges individuals to take responsibility for their own protection.
Describes government as a tool for promoting welfare and securing freedom.
Warns about the dangers of government turning against its citizens.
Encourages people to make a list of necessary items and be prepared for emergencies.
Emphasizes the importance of individual preparedness, especially in the face of future challenges like climate change.
Criticizes government leaders who prioritize economic benefit over lives during crises.
Expresses disappointment in the government's lack of protection and preparedness.
Calls for individuals to be proactive in preparing for emergencies to maintain their standard of living.

Actions:

for prepared individuals,
Make a list of necessary items for emergencies (implied)
Prepare for future emergencies by slowly acquiring needed supplies (implied)
Ensure personal preparedness for emergencies (implied)
Stay at home and wear a mask if going out (implied)
</details>
<details>
<summary>
2020-04-05: Let's talk about masks.... (<a href="https://youtube.com/watch?v=bgmt3yeoiUU">watch</a> || <a href="/videos/2020/04/05/Lets_talk_about_masks">transcript &amp; editable summary</a>)

Beau advises wearing masks, repurposing items as masks, and setting a good example in the absence of real leadership to protect oneself and society during the pandemic.

</summary>

"Wear the mask. If you have the opportunity, wear the mask."
"We are in this together and we will get through it together."
"Now nobody has to know that you're not invincible."
"You paid nine bucks for it, get some use out of it."
"We do not have real leadership, we have to lead ourselves."

### AI summary (High error rate! Edit errors on video page)

Advises wearing masks to protect oneself, loved ones, and society.
Mentions President not wearing a mask despite the advice.
Adjusted his beard to be mask compliant.
States his mask seals at the neck for better protection.
Emphasizes the importance of setting a good example in the absence of real leadership.
Addresses men's reluctance to wear masks due to questioning their invincibility and toughness.
Demonstrates how to repurpose a tactic cool item into a mask.
Encourages using any available item as a mask if needed.
Stresses the significance of taking precautions seriously.
Offers to do a live stream bonfire to combat boredom and foster community spirit.

Actions:

for community members,
Use any available item as a mask if needed (exemplified)
Join Beau's live stream bonfire for community engagement (suggested)
</details>
<details>
<summary>
2020-04-04: Let's talk about unfinished dreams and MLK.... (<a href="https://youtube.com/watch?v=hWwRks143gA">watch</a> || <a href="/videos/2020/04/04/Lets_talk_about_unfinished_dreams_and_MLK">transcript &amp; editable summary</a>)

April 4th, 1968 marked MLK's loss, but Beau focuses on his unrealized vision of uniting for a just society today.

</summary>

"Everything's impossible until it isn't."
"We collectively. The people at the bottom."
"If we are supposed to be a government of the people, by the people, and for the people, why doesn't it ever represent the people?"

### AI summary (High error rate! Edit errors on video page)

April 4th, 1968 marks the day Martin Luther King was lost in Memphis.
King was working on the Poor People's Campaign, uniting across racial, religious, and gender lines.
The campaign aimed for housing, living wage, healthcare—issues still relevant today.
Beau wonders what could have been if King hadn't been lost in 1968.
King had already achieved the "impossible," understanding the need for societal change.
The current situation reveals a lack of government interest in the people.
Beau urges solidarity among communities, not a left-right but an up-down alliance.
People at the bottom hold power in reshaping society.
The economy won't recover without collective action.
It's time to push for a just society with basic necessities for everyone.

Actions:

for community members,
Show solidarity through community support (implied)
Push for a society that works for everybody (implied)
Advocate for housing, healthcare, and living wage (implied)
</details>
<details>
<summary>
2020-04-04: Let's talk about PR in a time of crisis.... (<a href="https://youtube.com/watch?v=U-nNSV2xT40">watch</a> || <a href="/videos/2020/04/04/Lets_talk_about_PR_in_a_time_of_crisis">transcript &amp; editable summary</a>)

Beau criticizes the Trump administration's PR-focused response, pointing out the lack of leadership in crisis management and urging voters to think carefully about future choices.

</summary>

"It's all PR. Everything they've done is PR."
"There's no substance. There's no leadership."
"The administration isn't made up of leaders. It's made up of panderers."
"Every move he makes is for PR."
"Without real leadership, it's going to be rough."

### AI summary (High error rate! Edit errors on video page)

Critiques the Trump administration's handling of the current crisis.
Points out the administration's focus on PR stunts rather than real solutions.
Calls out Jared Kushner for giving advice to voters instead of managing the crisis.
Questions the competence of the administration's leadership in managing the response.
Mentions the administration's failure to properly maintain the strategic national stockpile.
Criticizes the administration's use of PR to cover up mistakes and missteps.
Talks about the administration's failure to follow through on promises like ordering ventilators.
Emphasizes the lack of substance and leadership in the Trump administration's response.
States that medical professionals are working despite the lack of leadership from the administration.
Urges people to think carefully about who they vote for, considering future crises.

Actions:

for voters, concerned citizens,
Question and critically analyze political leadership (implied)
Stay informed about government actions and responses (implied)
Vote thoughtfully and responsibly in upcoming elections (implied)
</details>
<details>
<summary>
2020-04-03: Let's talk about where Americans can find leadership.... (<a href="https://youtube.com/watch?v=c7sg6Y19hUE">watch</a> || <a href="/videos/2020/04/03/Lets_talk_about_where_Americans_can_find_leadership">transcript &amp; editable summary</a>)

Beau criticizes the lack of leadership in the Oval Office during the crisis and urges individuals to take charge and inform themselves amidst governmental inadequacies.

</summary>

"Leadership is telling a crowd of people what they don't want to hear, but doing it in a way that motivates them to get through it."
"Aside from that, I suggest that maybe making sure that all of your first responders get sick is probably a bad idea."
"You have to be responsible as an individual."
"Most of the people we have in positions of power aren't leaders. They're panderers or they're entertainers."
"You have to be a leader."

### AI summary (High error rate! Edit errors on video page)

Criticizes the lack of leadership in the Oval Office during the current crisis.
Points out that leadership involves telling people what they don't want to hear but motivating them through it.
Notes the difference between leading and pandering to crowds.
Expresses disappointment in the elected entertainer instead of a leader.
Talks about governors taking action in the absence of federal leadership.
Warns against authoritarian measures like state troopers at borders.
Advises against putting first responders at risk of getting sick due to poor decisions.
Comments on a community considering door-to-door welfare checks, expressing reluctance.
Mentions a plan for welfare checks with color-coded signals.
Criticizes the lack of leadership and calls for individuals to take charge and inform themselves.
Emphasizes the importance of education and proper information in making decisions.
Urges people to follow basic guidelines like staying home, washing hands, and not touching faces.
Advocates for minimizing risk for essential workers by being efficient with trips outside.
Questions the effectiveness of government in managing citizen health during this crisis.
Encourages individuals to take responsibility since government leadership seems inadequate.

Actions:

for citizens, individuals,
Stay informed and educated on the current situation (implied).
Follow basic guidelines like staying home, washing hands, and not touching faces (implied).
Minimize risk for essential workers by being efficient with trips outside (implied).
Take responsibility as an individual and for society as a whole (implied).
</details>
<details>
<summary>
2020-04-03: Let's talk about wearing scrubs in public.... (<a href="https://youtube.com/watch?v=91qt_E3H3rM">watch</a> || <a href="/videos/2020/04/03/Lets_talk_about_wearing_scrubs_in_public">transcript &amp; editable summary</a>)

Confronting medical professionals over scrubs in public is irrational and dangerous, support rather than harass healthcare workers during these challenging times.

</summary>

"Medical professionals certainly have enough on their plate. They don't need to be worried about being harassed when they stop to pick up essentials."
"Nurses are like the mob. If you harass a nurse, every nurse in the country is gonna know your face."
"Doing that [proper handwashing] would be a whole lot more effective at keeping you safe than harassing some nurse or doc out in public."
"We have enough problems in society right now. We don't need a bunch of people running around causing more problems out of ignorance and fear."
"If you don't want to say thank you, just stay away."

### AI summary (High error rate! Edit errors on video page)

Medical professionals are being confronted in public for wearing scrubs, which is causing concern.
People are harassing doctors, nurses, and techs in grocery stores and parking lots about their scrubs.
It's irrational to harass medical professionals and get closer to them when concerned about potential illness transmission.
Droplets may be released if medical professionals are upset and raise their voice due to harassment.
Fabric doesn't harbor viruses well, unlike hard surfaces where viruses can live longer.
Nurses and doctors take precautions like changing shoes, using booties, and disinfecting before leaving the hospital.
People harassing medical professionals in public likely do not take the same precautions.
Not everyone wearing scrubs has been in contact with infected patients.
Medical professionals disinfect their belongings regularly, a practice that others may not follow.
Proper hand-washing techniques, like focusing on fingertips and thumbs, are more effective than harassing healthcare workers.
PPE shortage is a concern, and harassing healthcare workers is not a solution.
Nurses are a close-knit community, and harassing one could lead to repercussions from others in the field.
Harassing medical professionals adds unnecessary stress to their already challenging work environment.
Beau suggests contacting representatives about the PPE shortage instead of harassing healthcare workers.
Medical professionals need support, not harassment, especially during times of crisis.

Actions:

for healthcare supporters,
Call your representatives about the PPE shortage (suggested)
Practice proper handwashing techniques (implied)
Support medical professionals instead of harassing them (implied)
</details>
<details>
<summary>
2020-04-02: Let's talk about religious facilities right now.... (<a href="https://youtube.com/watch?v=9R_pLBRXmEw">watch</a> || <a href="/videos/2020/04/02/Lets_talk_about_religious_facilities_right_now">transcript &amp; editable summary</a>)

Beau raises concerns about risky congregational gatherings, advocating for livestreaming services and following best practices during the pandemic.

</summary>

"You owe me precisely zero explanation, none."
"Livestream. Livestream."
"In a moment of doubt, that's when it overcame them."
"Please do your part. Wash your hands. Don't touch your face. Stay home."
"You know what best practices are."

### AI summary (High error rate! Edit errors on video page)

Acknowledges people pausing or canceling Patreon support, assures no need for explanations.
Raises concern about religious leaders holding congregations despite risks in the current climate.
Advocates for livestreaming religious services as a safer alternative to in-person gatherings.
Points out that people tend to let their guard down in religious facilities, engaging in risky behaviors.
Questions the extreme test of faith in expecting divine protection during a pandemic.
Suggests using livestreaming platforms like YouTube for congregations to avoid physical gatherings.
Urges people to wash hands, avoid touching faces, and stay home to follow best practices.
Stresses the importance of not needing government orders to understand staying home is necessary.

Actions:

for religious leaders,
Livestream religious services to avoid risky in-person gatherings (exemplified).
Wash your hands, avoid touching your face, and stay home (exemplified).
</details>
<details>
<summary>
2020-04-02: Let's talk about 7 pitches and opportunity.... (<a href="https://youtube.com/watch?v=xhQLU212qFE">watch</a> || <a href="/videos/2020/04/02/Lets_talk_about_7_pitches_and_opportunity">transcript &amp; editable summary</a>)

Be prepared, plan ahead, and seize opportunities to achieve success, even during tough times.

</summary>

"She, 17-year-old. That is a once in a lifetime thing right there."
"Most people get struck out a whole lot more than they hit a home run."
"As we come out of this is the time to make your move."

### AI summary (High error rate! Edit errors on video page)

Recounts a significant baseball game from 1931 involving a 17-year-old pitcher named Mitchell facing Babe Ruth and Lou Gehrig.
Mitchell struck both Ruth and Gehrig out, a rare feat that doesn't happen often.
Draws a parallel between hitting a home run in baseball and achieving success in life.
Mentions that during tough times, like the current economic situation, there are opportunities for those on the lower end of the income spectrum to rise.
Encourages utilizing the current period to figure out goals and make concrete plans for future success.
Shares anecdotes of friends who found financial success during a recession by planning ahead.
Urges viewers to identify what they want and strategize how to achieve it, especially during economic downturns.
Emphasizes the importance of being prepared and ready to take action when the economy improves.
Suggests starting small and growing gradually, taking advantage of competitors' weaknesses and customer dissatisfaction.
Calls for viewers to be proactive in pursuing their goals and dreams, leveraging the current downtime to plan for future success.

Actions:

for entrepreneurs, dreamers, planners.,
Identify your goals and what you want to achieve during this downtime (suggested).
Develop a concrete plan outlining steps you can take to reach your goals (suggested).
Be proactive in preparing for future opportunities as the economy improves (implied).
</details>
