---
title: Let's talk about what we've learned....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=H7jYp72j2nk) |
| Published | 2020/04/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Challenges the belief in governments being effective at protecting citizens.
- Governments sometimes cut teams and measures meant for protection due to cost.
- Urges individuals to take responsibility for their own protection.
- Describes government as a tool for promoting welfare and securing freedom.
- Warns about the dangers of government turning against its citizens.
- Encourages people to make a list of necessary items and be prepared for emergencies.
- Emphasizes the importance of individual preparedness, especially in the face of future challenges like climate change.
- Criticizes government leaders who prioritize economic benefit over lives during crises.
- Expresses disappointment in the government's lack of protection and preparedness.
- Calls for individuals to be proactive in preparing for emergencies to maintain their standard of living.

### Quotes

- "The reality is it's not really good at its job."
- "Government is a very large, unwieldy tool."
- "You have to protect yourself."
- "From where I'm sitting, that very question damages the faith."
- "It doesn't matter how well prepared you are."

### Oneliner

Beau challenges the belief in government protection, urging individuals to prepare for emergencies and prioritize their safety over governmental support.

### Audience

Prepared individuals

### On-the-ground actions from transcript

- Make a list of necessary items for emergencies (implied)
- Prepare for future emergencies by slowly acquiring needed supplies (implied)
- Ensure personal preparedness for emergencies (implied)
- Stay at home and wear a mask if going out (implied)

### Whats missing in summary

Importance of individual action and preparedness in ensuring safety and security during crises.

### Tags

#Government #Preparedness #EmergencyPreparedness #IndividualResponsibility #Safety


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to go through the looking glass.
We're going to act as if we're already on the other side of this.
We're going to enter wonderland where all of this is gone.
So what have we learned during this?
We've learned that governments in general are pretty ineffective at protecting us.
It's often the claim that a government is there to protect you.
But the reality is it's not really good at its job.
In fact, often times it will cut the very teams and measures there to protect you because
they're expensive.
It's not the government's real purpose.
It's not a protectorment, it's a government.
It's not there to protect you.
You have to protect yourself.
In situations like this it becomes more and more apparent.
Government is a very large, unwieldy tool.
It is a tool, in its best case, it is a tool that is there to promote the general welfare
and secure its citizens freedom and personal well-being.
In its best case.
In its worst case, it's a tool that gets turned against you.
I would hope that most people are making a list of all of the things they needed during
this and making the decision to become prepared afterward.
A lot of this stuff that's needed, it's inexpensive, it has an almost indefinite shelf life, and
you can just buy it slowly over time because this isn't the big one.
This isn't the big one in this sense and it's not the most dangerous situation that we're
facing.
This is nothing compared to the issues that are going to arise with climate change.
You've got to be prepared.
The government isn't going to do it for you.
They don't have a plan and understand that at some point there was probably a verbal
conversation and if not, it was guiding every thought that was there.
How many people can die before it becomes politically untenable?
That was a question that occurred when you had governors delaying giving stay at home
advisories.
That's what they were thinking.
How many people can die before I really damage my reputation, before I damage myself, before
I damage faith in the government?
From where I'm sitting, that very question damages the faith.
The reality is there was a scale.
Economic benefit, you, your friends, and your family's lives.
At some point, one weighed out over the other.
That was a thought process that occurred.
If you still believe the government is there to protect you, understand we're all expendable.
We are all expendable in the pursuit of stability, which is what governments want.
They're not protectorments.
They're governments.
They are there to maintain calm.
You had some that took the lead, but most were really working that balance sheet to
see whether or not your life was going to be the tipping point that would turn people
against them.
Make a list of everything that you need.
Everybody, it doesn't matter how well prepared you are, you have things that you wish you
had right now.
I wish I had shop towels, the blue ones that come in a roll, because I could make my own
disinfectant wipes with them.
There's a whole bunch of stuff.
It doesn't matter how well prepared you are.
I'm one of those people.
I always have food and water and medicine, all of that stuff.
I have cleaning supplies, but I didn't have cleaning supplies that were ease of use, geared
towards ease of use.
I can make disinfectant with what I have, but not in the easy way.
That's something I would like.
At some point, when you start preparing, you get past basic survival and you get to the
point where you're talking about standard of living.
It would be nice if everybody was at that point.
When this is over, please do what you can to prepare.
Do what you can to put yourself in a situation where the next time there's panic buying,
you don't have to leave home.
It's something we talk about on this channel a lot, and it's something that we're going
to go into depth on once this is over.
Right now, there's not a lot people can do because you can't go anywhere.
You shouldn't be going anywhere.
If you do, wear a mask.
For those that don't know, that's why I had to make my beard N95 compliant, so here we
are.
Wash your hands.
Don't touch your face.
Stay at home.
If you have to go out, wear a mask.
Do your part to end this.
This doesn't have to be a massive thing, and we can restart the economy afterward.
Right now, what's important is human life.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}