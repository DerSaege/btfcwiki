---
title: Let's talk about origin stories, blame, and irrelevant debates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ly0Q5Cj8pNM) |
| Published | 2020/04/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Every villain needs an origin story, and the current worldwide villain's origin is debated between a market and a lab.
- Despite the origin debate, Beau doesn't care because it doesn't change the need for mitigation efforts.
- Beau believes that focusing on blame is a distraction from saving lives and improving international health security.
- Antibodies don't guarantee immunity, and people shouldn't let their guard down based on that belief.
- Beau stresses the importance of prioritizing immediate needs in the ongoing fight against the pandemic over assigning blame.
- Security standards in labs need to be raised to prevent future leaks, but finding a solution should be the priority over finding a scapegoat.

### Quotes

- "I don't care."
- "We have to worry more about saving lives than saving political careers at this point."
- "It's going to go on. I think we should focus on that."
- "Right now, rather than trying to find a scapegoat, we need to find a solution."
- "Y'all have a good night."

### Oneliner

Beau stresses the importance of focusing on saving lives and finding solutions rather than assigning blame in the ongoing fight against the pandemic.

### Audience

Global citizens

### On-the-ground actions from transcript

- Increase security standards at BSL-4 facilities (implied)
- Support the World Health Organization for effective oversight (implied)
- Prioritize saving lives over political interests (implied)

### Whats missing in summary

Beau's passionate delivery and emphasis on the immediate need to prioritize saving lives over engaging in irrelevant blame games.

### Tags

#OriginStories #PandemicResponse #CommunityHealth #GlobalSolidarity #LabSafety


## Transcript
Well howdy there internet people, it's Bo again.
So today we're gonna talk about origin stories,
blame, and irrelevant discussions.
So, every villain needs an origin story, right?
I mean, that's screenwriting 101.
Every villain needs an origin story.
The current worldwide villain,
the general consensus is that it originated in a market.
Now if you've missed the news,
US intelligence has not ruled out that it leaked out of a lab.
They're looking into a unverified report.
And they've gone out of their way now to say that it's premature to say anything,
blah, blah, blah, blah.
But they had already said that, already planted the seeds of the story.
I've had people from both sides, both camps,
people who think it originated in the market, people who think it originated in
a lab, smart people, try to convince me of their theory.
And they both said the same thing.
They're like, you're being very dismissive of this.
Why are you ignoring this?
Because I don't care, that's the simplest, most direct answer.
I don't care.
I don't care for a number of reasons.
One, I think this debate, this discussion,
only benefits those people in power in countries that bungled the response.
See, only people it helps.
They messed it up, now they need somebody to blame.
Who better than a foreign country?
It's really that simple.
I also think it's kind of irrelevant, and I don't think it's a good use of our time,
because it doesn't change anything.
Let's say, for example, later today, news breaks that it came from a lab.
Cool.
Does that mean you stop washing your hands?
You go out and lick a doorknob?
Of course not.
Doesn't change anything.
Right now, we should be focused on mitigation.
We're having a hard enough time with that, as it is.
I don't know that we need to be distracted.
I would also point out to the conservatives who are watching this, because it is generally
conservatives who go with the lab theory.
Not always, but generally.
You may not want to do that.
That may not be a good argument to push, because again, for the sake of argument, let's just
say, yeah, sure, that's where it came from.
What's the response?
We have to make sure it doesn't happen again.
Absolutely.
We have to increase security at BSL-4 facilities.
There's 50 of them all over the world.
Who would do the best job overseeing that?
That's not a question.
Who?
The World Health Organization would do the best job overseeing that.
The organization that Donald Trump is currently withholding funds from.
I'm not sure this is a theory you want to push in order to save his career.
That doesn't seem like it's going to go the way you think it will.
I would suggest that we worry more about saving lives than saving political careers at this
point.
This isn't over.
Since we're talking about things that haven't been ruled out, I would point out, there's
a whole lot of people going around right now that's like, well, I found out I had antibodies.
That doesn't mean that you're immune.
That doesn't mean that you're immune.
It could mean that.
It could.
But we don't know that yet.
It could mean that you're just going to have a better chance of fighting it off next time.
Or it could mean nothing.
We're still early in this fight.
We don't have a lot of information.
That's a dangerous belief.
Because if you believe simply because you have antibodies that you're immune, totally
immune, you may not be taking the precautions you should.
Right now, despite the reopenings, the ill-advised reopenings, we're still in this fight.
And it's going to go on.
I think we should focus on that.
We can worry about where it originated and how to solve that problem at a later date.
Right now, the people who are currently in jeopardy, most immediate need first.
They're the most immediate need.
We have to mitigate still.
That doesn't mean don't look into it.
Because if it did come from there, yeah, we need to beef up security.
We need to raise international standards.
I would point out that other leaks like that have happened.
One country, the United States, mailed vials, live samples, everywhere, accidentally.
Another country, well actually, it was also the United States, left vials in a cardboard
box just like unsecured, because why not?
Human error is a thing, even in labs like that.
So we probably should look into raising security standards there.
That's probably a good idea.
But right now, rather than trying to find a scapegoat, we need to find a solution.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}