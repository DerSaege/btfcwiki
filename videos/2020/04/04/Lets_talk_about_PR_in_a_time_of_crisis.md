---
title: Let's talk about PR in a time of crisis....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U-nNSV2xT40) |
| Published | 2020/04/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques the Trump administration's handling of the current crisis.
- Points out the administration's focus on PR stunts rather than real solutions.
- Calls out Jared Kushner for giving advice to voters instead of managing the crisis.
- Questions the competence of the administration's leadership in managing the response.
- Mentions the administration's failure to properly maintain the strategic national stockpile.
- Criticizes the administration's use of PR to cover up mistakes and missteps.
- Talks about the administration's failure to follow through on promises like ordering ventilators.
- Emphasizes the lack of substance and leadership in the Trump administration's response.
- States that medical professionals are working despite the lack of leadership from the administration.
- Urges people to think carefully about who they vote for, considering future crises.

### Quotes

- "It's all PR. Everything they've done is PR."
- "There's no substance. There's no leadership."
- "The administration isn't made up of leaders. It's made up of panderers."
- "Every move he makes is for PR."
- "Without real leadership, it's going to be rough."

### Oneliner

Beau criticizes the Trump administration's PR-focused response, pointing out the lack of leadership in crisis management and urging voters to think carefully about future choices.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Question and critically analyze political leadership (implied)
- Stay informed about government actions and responses (implied)
- Vote thoughtfully and responsibly in upcoming elections (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the Trump administration's response to the current crisis, focusing on their emphasis on PR over real solutions and lack of leadership in crisis management.

### Tags

#TrumpAdministration #CrisisManagement #Leadership #PRStunts #Voting


## Transcript
Well, howdy there, internet people.
It's Beau again.
So tonight we're gonna talk about public relations
during the times we are in.
There were a few people who voiced concern
that I brought the administration into that last video
and talked about their leadership.
The two things are inseparable.
You cannot talk about what's going on right now
without talking about the utter failure
that is the Trump administration.
Can't do it.
Now, at the same time that people are saying,
oh, we need to rally behind him and leave politics out of this,
which I can understand on some level.
They have no problem with the administration
using this crisis for PR stunts,
and they are PR stunts.
And the person who is supposed to be focused
on leading us through this,
spearheading the federal response, Jared Kushner,
is out giving advice to voters, campaigning,
but we can't talk about the administration.
Doesn't seem to make sense to me.
Jared Kushner told voters to think about
who will be a competent manager during a crisis.
I agree.
And he's saying this without any hint of irony
as the response he is spearheading
is shipping out broken ventilators
and rotten N95 masks.
Does not seem like competent management to me.
Then he talks about the strategic national stockpile.
And let's just say that he misspoke,
not that he intentionally lied to cover his own rear.
Just an accident.
And he said that, you know, this is the purpose of it.
It's for this.
And everybody who knows what it's for is like,
no, that's just not true.
But PR took over.
Rather than actually managing the situation,
they had to manage a PR crisis.
That's more important than what's actually going on.
They literally went to the website
and changed the mission statement
to match the idiocy that he said.
As if, A, people who are familiar with it wouldn't notice,
and B, it's not all archived.
That is some Orwellian 1984 stuff right there.
The dear leader's son-in-law made a boo-boo speaking.
So we're gonna change the mission of a federal agency.
Because this administration cannot accept responsibility
for anything.
It's all PR.
Everything they've done is PR.
We're gonna send ships, make sure it go down,
get a good photo op, make sure everybody knows
that the dear leader did it.
President Trump.
And then they get to where they're going.
1,000-bed ship.
Got 20 patients, which I get.
You don't want to bring the high-risk patients on the ship.
I can kind of understand that for real.
I get it.
But it's the Navy.
I imagine those ventilators are portable.
They could go to where they're needed.
I bet those aren't broke.
But they were maintained.
And you can blame the state of the stockpile on Obama or Bush
or whoever.
Doesn't matter.
Because this administration has had three years to fix it.
And this threat was not a secret.
It wasn't.
This has been known about.
They chose to ignore it.
They chose to focus more on re-election campaigns
than the safety of those people who would be voting.
Well, we're going to have the automotive company.
We're going to have them make ventilators.
Production Act.
OK, it sounds good.
It's good PR.
Great.
But see, the administration never actually sent the order.
So they're not taking orders for ventilators.
Because there's no follow-through.
It's just like the entire Trump brand.
It's all a facade.
It's all show.
It's all PR.
There's no substance.
There's no leadership.
And at every opportunity, the administration
is undermining the few medical professionals who are still
willing to work with them.
Because most of them can't put up with them.
The CDC advises that Americans should wear a mask.
But I'm not going to do it, because I'm a tough guy.
Sure you are.
In a lot of states, like here in Florida,
it seems right now that the administration is doing
an OK job by the metric that they use.
Poll numbers.
That's all they care about.
They do not care about your life.
They never did.
They care about those poll numbers.
And right now, they're plus or minus 5 points.
Not a big deal.
Not a big deal at all.
But see, that's going to change, because we're still
at the beginning of this.
We are still at the beginning of this.
And Floridians just found out that the last Republican
governor pretty much trashed their chances
of getting unemployment in a timely fashion.
That's probably going to weigh on their minds.
It's also probably going to weigh on their minds
when they find out that every VA hospital now
has a reefer truck sitting outside of it.
This isn't over.
Now, there are medical professionals
out there doing their jobs, working day and night
trying to mitigate this.
If they manage to, and they manage
to avert a major catastrophe, it will
be in spite of the Trump administration,
not because of it.
The administration has offered zero leadership, none.
They will attempt to take the credit if it's successful.
And if it's not, they'll blame everybody else,
because the administration isn't made up of leaders.
It's made up of panderers.
They will tell you what you want to hear and hope for the best
and hope they aren't discovered.
That's what they are.
It's what they've always been.
It is impossible to talk about this situation
without talking about the administration.
I happen to know a little bit about crisis management.
The number one rule is that you're prepared for it.
You activate the contingency plans
that you planned out decades in advance.
You're prepared.
You're ready.
You don't downplay it.
And you listen to the experts.
These are things the administration has not done
and will not do, because their egos will get in the way.
I want you to think about the amount of time that
must have been devoted to deciding to change the website,
because dear leader's son misspoke, son-in-law.
They're covering all their bases there,
but they're not doing it when it matters to your life.
Yeah, I think Kushner is right.
You need to think about who would be a competent manager
in a time of crisis, because the reality is
this didn't have to be one.
It didn't have to be one.
Go back to the first video I filmed on this.
I was completely unworried, because I'm
familiar with the plans.
I know what the government is capable of in this regard
if they're allowed to do their jobs,
if they don't have an administration hamstringing them.
That didn't happen.
It didn't happen because he was worried about his re-election.
He was worried about the economy.
He was worried about himself, not about the country.
Every move he makes is for PR.
I would love somebody to prove me wrong.
Kushner's right.
You need to think long and hard if you're
going to vote in 2020 who you're going to vote for,
because this didn't have to be like this.
And it's very likely that there will
be another crisis in the next four years,
there will be another crisis in the next four years that's
going to be bigger, because we're
going to be heading into some really bumpy times.
And without real leadership, it's going to be rough.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}