---
title: Let's talk about unfinished dreams and MLK....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hWwRks143gA) |
| Published | 2020/04/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- April 4th, 1968 marks the day Martin Luther King was lost in Memphis.
- King was working on the Poor People's Campaign, uniting across racial, religious, and gender lines.
- The campaign aimed for housing, living wage, healthcare—issues still relevant today.
- Beau wonders what could have been if King hadn't been lost in 1968.
- King had already achieved the "impossible," understanding the need for societal change.
- The current situation reveals a lack of government interest in the people.
- Beau urges solidarity among communities, not a left-right but an up-down alliance.
- People at the bottom hold power in reshaping society.
- The economy won't recover without collective action.
- It's time to push for a just society with basic necessities for everyone.

### Quotes
- "Everything's impossible until it isn't."
- "We collectively. The people at the bottom."
- "If we are supposed to be a government of the people, by the people, and for the people, why doesn't it ever represent the people?"

### Oneliner
April 4th, 1968 marked MLK's loss, but Beau focuses on his unrealized vision of uniting for a just society today.

### Audience
Community members

### On-the-ground actions from transcript
- Show solidarity through community support (implied)
- Push for a society that works for everybody (implied)
- Advocate for housing, healthcare, and living wage (implied)

### Whats missing in summary
The emotional depth and personal reflection that watching Beau's full message can provide.

### Tags
#MartinLutherKing #CivilRights #CommunityAction #SocietyChange #Solidarity


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're going to talk about unfinished dreams.
You see today, April 4th, 1968, in Memphis, we lost Martin Luther King.
Even with everything going on in the world right now, everything that's going to be in
the news, I would imagine the media brings him up today.
They almost always do.
And they memorialize him.
They recount the stories, tell you what he did, and how we lost a great civil rights
leader.
And all that's true, no doubt.
But that's not what I want to talk about.
I want to talk about what he didn't do.
Because in 1968, he was working on something else.
He was working on something else.
And it's something that would unite people across racial lines, religious lines, gender
lines, everything.
It was for everybody.
It was a project that would change the very moral fabric of the country.
It was called the Poor People's Campaign.
And its goals, very lofty, you'll recognize a lot of them.
Housing, a living wage, health care, you know, all the stuff we're still fighting for today.
I can't help but wonder what would happen if we hadn't lost him in 1968.
Because when people talk about it today, it's real easy for the naysayers to say, you'll
never get that done.
See it would have been different for him.
Because he had already accomplished a lot of stuff that was impossible.
Everything's impossible until it isn't.
And I can't help but think that he was smart beyond the way that we always think about
him.
Not just passionate.
Not just a great speaker.
He was smart.
See Vietnam was causing a lot of social upheaval.
And he knew that there was probably a good chance that infrastructure would have to change
after that.
That things would change.
People would be ready for a change.
And out of the ashes of that old system, you might be able to change the very moral fabric
of the country.
We're there now.
We're in that same situation.
Took a long time for it to cycle back around with everything lining up.
It's become very clear that the government is not really that interested in us.
And we're probably going to have to show a whole lot of solidarity through this.
To each other.
To our neighbors.
To our communities.
Not a left right thing.
It's an up down thing.
We will have a lot of power coming out of this.
We collectively.
The people at the bottom.
Because what they're concerned about.
What they were really concerned about this entire time.
The economy doesn't get back on track unless we say it does.
It might be time to push hard and fulfill that dream of creating a society that is just
for everybody.
A society that works for everybody.
A society that has housing, health care, food, a living wage, basic things.
A society that is promoting the general welfare.
And yeah, you will get those naysayers when you talk about it.
It's a guarantee that it's going to happen.
And they will tell you.
It's a dream.
It's utopia.
But you'll never get it because the system is against you.
Yeah.
It is.
And that's the point.
If we are supposed to be a government of the people, by the people, and for the people,
why doesn't it ever represent the people?
Why are the people at the bottom always the afterthought?
The average citizen of this country, why are they never actually taken into consideration
when decisions are made?
This current situation, it has weakened faith in the system that exists.
It's going to cause a lot of social upheaval.
And out of that old system, out of those ashes, a new one can arise.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}