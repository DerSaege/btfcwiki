---
title: Let's talk about a Twitter Q and A (Part 4)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0-0y4PWOZy0) |
| Published | 2020/04/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Provides a quick question and answer from Twitter for those who can't attend a live stream.
- Suggests steps to rehabilitate news media perception, focusing on reducing bias and increasing balance.
- Mentions the importance of journalistic practices like labeling speculation and file footage correctly.
- Talks about the need for a counter to outlets with dangerous rhetoric and the importance of mocking and debunking them.
- Addresses the impact of current events on the agricultural world, mentioning the surplus due to restaurant closures.
- Expresses views on the potential need for a bailout for farmers or corporate ownership in the agricultural sector.
- Shares thoughts on adopting a parliamentary system or eliminating political parties altogether.
- Explains the concept of a fifth column in relation to opening gates for independent voices in media.
- Comments on the challenges of fair representation in government amidst corruption and corporate influence.
- Speculates on the lack of public accountability for Trump's actions post his presidency.
- Talks about the value of participation in decentralized platforms like a symptom tracker during the absence of a coherent federal response.
- Compares free market capitalism with cronyism and suggests that corruption occurs once the government intervenes.

### Quotes
- "Mock them, debunk them, move on."
- "There has to be a counter to it."
- "Free market capitalism can only exist without a government."

### Oneliner
Beau suggests steps to rehabilitate news media perception, advocates for countering dangerous rhetoric, and shares insights on the agricultural sector, political systems, accountability, and capitalism.

### Audience
Media Consumers

### On-the-ground actions from transcript
- Counter dangerous rhetoric by mocking and debunking inflammatory outlets (implied).
- Support independent voices in media by seeking alternative news sources (implied).
- Participate in decentralized platforms for information-sharing and collaboration (implied).

### Whats missing in summary
Insights on immigration policies and accountability post Trump presidency.

### Tags
#NewsMedia #Bias #Agriculture #PoliticalSystems #Accountability #Capitalism #IndependentVoices


## Transcript
Well, howdy there, and happy Belitzbo again.
So I guess we're to part four of this.
This is going on a lot longer than I thought it would.
Quick recap, if this is the first one you're tuning into,
we are doing a quick question and answer from Twitter
for those who can't normally make a live stream
is what this boils down to.
Okay, question.
What steps could be taken to rehabilitate
news media perception among all Americans? Follow-up questions, how
dangerous should we regard sources like two that I'm totally not gonna name and
give air to the public trust? Okay, so what steps can be taken to
rehabilitate the news media's perception among all Americans? I think the first
thing that they would need to do would be start being less inflammatory, less
bias, and it doesn't matter which ally you're talking about, it's a matter
of the fact that they all are. And this channel, I'm biased. I have biases.
Everybody does. The difference is I'm pretty open about mine. When they tend to
present themselves as trusted and that they are factual and accurate, we report, you decide
type of thing, when they try to pass that off as objective, it becomes a problem, especially
when their bias is very transparent.
They would have to become more balanced, they'd have to live up to their marketing.
It's okay to have somebody who's a right-winger, as long as you also have somebody who's a
left-winger. When you're talking about discussing events, there was a time when
Reuters was the standard. Who, what, when, where, why? And the why is always at the end
and any speculation is marked as speculation. I think that would be a big
one. And then going back to just the normal standard practices of
journalism, like marking your stock footage, your file footage as file, rather
rather than trying to make it seem like you have somebody there filming it right now.
Little stuff like that would go a long way.
As far as those other outlets, I don't know that I would use the term dangerous.
They're only dangerous if they're not countered.
They're only dangerous if people aren't aware of what they are.
One of these that you've named is actually a frequent advertiser on this channel, and
I love it.
I love the fact that they fund my channel, denying them those resources.
There has to be a counter to it.
There has to be a counter to it.
And if there is a counter to it, then it's not dangerous.
If there's not, it runs amok, it does become dangerous.
So I would not suggest, when you're talking about really inflammatory outlets, I do believe
that mocking them is actually the right move, even though I normally think engaging is the
right thing to do.
With them, no.
There's no engaging that type of rhetoric, those that push those kind of theories.
Mock them, debunk them, move on.
What are your thoughts about how everything going on
is affecting in the agricultural world?
Commodities have crashed in February.
And with restaurants all closed, the surplus
is likely to be immense.
What kind of actions should the Fed
be taking to protect American against producers?
I'm going to assume that's a typo.
American agricultural producers.
I actually don't think there's much they need to do.
I know that's counterintuitive.
There's gonna be a surplus here.
There's gonna be surplus everywhere.
So it's gonna be cheap.
I don't think there's gonna be a lot of foreign competition.
I don't think people are gonna wanna import here
because they're not gonna get much for it.
Um, I don't foresee that being a huge issue
because I think the bigger issue is
that surplus is gonna be huge.
Because we eat out a lot and we're not doing that right now and that I think that I think farmers are in for a rough
time
I think farmers are in for a rough time and they're they're probably going to need a
Bell-out or it's just going to all become
Bigagriculture they're gonna buy up everything
That that those are the two things that are gonna happen one of those there's either gonna be a bell-out or it's all
gonna be corporate-owned  Should the United States adopt a parliamentary system?
There's a whole bunch of different remedies.
That's one, sure.
I like the idea of getting rid of parties altogether, more than anything.
But again, my views on where we're headed are pretty extreme.
They're pretty radical on that.
So the stepping stones to get there are just that to me, they're stepping stones.
I don't think they're fixes.
Please explain the whole fifth column thing.
Okay, so the fifth column, there's a story.
And it's basically there's a general, he has a city surrounded.
And he's like, I have four columns, north, south, east, and west.
I also have a fifth column inside the city ready to open the gates and forces the city
to surrender.
Rough version of the story.
The name The Fifth Column came from a news outlet, which is still just kind of just on
hiatus right now.
But the idea behind it was to help profile more independent voices.
There's a lot in the major news networks, there's a lot of gatekeeping going on.
fifth column, a news outlet designed to open those gates and allow more people
have a voice in the media. That was the general idea behind it. I know people
people read a lot into that. Is it even truly possible that we the people can
ever have a fair representative body of electives that's not owned by the big
corpse. Possible? Sure. Probable? No. People are corruptible and large
corporations have money. Representative government is good, but it's not perfect.
Let's see, I love your immigration videos, anything to do with it would be great, especially
the new order Trump's putting in place.
How could he be held accountable?
I don't know that Trump would ever be held accountable for his actions, realistically.
I think that the powers that be, the establishment figures, when they get back the Oval Office
they get it out of the hands of this guy, they're going to want to forget about this
as soon as possible.
I don't see, they're going to be working on mending the image of the United States.
And I think that any public accountability of Trump might undermine that in their eyes.
I think it's necessary, but I don't know that they will.
So I don't know.
I'm interested to see.
In the absence of a coherent federal response, is there value in participation via platforms
like this?
Hang on, because I don't know what this is.
Symptom tracker.
Maybe.
I'm all about decentralization when it comes to everything.
So the more information in the more hands, the better.
A lot of the big breakthroughs have not been coming through government auspices.
They've been coming through people working together.
I definitely, I don't know anything about that particular app, but yeah, I mean, one
of the things I use is actually a thermometer, like an internet of things.
It's tied to the web, and one of the things I use to track it is actually the readings
from these thermometers.
So that kind of information is useful.
Okay.
I'm interested in your take on free market capitalism versus cronyism.
The former is often referred to in place of the latter, I've been guilty of it myself.
Free market capitalism can only exist without a government.
That's the reality of it.
As soon as the government comes into play, there's corruption, there's cronyism, it
happens.
And if under free market capitalism, without a government, you're going to very quickly
have a de facto government.
with the money make the rules. So yeah I don't see a whole lot of difference. I
know people like to draw that that difference. I don't know that it's as
significant as a lot of people make it at the end of the day. Okay so here we
are again ten minutes so we're gonna go ahead and stop and come right back. Man
And I did not think there were going to be this many questions.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}