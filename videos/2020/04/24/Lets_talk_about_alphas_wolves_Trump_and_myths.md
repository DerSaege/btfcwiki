---
title: Let's talk about alphas, wolves, Trump, and myths....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eefKb6EPMzc) |
| Published | 2020/04/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the misconception of the term "alpha" in relation to masculinity, stemming from a study of wolves in captivity.
- Points out that the myth of the alpha as aggressive and dominating has been debunked, but still persists in popular culture.
- Contrasts the behavior of wolves in captivity (terrified, aggressive) with those in the wild (nurturing, protective).
- Suggests that American masculinity should aim to be like a free alpha in the wild, nurturing and protective of their community.
- Talks about toxic masculinity and how it doesn't mean all masculinity is bad, but certain behaviors are toxic.
- Mentions that the term "toxic masculinity" originated from men's rights activists concerned about the hypermasculine traits overshadowing nurturing aspects.
- Emphasizes the importance of being a real alpha, nurturing and protecting the community, instead of focusing on competition and aggression.
- Criticizes the notion of President Trump as an alpha, citing his lack of responsibility, inability to lead, and reliance on privilege.
- Encourages a shift from the myth of the aggressive alpha to embracing a more nurturing and cooperative approach to masculinity.
- Concludes by urging for a reevaluation of what it means to be an alpha and American masculinity.

### Quotes

- "The idea, the myth that exists in popular culture about an alpha, that came from a study of wolves."
- "I think American masculinity would be better served trying to be a free alpha rather than a terrified animal in a cage."
- "It's protecting and allowing their community to prosper."
- "An alpha who had to think. A real one."
- "If that's the definition of an American alpha, if that's the pinnacle of American masculinity, we're probably in real trouble."

### Oneliner

Beau dismantles the myth of the alpha male, advocating for a nurturing, protective form of masculinity over aggression and dominance.

### Audience
Men, Masculinity Advocates

### On-the-ground actions from transcript

- Challenge traditional notions of masculinity by promoting nurturing and protective behaviors in your community (exemplified)
- Educate others on the misconceptions surrounding the term "alpha" and toxic masculinity (suggested)

### Whats missing in summary

The full transcript dives deep into the flawed perceptions of masculinity, challenging individuals to rethink traditional roles and embrace a more nurturing and protective form of masculinity.

### Tags

#Masculinity #ToxicMasculinity #AlphaMale #Community #GenderRoles


## Transcript
Oh howdy there internet people, it's Beau again.
So today we're going to talk about wolves, alphas, the president, and the mystery of
American masculinity.
We're going to do this because a pundit by the name of Gorka said that the reason the
president wasn't liked was because he was an alpha.
The president is not an alpha.
President doesn't even know what an alpha is.
Neither does Gorka.
Most people who use the term in the sense of describing a tough guy don't know what
an alpha is.
The idea, the myth that exists in popular culture about an alpha, that came from a study
of wolves.
It was popularized in the United States in the 1960s by a guy named Meck who relied heavily
on research from an older researcher, a German guy, I can't remember his name.
The problem is all of that research was done on wolves in captivity, in zoos.
So the image of an alpha that most people have is what occurs when an animal is confined,
scared, thrown into a habitat with strangers, unsure that they're going to have enough to
eat, putting up a front because they're terrified.
That's what many alphas of today are striving for, to emulate the behavior that comes from
being terrified.
Meck went on to study wolves in the wild.
He felt so bad about how he cast wolves in that first book, he asked the publisher to
stop printing it.
It was selling incredibly well.
Asked them to stop printing it.
Spent ten years of his life trying to correct the misinformation that he unintentionally
helped spread.
A wolf in the wild, an alpha in the wild, a free alpha is an entirely different animal.
You think about the myth, you know, this commanding, dominating, aggressive, violent, takes whatever
he wants, concerned about himself.
That's the myth.
That's not reality.
That myth has been debunked for decades, but it endures.
Why?
Why does that endure?
For the same reason Trump's popularity endures among some groups.
It gives them a reason to be their worst.
Gives them a justification to act in the most self-serving and immoral, unethical ways.
Well that's how wolves act in the wild.
It's just nature.
No, it's actually not.
The president said it's okay, so it's okay.
No, that's not what it means.
An alpha in the wild, yeah, they're a leader, but they're not commanding.
They lead by example.
Yeah, they are aggressive.
They are assertive, but they're not violent for no reason.
A lot of the snarling you see around mealtime, that's the alpha telling some of the wolves,
you know, don't eat at all, there's other members of the pack.
The alpha's definitely not self-serving.
His entire mission, his entire purpose for being is to care for his pack, to care for
his community.
I think American masculinity would be better served trying to be a free alpha rather than
a terrified animal in a cage, lashing out because it doesn't know what to do, because
it's been forced into a role.
The other thing is that the alpha in the wild is a thinking animal.
When you really sit down and you think about this, it really does make more sense.
It's the myth, it's the image.
The whole idea of the alpha male in the American masculine sense, it's a front.
It's an image.
It's something that people are trying to play into, because that's what they've been conditioned
to think is right.
But many of them are willfully conditioned.
They know deep down that this is not right.
They probably know that it's not even true in nature.
They just reject reality, because they want a justification to not improve, to not become
better men.
I was part of that group that for a long time thought that a lot of men just didn't get
it.
If I point at a jug of milk and I say, that's spoiled milk, does that mean that all milk
is spoiled?
Same thing applies to the phrase toxic masculinity.
The same thing applies.
Just because certain behaviors are toxic does not mean that all masculinity is bad.
I would also point out that the origin of that term did not come from feminists or leftists.
It came from the first men's rights activists, the mythopoetics.
Their concern, and they were right, was that the hypermasculine was the term they used.
The aggressiveness, the violence, had been turned up so much that men were losing touch
with the deep masculine, is what they called it.
The nurturing, the developing, the thinking aspects of it.
That they had just lost their way, and that they were motivated out of fear.
They were right.
When you look at the real alphas, the free alphas, those that aren't confined, their
entire purpose in life is being nurturing.
It's protecting and allowing their community to prosper.
It's what they do.
They don't try to subjugate because they're not afraid of the competition.
Because they don't focus on competition, they focus on cooperation.
It would be great if a lot of the people who considered themselves alphas actually were,
in the real sense, the wild sense, the free sense, not the captive sense.
And sure, you can continue to go along the road of pop culture and the myth, that's fine.
But understand it's coming from a place of fear.
You're there kicking down on those that you believe to be weaker, more vulnerable.
But understand, you can only do that because of the system we exist in right now.
As that zookeeper ever stops throwing you his scraps, I would rather be around an alpha
who had to endure.
An alpha who was free.
An alpha who had to think.
A real one.
Rather than a terrified, caged animal, unsure of himself.
No, President Trump is not an alpha.
The idea that a man who accepts responsibility for nothing, who can't lead, who throws temper
tantrums, who is so secure in himself, he has a comb over and a spray tan, who was handed
every advantage in life, and still manages to whine and cry about how he was treated
very badly and it was very unfair.
If that's the definition of an American alpha, if that's the pinnacle of American masculinity,
we're probably in real trouble.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}