---
title: Let's talk about a Twitter Q and A (Part 3)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XL636MSKeb4) |
| Published | 2020/04/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responding to questions in a Twitter Q&A, sharing off-the-cuff responses to various topics like general strikes and stateless societies.
- Expressing support for strikes as effective tools, suggesting setting up parallel institutions to defeat existing government structures.
- Explaining why some people still support Trump: sunk cost fallacy and being in echo chambers that shield them from differing viewpoints.
- Contemplating how a stateless society with a highly educated populace might handle situations like the current one of staying home and taking precautions.
- Advocating against armed conflict and discussing the potential benefits of a general strike as a form of peaceful protest.
- Sharing thoughts on Earthships and sustainable living, acknowledging the political motivations behind certain architectural choices.
- Providing advice on dealing with stress, acknowledging its universality and suggesting finding individual coping mechanisms.
- Addressing the logistics and implications of a military coup in the United States, expressing doubts about its likelihood and discussing potential outcomes.
- Describing personal methods of de-stressing, including working in the yard and occasional workouts.

### Quotes

- "If you don't run your life, somebody else will."
- "Good ideas generally don't require force."
- "There is no simple way to deal with stress."

### Oneliner

Beau responds to various questions in a Twitter Q&A, discussing topics like general strikes, sustainability, stress management, and the implications of a military coup, advocating for peaceful solutions and individual coping mechanisms.

### Audience

Community members

### On-the-ground actions from transcript

- Set up parallel institutions to provide better services and undermine government control (implied)
- Encourage education and critical thinking in the community to foster a highly educated populace (implied)
- Support peaceful forms of protest like general strikes as effective tools for change (implied)
- Promote sustainable living practices and environmental awareness in local communities (implied)
- Advocate for peaceful conflict resolution and individual stress management techniques (implied)

### Whats missing in summary

Insights into the importance of critical thinking, education, and community-driven solutions in navigating societal challenges.

### Tags

#SocialChange #GeneralStrikes #Sustainability #StressManagement #CommunityEmpowerment


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So it is part three.
I've got a lot more questions than I thought we would
of our Twitter Q&A.
We're doing this for people
who can't make it to the live stream.
Now, I've glanced at these questions,
but haven't really read through them,
so you're getting kind of an off-the-hip response.
Okay, so where were we at?
All right.
If a general...
Well, I'm jumping right into this one.
If a general strike movement does build up,
do you see a possibility to have emergency services,
EMT, fire, police, join such action like in France?
If so, who has the numbers?
So, mobilized on behalf of the state military.
A general strike is probably not something
the government would call the military out for,
not even the National Guard,
as long as that strike was a general strike at home,
basically sheltering in place.
What's happening right now?
Now, if there were large-scale rallies,
you might see a response from the National Guard,
but it would be localized.
It would be localized.
I don't foresee...
From a defense standpoint,
you would not want to mobilize your military
against your own people on that scale,
simply because there are a lot of countries out there
that would love that and might take advantage of it.
It would be incredibly risky for them to do that.
So, I'm a big supporter of strikes in general
because they're incredibly effective.
Is there a path towards liberating institutions
from government control
without having to rebuild them entirely?
Set up a parallel institution.
In most cases, you can set up a parallel institution
and borrow the good pieces from the one that already exists
and leave the bad pieces out.
And if your institution is more responsive,
people will turn to it rather than the government one.
And then, in that manner,
you can defeat that government institution
by ignoring it.
Why do people still believe in Trump?
Sunk cost fallacy.
They put so much into believing into him
that they can't admit they were wrong.
At this point, if you're rational
and you're looking at all of the evidence,
you can't.
You can't support him anymore.
I know a lot of people who started off
as Trump supporters who have ceased to be Trump supporters
because they did look at all the evidence.
Most of those people who still support him
are in echo chambers.
They don't get out and hear other viewpoints,
and they don't hear the negative things that he's done.
They only hear outlets and people who sing his praises.
How would a stateless society handle something
like what's going on right now?
What's going on around?
A stateless society requires a highly educated populace.
How would it be handled?
Exactly as it is right now.
Everybody would be staying home.
Everybody would be taking precautions.
The only difference is that people would do it
because they know it's the right thing to do,
not because they're being told they have to.
The thing is, there are a lot of things
that could still be going on right now
if people were smart enough to take the precautions,
if they were educated enough to take the precautions
that they should while those activities were occurring.
The problem is we have a population
that is anti-education.
They don't want to be educated.
A large group of the United States is very comfortable
being willfully ignorant.
They think somebody else will take care of it for them.
And that's true.
That is true.
If you don't run your life, somebody else will.
And now that that's happening, they're complaining about it.
The actual response would be very much the same.
It just would be done without coercion.
It would be done without force.
Sent to you an inbox.
I have to look because that one.
OK.
Would you be willing to do a video about your time
as a contractor and what lessons you learned from that time?
In general terms, yeah.
That's something I could do in general terms,
but not specifics about events and stuff like that.
At times like this, do you believe a central government
is necessary?
No.
No, I don't.
I think in many cases, it's been a hindrance right now.
It goes back to that other question.
The responses would be the same.
The only difference is without a central government,
you wouldn't have as much political pressure
to believe what one party says.
You would, if you're going to build boots,
if you're going to make boots, you consult the boot maker.
And an educated populace would know that.
And they would turn to the doctors.
They would get the advice from them.
They wouldn't get it filtered through what's
best for whatever political party they enjoy.
Opinions.
Opinion on goats.
I like them.
They're funny.
That's probably code for something I don't understand.
Clearly, there is need for revolution after this.
And you often state armed would be the worst option.
So is general strike the best option?
I often think that we are producers and consumers.
So if we can unite, we can change the game.
Your thoughts on how this can be achieved?
I think a general strike could be a fantastic thing,
depending on what the requests were to return to work.
It all depends on how it's done.
The devil's in the details on stuff like this.
The one thing I will definitely say,
and I will say Tom Blue in the face,
we do not want armed conflict in this country.
It's a horrible idea.
Most people aren't ready for it.
And it never really yields what the people want.
It just sets a new group of people in power.
Good ideas generally don't require force.
But yeah, I do think a strike is a good option.
I don't know that it's the best option,
but it's definitely high up there.
What's your view on Earth ships?
Michael Reynolds in parentheses.
I think that's the architect who builds stuff out of tires.
I like the general idea, as it relates to Gaia theory.
It should be sustainable.
I think that housing should be sustainable.
I'm not sure we have to go to the extreme of using waste
products to do it.
But at the same time, I think he does
that to highlight how much use is being
thrown away in those products.
I think there's a political motivation behind that.
All art is political on some level.
So what are your views on them?
Yeah, I think they're a good thing.
I think they're a good thing.
I don't think that they have to go to the extreme
that Michael Reynolds, I really hope that's the right guy,
does.
But I definitely like the idea of building
more sustainable living and things
that are within harmony with their surroundings.
How do we deal with the stress after coming out and not
being yourself?
My stay plans to lift the shelter May 1st.
And I'm supposed to go back to work when the doc allows.
But the anxiety and my conditioning
has me down, also not trying to pass it
to family, doubts to family.
OK, dealing with the stress of this is simple.
It's going to be there.
There is no simple way to deal with stress.
I saw that thing the other day where a boss called
one of their employees, like, how are you doing?
And the employee was like, I'm fine.
Like, no, you're not.
How are you doing?
Nobody's fine right now.
I think one of the key things to dealing with it
is understanding that you are stressed.
We all are.
We all show it in different ways.
But we're all stressed.
And finding the mechanism that allows you to deal with it,
because it's going to be very different than the mechanism
that allows me to deal with it.
I wouldn't be able to give you advice on that without really
knowing you.
Pros and cons of a military coup.
I want to understand the logistics and policy.
Would the military be in charge until November?
In the United States, I don't see that as a likelihood.
I think they're just going to run out the clock.
And that's never something you really want,
because once you attain that level of power,
it's very hard to give it up.
Now, you still have a few true blue American patriots,
those who believe in the Constitution,
within the higher ranks of DOD.
And I do believe that a large part of that officer corps
would, if they were ever in that situation,
take control and then establish elections pretty quickly.
However, there's no guarantee that those
would be the generals that actually wound up in charge.
So that's not really a safety valve.
That's a hope.
Maybe it works out.
But if that happens, it can go really bad real quick.
The logistics of something like that is it's normally a palace
coup.
It's done all in-house and hopefully very quietly.
It is normally how that works.
There's a lot of regions that have changes of government
like that pretty often.
OK.
How do you de-stress besides moonshine?
As odd as this may seem, this actually helps me de-stress.
I like to work in the yard.
I work out occasionally.
But again, everybody has their own methods of coping.
OK.
So we're there again.
Now I guess we're going on to part four,
see you in just a minute.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}