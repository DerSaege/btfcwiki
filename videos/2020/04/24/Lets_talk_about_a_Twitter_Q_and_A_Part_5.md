---
title: Let's talk about a Twitter Q and A (Part 5)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OMZ57LxjxFc) |
| Published | 2020/04/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Unexpectedly faced with numerous questions on various topics.
- Declines to show the shop due to lack of presentability.
- Advises to be prepared for disasters with limited resources.
- Stresses supporting policies over individual candidates.
- Acknowledges the detrimental impact of both Republicans and Democrats.
- Calls for a diversity of tactics in combating government corruption.
- Emphasizes focusing on accuracy in developing journalistic skills.
- Views the elimination of political parties and campaign finance reform as positive steps.
- Shares perspective on unlearning racist and sexist undertones in Southern culture.
- Expresses concerns about the long-lasting impact of Trump-appointed federal judges.
- Questions the lack of consideration among Americans for their global image.
- Recommends consuming information from various sources to find truth.
- Encourages maintaining hope despite challenges and uncertainties.
- Advocates for exposing children to diverse political views and promoting critical thinking.
- Urges people to start growing their own food amidst disruptions in supply chains.

### Quotes

- "I support policies and ideas."
- "Generally speaking, Americans do not try to put themselves in anybody else's shoes, much less foreigners."
- "Hope is a dangerous thing, and I think everybody should remain dangerous, stay full of hope."
- "I don't try to create little ideological foot soldiers."
- "The world is constantly changing. We shouldn't want it to be static. We should want it to change."

### Oneliner

Be prepared for disasters with limited resources, focus on policies over candidates, encourage diverse tactics against corruption, and maintain hope amidst uncertainties.

### Audience

Individuals, Parents, Citizens

### On-the-ground actions from transcript

- Start growing your own food to prepare for potential disruptions in supply chains (implied).
- Expose children to diverse political views and encourage critical thinking (implied).

### What's missing in summary

Deeper insights into unlearning racist and sexist undertones in Southern culture and the impact of Trump-appointed federal judges.

### Tags

#DisasterPreparedness #Journalism #PoliticalReform #CriticalThinking #CommunityEngagement


## Transcript
Oh, howdy there, internet people.
It's Beau again.
So we're back for part whatever this is now.
Really did not expect this many questions.
Okay, so can we see the rest of your shop?
I know it's not a set, so I want a tour.
Yeah, just not right now.
It's not exactly what I would call presentable.
As a disaster recovery specialist
for a government medical system,
what mitigation steps would you suggest
that I add to my DR plan
to mitigate a smoking hole catastrophe?
Phew.
I'd be prepared to work with a lot less resources
than you normally would.
If something is gonna go down right now,
it's going to tax an already taxed system.
I would be ready to respond
with only your personal resources,
your system's resources.
What candidate was your favorite this year?
I don't have favorite candidates.
I have policies.
I, and that's not a dodge.
I try not to support individual candidates.
I support policies and ideas.
I think the drive to look for a candidate to back
is becoming the more important thing
rather than finding the policies that you agree with.
What are, Republicans are,
Republicans are killing Americans.
Democrats are pretending they care,
pretending they're helpless
when they're actually co-conspirators.
What are we gonna do about it?
I don't know how to respond to that, honestly.
There's a whole lot in this
that I'm not sure that I agree with.
There's the idea that this is,
and maybe it's just the way I'm reading it,
but it makes it seem intentional.
And I don't want to go,
I don't want to go down that road.
I would need more,
more information about what you're meaning here.
Generally speaking,
when you're talking about responding to something,
especially government corruption in general,
I believe in a diversity of tactics.
How does one develop your journalistic skill set?
I would focus more on writing,
producing your final product and it being accurate.
If you focus on accuracy, everything else will come.
You will learn how to research.
You will learn how to interview.
You'll learn how to investigate.
If your primary objective is to turn out a piece
that is as accurate as possible.
Do you think the elimination of political parties
combined with serious campaign financial reform
repeal of Citizens United and the Patriot Act
and strict term limits might get us closer to sanity?
They're good stepping stones.
They're definitely putting us on the right track.
I'm new to your platform,
so you may have already discussed this,
but as a fellow Southerner in Louisiana
with a BA in Communication Studies,
I would love to hear your perspective on unlearning
the racist, sexist undertones of Southern culture.
I have done some videos on this and I will link them.
I'll try to find this video in this giant stream
that's going to be put out.
Let's see.
How will we ever remedy all the federal judges
Trump has appointed over time?
Yeah, that's going to be a huge delay.
That's going to cause a lot of issues down the line,
but we have to hope that they become less empowered
once Trump is out of office.
Being from a lot further south than y'all, Australia,
I wondered, does the average American ever consider
how they look in the eyes of the world?
Because at the moment, it's not looking so great.
Doesn't that bother you?
Do you have an opinion about it?
Yeah, we don't.
Generally speaking, Americans do not try to put themselves
in anybody else's shoes, much less foreigners, gosh.
It's not something we do.
We're, generally speaking, and this isn't true for everybody,
it's a huge generalization,
but we're a country of people who are, you know,
we're number one, we're number one,
we've been told that so long that we believe the mythology.
We're a nation that, in many of our schools,
the maps have our continent centered in it
and literally cut the other continents in half
at the edges of the paper,
just because it's not as important.
That's what we're dealing with here.
So I don't think that it's a common thing for Americans
to look at themselves through other people's eyes.
We should.
Out of all of our current newsmen we have nowadays,
you are one of the closest we have to Hunter S.
Thompson, a Gonzo journalist.
That's a huge compliment, if you don't know my opinion of him.
Thank you.
Who are some journalists you would recommend?
I recommend journalists based on their current work,
not their past work.
I think it's important when you're consuming information
to get as many different viewpoints as you can
from as many different sources as you can
and try to find the pieces that all match.
That's how you find truth.
That's how you find truth.
Okay, so now we're on to the last call.
Let's see.
When are you going live?
I should have done this as a live stream.
I didn't know there were going to be this many questions.
Is it okay to out an anti-vax, anti-mask protester in your town
who interacts with the public like real estate agents,
asking for a friend?
Generally when you're asking somebody if it's okay,
what you're asking for is moral and ethical advice.
When you're talking about something like this,
you know more information than anybody else,
and you have to decide whether or not it aligns
with your morals and your ethics.
If these people are posing a danger to society,
it might be important for people to know.
I mean, if this is somebody who would try to ride it out
and knowingly go around while infected, yeah,
I mean that would be something you should probably disclose.
However, if it's somebody that all they do is go,
you know, leave their house and go open a door,
or even just fax out information,
they may not even be showing houses right now.
They may just be using the little lockbox things.
Maybe not.
There's a lot of details here that I don't know,
but it's something you would have to decide on your own.
Ferry farmers.
Dairy farmers are dumping millions of gallons,
hogs and chickens, buying buried cattle next,
crops rotting in the field from restaurant closures,
all at the same time the purchasers are being rationed.
If consumers do seek out producers,
it will not sort itself out.
Yeah, I mean, there's not a question there, but yeah.
Yeah, I mean, there are going to be long-lasting impacts,
especially in the agricultural sector.
Your hope meter, what level is it at?
It's always full.
It's always full.
I do agree that hope is a dangerous thing,
and I think everybody should remain dangerous, stay full of hope.
Sometimes it's the only thing you got.
How do you instill, promote your political values into your children?
I expose my children to my political views.
I also expose them to other ones.
I hope.
My idea is that if my views are right,
and my children are exposed to a wide variety of views,
they'll probably come to mind.
I don't try to create little ideological foot soldiers.
I think that by the way I live, and them seeing what I do,
I take them with me when I go do my activist stuff.
I think they'll see that that is the best way.
But I would not be upset if they came to a different conclusion,
because it would mean at the very least I taught them to think for themselves.
Please give people a heads up to start growing their own food,
buying wholesale, buying direct from farmers,
lots of food available, but major disruptions to supply chains.
Yeah, that's a huge part of this channel.
Like even under normal circumstances I say grow your own food.
Is this the end of the world as we know it?
Yeah, but so was yesterday and the day before.
The world is constantly changing.
We shouldn't want it to be static.
We should want it to change.
Are your parents political?
Not like I am.
Let's see.
What's up with the shovel?
NCAA football this fall?
I have no idea.
Your favorite movie, book, rock band, beer, and hamburger joint?
I don't even know.
Hamburger joint I can answer.
It's called Tops in Niceville, Florida.
Is it true many of these early to open states like Georgia and Florida are partly doing
it to avoid unemployment payments?
Maybe.
Maybe.
That's a possibility.
Where are all the black male psychologists?
I don't know.
I don't know.
Okay, so that was all the questions.
I'm sorry it's in so many parts.
I didn't know it was going to turn out like this.
But we're going to do something similar for Patreon.
That was a question that was in my email box.
Do I answer my messages on Patreon?
I do, but I get a whole lot from all the different social media.
It takes time.
We're going to do another one and we will do that live stream so it doesn't end up being
broken up like this.
Anyway, so it's just a thought.
I'll have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}