---
title: Let's talk about hospitals refusing to allow their staff to bring in supplies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2bP2bKVyv5M) |
| Published | 2020/04/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hospitals in metropolitan areas are refusing to let staff bring in needed protective equipment that was donated or scrounged up.
- Reasons provided include concerns about not everyone getting the equipment, it being counterfeit, and unauthorized.
- Beau points out the flaw in these reasons, urging hospitals to prioritize those dealing with high-risk patients.
- He notes that even if the equipment is counterfeit, it's better than nothing, especially considering the current shortage.
- The focus on bottom line and PR seems to be the driving force behind these refusals, with money being spent on bonuses rather than necessary supplies.
- Beau vows to contact private donors of hospitals that deny staff protection, aiming to cut off funding until those responsible are no longer employed.

### Quotes

- "If they can get the equipment, and I don't care how they get it, if they can get it, let them use it."
- "You refuse to allow your staff to protect themselves."
- "You had money, too, and you chose to spend that money on bonuses for the administrative staff."
- "We will be going out of our way to choke off funding until those people responsible for this decision are no longer employed."

### Oneliner

Hospitals denying staff protective equipment prioritize profit over lives, risking safety and facing accountability from donors.

### Audience

Hospital staff, donors

### On-the-ground actions from transcript

- Contact private donors of hospitals denying staff protection (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the reasons hospitals use to deny staff protective equipment and calls out their prioritization of profit over safety. Viewing the entire speech gives a complete understanding of the urgency and frustration conveyed.

### Tags

#ProtectiveEquipment #HospitalSafety #Accountability #DonorAction #PrioritizingProfit


## Transcript
Well, howdy there internet people, it's Beau again.
Before we get into this, I wanna be very clear
that I have not run into this problem.
I haven't heard a whisper of this where I am.
It hasn't even crossed people's minds.
However, it has been brought to my attention
that in large metropolitan areas,
some of the hospitals there are refusing to allow their staff
to bring in the equipment they need to protect themselves.
Stuff that was donated, stuff they were able to scrounge up.
They're providing three reasons for this decision.
We're gonna go through those three reasons right now,
and then we're gonna get to the real reasons.
The reasons they're providing is that one,
well, not everybody would get it
if it was brought in in small amounts like that.
You're right, you're right.
But see, you are in charge of a hospital.
This isn't a classroom in a grade school.
Not everybody's gonna get a piece of bubble gum.
I understand that many administrators
at these large hospitals in major areas,
they've never seen a patient.
But there's a concept called triage.
Maybe you've heard about it.
Maybe it was in a memo or something.
The most immediate need would get it first.
Those are those actually dealing with the high-risk patients.
Those who, that's who should get it.
And I assure you that if you left it up to your staff,
that's who would get it.
The second is that it's counterfeit.
It could be counterfeit.
Yeah, that's a real risk.
Having been out here gathering up supplies,
I can tell you, yeah, that's a thing.
It's a real risk.
However, I would point out,
even going by the CDC guidelines
that you're using and citing,
cotton is more effective than nothing.
So even if it is counterfeit,
it's better than what you're giving them.
And I would assume that your nurses
and your docs could tell the difference.
In most cases, I would imagine
they would be able to spot it.
They're not actually good counterfeits.
The third is that it's unauthorized equipment.
Yeah, that doesn't even make sense
because the institution is who authorizes it.
It's unauthorized only because you refuse to authorize it.
You refuse to allow your staff to protect themselves.
These large hospitals
with these massive streams of revenue
understand you failed.
Everybody knew this was a possibility.
This isn't a surprise.
We all knew this could happen.
You failed to lay on the supplies necessary.
Now, smaller hospitals
that don't have that kind of budget,
yeah, they didn't have a choice.
But I've noticed that the hospitals
that are refusing to allow their staff
to protect themselves,
well, those are the big hospitals that have the budgets
because that's what they're concerned about,
the bottom line.
That's why you don't have the equipment.
That's why you didn't lay it on.
That's why you didn't have your own stockpile.
And right now, you're more concerned about PR.
That's the reason, because you don't want your nurses
to look mismatched.
You don't want your docs and nurses
to be doing a GoFundMe to get the equipment
you have an obligation to provide and failed to.
You can look at the state.
You can look at the feds all you want,
but you had money, too,
and you chose to spend that money on bonuses
for the administrative staff,
on corporate retreats, on stuff like that,
rather than getting the supplies
that would save your staff's life in this situation.
Now, those of us who are out here
trying to scrounge up supplies to protect your staff
because apparently you don't care,
right now we're busy,
but I can assure you when this is over,
hospitals that denied their staff
the opportunity to protect themselves,
we will be contacting every single one
of your private donors
and making sure they understand
how little you care about your staff.
We will be going out of our way to choke off funding
until those people responsible for this decision
are no longer employed.
They're saving lives.
If they can get the equipment,
and I don't care how they get it,
if they can get it, let them use it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}