---
title: Let's talk about stress and getting out of the house virtually....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kFhXb6LmMyY) |
| Published | 2020/04/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides tips for managing stress during challenging times.
- Suggests engaging in activities at home to alleviate boredom and cabin fever.
- Mentions Zooniverse as a platform for participating in crowd-sourced research projects.
- Talks about the variety of projects on Zooniverse, not just space-related ones.
- Encourages helping scientists with galaxy identification or exploring old manuscripts.
- Emphasizes the value of learning and contributing to society through these activities.
- Acknowledges the surplus time people have now and the drive for self-improvement.
- Advises against putting excessive pressure on oneself during this period.
- Introduces virtualmuseums.io for taking virtual tours of museums worldwide.
- Recommends virtual museum tours as a captivating activity, especially for kids.
- Points out the abundance of tools available now for exploration.
- Addresses questions about shortness of breath as a symptom and managing anxiety.
- Suggests differentiating between anxiety-related symptoms and actual health concerns.
- Recommends seeking coping mechanisms and managing anxiety through available resources.
- Acknowledges the mental health implications of the current situation and the importance of addressing them promptly.

### Quotes

- "There's no reason to stress yourself out."
- "There are going to be mental health ramifications to this."
- "Y'all try to have a good day."

### Oneliner

Beau provides tips for managing stress, suggests engaging in activities at home, and addresses concerns about shortness of breath and anxiety during challenging times.

### Audience

Individuals seeking stress management tips and coping mechanisms.

### On-the-ground actions from transcript

- Participate in crowd-sourced research projects on platforms like Zooniverse (implied).
- Take virtual tours of museums on virtualmuseums.io (implied).
- Seek out videos on coping mechanisms and anxiety management (implied).

### Whats missing in summary

The full transcript provides in-depth guidance on managing stress, engaging in productive activities at home, and addressing concerns about anxiety and mental health during challenging times.

### Tags

#StressManagement #CopingMechanisms #Anxiety #MentalHealth #VirtualActivities


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk a little bit
about managing stress.
I've gotten some questions about it,
get to that at the end.
But first, we're gonna talk about some things
you can do at home that can help alleviate the boredom
and maybe break the cabin fever a little bit.
The first is a while back I did a video
talking about how you can help astronomers
identify galaxies at home on a website called Zooniverse.
Zooniverse actually has tons of crowd sourced
research projects that you can help with.
It's not all based in space.
There are historical ones, there's all kinds of stuff.
You can help scientists jump into the future
and identify galaxies or you can help provide
a link to the past by going through old manuscripts.
It's something that can help you get through the day,
alleviate the boredom, help society in general,
and you learn something in the process which is cool.
We all have a lot of time on our hands right now.
Can't do a lot of the activities we're used to.
There is that drive for self improvement.
This can help satisfy that.
At the same time, you shouldn't put any expectation
on yourself on what you should do with this time.
There's no reason to stress yourself out.
Now, in addition to this, there's another website
called virtualmuseums.io.
When it comes up, it's a map, has little red pin marks
on it, you click on the pin mark, a little pop up comes up.
It's museums all over the world, Australia,
Mumbai, Berlin, the Smithsonian.
You click on it and you can take a virtual tour.
If you have kids, this is something cool to do,
especially with the Smithsonian because it pops up
and opens up in the room from right at the museum.
So they may find that interesting.
There's a whole bunch of stuff that we may never be able
to do in real life, but we have the time to explore
these tools that exist that we may never have used
before right now.
Okay, now, on to the questions.
Got a bunch of questions about one symptom in particular.
Shortness of breath is a symptom of what everybody's worried
about right now and it's a symptom of worrying about it.
If you're going about your day and maybe you have a couple
of the other symptoms and then you have shortness of breath,
that's probably cause for alarm.
You should pay attention to that.
However, if you're only getting shortness of breath
when you're going through the latest numbers and you're trying
to make sense of it because some of the numbers
are stabilizing and some are continuing to climb
and you're feeling a little overwhelmed,
that may just be anxiety.
There's a whole bunch of videos on YouTube that can help
give you some skills to manage it,
give you some coping mechanisms.
It's always something to pay attention to right now.
However, just be aware of when that symptom occurs.
And if it's only occurring when you're actively thinking
about this and you don't have any other symptoms,
it may not be anything to worry about.
It may just be that you need to relax.
Yeah, I know, easier said than done right now.
But it's just something to keep in mind.
And here's some tools, some activities that you can do
that can help you break the boredom and help get you
thinking about something else.
That's not actually the healthiest way of dealing with it,
but it's something that's available.
There are videos out there that carry you through
a whole bunch of different coping mechanisms.
Take the time to use them.
There are going to be mental health ramifications to this.
And the more we can do to alleviate them now, the better.
Anyway, it's just a thought.
Y'all try to have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}