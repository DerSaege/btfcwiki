---
title: Let's talk with Andy Ratto about mutual aid....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vgTam8Lbw98) |
| Published | 2020/04/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing Andy Rado, a lead organizer of New York City United Against Coronavirus, providing information and resources for mutual aid projects in response to the ongoing pandemic.
- Andy Rado explains that the project involves a variety of resources from government agencies, nonprofits, new groups, and individuals to address the extreme needs exacerbated by the coronavirus situation.
- Mutual aid projects aim to address existing problems like food insecurity, housing insecurity, and lack of access to medical resources through community organizing and resource distribution.
- Andy Rado shares his involvement in direct mutual aid organizing in Bed-Stuy, Brooklyn, and his role in disseminating vital information on available resources via social media and other communication channels.
- The importance of not duplicating existing resources and instead collaborating with established organizations to meet community needs effectively is emphasized.
- Advice is given for those looking to set up their own mutual aid network, suggesting starting by researching existing resources and partnering with established organizations.
- The focus is on meeting immediate needs while also considering the long-term societal and governmental shifts necessary to address systemic issues heightened by the pandemic.
- Beau and Andy Rado stress the importance of community connections, mutual support, and grassroots organizing in responding to crises at a local level.
- The need for continued community engagement beyond the pandemic, addressing ongoing issues like job loss, housing insecurity, and access to healthcare, is discussed.
- Andy Rado encourages individuals to contribute to mutual aid efforts by volunteering time, donating resources, or reaching out for help as part of a participatory and inclusive process.

### Quotes

- "We need to be prepared around housing issues that come up for people that can't pay rent."
- "Everyone has something to contribute, and everyone has the opportunity to make use of that."
- "We all have something to contribute, and we all are gonna face struggles and challenges."

### Oneliner

Be prepared to tackle housing issues, connect with existing resources, and contribute actively to mutual aid efforts to address community needs during the ongoing pandemic and beyond.

### Audience

Community members, volunteers

### On-the-ground actions from transcript

- Join existing mutual aid organizations in your community to contribute time or resources (suggested).
- Partner with established nonprofits or new groups to address local needs effectively (implied).

### Whats missing in summary

The full transcript provides detailed insights into the importance of mutual aid projects, community organizing, and the long-term impact of grassroots efforts in addressing systemic issues beyond the current crisis.

### Tags

#MutualAid #CommunityOrganizing #PandemicResponse #ResourceDistribution #GrassrootsEfforts


## Transcript
Let's try that.
Okay, it looks like we're going.
All right.
Oh, howdy there internet people, it's Bo again.
So tonight we have a bit of a professional heckler with us,
but today he's gonna talk about something
a little bit more important to what's going on.
So if you wanna know what's going on,
you wanna be able to follow along,
go ahead and head over to
http://bit.ly slash NYCCOROAVIRS.
If it's in Spanish,
you want it in Spanish, add dash ES to it.
All right.
So tell us about yourself.
My name's Andy Rado, I'm 35.
I live in Brooklyn, New York,
and I'm one of the lead organizers
of the New York City United Against Coronavirus
information and resources document,
which has become a clearinghouse of information
about mutual aid projects in New York City,
responding to coronavirus,
as well as other forms of information and resources,
which individuals can use as we respond
to this ongoing pandemic situation.
Now, are you affiliated with any government agency
or is this something you're doing on your own
or part of a non-governmental group?
This particular project is a collection of individuals.
What people will be able to see
as they browse the document
or as people do research in their own communities
is there are a variety of resources available
responding to coronavirus,
engaging in these sorts of mutual aid projects.
Some are from the government,
some are from the government,
some are from established nonprofits,
some are from new groups,
and some are really just kind of loosely
connected individuals.
And I think that all of those different organizations
have a role to play in the weeks and months ahead.
So what are you...
First, how would you classify them?
What would you call what you're doing?
Yeah, I mean, I think it's...
You know, we're in this sort of strange situation
where a lot of existing problems
are really being heightened
in extreme ways under coronavirus.
So there's nothing new about food insecurity
or housing insecurity
or a lack of access to medical resources.
And those problems were all being dealt with
by charities, by mutual aid organizing,
by governmental organizations,
by nonprofits, et cetera.
And because the need became so extreme so quickly,
a lot of people were having trouble
finding what was available
and connecting with resources.
And so I became a guide in some sense
to help people find what they were looking for.
And that was a lot of the initial impetus for me
to get involved in this specific way.
I'm also involved in other more direct
mutual aid organizing
in terms of helping people get access to food
and responding to other needs
that I'm doing in my neighborhood of Bed-Stuy, Brooklyn.
But what I also identified
was just a larger citywide issue
of people struggling to find the resources
that were available.
And I was able to begin pushing out that information
via social media, email lists,
these kind of signal and WhatsApp text chains,
and had a bit of kind of knowledge and experience
about how to be both a resource locator
and a disseminator of that information.
And so me and others have kind of played
that sort of guide role for people
who for the first time maybe were struggling
to find some of those resources.
Nice, nice.
Nice.
So if you were somewhere else,
you're watching this, you're not in New York,
and you want to emulate this,
you wanna set up your own mutual aid network,
you wanna provide your own clearing house
for your community,
where would you suggest people start?
The first thing I would say
is not to duplicate resources or projects
that already exist,
or reinvent any sort of structure
that might have already taken hold in the community.
So we're far enough along
that I think a lot of immediate needs are now being met,
either by previously existing organizations,
or by groups that have come together in February,
in March, in April.
And oftentimes that involves hunting around
and trying to find out what's been happening already,
and who are the people to connect with.
Because a lot of the mutual aid that's happening
is not big budget website, staff,
infrastructure, et cetera.
It's people putting a phone number on a flyer,
pasting it up on some lampposts down the street,
and then getting people out on foot or on bikes
to do grocery shopping, et cetera.
So part of the challenge that I was responding to,
and the challenge that is being replicated
in cities across the country,
is helping people to find those resources
that exist already.
So as a first step,
really beginning to dig around
and see what exists in the community.
There are some good kind of national level resources
that have been compiling that.
It's Going Down is one new source
that has a pretty comprehensive list
of mutual aid organizations operating at the city
or the community level.
And so seeking out and joining up
and partnering with those existing organizations.
Like I mentioned earlier,
the sorts of challenges that we are facing
under coronavirus are extreme,
but not necessarily new.
And so when we think about like,
well, how do we respond to people
who don't have access to food?
The people who are the best at that
are the folks who've been doing that
for the last year, five year, 10 years, et cetera,
who've developed relationships in their communities,
who know what the most hardest hit areas will be,
who've built up trust to have networks of both helpers
and those in need.
And so going to the experts
or going to people with those years of experience
tends to be able to build up a stronger infrastructure
and to be able to learn from their past experience.
That being said, at least in New York City,
the need was so large and so rapid.
We did see a number of new organizations starting up
and there are now these guides coming out
and these resources about how to organize your building,
which can be as simple as posting a flyer in the lobby
and setting up a WhatsApp or a signal chat
so that everyone can stay in touch.
And then moving from the building to the street,
to the neighborhood, to the city
as these kind of expanding circles
of both resources and those in need.
But overall, we're now seeing,
I think a large kind of groundswell
of people coming together to say,
we need to be prepared around housing issues
that come up for people that can't pay rent.
We need to worry about continuing to get people
access to medical care as hospitals are becoming overburdened,
food insecurity, et cetera.
And organizations that are doing this well,
tend to pay it forward in some sense
by putting out materials, resource guides,
tech handbooks, et cetera,
that do provide that information
that can be kind of used in a cookie cutter model
from city to city.
Yeah.
Yeah, there's definitely,
we're starting to see down here in the South,
we're starting to actually see impacts
and you're starting to see rural communities
that are having these issues.
It's the same principle for those that are in those areas.
It could be your neighborhood,
it could be, there's always those locations.
Like here where I live, real small town,
you would put this information at Sonic
rather than in the lobby of your building.
But there's always a workaround.
There's always a way to get that information out.
There's always a way to make it work.
And there's probably already organizations in your area
doing it in the South.
There's probably that guy at your church
who's not really that churchy,
but he coordinates a lot of the charity work.
That's probably the guy you wanna talk to.
That's probably the person
that has the connections in the community
because down here, a lot of it revolves around the church,
even things that are secular.
And I have noticed that a lot of times
that person is not really what you would consider faithful.
He's there because that's where the resources are
or she's there because that's where the resources are.
So if you're in a rural community, it can still be done.
It doesn't have to only be done in areas
with high population density.
But their models are probably more effective
because they're designed to handle a much higher volume
than we would ever see out in the country.
So anyway, just needed to throw that in
because we have a lot of people
that live in rural communities
that are looking for this kind of information
and don't really know how to start.
So I would ask, and here's,
well, first I gotta ask the question
everybody wants to know.
How does it feel to get to yell at people
the way that you have and gotten the press over it?
I oftentimes describe that as kind of the dessert
of organizing or the little treat
that comes once in a while.
The real valuable work I try and engage in
is long-term base building, political,
political education, pressure campaigns, et cetera.
That just involves a lot of time spent planning meetings
and on Skype calls and putting together resources.
But it's New York City and ever so often
there'll be an opportunity for,
to go out on a weekend
and Sean Spicer is doing a book tour
or Howard Schultz is running for president.
And just the things that I think a lot of people think
but don't necessarily get to say,
at least not to the people they're thinking them about.
I happen to have that rare opportunity to say
and it's fun.
And I think it often works or works well enough
in part because I'm sometimes saying something
that a lot of other people are thinking
or that journalists wish they could put in an article
but they're constrained by the kind of rules
and practices of journalism
that they can't write it themselves.
And they're just, they're hoping and wishing
someone puts it into words
and they're able to pass it along.
And particularly around coronavirus,
it's been frustrating because there's been
so much failure from our political leadership
in ways that is just being totally unaddressed.
We're seeing some pushback against Trump
from the press core,
but they're doing it in the way that the press does,
which is respectfully and respectfully
and within the kind of structure
of political journalism in America.
But at some point, the people who are directly suffering
because of mistakes and poor decisions from Trump
may get the chance to express that to him.
There've been really egregious failures
by Governor Cuomo and Mayor de Blasio in New York City
that have just really heightened the impact
of coronavirus in New York.
And Governor Cuomo gets to do his daily,
I guess, press briefings
where he's the strong, authoritative, commanding voice
and no one is really bringing up
these past decisions and actions
where more dramatic measures could have been taken earlier
to really curtail the impact.
So on a day-to-day basis,
the work I'm doing around mutual aid
is hours and hours every day
of these various projects.
But the little kind of dessert I may get at one point
is running into Cuomo or de Blasio
and being able to express myself towards them
in a way that I'm not hearing anyone say
or that is not being included in the media
in the way it should be.
Right, there's a whole lot of blame
as far as preparations for the election
as far as preparations beforehand
and acting on their own.
There's tons to go around for a whole lot of people.
Right now, it is mainly focused on the person
that is most vocal about how perfect they were.
I have a feeling that as more and more comes out
and as those preexisting conditions
within different communities are highlighted,
there's gonna be a moment where people look back
and say, wow, we could have solved this months
before this ever happened.
It's going to be a fun moment for me
because of the way I structure stuff.
But yeah, I definitely would see that as a dessert myself.
So here's a question I ask everybody that comes on.
If you could tell the people watching this
one thing that they can do in their community,
in their hometown, in their neighborhood, whatever,
to change the world, what would it be?
Yeah, you gotta fix the world, it won't sound right.
Yeah, I mean, one of the challenges I think of coronavirus
is the scale of the impact.
We're used to natural disasters in the United States,
hurricane, tornado, flood, earthquake, fire.
I'm from California originally.
We have our share of natural disasters
and those can be limited geographically.
They can be limited in terms of time,
in terms of the number of people that are affected.
And this particular moment we're in
feels very unlimited and infinite.
And it's expanding, it's getting worse.
I would say every day has been worse than the day before
without really end in sight in terms of the human impact.
As people are not working
and the kind of secondary economic impacts continue
to kind of snowball, the situation's getting worse.
So the recommendation that I would have for people
is to narrow it down and think as small
and as local as they can.
And we can talk a little bit about kind of
the philosophical or the political nature of mutual aid
and what it is and why we do it, et cetera.
But without kind of getting into the weeds of that,
what I would say is any one individual
is already an expert in the practice of mutual aid.
If you've ever done something like mow your neighbor's lawn
or take them grocery shopping or watch their kids
or set up one of those kind of meal trains
where such and such, your coworker is recovering from cancer
or a relative passed away
and the spouse and children are coping
and people are bringing by meals
on a regular schedule, et cetera.
These are all, I think, experiences that are
experiences that we've had, that we're familiar with
and that we have expertise in.
And so for people to kind of put aside,
just if they're watching this tomorrow,
put aside this upcoming election
and the stimulus checks and rents and mortgages
and think in my building or on my block,
who are the people in need
and what is the skill or experience
that I might be able to contribute?
I think that's a way to kind of ground all of us
in the work that we're doing
and to open up the sorts of connections
and build the relationships
that we're gonna need going forward.
Because we don't need to launch the neighborhood
or the street association coronavirus response committee
tomorrow.
That could take some time or maybe it already exists
or other people are gonna maybe take the lead on that.
But what each of us can do
is just kind of check in with our neighbors,
figure out how we can help
and be someone that's able to say,
if there's something you need, you can reach out to me
and I would like to be able to stay in touch with you
if there's something that I need.
And at least in New York City,
those sorts of conversations
and those sorts of connections are not always common.
There are people who live in apartment buildings
that do not know the names of the people in their building.
There are people who live on streets
who have never had a conversation
with the people who live across the street from them.
And that can be replicated across the country.
And also, we have this pivotal moment now
of people being able to say,
we're only going to be able to get through this
if we work together.
And that doesn't mean solving the biggest problem.
It doesn't mean solving the most important problem,
but it means taking the steps that will get us
closer to solving those bigger and important problems.
Nice.
Nice.
Okay.
So, what do you think the longevity of this stuff is?
These organizations, once they're built,
these networks, these community-level groups,
what happens after the smoke clears?
Do they still exist?
Do they morph into something else?
Do they help address other issues?
What's the dream?
Where does it go?
So, I mentioned before that the problems
that we're facing now are not new,
but they're larger in scope.
And so, I think it's, I would say,
basically a certainty that the problems
that we are struggling to solve now
will remain problems after coronavirus is over,
whatever exactly that means.
Let's say we come up with a vaccine and a cure next year.
We no longer have to worry about coronavirus.
There will still be people struggling
with losing their jobs, being in danger of eviction,
getting food every day, et cetera.
And it's useful for people in this moment
to get involved with this sort of organizing
because they can continue to do that post-coronavirus.
There are some specific ways
that we're responding in New York City
that may not be essential moving forward.
One of the major needs that we're dealing with
is people who are now homebound,
during coronavirus, because they are elderly
or immunocompromised or really,
it would be a danger for them to go grocery shopping.
So we have teams of volunteers
who will go grocery shopping for them.
In general, someone who may be elderly,
it's fine for them to normally go grocery shopping.
It's great for them to be out in the community.
Exercise is nice, sunlight is nice,
knowing your kind of corner bodega guy
and having a conversation
and having some communication is nice.
So we don't necessarily need to continue grocery delivery,
but we need to continue to assess
what are the unmet needs of the community.
But it's also, I think, important to kind of situate
this mutual aid organizing that we're doing
in terms of a larger political context,
by which I mean, there's a focus right now
on meeting people's material needs,
whether it's food, medicine, housing,
anything else that kind of directly,
the monetary needs that we can meet.
But we're also, I think, very cognizant
of the ways the kind of political and societal structures
have exacerbated the problems of coronavirus.
So we're not just talking about the coronavirus,
we're talking about the health insurance of coronavirus.
So there's a big debate in Congress right now
about what to do about people
who are losing their health insurance
because they've lost their jobs because of coronavirus.
And we happen to have a system of health insurance
in America, which for a lot of people
is tied to their job, and when someone loses their job,
they oftentimes will also lose their health insurance.
And we have people whose housing
is tied to their ability to work,
access to food is tied to their ability to work,
and it does not have to be that way.
There are plenty of other ways we can design society,
ways we can set up government programs
that allow people who are fired
not to be in danger of being hungry, homeless,
and without medical care.
So as we kind of think, well, in the medium to long term,
what exactly are we gonna be doing in the future?
What's gonna happen with these organizations
that we've built?
What's gonna happen with this community power
and capacity that's being created?
Well, we're gonna meet people's material needs
in the short term, but we're also gonna think about
what our vision is for the future
so that the next disaster, pandemic,
emergency situation does not recreate
all of these emergency conditions
and all of these kind of secondary societal impacts
that have created so much damage and destruction.
And those of us who've been on calls and in meetings
focused on how do we get people food
are gonna say, well, in 2021,
who's still hungry in our community
and how do we get them food?
But also what are the changes that we need to make?
What is the societal or governmental shifts
we need to push for
so that people are not gonna be hungry anymore?
And there's a lot of visions about how that can change
and how that can function.
There's kind of left-wing big government socialism.
There is a kind of anarchist mutual aid
non-governmental approach and a variety of other ways.
And I don't have strong principled beliefs
about kind of the best way to solve all these problems.
There's a variety of ways we can talk
about getting people healthcare,
about making sure that people who lose their jobs
do not become homeless.
And that is an important aspect to keep in our minds
as we struggle through coronavirus
is not to treat what's happening right now as unique
there's nothing unique about the way people are struggling
in New York City and across the country.
People have always struggled.
And the solutions that we should envision
are not just how do I get people groceries
for the next two weeks,
but how do we solve a problem where people in America
in 2020 do not have food in their apartments
and cannot afford food.
And that is worse because of coronavirus,
but that is not new because of coronavirus.
Right, right.
Okay, all right.
So plug your stuff.
Where do people need to go?
What do they need to look up?
Where does everybody need to kind of get more information
and where can they start?
Yeah, so you gave a bitly link at the top of the show
which is this information and resources document
for New York City, you know, bit.ly slash NYC coronavirus.
And that can give people in New York City
access really directly to information and resources
that they can make use of,
but also is kind of illustrating, like I mentioned earlier,
people all over the country being able to see
what sort of practices are, you know,
are ratcheting up in New York City,
how organizations are responding,
some best practices, et cetera.
Because New York really was at kind of the beginning
of this wave across the country
and was hit especially hard.
You know, I'm part of a group also called Bed-Stuy Strong.
Bed-Stuy's a neighborhood in Brooklyn.
And we have our own website, bedstuystrong.com,
which again, you know, really kind of allows people
to see what this looks like in practice
at the neighborhood level.
And then I mentioned It's Going Down,
which is just a nationwide publication,
which has been, you know, collecting their own sort
of information and resource documents
about what's been happening across the country.
But again, you know, for, you know,
any major city in the United States,
there have been people struggling with food insecurity,
housing insecurity, poverty, et cetera.
And already existing organizations and groups of people
have been working on solving those problems.
And those organizations, you know,
as a general rule are under-resourced.
They don't have the volunteers to respond.
They don't have the money to respond during, you know,
good times when we're not dealing with a global pandemic.
So, you know, any organization that I can think of
or that I'm aware of is being hit even harder
and they're out there right now.
And if you have time and you can volunteer,
you can give time.
If you have money and you can donate, you can give money.
And if you're someone in need, you can reach out.
Because really kind of one of the underlying philosophies
of this sort of mutual aid organizing
is a participatory process.
There's not, you know, the people with resources providing
and those in need taking,
but everyone has something to contribute
and everyone has the opportunity to make use of that.
And I think what we're seeing under coronavirus
is that everyone is being impacted.
Everyone is being affected
and we all have something to contribute.
And we all, I think, are gonna face struggles
and challenges and are gonna need to be able
to identify the resources we can reach out to
that will be able to help us.
Love it.
Okay.
All right, you got anything else?
Parting shot, anything else you wanna say?
I mean, you summed it up pretty well there.
Yeah, I think I closed on a nice summation
of what I was trying to express.
And so I won't, don't feel the need to belabor it.
All right.
Okay, well, then that's the show.
You know, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}