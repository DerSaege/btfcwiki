---
title: Let's talk about about dancing nurses and Matt Walsh....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=c2QCL7BpYEQ) |
| Published | 2020/04/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Nurses dancing has sparked controversy, with Matt Walsh criticizing their choice to dance instead of working.
- Beau shares a story about Joe Bear, who once danced before a high-stakes mission in a combat zone.
- Dancing before battle has historical roots in boosting morale and psychological preparedness.
- Nurses dancing may serve a similar purpose, mentally preparing themselves for their challenging work.
- Posting dance videos online may help raise morale and show solidarity with those in more strict hospital environments.
- Walsh's criticism of healthcare workers causing economic issues is refuted by Beau.
- Staying home aims to prevent hospitals from being overrun, not because they already are.
- Walsh's lack of understanding of high-stress jobs is evident in his comments about nurses.
- Right-wing pundits like Walsh are looking for scapegoats instead of taking responsibility for their actions.
- Beau stresses the importance of supporting healthcare workers and ensuring their mental readiness for their critical roles.

### Quotes

- "Dancing before battle, that dates back to prehistory."
- "Nurses, docs, dance, do primal chanting, paint your face, whatever it takes for you to pregame and get in the mindset to walk into that room and do your job, do it."
- "If you have a platform right now, you have an obligation to do your part to lead people, to keep them safe."

### Oneliner

Beau defends nurses' dancing as a morale booster and mental preparation for their challenging work while critiquing those like Matt Walsh who fail to understand their high-stress jobs.

### Audience

Healthcare advocates

### On-the-ground actions from transcript

- Support healthcare workers by showing appreciation for their efforts and understanding the challenges they face (implied)
- Educate others on the importance of mental preparedness for high-stress jobs, like those in healthcare (implied)
- Advocate for proper support and resources for frontline workers to ensure their well-being (implied)

### Whats missing in summary

The full transcript provides a deep dive into the importance of morale and mental preparedness for healthcare workers, shedding light on misconceptions and criticisms surrounding their actions.

### Tags

#Healthcare #Nurses #MentalHealth #Support #CommunityAwareness


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about dancing nurses.
And a guy named Matt Walsh from I guess the Daily Wire.
He has an issue with them dancing.
Now if you're unaware, nurses have been doing choreographed dances or conga lines, whatever,
posting it to Facebook, TikTok, YouTube, doesn't matter.
This has apparently hurt Mr. Walsh's feelings.
He feels that they should be busy, working, doing something.
That they shouldn't have time to dance or that this is an indication that it's not really
that bad or whatever.
So he made a tweet, got some backlash, made a video, and we're going to talk about all
that.
But first I want to tell you a story, and it's a story about somebody a lot of you may
know.
If you haven't met him, you probably heard me talk about him doing relief efforts and
stuff like that.
I just realized I didn't ask his permission to tell this story, but I'm going to tell
it anyway because I don't think he's going to care.
So if you've ever met Joe Bear, he's a nice guy.
He is a nice, fun guy.
There's no other way to describe him.
Something you may not know about him is that he once gave a dance of an exotic nature in
the back of an armored vehicle as he was rolling out with Task Force 20 to go snatch a high
value target in the middle of a combat zone.
Seems weird, right?
He shouldn't have time to do that.
He was doing something pretty important.
He shouldn't have time to do that.
Seems odd.
Why would you even want to dance like that anyway?
It's crazy.
Except it's not.
Dancing before battle, that dates back to prehistory.
Dates back to prehistory.
In fact, today, there are still indigenous cultures that will dance around a campfire
and chant and psych themselves up and get them ready for what they're going to deal
with.
Convince themselves that that opposition that they're going to face, it's not going to
touch them.
Gets their mind right.
Gets them psychologically prepared for what they're going to deal with.
It's good for morale.
It's kind of like maybe that's what the nurses are doing because they're walking into battle
too.
They're going to dance in that hallway.
Then they're going to walk into that room and dance with the Reaper to protect your
hypothetical loved one's life.
I would prefer they be in the right frame of mind.
So they're doing it for the age-old reason.
They're also posting them online to raise the morale and show camaraderie and solidarity
with people who are in hospitals that are maybe a little more rigid, that wouldn't
allow that.
Or maybe the videos are made in hospitals where it hasn't hit yet and they're trying
to raise the morale of those that are really in the midst of it.
This isn't a new thing.
It's been going on since before writing.
Okay, so let's get on to the tweet and the video and all that stuff.
So he made the tweet and the video from there certainly appears to be from a maternity ward,
but this bothered him.
He's taking this as evidence that either it's not that bad or they're neglecting patients,
whatever.
Again, we've just got to have somebody to blame right now.
After the backlash that should have been foreseeable, he makes a video.
The video says that health care workers caused us to voluntarily plunge ourselves into a
Great Depression.
That's not true.
That's inaccurate.
The advice of health care workers, which weren't nurses by the way, probably would have triggered
a recession.
That's true.
The depression came from people like Mr. Walsh, people who pushed the idea that this wasn't
a big deal, people that pushed the idea that they should still go out, people that provided
information that may have been, let's just say, less than accurate, such as Mr. Walsh's
tweet where he talks about we shouldn't have to handle the entire country the way we do
New York, because of population density.
Population density has nothing to do with this.
There's a whole bunch of people in South Dakota that can attest to that right now.
Everybody in that cluster.
It's not the population density of a city or state that matters, it's the activities
and how close they are together, and the longer people go out unnecessarily and engage in
those activities and get close and don't social distance and don't wash their hands and don't
wear a mask and don't do all of the things that they need to do to slow it down, the
longer this is going to last.
It takes us from a recession to a depression.
This could have been over in like 30 days had there been some solidarity, some camaraderie,
had people with a following actually got behind it and said, yeah, this is what we need to
do.
We kind of have that obligation.
I would suggest we have that obligation.
He goes on to say that we did this because the hospitals were overrun.
No, no, that's not why we're doing it.
It's not why we're staying home.
We're staying home to make sure they're not overrun.
The fact that some nurses have time to make videos kind of suggests that it's working.
That's the whole idea of that curve that everybody's talking about.
It seems like somebody with a following should have researched and understood this by now.
The idea is to keep it below the threshold that our medical infrastructure can handle.
So the whole point is to make sure they don't get overrun.
Then he makes the plea on behalf of the employees.
This is unprofessional.
This isn't a good idea.
This is wrong because there might be employees who lost their jobs who are now in a food
bank line and they're seeing this and thinking if nurses can dance, why can't I go back to
work and that's a good question, he says.
No, it's not.
No, it isn't.
And it's your duty as somebody with a following to explain why it's not because if they go
out, and they engage in activities that are not essential, then this gets prolonged and
the recession gets turned into a depression.
They can't go back to work so they don't end up like that cluster in South Dakota.
It's not a good question.
He goes on to say that he gets high-stress jobs.
No.
No, he doesn't.
I'm certain of that.
And he follows that up pretty quickly with the idea of saying that he knows, knows mind
you, that nurses aren't like going home and grieving for their loved one, or for his loved
one, his hypothetical loved one who lost their life.
They're not grieving for them.
They're not mourning them.
He thinks he knows that.
I can assure you that's wrong.
Nurses do in fact go home and grieve for patients.
They'll go home and grieve for patients that weren't theirs simply because they reminded
them of somebody.
Or maybe they were the same age as their kid.
It happens a lot.
I don't think he understands high-stress jobs because then the dancing and all of these
claims would not have been made.
They wouldn't have been made.
He goes on to say that he's not sorry because he knows that the more people that tell him
that he's wrong, well that just means that he's right because that makes sense.
This whole community of right-wing pundits, they're going to start doing this a lot.
They're going to be looking for scapegoats.
They're going to be trying to find people that they can pin this on and blame it on
because they don't want to accept responsibility for prolonging this because their whole shtick
on that side is to say, oh, pull yourself up by your bootstraps.
You're better than those people over there, those people that needed assistance.
And they do all of this pandering to make those people feel better than somebody below
them.
They do everything they can to kick down.
And now that the audience they pander to needs help and they need assistance, man, they got
to come up with somebody to scapegoat.
If they don't, it may shake the very foundations of what they believe, their whole platform,
because people might need help.
Maybe that means that those other people that they've been looking down on, kicking down
on, maybe they just had something happen too.
Nurses, docs, dance, do primal chanting, paint your face, whatever it takes for you to pregame
and get in the mindset to walk into that room and do your job, do it.
We got your back.
Those of us who actually get high stress jobs, those of us who have seen the downside of
not being focused.
And that's the last thing I want to touch on.
He talks about nurses needing to show more decorum because of the family members.
It would be upsetting to family members to see nurses dancing.
He retweeted a message that said that, you know, they're obviously not camera shy, so
why don't we have footage of these overrun hospitals?
It would be upsetting to family members to see nurses dancing, but it wouldn't be upsetting
to see their family members as patients gasping for breath just so some pundit, some podcaster
can have a talking point.
If you have a platform right now, you have an obligation to do your part to lead people,
to keep them safe.
And if that doesn't do it for you, your obligation, your responsibility to society, let me give
you the personal interest that motivates the right wing most of the time.
These are your Patreon sponsors.
These are the people that buy your merchandise.
These are the people that watch those ads that you get revenue for.
And your stances are literally killing them.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}