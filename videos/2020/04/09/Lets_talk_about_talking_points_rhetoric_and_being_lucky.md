---
title: Let's talk about talking points, rhetoric, and being lucky....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hfW0LSiAMoQ) |
| Published | 2020/04/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the concept of nationalism and how it is being invoked in current rhetoric.
- Points out the dangers of nationalism, especially during times of crisis.
- Emphasizes the importance of global cooperation in fighting against the current situation.
- Mentions the international support received in the form of medical supplies from various countries.
- Advocates for pushing back against nationalist rhetoric and focusing on global unity.
- Stresses the need for immediate cooperation and support across borders.
- Warns about the ineffectiveness of trying to contain the situation by locking down borders.
- Calls for proactive and worthy actions to be taken, rather than just invoking nationalism.
- Criticizes lackluster leadership and the damage caused to the country's reputation.
- Encourages staying at home, practicing good hygiene, and unity in the global fight against the situation.

### Quotes

- "We are in this together."
- "It doesn't care what color your passport is."
- "This thing, it does not care about your borders."
- "Do something worthy of being proud of."
- "We are all in this together."

### Oneliner

Beau addresses nationalism, advocates for global unity, and criticizes lackluster leadership during a time of crisis, stressing the importance of international cooperation in fighting against the current situation.

### Audience

Global citizens

### On-the-ground actions from transcript

- Reach out to international organizations or individuals to offer support and aid (suggested)
- Advocate for global cooperation and unity in fighting against the situation (implied)
- Practice good hygiene, stay at home, and avoid touching your face (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of nationalism, the importance of international cooperation, and the need to push back against divisive rhetoric to effectively combat the current situation.

### Tags

#Nationalism #GlobalUnity #Cooperation #Leadership #InternationalAid


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about talking points, rhetoric, and being lucky.
Because we are lucky, right?
Born in the greatest country on the planet, USA.
A country that is normally unscathed by things like this.
Because we've got those oceans protecting us.
It's been that way since we were founded.
And we tend to be proactive.
We're in a different situation now.
So the talking points have gone out.
Everybody's got their script and man they are sticking to it.
It's time to rally everybody.
Get everybody in the fight, everybody back in the calls.
The problem is we're the US.
And since we are the United States we only rally for one reason.
And you better hope that you're not the country we're rallying against.
Because all it takes is for somebody to wave a flag and we are ready.
And that's the rhetoric that's getting used.
That wartime rhetoric.
This is our Pearl Harbor moment.
And sure, as an analogy, okay, yeah it's going to be in history books, it's going to be relevant,
we all have to come together.
Got it.
If that's where it stops, fine.
But that's not where it's stopping.
Invoking that nationalism.
But the thing is, this isn't wartime.
We don't have any opposition.
None we can see anyway.
But it becomes all about the US.
Makes it real easy to write off those others, those people everywhere else.
Because they're not really a part of the US, so they're not with us.
And in wartime, if you're not with us, well you're against us.
And that allows us to justify things we might not normally justify.
Like withholding supplies.
Cutting off funding to international organizations trying to help.
Because they're not with us.
That nationalism.
That nationalism isn't helpful.
In the best of times, nationalism is a disease far more dangerous than what's out there right
now.
Because it infects your mind.
Causes you to take pride in things you had nothing to do with.
And hate people you've never met and did nothing to you.
I'm really glad other countries aren't using that rhetoric.
Because I picked up supplies today.
Cases upon cases.
Cases this one of the items.
Hope it's clear.
Hope you can read it.
Of course, if you want to read it, you better be familiar with Italian.
Italy.
They're having a hard time right now.
But we got gowns.
The masks came from China.
The goggles Taiwan.
The pressure infusers came from Mexico and Israel.
The long IV tubing so the nurses don't have to go into the room each time and they can
save on supplies.
Brazil.
We are in this together.
But we appear to be the only country that doesn't understand that.
That thing out there, it doesn't care what color your passport is.
It doesn't care what language you speak.
And it doesn't care what's on your birth certificate.
We are in this together.
We have to focus on the most immediate need first.
Some of those places are in the United States.
Some of them are elsewhere.
Cooperation is what will get us through this.
Because this thing, it does not care about your borders.
It's not going to get a visa.
And contrary to the branding, you can't actually lock down borders like that.
And if we do succeed in writing everybody else off and letting it run rampant everywhere
else and controlling it here, we never come off lockdown.
You better get real used to staying at home and watching Netflix.
Because it doesn't care about those imaginary lines.
Those borders, they only exist because we say what they do.
This thing, it exists either way.
Whether or not you believe in it.
So sure, it's our Pearl Harbor moment.
You want to be proud of the United States?
You want to invoke that nationalism?
Do something worthy of being proud of.
We have had years of lackluster leadership that has damaged our reputation everywhere.
And it's getting worse by the day.
When this type of rhetoric comes out, when that nationalism gets invoked, we have to
push back against it.
They're helping us.
We should probably help them too.
Because this is a global fight.
We are all in this together.
And that doesn't stop at the borders.
Anyway, it's just a thought.
Stay at home.
Wash your hands.
Don't touch your face.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}