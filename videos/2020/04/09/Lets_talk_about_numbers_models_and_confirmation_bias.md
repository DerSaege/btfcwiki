---
title: Let's talk about numbers, models, and confirmation bias....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Yht4551HDQk) |
| Published | 2020/04/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Confirmation bias occurs when individuals seek out evidence that supports their theory and ignore anything that contradicts it.
- The conservative movement in the United States is currently suffering from confirmation bias, believing that the COVID-19 situation was overblown.
- Some individuals argue that the lower numbers and revised models are proof that the pandemic was exaggerated by the media.
- The lack of leadership in the country has led to governors taking on the role of acting president to combat the crisis.
- Had certain actions been taken earlier, the impact of the pandemic could have been significantly reduced.
- Despite the decreasing numbers, it's vital to remain vigilant and push back against false narratives to prevent a resurgence.
- The importance of continuing precautions until a treatment is developed is emphasized to avoid a relapse.
- There is a concern that if people believe the pandemic was overhyped and start resuming normal activities too soon, there could be a resurgence of cases.
- Beau stresses the need for vigilance to prevent a second wave of infections and the importance of not letting our guard down prematurely.
- Remaining cautious and pushing back against misinformation is key to ensuring the situation does not worsen.

### Quotes

- "Had this been done sooner, those numbers would be even lower."
- "Half measures are half effective."
- "We have to remain vigilant."
- "It's more important than ever to push back against a false narrative like this."
- "And if we don't, those models, they're just going to get longer."

### Oneliner

Confirmation bias fuels skepticism in conservative circles about the severity of the pandemic, urging continued vigilance to prevent a resurgence.

### Audience

Public health advocates

### On-the-ground actions from transcript

- Remain vigilant and continue to follow safety guidelines to prevent a resurgence (implied)
- Push back against misinformation and false narratives surrounding the pandemic (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of confirmation bias, the impact of leadership decisions on the pandemic response, and the necessity of remaining cautious despite decreasing numbers.

### Tags

#ConfirmationBias #COVID19 #Vigilance #Misinformation #PublicHealth


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about numbers, models, and confirmation bias.
Confirmation bias is what happens when you're certain of something.
So you seek out evidence that supports your theory, and you ignore, well, everything else.
Anything that could suggest that you're wrong.
It happens a lot, and you will see it with people who research, quote, certain theories.
They look for things that support that theory, and then don't look at anything that might
cast doubt on it.
That is confirmation bias.
A large portion of the conservative movement in the United States is currently suffering
from this.
Those models, those numbers that we've all been worried about, they're being dropped.
They're getting lower and lower, they're being revised constantly by people who know what
they're doing, what they're looking at.
Because these numbers are getting lower, this is proof positive for many in the conservative
movement within the United States that this was all overblown.
That it was media hysteria, that it was overhyped, that it wasn't really that big of a deal,
that instead we all should have just gone about our business and sacrificed ourselves
on the altar of Wall Street.
Gotta save the economy.
It's just gonna be some old people, some compromised people, maybe a few healthy people, but you
know, if you're gonna make an omelet, gotta break some eggs, right?
And as they point to these numbers, they refuse to take into account the fact that large portions
of the country have been on lockdown for a month on Instagram, posting selfies like mugshots.
This will be spun in an attempt to prove that dear leader was right.
Dear leader was wrong.
He's pretty much always wrong.
Those models reflect the tenacity and the drive of those governors who took up the mantle
of acting president when the Oval Office became a vacant seat.
Because we don't have leadership.
We have somebody, well, going by intuition, with a panel of experts surrounding him, contradicting
him.
Had this been done sooner, those numbers would be even lower.
This is something that could have been avoided.
It could have been mitigated.
Had the president, dear leader, listened to those experts around him, not gutted the CDC,
not gotten rid of the National Security Council pandemic response team, not canned or forced
out people who were monitoring the very area this probably started.
The one action the president took that he likes to constantly take credit for, shutting
down air travel from China.
As it turns out, it does appear that most cases in New York, well, they came from Europe.
Half measures are half effective.
And as we watch those numbers decline, and we all take a deep sigh of relief, we need
to remember that there are going to be those who try to spin that politically and who try
to take credit for the very policies they opposed.
As they advocated for their constituents, whom they showed no regard for, to go out
and just go on about their day, now they want to claim credit for the actions of those who
said stay home and the outcome that can only be attributed to that.
And understand, it's not over.
We are still in for a very bumpy ride.
And even once those numbers drop and get real low, we're still going to have to keep this
up until we develop a treatment.
We're going to have, things are going to change in a big way.
That's why it's more important than ever to push back against a false narrative like this.
Because if people start to believe that it was overhyped, that it was media hysteria,
and they get back out there, as soon as it starts to clear up, it's going to flare right
back up.
And we're going to be right back where we started.
And we're going to have to go through all of this again.
We have to remain vigilant.
And if we don't, those models, they're just going to get longer.
And the longer they go on, the higher those numbers are going to be.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}