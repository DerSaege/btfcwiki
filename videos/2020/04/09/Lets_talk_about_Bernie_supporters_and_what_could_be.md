---
title: Let's talk about Bernie supporters and what could be....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pqYVdvauW6A) |
| Published | 2020/04/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Urges Bernie supporters to take action post-election.
- Encourages building worker co-ops and organizing.
- Suggests utilizing the current economic pause to make systemic changes.
- Points out the availability of cheap retail and restaurant spaces post-pandemic.
- Emphasizes the need for workers to lead themselves and create change.
- Advocates for action over waiting for political change.
- Stresses the importance of organizing and educating co-workers.
- Mentions the possibility of creating alternatives to existing systems.
- Advises taking advantage of the chaos for positive change.
- Calls for demonstrating that alternative ideas can work practically.

### Quotes

- "Build your worker co-op. Organize. Educate."
- "This is the time to do it."
- "It's a wide cross-section. All it takes is organization."
- "Not with your vote, but with your action."
- "It's time to lead ourselves."

### Oneliner

Bernie supporters urged to build worker co-ops, organize, and seize the current pause in the economy to create systemic change - it's time to lead ourselves with action, not just votes.

### Audience

Bernie supporters

### On-the-ground actions from transcript

- Build worker co-ops, organize, and educate co-workers to create systemic change (exemplified)
- Take advantage of available cheap retail or restaurant spaces to start new ventures (implied)
- Create alternatives to existing systems, like a worker-friendly Uber Eats competitor (implied)
- Lead by example and show practical success of alternative ideas through action (exemplified)

### Whats missing in summary

The full transcript provides detailed examples and a thorough explanation of how Bernie supporters can actively create change post-election by building worker co-ops, organizing, and seizing opportunities during the economic pause.


## Transcript
Well howdy there internet people, it's Bo again.
So today we gotta talk to the Bernie supporters.
Yeah.
Okay, so it didn't happen.
Again, what are you gonna do?
Don't worry, this is not a vote blue no matter who video.
You're gonna decide who to vote for on your own.
But what are you going to do?
If you supported Bernie, odds are you supported him
because of principles, ideas.
Ideas that the American people may not be ready for yet.
So what does that mean?
Does that mean you're wrong?
No, it means they're not ready for them yet.
But what can you do in the meantime?
You're gonna stay idle, wait for the next promising politician
that kind of aligns with your views?
Or are you gonna take advantage of the very,
very unique situation that we're in?
Right now the economy is paused.
Most Bernie supporters are granting him that support
out of economic reasons.
And the economy is paused.
You want a change in the economic system
that we have in the United States?
And the economy is paused.
When this is over, that race starts again.
And yeah, the billionaires and millionaires,
they're gonna have that inside track.
They're gonna have that inside lane.
They're gonna have that advantage.
But that doesn't mean that you're out of the race.
A lot of you have been furloughed or laid off
from a company, something that you know how to do,
and all of your co-workers know how to do.
Those major corporations that have plagued
the American worker for so long,
well, they've already trained the people you need.
They've already hired them.
They've already got them ready.
And right now they have nothing to do.
You know who your co-workers are.
If you want a society where workers have a say,
why not build it from the ground up?
Because it's clear you're not going to be in a position
to have it legislated for quite a while.
So why not take advantage of the situation that we're in?
Build your worker co-op.
Organize.
Educate.
So you can agitate when the race starts again.
Imagine organizing your co-workers
who are currently at home to set up a little worker co-op
in whatever your industry is.
Now, this doesn't apply to everybody.
Not everybody can do it.
You know, if you worked for a retail giant,
yeah, this may not work exactly.
But there's nothing to say you couldn't open a retail store
of some kind.
Because when the economy restarts,
a whole lot of things are going to be different.
You're going to have retail space available for cheap.
Because they're going to want to fill it.
Because a lot of them, they're empty now,
and they're going to be empty in the future.
If you work in a restaurant,
there's going to be restaurant space available.
Cheap.
There's going to be equipment available.
Cheap.
If you are a delivery driver,
and you're upset with how you're being treated,
there's no reason that you can't found a Uber Eats competitor
that takes care of its employees.
There's no reason.
There's nothing stopping you and your co-workers
from building the system you want from the ground up.
This is the time to do it.
This is the time to do it.
Because when that race starts again,
you can be responsible.
You can play your part in building that society you want.
You can make sure that everybody has a living wage.
If it goes well, a few months,
you can make sure everybody has health insurance.
And yeah, it's not the ideal,
but it's better than what we have right now.
And we're in the position to actually make that change.
It's become very clear that we can't count
on the government for much.
And I'm not saying that the Democratic establishment
didn't push the other candidate.
Not saying that at all.
I'm saying that it's very likely
that those ideas are ahead of their time,
that right now the majority of Americans won't back them.
But what if you showed them they work?
There have been examples.
There have been examples.
And everybody right now, most people,
especially essential workers, those sacrificial lambs,
they're probably less than thrilled
with your current employer.
This is the time.
This is the time.
They spent the money training them.
They spent the money training you.
Organize.
The race is paused.
And when it restarts,
you're going to have an opportunity that you're not going to have again.
If you want to make a move, this is the time to do it.
If you look at all of those billionaires,
Warren Buffett,
buy when there's blood in the streets.
Chaos, the chaos that we're experiencing right now,
provides the opportunity for people who can see through it,
who can plan, who can be ready to get ahead.
Wouldn't it be nice if this time the people that got ahead
were the people that care about their workers?
And the Bernie bros and the Bernie babes?
Aside from those names,
they're in the best position to make that happen.
It's a wide cross-section.
All it takes is organization.
And then once you have your little co-op,
your little worker co-op,
your organization that is playing within the rules,
but quietly playing by their own,
you can upset the status quo.
Because those other workers,
the ones that are forced to go back and work for these companies
that do not care about them,
they're going to want to work with you.
And it may inspire them to organize.
You get enough organization,
and maybe we can pause everything again if we need to.
You want deep systemic change?
You can do it.
You can do it.
Not with your vote, but with your action.
We haven't had real leadership in this country
for a very long time.
It's time to lead ourselves.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}