---
title: Let's talk about operators, the tacticool, and public gatherings....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NjE24GK3ijU) |
| Published | 2020/04/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the presence of individuals pretending to be operators in public gatherings.
- Emphasizes the importance of education and expertise in real warriors.
- Mentions the focus on saving lives and mitigating risk in high-speed operators' actions.
- Stresses the significance of listening to subject matter experts rather than propaganda.
- Expresses the belief that operators prioritize risk mitigation, which includes wearing a mask during public gatherings.
- Addresses the issue of people undermining efforts to save lives during the pandemic by not taking necessary precautions.
- Criticizes the political show and lack of precautions in public gatherings.
- Mentions the concept of herd immunity and its flaws in the context of the current situation.
- Encourages staying at home, washing hands, and wearing masks when necessary to combat the spread of the virus.
- Advocates for being educated and making informed decisions without the need for government orders.

### Quotes

- "Real warriors are educated."
- "They're about mitigating risk."
- "Knowledge is power. Education is power."
- "There shouldn't have to be government orders telling you to do this."
- "Shouldn't have to have an order, but because you know enough to stay home on your own."

### Oneliner

Beau stresses the importance of education, risk mitigation, and following health guidelines, urging individuals to make informed decisions without relying on government orders.

### Audience

Community members

### On-the-ground actions from transcript

- Stay at home, wash hands, and wear a mask when going out for essentials (implied)
- Strive to be educated and listen to subject matter experts (exemplified)

### Whats missing in summary

Insights on the impact of informed decision-making and education in public health crisis responses.

### Tags

#Education #RiskMitigation #PublicHealth #Community #HealthGuidelines


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about operators.
Because there's a whole lot of public gatherings going on right now.
And in every crowd there is somebody dressed up like an operator.
Who clearly isn't.
There is some tactical guy out there sporting his plate carrier and his oakleys.
And man, he's got it.
And I get it.
I get the desire to emulate and mimic those top tier high speed guys.
I get it.
I do.
I know a lot of them.
They are impressive individuals.
But they're not impressive because they can clear a room.
A lot of people can clear a room.
They're not impressive because of how they dress.
I want you to think back to the bios of those people who have had movies made about them.
Who've written books.
The people that actually embody that high speed, those top tier guys.
How many languages did they speak?
What were their degrees, plural, in?
These are not willfully ignorant people.
They are not factually challenged.
Real warriors are educated.
They go out of their way to become educated in things other than their primary profession.
Their entire job is risky.
Why do they train so much?
Why do they rehearse things?
Why do they do that?
Why do they spend so much time, so much of their lives, practicing?
It's to mitigate risk.
These guys don't go off on hunches.
It's to mitigate risk.
It's what they do.
Because while I know that in the US we focus on one aspect of it, but when they kick in
a door 90% of the time, the end goal is to save lives.
Yeah in a lot of situations they're going to take a few on the way, but the strategic
goal of what they're doing is to save lives, and they mitigate risk at every possible opportunity.
There's no way that one person can know everything, right?
So what do they do?
They trust subject matter experts.
They listen to those people who spent their lives in that field.
They don't listen to talking points.
They don't listen to propaganda.
Not when it comes to the mission.
They listen to the experts.
I'm starting to think the world would have been better served had Fosse come out wearing
a plate carrier.
Would have been able to get the attention of those that need some help.
Those who are right now undermining the mission.
If you were to take one of those high speed, top tier operators right now, and send them
to one of these public gatherings, and give them a choice of wearing one of these, or
wearing one of these, what do you think they'd take?
They can only have one.
I'm willing to bet an overwhelming majority would take the mask.
Because they're about mitigating risk.
They're also not whiny, generally speaking, guys.
They're really not.
They don't complain much.
Hanging out at home isn't a real big sacrifice to be asked to make.
And I know, there's this idea that maybe it's all blown out of proportion.
Maybe the experts are wrong, and the guy who reads the teleprompter on Fox is right.
Fine.
Let's entertain that idea for a second.
That's not the case.
We're already over 40,000.
And I know people are saying, well, that's just, that's like a flu.
You know, that's like a yearly...
Yeah, except it was in a month, with the country shut down.
This has saved thousands of lives.
Fact.
And you're undermining it.
You want to get back to normal.
Everybody does.
Everybody does.
Nobody wants to be stuck at home.
Well, very few of us.
But we have to finish the mission first.
And every one of these little gatherings, so you can stand out there and wave your flag
and cry and whine, undermines it.
Because you're not taking the precautions you should.
It's a stunt.
It's a political show encouraged by people who are worried about themselves and not worried
about you.
Tens of thousands in this short period of time are gone.
How many would that be had we just stayed open?
And I know there's some people talking about herd immunity.
We're working towards herd immunity.
I actually saw a government official from another country say that that's what they
were trying to achieve.
That's fantastic.
That makes sense.
Except for the fact that now we realize you can get it again.
This isn't the chicken pox.
And there's no evidence whatsoever to suggest how effective those antibodies are.
No evidence.
You think those top-tier high-speed operators that you want to emulate, you think they go
off on hunches in real life?
No.
They don't.
They know the number of feet between the power line and the building.
They don't go off on hunches.
They don't just wing it.
They plan.
And they're educated.
I have no problem with people wanting to emulate those guys.
Like I said, I know a lot of them.
They are people worthy of emulating in a lot of ways.
But if you just focus on the one side of it, you're missing what makes them impressive.
They are educated.
They take their time to learn about things and they stay forever curious.
Because it's going to save their life.
It's going to save the life of their teammate.
Knowledge is power.
Education is power.
Right now, that plate carrier, especially given the fact that a lot of them are like
mine, guys, real life, if yours folds like that because you don't have the plate in it,
it's basically a sweater vest.
These things aren't Kevlar.
It's nylon.
It's sad.
I mean it looks cool and all, but it provides no protection whatsoever.
And this will provide more protection.
Stay at home.
Wash your hands.
Don't touch your face.
If you have to go out for essentials, not to cry and whine, wear a mask.
Participate.
You're right.
There shouldn't have to be government orders telling you to do this.
Because we should have a population of people who strive to be educated.
Who listen to subject matter experts.
You're right.
Shouldn't have to have an order.
But not because you want to go to Baskin Robbins, but because you know enough to stay home on
your own.
It's just a thought.
Have a good day. Bye.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}