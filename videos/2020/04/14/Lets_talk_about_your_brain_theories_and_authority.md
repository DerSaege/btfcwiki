---
title: Let's talk about your brain, theories, and authority....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tTzzoA6VFCw) |
| Published | 2020/04/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how the brain seeks patterns to make sense of things like clouds resembling bunnies or faces.
- Talks about the motivation behind various theories - the desire to bring order out of chaos for comfort.
- Points out the common agreement among theories about ushering in a new authoritarian rule.
- Criticizes a theory that suggests the normalization of masks and gloves is a tactic for control.
- Questions the feasibility of theories that undermine the government's basis and ability to protect its citizens.
- Mentions how the enforcement class may be reluctant to exercise control due to undermining methods.
- Talks about the impact on military readiness and the restriction of social gatherings under authoritarian rule theories.
- Argues that chaos does not benefit those in power in the long term.
- Suggests a more plausible theory that an authoritarian system already exists, unnoticed by many.
- Addresses the coercion within the current system that limits individual autonomy and agency.
- Points out that many advocating against authoritarian rule are already living under its grasp.
- Notes that the government has had powers to enforce measures like stay-at-home orders for a long time.
- Asserts that surveillance measures should be opposed, especially due to government data handling concerns.
- Emphasizes the importance of existing in a system that allows freedom and autonomy.

### Quotes

- "Your brain is seeking out a pattern. It's trying to make sense of it."
- "The main plot point of these theories doesn't make sense."
- "The authoritarian rule people who subscribe to these theories are worried about, it's already here."
- "If you want freedom, you have to exist in a system that allows it."
- "If somebody can coerce you into going to risk yourself for their economic benefit mainly, and you get the crumbs, you are existing in Orwell's future."

### Oneliner

Beau explains theories seeking order from chaos, questions new authoritarian rule claims, and argues that current coercion hints at an existing authoritarian system.

### Audience

Activists, skeptics, truth-seekers

### On-the-ground actions from transcript

- Advocate for transparency in government surveillance practices (implied)
- Educate others about the potential coercion within existing systems (implied)
- Support policies that enhance individual autonomy and agency (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of how individuals may unknowingly exist within an authoritarian system and the importance of advocating for true freedom within societal structures.

### Tags

#BrainFunction #Theories #Authoritarianism #Coercion #Freedom


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about how your brain works and some of the wilder, let's
just call them theories, that are floating around out there.
When you look at a cloud and you see a bunny or a face or you see it in smoke or in a pattern
in the texture on the wall, are you actually seeing a bunny?
No, you're not.
Your brain is seeking out a pattern.
It's trying to make sense of it.
It's what your brain does.
It's designed to do that.
It makes you feel more comfortable to be able to identify something.
It makes you more comfortable to understand what you're dealing with, to be able to see
that.
So that's what's happening.
That's where a lot of these theories are coming from.
People are trying to bring order out of the chaos to make themselves feel more comfortable.
That's why there's a lot of variation in the theories themselves, in their starting points.
You know, it was this, it was that, it was these people, it was those people, it doesn't
matter.
Because we're going to skip over all of that.
We're going to go to the main plot point.
The main point that everybody's kind of agreeing on.
This was all designed to usher in a new authoritarian rule.
Cool.
Let's just talk about that for a second.
The string pullers of the world, those Ivy League individuals, these are generally pretty
smart people that get named in these things.
Their plan to usher in authoritarian measures was to create a situation in which they have
normalized wearing masks and gloves everywhere you go.
Because it's much easier to control people when you can't identify them.
They've created a situation in which half the country is stocking up on food, medicine,
water, a lot of them guns and ammo.
All of the stuff you would need to maintain or fight back in the event of an authoritarian
rule.
It's not looking so good.
This theory doesn't make a whole lot of sense.
More importantly, it undermines the very basis of government.
What is a government there for?
Why do people think they require a government?
Well they need to be protected.
It's clearly demonstrating the government is incapable of doing that.
Doesn't breed a whole lot of faith.
It makes the enforcement class, those cops and correctional workers, makes them unlikely
to exercise the government's monopoly on violence because they don't want to touch anybody.
It undermines their most trusted and most effective method of control.
It degrades military readiness, something they would need if they were intent on ushering
in this new regime.
It curtails creature features, concerts, sporting events, all of this stuff that ever since
the days of Rome, the elites, the rulers have known is required to keep the populace, us
common folk, from about rebelling.
Because everybody's at home, everybody's using electronic communications, which creates so
much sigint it's impossible to filter.
So they can't monitor it.
There's just too much.
It closes those circles of trust and creates ad hoc cells, de facto cells, something they
would be very unlikely to encourage.
The main plot point of these theories doesn't make sense.
Those people at the top, they benefit most from tranquility, from calm.
They can make big short-term gains during chaos, but overall, long-term growth, business
as usual, is what helps them.
But people want to see that bunny.
They want to bring that order out of chaos.
It makes them feel more comfortable.
Let me tell you a more likely theory.
The more likely theory is that it's already here.
And you don't know it.
South Dakota's governor was very reluctant to issue a stay-at-home order, so much so
that now it's kind of out of control there.
The mayor of Sioux Falls is begging for a stay-at-home order.
The mayor of Rapid City has signed a petition for one, along with like 30,000 other people
in a very low-population state, asking for one.
The governor, who is one of Trump's minions, said that it was the individual's right to
determine whether or not they go to work or stay home or go to church or whatever, because
we're in America and we have that freedom.
I agree with that.
I agree with that.
It is your right to determine that.
It is.
You have that authority, that agency.
You should have that.
So why aren't people doing it?
Because they already exist in a system that is authoritarian, and they don't know it.
There's an inherent coercion in our current system, because the average person can't exercise
that autonomy, that agency, that authority, unless the government tells their boss it's
okay.
Otherwise they'll get fired.
Their landlord will throw them out.
There's an inherent coercion in this system.
It's not a bug.
It's the design.
The authoritarian rule people who subscribe to these theories are worried about, it's
already here.
It is already here.
And well, they love Big Brother.
They're willing to go out and advocate and say, well, I need to go to work because if
I don't, then this will happen.
And they think it's a good idea.
Because they love Big Brother.
They love that system that much.
Most of those people who are already, who are advocating these theories, they're already
firmly in the grasp of that authoritarian rule.
The one they fear.
I was going to do a whole video on this next point, but there's no reason to.
People have been asking how long, or when did the government get all of these new powers?
When did they become able to do this and order people to stay at home and blah blah blah?
They've had them for a really long time.
They put them into widespread use more than a hundred years ago.
They've had them for much longer than that.
This isn't new.
Some of the surveillance measures are new, and those should be opposed.
I don't trust the government with my data, especially given the fact that they can't
even handle their own.
But it's not out of fear of some authoritarian regime.
Because that's already here.
And most of the population doesn't know we're living under it.
If you want freedom, you have to exist in a system that allows it.
And if your system has you under the boot of somebody else, you don't have freedom.
It doesn't matter what flag they wave, what song they play, or what quotes they use.
If somebody can coerce you into going to risk yourself for their economic benefit mainly,
and you get the crumbs, you are existing in Orwell's future.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}