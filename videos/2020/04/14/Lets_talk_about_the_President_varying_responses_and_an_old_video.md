---
title: Let's talk about the President, varying responses, and an old video....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tRLWOyBZLRE) |
| Published | 2020/04/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes the president for refusing to lead during the COVID-19 crisis.
- Points out the president's attempts to divert blame and not take responsibility for the situation.
- Mentions how the president disregarded advice from experts and failed to provide unified leadership, resulting in a prolonged crisis.
- Advocates for staying home as a solution to the pandemic, with government support for individuals.
- Calls for strong leadership to guide the country through the crisis and prioritize human life over the economy.
- Emphasizes the importance of unified action to effectively combat the pandemic.
- Expresses disappointment in the lack of leadership and decision-making at the national level.
- Urges individuals to make informed decisions and not rely solely on government directives.
- Warns against half measures and stresses the need for a comprehensive approach to battling the pandemic.
- Encourages everyone to prioritize public health and safety above all else.

### Quotes

- "His decisions caused this. And his decisions are prolonging it."
- "If you want leadership, you're going to have to do it."
- "You are expendable. You have to make the decision."
- "The shortest path to getting the economy back on track is everybody staying home for a month."
- "It's got to be everybody."

### Oneliner

Beau criticizes the president for lack of leadership during the pandemic, urging people to prioritize public health over the economy and make informed decisions.

### Audience

Americans

### On-the-ground actions from transcript

- Stay home for a month to help get the economy back on track (implied).
- Prioritize public health over economic concerns (implied).
- Make informed decisions regarding personal safety and well-being (implied).

### Whats missing in summary

Beau's detailed analysis of the president's actions and their impact, along with a call for unified action and prioritizing human life over economic interests.

### Tags

#Leadership #COVID-19 #PublicHealth #GovernmentResponse #CommunityLeadership


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're gonna talk about the president,
we're gonna talk about varying responses,
and we're gonna talk about that old video.
So the president gave his little
whatever that was today.
And he refused to lead, still.
He's still refusing to lead the country.
He's trying to divert attention.
You campaigned for the job, sir.
You're the focal point.
Your decisions, your actions matter
as much as it pains most of us to admit it.
Still trying to divert attention,
trying to blame the media,
trying to blame anybody but himself.
Refusing to take responsibility.
Saying that they did everything right.
20,000 dead.
And don't think he could have done anything better.
Said that nobody was even talking about it back then.
Back in January, back in February.
It's funny because somebody brought up
the first video I did on this,
which was in January, by the way.
And if you go back and watch it,
I'll put the link down there.
If you go back and watch it,
you'll find out that it opens with,
we're gonna talk about this because my inbox filled up.
My inbox filled up because the media was covering it.
That's just a lie.
It's just something he made up
because he can't accept responsibility.
Somebody brought it up today,
asking me if I wish I hadn't made it.
No, no, absolutely not.
I'm glad I made it.
Yeah, I mean, I do say, you know,
it's not time to panic.
Be calm.
All of that stuff.
I don't see this as doomsday.
It's not doomsday.
It's bad.
It's a whole lot worse than it had to be.
But if you go back and watch that video,
what you will see is that all of the stuff
that they've advised, as far as washing your hands,
sneezing or coughing into the crease of your arm,
keeping distance, and then some dramatic,
was the term I used, responses,
this is all normal.
It's all planned.
It's all part of the plans that have been on the books
for a really long time.
In that ending, go about your day,
do what you do, and let them do what they do.
They're good at their jobs.
And I'm talking about the people who respond to this.
I really wish the president had seen it
and followed that advice.
Because had he allowed the experts to do their jobs,
we wouldn't be here.
He's spending all of this time trying to divert blame
because he is to blame.
His decisions caused this.
And his decisions are prolonging it.
His failure to lead is prolonging it.
It's making it worse.
Right now, here in Florida, you've got one county
enacting a curfew and talking about requiring people
to wear masks.
Not too far away, you've got another one
talking about reopening the beaches.
Because there's no leadership.
You know, people expect Trump to take action.
And yeah, there's a whole bunch of tools
that are available to him.
The truth is, he doesn't need to use any of them.
He didn't.
All he needed to do was the one thing he loves,
get on TV and talk.
But instead of diverting blame and trying to paint himself
as the greatest and trying to save the economy
by downplaying it, he could have got on there and said,
hey, this is what we're going to have to do.
Everybody is going to have to stay home.
You don't need orders for that.
If the President of the United States came out
and he was forthright with it and said,
this is where we're headed, this is
what we're going to have to do, this
is what all the experts are saying,
we're going to have to stay home,
you wouldn't have the battle, the partisan battle,
over whether or not it's a hoax.
How bad is it?
Because there was leadership.
There was unified leadership.
But he couldn't provide it.
He didn't want to, because he was worried about the stock
market instead of you.
He was worried about those approval ratings.
And there are people who say, yeah,
the economy is really important.
We do have to worry about that.
Yeah, that's fine.
Understand that video was made in January.
February had wages shut down voluntarily.
Everybody just stayed at home for a month.
The government sent out checks to everybody sitting at home.
This would be over.
You know what?
It's Trump, so let's go ahead and give him the curve.
Let's give him a long learning curve,
and let's say he said it March 1.
We'd already be done.
The curve everybody's worried about would be stomped flat.
But it's not, because we have half measures.
We have partisan bickering.
We don't have leadership.
And that show, that circus today,
shows we're not going to get it.
That man is not a leader.
He's not going to be able to unify this country.
And we need that.
We need leadership.
We need somebody to set the example,
not somebody to countermand everything the experts say.
We should probably wear masks.
Well, I'm not going to wear one.
You really should social distance.
Come here, let me shake your hand to prove that he's tough.
I'd be real tough, too, if I had the Secret Service
checking everybody I came in contact with.
It's an act, just like everything else with him.
It's an act.
If you want leadership, you're going to have to do it.
You're going to have to make the decisions,
because there are a whole bunch of people right now
who are concerned about saving the economy.
And in the process, they're going to make it worse.
And they're not concerned about you.
They're going to, say, reopen.
You have to decide whether or not that's right,
because a lot of these people making these decisions
are in the pocket of somebody.
They're not thinking about you.
Look at that council.
You see any experts on this, on that council?
No, because the decisions have already been made.
They're going to sacrifice you.
You are expendable.
You have to make the decision.
You have to decide.
And we all have to do it as a country.
It's going to be harder than you think,
because the longer it's half measures, the more it travels.
You know, if County A locks down and does the right thing,
everybody stays home.
Everybody decides to do that on their own.
And the county next door is opening the beaches.
Doesn't matter.
It's going to go on and on and on.
This is what happened in the early 1900s.
There was no unified leadership.
There was no plan that people could get behind.
I would suggest that most Americans
are concerned more about human life.
And if you're concerned about the economy,
understand the shortest path to getting the economy back
on track is everybody staying home for a month.
Everybody, it can't be piecemeal.
It's got to be everybody.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}