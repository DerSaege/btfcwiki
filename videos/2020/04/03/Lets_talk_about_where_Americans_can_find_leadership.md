---
title: Let's talk about where Americans can find leadership....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=c7sg6Y19hUE) |
| Published | 2020/04/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes the lack of leadership in the Oval Office during the current crisis.
- Points out that leadership involves telling people what they don't want to hear but motivating them through it.
- Notes the difference between leading and pandering to crowds.
- Expresses disappointment in the elected entertainer instead of a leader.
- Talks about governors taking action in the absence of federal leadership.
- Warns against authoritarian measures like state troopers at borders.
- Advises against putting first responders at risk of getting sick due to poor decisions.
- Comments on a community considering door-to-door welfare checks, expressing reluctance.
- Mentions a plan for welfare checks with color-coded signals.
- Criticizes the lack of leadership and calls for individuals to take charge and inform themselves.
- Emphasizes the importance of education and proper information in making decisions.
- Urges people to follow basic guidelines like staying home, washing hands, and not touching faces.
- Advocates for minimizing risk for essential workers by being efficient with trips outside.
- Questions the effectiveness of government in managing citizen health during this crisis.
- Encourages individuals to take responsibility since government leadership seems inadequate.

### Quotes

- "Leadership is telling a crowd of people what they don't want to hear, but doing it in a way that motivates them to get through it."
- "Aside from that, I suggest that maybe making sure that all of your first responders get sick is probably a bad idea."
- "You have to be responsible as an individual."
- "Most of the people we have in positions of power aren't leaders. They're panderers or they're entertainers."
- "You have to be a leader."

### Oneliner

Beau criticizes the lack of leadership in the Oval Office during the crisis and urges individuals to take charge and inform themselves amidst governmental inadequacies.

### Audience

Citizens, Individuals

### On-the-ground actions from transcript

- Stay informed and educated on the current situation (implied).
- Follow basic guidelines like staying home, washing hands, and not touching faces (implied).
- Minimize risk for essential workers by being efficient with trips outside (implied).
- Take responsibility as an individual and for society as a whole (implied).

### Whats missing in summary

The full transcript provides detailed insights into the importance of leadership, individual responsibility, and informed decision-making during times of crisis.

### Tags

#Leadership #Responsibility #Inform #Crisis #Government #Individuals


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about leadership and where Americans can find it right now.
You got a lot of governors looking to the Oval Office.
They're not finding the leadership.
Go figure.
Leadership isn't Trump's forte, it never was.
He's not a leader.
Yeah he gets crowds roaring by telling them what they want to hear.
That's not leading, that's pandering.
Leadership is telling a crowd of people what they don't want to hear, but doing it in a
way that motivates them to get through it.
That's leadership.
We don't have that.
We didn't elect a leader, we elected an entertainer.
And we're stuck with that decision for the next year.
Those governors who are looking to him for leadership, looking to the Oval Office and
finding an empty chair, you're supposed to be leaders too.
Some aren't waiting, haven't waited, they moved ahead.
And they're doing what they think is in the best interest of their citizens.
And a lot of them are succeeding.
And then you have some who are doing what Americans do best.
Going for that authoritarian measure.
We're going to put the state trooper at the border.
Stop those people from coming in.
It's already everywhere.
That's not going to help.
Aside from that, I would suggest that maybe making sure that all of your first responders
get sick is probably a bad idea.
Because you're going to put your state troopers there and they're going to interact with all
of these people you believe to be ill.
Then they're going to interact with the EMTs and the paramedics at the next wreck, the
next car accident.
And then those EMTs and paramedics are going to interact with doctors and nurses.
Call me crazy.
That just doesn't seem like a wise decision.
And then you have, I guess there's a community up in the Northeast that's talking about doing
door to door welfare checks.
Hard pass.
I would really prefer that not to happen.
I wouldn't be answering my door.
If I was going out of my way and doing what I should and staying home, I would appreciate
you not bringing it to me.
The thing about it is that welfare checks are a good idea.
They're in the plan.
A plan does exist.
But I think in the plan, it's through something color-coded in the window.
I think green if you don't need anything, yellow if you want them to stop because you
do need something, and red if you want them to stop but you have somebody who's ill in
the home so they can take precautions.
I don't know.
It's been a while.
And while we're on that subject, let me go ahead and end another media fascination.
Right now there's a whole bunch in the media saying, oh well, there was this scenario that
was ran in 2017 that was very similar to this.
We knew.
Course I took was in 07.
It was this scenario.
It's been around for a long time.
This isn't a surprise.
There are plans on the books.
The president did not have to become an expert in managing situations like this overnight.
All he had to do was enact a plan that already existed.
But we don't have any leadership.
So he can't do it.
It's either that or when he gutted the CDC he got so deep he removed anybody with the
institutional memory to know there even is a plan.
There was.
CDC and FEMA used to give courses on it.
In the absence of leadership, you have to lead yourself.
Which means you have to educate and inform yourself.
And I know, there's a lot of models, there's a lot of information to sift through, but
hey, we've got time right now.
We have time.
You have to educate yourself because you don't want to be one of those leaders like the governor
who is just now finding out that asymptomatic people can transmit who's making a decision
without all of the information.
You've got to educate yourself.
And yeah, the models range.
The models range greatly.
30,000, 1.1 million.
DOD's from February was 80,000 to 130,000 because they assumed in their model that it
wouldn't be taken seriously.
I guess after three years they've learned what they're dealing with.
But models aside, what you do know is how to stop it.
What you can do to mitigate it.
Stay home, wash your hands, don't touch your face.
Those essential trips, don't space them out to alleviate cabin fever.
Don't do that.
Make a list.
Do it all at once.
Cut down the risk on those essential workers.
Those people out there providing the services that we need, and we're calling them essential
because expendable is just too on the nose.
We don't know enough to be gambling with people.
We know we have a threat and we know we have to deal with it.
We know we have no leadership.
There's an empty chair in the Oval Office.
You the individual at home, you have to lead yourself.
Most governors have proven themselves worthless.
They're looking to DC for leadership and it's not coming.
I would hope that the American people learned something from this.
The general justification for this overreaching federal government that we have is that they'll
protect us from this.
But the first time they're put to their test, they fail miserably.
What are we paying for?
If the government can't help manage the health of their citizens, can't provide for the general
welfare, what good is it?
You have to be responsible as an individual.
You have to take a share of responsibility for society at large because our government,
they're not doing it anymore.
If they ever did.
Most of the people we have in positions of power aren't leaders.
They're panderers or they're entertainers.
You have to be a leader.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}