---
title: Let's talk about wearing scrubs in public....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=91qt_E3H3rM) |
| Published | 2020/04/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Medical professionals are being confronted in public for wearing scrubs, which is causing concern.
- People are harassing doctors, nurses, and techs in grocery stores and parking lots about their scrubs.
- It's irrational to harass medical professionals and get closer to them when concerned about potential illness transmission.
- Droplets may be released if medical professionals are upset and raise their voice due to harassment.
- Fabric doesn't harbor viruses well, unlike hard surfaces where viruses can live longer.
- Nurses and doctors take precautions like changing shoes, using booties, and disinfecting before leaving the hospital.
- People harassing medical professionals in public likely do not take the same precautions.
- Not everyone wearing scrubs has been in contact with infected patients.
- Medical professionals disinfect their belongings regularly, a practice that others may not follow.
- Proper hand-washing techniques, like focusing on fingertips and thumbs, are more effective than harassing healthcare workers.
- PPE shortage is a concern, and harassing healthcare workers is not a solution.
- Nurses are a close-knit community, and harassing one could lead to repercussions from others in the field.
- Harassing medical professionals adds unnecessary stress to their already challenging work environment.
- Beau suggests contacting representatives about the PPE shortage instead of harassing healthcare workers.
- Medical professionals need support, not harassment, especially during times of crisis.

### Quotes

- "Medical professionals certainly have enough on their plate. They don't need to be worried about being harassed when they stop to pick up essentials."
- "Nurses are like the mob. If you harass a nurse, every nurse in the country is gonna know your face."
- "Doing that [proper handwashing] would be a whole lot more effective at keeping you safe than harassing some nurse or doc out in public."
- "We have enough problems in society right now. We don't need a bunch of people running around causing more problems out of ignorance and fear."
- "If you don't want to say thank you, just stay away."

### Oneliner

Confronting medical professionals over scrubs in public is irrational and dangerous, support rather than harass healthcare workers during these challenging times.

### Audience

Healthcare supporters

### On-the-ground actions from transcript

- Call your representatives about the PPE shortage (suggested)
- Practice proper handwashing techniques (implied)
- Support medical professionals instead of harassing them (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of why confronting medical professionals over scrubs in public is illogical and harmful, as well as offering insights into the precautions healthcare workers take and the repercussions of harassment.

### Tags

#Healthcare #Support #PPEshortage #CommunityPolicing #PublicHealth


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're gonna talk about wearing scrubs in public
because that's a thing,
that's a major cause of concern right now.
You know, those people who have dedicated their lives
to making sure that others don't get sick,
all of a sudden there are portions of the public
who feel that they may be doing something
that would cause that to happen.
So they are confronting them in grocery stores,
in parking lots about their scrubs.
So I'm going to give you the lazy person's version
of why you shouldn't do this.
Then I'm gonna tell you some things
that medical professionals may know that you may not.
Then I'm going to list just a few of the precautions
that they take that you may not even know you should take.
And then I'm gonna give you some self-interest
as to why not to do this.
First, if you see a doc or a nurse or a tech,
whatever, in a grocery store,
and you're worried that they may have something,
what kind of sense does it make to harass them
and get closer to them and talk to them
and upset them and make them raise their voice,
at which point droplets may come out?
I mean, honestly, you should just, call me crazy,
follow their advice, practice social distancing,
and stay away.
Going up and talking to somebody
who you're concerned may have something,
that's just dumb.
That is just dumb.
Don't do that.
Medical professionals know that this is fabric.
I mean, you know this is fabric, too,
but you may not know that this stuff
doesn't live well on fabric.
Does live well on floors, though, on hard surfaces.
It's why a lot of nurses and docs have different shoes
they change into at the hospital
or they have booties that they put on over them
or they bleach or lice all their feet,
their shoes, before they leave.
I'm willing to bet those who are harassing
medical professionals in public don't do that.
As they walk around in a grocery store
and the droplets come out,
they fall to the ground, and you walk on them.
Then, because you have groceries,
you walk into your house with your shoes on
because you've got to turn around and come back
and get the rest.
May not be a wise idea.
They also know that not everybody wearing scrubs
has seen a patient.
More importantly, they probably haven't seen a patient
that's carrying what you're worried about.
Aside from that, they disinfect their keys,
their phone, their ID badge, all of that stuff.
You may not do that.
They wash their hands,
and everybody washes their hands right now
because the advice came out.
Sing the happy birthday song and wash your hands.
You go like this and sing the happy birthday song
as fast as you can.
Maybe you go like this and get both sides.
You get between your fingers, you go like this.
How the weird way docs and nurses do it,
it's because they're getting their fingertips,
which is what you touch everything with.
They go like this on their thumbs
because that's what you need to grab something.
If you're not doing that, you should probably start.
You should probably start.
Doing that would be a whole lot more effective
at keeping you safe than harassing some nurse
or doc out in public.
On top of all of this stuff, they wear PPE.
They should be, and I know there's a shortage right now.
Maybe that's the reason people are concerned.
That PPE goes over this.
It stops anything from getting to it,
but there is a shortage.
So rather than harassing docs and nurses,
perhaps you should call your rep or senator
and get onto them for enabling that spoiled tangerine
in the Oval Office.
Call me crazy.
Now, aside from all of this,
a little bit of self-interest here.
Nurses are like the mob.
If you harass a nurse in a parking lot
or a grocery store or a convenience store, whatever,
at noon, by 5 p.m., every nurse in the country
is gonna know your face.
You better hope you don't need their help
because they'll give it to you.
They will because that's the type of people they are.
I wouldn't, but they would,
but they may not be as nice about it.
We have enough problems in society right now.
We don't need a bunch of people running around
causing more problems out of ignorance and fear.
Medical professionals certainly have enough on their plate.
They have enough to worry about.
They don't need to be worried about being harassed
when they stop to pick up essentials.
If you don't want to say thank you, just stay away.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}