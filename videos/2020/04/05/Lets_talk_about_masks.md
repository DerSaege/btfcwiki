---
title: Let's talk about masks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bgmt3yeoiUU) |
| Published | 2020/04/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Advises wearing masks to protect oneself, loved ones, and society.
- Mentions President not wearing a mask despite the advice.
- Adjusted his beard to be mask compliant.
- States his mask seals at the neck for better protection.
- Emphasizes the importance of setting a good example in the absence of real leadership.
- Addresses men's reluctance to wear masks due to questioning their invincibility and toughness.
- Demonstrates how to repurpose a tactic cool item into a mask.
- Encourages using any available item as a mask if needed.
- Stresses the significance of taking precautions seriously.
- Offers to do a live stream bonfire to combat boredom and foster community spirit.

### Quotes

- "Wear the mask. If you have the opportunity, wear the mask."
- "We are in this together and we will get through it together."
- "Now nobody has to know that you're not invincible."
- "You paid nine bucks for it, get some use out of it."
- "We do not have real leadership, we have to lead ourselves."

### Oneliner

Beau advises wearing masks, repurposing items as masks, and setting a good example in the absence of real leadership to protect oneself and society during the pandemic.

### Audience

Community members

### On-the-ground actions from transcript

- Use any available item as a mask if needed (exemplified)
- Join Beau's live stream bonfire for community engagement (suggested)

### Whats missing in summary

The full transcript provides detailed instructions on repurposing items as masks and offers community engagement through a live stream bonfire.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
Yeah, so today we're gonna talk about masks.
It is advised that you wear one, okay?
It protects you, it protects your loved ones,
it protects those people around you, it protects society in general.
Almost as soon as the advice came out,
the President said that he wouldn't be wearing one,
being a good leader and all and setting the example.
There were a number of people who said they would worry about it.
When I did, I have now made my beard mask compliant.
I would also like to point out that my mask, my real kit, seals at the neck.
I didn't have to do this.
This is just me trying to set a better example.
We do not have real leadership, we have to lead ourselves.
There were a number of people who reached out, who said that their boyfriend,
their husband, or whatever, did not want to do it.
Okay, men don't want to do it because it questions our invincibility.
Can't look tough with wearing one of these.
You've probably owned one of these.
If you're one of that sort, you've got one of these, right?
Cuz you are tactic cool.
Now you have a use for it, okay?
Fold it into a triangle the way you normally do when you hang it over your
shoulders.
Then, bring it up over, bring it like this.
Here, bring it tight.
That's it.
Now nobody has to know that you're not invincible.
Now yours probably has a pirate flag or something on it in the little checks.
Doesn't matter, it'll work.
This alone would help protect those around you.
Because more than likely, if you're the sort that is more worried about your image
than everything that's going on right now, you're probably likely to contract it.
Because you're not taking the precautions you should.
So you have this, more than likely, you have this, you have something like this.
Use it, use it.
I mean, come on.
When else are you gonna have the opportunity to wear it in public?
You paid nine bucks for it, get some use out of it.
Aside from that, other people have reached out saying that they are bored.
So I'm going to, hopefully, if it doesn't rain, I will do a bonfire tonight.
Set up a live stream and let the, just let it run.
I'll talk for a little bit, but I'll just keep it going so you all can hang out in the
chat and hang out and have something to do.
We are in this together and we will get through it together, as long as everybody takes the
precautions they should.
Wear the mask.
If you have the opportunity, wear the mask.
Do your part.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}