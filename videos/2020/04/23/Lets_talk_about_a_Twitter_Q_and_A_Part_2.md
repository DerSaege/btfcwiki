---
title: Let's talk about a Twitter Q and A (Part 2).....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6wJSugxRF5A) |
| Published | 2020/04/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Conducting a Twitter Q&A to reach those who can't attend live streams.
- Addressing questions about moving away from capitalism and consumer culture.
- Believes the nation will have to move away from consumer capitalism due to its unsustainability.
- Suggests ways to transition, like growing your own food and reducing consumption.
- Warns about the potential rise of a fascistic government in the U.S.
- Prefers not to focus on his colorful backstory but on current issues.
- Advocates for focusing on those who understand the severity of COVID-19 rather than skeptics.
- Surprised by the lack of proactive response to the pandemic from government agencies.
- Criticizes the F-35 program and questions the necessity of fighter pilots in modern warfare.
- Doubts the Republican Senate's ability to stand up to Trump.
- Expresses support for the Kennedy half dollar from 1976 as his favorite U.S. coin design.
- Shares insights on the role of the CIA in focusing on human intelligence and determining intent.
- Criticizes Governor DeSantis's handling of safety measures in Florida theme parks.
- Supports Gaia theory and the idea of earth as a self-regulating entity.
- Explains the term "operator" in the context of special operations or intelligence communities.

### Quotes

- "We're definitely going to move away from the consumer capitalism that we have today."
- "I try not to get into it, to be honest, because once you start talking about that, that's all people want to talk about."
- "If they really just don't get it, they don't get it."
- "We didn't act. We have teams within DOD. We have the CDC. We have entire agencies devoted to this."
- "I think it's outdated before it's ever in the field."

### Oneliner

Beau addresses questions on moving away from consumer capitalism, warns about fascism, and criticizes governmental responses amidst other diverse topics.

### Audience

Socially conscious individuals.

### On-the-ground actions from transcript

- Stay informed and follow health guidelines to combat COVID-19 (implied).
- Advocate for sustainable practices like growing your own food and reducing consumption (implied).
- Support agencies focusing on human intelligence to determine intent (implied).
- Stay critical of government actions and hold leaders accountable (implied).

### Whats missing in summary

Insights on Beau's views on perennial favorite videos and recommendations for sharing them.

### Tags

#ConsumerCapitalism #Fascism #COVID19 #GovernmentResponse #Sustainability #Intelligence #SpecialOperations #SocialResponsibility


## Transcript
Oh, howdy there, Internet people. It's Beau again. So this is part two of the Twitter Q&A.
We're doing this because some people can't make it to a live stream.
So we've put some tweets out and we're answering questions from them.
I haven't, I've glanced at them but haven't really read through them.
So you're getting an off-the-hip kind of answer just like from Twitter.
There is a part one that you can go back and watch if you want to see that.
You and Professor Darwin have both spoken about moving away from capitalism.
Do you think our nation's consumer culture will ever allow that realistically?
We love our stuff.
Aside from consumption, what other ways would we need to change our culture to move away
from capitalism?
For those on YouTube, Professor Darwin is re-education.
The channel re-education here.
Okay, do I think it will ever happen?
Yeah, we don't have a choice.
We're definitely going to move away from the consumer capitalism that we have today.
We do not have an option.
This isn't sustainable.
This is not sustainable.
The amount of products that are being put out is not sustainable.
We're going to have to alter it.
It's not a matter of whether or not the system will allow it because if the system doesn't
allow it, it'll crash.
There are some ways to move away from it ahead of time.
Some of the big things are growing your own food.
All the stuff we talk about on this channel, reduce, reuse, repurpose, recycle type of
stuff, that's going to help.
Changing your mindset to more of repairing stuff than just getting a new one.
Getting rid of the idea of always having to use disposables.
Things that last are probably better.
The immigration thing from the other day seemed like a horrific fascist move, but no one else
seems concerned.
Do you think it could happen here?
If so, would they ever reopen or keep us perpetually walled off to increase their abuse?
To be honest, I don't know what this is in reference to.
I don't know if this is about not giving people married to immigrants the stimulus or shutting
down the borders or what.
This administration does a lot of horribly fascistic stuff to immigrants as a matter
of course.
The wider question, could it happen here?
Absolutely.
And there are a lot of conditions that are set for it to happen here.
Historically speaking, a lot of the stuff we're going through are conditions that bring
that type of government about.
Trump could be an ideal headman for that because he has that fan base.
The problem is right now, because of what's going on, he's got to balance his rhetoric
and he's incapable of it.
He's not subtle.
So I think that our current situation may have actually lessened the chances of it happening.
But make no mistake, it still can.
Who are you?
You're so quiet about anything about who you are.
You've been shot, you speak three languages, you know a suspicious amount about breaking
into things.
Yeah, I have a colorful backstory.
I try not to get into it, to be honest, because a lot of times once you start talking about
that, once you open that door, that's all people want to talk about because it's interesting
to some.
I would prefer to talk about what's going on today rather than the stuff I did 10 or
15 years ago.
I was a contractor.
I was a consultant, whatever term you want to use.
I did a lot of crazy stuff when I was younger.
These skills and those stories, they are the skills of a misspent youth starting to reign.
I also don't like arguing from authority.
And when you present yourself as one of those type of people, that's the assumption people
get.
And I want people to question what I say.
How do you deal with people who are still skeptical about COVID-19 and thinking it's
a government hoax?
Don't.
At this point, if they don't get it, they don't get it.
They're a very loud group, but they're a minority.
They are a minority.
They don't have the influence they need.
Those who think it's a hoax, those who think it's overblown, that's all fine and good.
They're going to find out it's not.
And at this point, it's like triage.
You do what you can, but at some point you've got to move on.
If they really just don't get it, they don't get it.
And by this point, they're probably not going to.
What has surprised you the most in the response to this?
The thing that surprised me about the response to the current situation with everything going
around is that we didn't act.
We have teams within DOD.
We have the CDC.
We have entire agencies devoted to this, and it seems like they were all barred from doing
their job.
Or they couldn't get the authorization they needed to move forward.
That surprised me.
I never expected it to get this far because it didn't have to.
They're really good at their jobs if they would be allowed to do them.
What do you think of the F-35?
I can think of a whole lot better ways to spend that money.
I think it's outdated before it's ever in the field.
I think we are past the days of the fighter pilot.
I think it's going to be drones.
I think it's a horrendous waste of money.
And it's suffering from the sunk cost fallacy.
We've put so much into it, we don't want to scrap it.
But the reality is, it's unneeded.
It is unneeded.
Will the Republican Senate ever reach the point that they stand up to Trump?
Probably not.
Probably not.
There are a lot of Republicans that want him to tone it down.
That want him to try to be a more apt leader at the moment.
They're not getting what they want and they're still not standing up to him.
So no, I don't think they're going to.
I don't think they're going to grow a spine until he's gone.
What is your favorite U.S. coin design?
The Kennedy half dollar from 1976.
If you were head of the CIA, what would the agency be doing right now?
Disbanding?
I'm assuming you're asking what I think, if we're going to have an agency like that, what
should it be doing?
Its job, which is human intelligence.
It should be focusing on human intelligence.
And that is determining intent.
That is something we're going to talk about in, well, I'm not going to talk about it.
Deep Goat will in an upcoming video.
Intent.
That is what intelligence work is about.
All of the intercepts and all of that stuff, it's great.
It can tell you what happened, but it doesn't help develop good assessments about what's
going to happen.
And intelligence is really about figuring out the intent.
It's about telling the future.
If you can't do that, you're useless.
Hi Bo, here in Central Florida, the theme parks for Smart and Closed fairly early, but
with tourism being vital to the hospitality industry and the majority of our tourists
being international travelers, how can we assure them when DeSantis has disregarded
safety measures?
You can't.
Not honestly.
You can't.
The thing about it here that's so annoying is that we were so close.
Here in Florida, we were so close to getting to the point where we could realistically
not have problems with a rollout reopen.
Like we're talking weeks.
And he jumped the gun.
I don't know.
I wouldn't say that it is safe.
I wouldn't because there is still a high probability of resurgence now.
I hope I'm wrong.
I hope that we were over the line because we're close.
We're really close, but it wasn't certain yet.
What are your thoughts on Gaia theory?
I think it's true.
I think the earth is a self-regulating entity in the sense of it's all interrelated, you
know, and inorganic and organic matter on the planet has to find a balance.
We have to, we can still manipulate and improve our surroundings.
We don't have to go back to living in huts, living in caves, but we have to do it in a
sustainable way.
And so I think it is a holistic approach that we would have to take when we're talking about
ecology and environmental science.
What's an operator?
You dropped that term the other day and I got that you meant people who are dressed
in tactical gear, but I'm not sure what the term actually means.
I'm going to put a not safe for work video link down in the comment section for this
one.
Operator it used to be very, very specific to one group, but now it is widely used to
reference anybody that is in the field in the special operations or even intelligence
communities.
That's an operator, but they have to be somebody in the field.
The terminology is changing, but yeah, it's slang.
It's slang for somebody who's Siltine, Delta, SAS, I don't know where you're from, but it's
a special operations guy.
Which of your videos are your perennial favorites that you would recommend sharing with folks
who haven't heard you?
I'm actually putting together a playlist on this tonight, so you just hang on and that
will exist.
Wow, already another 11 minutes in.
We're going to go ahead and stop again and take a quick break and come back.
There's a reason I do it at 10 minute marks.
If this is the last one you're watching, it's just a thought.
Y'all have a good night, but there'll be another.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}