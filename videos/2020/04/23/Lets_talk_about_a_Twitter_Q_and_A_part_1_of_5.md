---
title: Let's talk about a Twitter Q and A (part 1 of 5)
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vxfNTnoe1kw) |
| Published | 2020/04/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Conducting a Q&A session to answer questions from Twitter due to viewers' inability to attend live streams.
- Explains the use of GIFs of women to express emotions, attributing it to women being more emotive and clear in their expressions.
- Comments on the trend of anti-intellectualism in right-wing media in the United States, where opinions are valued over facts.
- Addresses the issue of charging for firearms training due to increasing demand, considering safety training free but charging for proficiency.
- Talks about managing Patreon messages and the unexpected growth of his platform with limited infrastructure.
- Shares his proficiency in multiple languages that have become less fluent over time due to lack of practice.
- Mentions influential figures in his life, politically, philosophically, and economically, including Emma Goldman, Sophie Scholl, Hunter S. Thompson, Thomas Paine, and Thomas Jefferson.
- Responds to questions about Nostradamus and the concept of putting all cops in prison and setting prisoners free for public safety.
- Expresses his thoughts on North Korea, authoritarian governments, and the potential scenarios if Kim doesn't recover.
- Humorously declines an offer to be someone's dad and explains why he wouldn't write a book about past rescue operations.

### Quotes

- "Everybody has skills to contribute, and I'm happy that I have found my place in that."
- "If you put all cops in prison and set all prisoners free, will the streets be more or less safe."
- "I enjoy the fight, I do."

### Oneliner

Beau conducts a Q&A addressing various topics from anti-intellectualism in media to charging for firearms training, sharing insights on languages, influential figures, public safety scenarios, and declining a book about rescue operations.

### Audience

Creators and Activists

### On-the-ground actions from transcript

- Reach out to Beau for firearms training (suggested)
- Support creators with limited infrastructure like Beau on platforms like Patreon (implied)

### Whats missing in summary

Insights on Beau's thoughts and perspectives on a wide range of topics, including media trends, public safety, multilingualism, influential figures, and personal boundaries.

### Tags

#Q&A #Anti-Intellectualism #FirearmsTraining #Languages #InfluentialFigures #PublicSafety #Creators #Patreon


## Transcript
Well howdy there internet people, Lidsbo again.
So tonight, we are going to do a Q&A
from questions from Twitter,
a couple of different tweets in the mailbox.
We're doing this because people said
that they couldn't always make it to the live stream
and they have questions to ask.
Now, I haven't actually, I've glanced at them,
but I haven't looked through them,
so you're gonna get off the cuff answers
as if it was a live stream.
Why do you always use gifts of women to express your emotions?
That's what we're starting with, huh?
Well, if you don't know, I'm not going to tell you.
Women are generally more emotive in their expressions, so they're more clear.
That's why I use it, I guess. I don't know.
That's probably worthy of a psychological study.
study. Bo, on the off chance you read this, do you have any thoughts on right-wing media spreading
stuff like this? Not necessarily focused on this video in particular, but rather the trend of
anti-intellectualism. That's a very strong current in the United States. There's the idea that your
opinion matters as much as the facts. They don't. They don't. Yeah, everybody has a right to have an
opinion. Everybody has a right to say their opinion. Not every opinion has a
right to be heard. A lot of people express opinions that are just irrelevant.
I mean that's a horrible thing to say but it's true. The guy feeding you stuff
on the teleprompter from Fox or any of these other networks, it is what it is. If
If that's how you get your information and you only trust a couple of outlets you're only going to get part of the story you're only going to be you're just going to be misinformed so yeah the right-wing media does it the left-wing media does it to some degree they tend to be more factual but only because their viewers are more open to new ideas and they get caught faster I don't think it's an ethical thing I think it's
They just, they've learned not to do it.
Getting more and more requests to train folks in firearms.
Never thought of it as a business,
but it's taking a lot of my time
and I'm going to have to charge.
So, far safety is free,
but I have to start charging for proficiency.
I'm good at it, there's a resume here
with personal details I'm not going to say, okay, so the rate that you're saying is what
you work for normally at your shop, that's fair.
That is fair.
When you're talking the actual range time, that's fair.
If you're doing a group, as you say that you have here, I wouldn't charge each of them
that.
I would maybe total double this amount and break it up.
If you've got five people, split that up among the five.
That way it is worth your time, because a side gig like that
can eventually kind of overpower your life.
And you don't want to become financially dependent on doing
it, because you may decide that you don't want to.
Do you check your Patreon messages? I do. I do.
Between, like on Twitter I probably get 30, 40 direct messages a day. Same for
Facebook, Patreon. I try to get them done at night. It doesn't always happen.
Normally on the weekends I run through and try to get as many of them as I can.
I was not prepared for this to reach this level, to be honest.
I didn't expect it to blow up as quickly as it did.
Do not have the infrastructure that a lot of operations on YouTube have.
It's literally me, my wife, and Carrie.
That's the entirety of it.
Okay, so how many languages?
How many and which languages do you speak?
Languages are perishable, so if you don't use them, you lose them.
I'm passable in English, Spanish, Russian.
I can get by in a few other languages, have a working knowledge in Arabic, know the basics
of Chinese, little Irish, enough to get to from the airport to the hotel in a few other
languages but if you, it's one of those things, there was a time when I was fluent in multiple
languages but I just don't use them as much so I've lost it.
Philosophically, politically and economically, who are your biggest influences?
Emma Goldman, Sophie Scholl, Hunter S. Thompson, Thomas Paine, Thomas Jefferson.
Those were probably most influential in the beginning.
And then I started branching out and getting people from other cultures.
I had a lot of time to read at one point and I, although not necessarily all of the militancy
I really liked the way Malcolm X thought, I liked the way he processed things and I think I learned a lot from that.
Although, I tend to today agree more with the Martin Luther King strategy.
You know, carrot and a stick, but a bigger carrot.
Assisted living this looks like medical questions that my wife has answered
Please tell people there's a less sketchy way to deal with a
Totally not talking about how to do
What is the Apple song
The Apple song is something like I was just an apple sitting on a tree until you came along and picked me
It's from a kid's toy, it's not as significant as it has been made out to be.
How do you keep all of this from getting you down?
Fighting a battle every day for the survival of humanity is a heavy burden when the odds
are stacked against us.
When you say it like that, yeah, it sounds heavy.
I enjoy the fight, I do.
I enjoy being in it as much as possible.
Everybody has, it's a fight that's going to have to happen.
And everybody has skills to contribute and I enjoy
and I'm happy that I have found my place in that.
What is Nostradamus?
Forgive me if you've already done a video about that.
Nostradamus was a person or people who wrote a bunch
cryptic messages that were interpreted by various people to mean different things because
they were incredibly vague and they could be applied to different situations and then
people could always think that they were right.
I'm sorry that says what is Q, same thing.
If you put all cops in prison and set all prisoners free, will the streets be more or
are less safe. If you're talking about minimum, low, most, medium, yeah you could
let them out. They're not a danger to society. They made a mistake. The
overwhelming majority. When you get to some of the maxes, and this isn't
everybody on a max either, but when you get to some of the the higher level
security places, you know people, especially people who watch this channel,
channel. We like to believe that everybody's good, and most people are, but there are bad
people out there and there are people who will continue to wrong others. Maybe they
should be separated from society in some way. I don't believe the current prison system
is the way to do it, but I don't think that letting them out necessarily is a good idea
either. I'm hoping I'm getting the real meaning of that question. It's not easy to discern
From truth, from the news coming out of North Korea, what do you think is really happening
and what do you think would happen if Kim doesn't recover?
I'm not worried about it.
Authoritarian governments of any kind, they're based on hierarchy.
There will be a successor.
In some rare instances, you have infighting over who becomes the next person, but that
normally doesn't spill out onto the streets.
It's normally like a palace coup kind of thing, and that's fine.
I would hope, I would actually hope that there are some elements within his military that
are less propagandized and would be more willing to come out and start engaging in the international
community a little bit more.
So, will you be my dad?
I already got a lot of kids.
I know that a lot of younger people see me as a father figure of sorts.
that's fine. It's incredibly flattering. I think there's probably better role
models out there though, to be honest. Have you ever considered writing a book
about your past experiences in rescue operations? Nice way to put that. I bet
it would be fascinating. And there is a nice response there. You can't
tell those stories without telling how it's done and the methods in which that
kind of stuff is done hasn't changed much and I wouldn't want to impact
somebody else that's currently trying to help people because that type of
stuff does save lives. You don't normally think of it as a rescue. I prefer the
term emergency relocation but it saves lives and I wouldn't want to I wouldn't
want to undermine that. Maybe a fictional book that doesn't actually include the
details maybe that's in the future but as far as you know a story that's true
but the names have been changed to protect the guilty probably not because
I don't foresee the tactics that get used being changed anytime soon. Okay so
So we're already at 11 minutes.
We're going to go ahead and this is definitely
going to have to be two parts.
So we're going to stop real quick.
and it'll come right back.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}