---
title: Let's talk about a question from my 5-year-old and a construction project....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2NV-Sihjwvw) |
| Published | 2020/04/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau shares a surprising question from his 5-year-old about building a wall, inspired by a visit to the Vietnam memorial in DC.
- The idea of building a wall to memorialize the lives lost during the current crisis is discussed, with inscriptions of quotes from leaders.
- The narrative criticizes political elites for their disconnectedness from the struggles of ordinary workers, especially in vital jobs like meatpacking.
- There's a call for a monument tied to leadership to serve as a reminder of the consequences of their decisions, particularly in handling the pandemic.
- The importance of basing decisions on reliable information rather than hope or personal desires is emphasized.
- Criticism is directed at how economic considerations have influenced decision-making, benefiting the wealthy at the expense of the average worker.
- Beau stresses the need for individual agency and decision-making in the absence of effective leadership during the crisis.
- The discourse touches on the profit motives behind certain actions and products during this challenging time.

### Quotes

- "If you are in public office, your words have consequences."
- "You have to lead yourself. You have to make a decision on what you're going to do."
- "Nobody can make it for you. Nobody can advise you on it."
- "This needs to be built. And it needs to be tied to the leadership."
- "You can't run a country on hope. On a guess."

### Oneliner

Beau shares his 5-year-old's unexpected question about building a wall to memorialize the current crisis, criticizing political elites and stressing the need for informed decision-making and personal agency amidst a lack of effective leadership.

### Audience

Citizens, decision-makers

### On-the-ground actions from transcript

- Build memorials tied to leadership to remind them of the consequences of their decisions (suggested)
- Base decisions on reliable information rather than hope or personal desires (implied)
- Support policies that prioritize the well-being of workers over economic considerations (implied)

### Whats missing in summary

The full transcript dives deeper into the themes of accountability, decision-making, and economic disparities during crises.

### Tags

#Leadership #DecisionMaking #PoliticalElites #Memorial #EconomicJustice


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about a question from my 5 year old.
One I got last night that I was certainly not expecting.
So we're going to talk about that question, we are going to talk about an inevitable construction
project.
Inevitable being a key word.
Last night after bedtime my 5 year old gets up, he's like I'm hungry.
If you don't have kids, allow me to explain that they're never actually hungry.
This is I'm not ready to go to bed, but I know that my parents aren't going to send
me to bed if I say I'm hungry.
So as we're sitting there eating our pop tart, I'm eating mine, he's playing with his.
He asks me, are we going to build a wall for this?
I have no idea what he means.
No clue.
He had recently seen photos of me taking my father to see the wall in DC.
For those overseas if you don't know, the wall is a monument, a memorial to those lost
during Vietnam.
My son has overheard that the tolls are now similar.
Way a kid's mind works.
Guess we got to build a wall.
Wasn't ready for that question, hadn't put any thought into it, but man that is a fantastic
idea.
He is on to something.
We should build a wall, it could be done in much the same way.
The names of all those lost inscribed on the wall, by day, wall gets taller or shorter
based on the number of lost that day.
And in front of it, on the ground, we can inscribe quotes from those leading the country,
both in elected office and those in the media who are influencing thought.
So you can stand at the point where we're all waiting on a miracle to just make it disappear
and look down the wall.
You can stand at the point where the wall has leveled off in height, but those in the
media have decided that their stock portfolio is taking too much of a hit, so they start
pushing to reopen.
You can stand there and look over and see the inevitable second wave, inevitable being
the word the medical professionals are using.
You can stand at the point where Kushner decided to make fun of a lockdown crowd while his
father-in-law is ordering meatpackers back to work.
The idea behind that, the lockdown crowd, as if these people want to stay at home and
collect unemployment.
They just want to sit around because they're lazy.
Meatpackers.
I understand that the political elites in this country are very separated from us peons,
us commoners, but let me just go ahead and tell you, there is nobody lazy working in
a meatpacking plant.
They're not afraid of work, they're afraid of your judgment.
They're afraid that you're going to put the pocketbooks of your friends over their lives,
rightfully so.
That's what they're worried about.
These jobs are essential.
How about you guys, the political elites?
Why don't you all clock in for a week?
Show us how easy it is.
That job is harder than anything these political elites have done in their lives.
If they were lazy, they would not be working in a meatpacking plant.
And that goes for a lot of other jobs that go without thanks.
But this definitely needs to be done.
We should build a monument, memorial, and the leadership should be tied to it.
Not just for punitive measures, but for educational ones.
Because we've been through this before.
We've been through this exact scenario where we're getting it, and then people push to
reopen and it triggers that inevitable second wave.
We learned nothing from it.
Maybe the reason we learned nothing from it is because those in power weren't tied to
their mistakes.
If you are in public office, your words have consequences.
And all too often, those consequences can be measured in human lives.
I think we need to do it.
I think we need to build this wall.
We put it on the southern border.
They wanted one there.
Maybe we get a treatment.
Maybe.
Maybe something happens.
Contrary to all the data.
I hope that's what happens.
But that's not the way it's looking.
You can't run a country on hope.
On a guess.
On what you would like to happen.
You have to go off the best information at hand.
I understand there are economic considerations to factor in.
I get that.
There were alternatives initially.
Before the supply chain started having issues, there were alternatives.
But those in power chose to give that money to their buddies.
The millionaires and billionaires.
They couldn't wait a couple weeks to get their cash.
If they'd just given the money to the average worker, that money would have been spent.
It would have generated economic activity.
It would have flowed up to their friends eventually.
But I guess when you've got that much money, you can't wait.
I'm sure those still waiting on their stimulus checks are sympathetic.
This needs to be built.
And it needs to be tied to the leadership.
It's not politicizing it.
It's what happened.
It's historical.
These people are making decisions that are costing lives.
And they're doing it for their own monetary benefit.
And I hope I'm wrong.
I am hope one of those people who is still going in every day, trying to figure out a
treatment.
I hope they come up with something.
And make all of us who are worried look like we're crazy.
I hope that happens.
Until then, we have no leadership.
You have to lead yourself.
You have to make a decision on what you're going to do.
Nobody can make it for you.
Nobody can advise you on it.
You have to make that decision yourself.
Take the information at hand, the facts, and go off of that.
I would also remind everybody this is being done so a bunch of people can make money.
They won't make any money if you don't buy their products.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}