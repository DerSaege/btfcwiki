---
title: Let's talk about reopening Florida's schools....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FekR5HbW1k4) |
| Published | 2020/04/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor DeSantis of Florida is considering reopening schools, despite experts advising against it.
- DeSantis previously kept beaches open when experts recommended closing them, leading to negative consequences in South Florida.
- DeSantis lacks understanding of the subject matter, evident from his actions such as cross-contaminating during a meeting.
- Kids can catch COVID-19 and spread it, impacting not just children but also teachers, janitorial staff, and older family members they come in contact with.
- Reopening schools solely to appease political figures like President Trump is illogical and risks the lives of educators.
- Beau plans to remove his kids from school if they reopen, urging others to do the same to protect teachers from unnecessary risk.

### Quotes

- "There's no reason to put these people at risk."
- "I for one will yank my kids out of school."
- "Our teachers' lives are not something the governor of Florida should be able to cash in."

### Oneliner

Governor DeSantis' push to reopen schools in Florida disregards expert advice and risks educators' lives for political gain.

### Audience

Florida residents, parents, educators

### On-the-ground actions from transcript

- Remove kids from school if reopened (exemplified)
- Advocate for teachers' safety (exemplified)
- Refuse to send children to school if deemed unsafe (exemplified)

### Whats missing in summary

The full transcript provides additional context on Governor DeSantis' decision-making process and the potential consequences of reopening schools.

### Tags

#Florida #GovernorDeSantis #Schools #COVID19 #Education


## Transcript
Well howdy there Internet people, it's Beau again.
So today we're going to talk about the governor of Florida, DeSantis.
We're going to talk about education.
We're going to talk about logic, faulty logic.
If you don't know, the governor of Florida is considering mulling over reopening the
schools in spring.
Like now, wants to get kids back in school.
There's no reason to keep them at home, he says.
Before we get too far into this, I would like to point out this is the same governor that
played his part as the mayor from Jaws and kept the beaches open when every expert was
saying to close them.
We see the situation that caused in South Florida now.
This is the same governor that recently attended a meeting with a glove on one hand and not
on the other and throughout the entire meeting folded his hands together, touching his face,
cross-contaminating.
This is not a man who actually understands this subject matter.
He is probably not the best choice to be able to override every medical expert, all of the
health experts, everybody that's saying no, this is probably a bad idea.
His logic behind this is that, well, it doesn't impact kids.
Okay, first, kids do get, they can catch it.
They can get sick.
It doesn't have a high fatality rate among them.
But if they're sick and they go to a school, they can spread it to other people.
Now I understand that when you think of a school, you think of children.
And I understand the governor probably hasn't met with any teachers in a while, but if you
go to a school, you will see teachers, you will see janitorial staff, you will see the
staff in the kitchen, you will see the administration.
Not all of them are young.
So if you send kids to a school and they become infected, they will give it to people who
are older than them.
They will also take that home to their parents.
If you pack a bunch of kids into a room, it's more likely to spread.
This is a really bad idea.
There's no other way to say it.
I can understand the desire to reopen schools.
I can think of somebody who needs to go back to one.
Our teachers' lives, the lives of those people educating our children, are not something
the governor of Florida should be able to cash in so he can get a pat on the head from
President Trump.
And that's what this is.
There is no logical reason to reopen the schools.
It's just a curry favor to show that he's ready to reopen the economy with a bang, like
the president says, and in the process risk the lives of our teachers, of those people
we entrust our children to.
They protect our kids.
We should probably try to speak up and protect them now.
I for one will yank my kids out of school.
If the governor opens them right now.
I think everybody in Florida needs to do the same thing.
Because those teachers will go back to school.
They will show up.
And they will be the one to pay the price for the governor's desire to kneel before
President Trump.
We've got to start thinking of each other.
Because it's very clear the government isn't going to.
There's no reason to put these people at risk.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}