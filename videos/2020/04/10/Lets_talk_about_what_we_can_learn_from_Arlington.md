---
title: Let's talk about what we can learn from Arlington....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_1zlQPcabjg) |
| Published | 2020/04/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the history of Arlington National Cemetery, which was established in the 1860s after the Battle of the Wilderness.
- Mentions that Arlington was chosen for the cemetery due to its aesthetics, being already cleared, high ground to avoid flooding, and because it was Robert E. Lee's home.
- Points out that Robert E. Lee's decision to fight against the Union led to his home becoming a national cemetery, serving as a political statement.
- Draws a parallel between Arlington and Hart Island in New York, where unclaimed individuals are laid to rest.
- Expresses the need to not overlook the issues related to unclaimed bodies and the proximity to major problems.
- Suggests repurposing golf courses as visible sites for victims, contrasting with the hidden nature of Hart Island.
- Criticizes the current president for lacking basic information about antibiotics and viruses.
- Praises governors for their leadership in managing the crisis and combating federal incompetence.
- Emphasizes the importance of public reminders and visible sites as a lesson for the future.
- Advocates for repurposing golf courses as reminders of the current crisis.

### Quotes

- "We need to not overlook the issues related to unclaimed bodies and the proximity to major problems."
- "Those golf courses, to me, they're perfect."
- "They do not care about you."

### Oneliner

Beau delves into the history of Arlington National Cemetery, proposes repurposing golf courses as visible sites for victims, and criticizes federal incompetence during the crisis.

### Audience

Public citizens

### On-the-ground actions from transcript

- Repurpose golf courses as visible sites for victims (suggested)
- Advocate for public reminders and visible memorials (implied)

### Whats missing in summary

The full transcript provides a deeper historical context of Arlington National Cemetery and criticizes federal leadership during the crisis.

### Tags

#ArlingtonNationalCemetery #History #FederalIncompetence #GolfCourses #PublicReminders


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about Arlington, how it came into being, and what we can learn
from it today.
Because history will often set an example for us, and it will also teach us how to set
an example for others.
All we have to do is pay attention.
I definitely think there's a lesson to be learned there.
Now for those overseas, Arlington, if you've ever seen a movie that includes the service
for a fallen soldier, there's that sight with all the white headstones.
That's Arlington.
That is Arlington.
Arlington came into being in the 1860s after the Battle of the Wilderness.
All of the other sites were full, so they needed a new one.
They needed a new location for a national cemetery.
They chose Arlington because it was aesthetically pleasing, it was already cleared, it was high
ground so flooding wouldn't be an issue, and it was Robert E. Lee's home.
For those overseas, Robert E. Lee was a general who left the Union to fight with the Confederacy,
fought against the Union.
They took his home.
That failure in judgment on his part led to that.
It was as much a political statement as it was filling a need.
I think there is something to be learned from that.
I would imagine that most people watching this channel have seen the rather disturbing
drone footage from Hart Island in New York.
Now I will say that footage isn't as disturbing as it may seem.
That's been going on there a long time for people who went unclaimed or people who didn't
have money for services.
It's just now in the public eye, and that's part of the problem.
It's been going on a long time there, and even though some of those people may be re-interned
later, most people don't know that was happening because it's out of sight, it's out of mind.
This isn't something we need to forget about.
This isn't something that should be tucked away on some island where nobody knows, nobody
remembers.
We need to remember the shortages.
We need to remember how close we are to major, major issues.
I would suggest a different site, a series of sites.
There are golf courses around this country.
They are aesthetically pleasing.
They're already set up, landscaped.
It can be maintained, and they happen to be in most of the hot spots.
Seems like they'd be an ideal place.
They're already cleared.
If we are going to have a site or series of sites for the victims of this, I would suggest
they not be tucked away, that they be very visible to serve as a reminder.
The current president always wanted his name on something.
That's why he pushed so hard for that stupid and ineffective wall.
He wanted a vanity project.
Well here we go.
His golf courses can be the sites.
The president of the United States, even after all of this time, does not have the basic
information to make decisions.
It is a failure of leadership.
It is a failure of judgment.
It is a failure of decision making.
Today he talked about how brilliant the germ was for being able to outsmart antibiotics.
After all of this time, all of the briefings we would assume he had, he still doesn't know
that antibiotics don't work on viruses.
If he doesn't know that, a basic piece of information, how can he possibly understand
any of the other stuff that's needed to guide this country through this?
Those governors who are acting as presidents right now, they deserve the credit.
Because not just are they fighting what we're all fighting, they're fighting the federal
government's incompetence.
They're fighting the disinformation pushed by a man who talks about wonder drugs and
game changers, but doesn't know that antibiotics won't work.
I wouldn't take medical advice from him.
Call me crazy.
Those sites are perfect.
There's one in South Florida.
There's one in New York.
There's one pretty much everywhere.
There's a hot spot.
And they'll set an example, and they will serve as a reminder that this can happen again.
It will happen again.
Because the Republican Party in the United States, they're already downplaying it.
They're already acting like it wasn't a big deal, like we weren't on the brink, because
they don't want to accept the blame.
And if it's already being downplayed now, while that drone footage is being filmed,
imagine what's going to happen afterward.
There will be no changes.
The stockpile will not be improved.
We will not get more equipment.
We will not be ready next time.
They're making that abundantly clear now.
They do not care about you.
We need public reminders.
We need something front and center.
This is what happened, and it could have been worse.
Those golf courses, to me, they're perfect.
And he can finally have his name on something that will go down in history.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}