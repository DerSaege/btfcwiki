---
title: Let's talk with Deep Goat about Republicans admitting Russian interference...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rife6Cempk0) |
| Published | 2020/04/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans in the Senate acknowledged Russian interference in the 2016 election but didn't prove collusion, stating it's a law enforcement matter, not intelligence community's duty.
- Intelligence community focuses on determining intent and predicting the future, rather than catching someone in the act.
- The big unanswered question is why the Russian campaign shifted from undermining the election to supporting Trump.
- There's anticipation for the counterintelligence report to reveal how the US countered the Russian campaign and what Russian intelligence knew about Trump.
- Despite initial claims of a hoax, politicians now agree that the Russian interference occurred.
- The success of Russia's campaign to influence the election, even marginally, sets a dangerous precedent for future attempts.
- Concerns arise about whether Trump is unwittingly manipulated by Russia, given his lack of international savvy.
- The intelligence community's reluctance to reveal failures may hinder full transparency on the matter.
- Regardless of the impact, Russia's successful election interference demonstrates their ability and potential for future attempts.
- The alignment of Russia's interests with those of Trump raises suspicions about potential influence on his foreign policy decisions.

### Quotes

- "We need to know why. We need to know what Russian intelligence knew about the President that the voters don't."
- "At the end of the day, whether or not that campaign by the Russians had a huge effect or not, they won."
- "Maybe he doesn't even know he's being used."

### Oneliner

Republicans confirm Russian interference but avoid proving collusion, leaving unanswered questions on motives and impacts, indicating potential future threats.

### Audience

Voters, policymakers, analysts

### On-the-ground actions from transcript

- Demand transparency and accountability from elected officials regarding foreign interference (implied)
- Stay informed and engaged in understanding foreign policy decisions and their implications (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of Russian interference in the 2016 election and raises critical questions about potential foreign influence on US policies and political figures.

### Tags

#RussianInterference #Collusion #IntelligenceCommunity #ForeignInfluence #ElectionSecurity


## Transcript
aw, howdy there
here to see deep goat again I guess
now that the
Republicans in the Senate
have agreed that they have no reason
to dispute the findings of the intelligence community
and admitted that there was Russian interference in the 2016 election
I guess that's going to be a story again.
Not going to be a big one though, because they didn't prove collusion, right?
The intelligence community didn't do that.
See that's not an intelligence community duty.
That's law enforcement.
That's the FBI's job, not the intelligence community.
The intelligence community is about determining intent, telling the future, crystal balls
and all of that.
That's what it's really about.
Most times catching somebody in the act, that's an afterthought.
You want to know what's going on.
See we still don't know that.
Still don't know the one big question.
See we know what happened, when it happened, where it happened, how it happened.
But we don't know why it happened.
We don't know why the Russian campaign switched from simply undermining the election to let's
get Trump elected.
That's an interesting development, because they wouldn't shift in the middle of it without
a reason.
We had to know their intent.
Did they just know that he was going to be completely unreliable on the international
stage and undermine the United States throughout his entire tenure through sheer incompetence?
Did they know he would fail every domestic stress test that was put against him?
Or were they able to direct him?
I mean it's odd when you really think about it.
The other thing we need to note is that all those great politicians who were saying it's
a hoax, well now they agree it's true.
It happened.
I'm glad the armchair experts agree.
Those people with absolutely no training in tradecraft at all, well they get it.
But we still don't have the counterintelligence report.
That still hasn't come out yet.
That's going to be pretty revealing I imagine, because if it is released in any substantial
way we'll get to see some of the measures the US took to counteract the Russian campaign.
We've got to know the intent.
Was it really just to put a buffoon in the Oval Office?
Or is he an agent of influence?
Maybe one who doesn't even know that's what he is.
It's not like anybody's going to accuse the current Commander in Chief of being savvy
on the international stage.
Maybe he doesn't even know he's being used.
It wouldn't be surprising.
Not much is anymore.
The American people need to understand that even the President's own party could not dispute
that a foreign government waged a campaign to get him elected.
We need to know why.
We need to know what Russian intelligence knew about the President that the voters don't.
Now that information probably won't be forthcoming any time soon because the intelligence community
likes to keep its failures secret if they can.
And this is a failure.
This is a failure.
At the end of the day, whether or not that campaign by the Russians had a huge effect
or not, they won.
They set out to put somebody in the Oval Office and they did it.
Now maybe it only swung them a point or two.
Maybe it wasn't that effective.
But what it showed them was that it was possible.
They will try again.
And I would imagine they would certainly try to help the person that has strengthened their
hand all over the world.
From Syria to Iraq to Afghanistan to Ukraine to everywhere that man has attempted to engage
in foreign policy.
It all benefited Russia.
Maybe that's just a coincidence.
Anyway it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}