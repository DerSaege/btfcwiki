---
title: Let's talk about Small YouTuber Support....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fDDeHirzU3k) |
| Published | 2020/04/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the issue of small YouTubers needing support and recognition.
- Notes the surge of new channels emerging during the current times when people are at home.
- Encourages small YouTubers to describe their channel content rather than just seeking subscribers.
- Advises that having genuinely interested subscribers is more beneficial than a large number of disinterested ones.
- Stresses the importance of having an active, engaged audience for YouTube's algorithm.
- Offers to support and showcase new channels to his active community.
- Mentions a previous video with advice for new YouTubers, particularly those focused on news content.
- Teases upcoming content about preparedness and the importance of being ready.

### Quotes

- "You want people interested in your content more than subscribers."
- "It's better to have 100 subscribers who are truly interested in your content than 1,000 who aren't going to go watch it."
- "Describe your channel. Short little sentence, tell us what it is."
- "Hopefully you can pick up subscribers who are actually interested in the type of content you put out."
- "Y'all have a good night."

### Oneliner

Beau addresses the need for genuine engagement over mere numbers in supporting small YouTubers, urging creators to describe their content to attract interested subscribers.

### Audience

Small Content Creators

### On-the-ground actions from transcript

- Describe your channel content clearly in the comment section when reaching out to support other small YouTubers (suggested).
- Engage actively with new channels and creators by watching their content and providing feedback (implied).

### Whats missing in summary

Beau's supportive and inclusive community-building efforts shine through in the transcript. Viewing the full video will provide a deeper insight into his commitment to uplifting emerging voices on YouTube.

### Tags

#YouTubers #Support #Engagement #CommunityBuilding #ContentCreators


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about small YouTuber support.
Now if you're familiar with what's going on, don't do what you've been normally doing on
other channels.
We're going to amend it just a little bit.
So for those that don't know, there's a whole bunch of people at home.
Whole bunch of people who have never had time to get their voice out, they find that they
have time now.
That means there's a whole bunch of brand new channels that don't have a lot of people
to listen to them yet.
Don't have a lot of people to watch.
You know, a bunch of voices and nobody to hear them.
So what these smaller channels have decided is they're going to make videos like this
and then people will come along in the comment section and say, hey, I'm a small YouTuber
as well.
And then they subscribe to each other's pages.
Makes sense in theory.
We're going to change that just a little bit on here.
I found out about this because YouTube's magical algorithm was like, hey, you should watch
this video by Lizzie the Gangster Hippie.
And I was like, okay, why not?
Because YouTube's algorithm is normally pretty good.
So I found out about it.
Okay, so rather than putting just, hey, I'm a small YouTuber, write a short description
of what your channel's about.
You may not actually just want a whole bunch of subscribers if they're not interested in
it because what happens is when you upload a video, YouTube sends out a signal, you know,
the notifications.
If people don't respond to it, it's actually going to hurt you in the long run.
It's better to have 100 subscribers who are truly interested in your content than 1,000
who aren't going to go watch it.
Because if they're not watching it, that tells YouTube that nobody's going to watch your
stuff.
So you want people interested in your content more than subscribers.
You want people that are truly wanting to see what you're saying more than just a big
number.
So if you're participating in this, describe your channel.
Short little sentence, tell us what it is.
My comment section is normally pretty active and we're always looking for new channels
to watch.
So hopefully you can pick up subscribers who are actually interested in the type of content
you put out.
I'm also going to pin a video that I made a few months ago with advice for new YouTubers.
Give it a watch or don't.
Don't feel obligated.
It's really more for people who do news content than it is just general content.
So it may not even apply to you.
But anyway, for those on my channel who are normally watching this, I will get into the
why you need to get prepared thing.
I'll try to get that out maybe tomorrow morning.
I just saw this and it's going on tonight.
I thought it was pretty cool.
I thought you all might enjoy seeing a bunch of new channels, hearing some new voices.
Anyway it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}