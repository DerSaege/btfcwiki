---
title: Let's talk about Land O'Lakes, symbols, and the symbol-minded....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BupotrJw5xw) |
| Published | 2020/04/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Land O'Lakes removed a drawing of a native woman from their packaging, causing distress among Americans.
- Symbols are meant to evoke emotion and loyalty without requiring critical thought.
- Symbols, like corporate logos or flags, are designed to elicit immediate reactions.
- Beau criticizes the use of symbols to manipulate individuals who do not want to think critically.
- Political memes often aim to provoke emotional responses rather than inspire critical thinking.
- Americans expressed outrage over the removal of a drawing of a native woman from butter packaging, while real issues, like missing native women, are overlooked.
- Beau points out the danger of valuing symbols more than what they are meant to represent.
- Many individuals prioritize symbols, like national flags, over the true values they stand for.
- Beau argues that critical thinking is necessary for self-governance and making informed decisions.
- Symbols like political party logos often overshadow individuals' true beliefs and critical thinking.
- The focus on symbols in American society makes people easily manipulated and exploited by those in power.

### Quotes

- "Symbols are meant to evoke emotion and create loyalty without requiring critical thought."
- "We should probably think beyond symbols."
- "They care about using symbols to manipulate you. They don't care about you."
- "We are headed into tough times. We are going to have to critically think."
- "Butter is a divisive topic in this country, not because butter is that important, but because of the symbol."

### Oneliner

Beau criticizes the manipulation through symbols, urging people to think critically beyond loyalty to symbols in a society easily led by manipulative powers.

### Audience

Citizens, Critical Thinkers

### On-the-ground actions from transcript

- Question the symbols around you and their true significance (suggested)
- Foster a culture of critical thinking and questioning in your community (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the impact of symbols on society and the importance of critical thinking in the face of manipulation by those in power.

### Tags

#Symbols #CriticalThinking #Manipulation #AmericanSociety #PowerInfluence


## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to talk about butter.
We're gonna talk about Land O'Lakes,
symbols and the symbol-minded.
We're not gonna discuss whether or not
what the company did was the right move.
It's the 21st century, of course it was the right move.
That's not a discussion that any thinking person is having.
Of course it's the right move, that goes without saying.
That's not an interesting topic.
The reaction of people to that move, however,
that's interesting.
If you're not aware, Land O'Lakes,
an American butter company, removed a drawing
of a native woman from their packaging.
Predictably, a lot of Americans
are just simply distraught by this development.
They are upset.
Why?
People are asking why they did it.
I wanna know why people are upset.
It's a symbol.
It's a symbol.
That's what symbols are supposed to do.
Evoke emotion, create loyalty.
They're there to evoke an immediate reaction,
absent of critical thought.
That's their job.
And it doesn't matter if you're talking
about a corporate logo, a traffic sign, or a flag.
It's there to evoke an emotion
so you abandon critical thought.
So you don't have to think, you just have to react.
That's why it's there.
Symbols have been used to manipulate
those who don't want to think.
Since before writing.
And here we are in the 21st century.
People still haven't caught on.
I've often said I don't like memes.
I don't like them, generally, most of them,
for the same reason.
They boil things down to a false dichotomy.
Or they straw man or cherry pick.
They often just abandon all critical thought.
They don't inspire it.
Now, this isn't all of them, but most of them.
Most political memes are there to evoke a reaction
and nothing more, an emotional one.
And I don't know that that's how you get through to people.
People had an emotional reaction to Mia going missing.
Mia, I'm pretty sure that was her name.
And to prove that critical thought is
abandoned in this process, I would point out
that Americans took to social media
to express their disgust with a drawing of a native woman going
missing on a pack of butter.
Meanwhile, in this country, we have a widespread issue
with actual native women going missing.
And nobody seems to care.
Because they don't have a reaction,
an emotional reaction to people, just symbols.
They love that symbol more than whatever
it is it's supposed to represent.
And this is true of corporate logos and flags.
What happens when you have a national flag
and you have a national symbol?
You have a national symbol, but you don't have a national flag.
What happens when people begin to love the symbol more
than the things it is supposed to represent
is the symbol becomes worthless.
Many people who call themselves patriots today
have rendered the American flag worthless
because it doesn't represent.
You want to wave it and talk about your right,
your First Amendment right.
That's plural, by the way.
I would point out that long before the First Amendment
was the phrase, promote the general welfare,
the United States is a country that
is a country of the experiment.
And it required advanced citizenship.
It required an educated populace.
And when I say that, I don't mean a college degree or even
a high school diploma.
I mean the ability to critically think, to think for yourself.
That's what self-governance is about.
You have to be able to lead yourself.
You have to be able to make decisions for yourself.
Ideally, in this experiment, you would
elect someone to represent the decisions you would make.
But we don't do that anymore.
Why?
Symbols, elephants and donkeys.
We care more about the symbol than we do our own beliefs.
Butter is a divisive topic in this country,
not because butter is that important,
but because of the symbol.
Because we don't think beyond symbols much in this country
anymore.
That is why we are so easily led.
We are so easily led by the symbols.
And we are so easily led by the symbols.
We are so easily led.
We are so easily manipulated.
And we are so easily exploited.
Because any time your chosen party, elephant or donkey,
wants your support, all they have to do
is wave another symbol, one that you have
pledged your undying loyalty to, and one
Americans have no idea what it is supposed to represent.
We should probably think beyond symbols.
We are headed into tough times.
We are going to have to critically think.
Those in power, they care about staying in power.
They care about using symbols to manipulate you.
They don't care about you.
They're not leaders.
They're rulers.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}