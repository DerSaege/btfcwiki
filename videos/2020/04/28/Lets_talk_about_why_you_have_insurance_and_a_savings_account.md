---
title: Let's talk about why you have insurance and a savings account....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6r3cg3QZ_go) |
| Published | 2020/04/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reframes emergency preparedness as insurance and a savings account.
- Urges to prepare for extreme scenarios as it also prepares for more likely ones.
- Compares emergency preparedness to having homeowner's insurance.
- Suggests that being prepared can help in unforeseen circumstances like job loss.
- Points out the common occurrence of disasters in the United States.
- Emphasizes the peace of mind that preparedness brings.
- Encourages preparedness to stay ahead of panic buying and panicked people during emergencies.
- Uses a mountain lion analogy to illustrate staying one step ahead.
- Mentions the importance of having 30 days' worth of food for emergencies.
- Compares preparing for emergencies to putting on your mask first on an airplane before helping others.
- Criticizes the government's track record in dealing with emergencies.
- Advocates for being self-reliant in times of emergencies.

### Quotes

- "It's a savings account of tangible assets that you use in your daily life."
- "You don't have to deal with the effects of whatever the emergency is in most cases. You're having to deal with a bunch of panicked people."
- "Being prepared can only help."
- "Their track record for dealing with emergencies is not good."
- "You have to lead yourself."

### Oneliner

Beau reframes emergency preparedness as insurance and a savings account, urging preparation for extreme scenarios while criticizing the government's emergency response.

### Audience

Preppers and community-minded individuals.

### On-the-ground actions from transcript

- Stock up on 30 days' worth of food and essentials to prepare for emergencies (exemplified).
- Create a bug-out bag with necessary supplies for potential evacuations (exemplified).
- Take the initiative to lead yourself and be prepared for emergencies (implied).

### Whats missing in summary

Demonstrations on how to create a bug-out bag and specific items to include for different emergency scenarios.

### Tags

#EmergencyPreparedness #Insurance #SavingsAccount #Preparedness #SelfReliance


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna reframe something.
We're gonna try to, we're gonna think about it
in a different way, because I did that two part thing
on emergency preparedness,
and there were two very common comments
in the comment section.
One was where are we going?
And we'll get to that, that one's easy to explain.
The other one is why, why should I do this?
Part of that is the idea of it's never gonna happen here.
Which is funny, because one of the people,
and I warned him I was gonna make fun of this,
said that, you know, you seem so rational
about every other topic, except for this one.
It can happen here.
Thinking that it can't, or that you may never be faced
with an emergency, that might be a little irrational.
However, I can't say that I really blame the association
of emergency preparedness and being irrational,
because a large community,
within those who like to be prepared,
are less than rational about some things.
So it's a fair association.
However, that doesn't mean
that the behavior itself is irrational.
Taking it to an unjustified extreme is.
You don't have to start reading outlets that talk about,
you know, the UN and black helicopters,
if just because, you know, you put back some extra food.
So we're gonna reframe the way we think about this.
Rather than thinking of it as emergency preparedness
or prepping, it is insurance and a savings account.
You have homeowner's insurance, right?
If you own a home, you do, I would imagine.
Why?
Most of the things covered, for the loss of your home anyway,
those are extreme scenarios.
They're not really that likely to happen.
However, it's a big loss if it does.
It's the same principle.
And that is why you also see people prepare
for the most extreme scenarios.
Two reasons.
One, if you're prepared for the most extreme,
you're prepared for the little ones.
The second is, it's a lot like your homeowner's insurance
company is like, okay, well, we can give you
a $10 million policy or a $10,000 policy.
And the $10 million policy is a dollar more a month.
You're gonna pick the $10 million one.
It doesn't cost much more to be prepared
for like the most extreme scenarios
than it does to be prepared for those that are more likely.
And since preparing for the most extreme
also prepares you for the most likely,
it's better to go that route.
So that's why people prepare for some
of the wilder possibilities.
Now, you have a savings account, right?
And if you don't, you'd probably like one.
This is a savings account of tangible assets
that you use in your daily life, your disposables,
putting back food or whatever.
This is stuff that you would use.
And that's what it is.
It's a savings account.
You have a savings account for unforeseen emergencies,
things that pop up that you didn't plan for.
People look at this and they associate it with disaster.
And yeah, that's part of it.
However, what if you lose your job?
Be really cool to have like 30 or 90 days worth of food,
right?
Get rid of that bill.
It's the same principle.
You're preparing for emergencies.
Doesn't just have to be major ones.
Now, as far as the idea that it wouldn't happen to you
or you wouldn't need it, hurricanes, blizzards,
earthquakes, forest fires, all of these things, floods,
they occur all the time in the United States.
And when they occur, you might have
trouble getting what you need.
If you already have it, that's one less worry.
It's just removing.
You're just taking worries off of the table.
That's really what you're doing.
If you go through and you do long-term storage stuff,
you get it put back.
You don't have to worry about it again.
And then if something happens, you
don't have to worry about it.
You're done.
You know that you have this.
It's peace of mind.
When people start panic buying because of a news story
or whatever, it doesn't affect you.
You don't have to rush out because you already have it.
You're already set up.
And that's what a lot of it is about.
You're not actually having to deal
with the effects of whatever the emergency is in most cases.
You're having to deal with a bunch of panicked people.
And that's what you want to avoid.
You want to stay one step ahead of them.
So there's two guys.
They're out camping.
One morning, they walk out.
And there's a mountain lion just chilling right outside the tent.
And this one guy, he starts putting on his sneakers.
The guy next to him's like, you are never
going to outrun a mountain lion.
I don't have to outrun a mountain lion.
That's what you're trying to do.
Stay one step ahead, and then you
don't have to worry about any of the human problems that
are associated with disasters.
Because you don't have to go out.
If the average person in the United States,
the average person has a week to two weeks of food
in their home.
If you have 30 days, you'll probably
be able to avoid dealing with the worst of whatever happens.
Order may be restored.
Normalcy may come back in that time period.
The other thing is that after Hurricane Michael,
I did a bunch of relief stuff down in Panama City.
I was able to do that because we were already taken care of here.
Being prepared for an emergency is
a lot like being on an airplane and putting your mask on first.
Then you can help the person sitting next to you.
There's a lot of philosophical stuff that goes along with this.
I'll put some links down below to videos about it.
But it's not just for right wing loons.
It's for everybody.
Being prepared can only help.
Now, where are we going?
That's just terminology.
People call it a bug out bag.
But imagine Hurricane Sandy up north,
a whole bunch of people stuck in their homes, no power.
That bag has everything they need to cook,
everything they need to see.
It gives them entertainment.
Whatever you put in your bag is what
you're going to have available.
It's not really for bugging out.
But it is better to have it in something mobile in case
you do have to evacuate.
Because whatever the emergency is,
whatever the unforeseen circumstance is,
may require you to evacuate to somewhere else.
It's better to have all your stuff ready to go.
So just because you're calling it a bug out bag
doesn't necessarily mean you're disappearing off
into the woods.
But this is something that I can't stress enough.
It's one of those things that if you need it,
you're going to be really happy that you have it.
And our government here in the United States,
their track record for dealing with emergencies is not good.
And we've learned nothing from this, nothing.
We can tell from the response.
There's still no unified response,
even after all this time.
It's all ad hoc.
And the longer it is less than organized,
the longer this is going to go on.
But it's one of those things that
goes back to one of the whole premises of this channel.
You have to lead yourself.
There are some times when an emergency happens,
the National Guard, FEMA, whoever,
they're not going to be there for a while.
And until then, it's just us.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}