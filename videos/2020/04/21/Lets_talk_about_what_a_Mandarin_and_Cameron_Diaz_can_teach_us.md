---
title: Let's talk about what a Mandarin and Cameron Diaz can teach us....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-FIw-PPsg7s) |
| Published | 2020/04/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Analyzes the Mandarin parable and its derivative, "Button Button," which is the basis for the movie "The Box."
- Explores the universal truth behind these stories about moral dilemmas.
- Mentions the current situation we are in and the need for moral reflection.
- Talks about being judged and tested in the face of cooperation and selflessness.
- Urges people to stop allowing those in power to divide and manipulate us.
- Emphasizes the importance of cooperation, especially in times of crisis.
- Compares the current situation to a dress rehearsal for larger challenges like climate change.
- Encourages individuals to take responsibility and cooperate for the greater good.
- Raises questions about personal accountability and ethical decision-making.
- Suggests that staying home and doing our part is vital in times of crisis.

### Quotes

- "We may not be being judged in the same manner, but we're being tested."
- "We've got to stop letting those in power divide us."
- "This is a dress rehearsal for some of the stuff we have coming down the pike if we don't change."
- "You should probably do your part. You should probably cooperate."
- "There's going to be a whole lot of people that aren't around anymore to tell you you're wrong."

### Oneliner

Beau explains the moral lessons from the Mandarin and "Button Button," urging cooperation and selflessness in the current crisis as a dress rehearsal for greater challenges like climate change.

### Audience

Individuals, Community Members

### On-the-ground actions from transcript

- Cooperate with others in your community by following health guidelines and staying at home (implied).
- Take personal responsibility by making ethical decisions that prioritize the well-being of others (implied).
- Refrain from actions that put vulnerable individuals at risk, even if it seems distant or less impactful to you (implied).

### Whats missing in summary

The full transcript provides deeper insights into the parallels between moral dilemmas presented in fictional stories and the ethical challenges we face in reality, urging individuals to prioritize cooperation and selflessness for a better future.

### Tags

#Cooperation #MoralDilemma #Responsibility #Community #Ethics


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about the Mandarin, Cameron
Diaz, and what they have to teach us about the current
situation we're in, talk a little bit about moral
distance as well.
I'm going to discuss some fictional works.
I'll lay out a little bit of the plot, enough to get the
point across, but I'm not going to ruin these things.
So if you want to read or watch them after this, you'll
be fine. The first thing we're going to talk about is the Mandarin. It's a story
about a poor Portuguese guy and the devil comes to him. He's like, hey, you ring
that bell? This guy you've never met, half a world away, this Mandarin guy
living in China. Oh, he's done for and he's rich and you're gonna inherit all
all of his wealth.
Now, of course, the guy rings the bell.
The story wouldn't go on if he didn't.
And predictably, trouble follows.
Now, if you're sitting there right now going, man,
that sounds a whole lot like a Cameron Diaz movie
called The Box.
It's because The Box is based on Button Button, which
is a Mandarin parable, which comes from the Mandarin, which
is actually alleged to be ripped off from something else.
All this stuff is very derivative, but the plot is there.
The reason it is universal and has gone on for hundreds of years is because it's true.
It's true.
If you're not familiar with the box, short overview of that, box shows up a little bit
later, guy shows up to explain it.
There's a button in the box.
You push the button, you get a million dollars and somebody you've never met, you don't
No, they're done for.
The story wouldn't go on if the button didn't get pushed.
So he comes back, picks up the box.
They're like, what happens next?
Well, I've got to give the box to somebody else, somebody
you've never met.
That doesn't actually ruin the story, if you haven't seen it.
I understand that movie was not critically acclaimed.
I actually think they did a really good job of adapting it,
because it is nothing like button button,
but carries a pretty similar message.
Anyway, the point to all of this
is we're all going through this right now.
In both of those stories, they were being judged.
We may not be being judged in the same manner,
but we're being tested.
Tested to see if we can cooperate,
tested to see if we can think of those other than ourselves.
Or if we are willing to push that button and reopen,
and most of the people advocating for it
are doing so for a whole lot less than a million dollars.
And that's the thing, even though it's distant,
even though it's somebody you've never met, you may not know,
You're still putting them down for money.
Would you do it in any other situation or is it because you can use the authority of
government to justify it?
Well they reopened so it was okay.
You know you're putting people at risk and yeah you may be one of the young healthy ones,
those that don't have much of a risk, that's fine, I get that.
But would you do the same to an elderly person or a compromised person, a vulnerable person
if you had to use a knife?
Would you charge the same?
Probably not.
We've got to stop letting those in power divide us.
We have to start cooperating.
We have to stop letting them convince us to kick down those who are more vulnerable than
us because I understand there's a whole lot of people think this is the end of the world
and everything.
This is nothing.
This is a dress rehearsal for some of the stuff we have coming down the pike if we don't
change, if we don't start to cooperate.
This is nothing.
I know that it doesn't seem like much because there's that diffusion of responsibility
for it.
You've got a guy in a lab coat telling you it's okay, right?
Milgram reference, anyway.
Only it's not a lab coat, it's a suit and tie, somebody at a podium telling you that
it's alright.
Would you do it with your own hand?
If the answer is no, you should probably stay home.
You should probably do your part.
You should probably cooperate.
Because this, this is nothing in comparison to, say, climate change.
And I understand you don't believe in that either.
But just like this was nothing, there's going to be a whole lot of people that aren't
around anymore to tell you you're wrong.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}