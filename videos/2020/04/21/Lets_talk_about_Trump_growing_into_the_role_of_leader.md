---
title: Let's talk about Trump growing into the role of leader....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rJNYl2nByKs) |
| Published | 2020/04/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is being seen as growing into the role of a world leader by closing borders to immigration, branding it as protecting American jobs.
- The decision to close borders is seen as a move to protect against the severity of the situation and encourage people to stay home.
- Trump's base understands there are no jobs to protect and that his branding is a way for him to demonstrate leadership.
- The real motivation behind closing borders is to show leadership and protect both Americans and foreign workers.
- Trump had to frame his decision in a certain way due to his international image as a "complete buffoon."
- Beau suggests that Trump is trying to empower governors to become leaders in their own right by taking actions such as reopening states cautiously.
- Trump's actions are viewed as an attempt to show leadership and create a sense of control in a chaotic situation.
- Despite criticisms of Trump's handling of the situation, Beau believes in giving him a second chance to prove himself as a leader.
- Beau compares Trump's actions to those of a true leader who empowers others and sacrifices for the greater good.
- He acknowledges the skepticism towards Trump's motives but ultimately believes in rallying behind the overall plan.

### Quotes

- "He's becoming the leader we all knew he was."
- "It was making sure that those governors could be seen as true leaders."
- "Trump is a real leader, and he's finally showing it."

### Oneliner

Trump is seen as growing into a world leader by closing borders, framing it as protecting American jobs, and empowering governors to take on leadership roles in response to the crisis.

### Audience

Political observers

### On-the-ground actions from transcript

- Rally behind leaders who demonstrate true leadership (implied)
- Empower governors to take on leadership roles (implied)

### Whats missing in summary

Analysis of the potential impact of Trump's actions on the political landscape and public perception.

### Tags

#Leadership #COVID-19 #Trump #Immigration #Governors


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the president
growing into the role as world leader,
something we all knew was possible.
We all knew that deep down Trump was a leader.
We knew it.
We just, it took time for him to get to the point
where he could demonstrate.
And we're now there.
And we got to give him the opportunity
to grow into that role.
Telling you, when I heard the news last night,
it was as if I woke up in a world of my own.
So if you don't know, the president of the United States,
the leader of the free world, is going
to be closing the borders to immigration.
And this is him finally understanding
the severity of the situation.
It's fantastic.
And I know he's branding it in his own way.
But if you think about it, it's the only way he could do it.
It's the only way that he could grow into that role.
It's his 4D chess.
He's branding it as protecting American jobs.
But even the person in his base that
has that red hat on way too tight,
they know there are no jobs to protect.
They're aware of that.
So why'd he do it?
We're number one in this.
It's not like they're going to come here and bring it.
It's already here.
So what's the real motivation?
What's he really trying to do?
It's simple.
He's becoming the leader we all knew he was.
He's telling everybody, you're safer at home,
shelter in place, don't come here.
Don't come here.
Because it's bad.
We don't have a handle on it yet.
We need more testing.
We need a little bit longer of people using the precautions
that are necessary.
And I don't know what happened.
I don't know if Fosse finally sat him down and was like,
look, this is where we're at.
But whatever happened, he's there.
And he had to frame it in this weird way
because he knows that on the international stage,
he's seen as a complete buffoon.
He can't take leadership in the standard way of just going out
and saying what he means.
He's got to hide it a little bit.
Because if he came out and said that, nobody would believe him.
So he had to come out and say, it's about American jobs,
and stick to that xenophobic rhetoric that the liberal left,
the radical left, has pinned on him.
That's not him.
He's an honest straight shooter.
And his base, they understand this.
They know that he's not really concerned about jobs
that don't exist.
They know the reason they don't have a job right now
is because of his complete, total, and utter lack of leadership
in the beginning.
But that shouldn't dissuade them from allowing him to grow.
Everybody needs a second chance.
And they understand the reason they don't have any money right now
is because him and his party were more interested in getting money
for big companies that they're invested in, like his hotels,
than they were in putting money into the hands of the average worker.
They know that.
It's not like his base is completely out of touch with the reality
and brainwashed.
He's trying to protect those foreign workers.
And it's our job to make sure that he can.
Now, the next thing he's going to have to do
is do what a real leader does.
Create other leaders.
That's what a leader does.
They don't rule by edict.
They create other leaders.
So he's got to reach out to all those governors
whose political career he put on the line
with this whole liberate and reopen thing
and tell them that they have to fight him.
See he's got to empower them.
It was all a plan.
It was 4-D chess.
It was making sure that those governors could be seen as true leaders.
And Trump, having that political capital that he does,
was willing to take the hit.
That's the sign of a true leader right there.
Like that captain of that aircraft carrier.
You know, the one they fired.
That's what a leader does.
Expends their political capital for others.
So what you're going to see is you're going to see these governors
start to realize and start to publicly say,
hey look guys, even if we do reopen,
you're still really going to have to only go out for essentials.
You're going to have to limit your travel.
You're going to have to maintain social distancing.
You're still going to have to wash your hands.
That's what's going to have to happen.
And that's what's going to happen.
Because Trump is a real leader.
And he's finally showing it.
We have to make sure that we rally behind the man
whose overall plan makes complete sense,
if you think about it in the Alice in Wonderland sense.
It's either that, or he believes that his base is so out of touch with reality,
that as long as he gives them somebody to scapegoat,
and look down on and kick down at,
somebody that has absolutely nothing to do with their problems,
that they'll still vote for him again.
I mean that's really cynical.
I don't think that that's the case.
Politicians, especially Donald Trump, they're known for being truthful people.
I think that any statement to the contrary, well that's just un-American.
It's unpatriotic.
Those foreign workers, they are safer at home.
And the president said so.
Because it's out of control here.
And now our governors have to act.
Because he's creating more leaders, because he is a good leader.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}