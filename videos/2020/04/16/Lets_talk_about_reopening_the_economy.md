---
title: Let's talk about reopening the economy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Hhf9ktqzxgY) |
| Published | 2020/04/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Dates for reopening the economy like May 1st or June 15th are irrelevant as the power lies with individuals, not politicians.
- The economy will only recover when people have confidence, not when the government reopens prematurely.
- Reopening too soon without consumer confidence will lead to a severe economic depression.
- Premature reopening can devastate small businesses with no consumer spending.
- Politicians pushing for immediate reopening are prioritizing their own interests over public safety.
- Hotspots for the virus are not just big cities but also smaller areas like Albany, Georgia, Idaho, and Mississippi.
- The pandemic will only end with a reliable treatment or vaccine, not based on political wishes.
- Trusting experts and following guidelines is more effective than protesting and spreading misinformation.
- Rushing to reopen the economy without proper precautions will lead to more spikes and loss of public confidence.
- Government relief for workers is vital until the situation is under control to prevent economic collapse.

### Quotes

- "The economy will get back on track when you have confidence. Not before."
- "If you want to destroy this economy, support these politicians."
- "The government should spend its time trying to do its job, rather than trying to spin its way out of this."
- "If you're done with the US, if you're just ready to bankrupt everybody, let's reopen now."
- "Wash your hands. If you have to go out, wear a mask. Don't touch your face."

### Oneliner

Reopening the economy prematurely without consumer confidence will lead to economic collapse, prioritize public safety over politics. 

### Audience

Individuals, Public

### On-the-ground actions from transcript

- Stay at home, follow guidelines for safety (implied)
- Wear a mask if you have to go out (implied)
- Wash hands regularly to prevent the spread of the virus (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the economic repercussions of premature reopening and the importance of public confidence in restoring the economy.

### Tags

#Economy #Reopening #Government #PublicSafety #TrustExperts


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about reopening the economy
and the dates that are being thrown out.
May 1st, June 15th, whatever, it doesn't matter.
And I mean that literally.
Those dates do not matter.
The pundits, the politicians, the councils, the governors,
the president, they can reopen in theory
whenever they want, but it means nothing.
It means absolutely nothing because they don't get
to determine when the economy reopens, you do.
And this isn't one of my normal videos where I'm like,
you know, you don't understand the power we'd have
if we would only work together.
No, it's not one of those, you as an individual.
The politicians can say whatever they want.
They can say the troops will be home by Christmas
until they're blue in their face.
It doesn't matter.
The economy will get back on track when you have confidence.
Not before.
If they reopen too soon, they will destroy the economy.
It's not destroyed right now, it's paused.
It is paused.
But if they reopen too soon, they will destroy it completely.
It will go from a very, very bad recession
into a really, really bad depression very quickly
because the consumer won't have confidence.
They won't feel safe, so they won't go out.
They won't spend money.
Right now, you have a whole bunch of people sitting at home
because their workplace is closed.
Those companies, yeah, they're in a bad situation.
They have some expenses.
They have rent.
They have the normal stuff, and they don't have any revenue,
and that's bad.
That's business 101, right?
So what happens when they bring people back to work
because the government says, we're reopening,
and there's no more relief coming?
They bring people back in, and then they have payroll,
and all of the normal stuff that goes day-to-day
with keeping whatever that business is open,
but the consumer still stays at home,
still doesn't feel safe.
What happens then?
They're in a worse situation.
You can kiss every small business in this country goodbye.
It's over.
These politicians, these pundits,
they are out there trying to line their own pockets
and nothing more.
Anybody who's telling you that we need to get back to work now,
they're selling something, or they're grifting.
That's what the social hotspots were going to be, big cities.
And the media is doing a pretty good job of making it seem like
that's where it's at, because they're focusing on the number
of people who didn't make it, those who didn't recover.
However, if you switch that to the rate per 10,000
of the people who've got it, those hotspots switch real quickly.
They become places like Albany, Georgia, Idaho, Mississippi,
places like that.
This is everywhere.
It will continue to wreak havoc until it's over.
This ends when it ends, when we have a treatment or a vaccine.
That's when it ends, a reliable treatment, an effective one.
That's when this ends, not before,
not when the president wishes it would end.
It's not how this works.
There were a whole bunch of people went up to Michigan, in Michigan,
went up there and ranted and raved and screamed, whined and cried,
and blocked a hospital.
That side of the political spectrum in the United States
is very fond of the saying, facts don't care about your feelings.
Yeah, we get it.
You're upset.
Everybody's upset.
We are all in this together.
Some of us actually realize that.
You going up there and screaming doesn't actually change anything.
It doesn't.
It doesn't make them find a treatment faster.
Everybody's upset.
Some of us are just dealing with it a little bit better,
because some of us have learned to trust the experts rather than the people that
have a long, long history of lying.
If you want to destroy this economy, support these politicians.
If you are an accelerationist and you are just done with the United States,
support those politicians that want to reopen now,
because that's what's going to happen.
Pick on Michigan.
I'll go ahead and talk about Florida.
Florida's governor is ready to go.
He's like, yeah, we need to reopen.
Get everybody back.
Cool.
Sounds good.
So all these hotels down on the beach, all these resorts,
all these tourist attractions, you open up.
There's no more relief coming.
They bring their employees in.
That's great.
But nobody's traveling.
Nobody's going to come here.
That is the fastest way to bankrupt this state.
If these politicians stop for one second to care about the average worker
and think about their position and how the average person is going to respond,
they would understand that.
The troops won't be home by Christmas.
This is going to be a long thing.
Even after safe at home is over, you're still
going to have to social distance until there is a reliable, effective treatment
or a vaccine.
They're selling something.
What they're selling is your life. It's not safe.
It won't be safe until there is a reliable, effective treatment.
Nothing's going to change that.
No amount of political posturing, no amount
of yelling at different media outlets, no amount
of defunding organizations that are combating it.
It doesn't change the facts.
This is not under control.
The government needs to provide relief.
If they want to do something, they need to provide relief to the average worker
until it's under control.
Pushing it too soon, promising the troops will be home by Christmas means nothing.
And let's say they do bring the troops home by Christmas and it's not over.
They lose.
Same thing here.
If we reopen before it's over, we lose.
Because every time there's a spike, and there will be, confidence gets lower.
And right now, people are, most people, understand the situation.
And they're doing what they can to not transmit.
If the government says it's over, well, it's over.
And all those precautions get thrown into the wind.
It'll be back.
The government should spend its time trying to do its job,
rather than trying to spin its way out of this.
The failures that have occurred, they're gonna be publicized.
There's no amount of spend doctoring.
There's no amount of PR that's gonna change that.
The only thing that can make up for it is actually doing your job now.
If you're done with the US, if you're just ready to bankrupt everybody, let's reopen
now.
Otherwise, stay at home.
Wash your hands.
If you have to go out, wear a mask.
Don't touch your face.
All the stuff you know you're supposed to be doing.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}