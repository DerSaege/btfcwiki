---
title: Let's talk about why Dr. Fauci wants you....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Euadz5isg9o) |
| Published | 2020/04/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Public service announcement about volunteering for a study led by Dr. Fosse with 9,999 other people.
- Goal is to track the spread of the virus among those who may not have known they had it by analyzing antibodies in blood.
- Volunteers must be 18 or older, currently healthy, not showing symptoms, and not tested positive before.
- The study involves a teleconference patient exam and blood analysis for antibodies.
- Information gathered will help understand the spread and assist in mitigation efforts.
- Volunteers can email clinicalstudiesunit@NIH.gov to participate.
- Beau acknowledges trust issues but stresses the importance of this study for a clearer picture.
- Results may reveal how far the virus has spread unnoticed and guide future containment efforts.
- Participation is voluntary, no pressure to join if uncomfortable.
- Sending an email expressing interest can lead to more information about the study.

### Quotes

- "If you're up to it, send the email."
- "Knowing how far it has spread among people who didn't have severe symptoms is really important."

### Oneliner

Beau encourages volunteering for a study tracking virus spread through antibodies, stressing the importance of participation for a clearer picture of the situation.

### Audience

Potential volunteers

### On-the-ground actions from transcript

- Email clinicalstudiesunit@NIH.gov expressing interest in the study (suggested)

### Whats missing in summary

Details on the potential impact of volunteering and how it can contribute to understanding and combating the virus on a larger scale.

### Tags

#Volunteering #PublicService #ResearchStudy #CommunityHealth #VirusSpread


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about a public service announcement.
This video is a public service announcement, that's really what it is.
Short version, Dr. Fosse wants you and a ragtag group of 9,999 other people to volunteer.
Everybody wants to pitch in, everybody wants to get back to normal, everybody wants to
get out of the house.
This can help get us there.
At the same time, there are going to be some people with some trust issues on this one.
There are going to be some people reluctant to go through with this, and that's okay.
I definitely understand that point of view.
But if you are interested in doing it, you have to be 18, you can live anywhere in the
United States.
You have to be currently healthy, you can't be showing any symptoms at all, and you can't
have tested positive for it in the past.
Now if you volunteer and they accept you, they will do a teleconference patient exam
type thing with you.
And then after that, they'll take your blood and analyze it.
They're looking for antibodies.
So the idea of the study is to track the spread among people who didn't know they had it.
So what they're doing is looking for these antibodies that are developed in your blood
to fight this thing off.
So they're trying to get a clear picture of where it's been.
You're helping them look back in time.
This will help them with their mitigation efforts.
Right now that's the phase we're in.
After this phase, we can go to a more strategic containment phase.
You know, where it's not entire states, it's little pieces at a time.
This puts us on the road to get there.
Now if you want to volunteer, you need to send an email to clinicalstudiesunit at NIH.gov.
That is clinicalstudiesunit at NIH.gov.
I will put that email in the description.
Again I understand not everybody is going to be thrilled at the prospect of giving the
feds their blood.
I will say I've sent an email.
I'm willing to do it, but I've already got my DNA on record, so whatever.
Knowing how far it has spread among people who didn't have severe symptoms is really
important.
We may find out that we're further along in this than we thought.
We may find out that we've got a lot further to go.
But this information is going to be critical for them to get a clear picture.
And right now, with testing the way it is, they're getting a very incomplete image of
what's happening.
It's very ad hoc.
This will help move us along.
Now again, I wouldn't want to put any pressure on anybody to do this.
If you don't feel comfortable with it, don't send an email.
But it is something that some people would be willing to do, and it's something that
can really help.
So if you're up to it, send the email.
One more time, clinicalstudiesunit at NIH.gov.
And send an email saying you're interested in the study.
They'll send you more information.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}