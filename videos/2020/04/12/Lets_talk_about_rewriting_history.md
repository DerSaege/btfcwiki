---
title: Let's talk about rewriting history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zw0HErsYqX4) |
| Published | 2020/04/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- History is constantly rewritten as we gather more information and gain a clearer picture of what happened.
- The idea of rewriting history is not inherently negative; it is a necessary process as history is a living thing.
- Some individuals resist rewriting history because they have given up on learning or are not interested in challenging American mythology.
- Historical figures, like Columbus and Pocahontas, are often portrayed inaccurately in American mythology.
- Rewriting history is vital for creating a positive future and avoiding repeating past mistakes.

### Quotes

- "History is a living thing."
- "People are flawed. People make mistakes."
- "Rewriting history is incredibly important."
- "Every time we get a new piece of information, every time the hindsight picture becomes more clear, we have to rewrite history."
- "We need to be aware of the notes so we don't make the same mistakes again."

### Oneliner

History is constantly rewritten as new information emerges, challenging American mythology and reminding us of the importance of rewriting history for a positive future.

### Audience

History enthusiasts, educators, activists

### On-the-ground actions from transcript

- Challenge and question the established historical narratives (implied)
- Educate yourself on accurate historical accounts and encourage others to do the same (implied)
- Advocate for the inclusion of diverse perspectives in historical education (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the resistance to rewriting history and the importance of acknowledging flaws in historical figures for a more accurate understanding of the past.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about rewriting history.
Because there is a common complaint.
You're rewriting history.
We don't want to rewrite history.
And that sounds good.
Because it evokes this idea when you say rewriting history, it sounds very Orwellian.
The problem is we rewrite history all the time.
As we gather more information, as we get a clearer picture of what happened, history
gets rewritten.
It's constant.
History is a living thing.
As we get new information, we are obligated to rewrite history.
It doesn't end.
It will continue to go on forever.
As we get further away from the date in question, whatever that date is, we'll determine whether
or not it was historically significant, whether or not it's history, or whether or not it's
something that gets relegated to a dustbin, becomes an anecdote, a cool story.
And then we also get to look back and say, wow, at the time we didn't think this was
important, but it led to this, this, and this.
And it becomes historical, when at the time it may not have seemed it.
It may not have seemed important.
As near as I can tell, there are two types of people who say this.
Don't rewrite history.
The first group are people who have just given up on learning.
They learned history in high school or college, and they want everybody to stick to this agreed-upon
narrative because then they don't have to learn anything new.
So one group is just lazy, but that's the smallest group.
The much larger group are people who aren't actually interested in history.
What they don't want rewritten is American mythology.
You know, you have people who say this, don't rewrite history when you're talking about
Columbus.
You know, if you bring up the actual impacts of his visits, people get a little upset because
it undermines this image of him as a hero, this image of him as this great person.
The reality is historical figures are people.
People are flawed.
People make mistakes.
And leaving out the impacts of his visits, that's rewriting history.
That's Orwellian.
That's when you're removing something that's a part of the story that's tangible that has
to be there for it to make any sense.
The same thing happens with Pocahontas.
Her and John Smith were in love.
No, that's not true.
That's not history.
That's mythology.
The Disney version of that story is not history.
It's made up.
But it's what people want to believe because they don't want to accept that many of their
heroes were flawed.
I would suggest that rewriting history is incredibly important.
Every time we get a new piece of information, every time the hindsight picture becomes more
clear, we have to rewrite history.
I would suggest that it's the only way to write a positive future because history doesn't
repeat but the tune is the same a lot of times.
And we need to be aware of the notes so we don't make the same mistakes again.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}