---
title: Let's talk about rights and responsibilities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2rXfJdZTDJA) |
| Published | 2020/04/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the concept of rights and responsibilities, particularly in the context of the U.S. Constitution's Bill of Rights.
- Emphasizes the importance of understanding the inherent responsibilities that come with exercising rights.
- Gives examples like the right to free speech and assembly, linking them to the responsibility to ensure those rights are exercised in a way that benefits society.
- Talks about the Second Amendment and the responsibility it entails in protecting the community.
- Criticizes those who claim to be ready to defend rights but fail to fulfill basic responsibilities.
- Raises the idea that ultimate freedom comes with shared responsibility for everything.
- Points out the selfishness of focusing only on rights without acknowledging responsibilities.
- Challenges the notion of using certain groups as shields for personal struggles.
- Calls for consistent support and care for all, not just in times of crisis that impact the middle class.
- Condemns the privilege and transparency of those who prioritize rights over obligations to society.

### Quotes

- "If you are not concerned about your responsibilities and you're only concerned about your rights, you're just selfish."
- "Don't use them for that."
- "It just reeks of privilege."
- "You have the right to free speech. It's in the First Amendment. Why was that put in there? Was it so anybody could say any silly little thing that popped into their head? No, of course not."
- "But you can't make the sacrifice of staying home and watching Netflix to protect your community."

### Oneliner

Beau delves into the intertwined nature of rights and responsibilities, exposing the selfishness of prioritizing one over the other, urging for a balance that truly benefits society.

### Audience

Rights and Responsibilities Activists

### On-the-ground actions from transcript

- Ensure that your exercise of rights is accompanied by a sense of responsibility towards society (implied).
- Advocate for policies that support living wages and access to healthcare for all individuals (implied).
  
### Whats missing in summary

The full transcript provides a comprehensive exploration of the societal implications of prioritizing individual rights over collective responsibilities, urging for a more balanced and empathetic approach to community welfare.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight we're gonna talk about rights.
It's a hot topic right now.
Everybody's talking about their individual rights.
I'm gonna give the short version up front
for the people who may just need to hear the advice.
If you have somebody in your life
who is constantly talking about their rights
and what they have a right to do,
yet they never talk about another R word
that goes along with it,
and they never act on that other R word,
you can go ahead and cut them out of your life.
They are not gonna contribute anything of value to it.
It is what it is.
Okay, so in the U.S., at least,
when people talk about their rights,
they talk about the enumerated ones,
the ones listed in the Bill of Rights in our Constitution.
Most people really only know the first two amendments,
to be honest, but we can use those as an example of this,
because in every right,
there is an inherent responsibility.
You have the right to free speech.
It's in the First Amendment.
Why was that put in there?
Why did the founders say, we need to write this down?
Was it so anybody could say any silly little thing
that popped into their head?
No, of course not.
That wouldn't be worthy of inclusion.
Why was it there?
It was there so society could advance.
It was there to protect new ideas, you know,
so people could advance a controversial theory
and it not be labeled as heresy,
so you could criticize your government.
That's why it's there.
In order to do that, you have to know
how your government works.
Inherent in the right to free speech
is the responsibility to become educated,
so what you say actually has value.
You have the right to an assembly,
to peaceably assemble.
You have the responsibility
not to block off a hospital when you do it.
That seems like it should go without saying.
You know, we'll just go ahead
and skip to the Second Amendment.
It's the one most people are familiar with,
and typically the people who view this right as sacred
are the biggest offenders.
Shall not be infringed.
The right to keep and bear.
Cool. Why?
It's actually written in this one.
They made it real clear,
because at any moment,
you can be called forth to protect your community.
That's the responsibility that goes along with it.
Does that responsibility disappear
if you haven't picked one of those things up?
It doesn't. It's still there.
You have a responsibility to protect your community.
We got a whole bunch of people out there
talking about how they're ready.
They are ready to defend the country
and individual liberty.
Yeah.
I'm supposed to trust you in a firefight.
I can't even trust you not to make unnecessary trips
to the store to get Twinkies.
You will pardon me
if I don't sleep soundly with you on watch.
You're gonna make the ultimate sacrifice
for truth, justice, the American way.
Mom and apple pie and all that stuff.
But you can't make the sacrifice of staying home
and watching Netflix to protect your community.
The responsibility that goes along with that right.
Again, you will pardon me
if I doubt your commitment to the cause.
If you want ultimate freedom,
you want the right to do anything,
as long as you are not infringing on somebody else.
And that's the society I want.
There's a catch to that, though.
If you have the right to do anything,
you also have shared responsibility for everything.
If you are not concerned about your responsibilities
and you're only concerned about your rights,
you're just selfish.
That's all it boils down to.
And it's very transparent.
These same people all of a sudden,
well, we have to get the economy back on track
because of those poor people.
Those poor people.
See, I know a lot of people on the lower rungs
of the socioeconomic ladder.
They're fine.
I mean, they're struggling,
but they always have been.
That's not new for them.
Calling them forward,
you're just using them as a shield.
We have to help them, because in reality,
what it boils down to is you don't want to admit
that for the first time in your life, you're struggling.
You need help.
Don't use them for that.
Yeah, we do need to help them,
but we need to help them all the time,
not just when it also impacts the middle class.
If people actually cared about those
we are deeming essential now,
they'd probably have a living wage,
probably have access to health care, stuff like that.
But it's those who have focused solely on their rights
and not on their responsibilities,
their obligations to humanity, to society.
Don't do that.
It just reeks of privilege,
and it's extremely transparent.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}