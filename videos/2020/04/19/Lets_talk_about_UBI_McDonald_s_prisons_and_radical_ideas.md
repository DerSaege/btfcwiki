---
title: Let's talk about UBI, McDonald's, prisons, and radical ideas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qmaLRP65c0o) |
| Published | 2020/04/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Societal change is imminent and historically triggered by events like the current one, potentially related to income inequality.
- Many seek an egalitarian society but may not know exactly what they are looking for due to the flaws in the current system.
- Reading social media arguments led Beau to reconsider his opinion, even changing it based on a counter-argument against Universal Basic Income (UBI).
- Beau challenges the idea that people wouldn't work at minimum wage jobs like McDonald's if their basic needs were met, pointing out the societal stigma attached to such jobs due to low pay.
- He questions the belief that comfort is the enemy of radical thought, acknowledging that even in times of comfort, people can still strive for radical change.
- Beau argues that radical thought and mutual aid, like making masks during the current crisis, can occur even when people are not pushed to extreme discomfort.
- Drawing from experiences in prison, Beau illustrates that human nature drives individuals to seek improvement even in challenging environments where basic needs are met.
- The current system is deemed broken by Beau, leading him to reject a return to the status quo and advocate for something different, although unsure if UBI is the solution.

### Quotes

- "People always want to better themselves, better their situation, better society, most people."
- "Human nature provides the carrot."
- "Even in one of the most depressing environments in the world, human nature still drives for improvement."

### Oneliner

Societal change, driven by income inequality, challenges stigmas and comforts, showing human nature's pursuit of improvement.

### Audience

Activists, policymakers, advocates

### On-the-ground actions from transcript

- Engage in mutual aid efforts like making masks for hospitals to support the community (exemplified)
- Advocate for systemic changes to address income inequality and societal flaws (implied)

### Whats missing in summary

The full transcript provides a deep exploration of societal change, income inequality, comfort's role in radical thought, and human nature's drive for improvement.

### Tags

#SocietalChange #IncomeInequality #UBI #RadicalThought #MutualAid


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're going to talk about life after this.
Societal change.
It's coming.
Should come, anyway.
Historically speaking, events like this trigger societal change.
They trigger shifts.
Given the current level of income inequality in this country, it's probably going to have
something to do with that, because it's all being highlighted.
The facade is just falling down around our ears like a cheap movie set.
Most people who follow this channel view a radical idea as a good thing.
They're looking for some egalitarian society.
They may not know exactly what they're looking for, but they know that this system isn't
what they want.
It doesn't work.
Doesn't work for everyone, and therefore it doesn't work.
Because of that, a lot of theories are being floated.
I'm spending a lot of my day reading social media and watching the arguments.
One of the counter arguments to one got me thinking, got me actually to change my opinion.
An argument against UBI got me to change my opinion.
And it basically boiled down to, you know, if everybody had their base needs met, well
then nobody would work at McDonald's.
First, if you're willing to throw away a theory that might lead to an egalitarian society
simply because you won't be able to get a Happy Meal, that's pretty cringey.
Aside from that, it's flat out wrong.
It's wrong.
People work at McDonald's now.
What is wrong with working at McDonald's?
There's a social stigma attached to it.
But is it shameful?
Is there something wrong with doing that job?
Do you look at somebody who owns a burger stand, who does the exact same thing, you
look at them the same way?
No.
There is a social stigma attached to McDonald's and other minimum wage jobs because they're
minimum wage.
Because they don't make a lot of money.
There's nothing wrong with the job.
There's nothing wrong with cooking burgers.
But because it doesn't pay well, and everybody knows it doesn't pay well, there's a social
stigma attached to it.
But people are doing those jobs now anyway.
And a lot of them aren't having their basic needs met.
So yeah, people would still do them.
Because people always want better.
People always want to better themselves, better their situation, better society, most people.
You don't have to keep people motivated with the threat of abject poverty.
It doesn't even make any sense.
You don't need that stick.
Human nature provides the carrot.
People will do those jobs.
And they'll be able to do them and lead a better life.
Now, UBI in general, I've talked about it before.
And I've said I've looked at it pragmatically, looking at the math, the theory, all of that
stuff.
Yeah, it'll probably work.
But I always kind of qualified it by saying I'm against it on philosophical grounds.
That counter-argument, making me question that, making me question those philosophical
grounds, because I think most people, when I said that, they know I don't believe good
ideas require force most of the time.
There's an element of coercion inherent in that that I don't like.
Now it's no more than any of the other coercion that comes from our government, but it's there.
I think people wrote it off to that.
That's not actually it.
I believe, believed, maybe still believe, maybe don't, not sure, still kind of mulling
it over, that comfort is the enemy of radical thought.
If you want radical change, you want radical thought, you want revolutionary change, you
want systemic change, people can't be comfortable.
Because if they're comfortable, well, then why would they want change?
It's a real cynical view.
That's why I never really elaborated on it.
I'm not sure it's true, though.
I'm not sure it's true.
Because right now, there's a whole bunch of people stuck at home.
They don't have a choice.
They're stuck there.
And yet they're still trying to better things.
Still using 3D printers to make masks, people sewing masks, giving them to hospitals for
no personal gain.
Just trying to occupy their time and better themselves, society in general, the world
around them, all at once.
No benefit to it.
Doing that, engaging in that kind of mutual aid, is radical.
That's radical thought.
And these people, yeah, they're not super comfortable in the sense that they can go
do whatever they want, but they're not uncomfortable.
Not to the level I was picturing it needing to get at before things changed.
Maybe free time, the ability to live life rather than just trying to survive, would
encourage radical thought.
Maybe if it happened here, it could be exported.
And people in other places could see a more egalitarian society function.
Now is UBI the answer?
I don't know.
But what I do know is that counter-argument made me realize that radical thought can occur
in any situation.
And the premise of people just sitting around doing nothing is patently false.
Give you another example.
I made a joke the other night on Twitter.
And I realized that there are a whole lot more people following this channel that have
done time than I would have imagined.
I said, some of you couldn't do 30 days in the hole, and it shows.
And there were a whole bunch of people that responded to that in various ways that let
me know they had done time.
Prison is a perfect example of what I'm talking about.
It completely destroys the idea that if people's base needs are met, they won't do anything.
Because in prison, food, water, shelter, clothing, albeit substandard medical care, it's all
there.
It's all there.
However, and those people who I'm assuming will be in the comment section, go ahead and
tell everybody what your side hustle was.
Everybody had one, right?
Everybody did something.
Could have just been stealing food from the mess hall, chow hall.
Everybody had some side gig to better themselves, better their surroundings.
Why?
It certainly wasn't enough to put you on solid economic footing when you got out, unless
you were doing something really stupid.
It was just to make things a little bit better at the moment.
Even in one of the most depressing environments in the world, human nature still drives for
improvement.
I would imagine that if it's going to happen in a depressing, hopeless environment like
that, where you know nothing's really going to change, it would work where change is possible,
if we allow change to be possible.
I don't know if UBI is the answer.
I still have some other qualms with it.
What I do know is that going back to the status quo is not the answer.
This system is broke.
It's shown.
It's clearly demonstrated.
This system doesn't work.
We need something else.
Anyway it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}