---
title: Let's talk with Ti....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8BZ0QsapPg8) |
| Published | 2020/04/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Came across a video that kept appearing in his feed about hair growth tips that led to connecting with tonight's guest.
- They dive into the guest's evolution from beauty and fashion content to discussing social issues like racism and white privilege.
- The guest felt compelled to speak out on politics, particularly about Biden, due to frustrations with the DNC and mainstream media.
- Despite risking subscribers, the guest used their platform to express dissatisfaction with the political landscape.
- The guest hosts Saturday night live streams during quarantine, discussing pandemic profiteering and supporting local businesses.
- The ongoing theme of the guest's content revolves around addressing privilege, inequity, and amplifying marginalized voices.
- Both Beau and the guest touch on the responsibility of using their platforms to reach and impact their audience.
- The guest shares insights into staying at home during the pandemic and their background as a writer for digital content.
- They touch on the importance of voting and the challenges faced during primary elections amid voter suppression concerns.
- The guest expresses their perspective on the upcoming election, advocating for voting ethics and sharing their views on Biden and third-party options.

### Quotes

- "Compassion is not for the weak. Empathy is for the strong."
- "I'm fighting for a lot of the things that my ancestors were fighting for."

### Oneliner

Beau connects with a content creator discussing their shift from beauty to social issues, political frustrations, pandemic profiteering, amplifying marginalized voices, and the importance of compassion in a challenging world.

### Audience

Content creators, social justice advocates

### On-the-ground actions from transcript

- Support local businesses to counter pandemic profiteering (implied)
- Exercise compassion and empathy in interactions and activism (implied)
- Take steps to address privilege and inequity in your community (implied)

### Whats missing in summary

The full transcript provides depth on navigating activism, political engagement, and personal values in today's complex societal landscape. Watching the full interview offers a nuanced understanding of the guest's journey and perspectives.

### Tags

#SocialJustice #PoliticalActivism #CommunitySupport #Empathy #VotingRights


## Transcript
Well, howdy there internet people, it's Bo again.
So tonight we're gonna be talking to somebody who's here
because YouTube's algorithm is really creepy good.
So I was scrolling my feed
and you can imagine what my feed would look like.
It's a bunch of political stuff
and then some crazy prepper stuff
and all of this, just stuff that you would normally
associate with me.
And then there's a video that keeps popping up
telling me how to get past the plateau
when my hair stops growing.
And I'm like, well, that's weird,
but it showed it a couple of times
and I'm like, YouTube's algorithm's pretty good
so I'll go ahead and click on it.
And then I clicked on some of the other videos
and sure enough, it was just showing me the wrong video
from the right channel.
So that brings us to tonight's guest.
You wanna introduce yourself?
What's up everybody?
I am T, AKA the nappy headed jojoba
and based on Bo's story,
I've done quite a bit of different types of content
over the years, needless to say.
So after, I guess you did beauty and fashion stuff,
but the one that caught my attention was one that said,
well, I'll let you get to it,
but the one line in it was like,
and I know people are gonna unsubscribe and get mad for me
interjecting this kind of content onto the channel
and it was about Biden.
So tell us about that.
And I wanna know what prompted you
to start talking about it,
to open up to people and to talk about it.
And open up to politics like that.
Before I answer, your voice,
once you started the recording
has sounded really garbled in the mic.
And I'm just worried that your audio
and your end is going to record that way.
And is there a way for you to like check playback
for yourself just to make sure you can actually use all this?
It's not, but let's see if that helps.
Cause it sounded normal before,
but once you started the recording,
it sounds like you're going through a voice scrambler.
Yeah, I'm way out in the boonies
and it actually, once the recording start,
it takes a while for it to catch up.
But honestly, when it comes to these videos,
nobody's really here to hear me anyway.
It's okay though.
What if they wanna listen to you?
And you sound fine, so we're good.
All right, if you say so.
Well, in any event, to answer your question,
over the past year and a half or so,
I was just getting really bored
with my channel content in general.
So I was starting to branch out
from just doing fashion, beauty, things like that,
to talking about more social issues,
kind of a racism, white privilege 101, so to speak,
because I've gone to predominantly white institutions
most of my life and therefore I have a lot of white friends
who have no idea what my actual day-to-day experience
has been like.
They have no idea what kind of challenges and things I face.
And I just kind of wanted to get some things off of my chest
because I was feeling extremely unfulfilled
talking about things as frivolous as lipstick and hair only.
I still like talking about that stuff,
just not all the time.
And there are definitely more important substantive things
happening in the world.
So that's continued to evolve.
And while continuing to explore
those larger social issues, being an SJW, I suppose,
this election, this primary has been something
that has been looming,
but something that I didn't really know
if I wanted to touch exactly
because it's one thing to talk about broad things,
like even racists will pretend to think racism is bad.
I'm not going to lose subscribers if I say racism is bad.
But if you actually start endorsing
a particular political candidate,
it's that old cliche,
never talk about religion or politics.
So the urgency of what I have seen happening in this primary
in terms of the treachery and machinations
of the DNC and mainstream media,
I just felt it on myself that I had to do something,
that I had to say something after Super Tuesday
because I was fed up, quite frankly.
I'm just tired of seeing the same things happening
that we saw happen four years ago
and that we've seen go on, frankly,
for far longer than that.
And I've only got one YouTube channel and one voice,
but I'm damn sure I'm going to use it.
Nice, nice.
And the gist of that video was
Biden is not my man, basically.
I mean, short version.
I hope you're treating it kindly, yes.
Yeah.
And then I didn't get to watch it,
but you did a live stream here in the last few days
that was, I noticed that 1% was in the title.
Tell us what was, sum that one up for us.
Yeah, I've been doing Saturday night lives every week
since we're all in quarantine anyway.
And we've all got more time on our hands
and it's just a nice way to kind of chat about things
in a little bit more real time
than necessarily I would be able to
with a regular YouTube video,
where I have to edit it and whatnot.
And I've just been noticing
that ever since the pandemic began,
there've been so many pandemic profiteers
with these corporations and companies,
all of a sudden being even more predatory
in their capitalism and in their practices
and in the ways that they're trying to exploit people
in a very vulnerable time
where people are much more inclined to shop,
to self-soothe, to make reckless decisions,
just in that F it, YOLO,
I've already lost my job anyway, like who cares?
And I just felt it was so predatory
because of the inundation of emails I'm getting every day
that I wasn't getting before all of this started.
I'm getting emails from companies
that I haven't even heard from or purchased from
in five, 10 years.
I'm like, how do you even still have my email address?
And you're contacting me now about free shipping.
That's your offer?
Get out of my face.
So it just really irritates me.
And if anyone should be,
if you have the funds and you don't,
if you actually are able to pay your bills
and buy food right now and you have funds to spare,
don't kick it upstairs to corporations,
support local businesses
because they're the ones suffering, not Nordstrom.
Nice.
All right.
So that's what that was about.
Okay.
I guess I will, I guess I have to go back and watch it.
So you said you did a white privilege 101.
You want to give us a craft for real quick?
Well, I mean, it's not like one particular video.
It's just an ongoing theme of trying to,
I guess thematically with any of the videos that I do
where I discuss issues of privilege
and inequity and things like that,
I try to have patience because I know a lot of my audience
is obviously going to be a lot of black women
because people want to watch people who they relate to
and who look like them.
So that is my core audience.
And I want to speak up for women
who are pretty much across the board,
underrepresented and often marginalized.
I mean, black women get the short end of the stick
far more often than is warranted,
but that's not the only people watching my videos.
And I want to try to help them understand the perspective
and the experience that we're facing
because I have this little platform, you know,
it's sort of like, you know, that old hip hop credo,
each one teach one.
I don't necessarily begrudge anyone
for not understanding my perspective
because I say all the time,
the only perspective that you can have is your own,
but compassion lies in trying to understand perspectives
that are not your own.
So if someone is trying to deny my experiences
as a black woman, as whatever,
then that person's getting blocked.
It's pretty easy.
It's actually been very convenient to do a culling
by doing these videos and figuring out who needs to go.
So I know some people are probably unsubscribed from me
because they still want me to just exclusively talk
about hair conditioner,
but I mean, I think we kind of get it by now.
Like, I think we all know how to braid our hair
at this point.
Nice.
You know, you kind of hit on something.
You know, there's some of the videos that I do, you know,
that I'm like, this is really for 5% of my audience.
It's like 5% of the people that are going to watch this,
it matters too.
Yeah.
But you're right.
If you have a platform,
you kind of have that responsibility to,
if you can get a message to that 5%, get it out there.
Yeah.
Even with my first Biden video.
Yeah, thank you.
Like that first Biden video,
I had no idea that that was going to get shared
and circulated the way it did.
I don't think I've ever had a video hit
over a hundred thousand views that quickly ever.
Like I'm very used to having a very like blue collar
kind of YouTube channel,
cause it's a side hustle, you know, it's a hobby for me.
It's just something that I enjoy.
It's not my career.
So that was very surprising to me
because I felt I was doing,
I was taking a pretty big risk in doing that.
Again, don't talk about religion or politics,
but it's like, listen,
if this can convince five people to talk to their parents
and grandparents and convince them to not vote for Biden,
that is worth losing however many subscribers to me.
Like I, this is not my living.
So, you know, it's just some,
sometimes you have to use, you know,
a platform that can reach infinite people really.
Once it's on the internet,
it can reach pretty much anywhere except China.
But even if that platform is in theory infinite,
sometimes you need to talk to a small portion of it.
Absolutely.
Absolutely right.
So you've been watching, I guess,
at least know a little bit about my channel.
Is there anything you would like to say
to the people on my channel?
I feel like you probably have a very dope audience.
I don't really go down in the comments too much
because a huge part of,
I'm a writer and producer and director.
So I tend to make a lot of content
for like corporate YouTube channels and whatnot.
So I just know how dark things can get
in the YouTube comment section.
Like if you wanna lose,
if you're feeling really good about life
and you wanna destroy that,
go and read some YouTube comments.
But I imagine nonetheless that across the board,
because like, I'm very lucky to have a very,
for the most part,
you're always gonna get a couple of bad apples.
I've got a very wonderful YouTube audience
and I really appreciate them.
I feel like we have our little parasocial family
and it's wonderful.
And I do think a big part of that is attracting what you are.
So I imagine that you two have an amazing audience
for the most part.
You're always gonna have a couple of loopholes.
But if I could say anything to your audience,
I think, and this is just something
I've been thinking about lately,
particularly because of this debate as if Biden is viable.
But anyway, have the courage to have compassion.
I think people think of strength as being this bully
and a strong man type like Trump
or like Biden used to be right now.
He's a husk of a man.
But the truth is strength,
empathy is not for the weak.
It really isn't.
Having compassion, having empathy, having sympathy,
these things are not for the weak.
In fact, you have to be strong to have those things.
And I really hate that there's this perception
and this rhetoric of hyper masculinity being equated
with being cruel and callous
because that's just not the way it is.
I like it.
So is there,
how are you dealing with being stuck at home,
being stuck at, or are you staying home?
Are you doing what you're supposed to?
Yes, I mean, I do have a dog.
You might've seen her skulking around in the background.
I do have a dog,
so I have to take her out a few times a day
and I still have to go to the bank or to get groceries.
So I'm probably going out out to do things like that
maybe once a week because I still like to eat fresh food
while we still have it.
We're still somewhat pre-apocalypse.
So I have been mostly staying home.
And to be honest with you,
it's not a huge adjustment for me
because other than maybe going out for drinks
and dancing with friends on the weekend
and going to my gym,
which I would do pretty much every day, I can't do that.
So I'm working out at home.
The dog is just like, what the hell are you doing?
She usually leaves the room.
And beyond that, it hasn't been a huge adjustment
because when it comes to staying home and avoiding crowds,
that's my lifestyle.
I've been training for this my whole life.
So it hasn't been that bad for me.
And I'm lucky because I'm self-employed.
So typically I'm home anyway.
As a writer, I'm usually doing my work
right where you see me right now.
So what do you write?
I write shows.
So, I mean, I'm not supposed to say the type of jokes
that I write, but they rhyme with sick
and it starts with D.
And I have been mostly doing digital content
for the past several years.
So I write a lot of explainer type shows.
If you've ever heard of the show,
I don't write for this show.
I'm specifically choosing a show
that I don't write for it as an example,
but Adam Ruins Everything, that sort of show,
where it's just like, why is this like that?
I'm the person behind shows like that.
Very cool.
Yeah.
That's surprising.
So your channel is literally a hobby.
And now, I mean, it's not a small channel though.
No, it was definitely treading water for a long time there
because, again, how many videos can you make
about braiding your hair or whatever?
But once I started talking about things
that were more important to me,
I feel like people notice.
Ironically, when I decided to start making videos
as if no one was watching, people started watching.
And it's cool.
I mean, it's great to feel like people are interested
in my rantings because that's a surprise.
Yeah, that's actually very, very relatable.
Yeah, this was not supposed to turn into what it did.
Glad it did, but yeah, it was a surprise.
Right?
So here's, yeah.
So here's one question I ask everybody
that comes on the show.
What is one thing that people can do in their own community
to make the world a better place?
I would say have the courage to work for a reality
that you may never live to see.
Because I'm fighting for a lot of the things
that my ancestors were fighting for.
We still ain't got them,
but that doesn't mean I'm gonna give up.
So if it comes down to making YouTube videos
that five people watch,
maybe one of those people is gonna tell another person
and maybe that person will tell three people
and eventually the message will spread.
But don't give up on good.
Nice.
Nice.
Okay, and I just kind of realized
that we were talking about that video
as if everybody had seen it and we're saying,
don't, you know, you're saying don't vote for Biden.
We might wanna like kind of stretch that out.
Who might they should vote for?
Who would you recommend?
Who else would you recommend them not vote for?
Well, it's interesting because when I made it clear
that I don't support Biden,
he is of course repeatedly being called a presumptive nominee
because let's face it, the numbers don't look good for Bernie
but people are like,
so does that mean you're gonna vote for Trump?
And I'm like, have you met?
Is that a real question?
I can't even dignify that with response, of course not.
But I mean, I am clearly a Bernie bro.
I mean, this is what we look like, textbook Bernie bro.
And I think one thing that's really irritated me
about this entire primary as the, you know,
the whoopie Goldbergs of the world
and the Alyssa Milano's continue to help put the fix in
to force Biden down our throats.
The worst part perhaps is this false equivalency
that people who refuse to vote for Biden
are somehow responsible for Trump getting four more.
Nobody owes Joe Biden their vote, least of all me.
I don't owe Joe Biden my votes.
And that does not mean that I'm gonna go vote for Trump
instead of if that's what it comes down to in November.
I am privileged enough to not be in a swing state.
So from an ethical level,
I don't have to be guilted or shamed
into having to hold my nose and vote Biden.
Like I don't have to do that.
I'm gonna win my state regardless.
However, I don't necessarily agree
with anyone doing that either
because that's just playing the DNC's game.
Again, trying to dismantle master's house
with master's tools.
The whole thing's gotta go.
So I don't really try to tell anyone how to vote,
but I am trying to persuade people
to not just hold their nose and vote for Biden
because it doesn't have to be this way.
So let's assume the DNC gets their way.
You're definitely not saying to vote blue no matter who.
The most inspiring slogan I've ever heard in my life.
Right?
Here, please accept this mediocrity.
So would you suggest people that are not in swing states
vote third party if they're not in favor of Biden?
Do they stay home?
I never advocate staying home
simply because as a person of color,
people literally died for me to have rights like voting.
So I have voted in every election
that there has been since I became of age
and I will continue to do so.
You know, I do hear rumblings of a write-in Bernie campaign.
I don't really know what the answer is.
I'm still kind of trying to focus on this battle
before the next one, which will be the general.
I'm just like, maybe Bernie can turn things around
because I do feel that Bernie
still has a very energized base,
but voter suppression is very real.
Even today, the day that we're recording this interview,
there was the Wisconsin Democratic primary.
And of course the GOP in cahoots with Joe Biden's campaign
forced the primary through putting everyone at risk.
And I believe in Milwaukee,
they went down from something like almost 200
polling places to five,
just because polling place workers were just like,
you're not gonna catch me putting my health and life at risk
just because you guys aren't smart enough to delay
or do a mail-in primary.
But things like this, I mean,
it's just voter suppression by another name,
not to mention having insane lines.
We people keep talking about how young people
aren't showing up for Bernie.
They're not showing up for Bernie.
I also don't know if that's necessarily true
because one, you can just say that
and then people aren't necessarily going to vet it
and prove that that's not true.
People don't usually actually go and follow up on that data.
And two, there is voter suppression
and that even here in California,
here in Los Angeles where I live,
I remember on Super Tuesday when we voted,
the lines were longer than I can remember seeing
except for, oh, maybe 2008.
Interesting.
And I also suddenly had a new polling place.
So there's all sorts of weird things going on.
Luckily for me, I vote by mail.
So I actually just take my ballot to my polling place,
but I was just looking at all the people in line
like sucks to do you.
And then I had friends who were waiting in line
for three, four, five, six hours, which is unheard of here.
And again, I think it's much more likely
to suppress young voters' ability to vote
because even if they go to the polling place
and they see that they have to wait four hours,
how many 25-year-olds do you think are able
to just blow off work and wait in line five hours to vote?
Zero, 0.0.
Maybe people who are older, our favorites, the boomers,
maybe some of them have a little bit more time,
maybe they're retired,
maybe they have more flexible schedules
because they're in more higher up positions
where they're able to actually take off time to vote.
Frankly, everyone should be able to take the day off to vote
but we're not there yet.
So in any event, all of these things make me wonder
if there is any way out of this primary
where we aren't going to come out with Biden on the ballot.
It seems like the Democratic Party and the DNC,
like as I said, the fix is in.
So that's very disheartening.
So come November, if it is Trump and Biden on the ballot,
which is what they seem to want,
I'm not out here trying to tell anyone how to vote.
I'm not trying to tell anyone to stay home or not.
I mean, that's not my place.
I do not call myself an influencer
and I will not let anyone else call me an influencer
but I do realize that people tend to value the opinions
of the YouTubers they like and perhaps who they dislike.
And I feel like that would be irresponsible of me to say,
go do this just because I have a YouTube channel.
So it's really up to each individual
what they do on that day in November.
I mean, God only knows if we're still going
to have a planet in November
because she's getting rid of us clearly.
But for me, I will be voting in November,
I guess mailing it in rather than dropping it off
at my local polling place.
But I don't see myself voting for Biden on that day,
regardless, I'm not voting blue no matter who.
Okay.
You're welcome.
I've got a question because I'm hearing you talk
and I'm just, what is your perfect world?
What's it look like?
Not like this.
I, it's almost hard to imagine
because it's so different from what it is now.
I mean, I don't, it's interesting
because people always say,
oh, the American dream from rags to riches.
That's one of the most misquoted things of all time.
It's from rags to respectability.
And there's far too few people
who are even able to achieve that,
particularly millennials like me who have now lived through
or well, we're living through the second,
like two of the greatest financial crises
of the past century.
It's ridiculous.
The 2008 recession and now this one,
really just out of control to experience
in such close proximity for one generation to go through.
You know what I'm saying?
But I do wanna see a world where people don't have to die
because they can't afford to go to a doctor
or because they got sick
and then couldn't afford to pay their bills
and went bankrupt.
I mean, you're talking to a person
who doesn't have health insurance.
I just have to hope I don't get sick.
I would like to see a world where people are paid fairly
regardless of what type of job they do.
You know, it shouldn't be a thing where minimum wage
means you're constantly struggling to make your ends meet.
You should be able to take a day or two off a week.
It shouldn't be like unrealistic
to have one to two days off each week
and to still be able to live a life comfortably
where you're able to put food on the table
and not constantly be chasing your tail.
The world I wanna see is one where the average person
gets to live a reasonably lovely life.
It doesn't have to be private jets.
It doesn't have to be, oh, I must live at the airport
because I'm so booked and busy
and I'm always like this sort of fetishizing
of being a famous YouTuber influencer.
I don't even understand wanting that, quite honestly.
I just want to have money because I want freedom.
And I only want enough money to have freedom.
I don't need a gold toilet.
Well, yeah, it's one of those things
and it's hard to explain to people
who have that economic stability.
Yes.
They didn't have that, that freedom is gone.
The freedom that a lot of people enjoy
when it comes to, well, I'm gonna go do this for the weekend,
there are people that would have to stay for six months
to go do that for the weekend.
Absolutely.
And it's hard to get that across to people at times.
But so from what I'm hearing, most of your major concerns
are economic.
Is that fair to say?
I think those are at the front of my mind
because of our current situation,
because I'm seeing so many people lose their jobs
and unemployment rates are skyrocketing.
And it's all centered around these core issues
of medical care and lack thereof.
But in terms of the health care,
and lack thereof, but in terms of just racial inequities,
that is a very long video.
Because the world I wanna see when it comes to that,
we ain't got the time.
All right.
It needs to be a page one rewrite
on how I would like to see the world change
with regard to the treatment of black people, period.
All right.
Yeah.
I don't even have a summary on that one.
That's a long thing, right?
It's a long thing.
And I don't even know if it's something
that can even realistically happen.
I think at least by starting to create
more economic fairness, that will at least affect
the predicament that black people face,
because indisputably, most black people
are poor to begin with.
It's just, there hasn't ever been a chance
for us to really thrive because,
I think Don Cheadle said this,
America's birth defect is slavery.
And we've never, as a people,
fully been able to recover from that.
It's like, just because you see Will Smith
doesn't mean we finally did it.
You know what I mean?
And it's difficult because I think a lot of people
are still feeling that they are personally attacked
anytime a black person brings up our plight
or brings up something like white privilege.
Like, well, if I admit white privilege exists,
then I'm guilty.
So I'm just gonna deny that it exists.
There seems to be a weird dynamic like that.
So, I mean, if you want a summary,
I think that would be step one,
is stop denying our experience
and giving us this bootstrap rhetoric
as if we're imagining these things that are happening,
things like redlining and obviously everything going on
with the prison system as it is,
which is just slavery 2.0.
Yeah.
Yeah.
I can definitely see from a pragmatic view,
the economic issues, the lack of generational wealth,
all of this stuff definitely feeds into
perpetuating a system that at least on the surface,
everybody would like to see go away.
Yeah.
Whether or not everybody actually feels that way
is a different story, but.
Yeah, a lot of it is definitely lip service.
And I think it's encapsulated really well
by something the late Toni Morrison said.
She said, if you can only be tall
by having someone else on their knees
and there's a serious problem.
And that is pretty much what we're dealing with
in a nutshell.
Right.
Right.
Yeah.
Okay.
Well, now that's depressing.
Tell me something funny.
No.
No.
No.
And that went way off.
Let's see.
Okay, so we've talked about your channel.
Do you have any projects or anything you want to talk about
or anything you want to plug?
I do avoid talking about my professional work
because of fear of being doxxed.
So.
Nice.
Yeah, I undo that.
No, I mean, just if you guys want to check out
my YouTube channel, it's much appreciated.
I also have a Patreon where I do private streams
and it's a little bit nicer because the chats go slower
and I can actually see everyone and respond to them.
And it's just, it's a good time.
All right, well, give everybody the name again.
Yeah, I mean, I have, I wasn't thinking about this
when I came up with this channel name,
but it's perfect for SEO because if you search it,
you're gonna get me.
My channel is called Nappy Headed Jehovah
and that's N-A-P-P-Y-H-E-A-D-E-D-J-O-J-O-B-A.
Yeah, yeah, that's definitely one that,
it's one of those terms that you're gonna have to say that
a lot, especially if you're talking to,
interviewing you, because they're not gonna say it.
They're gonna be like, I'm gonna make you say that.
Oh yeah, it's so funny.
I've had people be like, is that racist
if I say your channel name?
And I don't know, I got it in a sick, twisted way.
I kind of like watching people squirm.
I don't know, I don't know, I don't know.
All right, so any parting shots,
anything you wanna get out there last minute?
I don't think so.
Just once again, the whole like liberal snowflake,
that's a bunch of BS.
Compassion is not for the weak.
Empathy is for the strong.
Thank you.
All right, all right, everybody.
So that's the show for tonight.
It's just a thought, y'all have a good night.
All right.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}