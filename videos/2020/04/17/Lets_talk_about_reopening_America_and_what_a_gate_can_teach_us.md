---
title: Let's talk about reopening America and what a gate can teach us....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4KQUJHlt-Mg) |
| Published | 2020/04/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Receives a call to conduct a security survey on a high rise construction site, despite it not being his area of expertise.
- Agrees to visit the construction site in the panhandle as a favor for a friend, even though it's a long drive.
- Security consultants are usually expensive, but Beau is intrigued due to the ongoing mystery at the construction site.
- Arrives at the meeting spot assuming he's meeting a real estate developer but is surprised by a blue-collar guy in casual attire.
- Discovers that heavy construction equipment has been going missing and suspects the security guards.
- Realizes that the gate in the fence, supposedly locked for months, has a lock that shows no signs of rust, indicating foul play.
- Determines that someone cleverly replaced the lock, leading to the thefts.
- Declines payment for his services and learns that the project manager owns the restaurant and the high rise building.
- Emphasizes not to judge people by appearances, the importance of true expertise, and the need to look beyond surface appearances of security.
- Warns against following advice from pseudo-experts like TV personalities who lack real credentials.

### Quotes

- "Don't judge people by their appearances. You'll be wrong a lot."
- "Just because something appears to be secure, doesn't mean that it is."
- "Follow the experts that actually are experts in the field you need."

### Oneliner

Beau discovers theft at a construction site, uncovering the importance of true expertise and not judging by appearances.

### Audience

Security professionals, truth seekers.

### On-the-ground actions from transcript

- Support blue-collar workers in your community (implied).
- Consult real experts in fields requiring specialized knowledge (implied).
- Double-check security measures in your surroundings (implied).

### Whats missing in summary

The full transcript provides a detailed narrative on the importance of true expertise and avoiding assumptions based on appearances.

### Tags

#Security #Expertise #Judgment #Appearances #Theft #Community #Action


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about assumptions, appearances, and experts, and what a gate
at a construction site can teach us about that.
Years ago I was working down in South Florida, this was like 15 years or so ago.
I get a call from somebody else in the same line of work and he's like, hey, you got any
interest in doing a security survey on a high rise construction site?
What?
No, of course not.
No, why would you, no, that's not actually my area.
What's up, why are you asking me this?
And he's like, well it's for a friend of mine.
And he tells me that the guy who installed the cameras and everything initially, he's
a mutual friend of ours and he's good.
And that after one of the incidents where they had lost literally tons of heavy construction
equipment, they had a local come out and check the stuff and everything's still secure.
Okay so it's a puzzle, yeah, I'm vaguely interested.
And he's like, yeah, there's something weird going on, there's a human element to this
that we're missing.
Yeah I'll stop by, where's it at?
Assuming it's somewhere close.
It's not.
And I'm like, well it's a bit of a drive, but it's in the panhandle.
Fair enough, I haven't been home in like more than a year.
Sure I'll do it.
Tell them to set up something for this weekend.
And I grab my bag and get in the car.
Don't discuss price.
Now this is a good point to tell you if you don't know.
Security consultants are ridiculously expensive, good ones anyway.
Most companies cannot afford one.
Period.
It's just out of their budget.
But this is a high rise development, I'm thinking, sure, maybe.
So I get up there, assuming I'm meeting a real estate developer and I'm wearing my pinstripe
shirt and slacks and I walk into the bar on the beach that he's elected to meet at.
And I see a guy in cut off, torn up blue jeans, flip flops, a polo shirt with paint on it,
and he's sunburned.
This is a blue collar guy.
More than likely, this is a blue collar guy.
He probably cannot afford me.
But whatever, favor for a friend, I'm already up here, got to come home, not a big deal.
Sit down and start talking to him.
And he lays out that this has been going on weeks and they just cannot figure it out.
Mike, have you checked your security guards?
Have you looked into them, see if they owe anybody money, anything like that?
Oh no, no, no, no, I've known those guys for years and I'm a great judge of character.
Okay.
At least this isn't going to take long.
I think I've cracked the case already.
If all of your physical security is working and something is going wrong, it is more than
likely your security staff.
There's something there.
It's at this point that he tells me, you don't look the way I pictured you.
Really?
What were you picturing?
He's like, well, you know, to hear my buddy tell it, I thought you'd look like one of
the guys from the news.
Thought I was going to look like somebody from Blackwater.
Great.
No, I don't even own a pair of Oakleys.
Let's just get to the site, you know?
At this point, he waves to the staff to put our mill on his tab.
We go out to his truck, which is a 1989 Ford Explorer with dents in the door.
The interior is covered in stains and ripped up seats.
And I am certain at this point that I am billing my buddy for this little adventure.
We get to the construction site.
Driving up to it, it looks like normal.
High rise building.
There's a gate.
Everything looks to be functioning at first glance.
His chief guard is there.
And I start asking him questions.
You can tell this guy has answered these questions like a hundred times and he's already annoyed,
but to be honest, I don't care.
Like all these cameras work, right?
Yeah, they work.
And you've reviewed them for the time in question.
Yeah, of course.
This is the only entrance?
No no no, there's a boardwalk on the back.
It has cameras too.
And they work.
And I've reviewed them.
Alright, whatever.
Be like that.
And I start asking him who his guards are.
Maybe I know them.
What's their name?
Trying to get their full name so I can kind of dig into them later.
We start walking around the site.
Get to where the boardwalk is.
And there's no, it's highly unlikely that somebody is taking apart heavy construction
equipment, wheeling it down this thing and then wheeling it down the beach.
And honestly, if they are doing it, good for them.
You know, they've earned it.
It's like a lot of work.
And the cameras are there and they would have been spotted.
We walk around the next corner.
And there's a gate.
There's a gate in the fence.
And I'm like, what's this?
And he's like, well that's where they brought the crane in.
I'm like, it's an entrance.
He's like, oh no no no no, that's been locked for months.
And here we go.
Okay, do you mind opening it for me?
Now what I'm expecting to happen at this point is for him to pull out his keys and go, oh
wow, they're missing, golly gee whiz, I don't know how that happened.
Because you gave them to the people who were stealing stuff.
But that's not what happened.
He pulled out the keys.
Both keys are there to the padlock.
They're both marked.
Slides it in, but it doesn't turn.
Well, that's surprising.
That's not what I expected to happen right there.
And I start looking at the chain and the lock that's been sitting for months in the air
by the beach, the salt air.
There's no surface rust on it.
None.
In Florida.
Okay.
Somebody cut your lock off and put their own on.
Somebody's really smart.
Because anybody walking by who's a physical security person, they're going to grab the
lock, they're going to yank on it.
It's secured.
That's what they're going to do.
Cut it off, replace this, put up a camera.
Sure enough, a couple days later, somebody came up and tried to unlock it.
Now the project manager, the guy in the cutoffs, he reaches for his wallet.
What do I owe you?
Like he's going to slip me a 20 or something.
I'm like, no, no, no, we're good.
After talking to the guy, my read on him is that he's like the foreman, the project manager,
whatever.
He's rattling his network, trying to do his job.
And if he didn't get it done, he probably would have got fired.
Fine.
You know, whatever.
Glad I could help.
Don't worry about it.
Not a big deal.
He's like, well, at least let me buy you a drink and a steak.
Sure.
I will always take a drink and a steak.
It is over this meal that I find out the reason he was able to put our first meal on a tab
is because he owns that restaurant and he owns the high rise and a couple others.
And another restaurant we ate at later.
There's a blue collar guy that made it big during the real estate thing, just never changed,
never showed it off.
Really cool guy.
I actually worked for him a couple of times after this.
The point of all of this, there's a couple of things we can take away from it.
Number one, don't judge people by their appearances.
You'll be wrong a lot.
Number two, just because something appears to be secure, doesn't mean that it is.
Doesn't mean that it is.
And we can take away something about experts.
Right now, there's a whole bunch of people who are taking advice from people who seem
to have credentials.
Dr. Phil, Dr. Oz, that chiropractor.
None of these people have ever actually studied the subject at hand.
Their opinion does not matter.
They're a lot like the guy that did the spot check.
They did everything they know to do, but that doesn't matter because that wasn't really
the area that needed an expert.
It needed somebody that had a different skill set.
The physical security guy didn't do anything wrong.
He did his job to the letter.
Everything was secured, but he didn't think beyond that.
These TV doctors and these pundits, they're going against the general consensus of the
people who have spent their lives in that field, who spent their lives studying how
to control stuff like this.
Their opinion doesn't matter.
They may not be seeing something.
Or maybe they're like kind of what I thought the security guard was doing.
Maybe they have an ulterior motive.
Right now, everything is closed up.
The gates are up.
It's a lot like that construction site.
But if we open it, even a little, even that side gate, you can turn up with missing equipment
or missing people.
It does not matter what the political pundits say.
It's completely irrelevant.
We are going to have to safer at home, shelter in place type thing, social distance until
we have an effective, reliable, available treatment or a vaccine.
Nothing's going to change that.
No amount of debate will shift that.
That's the reality.
You can whine and cry about it all you want.
You can present whatever set of alternative facts you want.
But the reality is, if you cut that lock off and put your own on for your own agenda, it's
going to go bad.
So takeaways.
Don't assume that somebody who looks like a beach bum is.
Follow the experts that actually are experts in the field you need.
And just because something appears to be secure.
Doesn't mean that it is.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}