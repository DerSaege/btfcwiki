---
title: Let's talk about sunk costs and being wrong....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Yssxf8QZ0Z4) |
| Published | 2020/04/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the importance of being okay with being wrong and the sunk cost fallacy.
- Mentions seeing the sunk cost fallacy being related to thought for the first time.
- Explains that a sunk cost is an expense in business that won't be recouped.
- Describes the sunk cost fallacy in the context of buying equipment that doesn't work as expected.
- Emphasizes the need to cut losses and move on rather than trying to salvage a bad investment.
- Draws parallels between the sunk cost fallacy in business decisions and beliefs or ideas.
- Points out that people often struggle to admit they're wrong and cling to incorrect beliefs.
- Stresses the importance of being open to changing opinions based on new information.
- Shares examples of how the sunk cost fallacy plays out in politics, specifically referencing supporters of a certain political figure.
- Asserts the need for responsive and articulate leadership, differentiating it from what he sees in current leadership.

### Quotes

- "It's OK to be wrong."
- "New information should always change your opinion."
- "We need responsive leadership."

### Oneliner

Be open to change, accept being wrong, and beware the sunk cost fallacy in thought, business, and politics for better leadership and decision-making.

### Audience

Individuals, Voters, Citizens

### On-the-ground actions from transcript

- Challenge and rethink your beliefs based on new information (implied).
- Advocate for responsive and articulate leadership in your community (implied).
- Encourage others to be open to changing opinions when new information arises (implied).

### Whats missing in summary

The full transcript provides a deeper insight into the importance of admitting when one is wrong, the consequences of the sunk cost fallacy, and the impact of these factors in business, thought, and politics. Watching the full video can provide a more comprehensive understanding of these concepts.

### Tags

#Leadership #SunkCostFallacy #Politics #OpenMindedness #DecisionMaking


## Transcript
Well, howdy there internet people, it's Beau again.
So tonight we are going to talk about being wrong,
and how it's okay to be wrong.
And we're going to talk about the sunk cost fallacy,
and how that relates to thought.
We're going to do that because today, for the first time,
I saw somebody mention the sunk cost fallacy
in relationship to thought.
I'd heard the term 100 times before.
It always had to do with business.
That's where the term originated.
And when I saw it used that way in relation to thought,
I was like, man, that doesn't even make sense.
And I was wrong.
It's okay to be wrong.
Sat there and thought about it.
I was like, man, that's actually a perfect analogy.
It makes complete sense.
OK, so to understand the sunk cost fallacy,
first you have to understand what a sunk cost is.
A sunk cost is an expense in business
that you're not getting back.
It's a capital expenditure.
It's money you put out that you're not going
to easily or readily recoup.
Now that could be research and development.
It could be advertising, things that you expect.
Or it could be a bad decision sometimes, too.
But the key thing is it's money that is expended.
It's capital that is expended that you're not
going to get back.
The sunk cost fallacy comes into play
when you buy a piece of equipment.
And that equipment's job is to make you more productive.
And it doesn't work right.
Doesn't do what you thought it was going to do.
At that point, you have two options.
Option one is to cut your losses, waste that money
on that equipment, get a new piece of equipment,
and become more productive.
Piece of equipment that works this time.
However, most people, many people,
fall prey to the sunk cost fallacy, which
is where they say, well, I don't want to waste that money.
So they end up trying to take that piece of equipment
and make it work.
In the process, they waste a bunch of time.
Time is money.
They become less productive.
And what ends up happening is they end up
wasting more money in an attempt to not
waste the initial sunk cost.
A sunk cost is expended.
Once that money is out the door, it
should not play any part in further decision making.
Either this piece of equipment works or it doesn't.
It shouldn't factor in to anything in the future.
Because you could find out you're wrong,
and it's OK to be wrong.
It's OK to be wrong about something.
How does this relate to thought?
The exact same way.
You believe something, and you're wrong.
You believe an idea that is incorrect.
A whole bunch of people, this is the flu.
It's not a big deal.
They're wrong.
Evidence has shown that they're wrong,
but they don't want to be wrong.
So what they do is they go out of their way
to present it as if they were right.
They have to reject more information and embrace,
let's just call them alternative facts.
They have to embrace more information that's wrong.
Just like with the normal use of that term,
a capital expenditure, you end up
wasting more and more money as time goes on.
If you fall prey to the sunk cost fallacy in thought,
you become more and more wrong as time goes on.
There's nothing wrong with being incorrect about something.
Everybody is wrong about something.
Right now, an idea that you believe to be true is false.
Me too.
Everybody.
The problem arises when you don't change your opinion
based on new information.
New information should always change your opinion.
Every time you get a piece of new information,
you should reevaluate what you believe.
Sometimes it will reinforce it.
You'll say, ha, I was right.
And sometimes you'll go, I guess I was wrong about that one.
And that's OK.
It's OK to be wrong.
The reason I think we need to stress this
is because it takes place in business.
It takes place in thought.
And because it takes place in thought,
it takes place in politics.
There's a whole bunch of people who believed
the president was something.
They went out and they campaigned for him.
They argued for him on Facebook.
Then he got elected.
Turned out he wasn't that person that they thought he was.
But they don't want to admit that.
Instead, they become more wrong as time goes on.
They don't want to admit their initial investment
of their faith in this candidate was bad.
So they invest more and more, wasting more and more.
How many times have you talked to somebody who was a Trump
supporter and you're like, well, he said this.
He never said that.
Here's the quote.
OK, well, he said that, but that's not what he meant.
Well, this is what he did.
Well, you just don't understand his grand plan.
That is somebody that has fallen prey to the sunk cost fallacy.
That is somebody who is rationalizing everything,
everything that comes their way that speaks out
against dear leader.
We need to remind everybody that it's OK to be wrong,
because we are going to be wrong.
Because we are going to be going through some things
as a country.
We don't need this man at the helm.
He's incapable of leadership.
If you go back through these videos,
I attack his ideas a lot, a lot over the years.
You rarely see me attack his character.
You rarely see me attack his character,
because he was never tested.
And I was always willing to give him the benefit of the doubt
that when the time came, if the time came,
he could rise to the occasion.
The time came, and he couldn't.
And contrary to his little media spectacles
that he keeps putting on, we're nowhere near the end of this.
He's going to try to brand his way out of it
to keep those people who are invested in that sunk cost
fallacy.
He's going to try to keep them on his side.
We've got to find a way to let them know it's OK to be wrong.
And it's OK to change your opinion when new information
becomes available.
Because as we move forward, eventually, we
are going to be dealing with the economy.
He couldn't lead, given all the tools and all the templates
he had available.
He couldn't just follow a simple plan.
I'm not sure the man with multiple bankruptcies
is the guy to lead us out of what we're headed into.
Because he himself falls victim to sunk cost fallacy.
He doesn't want to admit he's wrong.
So he is slow to walk things back.
We need to be responsive.
We need responsive leadership.
We need articulate leadership.
We need leadership that is actually going to lead.
And it's not anybody with the last name Trump.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}