---
title: Let's talk about religious facilities right now....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9R_pLBRXmEw) |
| Published | 2020/04/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Acknowledges people pausing or canceling Patreon support, assures no need for explanations.
- Raises concern about religious leaders holding congregations despite risks in the current climate.
- Advocates for livestreaming religious services as a safer alternative to in-person gatherings.
- Points out that people tend to let their guard down in religious facilities, engaging in risky behaviors.
- Questions the extreme test of faith in expecting divine protection during a pandemic.
- Suggests using livestreaming platforms like YouTube for congregations to avoid physical gatherings.
- Urges people to wash hands, avoid touching faces, and stay home to follow best practices.
- Stresses the importance of not needing government orders to understand staying home is necessary.

### Quotes

- "You owe me precisely zero explanation, none."
- "Livestream. Livestream."
- "In a moment of doubt, that's when it overcame them."
- "Please do your part. Wash your hands. Don't touch your face. Stay home."
- "You know what best practices are."

### Oneliner

Beau raises concerns about risky congregational gatherings, advocating for livestreaming services and following best practices during the pandemic.

### Audience

Religious Leaders

### On-the-ground actions from transcript

- Livestream religious services to avoid risky in-person gatherings (exemplified).
- Wash your hands, avoid touching your face, and stay home (exemplified).

### Whats missing in summary

Importance of prioritizing community safety and well-being over traditional practices during challenging times.

### Tags

#ReligiousLeaders #Livestreaming #PandemicSafety #BestPractices #CommunitySafety


## Transcript
Well howdy there internet people, it's Beau again.
Before we get started tonight,
I want to put something out there
because I've gotten a few messages from people
explaining why they've had to pause
or cancel their Patreon right now.
You owe me precisely zero explanation, none.
It should not be a concern.
Everybody right now is going to go through hard times.
I take no personal offense to it,
you do not owe me an explanation.
Okay, so now on to the actual topic at hand,
which may be related in some way.
We're going to talk about congregations,
religious services, something we do not normally talk about
on this channel because I believe people's religious beliefs
aren't really up for public debate.
But we need to talk about religious services right now
in this current climate because I'm noticing
more and more religious leaders say
that they're going to have services,
especially here in Florida because in the emergency order
there is a written-in exemption for it.
It's an exemption, it's not a requirement.
You don't have to do it.
And it is risky.
It is risky.
There are other alternatives.
For those religious leaders who feel that it is an imperative
that they absolutely must have fellowship
or reach out to their flock, I get it.
Good for you.
I get it.
Livestream.
Livestream.
If you hold them in person, if you are good at what you do
and you have fostered a sense of community and family
within your facility, people are going to let their guard down.
People are creatures of habit when they show up at that facility.
They are going to do what they normally do.
They are going to shake hands.
They're going to try to keep their voices low.
So they're going to lean in and whisper.
Whole bunch of things happen in religious facilities
that are not best practices right now.
And that can be avoided via livestream.
And I know there are some out there who would say,
well, the higher power, God, will protect.
Okay.
Okay.
But unless your doctrine includes taking up serpents,
this may be a bit of an extreme test of faith.
And what happens when God's plan includes one or many of your flock dying?
What are you going to say?
Didn't have enough faith?
In a moment of doubt, that's when it overcame them.
That's why you always have to have faith.
I am not sure that your congregation will accept that.
Livestream.
There are alternatives.
There are other things that can be done.
And I want to address the elephant in the room.
And I want to be clear about this before I say it.
This is some, not all, religious leaders.
But many have a large facility that they have to pay for.
When you pass that plate, everybody's touching it.
If that's your concern, livestream.
Stream on YouTube.
People can make contributions that way.
I don't think it is a good idea to pack people that tightly together in an enclosed space
where if you've done your job, they feel like family.
DOD's ordered 100,000 bags for people.
Please do your part.
Wash your hands.
Don't touch your face.
Stay home.
We shouldn't have needed a federal government order or a state government order telling
us that best practices right now are to stay home.
Even if there is an exemption for religious services, you don't need them to order you
to stay home.
You know what best practices are.
Anyway it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}