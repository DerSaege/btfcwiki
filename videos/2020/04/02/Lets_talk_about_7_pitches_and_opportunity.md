---
title: Let's talk about 7 pitches and opportunity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xhQLU212qFE) |
| Published | 2020/04/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts a significant baseball game from 1931 involving a 17-year-old pitcher named Mitchell facing Babe Ruth and Lou Gehrig.
- Mitchell struck both Ruth and Gehrig out, a rare feat that doesn't happen often.
- Draws a parallel between hitting a home run in baseball and achieving success in life.
- Mentions that during tough times, like the current economic situation, there are opportunities for those on the lower end of the income spectrum to rise.
- Encourages utilizing the current period to figure out goals and make concrete plans for future success.
- Shares anecdotes of friends who found financial success during a recession by planning ahead.
- Urges viewers to identify what they want and strategize how to achieve it, especially during economic downturns.
- Emphasizes the importance of being prepared and ready to take action when the economy improves.
- Suggests starting small and growing gradually, taking advantage of competitors' weaknesses and customer dissatisfaction.
- Calls for viewers to be proactive in pursuing their goals and dreams, leveraging the current downtime to plan for future success.

### Quotes

- "She, 17-year-old. That is a once in a lifetime thing right there."
- "Most people get struck out a whole lot more than they hit a home run."
- "As we come out of this is the time to make your move."

### Oneliner

Be prepared, plan ahead, and seize opportunities to achieve success, even during tough times.

### Audience

Entrepreneurs, dreamers, planners.

### On-the-ground actions from transcript

- Identify your goals and what you want to achieve during this downtime (suggested).
- Develop a concrete plan outlining steps you can take to reach your goals (suggested).
- Be proactive in preparing for future opportunities as the economy improves (implied).

### Whats missing in summary

Practical examples of how individuals can strategically position themselves during tough times to achieve success in the future.

### Tags

#Baseball #Success #Planning #Opportunities #EconomicDownturn


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a baseball game.
Kinda.
Not really.
We're going to talk about seven pitches of a baseball game that occurred in 1931 on April
2nd.
There's a lot that can be pulled from those seven pitches.
So there's a 17 year old signed to this minor league team, last name Mitchell.
This team is going to play an exposition game against the New York Yankees.
Now Mitchell's the relief pitcher and the first inning, Mitchell gets called in.
And who's coming up to the plate?
Babe Ruth, followed by Lou Gehrig.
Now for those overseas who know nothing about American baseball, this is a challenge.
These are huge names.
And first pitch, ball.
Second pitch, strike.
Third pitch, strike.
And then Jackie Mitchell struck out Babe Ruth.
And then she struck out Lou Gehrig.
She, 17 year old.
That is a once in a lifetime thing right there.
That is something that does not happen to people.
They don't get it on their first shot like that.
That is not, that's not normal.
Most people are more like Babe Ruth.
Babe Ruth is known as the king of home runs.
Also the king of strikeouts.
Was struck out almost twice as many times as he hit a home run.
In fact, held the career record for most home runs for 29 years.
So Mickey Mantle, another great American baseball player, took the title.
So there's something to be said for that.
Some people, well they get lucky.
Right out of the gate as a teenager, they are striking out Babe Ruth and Lou Gehrig
in one game.
That's cool.
But most people get struck out a whole lot more than they hit a home run.
Most people have to keep coming up to the plate.
This current mess that we're in is going to present an opportunity for a lot of people
who are at the upper end of the low income spectrum to come back up to the plate.
The economy turning the way it is, there's going to be a lot of opportunity.
But you have to be ready for it.
Luckily, most people have a lot of time on their hands right now.
And yeah, it's stressful and there's a lot going on.
But it might be time to figure out what you consider a home run, what you want, and figure
out how to get it.
Figure out what steps you can take now.
Come up with a plan because when the economy kicks back off is the time to make your move.
I've talked about it before in other videos.
I have a lot of friends who went from barely scraping by to six figures during the last
recession right as they were coming out of it and they all planned it during the recession.
So if you have the time, which most of us do, you might want to figure out what it is
that you actually want and how you can achieve it.
What you're looking for.
Figure out the goals and then come up with a concrete plan.
Most of these guys that I know, they started off real small because they didn't have a
lot and it grew because their competition had overextended.
Their customers were unhappy.
So there was that magical moment when they could make their move.
Somebody is going to do it.
I would prefer it be the people who watch this channel because I would prefer people
with the mindset of those who typically watch this channel to become the people with some
means.
So I know that's vague.
It's not concrete but I don't know everybody.
I don't know what your version of a home run is.
But you do.
You've got a good idea of what you want and you've got a good idea of what you're good
at.
As we come out of this is the time to make your move.
Anyway, it's just a thought. I have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}