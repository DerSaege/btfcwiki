---
title: Let's talk about what a naming tradition can teach us about listening....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tRKzhKrxIIg) |
| Published | 2020/04/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of intent in life and how it should be measured, even though the effect is usually more significant.
- Talks about the naming traditions in the US military for ships, tanks, aircraft, drones, and helicopters, and how they are usually named after things that embody importance or honor.
- Addresses the naming tradition of helicopters in the Army, generally named after native groups or leaders, and clarifies that it was never meant to mock natives.
- Shares the historical context behind naming helicopters after native groups, like the Sioux, and how it was meant to acknowledge their speed, surprise, and violence of action from horseback.
- Emphasizes that the naming traditions are intended as a huge honor when it comes to people, not as insults.
- Mentions that not all natives are against this naming tradition and that some see it as a significant honor.
- Suggests that the opinion on whether this tradition should continue should be left to those impacted by it.
- Encourages understanding the perspective of others in a debate or discussion and listening while talking to potentially reach a resolution.

### Quotes

- "Intent is really important in life."
- "Was never meant to be an insult."
- "We may be closer to a resolution than we imagine if we actually listen while we talk."

### Oneliner

Understanding naming traditions in the military sheds light on honoring rather than insulting, urging consideration of intent and perspectives in debates.

### Audience

Military members, activists, historians

### On-the-ground actions from transcript

- Respect and honor the perspectives of those impacted by naming traditions in the military (implied)
- Engage in open, respectful dialogues to understand differing viewpoints (implied)

### Whats missing in summary

The full transcript provides a deeper historical context and nuanced understanding of the naming traditions in the US military, which can further inform perspectives on the topic.

### Tags

#NamingTraditions #Military #NativeGroups #Debate #Honor


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're gonna talk about what's in a name.
We're gonna talk about what a naming tradition
and a discussion about a naming tradition
can teach us about our own discussions
and how sometimes we fail to make an accurate judgment
about where the other person is coming from
and how if we're a third party to a discussion,
we can project our views into that discussion
and come away with a completely inaccurate read.
Intent is really important in life.
I mean, the effect is normally more important,
but intent is something that should be measured.
There's a discussion that takes place a lot
in academic circles, specifically among liberal academics,
and it deals with a naming tradition.
And the way it gets framed assumes two things.
One, that an entire demographic thinks the same way,
and a complete misunderstanding of another culture.
Doesn't sound like something that would happen
in liberal academic circles, right?
But it does.
Okay, so the US has naming traditions.
What do we name our ships after?
Important places, people, important battles,
important ideas, important being the key thing here.
Important ideas, in case you're wondering,
like mercy, comfort.
Those are two ships people probably have
at the front of their mind right now.
What about our tanks?
Abrams, Bradley, Sherman, important military leaders,
people to be honored.
What about our fixed-wing aircraft?
Falcons, Raptors, Eagles,
something that embodies the piece of equipment,
birds of prey.
Makes sense.
Makes sense.
Even drones, and drones are a good example
because it shows that it's not just a broad category
that gets slapped onto a type of equipment.
You have the famous ones, Predator, Reaper,
very accurate names for those things.
You also have the Hawk, the Global Hawk.
That name's not like the other ones
because it has a different mission.
It's a surveillance platform.
Flies around and looks down.
Makes sense.
What do we name our helicopters after?
Now, this is generally speaking.
There are exceptions to this,
but the Army normally names helicopters
after native groups or native leaders.
There is a lot of discussion about this,
a lot of discussion about this.
To understand where it comes from,
we need to go back to when it started
because there's this idea that it was,
it's a way of mocking natives.
It's not.
It was never meant to be like turning them
into a cartoonish mascot.
I'm going to paraphrase General Howells right now.
He took over when the US Army had two helicopters,
and he's basically like,
you named it the Dragonfly and the Hoverfly.
Kind of boring names for the future of warfare.
This is going to allow us to show up from out of nowhere,
hit our opposition, and then disappear.
It's going to change everything.
We're going to be able to emulate the tactics
of the Plains Indians.
The next helicopter was named the Sioux.
From the very beginning, it was an acknowledgment
that it took 100 years and a flying machine
for the US military to deliver the same speed,
surprise, and violence of action
that natives could deliver from horseback.
Never meant to be an insult.
And the names are actually more fitting than you might imagine.
It's not like the Army gets a new helicopter,
and they're like, slap a native name on it.
It doesn't work that way.
Most people are familiar with the Black Hawk
because of the movie Black Hawk Down.
In that movie, what is the helicopter doing?
Taking warriors on a raid, right?
Black Hawk was a native leader.
One of the things he was famous for, taking warriors on raids.
Chinook, huge aircraft, two rotors up top,
creates a whole lot of wind.
Might be interesting to Google and see
the relationship between the word Chinook and wind.
If you had a piece of equipment whose entire job was
to show up from out of nowhere and destroy
cav units, armored cav, but still, Apache
would be a pretty fitting name.
Never meant to be an insult. You can
look at the other naming traditions
and realize that when you are talking about people,
it's always intended to be a huge honor.
The other side of this is the idea
that all natives are against this.
That's not true.
Natives do not all think the same way.
It's worth noting the most recent addition
to this long tradition is the Lakota.
And when that helicopter kind of made its debut,
the tribe from Standing Rock, representatives
from that tribe were on hand to celebrate and bless it.
It's not, this discussion isn't really
taking place in the way it gets framed from outside.
We're projecting a lot of our own opinions
onto this discussion.
There are a lot of natives who are in the military who
understand you really don't get much higher of an honor
than having a major piece of equipment named after you.
So not all natives are opposed to this.
Some are, absolutely.
And whether or not this tradition should continue,
that's certainly a valid discussion.
We're entering a new age where it's
less than appropriate at times.
However, I myself would just cede my opinion
and give my vote to the people who
are impacted by it, by the people who that equipment is
being named after.
I wouldn't try to force my view on it.
And I've seen a lot of really smart people
fall for the idea that the US military is mocking natives
and naming their equipment after people that they've
conquered as an insult.
Was never meant to be that way.
Intent is important.
And understanding where the other person is coming from
is important.
We should remember that.
Because there are a lot of times when
we get involved in a discussion, we
are passionate about something, that we
forget to take into account where the other people are
coming from.
And we may be closer to a resolution
than we imagine if we actually listen while we talk.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}