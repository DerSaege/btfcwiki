---
title: Let's talk about a supply chain talking point....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LxTmhbFLoYA) |
| Published | 2020/04/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes the idea of bringing all production back home to fix the supply chain, labeling it absurd and a talking point pushed by those in power.
- Explains that the current supply chain issues are due to global problems and increased demand, not evil foreigners as suggested by the talking point.
- Argues that decentralization and distributed networks usually improve systems, pointing out the flaws in isolationist approaches.
- Notes that localized supply chains can lead to vulnerabilities, as seen in the struggling meat packing industry with ripple effects on ranchers.
- Mentions the predicted shortages that have now occurred due to lack of preparation and blames the need for scapegoats on those in power.
- Compares globalists who want one jurisdiction for the world to nationalist governments, stating that the difference is only in scale.
- Advocates for redundancy, decentralization, and distribution of networks as beneficial, contrasting this with the weakening effects of isolationism and nationalism.
- Emphasizes the importance of strong community networks over relying on state and federal governments for protection.
- Concludes by warning that not learning from past mistakes will result in more vulnerabilities and situations like the current supply chain issues.

### Quotes

- "If you have one, you have none."
- "A nationalist is just a low ambition globalist."
- "We can't rely on governments to make sure that we're all okay."
- "It's present in a lot of other videos but I've never really gone into it in this way."
- "It's very clear that we haven't learned anything from this."

### Oneliner

Beau debunks the absurdity of isolationist supply chain fixes, advocating for decentralization and community resilience over nationalist ideologies.

### Audience

Community members, Activists

### On-the-ground actions from transcript

- Build strong community networks for resilience (implied)
- Advocate for decentralized systems in your community (implied)
- Challenge nationalist ideologies with facts and logic (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the flaws in isolationist supply chain fixes and the importance of community resilience in combating global issues.

### Tags

#SupplyChain #Decentralization #CommunityResilience #Nationalism #Globalism


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about a talking point that is gaining ground.
It's about the supply chain and the way we need to fix it according to the talking point
is to bring all production home and just rely on what we have here in the United States.
Therefore we don't purchase anything from overseas, therefore our supply chain is more
secure.
Every once in a while a talking point comes out and becomes accepted that is so absurd
that it makes you realize the American people will believe almost anything those in power
tell them to believe.
Let me just start by saying there are very very few things in the world that are not
made better by distributed networks and decentralization.
Just start with that.
But let's run through this little thought experiment.
Right now the reason we're having trouble getting supplies is because it's a global
problem and facilities all over the world have been impacted and there's increased demand
and that's one of the reasons that increased demand is one of the reasons this talking
point has surfaced but we'll get to that.
That's the reason.
It's not evil foreigners, it's increased demand and lower production because all of these
facilities were impacted.
If we bring it home it no longer takes a global problem to disrupt the supply chain.
It can be incredibly localized just at that production facility.
Now can we have a facility here too?
Yeah!
To complement the other ones.
We still have to purchase stuff from over there to keep them running so those facilities
are large enough to deal with the demand if ours goes down.
Redundancy is always good.
If you have one you have none.
Bringing everything home and becoming an isolationist makes our supply chain more vulnerable, not
less, because a small incident can disrupt it rather than it taking a worldwide one.
Don't believe me?
How's the meat packing industry doing right now?
Pretty bad, right?
And that's having a ripple effect.
Ranchers are having issues now because of the bottleneck at the meat packing plants.
That's what happens if you have a localized supply chain.
Now luckily we have redundancy for that.
So there's meat being imported.
If we didn't have that we'd have an even bigger problem right now.
This talking point has surfaced not because it's true, but because those in power need
someone to blame.
And who better than those others?
Those foreign people.
Those people that Americans just love to scapegoat.
Those in power knew this was an issue.
The shortages that are occurring now and have been occurring, they have been predicted for
ten or fifteen years.
We all knew it was a possibility.
We knew this could happen at any moment.
And it will happen again.
The more we encroach on unspoiled areas and the warmer the climate gets, the more often
this is going to happen.
They didn't act.
They didn't get the supplies that we needed before we needed them.
They didn't prepare so they need a scapegoat.
And they know they can count on that base that has been conditioned to hear globalist
as some evil term to buy into it because that nationalism is running rampant.
You're not familiar with the whole globalist thing?
It boils down to this.
There's a group of people out there who want to bring the entire world under one jurisdiction.
That's the idea.
And that small group of people, well they'll make decisions for everybody.
If you don't follow their rules, well you'll have violence visited upon you.
Yeah man, that sounds horrible.
I would not want to live in a place like that.
Please explain to me though the difference between that and a nationalist government.
You can't.
Because the only difference is scale.
A nationalist is just a low ambition globalist.
I personally think there are better ways to run the world.
I don't think that we need to entrust a small group of people to make decisions for everybody.
You can run your life better than they can.
But that's why this talking point is being believed even though it can't pass a basic
logic test.
Redundancy, decentralization, distribution of networks is always good.
There are very, very few things in the world, I honestly can't think of one off the top
of my head, that don't benefit from it.
We can play the isolationist nationalist game all we want.
It's just going to make us weaker.
It will make us weaker.
It will make us less capable of responding to situations like this.
And there will be more of them because we've learned nothing.
It's very clear that we haven't learned anything from this.
That the government hasn't learned anything from this.
Because we're not even through this one.
It really seems like they're trying to make it worse.
We're probably going to revisit that topic, if you have one, you have none, a whole lot
this month.
It's present in a lot of other videos but I've never really gone into it in this way.
You've heard me talk about redundant power structures.
If you have one, you have none.
If your community network is strong enough, it doesn't matter what happens at the state
and federal levels, right?
Heard me say that.
I've said it a lot.
This is why.
This is why.
We can't rely on governments to make sure that we're all okay.
It's not what they do.
A government is there to control you.
It's not there to protect you.
And falling in line and becoming more dependent at a national level is just going to make
us weaker, not stronger.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}