---
title: Let's talk about preparing for the next time (Part 1)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BALZGFZ_yxs) |
| Published | 2020/04/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Encourages preparedness after recent events, stressing the inevitability of future crises.
- Acknowledges the challenge of preparing for multiple people with varying needs and limited space.
- Recommends purchasing a pre-packed bag for efficiency when prepping for a larger group.
- Emphasizes the importance of a comprehensive medical kit beyond basic first aid supplies.
- Advises obtaining medication from the dollar store due to affordability and variety.
- Suggests researching the Shelf Life Extension Program for medicine potency over time.
- Recommends an Everyday Carry (EDC) kit for basic essentials always on hand.
- Advocates for investing in durable, affordable knives for practical use.
- Mentions the relevance of a fishing kit and storing seeds for sustainability.
- Proposes collapsible water containers for compact water storage in small living spaces.

### Quotes

- "They want to be ready for the next event. There will be another one, there will always be another one."
- "Preparing ahead of time, stockpiling stuff, getting ready. However, that requires space."
- "You can kit somebody up completely for that."
- "You have to actually go and read this stuff because it doesn't apply to all medicine."
- "You don't have to have them around the whole time for taking up space."

### Oneliner

Be prepared for future crises by investing in essentials like medical kits, water containers, and tools for under $600.

### Audience

Preparedness Enthusiasts

### On-the-ground actions from transcript

- Purchase a pre-packed bag for efficiency in emergency preparedness (implied).
- Obtain a comprehensive medical kit beyond basic first aid supplies (implied).
- Research the Shelf Life Extension Program for medicine potency over time (implied).
- Invest in durable, affordable knives for practical use (implied).
- Store seeds for long-term sustainability (implied).

### Whats missing in summary

The full transcript provides detailed guidance on budget-friendly, practical emergency preparedness strategies, covering medical kits, water storage, tools, and more.

### Tags

#Preparedness #Emergency #MedicalKits #WaterStorage #Tools


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about being prepared.
There are a whole lot of people who want to get prepared now
after what has recently happened.
They want to be ready for the next event.
There will be another one, there will always be another one.
If you have any interest in this topic,
I strongly suggest you watch this video.
We're gonna go into a lot more detail than we have before.
And I have props today.
This started because somebody on the live stream was like,
hey, I want to take my stimulus and get prepared.
Like awesome, send me your details,
tell me about your personal situation
and we'll turn it into a video, we'll get you prepped up.
Because $1,200 for one person, that's awesome.
You can kit somebody up completely for that.
$1,200 for six people, well that's a challenge.
And this is why YouTube videos on this subject
are not particularly helpful most times
because they do not take into account
your personal situation.
They are made generally for people like me,
people that have outdoor skills, people that have training,
people that are more comfortable with it.
There aren't many made for the average person.
So the details come in and yeah, it's six people, not one.
That's a big difference.
There are two schools of thought
when it comes to getting prepared.
One is skill-based.
That is you learn a whole bunch,
therefore you don't need much.
That's normally for young, fit, single people.
As you get a house full of kids,
you realize that you may be able to survive
on next to nothing,
but you may need to prepare a little bit more for them.
And that's the other school of thought,
preparing ahead of time, stockpiling stuff, getting ready.
However, that requires space.
If you live in a small apartment,
that gets real hard and you have to get creative.
And that is the situation we are dealing with.
Somebody with little training and little space.
Yeah, that's a challenge.
That's a challenge, which makes it fun.
Okay, so in this case,
because we're talking about six people
who don't have any of this stuff,
I'm gonna do something I normally would say don't do.
I'm gonna recommend getting a pre-packed bag,
just buying one.
Normally I would suggest you pack it yourself.
However, if you have that many people,
it makes sense to just go ahead
and have some company do it for you.
I've never seen a six-person bug-out bag.
It may exist.
I have seen four-person,
and you can make do with that,
and the stuff that you know you're gonna need
additional of, you can just throw at.
A four-person bug-out bag runs around 150 bucks.
That has the canteens, matches, flashlights,
the basic stuff that you're just gonna have to have.
Everybody's gonna have to have.
It's a good building block.
It's a good starting point.
It's probably also gonna come with a cool military bag,
which you're gonna throw away.
You're not gonna use it for this,
and we'll explain why later.
So that's one big purchase.
The next one is a medical kit,
and by a medical kit, I do not mean
a $20 first aid kit from Walmart.
It's just a bunch of Band-Aids.
Good ones, I've done a video
that shows a real high-end one.
Good ones that are readily available everywhere,
the M17 and the M39 medical bags from DOD.
They are packed with everything you need
to treat an injury.
They have everything you need.
There's not much that you're gonna encounter
that those things won't help with when it comes to injury,
but that doesn't help with illness.
So the next thing you have to do
is go to the dollar store to get your meds.
You go there because it's inexpensive,
and you can get a wide variety of stuff
to treat a wide variety of illnesses.
They also tend to use pressed pills there,
not gel caps, which is important.
There's a thing, there's a study
called the Shelf Life Extension Program.
If you're going to do this,
you have to actually go and read this stuff
because it doesn't apply to all medicine,
and you need to be certain that the ones
that you're counting on fall into the category
I'm talking about here.
And this is an important note.
You have to actually read it.
You have to check your specific med.
Generally, what they have found with pressed pills,
this doesn't apply to gel caps or liquid stuff,
anything that's biological,
they don't go bad,
not in the sense that they become harmful.
They become less potent,
which means they'll still do some good
depending on the type of med.
Make sure you go read it and check it.
I can't stress that enough.
But they have found that many meds 10 years later
will still retain like 90% of their potency.
So I would read that first
and then pick the meds that match up
with that program that you can get your hands on.
Okay, the next thing is your EDC kit.
EDC means Everyday Carry.
That's like a Swiss Army knife,
multi-tool, little baby flashlight,
lighter, stuff like that.
And as the name suggests,
you should have it on you all the time,
just in case.
Okay, the follow-up is a knife.
Now, generally people like me who are outdoorsy,
we use our knives every day.
So when we recommend a knife,
it's gonna be a knife that stands up
to everyday use for years.
You probably don't need a Gerber
or a Spyderco or something like that.
You probably don't need a $300 knife
because you don't use it every day.
Okay, Camillus, this brand,
you can pick them up at most big box stores.
For a tenth of the price of one of the knives
that is normally recommended by somebody like me,
you can get three of these.
They're literally like 10 bucks a piece.
And they work, they're durable, they last.
You wanna get three of them.
If you have one, you have none
because the second you need it, it's gonna break.
So go ahead and get three.
Now that's 30 bucks.
So, so far we're at 150 for the bag,
200 for the medical kit, 20 bucks for the EDC stuff,
$30 for the knife or knives.
Fishing kit, if it is relevant to you, 30 to 40 bucks.
So you can get fish.
Then that helps not have to store as much food.
Seeds, in case things go really crazy.
You can get some that are pre-packed for long-term storage.
They'll come in like a bucket or a Mylar bag,
something like that.
The Mylar bags tend to be the best bang for the buck.
Under normal circumstances, it's like $15.
I've seen them now because of everything going crazy.
They're like 80 bucks.
They're not worth that.
They're the same seeds that you would get in a paper bag,
like heirloom seeds in any garden shop.
They're just packed differently.
If you can find it at a reasonable price,
it's probably worth having it pre-packed for you.
If not, do it yourself.
Okay, the next thing we're gonna talk about is water.
If you have a small apartment,
you can't store a whole bunch of gallons of water.
You don't have the room.
These are collapsible water containers.
They're like two bucks.
You can pick them up online.
That one I think is two and a half gallons.
They have some that are five, whatever.
But you spend 10, $15 like that, and that's it.
I mean, you fill them up when you need them
or when you suspect that you're gonna need them.
You don't have to have them around the whole time
for taking up space.
So that's getting a little creative with the space there.
In case the water coming out of your tap is bad,
you're gonna need a Sawyer or a LifeStraw
or water purification tablets.
Sawyers and LifeStraws, they work the same way.
The water goes through a filter.
The tablet, you drop the tablet in,
makes the water taste a little funny,
but it purifies it from non-chemical stuff.
And that's something you need to be aware of.
This doesn't mean that you can,
that this stuff doesn't treat runoff from industrial places.
That is just something to take note of.
People have an opinion on which is best.
I don't, I really don't.
I don't think it matters that much.
I will say that I have both.
Tablets are useful for a large party, like a group of six.
In case somebody has to venture off for a couple of days.
They may not be able to take one of the straws with them
because you may need it,
but you can tear off a strip of purification tablets.
So they are useful.
None of that stuff takes up much space.
For shelter, Mylar tents, incredibly inexpensive,
and they work.
They are crinkly, they're noisy,
and they're bright silver,
so it wouldn't be great if you're looking to lay low.
You may want to get a poncho liner or something like that
to subdue it a little bit if you want to.
We're looking at 20 bucks.
That takes care of food, water, fire, shelter,
knife, and medicine.
All of that is less than $600.
So the next thing we're gonna talk about
in the next part of the video is,
number one, why you threw that bag away,
and you're not gonna use it for this.
We're gonna talk about food stores,
the different kinds of food that you can store
for long periods of time,
and stuff that isn't mandatory,
but that you may want anyway.
All right.
So part two should be coming soon.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}