---
title: Let's talk about preparing for next time (Part 2)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tKoD9hZPbUE) |
| Published | 2020/04/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Advises viewers to watch part one before continuing with the current video for context.
- Provides detailed information on stocking up on food supplies for emergencies, including canned goods and freeze-dried options.
- Mentions the scarcity of certain emergency food supplies and the high prices on the secondary market.
- Talks about the nutritional value and drawbacks of MREs (Meals Ready to Eat).
- Suggests basic food options for being prepared, costing around $600.
- Recommends additional items like an e-tool (foldable shovel) and a ham radio for emergencies.
- Shares free resources such as downloading survival and medical manuals from archive.org.
- Warns against looking too militaristic while prepping and suggests blending in during emergencies.
- Emphasizes the importance of being prepared for future crises and not relying solely on government responses.
- Clarifies that he is not endorsing any specific brands and encourages having at least thirty days worth of food stocked up.

### Quotes

- "It's a good idea to be prepared."
- "Your goal here is to just ride out the initial wave of craziness."
- "You're going to have to take care of it yourself."
- "It's probably better to look homeless than look like somebody in the military."
- "Y'all have a good night."

### Oneliner

Beau provides detailed advice on stocking up on food supplies, additional emergency items, and free resources, stressing the importance of being prepared for future crises.

### Audience

Preppers and those interested in emergency preparedness.

### On-the-ground actions from transcript

- Stock up on canned goods, freeze-dried foods, dried rice, and beans for emergency food supplies (suggested).
- Purchase additional items like an e-tool (foldable shovel) and a ham radio for emergencies (suggested).
- Download survival and medical manuals from archive.org for free (suggested).

### Whats missing in summary

Practical tips on blending in during emergencies and the importance of self-reliance in preparing for future crises.

### Tags

#EmergencyPreparedness #FoodSupplies #SurvivalSkills #Prepping #SelfReliance


## Transcript
Well, howdy there internet people, it's Beau again.
So we are back for part two.
And if you have not watched part one, go watch it now, otherwise this is not going to make
any sense, because this has not been a normal set of videos.
Okay so if you've heard me talk about this stuff before, and I have been less specific,
when I say canned goods, you're probably picturing something like this.
If you were talking about for six people, or even for one long term, you might want
to be thinking something a little bit bigger.
Number ten cans like this, food service size.
You get more bang for your buck.
Cans like that run five to fifteen bucks, depending on what's in them.
They have a shelf life of two to three years.
You also have this as an option.
It's a number ten can, the difference is the contents is freeze dried or dehydrated.
They cost about four times as much.
The reason you may want to consider them, this can expires in 2037, and it's not new.
You have shelf life of twenty or thirty years, when you're talking about something like this.
So this is something you don't have to worry about rotating, you can get enough and just
have it set in the back of the pantry and never have to worry about it again.
Buying these right now is difficult.
They've been bought up, the companies are producing, trying to catch up.
There's a whole bunch of companies that make it.
All of these brands are different.
I'm not endorsing any particular one right now.
You can find them on the secondary market at a huge markup.
Another option, dried rice, dried beans.
A bag like this is like twenty bucks.
It has two hundred servings in it.
So it's, I mean that's very cost effective.
Has a shelf life of two to three years, if you keep it in a cool dry place, which you
should do with all this food.
Cool means room temperature.
Doesn't have to be like forty degrees.
Yet another option are the buckets, the famous buckets that get hawked on a lot of right
wing channels.
They're just like the cans, freeze dried or dehydrated.
They're a little more expensive because there's a wider variety of food in it.
You don't suffer palate burnout eating the same thing over and over again.
A bucket like that's like a hundred bucks for thirty days worth of breakfast.
Something else that people are looking for are MREs, meals ready to eat, rations from
the military.
They are also in short supply right now and the markup on the secondary market is huge.
Under normal circumstances a case should run about sixty bucks.
That's twelve of them.
I have seen single MREs going for thirty dollars.
They're not worth it.
They're not that good.
They have high nutritional value.
They have what you need to stay active.
They are not great on your insides.
And that is true for every nation's MREs that I've ever had, ever seen, anything with the
exception of the French.
French soldiers, not joking when I say this, will trade their MREs to Americans so they
can take ours home to make fun of us.
Ours have like a little wheel of cheese and a canned ham in it.
They're fantastic.
Ours are, they look like baby food and taste almost as good.
Okay so that covers your basic food options for being prepared if you want to develop
food stores.
That first list that we went through was six hundred bucks.
So if you're using your stimulus check for this you have six hundred dollars to play
with for developing food stores.
Some items that you may want that aren't necessarily mandatory.
One of them is an e-tool.
It's a foldable shovel.
You have these, they're in surplus stores.
I've actually never bought one.
I don't know what they cost.
But it's a foldable shovel, it's an axe, it's a hammer, it's a weapon, it's whatever you
need.
I doubt they're very expensive.
Something else you might want to consider is one of these.
It's a ham radio.
Understand if you don't have a license you can't use it.
You can't play with it.
However, in an emergency you may want to have one.
If you transmit on one of these and you don't have a license, it's a crime and the fines
are actually pretty steep.
But picking one of those up, they're like 30-40 bucks.
That one's made by Baofeng I think.
They're worth having around especially if you have one that allows you to program the
frequency on the front of it rather than with a computer.
You obviously might want to keep a hand crank FM radio just in case something goes really,
really bad.
Okay, so some free stuff, some free resources you may want.
Number one, if you have skills, the person we're using as an example here said that he
was decent with blue collar skills.
Make sure you have your hand tools because then you become an asset.
You become useful.
You have something that you can do.
So make sure you take those with you.
Now for those without training, you can go to archive.org and download survival manuals,
medical manuals from the military for free.
They cost nothing.
They are incredibly useful.
The military makes those manuals to be understood by everybody.
They have little drawings in them.
They're great.
They are fantastic.
You can just type in download field manual archive and it will bring you to a giant list
covering every topic imaginable.
It's a great resource.
Now in the beginning I said you're going to throw away that bag.
There is a tendency among people when they start preparing to get military stuff.
That's not necessarily a good idea.
That pre-packed bug out bag may come with like a molly bag, a military looking bag.
You don't necessarily want that.
If you're walking around and you have a plate carrier and you look all kitted up and you
run across somebody who does have a lot of training and they notice that your plate carrier
is clean, that the pouches are still stiff because it hasn't been used much and your
boots are new, they're not going to see that as somebody who knows what they're doing,
somebody who's experienced.
They're going to see that as, hey, free stuff.
It may be better to blend in, to not look like you have anything.
It's probably better to look homeless than look like somebody in the military.
Try to look unimportant.
Your opposition might be low on ammo.
That theory is called the gray man theory if you want to read into it.
But that gives everybody a rough overview with a whole lot more specifics than I've
ever done on stuff that pretty much everybody should have.
What we're going through now, it will happen again.
Your goal here is to just ride out the initial wave of craziness so you don't have to go
to the store when everybody else is panic buying because you already have the stuff
on hand.
If things go really bad, you have a good base with which to work from.
You don't have to go into building a bunker doomsday mode to be prepared and to stay safe.
There's no reason to go overboard and go crazy with this.
Your stimulus check can outfit six people.
Twelve hundred bucks.
So yeah, there's a rough overview.
I'm sure there will be questions.
I will try to answer as many as I can.
Aside from that, I really want to stress, I've made these videos before all this happened
and people are very interested in it now.
It's a good idea.
It's a good idea to be prepared.
We didn't learn anything from this.
The government learned nothing from it.
They're not going to be any better at responding to the next issue.
You're going to have to take care of it yourself.
The other thing I want to say is that I'm not being compensated by any of these brands.
I went out of my way to make sure that I wasn't showcasing any particular brand more than
another one.
It would be a good idea to at least have thirty days worth of food put back.
Once all this settles down, get it together if you have the ability.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}