---
title: Let's talk about the President's big historic plasma announcement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-oNFis8FhkQ) |
| Published | 2020/08/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump made a historic announcement to boost his image before the convention.
- Convalescent plasma is beneficial, but it's not a breakthrough or a cure for everyone.
- The plasma helps around one-third of people, which is significant but not a complete solution.
- It's vital to continue taking precautions like washing hands, wearing masks, and staying home.
- The plasma is just one tool in the toolbox until a safe treatment or vaccine is available.
- The World Health Organization predicts dealing with the pandemic for another two years.
- People should not become complacent or think the situation is resolved due to this development.
- Around 30-33% of individuals benefit from the plasma treatment.
- Emergency authorization makes the plasma more available, but it's not a drastic change.
- It's necessary to maintain caution and not let the positive news lead to lax behavior.

### Quotes

- "This isn't a game changer."
- "Don't let the president oversell something that really isn't the game changer he's making it out to be."
- "We're not even close."
- "It's good news for them. It's great news for them."
- "Just let's temper the overselling of it."

### Oneliner

President Trump's announcement on convalescent plasma isn't a cure-all, just a helpful tool amidst ongoing precautions and the need for a long-term solution.

### Audience

General Public

### On-the-ground actions from transcript

- Continue taking precautions: wash hands, wear masks, stay home (implied)
- Stay informed about developments in treatments and vaccines (implied)
- Avoid complacency and continue following health guidelines (implied)

### Whats missing in summary

The full transcript provides additional context on the limitations of convalescent plasma and the importance of maintaining caution despite positive news.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about announcements, and of course I am talking about President
Donald J. Trump's historic announcement.
We're going to talk about that and why maybe you shouldn't oversell things.
The appearance is that the President needs good news right before the convention, so
he's kind of playing this up.
And that's all fine and good, if it was just about any other kind of news.
When it comes to something like this, you can't oversell it.
If you've watched this channel, you know I'm really interested in this.
When it comes to convalescent plasma, every medical professional that I have talked to
says that this is good stuff.
It helps a lot.
However, it's not a breakthrough.
It's not a cure in any way.
At the end of the day, this is really helpful for about a third of people.
That's big.
It's good news.
It is great news.
But it's a stopgap.
It's not a cure.
We're not out of the woods.
Most importantly, don't stop taking the precautions.
Wash your hands, don't touch your face, stay at home.
If you have to go out, wear a mask.
Do all of the stuff you're supposed to.
This isn't a game changer.
This is one more tool in the toolbox to help get us through until we have a safe, effective,
reliable, available treatment or vaccine.
Nothing's going to change that.
Nothing will change that.
Until we get to that point, everything's a stopgap.
This is a good one.
This is good news.
Don't get me wrong.
However, the way the president kind of delivered the news may make people think we're done.
We're not even close.
We aren't even close.
The World Health Organization is suggesting that we're going to be looking at this for
another two years.
So don't get lax.
Don't think we're done.
This helps about 30% of people, 33% of people, something like that.
It's good news for them.
It's great news for them.
However, there's no guarantee that you're going to be part of the 33% rather than the
66%.
Still do everything you're supposed to.
Don't help this spread.
Don't get complacent.
Don't let the president oversell something that really isn't the game changer he's making
it out to be.
The emergency authorization just makes it a little bit more widely available.
But it was already pretty available.
So this isn't a huge change.
But it is good news.
And we should let him have the good news.
Just let's temper the overselling of it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}