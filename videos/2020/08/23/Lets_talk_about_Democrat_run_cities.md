---
title: Let's talk about Democrat run cities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CzanDzmqbwg) |
| Published | 2020/08/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how repeating statistics or statistic-like information can lead to false correlations being accepted.
- Points out that the President repeatedly mentions "Democrat run cities" to link Democratic policies with high crime rates.
- Notes that most cities are Democrat-run because the Democratic party appeals to urban residents.
- Mentions that looking at states, many of the top states for violent crime voted for Trump and have Republican governors.
- Refers to a company, SafeWise, that ranks dangerous cities, showing that some Republican-run cities have high crime rates.
- Counters the idea of a causal link between a party being in control and crime rates, attributing crime to poverty, income inequality, lack of education, and opportunities.
- Concludes that the President's claims about Democrat-run cities and high crime rates are baseless and meant to smear certain areas.
- Suggests the President's fixation on Chicago may stem from personal grievances rather than factual basis.

### Quotes

- "Crime is related to poverty, income inequality, lack of access to education, stuff like that. Not having opportunities, that's what has a causal link to crime. Not red or blue."
- "There is absolutely zero causal link between a party being in control and crime rates. That's not a thing."
- "This whole idea is garbage."
- "It's almost like maybe somebody he doesn't like kind of came into politics from there and he just won't let it go because he's a petty, petty man."
- "Y'all have a good day."

### Oneliner

Beau debunks the false correlation between Democrat-run cities and high crime rates, attributing crime to poverty and lack of opportunities, not political affiliation.

### Audience

Fact-checkers, Voters, Community Members

### On-the-ground actions from transcript

- Fact-check statistics and claims made by politicians (implied)
- Support community programs addressing poverty, education, and income inequality (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of how crime rates are linked to societal factors rather than political party affiliation.

### Tags

#Statistics #Debunking #Politics #CrimeRates #Poverty #Education #CommunityJustice


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about statistics and how if somebody repeats a statistic over
and over and over again, or something that sounds like a statistic, people may grow to
accept a false correlation.
I'm going to go ahead and tell you now, you've got to watch this whole video.
By somebody I'm talking about the President, and by the people who believe it I'm talking
about his base, because they tend to not fact check or critically think.
And the concept that he keeps repeating that sounds like a statistic is this whole Democrat
run cities thing.
When he says that, what he's trying to do, his intent is to establish a causal relationship
between being a Democrat run city, so Democratic policies, and high crime.
That's what he's trying to do.
There's a couple problems with this.
First, pretty much all cities are Democrat run because the Democratic party appeals to
people who live in cities.
Republicans tend to appeal to those who live in rural areas.
You can check this for yourself by pulling up any electoral map, looking at the counties.
The counties that voted blue are where major cities are.
But if this concept was true, that Democratic policies led to high crime, it would be true
of states as well.
Because if you're just looking at cities, of course it's going to be Democrat, because
almost all cities are Democrat, but states are different.
And at the very least, you would expect to find, like Illinois, because he's always talking
about Chicago, you would expect to find Illinois in the top five, at least the top ten, right?
So what happens if we look at that?
2018 violent crime data for states, top ten, Alaska, New Mexico, Tennessee, Arkansas, Nevada,
Louisiana, Alabama, Mississippi, South Carolina, and Arizona.
All but two of these voted for Trump.
All but three have Republican governors.
Weird.
But see, that only tells part of the story.
What we'd have to do is find a deviation.
Have to find something that is outside of the norm.
So what we'd need to do is look for cities, look at the top five cities, and see if they
line up with the states.
That'd be a good idea.
So there's a company called SafeWise, a security company, and they put out a product that ranks
the most dangerous cities using the same DOJ data set, and they use a methodology of violent
and property crime.
So number one, Anchorage lines up with Alaska.
Number two, Albuquerque lines up with New Mexico.
Number three is Memphis, lines up with number three, Tennessee.
Number four is Wichita, doesn't line up with Arkansas.
Number five is Lubbock, doesn't line up with Nevada.
Interesting thing about Wichita and Lubbock, they're Republican run, they have Republican
mayors.
So the deviation here appears to show that Republicans actually have higher crime rates
in states and in cities because they're the deviation.
What about Chicago?
Illinois is ranked 19th, right behind South Dakota.
And Neighborhood Scout, which is a real estate type thing, they use violent crime because
I don't want to just use violent and property crime because people say that'll throw it
off.
So if you just use violent crime, which is what the president tends to focus on when
he's talking about Chicago, it's worth noting that Chicago is 64th behind a whole bunch
of towns you wouldn't really expect.
So what did we learn here?
That the problem is actually Republicans, right?
No, no, not really.
There is absolutely zero causal link between a party being in control and crime rates.
That's not a thing.
He's just made that up.
I put this together by using violent crime rates for the state, violent and property
for the city, and then violent again for Chicago.
If you're not comparing apples to apples, it doesn't work, so don't cite this.
This is just an illustration to prove that it's garbage.
This whole idea is garbage.
Crime is related to poverty, income inequality, lack of access to education, stuff like that.
Not having opportunities, that's what has a causal link to crime.
Not red or blue.
That's not a thing.
Doesn't really exist.
This is just more worthless tweeting from the president.
There's no basis for it.
It's just him attempting to smear Chicago.
Why he would have an issue with Chicago, I don't know.
It's almost like maybe somebody he doesn't like kind of came into politics from there
and he just won't let it go because he's a petty, petty man.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}