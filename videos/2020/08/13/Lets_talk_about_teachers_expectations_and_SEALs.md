---
title: Let's talk about teachers, expectations, and SEALs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=K0_HK9Ma2gc) |
| Published | 2020/08/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor DeSantis stumbled into a good idea, urging teachers to be like seals in the Martin County school system.
- Teachers need proper training, not just guidelines on infection control, to provide in-person instruction.
- Training should be hands-on, rehearsals should be done repeatedly until teachers can handle situations in their sleep.
- Teachers need an inexhaustible budget for supplies, tools, training, and support.
- They should have access to experts, advisors, and total information awareness for student welfare.
- Expecting teachers to be like SEALs without proper resources is risky and unrealistic.
- SEALs train for years to improvise in the field; teachers are trained to teach, not manage public health crises.
- The comparison between teachers and seals is empty rhetoric if teachers aren't given necessary support.
- Treating teachers like SEALs without proper training risks their lives without recognition.
- Beau questions the sincerity behind the rhetoric of treating teachers like SEALs for motivation.

### Quotes

- "Expect our teachers to behave like seals."
- "Treating teachers like SEALs right now is risking their life."
- "This is a little outside of their scope."
- "They won't even get a flag."
- "It's just a thought."

### Oneliner

Governor DeSantis compares teachers to seals, but Beau questions the practicality and risks without proper training and support.

### Audience

Teachers, Education Boards

### On-the-ground actions from transcript

- Provide teachers with hands-on training on infection control (implied).
- Ensure teachers have access to necessary supplies, tools, and resources for effective teaching (implied).
- Establish a support system with experts available 24/7 for teachers (implied).

### Whats missing in summary

The full transcript provides detailed insights into the challenges teachers face when compared to seals and the need for adequate support and training.

### Tags

#Teachers #Training #Support #Education #PublicHealth


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about teachers.
Florida, Governor DeSantis, expectations, and seals.
Governor DeSantis stumbled into a pretty good idea,
to be honest, he did.
I don't know who came up with it,
how this came out of his mouth, but it is a good idea,
I'm gonna give credit where credit is due.
See, he was giving a little speech,
trying to rally the troop, I'm sorry,
trying to motivate teachers, he said this to teachers.
Just as the seals surmounted obstacles
to bring OBL to justice, so too will the Martin County
school system find a way to provide meaningful choice
of in-person instruction or continue distance learning.
All in, all the time.
Yes, that is what I'm talking about,
I can get behind this, this fantastic idea.
We need to expect our teachers to behave like seals.
This is great, I am with this, and I know some of you
think I'm being sarcastic right now, but I am not.
This is fantastic, I am 100% on board with this.
Expect our teachers to behave like seals.
But that means we gotta treat them like seals, right?
So when's our training start?
Because if we're treating them like seals,
we can reopen schools in 2023,
because that's how long it takes.
How much training do teachers get on infection control?
And by training, I mean real training, hands-on training,
the kind that makes you the best at your job,
not some guidelines posted to a website,
not a Zoom meeting or a 40-minute briefing, real training.
So they can get up to speed.
And then once they're up to speed,
that's when the rehearsals start,
and they practice over and over and over again, no students.
But they continue to practice until they can do it
in their sleep.
I'm assuming that they will have a virtually inexhaustible
budget, anything they want, anything they need, they get.
No more posting to Facebook or Twitter,
asking for supplies because the government won't pay for them.
Doesn't even have to be anything they need, really.
Just something that might make their job easier,
might make them more effective,
two teachers leaned up against lockers.
You know, Widget X would really help out
in this kind of situation.
And then poof, like magic, five of Widget X
show up so they can test it out.
You have an entire sub-agency dedicated
to developing new and better tools for them, right?
They're going to have access to the best advisors, experts
in every field that they can call 24 hours a day
to ask any question that they may have.
They're going to have an entire apparatus backing them up,
making sure they have the best tools, the best training,
the best equipment, the newest information.
They have total information awareness.
They're going to know where every asset on that field,
I am sorry, every student in that school and their status,
they're going to know that all the time.
Because if you're going to have those kind of expectations,
you have to give them the tools and resources, right?
To suggest teachers behave like SEALs without that,
that doesn't seem like a really good idea to me.
Seems like something that might cause problems.
SEALs can improvise in the field as well as they do,
as effectively as they do, because they trained for it
for years.
Teachers trained to teach, not manage a public health issue.
This is a little outside of their scope.
You want to do this?
I am all in, all the time.
But I don't think you do, because the only way you're
treating teachers like SEALs right now
is you're risking their life.
They won't even get a flag.
This little speech, it was great rhetoric.
And that's what it was, empty rhetoric,
something to motivate people, half a league, half a league,
half a league onward.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}