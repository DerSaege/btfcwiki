---
title: Let's talk about the Post Office and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8EaA2Zt2vDw) |
| Published | 2020/08/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is attempting to subvert the post office to influence the election outcome in his favor.
- The post office is explicitly mentioned in the Constitution under Article 1, section 8, showcasing its significance.
- Unlike other government services, the post office is not meant to turn a profit; it is a vital government service.
- The idea that the post office should be profitable is a misconception pushed to pave the way for privatization.
- Trump's actions regarding the post office are part of his pattern of trying to manipulate and cheat in elections.
- There should be strong opposition to Trump's attempts to interfere with the election through the post office.
- Trump's behavior is reflective of prioritizing personality over policies and party over principles.
- Americans must question if they are tired of Trump's disregard for the Constitution and the rule of law.
- It is imperative to move away from supporting individuals or parties blindly and focus on policies instead.
- Ultimately, the call is for Trump to be removed from power to safeguard democracy.

### Quotes

- "The post office is explicitly mentioned in the Constitution."
- "If you think the post office is supposed to turn a profit, you have been tricked."
- "This is Trump trying to rig the election, trying to cheat."
- "Trump needs to go."
- "This is what happens when you have a cult of personality."

### Oneliner

Trump's attempt to manipulate the post office to sway the election reveals a dire need for Americans to prioritize democracy over personality and remove him from power.

### Audience

Concerned American citizens

### On-the-ground actions from transcript

- Contact Congress members to push back against attempts to undermine the election (exemplified)
- Advocate for protecting democracy by raising awareness about the importance of upholding the rule of law (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's intentions regarding the post office and the implications for democracy, urging Americans to focus on policies rather than personalities to safeguard democratic values.

### Tags

#PostOffice #Trump #ElectionInterference #Democracy #Constitution


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the post office and Trump because we have to.
He's trying to subvert the post office so he can undermine the election and tamper with
it, adjust the outcome so it's more beneficial to him.
He's rigging the election.
The same thing he's whined and cried about this entire time because that's what he does.
Pretty much anything he accuses Democrats of is something he's actively attempting to
do at the time.
There's no reason to go through all of the normal stuff.
Let's just hit the high points here.
The post office is one of the few entities explicitly mentioned in the Constitution.
Article 1, section 8 says that Congress has the power to establish post offices and roads.
I would suggest that those who are against the post office, who are okay with this move,
I would suggest that they look and see what else is explicitly named because there aren't
many things.
For example, why don't you flip through the Constitution looking for the explicit mention
of an immigration agency because it's not there.
That's how important the post office is.
It is a government service.
That's why it's everywhere.
That's one of the other things.
It's not turning a profit.
It needs money.
If it was successful, it wouldn't need funding.
Like the Department of Defense, fire departments, schools, all other government services.
This concept that it needs to turn a profit and it needs to be able to pre-fund all of
this stuff, this was an obstacle that was thrown up so eventually the post office would
have the image that it has now where it's ineffectual.
So it could be privatized.
UPS FedEx could take over and make that money.
That's the reason it's there.
It's a talking point.
You have been duped.
If you believe this, if you think the post office is supposed to turn a profit, is supposed
to be this highly profitable entity, you have been tricked.
It is a government service.
That is why it is explicitly mentioned in the Constitution.
This is what it is.
It shouldn't be a surprise to anybody.
This is Trump trying to rig the election, trying to cheat.
Fraudulent elections are the 14th characteristic of his ideology.
So of course he's going to try to do it.
Yeah, there should be strong pushback from Congress, from everybody about this.
This is an overt attempt to tamper with the election.
It is what it is.
The question is now are enough Americans tired of him shredding the Constitution?
Are there enough Americans tired of him trying to subvert the rule of law?
Are there enough Americans who want him gone?
That's where we're at.
We can expect this kind of behavior from Trump from now until the election and probably some
afterward.
This is what happens when you have a bunch of people call for a cult of personality and
focus their energy on backing a person rather than backing policies.
Backing a party instead of backing policies.
This is what happens.
This is what occurs.
It's something we should avoid in this country.
Trump needs to go.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}