---
title: Let's talk about the RNC and moderate voters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ccWLUzFcJeU) |
| Published | 2020/08/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Moderate conservatives faced a tough decision after the Democratic primary, choosing between Biden and Trump.
- They were initially drawn to Biden but were concerned about his stance on the Second Amendment.
- The Republican National Convention reminded them of the reasons they were willing to cross over to Trump.
- Beau points out that Biden may not have the political capital to push through a gun ban.
- He suggests that Biden might be lying or throwing red meat to his base.
- Even if a gun ban were enacted, Beau questions if moderate conservatives would actively comply.
- Beau predicts that if Biden loses, a moderate Republican candidate may promise to repeal the ban.
- He argues that using the National Firearms Act to enforce a ban logistically wouldn't work.
- Moderate conservatives are torn between an authoritarian figure (Trump) and a candidate with policy differences.
- Beau acknowledges the dilemma these conservatives face but suggests they might be overcomplicating their decision-making process.

### Quotes

- "You can stand on principle. You can. But if you're going to stand on principle, that means you can't vote."
- "It's a tough decision to have to make."
- "Y'all have a good day."

### Oneliner

Moderate conservatives face a tough decision between an authoritarian figure and policy differences on the Second Amendment, potentially overcomplicating their voting choice.

### Audience

Moderate conservatives

### On-the-ground actions from transcript
- Analyze and understand the policies of political candidates beyond rhetoric (implied)
- Stay informed about the potential implications of proposed policies on issues that matter to you (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the dilemma faced by moderate conservatives in the upcoming election, offering insights on political ideologies and potential future scenarios.

### Tags

#ModerateConservatives #SecondAmendment #ElectionDecision #PoliticalAnalysis #Authoritarianism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk to the moderate conservatives.
Boy do I feel sorry for y'all right now.
Because it all seems so simple right after the Democratic primary.
You started off with Trump, you recognized him for what he is.
You're like no, you can't do that.
Democratic primary is going on and you're sitting there just hoping for anybody you
can get behind.
And you end up with Biden, Republican-lite, somebody so far to the right that those people
on the actual left dislike him more than you do.
And everything was fine.
You knew what you were going to do.
And then you saw his platform.
Had that ban on it.
And you're a 2A supporter.
Got it.
Can't do that.
So you go back to Trump.
And then the RNC happened.
And you got reminded of all the reasons that you were willing to cross over.
You saw those continuing displays of nationalism.
The flagrant violation of the law.
Saw Americans get divided up between us and them.
Scapegoated.
Saw the administration accept responsibility for nothing and display no leadership whatsoever.
Outlanding people who hadn't been in power in four years for all of their failures.
And you're reminded of what sent you over there to begin with.
Because you're a student of history.
You're a 2A supporter.
A real one.
Not a gun nut.
They never left Trump.
But you recognize it for what it is.
So now you're in this position.
You got this authoritarian that you can't get behind.
That you worry about.
And then you have this person advocating a policy you can't get behind.
And it seems like a really tough situation.
And then I started thinking about it last night.
Because a whole bunch of you were in the replies on Twitter.
And something dawned on me.
So let's just say Biden takes office.
Realistically, he's probably not going to have the political capital to push through
any ban.
You know that.
I know that.
He's got to fix the mistakes of four years of Trump.
Because everything that Trump did, it's going to come to a head in the next four years.
The bad economic policies.
The fallout from the mismanagement of the public health issue.
His foreign policy disasters.
All of it.
It's all going to fall on Biden's head.
And Biden's going to want what every first term administration wants.
A second term.
With all of that negative press he's going to get cleaning up Trump's messes, he's probably
not going to push that through.
Not going to have the political capital to do it.
You also have to entertain the idea that he's just lying.
Because he's a politician and betting on a politician lying is normally a pretty safe
bet.
It could just be red meat he's throwing to his base.
Like when Trump pretends he cares about the country.
And then you also have to remember he's an Obama man.
I know, gun nuts right now.
He's going to take the guns.
But if you're actually a second amendment supporter you follow policy not rhetoric.
And you know that Obama was one of the most pro-2A presidents we've had in a while.
More so than Trump.
So maybe it'd be like that.
But you do have to entertain the possibility that he does, for whatever reason, decide
to push this through.
And he can get it through the House and the Senate and get it signed.
This is what dawned on me last night.
What happens then?
See I was a part of the second amendment community for a really long time.
And it didn't really click until last night talking to some of you.
What are you going to do if that ban gets enacted?
Realistically, nothing.
You're not going to do anything.
You're not going to be in any hurry to go get your stamp.
You know it and I know it.
And the reality is there's going to be $300 million to process.
More than.
Right now they're on an eight month wait.
With the level that they have at this moment.
Even if he got it through his first day, they wouldn't even be done processing them by the
end of his first term.
Wouldn't happen.
So then what?
He doesn't get re-elected.
You know it and I know it.
Because some moderate conservative is what the Republicans will run.
If Trump loses, especially if he loses in big enough numbers, the Republican party is
not going to run another candidate like him.
It's not a winning strategy.
They'll run a moderate.
One of your people.
And what is that moderate Republican going to promise those folks in swinging states?
To get rid of that ban?
You know it and I know it.
So the reality is you have an authoritarian who is so bad willing to get you to change
parties.
It was bad enough to send you to another party.
And you have a person who generally you align with except for this one issue.
And the reality is that even if he gets it enacted, by the time that window is closing
to have all your registration and stuff done, you're going to have a different president
who's going to cancel it.
Using the NFA to backdoor a ban like that, it's not going to work.
I mean not just the whole issue with marginalized people.
Logistically, it's not going to work.
There's way too many to do.
You would not finish them in one term.
And if you try it, you're not going to get a second term because everybody who's been
waiting a year and a half, they're not going to vote for you.
Realistically it doesn't seem like that big of an issue.
Not really.
But then you have that authoritarian.
The one you actually worry about.
The one that has shown every sign of undermining everything this country stands for while blaming
other people because he can't take responsibility for his actions.
And you're probably a little worried because you're seeing a whole lot of people fall for
it.
That's why you're a ping pong ball right now bouncing back and forth between the candidates.
Because you see it.
You know what he is.
And that convention put it on full display.
If you switch the language, you probably wouldn't be able to tell them apart.
Not really.
And you know that because you are a student of history.
Because every Second Amendment supporter is.
You can stand on principle.
You can.
But if you're going to stand on principle, that means you can't vote.
Or you have to vote third party.
If you're going to stand on that idea that I will never vote for somebody who's proposing
a ban, you can't vote.
Because the other part to this is odds are Trump's going to do it too.
After all, he does like to take the guns first and worry about due process second.
He has a worse Second Amendment record than Obama did.
I don't know.
I mean, I feel for you.
It's a tough decision to have to make.
It really is.
But I think you might be making it harder than it has to be.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}