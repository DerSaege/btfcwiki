---
title: Let's talk about Biden's possible foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IG5mkdk0L3k) |
| Published | 2020/08/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau criticizes Trump's foreign policy, calling it a complete failure and an unmitigated disaster.
- Beau expresses doubts about Biden's ability to clean up Trump's mess due to lack of expertise.
- Biden established a foreign policy team separate from the usual advisory team, indicating seriousness.
- The foreign policy team includes 2,000 people divided into working groups, specifically chosen to address current issues.
- Biden's team focuses on arms control internationally and defending human rights and marginalized communities.
- There is a separate working group dedicated to addressing racial injustice in the United States and its impact on foreign policy.
- Biden's team is likely to be pro-refugee and focused on upholding international and domestic laws.
- The team aims to undo much of the damage caused by Trump's policies, although maintaining American dominance remains a core goal.
- Beau acknowledges that the new foreign policy crew under Biden, if utilized effectively, could make significant improvements.

### Quotes

- "Unlike Trump, who only knows what the leadership of the country tells him, Biden's team has access to information from various sources."
- "It's not about creating a better world; it's about maintaining American supremacy."
- "This crew could actually undo most of the damage Trump has done."

### Oneliner

Beau examines Biden's potential foreign policy, focusing on expertise, arms control, human rights, racial injustice, and refugee support while maintaining American dominance.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Contact organizations supporting arms control (implied)
- Join groups advocating for human rights and marginalized communities (implied)
- Support refugee aid organizations (implied)
- Advocate for racial justice in domestic policies to improve international standing (implied)

### Whats missing in summary

Insights on the potential challenges and obstacles Biden's foreign policy team may face in implementing their agenda effectively.

### Tags

#Biden #ForeignPolicy #ArmsControl #HumanRights #Refugees


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
Biden's foreign policy, should he be elected. If you have watched this channel for any length of
time, you know that foreign policy is something I keep up on, and I think it's really important
because it impacts everything. You also know that because Trump decided to take a very hands-on
approach to it, it is currently a complete failure. It is an unmitigated disaster in pretty much every
respect, not to put too fine a point on it. I have expressed doubts about Biden's ability to fix this,
to clean up Trump's mess, because it's a big one and it requires a lot of expertise that he doesn't
really have. Biden apparently had the same concerns. He apparently understands his limitations.
He has established a foreign policy team. That in and of itself is nothing. Every presidential
candidate puts together a foreign policy team to advise them. Biden built a second state department.
It's got 2,000 people. It is divided into working groups. The names that we have access to,
some of which have been published and some are rumor mill, they're incredibly appropriate.
They're the exact people needed to clean up this mess. It's actually impressive, to be honest.
Okay, so everything I'm about to say here needs to have a couple of disclaimers in front of it.
One, this is assuming that the names we have are accurate and that they carry over to the
administration and that Biden listens to them, because you can have a pool of brilliant people
at your disposal, but if you don't use them, it doesn't do any good. The other part of it is
that we have to keep in mind, and a lot of the people on this channel need to remember this
specifically, this is in pursuit of American foreign policy. So this is still about maintaining
the American empire. The difference is this foreign policy would be unlike anything we have seen
in my lifetime. It's a very different crew of people. There are some holdovers from the Obama
era, but not many, and they were the more, those more likely to try to establish trade than use
force in most cases. There are a couple that you're like, hmm, one of these is not like the
other type of thing. Okay, so as long as we understand those things, we can guess a little bit.
First is that President Biden would have access to information, and by that I mean he has working
groups established to everywhere, to every country, every region, not just those that are important
geopolitically, everywhere, and the people associated with these working groups,
they're appropriate. They're people who know what they're doing. So unlike Trump, who only knows
what the leadership of the country tells him, Biden would have access to information that could
help him gauge how American actions would be read by the average person in those countries.
Whether or not he takes that into consideration during his decision making is an entirely
different question, but he would have access to that information, and putting it together this
early leads me to believe that he would want to use it. This is going to be a surprise to anybody
familiar with Obama's foreign policy. There are a whole lot of people who are not going to be
on the published list and in the rumor mills that are really for arms control, not domestic arms
control. I'm talking about international arms control, making sure certain countries don't get
missile systems, for example, a lot of them, so many that it is safe to assume that that would be
a key piece of Biden's foreign policy. Another thing is that there are a whole lot of LGBTQ people.
Now, this could just be decent hiring practices so they're not discriminated against,
or it could mean that the Biden administration plans on attempting to reassert the United States
as a defender of human rights and trying to speak for the rights of marginalized people,
not just this community, but religious and ethnic minorities as well.
Again, it could just be they didn't discriminate against them. It's sad that you don't know which,
but there are enough of them to make, to create speculation that that's going to be a part of
his foreign policy. He has a separate working group dedicated to racial injustice in the United
States. Now, if you work for the Trump administration, you're probably saying,
what does that have to do with foreign policy? Well, Stephen, the way our domestic law enforcement
agencies act limits our ability to curtail the mistreatment of dissidents in other countries.
If, for example, the leader of the United States was talking about dominating the streets and
America's law enforcement agencies were very visible in their attempt to do that,
it makes it impossible for the United States to speak up for dissidents overseas, because we
behaved as bad or worse as the country we want to chastise. These are dissidents who might be
friendly to the United States, who we might want to help. This is a consideration the Trump
administration never would have even thought of, and Biden has a working group dedicated to it
before the election. That's actually pretty impressive. It would be incredibly pro-refugee.
Now, let me differentiate between immigration, enforcement, and refugees. Enforcement is
domestic. That doesn't have anything to do with what we're talking about right now. We don't have
a read on that. Immigration is different from being a refugee. Now, we don't know much about
the immigration stuff yet. Those who we have access to or who are rumored to be involved
are very pro-refugee, and they understand international law. They understand that we are
violating international law. We are violating our own laws. We are violating our obligations
under the Constitution. I would assume that with the people they have, that a lot of Trump's
policies would disappear overnight. They're not going to want to maintain them for any length of
time at all because they're illegal. So, that's a win. Now, these are not people who would go as
far as say I would want them to go, but it is light years beyond what the Trump administration
has or Obama. So, that's good. All of this is great, but we do need to keep in mind that even
though many of those who are involved with this, alleged to be involved with this, are the type
who would want to switch America from being the world's policeman to being the world's EMT,
it would still be being the world's EMT in pursuit of maintaining American dominance. That isn't
going to change. So, even with all of this good news, understand it's not about creating a better
world. It's about maintaining American supremacy. Although, I would suggest that doing it through
trade is probably better than doing it through force. So, at the end of the day, assuming all
the information we have is true, this is a crew that could actually undo most of the damage Trump
has done. Some of it is irreparable, but they could fix a lot of it. Now, again, this requires
Biden to actually listen to them. We don't know that he would do that, but what we do know is
there are enough foreign policy experts willing to help at this stage that if he decided to take it
seriously, he would have access to pretty much anybody he wanted that knows what they're doing.
So, take that as some good news for the weekend, I guess. Anyway, it's just a thought. Y'all have
a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}