---
title: Let's talk about a PSA, Trump, and gone in 60 seconds....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wolPGhfNsR4) |
| Published | 2020/08/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- FDA warns against using certain hand sanitizers due to ineffectiveness and contamination.
- President criticized Dr. Fauci's COVID-19 response, claiming US high cases due to extensive testing.
- European countries shut down deeper than the US, leading to better COVID-19 outcomes.
- Lack of unified national response and leadership contributes to US COVID-19 situation.
- President's actions worsen the already bad COVID-19 situation.
- Administration lacks a clear plan to address the devastating COVID-19 impact.
- Allegations that the administration prioritized political interests over effective response strategies.
- The president's divisive actions disregard the well-being of all Americans, regardless of political affiliation.
- Beau criticizes the president for tweeting falsehoods and undermining national leadership during the crisis.

### Quotes

- "He's tweeting out falsehoods. And he's doing so at the expense of American lives."
- "We still have no national leadership."
- "The only leadership we're getting at the national level, the president tries to undermine at every turn."

### Oneliner

Beau warns against dangerous hand sanitizers, criticizes President's actions worsening US COVID-19 situation, and calls for unified national leadership. 

### Audience

Public Health Advocates

### On-the-ground actions from transcript

- Avoid using hand sanitizers listed by the FDA as ineffective or contaminated (implied)
- Advocate for a unified national response to COVID-19 (implied)

### Whats missing in summary

Detailed analysis and context on the impact of the president's actions on the COVID-19 crisis.

### Tags

#COVID-19 #HandSanitizers #PublicHealth #NationalLeadership #PoliticalResponsibility


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to start off with a little bit of a public service announcement.
The FDA has identified a hundred or more hand sanitizers that you need to not use.
Some are ineffective, they have low levels of the active ingredients, or they have been
contaminated with something else and they're dangerous.
There will be a link down below that if you scroll down a little bit it's got a search
bar, type in the name of the hand sanitizer you have and you'll be able to check to see
if it's on the list.
Okay, so yesterday the president lashed out at Dr. Fauci via tweet, as he tends to do
when he gets bad news, so we're going to talk about that in depth.
We'll just go ahead and start off by saying that's one.
Okay, so Fauci, he explains why the US is faring worse than European countries, because
they shut down deeper, meaning they closed down like 95% of their economy, whereas we
only did about 50, and we reopened too soon.
That's the statement.
It's backed up by fact, it's what happened.
The president tweeted out, with a clip of him doing this, he says, wrong.
We have more cases because we have tested far more than any other country, 60 million.
If we tested less, there would be less cases, that's totally not how that works, if you
don't know.
The cases would still be there, we just wouldn't know.
How did Italy, France, and Spain do?
Go ahead and say that's two right now.
Now Europe sadly has flare-ups, most of our governors worked hard and smart, that's...
We will come back strong.
Most of the governors, a lot of them anyway, ignored the guidelines.
They reopened too soon, this is where we're at.
Sometimes in some places there was a lack of compliance.
There's a whole lot of reasons for this.
It's probably because we don't have a unified response, and we have no leadership.
We have no response from the national level, we have no leadership from the national level,
which would be Donald's job.
Perhaps while this is going on, and we're in the middle of all of this, it's not the
best time for the president to attack the one person that kind of knows what's going
on.
That's three.
If you're wondering what I'm counting, on July 30th we lost a person every 60 seconds
to this.
Meanwhile the president is golfing, the president is lashing out at the only person who seems
to have the trust of the American people.
He is encouraging governors to disregard guidelines.
He has politicized this.
This is why we're in the situation we're in.
He made a bad situation that he had no control over worse.
It's where we're at.
And there's nothing he can tweet that's going to change that.
If we don't test for it, we just don't know they're there.
Doesn't mean that people don't die.
That's four.
This rate, if it keeps up, that's, I mean, it's devastating.
It's devastating.
And there's nothing that is, nothing the administration is proposing to stop it.
They've thrown their hands up.
They're trying to divert and change the subject.
They're trying to tweet their way out of it.
There are reports that there was somewhat of a plan, but it was scrapped when it was
realized that most of the impact at the time was affecting blue states, the president's
opposition.
So he didn't care.
I'd like to point out that in most states, things are pretty evenly split, more or less.
So if you were to let a blue state go, you'd lose a lot of red people, too.
That's five.
You know, a blue state isn't wholly populated by Democrats.
The president and his team know this.
Understand if you are a Republican, they don't care.
They do not care about you.
They never have.
He's a game show host.
He's good at branding.
He's good at conning the gullible.
That's it.
He's tweeting out falsehoods.
And he's doing so at the expense of American lives.
It's out of hand.
We still have no national leadership.
The only leadership we're getting at the national level, the president tries to undermine at
every turn.
That's probably something we should consider and think about.
Anyway, it's just a thought.
That's six.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}