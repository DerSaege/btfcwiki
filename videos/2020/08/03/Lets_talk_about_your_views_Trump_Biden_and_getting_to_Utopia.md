---
title: Let's talk about your views, Trump, Biden, and getting to Utopia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ciegoYE5tVY) |
| Published | 2020/08/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Emphasizes the importance of having a clear understanding of your core beliefs to bring about social change.
- Stresses the significance of knowing your ideal world or utopia to guide the steps towards achieving it.
- Acknowledges that deep systemic change will not happen overnight and may take generations.
- Contrasts the current foreign policy landscape with his ideal of American foreign policy focusing on peace and development.
- Advocates for celebrating small wins and acknowledging incremental progress towards long-term goals.
- Reveals his pro-immigration stance but strategically advocates for attainable ideas to persuade those who oppose his views.
- Warns against advocating ideas that are too extreme or far outside the mainstream as they may not resonate with the current societal mindset.
- Encourages individuals to determine their beliefs, envision their ideal society, and work towards it globally.
- Urges the importance of maintaining pressure and accountability on future administrations for real change.
- Reminds that progress requires consistent effort, focus, and unity even under a new administration.

### Quotes

- "You have to figure out what you believe, what your ideal society looks like."
- "If you want freedom, if you want systemic changes, you can't stop just because the new guy has a D instead of an R."
- "You have to figure out where you fit into that battle."
- "We progress. We move forward."
- "It's possible to get there. We just have to work for it."

### Oneliner

Beau stresses the need to understand core beliefs, visualize utopia, celebrate small wins, and maintain pressure for lasting social change even under new leadership.

### Audience

Activists, Advocates

### On-the-ground actions from transcript

- Determine your core beliefs and envision your ideal society (implied).
- Advocate for attainable ideas to persuade those in opposition (implied).
- Maintain pressure and hold future administrations accountable for real change (implied).

### Whats missing in summary

Beau's impassioned call for individuals to understand their beliefs, envision a better society, celebrate incremental progress, and continuously push for systemic change is best experienced in full for a comprehensive understanding.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about your beliefs, your core beliefs and why it is
incredibly important
for you to have a very firm grasp
on what you believe
if you want to change the world.
If you want to be effective
at creating social change.
You have to have a clear picture
of what you want your ideal world to look like, your utopia.
Because most people who watch this channel
want deep systemic change
on a whole bunch of different levels.
It's not something that's going to happen overnight.
It's going to take time. Some of the ideas will take generations
to get to.
Some of the ideas that are
most important to me
I know
I will be long gone
before they are ever realized.
But
if you don't know
what your utopia is,
if you don't know what it's
supposed to look like,
you can't help guide the conversation
to get the steps to get there.
A lot of the stuff we want
there's no direct path from where we are
to what we want.
So we have to look for the stepping stones.
The reason I'm bringing this up is because
after that recent video about Biden's possible
future foreign policy,
a whole bunch of people commented like
you're saying this is a win but you don't seem really enthusiastic about it.
Yeah, that's a pretty accurate read on how I feel about it.
Is it better than Trump's foreign policy? Yes. Yes, absolutely.
But my teenager could come up with a
foreign policy better than Trump's.
Anything the Biden team
puts out is going to be better than Trump's foreign policy.
That's not saying anything.
My views,
my ideals,
or my core beliefs
say American foreign policy should be
is stuff that
is literally laughed at
today.
We had a Democratic candidate
actually kind of advocate for some of them
and it became a joke because it's too far outside the Overton window.
We are not going to wake up during the Biden administration
and have a foreign policy that is
primarily about being the world's EMT
rather than the world's policeman,
about developing infrastructure
in other countries, about raising the standard of living in other countries
because all of these things
helped preempt conflict.
In order to get to the world I want,
we have to have a world at peace.
So American foreign policy is really important for that.
We're not going to get there from here, where we're at right now.
However, a foreign policy that is more based on trade
than force,
that might help get us there. That might be a stepping stone
because
trade requires
infrastructure.
It might raise the standard of living in these countries
depending on how much wealth
the
companies extract along the way.
In order to have any wins,
you have to know where you're going.
Then you can kind of take a small acknowledgement
of these baby steps.
A lot of these ideas are going to take generations
to get to.
You have to celebrate the baby steps along the way,
even if less than enthusiastically.
I think most people watching this channel know that I am
ridiculously pro-immigration.
That's not really a secret.
I don't advocate in these videos for my more radical ideas.
I advocate for those that are attainable
because my goal on this channel
is to hopefully sway those who are in opposition.
That's what I'm trying to achieve, so I don't advocate for the more radical
ideas. And even downplaying
my
my beliefs a little bit,
I still have that person that shows up every once in a while and
you're one of those open borders guys.
No, that's ridiculous. I'm not in favor of open borders. I don't even support borders.
When your
core belief is so far outside the Overton window that the straw men being
made to come at you
aren't extreme enough,
that's probably not an idea
that the world is ready for yet.
So you may not want to advocate for that
if your goal
is to shift those who are in opposition.
Now, if you're one of those people who is
more theory inclined,
you're not looking for
results,
you're looking to shift that window,
yeah, advocate for those extreme ideas. Advocate for open borders.
Advocate for
the disillusion
of checkpoints.
All of that.
Help shift that Overton window.
But understand it takes a very special person to do that for any length of
time
because you don't get any wins.
You're advocating for something that is generations away.
So it takes a lot of self-discipline
to stay on that level.
So you have to figure out what you believe,
what your ideal society would look like.
You know, do you want it to
have a hierarchy?
Do you want it to use force?
All of these things you have to kind of figure out.
And then once you figure out what you think is right and wrong,
try to apply it on a global level.
And it helps you figure out the steps to get there.
The reason I'm bringing this up
is because hopefully
we're about to have an administration change in the United States.
And that's good and it's bad.
It's good
because,
I mean look around,
it's bad
because right now
anybody who wants
deep systemic change
is united
because we have the personification
of everything that is wrong with the United States sitting in the Oval Office.
Got everybody on the same team.
That's helpful in a way.
If somebody was to
come to power that was
less controversial
in the way they speak or tweet,
that was
a little better
at not saying the quiet part out loud,
that unity would fade
unless people get a firm grasp on what they believe and what they want now.
And then hold that next administration
accountable.
Get to where we want to go.
If people don't maintain the pressure,
understand that a Biden administration
will just be a return to the status quo.
It will be a return
to
the very things that created Trump.
We have to be ready
to push
the Biden administration further.
To make them play out their part
in the story of humanity.
This is what humans do.
We progress.
We move forward.
Yeah, there's setbacks every once in a while,
but on a long enough timeline,
the progressives always win.
Right now we have the machinery for change. We have the unity. We have
everything we need.
We just have to keep focus
if there is a change of administration.
If you want freedom,
if you want
all of those reforms
and
systemic changes
that we've talked about for the last few years,
you can't stop
just because
the new guy has a D
after his name instead of an R.
So, it might be time
for everybody to figure out what
they really want.
What they want deep down.
What kind of world they want to leave behind.
What kind of steps they want to take
so the next generation
can take a few more
and get us to that utopia.
You know,
that place where everybody has freedom,
where everybody has a decent standard of living.
It's possible to get there.
We just have to work for it.
And you have to figure out where
you fit into that battle.
You have to figure out what your ideas are
and how you can help shape the conversation
at the dinner table
so it gets shaped on social media,
so it changes the public narrative,
so hopefully it changes public policy.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}