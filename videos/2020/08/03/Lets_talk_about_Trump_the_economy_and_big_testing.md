---
title: Let's talk about Trump, the economy, and big testing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=v-657b2MUmI) |
| Published | 2020/08/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes President Trump for linking the increase in COVID-19 cases to increased testing.
- Points out the inaccuracy in Trump's statement about testing causing the rise in cases.
- Emphasizes that hospitalizations have increased significantly, unrelated to testing.
- Argues against reopening schools, stating it will contribute to the spread of the virus and harm the economy.
- Addresses the importance of following CDC guidelines for reopening.
- Expresses concern about the lack of a unified plan and the potential for more loss of life and economic damage.
- Urges Trump to lead the nation in taking the necessary steps to combat the pandemic.

### Quotes

- "Cases up because of big testing, cases are up because more people have it."
- "Opening the schools is a bad idea. It's going to help it spread. It's going to hurt the economy."
- "Because we're not getting back on track, because we're not taking the steps we need, because we don't have a unified plan, all of this drags on."
- "If the president is concerned about the economy, he's going to have to lead the nation into doing the right thing."
- "In order to set the example, you have to take responsibility for something."

### Oneliner

Beau criticizes Trump's misleading COVID-19 testing claims, warns against reopening schools, and stresses the need for a unified plan to combat the pandemic.

### Audience

Concerned citizens, policymakers

### On-the-ground actions from transcript

- Follow CDC guidelines for COVID-19 prevention and reopening schools (suggested)
- Advocate for a unified national plan to combat the pandemic (suggested)
- Encourage responsible leadership to prioritize public health and safety (suggested)

### Whats missing in summary

Beau's detailed analysis and passionate plea for effective leadership and adherence to expert advice during the pandemic.

### Tags

#COVID19 #Trump #CDC #PublicHealth #Pandemic #Leadership


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Trump, big testing, and nonsense.
The President of the United States is one of the few people capable of packing so much
nonsense into a tweet that even Alice gets upset.
Today he tweeted out, cases up because of big testing, big in all capital letters of
course.
Much of our country is doing very well.
Open the schools.
I want to pause for a second and just draw attention to the fact that the President of
the United States, who is at heart supposed to be a politician, somebody who can construct
statements that make sense, thought it was a good idea to put cases up and open the schools
in the same tweet.
Twitter limits how much you can put into a single tweet.
He decided these two ideas should be associated with each other.
That decision alone should bar him from running the country, to be completely honest.
Now I can understand the President's desire to reopen the schools after looking at these
sentences but this is nonsense.
This isn't accurate.
Cases up because of big testing, cases are up because more people have it.
This idea that it's linked to testing, yes, we know more people have it because of the
testing but that doesn't mean it's not spreading.
We can look at hospitalizations which are through the roof.
It has nothing to do with testing.
That's inaccurate.
It's made up.
It's a lie.
The reality is that we've gone from 30,000 or so on July 1st to 60,000 or so hospitalizations
on August 1st.
Hospitalizations are up.
That doesn't mean that people are getting tested and they're not impacted.
It means it's spreading because we have more severe cases.
Opening the schools is a bad idea.
It's going to help it spread.
It's going to hurt the economy.
It's going to delay us getting back on track, not to mention the human cost of this horrible
decision.
Aside from that, let's just say what he's saying is true, that it's the testing leading
to the higher number of cases.
This isn't true.
It's not accurate, but let's pretend that it is.
Even if that was true, we still don't open according to the CDC, according to the reopening
guidelines they put out before he started putting political pressure on them, before
he started meddling, before he started being more concerned about his own reelection chances
than American lives, we had to have a decline, not going up, not even staying the same.
When people talk about him disregarding the advice of experts, this is what we're talking
about.
They're altering the statements put out to suit their political ends.
We're finally at the point where even those medical experts who wouldn't burst out laughing
and publicly embarrass the president when he asked about disinfectants, even they're
coming out now and saying, in rural America, you're really not safe from this.
We have to take this seriously.
Even they're coming out now because they understand the potential for loss of life.
Forget about the economy for a second.
Loss of life.
That loss of life, since most people care about money, is going to directly translate
into a delayed economy.
Because we're not getting back on track, because we're not taking the steps we need, because
we don't have a unified plan, all of this drags on.
The longer it drags on, the worse the economy will get.
If the president is concerned about the economy, he's going to have to lead the nation into
doing the right thing.
It doesn't have to be ordered.
It could just be him setting the example.
The problem is Trump doesn't know how to do that.
Because in order to set the example, you have to take responsibility for something.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}