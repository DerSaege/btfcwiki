---
title: Let's talk about the American man and the toughest guy at the table....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=R7QvtVlTj0I) |
| Published | 2020/08/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the myth of the ideal American man and the tough guy image that one side of the political spectrum has been discussing.
- Refuting false assertions made by prominent figures like the President of the United States and Ben Shapiro regarding men being insulted by certain events.
- Sharing a personal experience about a group of tough guys he knew who defied the stereotypical tough guy image.
- Describing how these men, despite their tough exterior, focused on diverse topics like sports, music, and family rather than projecting toughness.
- Revealing the true toughest guy at the table, not because of physical strength but because he was a single dad to six kids.
- Emphasizing that true masculinity is not about destruction but creation, about lifting people up and making things better.
- Challenging the country's idea of masculinity and pointing out that real tough guys don't advertise their toughness but view fatherhood as the toughest assignment.
- Questioning the societal perception of masculinity and advocating for a shift towards understanding it as a force for good and upliftment rather than destruction.
- Suggesting that masculinity is about building something better and uplifting people rather than hurting or destroying.

### Quotes

- "Masculinity is not about hurting people. It's not about destruction. It's about making things better and uplifting people."
- "The hardest uniform that any of those guys had apparently ever seen wasn't a plate carrier. It was a burp cloth."

### Oneliner

Beau challenges the myth of the ideal American man, advocating for masculinity defined by upliftment and creation rather than destruction.

### Audience

Men, fathers, society

### On-the-ground actions from transcript

- Validate and support diverse expressions of masculinity (implied)
- Recognize and appreciate the role of fatherhood as a tough assignment (implied)
- Challenge stereotypes and misconceptions about masculinity (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of societal perceptions of masculinity and advocates for a more nuanced understanding beyond traditional tough guy stereotypes.

### Tags

#Masculinity #Fatherhood #MythsofManhood #Stereotypes #Upliftment


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the American man, the ideal American man, the myth of the
ideal American man, that tough guy image.
We're going to talk about that because one side of the American political spectrum has
been talking about it a lot lately.
They've been making a lot of false assertions.
You have the President of the United States saying that men are going to be insulted because
of Harris' selection.
No that's not a thing.
No man is insulted by that.
Some little boys may be, but no man will be.
You got the whole Ben Shapiro thing.
You have a giant list of men on the right that have decided to make this the talking
point of the month.
And then apparently you have a rather prominent woman on the right side kind of go on a rant.
I didn't track this down and watch the whole thing, but I saw little clips.
And she said that the men were all fake.
Trash was one of the words that she used.
It's funny, because of the jobs that I've had over my life, I know some tough guys,
that those ideal American men, the ones that get movies made about them, stuff like that.
And this one crew, they always worked together.
And we're talking about a group of guys that moved as one at all points in time.
And they were working, after they stopped working, they kind of scattered around the
country.
But through a weird twist of fate, a few years ago, they were all pretty close to each other,
geographically speaking.
So they decided to meet up at this place called McGuire's.
They invited me, and I went.
I wasn't part of this little circle, but I'd worked with them before, I'd seen them.
I was one of the last ones to arrive, because I live closest, that's how that works.
Go in, walk past the hostess, because there's only one little area that this many, that
could seat this many people.
And when I see them, I couldn't stop laughing, because I'd never seen them outside of a work
environment.
You know, you probably have this image of guys like that.
And all of them wearing Oakleys and shirts with the American flag on it, and stuff like
that.
One of them looked like he'd just walked off the beach, because he had, because he's a surfer.
Another one looked like he had just walked out of a rap video.
Another one looked like one of the rich kids of Instagram.
Another one, cowboy boots, belt buckle.
And another one was dressed fabulously, looked like he'd spent all night partying at a club
in Pensacola called Emerald City, because he had.
If you're not familiar with it, go ahead and Google it, and then add that to your definition
of tough guy.
And there were some other people there, all dressed differently, because being a tough
guy, being that ideal American man, that's not their identity.
It's just a personality trait.
Up until I sat down at that table, every person there, we're talking real warriors, real patriots,
kind of guys who would fight their way out if they were pinned down and out of ammo.
They'd fight their way out with rocks and sticks.
As we're all sitting there, we're talking, and they talked about everything, except their
job, except being tough.
Talked about sports, talked about music, talked about movies, their families, all of that
stuff.
And then there came a moment when everybody at that table recognized who the toughest
guy at the table was.
And it wasn't because he was 6'6", and at the time, you know, nothing but muscle.
It was because he was talking about his latest assignment and how difficult it was, and you
could see the faces on everybody, just kind of like, whoa.
His latest assignment was that he had become a single dad to six kids.
Everybody at that table was like, wow.
Men who volunteered to go into impossible situations, men who on a daily basis would
agree to do things they knew they may not walk away from.
Nobody at that table was volunteering for that.
Most of the country, most of the male population, looks to guys like that as the tough guys,
those people to emulate.
That's the image they want to project to the world.
The funny thing is, those guys, they don't project that image to the world.
They don't walk around and advertise, hey, I'm a tough guy.
And they look at fatherhood as being the hardest assignment.
I think the country may have its idea of masculinity a little messed up.
Think they may be off a little bit, because the real tough guys, it's not their image.
Those that they idolize, idolize something else.
They see that as more manly, more difficult, more of a challenge.
Masculinity is not all about destruction.
A lot of it has to do with creation.
A lot of it has to do with good things, not just tearing down what somebody else has done.
Masculinity is not about keeping people down.
It's about lifting them up.
Understand that every guy at that table, when they were doing horrible things, in their
mind on some level, they were doing it to build something better.
Masculinity isn't about hurting people.
It's not about destruction.
It's not about killing.
It's about making things better and uplifting people.
The hardest uniform that any of those guys had apparently ever seen wasn't a plate carrier.
It was a burp cloth.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}