---
title: Let's talk about the Biden Harris campaign and what it means for us....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=B3_QS9tUpdE) |
| Published | 2020/08/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the strategy behind the Biden-Harris campaign and why it seems to alienate some Democratic voters.
- Describes how the Democratic Party is targeting moderate voters rather than leftists to secure votes and deny votes to Trump.
- Points out that the campaign aims to beat Trump on Twitter and personal attacks rather than policy.
- Notes the importance of Black women as loyal Democratic voters and how Harris appeals to them.
- Analyzes the dynamics of the Biden-Harris campaign and how they plan to counter Trump's attacks.
- Urges viewers who seek deep systemic change to focus on down-ballot races for real change.
- Addresses the different schools of thought among leftists regarding voting in the upcoming election.
- Emphasizes the need for community action and involvement beyond voting for a just society.
- Stresses the importance of individual action and building community networks, especially for leftists seeking systemic change.

### Quotes

- "The Democratic Party has decided to forego those votes. We don't need the leftist vote."
- "If you are a liberal, if you are a Democratic party loyalist, you've got a ticket that might win."
- "There is no cavalry coming. It's just us. We have to make the changes."
- "Those community networks are more important than ever."
- "Being active, beyond voting, this isn't a once every four year or once every two year thing."

### Oneliner

The Biden-Harris campaign strategy targets moderates, alienating leftists, while community action becomes vital for systemic change seekers.

### Audience

Leftist activists

### On-the-ground actions from transcript

- Build community networks and foster involvement beyond voting (implied)
- Focus on down-ballot races for real change (implied)
- Be active in striving for a functioning, free, and just society (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the Biden-Harris campaign strategy, the importance of community action, and the need for systemic change beyond elections.

### Tags

#BidenHarris #DemocraticParty #CommunityAction #SystemicChange #LeftistMovement


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about the Biden-Harris campaign.
Because that's a thing. It's official.
We're going to talk about why that announcement seemed to throw a wall up
between two groups of people who seemed most likely to back a Democratic ticket.
If you were on social media at the time the announcement was made,
you probably saw a lot of people who were unhappy.
We're going to explain why that is.
We're going to explain that that wasn't a surprise to the Democratic Party.
It's part of the strategy. They're counting on that.
We're going to talk about what a Biden-Harris campaign and possible presidency would mean to us.
And we're going to talk to those people who are, let's just say, less than enthusiastic about the Biden-Harris ticket.
Okay. The Democratic Party is really made up of two groups of people.
Okay. The first are liberals. Liberals are center-right.
On the real scale, they're center-right.
I know in the U.S. they're the left, but they're center-right.
Then there's a small fraction of the Democratic Party, or people who vote Democrat,
who are actual leftists, who cross over into that territory.
The Democratic Party has decided to forego those votes.
We don't need the leftist vote. We're going to try to pull votes from the center.
We're going to go for moderates.
The idea is that they can pull more votes from there, and that these are people who may vote for Trump
if they don't vote for the Democratic candidate.
So they're trying to get votes and deny votes to Trump at the same time.
That's the goal.
The overall strategy is basically they want to shore up their supporters.
And who are the most loyal voters for the Democratic Party? Black women.
Harris certainly appeals to them.
However, I don't think it's fair to say that's the only reason she was chosen,
because she brings something else huge to the campaign because of the strategy.
They're not going to try to defeat Trump on policy.
Why? Because Trump doesn't even know what policy is. He doesn't have one.
Even if he had one, he wouldn't stick to it. He would just lie and change it.
That's a losing campaign.
They're going to try to beat him on Twitter.
They're going to try to beat him on the personal attack level.
Trump has two main lines of attack right now.
They're radical leftists and law and order.
Neither Biden nor Harris is a radical leftist. They're not even moderate.
They're not even leftists. They're liberals.
The running joke in leftist circles right now is that this is one heck of a Republican ticket.
This campaign will get active opposition from leftists, and they're counting on that.
Because if they're getting opposition from the actual leftists,
well, then those in the center are going to see that they're not leftists.
Law and order.
There is nobody on the American political stage more law and order than Harris.
Locking people up, that's like her thing.
He's not going to be able to hold a candle to her in this regard.
He's not going to be able to use that line of attack.
And then if he tries to paint her as a leftist or anything like that,
he has to explain why he donated to her. Twice. And a vodka. Once.
This is a...
They're trying to beat him at his own game.
They're trying to show how little substance he has.
And then they're trying to bait him.
Trump is famous for attacking women in incredibly sexist ways.
But Harris isn't Clinton.
Clinton was generally disliked by a whole lot of people.
Harris is disliked by leftists.
If he comes at her the same way he comes at a lot of women politicians,
he might upset women voters, specifically those in the suburbs.
Those that he has to get to win.
That's their plan.
It's cynical. It's superficial.
It is lacking in substance and it will probably work.
So, if you are a liberal, if you are a Democratic party loyalist,
you've got a ticket that might win.
However, if you're watching this channel, I have to assume that you want deep systemic change.
Otherwise, you probably wouldn't be watching this channel.
You want real change.
You are not going to get that from Biden-Harris.
They are as establishment and status quo as they come.
If you want that and you want to use the political system to do it,
you had better be looking at those other races down ballot.
If you don't stack the House and the Senate with AOC types,
you are going to get a kinder, gentler face on a whole lot of the exact same policies.
Some of the more horrendous stuff will disappear,
but overall, not much is going to change.
You have to have those radical people willing to make those changes in the House and the Senate
so they can propose them and get the support to make it politically untenable for Biden to say no.
Okay, so if you are one of those people who is less than enthusiastic about this ticket,
let me start by saying I feel you. This is not my dream ticket, okay?
Yeah, there are three schools of thought right now among leftists.
The first is not going to vote.
Some out of just spite.
Some because they have a moral objection to voting.
They don't want to use the power of the state against their neighbor.
Got it. Got it.
You have to vote your conscience or not vote your conscience.
I understand where you're coming from on this one.
There are some that want to vote third party.
Again, you have to vote your conscience.
If you cannot get behind Harris because of some of the authoritarian background that she has,
I understand that. I do, really.
And then the third is to grit your teeth and vote Biden-Harris
and do it with the understanding that either way, you know,
next year you're going to be opposing the administration.
No matter what, you might as well oppose the one that's going to give you the most freedom of movement
and that it's going to be easier to oppose Biden-Harris than it is Trump-Pence.
There is a subset of people who believe that the right answer is to back Trump
because he helps keep people motivated and speaking out.
I would suggest that's a bad idea.
I don't think that that's actually helpful.
I understand the logic.
I just don't know that I would be willing to do that.
I want to add something else that I think people should consider.
I think it's incredibly important that Trump lose.
I'm not saying that it's got to be Biden and Harris.
I don't care if they win, but it's incredibly important that Trump lose.
I said this during the impeachment.
I wasn't really that upset when he wasn't convicted and removed
because I think it's incredibly important for those people who he marginalized,
that he pushed down, for them to see their neighbors reject it.
Him suffering a resounding defeat will help make sure there's not another one.
If his rhetoric proves to be failing rhetoric, that it can't get a re-election,
they're not going to try to use that again.
That message is really important.
Again, I'm not saying to vote for any particular candidate.
I just think it's incredibly important that Trump lose, that that message is sent,
that the United States has moved past this.
This is not how you make America great.
It wasn't great when these were the policies the first time.
We don't need to try them again.
This rhetoric is failing.
I think that message needs to be sent.
Okay, so what does this mean for everybody watching this channel?
All the stuff we talk about is more important than ever.
When we're talking about individual action, building community networks, stuff like that.
If you're on the left, if you're actually one of the leftists, you already know this.
Top-down leadership doesn't work.
I don't know why we all get mad.
This happens every time.
It's very unlikely that you're going to get somebody in the White House
that is going to promote these ideas.
The amount of money that it takes to get there leads to corruption.
It leads to accepting those ideas, that capitalism, that this system is the way to go.
If you want something else, you're going to have to build it.
And if you are a liberal, understand the reforms you want made, they're not going to happen.
They're not going to happen, especially not to the level that's going to reach your community.
Those community networks are more important than ever.
Being active, beyond voting, this isn't a once every four year or once every two year thing.
If you want a functioning, free, just society, you have to be involved.
There is no cavalry coming.
It's just us.
We have to make the changes.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}