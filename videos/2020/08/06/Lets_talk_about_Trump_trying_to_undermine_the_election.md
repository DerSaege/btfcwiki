---
title: Let's talk about Trump trying to undermine the election....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6zReXSiuavI) |
| Published | 2020/08/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President's changing views on mail-in voting discussed.
- President was for mail-in voting in Florida on August 4th, then against it in Nevada on August 5th.
- Beau suggests president should create a list of states he believes should or shouldn't be able to vote.
- President filing suit in Nevada to suppress the vote.
- Accusations of the president attempting to rig the election and deny the right to vote to his opposition.
- Criticism towards those who remain silent despite claiming to be patriots and lovers of the country.
- Warning that if the president succeeds in rigging the election, it marks the end of the United States as an experiment.
- Prediction that the president's view on mail-in voting will change based on which population it favors.
- Condemnation of the president's active attempt to undermine and rig the election.

### Quotes

- "The president is using the weight of the Oval Office to rig the election."
- "If he succeeds in this, if the president can rig the election, that's it. The United States is done."
- "Those who claim to be patriots and lovers of the country will remain silent and sell out their country."
- "This is an active attempt by the president of the United States to undermine the election, to rig the election."
- "What's going to happen the second there is a state that has a population of probable mail-in voters that seem like they're going to vote for him?"

### Oneliner

President's changing views on mail-in voting and active attempt to rig elections threaten the foundation of democracy in the United States, with a warning of the country's demise if successful.

### Audience

Voters, patriots, activists

### On-the-ground actions from transcript

- Contact local election officials to ensure fair and equal access to voting (implied)
- Join or support organizations working to protect voting rights and ensure fair elections (implied)

### Whats missing in summary

Analysis of potential consequences of voter suppression tactics and suggestions on how individuals can actively protect democracy.

### Tags

#MailInVoting #ElectionRigging #VoterSuppression #Democracy #UnitedStates


## Transcript
Well howdy there internet people, it's Beau again.
So we are going to talk about the president's ever evolving view of mail-in voting.
Again, this isn't an old video.
I know it seems like we just talked about this like 36 hours ago, because we did in
a video that I ended by saying he's going to flip-flop on this again, and he did.
That video went out August 4th when the president was against, no, he was for it then.
He was against it before that, after he was for it.
But on August 4th he was for mail-in voting in Florida, and then on August 5th he's
against it in Nevada.
Make sure we have that right.
Florida good, democracy, freedom, voting, all that stuff.
Nevada bad, fraud, rigged election, blah blah blah.
It would just be peachy keen if the president of the United States could put out a list
with all the states on it, and maybe put a gold star next to the states that he believes
have earned the right to vote, and like a red frowny face by those that he thinks shouldn't
be able to vote.
That would be wonderful.
Maybe even better if he could include like a statement of reasons as to why some states
are more susceptible to fraud.
You know, something in their process that makes them more susceptible to it.
It would be great because if he did that, then we could compare it to the other states
and find the difference.
There's not one, but we could pretend that he wasn't doing what he is.
In Florida, there's a high probability that the mail-in vote will go his way.
In Nevada, it will go against him.
So as the president rails about rigged elections and voter fraud, he's filing suit in Nevada
to attempt to suppress the vote.
The president is using the weight of the Oval Office to rig the election.
He's attempting to allow his supporters to vote and deny the right to vote to his opposition.
We've seen that playbook before.
Most times we don't care, but if this was like an oil-rich country somewhere and this
was going on, we'd already be talking about liberating it.
I'm sure that there are a whole bunch of people right now on social media and they
have like American flag profile pictures or copies of the Constitution or maybe like a
screaming bald eagle or whatever, and they're tweeting the president telling him that he
can't do this because they're real patriots and they love this country.
They're raising their voice and explaining that the very foundation of this country rests
in free elections.
That's not happening.
That's not happening.
Those would-be patriots are still waving their flags with their rights and they'll remain
silent as they have the last three and a half years.
They will complete the transaction and completely sell out their country with this.
If he succeeds in this, if the president can rig the election, that's it.
The United States is done.
The great experiment is over.
Did not produce the results expected.
I'm going to end this video the same way I ended the last one.
His view on mail-in voting will change.
He'll flip-flop on this again.
Just wait.
What's going to happen the second there is a state that has a population of probable
mail-in voters that seem like they're going to vote for him?
At that point, mail-in voting will be good again.
And then it'll be bad when it's a population that's going against him.
We've seen this before.
This is an active attempt by the president of the United States to undermine the election,
to rig the election.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}