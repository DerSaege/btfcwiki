---
title: Let's talk about the news out of New York's AG....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kutFAu9XNHI) |
| Published | 2020/08/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau discloses his long-standing support for the second amendment before discussing the recent news coming out of New York regarding the NRA.
- The Attorney General in New York is pursuing actions that may lead to the dissolution of the NRA due to allegations of corporate misconduct.
- Beau clarifies that the issue is not an attack on the second amendment but rather focuses on corporate behavior.
- He points out that the NRA primarily represents manufacturers, not owners, and functions as a lobbying group.
- Republicans are likely to frame the situation as an attack on the second amendment, as evidenced by a tweet from the President of the United States.
- Beau contrasts the President's accusations against Joe Biden with past statements where the President advocated for taking guns away without due process.
- He criticizes the NRA for not standing up against overreach, particularly because their main clients are manufacturers, specifically law enforcement.
- Beau expresses skepticism towards the NRA and suggests that supporters may be surprised by how their money is spent.
- He acknowledges the political implications of the situation and the risk of attorney generals becoming politically motivated.
- Beau believes that despite the uncertainties, there may be benefits from the developments surrounding the NRA in 2020.

### Quotes

- "Your guns will be taken away immediately and without notice. Yeah that sounds good and everything. You know, except none of that's true."
- "Trump is not a supporter of the second amendment. He never has been."
- "If you successfully defund, ban or whatever, anything, it just becomes a second market."

### Oneliner

Beau clarifies the NRA situation in New York, challenges misconceptions about gun rights, and questions the NRA's true allegiance, revealing the political implications and potential benefits amidst uncertainties.

### Audience

Advocates for gun rights

### On-the-ground actions from transcript

- Investigate how lobbying groups, like the NRA, utilize funds (exemplified)
- Stay informed about political actions that may impact gun rights (exemplified)

### Whats missing in summary

The full transcript provides additional context on the NRA controversy and raises questions about political motivations, calling for a closer examination of the NRA's actions and spending.

### Tags

#GunRights #NRA #SecondAmendment #PoliticalImplications #CorporateMisconduct


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about all the news coming out of New York.
What it means long term.
What the implications are for various groups.
Before we get into it, because there's a whole bunch of new people on the channel, I feel
like I should make a disclosure.
I have a long documented history of supporting the second.
On this channel you can find it.
Huge supporter.
Massive.
Okay.
It's not a secret, but I feel like I should disclose it before I say this.
Especially given what I'm going to say.
Okay.
So what's actually happening up there if you don't know.
The Attorney General is moving to, moving in a direction that if successful could lead
to the disillusion of the NRA.
The allegations, they center on corporate misconduct.
This is not an attack on the second.
It's not.
It has to do with corporate behavior.
I would also like to point out that generally speaking the NRA does not represent owners.
It represents manufacturers.
It's a lobbying group.
I mean at the end of the day that's what they are.
It will be framed as an attack on the second by Republicans.
That's how they're going to try to push it.
And to prove that, an hour ago the President of the United States tweeted this nonsense.
Just like radical left New York is trying to destroy the NRA, if Biden becomes President
your great second amendment, all capital letters, doesn't have a chance.
Your guns will be taken away immediately and without notice.
No police, no guns.
Yeah that sounds good and everything.
You know, except none of that's true.
But I want to give you a different quote.
Tell me if you can guess who said it.
I like taking the guns early, like in this crazy man's case that just took place in Florida.
To go to court would have taken a long time.
Take the guns first, go through due process second.
Now let's go back to that allegation.
Your guns will be taken away immediately and without notice.
What he is accusing Joe Biden of is something he has actively advocated himself.
Joe Biden hasn't.
To be clear, Joe Biden is not my man.
And I know those in the second, those who are more concerned about their political party
than their principals would say, well he was talking about this special case.
Yeah, but see we don't get to know that because due process comes afterward.
Trump is not a supporter of the second amendment.
He never has been.
He never has been.
Okay, so what's the response from the other side?
The Republicans are going to go after a different group, probably a family planning group.
And they're going to try to use the same legal tactics against them.
At the end of the day, what both sides need to remember is that this is the U.S.
If you successfully defund, ban or whatever, anything, it just becomes a second market.
It's the U.S.
We thrive on less than legal markets.
It's our thing.
The only thing we like more than that is liberating other countries under false pretenses.
This has set up a political battle.
I think it's kind of a good thing because I do think that a lot of supporters of the
second need to know the president's actual views.
And I think it would do them some good to see how the NRA actually spends their money
because it's not spent the way they think.
And I think those would be two byproducts of this.
But there is also a pretty big risk of attorney generals of the various states becoming politically
motivated.
Now, I don't know that that's what this case is.
The NRA, they very well could be guilty of what they're being accused of.
I don't know.
I haven't seen the evidence.
I will withhold judgment until then.
But I will tell you, as a staunch supporter of the second, I do not have a high opinion
of the NRA.
I never have.
Anytime they're needed, they're completely silent because they represent manufacturers.
And manufacturers' biggest clients, best clients, are cops.
Those are the ones that have the most lucrative deals.
So they don't want their group to stand up against any overreach.
The NRA doesn't really represent gun owners, to my way of thinking.
Represents manufacturers.
And I think that if you give money to them, you might want to know how that money's spent.
I think you're going to find out.
I don't think you're going to be happy.
At the end of the day, it's another development in 2020 that's going to have long-lasting
implications, and we're going to have to ride it out.
But I think the benefits on this one may outweigh the negative sides.
We'll have to see.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}