---
title: Let's really talk about the laws of politics and getting around them....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Vmd3bTyRmWc) |
| Published | 2020/08/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how laws, like Murphy's laws of combat, can be applied to politics for those wanting to be politically active beyond social media.
- Emphasizes the importance of understanding that allies, not enemies, can sometimes be the biggest challenge in grassroots organizing due to differing views or mistakes.
- Advises grassroots organizers to acknowledge their vulnerability and not to see themselves as invincible.
- Stresses the need for adaptability in plans because no plan survives first contact and there is no such thing as a perfect plan.
- Encourages activists to believe in their cause genuinely rather than seeking recognition or medals.
- Warns about established roads always being mined in politics, indicating the need for innovative strategies rather than following traditional paths.
- Caution against staying when someone suggests "watch this" at street events, as it signals potential escalation.
- Reminds activists not to dismiss diversions as unimportant, as they might be part of a larger strategy.
- Mentions that experience is gained just after it is needed, urging persistence despite initial failures.
- Points out the significance of clear communication in slogans and messages to avoid misunderstandings.
- Acknowledges that success often happens unnoticed, while failures are magnified in public, especially in politics.
- Differentiates between professional predictability and amateur unpredictability, advising flexibility in dealing with both.
- Concludes by stressing the importance of community networks in activism, stating that change cannot be achieved alone.

### Quotes

- "Friendly Fire isn't. Your allies can be more devastating to you than whatever you're working against."
- "No plan survives first contact. Be adaptable."
- "You're not Superman. Keep that in mind."
- "Cavalry is not coming. It's just you."
- "Success occurs when nobody is watching."

### Oneliner

Beau shares insights on applying Murphy's laws to politics, stressing adaptability, genuine belief in causes, and the power of community in effective activism.

### Audience

Community organizers, activists.

### On-the-ground actions from transcript

- Build a strong community network committed to making your community better (suggested).
- Form a team with friends dedicated to supporting each other and working towards common goals (exemplified).
- Acknowledge the importance of allies and maintaining unity within the group (implied).

### Whats missing in summary

The full transcript provides detailed guidance on navigating politics through the lens of Murphy's laws, offering valuable insights for aspiring activists.

### Tags

#Activism #CommunityBuilding #GrassrootsOrganizing #PoliticalEngagement #Adaptability


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
some laws. Not real laws, figurative laws. And how they apply outside of the world they
were created in. This video is mainly for people who want to become politically active,
and by that I mean off of social media. Who want to get out and influence things in the
tangible world. That's not to downplay those people who use their social media accounts
to influence things. That is incredibly important and it needs to go on. But a lot of people
want to get out from behind their computer screen. And there are some laws generated
by a guy named Murphy attributed to him. Murphy was a grunt. Murphy has more than a hundred
laws attributed to him. It didn't actually come up with all of them. In fact, nobody
really knows if Murphy was a real person. But Murphy's laws of combat are transferable
to politics. Why? Because war is a continuation of politics by other means. Class fits. A
lot of the rules overlap because they're the same thing. We've talked about it on this
channel how there are some strategies that are used in war that are really just PR campaigns
with violence. It's politics. Okay, so if you're a grassroots organizer you need to
understand that friendly fire isn't. Why is that important? Your allies, those people
on the same team as you, can be more devastating to you than whatever it is you're working
against because they may have a slightly different view so they engage in infighting. Or they
just may make a mistake, scramble messaging, whatever. We've talked about that bus analogy
before on this channel. If you and another person get on a bus in Florida and you're
headed to California and they're only headed to Texas, y'all can ride with each other.
Y'all can roll out together. Eventually one of you is going to get off the bus. But until
then you're headed the same direction, you're on the same team. And that needs to be something
that everybody keeps in mind. You are not Superman. Take note Marines. That's the law.
Take note street activists. A lot of the stuff you engage in you can actually get hurt doing.
You're not immune, you're not Superman, you're not superwoman. Keep that in mind. A lot of
the stuff that goes on is dangerous. No plan survives first contact. And this is also coupled
with there's no such thing as a perfect plan. What this means is that you have to be adaptable.
You know, everybody has this idea of how their fundraising campaign or their signature drive
or their demonstration, their public event. They have an idea of how it's going to go.
It's not going to go that way. It never does. You have to be adaptable. Otherwise you will
become overtaken by events and will fail. Your plan will not succeed. Not in its first
draft. Promise. Thousands of veterans earn medals for bravery every day. Some even get
awarded. What that means is that you can't do this. You can't become an activist. You
can't get out there and get into politics for recognition. Because odds are you're not
going to get it. You have to truly believe in the cause. You have to be in it for that.
That's why you're going to be brave. That's why you're going to take the risks. That's
why you're going to dedicate yourself to it. Not for social media clout, not for recognition,
but to actually get something done. If you're in it for medals, I would just like to remind
everybody most medals of honor are handed to a family member. Recognition isn't that
important. Winning is. Decisions made over your head are seldom in your best interest.
Every grassroots group, every community network, every political cause that even remotely begins
to be successful, a major political party will try to co-opt that message. Sometimes
that's good. Sometimes that's the goal, is to get a major political party to pick up
your message. But what you have to remember is those who are over your head, those that
outrank you, those that are part of those political machines, most of them are not in
it for the cause. They're in it for recognition. They're in it for power. Which means when
they get behind closed doors, they don't really care about you. You become a chip. And they
will cash you in whenever it suits them. This is important for people to understand, because
if you understand this, you are less likely to succumb to partisan politics and thinking
that one side of the aisle or the other is your friend. Most aren't on either side. Established
roads are always mined. Established roads are always mined. Fact. In politics, what
this means is that you can't do it the same way. If you're out there in the community,
you can't use the tactics that have been used before. And people try to encourage you to
do that. Ignore them. Well, they could do it the way Martin Luther King did. No, they
can't. The establishment's already seen that. They've got defenses against it. They have
checkpoints up. You have to do it differently if you want to be effective. You can follow
the same general idea, but you can't use that same road. It will not work. You'll walk into
an ambush. When the warrant officer laughs and says, watch this, leave. This is mainly
for people who are at street events. When the person in a mask says, watch this, leave.
Odds are you don't want to be around. That's typically when things are going to escalate.
There are people who are willing to take things to a more drastic level than you. You don't
want to be around. If that isn't you, if you're not the one saying, watch this, and laughing,
you need to remove yourself. It's better for everybody, everybody involved, if people who
are not involved vacate and they're not there. The diversion you're ignoring is the main
attack. We hear this a lot. Well, that's just a distraction. That's just a diversion from
this issue over here. The establishment can do a whole bunch of bad things at one time.
They really can. They can multitask. Most times that diversion, that distraction, is
just something else they want to accomplish. And they know that people are overwhelmed.
There's a lot going on. So they can't focus on everything at once. It's probably not a
diversion. It still needs to be addressed. Yes, prioritize. Make sure you are fighting
the fights that not only need to be fought, but those that are big enough to matter and
small enough to win. But don't discount something that the establishment is doing, that your
opposition is doing, simply because it happened at an inopportune time. Because you'd rather
focus on something else. Filled experience is something you don't get until just after
you need it. This goes for everything. This goes for street events, public speaking, fundraising,
signature drives, whatever, community networking in general. You're going to fail in the beginning.
You are going to mess stuff up. You get better each time. Don't give up. Every command that
can be misunderstood will be misunderstood. In the political world, this really applies
to slogans. If your slogan isn't clear, if your talking points aren't clear, people will
misunderstand them because they want to. It's something you need to be aware of, not necessarily
avoid. You know, everybody in the country knows, really knows, what Black Lives Matter
means. There's no real debate over that. Nobody's confused over it. They just pretend like they
are. They're misunderstanding it on purpose. I would suggest that in some cases that's
okay. I would suggest in that case, it's been incredibly beneficial because it keeps people
talking about it, discussing it, and debating it. And many activities, when you're talking
about politics out in the real world, grassroots stuff, community stuff, getting attention
is the main thing. Keeping the conversation going is the main thing. So, I don't know
that you would want a super clear slogan all the time, but you want one that can be easily
explained. Success occurs when nobody is watching. Failure occurs in front of the general. In
politics, failure occurs on prime time. If you're setting up rallies, and you have 100
days of rallies where everything went fine, everything was peaceful, everything was great,
there were no issues whatsoever, and you have one that gets out of hand, which one makes
the news? Your failures will be broadcast. Accept it. Accept it and learn from them.
Don't get mad because even when there's a failure, it's getting media attention. Even
a failure helps keep that conversation going. And in a lot of cases, that's really the goal.
Professional soldiers are predictable, but the world is full of amateurs. One that is
incredibly important in both settings. When you are dealing with professional people,
you can predict them. You've got a pretty good idea of what they're going to do because
they're trained. When you're dealing with amateurs, you have no clue what they're going
to do. And this goes a whole bunch of different ways. One, you need to be aware of it so you
can embrace it at times. If you have a plan, and it's just not getting any traction, and
somebody who knows nothing about this, who just showed up, is like, hey, why don't we
try this? And it completely doesn't make sense. It's unexpected. It just, it seems ignorant.
Odds are that's going to work because the other side isn't expecting it. You also need
to be prepared for the time when everything's going great. And one of the people who just
started, you know, getting out there and just became politically active, talks to the media
and says something that can discredit your whole movement. Professional soldiers are
predictable, but the world is full of amateurs. The last one, the one that is most important,
cavalry is not coming. It's just you. If you're going to get out there and engage in community
building, grassroots stuff, you're on your own. Odds are, despite internet rumors, nobody's
coming to fund you. Nobody's going to write you a big check or rent you buses. It's just
you, and you've got to figure it out. So we're going to take a quick break and come back
and we're going to give an idea of how all of this can be exploited. How these laws,
you can turn this to your favor. Okay, so you got a good idea about the laws now, process
that. You can't do it alone. You need a community network. You need a team. People talk about
this, ask about it. It's you and your friends that just make the overt commitment to making
your community better. That's it. That's your only goal. Don't organize for any other reason
in the beginning. Just to make your community better and to help each other out. When one
of you needs something, the other one is going to help. And you make it a commitment. You
make it formalized beyond just your normal friendship. And you start there. It'll grow.
There will be more people that want to get in on this as you go. But if you do that,
and you get that community network put together, a lot of these problems, you don't have to
worry about anymore. The bus analogy, Friendly Fire, you're all on the same team and you
know it. You're going to work together. You don't have people trying to escalate things
and get past what you're willing to do. You're adaptable because you have a whole bunch of
different people with different skill sets. You have experience in different areas. All
of the problems that get raised by these laws are solved with a good team, with a good group
of people, a good network. And then it becomes easy because it is the, it's the idea that
it's you guys, it's your group, it's your team against the world. You will have each
other's back. A sprit de corps will form. You will become a unit and you will be a whole
lot more effective. You can't do all of this on your own. You need a group. If you want
to get out there, you want to make real change, you cannot do it alone. And the first step
to having a team to back you up is building that team. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}