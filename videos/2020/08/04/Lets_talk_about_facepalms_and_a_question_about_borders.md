---
title: Let's talk about facepalms and a question about borders....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=P9e7OpWkbUc) |
| Published | 2020/08/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of open borders and challenges the idea that it is impossible or hard to understand.
- Points out how we limit our own thinking by not recognizing things that are already around us.
- Uses the example of traveling within the United States to illustrate the concept of open borders.
- Emphasizes the need to address inequality globally before implementing open borders on a larger scale.
- Suggests that open borders between countries like the US and Canada could be implemented successfully.
- Addresses concerns about the economic impact and entry-level job availability with open borders.
- Encourages imagining a world without defined borders and giving freedom a chance.
- Urges to start thinking beyond national identities and flags to envision a better world.

### Quotes

- "We've lived in it. It exists."
- "We just don't think of it that way because we've limited our own thought."
- "Let's give freedom a chance."
- "We just don't think of it that way because we define ourselves more as Americans than Floridians or Georgians."
- "It's easy to imagine if you try."

### Oneliner

Beau explains the concept of open borders by challenging limitations in thought and urging to give freedom a chance.

### Audience

Global citizens

### On-the-ground actions from transcript

- Advocate for addressing global inequality issues as a first step towards implementing open borders (suggested)
- Encourage regional collaborations and agreements, like between the US and Canada, to facilitate open borders (exemplified)
- Challenge limitations in thought and national identities by envisioning a world without defined borders (implied)

### Whats missing in summary

In-depth exploration of the economic, social, and political implications of implementing open borders globally.

### Tags

#OpenBorders #Freedom #GlobalInequality #NationalIdentities #Imagination


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about one of my favorite questions.
It's a question I've answered numerous times
and I still love answering it because
it's easy to answer and it illustrates not only
the point in question but a wider one
about how we limit our thought.
We block ourselves in
and sometimes we can't recognize things
that are all around us.
We will proclaim something as impossible
or hard to understand
when it is
easily demonstrated
that it is possible.
So I got a message and I'm going to read the whole thing
because it's funny.
Okay, so
Beau,
I know you said you don't advocate for it now
but I'm very curious about something you said.
I get your end goal is just people doing the right thing all over the world
but to get to that point we'd have to, by your own admission,
go through a phase of open borders.
I'm just not sure how to picture such a world.
Different countries have different laws.
They have different taxes.
They have different governments and concerns.
These legal jurisdictions
would have to still exist to represent the people
in theory.
Nice addition.
How can someone travel between these jurisdictions
without that jurisdiction clearing them?
How would people traveling know the different laws?
What happens if you break a law in one jurisdiction and then go to another?
One of the reasons I love your channel is because you make the abstract concrete.
I love freedom and I want to give it a chance. I want to understand
how this is possible.
I need one of those weird analogies
that makes me face palm
for not seeing it before.
So the concerns,
the different laws,
different taxes,
extradition,
all that stuff.
I mean that's a concern.
You'd need an agreement.
You described it. There would be an agreement governing
the different nation states
that have opened their borders.
It's really that simple.
That's probably still pretty abstract.
So let's do it another way.
You already live there.
You live in this society.
If you live in the United States,
you live in this society.
When you travel from Tennessee to Georgia,
you don't go through a checkpoint.
But we've limited our thinking
because we think of it in the sense of
the United States is.
Rather than the United States are.
United States, plural.
Plural states that are united.
They have a governing agreement,
the Constitution.
Open borders is already here.
We live in it.
You get taxed where you work.
I guess maybe in California you get taxed where you live.
I think that varies.
There's extradition.
You know that when you go to Colorado or Nevada,
you can do things that you can't do in other states.
This world that seems so impossible,
it's already here.
We've lived in it.
It exists.
When you're talking about it on a global scale
or a regional scale,
it's just adding new states.
Adding more areas,
more jurisdictions to that agreement.
That's really it.
This isn't a groundbreaking or controversial thing.
We already live in it.
We just don't think of it that way
because we've limited our own thought.
We've defined ourselves so much by that flag
and those lines on a map
that we can't imagine a better world.
We can't imagine a world without it.
We should probably start.
We should probably start.
Is this something that can be done
with a high rate of success today?
Not really.
We need to address some inequality issues
around the world first.
That would be step one.
Now could it be done regionally,
like between the US and Canada?
Yeah, tomorrow.
Wouldn't change a thing.
It wouldn't matter at all.
There wouldn't be a flood of people
heading from Canada into the US.
There might be from the US to Canada though.
But even if that happens,
which is a concern when people talk about it,
understand that when more people show up,
while they produce and consume,
it helps the economy.
It doesn't hurt it.
Yeah, in the beginning,
there would be a run on entry-level positions.
That would happen,
unless we address inequality first.
But the idea that this is impossible
or that it's hard to imagine,
it's really easy to imagine.
We already live there.
We probably face bombing.
But if it makes you feel better,
I have been asked this question
literally a hundred times.
It's something in American psychology,
we just don't think of it that way
because we define ourselves more as Americans
than Floridians or Georgians.
But we already have states with open borders.
So it's possible.
It's easy to imagine if you try.
Let's give freedom a chance.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}