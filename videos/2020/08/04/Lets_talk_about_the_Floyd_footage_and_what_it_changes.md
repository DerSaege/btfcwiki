---
title: Let's talk about the Floyd footage and what it changes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qm6CMkIkYIY) |
| Published | 2020/08/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the need to talk about managing situations better in light of recent events.
- Expressing concern over individuals supporting actions that deviated from best practices.
- Demonstrating a scenario with an assistant, Zed, who is alleged to have done something wrong.
- Explaining techniques like inducing compliance through urgent commands and physical methods to get someone in a car.
- Emphasizing the importance of not using unnecessary force once a suspect is subdued and cuffed.
- Challenging the notion that non-compliance justifies excessive force.
- Asserting that using force beyond what is necessary is unacceptable, and individuals should be held accountable if they do so.
- Concluding with a reminder that the footage of an incident does not change the facts of the case.

### Quotes

- "Do it now."
- "If you do it the same way, you need to turn in your badge."
- "That footage changes nothing, as far as this case."

### Oneliner

Beau addresses managing situations better, demonstrating tactics to induce compliance, and condemning excessive force in a thought-provoking video.

### Audience

Community members

### On-the-ground actions from transcript

- Practice de-escalation techniques when dealing with challenging situations (exemplified)
- Advocate for accountability and adherence to best practices in law enforcement (exemplified)
- Educate others on the importance of using only necessary force in law enforcement interactions (exemplified)

### Whats missing in summary

Real-life examples and practical demonstrations of techniques for managing situations better in law enforcement interactions.

### Tags

#CommunityPolicing #DeEscalationTechniques #Accountability #LawEnforcement #UseOfForce #Justice


## Transcript
Well howdy there internet people, it's Bo again.
I guess it's safe to say that today is going to be a little bit different.
Today we're going to talk about bending and not breaking.
We're going to talk about how to manage situations a little bit better.
I think everybody saw what came out last night and everybody's got an opinion on it and that's
fine.
My concern arises when I see a whole bunch of people with blue lines for profiles saying
that they would have done the exact same thing, knowing the outcome and knowing, at least
you should know, that best practices weren't really used.
I'm a layman.
I don't know much about this stuff, but I read a little bit about it.
So we're going to talk about what I read.
Now maybe this stuff is all out of date and none of this stuff will work anymore.
Something has happened and none of this stuff is within policy.
Point out, obviously at this point, don't try this at home.
This is for informational purposes only.
I'm demonstrating something here.
I have an assistant to help me.
This is Zed.
Zed's done something wrong.
Zed needs to get in the car.
He's alleged to have done something.
So you do what you're supposed to, right?
Cut them up, double lock them.
Let's make sure we do everything right here.
Follow policy.
Double lock them.
Mr. Zed, get in the car, please.
Do it now.
Do it now.
That statement that's in pretty much all the manuals, all the videos, because psychologically
it induces compliance because it makes it sound urgent.
Do it now.
I'm pretty sure that people are supposed to say that, but again, I don't know.
Now, you have him here.
Mr. Zed, get in the car, do it now.
Mr. Zed, Mr. Zed, he has a phobia.
He doesn't want to get in the car.
You're not really resisting.
He's just not really complying.
See, the funny thing about the human body, now if we're being realistic, he's a big guy.
So you're going to come underneath.
You're going to grab the chain or the hinge on those cuffs.
Get here, get really tall, you've got to come under the arm.
You lift.
See the amazing thing about the human body is that when you hit a certain point, it doesn't
bend anymore.
But somewhere else, well, it's crazy.
Magic.
I would imagine somebody in this position would be a whole lot easier to get in the
car.
Let's say they kick back, you're actually out of the way.
Let's say you just can't get them because they're too big.
You're already in position.
Comma peroneal strike.
That point should be pretty easy.
If you have a whole bunch of people, you could just lift his legs up and put him in.
But again, I don't know, maybe there's some reason they don't want you doing that anymore.
Perhaps if your dad's like this, car door behind him open, there's a notch right here,
you can feel it right there.
If you reach in and press down, you'll feel your chin want to go down because it hurts.
That would make him sit down.
If he is in the car, and just his legs are out, any of the little procedures I'm about
to show could probably get him to put his legs in.
Or somebody could just offer to roll down the window.
Sometimes that's all it takes.
For some reason though, in our hypothetical situation, somebody decides to open the door
on the other side of the vehicle.
I would suggest that that's ill-advised in almost every scenario.
Mr. Zed ends up laying on the ground.
Now at this point, you're either going to want to wait for an ambulance or get him in
the car.
If you're waiting for an ambulance, you don't have to climb all over.
In fact, I'm pretty sure it's in the books not to do that.
But if you're still trying to get him in the vehicle, get on your knees beside him.
Right here behind the ear, there's a little notch right there.
If you apply pressure to this side of the head and press in there, you can probably
get compliance.
You go like this, right there behind the ear.
If it doesn't work, you know what, your hand's already in position, go like this, come under
the bridge, and lift.
Go ahead and try and place your hand on the back of your head right here and lift up.
Let's say that this is the tenth of a percent of a population that this technique, that
neither one of these techniques will work on, you're also in the position to go right
under the jaw.
All of this works together.
You can get him back to the door, in which case the whole thing starts over again.
While you're doing this, by the way, you should be saying, Mr. Zed, get up, do it now, do
it now.
Not stop resisting, because that doesn't help them make a decision.
You're telling them to stop something.
You want to be proactive and get them to do something.
And then you get back to where you were at.
Cuss, lift, in the car.
A lot of people seem to think that video somehow exonerates those involved because, well, he
resisted.
He wasn't compliant.
I don't know that I would call that resisting.
Aside from that, I would like to point out that resisting is a charge.
That's a charge that eventually gets resolved before a jury or a judge.
We do not have judges and juries in the street.
That's not how it works here.
Doesn't matter.
Even if he did resist, it doesn't matter.
Once the suspect, once Mr. Zed, is laying on the ground cuffed, there's no reason to
continue using force.
If you would do it the same way, you need to turn in your badge.
Again, maybe some of this is outside of policy now.
I don't know.
I mean, I guess, you know, they don't want anybody hurt, so they wouldn't want these
things used because maybe they would hurt somebody or something, as opposed to what
happened.
That footage changes nothing, as far as this case.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}