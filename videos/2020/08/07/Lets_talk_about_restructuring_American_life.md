---
title: Let's talk about restructuring American life....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=E3tWByr3sd4) |
| Published | 2020/08/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau questions if it's time to restructure the American way of life due to a lack of response at the federal level to serious issues.
- He suggests that the structure of American life may be the root cause of public health issues, citing inequality, lack of healthcare, low wages, and inadequate education.
- The cycle of despair and crime, exacerbated by the war on drugs and militarized police, has led to a climate of fear and division in the country.
- Politicians exploit this fear to maintain power, leading to the spread of disinformation and falsehoods that endanger lives.
- Beau criticizes politicians like Kevin McCarthy for contributing to the current situation by enabling the president's actions and suppressing the vote.
- He argues that appealing to tradition is not a valid reason to resist change and that it's time for America to evolve towards a fair and just society.
- Beau calls for a restructuring of the American way of life to fulfill the promises of the founding documents and address the deep-rooted inequality in society.

### Quotes

- "It's time to restructure the American way of life."
- "You can help or you can get out of the way, but it's time for it to happen."
- "Times have changed, you have to as well."
- "There's no upward mobility for people."
- "It's time to move forward."

### Oneliner

Beau questions the need to restructure the American way of life and points out how inequality and systemic issues are at the core of current challenges, calling for a move towards a fairer society.

### Audience

American citizens

### On-the-ground actions from transcript

- Advocate for policies that address inequality and provide better access to healthcare and education (implied)
- Support politicians who prioritize fairness and justice in society (implied)
- Get involved in grassroots movements that aim to bring about positive change in the community (implied)

### Whats missing in summary

The full transcript provides a deep analysis of the systemic issues plaguing American society and the urgent need for structural change to address inequality and create a fairer, more just society.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about restructuring the American way of life.
Whether it's time to restructure the United States, change the way we live on a significant
level and if so, why?
We're going to talk about this because an American politician, Kevin McCarthy, now for
those overseas, he is the leader of the Republicans in the House of Representatives.
He doesn't make a lot of headlines, he tends to keep his head down, but he popped up to
make a tweet.
And to be honest, under normal circumstances, I would agree with what he's saying.
It makes sense.
I'm not sure it applies in this particular situation.
In fact, I think there may be some things that negate the entire idea.
But because of that, that's what we're going to talk about.
So what did he say, right?
He said, Dear Democrat Party leaders, this is an international health crisis, not a political
opportunity to restructure American life.
I get it.
What he's saying here is that if you have a serious issue, that's not the time to exploit
that, to advance your own political agenda.
Yeah, that makes sense.
You wouldn't want somebody's political agenda getting in the way of a response, especially
if it's a serious issue.
The problem is we don't have a response.
There is no response from the federal level, so that doesn't matter.
Doesn't even play into it.
More importantly, I think he's missing a bigger piece of this.
What if the reason we're having a public health issue right now is because of the structure
of American life?
Stick with me on this.
What if there's a lot of apprehension because tens of millions of Americans don't have health
insurance?
Because there's a massive amount of inequality, so much so that 40 million Americans are on
food stamps and a lot of them could be working full time at 40 hours a week and still qualify
because wages are so low.
Because they make low wages, they end up living in areas that are low income.
Therefore the property taxes are low, which means the schools in those areas aren't really
properly funded, which means their kids don't necessarily get the best education.
This situation creates a lot of despair and crime, so much so that there's 2.3 million
people currently locked up and we have headlines about a guy about to do life for stealing
a pair of hedge clippers.
We all saw the recent headlines about the child getting locked up for not doing their
homework and because we choose to lock people up instead of treat them, we've lost the war
on drugs and the only thing that has brought us is a whole bunch of failure and militarized
police.
Those militarized police are why we have cities burning across the country because most Americans
are tired of living under what seems to be the rule of an occupying army and this has
contributed to a general climate of fear that is exploited by politicians via tweet to stoke
that fear to divide people to help keep themselves in power.
This has worked so well and they have scared Americans so much that Americans are building
a wall on the southern border to cower behind and when the fear mongering fails, the politicians
move to disinformation and outright falsehoods and that has gotten so bad and gone to such
a degree that social media platforms have started suspending and banning political campaigns
because their talking points are so false about such serious issues that it's actually
risking people's lives and because there's so much inequality, there are some people
who didn't get an education that can help them discern fact from fiction and the whole
cycle starts all over again.
Maybe we're having a public health issue because of the structure of American life, hypothetically
speaking of course.
If that was the case, then it would make sense.
It would make sense to try to evolve and move forward and change things.
I would like to point out that this structure was in large part built by people like Mr.
McCarthy and the current situation was certainly exacerbated by his party choosing to enable
the president, not hold him accountable and stand idly by as he attempts to suppress the
vote.
If you want to talk about the American way of life and tradition and everything like
that, you can't simultaneously sit idly by while the president attempts to undermine
the election which is the foundation of American life.
You can't do that.
It's probably time.
If we want to move forward, if we want to have a fair and just society, we're going
to have to make some changes.
Appealing to tradition is bad.
That's the worst reason to do something.
We've always done it this way.
Yeah, that's why we've been in this mess.
Times have changed, you have to as well.
It's time to move forward.
And I will go ahead and let those on Capitol Hill know because I know y'all don't talk
to us commoners, but we don't care who does it.
We don't care if it's Democrats or Republicans or Libertarians or Greens or the people in
the street.
We don't care.
What we know is that you can start at any point in this list and eventually it just
cycles back around and nothing changes because there's that much inequality.
There's no upward mobility for people.
It is time to restructure the American way of life.
It's time to fulfill those promises in those founding documents.
You can help or you can get out of the way, but it's time for it to happen.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}