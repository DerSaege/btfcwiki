---
title: Let's talk about good trouble in Paulding County, Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QM3DT6Fdhkw) |
| Published | 2020/08/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Uplifting story from Paulding County, Georgia about students documenting school reopening.
- Students tally mask-wearing students, revealing lack of safety measures by the school.
- Act of advocacy journalism by students to inform public.
- School suspends students for exposing truth, likened to villains from Scooby-Doo.
- Student Hannah goes on CNN, suspensions lifted, but school fails to take further action.
- School refuses to mandate masks citing enforcement difficulties.
- Students warned school, punished instead of heeded, likely due to political reasons.
- Students' efforts should be encouraged, referenced as "good trouble."
- Uncertainty if school will change and protect students in the future.
- Implication that consequences of school's inaction may lead to sick students.

### Quotes

- "They were warned by their students. Rather than heed the warning, they punished the students."
- "What those students did is impressive. It should be encouraged."
- "You better hope that one of those sick kids isn't her."

### Oneliner

Students expose lack of safety measures in school reopening through advocacy journalism, facing punishment instead of praise.

### Audience

Students, educators, activists

### On-the-ground actions from transcript

- Support student journalists in advocating for safety measures at schools (implied)
- Encourage and empower students to speak up for the public good (implied)
- Stay informed about school policies and advocate for necessary changes (implied)

### Whats missing in summary

The emotions and determination of the students as they face adversity and push for the safety of their peers might be better understood by watching the full transcript.

### Tags

#SchoolSafety #StudentJournalism #Advocacy #PublicGood #Activism


## Transcript
Well howdy there internet people, it's Beau again.
So today we've got an uplifting story out of Paulding County, Georgia.
It's kind of uplifting anyway.
It has its ups and downs. So they've already gone back to school there
in person. Two students decide to document the reopening. I don't know if
they were working together, only one's been named so far.
And they get imagery and they keep a tally
how many students are wearing masks, how many aren't. And they demonstrate very
clearly that the school is not
engaging in what most would consider to be the very bare minimums
to keep the students safe. It's the way it appears.
You know, they gathered
information, they investigated, to put it out there for public consumption
and then they published it. That's journalism. In this case it was in
pursuit of the public good,
advocacy journalism, the best kind of journalism. Good stuff, impressive to be
honest.
The school finds out about it. Do they
recognize and acknowledge the talent they have sitting in front of them?
Maybe try to nurture it and talk to them about,
you know, maybe a school paper or journalism as a career?
Apparently not. Apparently they turned into the villains from Scooby-Doo and were
basically like, I would have got away with it if it weren't for these meddling kids
and suspended them. Then they found out how Twitter works
when Hannah, the student who's been named,
she winds up on CNN doing an interview.
It's now the next day. The suspensions,
well, they've been deleted, they can go back to school on Monday,
and it seems like that's a happy ending. That's the end of the story, right?
But it's not, because it doesn't seem like
the school is going to do anything further to protect the kids.
So it just continues.
The story isn't over yet. I could write the rest of the story now,
because the way this is going to play out is the second that they're spread
in that school, it all comes back up.
And the story is, the school didn't do anything to protect the kids,
they were warned by their students. Rather than heed the warning, they punished the
students.
When they got called out on the punishment, they got rid of the punishment,
but still didn't act.
In fact, they said, well, we can't mandate masks,
you know, it's impossible to enforce address code at a place where we enforce
address code every day.
We just can't do that.
That's the story. I could write it today.
How it's going to play out, unless the school changes.
The sad thing is, they probably won't.
It's probably political, and
the kids are going to end up paying the price for it.
Despite those students' best efforts.
What those students did is impressive. It should be encouraged.
In her interview, she referenced good trouble. Yeah,
that's exactly what it was, and good for you. Good for you.
I hope I'm wrong.
I hope this is the end of the story.
But I've got a feeling it won't be. We'll be hearing about this school again, I'm
pretty sure.
And I can tell you this, if you're part of that school,
if you're part of the administration there, you better hope
that one of those sick kids isn't her.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}