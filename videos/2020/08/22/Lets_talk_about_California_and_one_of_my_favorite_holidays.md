---
title: Let's talk about California and one of my favorite holidays....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MBH2fejaHwc) |
| Published | 2020/08/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of California and a unique holiday celebrated by about 500 people for the past five years.
- Mentions a veteran named Brian who made a thought-provoking statement about the origins of freedoms and rights.
- Describes the unconventional holiday, Think a Criminal Day, on August 22nd, honoring those who were once deemed criminals but later recognized as heroes.
- Explains the current situation in California with over two dozen major wildfires and 300 smaller ones.
- Points out the usual practice of using inmates to fight fires for minimal pay but how half the teams are now unavailable due to social distancing measures inside prisons.
- Notes that due to the lack of inmate firefighters, the governor called in soldiers from the National Guard to assist.
- Raises the question of how many historical heroes had mug shots, linking back to the idea of criminals contributing to positive change.
- Encourages viewers to ponder on the concept shared and wishes them a good day.

### Quotes

- "Therefore, August 22nd is Think a Criminal Day."
- "How many of them had mug shots?"
- "Y'all have a good day."

### Oneliner

Beau introduces the unconventional holiday "Think a Criminal Day" while discussing California's wildfire situation and the involvement of soldiers due to inmate firefighters' unavailability, prompting reflection on the criminal origins of some historical heroes.

### Audience

Community members

### On-the-ground actions from transcript

- Support firefighter relief efforts (exemplified)
- Volunteer for wildfire aid organizations (exemplified)

### Whats missing in summary

The emotional impact and depth of historical figures who were once considered criminals but later revered for their contributions.


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about California and everything going on there.
And we're going to talk about one of my favorite holidays.
A very unconventional, unorthodox, and certainly unofficial holiday that's been around for like
five years and has maybe celebrated about like 500 people. And we're going to talk about a bizarre
situation that has brought that whole thing full circle. But before we do that,
I want you to picture some of your heroes. People who have truly changed the world.
Get them in your mind. Because years ago, a friend of mine, a guy named Brian,
he was a veteran who had gone over and definitely done his part.
He said something and it caught everybody's attention.
He said that while it may be somebody in his profession, a soldier,
who at times protects freedom or protects your rights,
that more often than not, those freedoms or rights were obtained by a criminal.
Interesting concept, right? But it's true. Obviously it doesn't apply to all criminals.
However, from the heretics of old who brought about modern science,
to those who just would not stop doing what they had always done, and therefore today,
you can have the beverage of your choice, to those who might have bent the rules
to make sure that you have the right to vote, to those who stood against some of the greatest
injustices the world has ever known.
They might be heroes today, but then they were criminals.
Therefore, August 22nd is Think a Criminal Day.
What does this have to do with California, right?
California is currently battling more than two dozen major wildfires.
And 300 smaller ones. Under normal circumstances, they have a resource that they can tap into.
Inmates. Inmates getting paid two to five dollars a day and a dollar an hour while they're actually
out there working against the fire. However, because you can't just go out and do nothing,
however, because you can't really socially distance inside, half of those teams, they're down.
They're not out there working. And that put the state of California in a pretty tight spot.
So much so, that when the inmate heroes were unavailable,
the governor called out soldiers, the National Guard, to pick up the slack.
It all kind of came full circle.
So, think back to those heroes that you pictured in the beginning.
How many of them had mug shots?
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}