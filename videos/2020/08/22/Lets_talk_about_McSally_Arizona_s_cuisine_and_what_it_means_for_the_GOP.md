---
title: Let's talk about McSally, Arizona's cuisine, and what it means for the GOP....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YydF2r2Krg0) |
| Published | 2020/08/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Martha McSally's plea for donations in the Arizona Senate race suggests GOP fundraising struggles.
- McSally's message implies a need for resources to compete but at the expense of individuals' meals.
- The plea for donations, even as low as a dollar, raises concerns about the party's financial situation.
- The senator's message may indicate a desire for new leadership among the populace.
- Beau criticizes the plea, pointing out that it asks those without means to sacrifice for politicians.
- Comparing the situation to Marie Antoinette's offer of cake, Beau suggests this is a negative sign for the GOP.
- Beau notes that McSally's opponent is an astronaut, questioning the necessity of such fundraising tactics.
- Criticism is directed at the idea of sacrificing meals for political campaigns, especially for billionaires' benefit.
- Beau implies that voters in Arizona should carefully weigh their choices without outside influence.
- The message concludes with Beau leaving it to viewers to decide on the implications of the situation.

### Quotes

- "You know, politicians have a long, illustrious history of taking food off of the working person's table, but typically they wait until after the election to do it."
- "This is a plea to those without means, to go without so a politician can get into power."
- "I'd point out that even Marie Antoinette offered cake."
- "If you want to skip a meal so billionaires can get tax cuts, that's on you."
- "Anyway, it's just a thought. I have a good night."

### Oneliner

Senator McSally's plea for donations in the Arizona Senate race reveals GOP fundraising struggles, asking individuals to sacrifice meals for political power.

### Audience

Voters in Arizona

### On-the-ground actions from transcript

- Vote thoughtfully in Arizona Senate race (implied)

### What's missing in summary

The full transcript provides additional context on the GOP's fundraising challenges and how it may impact the political landscape in Arizona.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the political cuisine being offered out in Arizona, because
voters have definitely been handed something to chew on.
And what this statement means for the GOP and the message it's sending to the rest of
the United States.
A senatorial candidate out there, Senator Martha McSally said, and I quote, we're doing
our part to catch up, you know, to get our message out, but it takes resources.
So anybody can give, I'm not ashamed to ask, to invest.
If you can give a dollar, five dollars, if you can fast a meal and give what that would
be, wow, you know, politicians have a long, illustrious history of taking food off of
the working person's table, but typically they wait until after the election to do it.
This statement, this message, shows that the Republican party is having real issues fundraising
for Senate races.
This could be indicative of a populace that wants new, wants some kind of leadership.
And when you really think about what's being said here, if you don't have a dollar to spare,
if you don't have five dollars to spare, skip a meal.
This isn't a plea to those who have means.
This is a plea to those without means, to go without so a politician can get into power.
I'd point out that even Marie Antoinette offered cake.
This is probably not a good sign for the GOP, not at all.
And while I don't endorse candidates, I would like to point out that her opponent is a literal
astronaut, fairly certain that that person could figure out some way to run their campaign
without asking people too fast in his honor.
But you know, it's a decision for the people of Arizona to make.
You don't need outside influence there.
If you want to skip a meal so billionaires can get tax cuts, that's on you.
Anyway, it's just a thought. I have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}