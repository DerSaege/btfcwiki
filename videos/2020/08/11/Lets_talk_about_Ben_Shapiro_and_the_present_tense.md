---
title: Let's talk about Ben Shapiro and the present tense....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8EqHoPqfy1g) |
| Published | 2020/08/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses Ben Shapiro's controversial commentary on a music video, expressing embarrassment on Shapiro's behalf.
- Ben Shapiro reviewed a suggestive music video by reading the lyrics aloud with feigned moral outrage.
- Beau criticizes Shapiro for questioning if the video represents what feminists fought for, pointing out the hypocrisy in his argument.
- He argues that feminism is about allowing individuals to express themselves freely and be independent, full, and rounded human beings.
- Beau condemns societal pressures that dictate how women should behave and be perceived, contrasting it with men's freedom from such expectations.
- He questions Shapiro's selective defense of rights and objectification, suggesting a double standard in his views.
- Beau challenges Shapiro to recognize and respect women's rights to be who and what they want, irrespective of societal expectations.
- He concludes by noting that facts do not care about feelings and hints at the potential impact of embracing true equality.

### Quotes

- "Is this what feminists fought for?"
- "But anytime somebody from a group that isn't me and him, the way we look, you know, anytime somebody from a different group tries to use that system and use the same things to get ahead, well he's right there to slap them down."
- "Ben doesn't like feminism. Got it."
- "They have rights, and that's a fact."
- "And facts do not care about your feelings."

### Oneliner

Beau criticizes Ben Shapiro's views on feminism and calls for true equality based on individual rights and freedom of expression.

### Audience

Social commentators

### On-the-ground actions from transcript

- Challenge societal pressures and double standards (implied)
- Support and uplift voices advocating for true equality (implied)
- Encourage respect for individual rights and freedom of expression (implied)

### Whats missing in summary

Deeper insights into the nuances of feminism and societal expectations regarding expression and autonomy.

### Tags

#BenShapiro #Feminism #Equality #FreedomOfExpression #DoubleStandards


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Ben Shapiro reviewing a music video.
For real.
If you're on Twitter I know you're probably expecting me to talk about his secondary commentary
on Twitter.
I'm going to leave that to people with a stronger comedic background than myself because I'm
certain those videos are being made as we speak.
Because wow.
I don't know if he's embarrassed but I'm embarrassed for him.
That was something else.
If you have no idea what I'm talking about, Ben Shapiro reviewed a music video.
And by reviewed I mean he read the lyrics aloud while feigning moral outrage.
It's pretty funny.
Admittedly it's an incredibly suggestive video.
It pushes the envelope.
Fact.
Fact.
Welcome to art.
It's what happens.
But when he gets done being all in his feelings about the lyrics he asks a question that always
bothers me.
Is this what feminists fought for?
First it's fight, present tense, because of people like Ben.
Because of people like Ben.
See they have a right to make this video.
Same first amendment right that he and I use every day.
I don't ever remember being asked if I was living up to a standard that was set.
If I was being a good little boy and doing with my rights what somebody else thinks I
should.
It's never happened.
Not to me.
Probably not to Ben.
But it happened to them because there's still that idea that we get to have some kind of
paternalistic control.
He goes on to say that feminism wasn't about making women independent, full, rounded human
beings.
I would suggest that most people, I'm not so sure about Ben anymore, know that that
part of a person's life, the bedroom, that that's part of the human experience.
And that expressing themselves about that in whatever manner they choose because they
don't have to justify it, that does make them independent, full and rounded.
And see that's what it's about.
It's still about control.
That's why feminism is present tense.
Because while legally they have the right, there's that societal pressure, that systemic
idea that they have to behave like a little princess.
They have to do what they're told.
They have to fit into a mold.
And if they step outside of that and they appear promiscuous, well then it's time for
somebody to come in and tell them what's what.
Because they're not doing what they should with their rights.
I wonder what would happen if somebody went to Ben and told him that, well, your Second
Amendment right, I don't really think you need this particular one.
I don't think you should use this.
I wonder what he'd say.
That's the thing about rights.
They exist.
Legally they exist right now.
There's no law stopping it, right?
But there's that societal pressure.
I'm willing to bet that I could go through Ben's videos and his commentary and I doubt
I would find him talking about a male model, a male movie star objectifying himself.
Because that standard doesn't apply to us.
At the end of the day, that objectification argument can be made by some people, but not
Ben.
Not Ben.
Because his whole thing is defending the very system that requires it.
That commodifies people, their bodies, their time, everything.
I'm sure that in Ben's mind, he and I are different than those women because we use
our mouths instead of our chests.
I don't think there is a difference.
I don't think there is a difference.
At the end of the day, we are using assets that we may have to try to exist in this system,
the system that he constantly defends as fair and just and how it should be.
But anytime somebody from a group that isn't me and him, the way we look, you know, anytime
somebody from a different group tries to use that system and use the same things to get
ahead, well he's right there to slap them down.
I don't know, you're not really living up to what I think you should do with your rights.
Ben doesn't like feminism.
Got it.
Cool.
The really cool part about it is that if Ben wanted to, he could help stop feminism.
All he has to do is remember one thing, and that's that women have to be two things.
Who and what they want.
They have rights, and that's a fact.
And facts do not care about your feelings.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}