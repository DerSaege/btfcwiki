---
title: Let's talk about a message to the Biden campaign....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ACkEmdWJzFE) |
| Published | 2020/08/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Moderate conservatives want to see Trump lose, not necessarily support Biden.
- They are willing to cross over to send a message against Trumpism.
- The assault weapons ban in the Democratic Party platform is a deal-breaker for them.
- Poor people could be disproportionately affected by the gun ban due to taxes.
- They believe in the right to defend themselves, especially marginalized communities.
- Trump's mishandling of public health, civil unrest, and economy reinforces their belief in gun ownership.
- Winning the election is not as critical to them as sending a message against Trumpism.
- Moderate conservatives view the election as a way to reject Trumpism, not just a choice between Biden and Trump.
- Beau urges the Biden campaign to reconsider the assault weapons ban to secure these votes.
- Dropping the gun ban could help Biden win in numbers significant enough to send a message.

### Quotes

- "They want to send a message to their neighbors, to the marginalized people who have been directly impacted by this administration."
- "If you want these votes, you got to drop this."
- "You may win without them, but are you going to win in numbers big enough to send that message?"
- "This isn't an election between Biden and Trump."
- "They will not cross over with this ban on the table."

### Oneliner

Moderate conservatives want to reject Trumpism and send a message through their votes, urging the Biden campaign to reconsider the assault weapons ban to secure their support.

### Audience
Campaign strategists

### On-the-ground actions from transcript

- Reassess the assault weapons ban in the Democratic Party platform (suggested)
- Engage with moderate conservatives to understand their concerns and motivations (exemplified)
- Tailor messaging and policies to appeal to voters beyond traditional party lines (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the mindset of moderate conservatives and their motivations for voting, urging political campaigns to prioritize messaging that resonates with their values.

### Tags

#ModerateConservatives #ElectionMessaging #AssaultWeaponsBan #RejectTrumpism #PoliticalCampaigns


## Transcript
Well howdy there internet people, it's Beau again.
So today we have a message directly to the Biden campaign.
To start off with, I'm that resident of a swing state.
I'm the person you're reaching out for.
My friends, a whole lot of them, are those moderate conservatives.
Those people that you have been doing everything to get on your side.
None of us look at Biden and say, hey, that's my man.
He's gonna represent my interests.
I'm excited to go vote for him.
None of them, okay?
But they all want to see Trump lose.
They don't wanna go vote for Biden to see Biden win.
They don't really care about a Biden administration.
They wanna see Trump lose.
And in big numbers, those moderate conservatives who are willing to cross over.
They see themselves as, they're not, but
they see themselves as fiscally conservative and socially liberal.
That's what they see.
That's who they think they are.
They care about civil rights.
That's why they're willing to cross over.
They want to send a message.
That message is that Trumpism and the racism,
sexism, bigotry, the overt nationalism,
all of that stuff that goes along with it is bad.
They're crossing over to send a message to their neighbors, mainly.
Has nothing to do with who takes power.
They want to send a message to their neighbors,
to the marginalized people who have been directly impacted by this administration.
They want them to see America reject Trumpism on every level.
They want a big win for Biden at the polls, not because they support Biden,
but because a crushing defeat for Trump will send that message,
not just to their neighbors, but also to the Republican Party saying, hey,
don't run another candidate like this.
That's what they want.
And the Biden campaign was doing great with them until about a week ago, right?
And then the numbers started sliding.
And you can't figure out why.
You have no clue.
Trump's still doing a horrible job at everything.
So why are the numbers changing?
It was about this time that everybody realized the Democratic Party platform
included that assault weapons ban.
And the assault weapons ban currently up on Capitol Hill
is way more than just an assault weapons ban.
It pretty much bans any semi-auto design in the last 100 years.
And it has some cool little exemptions to make sure that rich folk can keep
their guns.
But poor people end up getting disarmed because they can't pay for the taxes.
These are people who are crossing over to help the marginalized.
That's their motivation.
Marginalized people are typically lower income.
This gun ban, which they're going to be against to begin with,
is directly impacting the very people they're crossing over to help.
This is a losing proposition.
They won't do it.
They're not going to cross over for this.
To them, ideologically, they're those bootstrap people, right?
The low income people, the marginalized people,
they need to be able to defend themselves more than anybody.
That's the way they look at it.
And all of the stuff that you're talking about that Trump has handled horribly,
the public health issue, the civil unrest,
we know he's going to just completely botch the hurricane response,
the economy, the supply chain, all of this stuff,
to them, do you know what you need?
A gun.
If it gets really bad, you need a gun.
That's their whole ideology.
They're willing to cross over to help a certain group of people.
And you want to deny that group of people a gun, which to them,
it's a centerpiece of their ideology.
It doesn't matter if you think it's right.
You're not going to change their opinion on this.
Not in the time you have left before the election.
You can have these votes, but you have got to come off this.
And the important thing here is not putting the Democratic Party in power.
They don't care about that.
They care about sending a message, and so should you.
This isn't an election between Biden and Trump.
Most of the country isn't viewing it that way.
Those who are being active in the process, they care about sending a message.
They care about rejecting Trumpism.
If you want those moderate conservatives that you sacrificed the left for,
you just threw them under the bus.
If you want them, you have to come off this band.
You may win without them, but are you going to win in numbers big enough to send
that message, or are you going to still have to continue this
cultural fight that's going on in this country that is leading to some really bad stuff?
And I know you're saying, well, they should just cross over anyway and
forget about that one issue.
Sure, that's going to happen.
That's about as likely as somebody who supports family planning voting for
a Republican.
It's not going to happen.
If you want these votes, you got to drop this.
You may be able to win without them, but you're not,
you probably won't be able to send the message.
You're probably not going to get enough votes to send the message to the country,
to those people who are marginalized, to those people who are impacted,
and to the Republican Party, that this is unacceptable.
You got to come off this if you want those votes.
They will not cross over with this ban on the table.
This is make or break for them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}