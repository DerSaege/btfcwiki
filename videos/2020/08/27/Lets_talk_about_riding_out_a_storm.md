---
title: Let's talk about riding out a storm....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VEwnKqSZuYI) |
| Published | 2020/08/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides tips for people not interested in helping during a storm but wanting to make it through safely.
- Emphasizes the importance of preparation and knowing your network.
- Advises on staying mobile and being ready to move at all times after the storm.
- Suggests seeking shelter in people's residences rather than abandoned warehouses.
- Recommends moving together with your group and looking unassuming to avoid drawing unwanted attention.
- Stresses the importance of focusing on your circle during the aftermath of the storm.
- Mentions the scarcity of emergency services post-storm and the need to rely on your own resources.
- Encourages being cautious when encountering groups looking to take advantage of the situation.
- Notes the value of comfort items and the uncertainty of when infrastructure will be restored.
- Reminds to gather all necessary supplies and be prepared for the possibility of more storms in the future.

### Quotes

- "Mobility is life."
- "Comfort items worth their weight in gold."
- "Nothing else matters."

### Oneliner

Beau provides tips for surviving a storm for those not interested in helping, focusing on preparation, mobility, and self-reliance post-storm.

### Audience

Survivalists

### On-the-ground actions from transcript

- Identify your circle and network for emergencies (implied)
- Gather necessary supplies and comfort items for survival (implied)
- Stay mobile and be prepared to move at all times (implied)

### What's missing in summary

The detailed examples Beau provides on how to navigate through and survive a storm for those solely focused on personal survival.

### Tags

#StormPreparation #SurvivalTips #EmergencyPlanning #CommunitySafety #DisasterManagement


## Transcript
Well howdy there internet people, it's Bo again. So there's a storm coming. And since
I've been through a few storms, I'm going to give some tips for those people who don't
really, they're not looking to help during the storm. They just want to make it through
the storm. And even if Laura isn't going to impact you, there may be other storms on the
horizon. So everybody should listen to this. Something else may come up that does impact
you, occur near you. And again, this is for people who are just wanting to make it through
the storm, not wanting to get involved in any reconstruction or relief efforts, not
looking to help out during the storm. Hopefully, if you've watched these videos long enough,
you've made some forms of preparation. You know who your network is, your circle, your
little group of friends. If not, you'll figure it out along the way. Everybody knows what
to do instinctively during that first wave of energy from a storm. It doesn't matter
what kind of storm it is, they all start that way. Any big impact starts off with a massive
wave of energy. And eventually it gets expended. And that's really the most dangerous time,
because it's not the longest lasting time. That can be over pretty quickly. It's that
time afterward, once the infrastructure is damaged, that can really just drag on. And
for those people who are just looking to make it through the storm, that becomes the more
dangerous time. During that initial wave of energy, you hunker down, you wait. You just
ride it out. Once the dust settles, mobility is life. You need to be able to move at all
times. At all times. You basically stay packed, because even though the storm is over, something
could happen. There could be a levee that breaks. Something happens that creates a situation
where you have to leave where you're at. So that circle that you have decided on, those
people you truly care about, hopefully it's small because you have to supply all these
people, you move. And during that time when you're riding out the storm, you're thinking
of the locations you can go to, that you can go and secure. Probably other people's residences.
In the movies people go to abandoned warehouses and everything, and that looks cool, but that's
not the best thing to do. People's residences are normally best, especially rural ones,
because they typically have the tools necessary on hand to survive, once infrastructure goes
down. So once you're moving, a couple of things. First, everybody moves together. Unless
you really know what you're doing, don't split up. And unless you really know what
you're doing, try to look unimportant. Don't suit up. Don't look like a cop or a soldier,
because there may be people trying to take advantage of the situation that kind of makes
you a target. Aside from that, there may be people who need help, and if you're just looking
to make it through the storm, you only care about your circle. You don't want to get involved
in somebody else's issues. If you look like you know what you're doing, they might ask
for help, and you might feel obligated to give it. But that's not your goal at the outset,
right? So try to look unimportant. There is no safe zone after a big storm, because once
that initial wave of energy is expended, infrastructure is down, which means emergency services, the
things that you tend to count on, they're gone. They're either overworked, or they're
just not responding. It's just you and your circle. So if you meet some group of people
who's trying to take advantage of the situation, people who are actually looking forward to
that storm, if you're meeting them and you run into that kind of opposition, understand
you engage them only on your terms. It's not a movie. You can run away. That's okay. There's
nothing wrong with that, and it may take sacrificing something, some thing, a vehicle or something
like that, leaving it behind, because maybe that's what they want. Anything that doesn't
breathe can be replaced. If you're just looking to ride out the storm, keep that in mind.
Nothing else matters. Comfort items, toilet paper, soap, shampoo, razors, stuff like that,
worth their weight in gold, because nobody's really sure how long it's going to take for
the infrastructure to come back for order to be restored. So when you are gathering
your supplies, make sure you get it all. When you move to that other location, make sure
you have everything, because you don't know how long it's going to last either. So with
the likelihood of more storms coming, and the possibility of even wilder storms coming,
this is just something to keep in mind. And it's not something to become paranoid about.
It's not something to overthink. But just get a good idea of who your circle is, who
your network is going to be, and where you're going to go if you are not looking to help
during that storm, if you're just wanting to get through it. Because Laura's probably
going to be pretty bad. It looks like it's going to be about as bad as Michael, but there
are other ones that can happen. So even if this one doesn't directly impact you, kind
of come up with a plan for this. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}