---
title: Let's talk about mistakes at the DNC, framing, and solutions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mJwsZ8mm9v0) |
| Published | 2020/08/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Democratic Party's pursuit of drastic gun regulations risks alienating potential voters, including disaffected Republicans.
- Implementing an assault weapons ban may not be effective in gaining votes and could push away moderate voters.
- The focus on gun regulations could undercut the Democrats' message about the dangers of Trump's administration.
- The United States' unique gun ownership landscape, with more guns than people, complicates the effectiveness of proposed bans.
- Previous assault weapons bans were critiqued for being ineffective and not addressing the core issues.
- Implementing bans on semi-automatic firearms may not be logistically feasible due to the high number of guns in circulation.
- Any significant gun regulation must address loopholes related to animal cruelty and domestic violence convictions.
- Secure storage requirements for firearms and raising the minimum age for purchasing guns are suggested as effective measures with minimal pushback.
- Comprehensive education campaigns and identifying behavioral predictors are vital in preventing gun violence.
- Addressing social issues like mental health, education, and providing support systems is key to changing the culture around guns.

### Quotes

- "Aside from that, they pushed away those voters, at the very least risked them. And they got nothing for it."
- "It's more dramatic because it's, I mean, it is more dramatic. So it gets the headlines."
- "Address the motive, address the reasons it's happening, the breakdowns in society that are causing this."

### Oneliner

The Democratic Party's pursuit of drastic gun regulations risks alienating voters and may not effectively address underlying societal issues.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Close loopholes related to animal cruelty and domestic violence convictions (suggested)
- Advocate for secure firearm storage requirements (suggested)
- Support raising the minimum age for purchasing guns (suggested)

### Whats missing in summary

The full transcript provides a comprehensive analysis of the challenges and potential solutions related to gun regulations in the United States. 

### Tags

#GunRegulations #PoliticalStrategy #SocialIssues #DemocraticParty #SocietalChange


## Transcript
Well howdy there internet people, it's Bo again.
So today, it's probably going to be a long one,
we're going to talk about mistakes, framing, talking
points, solutions, a whole bunch of stuff.
And we're going to be delving into an issue
that I've talked about before on this channel a lot,
but haven't done so in a while.
And it is an incredibly, well let's just
say it's a divisive topic.
So the Democratic Party has decided to openly state
that they're going to move forward
in pursuit of drastic regulations that
are going to be opposed by the very people
they're trying to reach across the aisle to.
All of those moderates they've been trying to recruit
and trying to bring to their side,
they've made something part of their platform
that is going to alienate a large portion of them.
They've said that they're going to go after an assault weapons
ban.
OK, that's a mistake.
That's going to be a mistake.
To those who may want heavy regulation,
you may look at Donald Trump as a defender
of the Second Amendment.
He's not.
He's not.
A whole lot of the people who are ready to cross party lines,
they were ready because they had a suspicion
that Trump might enact legislation
when it comes to firearms.
Now they have a suspicion about Trump,
and they have a promise from Biden.
And for a lot of people, this is a make or break issue.
This is something that they're not going to bend on.
The gamble here that the Democratic Party is taking
is that they're hoping these disaffected Republicans,
that they dislike Trump more than they like their guns.
That is not a safe bet.
That is not a safe bet.
That's not a bet that's going to pay off right there.
Aside from that, they pushed away those voters,
at the very least risked them.
And they got nothing for it.
Anybody who is in support of an assault weapons ban,
they're already on the Democratic side.
They didn't gain any votes with this,
but they alienated a lot of people.
It also created some mixed messaging
that undercut the most important message
the Democrats have right now.
Simultaneously, it's really hard to say democracy is at risk
or we're right around the corner from totalitarianism.
Oh, and by the way, we're going to disarm you.
That doesn't play well with a lot of people.
That's something that it's hard to take both of those to heart.
So it undercuts the most important message
that Trump is dangerous, that he has to???
that that's the most important thing
is to get Trump out of office.
This idea of we're right around the corner from it
and we're going to disarm you, there is no guarantee
that we won't get a competent Trump in another four years.
Those who are moving away from Trump
are doing so because they recognize that danger.
They recognize it.
What do Republicans do when they see a danger?
What do they go by?
Guns.
This is something that is striking at the very heart
of the people they're trying to appeal to.
This???I think this was a big mistake
and it may be a deciding one.
The number of people that are going to be alienated by this
are a lot more than I think they believe.
Now, I'm sure there are some people right now going,
well, we have to do something.
And I agree.
I agree.
Other countries did this.
They did and it kind of worked for them.
However, they were in a different place.
The United States has more guns than people.
No other country had ownership even remotely close to that.
I've made that point before,
but I never had good framing for it.
I never had a way to demonstrate how important
that little tidbit is in the grand scheme of things
until the recent public health issue.
Right now, you have a whole bunch of Republicans saying,
hey, we got to reopen the schools.
They did it in other countries.
What's the difference?
Other countries had much lower case numbers.
Other countries had stuff under control.
They had won the culture war.
They had won that debate, the public debate,
and they got everybody on board with it.
Then, once the case numbers got down,
they were able to reopen the schools.
This is the same thing.
You have to get the public debate solved first
before you can enact anything this dramatic
because at the end of the day, you have two types of bans
that they can go with.
They can go with the assault weapons ban
like the one we had before, completely ineffective.
For those who are going to point to the statistics,
compare them to trends.
It was ineffective.
It had to be because it didn't take a single gun
off the street and it didn't stop anybody from buying anything.
What it did was it banned cosmetic features.
If you wanted to go buy Model X, well, you could.
It just had a different stock.
It didn't change the way it functioned at all.
That wasn't even addressed.
It changed the names of some,
but it didn't severely limit the ability
to get your hands on any of it.
Now, the alternative to that would be
to go to the other extreme,
which would be to ban all semi-automatics,
which would be pretty much every new firearm design
in like the last 100 years,
which is going to anger tens of millions of people
who aren't going to comply with it
because the numbers are too high,
because the public debate hasn't been resolved.
It's not like the UK and Australia.
It just turns it into another form of prohibition.
And as we learned in the 20s and by the one that's ongoing now,
that doesn't work in the US.
We thrive on, let's just call them secondary markets.
It's like our thing.
And that's what it would do,
is it would push the market underground
because of 3D printing, because of home manufacture,
because of the number already in circulation.
It's logistically not really possible.
And we don't have a list.
That's another deciding thing.
We don't have a list.
The closest thing in the US that exists to a list of,
you know, where you could find them would be NRA membership.
They'd subpoena that list.
They'd get that list.
And all of those people who, yeah, anyway,
that's always funny to me.
But those would be the first people that had theirs taken.
And then from there, though, you don't have anywhere to go.
And that's only a fraction of owners.
Again, we have more in the United States
than we have people.
We have a huge issue.
We're not at the point where we can do the dramatic stuff.
Now, so how do you solve it?
Because you have a whole bunch of politicians
with these dramatic talking points,
but that's what they are.
They're talking points.
They're either not plausible or they're ineffective.
It's a lot like politicians with that other issue.
Who do you have to listen to?
The experts, the people that actually know about it.
The reason that first ban was such a joke
is because they had people involved in writing it
that knew nothing of the subject.
And we've learned from the recent one
that that's not how you do things.
You listen to the experts.
It may come as a surprise to those who want regulation,
but there are a lot of Second Amendment advocates
who are more than happy to help come up with ideas
that would actually help curtail the problem
rather than just the means.
Some that are extremely popular.
Now, some of these you get some pushback on, but not much.
The first is there are loopholes when it comes
to being mean to animals and DV.
In theory, if you're convicted of hurting a family member,
you lose your right to own.
However, if you cut a plea deal and it gets knocked back
and it's not called that, well, then you don't.
Then it's OK.
Those loopholes have to be closed.
And that's going to be more effective at preventing
the type of situations everybody's talking about,
the ones that happen at schools.
Those are huge indicators.
Being mean to animals and domestic issues,
those are huge indicators of future problems.
That right there needs to be solved.
Number one, that needs to be at the top of the priority list.
And you're not going to have any pushback from the gun
community on that one.
I don't know a single Second Amendment supporter
that has an issue with that, most openly advocate for it.
I'm sure they exist.
I'm sure there's a small minority out there
that would oppose it, just on the shall not
be infringed side of things.
But it's not going to be many.
And it's not going to be enough to stop it.
Another one, get a little bit more pushback on this one,
is requiring that they be locked if they're not
in the immediate possession of the owner.
Because when these things happen at schools, most times
the student didn't buy it.
They went to a gun cabinet and they took it
because it wasn't secured.
Requiring that they be secured is a big one
that could stop a lot of problems before they start.
And again, that's going to be one
that you're going to have people whine about.
But nobody's going to care.
And they'll comply with it.
Most responsible owners do anyway.
I know back when I owned, I had everything secured.
And I didn't know many who didn't.
And another one that's one of those that they'll whine about.
But at the end of the day, they'll be like,
yeah, it'll probably work, is raising the age a couple years.
Although there aren't many examples of it happening,
I think there's at least one where an 18-year-old student
bought it.
Because you can get them at 18.
Raising it a couple years, they're no longer in school.
These are simple, easy, effective solutions
that you're not going to get a lot of pushback on.
And they'll be complied with, which is more important.
Now, this is all legislative stuff.
But that's not the real fight.
That's not the real battle, not if you actually
want to solve the problem.
So along with this, and way more important,
is a massive education campaign dealing with everything
from safety, what to do if you find one that's unlocked,
all of the normal stuff, and then
how to identify the predictors.
Because the feds have pretty much figured out
the behaviors and traits that people exhibit before one
of these things happens.
They know.
Do you?
Because they didn't put any effort into telling everybody.
And if we're going to preempt it, we have to know.
That would be a big one.
Along with that, another campaign,
I guess would be the term for it, an advertising campaign,
to demonstrate that the gun doesn't make the man.
Because that's a big part of the problem right now.
It's turned from a tool into a symbol of masculinity.
And the way you solve all your problems, that's got to change.
That's got to change.
I would enlist the help of the media
when it comes to an incident that does happen.
The only time that name gets brought up
is when it is revealing an embarrassing fact about them.
And I mean everything, everything they've ever done
that's embarrassing, everything they wouldn't want people
to know.
That's what the coverage is.
No fame, no glory, just being mocked.
All of this, it helps.
Now, along with the education campaign
and being able to identify these things,
we have to make sure that once they're identified,
they can actually get counseling.
Because just knowing it's going to happen,
that doesn't really help anybody.
So real counselors have to be available.
These are real solutions.
These are things that will actually help.
Rather than just remove the means,
you're helping get rid of the motive, which
is far more important.
Even if you remove the means, you still
have an angry young man ready to do something bad,
address the motive.
So you have to have the counselors.
But all of this stuff, this is dealing
with a very, very, very small percentage of the issue.
It's more dramatic because it's, I mean, it is more dramatic.
So it gets the headlines.
So it gets the attention.
So it gets the talking points.
So it gets the policies.
Those that are listed as assault weapons,
they make up about 2%, 2% of murders.
Not something that can really be the keystone
of a comprehensive plan.
Most, the best way to address it would
be doing stuff that most people on this channel
want to do anyway, addressing social issues,
having safety nets, having a living wage,
increasing access to health care, mental health care,
education, all of this stuff, ending prohibition.
Because a whole lot of this stems from that black market.
All of this stuff taken together,
it changes the culture.
And that's what we have to do.
Because before we ever get to the point
where we can actually deal with one of these bans,
we'd have to get it to the point where
people would comply with it.
And right now, they won't.
The ultimate irony in all of this
is that if you change the culture
and you got it back to it being viewed as a tool,
seen like a fire extinguisher, something you have,
you know how to use, but you pray to God you never have to,
the ban wouldn't be necessary.
And at the end of the day, that's the route to go.
The route to go is to address the motive,
address the reasons it's happening,
the breakdowns in society that are causing this.
Getting rid of the means doesn't mean anything.
Because as we've seen lately, they'll use cars.
There's a wider issue here.
This is just a symptom.
If you want to get to root causes, you have to get to them.
And most of them aren't even legislative.
Most of them are just making sure the money's there
to do the things we know need to be done.
Most of it isn't creating legal punishments.
In fact, honestly, aside from the DV and animal stuff,
the other ones aren't really necessary.
They would help.
But they're not necessary if we could do the other things.
They're more of a stopgap while the culture is changing.
But that's going to be a more effective route.
With 3D printing, with the ability to manufacture at home,
with the number in circulation, we are not in a position
to do the same things that the UK or Australia did.
Our case numbers are too high.
We're not at the point where we can open the schools yet.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}