---
title: Let's talk about Steven Bannon's situation and what we can learn....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=m7iMuGZYlIs) |
| Published | 2020/08/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Steve Bannon, former campaign advisor to Donald Trump, is facing charges related to a campaign to raise money for a border wall.
- Bannon and others allegedly pocketed some of the money meant for the wall and used it for personal expenses.
- If convicted, Bannon could face seven to nine years in prison.
- Bannon has pleaded not guilty, but there's a possibility of a plea deal for a reduced sentence.
- Social media response to Bannon's charges revealed Republicans blaming Democrats, showing party loyalty above all else.
- Political parties in the US have created a situation where supporters defend party members even if they harm them personally.
- The focus on party loyalty over policy has led to defending actions against one's own interests.
- Parties were originally meant to assist in getting legislation passed but now contribute to corruption.
- Americans need to shift focus from party affiliation to policy and its long-term effects.
- The current situation in the US is a result of prioritizing party allegiance over examining policy effects.

### Quotes

- "Parties are really there to assist in the good form of corruption."
- "Americans have decided that it's easier to look for an R or a D than it is to actually look at policy."
- "You have people who are defending the people who took advantage of them personally."

### Oneliner

Steve Bannon's charges reveal loyalty to political parties over personal interests, urging Americans to focus on policy, not party.

### Audience

Citizens, Voters

### On-the-ground actions from transcript

- Examine policies over party loyalty (implied)
- Advocate for transparency and accountability in political actions (implied)

### Whats missing in summary

Importance of holding politicians accountable and prioritizing policies over party loyalty.

### Tags

#SteveBannon #Politics #PartyLoyalty #Policy #Accountability


## Transcript
Well, howdy there internet people, it's Beau again.
So today we are going to talk about Donald Trump's former campaign advisor,
Steve Bannon, and some of the issues that he's facing.
And by issues, I mean charges.
We're gonna briefly discuss that, and then we're gonna talk about what we can
learn from that based on the social media response to what he is facing.
So, brief overview of the allegations against him.
There was a campaign to raise money to build a wall on the southern border.
The initial premise behind it was to raise the money,
turn it over to the federal government.
Then the people behind it actually learned how the federal government works and
realized they can't do that.
That's apparently when things went sideways.
It's alleged that Bannon and others pocketed some of the money,
used it to fund their personal lifestyle.
This is what led to the charges.
If convicted, he's looking at seven to nine years,
more or less, based on sentencing guidelines.
Theoretically, he could take a plea deal and
get a reduction for providing what is called substantial assistance.
However, in the Southern District of New York, you don't get to pick and choose.
It's all or nothing, which means he would have to answer questions
about everybody that he's dealt with, to include the president.
That would knock his sentence down to three and
a half to four and a half years, if he did that.
Okay, so that's where we're at.
It should be noted, Bannon has pleaded not guilty to this,
as we're standing here now.
But see, something else happened when all of this occurred.
When the news broke, a unique thing happened on social media.
The allegation is that a Republican ripped off a bunch of Republicans and
took their money, and on social media, Republicans have decided to blame
Democrats.
Why?
Because political parties in the United States have
basically induced Stockholm Syndrome among their supporters.
It's gotten to the point where those who are party loyalists
will defend the actions of other members of their party,
even if they're the people being harmed by them.
We've talked a lot about voting against your own interests.
I'm certain right now, there are people who donated to this campaign,
who are actively defending Steve Bannon, and
blaming the Democrats, saying that it's a witch hunt or whatever.
This is one of the reasons the United States is headed the right direction it is.
Understand, parties are really there to assist in
the good form of corruption.
That's really why they're there.
You back me on my bill, I'll back you on yours.
That's why they started, that's why they exist,
to help form coalitions to get legislation passed.
However, as people began to focus more on party rather than policy,
this is what happens.
You have people who are defending the people who took advantage of them
personally, and those who are going against their interests,
they will defend a party that is actively operating against them.
It's probably something that we need to focus on in the United States.
That's part of the reason we're in the situation we're in,
is because Americans have decided that it's easier to look for
an R or a D than it is to actually look at policy and
to see the long term effects of it.
Anyway, it's just a thought.
Y'all have a good day.
Good luck, Mr. President. Thank you.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}