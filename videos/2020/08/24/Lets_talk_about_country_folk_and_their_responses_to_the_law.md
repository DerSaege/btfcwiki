---
title: Let's talk about country folk and their responses to the law....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5-v7Juv2bvU) |
| Published | 2020/08/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Filming outside due to a storm, discussing a recent incident where the police went too far.
- Addressing white country folks about their reactions to such incidents.
- Noting the disconnect between online support for police actions and real-life community values.
- Sharing how the community comes together to prepare for an upcoming storm, showcasing true solidarity.
- Emphasizing the importance of principles over superficial differences like skin tone.
- Criticizing those who change their principles based on race and lack consistency.
- Mentioning the implications of being untrustworthy in a close-knit community.
- Encouraging people to stick to their principles and not waver based on race.
- Reminding the audience of historical figures like moonshiners and the origins of the term "redneck" in collective action.
- Criticizing the shift in values influenced by social media and external factors.
- Urging individuals to stay true to their roots and not be swayed by external influences.

### Quotes

- "People hop on Facebook and you make your little post and you get four or five likes from people you know, people who actually live around you, from your actual physical community."
- "Your real principle is something else. And every time this happens, that mask slips a little bit."
- "Meanwhile, you're out there cheerleading for Boss Hogg."
- "You let Facebook, you let some dude with a red hat change who you are on a level so deep that those people who know you in real life don't want anything to do with you."
- "Either way, you better see if those people on Facebook come help you with the storm."

### Oneliner

Beau addresses white country folks, urging them to stay true to their principles and community values amidst societal influences and online disconnect.

### Audience

White country folks

### On-the-ground actions from transcript

- Reconnect with real-life community members to build trust and support (implied).
- Uphold consistent principles regardless of skin tone (implied).
- Stay true to historical community values like solidarity and collective action (implied).

### Whats missing in summary

Beau's passionate delivery and historical references can be best understood by watching the full transcript.

### Tags

#CommunityValues #Principles #Solidarity #HistoricalReferences #SocialInfluences


## Transcript
Well howdy there internet people, it's Bo again.
So today is going to be a little bit different, obviously.
Can't film in the shop, big storm came in.
So today, we had another one of those situations last night, right?
We're not actually going to talk about the situation.
We're going to talk about something related to the situation, but a little bit separate
from it.
This video is for people who look like me and sound like me.
My people, white country folk.
Because any time one of these situations happens, and by situation I mean the cops go a little
bit too far, and by a little bit too far I mean a whole lot too far.
People hop on Facebook and you make your little post and you get four or five likes from people
you know, people who actually live around you, from your actual physical community.
And you get a whole bunch of likes and comments and shares from people you've never met.
And see I think that's leading to a misunderstanding of how popular those opinions are among people
like me.
Big storm, right?
First storm of the season.
Everybody knows the real big ones are going to come later, so today, starting last night,
people started calling each other, making sure everybody was ok.
Everybody had what they needed for the big storms coming later.
Talking to a friend, I'm like, hey, you heading over to person X's house?
And he just says, nah.
No explanation because I don't need one.
I know why he's not going.
Same reason I'm not.
See your community, your physical community, they see those posts.
Those are not popular opinions.
We also see the posts where you're like, oh, poor LaVoy, poor Randy.
Oh but that person that's got even the slightest bit of a tan?
They should have just accepted that life and death game of Simon Says the cops challenge
them to.
They should have complied.
Nobody should resist.
Meanwhile, you can say that on Facebook because they don't know you.
However, I was with you when you got hit in the head with a mag light for resisting.
People who live in the country, we tend to be anti-authoritarian.
We like the freedom that comes with living out away from everybody.
We rely on each other.
We rely on principle.
If your principles are so weak that they can be overridden by skin tone, something that
superficial, they're not really your principles, are they?
It's just something you say.
If it's just something you say, you can't really be trusted, can you?
You know what that means in a community like this.
Nobody wants to be around you.
Nobody wants anything to do with you.
Nobody's going to help you because you can't be trusted.
You can have different principles.
Sit around and argue about that all day long, as long as you stick to them.
But if you're, don't tread on me, but that only applies to certain color people, we know
that's not really your principle.
Your real principle is something else.
And every time this happens, that mask slips a little bit.
We don't like it.
These are not popular opinions anymore.
You need to remember, Hollywood gave people like us two shows.
One about a lawman didn't even carry a gun, and one about guys who, well, the entire show
was about them resisting.
And you pretend you love it, because it gives you an excuse to have that stupid flag.
No, no, no, I'm not really like that.
I just really like Dukes of Hazzard.
Meanwhile, you're out there cheerleading for Boss Hogg.
We all see it.
It changes people's opinion of you, because what it tells us is that you don't have any
principles, not any that you'd stand for.
You need to remember where you come from.
You need to remember that moonshiners, they were the original defund the police, not paying
taxes, going out of their way.
You need to remember that the term redneck, one of the more likely places it came from,
had to do with organized labor, unions, collective bargaining, communities coming together.
All that stuff that now, you're like, oh no, no, no, that's bad.
That's Marxism.
Nothing changed.
Nothing changed, except you.
You let Facebook, you let some dude with a red hat change who you are on a level so deep
that those people who know you in real life don't want anything to do with you.
I don't know that they changed you either.
They just made you show it.
Either way, you better see if those people on Facebook come help you with the storm.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}