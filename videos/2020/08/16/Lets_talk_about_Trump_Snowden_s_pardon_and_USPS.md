---
title: Let's talk about Trump, Snowden's pardon, and USPS....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5dJLmOT0fCw) |
| Published | 2020/08/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's consideration of pardoning Edward Snowden is a distraction from his attack on the United States Postal Service.
- Trump's past tweets about Snowden show a different stance than his current consideration of a pardon.
- Trump's administration is more damaging to the U.S. than Snowden ever was.
- Beau supports a pardon for Snowden, believing he acted to honor his oath.
- Trump's move is to shift focus from the Postal Service and create a new debate.
- The U.S. Postal Service is vital for the election and Trump's actions against it are more harmful than Snowden's leaks.

### Quotes

- "This is an attempt to draw attention away from Trump's move against the United States Postal Service because that is what is in the news cycle right now."
- "The U.S. Postal Service is pretty critical to the election, and Trump's moves against it are far more damaging to the United States than anything, anything leaked by Snowden."

### Oneliner

Trump's consideration of pardoning Edward Snowden is a distraction from his damaging actions against the U.S. Postal Service, which is critical for the election.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Contact your representatives to express support for the U.S. Postal Service and demand protection for its critical role in the election (implied).

### Whats missing in summary

The emotional impact of Beau's disappointment in Trump's actions and the urgency to prioritize protecting the U.S. Postal Service.

### Tags

#EdwardSnowden #DonaldTrump #USPostalService #Election #Distraction


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about
President Donald J. Trump's ever-evolving position on Edward Snowden. If you're unaware,
he has suggested that he is very strongly considering pardoning Mr. Snowden. He's not.
This is an attempt to draw attention away from Trump's move against the United States
Postal Service because that is what is in the news cycle right now. This is his way
to get people talking about something else because he doesn't want them talking about
his attempt to undermine the very foundations of this country and rig the election. That's
what this is about. His past tweets, of which there are tons, none of them speak very highly
of Mr. Snowden. We're going to run through three of my favorites real quick just to give
you a good feel on what he has said if you don't know. Message to Edward Snowden. You're
banned from this universe unless you want me to take you back home to face justice.
Now granted, this was made in 2013 when the President being able to hold a cup with one
hand or walk down a ramp wasn't cause for applause. Okay, here's another one. Isn't
it sad the way Putin is toying with Obama regarding Snowden? We look weak and pathetic.
Could not happen with a strong leader. All I can say is that if I were President, Snowden
would have already been returned to the U.S. by their fastest jet and with an apology.
By their fastest jet. He's talking about a Russian jet. See, maybe we just misunderstood
this. Maybe he meant he was going to get Snowden home and apologize to him and pardon him.
Maybe he was telling the truth then. Doesn't seem like what he meant in context though.
This is an attempt by the Trump administration to change the story because the Trump administration
is seeking to do more damage to the United States than Snowden ever did. To be very clear
on this, I actually support a pardon for Snowden. If Trump issued one, it would be one of the
few things he's done in this administration that I think is a good move. My read on Snowden's
activities is that he did what he felt he had to do to honor his oath. That's honestly
the way it looks to me. I know there's a lot of debate about that, but that's my read on
it. If the President is serious about, let's say, reforming the way we treat those who
bring less than favorable information to light, I have an entire list of real winners that
could probably use a pardon as well. But we know that's not really the case. This is just
an attempt to take a name that is popular, that is divisive, that there is a lot of debate
surrounding their activities and use that to change the story, to create a new debate
to get eyes off of the United States Postal Service. That's what this is about. If it
was anything else, he just would have pardoned him. He wouldn't have talked about it like
this. The goal is to create debate. The goal is to change the story. We have to kind of
focus in on this one. The U.S. Postal Service is pretty critical to the election, and Trump's
moves against it are far more damaging to the United States than anything, anything
leaked by Snowden. And we really need to keep that in mind. Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}