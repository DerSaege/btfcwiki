---
title: Let's talk about Arizona, Georgia, and doing what you're told....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aOqShir_PYY) |
| Published | 2020/08/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Raises the difference between doing what you're told and doing what's expected of you.
- Talks about the situation in Arizona where teachers are pushing back against reopening schools.
- Mentions teachers in Georgia also resisting the pressure to reopen schools in person.
- Shares a story of a parent in Georgia who pulled her kids out of school due to safety concerns.
- Questions the authority of governors pushing for school reopenings.
- Emphasizes that protecting kids is the community's responsibility, not the governors'.
- Criticizes the prioritization of money and power over safety by governmental authorities.
- Reminds that Americans should hold government accountable and prioritize the well-being of children.
- Advocates for online instruction as the least risky option during the pandemic.
- Calls for innovation in the education system and for parents to prioritize their children's safety.

### Quotes

- "Do what's expected of you."
- "Your job is to protect those kids."
- "Don't let a government authority put your child at risk."
- "They care about money and power."
- "The fact that the person has a government title means nothing."

### Oneliner

Beau raises the importance of doing what's expected to protect children, not just following orders, amidst school reopening debates.

### Audience

Parents, Teachers, Community Members

### On-the-ground actions from transcript

- Support teachers pushing back against unsafe school reopenings (implied).
- Participate in community actions like call floods if teachers request support (implied).
- Prioritize children's safety over governmental orders (implied).

### Whats missing in summary

The full transcript provides more context and detailed examples of individuals prioritizing safety over external pressure, urging community support for teachers, and questioning governmental decisions regarding school reopenings.

### Tags

#Parenting #CommunitySupport #Education #GovernmentAccountability #Safety


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about doing what you're told and doing what's expected of you.
Now sometimes those two things aren't the same.
To start off with tonight, I want you to imagine you get a phone call right now.
And it's from somebody that you're vaguely aware of, you know they exist, but they're
not your friend.
They've never been to your house, they've never met your kids, you don't really know
this person.
But for whatever reason they call you to remind you that their primary concern in life is
money and power.
That's what they're about.
Out in Arizona you have a whole bunch of teachers calling in, some flat out resigning.
Because the governments there, the state and local governments have decided to reopen the
schools.
Those teachers are not doing what they're told, but they are certainly doing what is
expected of them.
In the most dire situations, we count on teachers to protect the kids.
That's what they're doing.
They're pushing back against this idea of reopening because it's not safe.
And they know that.
If they need anything, they should have our full support.
I'm not just talking about people in Arizona.
If they ask for a call flood, you can participate in it even if you're out of state.
They're doing what is expected of them.
And I would imagine that they are expecting the community to back them up.
In Georgia, they have the same situation going on.
State and local governments pushing to reopen the schools in person.
You had a parent there.
She dropped off her kid.
The kid came home that first day and was like, you know, there's a whole bunch of kids in
class, you know, 30 of them.
Most of them aren't wearing masks.
Mom yanked those kids out of school.
Not doing what she's told, certainly doing what's expected of her.
If to use her words, if you feel you're dropping your kids off at a death trap, you probably
shouldn't.
Keep in mind, even the White House is saying that Georgia is having real issues combating
this.
The White House that has downplayed this at every turn in every possible way.
Even the White House acknowledges the issues they're having there.
That person who called you, I want you to imagine that two days from now, they call
you up and say, hey, I need you to drop your kid off in a dark alley for me.
Don't worry, they're going to be all right.
Would you do it?
Would you expect anybody to do it?
Probably not.
And if anybody did, they would be subject to derision from the entire community.
How could you do that?
What's the difference between those phone calls and the order put out by some person
that just happens to have governor as a title?
These governors have made it very clear their primary concern is money and power.
That's what they're about.
They do not care about the kids.
They never did.
They're the ones who are telling you, you have to send your kids off to do this.
Would you do it if they said any other situation, if they told you to drop them off anywhere
else where you felt that they wouldn't be safe?
Of course not.
You don't have to do what you're told.
Do what's expected of you.
Your job is to protect those kids.
It's not the governor's job.
It's not.
They've made that abundantly clear.
They don't care.
They care about money and power.
This idea that Americans are supposed to obey governmental authorities, it's really taken
hold and it's kind of messed up because the country was founded on the exact opposite
idea that government should take orders from the people and have consent of the governed.
They're representatives of you.
They're supposed to obey you, not the other way around.
If you are facing this choice and your choice is doing what you're told by somebody who
doesn't have your best interest at heart or doing what is expected of you to protect
your kids, this shouldn't be a tough decision.
The fact that the person telling you, issuing that order, has a government title means nothing.
You still have the same expectations.
You are still expected to protect your kids.
These entities do not care about them.
All of the guidelines say the least risk is online instruction.
All of them.
There are no exceptions to this.
Why take the risk?
We need to innovate and change our school systems to begin with.
Here's the perfect time.
Don't let a government authority who is supposed to be taking orders from you put your child
at risk.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}