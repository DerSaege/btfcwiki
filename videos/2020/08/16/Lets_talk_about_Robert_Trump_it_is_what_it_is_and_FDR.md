---
title: Let's talk about Robert Trump, it is what it is, and FDR....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RHYHuFxLeEA) |
| Published | 2020/08/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump's brother, Robert Trump, passed away, sparking different reactions and trends on Twitter.
- The phrase "wrong Trump" trended on Twitter, causing moral outrage and demands for compassion.
- Some individuals demanded condolences for Robert Trump, while others dismissed the situation with "it is what it is."
- Beau points out the irony of those demanding compassion now, who previously prioritized economy over lives.
- Fear, according to Beau, is a necessary emotion that keeps individuals alive, especially in justifiable circumstances.
- Beau references FDR's quote about fearing fear itself, questioning its relevance and meaning in today's context.
- He dives into FDR's inaugural speech, focusing on the rejection of materialism and the pursuit of noble social values.
- Beau criticizes those using FDR's quote to justify prioritizing monetary profit over social values and public well-being.
- The speech condemns the callous and selfish pursuit of material wealth, urging a shift towards more noble societal values.
- Beau underscores the need to address each statistic as a tragedy rather than dismissing them as mere numbers.
- He advocates for proactive efforts to address issues instead of using statistics as excuses for inaction.
- Beau acknowledges historical examples where people found leadership and worked for change instead of succumbing to defeatism.
- While Beau acknowledges the importance of mourning individuals like Robert Trump, he also stresses the broader systemic issues at play.
- He contrasts past leaders with the current political climate, questioning the acceptance of failure and lack of empathy in leadership.
- Beau concludes by encouraging reflection on the type of leadership needed for positive change in America.

### Quotes

- "We have nothing to fear but fear itself."
- "One is a tragedy, a million is a statistic."
- "If you're looking for change, if you're looking to make America great again, you probably don't want a leader who is that accepting of his own failures."

### Oneliner

Beau dissects reactions to Robert Trump's passing, questioning the acceptance of failures and urging a shift towards noble social values in leadership amidst the ongoing crisis.

### Audience

Americans

### On-the-ground actions from transcript

- Seek leadership that prioritizes noble social values over material profit (implied).
- Advocate for proactive efforts to address societal issues instead of dismissing them as statistics (implied).
  
### Whats missing in summary

The emotional depth and nuanced analysis present in Beau's full transcript.

### Tags

#Leadership #SocialValues #Fear #AmericanValues #Change


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about President Trump's brother, Robert Trump.
We're going to talk about that news, the reaction to that news, the reaction to the reaction,
and we're going to view all of this through the lens of a quote, a very American quote,
a quote that I'm certain most people watching this have heard repeatedly, a quote that gets
used all the time, and in a truly American twist, almost nobody knows the context of
that quote.
And bringing it into context, bringing it into focus, really shows exactly how far we
have strayed from the ideals that we think make up America.
Okay so if you don't know, last night President Trump's brother, Robert Trump, passed.
Almost immediately when news broke, the phrase, wrong Trump, began trending on Twitter.
If you don't know how Twitter works, the more people who are saying something, the
more likely it is to trend.
So you had a significant portion of people saying that that was the wrong Trump.
This of course caused some moral outrage.
Some people were very upset by this.
They demanded condolences and compassion.
That was met by a chorus of people saying, well it is what it is.
And that is the current state of the United States.
It becomes even more American and even more 2020 when we acknowledge that many of those
demanding compassion were the same people who not too long ago were demanding that Robert
Trump's demographic lay themselves down for the economy.
And if you questioned that and you said no, no, no, no, one is a tragedy, 170,000 is apparently
a statistic, you would hear something along the lines of, well you can't stop at all,
you know, you can't live your life in fear.
And that sounds good and everything, but what does that even mean?
You can't live your life in fear.
Yes you do.
Fear is what keeps you alive.
If you didn't live in fear, if you didn't have a sense of fear, you wouldn't be here.
Fear keeps you alive, especially when you're talking about something that is identifiable
and it's justified.
And then you're hit with that quote, we have nothing to fear but fear itself.
Okay, so what does that say, what does that mean in context?
What was FDR trying to say?
Trump's inaugural speech, I'm not going to go through the whole thing because dude, it
was really long winded and it's about like 20 minutes long.
But I'm going to go through a significant portion of it, stick with me because maybe
there's some value in it today.
So first of all, let me assert my firm belief that the only thing we have to fear is fear
itself.
Nameless, unreasoning, unjustified terror, which paralyzes needed efforts to convert
retreat into advance.
In every dark hour of our national life, a leadership of frankness and of vigor has met
with that understanding and support of the people themselves, which is essential to victory.
And I'm convinced that you will again give that support to leadership in these critical
days.
Man, that is really heavy, that is a heavy introduction.
What is he talking about?
Let's find out.
He goes on to say, rulers of the exchange of mankind's goods have failed through their
own stubbornness and their own incompetence, have admitted their failure and have abdicated.
Practices of the unscrupulous money changers stand indicted in the court of public opinion,
rejected by the hearts and minds of men.
This is starting to sound relevant.
The money changers have fled from their high seats in the temple of our civilization.
We may now restore that temple to the ancient truths.
The measure of the restoration lies in the extent to which we apply social values more
noble than mere monetary profit.
I'm going to ask that you not use a quote from this speech to justify setting aside
social values in the pursuit of monetary profit.
Recognition of the falsity of material wealth as the standard of success goes hand in hand
with the abandonment of the false belief that public office and high political position
are to be valued only by the standards of pride of place and personal profit.
This really was written a long time ago.
I know it sounds like now.
And there must be an end to a conduct in banking and in business which too often has given
to a sacred trust the likeness of callous and selfish wrongdoing.
The people using that quote to justify reopening the economy are the exact people being chastised
in the speech.
They are the callous, the selfish, those who are pursuing material possessions, those who
are interested in pride of place.
The fear he was talking about here is fear of change, is fear of moving forward.
He wasn't saying just throw caution to the wind.
There's that other saying, one is a tragedy, a million is a statistic.
If we want more noble social values, we're going to have to acknowledge that yes, a million
is a statistic, but every increment in that statistic is a tragedy.
And we're going to have to work to stop it.
Because that's what we do in the United States.
We use statistics oftentimes to justify not fixing things.
The problem becomes too big.
We just need to sit on our couch.
We can't actually push back.
I would point out that we have had issues like this in the past.
And people didn't give up.
People found leadership.
They didn't have it in the beginning.
They became it.
They became the people on the front lines, those really working for change, those holding
up a mirror to the absurdity of the current system.
So I'm not going to condemn those people who said it is what it is.
If you follow me on Twitter, you know I'm actually one of them.
Because that is what it's doing.
It's holding up a mirror.
And while as one person, when we're talking about Robert Trump, yeah, that's a pretty
horrible statement, admittedly.
There were 170,000 Americans who heard their president offer no condolences.
Just an acceptance.
It is what it is.
Doesn't matter.
We have to move forward.
Open up that economy.
We're going to lose some people, but material possession is worth it.
We don't want to change because we have to prove we're not afraid.
We have gone from leaders, people in political office, who could deliver a speech like this
and have lines from it resonate so much and last so long that people actually forget what
they meant, but they had that universal appeal.
Trump's famous quote is probably going to be, it is what it is.
If you're looking for change, if you're looking to make America great again, you probably
don't want a leader.
Who is that accepting of his own failures?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}