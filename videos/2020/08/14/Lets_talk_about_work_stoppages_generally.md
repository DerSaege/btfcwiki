---
title: Let's talk about work stoppages, generally....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=P46s6djBBoU) |
| Published | 2020/08/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Congress left without providing relief for normal folks.
- Suggestions of a mass strike are surfacing across different circles.
- Strikes have a history dating back to 500 BC in Rome.
- Strikes work because they impact everyone, not just one industry.
- Government reacts to strikes due to economic impact and lack of control over them.
- Community networks are vital for organizing strikes.
- Pitfalls in the US for a general strike include polarization and geography.
- Strikes in the US must focus on specific, unifying issues to be effective.
- A call for relief and a public health response could be key demands.
- Having influential figures supporting the strike idea can lend it legitimacy and expedite government response.

### Quotes

- "Everybody stops working."
- "You have to make this a battle big enough to matter, but small enough to win."
- "The fact that it's being talked about in numbers so big that it becomes a trending topic on Twitter gives you hope."
- "It is one of the biggest tools of social change."
- "Y'all have a good day."

### Oneliner

Suggestions of a mass strike emerge in the US due to Congress's lack of relief for normal folks, necessitating community networks and specific demands for effective action.

### Audience

Working-class activists

### On-the-ground actions from transcript

- Organize community networks for strike preparation (implied)
- Rally influential figures to support the strike idea (implied)

### Whats missing in summary

Importance of community organizing and influencer support for successful strikes.

### Tags

#GeneralStrike #CommunityOrganizing #WorkingClassActivism #InfluencerSupport #ReliefDemands


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're gonna talk about an idea
that's being floated, a suggestion that is coming up
by a surprising number of people
in a whole lot of different circles.
This suggestion is occurring because Congress left
without providing any relief for us normal folk,
us down at the bottom.
They decided they were gonna take
couple weeks off. So a whole lot of people are suggesting that
everybody take a couple weeks off. So today, we're going to
talk about strikes. Generally, we're going to talk about their
history, we're going to talk about why they work, how they
work, talk about what you need in a country that's considering
one. And then we're going to talk about some pitfalls that
would exist in the United States that may not exist in other
countries. Okay, so the history of mass work stoppages. There are a whole lot of
different movements that like to lay claim to the idea that, well, we gave birth
to this. They're all wrong, okay? The earliest one that I'm aware of, and there
are probably some even earlier than this, but the earliest one that I'm aware of is
is 2500 years ago. 500 BC in Rome, there was a series of them and it worked. One almost
toppled De Gaulle's government. In 1947, there was one being planned in Japan and MacArthur
stepped in. He's like, no, no, you can't do this. We're still trying to get this country
back on its feet this is too deadly of a social weapon to be used right now. His
words. Why? Why is it so effective? Because it's everybody. Everybody stops
working. When you're talking about a normal strike it's one industry. So those
super wealthy people they don't really even fill it because they have income
from other places, but when it's a whole bunch of industries, when it's pretty much everybody
who isn't in a critical field, they fill it.
And these are the people that have enough money to own Congress people.
So the establishment, the government establishment, in whatever country you're talking about,
they tend to respond because nobody likes to get an angry phone call from their boss.
And when it's across the board, in a whole bunch of industries, it impacts the GDP.
We got to see what happens in a disorganized manner when everybody just stays at home.
Impacted the GDP pretty significantly, the gross domestic product.
And government took notice pretty quickly.
reason these strikes work is because the government's not in control of it. They
can't convince people, oh well if you don't go back to work we're gonna not
give you relief. We're gonna cut your unemployment to make sure you lazy bums
get out there and sacrifice yourself on the altar of the stock market. They don't
have those tools. So that's why they're effective and that's how they work. What
you need? You need community networks. That's not me staying on brand. You have to have them.
Because they're across industry, you have to have people in the community who can, number one,
tell people that it's going on. Number two, help organize. Because once it starts, these things can
last weeks. People are going to need stuff. You know, everybody stocks up before one of these.
but eventually somebody's going to need milk. A baby's going to run out of formula.
There has to be a network in place to make sure that everybody gets the
assistance they need. So you have to have those. There are a couple of pitfalls in
the US that are specific to the US. The first is that we are incredibly polarized.
In a lot of countries, when it gets to the point, when it gets so bad that people are
proposing a general strike, well, they kind of align enough to get major reforms.
In the U.S., we're too polarized for that.
It would have to be limited to the situation at hand because you need general appeal to
have a general strike.
The list of requests can't have any divisive issues on it,
otherwise you'll drive people away
and it will be less effective.
So in this situation, it would have to be limited to relief
for the working class and, I don't know,
a public health response.
That would be nice too.
That's pretty much it.
That's all you could really ask for.
Anything else would start driving people away.
You have to make this, especially the first time something like this would be organized,
you have to make it a battle big enough to matter, but small enough to win.
The other problem the U.S. has is geography. The United States is huge, huge. Even with those
community networks in place, you're talking about tens of thousands of them organizing,
trying to make something happen and make it livable once it does.
In the U.S. you would probably need people with platforms, big platforms,
people who could capture the attention of the media,
people who maybe already have a little bit of an image
of being incredibly popular among the working class.
Maybe already have a little bit of an image of being radical.
Maybe they're part of the government,
and their call or their support could lend the idea legitimacy.
Now, I can't think of anybody like that.
But if you can, maybe tweet her the idea.
The other benefit to having somebody inside the establishment supporting it is the establishment
is quicker to take notice because the idea that the working class can organize something
on a national level without them, well that makes them nervous.
Nobody wants that.
So if somebody within the establishment, somebody within government lends their support to it,
all of a sudden they have to take notice and have to take notice quickly because they don't
want the example set of organization without government.
They don't want that to happen.
So if you had this mythical person start making that call, start supporting it, start even
mentioning it.
You could probably get relief and a public health response without ever having the strike
because they don't want it.
That's why they're effective.
Anyway, so that's a general look at a general strike.
That's kind of how they work, how they function, why they're effective, and some of the stuff
that is kind of unique to the U.S.
There are other countries that applies to, but it certainly applies here.
I think it's amazing that this is a topic that is coming up.
It is one of the biggest tools of social change.
It is something that can truly alter the fabric of society.
I don't know that we're at the point where a general strike can occur in the United States,
but the fact that it's being talked about in numbers so big that it becomes a trending
topic on Twitter gives you hope, at least a little.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}