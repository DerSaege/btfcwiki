---
title: Let's talk about how conservatives are backing AOC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2nGKs-ehjNU) |
| Published | 2020/08/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Using an Alice analogy to describe the current state of the United States where labels divide people.
- Conservatives advocate for social democratic policies like health care, housing, free college, and more.
- Despite denying it, conservatives support similar policies for subsets of the population, like the military.
- Conservatives believe these policies make people more productive and efficient.
- They fight for these policies within the military, recognizing its importance.
- The argument that military members "earned" these benefits is refuted as others in society could also benefit.
- Top officials push for division among Americans to maintain control and profit.
- Access to social features wouldn't make people lazy, as conservatives already support them for certain groups.
- Divisions in society benefit those at the top who want to maintain control and profit.
- Allowing access to social features may prevent young individuals from being easily recruited for unnecessary wars.

### Quotes

- "Everything is what it isn't."
- "They are trained to oppose it by those on the top who want those on the bottom divided."
- "Stop turning them into combat vets."

### Oneliner

Beau uses an Alice analogy to show how conservatives inadvertently support social democratic policies and the impact of division on society.

### Audience

Activists, Voters, Community Members

### On-the-ground actions from transcript

- Advocate for social democratic policies in your community (implied)
- Support initiatives that provide healthcare, housing, and education access for all (implied)
- Challenge divisions and work towards unity in your community (implied)

### Whats missing in summary

The full transcript delves into the hidden support conservatives have for social policies and the impact of societal division on maintaining control and profiting from insecurity.

### Tags

#AliceAnalogy #SocialDemocraticPolicies #Conservatives #Unity #CommunityActions


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about another Alice analogy.
The reason I love them is because they are very fitting for today.
You know, the whole idea is that it's nonsense.
Everything is what it isn't.
And that's pretty apt for the current state of the United States, where people who agree
on things will oppose each other simply based on labels.
So today we are going to talk about how conservatives constantly advocate for the policies that
AOC wants.
Really.
In fact, conservatives are actually better at bringing social democratic policies to
the people than social democrats are.
Because conservatives constantly advocate for health care, better health care even.
Guaranteed bank loans for houses, mental health care, free college, food security, housing,
better funded schools, daycare access, legal help for like estate planning and stuff like
that, family planning, and retirement for everybody.
Those are social democratic policies.
That's what AOC and her crew, that's what they want.
Right now the conservatives are like, you got us wrong.
We don't advocate for that.
That's wrong, if Americans had that, well they'd be lazy.
But see they know that's not true because they do advocate for it constantly for a subset
of the population.
And I don't think there is a conservative out there who would be willing to come forth
and say that members of the US military are lazy.
I don't believe that person exists.
The military has all that.
Why?
Because deep down conservatives know that it makes people more productive.
It makes the organization more efficient.
They know this.
That's why they fight so hard for the military to have it.
And if you were to ask conservatives about the military, they would say that it's the
greatest on the planet.
If it makes the military the greatest on the planet, what do you think it would do for
the rest of the country?
The other argument would be that, no, no, no, see those in the military, they earned
it.
They earned it.
The factory worker, the farmer, the truck driver, the teacher, the fireman, they didn't,
I don't think it's that either.
I think they know the country would be better off if people had this.
But they're trained.
They are trained to oppose it by those on the top who want those on the bottom divided,
who don't want the average American to have a sense of security because the system requires
that they be insecure.
So they constantly chase that dollar.
So those at the top can make more dollars.
That's what it's about.
I would suggest that the average conservative knows that this kind of access to these social
features would not make people lazy.
They know it doesn't.
They advocate for it for the very people we expect to be least lazy.
I think it really has to do with them just accepting that those at the top need us divided.
And the other side to this, and one of the other reasons those at the top don't want
everybody to have this, is because if everybody did have it, I imagine it would be a whole
lot harder to get young poor kids to sign up to go fight unnecessary wars so those at
the top can make more money.
You want to support the troops, you want to support the vets, stop turning them into combat
vets.
If everybody had this, that would probably be a step in that direction.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}