---
title: Let's talk about TikTok teens and youth in politics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BV_zT9X1HcY) |
| Published | 2020/08/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of youth involvement in politics, particularly focusing on TikTok teens.
- He challenges the notion that youth are too inexperienced or uneducated to participate in politics by citing historical examples.
- Beau lists several historical figures who made significant contributions to American politics at a young age.
- He argues that the education of young people is the responsibility of the older generation.
- Beau suggests that forward-thinking ideas from younger individuals could benefit society.
- He questions the idea that age is the sole qualifier for understanding politics.
- Beau points out that younger individuals have to live with the consequences of political decisions for a longer time.
- He concludes by encouraging listeners to think about the importance of youth involvement in politics.

### Quotes

- "Young people have a very strong and illustrious tradition of being involved in American politics."
- "It is a citizen's primary job to make sure that the next generation is properly educated."
- "Maybe those are the voices we need to listen to the most."
- "To suggest that age is the only thing that can qualify somebody or even a qualification to understand politics, to have an opinion on it worth listening to, is just wrong."
- "Those who are younger have to live with the consequences a whole lot longer."

### Oneliner

Beau challenges misconceptions about youth in politics, citing historical examples and advocating for forward-thinking ideas from younger voices.

### Audience

Youth, Educators, Activists

### On-the-ground actions from transcript

- Support youth education (exemplified)
- Encourage forward-thinking ideas (exemplified)

### Whats missing in summary

The full transcript provides a historical perspective on youth involvement in politics, urging listeners to value the perspectives of younger individuals and support their education.

### Tags

#YouthInPolitics #Education #ForwardThinking #HistoricalPerspective #CommunityInvolvement


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about the youth.
Those TikTok teens getting involved in politics and how that's a bad thing because they don't
know enough.
They're too young, too inexperienced, don't really know what's going on yet.
They're just not educated enough.
First I would suggest that that's not quite as strong a point as those people who say
it think it is.
But before we get into that, I want to point out some historical realities real quick.
When the Declaration of Independence was signed, John Paul Jones was 28, you know, the father
of the US Navy.
Henry Knox was 25.
He went on to run all of our artillery.
James Madison was 25.
Betsy Ross was 24.
Alexander Hamilton was 21.
Nathan Hale was also 21.
He was involved in Dalton's Rangers, which pretty much the forerunner to all American
special operations.
Aaron Burr was 20.
John Trumbull was 20 as well.
He's the guy who did all the sketches of the British and American lines and went on to
be known as the painter of the revolution.
That painting you've seen of the signing, that was him.
Henry Lee III was 20.
Two years later, he was Major Lee of Lee's Legion and pioneered what we know today as
maneuver warfare.
James Monroe was 18.
Lafayette was 18.
He was a major general by 19.
And just a few short years later in 1781 at a place called Yorktown, his troops bottled
up Cornwallis and won the war.
I'm going to suggest that young people have a very strong and illustrious tradition of
being involved in American politics.
I would also point out that if you're going to say, well, it was different back then.
Young people were better educated.
They knew more.
That's not an indictment of young people today.
That's an indictment of us because we're the ones responsible for their education.
I would suggest that it is a citizen's primary job to make sure that the next generation
is properly educated.
So I'm not sure that our failure would be a mark against them.
I would also suggest that perhaps it's better to get some more forward-thinking ideas, some
ideas that are coming from people who don't think everything's impossible, who don't look
to the past and see that as the future, who aren't stuck in denying that the world is
going to move forward.
Maybe those are the voices we need to listen to the most.
It worked out pretty well for us in the past.
I would imagine that there's a whole lot of really good ideas floating around out there
right now that are being ignored simply because the person who came up with them was the age
of Alexander Hamilton or Aaron Burr or James Monroe or Lafayette.
To suggest that age is the only thing that can qualify somebody or even a qualification
to understand politics, to have an opinion on it worth listening to, is just wrong.
More importantly, I would point out that those who are younger have to live with the consequences
a whole lot longer.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}