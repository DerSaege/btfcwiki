---
title: Let's talk about Trump's promise to the suburban voters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cecY7g0USWg) |
| Published | 2020/08/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing suburban voters and their importance in the upcoming election.
- President Trump's promise to bring law and order to the suburbs.
- Exploring the idea that grievances exist in major cities and must be acknowledged.
- Critiquing Trump's approach of sending in troops without addressing underlying issues.
- The failure of security clampdowns and historical examples of why they don't work.
- Emphasizing the importance of acknowledging grievances to move towards a solution.
- Comparing Trump and Biden's approaches to addressing social unrest.
- Questioning Trump's ability to handle the complex issue of racial injustice.
- Encouraging listeners to think critically about the strategies of political candidates.

### Quotes

- "If you don't address the grievances, this will just continue."
- "Sending in a bunch of cops is probably a bad idea."
- "All that matters is that they believe the grievances are justified."
- "Do you really believe that Donald Trump is up to the task?"
- "But I do know that he has acknowledged the grievances."

### Oneliner

Beau addresses the suburbs, criticizes Trump's approach to unrest, and questions his ability to handle grievances compared to Biden, urging critical thinking.

### Audience

Suburban voters

### On-the-ground actions from transcript

- Campaign for political candidates who acknowledge and plan to address grievances (implied)
- Engage in critical thinking and research on candidates' approaches to social unrest (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's strategy towards suburban voters and social unrest, urging listeners to critically think about the implications and solutions.

### Tags

#SuburbanVoters #SocialUnrest #Election2020 #Grievances #LawAndOrder


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk directly to the suburban voters.
Everybody else is.
I figured I should too, you know.
Everybody's out for the suburb vote right now.
Critical demographic this year.
So we're going to talk about one of the offerings on the table.
Well one of the candidates is offering in order to earn the vote of the suburbs.
We're going to talk about President Trump and what he's promising the people in the
suburbs.
See Trump understands that those in the suburbs see what's happening in major cities and that
they don't want that happening in their neighborhood.
Fair enough, that makes sense.
I get it. Cool.
Understood.
So he's going to promise law and order via tweet, you know.
We're going to send in the cops, send in the troops, do whatever we have to to put this
down.
That sounds good.
Makes sense, right?
But it only makes sense if you believe they're doing it just to do it.
That it really is them just burning down their cities for no reason.
They don't have a legitimate grievance.
They're just mindless.
And that's why he constantly pushes that propaganda.
Because you have to believe that in order to accept his offering.
The reality is they do have grievances.
Racial injustice, income inequality, disparity in the criminal justice system, limited access
to opportunity.
There are grievances.
And pause.
It doesn't matter if you think the grievances are justified sitting in the suburbs.
All that matters is that they believe the grievances are justified.
Your opinion doesn't matter.
Really doesn't.
If they believe those grievances are real, then his strategy doesn't make any sense at
all.
In fact, it will accomplish the exact opposite of what he's promising.
Send in the troops, right?
What can the troops do?
National Guard shows up.
They lock down a specific geographic area.
And in that area, nothing can happen.
If he sends enough troops, yeah.
True enough.
Can lock it down.
But see, it doesn't address the grievances.
So the people engaged in this, they're still going to want to express their frustration.
But they can't do it where they were doing it.
And because income inequality is a thing, they don't have a lot of money to get outside
of that geographic area, to get outside of that security clampdown.
They can only get about as far as your house in the suburbs.
If you don't address the grievances, this will just continue.
A security clampdown always fails.
It's actually in the manuals to not do this.
It really is.
In fact, one of the goals of a movement is to provoke a security clampdown.
Because then you in the suburbs, as that clampdown spreads, you don't like it.
Therefore, you fill some of the grievances yourself.
That's how this works.
It's in the books.
Look back through history.
Any government, any politician that has attempted this, failed.
All they did was spread it.
It's throwing gas on a fire.
I would suggest that just intuitively, if one of the complaints is the treatment they
receive at the hands of law enforcement, that sending in a bunch of cops is probably a bad
idea.
Again, it doesn't matter if you believe their grievances are justified.
All that matters is that they do.
Because they're not going to give up.
We've dealt with this before in this country, the civil rights movement.
And because we didn't deal with it completely, it has cycled back around.
We're going to have to fix this eventually.
This type of social unrest has taxed some of the greatest presidents this country has
ever seen.
It was hard for them.
Do you really believe that Donald Trump is up to the task?
He doesn't even understand the issue.
Or he does and he's lying to you.
Saying that there isn't a reason for the grievance.
Either way, he's probably not going to engage in good faith with the grievances.
Which means it's going to continue.
And if he engages in a security clampdown, it's going to spread.
If you want this in the suburbs, make sure you campaign for Trump.
I don't know Biden's plan to deal with this.
But I do know that he has acknowledged the grievances.
Which is the first step to doing anything about it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}