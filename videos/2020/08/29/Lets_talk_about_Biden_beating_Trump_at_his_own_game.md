---
title: Let's talk about Biden beating Trump at his own game....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3_s08mBjSng) |
| Published | 2020/08/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The internet has given us new tools to connect with people, even those in their own echo chambers.
- In the past, seeking opposing views was necessary; now, it's easier to come across diverse perspectives.
- Individuals still tend to tailor their online feeds to support their beliefs, but there are ways to introduce opposing viewpoints.
- The Biden campaign acquired the domain keepamericagreat.com to showcase the disparities between Trump's promises and actions.
- Trump is depicted as seeking a do-over rather than re-election, shifting blame for failures onto others.
- Trump's lack of leadership and accountability is emphasized.
- Sharing keepamericagreat.com under the guise of supporting Trump could challenge his supporters with contrasting information.
- Trump's campaign lacks substantial policy, focusing instead on denigrating opponents and owning the libs.
- Demonstrating Trump's failures in trolling and belittling could be effective in reaching his supporters.
- Biden's savvy move with campaign domain names could be eye-opening for Trump voters.

### Quotes

- "He wants people to forget the last four years, blame that on the radical left of Joe Biden, and just let him do what has always occurred in his life."
- "Not only is he a failure as a president, he's not even the cool internet troll they thought he was."

### Oneliner

The internet offers tools to challenge echo chambers and confront Trump's lack of leadership and accountability.

### Audience

Social media users

### On-the-ground actions from transcript

- Share keepamericagreat.com on social media under pro-Trump hashtags (implied).

### Whats missing in summary

The full transcript dives into leveraging internet tools to challenge online echo chambers and expose Trump's shortcomings, urging action to confront misinformation and biases.

### Tags

#Internet #EchoChambers #Biden #Trump #SocialMedia #Accountability


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the internet and what a wonderful place it is.
And how it has provided us new tools and new methods of reaching out to people.
People who at times may just want to sit in their own little echo chamber.
Because before this you had to look for an opposing view.
You couldn't just find one, you couldn't stumble upon one by accident.
You had to seek one out.
And now, yeah, people can still tailor their feeds to just reinforce their own beliefs.
People do it all the time.
But there's always a way to sneak in now.
And hypothetically speaking, if the Biden campaign got the domain keepamericagreat.com,
they did.
And they created a website that highlighted everything that Trump said was going to happen
versus what did happen during his administration.
They did.
And just pointed out that he's not running for re-election, he's begging for a do-over.
When you hear his campaign speeches, he's talking about all of the bad things that have
happened in the last four years and blaming it on the Democrats because he can't accept
responsibility for his own failures as president.
He's not a leader, he never has been.
That's what he wants.
He wants people to forget the last four years, blame that on the radical left of Joe Biden,
and just let him do what has always occurred in his life.
Him to be able to lie his way out.
Him to be able to get away without accountability.
That's what he wants.
Now if people were to say, I don't know, start posting keepamericagreat.com on Facebook or
tweeting it out, a little message that makes it seem as though they support Trump.
Trump supporters, they would probably click on that link and they would be confronted
by that.
It may not do any good as far as the policy goes.
But a whole lot of Trump supporters don't care about policy.
They don't know policy because he doesn't have any.
He doesn't actually have policy to run on.
He runs on belittling people, trolling them, owning the libs.
It would probably be pretty effective to show that he can't even do that right anymore.
When Joe Biden, as technologically savvy as he is, was able to take his campaign domain
name.
I think it's probably something that Trump voters should see if anybody was willing to
tweet that out, perhaps under the hashtag Trump 2020 landslide or something along those
lines.
Kind of insert it among a bunch of pro-Trump content.
People would find it.
They'd be confronted with it.
They would have to accept not only is he a failure as a president, he's not even the
cool internet troll they thought he was.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}