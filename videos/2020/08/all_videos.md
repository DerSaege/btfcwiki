# All videos from August, 2020
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2020-08-31: Let's talk about mapping out Trump's decision.... (<a href="https://youtube.com/watch?v=IMfU2NIPyqw">watch</a> || <a href="/videos/2020/08/31/Lets_talk_about_mapping_out_Trump_s_decision">transcript &amp; editable summary</a>)

Beau questions Trump's motives behind adopting a controversial herd immunity strategy and warns against the associated risks, urging responsible health practices for everyone's safety.

</summary>

"He's willing to sacrifice his base."
"As long as his base continues to believe everything that he says, he can get away with this."
"Don't be fooled by this into thinking that herd immunity is suddenly a good idea."
"Wash your hands. Don't touch your face. Don't go out. Stay at home."
"Eventually we will get through this at some point."

### AI summary (High error rate! Edit errors on video page)

Analyzing Trump's decision-making process regarding Dr. Scott Atlas's addition to the task force.
Speculating on whether Trump's change in strategy is a genuine shift or politically motivated.
Questioning the rationale behind adopting a controversial strategy of herd immunity.
Predicting Trump's potential narrative shift towards herd immunity due to vaccine unavailability before the election.
Criticizing the prioritization of reelection over public health and potential risks of herd immunity.
Warning against being misled into thinking herd immunity is a viable solution.
Pointing out the potential negative impact of herd immunity on Trump's reelection chances.
Advocating for responsible health practices like handwashing, wearing masks, and social distancing.

Actions:

for voters, concerned citizens,
Wash your hands, wear a mask, practice social distancing (implied)
Stay at home if possible (implied)
</details>
<details>
<summary>
2020-08-31: Let's talk about keeping your community network going.... (<a href="https://youtube.com/watch?v=QF2U6oplx8Y">watch</a> || <a href="/videos/2020/08/31/Lets_talk_about_keeping_your_community_network_going">transcript &amp; editable summary</a>)

Beau advocates for establishing engaging community networks with diverse activities and resources to enhance civic engagement and local resilience.

</summary>

"Establishing one of these networks is probably one of the more effective things you can do for civic engagement."
"It's always been like that."
"Bring your group together."
"If you haven't started one of these, it might be time."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Advocates for establishing community networks at the local level with friends, coworkers, and committed individuals aiming to better their community.
Emphasizes the importance of keeping community networks engaging and fun to maintain interest and prevent people from losing interest over time.
Suggests adapting activities to cater to different personality types within the group, ensuring inclusivity and sustainability.
Recommends engaging in cross-training and skill-sharing activities to broaden skill sets and enrich the network's members.
Proposes "Adventure Day" as a spontaneous group activity to keep members interested and adaptable, fostering group cohesion.
Encourages building network resources like community gardens, medical kits, and disaster relief stashes to strengthen the group and provide support in times of need.
Advocates for creating community resources such as library boxes or food boxes to benefit not only the network but also the wider community, enhancing public perception and support.
Stresses the significance of community networks in facing challenges and uncertainties, advocating for their establishment to enhance civic engagement and resilience at the local level.

Actions:

for community activists and organizers.,
Establish a community network with friends, coworkers, and committed individuals (suggested).
Organize engaging activities like cross-training, skill-sharing, and Adventure Days within the network (implied).
Build network resources such as community gardens, medical kits, and disaster relief stashes (implied).
Create community resources like library boxes or food boxes for public benefit (implied).
</details>
<details>
<summary>
2020-08-30: Let's talk about back and forth, where we go, and how we got here.... (<a href="https://youtube.com/watch?v=I3KJQ-Cckdg">watch</a> || <a href="/videos/2020/08/30/Lets_talk_about_back_and_forth_where_we_go_and_how_we_got_here">transcript &amp; editable summary</a>)

The country is on the brink of chaos due to divisive rhetoric and the pursuit of power, urging for unity and communication to prevent further escalation.

</summary>

"We are there. We're there. And we got here because a whole bunch of people got really rich dividing this country."
"This country needs people who can unify, needs people who can help heal the wounds that have been torn open by inflammatory rhetoric."
"You need to think back through your life and try to remember any time that was like this."
"This country isn't united, and it's not going to be able to unite under Trump. It will not heal under Trump."
"The most important thing for this country at this moment in time is to take a step back, because once this starts, it doesn't stop easily."

### AI summary (High error rate! Edit errors on video page)

Talks about the current state of affairs in the country, referring to it as a mess and questioning what comes next.
Describes the progression from civil rights debates to violent marches and how it has become an "one for one" situation.
Points out the divisive rhetoric in the media and political landscape, leading to a PR campaign with violence.
Criticizes the role of media personalities and politicians in stoking extreme division for financial and political gain.
Warns about the dangers of the country being torn apart due to inflammatory rhetoric and the pursuit of power.
Urges for leadership that can unite people and heal the wounds caused by division.
Emphasizes the need for individuals to communicate, find common ground, and reject attempts to incite conflict.
Expresses concern about the manufactured enemy narrative created to keep people loyal and divided.
Questions the effectiveness of current leadership in uniting and healing the country.
Encourages people to step back, start dialogues, and prevent further escalation towards chaos.

Actions:

for citizens, activists, leaders,
Communicate with those around you to find common ground and bridge divides (suggested)
Reject attempts to incite conflict and violence in your community (exemplified)
</details>
<details>
<summary>
2020-08-29: Let's talk about Trump's promise to the suburban voters.... (<a href="https://youtube.com/watch?v=cecY7g0USWg">watch</a> || <a href="/videos/2020/08/29/Lets_talk_about_Trump_s_promise_to_the_suburban_voters">transcript &amp; editable summary</a>)

Beau addresses the suburbs, criticizes Trump's approach to unrest, and questions his ability to handle grievances compared to Biden, urging critical thinking.

</summary>

"If you don't address the grievances, this will just continue."
"Sending in a bunch of cops is probably a bad idea."
"All that matters is that they believe the grievances are justified."
"Do you really believe that Donald Trump is up to the task?"
"But I do know that he has acknowledged the grievances."

### AI summary (High error rate! Edit errors on video page)

Addressing suburban voters and their importance in the upcoming election.
President Trump's promise to bring law and order to the suburbs.
Exploring the idea that grievances exist in major cities and must be acknowledged.
Critiquing Trump's approach of sending in troops without addressing underlying issues.
The failure of security clampdowns and historical examples of why they don't work.
Emphasizing the importance of acknowledging grievances to move towards a solution.
Comparing Trump and Biden's approaches to addressing social unrest.
Questioning Trump's ability to handle the complex issue of racial injustice.
Encouraging listeners to think critically about the strategies of political candidates.

Actions:

for suburban voters,
Campaign for political candidates who acknowledge and plan to address grievances (implied)
Engage in critical thinking and research on candidates' approaches to social unrest (implied)
</details>
<details>
<summary>
2020-08-29: Let's talk about Biden beating Trump at his own game.... (<a href="https://youtube.com/watch?v=3_s08mBjSng">watch</a> || <a href="/videos/2020/08/29/Lets_talk_about_Biden_beating_Trump_at_his_own_game">transcript &amp; editable summary</a>)

The internet offers tools to challenge echo chambers and confront Trump's lack of leadership and accountability.

</summary>

"He wants people to forget the last four years, blame that on the radical left of Joe Biden, and just let him do what has always occurred in his life."
"Not only is he a failure as a president, he's not even the cool internet troll they thought he was."

### AI summary (High error rate! Edit errors on video page)

The internet has given us new tools to connect with people, even those in their own echo chambers.
In the past, seeking opposing views was necessary; now, it's easier to come across diverse perspectives.
Individuals still tend to tailor their online feeds to support their beliefs, but there are ways to introduce opposing viewpoints.
The Biden campaign acquired the domain keepamericagreat.com to showcase the disparities between Trump's promises and actions.
Trump is depicted as seeking a do-over rather than re-election, shifting blame for failures onto others.
Trump's lack of leadership and accountability is emphasized.
Sharing keepamericagreat.com under the guise of supporting Trump could challenge his supporters with contrasting information.
Trump's campaign lacks substantial policy, focusing instead on denigrating opponents and owning the libs.
Demonstrating Trump's failures in trolling and belittling could be effective in reaching his supporters.
Biden's savvy move with campaign domain names could be eye-opening for Trump voters.

Actions:

for social media users,
Share keepamericagreat.com on social media under pro-Trump hashtags (implied).
</details>
<details>
<summary>
2020-08-28: Let's talk about the RNC and moderate voters.... (<a href="https://youtube.com/watch?v=ccWLUzFcJeU">watch</a> || <a href="/videos/2020/08/28/Lets_talk_about_the_RNC_and_moderate_voters">transcript &amp; editable summary</a>)

Moderate conservatives face a tough decision between an authoritarian figure and policy differences on the Second Amendment, potentially overcomplicating their voting choice.

</summary>

"You can stand on principle. You can. But if you're going to stand on principle, that means you can't vote."
"It's a tough decision to have to make."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Moderate conservatives faced a tough decision after the Democratic primary, choosing between Biden and Trump.
They were initially drawn to Biden but were concerned about his stance on the Second Amendment.
The Republican National Convention reminded them of the reasons they were willing to cross over to Trump.
Beau points out that Biden may not have the political capital to push through a gun ban.
He suggests that Biden might be lying or throwing red meat to his base.
Even if a gun ban were enacted, Beau questions if moderate conservatives would actively comply.
Beau predicts that if Biden loses, a moderate Republican candidate may promise to repeal the ban.
He argues that using the National Firearms Act to enforce a ban logistically wouldn't work.
Moderate conservatives are torn between an authoritarian figure (Trump) and a candidate with policy differences.
Beau acknowledges the dilemma these conservatives face but suggests they might be overcomplicating their decision-making process.

Actions:

for moderate conservatives,
Analyze and understand the policies of political candidates beyond rhetoric (implied)
Stay informed about the potential implications of proposed policies on issues that matter to you (implied)
</details>
<details>
<summary>
2020-08-27: Let's talk about riding out a storm.... (<a href="https://youtube.com/watch?v=VEwnKqSZuYI">watch</a> || <a href="/videos/2020/08/27/Lets_talk_about_riding_out_a_storm">transcript &amp; editable summary</a>)

Beau provides tips for surviving a storm for those not interested in helping, focusing on preparation, mobility, and self-reliance post-storm.

</summary>

"Mobility is life."
"Comfort items worth their weight in gold."
"Nothing else matters."

### AI summary (High error rate! Edit errors on video page)

Provides tips for people not interested in helping during a storm but wanting to make it through safely.
Emphasizes the importance of preparation and knowing your network.
Advises on staying mobile and being ready to move at all times after the storm.
Suggests seeking shelter in people's residences rather than abandoned warehouses.
Recommends moving together with your group and looking unassuming to avoid drawing unwanted attention.
Stresses the importance of focusing on your circle during the aftermath of the storm.
Mentions the scarcity of emergency services post-storm and the need to rely on your own resources.
Encourages being cautious when encountering groups looking to take advantage of the situation.
Notes the value of comfort items and the uncertainty of when infrastructure will be restored.
Reminds to gather all necessary supplies and be prepared for the possibility of more storms in the future.

Actions:

for survivalists,
Identify your circle and network for emergencies (implied)
Gather necessary supplies and comfort items for survival (implied)
Stay mobile and be prepared to move at all times (implied)
</details>
<details>
<summary>
2020-08-27: Let's talk about a message to the Biden campaign.... (<a href="https://youtube.com/watch?v=ACkEmdWJzFE">watch</a> || <a href="/videos/2020/08/27/Lets_talk_about_a_message_to_the_Biden_campaign">transcript &amp; editable summary</a>)

Moderate conservatives want to reject Trumpism and send a message through their votes, urging the Biden campaign to reconsider the assault weapons ban to secure their support.

</summary>

"They want to send a message to their neighbors, to the marginalized people who have been directly impacted by this administration."
"If you want these votes, you got to drop this."
"You may win without them, but are you going to win in numbers big enough to send that message?"
"This isn't an election between Biden and Trump."
"They will not cross over with this ban on the table."

### AI summary (High error rate! Edit errors on video page)

Moderate conservatives want to see Trump lose, not necessarily support Biden.
They are willing to cross over to send a message against Trumpism.
The assault weapons ban in the Democratic Party platform is a deal-breaker for them.
Poor people could be disproportionately affected by the gun ban due to taxes.
They believe in the right to defend themselves, especially marginalized communities.
Trump's mishandling of public health, civil unrest, and economy reinforces their belief in gun ownership.
Winning the election is not as critical to them as sending a message against Trumpism.
Moderate conservatives view the election as a way to reject Trumpism, not just a choice between Biden and Trump.
Beau urges the Biden campaign to reconsider the assault weapons ban to secure these votes.
Dropping the gun ban could help Biden win in numbers significant enough to send a message.

Actions:

for campaign strategists,
Reassess the assault weapons ban in the Democratic Party platform (suggested)
Engage with moderate conservatives to understand their concerns and motivations (exemplified)
Tailor messaging and policies to appeal to voters beyond traditional party lines (implied)
</details>
<details>
<summary>
2020-08-26: Let's talk about how conservatives are backing AOC.... (<a href="https://youtube.com/watch?v=2nGKs-ehjNU">watch</a> || <a href="/videos/2020/08/26/Lets_talk_about_how_conservatives_are_backing_AOC">transcript &amp; editable summary</a>)

Beau uses an Alice analogy to show how conservatives inadvertently support social democratic policies and the impact of division on society.

</summary>

"Everything is what it isn't."
"They are trained to oppose it by those on the top who want those on the bottom divided."
"Stop turning them into combat vets."

### AI summary (High error rate! Edit errors on video page)

Using an Alice analogy to describe the current state of the United States where labels divide people.
Conservatives advocate for social democratic policies like health care, housing, free college, and more.
Despite denying it, conservatives support similar policies for subsets of the population, like the military.
Conservatives believe these policies make people more productive and efficient.
They fight for these policies within the military, recognizing its importance.
The argument that military members "earned" these benefits is refuted as others in society could also benefit.
Top officials push for division among Americans to maintain control and profit.
Access to social features wouldn't make people lazy, as conservatives already support them for certain groups.
Divisions in society benefit those at the top who want to maintain control and profit.
Allowing access to social features may prevent young individuals from being easily recruited for unnecessary wars.

Actions:

for activists, voters, community members,
Advocate for social democratic policies in your community (implied)
Support initiatives that provide healthcare, housing, and education access for all (implied)
Challenge divisions and work towards unity in your community (implied)
</details>
<details>
<summary>
2020-08-26: Let's talk about TikTok teens and youth in politics.... (<a href="https://youtube.com/watch?v=BV_zT9X1HcY">watch</a> || <a href="/videos/2020/08/26/Lets_talk_about_TikTok_teens_and_youth_in_politics">transcript &amp; editable summary</a>)

Beau challenges misconceptions about youth in politics, citing historical examples and advocating for forward-thinking ideas from younger voices.

</summary>

"Young people have a very strong and illustrious tradition of being involved in American politics."
"It is a citizen's primary job to make sure that the next generation is properly educated."
"Maybe those are the voices we need to listen to the most."
"To suggest that age is the only thing that can qualify somebody or even a qualification to understand politics, to have an opinion on it worth listening to, is just wrong."
"Those who are younger have to live with the consequences a whole lot longer."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of youth involvement in politics, particularly focusing on TikTok teens.
He challenges the notion that youth are too inexperienced or uneducated to participate in politics by citing historical examples.
Beau lists several historical figures who made significant contributions to American politics at a young age.
He argues that the education of young people is the responsibility of the older generation.
Beau suggests that forward-thinking ideas from younger individuals could benefit society.
He questions the idea that age is the sole qualifier for understanding politics.
Beau points out that younger individuals have to live with the consequences of political decisions for a longer time.
He concludes by encouraging listeners to think about the importance of youth involvement in politics.

Actions:

for youth, educators, activists,
Support youth education (exemplified)
Encourage forward-thinking ideas (exemplified)
</details>
<details>
<summary>
2020-08-25: Let's talk about how to take a fall well with your neighbors.... (<a href="https://youtube.com/watch?v=FFTBc1hgGu0">watch</a> || <a href="/videos/2020/08/25/Lets_talk_about_how_to_take_a_fall_well_with_your_neighbors">transcript &amp; editable summary</a>)

Beau stresses the importance of speaking the audience's "language," avoiding hypocrisy call-outs, and focusing on loving one's neighbor to address transgressions effectively.

</summary>

"Don't believe me? Let's say hypothetically there was a well-known religious pundit who got caught up in a lifestyle that was just outside of the norm."
"The failure of most people like this is not the hypocrisy. It's that they didn't love their neighbor as themselves."
"And that's the real problem. Everywhere."

### AI summary (High error rate! Edit errors on video page)

Emphasizes the importance of speaking the same "language" as the audience you are trying to reach to effectively communicate your message.
Talks about framing things in a way that appeals to the target audience, rather than trying to make points that appeal to oneself.
Mentions how individuals often fall into hypocrisy, especially in the conservative movement, and how pointing out this hypocrisy can inadvertently strengthen their position.
Gives an example of a well-known religious figure who falls from grace and manipulates the situation by framing it as a moral lesson about temptation and money.
Argues that focusing on hypocrisy is not the most effective approach, as the real issue lies in marginalizing others and failing to love and accept them.
Encourages using the narrative of loving one's neighbor as a more impactful way to address transgressions and create positive change.

Actions:

for communicators and activists,
Reframe messages to resonate with the audience (exemplified)
Focus on promoting love and acceptance within communities (exemplified)
</details>
<details>
<summary>
2020-08-25: Let's talk about evictions, miners, and Blair Mountain.... (<a href="https://youtube.com/watch?v=z-qZVy0TblY">watch</a> || <a href="/videos/2020/08/25/Lets_talk_about_evictions_miners_and_Blair_Mountain">transcript &amp; editable summary</a>)

Blair Mountain's miners' struggle against oppressive mine operators led to federal legislation and popular support for labor rights, showcasing the impact of grassroots organizing in history.

</summary>

"During an economic downturn, the rich folk couldn't weather the storm, so they further kicked down."
"Rednecks have labor organizing, unions. It's in their DNA."
"All of this happened because during an economic downturn, the rich folk couldn't weather the storm, so they further kicked down."
"They provoked that overreaction during a security clampdown."
"It's a really important little chapter in American history, but it often gets overlooked."

### AI summary (High error rate! Edit errors on video page)

Blair Mountain incident in West Virginia, 99 years ago, had significant impacts on American history and policy due to evictions and miners' struggle.
Mine operators cut wages post-World War I, leading to miners organizing for better conditions like an eight-hour workday and hourly pay.
United Mine Workers played a vital role in organizing miners against oppressive mine operators who enforced their will through gun thugs.
A conflict on May 19, 1920, led to the arrest of gun thugs, with a cop becoming a symbol of miners' struggle for better conditions.
The cop faced charges and later a coal transport station explosion accusation, which intensified the battle for fair treatment.
A sheriff, backed by gun thugs and mining companies, faced off against thousands of miners heading towards Blair Mountain.
Fighting erupted on August 25, 1920, with the miners expecting federal support but ultimately facing arrests and heavy charges.
Despite the union's initial downfall, the miners' actions led to federal legislation and widespread popular support for labor rights.
The term "rednecks" originated from the miners wearing red scarves and gun thugs wearing white armbands at Blair Mountain.
The rich exploiting the economic downturn by pushing down on the little people resulted in evictions and labor struggles.

Actions:

for history enthusiasts, labor rights advocates,
Visit the Mine Wars Museum in West Virginia to learn about the origins of labor organizing and unions (exemplified).
Support grassroots labor organizations and unions in your community (implied).
</details>
<details>
<summary>
2020-08-24: Let's talk about country folk and their responses to the law.... (<a href="https://youtube.com/watch?v=5-v7Juv2bvU">watch</a> || <a href="/videos/2020/08/24/Lets_talk_about_country_folk_and_their_responses_to_the_law">transcript &amp; editable summary</a>)

Beau addresses white country folks, urging them to stay true to their principles and community values amidst societal influences and online disconnect.

</summary>

"People hop on Facebook and you make your little post and you get four or five likes from people you know, people who actually live around you, from your actual physical community."
"Your real principle is something else. And every time this happens, that mask slips a little bit."
"Meanwhile, you're out there cheerleading for Boss Hogg."
"You let Facebook, you let some dude with a red hat change who you are on a level so deep that those people who know you in real life don't want anything to do with you."
"Either way, you better see if those people on Facebook come help you with the storm."

### AI summary (High error rate! Edit errors on video page)

Filming outside due to a storm, discussing a recent incident where the police went too far.
Addressing white country folks about their reactions to such incidents.
Noting the disconnect between online support for police actions and real-life community values.
Sharing how the community comes together to prepare for an upcoming storm, showcasing true solidarity.
Emphasizing the importance of principles over superficial differences like skin tone.
Criticizing those who change their principles based on race and lack consistency.
Mentioning the implications of being untrustworthy in a close-knit community.
Encouraging people to stick to their principles and not waver based on race.
Reminding the audience of historical figures like moonshiners and the origins of the term "redneck" in collective action.
Criticizing the shift in values influenced by social media and external factors.
Urging individuals to stay true to their roots and not be swayed by external influences.

Actions:

for white country folks,
Reconnect with real-life community members to build trust and support (implied).
Uphold consistent principles regardless of skin tone (implied).
Stay true to historical community values like solidarity and collective action (implied).
</details>
<details>
<summary>
2020-08-23: Let's talk about the President's big historic plasma announcement.... (<a href="https://youtube.com/watch?v=-oNFis8FhkQ">watch</a> || <a href="/videos/2020/08/23/Lets_talk_about_the_President_s_big_historic_plasma_announcement">transcript &amp; editable summary</a>)

President Trump's announcement on convalescent plasma isn't a cure-all, just a helpful tool amidst ongoing precautions and the need for a long-term solution.

</summary>

"This isn't a game changer."
"Don't let the president oversell something that really isn't the game changer he's making it out to be."
"We're not even close."
"It's good news for them. It's great news for them."
"Just let's temper the overselling of it."

### AI summary (High error rate! Edit errors on video page)

President Trump made a historic announcement to boost his image before the convention.
Convalescent plasma is beneficial, but it's not a breakthrough or a cure for everyone.
The plasma helps around one-third of people, which is significant but not a complete solution.
It's vital to continue taking precautions like washing hands, wearing masks, and staying home.
The plasma is just one tool in the toolbox until a safe treatment or vaccine is available.
The World Health Organization predicts dealing with the pandemic for another two years.
People should not become complacent or think the situation is resolved due to this development.
Around 30-33% of individuals benefit from the plasma treatment.
Emergency authorization makes the plasma more available, but it's not a drastic change.
It's necessary to maintain caution and not let the positive news lead to lax behavior.

Actions:

for general public,
Continue taking precautions: wash hands, wear masks, stay home (implied)
Stay informed about developments in treatments and vaccines (implied)
Avoid complacency and continue following health guidelines (implied)
</details>
<details>
<summary>
2020-08-23: Let's talk about Democrat run cities.... (<a href="https://youtube.com/watch?v=CzanDzmqbwg">watch</a> || <a href="/videos/2020/08/23/Lets_talk_about_Democrat_run_cities">transcript &amp; editable summary</a>)

Beau debunks the false correlation between Democrat-run cities and high crime rates, attributing crime to poverty and lack of opportunities, not political affiliation.

</summary>

"Crime is related to poverty, income inequality, lack of access to education, stuff like that. Not having opportunities, that's what has a causal link to crime. Not red or blue."
"There is absolutely zero causal link between a party being in control and crime rates. That's not a thing."
"This whole idea is garbage."
"It's almost like maybe somebody he doesn't like kind of came into politics from there and he just won't let it go because he's a petty, petty man."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains how repeating statistics or statistic-like information can lead to false correlations being accepted.
Points out that the President repeatedly mentions "Democrat run cities" to link Democratic policies with high crime rates.
Notes that most cities are Democrat-run because the Democratic party appeals to urban residents.
Mentions that looking at states, many of the top states for violent crime voted for Trump and have Republican governors.
Refers to a company, SafeWise, that ranks dangerous cities, showing that some Republican-run cities have high crime rates.
Counters the idea of a causal link between a party being in control and crime rates, attributing crime to poverty, income inequality, lack of education, and opportunities.
Concludes that the President's claims about Democrat-run cities and high crime rates are baseless and meant to smear certain areas.
Suggests the President's fixation on Chicago may stem from personal grievances rather than factual basis.

Actions:

for fact-checkers, voters, community members,
Fact-check statistics and claims made by politicians (implied)
Support community programs addressing poverty, education, and income inequality (implied)
</details>
<details>
<summary>
2020-08-22: Let's talk about McSally, Arizona's cuisine, and what it means for the GOP.... (<a href="https://youtube.com/watch?v=YydF2r2Krg0">watch</a> || <a href="/videos/2020/08/22/Lets_talk_about_McSally_Arizona_s_cuisine_and_what_it_means_for_the_GOP">transcript &amp; editable summary</a>)

Senator McSally's plea for donations in the Arizona Senate race reveals GOP fundraising struggles, asking individuals to sacrifice meals for political power.

</summary>

"You know, politicians have a long, illustrious history of taking food off of the working person's table, but typically they wait until after the election to do it."
"This is a plea to those without means, to go without so a politician can get into power."
"I'd point out that even Marie Antoinette offered cake."
"If you want to skip a meal so billionaires can get tax cuts, that's on you."
"Anyway, it's just a thought. I have a good night."

### AI summary (High error rate! Edit errors on video page)

Senator Martha McSally's plea for donations in the Arizona Senate race suggests GOP fundraising struggles.
McSally's message implies a need for resources to compete but at the expense of individuals' meals.
The plea for donations, even as low as a dollar, raises concerns about the party's financial situation.
The senator's message may indicate a desire for new leadership among the populace.
Beau criticizes the plea, pointing out that it asks those without means to sacrifice for politicians.
Comparing the situation to Marie Antoinette's offer of cake, Beau suggests this is a negative sign for the GOP.
Beau notes that McSally's opponent is an astronaut, questioning the necessity of such fundraising tactics.
Criticism is directed at the idea of sacrificing meals for political campaigns, especially for billionaires' benefit.
Beau implies that voters in Arizona should carefully weigh their choices without outside influence.
The message concludes with Beau leaving it to viewers to decide on the implications of the situation.

Actions:

for voters in arizona,
Vote thoughtfully in Arizona Senate race (implied)
</details>
<details>
<summary>
2020-08-22: Let's talk about California and one of my favorite holidays.... (<a href="https://youtube.com/watch?v=MBH2fejaHwc">watch</a> || <a href="/videos/2020/08/22/Lets_talk_about_California_and_one_of_my_favorite_holidays">transcript &amp; editable summary</a>)

Beau introduces the unconventional holiday "Think a Criminal Day" while discussing California's wildfire situation and the involvement of soldiers due to inmate firefighters' unavailability, prompting reflection on the criminal origins of some historical heroes.

</summary>

"Therefore, August 22nd is Think a Criminal Day."
"How many of them had mug shots?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of California and a unique holiday celebrated by about 500 people for the past five years.
Mentions a veteran named Brian who made a thought-provoking statement about the origins of freedoms and rights.
Describes the unconventional holiday, Think a Criminal Day, on August 22nd, honoring those who were once deemed criminals but later recognized as heroes.
Explains the current situation in California with over two dozen major wildfires and 300 smaller ones.
Points out the usual practice of using inmates to fight fires for minimal pay but how half the teams are now unavailable due to social distancing measures inside prisons.
Notes that due to the lack of inmate firefighters, the governor called in soldiers from the National Guard to assist.
Raises the question of how many historical heroes had mug shots, linking back to the idea of criminals contributing to positive change.
Encourages viewers to ponder on the concept shared and wishes them a good day.

Actions:

for community members,
Support firefighter relief efforts (exemplified)
Volunteer for wildfire aid organizations (exemplified)
</details>
<details>
<summary>
2020-08-21: Let's talk about the USPS and why the Postal Service always needs money.... (<a href="https://youtube.com/watch?v=fojUfG8miCY">watch</a> || <a href="/videos/2020/08/21/Lets_talk_about_the_USPS_and_why_the_Postal_Service_always_needs_money">transcript &amp; editable summary</a>)

The Postal Service seeks funding as a vital government service, critical for democracy, facing challenges due to misconceptions and political influences.

</summary>

"The Postal Service isn't asking for a bailout, it's asking for funding."
"The postal service is not a business. It's a government service."
"He wants to suppress the vote because he doesn't care about the Constitution. He doesn't care about democracy. He doesn't care about the republic, and he never has."

### AI summary (High error rate! Edit errors on video page)

Addresses the financial struggles of the Postal Service and its frequent need for funding from Congress.
Points out that the Postal Service is a government service, not a business, explicitly authorized in the Constitution.
Mentions the influence of campaign contributions on decisions related to the Postal Service, citing Mitch McConnell's ties to UPS and FedEx.
Raises concerns about sacrificing services for rural areas if the Postal Service faces funding challenges.
Emphasizes the importance of government services like the Postal Service, even if they don't turn a profit.
Criticizes the prioritization of money over providing an essential service, referencing the Flint water crisis.
Stresses the significance of the Postal Service in maintaining fair voting practices, especially during times of crisis.
Condemns actions that hinder mail-in voting and suggests they are aimed at suppressing the vote.
Expresses a lack of care for democracy, the Constitution, and the republic by certain individuals in power.
Advocates for understanding the true nature of the Postal Service as a government service and the need to support vital services.

Actions:

for voters, postal service supporters,
Contact your representatives to advocate for adequate funding for the Postal Service (implied)
Support initiatives that aim to protect and enhance mail-in voting processes (implied)
</details>
<details>
<summary>
2020-08-20: Let's talk about mistakes at the DNC, framing, and solutions.... (<a href="https://youtube.com/watch?v=mJwsZ8mm9v0">watch</a> || <a href="/videos/2020/08/20/Lets_talk_about_mistakes_at_the_DNC_framing_and_solutions">transcript &amp; editable summary</a>)

The Democratic Party's pursuit of drastic gun regulations risks alienating voters and may not effectively address underlying societal issues.

</summary>

"Aside from that, they pushed away those voters, at the very least risked them. And they got nothing for it."
"It's more dramatic because it's, I mean, it is more dramatic. So it gets the headlines."
"Address the motive, address the reasons it's happening, the breakdowns in society that are causing this."

### AI summary (High error rate! Edit errors on video page)

The Democratic Party's pursuit of drastic gun regulations risks alienating potential voters, including disaffected Republicans.
Implementing an assault weapons ban may not be effective in gaining votes and could push away moderate voters.
The focus on gun regulations could undercut the Democrats' message about the dangers of Trump's administration.
The United States' unique gun ownership landscape, with more guns than people, complicates the effectiveness of proposed bans.
Previous assault weapons bans were critiqued for being ineffective and not addressing the core issues.
Implementing bans on semi-automatic firearms may not be logistically feasible due to the high number of guns in circulation.
Any significant gun regulation must address loopholes related to animal cruelty and domestic violence convictions.
Secure storage requirements for firearms and raising the minimum age for purchasing guns are suggested as effective measures with minimal pushback.
Comprehensive education campaigns and identifying behavioral predictors are vital in preventing gun violence.
Addressing social issues like mental health, education, and providing support systems is key to changing the culture around guns.

Actions:

for politically engaged citizens,
Close loopholes related to animal cruelty and domestic violence convictions (suggested)
Advocate for secure firearm storage requirements (suggested)
Support raising the minimum age for purchasing guns (suggested)
</details>
<details>
<summary>
2020-08-20: Let's talk about Steven Bannon's situation and what we can learn.... (<a href="https://youtube.com/watch?v=m7iMuGZYlIs">watch</a> || <a href="/videos/2020/08/20/Lets_talk_about_Steven_Bannon_s_situation_and_what_we_can_learn">transcript &amp; editable summary</a>)

Steve Bannon's charges reveal loyalty to political parties over personal interests, urging Americans to focus on policy, not party.

</summary>

"Parties are really there to assist in the good form of corruption."
"Americans have decided that it's easier to look for an R or a D than it is to actually look at policy."
"You have people who are defending the people who took advantage of them personally."

### AI summary (High error rate! Edit errors on video page)

Steve Bannon, former campaign advisor to Donald Trump, is facing charges related to a campaign to raise money for a border wall.
Bannon and others allegedly pocketed some of the money meant for the wall and used it for personal expenses.
If convicted, Bannon could face seven to nine years in prison.
Bannon has pleaded not guilty, but there's a possibility of a plea deal for a reduced sentence.
Social media response to Bannon's charges revealed Republicans blaming Democrats, showing party loyalty above all else.
Political parties in the US have created a situation where supporters defend party members even if they harm them personally.
The focus on party loyalty over policy has led to defending actions against one's own interests.
Parties were originally meant to assist in getting legislation passed but now contribute to corruption.
Americans need to shift focus from party affiliation to policy and its long-term effects.
The current situation in the US is a result of prioritizing party allegiance over examining policy effects.

Actions:

for citizens, voters,
Examine policies over party loyalty (implied)
Advocate for transparency and accountability in political actions (implied)
</details>
<details>
<summary>
2020-08-19: Let's talk about building Democratic bridges and why we called Mexie.... (<a href="https://youtube.com/watch?v=A2cxHr7RkpI">watch</a> || <a href="/videos/2020/08/19/Lets_talk_about_building_Democratic_bridges_and_why_we_called_Mexie">transcript &amp; editable summary</a>)

Beau explains why reaching out to radical leftists involves framing support for Biden-Harris as harm reduction rather than alignment with their beliefs.

</summary>

"Biden and Harris are less bad than Trump-Pence."
"Harm reduction, that's the way you're going to reach them."
"Most people watching this channel are social Democrats."
"Almost everybody who watches this channel is on the lower axis. They're anti-authoritarian."
"Establish the bus that everybody's going to get on as defeating Trump."

### AI summary (High error rate! Edit errors on video page)

Talks about communicating, building bridges, and engaging with different political ideologies.
Explains the rationale behind choosing to feature a radical leftist named Maxi.
Addresses the question of why showcase a group opposed to Biden-Harris during a critical election period.
Describes the challenges in reaching out to radical leftists who do not see Biden-Harris as favorable.
Explores the political spectrum beyond left and right, discussing the up and down authoritarian axis.
Points out that radical leftists view Biden and Harris as unsupportive of their anti-capitalist and anti-authoritarian beliefs.
Advocates for framing support for Biden-Harris as harm reduction rather than focusing on their positive attributes.
Suggests finding common ground on issues related to marginalized people and workers rather than broader topics like the economy.
Differentiates between supporting Democrats out of alignment with their platform versus seeing them as less harmful.
Emphasizes the importance of defeating Trump as a unifying goal, especially for those who do not fully support Biden and Harris.

Actions:

for community members, political activists.,
Challenge yourself to find common ground with individuals holding different political beliefs (implied).
Prioritize harm reduction narratives when engaging with individuals who do not fully support a particular candidate (implied).
Engage in respectful discourse and bridge-building efforts across political spectrums (implied).
</details>
<details>
<summary>
2020-08-19: Let's talk about an update on Florida's teachers in Martin County.... (<a href="https://youtube.com/watch?v=6nHIdSbG7rE">watch</a> || <a href="/videos/2020/08/19/Lets_talk_about_an_update_on_Florida_s_teachers_in_Martin_County">transcript &amp; editable summary</a>)

Politicians' talking points fail to change reality for teachers lacking support, while prioritizing school reopenings for economic gains amid a pandemic.

</summary>

"Politicians' talking points don't alter reality."
"Teachers lack training, funding, and logistical support."
"Many politicians prioritize reopening schools for economic reasons."
"Cute speeches don't change reality."
"Countries that shut down effectively are doing much better."

### AI summary (High error rate! Edit errors on video page)

Politicians' talking points don't alter reality, regardless of phrasing or metaphors used.
Governor DeSantis referenced Martin County's school teachers, drawing a controversial comparison.
Teachers lack training, funding, and logistical support to provide in-person instruction effectively.
The Martin County Public School Teachers Union President criticized the situation as a logistical nightmare.
Despite motivational speeches, no practical backup was provided, leading to student and employee quarantines.
Many politicians prioritize reopening schools for economic reasons, not genuine concern for education.
The focus is on providing childcare so parents can return to work.
Beau suggests that economic assistance is necessary for parents to stay home with their kids during the pandemic.
Countries that shut down effectively are faring better economically and in terms of public health.
Cute speeches from politicians do not change the harsh reality faced by many, as evidenced by the 170,000 lives lost in the country.

Actions:

for politically aware citizens,
Contact Senators and Representatives for a plan providing economic assistance to families (suggested)
Advocate for properly funded schools and logistical support for teachers (implied)
</details>
<details>
<summary>
2020-08-18: Let's talk about liberals, the left, and a phone call.... (<a href="https://youtube.com/watch?v=NnVQ7UKNr1o">watch</a> || <a href="/videos/2020/08/18/Lets_talk_about_liberals_the_left_and_a_phone_call">transcript &amp; editable summary</a>)

Beau breaks down the distinctions between liberals, leftists, socialists, and more in American politics, advocating for systemic change over minor adjustments.

</summary>

"Leftists know that the whole system is corrupt. The contradictions of capitalism will always erode any of the good tweaks that we try to make under this broader system."
"What's scary is letting the status quo continue."
"We have the infrastructure. We have the technology. We have the resources. We have the money to make sure that everyone is taken care of right now. And we're just not doing it?"

### AI summary (High error rate! Edit errors on video page)

Explains the differences between liberals, leftists, social democrats, socialists, communists, Marxists, and democratic socialists in the American political landscape.
Leftists are those who want to fundamentally change the oppressive system, not just make minor adjustments like liberals.
Social Democrats fight for more social programs within the existing system.
Socialists advocate for a system where workers control their workplaces, not under capitalist exploitation.
Communists envision a classless, stateless society where needs are met, not profits generated.
Marxists work towards socialism and use Marxist economic analysis to understand the world.
Democratic socialists vary in mainstream perception pushing for social democratic policies or socialist transformation through voting.
Liberals believe in reforming capitalism with minor tweaks, while leftists see the entire system as corrupt and exploitative.
Leftists do not support capitalism as they view it as perpetuating inequalities.
The importance of leftist ideas lies in challenging the status quo and advocating for a more equitable society.

Actions:

for political enthusiasts,
Join organizations advocating for systemic change (implied)
Study Marxist economic analysis to understand the economy better (implied)
Advocate for social programs to curb neoliberal excesses (exemplified)
</details>
<details>
<summary>
2020-08-18: Let's talk about Trump's response to Michelle Obama at the DNC.... (<a href="https://youtube.com/watch?v=ZkOLAtehuFI">watch</a> || <a href="/videos/2020/08/18/Lets_talk_about_Trump_s_response_to_Michelle_Obama_at_the_DNC">transcript &amp; editable summary</a>)

Beau criticizes Trump's divisive tactics and lack of leadership, pointing out his reliance on fear-mongering and failure to prioritize national security over personal vendettas.

</summary>

"He's a failure. He's low-performing. That's the reason he's lashing out like a Halo 3 player."
"He's not going to be able to protect you from whatever it is he tells you to be afraid of."
"If you fall for it, you are gullible."
"His whole goal is to gather a group of low-information, uneducated voters and tell them to fear everything."
"He doesn't take national security seriously, obviously."

### AI summary (High error rate! Edit errors on video page)

President Trump tweeted multiple times in response to former First Lady Michelle Obama's criticism during the Democratic National Convention.
Trump referred to himself in the third person in his tweets, claiming he wouldn't be in the White House without Obama.
Beau argues that Trump's presidency is a result of gathering uneducated, racist white supporters and stoking fear among the population.
Trump focused on attacking the Obama administration instead of addressing current issues effectively.
Beau criticizes Trump for prioritizing poll numbers over the lives lost during the current health crisis.
Trump accused the Obama-Biden administration of being corrupt, including baseless claims of spying on his campaign.
Beau points out Trump's lack of understanding of treason, as he misuses the term in his tweets.
Trump's deflection towards a former government employee, Miles Taylor, showcases his ignorance and lack of seriousness about national security.
Beau contrasts the divisiveness under the Obama administration with the current level of hatred and anger in the country under Trump.
Beau concludes by criticizing Trump's leadership, stating that he thrives on division and preys on the fears of gullible individuals.

Actions:

for voters, concerned citizens,
Challenge fear-mongering narratives in your community (implied)
Prioritize understanding issues over falling for manipulative tactics (implied)
Educate others on the importance of national security (implied)
</details>
<details>
<summary>
2020-08-17: Let's talk about the not so liberal Democratic National Convention.... (<a href="https://youtube.com/watch?v=4TefiPT3C0A">watch</a> || <a href="/videos/2020/08/17/Lets_talk_about_the_not_so_liberal_Democratic_National_Convention">transcript &amp; editable summary</a>)

Democratic National Convention aims to show a center-right approach, appealing to moderate Republicans and targeting Trump's denial of re-election, despite potential discontent from the left.

</summary>

"It does reiterate the point that this is going to be a very center-right administration."
"Their goal is to deny Trump a re-election, period."
"That's who this is supposed to appeal to."

### AI summary (High error rate! Edit errors on video page)

Democratic National Convention aims to show a center-right Biden-Harris administration, appealing to moderate Republicans.
Strategy includes snubbing progressives like the squad in favor of literal Republicans.
While left-leaning individuals may be upset, the strategy is likely working by attracting moderate Republicans.
Contrasts DNC's bipartisan unity approach with the expected Trump-centric Republican convention.
Goal is to deny Trump re-election, focusing on stopping him rather than implementing liberal policies.
Administration is projected to be bland, centered on countering Trump's impact.
Emphasizes that the strategy targets center-right and moderate Republicans, rather than left or progressive individuals.
Effectiveness of the strategy in gaining votes remains to be seen, but social media reactions suggest it is working.
Appeals to those who may have previously supported Trump but now have doubts.
Overall strategy focuses on denying Trump a second term, with success likely among certain Republican demographics.

Actions:

for political observers,
Engage in political discourse with moderate Republicans to understand their perspectives (implied)
Share information about the DNC's strategy with others to raise awareness (exemplified)
</details>
<details>
<summary>
2020-08-17: Let's talk about how Trump is playing Kamala Harris like a video game.... (<a href="https://youtube.com/watch?v=ADaDy7ClSDQ">watch</a> || <a href="/videos/2020/08/17/Lets_talk_about_how_Trump_is_playing_Kamala_Harris_like_a_video_game">transcript &amp; editable summary</a>)

Trump's behavior towards female leaders mirrors a study on male hostility in gaming, revealing deep-seated insecurities and inadequacies.

</summary>

"Trump is playing Harris like a video game."
"Politics isn't a video game, especially on the international stage."
"Deep down he behaves this way because he knows he's a failure."

### AI summary (High error rate! Edit errors on video page)

Trump is playing Harris like a video game, revealing insights into himself.
A study on Halo 3 explained why women players face hostility in male-dominated spaces.
Poor performing males reacted hostilely to women disrupting male hierarchy.
They were submissive to male players but hostile towards women.
Higher skilled men reacted positively to women teammates.
Trump's interactions with world leaders mirror these findings.
Putin and others can manipulate Trump, showing his submissive behavior.
Trump reacts negatively to women leaders, like Greta Thunberg.
Politics isn't a game with clear scores; Trump's behavior stems from personal acknowledgment of failure.
Trump's international behavior reveals his deep-rooted insecurities and inadequacies.

Actions:

for political observers, feminists,
Analyze and challenge power dynamics in male-dominated spaces (implied)
Support women leaders and challenge gender biases in politics (implied)
</details>
<details>
<summary>
2020-08-16: Let's talk about Trump, Snowden's pardon, and USPS.... (<a href="https://youtube.com/watch?v=5dJLmOT0fCw">watch</a> || <a href="/videos/2020/08/16/Lets_talk_about_Trump_Snowden_s_pardon_and_USPS">transcript &amp; editable summary</a>)

Trump's consideration of pardoning Edward Snowden is a distraction from his damaging actions against the U.S. Postal Service, which is critical for the election.

</summary>

"This is an attempt to draw attention away from Trump's move against the United States Postal Service because that is what is in the news cycle right now."
"The U.S. Postal Service is pretty critical to the election, and Trump's moves against it are far more damaging to the United States than anything, anything leaked by Snowden."

### AI summary (High error rate! Edit errors on video page)

Trump's consideration of pardoning Edward Snowden is a distraction from his attack on the United States Postal Service.
Trump's past tweets about Snowden show a different stance than his current consideration of a pardon.
Trump's administration is more damaging to the U.S. than Snowden ever was.
Beau supports a pardon for Snowden, believing he acted to honor his oath.
Trump's move is to shift focus from the Postal Service and create a new debate.
The U.S. Postal Service is vital for the election and Trump's actions against it are more harmful than Snowden's leaks.

Actions:

for voters, concerned citizens,
Contact your representatives to express support for the U.S. Postal Service and demand protection for its critical role in the election (implied).
</details>
<details>
<summary>
2020-08-16: Let's talk about Robert Trump, it is what it is, and FDR.... (<a href="https://youtube.com/watch?v=RHYHuFxLeEA">watch</a> || <a href="/videos/2020/08/16/Lets_talk_about_Robert_Trump_it_is_what_it_is_and_FDR">transcript &amp; editable summary</a>)

Beau dissects reactions to Robert Trump's passing, questioning the acceptance of failures and urging a shift towards noble social values in leadership amidst the ongoing crisis.

</summary>

"We have nothing to fear but fear itself."
"One is a tragedy, a million is a statistic."
"If you're looking for change, if you're looking to make America great again, you probably don't want a leader who is that accepting of his own failures."

### AI summary (High error rate! Edit errors on video page)

President Trump's brother, Robert Trump, passed away, sparking different reactions and trends on Twitter.
The phrase "wrong Trump" trended on Twitter, causing moral outrage and demands for compassion.
Some individuals demanded condolences for Robert Trump, while others dismissed the situation with "it is what it is."
Beau points out the irony of those demanding compassion now, who previously prioritized economy over lives.
Fear, according to Beau, is a necessary emotion that keeps individuals alive, especially in justifiable circumstances.
Beau references FDR's quote about fearing fear itself, questioning its relevance and meaning in today's context.
He dives into FDR's inaugural speech, focusing on the rejection of materialism and the pursuit of noble social values.
Beau criticizes those using FDR's quote to justify prioritizing monetary profit over social values and public well-being.
The speech condemns the callous and selfish pursuit of material wealth, urging a shift towards more noble societal values.
Beau underscores the need to address each statistic as a tragedy rather than dismissing them as mere numbers.
He advocates for proactive efforts to address issues instead of using statistics as excuses for inaction.
Beau acknowledges historical examples where people found leadership and worked for change instead of succumbing to defeatism.
While Beau acknowledges the importance of mourning individuals like Robert Trump, he also stresses the broader systemic issues at play.
He contrasts past leaders with the current political climate, questioning the acceptance of failure and lack of empathy in leadership.
Beau concludes by encouraging reflection on the type of leadership needed for positive change in America.

Actions:

for americans,
Seek leadership that prioritizes noble social values over material profit (implied).
Advocate for proactive efforts to address societal issues instead of dismissing them as statistics (implied).
</details>
<details>
<summary>
2020-08-16: Let's talk about Arizona, Georgia, and doing what you're told.... (<a href="https://youtube.com/watch?v=aOqShir_PYY">watch</a> || <a href="/videos/2020/08/16/Lets_talk_about_Arizona_Georgia_and_doing_what_you_re_told">transcript &amp; editable summary</a>)

Beau raises the importance of doing what's expected to protect children, not just following orders, amidst school reopening debates.

</summary>

"Do what's expected of you."
"Your job is to protect those kids."
"Don't let a government authority put your child at risk."
"They care about money and power."
"The fact that the person has a government title means nothing."

### AI summary (High error rate! Edit errors on video page)

Raises the difference between doing what you're told and doing what's expected of you.
Talks about the situation in Arizona where teachers are pushing back against reopening schools.
Mentions teachers in Georgia also resisting the pressure to reopen schools in person.
Shares a story of a parent in Georgia who pulled her kids out of school due to safety concerns.
Questions the authority of governors pushing for school reopenings.
Emphasizes that protecting kids is the community's responsibility, not the governors'.
Criticizes the prioritization of money and power over safety by governmental authorities.
Reminds that Americans should hold government accountable and prioritize the well-being of children.
Advocates for online instruction as the least risky option during the pandemic.
Calls for innovation in the education system and for parents to prioritize their children's safety.

Actions:

for parents, teachers, community members,
Support teachers pushing back against unsafe school reopenings (implied).
Participate in community actions like call floods if teachers request support (implied).
Prioritize children's safety over governmental orders (implied).
</details>
<details>
<summary>
2020-08-15: Let's talk about what 2020 can teach us about the last ten years.... (<a href="https://youtube.com/watch?v=SEU_AQcUVjI">watch</a> || <a href="/videos/2020/08/15/Lets_talk_about_what_2020_can_teach_us_about_the_last_ten_years">transcript &amp; editable summary</a>)

2020 teaches us not to trust politicians on science, prioritize urgent action over their economic interests, and elect officials who address critical issues promptly.

</summary>

"Don't trust politicians when it comes to science."
"We have to act or it's going to get out of hand."
"Politicians will always put the economy, their pocketbook, over you."
"It's going to be too late. It's going to get way out of hand."
"If we wait, by the time we get to the point where everybody knows that the politicians lied, it's going to be too late."

### AI summary (High error rate! Edit errors on video page)

2020 teaches a lesson on not trusting politicians when it comes to science.
Politicians have made inaccurate claims about the disappearance of cases and impact on kids regarding the current issue.
Experts have consistently warned about the seriousness of the situation and the need for action.
Politicians prioritize the economy over the well-being of the people, especially in matters of science.
Climate data from the last 10 years shows alarming trends, but politicians downplay the severity of the situation.
Scientists stress the importance of immediate action to prevent the situation from escalating further.
The urgency to elect officials who prioritize addressing critical issues like climate change.
Voters should hold politicians accountable for prioritizing short-term gains over long-term consequences.
Delaying action on pressing issues can lead to catastrophic outcomes.
Beau urges the American people to learn from past mistakes and not wait until it's too late to act.

Actions:

for voters, concerned citizens,
Elect officials willing to prioritize urgent matters like climate change (implied).
Hold politicians accountable for prioritizing short-term gains over long-term consequences (implied).
</details>
<details>
<summary>
2020-08-15: Let's talk about UBI and living wages.... (<a href="https://youtube.com/watch?v=1ZADrnNuiPk">watch</a> || <a href="/videos/2020/08/15/Lets_talk_about_UBI_and_living_wages">transcript &amp; editable summary</a>)

Criticizing flawed arguments against UBI, Beau challenges the belief that providing a basic income will disincentivize work and argues for fair wages for all.

</summary>

"Being poor is a lack of cash, not a lack of character."
"Wages stagnated. And now they're so out of whack, what it
will take to bring them into where they should be seems just out there."
"People didn't get to where they were at simply because they made good choices or made bad choices."

### AI summary (High error rate! Edit errors on video page)

Criticizes a flawed argument against Universal Basic Income (UBI) that suggests it will disincentivize work.
Questions why people take on college debt or aim for higher-paying jobs if a basic income is provided.
Challenges the notion that minimum wage workers will stop working if given a basic income.
Points out that being poor is a lack of cash, not a lack of character, and everyone desires better opportunities.
Argues that stagnant wages lead to people being taken advantage of by both companies and the government.
Advocates for fair wages and criticizes the idea that some individuals deserve to be in poverty.
Stresses the importance of not looking down on those with less power or influence in society.

Actions:

for advocates for fair wages.,
Advocate for fair wages for all workers (implied).
Support policies that uplift individuals out of poverty (implied).
</details>
<details>
<summary>
2020-08-14: Let's talk about work stoppages, generally.... (<a href="https://youtube.com/watch?v=P46s6djBBoU">watch</a> || <a href="/videos/2020/08/14/Lets_talk_about_work_stoppages_generally">transcript &amp; editable summary</a>)

Suggestions of a mass strike emerge in the US due to Congress's lack of relief for normal folks, necessitating community networks and specific demands for effective action.

</summary>

"Everybody stops working."
"You have to make this a battle big enough to matter, but small enough to win."
"The fact that it's being talked about in numbers so big that it becomes a trending topic on Twitter gives you hope."
"It is one of the biggest tools of social change."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Congress left without providing relief for normal folks.
Suggestions of a mass strike are surfacing across different circles.
Strikes have a history dating back to 500 BC in Rome.
Strikes work because they impact everyone, not just one industry.
Government reacts to strikes due to economic impact and lack of control over them.
Community networks are vital for organizing strikes.
Pitfalls in the US for a general strike include polarization and geography.
Strikes in the US must focus on specific, unifying issues to be effective.
A call for relief and a public health response could be key demands.
Having influential figures supporting the strike idea can lend it legitimacy and expedite government response.

Actions:

for working-class activists,
Organize community networks for strike preparation (implied)
Rally influential figures to support the strike idea (implied)
</details>
<details>
<summary>
2020-08-13: Let's talk about the Post Office and Trump.... (<a href="https://youtube.com/watch?v=8EaA2Zt2vDw">watch</a> || <a href="/videos/2020/08/13/Lets_talk_about_the_Post_Office_and_Trump">transcript &amp; editable summary</a>)

Trump's attempt to manipulate the post office to sway the election reveals a dire need for Americans to prioritize democracy over personality and remove him from power.

</summary>

"The post office is explicitly mentioned in the Constitution."
"If you think the post office is supposed to turn a profit, you have been tricked."
"This is Trump trying to rig the election, trying to cheat."
"Trump needs to go."
"This is what happens when you have a cult of personality."

### AI summary (High error rate! Edit errors on video page)

Trump is attempting to subvert the post office to influence the election outcome in his favor.
The post office is explicitly mentioned in the Constitution under Article 1, section 8, showcasing its significance.
Unlike other government services, the post office is not meant to turn a profit; it is a vital government service.
The idea that the post office should be profitable is a misconception pushed to pave the way for privatization.
Trump's actions regarding the post office are part of his pattern of trying to manipulate and cheat in elections.
There should be strong opposition to Trump's attempts to interfere with the election through the post office.
Trump's behavior is reflective of prioritizing personality over policies and party over principles.
Americans must question if they are tired of Trump's disregard for the Constitution and the rule of law.
It is imperative to move away from supporting individuals or parties blindly and focus on policies instead.
Ultimately, the call is for Trump to be removed from power to safeguard democracy.

Actions:

for concerned american citizens,
Contact Congress members to push back against attempts to undermine the election (exemplified)
Advocate for protecting democracy by raising awareness about the importance of upholding the rule of law (suggested)
</details>
<details>
<summary>
2020-08-13: Let's talk about teachers, expectations, and SEALs.... (<a href="https://youtube.com/watch?v=K0_HK9Ma2gc">watch</a> || <a href="/videos/2020/08/13/Lets_talk_about_teachers_expectations_and_SEALs">transcript &amp; editable summary</a>)

Governor DeSantis compares teachers to seals, but Beau questions the practicality and risks without proper training and support.

</summary>

"Expect our teachers to behave like seals."
"Treating teachers like SEALs right now is risking their life."
"This is a little outside of their scope."
"They won't even get a flag."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Governor DeSantis stumbled into a good idea, urging teachers to be like seals in the Martin County school system.
Teachers need proper training, not just guidelines on infection control, to provide in-person instruction.
Training should be hands-on, rehearsals should be done repeatedly until teachers can handle situations in their sleep.
Teachers need an inexhaustible budget for supplies, tools, training, and support.
They should have access to experts, advisors, and total information awareness for student welfare.
Expecting teachers to be like SEALs without proper resources is risky and unrealistic.
SEALs train for years to improvise in the field; teachers are trained to teach, not manage public health crises.
The comparison between teachers and seals is empty rhetoric if teachers aren't given necessary support.
Treating teachers like SEALs without proper training risks their lives without recognition.
Beau questions the sincerity behind the rhetoric of treating teachers like SEALs for motivation.

Actions:

for teachers, education boards,
Provide teachers with hands-on training on infection control (implied).
Ensure teachers have access to necessary supplies, tools, and resources for effective teaching (implied).
Establish a support system with experts available 24/7 for teachers (implied).
</details>
<details>
<summary>
2020-08-12: Let's talk about the Biden Harris campaign and what it means for us.... (<a href="https://youtube.com/watch?v=B3_QS9tUpdE">watch</a> || <a href="/videos/2020/08/12/Lets_talk_about_the_Biden_Harris_campaign_and_what_it_means_for_us">transcript &amp; editable summary</a>)

The Biden-Harris campaign strategy targets moderates, alienating leftists, while community action becomes vital for systemic change seekers.

</summary>

"The Democratic Party has decided to forego those votes. We don't need the leftist vote."
"If you are a liberal, if you are a Democratic party loyalist, you've got a ticket that might win."
"There is no cavalry coming. It's just us. We have to make the changes."
"Those community networks are more important than ever."
"Being active, beyond voting, this isn't a once every four year or once every two year thing."

### AI summary (High error rate! Edit errors on video page)

Explains the strategy behind the Biden-Harris campaign and why it seems to alienate some Democratic voters.
Describes how the Democratic Party is targeting moderate voters rather than leftists to secure votes and deny votes to Trump.
Points out that the campaign aims to beat Trump on Twitter and personal attacks rather than policy.
Notes the importance of Black women as loyal Democratic voters and how Harris appeals to them.
Analyzes the dynamics of the Biden-Harris campaign and how they plan to counter Trump's attacks.
Urges viewers who seek deep systemic change to focus on down-ballot races for real change.
Addresses the different schools of thought among leftists regarding voting in the upcoming election.
Emphasizes the need for community action and involvement beyond voting for a just society.
Stresses the importance of individual action and building community networks, especially for leftists seeking systemic change.

Actions:

for leftist activists,
Build community networks and foster involvement beyond voting (implied)
Focus on down-ballot races for real change (implied)
Be active in striving for a functioning, free, and just society (implied)
</details>
<details>
<summary>
2020-08-12: Let's talk about the American man and the toughest guy at the table.... (<a href="https://youtube.com/watch?v=R7QvtVlTj0I">watch</a> || <a href="/videos/2020/08/12/Lets_talk_about_the_American_man_and_the_toughest_guy_at_the_table">transcript &amp; editable summary</a>)

Beau challenges the myth of the ideal American man, advocating for masculinity defined by upliftment and creation rather than destruction.

</summary>

"Masculinity is not about hurting people. It's not about destruction. It's about making things better and uplifting people."
"The hardest uniform that any of those guys had apparently ever seen wasn't a plate carrier. It was a burp cloth."

### AI summary (High error rate! Edit errors on video page)

Exploring the myth of the ideal American man and the tough guy image that one side of the political spectrum has been discussing.
Refuting false assertions made by prominent figures like the President of the United States and Ben Shapiro regarding men being insulted by certain events.
Sharing a personal experience about a group of tough guys he knew who defied the stereotypical tough guy image.
Describing how these men, despite their tough exterior, focused on diverse topics like sports, music, and family rather than projecting toughness.
Revealing the true toughest guy at the table, not because of physical strength but because he was a single dad to six kids.
Emphasizing that true masculinity is not about destruction but creation, about lifting people up and making things better.
Challenging the country's idea of masculinity and pointing out that real tough guys don't advertise their toughness but view fatherhood as the toughest assignment.
Questioning the societal perception of masculinity and advocating for a shift towards understanding it as a force for good and upliftment rather than destruction.
Suggesting that masculinity is about building something better and uplifting people rather than hurting or destroying.

Actions:

for men, fathers, society,
Validate and support diverse expressions of masculinity (implied)
Recognize and appreciate the role of fatherhood as a tough assignment (implied)
Challenge stereotypes and misconceptions about masculinity (implied)
</details>
<details>
<summary>
2020-08-11: Let's talk about Ben Shapiro and the present tense.... (<a href="https://youtube.com/watch?v=8EqHoPqfy1g">watch</a> || <a href="/videos/2020/08/11/Lets_talk_about_Ben_Shapiro_and_the_present_tense">transcript &amp; editable summary</a>)

Beau criticizes Ben Shapiro's views on feminism and calls for true equality based on individual rights and freedom of expression.

</summary>

"Is this what feminists fought for?"
"But anytime somebody from a group that isn't me and him, the way we look, you know, anytime somebody from a different group tries to use that system and use the same things to get ahead, well he's right there to slap them down."
"Ben doesn't like feminism. Got it."
"They have rights, and that's a fact."
"And facts do not care about your feelings."

### AI summary (High error rate! Edit errors on video page)

Beau addresses Ben Shapiro's controversial commentary on a music video, expressing embarrassment on Shapiro's behalf.
Ben Shapiro reviewed a suggestive music video by reading the lyrics aloud with feigned moral outrage.
Beau criticizes Shapiro for questioning if the video represents what feminists fought for, pointing out the hypocrisy in his argument.
He argues that feminism is about allowing individuals to express themselves freely and be independent, full, and rounded human beings.
Beau condemns societal pressures that dictate how women should behave and be perceived, contrasting it with men's freedom from such expectations.
He questions Shapiro's selective defense of rights and objectification, suggesting a double standard in his views.
Beau challenges Shapiro to recognize and respect women's rights to be who and what they want, irrespective of societal expectations.
He concludes by noting that facts do not care about feelings and hints at the potential impact of embracing true equality.

Actions:

for social commentators,
Challenge societal pressures and double standards (implied)
Support and uplift voices advocating for true equality (implied)
Encourage respect for individual rights and freedom of expression (implied)
</details>
<details>
<summary>
2020-08-10: Let's talk about transitionary periods.... (<a href="https://youtube.com/watch?v=tGRLjWsi2Do">watch</a> || <a href="/videos/2020/08/10/Lets_talk_about_transitionary_periods">transcript &amp; editable summary</a>)

Transitionary periods in countries and personal lives call for celebration and using new titles to mark significant changes, reflecting true friendship and support.

</summary>

"If somebody goes through a transitionary period and you're their friend, you want to celebrate it with them."
"If somebody goes through something, goes through some form of transition and you refuse to use the new way of addressing them, you're probably not their friend."

### AI summary (High error rate! Edit errors on video page)

Transitionary periods occur in countries and personal lives, like in the US in recent years.
Celebrates friends' accomplishments, like promotions, finishing a doctorate, having a child, getting married, and more.
Acknowledges the significance of using new titles or names to mark these transitions.
Emphasizes the importance of supporting friends during their transitionary periods.
Views refusal to acknowledge and use new titles as a sign of not being a true friend or a good person.
Encourages celebrating and embracing changes in friends' lives.

Actions:

for friends and allies,
Celebrate friends' achievements with them (exemplified)
Use new titles or names to acknowledge and celebrate their transitions (exemplified)
</details>
<details>
<summary>
2020-08-10: Let's talk about the pic, the pendulum, and Paulding.... (<a href="https://youtube.com/watch?v=cCh6_qW7ZXI">watch</a> || <a href="/videos/2020/08/10/Lets_talk_about_the_pic_the_pendulum_and_Paulding">transcript &amp; editable summary</a>)

In Paulding County, a 15-year-old's advocacy triggers school closures and prompts community reflection on the importance of precautions and leadership to combat COVID-19.

</summary>

"She saved lives."
"Realistically, she chose one that she really shouldn't have won, but she did."
"May not win exactly the way she wanted to. But victory rarely looks the way you think it's going to."

### AI summary (High error rate! Edit errors on video page)

Returning to Paulding County due to a new development involving school closures and distance learning.
Uncertainty about the duration of school closures based on the current number of cases.
A 15-year-old advocate pushed for precautions like a mask mandate, acknowledging the limitations of her influence.
Governments often oscillate between inaction and overreaction in their responses.
Despite initial resistance, the community desires in-person schooling for their children.
The community's wish clashes with the rising COVID-19 cases due to insufficient precautions.
Media attention was drawn to the county due to documentation by the young advocate.
The community must implement precautions to avoid repeated school closures or tragic outcomes.
Beau praises the young advocate's courage and impact in saving lives through her actions.
Lack of effective leadership and precautionary measures contributed to the crisis in the community.

Actions:

for community members, activists,
Implement strict precautionary measures in schools and communities (suggested)
Advocate for mask mandates and safety protocols (implied)
Raise awareness about the importance of preventive actions (exemplified)
</details>
<details>
<summary>
2020-08-09: Let's talk about an update on Paulding County schools and how you can help.... (<a href="https://youtube.com/watch?v=EM3SdPI39ew">watch</a> || <a href="/videos/2020/08/09/Lets_talk_about_an_update_on_Paulding_County_schools_and_how_you_can_help">transcript &amp; editable summary</a>)

Paulding County Schools face COVID-19 challenges, urging community action and plasma donation for treatment in the absence of effective leadership.

</summary>

"We're gonna have to listen to the medical experts. We're gonna have to act, because they're not going to."
"It's not really about putting kids at risk just to reopen the economy."
"We need to do it ourselves."

### AI summary (High error rate! Edit errors on video page)

Paulding County Schools in Georgia reopened in person, with students documenting the reopening and posting online about non-compliance with safety measures.
The school initially suspended the students but rescinded the suspensions after public involvement.
Six students and three staff members have tested positive for COVID-19 after being in the school, suggesting that the numbers will continue to rise.
During a school board meeting, suggestions were made to move students around every 14 minutes as a safety measure, which may not effectively prevent transmission.
Beau recommends recovered individuals to donate plasma, as it contains antibodies that can potentially help in treatment.
Leadership at federal, state, and county levels is lacking, and Beau urges people to listen to medical experts and take action themselves.

Actions:

for community members, covid-19 survivors,
Donate plasma if you have recovered from COVID-19 (suggested)
Listen to medical experts and take necessary actions (implied)
</details>
<details>
<summary>
2020-08-09: Let's really talk about the surprising origins of consent-based policing.... (<a href="https://youtube.com/watch?v=io_0t3hpz00">watch</a> || <a href="/videos/2020/08/09/Lets_really_talk_about_the_surprising_origins_of_consent-based_policing">transcript &amp; editable summary</a>)

The origins of consent-based policing in the U.S. date back to the Declaration of Independence, with core principles focusing on community engagement, prevention over punishment, and public cooperation.

</summary>

"Consent-based policing is as American as apple pie."
"The government derives its power from the consent of the governed. It is supposed to display that just power with consent."
"The public are the police, the police are the public."
"Efficiency is the absence of crime, not a lot of arrests."
"Moving in the direction of the Declaration of Independence is probably more American than moving in the direction of totalitarian governments."

### AI summary (High error rate! Edit errors on video page)

The origins of consent-based policing in the United States date back to the Declaration of Independence, where the government's power was meant to derive from the consent of the governed.
The core principles of consent-based policing include the idea that the police are part of the public and effective policing is measured by the lack of crime, not the number of arrests.
Both conservative and liberal ideologies can find alignment in consent-based policing, which aims to break up the government's monopoly on violence and have law enforcement become a part of the community.
Implementing consent-based policing requires a shift in laws to better represent the will of the people, allowing for a focus on prevention through community engagement rather than punishment.
Beau outlines nine standing orders for consent-based policing, including prioritizing crime prevention, gaining public approval and cooperation, offering impartial service, and using force only as a last resort after persuasion and warnings fail.
Beau references the historical basis of these principles, dating back to Robert Peel's standing orders for British cops in 1829, which emphasized community policing and minimal force usage.
He challenges the notion that the presence of guns in the U.S. should prevent the adoption of consent-based policing, pointing to successful models in countries like the UK, Canada, and Australia with similar histories of armed populations.

Actions:

for law enforcement agencies,
Advocate for a shift towards consent-based policing by engaging with local law enforcement agencies and policymakers (implied).
Support initiatives that prioritize community engagement, prevention strategies, and public cooperation in policing practices (implied).
</details>
<details>
<summary>
2020-08-08: Let's talk about the intent of other nations and the US election.... (<a href="https://youtube.com/watch?v=qXStrjS1M7M">watch</a> || <a href="/videos/2020/08/08/Lets_talk_about_the_intent_of_other_nations_and_the_US_election">transcript &amp; editable summary</a>)

Other countries' preferences in the US election reveal insights into their intent, with China surprising by supporting Biden for trade stability over military confrontation.

</summary>

"Every country on the planet has a preference for who leads the US and could attempt to influence the outcome."
"Germany, France, the UK, and traditional allies have preferences; German intelligence might release compromising information on Trump."
"Nations supporting Biden prioritize stable foreign policy, trade agreements, and a return to normalcy over military aggression."

### AI summary (High error rate! Edit errors on video page)

Other countries have preferences and intents regarding the US election, influenced by US foreign and domestic policies.
Every country on the planet has a preference for who leads the US and could attempt to influence the outcome.
Coordination and cooperation between a candidate and another nation become a concern, indicating potential allegiance.
Germany, France, the UK, and traditional allies have preferences; German intelligence might release compromising information on Trump.
Russia continues to support Trump due to his favorability for them, while China surprises by backing Biden primarily for trade stability.
China's backing of Biden indicates their focus on stable trade agreements rather than military confrontation.
The information and propaganda from the Trump administration about China being adversarial are questioned based on China's support for Biden.
Nations supporting Biden prioritize stable foreign policy, trade agreements, and a return to normalcy over military aggression.
Both US presidential campaigns need monitoring to ensure they are not coordinating with foreign nations for security reasons.

Actions:

for policymakers, voters, citizens,
Monitor both US presidential campaigns for potential coordination or cooperation with foreign nations (suggested).
</details>
<details>
<summary>
2020-08-08: Let's talk about opening schools and the economy.... (<a href="https://youtube.com/watch?v=vzFbZDN63Kk">watch</a> || <a href="/videos/2020/08/08/Lets_talk_about_opening_schools_and_the_economy">transcript &amp; editable summary</a>)

Members of Congress push to send kids back to school for the economy, but Beau argues the focus should be on fixing the economy and not exploiting poor children.

</summary>

"Nobody cares."
"Maybe the real work that needs to be done to help the economy is you."
"The poor are never the source of your problem."

### AI summary (High error rate! Edit errors on video page)

Congress members are pushing the idea that sending kids back to school is vital for the economy, but it isn't resonating with Americans.
Lower-income Americans have already suffered significantly in the economy, with many losing their jobs.
The statement about harming the economy by keeping kids at home implies exploiting them for economic gain.
Beau questions why the focus isn't on Congress fixing the economy rather than rushing poor kids back to school.
He suggests that Congress should focus on raising wages to a living point and providing better support for families.
Beau points out that the real work to help the economy should start with Congress, not poor kids.
Sending kids back to school, even with guidelines, poses more risk compared to staying at home.
He calls for accountability on Capitol Hill rather than blaming lower-income individuals.
The source of economic problems lies with those in power, not the powerless.
Beau warns that if exploitation continues, the middle class will eventually become the working poor.

Actions:

for congress members,
Advocate for raising wages to a living point and providing better support for families (implied).
</details>
<details>
<summary>
2020-08-07: Let's talk about restructuring American life.... (<a href="https://youtube.com/watch?v=E3tWByr3sd4">watch</a> || <a href="/videos/2020/08/07/Lets_talk_about_restructuring_American_life">transcript &amp; editable summary</a>)

Beau questions the need to restructure the American way of life and points out how inequality and systemic issues are at the core of current challenges, calling for a move towards a fairer society.

</summary>

"It's time to restructure the American way of life."
"You can help or you can get out of the way, but it's time for it to happen."
"Times have changed, you have to as well."
"There's no upward mobility for people."
"It's time to move forward."

### AI summary (High error rate! Edit errors on video page)

Beau questions if it's time to restructure the American way of life due to a lack of response at the federal level to serious issues.
He suggests that the structure of American life may be the root cause of public health issues, citing inequality, lack of healthcare, low wages, and inadequate education.
The cycle of despair and crime, exacerbated by the war on drugs and militarized police, has led to a climate of fear and division in the country.
Politicians exploit this fear to maintain power, leading to the spread of disinformation and falsehoods that endanger lives.
Beau criticizes politicians like Kevin McCarthy for contributing to the current situation by enabling the president's actions and suppressing the vote.
He argues that appealing to tradition is not a valid reason to resist change and that it's time for America to evolve towards a fair and just society.
Beau calls for a restructuring of the American way of life to fulfill the promises of the founding documents and address the deep-rooted inequality in society.

Actions:

for american citizens,
Advocate for policies that address inequality and provide better access to healthcare and education (implied)
Support politicians who prioritize fairness and justice in society (implied)
Get involved in grassroots movements that aim to bring about positive change in the community (implied)
</details>
<details>
<summary>
2020-08-07: Let's talk about good trouble in Paulding County, Georgia.... (<a href="https://youtube.com/watch?v=QM3DT6Fdhkw">watch</a> || <a href="/videos/2020/08/07/Lets_talk_about_good_trouble_in_Paulding_County_Georgia">transcript &amp; editable summary</a>)

Students expose lack of safety measures in school reopening through advocacy journalism, facing punishment instead of praise.

</summary>

"They were warned by their students. Rather than heed the warning, they punished the students."
"What those students did is impressive. It should be encouraged."
"You better hope that one of those sick kids isn't her."

### AI summary (High error rate! Edit errors on video page)

Uplifting story from Paulding County, Georgia about students documenting school reopening.
Students tally mask-wearing students, revealing lack of safety measures by the school.
Act of advocacy journalism by students to inform public.
School suspends students for exposing truth, likened to villains from Scooby-Doo.
Student Hannah goes on CNN, suspensions lifted, but school fails to take further action.
School refuses to mandate masks citing enforcement difficulties.
Students warned school, punished instead of heeded, likely due to political reasons.
Students' efforts should be encouraged, referenced as "good trouble."
Uncertainty if school will change and protect students in the future.
Implication that consequences of school's inaction may lead to sick students.

Actions:

for students, educators, activists,
Support student journalists in advocating for safety measures at schools (implied)
Encourage and empower students to speak up for the public good (implied)
Stay informed about school policies and advocate for necessary changes (implied)
</details>
<details>
<summary>
2020-08-06: Let's talk about the news out of New York's AG.... (<a href="https://youtube.com/watch?v=kutFAu9XNHI">watch</a> || <a href="/videos/2020/08/06/Lets_talk_about_the_news_out_of_New_York_s_AG">transcript &amp; editable summary</a>)

Beau clarifies the NRA situation in New York, challenges misconceptions about gun rights, and questions the NRA's true allegiance, revealing the political implications and potential benefits amidst uncertainties.

</summary>

"Your guns will be taken away immediately and without notice. Yeah that sounds good and everything. You know, except none of that's true."
"Trump is not a supporter of the second amendment. He never has been."
"If you successfully defund, ban or whatever, anything, it just becomes a second market."

### AI summary (High error rate! Edit errors on video page)

Beau discloses his long-standing support for the second amendment before discussing the recent news coming out of New York regarding the NRA.
The Attorney General in New York is pursuing actions that may lead to the dissolution of the NRA due to allegations of corporate misconduct.
Beau clarifies that the issue is not an attack on the second amendment but rather focuses on corporate behavior.
He points out that the NRA primarily represents manufacturers, not owners, and functions as a lobbying group.
Republicans are likely to frame the situation as an attack on the second amendment, as evidenced by a tweet from the President of the United States.
Beau contrasts the President's accusations against Joe Biden with past statements where the President advocated for taking guns away without due process.
He criticizes the NRA for not standing up against overreach, particularly because their main clients are manufacturers, specifically law enforcement.
Beau expresses skepticism towards the NRA and suggests that supporters may be surprised by how their money is spent.
He acknowledges the political implications of the situation and the risk of attorney generals becoming politically motivated.
Beau believes that despite the uncertainties, there may be benefits from the developments surrounding the NRA in 2020.

Actions:

for advocates for gun rights,
Investigate how lobbying groups, like the NRA, utilize funds (exemplified)
Stay informed about political actions that may impact gun rights (exemplified)
</details>
<details>
<summary>
2020-08-06: Let's talk about Trump trying to undermine the election.... (<a href="https://youtube.com/watch?v=6zReXSiuavI">watch</a> || <a href="/videos/2020/08/06/Lets_talk_about_Trump_trying_to_undermine_the_election">transcript &amp; editable summary</a>)

President's changing views on mail-in voting and active attempt to rig elections threaten the foundation of democracy in the United States, with a warning of the country's demise if successful.

</summary>

"The president is using the weight of the Oval Office to rig the election."
"If he succeeds in this, if the president can rig the election, that's it. The United States is done."
"Those who claim to be patriots and lovers of the country will remain silent and sell out their country."
"This is an active attempt by the president of the United States to undermine the election, to rig the election."
"What's going to happen the second there is a state that has a population of probable mail-in voters that seem like they're going to vote for him?"

### AI summary (High error rate! Edit errors on video page)

President's changing views on mail-in voting discussed.
President was for mail-in voting in Florida on August 4th, then against it in Nevada on August 5th.
Beau suggests president should create a list of states he believes should or shouldn't be able to vote.
President filing suit in Nevada to suppress the vote.
Accusations of the president attempting to rig the election and deny the right to vote to his opposition.
Criticism towards those who remain silent despite claiming to be patriots and lovers of the country.
Warning that if the president succeeds in rigging the election, it marks the end of the United States as an experiment.
Prediction that the president's view on mail-in voting will change based on which population it favors.
Condemnation of the president's active attempt to undermine and rig the election.

Actions:

for voters, patriots, activists,
Contact local election officials to ensure fair and equal access to voting (implied)
Join or support organizations working to protect voting rights and ensure fair elections (implied)
</details>
<details>
<summary>
2020-08-05: Let's talk about you paying for Texas and Florida's response.... (<a href="https://youtube.com/watch?v=vMVr-WAXX_k">watch</a> || <a href="/videos/2020/08/05/Lets_talk_about_you_paying_for_Texas_and_Florida_s_response">transcript &amp; editable summary</a>)

Florida and Texas exempt from costs, hinting at political motives, while citizens bear burden; satire on government absurdity.

</summary>

"Florida and Texas are exempt from paying millions to cover 25% of the National Guard deployments' cost."
"The president's only concern is himself. But don't worry. We're all in this together. You guys a little bit more than us, though, I guess."
"The American Republic will endure until politicians realize they can bribe people with their own money."

### AI summary (High error rate! Edit errors on video page)

Florida and Texas are exempt from paying millions to cover 25% of the National Guard deployments' cost, unlike other states.
It doesn't make sense to exempt these prosperous states when metrics like being the hardest hit or ability to come up with money are considered.
Possible reasons for this exemption include Trump not wanting to cause them expenses due to shaky support and governors prioritizing his re-election over citizens' welfare.
There are no clear explanations for why Florida and Texas were exempted while Mississippi, for example, could be considered for exemption.
Beau sarcastically thanks Florida and Texas for starting to chip in and pay for responses, implying the absurdity of the situation.
The situation serves as a reminder that the president's main concern seems to be himself, rather than the well-being of citizens.
The sarcastic tone continues as Beau mentions the generosity of these states in paying for others' expenses.
Beau concludes with a reminder about the American Republic enduring until politicians realize they can bribe people with their own money, adding a touch of satire.

Actions:

for politically active citizens,
Contact local representatives to raise concerns about the unfair exemption of Florida and Texas from costs (suggested).
Organize community dialogues to raise awareness about the potential political motives behind this exemption (implied).
</details>
<details>
<summary>
2020-08-05: Let's talk about Trump now being in favor of mail in voting.... (<a href="https://youtube.com/watch?v=e1hISo4kNgM">watch</a> || <a href="/videos/2020/08/05/Lets_talk_about_Trump_now_being_in_favor_of_mail_in_voting">transcript &amp; editable summary</a>)

President's stance on mail-in voting is driven by ego and power, aiming to create doubt and undermine the election for personal gain.

</summary>

"It's about him. It's about his ego and his desire to retain power."
"Don't try to overthink it. It's all about Trump."
"He doesn't care about you. He never did."
"He doesn't care about America. He never did."
"He doesn't care about your voice. He cares about power."

### AI summary (High error rate! Edit errors on video page)

President's stance on mail-in voting has shifted multiple times.
President's approval ratings are low due to his performance.
Mail-in voting is set to begin in many places soon.
President's tweet in support of mail-in voting in Florida includes a MAGA hashtag.
President believes he will win in Florida.
President is likely to claim rigged elections in states that vote against him.
President's goal is to create doubt and undermine the election.
Trump's support or opposition to mail-in voting is about his ego and desire for power.
Trump's decisions are solely driven by what benefits him.
Trump doesn't care about the American people or the Constitution.

Actions:

for voters,
Request a ballot and vote by mail in Florida (suggested)
Be prepared to counter attempts to undermine the election (implied)
</details>
<details>
<summary>
2020-08-05: Let's talk about DeSantis, Florida, leadership, and initiative.... (<a href="https://youtube.com/watch?v=SzWlZYz439o">watch</a> || <a href="/videos/2020/08/05/Lets_talk_about_DeSantis_Florida_leadership_and_initiative">transcript &amp; editable summary</a>)

Beau addresses the lack of leadership and accountability in American politics, calling for community solidarity in the face of government negligence.

</summary>

"If you have the means, you have the responsibility to act."
"Sometimes there's justice and sometimes there's just us."
"They're admitting they're just puppets of lobbyists and government functionaries."
"They don't even have to pretend they're doing their job."
"Well nobody told me I needed to do that."

### AI summary (High error rate! Edit errors on video page)

Addressing the lack of leadership and accountability in American politics.
Criticizing Governor DeSantis for deflecting blame and failing to take responsibility for Florida's unemployment issues.
Explaining the criteria for receiving unemployment benefits in Florida.
Expressing disbelief at Governor DeSantis' excuse of not fixing the problem because no one asked him to.
Pointing out the entitlement and lack of initiative among the political ruling class.
Emphasizing the importance of individuals looking out for each other in the absence of government support.
Advocating for setting up community networks and practicing mutual aid.
Reminding that in times of injustice, it comes down to "just us" taking care of each other.
Condemning the entitled attitude of politicians who neglect their duties.
Using a cashier analogy to illustrate the absurdity of politicians shirking their responsibilities.

Actions:

for community members,
Set up a community network for mutual aid (suggested)
Commit to looking out for each other within your community (suggested)
</details>
<details>
<summary>
2020-08-04: Let's talk about the Floyd footage and what it changes.... (<a href="https://youtube.com/watch?v=qm6CMkIkYIY">watch</a> || <a href="/videos/2020/08/04/Lets_talk_about_the_Floyd_footage_and_what_it_changes">transcript &amp; editable summary</a>)

Beau addresses managing situations better, demonstrating tactics to induce compliance, and condemning excessive force in a thought-provoking video.

</summary>

"Do it now."
"If you do it the same way, you need to turn in your badge."
"That footage changes nothing, as far as this case."

### AI summary (High error rate! Edit errors on video page)

Addressing the need to talk about managing situations better in light of recent events.
Expressing concern over individuals supporting actions that deviated from best practices.
Demonstrating a scenario with an assistant, Zed, who is alleged to have done something wrong.
Explaining techniques like inducing compliance through urgent commands and physical methods to get someone in a car.
Emphasizing the importance of not using unnecessary force once a suspect is subdued and cuffed.
Challenging the notion that non-compliance justifies excessive force.
Asserting that using force beyond what is necessary is unacceptable, and individuals should be held accountable if they do so.
Concluding with a reminder that the footage of an incident does not change the facts of the case.

Actions:

for community members,
Practice de-escalation techniques when dealing with challenging situations (exemplified)
Advocate for accountability and adherence to best practices in law enforcement (exemplified)
Educate others on the importance of using only necessary force in law enforcement interactions (exemplified)
</details>
<details>
<summary>
2020-08-04: Let's talk about facepalms and a question about borders.... (<a href="https://youtube.com/watch?v=P9e7OpWkbUc">watch</a> || <a href="/videos/2020/08/04/Lets_talk_about_facepalms_and_a_question_about_borders">transcript &amp; editable summary</a>)

Beau explains the concept of open borders by challenging limitations in thought and urging to give freedom a chance.

</summary>

"We've lived in it. It exists."
"We just don't think of it that way because we've limited our own thought."
"Let's give freedom a chance."
"We just don't think of it that way because we define ourselves more as Americans than Floridians or Georgians."
"It's easy to imagine if you try."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of open borders and challenges the idea that it is impossible or hard to understand.
Points out how we limit our own thinking by not recognizing things that are already around us.
Uses the example of traveling within the United States to illustrate the concept of open borders.
Emphasizes the need to address inequality globally before implementing open borders on a larger scale.
Suggests that open borders between countries like the US and Canada could be implemented successfully.
Addresses concerns about the economic impact and entry-level job availability with open borders.
Encourages imagining a world without defined borders and giving freedom a chance.
Urges to start thinking beyond national identities and flags to envision a better world.

Actions:

for global citizens,
Advocate for addressing global inequality issues as a first step towards implementing open borders (suggested)
Encourage regional collaborations and agreements, like between the US and Canada, to facilitate open borders (exemplified)
Challenge limitations in thought and national identities by envisioning a world without defined borders (implied)
</details>
<details>
<summary>
2020-08-04: Let's really talk about the laws of politics and getting around them.... (<a href="https://youtube.com/watch?v=Vmd3bTyRmWc">watch</a> || <a href="/videos/2020/08/04/Lets_really_talk_about_the_laws_of_politics_and_getting_around_them">transcript &amp; editable summary</a>)

Beau shares insights on applying Murphy's laws to politics, stressing adaptability, genuine belief in causes, and the power of community in effective activism.

</summary>

"Friendly Fire isn't. Your allies can be more devastating to you than whatever you're working against."
"No plan survives first contact. Be adaptable."
"You're not Superman. Keep that in mind."
"Cavalry is not coming. It's just you."
"Success occurs when nobody is watching."

### AI summary (High error rate! Edit errors on video page)

Explains how laws, like Murphy's laws of combat, can be applied to politics for those wanting to be politically active beyond social media.
Emphasizes the importance of understanding that allies, not enemies, can sometimes be the biggest challenge in grassroots organizing due to differing views or mistakes.
Advises grassroots organizers to acknowledge their vulnerability and not to see themselves as invincible.
Stresses the need for adaptability in plans because no plan survives first contact and there is no such thing as a perfect plan.
Encourages activists to believe in their cause genuinely rather than seeking recognition or medals.
Warns about established roads always being mined in politics, indicating the need for innovative strategies rather than following traditional paths.
Caution against staying when someone suggests "watch this" at street events, as it signals potential escalation.
Reminds activists not to dismiss diversions as unimportant, as they might be part of a larger strategy.
Mentions that experience is gained just after it is needed, urging persistence despite initial failures.
Points out the significance of clear communication in slogans and messages to avoid misunderstandings.
Acknowledges that success often happens unnoticed, while failures are magnified in public, especially in politics.
Differentiates between professional predictability and amateur unpredictability, advising flexibility in dealing with both.
Concludes by stressing the importance of community networks in activism, stating that change cannot be achieved alone.

Actions:

for community organizers, activists.,
Build a strong community network committed to making your community better (suggested).
Form a team with friends dedicated to supporting each other and working towards common goals (exemplified).
Acknowledge the importance of allies and maintaining unity within the group (implied).
</details>
<details>
<summary>
2020-08-03: Let's talk about your views, Trump, Biden, and getting to Utopia.... (<a href="https://youtube.com/watch?v=ciegoYE5tVY">watch</a> || <a href="/videos/2020/08/03/Lets_talk_about_your_views_Trump_Biden_and_getting_to_Utopia">transcript &amp; editable summary</a>)

Beau stresses the need to understand core beliefs, visualize utopia, celebrate small wins, and maintain pressure for lasting social change even under new leadership.

</summary>

"You have to figure out what you believe, what your ideal society looks like."
"If you want freedom, if you want systemic changes, you can't stop just because the new guy has a D instead of an R."
"You have to figure out where you fit into that battle."
"We progress. We move forward."
"It's possible to get there. We just have to work for it."

### AI summary (High error rate! Edit errors on video page)

Emphasizes the importance of having a clear understanding of your core beliefs to bring about social change.
Stresses the significance of knowing your ideal world or utopia to guide the steps towards achieving it.
Acknowledges that deep systemic change will not happen overnight and may take generations.
Contrasts the current foreign policy landscape with his ideal of American foreign policy focusing on peace and development.
Advocates for celebrating small wins and acknowledging incremental progress towards long-term goals.
Reveals his pro-immigration stance but strategically advocates for attainable ideas to persuade those who oppose his views.
Warns against advocating ideas that are too extreme or far outside the mainstream as they may not resonate with the current societal mindset.
Encourages individuals to determine their beliefs, envision their ideal society, and work towards it globally.
Urges the importance of maintaining pressure and accountability on future administrations for real change.
Reminds that progress requires consistent effort, focus, and unity even under a new administration.

Actions:

for activists, advocates,
Determine your core beliefs and envision your ideal society (implied).
Advocate for attainable ideas to persuade those in opposition (implied).
Maintain pressure and hold future administrations accountable for real change (implied).
</details>
<details>
<summary>
2020-08-03: Let's talk about Trump, the economy, and big testing.... (<a href="https://youtube.com/watch?v=v-657b2MUmI">watch</a> || <a href="/videos/2020/08/03/Lets_talk_about_Trump_the_economy_and_big_testing">transcript &amp; editable summary</a>)

Beau criticizes Trump's misleading COVID-19 testing claims, warns against reopening schools, and stresses the need for a unified plan to combat the pandemic.

</summary>

"Cases up because of big testing, cases are up because more people have it."
"Opening the schools is a bad idea. It's going to help it spread. It's going to hurt the economy."
"Because we're not getting back on track, because we're not taking the steps we need, because we don't have a unified plan, all of this drags on."
"If the president is concerned about the economy, he's going to have to lead the nation into doing the right thing."
"In order to set the example, you have to take responsibility for something."

### AI summary (High error rate! Edit errors on video page)

Criticizes President Trump for linking the increase in COVID-19 cases to increased testing.
Points out the inaccuracy in Trump's statement about testing causing the rise in cases.
Emphasizes that hospitalizations have increased significantly, unrelated to testing.
Argues against reopening schools, stating it will contribute to the spread of the virus and harm the economy.
Addresses the importance of following CDC guidelines for reopening.
Expresses concern about the lack of a unified plan and the potential for more loss of life and economic damage.
Urges Trump to lead the nation in taking the necessary steps to combat the pandemic.

Actions:

for concerned citizens, policymakers,
Follow CDC guidelines for COVID-19 prevention and reopening schools (suggested)
Advocate for a unified national plan to combat the pandemic (suggested)
Encourage responsible leadership to prioritize public health and safety (suggested)
</details>
<details>
<summary>
2020-08-02: Let's talk about a PSA, Trump, and gone in 60 seconds.... (<a href="https://youtube.com/watch?v=wolPGhfNsR4">watch</a> || <a href="/videos/2020/08/02/Lets_talk_about_a_PSA_Trump_and_gone_in_60_seconds">transcript &amp; editable summary</a>)

Beau warns against dangerous hand sanitizers, criticizes President's actions worsening US COVID-19 situation, and calls for unified national leadership.

</summary>

"He's tweeting out falsehoods. And he's doing so at the expense of American lives."
"We still have no national leadership."
"The only leadership we're getting at the national level, the president tries to undermine at every turn."

### AI summary (High error rate! Edit errors on video page)

FDA warns against using certain hand sanitizers due to ineffectiveness and contamination.
President criticized Dr. Fauci's COVID-19 response, claiming US high cases due to extensive testing.
European countries shut down deeper than the US, leading to better COVID-19 outcomes.
Lack of unified national response and leadership contributes to US COVID-19 situation.
President's actions worsen the already bad COVID-19 situation.
Administration lacks a clear plan to address the devastating COVID-19 impact.
Allegations that the administration prioritized political interests over effective response strategies.
The president's divisive actions disregard the well-being of all Americans, regardless of political affiliation.
Beau criticizes the president for tweeting falsehoods and undermining national leadership during the crisis.

Actions:

for public health advocates,
Avoid using hand sanitizers listed by the FDA as ineffective or contaminated (implied)
Advocate for a unified national response to COVID-19 (implied)
</details>
<details>
<summary>
2020-08-02: Let's talk about Biden's possible foreign policy.... (<a href="https://youtube.com/watch?v=IG5mkdk0L3k">watch</a> || <a href="/videos/2020/08/02/Lets_talk_about_Biden_s_possible_foreign_policy">transcript &amp; editable summary</a>)

Beau examines Biden's potential foreign policy, focusing on expertise, arms control, human rights, racial injustice, and refugee support while maintaining American dominance.

</summary>

"Unlike Trump, who only knows what the leadership of the country tells him, Biden's team has access to information from various sources."
"It's not about creating a better world; it's about maintaining American supremacy."
"This crew could actually undo most of the damage Trump has done."

### AI summary (High error rate! Edit errors on video page)

Beau criticizes Trump's foreign policy, calling it a complete failure and an unmitigated disaster.
Beau expresses doubts about Biden's ability to clean up Trump's mess due to lack of expertise.
Biden established a foreign policy team separate from the usual advisory team, indicating seriousness.
The foreign policy team includes 2,000 people divided into working groups, specifically chosen to address current issues.
Biden's team focuses on arms control internationally and defending human rights and marginalized communities.
There is a separate working group dedicated to addressing racial injustice in the United States and its impact on foreign policy.
Biden's team is likely to be pro-refugee and focused on upholding international and domestic laws.
The team aims to undo much of the damage caused by Trump's policies, although maintaining American dominance remains a core goal.
Beau acknowledges that the new foreign policy crew under Biden, if utilized effectively, could make significant improvements.

Actions:

for policy analysts, activists,
Contact organizations supporting arms control (implied)
Join groups advocating for human rights and marginalized communities (implied)
Support refugee aid organizations (implied)
Advocate for racial justice in domestic policies to improve international standing (implied)
</details>
<details>
<summary>
2020-08-01: Let's talk about admitting mistakes, arrogance, and schools.... (<a href="https://youtube.com/watch?v=-yj53BZ_EEI">watch</a> || <a href="/videos/2020/08/01/Lets_talk_about_admitting_mistakes_arrogance_and_schools">transcript &amp; editable summary</a>)

Admitting mistakes is vital to stopping compounded errors, preventing further harm, and moving towards a unified response for a better future in America.

</summary>

"Admitting them is the first step in stopping it because a mistake just compounds until it's admitted and corrected."
"We cannot make America great until we admit that we weren't."
"It's okay to make mistakes. Not correcting them, that's when it becomes a real issue."
"The American people will forgive mistakes. They won't forgive people being lost because of arrogance."
"Admit them so we can fix them."

### AI summary (High error rate! Edit errors on video page)

Everyone makes mistakes, but admitting them is the first step in stopping them.
People are often reluctant to admit mistakes if their image is based on being infallible.
Mistakes have been made in the country, impacting its response to crises compared to other nations.
Public health officials advised policy makers on errors like reopening too soon, but policy changes were not made.
Students tested positive for the virus, yet there was no change in policy.
The longer mistakes go unacknowledged, the longer the crisis will persist.
Policy makers need to admit their mistakes and implement corrections to prevent further harm.
Delayed admission of mistakes can lead to increased loss of lives and damage to the economy.
A unified response and plan of action are necessary to address the crisis effectively.
Politicians' refusal to admit mistakes and prioritize ego over correction hinders progress.
The American people are forgiving of mistakes but not of arrogance leading to losses.
Admitting mistakes is key to addressing the current crisis and systemic issues that have been ignored.
Failure to admit and correct mistakes will perpetuate ongoing issues affecting the economy and society.
Without acknowledging past mistakes, progress towards resolving issues will be impeded.
Admitting failures is necessary for moving forward and improving the situation for all.

Actions:

for policy makers, public health officials, american citizens,
Public health officials must continue advising policy makers on necessary corrections (implied).
Policy makers need to acknowledge mistakes and implement changes promptly (implied).
American citizens can advocate for transparent and accountable leadership (implied).
</details>
