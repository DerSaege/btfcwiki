---
title: Let's talk about keeping your community network going....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QF2U6oplx8Y) |
| Published | 2020/08/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Advocates for establishing community networks at the local level with friends, coworkers, and committed individuals aiming to better their community.
- Emphasizes the importance of keeping community networks engaging and fun to maintain interest and prevent people from losing interest over time.
- Suggests adapting activities to cater to different personality types within the group, ensuring inclusivity and sustainability.
- Recommends engaging in cross-training and skill-sharing activities to broaden skill sets and enrich the network's members.
- Proposes "Adventure Day" as a spontaneous group activity to keep members interested and adaptable, fostering group cohesion.
- Encourages building network resources like community gardens, medical kits, and disaster relief stashes to strengthen the group and provide support in times of need.
- Advocates for creating community resources such as library boxes or food boxes to benefit not only the network but also the wider community, enhancing public perception and support.
- Stresses the significance of community networks in facing challenges and uncertainties, advocating for their establishment to enhance civic engagement and resilience at the local level.

### Quotes

- "Establishing one of these networks is probably one of the more effective things you can do for civic engagement."
- "It's always been like that."
- "Bring your group together."
- "If you haven't started one of these, it might be time."
- "Y'all have a good day."

### Oneliner

Beau advocates for establishing engaging community networks with diverse activities and resources to enhance civic engagement and local resilience.

### Audience

Community activists and organizers.

### On-the-ground actions from transcript

- Establish a community network with friends, coworkers, and committed individuals (suggested).
- Organize engaging activities like cross-training, skill-sharing, and Adventure Days within the network (implied).
- Build network resources such as community gardens, medical kits, and disaster relief stashes (implied).
- Create community resources like library boxes or food boxes for public benefit (implied).

### Whats missing in summary

The full transcript provides detailed insights on how to establish and maintain engaging community networks, offering practical examples and suggestions to enhance local resilience and civic engagement.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about your community network and how to keep it moving.
If you are newer to the channel you may not have heard me talk about these before, but
I'm a pretty big proponent of establishing community networks.
Networks at your local level that basically are made up of your friends, your coworkers,
those who have made an open commitment to making their community better in some way.
This little circle of friends, this little network is adaptable.
You don't make it about a specific cause, you make it about a general idea so it can
adapt to different situations.
I've had a lot of questions about them over the last couple of weeks.
One of the big ones is how to keep people interested in it.
Because people start off and it starts off strong and there's a whole bunch of activism
going on and there's positive changes made in the community and everything is great.
And then people lose interest.
In the videos where I really break these down I talk about how it's important to make it
fun.
Because otherwise it becomes a second job and nobody wants that.
You have to have social interaction that goes along with it.
Doesn't mean that it can't also be productive, but it has to be fun.
It has to be something people want to do, otherwise they stop doing it.
The other thing you have to keep in mind is different personality types.
If you're the person who is setting this in motion, you're probably pretty extroverted.
A lot of people aren't.
So if every week or every month, however you have your set up, people are going out and
getting out in public and talking to people and doing all this stuff, it may become very
taxing for them.
You have to give them a break.
So I'm going to give you four things that my little circle does, some less frequently
than we used to, to kind of break it up.
The first is cross-training, the skill shares.
There are two ways to do this.
The first one is a planned one.
That's where everybody shows up and you know what you're going to be talking about that
day.
More than likely you picked a manual, like a medical manual, and everybody took a piece
of it and mastered that skill.
Then when you get together, everybody teaches the other people how to do that skill.
So by the time you're done, everybody knows the whole manual.
Those are the planned ones.
Unplanned ones are when everybody shows up and nobody knows what they're going to be
learning because everybody has decided to share a different skill.
This could be a blue collar skill.
It could be cooking.
It could be a history lesson.
It could be anything at all.
You don't know.
It keeps people on their toes.
It keeps it interesting.
This helps make your community network more well-rounded.
It also gets everybody on the same page when it comes to skill sets.
It enriches everybody that's involved in it in addition to making the network stronger.
These are all good things.
The next one is, we call it Adventure Day.
I don't know.
Usually it's spur of the moment in the sense of everybody knows that something's going
to happen that day so they have the day off or they're planned for it.
But you don't know what.
Everybody proposes a suggestion on scene and you go do it right then.
No going to change clothes.
No going to pick stuff up.
You do it right then.
It makes people more adaptable.
In the past, a canoe trip.
One was just a wild trip to New Orleans.
It wound up lasting two days.
Bungee jumping.
These are all different things that it keeps people interested.
It keeps people on their toes.
It's something fun that the network does as a group that isn't necessarily work.
Now at the same time, you're still making people more adaptable and you are becoming
tighter as a group because you're engaging in something.
Now that's more for young people, the ones that I listed.
As you get older, it does change.
It could be an escape of the room challenge, paintball, whatever.
The point is that the network is working as a team and it helps develop that unit integrity.
The third thing you can do is build a network resource which is everybody's getting together
and this is something that makes the network more stable.
For example, it could be everybody getting together to put a garden in at everybody's
house if you don't have space, a container garden, planting fruit trees, whatever.
But the point is it's establishing a resource that the network can tap into later if need
be.
You can come together and put together an advanced medical kit.
Depending on your economic level, maybe everybody's putting one together.
If not, everybody's chipping in to put together a really good one that can deal with everything
because most people, they have a first aid kit.
They don't have a trauma kit.
If you combine this with the cross-training, not just will you have this resource, people
will know how to use it.
That's another thing that I've seen is people have the equipment but don't know how to use
it because they never trained.
A side benefit to that is if you're building one for the whole network, people are chipping
in for it.
It may only be $20 but there's that transaction that occurs.
For whatever reason, chipping in $20 makes people more invested in it.
Even though the time they've put in is worth way more than $20, that cash for whatever
reason seems to keep people tied to it.
Another thing you can do is put together a disaster relief stash.
So it's something that everybody can tap into in the event something goes bad.
Now you're not going to be able to build one that's going to be big enough for everybody,
not without a warehouse, but there's a safety net.
You're establishing your own social safety net here.
As we know, the government response to disasters is less than ideal.
You have your own.
The fourth thing you can do is build a community resource.
That can be anything from a library box.
I actually saw this little town near me where somebody just put one in and it just made
my day.
So if you don't know what these are, they're little boxes that are set out maybe in your
front yard, somewhere where you have authorization, and it's stocked with books.
People take one, leave one, that kind of thing.
Couple of things happens with this.
One, it's something fun to do for the group.
Two, it's something that's public, that not just does your network see as something they
did for the community that will be ongoing, but it's something the public sees about your
community.
Winning hearts and minds and all of that.
So they start to see you as an asset, which means when you ask for their assistance, they're
more likely to give it because they see you trying to help the community.
It can be a library box, you can do a food box.
There's a whole bunch of different stuff like that that you can do.
You can establish something for the community.
If somebody has an in with the city council or county commission or whatever it is, planting
those fruit trees, but at a park if they'll let you.
Repairing a playground, stuff like that.
Little things like this that don't, they're activism, but they don't jump all the way
into the full scale stuff that these networks normally get involved in.
Everybody needs a break if you're going to suffer burnout.
I know given the current political climate right now in the US, what a lot of people
believe is on the horizon, this seems like a weird thing to talk about.
However, I would point out that if you were a part of one of these networks, if you had
a close circle that you knew you could rely on and that you trained with, that you worked
with, that you knew were committed to making things better, what's on the horizon?
It's a little less scary.
It's a little less scary.
I have a whole bunch of videos about how to establish these, how to build these.
These networks can in a sense help replace a lot of government functions.
People coming together, working together, they can help your community get to a situation
where you're not as reliant on the outside.
That may become important.
It's important today in any political climate because that's how you make things better.
However, when Michael happened here, this network was really important.
Under normal circumstances, they make things better.
When things get bad, they can kind of help maintain some semblance of what's normal.
If you don't have one of these, if you haven't started one of these, it might be time.
It might be a really good idea.
Bring your group together.
I will say that I heard the group in Chattanooga called themselves the Fifth Comm.
I don't mind.
I don't have a branding issue with that or anything.
I would just point out that if you Google that term, it doesn't come back to the cool
story about a news outlet.
It comes back to a term you may not necessarily want to be associated with.
At the end of the day, our current political climate, and it doesn't matter if we're talking
about what may or may not be on the horizon and everybody's fear right now, it doesn't
matter who wins the election.
What gets the goods, what gets stuff done, is action at the community level.
It's always been like that.
Establishing one of these networks is probably one of the more effective things you can do
for civic engagement.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}