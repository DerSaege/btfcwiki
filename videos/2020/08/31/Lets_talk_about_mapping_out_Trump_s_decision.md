---
title: Let's talk about mapping out Trump's decision....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IMfU2NIPyqw) |
| Published | 2020/08/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing Trump's decision-making process regarding Dr. Scott Atlas's addition to the task force.
- Speculating on whether Trump's change in strategy is a genuine shift or politically motivated.
- Questioning the rationale behind adopting a controversial strategy of herd immunity.
- Predicting Trump's potential narrative shift towards herd immunity due to vaccine unavailability before the election.
- Criticizing the prioritization of reelection over public health and potential risks of herd immunity.
- Warning against being misled into thinking herd immunity is a viable solution.
- Pointing out the potential negative impact of herd immunity on Trump's reelection chances.
- Advocating for responsible health practices like handwashing, wearing masks, and social distancing.

### Quotes

- "He's willing to sacrifice his base."
- "As long as his base continues to believe everything that he says, he can get away with this."
- "Don't be fooled by this into thinking that herd immunity is suddenly a good idea."
- "Wash your hands. Don't touch your face. Don't go out. Stay at home."
- "Eventually we will get through this at some point."

### Oneliner

Beau questions Trump's motives behind adopting a controversial herd immunity strategy and warns against the associated risks, urging responsible health practices for everyone's safety.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Wash your hands, wear a mask, practice social distancing (implied)
- Stay at home if possible (implied)

### Whats missing in summary

Detailed analysis of Trump's shifting COVID-19 strategy and the potential consequences of prioritizing politics over public health.

### Tags

#Trump #COVID19 #HerdImmunity #Election2020 #PublicHealth #Responsibility


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to try to map out a decision that Trump has made.
See if we can get an atlas and figure out why he decided to turn and do a 180 when he
did.
See if it's really a change of heart, a change of policy, if it's in the best interest of
the American people, or if it's just what's politically expedient.
And that perhaps there's another motive.
If you don't know, the president has added Dr. Scott Atlas to the task force to deal
with the current public health thing.
Atlas is the doctor who backed Governor DeSantis' plan for the schools here in Florida.
So we can just safely assume that the president brought him to this position because of his
stellar record of success.
Atlas is a proponent of, let's just call it a less than proven and controversial strategy
of just hoping to obtain herd immunity.
This is when you pretty much just let everybody get it except for the old people.
Why would he do this?
That doesn't even make sense.
I mean, why would he do this turnaround now?
Why would he adopt this now?
Why would he bring this person on now at this moment?
And is he going to follow that advice?
Of course he is.
It's going to become the new talking point because the vaccine is not going to be ready
by the election.
That's what it boils down to.
That news just came out too.
So he's going to change the strategy between now and the election and say, oh, well, we're
just going to go for herd immunity.
That way he doesn't have to address the fact that in his platform and his campaign promises,
the number one bullet point for dealing with this is to have the vaccine by 2020, by the
end of this year.
And that doesn't look like it's going to happen now.
So in order to change the story and not admit that he has failed at one of his campaign
promises before he even got reelected, he's going to bring in Mr. Atlas, Dr. Atlas, sorry,
bring in Dr. Atlas and allow him to guide the policy and say, oh, he said go with herd
immunity.
So I'm listening to the experts just like the liberals told me to.
And then if it fails, probably will, he'll be able to say, oh, no, well, that's Atlas's
fault.
We're going to get rid of him.
By the way, I went ahead and got us a vaccine in the meantime.
Assuming he gets reelected.
If he doesn't, the plan works anyway because he doesn't have to address his failure, which
is Trump's entire MO.
Do not be fooled by this into thinking that herd immunity is suddenly a good idea.
It's not.
That's not the ideal way to go about this.
In fact, there's some evidence to suggest that it won't even work.
We don't know yet.
We do not know enough about it to adopt a strategy that controversial.
It's very risky.
But the president doesn't care.
The president doesn't care about people.
He cares about reelection.
So as long as his base continues to believe everything that he says, he can get away with
this.
That's really what this boils down to.
He's willing to sacrifice his base.
The thing is, he's done this enough that it may significantly impact his reelection chances.
I don't know if he's done the math on this yet, but herd immunity carries a pretty high
cost with it.
And the cost is typically borne by the people who vote for him.
He didn't win the popular vote last time.
He won by the electoral college.
And the number that we might lose under the herd immunity strategy is enough to swing
the states that he won last time.
It's a bold strategy.
We'll see how it plays out for him.
As far as everybody else, everybody who is sane, wash your hands.
Don't touch your face.
Don't go out.
Stay at home.
If you have to go out, wear a mask.
Do all the stuff you're supposed to.
There's a whole lot of evidence now that shows that those things are incredibly effective.
So just keep it up, and eventually we will get through this at some point.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}