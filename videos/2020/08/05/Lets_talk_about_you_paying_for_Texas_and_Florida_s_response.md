---
title: Let's talk about you paying for Texas and Florida's response....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vMVr-WAXX_k) |
| Published | 2020/08/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Florida and Texas are exempt from paying millions to cover 25% of the National Guard deployments' cost, unlike other states.
- It doesn't make sense to exempt these prosperous states when metrics like being the hardest hit or ability to come up with money are considered.
- Possible reasons for this exemption include Trump not wanting to cause them expenses due to shaky support and governors prioritizing his re-election over citizens' welfare.
- There are no clear explanations for why Florida and Texas were exempted while Mississippi, for example, could be considered for exemption.
- Beau sarcastically thanks Florida and Texas for starting to chip in and pay for responses, implying the absurdity of the situation.
- The situation serves as a reminder that the president's main concern seems to be himself, rather than the well-being of citizens.
- The sarcastic tone continues as Beau mentions the generosity of these states in paying for others' expenses.
- Beau concludes with a reminder about the American Republic enduring until politicians realize they can bribe people with their own money, adding a touch of satire.

### Quotes

- "Florida and Texas are exempt from paying millions to cover 25% of the National Guard deployments' cost."
- "The president's only concern is himself. But don't worry. We're all in this together. You guys a little bit more than us, though, I guess."
- "The American Republic will endure until politicians realize they can bribe people with their own money."

### Oneliner

Florida and Texas exempt from costs, hinting at political motives, while citizens bear burden; satire on government absurdity.

### Audience

Politically active citizens

### On-the-ground actions from transcript

- Contact local representatives to raise concerns about the unfair exemption of Florida and Texas from costs (suggested).
- Organize community dialogues to raise awareness about the potential political motives behind this exemption (implied).

### Whats missing in summary

The full transcript provides a deeper dive into the political dynamics and satirical commentary on government decisions, offering a more nuanced understanding of the situation.

### Tags

#Florida #Texas #PoliticalExemption #NationalGuard #Satire


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Florida and Texas.
I'm going to go ahead and start off by saying that I was going to do this as a satire video
but couldn't come up with a way to make this more absurd than how it appears already.
So I'm just going to tell you what happened and you can laugh this morning at the farce
that is our current government.
So President Trump has decided from up on high that it's time for the states to start
paying their own way.
Everybody needs to chip in, we're all in this together type of thing.
And that states are going to have to start paying millions to cover 25% of the cost of
the National Guard deployments that are due to the current situation.
All of the states except for Florida and Texas.
If you use any metric that makes sense, it doesn't make sense.
It makes no sense to exempt those two states.
If you're talking about the hardest hit, yeah we're getting hammered but we're not the hardest
hit.
If you're talking about how prosperous the states are and their ability to come up with
the money, both Florida and Texas are pretty prosperous states.
If you use any metric that makes sense, we'd be exempting Mississippi right now.
But we're not.
Florida and Texas.
With no explanation, we have no idea why.
There are two possible reasons that I can come up with and they're probably related.
The first is that both Florida and Texas were states that Trump won last time and that are
kind of shaky for him right now.
So he doesn't want to cause them any more expenses because he's afraid it's going to
cost him votes.
The other is that both states have a governor that followed him like a lost puppy into this
mess.
It appears that both of those governors put the president's re-election ambitions above
the welfare of their own citizens.
And this may be the president's way of rewarding them, you know.
I'm open to other suggestions for this.
I've got nothing.
I mean, I've asked.
Nobody can offer any other suggestion, any other reason why these two states in particular
were exempted.
On behalf of people living in Florida and Texas, I'd like to thank you guys for starting
to chip in and pay for your own responses and pay for ours.
That's very generous of you.
We appreciate that.
This certainly appears to be another reminder that the president's only concern is himself.
But don't worry.
We're all in this together.
You guys a little bit more than us, though, I guess.
Seriously, thank you.
It's very nice.
And I would just like to remind everybody that the American Republic will endure until
politicians realize they can bribe people with their own money.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}