---
title: Let's talk about DeSantis, Florida, leadership, and initiative....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SzWlZYz439o) |
| Published | 2020/08/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the lack of leadership and accountability in American politics.
- Criticizing Governor DeSantis for deflecting blame and failing to take responsibility for Florida's unemployment issues.
- Explaining the criteria for receiving unemployment benefits in Florida.
- Expressing disbelief at Governor DeSantis' excuse of not fixing the problem because no one asked him to.
- Pointing out the entitlement and lack of initiative among the political ruling class.
- Emphasizing the importance of individuals looking out for each other in the absence of government support.
- Advocating for setting up community networks and practicing mutual aid.
- Reminding that in times of injustice, it comes down to "just us" taking care of each other.
- Condemning the entitled attitude of politicians who neglect their duties.
- Using a cashier analogy to illustrate the absurdity of politicians shirking their responsibilities.

### Quotes

- "If you have the means, you have the responsibility to act."
- "Sometimes there's justice and sometimes there's just us."
- "They're admitting they're just puppets of lobbyists and government functionaries."
- "They don't even have to pretend they're doing their job."
- "Well nobody told me I needed to do that."

### Oneliner

Beau addresses the lack of leadership and accountability in American politics, calling for community solidarity in the face of government negligence.

### Audience

Community members

### On-the-ground actions from transcript

- Set up a community network for mutual aid (suggested)
- Commit to looking out for each other within your community (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the lack of leadership and accountability in American politics, urging individuals to take action and support each other in the absence of governmental assistance.

### Tags

#Leadership #Accountability #CommunitySolidarity #GovernmentNegligence #MutualAid


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about leadership.
We're going to talk about taking initiative.
We're going to talk about passing the buck and accepting responsibility.
We're going to talk about all those things that leaders are supposed to do by default
or supposed to avoid by default.
We're going to do this because we've hit that point in American politics where those at
the highest level refuse to accept responsibility for anything, refuse to take initiative to
do their jobs, the very things they were sent there to do.
There was an interview here in Florida with the governor here, DeSantis, and he was asked
about the unemployment problem we're having.
The program here is just a disaster.
When he was asked about it, he did what politicians do, he deflected.
He said it was somebody else's fault, didn't have to take responsibility for it and all
that stuff.
The only thing that was surprising was that he threw a fellow Republican, Rick Scott,
under the bus and basically said it looked as though the system was designed to deny
as many claims as possible and to throw up so many roadblocks that people just gave up,
gave up on getting the money that they were entitled to.
Now let me stop here and explain, in Florida to get unemployment you have to have been
working and then fired or greatly reduced in hours through no fault of your own.
This is not a lazy entitlement program or anything like that, so just save that for
somewhere else.
So he goes through and he lays out why it's not his fault, why somebody else was responsible
for it, throws another Republican under the bus, and then he gets asked why he didn't
fix it.
He said because nobody had asked him to.
It's a statement that just boggles my mind.
I'm fairly certain that when the people of Florida sent him to Tallahassee, that was
them asking him to.
They were asking him to look out for his interests.
I don't know who he thinks should have told him to fix a basic function of government.
Lobbyists?
The president accepting more marching orders from Trump?
I don't know who should have asked him.
It was his responsibility to fix it, but he needed somebody to tell him to do his job?
They're at the point where they don't even feel like they need to display any initiative.
They're admitting they're just puppets of lobbyists and government functionaries.
It's surprising and it's not.
We all knew that this is how it is.
I think on some level everybody knew that, but they at least pretended before.
They've gotten to that point where the ruling class in this country, that political class,
they feel so entitled to those positions, so entitled to rule over their lessors, that
they don't even feel they need to pretend that they're looking out for the little guy.
Governor DeSantis had the means, therefore he had the responsibility.
He had the responsibility to act because he could.
It's very fair to point fingers at DeSantis and at Scott.
They both failed.
It's fair.
We just need to remember that we're now painfully aware of how inept, how incompetent our government
is.
It makes us looking out for each other that much more important.
Rule 303, if you have the means, you have the responsibility to act.
They're not going to help.
This is probably the time to set up your little community network.
Get your group of friends together and make that open statement.
Really commit to the cause of mutual aid, of you guys helping each other out, of you
taking that step and saying it out loud, we're going to look out for each other.
We're going to look out for our communities.
Because it's very clear the ruling class in this country isn't going to do it.
Sometimes there's justice and sometimes there's just us.
Right now, it's just us.
We need to remember that more than ever as the country descends into this mess that has
been created by electing these people who feel entitled, these lazy entitled people.
They don't even have to do their job.
They don't even have to pretend that they're doing their job.
Well nobody told me I needed to do that.
If you were a cashier and somebody walked up to check out and you just stood there and
you told your boss, well, they never said that they wanted me to ring them up, what
would happen?
Same thing should probably happen here.
That person probably shouldn't have a job anymore.
They are so entitled.
They feel so far above the rest of us that they don't even need to pretend they're looking
out for their constituents anymore.
Anyway it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}