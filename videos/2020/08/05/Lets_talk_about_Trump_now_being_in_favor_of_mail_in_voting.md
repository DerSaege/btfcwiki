---
title: Let's talk about Trump now being in favor of mail in voting....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=e1hISo4kNgM) |
| Published | 2020/08/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President's stance on mail-in voting has shifted multiple times.
- President's approval ratings are low due to his performance.
- Mail-in voting is set to begin in many places soon.
- President's tweet in support of mail-in voting in Florida includes a MAGA hashtag.
- President believes he will win in Florida.
- President is likely to claim rigged elections in states that vote against him.
- President's goal is to create doubt and undermine the election.
- Trump's support or opposition to mail-in voting is about his ego and desire for power.
- Trump's decisions are solely driven by what benefits him.
- Trump doesn't care about the American people or the Constitution.

### Quotes

- "It's about him. It's about his ego and his desire to retain power."
- "Don't try to overthink it. It's all about Trump."
- "He doesn't care about you. He never did."
- "He doesn't care about America. He never did."
- "He doesn't care about your voice. He cares about power."

### Oneliner

President's stance on mail-in voting is driven by ego and power, aiming to create doubt and undermine the election for personal gain.

### Audience

Voters

### On-the-ground actions from transcript

- Request a ballot and vote by mail in Florida (suggested)
- Be prepared to counter attempts to undermine the election (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's motivations behind his stance on mail-in voting, shedding light on his ego-driven decisions and lack of concern for the American people.

### Tags

#MailInVoting #Trump #ElectionUndermining #VoterSuppression


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about how the president is now in favor of mail-in voting,
you know because he was against it after he was for it and then he's changed his tune
again.
Shouldn't come as a surprise if you understood why he was opposed to it in the beginning.
He tried to differentiate between absentee and mail-in voting.
There's no difference.
Absentee you have to request, mail-in voting they deliver to you.
Neither is rife with any problems.
I want to say it's like 99.97% of votes are legitimate.
There's no large voter fraud problem.
That's made up, something that doesn't really exist.
But see, the president's poll numbers, they were in the basement.
They were really bad because he's doing a bad job.
So Americans don't approve of his performance.
Mail-in voting in a lot of places starts like next month.
So if they start voting when they don't support him, they'll probably vote against him.
That's the way he was looking at it.
Now he's probably figured out that here in Florida a lot of mail-in votes go to the Republican
Party.
Now whether or not that transfers to him, that's an entirely different question.
But it's probably what led to this tweet.
Whether you call it vote by mail or absentee voting, in Florida the election system is
safe and secure.
Tried and true, Florida's voting system has been cleaned up.
We defeated Democrats' attempts at change.
So in Florida, I encourage all to request a ballot and vote by mail.
Hashtag MAGA.
And it's full of all of the random capitalization you would expect from a Trump tweet.
See he thinks he's going to win here.
So this election, well it's good.
We need to count those.
But those in those other states that would vote against him, those are rigged.
This shouldn't come as a surprise, it's characteristic.
It's the 14th characteristic of people of his ilk.
People who want to suppress the vote.
People who want fraudulent elections.
They try to cast doubt on it.
They try to undermine it.
It's what they do.
It's what they all did.
It is the 14th characteristic.
And he's filling it out.
Don't be surprised when in another week or two he says that it's going to cause rigged
elections again.
When he's talking about a state that is going to vote against him.
His goal here is to undermine the election.
To create a situation where there's doubt.
Because he's fairly certain he's going to lose.
Because he's a bad president.
At the end of the day, Trump's support or opposition to mail-in voting has nothing to
do with safety or security or fraud or anything like that.
It's about him.
It's about his ego and his desire to retain power.
Nothing more.
Don't try to overthink it.
It's all about Trump.
Every decision he makes.
It's what helps him.
He doesn't care about you.
He never did.
He doesn't care about America.
He never did.
He doesn't care about the Constitution or the election.
He never did.
He doesn't care about your voice.
He cares about power.
Anyway, don't be surprised when he flip-flops on this yet again.
It will happen.
Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}