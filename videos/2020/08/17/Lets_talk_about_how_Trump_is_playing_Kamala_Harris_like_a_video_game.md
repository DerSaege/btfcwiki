---
title: Let's talk about how Trump is playing Kamala Harris like a video game....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ADaDy7ClSDQ) |
| Published | 2020/08/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is playing Harris like a video game, revealing insights into himself.
- A study on Halo 3 explained why women players face hostility in male-dominated spaces.
- Poor performing males reacted hostilely to women disrupting male hierarchy.
- They were submissive to male players but hostile towards women.
- Higher skilled men reacted positively to women teammates.
- Trump's interactions with world leaders mirror these findings.
- Putin and others can manipulate Trump, showing his submissive behavior.
- Trump reacts negatively to women leaders, like Greta Thunberg.
- Politics isn't a game with clear scores; Trump's behavior stems from personal acknowledgment of failure.
- Trump's international behavior reveals his deep-rooted insecurities and inadequacies.

### Quotes

- "Trump is playing Harris like a video game."
- "Politics isn't a video game, especially on the international stage."
- "Deep down he behaves this way because he knows he's a failure."

### Oneliner

Trump's behavior towards female leaders mirrors a study on male hostility in gaming, revealing deep-seated insecurities and inadequacies.

### Audience

Political observers, feminists

### On-the-ground actions from transcript

- Analyze and challenge power dynamics in male-dominated spaces (implied)
- Support women leaders and challenge gender biases in politics (implied)

### Whats missing in summary

The full transcript provides a deeper insight into Trump's behavior and its underlying causes.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about how President Donald J. Trump is playing Harris like a video
game.
And what his campaign against her says about him, more importantly, what it says that he
knows about himself.
We're going to do this because I made a joke in a recent video, kind of an offhand statement.
The President said that men were bothered by the idea of a woman being nominated for
Vice President.
I said that wasn't true.
I said that no men were bothered by that, but maybe some little boys were.
Was a joke.
I had women push back on that statement and say no that's not true, there are men who
are bothered by it.
Now to be honest, had it been men saying that I would have written it off and just believed
it was them trying to justify their own behavior.
But the observation was there.
Ever since then I've been trying to figure out why that might be.
I found my answer in a 2015 study about video games, specifically Halo 3 was the one they
used.
And they were trying to determine why women were subjected, why women players were subjected
to hostility from their teammates.
They viewed it through the lens of evolutionary psychology and what they concluded was that
female initiated disruption of a male hierarchy incites hostile behavior from poor performing
males who stand to lose the most status.
I mean that's funny in and of itself, but there's more to the study.
Not just did it determine that men who were poor performing, that were low skilled, that
they reacted this way to women, it also determined that they were more submissive to players
who were men.
Look at Trump's interactions on the world stage.
Look at the way he reacts to other world leaders.
This is why Putin and people like Putin can basically pull him around by his ear.
And if you look at the way he reacts to world leaders who are women, even if they're not
traditional world leaders, look at the way he reacted to Greta.
It demonstrates that this is true, that this completely fits him.
Like to take this moment to stop and point out that higher skilled, higher performing
men didn't do this.
They reacted more positively towards women teammates.
But see there's one other piece to this.
Politics isn't a video game, especially when you take it to the international stage.
There's no objective score.
It's not like he can look at a screen and know that he's poor performing, know that
he's low skilled.
There's nothing objective telling him this.
You know, it's not like domestic politics where he has poll numbers or something that
he can go off of.
On the international stage, this behavior has to be rooted in his own personal acknowledgement
that he is low performing, that he's low skilled.
There's nothing telling him.
He has to know deep down under the facade, under the image that he tries to put forth
of being a tough guy, deep down he behaves this way because he knows he's a failure.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}