---
title: Let's talk about admitting mistakes, arrogance, and schools....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-yj53BZ_EEI) |
| Published | 2020/08/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Everyone makes mistakes, but admitting them is the first step in stopping them.
- People are often reluctant to admit mistakes if their image is based on being infallible.
- Mistakes have been made in the country, impacting its response to crises compared to other nations.
- Public health officials advised policy makers on errors like reopening too soon, but policy changes were not made.
- Students tested positive for the virus, yet there was no change in policy.
- The longer mistakes go unacknowledged, the longer the crisis will persist.
- Policy makers need to admit their mistakes and implement corrections to prevent further harm.
- Delayed admission of mistakes can lead to increased loss of lives and damage to the economy.
- A unified response and plan of action are necessary to address the crisis effectively.
- Politicians' refusal to admit mistakes and prioritize ego over correction hinders progress.
- The American people are forgiving of mistakes but not of arrogance leading to losses.
- Admitting mistakes is key to addressing the current crisis and systemic issues that have been ignored.
- Failure to admit and correct mistakes will perpetuate ongoing issues affecting the economy and society.
- Without acknowledging past mistakes, progress towards resolving issues will be impeded.
- Admitting failures is necessary for moving forward and improving the situation for all.

### Quotes

- "Admitting them is the first step in stopping it because a mistake just compounds until it's admitted and corrected."
- "We cannot make America great until we admit that we weren't."
- "It's okay to make mistakes. Not correcting them, that's when it becomes a real issue."
- "The American people will forgive mistakes. They won't forgive people being lost because of arrogance."
- "Admit them so we can fix them."

### Oneliner

Admitting mistakes is vital to stopping compounded errors, preventing further harm, and moving towards a unified response for a better future in America.

### Audience

Policy Makers, Public Health Officials, American Citizens

### On-the-ground actions from transcript

- Public health officials must continue advising policy makers on necessary corrections (implied).
- Policy makers need to acknowledge mistakes and implement changes promptly (implied).
- American citizens can advocate for transparent and accountable leadership (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the impact of admitting mistakes, urging for accountability to address current crises effectively.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about making mistakes and admitting them.
Everybody makes mistakes.
Admitting them is the first step in stopping it because a mistake just compounds until
it's admitted and corrected.
The likelihood of somebody admitting a mistake is diametrically opposed to how much of their
image is based on the idea that they're infallible.
If they push the idea that they're always right, when they make a mistake they're very
reluctant to admit it and therefore they're very reluctant to change it.
We have made mistakes in this country.
We've made them throughout history and we've made some big ones very recently.
This is borne out in the evidence when you compare how the US is doing versus other countries.
Those in political office, those who make policy do not want to admit their mistakes.
Around the same time that public health experts were advising policy makers on the mistakes
that were made and were saying, hey the reason this is going on is because we didn't shut
down far enough and we reopened too soon, there were students walking around two schools.
Those students were positive.
As of yet there has been no change in policy.
We will find out in a couple of weeks how many, if any, those positive students transmitted
it to.
And then a little while later we'll find out how many the students who had it transmitted
to them, how many people they transmitted it to.
And it's just going to go on.
We made mistakes and we're not admitting them.
The longer public health officials have to talk to a wall, the longer this is going to
go on.
Our policy makers have to admit that they made mistakes.
They're flawed.
That's okay.
I understand.
When you're looking at 150,000 gone because of the mistakes, it's hard to admit that that's
your fault.
But that number will double or triple depending on how long it takes them to admit their mistakes
and correct them.
For those that don't care about the human cost, the economy will suffer the longer this
goes on.
It will get worse and worse.
It will compound.
There has to be a unified response.
There has to be a unified plan of action.
Otherwise this will just continue.
It will drag on.
We've seen it.
It's in history books.
This is how it happens.
And this is mainly at this point, there's a pretty overwhelming consensus on what works
and what doesn't.
At this point, the only thing stopping us from solving this issue, from working towards
getting things back to normal, are politicians who want things back to normal now and are
too arrogant and too concerned with their own ego to admit that they were wrong.
Those politicians who made mistakes, that's okay.
It's okay to make mistakes.
Not correcting them, that's when it becomes a real issue.
The American people will forgive mistakes.
They won't forgive people being lost because of arrogance.
And that's what it boils down to now.
You have politicians ripping into public health experts because they don't like the answers
they're getting, because the answers don't match the political talking points they were
handed by another politician.
It's time to admit our mistakes as a country.
And this goes to the handling of this crisis and all of the mistakes that we made before,
that we try to sweep under the rug.
We all know they were made.
Admit them so we can fix them.
If we pretend like they weren't made, they will go on.
All of these issues will go on.
The public health crisis, all systemic issues, will continue until we admit that they're
there.
They will continue to be a drag on the economy, a drag on human life, and a drag on the American
way of life.
We cannot make America great until we admit that we weren't.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}