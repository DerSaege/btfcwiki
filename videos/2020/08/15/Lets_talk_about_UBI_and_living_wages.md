---
title: Let's talk about UBI and living wages....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1ZADrnNuiPk) |
| Published | 2020/08/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Criticizes a flawed argument against Universal Basic Income (UBI) that suggests it will disincentivize work.
- Questions why people take on college debt or aim for higher-paying jobs if a basic income is provided.
- Challenges the notion that minimum wage workers will stop working if given a basic income.
- Points out that being poor is a lack of cash, not a lack of character, and everyone desires better opportunities.
- Argues that stagnant wages lead to people being taken advantage of by both companies and the government.
- Advocates for fair wages and criticizes the idea that some individuals deserve to be in poverty.
- Stresses the importance of not looking down on those with less power or influence in society.

### Quotes

- "Being poor is a lack of cash, not a lack of character."
- "Wages stagnated. And now they're so out of whack, what it
  will take to bring them into where they should be seems just out there."
- "People didn't get to where they were at simply because they made good choices or made bad choices."

### Oneliner

Criticizing flawed arguments against UBI, Beau challenges the belief that providing a basic income will disincentivize work and argues for fair wages for all.

### Audience

Advocates for fair wages.

### On-the-ground actions from transcript

- Advocate for fair wages for all workers (implied).
- Support policies that uplift individuals out of poverty (implied).

### Whats missing in summary

The full transcript delves deep into the issues surrounding wages, poverty, and societal perceptions, providing a comprehensive analysis of the flawed arguments against Universal Basic Income and the importance of fair wages for all individuals.

### Tags

#UniversalBasicIncome #Wages #Poverty #Fairness #Society


## Transcript
Well, howdy there, In-N-Out people. It's Beau again.
You know, every once in a while an idea takes hold
among Americans
and it's just so bad.
The argument is so flawed
that when it hits my noggin,
just, no, can't do it. Nay, nay, I say.
No, no, no.
So today we're going to talk about UBI
and one of the arguments that gets used against it.
Now, if you don't know, I'm not actually a proponent of UBI,
of universal basic income,
but this argument is just that bad
that I feel like I want to say something about it.
And the reason it's bad is not necessarily
what's being said.
It's that it's an underlying theme
that hits a bunch of other policies as well.
The idea is that universal basic income
would disincentivize work.
That if people are making a grand a month,
they're just having it handed to them,
$12,000 a year,
well then they won't work.
See, there's a fundamental flaw in this argument
and it leads to a bunch of bad thoughts,
bad things, and bad policies.
So first, we're going to get rid of that argument.
We're going to demonstrate that it's not true.
And then, we're going to explain why it's such a bad concept.
Why do people take on college debt?
$12,000 a year, that's a little less the minimum wage.
Why do people take on college debt?
People who go to college today,
are they really seeking an education
or are they seeking credentialing
so they can get a higher paying job?
Doesn't make sense.
Why would they go through all that trouble
to get a higher paying job?
It's $12,000 a year is enough.
Anything beyond that would disincentivize them.
What about those people who are working,
making $36,000 a year,
but they're really bucking for that promotion?
That doesn't make sense either
because that's three times the amount
that should disincentivize them.
They shouldn't want to do anything else.
They're already making three times more
than what it would take to make them not want to work.
What about trust fund kids?
They don't work at all, but most of them still do.
They do investments or they get a job to make more money.
Why?
They're certainly making more than a thousand bucks a month.
We should just pay everybody in the military
a thousand dollars a month.
Everybody, across the board.
I mean, that's all it takes, right?
And especially with them, they got housing,
cheap food, health care, recreation, they're on post.
I don't know.
I guess they have a job that just has to be done, right?
It's funny because I think we just all learned
that a whole lot of minimum wage jobs are essential.
They have to be done.
And that's where this argument gets pointed to.
It will disincentivize work for those at the bottom,
for those making minimum wage, a thousand bucks a month.
See, I don't know.
Are they somehow different?
Do they have a different character?
Are they different kinds of people?
Do they not want more?
Do they not deserve more?
See, a thousand dollars a month, that's
awfully close to minimum wage.
So why would they work, though, if they could get it for free?
See, the fundamental argument here is that they are different.
They have a different character.
Being poor is a lack of cash, not a lack of character.
They want better, just like everybody else.
It's human nature.
It would not disincentivize work.
It wouldn't.
Not for anybody.
I mean, that's not something that's going to be widespread.
So the reason this argument gets made
is because we have to justify minimum wage workers making
so little.
That's the reason the argument gets made.
We have to find some way to suggest that they're different.
Because otherwise, making just above the poverty line,
working 40 hours a week, that seems pretty bad.
A society probably shouldn't allow that.
And see, this is where the idea comes in.
Well, you know, that's just for teens.
It's not.
Quote for you.
No business, which depends for its existence
on paying less than living wages to its workers,
has any right to continue in this country.
By business, I mean the whole of commerce,
as well as the whole of industry.
By workers, I mean all workers, the white collar class,
as well as the men in overalls.
And by living wages, I mean more than a bare subsistence level.
I mean the wages of a decent living.
That's FDR, the guy who enacted the minimum wage.
It was never supposed to be for teens or for people retiring
or whatever argument you want to throw out.
It was supposed to provide a decent living.
But see, wages stagnated.
And now they're so out of whack, what
it would take to bring them into where they should be
seems just out there.
Nobody should get paid $15 for flipping burgers.
I only make $12 an hour, and I'm an EMT.
EMT.
I would suggest that, and will prove,
that the minimum wage workers are being taken advantage of,
and that so is the EMT.
But see, what happens is the EMT doesn't notice it,
because the EMT has somebody below him or her
that's doing worse.
So it has somebody to look down on.
It has somebody to kick down at and suggest
that they're different.
They have a different character.
That's why I'm up here and they're down there.
They don't notice, because you have that image, that idea,
that somehow, if they were handed $12 grand,
well, they would just sit at home, because they're lazy.
But you wouldn't do that, right?
The EMT wouldn't do that.
The reason this is happening is because wages are stagnant.
If you compare the average wage to productivity, you'll see it.
There's charts for this, tons of them.
You can compare it to pretty much anything.
Any metric you want to use, you will
see that wages are stagnant.
Meanwhile, the large companies continue to make more and more
and more and more.
The economics of it is there, but let's
put this into something that is more easily relatable.
Let's say somebody says, hey, you've got two acres of corn.
I'll harvest them and give you an acre's worth of the corn.
OK, deal.
That was the baseline.
That's the decent living.
What if you were to walk out to your field later
and find out that they harvested four acres of corn,
but only gave you an acre?
You would probably want another acre's worth of corn.
That's what this is.
That's an attempt to get wages back to where they should be,
if they were linked to productivity or anything else,
And see, here's the other part about it,
because you still have that EMT.
If people are getting $1,000 a month,
that's almost poverty level.
So there'd be a whole lot less people in poverty,
which means there'd be less people qualifying
for assistance.
Because right now, as it stands, not just are they
taking advantage of the EMT because they're
paying the person too little.
Then, once the company's done taking advantage of them,
the government comes along and takes advantage of them
for the company.
Because the taxes that come out, instead
of being used to benefit society as a whole,
they get used to provide assistance
to people who are working, who should
be making a decent living.
So the EMT ends up subsidizing the wages of the minimum wage
worker.
Also, the companies can make more money.
It doesn't disincentivize work.
That idea is based on the concept that poor people are
somehow different, that minimum wage workers, well,
they're lazy.
They deserve to be there.
They made bad choices, that somehow they're
just different kind of people.
They don't have the same makeup as literally everybody else,
from a college student to the trust fund kid to the soldier
to the person working at the insurance company
bucking for a promotion.
Those minimum wage workers, they're different.
That's where the idea leads.
That's why it's a bad idea.
Because then you can justify the fact
that you have people working 40 hours a week living in poverty,
getting assistance that you're paying for,
so a big company doesn't have to pay them a living wage.
When you make that argument, when
you make the argument that wages don't
need to be raised for the people at the very bottom, what
you are saying, quite literally, is
that I'm OK with being taken advantage of,
and I'm OK with subsidizing their assistance.
Also, the big companies can make more.
It is a horrible premise.
It is a horrible idea.
People didn't get to where they were at simply
because they made good choices or made bad choices.
There's a lot of factors that go into it.
Being poor is a lack of cash, not a lack of character.
We need to stop conflating the two.
And we need to remember that those below us
on the socioeconomic ladder, those with less influence,
less power, and in this country, money is power,
they are never the source of our problems, never kick down.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}