---
title: Let's talk about what 2020 can teach us about the last ten years....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SEU_AQcUVjI) |
| Published | 2020/08/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- 2020 teaches a lesson on not trusting politicians when it comes to science.
- Politicians have made inaccurate claims about the disappearance of cases and impact on kids regarding the current issue.
- Experts have consistently warned about the seriousness of the situation and the need for action.
- Politicians prioritize the economy over the well-being of the people, especially in matters of science.
- Climate data from the last 10 years shows alarming trends, but politicians downplay the severity of the situation.
- Scientists stress the importance of immediate action to prevent the situation from escalating further.
- The urgency to elect officials who prioritize addressing critical issues like climate change.
- Voters should hold politicians accountable for prioritizing short-term gains over long-term consequences.
- Delaying action on pressing issues can lead to catastrophic outcomes.
- Beau urges the American people to learn from past mistakes and not wait until it's too late to act.

### Quotes

- "Don't trust politicians when it comes to science."
- "We have to act or it's going to get out of hand."
- "Politicians will always put the economy, their pocketbook, over you."
- "It's going to be too late. It's going to get way out of hand."
- "If we wait, by the time we get to the point where everybody knows that the politicians lied, it's going to be too late."

### Oneliner

2020 teaches us not to trust politicians on science, prioritize urgent action over their economic interests, and elect officials who address critical issues promptly.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Elect officials willing to prioritize urgent matters like climate change (implied).
- Hold politicians accountable for prioritizing short-term gains over long-term consequences (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how politicians prioritize economic interests over public well-being in matters of science and climate change.

### Tags

#Science #ClimateChange #Politicians #Trust #UrgentAction #Accountability


## Transcript
Well, howdy there, internet people. It's Beau again. So today we're going to talk about what
this year, 2020, can teach us about the last 10. Because there's a really important lesson
to be learned in 2020, although I'm not sure it has been learned by the American people.
That lesson's pretty simple. Don't trust politicians when it comes to science.
They will make stuff up. We've seen it all year. What have you heard so far? Well, soon,
all those cases, it'll be zero cases. It'll disappear like a miracle. It'll disappear
once summer comes, once it warms up. Can't impact kids. It's just something we have to
go through. Have to let it run its course. It's naturally occurring. That's what the
politicians have been saying. What have the experts been saying? They've been pretty consistent
on their messaging. This is something we have to pay attention to. We have to act or it's
going to get out of hand. Yeah. That's kind of what happened. Kind of what happened. Now,
those who choose to believe the politicians will say, you know, their models weren't exactly
right. Yeah. Models are only as good as the data you put into them. And early on, you
don't have the best data. Aside from that, when, once a model becomes public and people
see it, a lot of times they'll adjust their behavior because of that. Wow. We really don't
want to do that. So they'll take small steps for themselves, which will change the outcome
of that model. Okay. The end goal there, the end thing that everybody should learn is you
can't trust politicians. They will always put the economy, their pocketbook, over you.
Seems like it should go without saying because they do it all the time and everybody understands
this on other subjects. But when it comes to science, for some reason, they trust the
government, which just, I don't think I'll ever understand that. Let me give you some
facts about the last 10 years. 2019 was one of the hottest three years on record since
records began in the 1800s. The two hotter years were 2015 and 2016. The six hottest
years on record are the last six. Surface sea temperatures, second highest on record.
Air temperatures over the poles, second highest. Sea levels have risen to new record-breaking
heights for the eighth year in a row. Glaciers are melting at an alarming rate for the 32nd
year in a row. What are the politicians telling you? Don't worry. It's just something we have
to go through. It's natural. Don't worry about it. Not going to be a big deal. It'll disappear
on its own. It'll fix itself. All that sounds familiar, right? How's that working out for
us? Not so good. Messaging from scientists, from the experts, has been pretty consistent.
We have to act or it's going to get out of hand. We have to act or this is going to get
out of hand. And once it gets out of hand, it's like an 18-wheeler. You can hit the brakes
all you want, but it doesn't stop on a dime. It keeps going. This is something we have
to act on. Now, we have to get people in government, in high positions, who are willing to act
on this immediately. Needs to be a pretty big priority. This needs to be one of your
uh, one of the considerations that you take into account when you're considering your
vote. If a politician is telling you that none of this is true, that it's a hoax, it's
the same scenario. They're putting the economy, short-term gains, their pocketbook over you.
I hope the American people learn this lesson. Because if we wait, like we have with the
current issue, by the time we get to the point where everybody knows that the politicians
lied, where everybody is well aware of it because they can see it with their own eyes,
it's going to be too late. It's going to get way out of hand. It is going to be a disaster
movie. Anyway, it was just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}