---
title: Let's talk about back and forth, where we go, and how we got here....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=I3KJQ-Cckdg) |
| Published | 2020/08/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the current state of affairs in the country, referring to it as a mess and questioning what comes next.
- Describes the progression from civil rights debates to violent marches and how it has become an "one for one" situation.
- Points out the divisive rhetoric in the media and political landscape, leading to a PR campaign with violence.
- Criticizes the role of media personalities and politicians in stoking extreme division for financial and political gain.
- Warns about the dangers of the country being torn apart due to inflammatory rhetoric and the pursuit of power.
- Urges for leadership that can unite people and heal the wounds caused by division.
- Emphasizes the need for individuals to communicate, find common ground, and reject attempts to incite conflict.
- Expresses concern about the manufactured enemy narrative created to keep people loyal and divided.
- Questions the effectiveness of current leadership in uniting and healing the country.
- Encourages people to step back, start dialogues, and prevent further escalation towards chaos.

### Quotes

- "We are there. We're there. And we got here because a whole bunch of people got really rich dividing this country."
- "This country needs people who can unify, needs people who can help heal the wounds that have been torn open by inflammatory rhetoric."
- "You need to think back through your life and try to remember any time that was like this."
- "This country isn't united, and it's not going to be able to unite under Trump. It will not heal under Trump."
- "The most important thing for this country at this moment in time is to take a step back, because once this starts, it doesn't stop easily."

### Oneliner

The country is on the brink of chaos due to divisive rhetoric and the pursuit of power, urging for unity and communication to prevent further escalation.

### Audience

Citizens, Activists, Leaders

### On-the-ground actions from transcript

- Communicate with those around you to find common ground and bridge divides (suggested)
- Reject attempts to incite conflict and violence in your community (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of how societal division has been fueled by media sensationalism and political agendas, urging for unity and communication to prevent further chaos.

### Tags

#Division #Unity #InflammatoryRhetoric #PoliticalLeadership #CommunityEngagement


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a mess.
What's next right?
Where are we going from here?
That's the question. Because it
appears that we've entered the
back and forth phase of all this. It's what it looks like.
We don't know yet, but that is what it looks like.
Just a quick little recap on how we got here.
It started as a discussion, debate,
over civil rights. Then it moved to marches,
and marches got rough. And now it certainly looks like
it's one of yours for one of mine. I will let the people from Belfast tell you
what happens next. I have to go look for my coffee grinder apparently.
See the thing is
we're already there. If you've watched
this channel long enough, if you've watched my videos about this topic in particular,
you know I've gone to great lengths to explain
how these things start, how they grow, how they mature,
and what they become. And if you go back through the videos when I talk about what they become,
I use the same phrase over and over again because it's the best way to describe it.
It's really easy to remember. When these things mature
and they're right there, they're at the point,
they become a PR campaign
with violence. We don't know
at the time of filming, we don't know what happened. We have no clue what happened in Portland.
But I know that we're already at that point.
Because there was a mix-up in the initial reporting.
The initial reporting made it seem like the victim was
part of one group, and then as it was clarified,
seemed to suggest he was part of another. If you were on social media at the time,
you had to see the rhetoric. Do a 180.
Everybody. If they were saying it was justified,
well suddenly it became unjustified, and vice versa.
Because it's no longer about the facts. We still don't know the facts.
It's about affiliation.
It's about branding. It's about controlling the narrative.
It's about managing the public perception of the event.
It's a PR campaign with violence.
We are there. We're there. And we got here
because a whole bunch of people got really rich dividing this country.
Playing this game of, I'm gonna be sensational,
say something just a little bit more extreme than the show that comes on
before or after me.
That way I'll get the headlines. The problem is
everybody's playing that game. So the rhetoric just became more
and more and more extreme until people
were justifying and defending the indefensible, I'm looking at you, Tucker Carlson.
This continued for years.
And you have politicians. They don't want to be
looked at as soft, so they adopted this absurd rhetoric.
Painting fellow Americans as the opposition.
Othering people. Creating scapegoats.
Doing all the things that we see
in countries we liberate.
You have these politicians waving the American flag
while destroying the country
it's supposed to represent. Tearing it apart with their words.
Dividing Americans up so they can shore up their base.
Because while those pundits care about the money and the ratings,
the politicians care about the money and the power.
And then, see,
then we got that real far-right winger.
Everybody just fell in line because nobody wanted to say, whoa, whoa, whoa,
hang on a second, I didn't really mean all that stuff, I was just saying it to get paid.
So they all fell in line and they all supported him. And this is the result.
Steps away. We are about to go through the door
to the unimaginable in this country.
And it was all over money and power.
It was all a bunch of rich old guys trying to secure their place in the world.
Playing up ideological gaps that initially weren't really there.
But as that rhetoric got used
and people got moved further and further and further to the right,
then they developed. Then they became real.
They weren't there in the beginning.
And now they are.
And we see the results in the street
as they continue to push a leader who
profits from Americans being divided.
What this country needs now more than anything
is leadership who can unite,
bring people together, make people realize,
especially those who are out in the streets right now,
make them realize they have more in common with each other
than they do with their representative in DC.
See, if this does spiral out of control,
all these people, the people who started it,
the people who inspired it, they're going to be nowhere to be found.
They will be safely on set somewhere or they will have a security detail
protecting them.
Your family won't.
This country needs people who can unify,
needs people who can help heal the wounds
that have been torn open by inflammatory rhetoric.
This country needs to stop watching
those who go out of their way to inflame and incite.
This country is going to be torn apart
by people who actually have the motto
and the idea in their head that they're saving it.
The country isn't going to go backwards.
It's going to continue to move forwards.
How that happens, well, now that's up for debate.
Progress is always difficult,
but it doesn't have to be like this.
All it takes is for people to realize their mistake
and take a step back, start talking to each other,
stop kicking down, trying to subjugate others
and impose your will.
That's all it takes.
We can do it, but there are a lot of people
with a lot of power right now who want anything
other than a united country,
because if the country unites,
if the country starts talking instead of pointing fingers,
instead of arguing, instead of fighting,
they know they won't have a job.
You need to think back.
This is especially true of a voting base
that is most loyal to one of the biggest offenders.
You need to think back through your life
and try to remember any time that was like this.
And whether the changes that are going on right now,
the things that the Democrats are half-heartedly advocating for,
like Medicare for all and stuff like that,
if that's more inflammatory than, say, I don't know,
ending segregation,
that seems kind of like a small thing in comparison, right?
So why are we suffering the same types of civil unrest?
Because it's a manufactured enemy.
Donald Trump has gone out of his way
to divide this country to keep you loyal.
He's gone out of his way to get you to view
anybody who questions him as fake news, as false.
Only Donald Trump knows the truth.
Only Donald Trump can guide this country.
The thing is, he's been guiding the country for four years,
and this is where we're at.
If he could do all of this stuff, why hasn't he done it?
This country isn't united,
and it's not going to be able to unite under Trump.
It will not heal under Trump.
It will only get worse.
These pundits, these people who thrive
on fear and anger,
they've driven the country to the brink,
and they've done it while wearing
American flag lapel pins.
We haven't gone through that door yet,
but we're really close, and the mindset is already there.
It's already about the narrative.
It's already about the PR.
All it takes now is for somebody
who can't really see what they're doing to go too far.
Then it's out of control.
Then it's done, and everybody's going through the door,
whether you want to or not.
The most important thing for this country
at this moment in time is to take a step back,
because once this starts, it doesn't stop easily.
We're right there.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}