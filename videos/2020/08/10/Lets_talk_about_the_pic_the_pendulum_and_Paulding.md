---
title: Let's talk about the pic, the pendulum, and Paulding....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cCh6_qW7ZXI) |
| Published | 2020/08/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Returning to Paulding County due to a new development involving school closures and distance learning.
- Uncertainty about the duration of school closures based on the current number of cases.
- A 15-year-old advocate pushed for precautions like a mask mandate, acknowledging the limitations of her influence.
- Governments often oscillate between inaction and overreaction in their responses.
- Despite initial resistance, the community desires in-person schooling for their children.
- The community's wish clashes with the rising COVID-19 cases due to insufficient precautions.
- Media attention was drawn to the county due to documentation by the young advocate.
- The community must implement precautions to avoid repeated school closures or tragic outcomes.
- Beau praises the young advocate's courage and impact in saving lives through her actions.
- Lack of effective leadership and precautionary measures contributed to the crisis in the community.

### Quotes

- "She saved lives."
- "Realistically, she chose one that she really shouldn't have won, but she did."
- "May not win exactly the way she wanted to. But victory rarely looks the way you think it's going to."

### Oneliner

In Paulding County, a 15-year-old's advocacy triggers school closures and prompts community reflection on the importance of precautions and leadership to combat COVID-19.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Implement strict precautionary measures in schools and communities (suggested)
- Advocate for mask mandates and safety protocols (implied)
- Raise awareness about the importance of preventive actions (exemplified)

### Whats missing in summary

The full transcript provides a detailed account of how a young advocate's actions influenced community responses to COVID-19, showcasing the importance of individual advocacy in public health crises.

### Tags

#COVID19 #CommunityAdvocacy #SchoolClosures #Precautions #Leadership


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the PIC and the Pendulum.
We're going back to Paulding County because there's been another development.
The schools are going to be closed for a few days.
Don't know exactly how many just yet, but in the meantime they'll be doing stuff via
distance learning which suggests to me that they already had that capability set up which
is kind of interesting.
We don't know exactly how many days they're going to be closed because their current number
is based on the current number of cases.
That number is likely to rise.
Therefore the number of days that they're going to have to kind of shut down, that's
also going to rise.
The account associated with the young woman who kind of got the ball rolling on this tweeted
something and it almost seemed like she was beating herself up over the outcome because
she basically said that this isn't what she wanted.
She wanted precautions, namely masks.
She wanted a mask mandate.
And it makes sense.
I get it.
And yeah, a 15 year old was not capable of forcing a government entity to behave in a
rational manner.
Millions of voters have not been able to accomplish that feat in decades.
So what governments do, they swing back and forth, typically between inaction and overreaction.
I don't really think this is an overreaction.
I think it's what's going to have to happen.
But at the end of the day, I think she'll probably get what she wants because this community,
for whatever reason, really wants their kids at school in person.
They feel that's very important.
They're not getting that right now.
They're not getting that right now because there's a whole bunch of cases.
There's a whole bunch of cases and the school board had to act on it because there were
a whole bunch of media eyes on that county.
And they have a whole bunch of cases because they're not taking precautions, like at all,
apparently.
And the media eyes were there because of her pick, because of her documentation.
She got that ball rolling.
Now if that community really wants their kids back at school, they're going to have to take
precautions.
They don't have a choice.
Otherwise, they're going to close down every few days or they're going to have a really
big section in the yearbook for memorials.
Those are kind of the options.
So she's probably going to get what she wants in the long term.
It's going to take a while to get there, like any fight worth having.
It's going to take a minute.
You know, the advice I always give on this channel is to pick battles big enough to matter
but small enough to win.
Realistically, she chose one that she really shouldn't have won, but she did.
And I think the final outcome is going to be what she wants.
But even if it's not, even if it's not, she needs to remember that this break right now
with this school closed, that is stopping transmission.
And it's stopping the transmission of the people who would have caught it while the
school was open.
At the end of the day, her choice to act when she had the means to act saved lives.
She's a hero.
There's no overstating this.
She saved lives.
Her actions may not have given her the exact outcome she wanted right now, but it saved
lives.
If governments behaved with common sense, none of this would have happened.
Because there would have been real leadership in that community to begin with before school
ever went back.
There would have been people wearing masks.
There would have been people taking it seriously, setting up the precautions needed to open
the school safely, as safe as possible.
If you've watched this channel, you probably know how I feel about that.
But this community, that's what they want.
They want those kids back at school, in person.
They're going to have to take a lot of precautions if they want that.
There's no wishing this away.
It's either take the precautions, or the school opens and closes and opens and closes, and
disrupts the parents' lives.
Because I have a feeling that that may really be the root cause here, in a lot of cases.
I do not think she should be too hard on herself.
She got a county government in Georgia to take something seriously that most of the
country has been trying to get government entities like that one to take seriously for
months with no success.
May not win exactly the way she wanted to.
But victory rarely looks the way you think it's going to.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}