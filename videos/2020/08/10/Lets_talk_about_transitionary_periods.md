---
title: Let's talk about transitionary periods....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tGRLjWsi2Do) |
| Published | 2020/08/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Transitionary periods occur in countries and personal lives, like in the US in recent years.
- Celebrates friends' accomplishments, like promotions, finishing a doctorate, having a child, getting married, and more.
- Acknowledges the significance of using new titles or names to mark these transitions.
- Emphasizes the importance of supporting friends during their transitionary periods.
- Views refusal to acknowledge and use new titles as a sign of not being a true friend or a good person.
- Encourages celebrating and embracing changes in friends' lives.

### Quotes

- "If somebody goes through a transitionary period and you're their friend, you want to celebrate it with them."
- "If somebody goes through something, goes through some form of transition and you refuse to use the new way of addressing them, you're probably not their friend."

### Oneliner

Transitionary periods in countries and personal lives call for celebration and using new titles to mark significant changes, reflecting true friendship and support.

### Audience
Friends and allies

### On-the-ground actions from transcript

- Celebrate friends' achievements with them (exemplified)
- Use new titles or names to acknowledge and celebrate their transitions (exemplified)

### Whats missing in summary
The nuanced examples of friends' transitions and the importance of embracing change in personal relationships.

### Tags
#TransitionaryPeriods #CelebrateChanges #Friendship #Support #EmbraceTransitions


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
transitionary periods. You know we we know we have them historically speaking.
They occur all the time when we're talking about countries. They uh the US
for the last few years has certainly been in a transitionary period but they
happen in our personal lives as well. You know over the last few years a lot of my
friends have gone through changes and because I'm their friend I want to
celebrate it. I had a friend that got promoted, he was in the army, could not
wait to call him top. Another friend she finished her doctorate, couldn't wait to
call her doctor. Now partially because her last name is Jones and her field is
archaeology and come on. Another friend, couple actually, they've been trying to
have a kid for years, had to go to a specialist, finally took, could not wait
to call her mom. Another friend got married and first thing I said to her
afterward was addressing her by her new name.
Friend got promoted at work, been at that company forever, couldn't wait to call
him director. Another friend in the military went from enlisted to officer,
couldn't wait to call him sir. Another friend, not such a great scenario, she got
divorced, could not wait to call her miss and use her maiden name because in this
case that person she was married to didn't really let her be who she was,
really kind of kept her down. So that new title and that other name gave her a lot
of freedom, really symbolized that for her. It's a good thing. I know some people
change their entire names. If somebody goes through a transitionary period and
you're their friend, you want to celebrate it with them. You want to take
part in that and if it comes with a new title, a new way of addressing them, you
look forward to using it if you're their friend. If somebody goes through
something, goes through some form of transition and you refuse to use the new
way of addressing them, you're probably not their friend. You're probably not even
a good person. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}