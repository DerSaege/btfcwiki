---
title: Let's talk about building Democratic bridges and why we called Mexie....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A2cxHr7RkpI) |
| Published | 2020/08/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about communicating, building bridges, and engaging with different political ideologies.
- Explains the rationale behind choosing to feature a radical leftist named Maxi.
- Addresses the question of why showcase a group opposed to Biden-Harris during a critical election period.
- Describes the challenges in reaching out to radical leftists who do not see Biden-Harris as favorable.
- Explores the political spectrum beyond left and right, discussing the up and down authoritarian axis.
- Points out that radical leftists view Biden and Harris as unsupportive of their anti-capitalist and anti-authoritarian beliefs.
- Advocates for framing support for Biden-Harris as harm reduction rather than focusing on their positive attributes.
- Suggests finding common ground on issues related to marginalized people and workers rather than broader topics like the economy.
- Differentiates between supporting Democrats out of alignment with their platform versus seeing them as less harmful.
- Emphasizes the importance of defeating Trump as a unifying goal, especially for those who do not fully support Biden and Harris.

### Quotes

- "Biden and Harris are less bad than Trump-Pence."
- "Harm reduction, that's the way you're going to reach them."
- "Most people watching this channel are social Democrats."
- "Almost everybody who watches this channel is on the lower axis. They're anti-authoritarian."
- "Establish the bus that everybody's going to get on as defeating Trump."

### Oneliner

Beau explains why reaching out to radical leftists involves framing support for Biden-Harris as harm reduction rather than alignment with their beliefs.

### Audience

Community members, political activists.

### On-the-ground actions from transcript

- Challenge yourself to find common ground with individuals holding different political beliefs (implied).
- Prioritize harm reduction narratives when engaging with individuals who do not fully support a particular candidate (implied).
- Engage in respectful discourse and bridge-building efforts across political spectrums (implied).

### Whats missing in summary

The nuances of engaging with individuals across different political ideologies and the importance of harm reduction narratives for building bridges.

### Tags

#Communication #BuildingBridges #PoliticalEngagement #RadicalLeftists #HarmReduction


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about communicating, building bridges, riding buses, allies, all
that stuff we normally talk about on this channel.
We're going to be a little more specific about it today and we're going to talk about why
we called Maxi.
There's a whole bunch of people, if you haven't watched the last video, go watch that one
before you watch this.
There's a whole lot of people who are asking, why would you choose right now, this moment,
to highlight this group of people who are opposed to Biden-Harris?
You have said yourself that you think it's incredibly important that Trump is defeated
at the polls.
So why would you showcase them?
Why would you showcase those ideas right now when it's so important to be unified?
Because I'm watching a lot of Democrats try to reach out to them, try to reach out to
the real leftists, the people who are actually left, not American left, left by international
standards, the radical leftists.
Maxi is a radical leftist.
They're trying to reach out by highlighting Biden and Harris's good points.
That may be counterproductive when you actually listen to their beliefs, listen to their ideas.
There's not much on the Biden-Harris ticket for them.
There's nothing.
In addition to the left and right, which we talk about in the United States a whole lot,
there's another axis.
There's up and down, authoritarian at the top, anti-authoritarian at the bottom, libertarian,
but if you use that term in the United States, everything goes sideways.
Has nothing to do with the Libertarian Party.
Most leftists, not all, most leftists, especially radical leftists, are left and down.
So they're anti-capitalist and anti-authoritarian.
The Biden-Harris ticket has nothing for them.
Biden, even by a Democrat's description, is a corporate dim.
You've heard the term.
He's very capitalistic, very status quo.
Harris is very authoritarian.
There is nothing that you're going to be able to say about the Biden-Harris ticket that
is going to convince radical leftists that they're good.
There's no way to get from point A to point B on that one.
There's no bridge that can be built.
And that's the way it's being framed.
At the same time, when you look in the comment section of that last video, you will see Democrats
referring to these ideas as idealistic, contrary to human nature, a failed system.
These are all terms you can find down there.
Ridiculous.
And they're saying that the radical left views the Democratic Party platform the exact same way.
They view it as a system that has already failed.
So trying to show how Biden is going to uphold that system, that's not going to convince
them to help at all.
It's going to have the opposite effect.
When you sit there and say, well, he's going to do this and do this and help reform capitalism
and all of this stuff, you're actually turning them further away.
So the idea here, if you want to reach out to this group, if you want to reach out to
radical leftists, you cannot frame it as these are the reasons Biden is good.
These are the reasons Harris is good.
You have to frame it in the sense of harm reduction.
Biden and Harris are less bad than Trump-Pence.
That's the only way for that bridge to get built.
And when doing that, I would steer clear of issues like the economy, law enforcement,
stuff like that.
You're not going to find much common ground.
Where you might find common ground is the treatment of marginalized people and the treatment
of workers, specifically workers, not the economy as a whole, but the way the Biden
administration might be more favorable to blue collar workers.
Those are the bridges that can be built.
If you try to do blue no matter who, you have to understand the radical left, they're not
blue.
They don't align with Democrats because they believe in the platform.
They align with them because they see it as less harmful.
Harm reduction, that's the way you're going to reach them.
Aside from that, there's a whole bunch of people saying, I don't know where I fall,
you know, as she went through the various groups.
I will tell you that overwhelmingly, most people watching this channel are social Democrats.
They want, you know, everybody to have their base needs met.
They want justice.
They want equity, equality, these things, but they also still support capitalism to
some degree.
So you're a social Democrat.
I will also say that on this channel, the reason you have a wider spectrum of political
beliefs is not the left right thing, which is what we focus on in America.
It's that up down axis.
Almost everybody who watches this channel is on the lower axis.
They're on the downside.
They're anti-authoritarian.
Almost everybody.
If you watch this channel and the ideas of community networking, of getting out there
and leading yourself, if these things appeal to you, you're on the lower side.
You're anti-authoritarian.
Now, how far down, that varies.
But there are very, very, very few people who are on the authoritarian side who watch
this channel.
So maybe there's a bridge that can be built there.
But with Harris on the campaign, it makes it pretty hard.
So there's the general idea.
I know that for a lot of Democrats, it was maybe painful to hear Mexie describe it the
way she did.
But that's how radical leftists view the Democratic Party.
They're not really allies.
They're less harmful.
So you have to establish the bus that everybody's going to get on as defeating Trump.
That's it.
Nothing else.
You're not going to be able to appeal to them with Biden and Harris's policies because they
don't align.
They're contrary in a lot of ways.
Farm reduction is the only way to get that bridge built.
Now I would also point out that by the Democratic Party strategy, they want people like Mexie
out there talking bad about Biden and Harris because of the more radical leftists who are
opposed to Biden and Harris, the more comfortable the moderates feel.
Is it going to work?
We'll find out.
I would say no.
I would.
It might work this time because there are a lot of people who are disaffected by Trump.
Normally I think this is a bad strategy, not just on an ethical and moral level.
I don't think it would work.
This election might be different simply because Trump is such a failure.
I mean, there's no other way to put it.
He is so bad that all the Democratic Party may have to do is remove the idea that they're
socialists, and they may pick up a whole lot of moderate votes.
And the reality is they're not socialists.
Who better to tell them that they're not socialists than actual socialists?
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}