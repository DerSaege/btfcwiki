---
title: Let's talk about an update on Florida's teachers in Martin County....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6nHIdSbG7rE) |
| Published | 2020/08/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Politicians' talking points don't alter reality, regardless of phrasing or metaphors used.
- Governor DeSantis referenced Martin County's school teachers, drawing a controversial comparison.
- Teachers lack training, funding, and logistical support to provide in-person instruction effectively.
- The Martin County Public School Teachers Union President criticized the situation as a logistical nightmare.
- Despite motivational speeches, no practical backup was provided, leading to student and employee quarantines.
- Many politicians prioritize reopening schools for economic reasons, not genuine concern for education.
- The focus is on providing childcare so parents can return to work.
- Beau suggests that economic assistance is necessary for parents to stay home with their kids during the pandemic.
- Countries that shut down effectively are faring better economically and in terms of public health.
- Cute speeches from politicians do not change the harsh reality faced by many, as evidenced by the 170,000 lives lost in the country.

### Quotes

- "Politicians' talking points don't alter reality."
- "Teachers lack training, funding, and logistical support."
- "Many politicians prioritize reopening schools for economic reasons."
- "Cute speeches don't change reality."
- "Countries that shut down effectively are doing much better."

### Oneliner

Politicians' talking points fail to change reality for teachers lacking support, while prioritizing school reopenings for economic gains amid a pandemic.

### Audience

Politically aware citizens

### On-the-ground actions from transcript

- Contact Senators and Representatives for a plan providing economic assistance to families (suggested)
- Advocate for properly funded schools and logistical support for teachers (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the impact of political decisions on education and public health during the pandemic.

### Tags

#Education #Politics #Pandemic #EconomicAssistance #CommunitySupport


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about logistics and talking points.
And how a politician's talking points don't actually change reality.
Doesn't matter how well they phrase it, what cute little metaphor or analogy they use,
doesn't change reality.
It is what it is.
To talk about this, we are going to discuss something we talked about last week, provide
a little update on it.
If you don't know, Governor DeSantis gave a little speech referencing Martin County
and their school teachers.
And in it, there was a comparison made that let's just say I took issue with.
He said, just as the Sills surmounted obstacles to bring OBL to justice, so too would the
Martin County school system find a way to provide parents with a meaningful choice of
in-person instruction or continued distance learning.
All in, all the time.
Man that sounds good.
I don't necessarily think it's fair to expect our teachers to improvise on the level that
Sills do.
Not because they don't have the dedication, but because they don't have the training.
They don't have the funding.
They don't have the logistical support or the planning.
It's not what they train to do.
They train to be teachers.
And I went through this before.
The Martin County Public School Teachers Union President said, described the situation as
a logistical nightmare beyond silly and poor planning.
The Governor's fine words and motivational speech urging people half a league onward
changed nothing.
There was no backup provided and the result of that is 292 students quarantined along
with another 16 employees.
A large portion of American politicians have taken the strategy of trying to talk their
way through this.
Just keep people moving, keep the economy going because that's what this is about.
This is why those in political office keep pushing to reopen the schools.
It's not really because they care about a student's education.
If they did, the schools would be properly funded.
Has nothing to do with that.
It has to do with providing parents a babysitter so parents can go back to work.
That's what they care about.
At the end of the day, it's self-destructing because the longer this drags on, the more
we reopen and close, the worse the economy is going to get.
Perhaps instead of making cute speeches, the Governor could get on the horn with Senators
and Representatives.
You know, those people who are in theory supposed to be representing the people of Florida in
D.C. and suggest that we get a plan that provides people the economic assistance they need so
they can stay home with their kids.
Because that's what it's going to take.
Otherwise, this is going to continue to drag on.
There's a reason those countries that shut down in unison and stayed shut down and shut
down deeper, they're already kind of out of it.
They're doing much better.
Cute speeches don't change reality and there's 170,000 people in this country who could
attest to that, but they can't anymore.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}