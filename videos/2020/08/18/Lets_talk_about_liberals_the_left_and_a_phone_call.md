---
title: Let's talk about liberals, the left, and a phone call....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NnVQ7UKNr1o) |
| Published | 2020/08/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the differences between liberals, leftists, social democrats, socialists, communists, Marxists, and democratic socialists in the American political landscape.
- Leftists are those who want to fundamentally change the oppressive system, not just make minor adjustments like liberals.
- Social Democrats fight for more social programs within the existing system.
- Socialists advocate for a system where workers control their workplaces, not under capitalist exploitation.
- Communists envision a classless, stateless society where needs are met, not profits generated.
- Marxists work towards socialism and use Marxist economic analysis to understand the world.
- Democratic socialists vary in mainstream perception pushing for social democratic policies or socialist transformation through voting.
- Liberals believe in reforming capitalism with minor tweaks, while leftists see the entire system as corrupt and exploitative.
- Leftists do not support capitalism as they view it as perpetuating inequalities.
- The importance of leftist ideas lies in challenging the status quo and advocating for a more equitable society.

### Quotes

- "Leftists know that the whole system is corrupt. The contradictions of capitalism will always erode any of the good tweaks that we try to make under this broader system."
- "What's scary is letting the status quo continue."
- "We have the infrastructure. We have the technology. We have the resources. We have the money to make sure that everyone is taken care of right now. And we're just not doing it?"

### Oneliner

Beau breaks down the distinctions between liberals, leftists, socialists, and more in American politics, advocating for systemic change over minor adjustments.

### Audience

Political enthusiasts

### On-the-ground actions from transcript

- Join organizations advocating for systemic change (implied)
- Study Marxist economic analysis to understand the economy better (implied)
- Advocate for social programs to curb neoliberal excesses (exemplified)

### Whats missing in summary

In-depth examples and nuances of leftist political ideologies and how they differ from mainstream liberal perspectives.

### Tags

#Politics #Leftists #SocialChange #SystemicTransformation #Ideologies


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a question I've been getting a lot ever since I did that
video about the Biden-Harris campaign.
In that video I drew a distinction between two groups that are part of the Democratic
Party and I've had a lot of people ask me about it, ask me to clarify it, ask me to
explain the difference.
It's a complicated subject.
So rather than make it an academic thing that is something we need a PhD for, I'm just going
to give you examples, you know, and we're going to talk about who's a liberal and who's
a leftist on the American political stage.
So starting off with the leftists, we have...
Beau, it's 3am.
Hey Mexie, you're a radical leftist.
Is Biden your people?
Is he a leftist?
What?
Like American left or everyone else's left?
Your left.
My God, no.
He's barely even a Democrat.
You might as well be a Republican with his record.
Okay, what about Harris?
Also no.
How about Pelosi?
Definitely not.
Nope.
Nope.
No.
No.
No.
Definitely not.
No.
I'm not going to tell you, no.
There's no.
No.
Are you telling me we don't have any leftists, any radical leftists?
What about AOC and Bernie?
They're social Democrats that call themselves democratic socialists, so still no.
Okay.
So what is a leftist?
Well, not to gatekeep here, but a leftist is generally someone who understands that
we cannot work within this unjust system dominated by elites, by the bourgeoisie.
A leftist is someone who wants to change the entire system that oppresses us, not just
make some minor tweaks to it or try to put a happy face on it like the liberals or Democrats
try to do.
What's a social Democrat?
Well, social Democrats try to fight for more social programs, to give people greater equality
of opportunity under this ruthless, ruthless system, to try and fight some of the excesses
of neoliberal economics.
You can think about the Nordic model, so Norway, Denmark, that kind of thing, where people
have healthcare, they have education, and just generally more of the things that you
need to survive, more so than under ruthless free market capitalism anyway.
So I don't know.
If we're thinking about a continuum, then they're definitely more to the left than Democrats,
but I don't know if they're really leftists.
Okay, how about a socialist?
Now, a socialist knows that social democracy is never enough.
It will always be rolled back, and there's frankly very little chance that we're going
to get to a radically transformed society through bourgeois elections where both major
parties represent the elites and not the people.
So a socialist wants an entirely different system, one based not around profit generation
or endless growth, but a system where workers are in control of their own workplaces.
They're not being exploited for their labour to make people like Jeff Bezos trillionaires.
Well, then what's a communist?
Well, communism is actually a stateless, classless society.
So a communist is someone who is fighting for that world, fighting for our economy to
be democratized, fighting for everyone to be guaranteed the necessities of their existence,
like housing, education, healthcare, food, community, and we work together to produce
things based around need, not around profit generation, not around making people mega-rich.
You know, it's a world where people are free individuals, not shackled by wage servitude
to a capitalist class that owns everything and is keeping it from us, frankly.
All right, what about a Marxist?
Well, a Marxist is basically somebody fighting for that world, fighting to move through the
stages of socialism to full communism.
Or a Marxist is someone who uses Marxist economic analysis to understand the world, because
Marxist capital, well, let me tell you, a gift, truly.
It's, you know, it's a bit of a long read, but it's very, it's indispensable for understanding
the economy and the way things are in the world today.
Okay, what about a democratic socialist as opposed to a social democrat?
See, this is a bit of a confusing term, because a lot of people who call themselves democratic
socialists are really pushing for social democratic policies.
So, I would say in the mainstream, in the media, people like AOC, they're really pushing
for social democratic policies.
They're pushing for greater social programs to curb some of the worst excesses of neoliberalism.
But, there are people who do call themselves democratic socialists, and what they mean
by that is that they're down for a socialist transformation of society, but they think
that we can get there by just voting for it at the ballot box instead of more militant
forms of direct action.
Okay, clear as mud.
What's the difference between a liberal and a leftist?
Well, liberals think that we can put a happy face on capitalism.
They think the status quo is generally fine.
They're just looking for a return to normalcy, a return to everyone just being exploited
in silence and not really knowing or making a fuss about it, right?
They don't see much of a problem with neoliberalism or neoliberal globalization or exploitation.
They think that if we can just make a few small tweaks, a few small tweaks, that's
all it'll take, and then we'll be good, right?
But leftists, leftists know that the whole system is corrupt.
The whole system is the problem.
The contradictions of capitalism will always erode any of the good tweaks that we try to
make under this broader system, and it'll eventually just keep funneling profits into
fewer and fewer hands.
And then those people, those few people who have amassed all of this wealth, well, they're
going to have an undue influence over our democratic processes and all of our institutions.
And so unsurprisingly, they're just going to keep fashioning our political economy in
a way that serves them, not the people, not the planet.
So, I mean, I think that that's really the main difference.
Can a leftist support capitalism?
I'm going to go ahead and gatekeep here and say no.
What do you think the most important leftist idea is for people to know about?
That they're not scary.
They're, they're not even really that radical.
I mean, they just make the most sense.
I mean, what's scary is letting the status quo continue.
What's scary is sitting back and feeling helpless watching both major political parties
just continue to let people drown in poverty, to be crushed under unpayable debts, to not
be able to access adequate healthcare, etc.
I mean, there are millions of people who stand to become homeless very soon because we have
an economic system that sees the necessities of life as commodities and not social rights.
And who benefits?
Who benefits from all of that misery of the people?
It's the capitalists.
It's always the capitalists.
And with automation, with outsourcing, with pandemics now putting people out of work,
it just doesn't make sense to have a class of owners and a class of laborers underneath
them who are scrambling and fighting each other for scraps.
We have the infrastructure.
We have the technology.
We have the resources.
We have the money to make sure that everyone is taken care of right now.
And we're just not doing it?
Because that would mean that some of the most richest people, the richest, richest people
at the very top might become ever so slightly less rich.
So to me, that's absolutely unbelievable.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}