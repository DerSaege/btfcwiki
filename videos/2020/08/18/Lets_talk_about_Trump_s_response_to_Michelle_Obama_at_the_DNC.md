---
title: Let's talk about Trump's response to Michelle Obama at the DNC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZkOLAtehuFI) |
| Published | 2020/08/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump tweeted multiple times in response to former First Lady Michelle Obama's criticism during the Democratic National Convention.
- Trump referred to himself in the third person in his tweets, claiming he wouldn't be in the White House without Obama.
- Beau argues that Trump's presidency is a result of gathering uneducated, racist white supporters and stoking fear among the population.
- Trump focused on attacking the Obama administration instead of addressing current issues effectively.
- Beau criticizes Trump for prioritizing poll numbers over the lives lost during the current health crisis.
- Trump accused the Obama-Biden administration of being corrupt, including baseless claims of spying on his campaign.
- Beau points out Trump's lack of understanding of treason, as he misuses the term in his tweets.
- Trump's deflection towards a former government employee, Miles Taylor, showcases his ignorance and lack of seriousness about national security.
- Beau contrasts the divisiveness under the Obama administration with the current level of hatred and anger in the country under Trump.
- Beau concludes by criticizing Trump's leadership, stating that he thrives on division and preys on the fears of gullible individuals.

### Quotes

- "He's a failure. He's low-performing. That's the reason he's lashing out like a Halo 3 player."
- "He's not going to be able to protect you from whatever it is he tells you to be afraid of."
- "If you fall for it, you are gullible."
- "His whole goal is to gather a group of low-information, uneducated voters and tell them to fear everything."
- "He doesn't take national security seriously, obviously."

### Oneliner

Beau criticizes Trump's divisive tactics and lack of leadership, pointing out his reliance on fear-mongering and failure to prioritize national security over personal vendettas.

### Audience

Voters, concerned citizens

### On-the-ground actions from transcript

- Challenge fear-mongering narratives in your community (implied)
- Prioritize understanding issues over falling for manipulative tactics (implied)
- Educate others on the importance of national security (implied)

### Whats missing in summary

In-depth analysis and additional context on the impact of divisive leadership and fear-based tactics on society.

### Tags

#DonaldTrump #MichelleObama #PoliticalCritique #Leadership #FearMongering


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about President Donald J. Trump's response to the Democratic
National Convention.
Specifically, his response to one person who spoke who certainly didn't bother him.
Last night, if you don't know, former First Lady Michelle Obama kind of took center stage.
She in summation said the President was in over his head and that it is what it is.
As soon as she said it, we all knew we would be treated to the President tweeting this
morning lashing out.
And that happened a couple of times.
In a short period of time.
His advisors probably told him it was a bad idea to address her directly.
So we have tweets like this.
Somebody please explain to Michelle Obama that Donald J. Trump, you know, nothing says
you're secure and not narcissistic like referring to yourself in the third person.
That Donald J. Trump would not be here in the beautiful White House if it weren't for
the job done by your husband, Barack Obama.
Because she needed that clarified.
Okay now, President Trump is in the White House because he gathered a coalition of low
information uneducated voters and told them to be afraid.
He played on fear.
That's why he's there.
Make no mistake about it.
He wasn't running against Obama.
That wasn't happening.
Obama fulfilled two terms.
He apparently understood something the current President doesn't and that that's all you
can do.
That's how he wound up there.
It wasn't because of the Obama administration.
That really was because he gathered a group of uneducated racist white supporters.
That's what happened.
And then continuing to go after the Obama administration, please everybody remember
he's not running against President Obama.
He takes them to task for their response to a public health issue, you know, because Trump's
facing one.
And he said that their response was considered to be weak and pathetic.
Check out the polling.
It's really bad.
The big difference is that they got a free pass from the corrupt fake news media.
No, the big difference is about 160,000 people.
That's the big difference.
The fact that the current President of the United States is looking at poll numbers rather
than the number lost should tell you a whole lot.
He doesn't care about you.
He never did.
He doesn't care about your well-being, your job, or anything else.
He only cares about how he can use that to serve himself.
Sadly, the President is so incompetent he doesn't realize that his poll numbers are
a reflection of the 160,000 extra people.
The next one.
The Obama-Biden, one word, administration was the most corrupt in history, including
the fact that they got caught spying on my campaign.
All capital letters so you know he's not upset and bothered.
The biggest political scandal in the history of our country.
It's called treason and more.
Thank you for your kind words, Michelle, because I'm totally not angry and venting because
I can't do anything about it.
No, it's not called treason.
That's not treason.
Once again, the President of the United States is showing that he has never read the Constitution
or doesn't understand it.
Article three is really clear on this.
Treason is levying war against the United States, not hurting the current President's
feelings.
Even if all of the allegations that Trump was making against the Obama administration
were true, it's still not treason.
And then, perhaps the weirdest deflection I have ever seen in my life.
Many thousands of people work for our government.
With that said, a former disgruntled employee, all capital letters, named Miles Taylor, who
I do not know, never heard of him, said he left and is on the open arms fake news circuit,
whatever that means, said to be a real stiff, they will take anyone against us.
I'm not sure that admitting you don't know the Department of Homeland Security's Chief
of Staff is quite the flex you think it is, Mr. President.
It kind of lends credibility to the idea that you don't take national security seriously.
This is definitely somebody you should have met once or twice.
I've never met him, but you know, I'm not President.
I know some people who have, and yeah, I guess from their depiction of him, he is kind of
stiff.
He's very calm and very bland, which is kind of his entire job as Chief of Staff, because
he has to get a whole bunch of different personalities to work well together.
Something you've failed to do, Mr. President.
People forget how divided our country was under Obama-Biden.
The anger and hatred were unbelievable.
I'm not joking.
He tweeted this.
They shouldn't be lecturing us.
I'm here as your President.
Man.
I don't remember constant demonstrations in the street or quite as many cities being on
fire at the same time, the same number of people being marginalized.
I don't remember Democrats flocking to help Republicans get elected.
I don't???I do remember the President, President Obama, being reelected.
Yeah, there were people that had issues with him.
With the exception of racists, they were over his policies.
I had issues with some of his policies.
The level of hatred and divisiveness, nowhere near the same.
Not even close, not even the same league.
The President, the current President, is in the Oval Office because he divided America.
That was his strategy.
It's still his strategy.
His whole goal is to gather a group of low-information, uneducated voters and tell them to fear everything,
be afraid of the brown person, the leftist, anybody that he thinks that they don't understand.
That's what he's doing.
If you're one of his target audience, please take a moment and try to define the words
you're afraid of.
If you don't know what they are, it's probably because he's playing on your ignorance.
That's his whole thing.
He's trying to con gullible people.
If you fall for it, you are gullible.
He's not a good leader.
He can't handle a crisis.
He can't even handle the former First Lady teasing him.
He doesn't take national security seriously, obviously.
He's not going to be able to protect you from whatever it is he tells you to be afraid of.
He's a failure.
He's low-performing.
That's the reason he's lashing out like a Halo 3 player.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}