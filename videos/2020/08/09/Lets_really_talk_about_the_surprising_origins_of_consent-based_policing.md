---
title: Let's really talk about the surprising origins of consent-based policing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=io_0t3hpz00) |
| Published | 2020/08/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The origins of consent-based policing in the United States date back to the Declaration of Independence, where the government's power was meant to derive from the consent of the governed.
- The core principles of consent-based policing include the idea that the police are part of the public and effective policing is measured by the lack of crime, not the number of arrests.
- Both conservative and liberal ideologies can find alignment in consent-based policing, which aims to break up the government's monopoly on violence and have law enforcement become a part of the community.
- Implementing consent-based policing requires a shift in laws to better represent the will of the people, allowing for a focus on prevention through community engagement rather than punishment.
- Beau outlines nine standing orders for consent-based policing, including prioritizing crime prevention, gaining public approval and cooperation, offering impartial service, and using force only as a last resort after persuasion and warnings fail.
- Beau references the historical basis of these principles, dating back to Robert Peel's standing orders for British cops in 1829, which emphasized community policing and minimal force usage.
- He challenges the notion that the presence of guns in the U.S. should prevent the adoption of consent-based policing, pointing to successful models in countries like the UK, Canada, and Australia with similar histories of armed populations.

### Quotes

- "Consent-based policing is as American as apple pie."
- "The government derives its power from the consent of the governed. It is supposed to display that just power with consent."
- "The public are the police, the police are the public."
- "Efficiency is the absence of crime, not a lot of arrests."
- "Moving in the direction of the Declaration of Independence is probably more American than moving in the direction of totalitarian governments."

### Oneliner

The origins of consent-based policing in the U.S. date back to the Declaration of Independence, with core principles focusing on community engagement, prevention over punishment, and public cooperation.

### Audience

Law enforcement agencies

### On-the-ground actions from transcript

- Advocate for a shift towards consent-based policing by engaging with local law enforcement agencies and policymakers (implied).
- Support initiatives that prioritize community engagement, prevention strategies, and public cooperation in policing practices (implied).

### Whats missing in summary

Historical context and reasoning behind the development of consent-based policing. 

### Tags

#ConsentBasedPolicing #CommunityEngagement #PreventionOverPunishment #LawEnforcement #DeclarationOfIndependence


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to do a video request.
Somebody asked me to talk about a particular topic and it's a pretty big discussion in
the United States right now and they even sent me links for research and we're going
to talk about what's in those links but that's going to be in the second half because I don't
know that that's the best way to frame it because this topic, it's widespread in the
US and when people discuss it, they act like it's something new.
They act like it's this concept that's going to have to be introduced to the United States
rather than the fact that it's an idea, a principle that has existed in the United States
since the very, very beginning.
And I mean that literally.
So today we're going to talk about the surprising origins in the history of consent-based policing.
You know, that's that new liberal thing where they want to send out social workers and stuff
like that, right?
Yeah, you're all about to be surprised.
When I say the idea of consent-based policing has been here since the very beginning, I
mean that very literally.
Very literally.
I'm going to read a quote and it's a quote that every American has probably heard a thousand
times but I'm going to read the next sentence and then everything might come into focus
a little bit in this discussion.
We hold these truths to be self-evident, that all men are created equal, that they are endowed
by their creator with certain unalienable rights, that among them are life, liberty,
and the pursuit of happiness.
Everybody knows this quote in the U.S.
Everybody's heard it over and over again.
It's from the Declaration of Independence.
The very next line, that to secure these rights, governments are instituted among men deriving
their just powers from the consent of the governed.
From the very beginning, from the Declaration of Independence, this is how it was supposed
to be done.
The government derives its power from the consent of the governed.
It is supposed to display that just power with consent.
It's literally written down.
This isn't something new.
Consent-based policing is as American as apple pie.
Okay, so what are the two main ideas, the two main principles of consent-based policing?
The first is that the police are the public and the public are the police.
That's one of them and that's an incredibly American conservative idea and we'll get to
that.
The second context is that police are effective because of a lack of crime, not because they
made a lot of arrests.
That's also incredibly American.
Okay, so to show how conservative this new liberal idea is, if you're a Republican, you
know the answers to these, if a sheriff has a problem he can't handle on his own or she
can't handle on her own, what can she do with members of the community?
Deputize them, right?
Why?
Because the police are the public.
The public are the police.
The only thing that can stop a bad guy is a good guy, right?
The public are the police, the police are the public.
Here's one every Republican worth his salt will know, a well-regulated, and that's the
whole people, right?
Because the public are the police, the police are the public.
This is not a new idea.
This is not some liberal thing.
This is as American as it gets.
This is as conservative as it gets.
Now if you are on the other side of the political spectrum, don't worry, it embodies some of
your core beliefs as well.
Engaging in consent-based policing means engaging in community-based policing, which means it
breaks up the government's monopoly on violence.
There should be no debate over this in the U.S.
This is where we need to head.
This is where we've got to go.
This is where we should have been the whole time.
It's in the Declaration of Independence.
There should be no discussion about whether we should do it.
It should be about how we're going to go about doing it.
Now one of the big criticisms of this is that, you know, cops aren't social workers and can't
send social workers out there to meet the bad man, and what are they going to do then?
If you're hearing this rhetoric or you've said something like this, you're a conservative,
okay?
Which means you know who Sheriff Andy Taylor is.
Andy Griffith.
How did he engage in most interactions with the public?
Persuasion, advice, and warnings, right?
Remember those three, persuasion, advice, and warnings.
You're going to see that material again.
But if there really was somebody from out of town, you know, a bad man that showed up
in Mayberry, he had that rack in his office.
He didn't carry because he didn't need it.
He was part of the community.
Police are the public.
The public are the police.
But he had that rack in his office, and that was some pretty modern stuff for the time.
Yeah, I mean, telling Barney to get his bullet, that's still part of it.
It's an option, but it's the very, very, very last option.
And that didn't happen very often.
So what's it look like if consent-based policing was to be enacted in the United States?
What would it look like?
The first thing that would have to happen, some of the laws would have to change.
Because if you're going to engage in consent-based policing, you need consent of the governed,
which means the laws have to reflect the will of the people, which means a big one for a
lot of people would be, let's say, hypothetically speaking, the overwhelming majority of the
population says a certain plant is okay.
You can't make that plant illegal because you have to go off of the will of the people.
Again, an incredibly American idea.
The government is made up of we the people.
If that was done, consent-based policing could happen very, very easily.
It wouldn't be a big deal.
Your average cop out there on the street, they encourage people to get treatment.
They engage in conversations.
They use good humor.
They provide advice, persuasion, and give warnings in order to prevent crime, keep order.
That's it.
Now that's like 90%, 95% of most police interactions, but every once in a while there is that bad
man, right?
That's when cops actually get to be operators.
Right now, a lot of them think that that's what they need to do all the time.
It's not.
It's not.
It's counter to everything this country is supposed to embody.
It's supposed to be the last resort, but they might get to do it, which means they have
to actually behave like operators, which means they have to train.
They have to learn about surveillance, intelligence work.
They have to actually learn how to do their job, which means they're going to spend 40
hours a week training for months, so one day, six months after they start, they can conduct
a three-minute operation and have it go down the right way.
That subset of law enforcement, they're not paid for what they do on a daily basis.
They're paid for what they can do if they have to, because we're no longer judging law
enforcement effectiveness by the amount of arrests they have.
We're judging it by the lack of crime that we have, so it's a lot of training, and the
public, they never see these guys.
They never see these cops, only in very, very rare circumstances.
The general idea of consent-based policing could be summed up as law enforcement, police,
they become a part of the community rather than an oppressor of it.
Okay, so we're going to take a quick break here, and we're going to come back into yet
another surprising history and a little bit of an origin story for where this came from,
and we're going to go through kind of the nine standing orders of consent-based policing.
Okay.
All right, welcome back.
So we're going to start with the nine standing orders, and this is how I would suggest it
be done.
The first one would be to recognize that the job of police is to prevent crime, prevent
crime and disorder as an alternative to using military force and punishment.
So you want to prevent crime rather than punish the crime.
You want to stop it before it starts.
Why?
Because people have unalienable rights, and you don't want to infringe upon those unless
you absolutely have to.
So you want to stop it before it starts.
The second is that power is dependent on public approval and public cooperation, the consent
of the governed.
The third is that respect for law enforcement is heavily weighed by how well they can secure
willing cooperation, because governments are instituted among men, and by men in this case
we're talking about humanity as a whole.
Number four, cooperation of the public diminishes proportionately with the amount of force required.
Just powers.
The more force you use, the less the public's going to be willing to cooperate with you,
because you're going to be seen as an oppressor rather than a part of the community.
You're going to be outside of it rather than a part of it.
You don't want that.
So the more force you use, the less cooperation you have.
The more cooperation you have, the less force you have to use, because you are using your
powers justly.
Number five, offer impartial service regardless of class or race or anything along those lines.
Be courteous and have good humor.
Why?
Because all men are created equal.
Everybody deserves equal protection under the law.
Number six, force is only used after persuasion, advice, and warnings fail, and only to the
minimum degree, because people are entitled to life and liberty.
It's a right.
Police are the public, public are the police.
Number seven, because these are duties that fall to everybody.
It's just the cop is getting paid to make that their full-time vocation, to take those
duties more seriously.
But these are duties that every citizen should play a part in.
Number eight, cops aren't judges.
Innocent until proven guilty.
Due process, all that stuff enshrined in the Constitution.
Number nine, efficiency is the absence of crime, not a lot of arrests.
I would suggest that that one is self-evident.
So those are the nine standing orders if you wanted to enact something like that.
Now those aren't my standing orders.
That's just me paraphrasing Robert Pill.
Robert Pill is the reason that British cops are called Bobbies.
These were his standing orders in 1829.
This isn't new.
When we talk about modern policing, we're talking about the 1800s.
This isn't new.
This isn't something that has to be introduced to the United States.
We're just that far behind the times.
We're that obsolete.
We're going the wrong way.
And I know somebody's already saying, well, they have guns, you know, here in the U.S.,
but they don't have them over in the U.K.
The British got the first real gun regulations were in 1903, and up until 1988, they had
some that were pretty comparable to what we have here in the U.S.
Now granted, Americans have a lot more because Cowboys and John Wayne or something, I don't
know.
The point is, this has worked since 1829, so you had 150 years or more with an armed
populace, and it worked.
It works in the U.K., it works in Canada, it works in New Zealand, it works in Australia,
it works all over the world, everywhere that has a modern police department.
But we don't want to enact it here because we're married to that idea that the gun makes
the man.
When that idea becomes prominent in law enforcement, it becomes prominent in the populace because
the police are the public, the public are the police.
It's going to be a reflection of that.
This is something we could do, it's something we should do.
I would strongly suggest that moving in the direction of the Declaration of Independence
is probably more American than moving in the direction of totalitarian governments the
world over, which is how a lot of our law enforcement is currently behaving.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}