---
title: Let's talk about an update on Paulding County schools and how you can help....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EM3SdPI39ew) |
| Published | 2020/08/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Paulding County Schools in Georgia reopened in person, with students documenting the reopening and posting online about non-compliance with safety measures.
- The school initially suspended the students but rescinded the suspensions after public involvement.
- Six students and three staff members have tested positive for COVID-19 after being in the school, suggesting that the numbers will continue to rise.
- During a school board meeting, suggestions were made to move students around every 14 minutes as a safety measure, which may not effectively prevent transmission.
- Beau recommends recovered individuals to donate plasma, as it contains antibodies that can potentially help in treatment.
- Leadership at federal, state, and county levels is lacking, and Beau urges people to listen to medical experts and take action themselves.

### Quotes

- "We're gonna have to listen to the medical experts. We're gonna have to act, because they're not going to."
- "It's not really about putting kids at risk just to reopen the economy."
- "We need to do it ourselves."

### Oneliner

Paulding County Schools face COVID-19 challenges, urging community action and plasma donation for treatment in the absence of effective leadership.

### Audience

Community members, COVID-19 survivors

### On-the-ground actions from transcript

- Donate plasma if you have recovered from COVID-19 (suggested)
- Listen to medical experts and take necessary actions (implied)

### Whats missing in summary

The full transcript provides additional details on the lack of leadership at different government levels and the importance of individuals taking responsibility for their actions during the pandemic.

### Tags

#COVID-19 #CommunityAction #PlasmaDonation #SchoolSafety #LeadershipVacuum


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So we're gonna do a little update today
on Paulding County Schools,
the schools there in Paulding County, Georgia.
And then we're gonna talk about
what you might be able to do to help
in the nationwide issue that we're facing right now.
Okay, so to catch you up,
if you don't know the saga in Paulding County,
the schools reopened in person.
Some students there documented the reopening.
Photos, kept tallies of the numbers of students
who were wearing masks, stuff like that.
They put it online.
The documentation that was produced
certainly seemed to show that the school was,
let's just say, less than compliant
with what we would consider to be best practices,
given everything that's going on right now.
In response, the school suspended the students.
After the school board found out what social media was,
and there was a little bit of public involvement,
they rescinded the suspensions.
When we did that video initially,
we were like, hey, this is what's gonna happen.
Not because we have a crystal ball,
but because we actually listened to the experts.
Okay, this was not that long ago,
and since then, Channel 2 investigative reporter,
Nicole Carr, got a copy of a letter,
and six students and three staff members
who were in the school last week have tested positive.
These numbers will go up.
They're already going to go up right now.
They're gonna go up a lot if practices don't change.
To add to this, it's been reported
that during a school board discussion,
the board was being informed about
how public health would like to be notified,
and basically, they were told that if students
were within six feet of each other for 15 minutes,
then they would need to know
if one of those students came up positive.
One of the suggestions reportedly floated
by the school board was to have a public health
report reportedly floated by the school board
was to move students around every 14 minutes.
I don't know if they don't understand
that it can be transmitted in less than 14 minutes,
or they were actively trying
to subvert public health officials,
or maybe there's a third option.
I don't know what it is.
Either way, I would suggest playing musical chairs
in the middle of this is probably a really bad idea,
because if you have a student that's positive,
and you move them to another desk,
another student will go sit in their desk.
That's probably a bad idea.
Again, lame in looking at this.
I would also suggest that moving students
around every 14 minutes does not lend well
to an educational environment.
There's probably gonna be some time lost,
and the whole idea is to get back to education, right?
That's why we have to do this.
It's why we have to take all these risks.
It's not really about putting kids at risk
just to reopen the economy.
That would be horrible.
That would be something we would never do in this country,
because we're not monsters, right?
Okay, again, that's the way it appeared to me.
I've got clips of footage on my Twitter
if you wanna look at it.
You can determine for yourself.
Okay, so what can you do to help?
If you've had this, and you've recovered,
you've fully recovered, this is anecdotal,
okay, this is me talking to medical professionals.
Go give plasma, according to 100%
of the people I've talked to.
Now, granted, it's not a study.
This is anecdotal.
This stuff is magic.
It works when other stuff won't,
but there's not a lot of it.
So if you were positive and you recovered,
go give plasma, because they take it,
they use the antibodies in it,
and they create this trend of giving plasma.
Antibodies in it, and they create this treatment,
and it is apparently pretty effective,
again, anecdotally.
I'll try to find a list of places that you can donate,
because I've seen in the comments section,
there are quite a few people who've had this.
It can save a life.
So if you have the ability, if you got the means,
you got the responsibility, right?
Okay, so there's your little Sunday update.
At the end of the day here, we need to remember,
we're not getting leadership from the federal level.
In a lot of states, we're not getting it
from the state level.
In some places, we're not getting it at the county level.
We're gonna have to do it ourselves.
We're gonna have to listen to the medical experts.
We're gonna have to act, because they're not going to.
We have ineffective leadership right now.
Anyway, it's just a thought.
Y'all have a good day. Thank you.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}