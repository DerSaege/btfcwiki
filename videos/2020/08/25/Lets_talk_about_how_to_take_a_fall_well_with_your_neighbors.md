---
title: Let's talk about how to take a fall well with your neighbors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FFTBc1hgGu0) |
| Published | 2020/08/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Emphasizes the importance of speaking the same "language" as the audience you are trying to reach to effectively communicate your message.
- Talks about framing things in a way that appeals to the target audience, rather than trying to make points that appeal to oneself.
- Mentions how individuals often fall into hypocrisy, especially in the conservative movement, and how pointing out this hypocrisy can inadvertently strengthen their position.
- Gives an example of a well-known religious figure who falls from grace and manipulates the situation by framing it as a moral lesson about temptation and money.
- Argues that focusing on hypocrisy is not the most effective approach, as the real issue lies in marginalizing others and failing to love and accept them.
- Encourages using the narrative of loving one's neighbor as a more impactful way to address transgressions and create positive change.

### Quotes

- "Don't believe me? Let's say hypothetically there was a well-known religious pundit who got caught up in a lifestyle that was just outside of the norm."
- "The failure of most people like this is not the hypocrisy. It's that they didn't love their neighbor as themselves."
- "And that's the real problem. Everywhere."

### Oneliner

Beau stresses the importance of speaking the audience's "language," avoiding hypocrisy call-outs, and focusing on loving one's neighbor to address transgressions effectively.

### Audience

Communicators and activists

### On-the-ground actions from transcript

- Reframe messages to resonate with the audience (exemplified)
- Focus on promoting love and acceptance within communities (exemplified)

### Whats missing in summary

The full transcript provides a deeper exploration of the pitfalls of focusing solely on hypocrisy rather than addressing the root issue of marginalization and lack of love and acceptance.

### Tags

#Communication #AudienceEngagement #CommunityBuilding #Hypocrisy #LoveThyNeighbor


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about speaking the language of the people you're
trying to reach.
And framing things
in a way that appeals to them because a lot of times when a golden opportunity
presents itself
we don't do that.
We don't do that. We
try to make our points
in a way that would appeal to us.
We're not trying to appeal to us.
If those points would work
they would already think like us.
And we're going to do all of this and talk about falling well.
Every once in a while,
pretty regularly,
a person from the
conservative movement
falls.
Something happens.
The family values party does not live up to its name.
This happens pretty often.
When it does,
especially if that person is uh...
has made their
points
in a religious framing,
when that happens
we often point to the hypocrisy.
Because that's what uh...
that's what shook a lot of
us
loose.
We see the hypocrisy and we're like, hey,
that's wrong.
But see,
the thing is
those people,
the people who are powerful
within that community
who have trained those who follow them
to kick down,
they already have a defense for that.
And that when you point out
that hypocrisy
you're helping them build their brand.
You're helping them
push the conversation
where they want it to be.
Don't believe me?
Let's say hypothetically
there was a well-known
religious pundit
who got caught up in
a lifestyle that
was just outside of the norm.
Not to say that outside of the norm is bad,
it's just outside of
the norm, what people
typically do.
When news breaks,
the hypocrisy charge gets leveled.
And he already won.
He won.
Because yeah, he's got to lay low for a little while,
but then he comes back.
He says, you know,
who among us
hasn't fallen prey
to the practice that was warned about all those years ago
and failed to remove the plank
from their own eye
before pointing out the speck in their brother's?
I failed in that regard.
I was led astray
by the root of all evil. I was enticed
by money.
See, I had
to be around
all of those elites,
those people with money, because I was trying to get the word out.
And I was around those Hollywood liberals.
And I saw what they did.
And it corrupted me.
It corrupted me.
And it was that money.
So starting today,
I'm not going to take a salary.
I'm not going to take any money.
Instead, that money's going to go into a trust.
And it's going to be used
to
do good works.
And as somebody
who can tell you
firsthand
the temptations that money brings,
let me take that burden off of you.
Send me your money.
Put it into that trust.
And they're right back to where they started.
Because that trust is administered by their attorney.
That's not the route to go.
Because that hypocrisy,
and they've got that down.
They know how to deal with that. They know how to spend that.
They can turn that against
their cultural enemies, those they have set up
as the opposition.
And they can keep marginalizing people.
Which at the end of the day,
that's Israel transgression.
Most times when you're talking about one of these people,
that's their real transgression.
The hypocrisy,
it is what it is.
You know, there are a whole lot of people who are born into a system or society
or cultural group,
and they can't be who they are.
Reinforcing that idea
doesn't seem wise to me.
When they're found out,
all the outrage does
is remind them that,
if that's the way you are,
you better keep it a secret.
Not really healthy.
But the real transgression
is marginalizing people.
And that's the language that should be used.
The failure of most people like this
is not the hypocrisy.
It's that they didn't love their neighbor
as themselves.
Because they couldn't love themselves.
Because they couldn't be open about who they were.
That's the route to go.
Because if that's the framing,
when something like this happens,
their only way
out of it
is to
stop leading the charge to marginalize people.
Stop,
stop pushing others
to kick down
and not love their neighbor.
You just frame it as hypocrisy. They can spin that
and they'll come back
with more power, more money than they ever had before.
They failed to love their neighbor.
And that's the real problem.
Everywhere.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}