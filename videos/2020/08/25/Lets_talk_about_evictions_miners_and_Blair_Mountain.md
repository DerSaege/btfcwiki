---
title: Let's talk about evictions, miners, and Blair Mountain....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=z-qZVy0TblY) |
| Published | 2020/08/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Blair Mountain incident in West Virginia, 99 years ago, had significant impacts on American history and policy due to evictions and miners' struggle.
- Mine operators cut wages post-World War I, leading to miners organizing for better conditions like an eight-hour workday and hourly pay.
- United Mine Workers played a vital role in organizing miners against oppressive mine operators who enforced their will through gun thugs.
- A conflict on May 19, 1920, led to the arrest of gun thugs, with a cop becoming a symbol of miners' struggle for better conditions.
- The cop faced charges and later a coal transport station explosion accusation, which intensified the battle for fair treatment.
- A sheriff, backed by gun thugs and mining companies, faced off against thousands of miners heading towards Blair Mountain.
- Fighting erupted on August 25, 1920, with the miners expecting federal support but ultimately facing arrests and heavy charges.
- Despite the union's initial downfall, the miners' actions led to federal legislation and widespread popular support for labor rights.
- The term "rednecks" originated from the miners wearing red scarves and gun thugs wearing white armbands at Blair Mountain.
- The rich exploiting the economic downturn by pushing down on the little people resulted in evictions and labor struggles.

### Quotes

- "During an economic downturn, the rich folk couldn't weather the storm, so they further kicked down."
- "Rednecks have labor organizing, unions. It's in their DNA."
- "All of this happened because during an economic downturn, the rich folk couldn't weather the storm, so they further kicked down."
- "They provoked that overreaction during a security clampdown."
- "It's a really important little chapter in American history, but it often gets overlooked."

### Oneliner

Blair Mountain's miners' struggle against oppressive mine operators led to federal legislation and popular support for labor rights, showcasing the impact of grassroots organizing in history.

### Audience
History enthusiasts, labor rights advocates

### On-the-ground actions from transcript
- Visit the Mine Wars Museum in West Virginia to learn about the origins of labor organizing and unions (exemplified).
- Support grassroots labor organizations and unions in your community (implied).

### Whats missing in summary
The full transcript provides a detailed account of the Blair Mountain incident and its long-lasting effects on American history, shedding light on the importance of grassroots organizing and standing up against oppressive systems.

### Tags
#BlairMountain #LaborRights #GrassrootsOrganizing #MineWars #History


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about evictions, miners, and mountains.
Because 99 years ago today, something happened up on Blair Mountain in West Virginia.
Something that doesn't often get covered in history books, though it should.
Because it, aside from the scale of what happened, it had a whole lot of down the road impacts
on American history, policy, all kinds of things.
But it gets overlooked, in my opinion, because of the message that it sends, and some of
the uncomfortable truths that it exposes.
So let me lay the scene.
During World War I, coal was king, making money, hand over fist.
However, at the end of the war, that wasn't the case anymore.
While most industries were going through the roaring 20s, coal was losing.
They were going down.
So the mine operators had to make their money somehow, so they cut wages.
And by wages, I mean scrip.
Scrip was little notes that could be used at the company store.
You could exchange this for real money at a cut rate, of course.
Miners were facing bad conditions, so they wanted better conditions.
They were just asking for absurd stuff, like an eight-hour workday, working five days a
week, hourly pay, wild stuff, stuff that's just unimaginable, crazy entitled people,
right?
They realized that the state and federal government, they're not going to help.
They're not going to get any relief from the government.
So they start organizing on their own.
They start coming together and supporting each other.
They start unionizing.
United Mine Workers is one of the important ones.
So that kind of lays out the scene.
It should also be noted that the mines really were the government.
During this period in time, in this area, the mines were the law, and they enforced
their law through gun thugs.
These were private contractors, private security brought in to enforce the will of the mines.
They were bad, all around bad, and they operated above the law.
So May 19th, 1920, some gun thugs roll into town, and they're there to evict people, illegal
evictions.
Incidentally, not that it mattered, because the miners were powerless to stop them.
They couldn't do anything about it.
The wealthy people had their own enforcement class, those people who were willing to accept
pay and do whatever they were told.
There was one cop in the area, Sid Hatfold, who wasn't in bed with the mining companies.
He goes to arrest the gun thugs.
The gun thugs say, well, we got a warrant for your arrest.
The mayor, who happens to be with him, asks to see the warrant.
The mayor says it's bogus, at which point one of the gun thugs unholsters.
A fight follows.
How it started is disputed.
We don't exactly know, not really.
What we do know is when the smoke cleared, there were seven gun thugs on the ground,
along with three residents, one of which was the mayor.
The cop gets charged, because he was not compliant with the ruling party there, so he gets the
blame.
He is acquitted, and this kind of turns him into the face of the miner struggle, becomes
a symbol of the battle to organize, to get better conditions.
It's completely unsurprising that in July of 1921, he gets charged with taking out a
coal transport station.
By taking out, I mean boom.
Whether or not he did it, again, we'll probably never know.
There are differing accounts of that as well.
Some say, yeah, he did.
Some say, no, the mine set him up.
At the end of the day, it doesn't really matter, because he had to go to McDowell County to
talk to the judge.
McDowell County was in the pocket of the mining companies.
Hatfold expressed some concerns about going there, and they were well-founded, because
when he showed up, three gun thugs ambushed him, took him out.
Miners were outraged, obviously.
Now most people were calling for calm, waiting to see what would happen.
Of course, the gun thugs were kind of exonerated.
More outrage.
People still called for calm, with the exception of one woman, Mary Harris Jones, otherwise
known as Mother Jones, like the magazine.
Low-intensity struggle followed.
Miner stuff, miner skirmishes.
Nothing too wild yet.
But then a rumor went out, and the rumor was that some gun thugs had taken out some organizers
and that their families had been caught in the crossfire.
Outrage.
Full-blown outrage, and miners are ready to go.
Mother Jones calls for calm.
She is accused of going soft.
Four to 13,000 miners head towards Blair Mountain, take trains, raid arm stations.
They're ready to go.
The opposing side is led by a sheriff who is in the pocket of the mining companies,
and he is backed up by a bunch of gun thugs, paid for by the mining companies.
They number one to two thousand.
Seems like it should be an easy win for the miners.
Problem is, the gun thugs have machine guns and airplanes to bomb the miners for real.
It pops off on August 25th.
By August 26th, it has gotten so bad that President Harding is like, okay, this has
to stop or I'm sending in my troops and my bombers.
The miners are like, okay, we've got the federal government's attention, we'll get some relief,
we'll get some better conditions.
They start to kind of fade away.
The story goes that the sheriff wanted that battle.
He wanted to break the union.
He wanted the fight.
A rumor circulates that some gun thugs took out some labor organizers and their families.
Back up the mountain, everybody goes.
Fighting continues until September 2nd.
Thousands of people, millions of rounds expended.
You can go up to that mountain today, you will still find spent shell casings, bullets,
weapons.
Federal troops arrive.
The miners think the federal troops are going to be on their side.
They were wrong.
The government went with the money.
Almost a thousand miners get arrested.
Some are acquitted by sympathetic juries.
Some go up on some pretty serious charges.
The end of the day, what happened?
When it's all said and done, the union was broke.
Membership dropped like a rock.
And it seemed as though all was lost.
However, from a wider perspective, what happened?
The miners provoked that overreaction from the government.
In this case, the government is the mining companies.
They provoked that overreaction during a security clampdown.
Probably didn't mean to, but that's what happened.
So long-term, membership went back up.
There were Senate hearings, which led to federal legislation, which led to court battles, which
led to it going all the way to the Supreme Court, which most importantly led to widespread
popular support from the people.
It's a really important little chapter in American history, but it often gets overlooked.
Interesting side note.
Up on that mountain, the gun thugs wore white armbands, and the miners wore red scarves,
rednecks.
You can see this at the Mine Wars Museum in West Virginia.
It's one of the origins of the term.
Rednecks have labor organizing, unions.
It's in their DNA.
100 years ago, though, in 99, not much has changed.
Not much has changed, not really.
All of this happened because during an economic downturn, the rich folk couldn't weather
the storm, so they further kicked down.
They pushed down on the little people, and that led to a bunch of evictions.
Something we might want to keep in mind.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}