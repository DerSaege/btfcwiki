---
title: Let's talk about the intent of other nations and the US election....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qXStrjS1M7M) |
| Published | 2020/08/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Other countries have preferences and intents regarding the US election, influenced by US foreign and domestic policies.
- Every country on the planet has a preference for who leads the US and could attempt to influence the outcome.
- Coordination and cooperation between a candidate and another nation become a concern, indicating potential allegiance.
- Germany, France, the UK, and traditional allies have preferences; German intelligence might release compromising information on Trump.
- Russia continues to support Trump due to his favorability for them, while China surprises by backing Biden primarily for trade stability.
- China's backing of Biden indicates their focus on stable trade agreements rather than military confrontation.
- The information and propaganda from the Trump administration about China being adversarial are questioned based on China's support for Biden.
- Nations supporting Biden prioritize stable foreign policy, trade agreements, and a return to normalcy over military aggression.
- Both US presidential campaigns need monitoring to ensure they are not coordinating with foreign nations for security reasons.

### Quotes

- "Every country on the planet has a preference for who leads the US and could attempt to influence the outcome."
- "Germany, France, the UK, and traditional allies have preferences; German intelligence might release compromising information on Trump."
- "Nations supporting Biden prioritize stable foreign policy, trade agreements, and a return to normalcy over military aggression."

### Oneliner

Other countries' preferences in the US election reveal insights into their intent, with China surprising by supporting Biden for trade stability over military confrontation.

### Audience

Policymakers, Voters, Citizens

### On-the-ground actions from transcript

- Monitor both US presidential campaigns for potential coordination or cooperation with foreign nations (suggested).

### Whats missing in summary

Deeper insights into the influence of foreign preferences on US elections and the implications for US stability and security.

### Tags

#US-election #Foreign-Relations #International-Influence #Trade-Stability #Policy-Monitoring


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the preferences and intent of other nations when it comes
to the US election.
What other countries want.
Because we had those reports come out, you know, country Y supports this candidate, country
X supports this candidate, and that some were going to attempt to interfere.
Had a lot of questions about it.
You know, is this true?
Probably.
Probably.
Every country on the planet, no exceptions, every country on the planet has a preference
when it comes to who's leading the United States.
Because our foreign policy and our domestic policy influences every country on the planet.
So they all have a preference.
Some would attempt to influence the outcome if they got information by happenstance.
Some would be much more proactive in that.
None of this is a surprise, none of this is really even a big deal.
It only becomes a concern if the candidate themselves begins coordinating and cooperating
with the other nation.
Then it matters.
If one of the candidates is, say, on TV asking another country for help, from that point
forward, you have to assume that that candidate is beholden to that country.
That's a concern.
Aside from that, it doesn't matter.
Our allies, you know, France, Germany, the UK, they have a preference.
Those traditional allies, they have a preference.
I can assure you that if German intelligence got their hands on compromising information
about President Trump, they would release it, no doubt.
They're going to want Biden because they need stability.
Trump is a destabilizing force in the United States.
It's good for adversarial nations.
Which brings us to the only interesting piece of information that came out of this little
conversation, this little report.
Russia is going to continue to support Trump.
No surprise there.
He's been good for them.
They're adversarial.
We know this.
They're confrontational militarily when it comes to jockeying for position in Europe,
and their actions in Asia certainly show that.
The only surprise is that China chose Biden.
That's a surprise.
So on the surface, it may not seem that way because the tariff wars and all of that stuff,
but that's all minor economic stuff at the end of the day.
They'll rebound from that.
That's not a big deal.
If they were adversarial and confrontational militarily, they would want Trump.
Trump's vision and leadership when it comes to strategy is not good.
This is why Australia is stepping up in regards to managing that sphere of influence because
they know the US isn't doing it anymore because we don't have the leadership to.
Whether or not you think that's a good thing, that's up to you.
But it tells us something about China's intent.
If China was militarily adversarial, they would want Trump just like Russia.
They don't.
They want Biden, which tells us that China's primary concern is trade, period.
They want the United States stable, and they want a return to the status quo.
Again, whether or not you think that's a good idea, that's a good thing.
That's up for debate.
But as far as their intent, them backing Biden tells us a lot.
Most importantly, it tells us that the information, propaganda, that has been coming out of the
Trump administration about China being adversarial is false, at least to the extent that he's
trying to paint them.
They are nowhere near as adversarial as the Trump administration is making them out to
be.
They're nowhere near as adversarial as I thought they were going to be, to be honest.
Any truly adversarial nation, when we're talking about the military, they're going to be backing
Trump.
They're going to want Trump to win.
A country that wants Biden in power wants a stable foreign policy, stable trade agreements.
They want a return to normalcy.
So when you see nations like China backing Biden, you need to downgrade them in your
threat assessment because they're not looking for a military confrontation.
They're willing to sacrifice a strategic position militarily in exchange for trade.
That's the only surprise in that whole thing.
And again, I definitely believe that both campaigns need to be watched to make sure
they are not coordinating or cooperating with the foreign nations.
That's incredibly important and it doesn't matter if either one of them is your particular
candidate.
They both need to be watched.
It's incredibly important for the safety and security of the United States on a bunch of
levels.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}