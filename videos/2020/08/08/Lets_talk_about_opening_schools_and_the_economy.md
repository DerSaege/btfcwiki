---
title: Let's talk about opening schools and the economy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vzFbZDN63Kk) |
| Published | 2020/08/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Congress members are pushing the idea that sending kids back to school is vital for the economy, but it isn't resonating with Americans.
- Lower-income Americans have already suffered significantly in the economy, with many losing their jobs.
- The statement about harming the economy by keeping kids at home implies exploiting them for economic gain.
- Beau questions why the focus isn't on Congress fixing the economy rather than rushing poor kids back to school.
- He suggests that Congress should focus on raising wages to a living point and providing better support for families.
- Beau points out that the real work to help the economy should start with Congress, not poor kids.
- Sending kids back to school, even with guidelines, poses more risk compared to staying at home.
- He calls for accountability on Capitol Hill rather than blaming lower-income individuals.
- The source of economic problems lies with those in power, not the powerless.
- Beau warns that if exploitation continues, the middle class will eventually become the working poor.

### Quotes

- "Nobody cares."
- "Maybe the real work that needs to be done to help the economy is you."
- "The poor are never the source of your problem."

### Oneliner

Members of Congress push to send kids back to school for the economy, but Beau argues the focus should be on fixing the economy and not exploiting poor children.

### Audience

Congress Members

### On-the-ground actions from transcript

- Advocate for raising wages to a living point and providing better support for families (implied).

### Whats missing in summary

Beau's impassioned call for accountability and action from those in power to address economic issues rather than exploiting vulnerable populations.

### Tags

#Education #Economy #Exploitation #Accountability #Wages


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about school and the economy
and how they're linked and how they're not.
Members of Congress
are now really kind of pushing that talking point. If we don't get kids back
to school
it's going to significantly harm the economy.
They don't seem to understand
why that talking point isn't resonating with
America.
So I'm going to try to shed a little bit of light on that.
See, back in April
forty percent
of lower-income Americans and by the Fed that's forty thousand dollars per
household or less
lost their jobs.
Their economy, the working-class American,
that's
their economy's already significantly hurt.
So when you make that statement that it's going to become significantly hurt
everybody knows
the quiet part of that sentence.
It's going to be significantly hurt to the point where it matters to members of
Congress.
Nobody cares.
Nobody cares.
The end
statement when you boil this down is we need to send poor kids back to Petri
dishes
so we can get our good little worker bees back where we need them so we can
continue to exploit them and make money.
Nobody cares.
That's why the talking point isn't resonating.
I've got a wild idea.
How about those members of Congress, the ruling class, those who have the
resources to have their kids have
private tutors and
do everything via distance learning.
Why don't y'all get back to work?
Why don't y'all do your jobs?
Why don't you actually attempt to fix the economy, the real economy,
rather than just put a band-aid on it to help your stock portfolio?
You could be using this time to
try to raise wages,
get them to a living point, you know, like the minimum wage was supposed to be.
It was supposed to be a living wage,
not a bare subsistence living.
We could do that.
That might help encourage people because if you did that, then one person
could support the family.
And then you'd have somebody to stay home.
And single-parent households, well, they could afford child care.
So everything works out.
We have enough automation to do this.
This isn't a big deal.
Maybe that's where the focus should be,
rather than trying to rush poor kids
back to a petri dish of a school.
Because even with the guidelines being followed,
it's classified as more risk.
Least risk is at home,
like your kids will be.
Maybe that's what
you guys should focus on.
Maybe the real work that needs
to be done to help the economy is you.
Maybe you all should stop looking to Section 8 housing
for a scapegoat.
Do your jobs.
The poor and working class in this country already have to send their older
kids
off to war
anytime
those on Capitol Hill need a bump in their defense or energy stocks.
Perhaps
asking them to send their younger kids
into a petri dish?
Maybe that's just a bridge too far.
Maybe that's just a little too much. Maybe that's why that talking point isn't
resonating.
And if you're part
of
the
population where it's going to get bad,
welcome to the club.
And I know that it's probably scary,
but don't
look down.
Don't look to the lower income people.
They don't have any power. They don't have any influence.
Look to Capitol Hill.
Those are the people that can fix the problem.
Don't kick down.
Don't look the way they want you to look.
Look at them.
The source of your problem
will never be somebody with less power and influence than you.
All of these problems originate on Capitol Hill.
They broke the economy
so they could exploit it.
So they could exploit American workers.
Now that's kind of
come back to bite them.
Rather than fixing their mistake,
they're trying to find a quick and easy way to continue to exploit them.
Understand,
as
this progresses,
as this continues,
and this process goes along,
you, the middle class,
those who are comfortable now,
you will become the working poor.
That's how this works.
Right now, you're okay.
But if you continue to allow them to exploit the American worker,
you won't be.
You're not going to be one of the ones
who gets rich.
You're not a temporarily embarrassed millionaire.
You're going to be in the same situation as those that you're looking down on right now.
The poor are never the source of your problem.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}