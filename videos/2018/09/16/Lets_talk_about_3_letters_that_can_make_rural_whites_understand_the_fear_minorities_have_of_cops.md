---
title: Let's talk about 3 letters that can make rural whites understand the fear minorities have of cops.
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=J5DBrOBIgNM) |
| Published | 2018/09/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the distrust that minorities have for law enforcement compared to white country folk.
- Illustrates how in rural areas, law enforcement is more accountable and part of a tighter community.
- Mentions the ability to hold law enforcement accountable through the ballot box or the cartridge box.
- Points out that minority groups lack institutional power to hold unaccountable men with guns in law enforcement accountable.
- Draws parallels between the distrust felt towards law enforcement by minorities and white country folk towards agencies like ATF and BLM.
- Addresses the frequency of unjust killings and lack of accountability in law enforcement.
- Encourages taking action by getting involved in local elections to ensure accountability.
- Suggests starting at the local level by electing good police chiefs to prevent corruption in federal agencies.
- Advocates for people from different backgrounds to unite and address the common issue of unaccountable men with guns in law enforcement.
- Urges individuals to care enough to take action and work together to solve the problem.

### Quotes

- "We can hold them accountable one way or the other, the ballot box or the cartridge box."
- "It's unaccountable men with guns. We can work together and we can solve that."
- "We've got to start talking to each other. We got the same problems."

### Oneliner

Exploring the distrust towards law enforcement, Beau urges unity and accountability to address the common issue of unaccountable men with guns.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Elect good police chiefs in local elections to prevent corruption (suggested)
- Advocate for accountability in law enforcement by participating in local elections (implied)
- Start dialogues with individuals from different backgrounds to address common issues (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of the distrust towards law enforcement, suggestions for accountability through community action, and the importance of unity in addressing systemic issues.

### Tags

#LawEnforcement #Accountability #CommunityAction #Unity #MinorityDistrust


## Transcript
Howdy there internet people, it's Bo again.
So we did that video on the Dallas shooting
and I noticed in the comments section on the shares
that there's a lot of white folk don't understand
the distrust that minorities have for law enforcement.
First, I'm trying to figure out why,
like why they don't understand it.
And it's pretty simple.
If you're a country person, you live out in the country,
who's the law?
The sheriff, right?
an elected official.
His deputies,
probably went to high school with them.
They might go to your church.
You probably know where they live.
They're accountable.
You can hold them accountable.
If one starts acting a fool, you can vote him out.
And if you can't vote him out and you're getting too bad, we all got that crazy cousin
with the.30-06 and nothing to lose.
If you laughed at that,
Probably the crazy cousin.
You know, it's different.
It's a tighter community.
They're more accountable, and they tend to go more by the
spirit of the law than the letter of the law.
Some kid comes back from boot camp at 19 and wants a beer.
Nobody's going to say anything.
It's different.
And it's different because they're accountable.
We can hold them accountable one way or the other, the
ballot box or the cartridge box.
So that kind of abuse is pretty rare out in the country.
And when it happens, it gets handled.
So see, minority groups, by definition, they're a minority.
They can't swing an election.
They don't have that institutional power.
They don't have the votes.
They're a minority.
So they can't do anything about it.
They've got unaccountable men with guns running around.
A lot of these police chiefs aren't even elected.
So trying to figure out how to relate that distrust and that fear
to white country folk.
And I figured it out, three letters.
A-T-F. All of a sudden, you've got that distrust.
You've got that fear.
You've got that anger.
You probably know somebody they did dirty.
You might know somebody they killed didn't need killing.
Say you live a little bit further out west,
and ATF doesn't do it for you.
BLM, got that distrust, got that fear, got that anger.
Why?
Because they're unaccountable men with guns.
Can't do anything about it.
See, here's the thing we got to remember.
They got a Ruby Ridge or a Lavoie Fennecum happening every week.
Every week there's some unjust killing.
And they can't do anything about it.
A bunch of unaccountable men with guns.
Now here's the thing.
If you're worried about the ATF or BLM,
you're probably telling your friends,
we've got to do something about this now.
Because right now it's somebody else,
but eventually it's going to be us, right?
And that sounds like what they're saying, doesn't it?
And it's true.
But see, here's the thing.
We've got that institutional power.
We can swing an election.
And in these cities where the police chief hadn't elected,
get rid of the mayor and make it clear why.
It's that simple.
All you got to do is care.
care enough to get involved.
Now, it's all good to say, oh, I care.
But most people need some kind of self-interest
to really get motivated.
So here you go.
Those ATF agents, BLM agents, where do they start off at?
Police departments in big cities, right?
It's normally where they start.
Can you imagine if you could get rid of the corruption
before they ever even got into a federal agency.
Get a good police chief in, he'll do it.
Right?
Maybe, it's worth a shot.
See, here's the thing.
All of us, you know, it doesn't matter
if you're in the holler of some West Virginia hillside
or you're in Kansas or whatever,
you got more in common with a black guy from the inner city than you're ever going to have
in common with your representative up in D.C.
We got to start talking to each other.
We got the same problems.
It's just different agencies.
It's different agencies, but it's the same problem.
It's unaccountable men with guns.
We can work together and we can solve that.
pretty easy really or not don't do anything about it let a bunch of people
get killed doesn't matter to you right it's not you now it's what you're telling
people about the ATF and BLM it's not you now get involved we got to start
working together it's gonna get real bad anyway y'all have a good day just
something to think about.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}