---
title: 'Let''s talk about #MeToo, #HimToo, and sexual harassment and assault....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-qoW7FIWWs8) |
| Published | 2018/09/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses sexual harassment and assault in the context of the Kavanaugh nomination.
- Provides advice on avoiding such situations, including safeguarding reputation and watching signals.
- Mentions ending dates at the front door, avoiding going home with anyone if drinking, and being cautious with words.
- Talks about the importance of keeping hands to oneself to avoid sending wrong signals.
- Acknowledges the "him-too" hashtag and addresses men's concerns about false accusations.
- Indicates that false accusations of sexual harassment or assault are rare, around 2% of the time.
- Stresses mutual accountability between men and women regarding actions and situations.
- Refutes the idea that clothing choices can cause rape, asserting that rapists are the sole cause.
- Concludes with a powerful statement: "Gentlemen, there is one cause of rape, and that's rapists."

### Quotes

- "Be very careful with the signals you send."
- "False accusations of sexual harassment or sexual assault turn out to be false about 2% of the time."
- "There is one cause of rape, and that's rapists."

### Oneliner

Beau provides advice on avoiding sexual harassment and assault, stressing accountability and debunking victim-blaming myths.

### Audience

Men, Women

### On-the-ground actions from transcript

- Safeguard your reputation by being mindful of the signals you send (implied).
- End dates at the front door to prioritize quality over quantity (implied).
- Avoid taking anyone home when drinking to prevent risky situations (implied).

### Whats missing in summary

In-depth analysis of the impacts of victim-blaming and misconceptions on sexual assault prevention.

### Tags

#SexualHarassment #AssaultPrevention #Accountability #EndVictimBlaming #RapeCulture


## Transcript
Well howdy there internet people, it's Bo again.
Today we are going to talk about me too, him too,
all the sexual harassment, sexual assault stuff
that has come back up in conversation
because of the Kavanaugh nomination.
I'm gonna do this a little differently.
I'm gonna give you some advice
on how to even avoid getting in these situations.
And right now there's a whole bunch of ladies going,
Bo, we've heard it.
No you haven't, no you haven't.
You haven't heard this advice.
And then there's probably some other people going,
this is victim blaming, maybe, maybe a little, maybe in 2% of cases. But at the
same time, if you leave your bicycle in front of the meth head's house and don't
put a lock on it, you can't really be surprised when it's not there in the
morning. So the advice is simple. First and foremost, safeguard your reputation. If
you get a certain kind of reputation, you're gonna attract a certain kind of
person. Number two, end the dates at the front door. You know, first couple times,
I know that sounds old-fashioned, but if you're looking for quality over
quantity, you're not gonna miss out on anything. You'll be alright. Number three,
if you're drinking, don't take anybody home. Don't go home with anybody. Don't
take anybody home that's been drinking. If you're at a party and everybody's
drinking, stay with the party. Don't go off into some room. The last two are the
most important. Watch what you say. If you say something sexual, even jokingly, it
might be taken as something sexual. And the last one is definitely the most
important. Be very careful with the signals you send. The easiest way to do that is
keep your hands to yourself. I mean that's kindergarten stuff. Put your
touching hands in your pockets. Right now there's a whole bunch of ladies mad at
In fact, I'm certain there's probably some that stopped and already commented talking about what a horrible person I am.
I'm not talking to ladies. This is for men because there's this him-too hashtag.
A whole bunch of men terrified that they're going to get accused of sexual harassment or sexual assault.
If you're that worried about it, it's because you've done something.
Those type of accusations, they turn out to be false about 2% of the time, as near as I can tell.
2%. They're pretty much always true.
So, here's the thing, guys, gentlemen, and I'm using that term loosely with the audience that I'm talking to.
If you're that worried about it, there's probably a reason.
a reason. And anytime it's a woman that has been assaulted men are always like
well what did she do to bring it on? How was she dressed? What's her
reputation? Was she drinking? That goes both ways fellas. That goes both ways.
Let's uh, you know you always stress accountability, self-accountability. Got
to be accountable for her actions. You need to be accountable for yours. Don't
Don't put yourself in a situation where you can be falsely accused 2% of the time.
Now as far as what a woman wears and how that impacts rape, I saw something on Facebook.
I'm telling you now it's deep and it's dark.
It is dark.
This is my version of a trigger warning.
The miniskirt said, am I really to blame?
And the hijab said, no, it happened to me too.
And the diaper didn't say anything.
Gentlemen there is one cause of rape, and that's rapists.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}