---
title: Let's talk about patriotism, propaganda, and the national anthem....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NHB6j6Tp1IY) |
| Published | 2018/09/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responding to an op-ed story sent by two people who knew a woman attacked for kneeling during the national anthem.
- Expresses irritation and reads the author's bio, prompting a response.
- Criticizes the author for calling the woman a petulant child for kneeling, questioning his understanding of protests.
- Challenges the author on patriotism, pointing out that patriotism involves correcting the government when it is wrong.
- Recounts a story about patriotism being displayed through actions, not just symbols, and suggests seeking guidance from a seasoned individual on patriotism.
- Differentiates between nationalism and patriotism, condemning the author's behavior towards the woman.
- Ends by asserting his stand on patriotism and inviting the author to contact him for further discourse.

### Quotes

- "Patriotism is not doing what the government tells you to."
- "Patriotism is correcting your government when it is wrong."
- "There is a very marked difference between nationalism, which is what this gentleman is talking about and patriotism."
- "You can kiss my patriotic behind."
- "You think that it's just some silly answer from somebody who doesn't really know anything."

### Oneliner

Beau responds to an op-ed story criticizing a woman for kneeling during the national anthem, discussing the difference between nationalism and patriotism and inviting further discourse.

### Audience

Patriotic individuals

### On-the-ground actions from transcript

- Contact Beau for further discourse on patriotism (suggested)
- Seek guidance from experienced individuals on patriotism (implied)

### Whats missing in summary

The emotional impact and depth of the discourse on patriotism and nationalism.

### Tags

#Patriotism #Nationalism #Protest #Community #Opinion


## Transcript
Well, howdy there internet people, it's Bo again.
Promise I'm not going to make a habit of this, but I've got to respond to an op-ed.
Not really an op-ed, it's a story.
But it was sent to me by two people who knew the woman that was attacked by this
story.
And, uh,
when I read it,
I got a little irritated.
And then, when I read the author's bio, I felt like I have to respond. And I think
it's something that everybody in this country should pay attention to. So the
story goes like this and I'll link it so you can read it for yourself. He's
walking, he's at a county fair and he's walking from the bathroom with his
daughter and the national anthem comes on and like good God, fear and Americans
they remove their hat, cover their hearts and stand there until they notice that
some social justice warrior has taken a knee and he says he felt rage, rage. Man
that's a strong word, rage. But luckily for her, her boyfriend was with her. Her
boyfriend's a big guy covered in tattoos and he's staring people down. Staring
people down. It's almost like this woman's gonna get attacked by people who
feel rage for kneeling. It's interesting. And this man who felt rage over this
actually called her a petulant child in his little story, because he says that he
knew she wasn't there to protest, she was just there to get attention. Well, Mr.
Lecturer on patriotism, that's what's in your bio, right? What's the purpose of a
protest again? Is it not to get attention? So you can then petition for redress of
grievances? It's in the Constitution of the United States. I'm very curious to
your lectures on patriotism. Patriotism is a lot more than standing and looking
at the flag. That's right. For a gentleman who's very upset about
disgraceful flag etiquette, where should you have been looking? You shouldn't
have been looking at the crowd. Shouldn't you have been looking up at that
flag? Pretty sure that's what you should have done. Anyway, and then as it goes on,
he says that we stand for the flag and we kneel for the fallen and that you
know that's a common statement but see it bothered me coming from him because
the only it raised this question and I don't I need an answer for it if you
watch this I would really appreciate you telling me exactly what's going on here
because I can only think of two options now maybe there's another one but I
I can only think of two, Mr. Spokesman for Law Enforcement today.
Either you're incompetent, because I mean that certainly sounds like a PR job, right?
Spokesman for Law Enforcement today.
Are you telling me that you have no idea what this protest is over?
You don't know that they are kneeling for the fallen?
Those failed by your brethren in the thin blue line?
Those unjustly murdered?
You don't know that?
you don't you're really bad at your job and if you do it's almost like you're
crafting a propaganda piece and you're leaving out some pretty important
details using slogans to try to manipulate the feeble-minded. Certainly
what it seems like. Now again maybe there's a third option if there is I'd
love to hear it but I can only think of the two but you know I'm not a smart guy
So maybe, maybe you can clue me in. Sir, patriotism is not doing what the
government tells you to, Mr. Lecturer on patriotism. In fact, the original
patriots of this country did the exact opposite. That's how the country got
founded. Patriotism is correcting your government when it is wrong. When the
original patriots, those people that I'm certain you speak of in your lectures,
spoke of their country, they were not talking about the United States. It
didn't exist. 1787, right? 1776. They weren't talking about the US. They were
talking about the country, the countryside, their community. For you,
that's that woman in the crowd that you're attacking. That's who you're
supposed to show loyalty to. Mr. Spokesman for Law Enforcement today, who
is berating a woman for exercising her constitutionally protected rights.
That's kind of chilling, isn't it?
I'm assuming at some point in time you were a cop.
Did you take an oath to support and defend the Constitution of the United States?
Because everything that she's doing is protected by it.
You know, after 9-11, I had the pleasure of watching a conversation between two people.
One of them was a full bird colonel, and the other one was a, well, he was a civilian,
but he's in a suit.
And at the time, I don't know if you remember, but back then, man, everybody, if you were
in a suit, you had that American flag lapel pin on.
And he didn't.
He didn't.
And the colonel asked him, and he wasn't accusatory about it because he knew the guy
knew his history. And he asked him, you know, why aren't you wearing an American
flag lapel pin? And the man responded by saying that he believed that patriotism
was displayed through actions, not worship of symbols. That's a good answer,
right? I mean that's not something you just come up with on the spot because
you get caught without your pen. That's a good answer. And you know, you
think that it's just some silly answer from somebody who doesn't really know anything.
He was a civilian. He was a civilian. He was a spooky civilian. No, if you catch my meaning.
This man served this country for 30 years doing god-awful stuff. In fact, he was in
Afghanistan the first time against the Soviets. I think he's got a pretty good idea of what patriotism
is. Maybe you should talk to him before you give your next lecture. I think that
would be advisable and if you contact me I'll put you in contact with him. I'm sure
that he had would have some things to say to you as well. So I'm sorry this is
kind of a personal video but it's a good teaching tool. I hope that people
understand there is a very marked difference between nationalism, which is
what this gentleman is talking about and patriotism and you you closed your
little temper tantrum but why I'm not gonna use the exact words but you
basically told her that she could kiss your American rear well sir you can kiss
my patriotic behind, okay? Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}