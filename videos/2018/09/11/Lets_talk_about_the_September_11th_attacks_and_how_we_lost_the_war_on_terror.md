---
title: Let's talk about the September 11th attacks and how we lost the war on terror...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XO6sdC3AlQ8) |
| Published | 2018/09/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls his experience on September 11th, watching the tragic events unfold live on TV.
- Expresses how a casualty of that day was the erosion of freedom and rights, not listed on any memorial.
- Talks about the erosion of freedoms post-9/11 due to government actions in the name of security.
- Mentions the strategic nature of terrorism and how it aims to provoke overreactions from governments.
- Points out the dangerous normalization of loss of freedoms in society.
- Urges for a grassroots approach to reclaiming freedom, starting at the community level.
- Advocates for teaching children to question and resist the normalization of oppressive measures.
- Emphasizes the importance of self-reliance in preparation for natural disasters and reducing dependency on the government.
- Suggests counter-economic practices like bartering and cryptocurrency to reduce government influence.
- Encourages surrounding oneself with like-minded individuals who support freedom and each other.
- Calls for supporting and empowering marginalized individuals who understand the loss of freedom.
- Talks about the effectiveness of U.S. Army Special Forces in training local populations as a force multiplier.
- Stresses the need for community building to resist government overreach and tyranny.

### Quotes

- "We didn't know it at the time, though, we were too busy, too busy putting yellow ribbons on our cars, looking for a flag to pick up and wave, or picking up a gun, traveling halfway around the world to kill people we never met."
- "You can't dismantle the surveillance state or the police state by voting somebody into office."
- "You have to defeat an overreaching government by ignoring it."
- "The face of tyranny is always mild at first. And we're there right now."
- "If your community is strong enough, what happens in DC doesn't matter because you can ignore it."

### Oneliner

Beau warns against the dangerous normalization of loss of freedoms post-9/11, urging for grassroots community action to reclaim freedom and resist government overreach.

### Audience

Community members

### On-the-ground actions from transcript

- Teach children to question and resist oppressive measures (implied)
- Prioritize self-reliance for natural disasters and reduce dependency on the government (implied)
- Practice counter-economic activities like bartering and cryptocurrency (implied)
- Surround yourself with like-minded individuals who support freedom (implied)
- Support and empower marginalized individuals who understand the loss of freedom (implied)
- Build community networks to resist government overreach (implied)

### Whats missing in summary

The full transcript provides deeper insights into the erosion of freedoms post-9/11 and the strategic nature of terrorism, urging individuals to take concrete actions at the community level to safeguard freedom and resist tyranny.

### Tags

#Freedom #Terrorism #CommunityAction #GovernmentOverreach #Grassroots


## Transcript
Well, howdy there, internet people.
It's Bo again.
And today is September 11th, years ago.
Seems like a lifetime ago, really.
A young engineer named Nick were ousted me out of my rack.
Plane just hit the World Trade Center.
Took me a minute to kind of wake up and get out there,
turn a corner, look at the TV, get there just in time
to see the second plane hit.
We knew then it wasn't an accident,
and we knew what that meant.
So we did what I think most men of our ilk would have done.
We grabbed a beer and a board game called Risk,
sat there and watched people die live on TV,
knowing a whole bunch of other people were about to die,
because somebody was going to pay for that.
See, the thing is, among the casualties of that day from the three towers in New York,
that field, and the Pentagon, there's one casualty that never gets listed, never gets
named, it's not on any memorial, but it was mortally wounded that day.
We didn't know it at the time, though, we were too busy, too busy putting yellow ribbons
on our cars, looking for a flag to pick up and wave, or picking up a gun, traveling halfway
around the world to kill people we never met, and most of them didn't have anything to
do with what happened that day.
But nonetheless, that casualty got closer to flatlining with each new law that got passed.
Freedom's breath got more and more shallow with every right and every freedom the government
took from you. Prior to September 11th, 2001, a lot of the things that seem normal today
were unthinkable. The idea of U.S. forces being engaged in torture, the idea of indefinite
detention, the idea that the government would have a record of you watching this video.
And that was something out of some science fiction movie showing the worst that can happen.
Today it's reality because we're scared and because we lost the war on terror.
I know there's some people who are going to say, no, no, no, no, we did, we lost it,
game over.
See terrorism isn't random.
Terrorism is not just killing for killing sake and hoping to scare people.
Terrorism is very strategic.
In fact, the definition, the real definition of terrorism is the use of violence or the
threat of violent acts designed to influence those beyond the immediate area of attack
to achieve a political, religious, ideological or monetary goal.
That's terrorism.
And that key thing there is beyond the immediate area of attack.
It's not about killing, it's about provoking a reaction.
And in most strategies of terrorism, the idea is to provoke an overreaction.
Those responsible for the September 11th attacks couldn't have wished for the response that
happened.
I mean, that is the best case scenario.
The American people just basically, here, take my rights, take my freedom.
that's what they need. They need the government to overreact and clamp down.
You see then the average person, person out here in the real world, they get mad.
When you go to get on an airplane and you get groped by the TSA, you get mad at
some Middle Eastern guy, you're mad at the government. And that's the idea. The
The idea is that it ferments rebellion and resistance to the government eventually.
This causes the downfall of that government.
That's the concept.
That's the strategy behind terrorism.
And the reason people use it is because it works.
Now normally, the government, the target government, isn't so willing to go along with it.
But in this case, freedom.
They packaged it, they sold it.
And now, a decade and a half later, they got people basically believing the more freedom
you give up, the more you support freedom.
You have people in positions of government saying, we were just following orders.
It's called the Nuremberg Defense, so they got hung.
Anyway, the thing is, we can't let this become normal.
We can't let this become normal.
And the reason I'm saying this is because I got a feeling, judging from the comments
on all these videos, that all these years, it's been you guys, y'all been giving freedom
CPR, but at the same time I see something in the comments section a lot that just kind
of blows my mind. Everybody's talking about how to vote freedom back. Nobody in power
is going to give you your rights back. They all sell you this under the idea that it's
temporary. It's not. They're not going to return these freedoms. You're not going to
dismantle the surveillance state or the police state by voting somebody into office. They
don't care. We've passed that point. We're at the point now where we have two options.
When you want to change someone's mind, you have two things you can do. You can put a
new idea in their brain or you can put a bullet in it. I'd really like to focus on
putting a new idea out there and the idea is that we've got to do this from
the bottom up. We have to do it at the community level. You're going to defeat
defeat an overreaching government by ignoring it.
And in that vein, I've come up with a couple ideas I'd like to run by you.
Here's some things that I think can be done at the local level, the community level, that
will help re-instill the idea that freedom is not doing what the government tells you
to do. And the first part about that is teaching the children quietly and that is
making sure that your kids know that this isn't normal. See above all the
thing that we have to do at all cost is to never allow this to become normalized.
We can't let the idea of soldiers, they're police officers, they got badges,
but now they're in MRAPs and carrying the same weapons people carried in
overseas. They're soldiers on street corners. We can't allow the idea of
constant surveillance to become normal. We have to fight it. And the way you
fight that is you make sure that your kids know that it's not right, that it's
not normal. And in that sense, you're kind of planting a shade tree that you're
never going to get to sit under. But you're creating a generation of people
that know what's wrong. You've got to prepare for the worst. It's one of the
other things that I think we need to focus on because A, as we saw during the
hurricanes last year and as we're about to see again, the government didn't come
to help, okay? They don't care. They don't care about you. FEMA is not going to come
help. So you need to become self-reliant and you need to be able to take care of yourself
and your community in the event of a natural disaster. Now, the reason this aids in the
idea of instilling freedom is because the more self-reliant you are, the less dependent
on government you are. The less dependent on government you are, the less they have
to hold over you. Most of the great movements in this country have realized
this. Now I mean and you can look to black activists in the 60s and 70s who
basically came up with the idea for food stamps and they ran it themselves. They
distributed food to help build that self-reliance in their community. To the
right-wing militia guys out there ready for doomsday. Self-reliance. It's important.
Counter-economics. Counter-economics is something that's it's a fancy word for
basically men to kind of starve the government because they they kind of
leech off of every economic transaction you make. Now to people my age and older
that means you know silver, gold, reduce, reuse, repurpose, recycle,
barter. To those younger there's this cryptocurrency thing, Bitcoin. Now I
understand the premise of it. It's basically a currency that isn't backed
by a government, and man, that sounds cool.
And then with a lot of the hippier people today, I don't know what to call them, you
know, farmers markets, organic growing, growing your food, not lawns, did a video on that.
All of that helps keep the money local, which means it doesn't go to these big corporations
which then turn around and give it to the government.
Give it to politicians in the form of bribes
or campaign contributions or whatever.
So we've got to start focusing on that.
Your circle of friends, if you're serious about this
and you want to really get into the fight,
you have to surround yourself with other people who are in it. And again, I really
do believe y'all the ones have been keeping freedom on CPR. Y'all the ones
been keeping it alive. So I think it's important that that happens and that you
guys support each other. The other thing is to look out for the downtrod. Look out
for those that have kind of hit the bottom because those people, more than anybody, are
going to understand how messed up things got.
And that goes to the homeless vet that everybody's going to pretend they care about today, even
though if they did, we wouldn't have homeless vets.
To the guy that just got out of the joint because he got wrapped up selling dope, some
non-violent crime that now his life is ruined. But maybe you own a business and
give him a job, help get him back on his feet. Maybe you can kind of help out
with that. These are people that understand freedom and understand the
loss of it. These are people that are probably more willing to get involved
and get active than you might imagine.
And then I've touched on this in other videos
and I've hinted to it, but I never really come out
and said it, the U.S. Army Special Forces,
it's the most feared fighting unit in the world,
the Green Berets, the reason they're the most feared
fighting unit in the world is not because they're
such adept killers, I mean they are,
Don't get me wrong, but the reason other governments are afraid of them is because they're teachers.
They show up in a foreign country and they take the local populace and they train them.
And that 12-man team can fill the battalion of soldiers in no time.
That's terrifying.
You need to be a force multiplier.
That's what they call it, force multiplication.
You need to get out there.
got to start talking to people. Do it on Facebook. Do it in real life. Talk about
what we've lost. Talk about how we can fix it and build your community. Because
if your community is strong enough, what happens in DC doesn't matter because you
can ignore it. And at the end of the day, that's how this is going to have to play
out unless we want it to get really bad. We're going to have to defeat a government that
is very overreaching by ignoring it. You know, the face of tyranny is always mild at first.
And we're there right now. We really are. And if we don't pay attention, we're going
to lose the ability to get it back. So something to think about this today.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}