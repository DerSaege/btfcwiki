# All videos from September, 2018
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2018-09-28: Let's talk about Kavanaugh, bar fights, and "what if it was your sister?".... (<a href="https://youtube.com/watch?v=FngaU-CK0Zk">watch</a> || <a href="/videos/2018/09/28/Lets_talk_about_Kavanaugh_bar_fights_and_what_if_it_was_your_sister">transcript &amp; editable summary</a>)

Beau recounts a bar incident to illustrate respecting women's autonomy, drawing parallels to the Kavanaugh hearing's impact on survivors and urging reflection on harmful narratives.

</summary>

"You might not want to do it again because you're altering real-life relationships over bumper sticker politics."
"Sexual assault is normally about power and control."
"You might not want to do it again because you're altering real-life relationships over bumper sticker politics."
"You already ruined that."
"How many of them do you know? How many of them talked about it with you?"

### AI summary (High error rate! Edit errors on video page)

Recounts a story from college where he intervened in a potentially dangerous situation at a bar to protect a girl he vaguely knew from being harassed by a guy and his friends.
Draws parallels between his bar incident and the current atmosphere around the Kavanaugh hearing, discussing the importance of respecting women's autonomy and agency.
Uses a hypothetical scenario with friends to illustrate the prevalence of sexual assault among women and how comments dismissing survivors can affect their willingness to come forward.
Criticizes the double standards and misconceptions surrounding men's emotions and reactions to trauma, contrasting them with the Kavanaugh hearings.
Emphasizes that sexual assault is about power and control, pointing out the significance of survivors speaking out against someone who may wield immense power.
Urges reflection on the impact of dismissing survivors and perpetuating harmful narratives, stressing the lasting damage it can cause to relationships.

Actions:

for all genders, allies,
Reach out to survivors in your community, offer support, and believe them (implied)
Educate yourself and others on the prevalence of sexual assault and its impact (suggested)
</details>
<details>
<summary>
2018-09-26: Let's talk about #MeToo, #HimToo, and sexual harassment and assault.... (<a href="https://youtube.com/watch?v=-qoW7FIWWs8">watch</a> || <a href="/videos/2018/09/26/Lets_talk_about_MeToo_HimToo_and_sexual_harassment_and_assault">transcript &amp; editable summary</a>)

Beau provides advice on avoiding sexual harassment and assault, stressing accountability and debunking victim-blaming myths.

</summary>

"Be very careful with the signals you send."
"False accusations of sexual harassment or sexual assault turn out to be false about 2% of the time."
"There is one cause of rape, and that's rapists."

### AI summary (High error rate! Edit errors on video page)

Addresses sexual harassment and assault in the context of the Kavanaugh nomination.
Provides advice on avoiding such situations, including safeguarding reputation and watching signals.
Mentions ending dates at the front door, avoiding going home with anyone if drinking, and being cautious with words.
Talks about the importance of keeping hands to oneself to avoid sending wrong signals.
Acknowledges the "him-too" hashtag and addresses men's concerns about false accusations.
Indicates that false accusations of sexual harassment or assault are rare, around 2% of the time.
Stresses mutual accountability between men and women regarding actions and situations.
Refutes the idea that clothing choices can cause rape, asserting that rapists are the sole cause.
Concludes with a powerful statement: "Gentlemen, there is one cause of rape, and that's rapists."

Actions:

for men, women,
Safeguard your reputation by being mindful of the signals you send (implied).
End dates at the front door to prioritize quality over quantity (implied).
Avoid taking anyone home when drinking to prevent risky situations (implied).
</details>
<details>
<summary>
2018-09-25: Let's talk about race and guns.... (<a href="https://youtube.com/watch?v=wXFtH3v2epI">watch</a> || <a href="/videos/2018/09/25/Lets_talk_about_race_and_guns">transcript &amp; editable summary</a>)

Beau breaks down the division within the gun community, the racial implications of gun control, and the importance of arming minorities for protection under the Second Amendment.

</summary>

"The intent of the Second Amendment is to strike back against government tyranny."
"Second Amendment supporters want to decentralize power, breaking up the government's monopoly on violence."
"The Second Amendment is there to protect racial minorities."
"Second Amendment supporters aim to arm minorities to protect against government tyranny targeting marginalized groups."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the division within the gun community between Second Amendment supporters and gun nuts.
Second Amendment supporters aim to arm minorities to protect against government tyranny targeting marginalized groups.
Second Amendment supporters can be blunt and use violent rhetoric, but their intent is to encourage self-defense.
Describes a violent image popular within the Second Amendment community that contrasts with the gun nut philosophy.
Differentiates between Second Amendment supporters who want to decentralize power and gun nuts who focus on might making right.
Mentions a program by the Department of Defense as a way to tell the difference between Second Amendment supporters and gun nuts.
Addresses the unintentional racism in some gun control measures, such as requiring firearms insurance disproportionately affecting minorities.
Talks about the racial implications of banning light pistols, which were predominantly bought by minorities.
Acknowledges the historical racial component of gun control but notes a shift due to awareness raised by Second Amendment supporters.
Emphasizes the importance of arming minority groups for their protection under the Second Amendment.

Actions:

for advocates, gun owners, activists,
Contact local gun stores to inquire about free training programs for minority and marginalized groups (implied).
Advocate for surplus firearms distribution to low-income families in collaboration with community organizations (suggested).
Raise awareness about unintentional racial implications of gun control measures targeting minorities (exemplified).
</details>
<details>
<summary>
2018-09-23: Let's talk about patriotism, propaganda, and the national anthem.... (<a href="https://youtube.com/watch?v=NHB6j6Tp1IY">watch</a> || <a href="/videos/2018/09/23/Lets_talk_about_patriotism_propaganda_and_the_national_anthem">transcript &amp; editable summary</a>)

Beau responds to an op-ed story criticizing a woman for kneeling during the national anthem, discussing the difference between nationalism and patriotism and inviting further discourse.

</summary>

"Patriotism is not doing what the government tells you to."
"Patriotism is correcting your government when it is wrong."
"There is a very marked difference between nationalism, which is what this gentleman is talking about and patriotism."
"You can kiss my patriotic behind."
"You think that it's just some silly answer from somebody who doesn't really know anything."

### AI summary (High error rate! Edit errors on video page)

Responding to an op-ed story sent by two people who knew a woman attacked for kneeling during the national anthem.
Expresses irritation and reads the author's bio, prompting a response.
Criticizes the author for calling the woman a petulant child for kneeling, questioning his understanding of protests.
Challenges the author on patriotism, pointing out that patriotism involves correcting the government when it is wrong.
Recounts a story about patriotism being displayed through actions, not just symbols, and suggests seeking guidance from a seasoned individual on patriotism.
Differentiates between nationalism and patriotism, condemning the author's behavior towards the woman.
Ends by asserting his stand on patriotism and inviting the author to contact him for further discourse.

Actions:

for patriotic individuals,
Contact Beau for further discourse on patriotism (suggested)
Seek guidance from experienced individuals on patriotism (implied)
</details>
<details>
<summary>
2018-09-20: Let's talk about guns, gun owners, school shootings, and "law abiding gun owners" (part 2).... (<a href="https://youtube.com/watch?v=wNtxtuQxUz8">watch</a> || <a href="/videos/2018/09/20/Lets_talk_about_guns_gun_owners_school_shootings_and_law_abiding_gun_owners_part_2">transcript &amp; editable summary</a>)

Beau explains why gun control measures fail and suggests solutions like raising the minimum gun-buying age and addressing domestic violence loopholes, pointing out the limitations of legislation.

</summary>

"Assault weapon is not in the firearms vocabulary."
"A soldier can enlist in the Army, come home, and not be able to buy a gun."
"Legislation is not the answer here in any way shape or form."

### AI summary (High error rate! Edit errors on video page)

Gun control has failed due to being written by those unfamiliar with firearms, leading to ineffective measures.
The term "assault weapon" is made up and lacks a concrete definition within firearms vocabulary.
Bans on assault weapons focused on cosmetic features rather than functionality, rendering them ineffective.
Banning high-capacity magazines is flawed as changing magazines can be swift, making the rush tactic infeasible.
Efforts to ban specific firearms like the AR-15 could lead to more powerful weapons being used in its place.
Banning semi-automatic rifles entirely is impractical, given their prevalence and ease of manufacture.
Restrictions on gun ownership based on age, particularly raising the minimum age to 21, could be a more effective measure.
Addressing loopholes in laws regarding domestic violence offenders owning guns could enhance safety measures.
Legislation focusing solely on outlawing certain firearms may not address the root issues effectively.
Beau stresses the need to look beyond laws and tackle deeper societal issues for a comprehensive solution.

Actions:

for advocates, activists, legislators,
Raise awareness about loopholes in laws regarding domestic violence offenders owning guns (suggested).
Advocate for raising the minimum age for purchasing firearms to 21 (suggested).
</details>
<details>
<summary>
2018-09-20: Let's talk about guns, gun control, school shootings, and the "law abiding gun owner" (Part 3) (<a href="https://youtube.com/watch?v=QbXTDuwSVkk">watch</a> || <a href="/videos/2018/09/20/Lets_talk_about_guns_gun_control_school_shootings_and_the_law_abiding_gun_owner_Part_3">transcript &amp; editable summary</a>)

Beau challenges pro-gun individuals, deconstructs cultural attitudes towards guns, and advocates for a shift towards true masculinity to address gun-related issues.

</summary>

"It's crazy how simple it is."
"Violence is always the answer, right?"
"Bring back real masculinity, honor, integrity."
"Bring it back to a tool, instead of making it your penis."
"You solve that, you're not gonna need any gun control."

### AI summary (High error rate! Edit errors on video page)

Addresses pro-gun individuals, challenging their perspective on gun control and cultural attitudes towards guns.
Questions the source of guns used in school shootings, pointing out the role of relatives or members of the gun culture.
Criticizes the glorification of guns in social media and its impact on influencing violent behavior.
Analyzes how guns have been perceived historically and how they have evolved into symbols of masculinity.
Critiques the response to school shootings and the underlying attitudes towards discipline and violence.
Clarifies the true intention of the Second Amendment and its historical context in relation to civil insurrections and government tyranny.
Challenges the concept of being a "law-abiding gun owner" and its implication in government actions.
Confronts the idea that violence is always the answer, debunking the association between guns and masculinity.
Advocates for a shift in societal values towards true masculinity, integrity, and empathy to address gun-related issues.
Emphasizes the importance of raising children with values that prioritize self-control and non-violence.

Actions:

for gun owners, activists, parents,
Educate about responsible gun ownership and the true purpose of the Second Amendment (implied).
Advocate for values of integrity, empathy, and self-control to address gun-related issues (implied).
Engage in community dialogues about masculinity and violence to foster a culture of non-violence (implied).
</details>
<details>
<summary>
2018-09-19: Let's talk about guns, gun control, school shooting, and "law abiding gun owners" (Part 1)... (<a href="https://youtube.com/watch?v=BxvxbZGjlv4">watch</a> || <a href="/videos/2018/09/19/Lets_talk_about_guns_gun_control_school_shooting_and_law_abiding_gun_owners_Part_1">transcript &amp; editable summary</a>)

Beau dives into the controversial topic of guns, debunking misconceptions about the AR-15 and setting the stage for discussing sensible gun control.

</summary>

"Your Second Amendment, that sacred Second Amendment, we're going to talk about that too."
"One gun that everybody knows because the media won't shut up about it. We're going to talk about the AR-15."
"There's nothing special about this thing."
"It's not the design of this thing that makes people kill."
"I know this one was kind of boring, but you need this base of knowledge to understand what we're going to talk about next."

### AI summary (High error rate! Edit errors on video page)

Addressing the controversial topic of guns, gun control, and the Second Amendment with the aim of sensible gun control.
Identifying three types of people: pro-gun, anti-gun, and those who understand firearms.
Using the example of the AR-15 to debunk misconceptions and explain its design.
Exploring the origins of the AR-15 and its use in the military.
Clarifying that the AR-15 is not a high-powered rifle and was not initially desired by the military.
Pointing out misconceptions perpetuated by the media regarding the AR-15 and its usage in shootings.
Describing the popularity of the AR-15 among civilians due to its simplicity and interchangeability of parts.
Contrasting the AR-15 with the Mini 14 Ranch Rifle, a hunting rifle with similar characteristics.
Emphasizing that the design of the AR-15 is not unique and has been around for a long time.
Concluding with a promise to address mitigation strategies in the next video.

Actions:

for gun policy advocates,
Educate others on the facts about firearms (implied)
Engage in informed discussions about gun control (implied)
</details>
<details>
<summary>
2018-09-16: Let's talk about 3 letters that can make rural whites understand the fear minorities have of cops. (<a href="https://youtube.com/watch?v=J5DBrOBIgNM">watch</a> || <a href="/videos/2018/09/16/Lets_talk_about_3_letters_that_can_make_rural_whites_understand_the_fear_minorities_have_of_cops">transcript &amp; editable summary</a>)

Exploring the distrust towards law enforcement, Beau urges unity and accountability to address the common issue of unaccountable men with guns.

</summary>

"We can hold them accountable one way or the other, the ballot box or the cartridge box."
"It's unaccountable men with guns. We can work together and we can solve that."
"We've got to start talking to each other. We got the same problems."

### AI summary (High error rate! Edit errors on video page)

Explains the distrust that minorities have for law enforcement compared to white country folk.
Illustrates how in rural areas, law enforcement is more accountable and part of a tighter community.
Mentions the ability to hold law enforcement accountable through the ballot box or the cartridge box.
Points out that minority groups lack institutional power to hold unaccountable men with guns in law enforcement accountable.
Draws parallels between the distrust felt towards law enforcement by minorities and white country folk towards agencies like ATF and BLM.
Addresses the frequency of unjust killings and lack of accountability in law enforcement.
Encourages taking action by getting involved in local elections to ensure accountability.
Suggests starting at the local level by electing good police chiefs to prevent corruption in federal agencies.
Advocates for people from different backgrounds to unite and address the common issue of unaccountable men with guns in law enforcement.
Urges individuals to care enough to take action and work together to solve the problem.

Actions:

for community members, activists,
Elect good police chiefs in local elections to prevent corruption (suggested)
Advocate for accountability in law enforcement by participating in local elections (implied)
Start dialogues with individuals from different backgrounds to address common issues (implied)
</details>
<details>
<summary>
2018-09-14: Let's talk about the Dallas shooting... (<a href="https://youtube.com/watch?v=6-d4C-sIlaI">watch</a> || <a href="/videos/2018/09/14/Lets_talk_about_the_Dallas_shooting">transcript &amp; editable summary</a>)

Beau questions the official narrative of a shooting in Dallas, pointing out potential motives and legal consequences, while criticizing police actions and lack of justice for victims, and connecting it to the Black Lives Matter movement.

</summary>

"There's two kinds of liars in the world."
"I'm not a lawyer, but that sure sounds like motive to me."
"If this is the amount of justice they get, they don't. They don't."
"Not one cop has crossed the thin blue line to say that's wrong."
"It doesn't matter what else you do."

### AI summary (High error rate! Edit errors on video page)

Expresses skepticism towards the official narrative of the shooting in Dallas, referencing experience with liars and motives.
Raises concerns about the shooting being a potential home invasion due to new information about noise complaints.
Points out the potential legal consequences in Texas for killing someone during a home invasion.
Questions the lack of intervention or opposition within the police department regarding potential corruption and cover-ups.
Connects the lack of justice in cases like this to the Black Lives Matter movement and the value placed on black lives.
Criticizes the actions of the police in searching the victim's home posthumously for justifications.

Actions:

for community members, justice seekers, activists.,
Speak out against police corruption and cover-ups within your community (implied).
Support movements like Black Lives Matter by advocating for justice and equality (implied).
</details>
<details>
<summary>
2018-09-11: Let's talk about the September 11th attacks and how we lost the war on terror... (<a href="https://youtube.com/watch?v=XO6sdC3AlQ8">watch</a> || <a href="/videos/2018/09/11/Lets_talk_about_the_September_11th_attacks_and_how_we_lost_the_war_on_terror">transcript &amp; editable summary</a>)

Beau warns against the dangerous normalization of loss of freedoms post-9/11, urging for grassroots community action to reclaim freedom and resist government overreach.

</summary>

"We didn't know it at the time, though, we were too busy, too busy putting yellow ribbons on our cars, looking for a flag to pick up and wave, or picking up a gun, traveling halfway around the world to kill people we never met."
"You can't dismantle the surveillance state or the police state by voting somebody into office."
"You have to defeat an overreaching government by ignoring it."
"The face of tyranny is always mild at first. And we're there right now."
"If your community is strong enough, what happens in DC doesn't matter because you can ignore it."

### AI summary (High error rate! Edit errors on video page)

Recalls his experience on September 11th, watching the tragic events unfold live on TV.
Expresses how a casualty of that day was the erosion of freedom and rights, not listed on any memorial.
Talks about the erosion of freedoms post-9/11 due to government actions in the name of security.
Mentions the strategic nature of terrorism and how it aims to provoke overreactions from governments.
Points out the dangerous normalization of loss of freedoms in society.
Urges for a grassroots approach to reclaiming freedom, starting at the community level.
Advocates for teaching children to question and resist the normalization of oppressive measures.
Emphasizes the importance of self-reliance in preparation for natural disasters and reducing dependency on the government.
Suggests counter-economic practices like bartering and cryptocurrency to reduce government influence.
Encourages surrounding oneself with like-minded individuals who support freedom and each other.
Calls for supporting and empowering marginalized individuals who understand the loss of freedom.
Talks about the effectiveness of U.S. Army Special Forces in training local populations as a force multiplier.
Stresses the need for community building to resist government overreach and tyranny.

Actions:

for community members,
Teach children to question and resist oppressive measures (implied)
Prioritize self-reliance for natural disasters and reduce dependency on the government (implied)
Practice counter-economic activities like bartering and cryptocurrency (implied)
Surround yourself with like-minded individuals who support freedom (implied)
Support and empower marginalized individuals who understand the loss of freedom (implied)
Build community networks to resist government overreach (implied)
</details>
<details>
<summary>
2018-09-08: Let's talk about what it's like to be a black person in the US.... (<a href="https://youtube.com/watch?v=WD8mWq0Hdcw">watch</a> || <a href="/videos/2018/09/08/Lets_talk_about_what_it_s_like_to_be_a_black_person_in_the_US">transcript &amp; editable summary</a>)

Unexpected reach of a Nike video led to deep reflections on cultural identity, heritage, and the ongoing impacts of slavery on collective identity, urging understanding beyond surface-level statistics.

</summary>

"Your entire cultural identity was ripped away."
"They have black pride because they don't know."
"That's a whitewash of the reality."
"How much of who you are as a person comes from the old country."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Unexpected wide reach of a Nike video led to insults and reflections.
Insult about IQ compared to a plant sparked thoughts on understanding being Black in the U.S.
White allies may grasp statistics but not the true experience of being Black.
Deep dive into cultural identity, heritage, and pride.
Historical context of slavery and its lasting impacts on cultural identity.
Reflections on cultural identity being stripped away and replaced.
Food, cuisine, and cultural practices as remnants of slavery.
The ongoing impact of slavery on cultural scars.
Call to acknowledge history and the need for healing.
Hope for a future where national identities are left behind.
Acknowledgment of uncomfortable history and the lack of pride for some.
Recognition of the significance of the Black Panther movie in providing pride.
Encouragement to truly understand the depth of the issue beyond surface-level talks.
Acknowledgment of cultural identity loss and the need for reflection and understanding.

Actions:

for white allies,
Acknowledge and understand the deep-rooted cultural impacts of historical events (implied).
</details>
<details>
<summary>
2018-09-05: Let's talk about burning shoes... (<a href="https://youtube.com/watch?v=mfDasT0zSpg">watch</a> || <a href="/videos/2018/09/05/Lets_talk_about_burning_shoes">transcript &amp; editable summary</a>)

Beau questions the logic behind burning symbols of protest and challenges the true meaning of freedom.

</summary>

"If you're that mad and you can't wear a pair of Nikes because of a commercial, take them and drop them off at a shelter."
"You're loving that symbol of freedom more than you love freedom."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau wants to burn some shoes today because it's raining and he feels the need to burn something.
He mentions not owning Nikes due to personal reasons but his son has a pair.
Beau questions the significance of burning shoes as a form of protest against Nike.
He suggests donating unwanted Nikes to shelters or thrift stores near military bases for those in need.
Beau challenges the idea of burning symbols without considering the workers behind the products.
He draws parallels between burning Nikes and burning the American flag as symbolic acts.
Beau questions the integrity of those who are quick to disassociate with Nike over a commercial but ignore their history of unethical practices.
He criticizes people who claim to value freedom but turn a blind eye to issues like sweatshops and slave labor.
Beau concludes by pointing out the irony of loving the symbol of freedom more than actual freedom itself.

Actions:

for consumers, activists, ethical shoppers,
Donate unwanted shoes to shelters or thrift stores near military bases (suggested)
Educate oneself on the ethical practices of companies before supporting them (implied)
</details>
