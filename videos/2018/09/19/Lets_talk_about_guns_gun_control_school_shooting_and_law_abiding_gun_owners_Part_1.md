---
title: Let's talk about guns, gun control, school shooting, and "law abiding gun owners" (Part 1)...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BxvxbZGjlv4) |
| Published | 2018/09/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the controversial topic of guns, gun control, and the Second Amendment with the aim of sensible gun control.
- Identifying three types of people: pro-gun, anti-gun, and those who understand firearms.
- Using the example of the AR-15 to debunk misconceptions and explain its design.
- Exploring the origins of the AR-15 and its use in the military.
- Clarifying that the AR-15 is not a high-powered rifle and was not initially desired by the military.
- Pointing out misconceptions perpetuated by the media regarding the AR-15 and its usage in shootings.
- Describing the popularity of the AR-15 among civilians due to its simplicity and interchangeability of parts.
- Contrasting the AR-15 with the Mini 14 Ranch Rifle, a hunting rifle with similar characteristics.
- Emphasizing that the design of the AR-15 is not unique and has been around for a long time.
- Concluding with a promise to address mitigation strategies in the next video.

### Quotes

- "Your Second Amendment, that sacred Second Amendment, we're going to talk about that too."
- "One gun that everybody knows because the media won't shut up about it. We're going to talk about the AR-15."
- "There's nothing special about this thing."
- "It's not the design of this thing that makes people kill."
- "I know this one was kind of boring, but you need this base of knowledge to understand what we're going to talk about next."

### Oneliner

Beau dives into the controversial topic of guns, debunking misconceptions about the AR-15 and setting the stage for discussing sensible gun control.

### Audience

Gun policy advocates

### On-the-ground actions from transcript

- Educate others on the facts about firearms (implied)
- Engage in informed discussions about gun control (implied)

### Whats missing in summary

In-depth analysis and detailed solutions for addressing gun control issues.

### Tags

#Guns #GunControl #SecondAmendment #AR15 #MediaBias


## Transcript
Howdy there, internet people.
It's Bo again.
We've talked about a lot of controversial things
in these videos.
This may prove to be the most controversial of all.
I've been asked to do this at least 40 times.
I put it off and put it off because it's a very
in-depth subject.
And it's been ruined by talking points on both sides, so many
talking points that nobody's talking about it.
Um, we're going to talk about guns.
We're going to talk about guns.
We're going to talk about gun control.
The good, the bad, the ugly.
We're going to talk about the Second Amendment.
We're going to talk about school shootings.
And right now, there's a whole bunch of people out there that
are pro-gun that are like, oh yeah, Hillbilly is about to
school these liberals on guns.
I am.
I'm definitely going to educate them.
But I'm going to educate them with the intent of them using
that education to hopefully come up with some gun
control that makes sense. Now you're mad. And that's okay because your Second
Amendment, that sacred Second Amendment, we're going to talk about that too. And
we're going to talk about the fact that odds are if you constantly describe
yourself as a law-abiding gun owner, you're probably part of the problem.
So three types of people in the world, pro-gun, anti-gun, and those who
understand how firearms work. So we are going to talk about firearms. We're going to use the an example
that everybody in the United States knows. One gun that everybody knows because the media won't shut
up about it. We're going to talk about the AR-15. Now this is a toy. I bought this at Dollar General
yesterday when I tried to film this video the first time. It got so long that even I didn't
want to watch it. That's why we're doing it in sections. So, the reason I bought it is because
one, it looks just like it. I mean it's accurate, it's scary accurate. Even more scary, everything
works. The magazine release, the selector switch, I mean the charging handle, the forward assist,
I mean it's even got the bolt release on it. I mean there's no bolt inside, but it's there.
I mean, this is amazing. A little small, but extremely accurate representation.
So, it's AR-15 or is it the M16? You can't tell really by looking at it. They look the
same. They really do. In fact, if you were to walk into a gun store, hand one, odds are
you guys are going to have to look right here, right at the receiver, at the selector switch
see what it is. But there's an audible difference between the two. The AR-15, which is the civilian
one, sounds like this. That's fast, and you can get rounds off that fast in real life.
That is fast. The M16, the military version, sounds like this. That's a whole lot faster.
a cyclic rate of 700 to 950 rounds a minute. That's a lot of bullets. So, what makes it
special? I mean, it's a military weapon. Even the civilian one is based on a military design,
therefore it's high powered, right? No, not at all. This is actually low power, low intermediate
is what I would classify it as.
The most gun sites will tell you that it's a varmint gun.
But in reality, the bullet,.223, is a varmint round.
It's low intermediate.
It's not really low.
Well, that doesn't make any sense.
Why on earth would the Army want a low-powered rifle?
They didn't.
Army didn't want this.
They were forced to take it by some bean counters in DoD up at the Pentagon.
See what happened was these guys got together and they figured
if they made the bullets smaller
you could fit more in the magazine
therefore
you could shoot more
in between changing magazines
kill more of the bad guy faster
therefore win the war faster.
And that makes sense
if you have never shot at anybody and never changed a magazine.
Um, doesn't make sense if you have.
Okay, so if it's not high powered, it must be high tech.
That's why the military wanted it.
That's why DOD wanted it.
When Eugene Stoner designed this thing, put it together, he designed it to be used by
the dumbest 17 year old kids you know.
And that's not a joke.
Really one of his goals.
I want you to think about everybody who joins the army.
They're not all geniuses.
every E1 in the world knows how to use this. So it's not high tech, it's not high
power. Why is it so special? It's not. That's the point. There's nothing special
about this thing. You talk to most serious shooters, nobody's gonna point
to this thing as like some great crowning achievement. It's decent. It's a
very great generic basic rifle. It's modular, that's cool. It's got a lot of variations.
If it's not that great, why does it get used in all these school shootings and all these mass
shootings? It doesn't. It doesn't, not in real life. The problem is the media can't identify
firearms. They think anything with a magazine well, a pistol grip, and a magazine is an AR-15,
Especially if it's black.
So they say that in the initial reporting and then, you know, by the time they correct
it, it doesn't matter.
So it seems like this is what's always used.
That's not really the case.
But it gets used often enough for this to be the example of what we're talking about.
Why?
Why is it used more than most?
Because it's popular.
Not just with mass shooters, but with everybody.
This is the most popular rifle in the United States.
Why?
we just said. It's simple. It's low power, so it has low recoil. Anybody can use it.
But then there's another reason that civilians like it. There's two really. One is really smart
and one is really stupid. The stupid reason is a bunch of guys want to get it, post their photo on
Facebook like this. I got the same gun the army has. No you don't. You got one that looks like it.
The smart reason is that because they're very similar, except in here, except in the engine,
a lot of parts are interchangeable, which means they're cheap because the Army's producing
it.
An M16 magazine will slide into an AR-15.
The stock, all the furniture, that's the plastic pieces, the barrels, I mean everything, pretty
much is interchangeable with the exception of the stuff inside the receiver, the stuff
that makes it full-auto. That's the only thing that isn't interchangeable between them.
An AR-15 is a semi-automatic rifle. Now, I want you to pull up right now, go to Google
Images, type in Mini 14 Ranch Rifle. You're going to look at that, you're going to say
That's a hunting rifle, and it is.
It's a varmint rifle.
Look at the caliber.
.223, same thing.
Rate of fire, same as the AR-15.
Everything is the same.
Everything.
This has been villainized because of the way it looks,
partly, and because it gets used in these school shootings
and in these mass shootings, a decent amount.
Not as much as the media says, but certainly more than a Mini 14.
But it's just because it's more popular.
Because it's more popular because basically any gun owner that has more than two guns,
they've got one of these.
Therefore, when some kid wants to shoot up a school, guess what's sitting in dad's gun
cabinet?
One of these.
It's that idea to mimic the military.
It's that idea, and this is a big part of it we can get into later, it's that idea
that this is important.
So how does it work?
On the inside, this is a magazine.
Bullets are inside this box.
As the bolt goes back and forth, it picks up another one, puts it in the chamber.
When you pull the trigger, a little pin comes forward, hits it, sends it down the barrel.
That's how it works.
There's nothing different between the AR-15
and every other semi-automatic rifle designed
in the last 100 years.
They've been around forever.
They have been around forever.
That's important because it's not the design of this thing
that makes people kill.
Fairly recent problem, it's probably not this.
But there's some things we can do to mitigate,
and that's what we're gonna talk about
the next video. I know this one was kind of boring, but you need this base of
knowledge to understand what we're going to talk about next. Okay, sorry for the
boring video, but y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}