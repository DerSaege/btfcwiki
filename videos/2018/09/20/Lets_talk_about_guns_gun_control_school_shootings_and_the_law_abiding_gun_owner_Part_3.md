---
title: Let's talk about guns, gun control, school shootings, and the "law abiding gun owner" (Part 3)
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QbXTDuwSVkk) |
| Published | 2018/09/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses pro-gun individuals, challenging their perspective on gun control and cultural attitudes towards guns.
- Questions the source of guns used in school shootings, pointing out the role of relatives or members of the gun culture.
- Criticizes the glorification of guns in social media and its impact on influencing violent behavior.
- Analyzes how guns have been perceived historically and how they have evolved into symbols of masculinity.
- Critiques the response to school shootings and the underlying attitudes towards discipline and violence.
- Clarifies the true intention of the Second Amendment and its historical context in relation to civil insurrections and government tyranny.
- Challenges the concept of being a "law-abiding gun owner" and its implication in government actions.
- Confronts the idea that violence is always the answer, debunking the association between guns and masculinity.
- Advocates for a shift in societal values towards true masculinity, integrity, and empathy to address gun-related issues.
- Emphasizes the importance of raising children with values that prioritize self-control and non-violence.

### Quotes

- "It's crazy how simple it is."
- "Violence is always the answer, right?"
- "Bring back real masculinity, honor, integrity."
- "Bring it back to a tool, instead of making it your penis."
- "You solve that, you're not gonna need any gun control."

### Oneliner

Beau challenges pro-gun individuals, deconstructs cultural attitudes towards guns, and advocates for a shift towards true masculinity to address gun-related issues.

### Audience

Gun owners, activists, parents

### On-the-ground actions from transcript

- Educate about responsible gun ownership and the true purpose of the Second Amendment (implied).
- Advocate for values of integrity, empathy, and self-control to address gun-related issues (implied).
- Engage in community dialogues about masculinity and violence to foster a culture of non-violence (implied).

### Whats missing in summary

The full transcript delves deeper into the historical context of gun ownership, societal attitudes towards masculinity, and the impact of violence on youth.

### Tags

#GunControl #Masculinity #ViolencePrevention #SecondAmendment #CommunityBuilding


## Transcript
Well, howdy there, internet people, it's Bo again.
And today is day three of our conversation about guns.
Now, if you're pro-gun up until now,
you probably had a whole lot to argue with.
Probably been nodding your head, yeah, yeah.
You might even think you heard a dog whistle in there.
Legislation's not really needed.
Sounds a lot like it's a cultural problem.
And we know what that means to most people in the gun crowd.
There is a cultural problem,
But it's not the one you're thinking of.
Where, when one of these kids shoots up a school,
where do you get the gun?
Nine times out of 10.
Relative, right?
Out of their gun cabinet?
Somebody in the gun crowd?
How many of you got a photo on Facebook dressed
like I am right now?
Got your plate carrier on, rifle.
Show you a killer, ready to kill over anything.
step on my flag, I'll kill you.
Can't imagine where some kid got the idea
that it might be okay to kill somebody
over something trivial, see, yeah, yeah.
You sit there and you throw that out there all the time.
You're ready to kill, ready to kill.
And then you're surprised when some kid
walks into a school, caps a bunch of children.
I wonder what that is, man.
I wonder what that shift was.
It's not the rifle.
Y'all know that.
That's the part about the gun crowd drives me insane.
Y'all know that it's not this.
Semi-automatics, they've been available to the civilian market for a hundred years.
It's only recently that this became an issue.
the same time this was no longer a tool and right now you're like no it's a tool
gun ain't gonna kill anybody by itself you're right it's a tool I don't see a
whole lot of pictures like this though no I can't I don't remember anybody no
it is a tool but it's the only tool you act like makes you a man and that's the
problem. That's what this has become. It's no longer just a tool. This is a symbol of masculinity.
This is manhood. Then think about your response to a school shooting. That kid just needed more
discipline, right? Go ahead and say what you mean though. Need to be smacked around a little bit
more, right? That's what you're really saying. Is it some kid been picked on a little bit? Maybe
feels a little bit emasculated because of it, picks up the symbol of masculinity, walks
into a school, solves that problem, because violence is always the answer, right?
It's crazy how simple it is.
And then you sit there, and right now there's people getting mad, because this is hitting
a little close to home, I imagine, and saying, well, I got a Second Amendment right, tone
of firearm.
You do. You do. Everybody. Everybody. The whole people were the exact words used by
the guy who wrote it. That's who the militia is. The whole people. It is an individual
right. But it has nothing to do with you taking a rifle like this and going down to the range
poking holes in paper to look cool. It has nothing to do with you hunting. It has nothing
to do with you defending your home from a burglar. It's not what the second amendment's
Where is it?
See, I'll know this, too.
You just ignore it.
Second Amendment had three real purposes, one of which we've been relieved of duty
of.
That's to put down civil insurrections.
That was the first one.
The militia of the whole, people could be called for it to put down civil insurrections.
Then, the other two, and there's no way to church this up, it is what it is, is to
kill government employees.
That's what the Second Amendment's there for.
One class of government employees would be foreign government employees in the sense
of an invasion and we're backing up the military as the militia, the whole people.
And the other is when the US government goes crazy, they become tyrannical.
They wrote that in there as like a safety valve.
We could fight back.
That's what the second amendment's there for.
Y'all know that though.
I know you know it, because you got stuff like this on your truck.
Stickers like this.
Come and take it.
The problem is, you've got it right next to a thin blue line sticker.
Who do you think is going to come take them?
Well, they're not going to come take them from you because you a law-abiding gun owner
is a term that makes my skin crawl.
Second Amendment has nothing to do with law-abiding gun owners, gentlemen.
Let's really think about this.
You can be a law-abiding gun owner and be completely complicit in the government's
tyranny. In fact, you have to be, right? If you're a law-abiding gun owner. They've got
you conditioned to say that. There's no great conspiracy to come take your weapons so you
don't fight back against the government they already know you're not going to because you're
a law-abiding gun owner. You can be a law-abiding gun owner at the same time you're pushing
people into ghettos in Nazi Germany. It doesn't mean anything. The whole point of the Second
amendment is that when the time came, if the time came, you wouldn't be a law-abiding gun owner.
But they got you whipped. They got you trained. Violence is always the answer, right?
Be a man. You don't believe that's it. Seen that meme on Facebook? It's got the bolt from one of
one of these. If your boyfriend doesn't know what this is, he's your girlfriend. It's
a symbol of masculinity now. It's not just a tool. Y'all made it that way. Y'all turned
it into that. And along with this, the violence. There's nothing wrong with this thing. It's
It's not this that's the problem.
It's the gun crowd.
Most of you guys are screaming about the second amendment.
Maybe we should start exploring other options to deal with problems than violence.
It's going to get worse, just so you know, because you've got to remember one thing.
One thing that's really important, a kid graduating high school this year hasn't seen a day of
peace in his entire life, been at war the entire time.
Violence is the answer.
They're heroes, right?
They got one of these, looks just like this.
They got one of these.
They're heroes, because they're solving their problems
because they're real men.
You want to solve these issues?
Bring back real masculinity, honor, integrity,
Looking out for those that are a little downtrodden.
You know, the things that all great,
masculine American men did.
Maybe shift this.
Bring it back to a tool,
instead of making it your penis.
Doesn't make you a man.
Not at all.
That's the biggest problem.
You solve that, you're not gonna need any gun control.
Because you got self-control.
You need to raise your kids a little bit better.
And it's not the other.
When we're talking about these school shootings, these mass shootings, oh it's the gun crowd.
Anyway, so there you go.
Y'all have a nice day.
Can't wait to see the inbox on this one.
I'll talk to you later.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}