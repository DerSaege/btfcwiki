---
title: Let's talk about guns, gun owners, school shootings, and "law abiding gun owners" (part 2)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wNtxtuQxUz8) |
| Published | 2018/09/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Gun control has failed due to being written by those unfamiliar with firearms, leading to ineffective measures.
- The term "assault weapon" is made up and lacks a concrete definition within firearms vocabulary.
- Bans on assault weapons focused on cosmetic features rather than functionality, rendering them ineffective.
- Banning high-capacity magazines is flawed as changing magazines can be swift, making the rush tactic infeasible.
- Efforts to ban specific firearms like the AR-15 could lead to more powerful weapons being used in its place.
- Banning semi-automatic rifles entirely is impractical, given their prevalence and ease of manufacture.
- Restrictions on gun ownership based on age, particularly raising the minimum age to 21, could be a more effective measure.
- Addressing loopholes in laws regarding domestic violence offenders owning guns could enhance safety measures.
- Legislation focusing solely on outlawing certain firearms may not address the root issues effectively.
- Beau stresses the need to look beyond laws and tackle deeper societal issues for a comprehensive solution.

### Quotes

- "Assault weapon is not in the firearms vocabulary."
- "A soldier can enlist in the Army, come home, and not be able to buy a gun."
- "Legislation is not the answer here in any way shape or form."

### Oneliner

Beau explains why gun control measures fail and suggests solutions like raising the minimum gun-buying age and addressing domestic violence loopholes, pointing out the limitations of legislation.

### Audience

Advocates, Activists, Legislators

### On-the-ground actions from transcript

- Raise awareness about loopholes in laws regarding domestic violence offenders owning guns (suggested).
- Advocate for raising the minimum age for purchasing firearms to 21 (suggested).

### Whats missing in summary

Full context and depth of Beau's arguments and solutions can be better understood by watching the complete video. 

### Tags

#GunControl #AssaultWeapons #DomesticViolence #Legislation #CommunitySolutions


## Transcript
Hey there internet people, it's Beau again.
This is going to be part two of the gun conversation.
If you didn't watch that first video, go back and watch it.
I know it's boring, but you need that information to understand what we're going to talk about
in this one, and that is gun control and why a lot of gun control has been an abject failure.
The main reason is that it was written by people that don't know anything about firearms.
So we're going to talk about gun control that has been tried.
control that is recommended and then we'll talk about something that might actually work
because a lot of the stuff that is like keynotes, like these are things they really want,
it's not going to work and it's not going to work and we can demonstrate why but we're also
going to come up with some solutions that might. Okay, got my toy AR-15 again. If you're not going
gonna go back and watch that first video as is highly recommended. This is a toy
but it works exactly like the AR-15. It's very functional, functional
representation. Okay, so what are the big gun control ideas? First one is to ban
assault weapons. Okay, tell me what one is and that's not a smart answer. Not being
a smart aleck when I say that. The reason the first assault weapons ban was an utter
failure is because there's nothing called an assault weapon. It's a made up term and
it's not a semantic argument which is, that's how it comes across in internet debates when
I see people talking about it. They think that it doesn't matter but it does. Now as
we learned in the last one, this is a semi-automatic rifle. This, the receiver, is what matters.
The assault weapons ban, didn't talk about this really.
The assault weapons ban set out criteria.
And the reason they had to was because the assault weapons
ban would be kind of like if Congress passed a law banning
zoomie cars.
And the automotive industry was like, what is that?
Because there's no word.
Assault weapon is not in the firearms vocabulary.
So when they set out that criteria,
And I'm going to give you actual criteria that they use
to classify something as an assault weapon that
has a collapsible stock.
That was one reason.
Now, the reason they have them is because soldiers
are different heights.
Therefore, the length it needs to be from the body
is different so you can get a good cheek weld.
That's one.
Doesn't have anything to do with lethality of the weapon.
Nothing.
Doesn't have anything to do with how it works.
nothing. Let's see, if it had a pistol grip, it was one. So if it was like this, this part
was the exact same. The receiver was the exact same. Same caliber, same rate of fire, everything.
But you held it like this instead of like this, it wasn't an assault weapon. Again,
doesn't make sense. A bayonet lug. You used to put a knife on the end of your gun. Because
you know there's a lot of criminals walking around stabbing folk when they're holding
rifle. With stuff like that, nothing that actually mattered. That's why it was a
failure. That is why it was a failure. Okay, so now that we understand how a
lack of knowledge can truly damage gun control, let's talk about the current
theories. The ones that people want. Ban high capacity magazines. Okay, 30 rounds
first off 30 round magazine looks like this. That's standard. That's not actually high capacity,
but let's say that it is. The idea is to cut it down, make it only 10 rounds, because then the
idea is that in a mass shooting, when the bad guy goes in bang bang bang bang, he can only get off
10 shots before he has to change magazines and then the good guys can rush him while he's changing
magazines okay when I hit this button I want you to start counting stop counting
when I pull the trigger okay that's it that's how long it takes to change a
magazine you can't rush somebody in that time and when the guy is changing
magazines that's a good time to get further away not closer to him that is
That's horrible advice.
That's going to get some kids killed, okay?
That is not the time to do it.
And if, you know, a lot of guys actually have, they've taped them, so they just switch them,
it's even faster.
Um, not, not a good idea.
More importantly, the magazine, it's a box, it's a metal box with a spring in it.
Two metal plates, one on each end.
One goes up and down, the other one stays put.
You can make them out of stuff from Home Depot.
It's not hard.
I'm actually horrible at it.
But I know a lot of guys that do it all the time.
There's not much to it.
It's not a piece that you're really going to be able to
regulate because there's nothing identifiable in it.
It's a spring and a couple of pieces of sheet metal.
It's going to be real hard to regulate.
So you're wasting a lot of energy, a lot of resources
trying to track something down that can be made.
And more importantly, it doesn't do any good.
The idea behind it is one of somebody that doesn't
understand how quickly you can change your magazines.
So that one doesn't work.
Really doesn't.
What about just banning the AR-15?
Just get rid of it by name.
Get rid of this thing, because it's the one that's used.
Oh, please don't do that.
Seriously.
Remember, the reason this is used
is because it's the most popular rifle in the United States.
It's popular.
That's why the kids have access to it,
because it's in their parents' home.
If you were to get rid of this, the next most popular rifles
are probably going to be a Garand or an M14.
And anybody who knows anything about firearms
just got a lump in their throat thinking
grand in a school because a grand, remember we talked about in the first
video, this is low intermediate power. A grand is not. It is high power. It is a
30.6 and in English what that means is there's no hiding behind a
locker. There's no hiding behind a door. Not really any hiding behind a center
block and if some kid sends a 30.6 around screaming down a
a crowded hallway, it's going to kill two or three people, one bullet.
So please don't, because the day a kid walks into a school with a grand, you're going
to be praying for these things to come back.
That's not it.
What about banning semi-automatic rifles altogether?
Okay, well, first, that's every rifle designed
pretty much in the last 100 years.
That's a big step right there.
More importantly, is that it's gonna be pretty ineffective.
I know people who said, well, they did it in other countries.
Well, they did, they did, and it kinda worked,
but not really, and we'll get to that.
Other countries didn't have more guns than people.
There's enough guns in the United States
to arm every man, woman, and child in it.
Of course, we have a gun problem,
but it's not because of the design
and the weapons that are out there.
These designs have been there for 100 years.
The gun problem's relatively new, so what else?
The other thing that people have to understand
is although these look high-tech, they're not.
They're not hard to make.
And that's the thing is, honestly, for me,
if I'm being honest, this, I can make this in this shop.
Maybe a day.
I was never good at this.
I can't get the timing right.
I don't know.
Just not a skill I have.
So there's that.
They're going to be readily available.
Now, the easiest way I can explain how these bands are not
going to work the way you think in the US is the M16.
Remember, we talked about the AR-15 is a civilian version.
The M16 is the military version.
That's the one that does this instead of that.
If you want to buy an M16 in the US, it's not banned.
Not really.
It's effectively banned.
It's still legal to buy one.
But you've got to get special permission from the ATF.
And it's going to run $20,000 to $30,000.
Now, semi-automatic, you can pick up in Walmart for like $600
the civilian version.
But the full auto, $20,000 to $30,000.
That's pretty expensive.
What if you want to buy one illegally?
$2,000 to $4,000.
$2,000 to $4,000.
No wait.
$2,000 to $4,000.
a tenth of the price. Guns are not like drugs. When you ban them they get cheaper, more accessible
to the people you don't want to have them. It's the exact opposite and that's something
we really got to consider. The idea behind gun control is to limit death, right? It's
get rid of that problem. If you ban them and this becomes a punishable offense just possessing
it, it's going to make people more violent when they have one. More importantly when
it comes to making them, it is easier to make this than it is this. So if you're going
If you're going to ban them, then the guys that are going to make them to fill that black
market, which is what prohibition does, it creates a market for it.
The guys that are going to make them, if they're both going to go to prison, if they're going
to go to prison for making either one, which one do you think they're going to make?
Logically, I think it's a horrible idea.
Now you're going to point to the bans in other countries and say, well, it worked.
They didn't really.
They switched the type of murder.
You look and all the statistics saying it worked, if you look and pay attention, it
says gun deaths or gun murders.
Look at the murder rate in these countries, it didn't really change anything, continued
the trend that they were on and that's pretty much all of them.
Well, it stopped mass shootings at least.
It did, it stopped mass shootings because guns are mostly gone.
They're still out there illegally, but they're mostly gone.
Kids aren't getting their hands on them.
Why don't you pull up the figures for arson?
Whole lot of people getting killed in arsons now.
Just changes the method, changes the means, doesn't actually end the problem.
Okay, so I've shot down a whole bunch of gun control ideas.
What are some that'll work?
There's got to be something that can be done, right?
Especially about school shootings.
I mean, this is important.
got to figure something out. There is and it's actually really simple. I mean
insanely simple and it got brought up after the Parkland shooting and
incidentally the Parkland shooter didn't use high-capacity magazines or what
they're calling high-capacity. He used 10-round magazines but after that
shooting somebody said well why don't we raise the age from 18 to 21? Yeah yeah
Yeah, that's a great idea.
That's brilliant, actually.
I mean, that's one of those things so simple it's going to
work.
Here's the thing, as it stands, an 18-year-old kid can
walk into a Walmart or a sporting goods store and pick
up something like this for about $600.
I'm sorry he's not going to, he's just going to get the one
that's already in his dad's cabinet, OK?
But those that do buy them, and it does happen, they can't.
out of school at 21. They're gone. And you say, well, it may turn it into a workplace
shooting. Maybe, but hopefully in those three years, he's grown up a little bit. Now,
the big objections to this, there were three, okay? Two of them aren't real objections.
One is, well, you know, so what you're telling me is that a soldier can enlist in the Army,
overseas fighting a war, come home and not be able to buy a gun.
You got your priorities mixed up.
Maybe instead of focusing on how to give an 18-year-old a gun when he comes back, we should
focus on stopping 18 and 19-year-old kids from getting shipped off to war.
I think that would be a much better solution.
Now there's also the complaint that, well, what if I want to go hunting?
I'm just a law-abiding gun owner.
I just want to go hunting.
See, that might work.
That argument might work with liberals that don't own guns, had never been around guns,
don't know anything about hunting.
Might work with them.
But you're talking to another redneck now.
You and I both know you've been hunting since you were 12.
Your parents bought that gun, right?
They bought you that, let's see, probably a Ruger 1022, put it in the cabinet.
That's what you started with.
And they let you use it, and they kept track of it, right?
Same thing.
Same principle.
It can still happen.
It's not going to stop anybody from going hunting.
That's stupid.
The only good argument I heard against it are those people who move out at 18 and need
a gun to protect themselves.
Yeah.
That's a risk.
It is.
I'm not going to lie.
for that one. I don't. But, this is what we're talking about. Now, here's the other thing.
Another one that needs to be addressed is people with domestic violence histories. And
right now the pro-gun guys are going, that's already a law. It is. It is a law. If you're
convicted of a domestic violence crime, even if it's a misdemeanor, you can't own a gun anymore.
Because, and the reason that was made is because domestic violence is a very
good indicator of future violence. So it makes sense. Now, the problem with that is that there's
a huge loophole. If the DA knocks the charge back to simple battery or anything other than a domestic
violence charge. Even if it was domestic violence, you don't get registered. Now the reason that
loophole exists is because law enforcement has a lot of domestic violence. And if a cop is
convicted of a misdemeanor crime of domestic violence, he can't own a gun anymore so he can't
be a cop. So they knock it back to some other misdemeanor. My question is why do we want those
types of people as cops. That's more of a reason to close that loophole, as far as I'm concerned.
So those are the two that I think would work. As far as regulating the actual design, stuff like that,
it's a lot like outlawing abortion. It's a lot like outlawing abortion.
It doesn't matter what your motives are, the reality is that it's going to happen.
It's going to happen.
There's nothing you're going to be able to do to change it.
When you make something illegal like that, you're just creating a black market.
What that means is that it's going to be the people you don't want that have the most
access and it's always been like that.
got to address some other issues rather than just laws. Legislation is not the
answer here in any way shape or form. Even those two that I recommended,
they're not going to do much. They aren't. They're going to mitigate and they're
going to help a little, but they're not going to solve the problem. We're going to
talk about truly solving the problem in the next video. So, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}