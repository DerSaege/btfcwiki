---
title: Let's talk about race and guns....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wXFtH3v2epI) |
| Published | 2018/09/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the division within the gun community between Second Amendment supporters and gun nuts.
- Second Amendment supporters aim to arm minorities to protect against government tyranny targeting marginalized groups.
- Second Amendment supporters can be blunt and use violent rhetoric, but their intent is to encourage self-defense.
- Describes a violent image popular within the Second Amendment community that contrasts with the gun nut philosophy.
- Differentiates between Second Amendment supporters who want to decentralize power and gun nuts who focus on might making right.
- Mentions a program by the Department of Defense as a way to tell the difference between Second Amendment supporters and gun nuts.
- Addresses the unintentional racism in some gun control measures, such as requiring firearms insurance disproportionately affecting minorities.
- Talks about the racial implications of banning light pistols, which were predominantly bought by minorities.
- Acknowledges the historical racial component of gun control but notes a shift due to awareness raised by Second Amendment supporters.
- Emphasizes the importance of arming minority groups for their protection under the Second Amendment.

### Quotes

- "The intent of the Second Amendment is to strike back against government tyranny."
- "Second Amendment supporters want to decentralize power, breaking up the government's monopoly on violence."
- "The Second Amendment is there to protect racial minorities."
- "Second Amendment supporters aim to arm minorities to protect against government tyranny targeting marginalized groups."
- "Y'all have a good day."

### Oneliner

Beau breaks down the division within the gun community, the racial implications of gun control, and the importance of arming minorities for protection under the Second Amendment.

### Audience

Advocates, Gun Owners, Activists

### On-the-ground actions from transcript

- Contact local gun stores to inquire about free training programs for minority and marginalized groups (implied).
- Advocate for surplus firearms distribution to low-income families in collaboration with community organizations (suggested).
- Raise awareness about unintentional racial implications of gun control measures targeting minorities (exemplified).

### Whats missing in summary

The full transcript provides a comprehensive overview of the intersection between race, gun control, and the Second Amendment, offering insights into the historical context and contemporary implications for minority communities.


## Transcript
Howdy there internet people, it's Bo again.
I thought we were done with the gun,
gun control conversation, but I guess not.
We've got a whole bunch of comments
in the comments section that have questions,
and I got some PM to me.
Some of them are really important.
Today we're gonna talk about gun control guns and race.
Because the two questions I got,
they're in direct contradiction to each other,
and they show exactly how muddled this conversation
gotten because of talking points. The first question was, is it true that most
Second Amendment supporters are racists? Oh no. Oh no no no. I'm sure that from
the outside looking in, the the gun community looks like one group of people.
I mean let's be honest, we all pretty much look the same. There's a clear
divide within the gun community. You have Second Amendment supporters, real
Second Amendment supporters and you have gun nuts or firearm enthusiasts, whatever you
want to call them.
A Second Amendment supporter wants minorities to have firearms.
In fact, they have outreach programs for that purpose.
The intent of the Second Amendment is to strike back against government tyranny.
If you understand that, odds are you understand that historically when a government becomes
tyrannical, it goes after marginalized and minority groups first. So a Second
Amendment supporter is very avid in their push to arm minorities. Now at the
same time, Second Amendment supporters are not known for being diplomatic. They
say things at times that are, let's just say, not tactful. Things like armed gays
don't get bashed. And when you hear that, you're like, man, what is wrong with this
guy? What he's hoping to do is jar you into buying a firearm. I mean,
it may sound bigoted, but it's coming from a good place. From where he's
standing, he's hoping that you're going to arm yourself so nothing bad happens to
It may not always come off that way.
Second Amendment supporters are more violent in their rhetoric.
In fact, this is a good one because it's violent rhetoric and it deals with race.
I caught a 30-day ban on Facebook because I shared a picture.
The picture, when you look at it, it's a black and white photo of a bunch of white
folk at a picnic.
It's what it looks like if you just glance at it.
The caption says, why do you need a 30-round magazine and the people at the picnic are
numbered 1 through 30?
Sounds crazy until you realize that's not a picnic, it's a lynching.
You can see the feet hanging in a tree and they're all looking at the camera smiling.
That is a very popular image within the Second Amendment community, not so much within the
Gun Nut community.
There's a difference in philosophy.
The Second Amendment crowd wants to decentralize power.
If everybody's armed, it breaks up the government's monopoly on violence.
They understand that the Second Amendment came into play to get the Civil Rights Act
passed.
The gun nut community is more about might makes right.
They're more about keeping that power for their end group.
You don't hear a lot about gun nut groups opposing legislation that is targeting minorities.
You do from the second amendment crowd, and we'll get into that.
But first I want to give someone that isn't within that community a quick way to tell
the difference between the two.
There's a program ran by the Department of Defense called the Civilian Markspenship Program.
They put surplus firearms out on the market pretty cheaply.
I think the Army should give those surplus firearms to citizens.
Gun nuts and Second Amendment supporters alike can be like, yeah, and start with low-income
families.
Second Amendment supporters, yeah.
Gun nuts?
Whoa, whoa, whoa, hang on now a minute.
Let's just, let's think about this.
Don't you think that would make the gang problem worse?
They will find a way to tactfully remind you that you'd be arming minorities.
That sentence has never let me down.
I've been using it for years and people out themselves very quickly when you say that.
When you're walking into a gun store, you can even see that divide there.
There are a lot of gun stores that provide free training to minority groups or marginalized
groups.
Those are Second Amendment gun stores.
You can walk into other ones and see the rebel flag hanging.
Those are gun nuts.
There is a huge difference.
The other question I got was, is gun control low-key racist?
No.
In some cases it's very racist.
In some cases it's unintentionally racist.
And in some cases it's not racist at all.
A good example of unintentionally racist is the push to have firearms insured.
Doesn't seem like it would be racist.
The idea behind it, in case you don't know, is that if insurance companies insured firearms,
they would keep track of their client, if their client started showing signs of becoming
an active shooter, they'd revoke his insurance, therefore he can't have a firearm anymore.
Makes sense, but let's look at a wider implication to it.
Who can't afford the insurance?
Low-income families, right?
Who's going to be disproportionately affected by that?
Pretty quick, it starts to come into focus.
Yeah, that's who's going to lose out.
It's going to disproportionately affect minority groups.
And again, from a Second Amendment standpoint, we want them to have firearms more than just
about anybody.
Who needs it more from a second amendment standpoint?
The black guy in the working class neighborhood or the white dentist in a gated subdivision.
One that was very racist was there was a push to ban light pistols.
By light I mean their weight, how much they weighed.
doesn't sound racist, doesn't seem to make sense, but it doesn't sound racist.
Here's the thing, light pistols are made with less expensive metal, so they're cheaper.
We called them Saturday Night Specials back in the day, I don't know if that term is
still around, but basically these things are pretty much exclusively bought by minorities
in urban areas. You know, if you're poor white folk, you're probably going to buy a cheap
shotgun. It was very targeted towards a very specific demographic of people. The Second
Amendment crowd lost their minds, raised a huge stink, it got defeated. You didn't hear
much from the gun nuts, though. They didn't really oppose it because it wasn't targeting
their end group. It wasn't targeting the people they want to have power. Now, historically,
gun control has had a racial component. I don't think that it is as big a problem today as
it used to be. And I think that part of that is because the Second Amendment crowd has
done a good job of pointing it out. Anytime it comes up, they're very quick to say, so
So you just want to take guns away from these people?
And there are very few congressmen and senators today that are willing to take that kind of
heat.
And to give you a good example of how the gun nut crowd will even idolize somebody who
has supported gun control, if it's targeting minorities, is Ronald Reagan.
He's seen as like Mr. NRA.
Maybe look at some of the gun control he signed because it was directly influenced because
of the Civil Rights Movement.
And so there is a marked difference.
Like I said, I don't think supporters of gun control today have that racial bias necessarily.
I think that they haven't thought about it.
I don't think they've ever really put it in that view.
They're looking at it from a very different lens, so they don't see it like that.
And a lot of them, you have to understand, a lot of them have no idea that the intent
of the Second Amendment, maybe not when it was written because it didn't apply to them,
but it's to arm minority groups.
When it was written, it was more about ideological minorities, as we include more and more people
in America's blanket of freedom, today it is more important for a racial
minority to be armed than just about anybody else. The Second Amendment is
there to protect them. It's supposed to be, anyway. You know, there's that
poem. You know, first they came for the Jews. I didn't say anything because I
wasn't a Jew. It goes on and on. Second Amendment supporters want to make sure
that if the government ever comes from minorities in the U.S., nobody has to say
anything because it's going to get so loud everybody's going to know. So there
there's a primer on race, guns, and gun control. Now there's a whole lot more to
this and there are there is a very sharp historical trend with with gun control
and racism I don't I don't see it as much today I really don't maybe I'm just
not looking at it through the right lens but most of what I see today even when
it does have a racial component to it most times it seems to be very
unintentional and it seems to be people not realizing what it's going to do and
then yeah no second amendment supporters believe me even when they're saying
something rude they're doing it with the intent of getting you to buy a gun so
Not, not the, not,
not the actions of a Klansman.
Y'all have a good day.
Y'all have a good day.
Okay.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}