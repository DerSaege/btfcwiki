---
title: Let's talk about what it's like to be a black person in the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WD8mWq0Hdcw) |
| Published | 2018/09/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Unexpected wide reach of a Nike video led to insults and reflections.
- Insult about IQ compared to a plant sparked thoughts on understanding being Black in the U.S.
- White allies may grasp statistics but not the true experience of being Black.
- Deep dive into cultural identity, heritage, and pride.
- Historical context of slavery and its lasting impacts on cultural identity.
- Reflections on cultural identity being stripped away and replaced.
- Food, cuisine, and cultural practices as remnants of slavery.
- The ongoing impact of slavery on cultural scars.
- Call to acknowledge history and the need for healing.
- Hope for a future where national identities are left behind.
- Acknowledgment of uncomfortable history and the lack of pride for some.
- Recognition of the significance of the Black Panther movie in providing pride.
- Encouragement to truly understand the depth of the issue beyond surface-level talks.
- Acknowledgment of cultural identity loss and the need for reflection and understanding.

### Quotes

- "Your entire cultural identity was ripped away."
- "They have black pride because they don't know."
- "That's a whitewash of the reality."
- "How much of who you are as a person comes from the old country."
- "It's just a thought."

### Oneliner

Unexpected reach of a Nike video led to deep reflections on cultural identity, heritage, and the ongoing impacts of slavery on collective identity, urging understanding beyond surface-level statistics.

### Audience

White allies

### On-the-ground actions from transcript

- Acknowledge and understand the deep-rooted cultural impacts of historical events (implied).

### Whats missing in summary

The full transcript delves deep into the ongoing effects of slavery on cultural identity, urging a reflective understanding beyond statistics and surface-level knowledge.

### Tags

#Understanding #CulturalIdentity #Heritage #Slavery #History


## Transcript
Well, howdy there, internet people, it's Bo again.
So that Nike video, that went a little bit wider
than I expected.
A lot of people saw it that I didn't count on.
That was supposed to be a joke for you guys
that normally watch this.
Didn't count on the extra couple hundred thousand people
seeing it, but I'm glad they did,
because they don't know the way I talk.
They don't know that I tend to switch
what I'm talking about in the middle of a video,
so a lot of them saw the first few seconds,
took off to the comments section, started insulting me.
all okay was all worth it because one guy in his insults got me thinking and I
had time to think about it because I was coming back from coming back from town
that's like 40 minute drive the insult was that I was my IQ was two points
higher than a plant and that I needed to or asked if I could open my hick mind
and understand what it was like to be a black person in the United States no
I can't. Neither can you. Neither can you. I know right now there's some white
ally out there learning the terminology that say, no I understand. No, you don't.
You may get the statistics, you may understand the numbers. You may
get that there's a huge disparity in the criminal justice system and
because of that it translates into more unjust violence of being visited upon
them, you may get that. You may understand that there's a huge income equality gap.
And you may get that that translates into a lower standard of living, lower health care. You may
understand all of that. Doesn't mean you understand what it's like to be black in the U.S. It means
you get the numbers. Maybe you understand a little bit of the history. See, it's way deeper than that
because you know when we think about slavery we think about the physical costs of slavery
and they're horrible no doubt but I think today what's more relevant is something else.
Where are you from? Not in the U.S. Where are you people from?
Odds are, if you're white, you know Ireland, Scotland, England, Germany, Italy, Greece,
France, where you from?
And based on your answer, I can tell you all kinds of things about you.
I can tell you whether your gravy is red, brown, or white.
I can tell you what kind of booze you probably drink.
Tell you what kind of food you eat.
what cuss words you use. It translates into a lot of things. The values you instill in
your kids, the jokes you tell, the biases you have. My kid's four years old. One of
them. He's four years old. If I look at him and I say, ooh ah, he's going to go up the
raw. Now if you're Irish you just laughed. If you're English you probably got a little mad
because that's part of your cultural identity. If you're not even one of those you have no idea
what that means. Four years old. Kid doesn't know where Ireland is. Couldn't point to it on a map
but it's already starting to get ingrained in him. He's already being able to take pride in
that cultural identity. Why don't we have Bantu pride, Congolese pride? Why don't we have that?
See, the sociologists, you know, they like to point to Black pride and say, oh well it's a
healing endeavor so, you know, Black people can get together and they, you know, they can join in
that struggle. That's a cop-out. That is a cop-out. That's a whitewash of the reality.
The reality is that slavery ripped that from them.
They have black pride because they don't know.
I don't know.
It wasn't in the slave owner's interest to keep track of the cultural heritage of his
property.
So that cultural identity that shapes you, it does.
Think about it.
How much of what you are as a person is linked to your heritage like that?
Stripped away, gone, and it got replaced.
It got replaced.
You know, down here in the South, especially out in the country, you go into a black kitchen,
somebody is going to know how to cook hog drives and chitlins.
pigs feet. Sit down here, that scene is black cuisine. That didn't come from Africa. Not
the way pasta or shepherd's pie came with us. That came from the plantations. That's
what the slave owner threw away and let them eat. They endured that for so long it became
part of their culture. They endured it for so long they got good at it, found a way to
make it flavorful? No. I can't understand what it's like to be black in the U.S. I
can't understand what it's like to have my cultural identity stripped away and
replaced with the knowledge that when my granddad was a kid people didn't think
he was human or that what is seen as my cuisine is because we got good at cooking
trash. That's going to affect a people, a collective people on a very deep level.
You know, when you say get over it, we're a long way away from that. We are a long
way from getting over slavery. Yeah, the physical scars, they've healed, they have.
Cultural ones are still there, because it's gone, it was stripped away, just vanished
in the air.
And I really want you to think about how much of who you are as a person comes from the
old country.
And then imagine what it would be like if you didn't have that.
if what it was replaced with wasn't love with the red, white, and blue because the
red, white, and blue didn't love you. It wasn't that long ago, guys. There are
people alive today that dealt with some pretty horrible stuff.
And it's getting better, but it's not solved. That problem's ongoing and it's
That's going to stay on going for a while.
You know, I've seen people say that they're not African American, that they're just American.
And I love it.
Good for you.
I cannot wait until the day comes when all of us just leave those national identities
behind and we're just people.
But that's a long way off.
And until then, we're going to have to address our history.
So while we may find it uncomfortable to address our history, because we don't like it, we
don't like the things that our people did, understand there's some people that don't
know theirs.
They don't have that source of pride to reach back to.
I actually think that's why that Black Panther movie did so well.
It's just like, you know, just like every European ethnicity has that origin myth, you know, it gave them that.
So, it's just a thought.
It's something to think about when you have convinced yourself as a white person that you truly understand what it's
like.
take a step back. It goes a lot further than having to have a talk with your
kids about how to deal with cops. Your entire cultural identity was ripped away.
Anyway, y'all have a nice night. Thanks for the insult because it gave me
something to think about.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}