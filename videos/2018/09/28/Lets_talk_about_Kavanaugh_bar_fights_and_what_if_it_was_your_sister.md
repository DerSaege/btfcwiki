---
title: Let's talk about Kavanaugh, bar fights, and "what if it was your sister?"....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FngaU-CK0Zk) |
| Published | 2018/09/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts a story from college where he intervened in a potentially dangerous situation at a bar to protect a girl he vaguely knew from being harassed by a guy and his friends.
- Draws parallels between his bar incident and the current atmosphere around the Kavanaugh hearing, discussing the importance of respecting women's autonomy and agency.
- Uses a hypothetical scenario with friends to illustrate the prevalence of sexual assault among women and how comments dismissing survivors can affect their willingness to come forward.
- Criticizes the double standards and misconceptions surrounding men's emotions and reactions to trauma, contrasting them with the Kavanaugh hearings.
- Emphasizes that sexual assault is about power and control, pointing out the significance of survivors speaking out against someone who may wield immense power.
- Urges reflection on the impact of dismissing survivors and perpetuating harmful narratives, stressing the lasting damage it can cause to relationships.

### Quotes

- "You might not want to do it again because you're altering real-life relationships over bumper sticker politics."
- "Sexual assault is normally about power and control."
- "You might not want to do it again because you're altering real-life relationships over bumper sticker politics."
- "You already ruined that."
- "How many of them do you know? How many of them talked about it with you?"

### Oneliner

Beau recounts a bar incident to illustrate respecting women's autonomy, drawing parallels to the Kavanaugh hearing's impact on survivors and urging reflection on harmful narratives.

### Audience

All genders, allies

### On-the-ground actions from transcript

- Reach out to survivors in your community, offer support, and believe them (implied)
- Educate yourself and others on the prevalence of sexual assault and its impact (suggested)

### Whats missing in summary

Importance of believing and supporting survivors, challenging harmful narratives, and fostering a culture of respect and accountability.

### Tags

#Respect #BelieveSurvivors #SexualAssaultAwareness #GenderEquality #CommunitySupport


## Transcript
Well, hi there, internet people, it's Bo again.
I'm gonna start off tonight with a story.
You know, back when I was in college,
yeah, Hillbilly went to college,
he was at a bar called Eight Seconds.
Come walking out of the bathroom and I hear this voice,
vaguely recognized, so I look over,
and it's this girl I know, kinda.
She's friends with my roommate, like, we know each other
but don't have each other's phone number, type of thing.
And I hear her saying, you know,
No, I'm not interested.
I'm just waiting for my friends.
And there's this guy I leaned over, got her kind of hemmed
up in the corner.
And so I slow down, take a sip of my beer, and lean up
against this railing.
And I'm looking, and she tells him a couple more times to
kind of back off.
And I see it's him, and then he's got his two buddies right
next to him at the bar.
But I do have that beer bottle.
So you know what I did, threw that beer bottle up the trash.
I'm not stupid, I'm not going to fight three guys,
throw the beer bottle in the trash, walk over there.
Don't have my drink yet?
You can see the look of relief on her face.
I turn to him, shake his hand.
And he's like, oh, nice to meet you.
Who are you?
And I say I'm her brother.
At the same time, she says, he's my boyfriend.
And he gets this puzzled look on his face.
And I'm like, we from West Virginia.
And he kind of laughs and turns around and walks off.
I wonder why.
He's been real pushy up till now.
It's interesting.
I mean, I know I'm John Wayne's son and can fight a dozen guys
at one time, but he don't know that.
I mean, I'm not a little guy, but I'm not a big guy either.
It wasn't that.
He wasn't intimidated, as much as I'd
to pretend that was the reason. The real reason is that he respected my property rights over her
than he respected hers as a person. It's really what it boiled down to.
Why not tell you that story? Because it's very relevant today. I'm watching the comment section
about the Kavanaugh hearing and I realized about the only way anybody's getting through to one of
of these guys that's saying that this is just all a lie and these women are liars
is by saying what if this was your sister, your daughter, your cousin,
whatever. It's that same thing that property relationship. So I'm gonna do
it a different way. What if it's your friend and we're gonna do some math.
Stick with me on this. We're gonna take whatever number of friends you have, you
You do the same math I'm doing, okay?
We're gonna say you got 500 friends.
Divide it by two,
because odds are it's 50-50, men and women.
Get rid of that half, so you got 250 left.
Getting rid of the men.
Now what if I tell you one out of 10 women
have been sexually assaulted?
Means 25 women on your friends list
have been sexually assaulted.
Did you know that?
Did you know all of them?
they kind of keep it quiet. 25 that's a scary number one out of ten. Scary. It's
even scarier because the real number is one out of five so double whatever number
you came up with and that's the real number. It's 50. Got 500 friends. So that's
50 women that saw your comments, why don't they come forward? You, if you made those
kind of comments, knowing nothing about what happened, you're the reason they don't
come forward. If you made those kind of comments, man, can you imagine that? Seeing your friend,
Somebody you considered a friend probably altered those relationships forever.
They're never going to look at you the same way.
That's why they don't come forward.
You know, it's interesting because we just had that Catholic priest case.
All those boys, now men, 35 years later coming forward, nobody asked them why they took so
long to come forward. Well, it's because men aren't emotional, right? They aren't
as prone to making stuff up. Aren't emotional. You believe that, you obviously
didn't watch a Kavanaugh hearing. I've never seen a man go from crying to
screaming to crying that fast in my entire life. He's an emotional rat.
Probably not fit to be a judge. You need to have a level head to do that. And by
emotional, I mean really. You know, I've never seen my wife behave like that.
She'd been pregnant a lot. Hormones going crazy. She never did anything like that.
But, you know, why didn't they come forward? Or, why did they come forward now?
Like they're just out to ruin this man's career.
It's interesting. If you were sexually assaulted, I imagine you'd want to know a little bit about it.
Probably learn a bit about it, read about it. First thing you're going to find out, sexual assault,
normally about sex. It's about power and control. Man, so if you see somebody that
took that power and control away from you, stripped it from you, and realize
they're about to be able to exercise power and control over 150 million other
women via the Supreme Court, man that seems like a powerful motivator to come
forward, doesn't it? And then once the first one came forward, just like in that
Catholic priest case, another one came forward. Then another one. Because they're
not out there alone, facing your comments. Facing those comments. It's crazy. It is
crazy. So the key takeaway from this, because I know it's not gonna do anything
with this particular incident, because you're too entrenched. You know, you've
already said too much, you can't take it back, and you don't want to look like
you're wrong, so you stick with it. You don't keep saying they're liars, I
understand that. Too much pride. Got it. You might not want to do it again because
you're altering real-life relationships over bumper sticker politics by
proclaiming you know what happened when you don't and by being the very reason
they don't come forward. So do the math and then think about it. How many of them
do you know? How many of them talked about it with you? I don't think any of
them will now because they're not gonna trust you. You already ruined that. Anyway
Y'all have a good night, just something to think about.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}