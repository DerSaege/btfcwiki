---
title: Let's talk about the Dallas shooting...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6-d4C-sIlaI) |
| Published | 2018/09/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses skepticism towards the official narrative of the shooting in Dallas, referencing experience with liars and motives.
- Raises concerns about the shooting being a potential home invasion due to new information about noise complaints.
- Points out the potential legal consequences in Texas for killing someone during a home invasion.
- Questions the lack of intervention or opposition within the police department regarding potential corruption and cover-ups.
- Connects the lack of justice in cases like this to the Black Lives Matter movement and the value placed on black lives.
- Criticizes the actions of the police in searching the victim's home posthumously for justifications.

### Quotes

- "There's two kinds of liars in the world."
- "I'm not a lawyer, but that sure sounds like motive to me."
- "If this is the amount of justice they get, they don't. They don't."
- "Not one cop has crossed the thin blue line to say that's wrong."
- "It doesn't matter what else you do."

### Oneliner

Beau questions the official narrative of a shooting in Dallas, pointing out potential motives and legal consequences, while criticizing police actions and lack of justice for victims, and connecting it to the Black Lives Matter movement.

### Audience

Community members, justice seekers, activists.

### On-the-ground actions from transcript

- Speak out against police corruption and cover-ups within your community (implied).
- Support movements like Black Lives Matter by advocating for justice and equality (implied).

### Whats missing in summary

The emotional impact and depth of analysis provided by Beau during his commentary.

### Tags

#Justice #PoliceCorruption #BlackLivesMatter #HomeInvasion #CommunityPolicing


## Transcript
Well, howdy there, internet people.
It's Bo again.
I wanna talk about that shootin' in Dallas.
You know, I'm not a super cop.
Like, I'm sure they got down there workin' this case
in Dallas, I'm sure they got these highly trained
investigators down there.
But I watched me a little bit of Andy Griffith,
and I got a little bit of experience with liars.
There's two kinds of liars in the world.
There's a bad liar, and a bad liar you gotta ask him
that question over and over and over again
to finally get to the truth.
good liar, a prepared liar, they're going to tell you the truth right up front, at least
a little bit of it. Makes it easier to lie if you put a little bit of truth in it. So
let's talk about what they say happened. She worked a long day. She's tired, confused,
and I've been there. You know, I work long days and you get grumpy. I get you. And I've
work long enough days where I've been a little bit disoriented. It happens. So she
gets home, goes to the wrong apartment, walks in because the door is open because
you know there's a lot of cops go around leaving their door open and goes inside
there's somebody there gives them commands they don't follow them so she
kills them. Okay but see today we found out something new according to these
newspapers, this lady's filed noise complaints against this guy before. So
let's go back to that story. She's worked a long day. She's tired. Probably wants to
go home, go to sleep. A little bit grumpy maybe. And she's filed noise complaints
against this guy before. I'm not a lawyer, but that sure sounds like motive
to me. Maybe he's making a little noise. She goes up, knocks on the door, says let
me in like the witnesses say. You know, it's not open because those doors close
automatically at that place. He opens the door and basically tells her to get lost.
She doesn't like that, so she kills him. That makes a whole lot more sense. But I mean,
I don't know. I'm not a cop. But there's one other thing bothering me about this.
Let's say I'm tired and I've got a gun.
I just want to go to sleep and I walk in your house and start giving you commands.
There's a word for that, two words actually, it's called a home invasion, it's called
a home invasion.
Her being a cop doesn't make her walking in this man's house lawful, even if the
door was open. When she resorted to violence, assaulted him, it's a home
invasion. So here's the thing, in Texas they don't have murder one. Different kind
of system there. But you know what happens? If you kill someone during a
home invasion in Texas? You're facing capital murder. Capital murder. What that
means is you're looking at execution. All of a sudden makes a whole lot of sense
why these cops are acting this way doesn't it? In his house unlawfully
assaults him. It's home invasion. Again, I don't know. I'm not a lawyer, not a cop.
Years ago, I talked to a guy named Kevin Crosby. He's a panther. It was him and a
whole bunch of white people. We're sitting around talking and the subject of
Black Lives Matter and All Lives Matter came up.
And he asked, and basically going around in a circle
answering his question.
You know, it gets to me, and I'm like, well,
of course Black Lives Matter.
I'll never forget it.
Smart guy.
Do they?
That's what he said.
Just the way he said it, too.
We're going to find out right now.
We're going to find out in texts in Dallas.
We're going to find out if black lives matter.
Because if this is the amount of justice they get, they don't.
They don't.
Now here's the thing.
One last little thing that's bothering me.
They searched this man's home afterward,
trying to find a reason to have murdered him.
I live in the South, man.
Police corruption down here, it's a sport.
I've never seen anything like that.
And what bothers me, not that it's done.
I mean, everybody's got friends, and friends
are going to cover for friends.
Dallas PD is a big organization.
Not one cop has crossed the thin blue line
say that's wrong. Not one. You know you guys always want to say it's a few bad
apples. See the thing is that saying it's not a few bad apples makes the others
look bad. It's not a few bad apples you know and you can just throw those away
and everything will be okay. It's a few bad apples spoils the bunch and that
That certainly appears to have happened in Dallas because if you're wearing one of those
uniforms and you let this slide, you're just as crooked as they are.
It doesn't matter what else you do.
You're trying to slander and villainize an unarmed man that was gunned down in his home.
You all need to think about that.
That is why people don't back the blue anymore.
That is why you're not looked at like Andy Griffith.
That's why people hate you, plain and simple.
Yeah, the idea that you enforce laws that aren't necessarily just has something to do
with it.
the fact that you're crooked has a whole lot more to do with it. Anyway, y'all have
a good night. Just something to think about, just a theory, and I really hope
there's some justice in this one.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}