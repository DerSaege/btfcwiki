# All videos from 2018
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14

Click on a video summary for some quotes, a point by point summary, and some possible actions for the topic.
2018 - [2019 &gt;](/videos/2019/all_videos.md)

## December
<details>
<summary>
2018-12-31: Let's talk about MS-13.... (<a href="https://youtube.com/watch?v=KM977L7pf0o">watch</a> || <a href="/videos/2018/12/31/Lets_talk_about_MS-13">transcript &amp; editable summary</a>)

MS-13, a product of American foreign policy, questions the need for a wall and advocates curbing violence at its source.

</summary>

"MS-13 is an American creation, not just in the sense that it was founded here, but in the sense that every step of the way, it was American foreign policy that built it."
"Tell me again why this is the reason we need a wall?"
"Maybe a better idea would be to curtail our foreign policy instead of building a wall."
"You never hear them calling for strict scrutiny or vetting of those people that the U.S. military trains to murder, torture, assassinate, and dominate a community by fear."
"Stop the problem before it starts."

### AI summary (High error rate! Edit errors on video page)

MS-13 has become a boogeyman, leading to calls for building a wall.
Started in California among Salvadoran refugees as a street gang for protection.
Civil war in El Salvador ended, and the US sent gang members back.
The US military trained Ernesto Deris, turning MS-13 into a criminal enterprise.
The School of Americas, linked to many atrocities, could be where Deris was trained.
MS-13 is an American creation due to US foreign policy at every step.
Beau questions the need for a wall and suggests curbing foreign policy instead.
Criticizes the lack of scrutiny on those trained by the US military for violence.
Urges stopping the problem at its source to prevent violence and fear.
Beau advocates for a shift in approach to address the root causes of issues like MS-13.

Actions:

for policy makers, activists, concerned citizens,
Advocate for changes in foreign policy to address root causes of issues like MS-13 (implied)
Support organizations working to prevent violence and support communities affected by gang activity (suggested)
</details>
<details>
<summary>
2018-12-28: Let's talk about the record breaking numbers of illegals and the wall gofundme.... (<a href="https://youtube.com/watch?v=6LAhmB0vzFI">watch</a> || <a href="/videos/2018/12/28/Lets_talk_about_the_record_breaking_numbers_of_illegals_and_the_wall_gofundme">transcript &amp; editable summary</a>)

Beau challenges the irrationality of spending $5 billion on a border wall for a non-existent issue, criticizing the lack of critical thinking in politics and society.

</summary>

"We are people who elect rulers instead of leaders."
"We are going to spend five billion dollars to address a problem that doesn't exist."
"It shows, one, you don't know how the government works, and two, there's actually a word for a blending of private business interests and the government. It's called fascism."
"So the wall, assuming it works, will actually pump more money into a criminal cartel that is destabilizing our next-door neighbor."
"Nobody in DC is going to look out for you."

### AI summary (High error rate! Edit errors on video page)

Border Patrol uses the number of border apprehensions as a gauge for the influx of immigrants, which is flawed due to technological advancements.
Despite claims of record-breaking illegal immigration, the numbers are actually at a 40-year low.
Trump's immigration policies may not be the reason for the low numbers, as they have remained relatively consistent for the past eight years.
The US is no longer a beacon of freedom and critical thinking, electing rulers instead of leaders.
The government's plan to spend $5 billion on a wall to address a non-existent problem is irrational.
Lack of critical thinking is evident as people donate to the GoFundMe for the wall without understanding government processes.
Beau humorously suggests starting a GoFundMe to audit the funds raised for the wall through the IRS.
Trying to influence politicians through private donations is concerning and hints at fascism.
Beau challenges the idea of the wall being effective, pointing out flaws in the plan and how smugglers could adapt.
The fear-mongering around issues like MS-13 is used by politicians to manipulate public opinion and gain support for ineffective policies.

Actions:

for critical thinkers, voters, activists,
Start educating yourself on government processes and policies (suggested)
Support and donate to organizations working towards comprehensive immigration reform (implied)
Engage in critical thinking and fact-checking before supporting political initiatives (suggested)
</details>
<details>
<summary>
2018-12-24: Let's talk about the war on Christmas.... (<a href="https://youtube.com/watch?v=pj-2YGiYM7c">watch</a> || <a href="/videos/2018/12/24/Lets_talk_about_the_war_on_Christmas">transcript &amp; editable summary</a>)

Beau explains the war on Christmas, urging for actions that embody love and compassion, not just words.

</summary>

"There is definitely a war on Christmas."
"Be loving in truth and deed, not word and talk."
"Do you think people look at you and say, man, I want to be like them?"

### AI summary (High error rate! Edit errors on video page)

Explains the concept of the war on Christmas and clarifies that it is not a joke video.
Recounts the story of the Good Samaritan from the Bible, focusing on the message of loving thy neighbor.
Draws parallels between the actions of Christians during Christmas and the teachings of loving thy neighbor.
Criticizes the focus on building walls instead of helping those in need, contradicting the message of the Good Samaritan.
Emphasizes the importance of showing love through actions, not just words.
Questions whether one's actions make others want to emulate them or avoid them.
Notes that the war on Christmas is not about holiday cups but about embodying the spirit of love and compassion.
Extends warm holiday wishes to people of different religions, acknowledging their celebrations.

Actions:

for christians, holiday celebrators,
Extend warm holiday wishes to people of different religions (exemplified)
</details>
<details>
<summary>
2018-12-24: Let's talk about Rule 303.... (<a href="https://youtube.com/watch?v=HbGKzleLJVc">watch</a> || <a href="/videos/2018/12/24/Lets_talk_about_Rule_303">transcript &amp; editable summary</a>)

Beau explains the concept of "Rule 303" and challenges law enforcement officers to fulfill their duty to protect without excuses or prioritizing self-preservation over others.

</summary>

"Rule 303 meant having the means at hand and therefore the responsibility to act, not might makes right."
"Your duty is not bounded by risk. Your duty is not to get up and go to work every day. The guy at Hardee's has that duty."
"They care about the students. That's it."
"The gun makes the man. Don't you think that might be reinforcing the thought process of those kids that go into schools and shoot the place up?"
"If you cannot truly envision yourself laying down your life to protect those kids, you need to get a different assignment."

### AI summary (High error rate! Edit errors on video page)

Explains the origins of "Rule 303" among military contractors, originating from a historical incident involving a soldier using his rifle caliber as justification for actions.
Emphasizes that "Rule 303" meant having the means at hand and therefore the responsibility to act, not might makes right.
Provides examples of how different situations have applied the concept of "Rule 303," including military interventions and law enforcement responses.
Criticizes the excuses made by law enforcement officers who fail to act in critical situations, such as the Parkland shooting.
Argues that duty is not bounded by risk and that officers have a responsibility to act, especially in protecting children.
Challenges the notion that equipment superiority should deter action, stressing the importance of fulfilling one's duty regardless of perceived risks.
Questions the message sent by prioritizing self-preservation over protecting those in need.
Urges school resource officers and law enforcement personnel to reconsider their roles if they are not willing to lay down their lives to protect others.

Actions:

for law enforcement officers,
Reassess your commitment to protecting others if you are in law enforcement and prioritize self-preservation over duty (implied).
</details>
<details>
<summary>
2018-12-22: Let's talk about Marines, cops, Kurds, and Mattis.... (<a href="https://youtube.com/watch?v=nCEToTsbcT8">watch</a> || <a href="/videos/2018/12/22/Lets_talk_about_Marines_cops_Kurds_and_Mattis">transcript &amp; editable summary</a>)

Beau delves into the concept of duty through the lens of recent events, from Mattis's resignation to the inherent lack of obligation for law enforcement to protect citizens, urging a reevaluation of governmental roles and citizen empowerment.

</summary>

"Law enforcement is not there to protect you. We do not live under a protectorment."
"Government is there to control you, not protect you."
"Do some cops save lives? Yes, but they do it in spite of the law, not because of it."

### AI summary (High error rate! Edit errors on video page)

The theme of duty is prevalent in recent news, particularly with the ruling from a federal court in Florida stating that law enforcement has no duty to protect individuals.
General Mattis's resignation is believed to be driven by his strong sense of duty, as described by other Marines who worked with him.
Mattis's disagreement with President Trump on withdrawing from Syria stems from his belief that it leaves allies, like the Kurdish people, vulnerable.
The Kurdish people, an ethnic minority spread across countries like Turkey, Syria, Iraq, and Iran, are seen as being left without support once again by the U.S.
The U.S. historically has not supported Kurdish independence due to geopolitical reasons and the Kurds' potential to destabilize the region.
The imminent U.S. withdrawal from Syria may present an uncertain but possibly opportune moment for Kurdish independence efforts.
Law enforcement, as per existing case law, is not mandated to protect individuals, even in extreme scenarios like mass shootings.
Beau underlines the idea that law enforcement is part of a government meant to control rather than protect citizens.
Despite some officers' acts of heroism, there is no legal obligation for law enforcement to protect individuals.
Beau concludes by urging people to be mindful of the government's role and the implications of legislation that may weaken citizens' ability to protect themselves.

Actions:

for citizens, activists,
Connect with local community organizations for support and advocacy (implied)
Educate others about the true role of law enforcement in society (implied)
Support legislation that empowers citizens to protect themselves (implied)
</details>
<details>
<summary>
2018-12-21: Let's talk about what we can learn from internet tough guys.... (<a href="https://youtube.com/watch?v=mY1qVWuFZDY">watch</a> || <a href="/videos/2018/12/21/Lets_talk_about_what_we_can_learn_from_internet_tough_guys">transcript &amp; editable summary</a>)

Beau delves into the parallels between the internet tough guy and American foreign policy, addressing insulation from consequences and the role of individuals in driving global change.

</summary>

"When we see it on TV, we turn it off. That's happening somewhere over there."
"Money makes the world go round, right?"
"It does have to be somebody from here."
"At this stage in the game, the way everything is interconnected, any revolution that is not global, it's not a revolution."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Analyzing the internet tough guy as a symptom and analogy for American foreign policy.
The internet tough guy's behavior stems from being insulated from consequences, much like American foreign policy.
Americans can ignore horrible foreign policy because they are shielded by the military machine.
The insulation from consequences leads to a disconnect from global issues, viewing them as unrelated.
Addressing the mentality of not needing to help foreigners because one was born in the U.S.
Exploring the notion that caring from the top income earners could drive global change.
Money plays a pivotal role in influencing change due to the wealth in certain countries.
Acknowledging that injustices often arise from massive corporations driven by consumer choices.
Emphasizing that change starts with individual actions and consumer decisions.
Stating that any revolution in today's interconnected world must be global to truly bring change.

Actions:

for global citizens,
Choose consciously where to spend money to influence corporate behavior (implied)
Start making changes individually in consumption habits (implied)
</details>
<details>
<summary>
2018-12-21: Let's talk about revolution.... (<a href="https://youtube.com/watch?v=Db7TUd0A9-M">watch</a> || <a href="/videos/2018/12/21/Lets_talk_about_revolution">transcript &amp; editable summary</a>)

Beau delves into the implications of revolution, warning against violence in the US and advocating for new ideas to drive change.

</summary>

"A new form of government. Changing forms of government. Something like that."
"In the United States, a violent revolution will be horrible."
"You can put a bullet in it, or you can put a new idea in it."

### AI summary (High error rate! Edit errors on video page)

Addressing the comments section's interest in revolution, especially after the Yellow Vest Movement, and activists in the US seeking a revolution.
Defines a revolution as not just regime change but bringing a new idea to the table, like changing forms of government.
Notes that Western civilization's trend in revolutions is to decentralize power and give more power to more people.
Warns about the consequences of a violent revolution in the United States, citing ethnic, racial tensions, and cultural divides.
Mentions that the US is not self-reliant anymore and how a revolution interrupting transportation could lead to food and supply shortages.
Emphasizes that violent revolution isn't the only way to change minds; focusing on new ideas is key to the future.
Talks about the potential devastating impact of a violent revolution in the US and the misguided belief in readiness for such a scenario.

Actions:

for activists, thinkers, citizens,
Prepare emergency supplies and have a plan in case of disruptions in transportation (implied)
Engage in constructive dialogues and debates about new ideas for governance (implied)
</details>
<details>
<summary>
2018-12-16: Let's talk about two psychological effects and first aid kits.... (<a href="https://youtube.com/watch?v=1lfWrmYPonc">watch</a> || <a href="/videos/2018/12/16/Lets_talk_about_two_psychological_effects_and_first_aid_kits">transcript &amp; editable summary</a>)

Beau breaks down psychological effects, expert confidence, and the bystander effect in emergency situations, urging action and preparedness with quality first aid kits to potentially save lives.

</summary>

"If you're walking down the street and you have a heart attack, you better hope there's only one other person on the street."
"Sometimes, the actual expert is less confident in their knowledge than somebody that knows nothing."
"Make sure that somebody else is helping before you just walk on by."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of psychological effects and first aid kits that could potentially save lives.
Contrasts different levels of knowledge and confidence in handling first aid kits between himself, his son, and his wife.
Explains the Dunning-Kruger effect in the context of expertise and confidence levels.
Talks about the bystander effect and its impact on people's willingness to act in emergencies.
Advocates for owning a first aid kit to combat the bystander effect and potentially save lives.
Provides advice on what kind of first aid kit not to get and recommends a company named Bear Paw Tac Med for quality kits.
Describes the contents and uses of different types of first aid kits offered by Bear Paw Tac Med.
Suggests the importance of carrying tools you understand the theory of, even if you may not know how to use them practically.
Addresses common questions about emergency medicine, including the safety of veterinary medications for humans.
Emphasizes the importance of taking action in emergency situations rather than assuming someone else will help.

Actions:

for health-conscious individuals,
Order a quality first aid kit from Bear Paw Tac Med (suggested)
Educate yourself on emergency procedures and first aid techniques (implied)
Familiarize yourself with the contents of your first aid kit (implied)
</details>
<details>
<summary>
2018-12-15: Let's talk about what an iconic photo can tell us about Border Patrol.... (<a href="https://youtube.com/watch?v=yPAf6oc8Drc">watch</a> || <a href="/videos/2018/12/15/Lets_talk_about_what_an_iconic_photo_can_tell_us_about_Border_Patrol">transcript &amp; editable summary</a>)

Beau recounts a Vietnam War story, criticizes Border Patrol agents for inhuman actions, and urges them to quit before being held accountable.

</summary>

"You proud of yourself?"
"Your badge is not going to protect you from that."
"Just following orders isn't going to cut it."

### AI summary (High error rate! Edit errors on video page)

Recounts a story from the Vietnam War, referencing a Viet Cong trooper who continued fighting despite severe injuries by strapping a bolt to himself.
Mentions the iconic photo from the Battle of Saigon where American troops showed humanity by giving water to their opposition.
Criticizes the inhuman actions of Border Patrol agents who dump out water left by volunteers for migrants crossing dangerous routes.
Compares the Border Patrol agents' actions to torturing videos posted by teenagers and questions their morality.
Points out that deterrence methods like dumping water won't stop people seeking safety, freedom, and opportunities.
Urges Border Patrol agents to quit their jobs, as they will eventually be held accountable for their actions.
Emphasizes that blindly following orders is not an excuse, as history has shown that defense doesn't hold up.

Actions:

for border patrol agents,
Quit your job at Border Patrol (implied)
</details>
<details>
<summary>
2018-12-14: Let's talk about social issues and comic books.... (<a href="https://youtube.com/watch?v=GTo9Xs5gpWQ">watch</a> || <a href="/videos/2018/12/14/Lets_talk_about_social_issues_and_comic_books">transcript &amp; editable summary</a>)

Beau lays bare the longstanding tradition of addressing social issues in comics, challenging fans to see beyond the surface and confront their privilege amid societal changes.

</summary>

"Social issues have always been in comics. You were just too dense to catch it."
"If you have a privilege for so long, and you start to lose it, it feels like oppression."
"Two chunks of plastic have had more of an impact than the actual stories of people who underwent some pretty horrible things."
"There is no tyrannical, feminist regime coming for you."
"It's always been there. The reason you're noticing now is because the social issues being addressed are striking at the heart of the comic book industry's core audience which is white males."

### AI summary (High error rate! Edit errors on video page)

Using Elf on the Shelf to address social issues has sparked anger and controversy on social media, particularly among those who are critical of feminists addressing social issues.
Beau shares his perspective on how comic books like Punisher, G.I. Joe, X-Men, and Superman have always addressed social issues, despite some fans missing the underlying themes.
He points out that even seemingly masculine-themed comics like Punisher and G.I. Joe actually tackle social issues like PTSD, feminism, and political commentary.
Beau explains how characters in these comics represent deeper themes such as trauma, feminism, and societal commentary.
He contrasts the strong feminist undertones in G.I. Joe with the perception that comics should not address social issues.
Beau challenges the idea that addressing social issues in comics is a recent phenomenon, suggesting that it has always been present but may have gone unnoticed by some fans.
The backlash against using Elf on the Shelf for social commentary is juxtaposed with the lack of empathy towards real-life stories of hardship, drawing attention to the disparity in reactions.
He brings attention to how two plastic characters, Libertad and Esperanza, have sparked more reaction than the real-life stories of individuals facing hardships at the border.
Beau shares a true story behind characters Libertad and Esperanza, shedding light on the real struggles that inspired his use of Elf on the Shelf for social commentary.

Actions:

for comic book enthusiasts,
Acknowledge and appreciate the underlying social issues addressed in comic books (implied).
Engage in critical analysis of media content to understand deeper themes and messages (suggested).
</details>
<details>
<summary>
2018-12-13: Let's talk about how I was wrong about asylum seekers applying in the first country they come to.... (<a href="https://youtube.com/watch?v=t3_Nzzl7jd8">watch</a> || <a href="/videos/2018/12/13/Lets_talk_about_how_I_was_wrong_about_asylum_seekers_applying_in_the_first_country_they_come_to">transcript &amp; editable summary</a>)

Beau admits being wrong, sheds light on asylum laws, and warns against relying on memes for legal knowledge, revealing the limited scope of the Safe Third Country Agreement.

</summary>

"Knowledge is power. Don't get your legal information from memes."
"People that know immigration law were so dumbfounded by this widespread belief."
"Unless you're moving along the U.S.-Canadian border. That's the only place this treaty applies."

### AI summary (High error rate! Edit errors on video page)

Admits being wrong about asylum seekers applying for asylum in the first country they come to.
Discovered a EU court ruling stating refugees must apply in the first member state they enter.
Explains the Safe Third Country Agreement between the US and Canada.
Emphasizes the importance of not getting legal information from memes.
Clarifies that the treaty only applies to the US and Canada, not other countries.
Mentions that Canada is considering scrapping the agreement due to the treatment of asylum seekers and refugees in the US.
Trump tried to designate Mexico as a safe third country, but the attempt was laughed at globally.
Points out the dangers and lack of safety standards in Mexico, making it unsuitable as a safe third country.
Expresses astonishment at the widespread belief that asylum seekers must apply in the first country they arrive in.
Concludes by stressing the importance of knowledge and not relying on memes for legal information.

Actions:

for legal enthusiasts, asylum advocates,
Read and research international treaties and laws (suggested)
Educate others on the limitations of the Safe Third Country Agreement (implied)
</details>
<details>
<summary>
2018-12-13: Let's talk about Cyntoia Brown.... (<a href="https://youtube.com/watch?v=ljIWxvKdjOg">watch</a> || <a href="/videos/2018/12/13/Lets_talk_about_Cyntoia_Brown">transcript &amp; editable summary</a>)

Beau questions the justice system in Tennessee and advocates for clemency for a girl involved in a controversial case, raising doubts and proposing scenarios to challenge perceptions of guilt.

</summary>

"You telling me a sex trafficker doesn't?"
"Send a message that is very fitting for the people of Tennessee."
"So rather than argue her innocence, I'm going to go a different way with it."

### AI summary (High error rate! Edit errors on video page)

Talking about the Brown case in Tennessee where a 16-year-old girl killed a man 14 years ago and was sentenced to 51 years.
Defense argues she was being sex trafficked by a guy named Cutthroat and killed the man in self-defense.
Beau expresses doubt about the defense's story, questioning the rareness of Stockholm Syndrome leading to not turning a firearm on a captor.
Beau expresses disbelief in the state's version of events and criticizes Tennessee's inclination towards punitive justice.
Beau presents scenarios where, legally speaking, the girl could be seen as more guilty, but questions when she did something wrong in the eyes of Tennessee residents.
Advocates for clemency for the girl based on her circumstances, including her age, developmental delays, and years already served in prison.
Emphasizes the girl's rehabilitation efforts in prison and argues for sending her home.
Questions the message the governor sends by not granting clemency, suggesting it could impact how traffickers view consequences in the state.
Encourages the governor to make a decision that represents the values of the people of Tennessee.

Actions:

for governor of tennessee,
Grant clemency to the girl involved in the case (suggested)
Advocate for fair treatment and rehabilitation of individuals involved in similar cases (exemplified)
</details>
<details>
<summary>
2018-12-10: Let's talk about hunting and Sex and the City.... (<a href="https://youtube.com/watch?v=29xS9oOUZ30">watch</a> || <a href="/videos/2018/12/10/Lets_talk_about_hunting_and_Sex_and_the_City">transcript &amp; editable summary</a>)

Beau dismisses hunting stereotypes, criticizes wealthy trophy hunters for fueling poaching markets, and suggests investing in local communities as an alternative to canned hunts.

</summary>

"It's about proving you're a man."
"Number one, that stops this from being just an advanced form of colonialism."
"It's about killing. It's about proving you're a man."
"So alternatively, if you are interested in this topic, my suggestion..."
"They don't want aid, they want business."

### AI summary (High error rate! Edit errors on video page)

Dismisses the stereotype that everyone from the South hunts, noting he used to hunt but stopped due to early mornings.
Distinguishes between hunters who eat what they kill and trophy hunters who go after exotic animals primarily for status.
Criticizes wealthy trophy hunters who go on canned hunts, where guides do all the work and hunters simply take the shot.
Points out that while some claim canned hunts support conservation efforts, the practice actually fuels a market for artifacts and poaching.
Suggests investing the hunting money in local communities instead, promoting self-sufficiency and economic growth.
Proposes alternatives like volunteering with anti-poaching forces or supporting advocates like Kristen Davis for animal conservation.
Recommends supporting organizations like the David Sheldrick Wildlife Trust for elephant conservation efforts.

Actions:

for conservationists, animal advocates,
Invest the hunting money in local communities to support self-sufficiency and economic growth (suggested).
Volunteer with anti-poaching task forces to combat poaching (suggested).
Support organizations like the David Sheldrick Wildlife Trust for elephant conservation efforts (suggested).
</details>
<details>
<summary>
2018-12-08: Let's talk about anti-feminist memes and the patriarchy.... (<a href="https://youtube.com/watch?v=hQ1aNRBdQAY">watch</a> || <a href="/videos/2018/12/08/Lets_talk_about_anti-feminist_memes_and_the_patriarchy">transcript &amp; editable summary</a>)

Beau addresses anti-feminist memes, patriarchy, and the importance of women defining themselves despite societal biases.

</summary>

"All women have to be two things, and that's it. Who and what they want."
"What you think doesn't matter."
"The fact that you don't find her attractive doesn't matter."
"That's your hangup, not hers."
"I believe in diversity of tactics, so whatever, they're drawing attention to a cause that still needs some attention."

### AI summary (High error rate! Edit errors on video page)

Reacts to backlash over wearing a t-shirt with Disney princesses and feminist themes by discussing anti-feminist memes and the patriarchy.
Describes two types of anti-feminist memes: one about women doing traditional household tasks and the other about feminism making women ugly.
Criticizes the shallow mindset of valuing a woman based on her ability to cook, clean, or do laundry.
Suggests that men who have issues with feminism may still be emotionally tied to their mothers rather than viewing women as independent individuals.
Addresses the "feminism makes you ugly" meme, which objectifies and criticizes women based on physical appearance.
Points out the hypocrisy of using a woman's attractiveness as a reason to discredit her feminist views.
Counters the argument that feminism is unnecessary by showcasing the ongoing existence of patriarchy in society, citing the lack of female representation in positions of power.
Challenges the notion that women are not promoted in the workforce due to working less or taking more days off, suggesting societal conditioning and gender biases as contributing factors.
Supports different tactics in advocating for gender equality while acknowledging that radical feminists may not always use methods he agrees with.
Emphasizes that women have the right to be themselves, pursue their goals, and not be defined by others' opinions or insecurities.

Actions:

for gender equality advocates,
Challenge anti-feminist attitudes and memes in your social circles (exemplified)
Support diverse feminist tactics to raise awareness for gender equality (exemplified)
Empower women to define themselves and pursue their goals (exemplified)
</details>
<details>
<summary>
2018-12-06: Let's talk about treating everybody the same.... (<a href="https://youtube.com/watch?v=41RBjXaNC6s">watch</a> || <a href="/videos/2018/12/06/Lets_talk_about_treating_everybody_the_same">transcript &amp; editable summary</a>)

Beau advocates for treating everybody fairly over the superficial equality of treating them the same, pointing out the necessity of understanding and embracing cultural differences for true fairness.

</summary>

"The goal shouldn't be to treat everybody the same. It should be to treat everybody fairly."
"Doesn't take you long to realize treating everybody the same is not treating everybody fairly."
"We demonize education. We demonize learning about other cultures, other religions, whatever."
"In order to treat everybody fairly, you have to know where they're coming from."
"We demonize education. And it's something we need to work on."

### AI summary (High error rate! Edit errors on video page)

Advocates for treating everybody fairly rather than the same to build a just society.
Recounts a personal story about the director of photography for Olin Mills and the realization that treating everybody the same isn't always fair.
Mentions McNamara's morons, illustrating the consequences of treating everyone the same during the Vietnam War.
Shares a personal aversion to Arab males due to a cultural difference in personal space.
Points out the challenge of promoting fair treatment in the U.S. due to a lack of education and understanding of other cultures.
Emphasizes the importance of learning about others' backgrounds to treat everybody fairly.

Actions:

for americans,
Learn about different cultures and religions to understand and treat others fairly (implied).
Advocate for education and cultural understanding in communities (implied).
</details>
<details>
<summary>
2018-12-04: Let's talk about a cop asking me for training.... (<a href="https://youtube.com/watch?v=BmjB7TUroyE">watch</a> || <a href="/videos/2018/12/04/Lets_talk_about_a_cop_asking_me_for_training">transcript &amp; editable summary</a>)

Beau shares insights on critical police training principles, like time, distance, and cover, addressing issues of excessive force and misunderstandings in law enforcement.

</summary>

"Time, distance, and cover. That's exactly what it sounds like."
"Auditory exclusion is the hearing counterpart to tunnel vision."
"The fact that that cop exercised the spirit of the law, rather than the letter of the law, is the only reason he's alive."
"The most dangerous people on the planet are true believers."
"You need to stay away from ideologically motivated people."

### AI summary (High error rate! Edit errors on video page)

Recounts an encounter with cops during a training session where one suggested hitting a suspect in the knee with a baton for being more effective.
Shares his concern about the lack of response from other officers in the room when this suggestion was made.
Describes a cop who reached out to him after watching a video on police militarization, admitting to not knowing certain critical concepts like time, distance, and cover.
Explains the importance of time, distance, and cover in police work, citing examples like Tamir Rice and John Crawford where not applying this principle resulted in tragic outcomes.
Talks about auditory exclusion in high-stress situations and how it can lead to misunderstandings and potentially dangerous actions by law enforcement.
Advises cops to give suspects time to comply with commands and not resort to excessive force due to misinterpretations.
Warns against conflicting commands among cops during apprehensions, which can escalate situations and lead to excessive force.
Mentions the dangers of positional asphyxiation and the importance of understanding it to prevent unnecessary harm during arrests.
Urges cops to wear proper armor and stay behind cover during shootouts to avoid harming others due to stress-induced mistakes.
Shares a personal anecdote about the importance of understanding the spirit of the law over its letter and warns against enforcing unjust laws blindly.

Actions:

for law enforcement officers,
Implement training sessions on critical concepts like time, distance, and cover (suggested).
Advocate for proper armor and cover usage during operations (suggested).
Educate fellow officers on auditory exclusion and its effects in high-stress situations (suggested).
</details>
<details>
<summary>
2018-12-03: Let's talk about globalism.... (<a href="https://youtube.com/watch?v=18fkpRoBP1s">watch</a> || <a href="/videos/2018/12/03/Lets_talk_about_globalism">transcript &amp; editable summary</a>)

Beau breaks down globalism vs. nationalism, exposing them as tools to manipulate and divert attention from true accountability, advocating for a better world governance approach.

</summary>

"All a nationalist is, is a low-ambition globalist."
"It's a method of keeping you kicking down, keeping you in your place."
"I am neither. I'm not a nationalist. I am not a globalist."
"Just my opinion there. There are some pretty smart folks out there."
"You're just being played, and it's turned into a buzzword."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of globalism and how it's often used derogatorily, especially by nationalists.
Mentions that some extremists use "globalist" as a code word for being Jewish.
Describes globalism as a potential new world order with a single government ruling over everyone.
Compares nationalists to globalists, stating they both aim to control and motivate people through different means.
Criticizes the idea of representative democracy and questions if politicians truly represent the public.
Points out that both globalism and nationalism divert people from holding those in power accountable.
Expresses his stance of not being a nationalist or a globalist, advocating for better ways to govern the world.
Defines nationalists as low-ambition globalists who seek power within their own corner of the world.

Actions:

for critical thinkers,
Challenge derogatory terms and stereotypes in your community (suggested).
Question politicians' accountability and representation (implied).
</details>
<details>
<summary>
2018-12-02: Let's talk about courage, Bundy Ranch, and Elf on the Shelf.... (<a href="https://youtube.com/watch?v=CB6fVkSiobM">watch</a> || <a href="/videos/2018/12/02/Lets_talk_about_courage_Bundy_Ranch_and_Elf_on_the_Shelf">transcript &amp; editable summary</a>)

Beau challenges those who typically mind their business to start speaking out for real freedom and equality, urging diverse voices to counter the negative representation by a vocal minority.

</summary>

"Courage is recognizing a danger and overcoming your fear of it."
"Somebody has got to start talking for those people who truly believe in freedom."
"Be that megaphone standing up for freedom. Real freedom."
"You are more important than you can possibly imagine at getting the message out to the people that need to hear it."
"Start lending your voices. You can't just sit on the sidelines anymore."

### AI summary (High error rate! Edit errors on video page)

Defines courage as recognizing danger and overcoming fear, not the absence of fear.
Mentions the Bundy Ranch incident where ranchers faced off with the Bureau of Land Management.
Declines to participate in the Bundy Ranch situation due to his disagreement with their tactics.
Acknowledges Eamonn Bundy's courage for openly criticizing the President's immigration policy to his far-right fan base.
Stresses that rural America supports freedom for all and believes in the Constitution.
Shares a personal story about encountering racist remarks while shopping for Christmas decorations.
Encourages those who usually mind their own business to start speaking out and getting involved in current issues.
Urges people who believe in freedom for all to become vocal and counter the negative representation created by a vocal minority.
Addresses the importance of diverse voices speaking up for freedom and equality.
Encourages Southern belles and young Southern females to use platforms like YouTube to spread messages of freedom and equality.

Actions:

for southern community members,
Start speaking out on social media platforms (suggested)
Use your voice to advocate for freedom and equality (implied)
Encourage diversity in voices speaking out (implied)
</details>
<details>
<summary>
2018-12-01: Let's talk about protesters and human shields.... (<a href="https://youtube.com/watch?v=KQXiKeKpI0A">watch</a> || <a href="/videos/2018/12/01/Lets_talk_about_protesters_and_human_shields">transcript &amp; editable summary</a>)

Beau addresses the use of human shields in protests, drawing parallels to historical events and criticizing the immoral actions of opening fire on unarmed civilians.

</summary>

"You do not punish the child for the sins of the father."
"You do not open fire on unarmed crowds."
"That's America. That's how we make America great, betraying everything that this country stood for."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of protest and human shields in the context of current events in the U.S.
Describes protesters upset with the government for not upholding rights and laws, resorting to violating international norms.
Recalls a historical event, the Boston Massacre, where protesters were met with violence from militarized forces.
Compares the recent events to the Boston Massacre, pointing out the use of human shields and the potential for harm.
Criticizes those cheering on violence against protesters, questioning their support for undermining the Constitution.
Raises concerns about the immoral act of using human shields and opening fire on unarmed civilians.
Expresses disbelief at the debate surrounding these actions and advocates for upholding moral standards.
Condemns betraying the values that America supposedly stood for and calls for reflection on the current state of the country.

Actions:

for activists, protesters, advocates,
Stand against the use of human shields in protests (implied)
Advocate for non-violent methods of crowd control (implied)
</details>

## November
<details>
<summary>
2018-11-28: Let's talk about the arguments against asylum seekers.... (<a href="https://youtube.com/watch?v=5QoYFrUEUpk">watch</a> || <a href="/videos/2018/11/28/Lets_talk_about_the_arguments_against_asylum_seekers">transcript &amp; editable summary</a>)

Beau explains the legal and moral imperatives for supporting asylum seekers while challenging prevalent misconceptions about immigration and advocating for empathy and understanding.

</summary>

"It's cheaper to be a good person."
"Stop projecting your greed onto them. Maybe they just want safety."
"Accept some responsibility for your apathy and stay and fix it."
"I know killers, real killers. I don't know any that open fire on an unarmed crowd."
"Your canteen still be cool because you got ice in your veins."

### AI summary (High error rate! Edit errors on video page)

Correcting arguments against asylum seekers' entry after wild comments.
Referencing U.S. Constitution, international treaties, and protocols on refugees.
Explaining the legality of claiming asylum and entering a country.
Addressing the argument about sponsoring immigrant families.
Contrasting the focus on helping homeless vets with aiding asylum seekers.
Critiquing the emphasis on money and questioning who will pay for refugee assistance.
Comparing the cost of assisting asylum seekers to other government expenditures.
Disputing claims that asylum seekers come for benefits and asserting they seek safety.
Linking current immigration issues to past U.S. involvement in Central American countries.
Challenging the notion of staying in a dangerous country rather than seeking asylum.
Illustrating the scale of a tragedy like Syria happening in the U.S.
Mocking internet tough guys and their unrealistic bravado.
Offering a humorous take on potential survival scenarios.
Acknowledging his own skills but critiquing those who boast about combat abilities.
Condemning the idea of opening fire on unarmed individuals, especially women and children.
Expressing readiness for challenging situations with a touch of sarcasm.

Actions:

for advocates for humane immigration policies.,
Contact Office of Refugee Resettlement to offer support for asylum seekers (suggested).
Advocate for increased funding for refugee assistance programs (implied).
Educate others about the legal rights of asylum seekers and refugees (implied).
</details>
<details>
<summary>
2018-11-27: Let's talk about tear gassing toddlers.... (<a href="https://youtube.com/watch?v=toNqAAoofkI">watch</a> || <a href="/videos/2018/11/27/Lets_talk_about_tear_gassing_toddlers">transcript &amp; editable summary</a>)

Beau Ginn questions the use of tear gas on children, challenges perceptions of peace, and criticizes fear-mongering leadership.

</summary>

"What are you going to do when it doesn't happen?"
"We tear gassed kids like any other two-bit dictatorship."
"Because they don't like seeing their government behave like Saddam's."
"Sometimes, we tear gassed kids."
"Says they're better than us because I can't say we didn't earn it."

### AI summary (High error rate! Edit errors on video page)

Addressing tear gas used on children in the United States.
Criticizing violence and justification over trivial matters.
Asking how one might react if their child was tear-gassed.
Challenging the perceived peaceful nature of certain groups.
Mentioning heavy cartel presence in specific areas.
Implying potential violence against Border Patrol agents.
Questioning the nation's response to fear-mongering leadership.
Criticizing tear-gassing children and the impact on the nation's image.
Noting the troops' disapproval of government behavior.
Speculating on the lack of violence along the border.

Actions:

for advocates for justice,
Challenge the normalization of violence against children (implied)
Speak out against tear-gassing and violence (implied)
Support non-violent solutions to conflicts (implied)
</details>
<details>
<summary>
2018-11-26: Let's talk about that shooting in Alabama.... (<a href="https://youtube.com/watch?v=db6dEBKhovg">watch</a> || <a href="/videos/2018/11/26/Lets_talk_about_that_shooting_in_Alabama">transcript &amp; editable summary</a>)

Police mistakenly shot a fleeing, unarmed man in Alabama, sparking racial bias debates and the need for unbiased threat assessment by law enforcement.

</summary>

"Being armed and black isn't a crime."
"Nothing suggests this man did anything wrong."
"Biases exist. If you don't acknowledge them, they will persist."
"Time, distance, and cover. If he starts to point his weapon at the crowd, that's what we might call an indicator."
"The establishment, the powers that be, will always be able to play us against each other."

### AI summary (High error rate! Edit errors on video page)

Police initially claimed they killed the shooter in Alabama, but later admitted the man did not shoot anyone.
The man, a legally armed black individual, ran away from the scene of a shooting in a mall.
Beau questions the rationality of shooting someone who fled from gunfire in a mall.
He points out the tendency for people to defend police actions in such situations, even when the victim did nothing wrong.
Beau delves into the man's military service history and addresses misconceptions surrounding his honorable discharge.
He criticizes the media's portrayal of the man based on a single photo to incite negative opinions.
Beau brings attention to the racial implications of being armed and black in America, referencing past incidents where armed black individuals were unjustly killed by police.
He challenges biases and urges for a critical examination of the situation, asserting that being armed and black is not a crime.
Beau addresses law enforcement directly, acknowledging the need for positive threat identification and a thorough assessment before resorting to lethal force.
He calls for the acknowledgment and dissolution of biases to prevent further injustices and manipulation by those in power.

Actions:

for law enforcement, activists, community members,
Challenge biases within yourself and others (implied)
Advocate for unbiased threat assessment training for law enforcement (implied)
Support initiatives that address racial bias in policing (implied)
</details>
<details>
<summary>
2018-11-22: Let's talk about the judge overruling Trump's asylum ban.... (<a href="https://youtube.com/watch?v=PLIujWl-Wvk">watch</a> || <a href="/videos/2018/11/22/Lets_talk_about_the_judge_overruling_Trump_s_asylum_ban">transcript &amp; editable summary</a>)

A judge overruled Trump's asylum ban, exposing his attempt to act like a dictator by challenging the Constitution.

</summary>

"Crossing the border while seeking asylum in the middle of the desert and saying, hey, I'm claiming asylum. lawful, completely legal, completely moral."
"If it's your party, they can set fire to the Constitution and you'll roast a marshmallow over it."
"The three branches of government, that is the basis of the United States Constitution."

### AI summary (High error rate! Edit errors on video page)

A judge overruled Trump's asylum ban, revealing the legality of seeking asylum by crossing the border without permission.
News outlets labeled asylum seekers as "illegals" and the situation as an invasion, intentionally misleading the public.
Seeking asylum by crossing the border is lawful, legal, and morally sound.
Asylum is meant to provide people with the ability to seek safety.
Trump tried to change the law through a proclamation, attempting to act like a dictator.
Trump's actions were a direct challenge to the Constitution and an attempt to dictate law.
The judicial branch intervened to stop Trump's attempt, while Congress failed to act.
Trump's defenders may claim patriotism but failed to uphold the Constitution.
Trump's actions undermined Congressional authority and the foundation of the Constitution.
Blind partisanship has led to a disregard for the Constitution and the principles it upholds.

Actions:

for us citizens,
Stand up for the Constitution and demand accountability from elected officials (implied).
Educate others on the importance of upholding the Constitution and the separation of powers (implied).
</details>
<details>
<summary>
2018-11-21: Let's talk about supporting the troops.... (<a href="https://youtube.com/watch?v=cljonq0TBUA">watch</a> || <a href="/videos/2018/11/21/Lets_talk_about_supporting_the_troops">transcript &amp; editable summary</a>)

Beau showcases a community effort led by veterans to provide aid, while also challenging the true support needed for veterans beyond mere lip service.

</summary>

"If you really want to help veterans, stop turning them into combat veterans because some politician waved a flag and sold you a pack of lies that you didn't check out."
"Before you can support the troops, you've got to support the truth."
"If the only time you bring up helping veterans is as an excuse not to help somebody else, I want you to think back to that story."

### AI summary (High error rate! Edit errors on video page)

Dennis heard about a group in need north of Panama City without supplies after a hurricane.
Terry contacted Beau to provide aid and they loaded up supplies from Hope Project.
Beau encountered a lady filling in for David at the Hope Project and explained the situation.
They discovered the community center in Fountain, Florida running low on food but with baby formula.
Beau and others redistributed supplies between the locations to ensure both had what they needed.
An 89-year-old man helped load supplies, showcasing incredible generosity and strength.
All involved in the aid effort were veterans, showing their commitment to helping others in need.
Beau points out the lack of support for veterans facing issues like delayed GI bill payments and homelessness.
He criticizes the lack of attention to veteran suicides and the use of troops for political purposes.
Beau challenges the notion of using veterans as an excuse to neglect other marginalized groups.
He urges genuine care and involvement to truly support veterans and prevent unnecessary conflict.
Beau advocates for prioritizing truth and understanding in supporting veterans and avoiding unnecessary wars.

Actions:

for community members, activists,
Assist local aid efforts (exemplified)
Support veterans facing issues like delayed GI bill payments and homelessness (exemplified)
Advocate for truth and understanding in supporting veterans and preventing unnecessary conflicts (exemplified)
</details>
<details>
<summary>
2018-11-19: Let's talk about a native story in honor of Thanksgiving.... (<a href="https://youtube.com/watch?v=rSxupv4rKPE">watch</a> || <a href="/videos/2018/11/19/Lets_talk_about_a_native_story_in_honor_of_Thanksgiving">transcript &amp; editable summary</a>)

Beau uncovers the harsh reality behind the romanticized Native American narratives in mainstream media, urging reflection on fabricated tales and misrepresentations.

</summary>

"Most of what you know of Native stories is false. It's completely false, completely made up."
"A girl was kidnapped, held hostage, forced into marriage, which means raped, and then died an early death."
"There's not a whole lot of stories about natives just being native."
"It's a good time to bring it up and just kind of remember that most of what you know of Native culture, Native myths, Native stories is pretty much completely fabricated."
"Y'all have a nice night, it was just a thought."

### AI summary (High error rate! Edit errors on video page)

Shares a Native American story of Matoaka, not the typical Thanksgiving narrative.
Matoaka learned English, traded supplies, and was eventually kidnapped by colonists.
Forced into marriage to escape captivity, she was taken to England as a showcase of "civilized savages."
Matoaka died young, under suspicious circumstances—either poisoned or from disease.
Debunks the romanticized tale of Pocahontas saving John Smith's life.
Expresses how most Native stories in popular culture are fabricated and romanticized.
Criticizes the prevalent portrayal of natives in media with the white savior narrative.
Mentions a movie plot where a character leaves the tribe, becomes an FBI agent, and returns as a hero.
References Leonard Peltier's unjust imprisonment by the FBI.
Raises awareness about the misrepresentation of Native culture and myths in mainstream narratives.

Actions:

for history enthusiasts, educators, activists,
Research and share authentic Native American stories to counter misconceptions (suggested)
Support Native activists and causes to raise awareness about misrepresentations in media (implied)
</details>
<details>
<summary>
2018-11-14: Let's talk about police militarization.... (<a href="https://youtube.com/watch?v=LB3HUXdmid4">watch</a> || <a href="/videos/2018/11/14/Lets_talk_about_police_militarization">transcript &amp; editable summary</a>)

Beau dismantles the "warrior cop" mentality, exposing the dangers of police militarization and the consequences of prioritizing enforcement over public protection.

</summary>

"If you're a cop in an area where feeding the homeless is illegal, you're a bad cop."
"Being a warrior, everybody in the military is a warrior."
"They've bought into their own propaganda."
"There is no war on cops."
"You're the weapon if you're a warrior."

### AI summary (High error rate! Edit errors on video page)

Police militarization is a critical issue that affects communities, leading to concerns about the "warrior cop" mentality.
The misconception that being a warrior means being a killer is problematic and has resulted in a focus on militarizing law enforcement.
The shift towards viewing law enforcement as strictly enforcing the law, rather than protecting the public, raises moral questions for officers.
Enforcing unjust laws, such as those criminalizing feeding the homeless, can make a cop complicit in wrongdoing.
Propaganda within law enforcement, including inflated statistics on officer deaths, contributes to a heightened sense of danger and leads to hasty decision-making.
The belief in a "war on cops" is not grounded in reality, yet it influences the desire for militarization and warrior-like behavior among law enforcement.
The use of military equipment like flashbangs and MRAPs by SWAT teams is excessive and often lacks proper training, leading to dangerous outcomes.
Lack of intelligence work before operations results in SWAT teams making deadly mistakes, as seen in cases of wrong addresses and unnecessary force.
The discrepancy between perceived and actual capabilities of SWAT teams can have lethal consequences when faced with real resistance.
Beau advocates for a reevaluation of law enforcement's role as public servants and protectors rather than warriors.

Actions:

for law enforcement reform advocates,
Reassess law enforcement's role as public servants and protectors rather than warriors (implied)
Advocate for proper training and accountability in law enforcement (implied)
Support efforts to demilitarize police forces and redirect resources towards community policing (implied)
</details>
<details>
<summary>
2018-11-12: Let's talk about red flag gun seizures.... (<a href="https://youtube.com/watch?v=5JoyD1iPnQI">watch</a> || <a href="/videos/2018/11/12/Lets_talk_about_red_flag_gun_seizures">transcript &amp; editable summary</a>)

Beau explains the flaws in the execution of red flag gun seizures and suggests a more sensible approach to prevent harm and uphold due process, urging swift action from all sides.

</summary>

"The idea is sound, okay, the execution isn't."
"Cops are picking up these tactics, and they're using them in a police setting when they're not designed to be."
"That due process is important."
"You've got a good idea, guys."
"It's a good idea. The execution is bad and the execution can be fixed very easily."

### AI summary (High error rate! Edit errors on video page)

Explains red flag gun seizures, where local cops can temporarily seize guns if someone is deemed a threat.
Acknowledges the sound idea behind the concept of red flag gun seizures but criticizes its flawed execution.
Raises concerns about the violation of due process when guns are seized without proper legal procedures.
Criticizes the tactical implementation of red flag gun seizures, particularly the early morning raids by cops.
Points out the negative consequences of using military tactics in civilian settings by law enforcement.
Advocates for proper training for law enforcement officers using military equipment to prevent unnecessary harm.
Suggests a better approach to executing red flag gun seizures, such as detaining individuals on their way home for due process instead of surprise raids.
Emphasizes the importance of due process in red flag gun seizures to prevent wrongful confiscation of firearms.
Urges both pro-gun and gun control advocates to work together to improve the execution of red flag gun seizure laws.
Encourages swift action to address the flaws in red flag gun seizure implementation before more lives are lost.

Actions:

for advocates, law enforcement, gun owners,
Contact your representatives to push for immediate fixes in the flawed implementation of red flag gun seizure laws (suggested).
Advocate for proper training for law enforcement officers using military equipment (suggested).
Work towards ensuring due process in red flag gun seizures to prevent wrongful confiscations (suggested).
</details>
<details>
<summary>
2018-11-05: Let's talk about fear and a tale of two neighborhoods... (<a href="https://youtube.com/watch?v=1BMl6phxWpU">watch</a> || <a href="/videos/2018/11/05/Lets_talk_about_fear_and_a_tale_of_two_neighborhoods">transcript &amp; editable summary</a>)

Beau talks about fear, community action, and the importance of taking personal responsibility instead of relying on government or succumbing to irrational fears.

</summary>

"It's fear. It's fear."
"If you got a problem in your community, fix it, do it yourself."
"We've got to stop being afraid of everything."
"One of these migrants may be a murderer, maybe, there's 7,000 people, I'm sure somebody's done something wrong in that group."
"It's up to you, it is up to you."

### AI summary (High error rate! Edit errors on video page)

Describes running supplies to Panama City after a hurricane to ensure people get what they need.
Talks about visiting two neighborhoods: a rough one and a nice one.
Shares an encounter in the rough neighborhood where gang members help distribute supplies efficiently.
Narrates an incident in the nice neighborhood where a neighbor with a gun accuses him of looting.
Expresses frustration at the fear that pervades society, leading to irrational behaviors.
Criticizes the reliance on government and the lack of personal initiative in solving community problems.
Encourages individuals to take action locally rather than waiting for governmental intervention.
Emphasizes the importance of teaching children through actions, not just words.
Argues against demonizing migrants fleeing Central America and calls for empathy and understanding.
Concludes by urging people to take responsibility for solving problems instead of waiting for others.

Actions:

for community members, activists,
Support non-governmental organizations like Project Hope Incorporated by donating supplies or volunteering (suggested)
Take initiative in your community by identifying and addressing local issues without waiting for government intervention (exemplified)
</details>
<details>
<summary>
2018-11-03: Let's talk about how the troops on the border can defend American freedom.... (<a href="https://youtube.com/watch?v=kWWouKv51zY">watch</a> || <a href="/videos/2018/11/03/Lets_talk_about_how_the_troops_on_the_border_can_defend_American_freedom">transcript &amp; editable summary</a>)

A reminder to soldiers: implement vague orders wisely, defend American freedom by upholding laws, and beware of political misuse of your honor and uniform.

</summary>

"Order comes down, it's vague. Gets more specific until it gets to those guys on the ground. And they've got to figure out how to implement it."
"You want to defend American freedom, you keep that weapon slung and you keep it on safe."
"Honor, integrity, those things. It's bred into you, right?"
"You wouldn't circumvent the law, earn that trust, keep that weapon slung and keep it on safe."
"When that vague order comes down, you ask for clarification. Email. Clarification."

### AI summary (High error rate! Edit errors on video page)

Recounts a story from the past about a general ordering a lieutenant to take a hill crawling with enemies, resulting in success without casualties.
Explains the process of vague orders becoming specific on the ground and the importance of implementation.
Talks about the unique chance for soldiers at the border to defend American freedom by upholding specific laws and rules of engagement.
Warns against mission creep, using Afghanistan as an example, and the illegal request for U.S. soldiers to act as emergency law enforcement personnel.
Emphasizes the need to stay within the law, especially regarding non-combatants, and the potential consequences for enlisted soldiers if rules are broken.
Advises soldiers to keep their weapons safe and slung, not getting involved in illegal activities.
Encourages soldiers to know the laws of war, the real rules of engagement, and not to follow vague or illegal orders.
Urges those at the border or heading there to learn how to legally apply for asylum in the United States.
Criticizes the political use of soldiers by higher-ups, warning about the misuse of trust and honor for political gains.
Suggests requesting clarification on vague orders, following the law, and being prepared for a potential need for defense against illegal actions.

Actions:

for soldiers, military personnel,
Learn how to legally apply for asylum in the United States (suggested)
Request clarification on vague orders and follow legal guidelines (implied)
Keep weapons safe and slung, avoiding illegal activities (implied)
</details>
<details>
<summary>
2018-11-01: Let's talk about a Kennedy legend you've probably never heard and why it's important today... (<a href="https://youtube.com/watch?v=9Q2S1hLWmo4">watch</a> || <a href="/videos/2018/11/01/Lets_talk_about_a_Kennedy_legend_you_ve_probably_never_heard_and_why_it_s_important_today">transcript &amp; editable summary</a>)

Beau raises concerns about the suspension of habeas corpus, linking it to a legend about Green Berets and urging people to understand its significance in preserving the rule of law and national security.

</summary>

"The suspension of habeas corpus is though, that is a threat to national security. It's a threat to your very way of life."
"The suspension of habeas corpus is something that can't be tolerated."
"The suspension of habeas corpus is an affront to everything that this country has ever stood for."
"You need to look it up. You need to realize exactly what it means for that to be gone."
"You can watch this country turn into another two-bit dictatorship or you can kill this party politics that you're playing."

### AI summary (High error rate! Edit errors on video page)

Tells the legend of President Kennedy meeting with selected Green Berets in 1961, making them take an oath to fight back against a tyrannical government.
Mentions the secrecy and lack of evidence surrounding the oath and the Green Berets' activities.
Talks about the involvement of Green Berets in Kennedy's honor guard after his assassination.
Raises questions about the existence of a Special Forces Underground and its investigation by the Department of Defense.
Points out the annual visit by active-duty Green Berets to Kennedy's grave site for a brief ceremony.
Connects Kennedy's criteria for fighting tyranny, including the suspension of habeas corpus, to current political considerations.
Criticizes the idea of suspending habeas corpus as a threat to the rule of law and national security.
Challenges the argument for suspending habeas corpus in dealing with migrants and asylum seekers.
Urges people to understand the significance of habeas corpus and its potential removal from legal protections.
Raises concerns about the erosion of the rule of law and the danger of unchecked presidential powers.

Actions:

for americans, activists, citizens,
Educate yourself and others on the significance of habeas corpus and its importance in upholding the rule of law (implied).
Advocate for the protection of legal rights and civil liberties in your community and beyond (implied).
Stay informed about political decisions that could impact fundamental rights and freedoms (implied).
</details>

## October
<details>
<summary>
2018-10-30: Let's talk about buying a gun because of fear of political violence.... (<a href="https://youtube.com/watch?v=Yv-0OQ8KSkM">watch</a> || <a href="/videos/2018/10/30/Lets_talk_about_buying_a_gun_because_of_fear_of_political_violence">transcript &amp; editable summary</a>)

Be prepared to choose and train with a firearm for self-defense, understanding its purpose is ultimately for protection through lethal force.

</summary>

"There is not a firearm in production, in modern production, that a man can handle that a woman can't."
"Each one is designed with a specific purpose in mind. Your purpose, based on what you've said, is to kill a person."
"If you're going to buy a firearm for the purposes that you've kind of expressed, you need to be prepared to kill."
"You know, and I'm not trying to dissuade you from doing it. I think it's a good thing."
"The best advice I can give you is to, you know, you've made the decision. Do it right."

### AI summary (High error rate! Edit errors on video page)

Warning viewers that the video will be long and dark due to the serious topic discussed.
Addresses the sexism present in the gun community, specifically targeting advice given to women.
Explains the misconception behind choosing a revolver for its ease of use and lack of safety features.
Advises against purchasing a firearm if not comfortable with the idea of using it to kill.
Emphasizes the importance of training and understanding the firearm before purchase.
Recommends selecting a firearm based on one's environment and purpose.
Compares choosing a firearm to selecting different types of shoes, each serving a specific purpose.
Encourages seeking guidance from experienced individuals and trying out different firearms at a range.
Stresses the necessity of training realistically and preparing to use the firearm effectively in potential life-threatening situations.
Outlines the key elements needed to win a firefight: speed, surprise, and violence of action.
Provides tactical advice on engaging in a firefight, including not leaving cover unnecessarily and prioritizing human life over material possessions.
Urges individuals to be prepared for the reality of using a firearm and understanding its purpose.
Recommends selecting a firearm based on military designs for simplicity and reliability.
Acknowledges the unfortunate reality of needing to prepare for potential political violence and offers support to viewers in making informed decisions.

Actions:

for women concerned about personal safety.,
Find someone experienced with firearms to help you choose and train with a firearm (implied).
Practice shooting in various positions and scenarios to prepare realistically for potential threats (implied).
Seek out ranges or individuals with access to different firearms for testing before making a purchase (implied).
</details>
<details>
<summary>
2018-10-30: Let's talk about Halloween costumes.... (<a href="https://youtube.com/watch?v=G7ANo1S0Jqk">watch</a> || <a href="/videos/2018/10/30/Lets_talk_about_Halloween_costumes">transcript &amp; editable summary</a>)

Beau addresses offensive Halloween costumes, criticizes using edginess as humor, and warns against hiding behind internet anonymity to spread hate.

</summary>

"Being offensive and edgy is not actually a substitute for having a sense of humor."
"But you can't be mad when you suffer the consequences of your actions."
"Bigotry is out. It's something that sane people have given up on."
"Try being a good person. Like I said, you can do whatever you want."
"People are going to remember it."

### AI summary (High error rate! Edit errors on video page)

Addresses offensive Halloween costumes, including blackface and dressing up as Trayvon Martin or Adolf Hitler.
Expresses lack of understanding for the shock when people face backlash for intentionally offensive actions.
Points out that being offensive and edgy is not a substitute for having a sense of humor.
Talks about how jokes about bullying and tormenting marginalized groups were considered acceptable in the past.
Criticizes those who hide behind internet anonymity to spew hate and offensive remarks.
Urges people to try being a good person instead of just being shocking or edgy.
Stresses that while individuals have the right to dress and act however they want, others have the right to look down on them.
Comments on the idea of political correctness and how it aims to change flawed ways of thinking.
Mentions the current political climate but warns against mistaking it for a permanent change in culture.
Concludes by stating that people will not accept or excuse harmful actions, even if they were meant as jokes.

Actions:

for social media users,
Be a good person (exemplified)
Refrain from making offensive jokes (exemplified)
Speak out against harmful actions (exemplified)
Challenge flawed ways of thinking (exemplified)
</details>
<details>
<summary>
2018-10-28: Let's talk about militias going to the border.... (<a href="https://youtube.com/watch?v=wN-XFH5giKQ">watch</a> || <a href="/videos/2018/10/28/Lets_talk_about_militias_going_to_the_border">transcript &amp; editable summary</a>)

Beau Ginn on the Mexican border with militia, raising concerns about the legality and ethical implications of their actions while potentially compromising their group's secrecy and intentions.

</summary>

"We're down here on the border protecting America with our guns, ready to kill a bunch of unarmed asylum seekers who are following the law."
"Hey, Colonel!"

### AI summary (High error rate! Edit errors on video page)

Beau is on the Mexican border with his militia commander, tasked with protecting America.
They won't stop refugees on the Mexican side as it violates U.S., Mexican, and international law.
Using guns to stop refugees could result in committing a felony and losing gun rights.
Sending back individuals at gunpoint from the American side is also against the law and goes against their core principles.
Beau acknowledges that individuals entering the U.S. have rights, including due process.
They are essentially backing up the Department of Homeland Security (DHS) despite being labeled potential terrorists or extremists in a DHS pamphlet.
Beau expresses concern that their actions could have negative consequences if they follow through with their rhetoric.
Beau questions the wisdom of their militia commanders and suggests they might be strategizing with "4D chess" to manage optics.
Despite portraying themselves as protecting America, Beau acknowledges the grim reality of potentially harming unarmed asylum seekers following the law.
Beau ends with a casual greeting to a Colonel, leaving the situation open-ended.

Actions:

for militia members, border patrol supporters,
Question the ethics and legality of their actions (implied)
Reassess the group's strategies and potential consequences (implied)
</details>
<details>
<summary>
2018-10-28: Let's talk about context.... (<a href="https://youtube.com/watch?v=iqNDL5FiLsg">watch</a> || <a href="/videos/2018/10/28/Lets_talk_about_context">transcript &amp; editable summary</a>)

Beau stresses the importance of context in understanding global events beyond misleading sound bites, cautioning against the rise of propaganda.

</summary>

"Context, it's really important."
"If you want to understand what's going on in the world, you have to look beyond the sound bites."
"Propaganda is coming into a golden age right now."
"Most times the sound bite, just like in Kennedy's speech, it's the exact opposite of what's really being said and really being done."
"Taking a whole bunch of information and getting down to what's actually important."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the importance of sound bites and context, using Kennedy's speech as an example.
Kennedy's speech, often quoted for specific sound bites, is misunderstood without the full context.
The speech was about the Soviet Union, not a vague conspiracy theory.
The main aim of the speech was to encourage self-censorship among journalists, editors, and publishers.
Beau criticizes the modern reliance on sound bites due to shortened attention spans from 24-hour news and social media.
He points out the dangers of Twitter as a platform for substanceless communication.
Beau questions the reasons behind U.S. involvement in Syria, attributing it to the U.S. starting the Civil War for ulterior motives.
The motive may have been related to competing pipelines supported by different countries.
Propaganda plays a significant role in shaping public perceptions and is on the rise.
Beau urges viewers to look beyond sound bites and understand the interconnected nature of global events.

Actions:

for media consumers, critical thinkers,
Question mainstream narratives (suggested)
Research beyond headlines (exemplified)
Analyze global events critically (implied)
</details>
<details>
<summary>
2018-10-26: Let's talk about fear of asylum seekers and the browning of America.... (<a href="https://youtube.com/watch?v=iU88FU4snpU">watch</a> || <a href="/videos/2018/10/26/Lets_talk_about_fear_of_asylum_seekers_and_the_browning_of_America">transcript &amp; editable summary</a>)

Beau challenges fear-based narratives on asylum seekers and racial diversity, envisioning a future where racial distinctions blur for a more united society.

</summary>

"Light will become darker. Dark will become lighter. Eventually we're all going to look like Brazilians."
"It's gonna be a lot harder to drum up support for wars that aren't really necessary because you're not killing some random person that doesn't look like you."
"Maybe the best thing for everybody is to blur those racial lines a little bit."
"Y'all have a nice night."

### AI summary (High error rate! Edit errors on video page)

Addresses fears about asylum seekers, including concerns about ISIS, MS-13, diseases, and more.
Questions the validity of these fears by pointing out the lack of evidence despite asylum seekers coming for 15 years.
Suggests that politicians use fear to win elections by tapping into people's insecurities.
Believes the real fear in society is different, illustrated through a personal story about interracial relationships.
Expresses confusion over the fear of diversity and the "browning of America."
Envisions a future where racial distinctions blur and politicians may have to lead differently.
Contemplates whether America was meant to be a melting pot or a fruit salad with separate elements.

Actions:

for americans,
Embrace diversity in your community by fostering relationships with people from different backgrounds (exemplified).
Challenge stereotypes and prejudices by engaging in open and honest dialogues with friends and family (implied).
</details>
<details>
<summary>
2018-10-24: Let's talk about the caravans.... (<a href="https://youtube.com/watch?v=9fCCnOsQiv0">watch</a> || <a href="/videos/2018/10/24/Lets_talk_about_the_caravans">transcript &amp; editable summary</a>)

Caravans fleeing violence will continue due to US policies; assisting migrants is cheaper than detention, and turning them away risks lives needlessly.

</summary>

"It's cheaper to be a good person."
"Blaming the victim."
"Give me one good reason to send these people back to their deaths."
"You can't preach freedom and then deny it."
"These people will die."

### AI summary (High error rate! Edit errors on video page)

Caravans have been coming for 15 years, and they will continue due to US drug war policies and interventions in other countries.
The current caravan is from Honduras, a country with a murder rate of 159 per 100,000, compared to Chicago's 15-20 per 100,000.
Tax dollars fund assistance for migrants, costing about half a billion dollars annually.
It's cheaper to assist migrants in getting on their feet than to detain them.
Migrants travel in large groups for safety, similar to the principle of trick-or-treating in groups.
Carrying other countries' flags allows migrants to connect with fellow countrymen during the journey.
Wearing military fatigues is practical for durability during long journeys.
Migrants are legally claiming asylum by arriving at ports of entry or within the country.
Blaming migrants for seeking asylum overlooks the impact of US foreign policy and the drug war on their home countries.
Beau challenges those advocating to turn migrants away by asking for one good reason to send them back to potential death, criticizing fear-based policies.

Actions:

for advocates, policymakers, activists,
Contact your representative to address how US foreign policy impacts other countries (suggested)
Advocate for changes in drug war policies and intervention practices (suggested)
</details>
<details>
<summary>
2018-10-22: Let's talk about what Trump said about the migrants.... (<a href="https://youtube.com/watch?v=mrSIIZzOGOo">watch</a> || <a href="/videos/2018/10/22/Lets_talk_about_what_Trump_said_about_the_migrants">transcript &amp; editable summary</a>)

President's actions to cut aid for not stopping people from leaving their countries signal a dangerous erosion of freedom and democracy, with the wall serving as a potential prison barrier for Americans.

</summary>

"That wall is a prison wall. It's not a border wall. It's gonna work both ways."
"The face of tyranny is always mild at first and it's just now starting to smile at us."

### AI summary (High error rate! Edit errors on video page)

President cutting off aid to Central American countries for not stopping people from coming to the United States is terrifying.
The President believes it's the head of state's duty to prevent people from leaving their country.
The wall along the southern border can also be used to keep Americans in.
Beau warns about the slow creep of tyranny and the danger of losing freedom and liberty.
Calling out those who support the President's actions as betraying their ideals for a red hat and slogan.
Describing the wall as a prison wall that can trap people inside.
Beau expresses concern about the implications of the President's statements on freedom and democracy.

Actions:

for citizens, activists,
Resist erosion of freedom: Stand up against policies threatening liberty (implied).
Educate others: Share information about the potential consequences of oppressive measures (implied).
</details>
<details>
<summary>
2018-10-21: Let's talk about when it's appropriate to call the police.... (<a href="https://youtube.com/watch?v=pG9TKx1J8eM">watch</a> || <a href="/videos/2018/10/21/Lets_talk_about_when_it_s_appropriate_to_call_the_police">transcript &amp; editable summary</a>)

Police can escalate any situation to the point of death; only call them when it's truly necessary. Non-compliance with police can lead to violence or death, even for minor offenses. Be cautious in involving law enforcement, as every call to 911 carries the possibility of a deadly outcome.

</summary>

"Every law is backed up by penalty of death."
"If nobody's being harmed, it's not really a crime."
"Don't call the law unless death is an appropriate response."
"Calling the police on someone for insignificant reasons puts their life in jeopardy."
"Every call to 911 carries the possibility of a deadly outcome."

### AI summary (High error rate! Edit errors on video page)

Police can escalate any situation, even trivial ones, to the point of death.
Jaywalking, a seemingly harmless act, can lead to fatal consequences.
Non-compliance with police can result in violence or death, even for minor offenses.
Calling the police on someone for insignificant reasons puts their life in jeopardy.
Only call the police when the situation truly warrants a response of death.
If nobody is being harmed, it's not a crime, even if it may be illegal.
Patience with dealing with unnecessary police intervention varies based on race.
Police involvement can escalate situations needlessly, especially for people of color.
The potential consequences of involving the police should be carefully considered.
Every call to 911 carries the possibility of a deadly outcome.

Actions:

for community members,
Practice de-escalation techniques when faced with conflicts (implied)
Educate yourself and others on alternatives to calling the police (implied)
</details>
<details>
<summary>
2018-10-20: Let's talk about leaving a real blackout to find a figurative one.... (<a href="https://youtube.com/watch?v=ReUajdqynRk">watch</a> || <a href="/videos/2018/10/20/Lets_talk_about_leaving_a_real_blackout_to_find_a_figurative_one">transcript &amp; editable summary</a>)

Beau warns against censorship of independent news outlets opposed to government policies and advocates for seeking alternative social media platforms to preserve free discourse.

</summary>

"Facebook and Twitter are no longer free discussion platforms."
"Be ready. Look for another social media network."
"This is going to be the end of free discourse on Facebook."

### AI summary (High error rate! Edit errors on video page)

Beau is out for a drive to escape the noise of the chainsaw and trees, giving him a chance to scroll through the internet.
Facebook and Twitter have censored independent news outlets opposed to government policies, alarming Beau.
Beau differentiates between these outlets and Alex Jones, noting the dangerous nature of Jones' content.
Many independent news outlets were shut down on Facebook and Twitter in one day, worrying Beau about the impact on free discourse.
Carrie Wedler, a journalist Beau recommends, had her outlet and personal Twitter account banned in the censorship sweep.
Outlets Beau works with were not affected, but he finds the censorship chilling as an independent network.
Beau warns against comparing the censored outlets to Alex Jones and stresses that dissenting from government policies is not a reason for censorship.
He points out that Facebook and Twitter are no longer platforms for free discourse and may be altering public discourse at the government's request.
Beau encourages people to seek alternative social media networks as censorship may not stop with these outlets.
He mentions platforms like Steam It and MeWe as alternatives where censored information may be found and shared.
Tens of millions of subscribers were affected by the censorship, leading Beau to believe this may mark the end of free discourse on Facebook.
Independent outlets like Anti-Media provide good reporting with accountability, something lacking in major corporate networks.
Beau urges people to prepare for a shift away from Facebook to maintain access to diverse perspectives and information.

Actions:

for social media users,
Find and join alternative social media networks like Steam It and MeWe to access and share censored information (suggested).
Prepare to shift away from Facebook to maintain access to diverse perspectives and information (implied).
</details>
<details>
<summary>
2018-10-18: Let's talk about natural disasters, looting, and being prepared.... (<a href="https://youtube.com/watch?v=dRayLlNeARk">watch</a> || <a href="/videos/2018/10/18/Lets_talk_about_natural_disasters_looting_and_being_prepared">transcript &amp; editable summary</a>)

Beau updates on hurricane aftermath, criticizes law enforcement's response to looting, praises Florida National Guard, and urges viewers to prepare emergency kits for survival.

</summary>

"In the event of a natural disaster, protecting the inventory of some large corporation should pretty much be at the bottom of any law enforcement agencies list."
"You can't count on government response. You can't."
"Simple things, you know, little flashlights, stuff like that, just have it all in one spot."
"We're not talking about building a bunker and preparing for doomsday."
"Please take the time to read it, to put one of those bags together."

### AI summary (High error rate! Edit errors on video page)

Updates internet audience on his situation after being caught in a hurricane.
Expresses gratitude for the comments and reassures everyone that he and his loved ones are safe, despite the aftermath of the hurricane.
Addresses the issue of looting after natural disasters and criticizes law enforcement's response.
Points out the media's biased portrayal of looting, especially concerning different races.
Criticizes law enforcement's decision to shut down areas due to looting threats, preventing volunteers from helping those in need.
Questions the priorities of law enforcement when it comes to protecting property over saving lives during a disaster.
Acknowledges the Florida National Guard's quick and effective response to the hurricane.
Urges people to be prepared for emergencies by assembling a basic emergency kit with essentials like food, water, and medical supplies.
Provides resources and links for viewers to create their emergency kits at various budget levels.
Emphasizes the importance of being prepared for emergencies without going overboard or breaking the bank.

Actions:

for community members,
Assemble an emergency kit with essentials like food, water, fire supplies, shelter, medical supplies, and a knife (suggested).
Put together emergency bags for different budget levels (suggested).
</details>
<details>
<summary>
2018-10-10: Let's talk about what we can learn from a Japanese history professor.... (<a href="https://youtube.com/watch?v=F8c3XkDAMiU">watch</a> || <a href="/videos/2018/10/10/Lets_talk_about_what_we_can_learn_from_a_Japanese_history_professor">transcript &amp; editable summary</a>)

Beau shares a tale urging against silence in the face of tyranny, stressing the importance of speaking out for freedom.

</summary>

"When you remain silent in the face of tyranny or oppression, you have chosen a side."
"Your silence is only helping them."
"Tyranny anywhere is a threat to freedom everywhere."

### AI summary (High error rate! Edit errors on video page)

Introduces a word-of-mouth story about a history professor in Japan before World War II.
The professor predicts the war and moves his family to the secluded island of Iwo Jima.
Despite having the potential to influence policy and possibly alter the course of the war, the professor chooses to remain silent.
Beau underscores the significance of not remaining silent in the face of tyranny or oppression.
Talks about the implications of remaining silent and how it equates to siding with those in power.
Emphasizes the interconnectedness of power dynamics and communication in today's world.
Criticizes the societal norm of avoiding topics like money, politics, and religion at the dinner table.
Urges for open discussions and dialogues on critical issues instead of sticking to talking points.
Stresses the importance of speaking out against tyranny to protect freedom.
Encourages starting necessary dialogues among ourselves rather than relying on politicians.

Actions:

for activists, advocates, citizens,
Initiate open dialogues with community members to address critical issues (suggested)
Speak out against tyranny and oppression in your local context (implied)
</details>
<details>
<summary>
2018-10-09: Let's talk about the perception of your comments on the Kavanaugh case.... (<a href="https://youtube.com/watch?v=pfsgb7CySdQ">watch</a> || <a href="/videos/2018/10/09/Lets_talk_about_the_perception_of_your_comments_on_the_Kavanaugh_case">transcript &amp; editable summary</a>)

Beau urges men to reconsider harmful comments, salvage relationships, and apologize for perpetuating a culture of disbelief towards sexual assault victims.

</summary>

"You don't get to rape her."
"You've given them all the reason in the world to never come forward."
"I got caught up in politics. I didn't care."

### AI summary (High error rate! Edit errors on video page)

Received a message from a young lady who is unable to face her father due to his comments, appreciating Beau's perspective.
Urges men to reconsider their harmful comments and salvage relationships, rather than focusing on divisive topics like the Ford Kavanaugh case.
Points out that one in five women have experienced sexual assault, and criticizes the common arguments used to doubt victims.
Addresses the question of why victims wait to come forward, attributing it to fear of not being believed or facing consequences.
Challenges the notion of victim blaming based on behavior, stating that regardless of circumstances, rape is never justified.
Calls out the damaging impact of justifying inappropriate behavior, leading to underreporting of sexual assaults.
Encourages men to have honest conversations with the women in their lives and apologize for harmful comments that perpetuate a culture of disbelief.

Actions:

for men and fathers,
Have honest and open conversations with the women in your life to understand the impact of harmful comments (suggested)
Apologize for perpetuating a culture of disbelief and victim blaming (suggested)
</details>
<details>
<summary>
2018-10-08: Let's talk about a toast for Justice Kavanaugh.... (<a href="https://youtube.com/watch?v=cswLNYAcjlc">watch</a> || <a href="/videos/2018/10/08/Lets_talk_about_a_toast_for_Justice_Kavanaugh">transcript &amp; editable summary</a>)

Beau sarcastically praises Kavanaugh's Supreme Court confirmation, celebrating perceived safety and freedom while disregarding rights and accountability.

</summary>

"We got him up there and now America is going to be safe. We're going to be free again."
"There's no way that's going to be abused because the government, they're good people."
"We're gonna be free now. Okay, so yeah, we had to give up some rights, and sell out our country, and tread the Constitution."
"Congratulations, I'm very glad that your life wasn't ruined."
"We beat the feminists, we beat the leftists, we got a man's man up there on the Supreme Court."

### AI summary (High error rate! Edit errors on video page)

Beau sarcastically toasts Justice Kavanaugh's ascension to the Supreme Court, celebrating that his life wasn't ruined.
Expresses relief that Kavanaugh won't have to go back to coal mining, implying privilege and entitlement.
Beau celebrates Kavanaugh's confirmation, claiming it will make America safe and free again.
Mocks the idea of Kavanaugh being tied down by concerns like jury trials, lawyers, or charges before detention.
Criticizes the notion that government surveillance and lack of accountability will keep people safe and free.
Ridicules the efficiency of the National Security Agency and their ability to gather information without warrants.
Sarcastically praises local law enforcement for being able to stop and detain people without reason.
Irony in Beau's confidence that government power won't be abused, dismissing concerns about rights violations.
Beau prioritizes safety and freedom over rights, implying a trade-off that undermines fundamental liberties.
Mocks the lack of scrutiny on Kavanaugh's rulings, attributing his support to beating feminists and leftists at any cost.

Actions:

for justice advocates,
Challenge unjust power structures (implied)
Advocate for accountability and transparency in government actions (implied)
</details>
<details>
<summary>
2018-10-07: Let's talk about authority figures, trusted sources, and my story.... (<a href="https://youtube.com/watch?v=fLSOMM1eCs4">watch</a> || <a href="/videos/2018/10/07/Lets_talk_about_authority_figures_trusted_sources_and_my_story">transcript &amp; editable summary</a>)

Beau warns against blind obedience to authority, advocates for critical thinking, and encourages viewers to trust themselves over external sources.

</summary>

"That obedience to authority, that submission to authority. This person is an authority figure. They're a trusted source, so we're gonna believe what they say. It's not a good thing."
"A lot of bad things have happened in history because people didn't do that."
"Trust yourself. Trust yourself."
"Ideas stand or fall on their own."
"Don't trust your sources, trust your facts, and trust yourself."

### AI summary (High error rate! Edit errors on video page)

Viewers question his identity and credibility due to a contradiction between what he says and what they expect.
People want to know his backstory to determine how much trust to place in his words.
Beau rejects blind trust in authority figures and encourages fact-checking and critical thinking.
He warns against blind obedience to authority, citing the dangers it poses.
Beau mentions instances where people failed to fact-check, leading to disastrous consequences like the Iraq war.
Referring to Milgram's experiments, Beau reveals how many individuals are willing to harm others under authority's influence.
He stresses the importance of trusting oneself and thinking independently rather than relying solely on authority figures.
Beau values dissenting opinions and encourages viewers to think for themselves.
He believes that ideas should be judged based on their merit, not on the speaker's credentials.
Beau concludes by advocating for fact-checking, trusting in personal judgment, and encouraging independent thinking.

Actions:

for viewers,
Fact-check information before sharing or believing it (implied)
Encourage critical thinking and independent judgment in your community (implied)
</details>
<details>
<summary>
2018-10-06: Let's talk about the Confederate flag and the New South.... (<a href="https://youtube.com/watch?v=FMIOCj7a3tI">watch</a> || <a href="/videos/2018/10/06/Lets_talk_about_the_Confederate_flag_and_the_New_South">transcript &amp; editable summary</a>)

Beau challenges the romanticized notion of the Confederate flag as heritage, asserting its racist connotations amidst a changing Southern identity.

</summary>

"It's drawing up images of the most immoral, evil, and worst times in Southern history."
"I don't think it's about heritage. Not for a second."
"That symbol doesn't represent the South; it represents races."
"It's a new South. Don't go away mad. Just go away."

### AI summary (High error rate! Edit errors on video page)

The Confederate flag represents a test for people discussing Southern culture.
Southern culture existed before and after the Confederacy, making the flag's symbolism questionable.
The Confederate flag evokes immoral and evil times in Southern history.
Beau questions why individuals choose to associate with a symbol tied to such negative connotations.
The original Confederate flag differs from the commonly recognized flag, representing Robert E. Lee's army.
Beau suggests flying the real Confederate flag if one truly seeks to honor heritage, not the battle flag.
The Confederate flag's recent history links it to opposition to desegregation and racist acts.
Beau challenges the notion that flying the Confederate flag is about heritage, citing its divisive history.
He recounts an encounter with a former gang member who offered insights on the symbolism of flag-waving.
Beau asserts that his Black neighbors embody Southern culture more authentically than flag-wavers.

Actions:

for southern residents,
Educate others on the true history and implications of the Confederate flag (suggested)
Advocate for the removal of Confederate monuments and symbols from public spaces (implied)
</details>
<details>
<summary>
2018-10-05: Let's talk about gun control (Part 5: Bumpstocks).... (<a href="https://youtube.com/watch?v=HqVMiL7Qi0Y">watch</a> || <a href="/videos/2018/10/05/Lets_talk_about_gun_control_Part_5_Bumpstocks">transcript &amp; editable summary</a>)

Beau explains bump stocks, disputes their life-saving potential, and examines their necessity under the Second Amendment, ultimately questioning the need for a ban.

</summary>

"It's not going to save any lives."
"A lot of guys believe that to exercise the intent of the Second Amendment, which is to fight back against the government if need be, you'd need to be able to lay down suppressive fire."
"No man, nothing should be banned."
"You're not gonna see me at a protest trying to make sure that a bump stock doesn't get banned."
"Nine of them are gonna be complete people you probably don't want to hang around."

### AI summary (High error rate! Edit errors on video page)

Explains how bump stocks work to mimic fully automatic fire.
Mentions the inaccuracy of fully automatic fire.
Recounts the infamous use of bump stocks in the Vegas mass shooting.
Points out that bump stocks may increase a shooter's lethality in enclosed spaces like schools.
Disputes the notion that bump stocks save lives during mass shootings.
Analyzes the necessity of bump stocks under the Second Amendment's intent.
Describes the different perspectives among gun enthusiasts and Second Amendment supporters regarding bump stocks.
Talks about the concept of suppressive fire and its relevance to the Second Amendment.
States his opinion that bump stocks are not necessary in the context of insurgency against the government.
Expresses reservations about banning bump stocks but acknowledges potential concerns.

Actions:

for firearm enthusiasts, second amendment supporters,
Question the necessity of bump stocks in relation to the Second Amendment (implied).
Engage in respectful and informed debates about firearm regulations (implied).
</details>
<details>
<summary>
2018-10-04: Let's talk about what combat vets can teach sexual assault survivors.... (<a href="https://youtube.com/watch?v=uZloqoevlvk">watch</a> || <a href="/videos/2018/10/04/Lets_talk_about_what_combat_vets_can_teach_sexual_assault_survivors">transcript &amp; editable summary</a>)

Beau addresses the stigma around dating sexual assault survivors, comparing their experiences to combat vets and challenging the concept of being "damaged goods."

</summary>

"You are not unclean. You are not damaged goods. You are certainly not unworthy of being loved."
"We are all damaged goods in some way."
"There's nothing wrong with you."
"It's idiotic."
"It is amazing what trauma can do to your memory."

### AI summary (High error rate! Edit errors on video page)

Admits feeling like a psychologist with overwhelming inbox messages.
Acknowledges the benefit of sharing tough experiences.
Advises women not to date combat vets due to detachment, fear of intimacy, and emotional issues.
Points out the traumatic stress experienced by sexual assault survivors and combat vets.
Questions the double standard of stigma towards dating sexual assault survivors.
Challenges the notion of being "damaged goods" and unworthy of love.
Criticizes the stigma around sexual assault survivors compared to combat vets.
Condemns the concept of marking or branding women based on past experiences.
Encourages women not to internalize feelings of being damaged goods.
Shares a personal story illustrating the impact of trauma on memory and experiences.

Actions:

for survivors and allies,
Support survivors by listening and validating their experiences (implied).
Challenge stigmas and double standards around dating survivors of sexual assault (implied).
Educate others on the similarities in emotional responses between combat vets and sexual assault survivors (implied).
</details>
<details>
<summary>
2018-10-03: Let's talk about Trump , Obama , and monsters... (<a href="https://youtube.com/watch?v=PJNF3tfo0RE">watch</a> || <a href="/videos/2018/10/03/Lets_talk_about_Trump_Obama_and_monsters">transcript &amp; editable summary</a>)

President Trump's actions mock sexual assault survivors and contribute to a dangerous culture of silencing victims, while Beau contrasts his character with Obama and warns against dehumanizing perpetrators.

</summary>

"There's a concerted effort in this country to silence anyone willing to come forth with a sexual assault claim, and it's being led by the President of the United States."
"Honor, integrity, are those words that come to mind when you think of President Trump?"
"The scariest Nazi wasn't a monster. He was your neighbor."
"Scariest rapist or rape-apologist, well they're your neighbor too."
"y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

President of the United States mocked sexual assault survivors on television, setting a shockingly low bar.
The President's actions contribute to a concerted effort in the country to silence sexual assault claims.
Referencing Trump's infamous "Grab-Em-By-The-P****" quote, Beau questions what it takes to stop such behavior.
Beau contrasts Trump with Obama, acknowledging policy disagreements but noting the vast difference in character.
He criticizes Trump for casting doubt on women coming forward with claims and talks about the presumption of innocence.
Beau warns against dehumanizing perpetrators, stressing that they are people, not monsters.
He draws a parallel between excusing Nazis as monsters and how society views rapists and assault predators.
Beau underscores the danger of overlooking evil in plain sight due to political allegiance and nationalism.

Actions:

for activists, advocates, voters,
Challenge efforts to silence sexual assault survivors (implied)
Advocate for the presumption of innocence and fair treatment for survivors (implied)
Refrain from dehumanizing perpetrators and understand the importance of accountability (implied)
</details>
<details>
<summary>
2018-10-01: Let's talk about the presumption of innocence... (<a href="https://youtube.com/watch?v=Zu3IWDRgtSw">watch</a> || <a href="/videos/2018/10/01/Lets_talk_about_the_presumption_of_innocence">transcript &amp; editable summary</a>)

Beau covers the presumption of innocence, Kavanaugh's support for law enforcement overreach, and the dangers of party politics.

</summary>

"It's almost like they aren't familiar with his rulings."
"You've bought into bumper sticker politics and you're trading away your country for a red hat."

### AI summary (High error rate! Edit errors on video page)

Comments on the presumption of innocence in relation to Kavanaugh and the job interview.
Notes Kavanaugh's support for law enforcement to stop individuals on the street.
Mentions Kavanaugh's support for warrantless searches by the National Security Agency.
Points out Kavanaugh's involvement in the indefinite detention of innocent people.
Criticizes Kavanaugh for apparently lying during his confirmation hearings.
Emphasizes the importance of the presumption of innocence in the American justice system.
Criticizes Kavanaugh for undermining the Fourth Amendment.
Talks about the dangers of party politics and signing away others' rights.
Warns about the potential consequences of supporting Kavanaugh's nomination based on party affiliation.
Urges people to be aware of the implications of supporting candidates without understanding their rulings.

Actions:

for voters, constitution advocates.,
Know the background of candidates before supporting them (implied).
</details>
<details>
<summary>
2018-10-01: Let's talk about dating my daughter and a joke that needs to die.... (<a href="https://youtube.com/watch?v=MMSAcZ90QI8">watch</a> || <a href="/videos/2018/10/01/Lets_talk_about_dating_my_daughter_and_a_joke_that_needs_to_die">transcript &amp; editable summary</a>)

Fathers intimidating their daughters' boyfriends with guns sends harmful messages about trust, autonomy, and empowerment.

</summary>

"It's time to let this joke die, guys. It's not a good look. It's not a good joke."
"Guns should not be used as props. If you're breaking out a gun, you should be getting ready to destroy or kill something."
"She doesn't need a man to protect her. She's got this."

### AI summary (High error rate! Edit errors on video page)

Fathers posting photos with guns next to their daughters' boyfriends is a common social media joke, but it sends a harmful message.
The joke implies that without the threat of a firearm, the boyfriend will force himself on the daughter, which should not be a possibility.
Waving a firearm around also suggests a lack of trust in the daughter's judgment and autonomy.
The message behind the photo is that the daughter needs a man to protect her and that she lacks the capability to defend herself.
This tradition of intimidating boyfriends with guns is based on a dated concept of property rights and is ultimately disempowering for the daughter.
Beau questions the need for a firearm to intimidate a teenage boy, stating that it says more about the father than the boyfriend.
Guns should not be used as props or toys; their presence should signal a serious intent to destroy or kill.
Beau shares a personal experience where he had to address a boyfriend's behavior without needing a firearm.
Fathers should focus on empowering their daughters, building trust, providing information, and fostering open communication.
Beau urges fathers to reconsider perpetuating this joke and to only bring out guns when absolutely necessary.

Actions:

for fathers, parents,
Have open and honest communication with your children about trust, autonomy, and safety (implied).
Foster empowerment and independence in your children by trusting their judgment and capabilities (implied).
Refrain from using guns as props or toys, and only bring them out when absolutely necessary (implied).
</details>

## September
<details>
<summary>
2018-09-28: Let's talk about Kavanaugh, bar fights, and "what if it was your sister?".... (<a href="https://youtube.com/watch?v=FngaU-CK0Zk">watch</a> || <a href="/videos/2018/09/28/Lets_talk_about_Kavanaugh_bar_fights_and_what_if_it_was_your_sister">transcript &amp; editable summary</a>)

Beau recounts a bar incident to illustrate respecting women's autonomy, drawing parallels to the Kavanaugh hearing's impact on survivors and urging reflection on harmful narratives.

</summary>

"You might not want to do it again because you're altering real-life relationships over bumper sticker politics."
"Sexual assault is normally about power and control."
"You might not want to do it again because you're altering real-life relationships over bumper sticker politics."
"You already ruined that."
"How many of them do you know? How many of them talked about it with you?"

### AI summary (High error rate! Edit errors on video page)

Recounts a story from college where he intervened in a potentially dangerous situation at a bar to protect a girl he vaguely knew from being harassed by a guy and his friends.
Draws parallels between his bar incident and the current atmosphere around the Kavanaugh hearing, discussing the importance of respecting women's autonomy and agency.
Uses a hypothetical scenario with friends to illustrate the prevalence of sexual assault among women and how comments dismissing survivors can affect their willingness to come forward.
Criticizes the double standards and misconceptions surrounding men's emotions and reactions to trauma, contrasting them with the Kavanaugh hearings.
Emphasizes that sexual assault is about power and control, pointing out the significance of survivors speaking out against someone who may wield immense power.
Urges reflection on the impact of dismissing survivors and perpetuating harmful narratives, stressing the lasting damage it can cause to relationships.

Actions:

for all genders, allies,
Reach out to survivors in your community, offer support, and believe them (implied)
Educate yourself and others on the prevalence of sexual assault and its impact (suggested)
</details>
<details>
<summary>
2018-09-26: Let's talk about #MeToo, #HimToo, and sexual harassment and assault.... (<a href="https://youtube.com/watch?v=-qoW7FIWWs8">watch</a> || <a href="/videos/2018/09/26/Lets_talk_about_MeToo_HimToo_and_sexual_harassment_and_assault">transcript &amp; editable summary</a>)

Beau provides advice on avoiding sexual harassment and assault, stressing accountability and debunking victim-blaming myths.

</summary>

"Be very careful with the signals you send."
"False accusations of sexual harassment or sexual assault turn out to be false about 2% of the time."
"There is one cause of rape, and that's rapists."

### AI summary (High error rate! Edit errors on video page)

Addresses sexual harassment and assault in the context of the Kavanaugh nomination.
Provides advice on avoiding such situations, including safeguarding reputation and watching signals.
Mentions ending dates at the front door, avoiding going home with anyone if drinking, and being cautious with words.
Talks about the importance of keeping hands to oneself to avoid sending wrong signals.
Acknowledges the "him-too" hashtag and addresses men's concerns about false accusations.
Indicates that false accusations of sexual harassment or assault are rare, around 2% of the time.
Stresses mutual accountability between men and women regarding actions and situations.
Refutes the idea that clothing choices can cause rape, asserting that rapists are the sole cause.
Concludes with a powerful statement: "Gentlemen, there is one cause of rape, and that's rapists."

Actions:

for men, women,
Safeguard your reputation by being mindful of the signals you send (implied).
End dates at the front door to prioritize quality over quantity (implied).
Avoid taking anyone home when drinking to prevent risky situations (implied).
</details>
<details>
<summary>
2018-09-25: Let's talk about race and guns.... (<a href="https://youtube.com/watch?v=wXFtH3v2epI">watch</a> || <a href="/videos/2018/09/25/Lets_talk_about_race_and_guns">transcript &amp; editable summary</a>)

Beau breaks down the division within the gun community, the racial implications of gun control, and the importance of arming minorities for protection under the Second Amendment.

</summary>

"The intent of the Second Amendment is to strike back against government tyranny."
"Second Amendment supporters want to decentralize power, breaking up the government's monopoly on violence."
"The Second Amendment is there to protect racial minorities."
"Second Amendment supporters aim to arm minorities to protect against government tyranny targeting marginalized groups."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the division within the gun community between Second Amendment supporters and gun nuts.
Second Amendment supporters aim to arm minorities to protect against government tyranny targeting marginalized groups.
Second Amendment supporters can be blunt and use violent rhetoric, but their intent is to encourage self-defense.
Describes a violent image popular within the Second Amendment community that contrasts with the gun nut philosophy.
Differentiates between Second Amendment supporters who want to decentralize power and gun nuts who focus on might making right.
Mentions a program by the Department of Defense as a way to tell the difference between Second Amendment supporters and gun nuts.
Addresses the unintentional racism in some gun control measures, such as requiring firearms insurance disproportionately affecting minorities.
Talks about the racial implications of banning light pistols, which were predominantly bought by minorities.
Acknowledges the historical racial component of gun control but notes a shift due to awareness raised by Second Amendment supporters.
Emphasizes the importance of arming minority groups for their protection under the Second Amendment.

Actions:

for advocates, gun owners, activists,
Contact local gun stores to inquire about free training programs for minority and marginalized groups (implied).
Advocate for surplus firearms distribution to low-income families in collaboration with community organizations (suggested).
Raise awareness about unintentional racial implications of gun control measures targeting minorities (exemplified).
</details>
<details>
<summary>
2018-09-23: Let's talk about patriotism, propaganda, and the national anthem.... (<a href="https://youtube.com/watch?v=NHB6j6Tp1IY">watch</a> || <a href="/videos/2018/09/23/Lets_talk_about_patriotism_propaganda_and_the_national_anthem">transcript &amp; editable summary</a>)

Beau responds to an op-ed story criticizing a woman for kneeling during the national anthem, discussing the difference between nationalism and patriotism and inviting further discourse.

</summary>

"Patriotism is not doing what the government tells you to."
"Patriotism is correcting your government when it is wrong."
"There is a very marked difference between nationalism, which is what this gentleman is talking about and patriotism."
"You can kiss my patriotic behind."
"You think that it's just some silly answer from somebody who doesn't really know anything."

### AI summary (High error rate! Edit errors on video page)

Responding to an op-ed story sent by two people who knew a woman attacked for kneeling during the national anthem.
Expresses irritation and reads the author's bio, prompting a response.
Criticizes the author for calling the woman a petulant child for kneeling, questioning his understanding of protests.
Challenges the author on patriotism, pointing out that patriotism involves correcting the government when it is wrong.
Recounts a story about patriotism being displayed through actions, not just symbols, and suggests seeking guidance from a seasoned individual on patriotism.
Differentiates between nationalism and patriotism, condemning the author's behavior towards the woman.
Ends by asserting his stand on patriotism and inviting the author to contact him for further discourse.

Actions:

for patriotic individuals,
Contact Beau for further discourse on patriotism (suggested)
Seek guidance from experienced individuals on patriotism (implied)
</details>
<details>
<summary>
2018-09-20: Let's talk about guns, gun owners, school shootings, and "law abiding gun owners" (part 2).... (<a href="https://youtube.com/watch?v=wNtxtuQxUz8">watch</a> || <a href="/videos/2018/09/20/Lets_talk_about_guns_gun_owners_school_shootings_and_law_abiding_gun_owners_part_2">transcript &amp; editable summary</a>)

Beau explains why gun control measures fail and suggests solutions like raising the minimum gun-buying age and addressing domestic violence loopholes, pointing out the limitations of legislation.

</summary>

"Assault weapon is not in the firearms vocabulary."
"A soldier can enlist in the Army, come home, and not be able to buy a gun."
"Legislation is not the answer here in any way shape or form."

### AI summary (High error rate! Edit errors on video page)

Gun control has failed due to being written by those unfamiliar with firearms, leading to ineffective measures.
The term "assault weapon" is made up and lacks a concrete definition within firearms vocabulary.
Bans on assault weapons focused on cosmetic features rather than functionality, rendering them ineffective.
Banning high-capacity magazines is flawed as changing magazines can be swift, making the rush tactic infeasible.
Efforts to ban specific firearms like the AR-15 could lead to more powerful weapons being used in its place.
Banning semi-automatic rifles entirely is impractical, given their prevalence and ease of manufacture.
Restrictions on gun ownership based on age, particularly raising the minimum age to 21, could be a more effective measure.
Addressing loopholes in laws regarding domestic violence offenders owning guns could enhance safety measures.
Legislation focusing solely on outlawing certain firearms may not address the root issues effectively.
Beau stresses the need to look beyond laws and tackle deeper societal issues for a comprehensive solution.

Actions:

for advocates, activists, legislators,
Raise awareness about loopholes in laws regarding domestic violence offenders owning guns (suggested).
Advocate for raising the minimum age for purchasing firearms to 21 (suggested).
</details>
<details>
<summary>
2018-09-20: Let's talk about guns, gun control, school shootings, and the "law abiding gun owner" (Part 3) (<a href="https://youtube.com/watch?v=QbXTDuwSVkk">watch</a> || <a href="/videos/2018/09/20/Lets_talk_about_guns_gun_control_school_shootings_and_the_law_abiding_gun_owner_Part_3">transcript &amp; editable summary</a>)

Beau challenges pro-gun individuals, deconstructs cultural attitudes towards guns, and advocates for a shift towards true masculinity to address gun-related issues.

</summary>

"It's crazy how simple it is."
"Violence is always the answer, right?"
"Bring back real masculinity, honor, integrity."
"Bring it back to a tool, instead of making it your penis."
"You solve that, you're not gonna need any gun control."

### AI summary (High error rate! Edit errors on video page)

Addresses pro-gun individuals, challenging their perspective on gun control and cultural attitudes towards guns.
Questions the source of guns used in school shootings, pointing out the role of relatives or members of the gun culture.
Criticizes the glorification of guns in social media and its impact on influencing violent behavior.
Analyzes how guns have been perceived historically and how they have evolved into symbols of masculinity.
Critiques the response to school shootings and the underlying attitudes towards discipline and violence.
Clarifies the true intention of the Second Amendment and its historical context in relation to civil insurrections and government tyranny.
Challenges the concept of being a "law-abiding gun owner" and its implication in government actions.
Confronts the idea that violence is always the answer, debunking the association between guns and masculinity.
Advocates for a shift in societal values towards true masculinity, integrity, and empathy to address gun-related issues.
Emphasizes the importance of raising children with values that prioritize self-control and non-violence.

Actions:

for gun owners, activists, parents,
Educate about responsible gun ownership and the true purpose of the Second Amendment (implied).
Advocate for values of integrity, empathy, and self-control to address gun-related issues (implied).
Engage in community dialogues about masculinity and violence to foster a culture of non-violence (implied).
</details>
<details>
<summary>
2018-09-19: Let's talk about guns, gun control, school shooting, and "law abiding gun owners" (Part 1)... (<a href="https://youtube.com/watch?v=BxvxbZGjlv4">watch</a> || <a href="/videos/2018/09/19/Lets_talk_about_guns_gun_control_school_shooting_and_law_abiding_gun_owners_Part_1">transcript &amp; editable summary</a>)

Beau dives into the controversial topic of guns, debunking misconceptions about the AR-15 and setting the stage for discussing sensible gun control.

</summary>

"Your Second Amendment, that sacred Second Amendment, we're going to talk about that too."
"One gun that everybody knows because the media won't shut up about it. We're going to talk about the AR-15."
"There's nothing special about this thing."
"It's not the design of this thing that makes people kill."
"I know this one was kind of boring, but you need this base of knowledge to understand what we're going to talk about next."

### AI summary (High error rate! Edit errors on video page)

Addressing the controversial topic of guns, gun control, and the Second Amendment with the aim of sensible gun control.
Identifying three types of people: pro-gun, anti-gun, and those who understand firearms.
Using the example of the AR-15 to debunk misconceptions and explain its design.
Exploring the origins of the AR-15 and its use in the military.
Clarifying that the AR-15 is not a high-powered rifle and was not initially desired by the military.
Pointing out misconceptions perpetuated by the media regarding the AR-15 and its usage in shootings.
Describing the popularity of the AR-15 among civilians due to its simplicity and interchangeability of parts.
Contrasting the AR-15 with the Mini 14 Ranch Rifle, a hunting rifle with similar characteristics.
Emphasizing that the design of the AR-15 is not unique and has been around for a long time.
Concluding with a promise to address mitigation strategies in the next video.

Actions:

for gun policy advocates,
Educate others on the facts about firearms (implied)
Engage in informed discussions about gun control (implied)
</details>
<details>
<summary>
2018-09-16: Let's talk about 3 letters that can make rural whites understand the fear minorities have of cops. (<a href="https://youtube.com/watch?v=J5DBrOBIgNM">watch</a> || <a href="/videos/2018/09/16/Lets_talk_about_3_letters_that_can_make_rural_whites_understand_the_fear_minorities_have_of_cops">transcript &amp; editable summary</a>)

Exploring the distrust towards law enforcement, Beau urges unity and accountability to address the common issue of unaccountable men with guns.

</summary>

"We can hold them accountable one way or the other, the ballot box or the cartridge box."
"It's unaccountable men with guns. We can work together and we can solve that."
"We've got to start talking to each other. We got the same problems."

### AI summary (High error rate! Edit errors on video page)

Explains the distrust that minorities have for law enforcement compared to white country folk.
Illustrates how in rural areas, law enforcement is more accountable and part of a tighter community.
Mentions the ability to hold law enforcement accountable through the ballot box or the cartridge box.
Points out that minority groups lack institutional power to hold unaccountable men with guns in law enforcement accountable.
Draws parallels between the distrust felt towards law enforcement by minorities and white country folk towards agencies like ATF and BLM.
Addresses the frequency of unjust killings and lack of accountability in law enforcement.
Encourages taking action by getting involved in local elections to ensure accountability.
Suggests starting at the local level by electing good police chiefs to prevent corruption in federal agencies.
Advocates for people from different backgrounds to unite and address the common issue of unaccountable men with guns in law enforcement.
Urges individuals to care enough to take action and work together to solve the problem.

Actions:

for community members, activists,
Elect good police chiefs in local elections to prevent corruption (suggested)
Advocate for accountability in law enforcement by participating in local elections (implied)
Start dialogues with individuals from different backgrounds to address common issues (implied)
</details>
<details>
<summary>
2018-09-14: Let's talk about the Dallas shooting... (<a href="https://youtube.com/watch?v=6-d4C-sIlaI">watch</a> || <a href="/videos/2018/09/14/Lets_talk_about_the_Dallas_shooting">transcript &amp; editable summary</a>)

Beau questions the official narrative of a shooting in Dallas, pointing out potential motives and legal consequences, while criticizing police actions and lack of justice for victims, and connecting it to the Black Lives Matter movement.

</summary>

"There's two kinds of liars in the world."
"I'm not a lawyer, but that sure sounds like motive to me."
"If this is the amount of justice they get, they don't. They don't."
"Not one cop has crossed the thin blue line to say that's wrong."
"It doesn't matter what else you do."

### AI summary (High error rate! Edit errors on video page)

Expresses skepticism towards the official narrative of the shooting in Dallas, referencing experience with liars and motives.
Raises concerns about the shooting being a potential home invasion due to new information about noise complaints.
Points out the potential legal consequences in Texas for killing someone during a home invasion.
Questions the lack of intervention or opposition within the police department regarding potential corruption and cover-ups.
Connects the lack of justice in cases like this to the Black Lives Matter movement and the value placed on black lives.
Criticizes the actions of the police in searching the victim's home posthumously for justifications.

Actions:

for community members, justice seekers, activists.,
Speak out against police corruption and cover-ups within your community (implied).
Support movements like Black Lives Matter by advocating for justice and equality (implied).
</details>
<details>
<summary>
2018-09-11: Let's talk about the September 11th attacks and how we lost the war on terror... (<a href="https://youtube.com/watch?v=XO6sdC3AlQ8">watch</a> || <a href="/videos/2018/09/11/Lets_talk_about_the_September_11th_attacks_and_how_we_lost_the_war_on_terror">transcript &amp; editable summary</a>)

Beau warns against the dangerous normalization of loss of freedoms post-9/11, urging for grassroots community action to reclaim freedom and resist government overreach.

</summary>

"We didn't know it at the time, though, we were too busy, too busy putting yellow ribbons on our cars, looking for a flag to pick up and wave, or picking up a gun, traveling halfway around the world to kill people we never met."
"You can't dismantle the surveillance state or the police state by voting somebody into office."
"You have to defeat an overreaching government by ignoring it."
"The face of tyranny is always mild at first. And we're there right now."
"If your community is strong enough, what happens in DC doesn't matter because you can ignore it."

### AI summary (High error rate! Edit errors on video page)

Recalls his experience on September 11th, watching the tragic events unfold live on TV.
Expresses how a casualty of that day was the erosion of freedom and rights, not listed on any memorial.
Talks about the erosion of freedoms post-9/11 due to government actions in the name of security.
Mentions the strategic nature of terrorism and how it aims to provoke overreactions from governments.
Points out the dangerous normalization of loss of freedoms in society.
Urges for a grassroots approach to reclaiming freedom, starting at the community level.
Advocates for teaching children to question and resist the normalization of oppressive measures.
Emphasizes the importance of self-reliance in preparation for natural disasters and reducing dependency on the government.
Suggests counter-economic practices like bartering and cryptocurrency to reduce government influence.
Encourages surrounding oneself with like-minded individuals who support freedom and each other.
Calls for supporting and empowering marginalized individuals who understand the loss of freedom.
Talks about the effectiveness of U.S. Army Special Forces in training local populations as a force multiplier.
Stresses the need for community building to resist government overreach and tyranny.

Actions:

for community members,
Teach children to question and resist oppressive measures (implied)
Prioritize self-reliance for natural disasters and reduce dependency on the government (implied)
Practice counter-economic activities like bartering and cryptocurrency (implied)
Surround yourself with like-minded individuals who support freedom (implied)
Support and empower marginalized individuals who understand the loss of freedom (implied)
Build community networks to resist government overreach (implied)
</details>
<details>
<summary>
2018-09-08: Let's talk about what it's like to be a black person in the US.... (<a href="https://youtube.com/watch?v=WD8mWq0Hdcw">watch</a> || <a href="/videos/2018/09/08/Lets_talk_about_what_it_s_like_to_be_a_black_person_in_the_US">transcript &amp; editable summary</a>)

Unexpected reach of a Nike video led to deep reflections on cultural identity, heritage, and the ongoing impacts of slavery on collective identity, urging understanding beyond surface-level statistics.

</summary>

"Your entire cultural identity was ripped away."
"They have black pride because they don't know."
"That's a whitewash of the reality."
"How much of who you are as a person comes from the old country."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Unexpected wide reach of a Nike video led to insults and reflections.
Insult about IQ compared to a plant sparked thoughts on understanding being Black in the U.S.
White allies may grasp statistics but not the true experience of being Black.
Deep dive into cultural identity, heritage, and pride.
Historical context of slavery and its lasting impacts on cultural identity.
Reflections on cultural identity being stripped away and replaced.
Food, cuisine, and cultural practices as remnants of slavery.
The ongoing impact of slavery on cultural scars.
Call to acknowledge history and the need for healing.
Hope for a future where national identities are left behind.
Acknowledgment of uncomfortable history and the lack of pride for some.
Recognition of the significance of the Black Panther movie in providing pride.
Encouragement to truly understand the depth of the issue beyond surface-level talks.
Acknowledgment of cultural identity loss and the need for reflection and understanding.

Actions:

for white allies,
Acknowledge and understand the deep-rooted cultural impacts of historical events (implied).
</details>
<details>
<summary>
2018-09-05: Let's talk about burning shoes... (<a href="https://youtube.com/watch?v=mfDasT0zSpg">watch</a> || <a href="/videos/2018/09/05/Lets_talk_about_burning_shoes">transcript &amp; editable summary</a>)

Beau questions the logic behind burning symbols of protest and challenges the true meaning of freedom.

</summary>

"If you're that mad and you can't wear a pair of Nikes because of a commercial, take them and drop them off at a shelter."
"You're loving that symbol of freedom more than you love freedom."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau wants to burn some shoes today because it's raining and he feels the need to burn something.
He mentions not owning Nikes due to personal reasons but his son has a pair.
Beau questions the significance of burning shoes as a form of protest against Nike.
He suggests donating unwanted Nikes to shelters or thrift stores near military bases for those in need.
Beau challenges the idea of burning symbols without considering the workers behind the products.
He draws parallels between burning Nikes and burning the American flag as symbolic acts.
Beau questions the integrity of those who are quick to disassociate with Nike over a commercial but ignore their history of unethical practices.
He criticizes people who claim to value freedom but turn a blind eye to issues like sweatshops and slave labor.
Beau concludes by pointing out the irony of loving the symbol of freedom more than actual freedom itself.

Actions:

for consumers, activists, ethical shoppers,
Donate unwanted shoes to shelters or thrift stores near military bases (suggested)
Educate oneself on the ethical practices of companies before supporting them (implied)
</details>
