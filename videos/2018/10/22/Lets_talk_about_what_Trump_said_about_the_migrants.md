---
title: Let's talk about what Trump said about the migrants....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mrSIIZzOGOo) |
| Published | 2018/10/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President cutting off aid to Central American countries for not stopping people from coming to the United States is terrifying.
- The President believes it's the head of state's duty to prevent people from leaving their country.
- The wall along the southern border can also be used to keep Americans in.
- Beau warns about the slow creep of tyranny and the danger of losing freedom and liberty.
- Calling out those who support the President's actions as betraying their ideals for a red hat and slogan.
- Describing the wall as a prison wall that can trap people inside.
- Beau expresses concern about the implications of the President's statements on freedom and democracy.

### Quotes

- "That wall is a prison wall. It's not a border wall. It's gonna work both ways."
- "The face of tyranny is always mild at first and it's just now starting to smile at us."

### Oneliner

President's actions to cut aid for not stopping people from leaving their countries signal a dangerous erosion of freedom and democracy, with the wall serving as a potential prison barrier for Americans.

### Audience

Citizens, Activists

### On-the-ground actions from transcript

- Resist erosion of freedom: Stand up against policies threatening liberty (implied).
- Educate others: Share information about the potential consequences of oppressive measures (implied).

### Whats missing in summary

The emotional impact and urgency conveyed by Beau's speech.

### Tags

#Freedom #Democracy #Tyranny #BorderWall #Activism


## Transcript
Well howdy there internet people, it's Bo again.
So the president said something and the media didn't really comment on it.
They didn't, at least not to my satisfaction.
To me it was terrifying.
He said he's cutting off aid to these countries in Central America because they're not doing
their job.
They're not stopping their people from coming here.
And that's scary.
That is scary.
And a lot of people don't understand why I think it's scary, so let me explain.
Let me put that in a different way.
The President of the United States said that he believed it was the head of state's job
to stop people from leaving their country, in case you're still not getting it.
The man who wants to build a wall along our southern border and militarize it said that
he believed it was his duty as a head of state to keep you in the United States if need be.
I've said it from the beginning, that wall works both ways.
It will be used to keep Americans in.
There's nothing out of context about what he said.
It was real clear.
That's the job.
their job and they're not doing it so they're going to lose that aid and right
now you may be saying you can't think of a situation in which you'd want to leave
the United States maybe that's true but did you ever think that you wouldn't be
able to? This is the slow creep of tyranny you know the face of tyranny is
always mild at first and it's just now starting to smile at us. What he
said is terrifying. Now, I know there's still a whole lot of people who consider
themselves to be patriots that support this man. If you believe in the ideals
of freedom and liberty, you can't if you've chosen to you've sold out your
ideals and your principles and your country for a red hat and a cute slogan.
That wall is a prison wall. It's not a border wall. It's gonna work both ways.
It's gonna keep you in
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}