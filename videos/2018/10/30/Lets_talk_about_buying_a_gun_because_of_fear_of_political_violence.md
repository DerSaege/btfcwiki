---
title: Let's talk about buying a gun because of fear of political violence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Yv-0OQ8KSkM) |
| Published | 2018/10/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Warning viewers that the video will be long and dark due to the serious topic discussed.
- Addresses the sexism present in the gun community, specifically targeting advice given to women.
- Explains the misconception behind choosing a revolver for its ease of use and lack of safety features.
- Advises against purchasing a firearm if not comfortable with the idea of using it to kill.
- Emphasizes the importance of training and understanding the firearm before purchase.
- Recommends selecting a firearm based on one's environment and purpose.
- Compares choosing a firearm to selecting different types of shoes, each serving a specific purpose.
- Encourages seeking guidance from experienced individuals and trying out different firearms at a range.
- Stresses the necessity of training realistically and preparing to use the firearm effectively in potential life-threatening situations.
- Outlines the key elements needed to win a firefight: speed, surprise, and violence of action.
- Provides tactical advice on engaging in a firefight, including not leaving cover unnecessarily and prioritizing human life over material possessions.
- Urges individuals to be prepared for the reality of using a firearm and understanding its purpose.
- Recommends selecting a firearm based on military designs for simplicity and reliability.
- Acknowledges the unfortunate reality of needing to prepare for potential political violence and offers support to viewers in making informed decisions.

### Quotes

- "There is not a firearm in production, in modern production, that a man can handle that a woman can't."
- "Each one is designed with a specific purpose in mind. Your purpose, based on what you've said, is to kill a person."
- "If you're going to buy a firearm for the purposes that you've kind of expressed, you need to be prepared to kill."
- "You know, and I'm not trying to dissuade you from doing it. I think it's a good thing."
- "The best advice I can give you is to, you know, you've made the decision. Do it right."

### Oneliner

Be prepared to choose and train with a firearm for self-defense, understanding its purpose is ultimately for protection through lethal force.

### Audience

Women concerned about personal safety.

### On-the-ground actions from transcript

- Find someone experienced with firearms to help you choose and train with a firearm (implied).
- Practice shooting in various positions and scenarios to prepare realistically for potential threats (implied).
- Seek out ranges or individuals with access to different firearms for testing before making a purchase (implied).

### Whats missing in summary

The full video provides in-depth guidance on selecting, training, and using firearms for self-defense, focusing on the importance of understanding the purpose of owning a firearm and being prepared for potential violent situations.

### Tags

#Firearms #SelfDefense #Training #PersonalSafety #ViolenceProtection #GenderEquality


## Transcript
Well howdy there internet people, it's Beau again.
So I got a message
and I'm going to answer it. I'm going to go ahead and warn everybody else now
that this is going to be a long video and it's going to be a dark video.
There's
definitely not my normal video but
The question seemed very sincere.
And so we're just going to go with it from there,
just understand that this may not
be a topic a lot of people are comfortable with.
So the message, Bo, because of what happened at the synagogue,
I've decided to purchase a gun.
I've gotten advice from my friends,
and they have told me to buy either a revolver or a shotgun.
Do you agree with this advice? If not, why?
Okay, you were told that because you're a woman.
You were told that because you are a woman.
There is a lot of sexism in the gun crowd.
Now, don't go yell at your friends.
This is extremely common advice.
Most people don't even understand the subtext
of what's being said with that advice.
Okay, so a revolver.
A revolver doesn't have a safety.
Now, you didn't put it in your message, but let me tell you
what you probably heard.
So you want to get a revolver because it doesn't jam.
It's very easy to use, and it's point and shoot.
See, those sound like great qualities, but the subtext of
that is, A, you don't have to train, because it's easy to
use, and it's point and shoot.
You will train, because if you're going to purchase a
firearm, you need to.
You need to really understand what you're dealing with.
The subtext of that though, easy to use means it doesn't have a safety.
Well, little lady, that's because you're panicky and emotional and you'll forget to
flip a switch.
That's where it comes from.
That's where that advice comes from.
Now, whoever told you that probably doesn't know that, but that's really where that advice
began.
Shotgun.
You probably heard something like, see, when you rack that shotgun, the intruder will hit
hear that and run away. Maybe. Maybe. Shotgun being racked is a very distinctive sound and
it has been known to make people vacate their bowels and the premises. It will definitely
trigger flight or freeze. What if it doesn't trigger flight? Your main advantage when dealing
with an intruder in your home is that you know the layout.
You can surprise them.
When you rack that shotgun in an attempt to scare them,
you've given away your position.
That's not what you want to do.
A shotgun, a firearm of any kind,
is not meant to scare somebody.
It is meant to kill somebody.
If you are not comfortable with that,
do not purchase a firearm.
So how should a woman choose a firearm?
Same way a man should.
There is not a firearm in production, in modern
production, that a man can handle that a woman can't.
Well, that doesn't really tell you anything, does it?
What are you going to do with it?
I actually went to your profile to kind of guess and
get a read on you.
You've got your profile locked down pretty tight.
Good for you.
I mean, I couldn't really find out anything about you other
than your name, which is impressive.
If you live in a rural area, you probably want a rifle.
If you live in an apartment, you probably want a pistol.
Your terrain, your environment, is going to
dictate what kind of firearm you need.
I don't know.
And the easiest way I've found to relate this to women,
as I rail about people being sexist,
if this comes off that way, I apologize in advance.
Stilettos, flats, pumps, crocs, those little strappy sandals,
they all have different roles, right?
All have different purposes, designed
with something specific in mind.
Firearms are the same way.
They are the same way.
Each one is designed with a specific purpose in mind.
Your purpose, based on what you've said,
is to kill a person.
That's what you're worried about.
That's why you're getting it.
You're worried about political violence.
And I feel for you, I do.
I don't think you're crazy.
I wish you were.
I wish the honest advice I could give you
would be that you don't need one.
Good for you.
you're concerned and you're willing to accept responsibility for your own safety.
That's the reason I'm making this video.
So, then what?
You figured out the role.
Now there's a whole bunch of them out there.
You can't pick one
by walking into a gun store and just holding it.
You need to fire one.
Now some gun stores will allow you to take a
rent one
and go to the range that they have there.
Buy the ammo there and shoot it.
You, I'm being a little bit presumptuous here
based on your last name and the reason you want this.
There's probably somebody in your circle
or at your religious institution that is,
well let's just say he might say never again a lot.
Find that guy, okay?
I guarantee you one, that there's one at your synagogue, alright, find him.
He probably has a whole variety of firearms that he would be more than willing to let
you sample, take to the range and shoot, figure out what you like.
You know, without getting too far into it, a weapon is an extension of yourself.
You need to find one that fits you.
You're not going to be able to pick one by reading about them.
You're going to have to shoot them.
And that guy, and I assure you he exists,
that guy probably see it as his duty
to help you in this regard.
So once you have decided on your firearm
and you've purchased it, you're never
going to go back to one of those ranges like that again.
You're never going to go back to a range that says,
You can fire one shot every two seconds.
You must be standing there like this.
You're not going to do that.
Because you're going to train like you fight.
You're going to train to kill, not poke holes in paper.
If you're going to buy a firearm for the purposes that
you've kind of expressed, you need to be prepared to kill.
That's what you're saying.
So, you're going to find somebody that's got some property, and you're going to go
out there.
Now, there may be a range in your area, depending on where you live, that has a function in
Kill House, that has the ability to double tap and all this stuff.
Most ranges don't allow it because of the liability, but you're going to be careful
and either you're going to go to one of those ranges that has those facilities and actually
allows you to train or you're gonna find some property and you're gonna go out
and you're gonna shoot somewhere safely and you're gonna learn how to shoot from
a crouching position laying down walking forward walking backward you're going to
learn how to pull it out of your purse or wherever you plan on keeping it I
wouldn't suggest it there actually there's a bunch of stuff that get in
your way if you're gonna carry you'll figure it out but understand you're just
going to have to try it out. Find out what's right for you. One of the big
things in the gun crowd is that everybody's got an ego and everybody
thinks their way is right. It's not. It's not. It may be right for them, but that
doesn't mean it's going to be right for you. So you're going to train. You're
going to learn how to maintain that weapon. You're going to take it apart, put
it back together. You're going to clean it. You're going to learn about it.
it, otherwise don't give one. You're gonna learn how to shoot safely and then once
you've established all of that, then you're gonna really start training. When
you go to shoot, don't take silhouettes, take paper plates. You're gonna shoot at
paper plates because of the silhouette and it's got the score on it, right? And it
looks like a person. It causes people to kind of succumb to the close enough
syndrome. Well I got one round here, one round here, one round over here. Well
that'll put somebody down. No. If you're doing this you're in a fight for your
life. It'll probably work isn't good enough. So you're gonna get a paper
plate because that's about the size of the kill area on a human chest and
and about the size of a human head.
And when you can put a magazine into that,
you're on the right track.
One of the reasons you don't want to go to a normal range
is because they want you to take that break.
They don't want rapid fire because of the liability.
In real life, when you're actually engaging somebody,
anybody worth shooting once is worth shooting twice.
Any time you pull that trigger and it's aimed at a person,
you're pulling it twice.
Now again, in the gun crowd, some advice you're going to get,
you're going to hear people talk about groupings.
And that's how quickly or how closely they
can put the bullets together.
And the tighter, the better.
That's true if you're talking about paper
and you're in a tournament.
In real life, you want about two, three inches
separating your rounds.
Because when a bullet hits, it creates a wound channel,
but it also kills tissue around the wound.
There's no sense in shooting the same tissue twice.
So you're going to want them separated a little bit.
But still within that paper plate area.
Now, every ear doctor in the world
is going to send me hate mail for this.
But I would, at least once, fire without air protection.
Because in real life, you may not have it.
And you don't want to be surprised.
I can tell you from personal experience, first time
I fired without air protection on, I was not ready for it.
It can be disorienting, especially
if you're in a closed space.
It is very loud and it can actually be painful.
You need to be prepared for that.
It's not something I would recommend doing a lot,
because you will suffer hearing loss.
But you don't want the first time you experience that
to be with somebody shooting at you.
You want to be prepared for all of this up front.
So now we've covered the basics of training,
And there's a lot more to it.
And as you start shooting, you'll pick up more.
You'll pick up more little things that you're going to want to do and learn how to do.
And that's part of it.
Purchasing a firearm, going to the range once and throwing it in a box is one thing.
Actually learning how to fight and be prepared for the type of political violence you're
talking about is something entirely different.
It's a journey.
It's not an event.
So then the question becomes, how do you win a firefight?
You need three things.
You need speed, surprise, and violence of action.
Speed, you're going to get through your training.
You're going to be able to do everything you need to do.
Change magazines, you're going to practice that.
You're going to practice pulling it out.
You're going to practice all of this, and you're going to get quick.
And then on a tactical, on a higher level than that, once you've decided to act, you
make the decision you're going to act, you're going to engage.
At that point, you've got to do it quickly, surprise, exactly what it sounds like.
TV, I got a gun, freeze. No, no. Unless you're required to by the laws in your area, you
don't do that. There's no reason to. When you're talking about somebody walking into
a synagogue and that's the type of person you're worried about engaging, don't play
fair. Play as dirty as you were legally allowed to. If you can shoot them in the back, do
This type of thing, you're talking about combat, there's no honor in combat, there's
no playing fair, you win or you die.
And violence of action, violence of action is the last thing and that means that once
you have engaged, you bring down as much firepower onto that poor SOB as you can, as quickly
as you can.
You shock him.
make it to where it's just terrifying for the last few minutes of his existence, seconds
hopefully. Pretty dark, right? That's the reality. That is the reality of what you're
talking about. You know, and I'm not trying to dissuade you from doing it. I think it's
a good thing. If you want to prepare and you want to protect yourself and you're going
to accept that responsibility. It is fantastic. It's great. But you can't go into it blind.
You can't go into it blind. You have to understand what you're actually doing. So, what about
a home intruder? What are you going to do? Are you going to rush up and meet him? No.
It's one of the things that's ingrained in men and women.
I think it goes back to caveman days.
The men would rush forward and meet the wolves at the door, literally.
Meet the predator at the door, and the women would put the kids behind them.
You still see this today.
Here's the thing.
Bullets come out, and they travel a great distance.
There is never a reason to leave cover to engage somebody in the open.
You don't do it.
You stay protected.
You stay behind something.
As far as kids, there's that instinct.
You put you between yourself and the children and it makes sense on an emotional level.
On a logical level, if you're going to be shooting at somebody, odds are they're going
to be shooting back.
What if they miss you?
behind you. You put those kids in a bathroom and a bathtub, out of sight, out
of the line of fire, and you've got time to do that because you're not going to
worry about material possessions in the event of an intruder in your home. You're
going to lock down human life. You're going to protect life and that is it. If
somebody's coming into your house to steal TVs or whatever, you're waiting at
the hallway, to the bedrooms. You're not going out there to meet them. A TV isn't
worth dying over. Only protect human life. Only protect what matters. You know, the
layout of your home and the layout of anywhere, if you know it and the person
attacking doesn't, the opposition doesn't know the layout, you can win easily
because you can use speed surprise and violence of action and you can use that
layout. You want to catch them in what's called a fatal funnel. A fatal funnel.
That's exactly what it sounds like. A hallway with no side doors, you wait till
they're halfway in it and that's when you open fire. They can't go to the sides,
they can't move forward or backward without being without meeting your
violence of action and that's what you're going to want to do because
again you're trying to kill that's what you're setting out to do never forget
that. Now everybody's gonna have recommendations. My recommendation is you
get a firearm that is whatever class you decide, whatever role it is that you
you've decided to fill, you get a firearm that was used by military or one based
on a design that we use by the military. And the reason for that is they're
simple. Most militaries all over the world, they accept just about anybody. So
the firearms have to be very easy to use. You know, I've said it before, the M-16
was designed to be used by the dumbest 17 year old kid. You know that's what you
want. You want something easy to use, easy to maintain, and very reliable that's
what military firearms are. Other than that, I mean I wish you luck. I wish you
luck. I really wish we didn't live in a world where a woman who appears to be in
her early 20s is worried about getting gunned down while she's worshipping, but
But it's the world we live in.
You're worried about political violence, and it's on the rise.
So I really do wish I could give you different advice.
I wish I could say, you don't need to do this.
I really do, but I can't.
The best advice I can give you is to, you know, you've made the decision.
Do it right.
it right. Learn the weapon. Never forget that it is to kill. Nothing else. If you're not
going to do anything else with it, then kill. So there's a very atypical video from me.
Anyway, a whole bunch of thoughts there.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}