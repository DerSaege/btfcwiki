---
title: Let's talk about a toast for Justice Kavanaugh....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cswLNYAcjlc) |
| Published | 2018/10/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau sarcastically toasts Justice Kavanaugh's ascension to the Supreme Court, celebrating that his life wasn't ruined.
- Expresses relief that Kavanaugh won't have to go back to coal mining, implying privilege and entitlement.
- Beau celebrates Kavanaugh's confirmation, claiming it will make America safe and free again.
- Mocks the idea of Kavanaugh being tied down by concerns like jury trials, lawyers, or charges before detention.
- Criticizes the notion that government surveillance and lack of accountability will keep people safe and free.
- Ridicules the efficiency of the National Security Agency and their ability to gather information without warrants.
- Sarcastically praises local law enforcement for being able to stop and detain people without reason.
- Irony in Beau's confidence that government power won't be abused, dismissing concerns about rights violations.
- Beau prioritizes safety and freedom over rights, implying a trade-off that undermines fundamental liberties.
- Mocks the lack of scrutiny on Kavanaugh's rulings, attributing his support to beating feminists and leftists at any cost.

### Quotes

- "We got him up there and now America is going to be safe. We're going to be free again."
- "There's no way that's going to be abused because the government, they're good people."
- "We're gonna be free now. Okay, so yeah, we had to give up some rights, and sell out our country, and tread the Constitution."
- "Congratulations, I'm very glad that your life wasn't ruined."
- "We beat the feminists, we beat the leftists, we got a man's man up there on the Supreme Court."

### Oneliner

Beau sarcastically praises Kavanaugh's Supreme Court confirmation, celebrating perceived safety and freedom while disregarding rights and accountability.

### Audience

Justice advocates

### On-the-ground actions from transcript

- Challenge unjust power structures (implied)
- Advocate for accountability and transparency in government actions (implied)

### Whats missing in summary

The full transcript provides a scathing criticism of celebrating Kavanaugh's confirmation while ignoring concerns about rights and accountability. Viewing the complete video offers a deeper understanding of the sarcasm and irony used to address serious issues.

### Tags

#Justice #Rights #Government #Accountability #Criticism


## Transcript
Well, howdy there Internet people, it's Bo again.
I have been asked to give a toast
to Supreme Court Justice Kavanaugh
and kind of celebrate his ascension to the throne,
you know, up to the Supreme Court.
Congratulations, I'm very glad that your life wasn't ruined.
I was worried there for a minute.
He's worried you're gonna have to go back
like mine coal or something. So, we did it guys. We did it. We got him up there and now
America is going to be safe. We're going to be free again. You know why? Because we finally
got us a guy up there who isn't going to be tied down or worried about silly things like
jury trials or lawyers or even charges before somebody gets indefinitely detained. I mean,
That's going to keep us free, keep us safe right there.
And then, to make it even easier for the government to find the bad people, because they are a
very efficient organization and they don't make mistakes ever.
The National Security Agency, they got them a guy up there who's totally down with them
gathering information on us, without a warrant, for no reason.
Um, again, it'll keep us free, keep us safe. And then, and then the local cops, you guys are good
too. You guys got somebody up there bad for you, because this guy's totally down, with you being
able to stop people for no reason whatsoever, grab them, like literally grab them, physically
grab them, pat them down, rub on them and stuff, I mean, and ask for that identification,
you know, papers please.
There's no way that's going to be abused because the government, they're good people.
There's no, there's no pattern of abuse there that we need to worry about.
at all. And all that's gonna keep us free. It's gonna keep us free. It's gonna keep
us safe. And that's what's important, right? Safety. And freedom. Without our
rights. You know, it seemed, you know, now that I think about it, it seems kinda silly. It's
almost like none of us even looked into this man's rulings. We were just real
to beat them feminists, and then leftists, but we did that. We did it. We beat the
feminists, we beat the leftists, we got a man's man up there on the Supreme Court,
and it's good for us. We're gonna be free now. Okay, so yeah, we had to give up
some rights, and sell out our country, and tread the Constitution, but we sure
beat them me too, girls.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}