---
title: Let's talk about what combat vets can teach sexual assault survivors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uZloqoevlvk) |
| Published | 2018/10/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Admits feeling like a psychologist with overwhelming inbox messages.
- Acknowledges the benefit of sharing tough experiences.
- Advises women not to date combat vets due to detachment, fear of intimacy, and emotional issues.
- Points out the traumatic stress experienced by sexual assault survivors and combat vets.
- Questions the double standard of stigma towards dating sexual assault survivors.
- Challenges the notion of being "damaged goods" and unworthy of love.
- Criticizes the stigma around sexual assault survivors compared to combat vets.
- Condemns the concept of marking or branding women based on past experiences.
- Encourages women not to internalize feelings of being damaged goods.
- Shares a personal story illustrating the impact of trauma on memory and experiences.

### Quotes

- "You are not unclean. You are not damaged goods. You are certainly not unworthy of being loved."
- "We are all damaged goods in some way."
- "There's nothing wrong with you."
- "It's idiotic."
- "It is amazing what trauma can do to your memory."

### Oneliner

Beau addresses the stigma around dating sexual assault survivors, comparing their experiences to combat vets and challenging the concept of being "damaged goods."

### Audience
Survivors and Allies

### On-the-ground actions from transcript

- Support survivors by listening and validating their experiences (implied).
- Challenge stigmas and double standards around dating survivors of sexual assault (implied).
- Educate others on the similarities in emotional responses between combat vets and sexual assault survivors (implied).

### Whats missing in summary

The full transcript delves into the emotional impact of trauma on individuals and challenges societal stigmas surrounding survivors of sexual assault, urging for empathy and understanding.

### Tags

#CombatVets #SexualAssaultSurvivors #Stigma #Trauma #Empathy


## Transcript
Well, howdy there, internet people, it's Bo again.
I feel like a psychologist should have been answering my inbox for the last few days.
I don't mean that in a mean way.
I mean that in the sense that I don't know that I have the skill set to adequately respond
to some of the messages I've gotten.
I appreciate, and it's very touching, that you feel comfortable enough with me to share
what you did.
They did jog something for me.
So there was some benefit.
So tonight we're going to talk about what women can learn
from combat vets.
Now before I get into this, understand, ladies,
you don't actually want to date a combat vet.
Ever.
No, don't.
OK?
They're detached.
They are detached.
They got a fear of intimacy.
They've seen things.
They've seen things they can't share with you.
And even if they did share it with you,
you wouldn't understand.
They got issues.
They might have tried to self-medicate.
The beginning, you know,
waiting for the VA to call.
Probably didn't want to talk about it in the beginning.
Can't take them everywhere.
Get certain sights, sounds, and smells
trigger responses in them.
You can't surprise them with a hug or a kiss.
And then there's the emotional problems, I mean that hypervigilance, let's just be
honest they're emotional basket cases, I mean they're broke, they're tainted, unclean,
Damaged goods are a very common theme in those messages.
Ladies, the emotions you're feeling, the responses you're having, they have nothing
to do with the act that was visited upon you.
They're the same emotions that every combat vet feels.
They're the same responses that every combat vet feels.
Because they're the exact same thing.
It's traumatic stress.
That's what you're experiencing.
You're not unclean.
You are not damaged goods.
You are certainly not unworthy of being loved.
Now I have a question for the guys.
Do you tell your female friends not to date combat vets?
You don't, do you?
In fact, you probably use it as a selling point when you're trying to hook up your
buddy, oh, he's a great guy, he was with me in Iraq in 06, right?
Same emotional responses, the same responses.
So why on earth is there a stigma about dating sexual assault survivors?
If you're running around and you tell a guy, oh don't date her, you know what happened
to her?
She's broke.
We're all broke.
We are all damaged goods in some way.
See the thing is what makes it worse is that it's a cop out.
That's not the real reason you tell your friends that and you know it.
And that's the only time you're ever going to hear a man say, oh she's got a fear of
intimacy.
Are you kidding me?
back to that old dated issue, that old dated concept. She's marked, right? She's claimed.
Joking. We haven't moved past that, huh? You still, somewhere in the back of your mind,
believe that your member can somehow fundamentally change a woman, place a brand on her. You
out of your mind. It's idiotic. Ladies, this isn't on you. This is not on you. There's
nothing wrong with you. Yeah, you've got some trauma. We all do. We all do. In some
way shape or form. It may not be as severe as a combat vet or a survivor of sexual assault,
everybody's broke in some way. You certainly shouldn't feel, you know my
wife's always told me you never tell a woman how she should feel. In this case
you certainly should not feel that you are damaged goods, that you are unworthy
of being loved. That was possibly one of the hardest phrases I have ever had to
read. While we are on the topic of combat vets and sexual assault, I'm going to tell
you a story, and you can infer from this whatever you will. I know a guy, and the first time
he was in combat, it was a very unusual situation. There were some people that were on the do
not resuscitate list. These are bad guys and they just got to go. They should not be breathing,
period. There was such a rush to get there that these guys are in a civilian van when
they pull up. He can tell you that he was sitting in the front passenger seat that there
was a tear in the seat that he was sitting on, that he didn't have his weapon. The weapon
he had was so scratched up that it was almost silver in the back.
He could tell you that the van had a crank down window, old school.
He could tell you he fired first.
And he could tell you that his internal response to shooting the person for the first time
was, I got him.
Complete surprise that he was actually able to make the shot.
Benny remembers sitting on his helmet, looking over at some other guys, and the only reason
he remembers this is because something very graphic happened, I'll spare you that.
And that was after the boat was cleared.
He doesn't remember going on the boat.
He doesn't remember getting in a firefight on the boat.
He can't tell you exactly which members of his team were in the back of that van.
It is amazing what trauma can do to your memory.
It is amazing what trauma can do to your memory.
Anyway, some things to think about.
I hope y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}