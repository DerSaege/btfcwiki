# All videos from October, 2018
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2018-10-30: Let's talk about buying a gun because of fear of political violence.... (<a href="https://youtube.com/watch?v=Yv-0OQ8KSkM">watch</a> || <a href="/videos/2018/10/30/Lets_talk_about_buying_a_gun_because_of_fear_of_political_violence">transcript &amp; editable summary</a>)

Be prepared to choose and train with a firearm for self-defense, understanding its purpose is ultimately for protection through lethal force.

</summary>

"There is not a firearm in production, in modern production, that a man can handle that a woman can't."
"Each one is designed with a specific purpose in mind. Your purpose, based on what you've said, is to kill a person."
"If you're going to buy a firearm for the purposes that you've kind of expressed, you need to be prepared to kill."
"You know, and I'm not trying to dissuade you from doing it. I think it's a good thing."
"The best advice I can give you is to, you know, you've made the decision. Do it right."

### AI summary (High error rate! Edit errors on video page)

Warning viewers that the video will be long and dark due to the serious topic discussed.
Addresses the sexism present in the gun community, specifically targeting advice given to women.
Explains the misconception behind choosing a revolver for its ease of use and lack of safety features.
Advises against purchasing a firearm if not comfortable with the idea of using it to kill.
Emphasizes the importance of training and understanding the firearm before purchase.
Recommends selecting a firearm based on one's environment and purpose.
Compares choosing a firearm to selecting different types of shoes, each serving a specific purpose.
Encourages seeking guidance from experienced individuals and trying out different firearms at a range.
Stresses the necessity of training realistically and preparing to use the firearm effectively in potential life-threatening situations.
Outlines the key elements needed to win a firefight: speed, surprise, and violence of action.
Provides tactical advice on engaging in a firefight, including not leaving cover unnecessarily and prioritizing human life over material possessions.
Urges individuals to be prepared for the reality of using a firearm and understanding its purpose.
Recommends selecting a firearm based on military designs for simplicity and reliability.
Acknowledges the unfortunate reality of needing to prepare for potential political violence and offers support to viewers in making informed decisions.

Actions:

for women concerned about personal safety.,
Find someone experienced with firearms to help you choose and train with a firearm (implied).
Practice shooting in various positions and scenarios to prepare realistically for potential threats (implied).
Seek out ranges or individuals with access to different firearms for testing before making a purchase (implied).
</details>
<details>
<summary>
2018-10-30: Let's talk about Halloween costumes.... (<a href="https://youtube.com/watch?v=G7ANo1S0Jqk">watch</a> || <a href="/videos/2018/10/30/Lets_talk_about_Halloween_costumes">transcript &amp; editable summary</a>)

Beau addresses offensive Halloween costumes, criticizes using edginess as humor, and warns against hiding behind internet anonymity to spread hate.

</summary>

"Being offensive and edgy is not actually a substitute for having a sense of humor."
"But you can't be mad when you suffer the consequences of your actions."
"Bigotry is out. It's something that sane people have given up on."
"Try being a good person. Like I said, you can do whatever you want."
"People are going to remember it."

### AI summary (High error rate! Edit errors on video page)

Addresses offensive Halloween costumes, including blackface and dressing up as Trayvon Martin or Adolf Hitler.
Expresses lack of understanding for the shock when people face backlash for intentionally offensive actions.
Points out that being offensive and edgy is not a substitute for having a sense of humor.
Talks about how jokes about bullying and tormenting marginalized groups were considered acceptable in the past.
Criticizes those who hide behind internet anonymity to spew hate and offensive remarks.
Urges people to try being a good person instead of just being shocking or edgy.
Stresses that while individuals have the right to dress and act however they want, others have the right to look down on them.
Comments on the idea of political correctness and how it aims to change flawed ways of thinking.
Mentions the current political climate but warns against mistaking it for a permanent change in culture.
Concludes by stating that people will not accept or excuse harmful actions, even if they were meant as jokes.

Actions:

for social media users,
Be a good person (exemplified)
Refrain from making offensive jokes (exemplified)
Speak out against harmful actions (exemplified)
Challenge flawed ways of thinking (exemplified)
</details>
<details>
<summary>
2018-10-28: Let's talk about militias going to the border.... (<a href="https://youtube.com/watch?v=wN-XFH5giKQ">watch</a> || <a href="/videos/2018/10/28/Lets_talk_about_militias_going_to_the_border">transcript &amp; editable summary</a>)

Beau Ginn on the Mexican border with militia, raising concerns about the legality and ethical implications of their actions while potentially compromising their group's secrecy and intentions.

</summary>

"We're down here on the border protecting America with our guns, ready to kill a bunch of unarmed asylum seekers who are following the law."
"Hey, Colonel!"

### AI summary (High error rate! Edit errors on video page)

Beau is on the Mexican border with his militia commander, tasked with protecting America.
They won't stop refugees on the Mexican side as it violates U.S., Mexican, and international law.
Using guns to stop refugees could result in committing a felony and losing gun rights.
Sending back individuals at gunpoint from the American side is also against the law and goes against their core principles.
Beau acknowledges that individuals entering the U.S. have rights, including due process.
They are essentially backing up the Department of Homeland Security (DHS) despite being labeled potential terrorists or extremists in a DHS pamphlet.
Beau expresses concern that their actions could have negative consequences if they follow through with their rhetoric.
Beau questions the wisdom of their militia commanders and suggests they might be strategizing with "4D chess" to manage optics.
Despite portraying themselves as protecting America, Beau acknowledges the grim reality of potentially harming unarmed asylum seekers following the law.
Beau ends with a casual greeting to a Colonel, leaving the situation open-ended.

Actions:

for militia members, border patrol supporters,
Question the ethics and legality of their actions (implied)
Reassess the group's strategies and potential consequences (implied)
</details>
<details>
<summary>
2018-10-28: Let's talk about context.... (<a href="https://youtube.com/watch?v=iqNDL5FiLsg">watch</a> || <a href="/videos/2018/10/28/Lets_talk_about_context">transcript &amp; editable summary</a>)

Beau stresses the importance of context in understanding global events beyond misleading sound bites, cautioning against the rise of propaganda.

</summary>

"Context, it's really important."
"If you want to understand what's going on in the world, you have to look beyond the sound bites."
"Propaganda is coming into a golden age right now."
"Most times the sound bite, just like in Kennedy's speech, it's the exact opposite of what's really being said and really being done."
"Taking a whole bunch of information and getting down to what's actually important."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the importance of sound bites and context, using Kennedy's speech as an example.
Kennedy's speech, often quoted for specific sound bites, is misunderstood without the full context.
The speech was about the Soviet Union, not a vague conspiracy theory.
The main aim of the speech was to encourage self-censorship among journalists, editors, and publishers.
Beau criticizes the modern reliance on sound bites due to shortened attention spans from 24-hour news and social media.
He points out the dangers of Twitter as a platform for substanceless communication.
Beau questions the reasons behind U.S. involvement in Syria, attributing it to the U.S. starting the Civil War for ulterior motives.
The motive may have been related to competing pipelines supported by different countries.
Propaganda plays a significant role in shaping public perceptions and is on the rise.
Beau urges viewers to look beyond sound bites and understand the interconnected nature of global events.

Actions:

for media consumers, critical thinkers,
Question mainstream narratives (suggested)
Research beyond headlines (exemplified)
Analyze global events critically (implied)
</details>
<details>
<summary>
2018-10-26: Let's talk about fear of asylum seekers and the browning of America.... (<a href="https://youtube.com/watch?v=iU88FU4snpU">watch</a> || <a href="/videos/2018/10/26/Lets_talk_about_fear_of_asylum_seekers_and_the_browning_of_America">transcript &amp; editable summary</a>)

Beau challenges fear-based narratives on asylum seekers and racial diversity, envisioning a future where racial distinctions blur for a more united society.

</summary>

"Light will become darker. Dark will become lighter. Eventually we're all going to look like Brazilians."
"It's gonna be a lot harder to drum up support for wars that aren't really necessary because you're not killing some random person that doesn't look like you."
"Maybe the best thing for everybody is to blur those racial lines a little bit."
"Y'all have a nice night."

### AI summary (High error rate! Edit errors on video page)

Addresses fears about asylum seekers, including concerns about ISIS, MS-13, diseases, and more.
Questions the validity of these fears by pointing out the lack of evidence despite asylum seekers coming for 15 years.
Suggests that politicians use fear to win elections by tapping into people's insecurities.
Believes the real fear in society is different, illustrated through a personal story about interracial relationships.
Expresses confusion over the fear of diversity and the "browning of America."
Envisions a future where racial distinctions blur and politicians may have to lead differently.
Contemplates whether America was meant to be a melting pot or a fruit salad with separate elements.

Actions:

for americans,
Embrace diversity in your community by fostering relationships with people from different backgrounds (exemplified).
Challenge stereotypes and prejudices by engaging in open and honest dialogues with friends and family (implied).
</details>
<details>
<summary>
2018-10-24: Let's talk about the caravans.... (<a href="https://youtube.com/watch?v=9fCCnOsQiv0">watch</a> || <a href="/videos/2018/10/24/Lets_talk_about_the_caravans">transcript &amp; editable summary</a>)

Caravans fleeing violence will continue due to US policies; assisting migrants is cheaper than detention, and turning them away risks lives needlessly.

</summary>

"It's cheaper to be a good person."
"Blaming the victim."
"Give me one good reason to send these people back to their deaths."
"You can't preach freedom and then deny it."
"These people will die."

### AI summary (High error rate! Edit errors on video page)

Caravans have been coming for 15 years, and they will continue due to US drug war policies and interventions in other countries.
The current caravan is from Honduras, a country with a murder rate of 159 per 100,000, compared to Chicago's 15-20 per 100,000.
Tax dollars fund assistance for migrants, costing about half a billion dollars annually.
It's cheaper to assist migrants in getting on their feet than to detain them.
Migrants travel in large groups for safety, similar to the principle of trick-or-treating in groups.
Carrying other countries' flags allows migrants to connect with fellow countrymen during the journey.
Wearing military fatigues is practical for durability during long journeys.
Migrants are legally claiming asylum by arriving at ports of entry or within the country.
Blaming migrants for seeking asylum overlooks the impact of US foreign policy and the drug war on their home countries.
Beau challenges those advocating to turn migrants away by asking for one good reason to send them back to potential death, criticizing fear-based policies.

Actions:

for advocates, policymakers, activists,
Contact your representative to address how US foreign policy impacts other countries (suggested)
Advocate for changes in drug war policies and intervention practices (suggested)
</details>
<details>
<summary>
2018-10-22: Let's talk about what Trump said about the migrants.... (<a href="https://youtube.com/watch?v=mrSIIZzOGOo">watch</a> || <a href="/videos/2018/10/22/Lets_talk_about_what_Trump_said_about_the_migrants">transcript &amp; editable summary</a>)

President's actions to cut aid for not stopping people from leaving their countries signal a dangerous erosion of freedom and democracy, with the wall serving as a potential prison barrier for Americans.

</summary>

"That wall is a prison wall. It's not a border wall. It's gonna work both ways."
"The face of tyranny is always mild at first and it's just now starting to smile at us."

### AI summary (High error rate! Edit errors on video page)

President cutting off aid to Central American countries for not stopping people from coming to the United States is terrifying.
The President believes it's the head of state's duty to prevent people from leaving their country.
The wall along the southern border can also be used to keep Americans in.
Beau warns about the slow creep of tyranny and the danger of losing freedom and liberty.
Calling out those who support the President's actions as betraying their ideals for a red hat and slogan.
Describing the wall as a prison wall that can trap people inside.
Beau expresses concern about the implications of the President's statements on freedom and democracy.

Actions:

for citizens, activists,
Resist erosion of freedom: Stand up against policies threatening liberty (implied).
Educate others: Share information about the potential consequences of oppressive measures (implied).
</details>
<details>
<summary>
2018-10-21: Let's talk about when it's appropriate to call the police.... (<a href="https://youtube.com/watch?v=pG9TKx1J8eM">watch</a> || <a href="/videos/2018/10/21/Lets_talk_about_when_it_s_appropriate_to_call_the_police">transcript &amp; editable summary</a>)

Police can escalate any situation to the point of death; only call them when it's truly necessary. Non-compliance with police can lead to violence or death, even for minor offenses. Be cautious in involving law enforcement, as every call to 911 carries the possibility of a deadly outcome.

</summary>

"Every law is backed up by penalty of death."
"If nobody's being harmed, it's not really a crime."
"Don't call the law unless death is an appropriate response."
"Calling the police on someone for insignificant reasons puts their life in jeopardy."
"Every call to 911 carries the possibility of a deadly outcome."

### AI summary (High error rate! Edit errors on video page)

Police can escalate any situation, even trivial ones, to the point of death.
Jaywalking, a seemingly harmless act, can lead to fatal consequences.
Non-compliance with police can result in violence or death, even for minor offenses.
Calling the police on someone for insignificant reasons puts their life in jeopardy.
Only call the police when the situation truly warrants a response of death.
If nobody is being harmed, it's not a crime, even if it may be illegal.
Patience with dealing with unnecessary police intervention varies based on race.
Police involvement can escalate situations needlessly, especially for people of color.
The potential consequences of involving the police should be carefully considered.
Every call to 911 carries the possibility of a deadly outcome.

Actions:

for community members,
Practice de-escalation techniques when faced with conflicts (implied)
Educate yourself and others on alternatives to calling the police (implied)
</details>
<details>
<summary>
2018-10-20: Let's talk about leaving a real blackout to find a figurative one.... (<a href="https://youtube.com/watch?v=ReUajdqynRk">watch</a> || <a href="/videos/2018/10/20/Lets_talk_about_leaving_a_real_blackout_to_find_a_figurative_one">transcript &amp; editable summary</a>)

Beau warns against censorship of independent news outlets opposed to government policies and advocates for seeking alternative social media platforms to preserve free discourse.

</summary>

"Facebook and Twitter are no longer free discussion platforms."
"Be ready. Look for another social media network."
"This is going to be the end of free discourse on Facebook."

### AI summary (High error rate! Edit errors on video page)

Beau is out for a drive to escape the noise of the chainsaw and trees, giving him a chance to scroll through the internet.
Facebook and Twitter have censored independent news outlets opposed to government policies, alarming Beau.
Beau differentiates between these outlets and Alex Jones, noting the dangerous nature of Jones' content.
Many independent news outlets were shut down on Facebook and Twitter in one day, worrying Beau about the impact on free discourse.
Carrie Wedler, a journalist Beau recommends, had her outlet and personal Twitter account banned in the censorship sweep.
Outlets Beau works with were not affected, but he finds the censorship chilling as an independent network.
Beau warns against comparing the censored outlets to Alex Jones and stresses that dissenting from government policies is not a reason for censorship.
He points out that Facebook and Twitter are no longer platforms for free discourse and may be altering public discourse at the government's request.
Beau encourages people to seek alternative social media networks as censorship may not stop with these outlets.
He mentions platforms like Steam It and MeWe as alternatives where censored information may be found and shared.
Tens of millions of subscribers were affected by the censorship, leading Beau to believe this may mark the end of free discourse on Facebook.
Independent outlets like Anti-Media provide good reporting with accountability, something lacking in major corporate networks.
Beau urges people to prepare for a shift away from Facebook to maintain access to diverse perspectives and information.

Actions:

for social media users,
Find and join alternative social media networks like Steam It and MeWe to access and share censored information (suggested).
Prepare to shift away from Facebook to maintain access to diverse perspectives and information (implied).
</details>
<details>
<summary>
2018-10-18: Let's talk about natural disasters, looting, and being prepared.... (<a href="https://youtube.com/watch?v=dRayLlNeARk">watch</a> || <a href="/videos/2018/10/18/Lets_talk_about_natural_disasters_looting_and_being_prepared">transcript &amp; editable summary</a>)

Beau updates on hurricane aftermath, criticizes law enforcement's response to looting, praises Florida National Guard, and urges viewers to prepare emergency kits for survival.

</summary>

"In the event of a natural disaster, protecting the inventory of some large corporation should pretty much be at the bottom of any law enforcement agencies list."
"You can't count on government response. You can't."
"Simple things, you know, little flashlights, stuff like that, just have it all in one spot."
"We're not talking about building a bunker and preparing for doomsday."
"Please take the time to read it, to put one of those bags together."

### AI summary (High error rate! Edit errors on video page)

Updates internet audience on his situation after being caught in a hurricane.
Expresses gratitude for the comments and reassures everyone that he and his loved ones are safe, despite the aftermath of the hurricane.
Addresses the issue of looting after natural disasters and criticizes law enforcement's response.
Points out the media's biased portrayal of looting, especially concerning different races.
Criticizes law enforcement's decision to shut down areas due to looting threats, preventing volunteers from helping those in need.
Questions the priorities of law enforcement when it comes to protecting property over saving lives during a disaster.
Acknowledges the Florida National Guard's quick and effective response to the hurricane.
Urges people to be prepared for emergencies by assembling a basic emergency kit with essentials like food, water, and medical supplies.
Provides resources and links for viewers to create their emergency kits at various budget levels.
Emphasizes the importance of being prepared for emergencies without going overboard or breaking the bank.

Actions:

for community members,
Assemble an emergency kit with essentials like food, water, fire supplies, shelter, medical supplies, and a knife (suggested).
Put together emergency bags for different budget levels (suggested).
</details>
<details>
<summary>
2018-10-10: Let's talk about what we can learn from a Japanese history professor.... (<a href="https://youtube.com/watch?v=F8c3XkDAMiU">watch</a> || <a href="/videos/2018/10/10/Lets_talk_about_what_we_can_learn_from_a_Japanese_history_professor">transcript &amp; editable summary</a>)

Beau shares a tale urging against silence in the face of tyranny, stressing the importance of speaking out for freedom.

</summary>

"When you remain silent in the face of tyranny or oppression, you have chosen a side."
"Your silence is only helping them."
"Tyranny anywhere is a threat to freedom everywhere."

### AI summary (High error rate! Edit errors on video page)

Introduces a word-of-mouth story about a history professor in Japan before World War II.
The professor predicts the war and moves his family to the secluded island of Iwo Jima.
Despite having the potential to influence policy and possibly alter the course of the war, the professor chooses to remain silent.
Beau underscores the significance of not remaining silent in the face of tyranny or oppression.
Talks about the implications of remaining silent and how it equates to siding with those in power.
Emphasizes the interconnectedness of power dynamics and communication in today's world.
Criticizes the societal norm of avoiding topics like money, politics, and religion at the dinner table.
Urges for open discussions and dialogues on critical issues instead of sticking to talking points.
Stresses the importance of speaking out against tyranny to protect freedom.
Encourages starting necessary dialogues among ourselves rather than relying on politicians.

Actions:

for activists, advocates, citizens,
Initiate open dialogues with community members to address critical issues (suggested)
Speak out against tyranny and oppression in your local context (implied)
</details>
<details>
<summary>
2018-10-09: Let's talk about the perception of your comments on the Kavanaugh case.... (<a href="https://youtube.com/watch?v=pfsgb7CySdQ">watch</a> || <a href="/videos/2018/10/09/Lets_talk_about_the_perception_of_your_comments_on_the_Kavanaugh_case">transcript &amp; editable summary</a>)

Beau urges men to reconsider harmful comments, salvage relationships, and apologize for perpetuating a culture of disbelief towards sexual assault victims.

</summary>

"You don't get to rape her."
"You've given them all the reason in the world to never come forward."
"I got caught up in politics. I didn't care."

### AI summary (High error rate! Edit errors on video page)

Received a message from a young lady who is unable to face her father due to his comments, appreciating Beau's perspective.
Urges men to reconsider their harmful comments and salvage relationships, rather than focusing on divisive topics like the Ford Kavanaugh case.
Points out that one in five women have experienced sexual assault, and criticizes the common arguments used to doubt victims.
Addresses the question of why victims wait to come forward, attributing it to fear of not being believed or facing consequences.
Challenges the notion of victim blaming based on behavior, stating that regardless of circumstances, rape is never justified.
Calls out the damaging impact of justifying inappropriate behavior, leading to underreporting of sexual assaults.
Encourages men to have honest conversations with the women in their lives and apologize for harmful comments that perpetuate a culture of disbelief.

Actions:

for men and fathers,
Have honest and open conversations with the women in your life to understand the impact of harmful comments (suggested)
Apologize for perpetuating a culture of disbelief and victim blaming (suggested)
</details>
<details>
<summary>
2018-10-08: Let's talk about a toast for Justice Kavanaugh.... (<a href="https://youtube.com/watch?v=cswLNYAcjlc">watch</a> || <a href="/videos/2018/10/08/Lets_talk_about_a_toast_for_Justice_Kavanaugh">transcript &amp; editable summary</a>)

Beau sarcastically praises Kavanaugh's Supreme Court confirmation, celebrating perceived safety and freedom while disregarding rights and accountability.

</summary>

"We got him up there and now America is going to be safe. We're going to be free again."
"There's no way that's going to be abused because the government, they're good people."
"We're gonna be free now. Okay, so yeah, we had to give up some rights, and sell out our country, and tread the Constitution."
"Congratulations, I'm very glad that your life wasn't ruined."
"We beat the feminists, we beat the leftists, we got a man's man up there on the Supreme Court."

### AI summary (High error rate! Edit errors on video page)

Beau sarcastically toasts Justice Kavanaugh's ascension to the Supreme Court, celebrating that his life wasn't ruined.
Expresses relief that Kavanaugh won't have to go back to coal mining, implying privilege and entitlement.
Beau celebrates Kavanaugh's confirmation, claiming it will make America safe and free again.
Mocks the idea of Kavanaugh being tied down by concerns like jury trials, lawyers, or charges before detention.
Criticizes the notion that government surveillance and lack of accountability will keep people safe and free.
Ridicules the efficiency of the National Security Agency and their ability to gather information without warrants.
Sarcastically praises local law enforcement for being able to stop and detain people without reason.
Irony in Beau's confidence that government power won't be abused, dismissing concerns about rights violations.
Beau prioritizes safety and freedom over rights, implying a trade-off that undermines fundamental liberties.
Mocks the lack of scrutiny on Kavanaugh's rulings, attributing his support to beating feminists and leftists at any cost.

Actions:

for justice advocates,
Challenge unjust power structures (implied)
Advocate for accountability and transparency in government actions (implied)
</details>
<details>
<summary>
2018-10-07: Let's talk about authority figures, trusted sources, and my story.... (<a href="https://youtube.com/watch?v=fLSOMM1eCs4">watch</a> || <a href="/videos/2018/10/07/Lets_talk_about_authority_figures_trusted_sources_and_my_story">transcript &amp; editable summary</a>)

Beau warns against blind obedience to authority, advocates for critical thinking, and encourages viewers to trust themselves over external sources.

</summary>

"That obedience to authority, that submission to authority. This person is an authority figure. They're a trusted source, so we're gonna believe what they say. It's not a good thing."
"A lot of bad things have happened in history because people didn't do that."
"Trust yourself. Trust yourself."
"Ideas stand or fall on their own."
"Don't trust your sources, trust your facts, and trust yourself."

### AI summary (High error rate! Edit errors on video page)

Viewers question his identity and credibility due to a contradiction between what he says and what they expect.
People want to know his backstory to determine how much trust to place in his words.
Beau rejects blind trust in authority figures and encourages fact-checking and critical thinking.
He warns against blind obedience to authority, citing the dangers it poses.
Beau mentions instances where people failed to fact-check, leading to disastrous consequences like the Iraq war.
Referring to Milgram's experiments, Beau reveals how many individuals are willing to harm others under authority's influence.
He stresses the importance of trusting oneself and thinking independently rather than relying solely on authority figures.
Beau values dissenting opinions and encourages viewers to think for themselves.
He believes that ideas should be judged based on their merit, not on the speaker's credentials.
Beau concludes by advocating for fact-checking, trusting in personal judgment, and encouraging independent thinking.

Actions:

for viewers,
Fact-check information before sharing or believing it (implied)
Encourage critical thinking and independent judgment in your community (implied)
</details>
<details>
<summary>
2018-10-06: Let's talk about the Confederate flag and the New South.... (<a href="https://youtube.com/watch?v=FMIOCj7a3tI">watch</a> || <a href="/videos/2018/10/06/Lets_talk_about_the_Confederate_flag_and_the_New_South">transcript &amp; editable summary</a>)

Beau challenges the romanticized notion of the Confederate flag as heritage, asserting its racist connotations amidst a changing Southern identity.

</summary>

"It's drawing up images of the most immoral, evil, and worst times in Southern history."
"I don't think it's about heritage. Not for a second."
"That symbol doesn't represent the South; it represents races."
"It's a new South. Don't go away mad. Just go away."

### AI summary (High error rate! Edit errors on video page)

The Confederate flag represents a test for people discussing Southern culture.
Southern culture existed before and after the Confederacy, making the flag's symbolism questionable.
The Confederate flag evokes immoral and evil times in Southern history.
Beau questions why individuals choose to associate with a symbol tied to such negative connotations.
The original Confederate flag differs from the commonly recognized flag, representing Robert E. Lee's army.
Beau suggests flying the real Confederate flag if one truly seeks to honor heritage, not the battle flag.
The Confederate flag's recent history links it to opposition to desegregation and racist acts.
Beau challenges the notion that flying the Confederate flag is about heritage, citing its divisive history.
He recounts an encounter with a former gang member who offered insights on the symbolism of flag-waving.
Beau asserts that his Black neighbors embody Southern culture more authentically than flag-wavers.

Actions:

for southern residents,
Educate others on the true history and implications of the Confederate flag (suggested)
Advocate for the removal of Confederate monuments and symbols from public spaces (implied)
</details>
<details>
<summary>
2018-10-05: Let's talk about gun control (Part 5: Bumpstocks).... (<a href="https://youtube.com/watch?v=HqVMiL7Qi0Y">watch</a> || <a href="/videos/2018/10/05/Lets_talk_about_gun_control_Part_5_Bumpstocks">transcript &amp; editable summary</a>)

Beau explains bump stocks, disputes their life-saving potential, and examines their necessity under the Second Amendment, ultimately questioning the need for a ban.

</summary>

"It's not going to save any lives."
"A lot of guys believe that to exercise the intent of the Second Amendment, which is to fight back against the government if need be, you'd need to be able to lay down suppressive fire."
"No man, nothing should be banned."
"You're not gonna see me at a protest trying to make sure that a bump stock doesn't get banned."
"Nine of them are gonna be complete people you probably don't want to hang around."

### AI summary (High error rate! Edit errors on video page)

Explains how bump stocks work to mimic fully automatic fire.
Mentions the inaccuracy of fully automatic fire.
Recounts the infamous use of bump stocks in the Vegas mass shooting.
Points out that bump stocks may increase a shooter's lethality in enclosed spaces like schools.
Disputes the notion that bump stocks save lives during mass shootings.
Analyzes the necessity of bump stocks under the Second Amendment's intent.
Describes the different perspectives among gun enthusiasts and Second Amendment supporters regarding bump stocks.
Talks about the concept of suppressive fire and its relevance to the Second Amendment.
States his opinion that bump stocks are not necessary in the context of insurgency against the government.
Expresses reservations about banning bump stocks but acknowledges potential concerns.

Actions:

for firearm enthusiasts, second amendment supporters,
Question the necessity of bump stocks in relation to the Second Amendment (implied).
Engage in respectful and informed debates about firearm regulations (implied).
</details>
<details>
<summary>
2018-10-04: Let's talk about what combat vets can teach sexual assault survivors.... (<a href="https://youtube.com/watch?v=uZloqoevlvk">watch</a> || <a href="/videos/2018/10/04/Lets_talk_about_what_combat_vets_can_teach_sexual_assault_survivors">transcript &amp; editable summary</a>)

Beau addresses the stigma around dating sexual assault survivors, comparing their experiences to combat vets and challenging the concept of being "damaged goods."

</summary>

"You are not unclean. You are not damaged goods. You are certainly not unworthy of being loved."
"We are all damaged goods in some way."
"There's nothing wrong with you."
"It's idiotic."
"It is amazing what trauma can do to your memory."

### AI summary (High error rate! Edit errors on video page)

Admits feeling like a psychologist with overwhelming inbox messages.
Acknowledges the benefit of sharing tough experiences.
Advises women not to date combat vets due to detachment, fear of intimacy, and emotional issues.
Points out the traumatic stress experienced by sexual assault survivors and combat vets.
Questions the double standard of stigma towards dating sexual assault survivors.
Challenges the notion of being "damaged goods" and unworthy of love.
Criticizes the stigma around sexual assault survivors compared to combat vets.
Condemns the concept of marking or branding women based on past experiences.
Encourages women not to internalize feelings of being damaged goods.
Shares a personal story illustrating the impact of trauma on memory and experiences.

Actions:

for survivors and allies,
Support survivors by listening and validating their experiences (implied).
Challenge stigmas and double standards around dating survivors of sexual assault (implied).
Educate others on the similarities in emotional responses between combat vets and sexual assault survivors (implied).
</details>
<details>
<summary>
2018-10-03: Let's talk about Trump , Obama , and monsters... (<a href="https://youtube.com/watch?v=PJNF3tfo0RE">watch</a> || <a href="/videos/2018/10/03/Lets_talk_about_Trump_Obama_and_monsters">transcript &amp; editable summary</a>)

President Trump's actions mock sexual assault survivors and contribute to a dangerous culture of silencing victims, while Beau contrasts his character with Obama and warns against dehumanizing perpetrators.

</summary>

"There's a concerted effort in this country to silence anyone willing to come forth with a sexual assault claim, and it's being led by the President of the United States."
"Honor, integrity, are those words that come to mind when you think of President Trump?"
"The scariest Nazi wasn't a monster. He was your neighbor."
"Scariest rapist or rape-apologist, well they're your neighbor too."
"y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

President of the United States mocked sexual assault survivors on television, setting a shockingly low bar.
The President's actions contribute to a concerted effort in the country to silence sexual assault claims.
Referencing Trump's infamous "Grab-Em-By-The-P****" quote, Beau questions what it takes to stop such behavior.
Beau contrasts Trump with Obama, acknowledging policy disagreements but noting the vast difference in character.
He criticizes Trump for casting doubt on women coming forward with claims and talks about the presumption of innocence.
Beau warns against dehumanizing perpetrators, stressing that they are people, not monsters.
He draws a parallel between excusing Nazis as monsters and how society views rapists and assault predators.
Beau underscores the danger of overlooking evil in plain sight due to political allegiance and nationalism.

Actions:

for activists, advocates, voters,
Challenge efforts to silence sexual assault survivors (implied)
Advocate for the presumption of innocence and fair treatment for survivors (implied)
Refrain from dehumanizing perpetrators and understand the importance of accountability (implied)
</details>
<details>
<summary>
2018-10-01: Let's talk about the presumption of innocence... (<a href="https://youtube.com/watch?v=Zu3IWDRgtSw">watch</a> || <a href="/videos/2018/10/01/Lets_talk_about_the_presumption_of_innocence">transcript &amp; editable summary</a>)

Beau covers the presumption of innocence, Kavanaugh's support for law enforcement overreach, and the dangers of party politics.

</summary>

"It's almost like they aren't familiar with his rulings."
"You've bought into bumper sticker politics and you're trading away your country for a red hat."

### AI summary (High error rate! Edit errors on video page)

Comments on the presumption of innocence in relation to Kavanaugh and the job interview.
Notes Kavanaugh's support for law enforcement to stop individuals on the street.
Mentions Kavanaugh's support for warrantless searches by the National Security Agency.
Points out Kavanaugh's involvement in the indefinite detention of innocent people.
Criticizes Kavanaugh for apparently lying during his confirmation hearings.
Emphasizes the importance of the presumption of innocence in the American justice system.
Criticizes Kavanaugh for undermining the Fourth Amendment.
Talks about the dangers of party politics and signing away others' rights.
Warns about the potential consequences of supporting Kavanaugh's nomination based on party affiliation.
Urges people to be aware of the implications of supporting candidates without understanding their rulings.

Actions:

for voters, constitution advocates.,
Know the background of candidates before supporting them (implied).
</details>
<details>
<summary>
2018-10-01: Let's talk about dating my daughter and a joke that needs to die.... (<a href="https://youtube.com/watch?v=MMSAcZ90QI8">watch</a> || <a href="/videos/2018/10/01/Lets_talk_about_dating_my_daughter_and_a_joke_that_needs_to_die">transcript &amp; editable summary</a>)

Fathers intimidating their daughters' boyfriends with guns sends harmful messages about trust, autonomy, and empowerment.

</summary>

"It's time to let this joke die, guys. It's not a good look. It's not a good joke."
"Guns should not be used as props. If you're breaking out a gun, you should be getting ready to destroy or kill something."
"She doesn't need a man to protect her. She's got this."

### AI summary (High error rate! Edit errors on video page)

Fathers posting photos with guns next to their daughters' boyfriends is a common social media joke, but it sends a harmful message.
The joke implies that without the threat of a firearm, the boyfriend will force himself on the daughter, which should not be a possibility.
Waving a firearm around also suggests a lack of trust in the daughter's judgment and autonomy.
The message behind the photo is that the daughter needs a man to protect her and that she lacks the capability to defend herself.
This tradition of intimidating boyfriends with guns is based on a dated concept of property rights and is ultimately disempowering for the daughter.
Beau questions the need for a firearm to intimidate a teenage boy, stating that it says more about the father than the boyfriend.
Guns should not be used as props or toys; their presence should signal a serious intent to destroy or kill.
Beau shares a personal experience where he had to address a boyfriend's behavior without needing a firearm.
Fathers should focus on empowering their daughters, building trust, providing information, and fostering open communication.
Beau urges fathers to reconsider perpetuating this joke and to only bring out guns when absolutely necessary.

Actions:

for fathers, parents,
Have open and honest communication with your children about trust, autonomy, and safety (implied).
Foster empowerment and independence in your children by trusting their judgment and capabilities (implied).
Refrain from using guns as props or toys, and only bring them out when absolutely necessary (implied).
</details>
