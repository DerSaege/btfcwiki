---
title: Let's talk about when it's appropriate to call the police....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pG9TKx1J8eM) |
| Published | 2018/10/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Police can escalate any situation, even trivial ones, to the point of death.
- Jaywalking, a seemingly harmless act, can lead to fatal consequences.
- Non-compliance with police can result in violence or death, even for minor offenses.
- Calling the police on someone for insignificant reasons puts their life in jeopardy.
- Only call the police when the situation truly warrants a response of death.
- If nobody is being harmed, it's not a crime, even if it may be illegal.
- Patience with dealing with unnecessary police intervention varies based on race.
- Police involvement can escalate situations needlessly, especially for people of color.
- The potential consequences of involving the police should be carefully considered.
- Every call to 911 carries the possibility of a deadly outcome.

### Quotes

- "Every law is backed up by penalty of death."
- "If nobody's being harmed, it's not really a crime."
- "Don't call the law unless death is an appropriate response."
- "Calling the police on someone for insignificant reasons puts their life in jeopardy."
- "Every call to 911 carries the possibility of a deadly outcome."

### Oneliner

Police can escalate any situation to the point of death; only call them when it's truly necessary. Non-compliance with police can lead to violence or death, even for minor offenses. Be cautious in involving law enforcement, as every call to 911 carries the possibility of a deadly outcome.

### Audience

Community members

### On-the-ground actions from transcript

- Practice de-escalation techniques when faced with conflicts (implied)
- Educate yourself and others on alternatives to calling the police (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the implications of involving law enforcement and the disproportionate impact on marginalized communities.


## Transcript
Well, howdy there, internet people, it's Beau again.
So I guess we need to talk about when it's the appropriate
time to call the police, because there's some confusion.
Here's the thing, and this is what everybody needs to
remember, there is no law so insignificant, so trivial, so
stupid, that a cop will not put a bullet in somebody over.
Every law is backed up by penalty of death.
Jay Walking is my favorite example.
There's ever a law that shouldn't exist, that's it.
Crossing the street in the wrong spot.
OK.
So you cross the street in the wrong spot.
The cop writes you a ticket.
You don't go to court today because you're not going to
pay a fine for crossing the street.
What happens?
You've committed no violence at this point.
Some men are going to show up, or they're going to wait for
you to get pulled over.
And then they're going to throw you in a cage.
Now, still, no violence, and you're already having violence visited upon you, but what
happens if you refuse to go?
You're not going to be thrown in jail over crossing a street.
So you respond, and you don't go with them.
You resist.
They kill you.
You resist hard enough, they kill you.
Every law, no matter how insignificant, is backed up by penalty of death.
So every Barbecue Becky or Permit Patty or Keyfob Katie that turns around and calls the
law is really putting this other person's life in jeopardy.
Nobody's being harmed by these actions.
Nobody's being hurt.
And that's really the only time you should call the law.
If death is an appropriate response to what's happening, that's it.
That's when you call the cops.
Any other time, mind your business.
Mind your business.
You know, out here we have that saying, strong fences make good neighbors.
That's true everywhere, you know.
If nobody's being harmed, it's not really a crime.
It may be illegal, but if nobody's being harmed, it's certainly not immoral.
You know, I was, and one other thing I got to touch on, because it kind of, when the
guy said it, it took me, it took me a minute.
I was talking about the key five Katie thing, and I'm like, now that dude was calm as a
Hindu cow.
I mean, I do not have the level of patience that is needed to deal with that level of
stupidity for that length of time I couldn't have done it I mean I I know my
limitations and that was far beyond it and the guy said well that's cuz you're
white what I didn't understand and he asked he's like what do you think is
gonna happen if a black guy gets loud and start screaming at a white lady
Yeah, somebody's going to call the cops.
Then what's going to happen?
Somebody who's on his way home minding his own business, first he gets harassed and then
he's got the cops showing up, he might get angry.
What if he doesn't want to comply?
happens. Don't call the law unless death is an appropriate response because it's
a very real possibility every time you dial 9-1-1. Anyway, it's just a thought.
y'all y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}