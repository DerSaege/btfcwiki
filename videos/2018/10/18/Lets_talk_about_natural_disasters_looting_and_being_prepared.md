---
title: Let's talk about natural disasters, looting, and being prepared....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dRayLlNeARk) |
| Published | 2018/10/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates internet audience on his situation after being caught in a hurricane.
- Expresses gratitude for the comments and reassures everyone that he and his loved ones are safe, despite the aftermath of the hurricane.
- Addresses the issue of looting after natural disasters and criticizes law enforcement's response.
- Points out the media's biased portrayal of looting, especially concerning different races.
- Criticizes law enforcement's decision to shut down areas due to looting threats, preventing volunteers from helping those in need.
- Questions the priorities of law enforcement when it comes to protecting property over saving lives during a disaster.
- Acknowledges the Florida National Guard's quick and effective response to the hurricane.
- Urges people to be prepared for emergencies by assembling a basic emergency kit with essentials like food, water, and medical supplies.
- Provides resources and links for viewers to create their emergency kits at various budget levels.
- Emphasizes the importance of being prepared for emergencies without going overboard or breaking the bank.

### Quotes

- "In the event of a natural disaster, protecting the inventory of some large corporation should pretty much be at the bottom of any law enforcement agencies list."
- "You can't count on government response. You can't."
- "Simple things, you know, little flashlights, stuff like that, just have it all in one spot."
- "We're not talking about building a bunker and preparing for doomsday."
- "Please take the time to read it, to put one of those bags together."

### Oneliner

Beau updates on hurricane aftermath, criticizes law enforcement's response to looting, praises Florida National Guard, and urges viewers to prepare emergency kits for survival.

### Audience

Community members

### On-the-ground actions from transcript

- Assemble an emergency kit with essentials like food, water, fire supplies, shelter, medical supplies, and a knife (suggested).
- Put together emergency bags for different budget levels (suggested).

### Whats missing in summary

The full transcript provides more details on Beau's personal experience during the hurricane aftermath and offers practical advice on preparing for emergencies.

### Tags

#Hurricane #EmergencyPreparedness #LootingAfterDisasters #CommunityResponse #NaturalDisasters


## Transcript
Well, howdy there, internet people, it's Bo again.
I'm back.
I'm sorry I've been absent.
We got caught in a little rainstorm.
Might've seen it on the TV.
A hurricane Michael hit us at a four here.
That was a surprise.
We weren't really expecting it to hit this far inland
with that much strength.
Just so everybody knows, I saw the comments.
Thank you very much.
The house is fine, the shop's fine, everybody's fine.
The dogs are fine, everything's okay.
had a lot of trees fall in, lost power, stuff like that.
There are a lot of people to the south of me
who are in a bad way, though.
So this is the perfect time to talk
about natural disasters, government response
to natural disasters, and what you
can do so you don't have to rely on the government response.
One of the things that has become a huge problem
in the aftermath of disasters is looting.
And it's not the looting that has actually become the problem.
The problem is law enforcement response to looting includes shutting down the county.
Now what that means is that volunteers can't come in to help.
And that is causing a real problem.
We had one local county to the south of Maine do that.
People that I knew participated in relief efforts in Harvey could not get in.
And these were people that could have done a lot of good, especially that first day.
And they were not allowed in. And that's an issue. Now the reason cops do this is
partially by media propaganda. You know, after every disaster, the helicopter flies
over and they find typically a black family that is looting. You know, carrying
bread and food and water, you know, because that's looting. And I'll go ahead and
tell you this right now, that's not looting, that's surviving. In the event of a natural
disaster like that, I cannot judge somebody harshly for that. But normally
that's what you see and they make a big deal out of it. People are looting. Now
Now the reason they started making a big deal out of it was after Katrina, when the government
response was lacking, they wanted to show what people had to resort to.
And there were definitely some unusual tones to some of the coverage.
If it was a black family, they were looting.
If it was a white family, they had found food.
You can look back and see that.
It's very blatant.
Anyway, but let's talk about real looting and let's talk about what the sheriff or the
law enforcement of an area of a jurisdiction is actually saying when they close down their
county because of the threat of looting.
Now I know they're cops and they pretty much believe everybody's a criminal, so we'll
give them two to one.
Let's say that for every one rescue worker that's going to come in, there are two people
looting and those two people looting each one of them has a van and that
rescue worker he's only gonna stay one day and he's gonna leave at lunch after
he saves one person. Looters do not go to people's home during a natural disaster
and conduct home invasions doesn't happen that that's not what they do they
go hit whatever unoccupied big-box store that has the power down and there's no
alarm. That's what they hit. So let's say that those two looters fill those vans
up, top to bottom, TVs, computers, everything. Anything they can get their
hands on of value. So what law enforcement is saying is that two van
fulls of wet TVs are worth more than your life. That's really what they're
saying. In the event of a natural disaster, protecting the inventory of
some large corporation should pretty much be at the bottom of any law
enforcement agencies list. That is not a high priority, but because of that,
people die. And that may sound dramatic, but we got a thousand people missing
just south of me, and a whole lot of them are from the county that closed down. Now
I got to wonder how many of those could have been found that first day when they
weren't letting people in. So now here we are a week into it. I've still yet to see
FEMA. I saw one red cross truck. The only federal agency that I have actually
seen was Border Patrol and they were getting out. They were leaving. They were
evacuating. Now while I'm sitting here trashing the government response, I do
want to point out that Florida National Guard, it seemed like they were on scene
before the wind stopped. They did a great job and they showed up. You could tell
these guys were these guys were rousted and I mean some of you could tell
they were Floridians, you know. They got their plate carriers on and
shorts, you know, completely out of regs, but it didn't matter because they were
doing the job and they were handing out food, water, were there to help people
that needed it. There are probably a lot of people that made it that would not
have if it hadn't have been for them and them showing up as quickly as they did.
So, but it is a state agency and Florida's National Guard is, well let's
just say it's a little different. Anyway, so the end result is you can't count on
government response. You can't. In fact, some of the things that your government
agencies may do will make it worse for you. So what can you do? If you watch
these videos often, you know normally there's a whole bunch of bags back here.
They're not there right now because they're in the house.
Putting together a kit, a bag, for emergencies is something that every
family should do. There are a lot of people that would not be in as bad a
situation as they are had they done that. Simple things, you know, little
flashlights, stuff like that, just have it all in one spot. The thing is when
people read one of these lists of stuff that's supposed to be in one of these
bags, they're like, well I have all this. You do, but it's spread all over your
house and you won't be able to find it. Take it, put it together. I'm gonna put
some links in the comments that will help people put one of these bags
together and I'll put a couple of them for different budget levels. Do it. Take
the time. It doesn't take long. It's not a big process. We're not talking
about building a bunker and preparing for doomsday. We're just talking about
enough to survive a week or two until things normalize and it's not hard and
it's really not that expensive and trust me we opened almost every one of those
bags we needed something out of it so definitely take take the time to do it
if you're not going to read the article short version you need food water fire
shelter medical supplies and a knife and when I say medical supplies I don't just
me in a first aid kit, if somebody in your family is on daily meds you need to have a
couple weeks of those extra in that bag because pharmacies will not be open.
Anyway, so just a thought, please, please take the time to read it, to put one of those
bags together. Anyway, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}