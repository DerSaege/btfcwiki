---
title: Let's talk about what we can learn from a Japanese history professor....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=F8c3XkDAMiU) |
| Published | 2018/10/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces a word-of-mouth story about a history professor in Japan before World War II.
- The professor predicts the war and moves his family to the secluded island of Iwo Jima.
- Despite having the potential to influence policy and possibly alter the course of the war, the professor chooses to remain silent.
- Beau underscores the significance of not remaining silent in the face of tyranny or oppression.
- Talks about the implications of remaining silent and how it equates to siding with those in power.
- Emphasizes the interconnectedness of power dynamics and communication in today's world.
- Criticizes the societal norm of avoiding topics like money, politics, and religion at the dinner table.
- Urges for open discussions and dialogues on critical issues instead of sticking to talking points.
- Stresses the importance of speaking out against tyranny to protect freedom.
- Encourages starting necessary dialogues among ourselves rather than relying on politicians.

### Quotes

- "When you remain silent in the face of tyranny or oppression, you have chosen a side."
- "Your silence is only helping them."
- "Tyranny anywhere is a threat to freedom everywhere."

### Oneliner

Beau shares a tale urging against silence in the face of tyranny, stressing the importance of speaking out for freedom. 

### Audience

Activists, Advocates, Citizens

### On-the-ground actions from transcript

- Initiate open dialogues with community members to address critical issues (suggested)
- Speak out against tyranny and oppression in your local context (implied)

### Whats missing in summary

The full transcript provides a thought-provoking narrative that challenges individuals to break the silence and have vital dialogues on pressing matters.

### Tags

#Tyranny #Freedom #SpeakOut #Silence #OpenDialogues


## Transcript
Well, howdy there, internet people, it's Beau again.
So tonight we're gonna start off with a tale.
I say a tale because it's a word of mouth story.
And over the years, some of the details have changed.
Every time I hear it, it's a little bit different.
But the main facts are the same,
so we're gonna stick with those.
Prior to World War II, before the shooting ever started,
there was a history professor in Japan.
He worked at one of those universities in Tokyo, very prominent fellow, very smart fellow.
He looks around and he sees it coming.
He can tell a war is on the horizon and he knows that when that war comes, Japan will
expand too far, too fast, and it is all going to come collapsing in on itself, and when
it does, where is it going to land?
Tokyo.
Why?
Because history does not repeat, but it rhymes.
So he's aware of all of this, and he wants to get out.
He doesn't want to be in Tokyo when that happens, so he starts doing some research, and he finds
this island.
Now, it's away from everything, but still in Japan.
It's a decent-sized island with a real low population.
There's only 1,000 people on it.
One cop, mail comes once a month, perfect, except for the fact there's no university,
nowhere for him to teach, but he is so certain of what's going to happen, he doesn't care.
He uproots his family and heads down there, and I think he becomes a sugar planter.
The name of the island was Iwo Jima.
Now if you don't know anything about World War II, just say that he jumped out of frying
pan and into the fire.
That iconic image of the Marines planting that flag and pushing it up, that was Mount
Harabachi on Iwo Jima became such an important image because that was the
scene of some pretty fierce fighting. And then afterward the Japanese army came in
and evacuated everybody. All the civilians had to get off the island. To
this day, it's only military there. So why did I bring this up? Because he
exercised his right to remain silent. See, he's a pretty prominent guy. He could
influence policy. Maybe he talked to some powerful people. Changed the course of
that war. Maybe he could just talk to normal people and get enough of them
together. Same thing. Maybe even stop the war. But he didn't. That's unlikely that
that would have happened, but he knew what was gonna happen if he did nothing.
He was so convinced of it he moved his entire family. When you remain silent in
In the face of tyranny or oppression, you have chosen a side.
It is not neutrality.
It is not being apolitical.
You are siding with those in power.
You are siding with the oppressor.
Power today has become so centralized and everything is so interconnected through communications
and economics that there's no running.
You can't, you know, it's not like the old days where you could head to the new world
or head out west, everything's connected.
That right to remain silent when you exercise it and you choose to be silent, you're not
really keeping your head down, you're not laying low.
They've got us trained in this country.
What are the three things you don't talk about at the dinner table?
Money, politics, and religion, right?
Sounds like a good rule.
What are the three things that everybody's getting killed over all the time?
Maybe it's not such a good rule.
Maybe we need to start talking about those things.
Maybe we need to start having discussions instead of debates.
Start talking about things instead of having talking points.
Because the people that are in power,
They're already in power.
Your silence is only helping them.
And that slow march of tyranny just moves forward without you saying something.
And tyranny anywhere is a threat to freedom everywhere.
So maybe it's time that we start talking about all the things we're not supposed to.
And talking about it amongst ourselves, instead of letting politicians who have a very vested
interest in keeping us fighting, they don't need to be involved in that conversation.
They don't need to be in that discussion.
They certainly don't need to be leading it.
Anyway, just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}