---
title: Let's talk about the presumption of innocence...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Zu3IWDRgtSw) |
| Published | 2018/10/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Comments on the presumption of innocence in relation to Kavanaugh and the job interview.
- Notes Kavanaugh's support for law enforcement to stop individuals on the street.
- Mentions Kavanaugh's support for warrantless searches by the National Security Agency.
- Points out Kavanaugh's involvement in the indefinite detention of innocent people.
- Criticizes Kavanaugh for apparently lying during his confirmation hearings.
- Emphasizes the importance of the presumption of innocence in the American justice system.
- Criticizes Kavanaugh for undermining the Fourth Amendment.
- Talks about the dangers of party politics and signing away others' rights.
- Warns about the potential consequences of supporting Kavanaugh's nomination based on party affiliation.
- Urges people to be aware of the implications of supporting candidates without understanding their rulings.

### Quotes

- "It's almost like they aren't familiar with his rulings."
- "You've bought into bumper sticker politics and you're trading away your country for a red hat."

### Oneliner

Beau covers the presumption of innocence, Kavanaugh's support for law enforcement overreach, and the dangers of party politics.

### Audience

Voters, Constitution advocates.

### On-the-ground actions from transcript

- Know the background of candidates before supporting them (implied).

### Whats missing in summary

Deeper analysis of how party politics can compromise constitutional rights.

### Tags

#PresumptionOfInnocence #Kavanaugh #PartyPolitics #Constitution #Rights


## Transcript
Well howdy there internet people, it's Beau again.
Did that video on sexual assault and I brought up Kavanaugh.
Man, comment section is filled, filled with people
talking about the presumption of innocence.
I didn't anticipate that.
I didn't anticipate it for two reasons.
One, it's a job interview, not a legal proceeding.
And two, you know Mr. Kavanaugh
Supports the ability of law enforcement to stop you on the street, demand your identification,
and then pat you down and search you?
Do you know that he supports the National Security Agency collecting metadata on you
through a warrantless search?
You know, he was apparently complicit in the indefinite detention of hundreds of innocent
people. You know that he apparently lied during his first confirmation hearings
about that same subject and later on you know he sits there says I don't know
anything about it. Later on it turns out that he was actually like you know I
don't know that the America or that the Supreme Court would be okay with
detaining people without a lawyer.
It is important to talk about the presumption of innocence.
It's a cornerstone of the American justice system.
It's extremely important.
Y'all have covered that in the comments section.
You know who doesn't get it?
Who does not get the presumption of innocence?
A man who spent his entire career trying to undermine it.
A man who was gutted the Fourth Amendment at almost every chance he had.
Seems odd.
Seems odd that all of a sudden he cares.
Seems odd that people would bring that up in his defense.
It's almost like they aren't familiar with his rulings.
It's almost like the only reason they support the candidate, the nomination of this man
to go on a court whose entire job is to safeguard the Constitution of the United States to include
the Fourth Amendment is that it's party politics.
See here's the thing, and this is the reason America has gone so far, so quickly, down
the toilet.
Because of this party politics and playing one side against the other, everybody's always
willing to sign away somebody else's rights because they don't think it's
going to apply to them.
Here's the thing guys, in just a few years, it's not going to be a Republican
president, it's going to be a Democrat and all the executive power that
Kavanaugh grants to Trump, that's going to be in the hands of some Democrat.
Might even be Hillary Clinton.
And if you don't kill yourself on the night that she's elected, you're going to have to live with that.
Knowing that somebody that doesn't believe in the Fourth Amendment, that has done everything he can to undermine your
presumption of innocence, is sitting on the Supreme Court.  It's just a thought, something you need to think about.
You can sit there and you can wave your flag and you can talk about making America great again.
Don't expect anybody who knows anything about this man's rulings or knows anything about the Constitution,
knows anything about the Supreme Court to believe a word you say.
You've bought into bumper sticker politics and you're trading away your country for a red hat.
for a red hat. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}