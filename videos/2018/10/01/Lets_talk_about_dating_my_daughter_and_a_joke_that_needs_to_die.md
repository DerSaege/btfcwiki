---
title: Let's talk about dating my daughter and a joke that needs to die....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MMSAcZ90QI8) |
| Published | 2018/10/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Fathers posting photos with guns next to their daughters' boyfriends is a common social media joke, but it sends a harmful message.
- The joke implies that without the threat of a firearm, the boyfriend will force himself on the daughter, which should not be a possibility.
- Waving a firearm around also suggests a lack of trust in the daughter's judgment and autonomy.
- The message behind the photo is that the daughter needs a man to protect her and that she lacks the capability to defend herself.
- This tradition of intimidating boyfriends with guns is based on a dated concept of property rights and is ultimately disempowering for the daughter.
- Beau questions the need for a firearm to intimidate a teenage boy, stating that it says more about the father than the boyfriend.
- Guns should not be used as props or toys; their presence should signal a serious intent to destroy or kill.
- Beau shares a personal experience where he had to address a boyfriend's behavior without needing a firearm.
- Fathers should focus on empowering their daughters, building trust, providing information, and fostering open communication.
- Beau urges fathers to reconsider perpetuating this joke and to only bring out guns when absolutely necessary.

### Quotes

- "It's time to let this joke die, guys. It's not a good look. It's not a good joke."
- "Guns should not be used as props. If you're breaking out a gun, you should be getting ready to destroy or kill something."
- "She doesn't need a man to protect her. She's got this."

### Oneliner

Fathers intimidating their daughters' boyfriends with guns sends harmful messages about trust, autonomy, and empowerment.

### Audience

Fathers, Parents

### On-the-ground actions from transcript

- Have open and honest communication with your children about trust, autonomy, and safety (implied).
- Foster empowerment and independence in your children by trusting their judgment and capabilities (implied).
- Refrain from using guns as props or toys, and only bring them out when absolutely necessary (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of the harmful implications of fathers using guns to intimidate their daughters' boyfriends, urging for a shift towards empowering and trusting daughters rather than resorting to intimidation tactics.

### Tags

#Parenting #Empowerment #Trust #Communication #GunSafety


## Transcript
Howdy there Internet people, it's Beau again.
I heard y'all wanna date my daughter,
you're gonna have her home by 11, right?
Yeah, it's that time of year again.
We're about to see all kind of photos on social media
of fathers standing there holding rifles and shotguns,
standing next to some teenage boy
that they're having to have a talk to.
I know it's a joke, nine times out of 10 today, it's a joke.
But let's talk about the joke,
because it's not a good joke,
and it's sending a message to your daughter,
I'm not sure that you realize the message is sending.
It's probably not one you actually want to send.
So what's a joke?
The joke is that if this boy isn't threatened with a
firearm, that he's going to force himself on your daughter.
Man, if you believe that, you probably shouldn't let him go
out.
I mean, that seems kind of reckless on your part.
I mean, that shouldn't even be a possibility.
If you even believe that in the slightest,
you got any inkling,
you need to stop that date before it starts.
Now, the alternative is that you don't believe that.
And then I gotta ask why you're waving a firearm around.
I know somebody's gonna say,
well, it's not about him forcing himself on her,
it's about bad judgment.
You're gonna kill him over bad judgment, huh, okay.
Let's talk about the message you're sending to your daughter.
First, she needs a man to protect her.
She's not capable of doing that herself.
And you'll be there, shotgun in hand, until she's married.
And you hand her off to some other man, right?
It's that property rights thing we talked about
in that other video.
It's a very dated concept.
And to be honest, guys, it's a little creepy.
The other messengers send in is that you don't trust her,
you don't trust her, you don't trust her judgment.
Because you don't trust her judgment
in the boy that she picked,
and you don't trust her to do what you want her to do.
So much so that you got to threaten to kill this boy.
So, if she wants to do something
that you don't want her to do,
he'll still say no.
Man, that's a lot of distrust there.
Not very empowering, any of this.
What do you think's gonna happen if young Juliet
goes a little bit further than daddy wants her to?
You think she's gonna come home and tell you?
Oh no, because you're gonna put a load of buckshot
in a Romeo.
You're killing communication with your daughter, guys.
And then think about that, two young people out there doing that, without all the information.
A lot of bad things can happen that way.
If only there was someone in their life, in her life, that cared enough to tell her how
to be safe.
Or I guess you could post a photo on Facebook.
I guess that's cool, too.
The other side to this coin is that I do understand
that there are times when you may need to put the fear
of God into some teenage boy.
I get that.
It might happen, I had to do it.
She was 16, he was a couple years older,
and he wasn't a bad guy in the sense
that he was gonna hurt her.
But he made a lot of dumb decisions.
You know what I didn't need to have that conversation?
A firearm.
You need a firearm to intimidate a teenage boy?
As a father, that says more about you than him.
These are not toys.
This is.
But guns aren't toys.
Guns aren't props.
If you're breaking out a gun, you should be getting ready to destroy or kill something.
If you're not, what are you doing?
What message are you really trying to send?
Does it have anything to do with your daughter?
Or does it have to do with you looking like a real man and in the process pointing out
that you can't intimidate a teenage boy without a gun.
I'm going to assume that since you took the time to take a photo with your daughter's
boyfriend that you're involved in her life.
You probably want her to feel empowered.
She doesn't need a man to protect her.
She's got this.
You probably want her to feel trusted.
You probably want her to have all the information she needs.
You probably want her to be able to come to you with anything.
It's time to let this joke die, guys.
It's not a good look.
It's not a good joke.
It's saying a lot of bad things all at once.
And please, put the guns away unless you need it.
Just something to think about.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}