---
title: Let's talk about leaving a real blackout to find a figurative one....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ReUajdqynRk) |
| Published | 2018/10/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau is out for a drive to escape the noise of the chainsaw and trees, giving him a chance to scroll through the internet.
- Facebook and Twitter have censored independent news outlets opposed to government policies, alarming Beau.
- Beau differentiates between these outlets and Alex Jones, noting the dangerous nature of Jones' content.
- Many independent news outlets were shut down on Facebook and Twitter in one day, worrying Beau about the impact on free discourse.
- Carrie Wedler, a journalist Beau recommends, had her outlet and personal Twitter account banned in the censorship sweep.
- Outlets Beau works with were not affected, but he finds the censorship chilling as an independent network.
- Beau warns against comparing the censored outlets to Alex Jones and stresses that dissenting from government policies is not a reason for censorship.
- He points out that Facebook and Twitter are no longer platforms for free discourse and may be altering public discourse at the government's request.
- Beau encourages people to seek alternative social media networks as censorship may not stop with these outlets.
- He mentions platforms like Steam It and MeWe as alternatives where censored information may be found and shared.
- Tens of millions of subscribers were affected by the censorship, leading Beau to believe this may mark the end of free discourse on Facebook.
- Independent outlets like Anti-Media provide good reporting with accountability, something lacking in major corporate networks.
- Beau urges people to prepare for a shift away from Facebook to maintain access to diverse perspectives and information.

### Quotes

- "Facebook and Twitter are no longer free discussion platforms."
- "Be ready. Look for another social media network."
- "This is going to be the end of free discourse on Facebook."

### Oneliner

Beau warns against censorship of independent news outlets opposed to government policies and advocates for seeking alternative social media platforms to preserve free discourse.

### Audience

Social media users

### On-the-ground actions from transcript

- Find and join alternative social media networks like Steam It and MeWe to access and share censored information (suggested).
- Prepare to shift away from Facebook to maintain access to diverse perspectives and information (implied).

### Whats missing in summary

The full transcript provides a detailed insight into the censorship of independent news outlets on social media platforms and the implications for free discourse.


## Transcript
Well howdy there internet people, it's Bo again.
We're out for a drive today because, well frankly,
I need to get out of the house.
Get away from the chainsaw and the trees.
So, here's the thing.
I come out of one blackout to realize I'm in another.
I finally get somewhere where I have enough
internet connection to scroll,
and I find out that Facebook and Twitter
has gone through and censored a bunch of outlets,
a bunch of news outlets.
Now they're all independent,
None of them are owned by massive corporations, and they all share one thing in common.
They're opposed to government policies, and that's terrifying.
That is terrifying.
You know, people are trying to make the connection to Alex Jones, and it's like, well, you know,
it wasn't a problem when they did it to Alex Jones.
You're right, it wasn't.
Alex Jones kind of advocated for people to be burned to death because of their sexual
orientation. Alex Jones talks about hybrid fishmen. Alex Jones is a con artist.
These outlets are news outlets for the most part. Some are sensationalist, some
are garbage, but some are real outlets. Even the sensationalist ones are filling
a purpose. There are a lot of people who won't realize there's a problem until
they see some crazy headline. And there's some that do that. Now before we get too
much further into this, I do in the interest of full disclosure, I need to
point out that I know a lot of these people. I know a lot of the editors and
the reporters that work at these places. Nick Burnaby over at Anti-Media, we're
friends. And I don't mean like, oh we've talked on Facebook. I mean like we've got
drunk in a casino together. We're friends. At the same time, Jason over at Free Thought
Project, well we're not friends. We've actually had some semi-public disagreements over his
presentation because it is a little sensationalist. But at the same time, it's filling a role
that some people really need. And when they start shutting down dissent on that level
800 pages in one day on Facebook and Twitter. That's insane. That is insane.
Now one journalist that I would actually suggest everybody follow, she is a friend of mine
and she's really good, her name is Carrie Wedler. Not only was her outlet caught up
in it but her personal Twitter account got banned and this was all in one day
and Facebook gave basically said that they engaged in spam which they don't
they share each other's work and I maybe it kind of looks systematic to a to a
computer but no they're not spam they don't share anything that isn't from you
you know, isn't news-oriented, it isn't what the people signed up for.
Now I should also point out that the outlets that I work with didn't get caught up in this.
None of them did.
They didn't, none of them got banned, so this isn't me saying, you know, bringing this up
because I was personally affected by it.
The Fifth Column, Pontiac, Tribune, Greed, none of them got hit, it all focused on these
other outlets but it's chilling to me because we're an independent network and
this very easily could have been us. You could have got on and I'd be gone.
It's kind of terrifying. Now the two things you need to take away from this
is first and foremost don't compare them to Alex Jones. Even the worst among them
wasn't that level, not even close. So don't let that comparison happen and
don't make that comparison. You know, there are some people that are out there
going, well, you know, you should have supported this and you should have spoke
up when it was Alex Jones. No, no. Alex Jones was calling for innocent people to
be killed.
No, he can be censored. Simply disagreeing with the government policy is a whole
other issue and that's what these outlets did. That was their offense. The
other thing you need to take away from this is that Facebook and Twitter are no
longer free discussion platforms. You know there were always reasonable
limits you know on what what could be said on either outlet and I understand
that they're private companies they can do what they want but now they've hit a
point where they're trying to alter public discourse and it appears that this
was done at the request of the government. Now Facebook is a private company and they
can do as they wish, but this is one step, this is one step short of an actual First
Amendment violation, a massive one, a coordinated one, specifically targeting outlets that speak
out against government abuses. One of these outlets, all they do is report on police misconduct.
that's it. One of them is kind of a marijuana advocacy thing. And then a lot of them talk
about foreign policy. And you know, they break stories related to government involvement
in the Mideast and in Africa. And a lot of times, it's the only place you can get that
information. So you need to be looking for another social media network. You need to
be getting ready to move. You know, it happened with MySpace. Everybody just left. It's gonna
happen with Facebook. Because this type of censorship, once it starts, it doesn't stop.
And yeah, Free Thought Project, it's sensationalist. It is. But it's no more sensationalist than
Fox News or CNN. So, be ready. Look, I know that some of these outlets are setting up
on places called Steam It. There's a website called Steam It, which is a little weird to
me to be honest. I've checked it out, it's a little odd. And then there's one called
MeWe, which is very similar to Facebook. That's where they're going. And that's where you're
going to have to find their information. Because now it's not going to be on Facebook. You're
going to have to find it there and then share it to Facebook. Now, this isn't a small thing
When we're talking about it, we're talking about tens of millions of subscribers that
were simply taken off the rolls by Facebook.
I know if anti-media had, I want to say 2.7 million subscribers, that one channel.
And you can go to their website, I mean, they post, you know, they have a kind of, I don't
want to say click bait.
They have very bold headlines, but the reporting is pretty good.
The reporting is good, it's accurate, and it's linked, which is nice.
I mean, that's something you don't find on Fox News or CNN.
When they make a claim about some study or some statistic, they link to it.
It's there.
You can click it and read it yourself.
I mean, that's better fact checking and better accountability than you find on major networks.
And again, these were all independent outlets and none of them are corporate run and they
all spoke out against the government and now they're all gone.
You can't find them on Facebook, you can't find them on Twitter.
So get ready to move because this is kind of going to be the end.
This is going to be the end of free discussion and free discourse on Facebook.
So anyway, just a thought and y'all have a nice day.
down and get some fresh air.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}