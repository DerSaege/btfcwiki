---
title: Let's talk about the Confederate flag and the New South....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FMIOCj7a3tI) |
| Published | 2018/10/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Confederate flag represents a test for people discussing Southern culture.
- Southern culture existed before and after the Confederacy, making the flag's symbolism questionable.
- The Confederate flag evokes immoral and evil times in Southern history.
- Beau questions why individuals choose to associate with a symbol tied to such negative connotations.
- The original Confederate flag differs from the commonly recognized flag, representing Robert E. Lee's army.
- Beau suggests flying the real Confederate flag if one truly seeks to honor heritage, not the battle flag.
- The Confederate flag's recent history links it to opposition to desegregation and racist acts.
- Beau challenges the notion that flying the Confederate flag is about heritage, citing its divisive history.
- He recounts an encounter with a former gang member who offered insights on the symbolism of flag-waving.
- Beau asserts that his Black neighbors embody Southern culture more authentically than flag-wavers.

### Quotes

- "It's drawing up images of the most immoral, evil, and worst times in Southern history."
- "I don't think it's about heritage. Not for a second."
- "That symbol doesn't represent the South; it represents races."
- "It's a new South. Don't go away mad. Just go away."

### Oneliner

Beau challenges the romanticized notion of the Confederate flag as heritage, asserting its racist connotations amidst a changing Southern identity.

### Audience

Southern residents

### On-the-ground actions from transcript

- Educate others on the true history and implications of the Confederate flag (suggested)
- Advocate for the removal of Confederate monuments and symbols from public spaces (implied)

### Whats missing in summary

The full transcript provides detailed insights on the problematic nature of the Confederate flag's symbolism and its ties to racism in Southern history.


## Transcript
Hey there, internet people, it's Beau again.
You know, almost as soon as somebody hears my accent talking about my views, somebody,
normally from the North, will be like, what do you think of the Confederate flag?
It's kind of like some kind of test, I guess, because, you know, there's that stereotype.
Okay. Well, I can understand wanting to display a tie to the South. I do. It's
wonderful down here. It is wonderful down here. It's a great place. It's great
culture. But the thing I don't understand is that Southern culture
existed long before the Confederacy and long afterward. I have no idea why people
people want to use that symbol as an icon to represent the South. It's
drawing up images of the most immoral, evil, and worst times in Southern history.
I don't understand how that's become a symbol. I don't understand how people can
think, you know, I want people to associate me with this, with all of this
evil, with all of this stuff that happened. I want people to associate me
with that, when there's so many other options.
To put it in a different perspective, it would be like saying, I want to display my American
heritage, so you walk around with smallpox infected blankets, handing them out to people.
It just, it doesn't make sense.
You know, the Confederacy, it was a failed nation that didn't last a generation over
a hundred years ago.
Nobody has a connection to that anymore.
And let's say you do, let's say for whatever reason, you actually have some connection
to the Confederacy.
Why don't you actually fly the Confederate flag?
And right now there's a bunch of people going, what?
Yeah, that red flag with the blue cross and the stars on it, that's not the Confederate
flag, that's the flag of the Army of Northern Virginia, it's a battle flag, it's Robert
E. Lee's flag, basically, it had nothing to do with the Confederacy.
original Confederate flag had a red stripe, a white stripe, a red stripe, a
blue field with stars in it. It looked a lot like the American flag. It had three
stripes though. Why not fly that? If you actually want heritage and some
connection to the Confederacy, why not fly that symbol? Nobody's gonna look at
that and be like, oh that guy's a racist. Doesn't have that connotation. People
don't look at that most of them don't even know what it is and if you're
actually concerned about heritage that'd be kind of cool test wouldn't it? I mean
you know there's a lot of people with rebel flags you know what they think is
the flag of the Confederacy who didn't know that it wasn't until this video
wouldn't it be cool to kind of weed them out unless that's not really the
heritage you're talking about and that's actually what I think it is for a lot of
people flying that flag.
See, it has another heritage.
It has another more recent use.
It was added to Georgia's state flag two years after
Brown v. Board of Education.
It was real clear what it meant.
You uppity folk better stay in your place.
It's what it meant.
The reason it is seen as a racist symbol is because its
most recent usage was by people who engaged in lynchings and assassinations
and church bombings and arsons. Of course it's seen as a racist symbol. It is. It's
that simple. I think that's probably the heritage that most people who fly are
are really reaching back to, the 1960s, opposition to desegregation.
Because otherwise, if it really had to do with the Confederacy, why aren't you flying
the Confederate flag, the real one?
Got a bunch of guys from Alabama and Mississippi
flying a flag from Virginia.
Nah, not buying it.
I don't think it's about heritage.
Not for a second.
I had a guy, and there's no nice way to say this,
he was a gang banger.
he was. We met through a very bizarre twist of fate. Nice guy. I mean, in my experience
with him, he was a nice guy, but he was a gangbanger. He told me that he gave, I wish
I could remember the exact word, something like, I give white boys a pass if they're
under 24, flying. I thought that was interesting. I asked him why. And he said that it was a
lot like black kids from the suburbs sporting gang colors. You know they're
looking for an identity they're looking for something to belong to and this is
this is how they think they can do it but if you know and that's okay as a kid
but if they're older and they haven't grown out of that they're probably
dangerous yeah yeah that's that's pretty good and that's been my experience that
has been my experience with it it is the thing is that symbol doesn't represent
the South it represents races it never represented the South ever it wasn't the
southern flag. It wasn't the Confederate flag. Yeah, I don't know, and there's somebody that's
going to say, well, you know, later, later on the Confederacy had a second flag and a third flag,
and you know, and it was in some of those. Yeah, it was a small part, a small part.
At no point in time was a red flag with a blue cross with those stars on it making up the entire
flag was at anything other than a battle flag for Robert E. Lee's army, and I think there
was an outfit out of Tennessee that used something similar.
It doesn't represent the South, it never did.
So it's not your heritage, at least not the one you're pretending it is.
And here's the thing about it, because we have a new South, gentlemen.
I live out in the country.
My nearest neighbors are black.
And I'll tell you this, they represent the South, Southern culture, Southern hospitality,
than anyone I have ever met that was waving one of those flags and I know
there's a lot of people that think that there's gonna be some resurgence and the
South will rise again you know in the 1920s that flag didn't have a racist
connotation back then was the symbol of lost causes I think that's probably more
fitting. You know, you guys tried recently, I know, racists from all over the country
went to Georgia to try to defend some Confederate monument and you got chased out by the people
in Georgia. In Georgia, they didn't want your racist rears.
As far as that ever symbolizing the South again, it won't.
We don't want you anymore.
It's a new South.
Don't go away mad.
Just go away.
Anyway, y'all have a nice night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}