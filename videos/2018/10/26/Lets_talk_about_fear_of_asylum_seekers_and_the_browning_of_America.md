---
title: Let's talk about fear of asylum seekers and the browning of America....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iU88FU4snpU) |
| Published | 2018/10/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses fears about asylum seekers, including concerns about ISIS, MS-13, diseases, and more.
- Questions the validity of these fears by pointing out the lack of evidence despite asylum seekers coming for 15 years.
- Suggests that politicians use fear to win elections by tapping into people's insecurities.
- Believes the real fear in society is different, illustrated through a personal story about interracial relationships.
- Expresses confusion over the fear of diversity and the "browning of America."
- Envisions a future where racial distinctions blur and politicians may have to lead differently.
- Contemplates whether America was meant to be a melting pot or a fruit salad with separate elements.

### Quotes

- "Light will become darker. Dark will become lighter. Eventually we're all going to look like Brazilians."
- "It's gonna be a lot harder to drum up support for wars that aren't really necessary because you're not killing some random person that doesn't look like you."
- "Maybe the best thing for everybody is to blur those racial lines a little bit."
- "Y'all have a nice night."

### Oneliner

Beau challenges fear-based narratives on asylum seekers and racial diversity, envisioning a future where racial distinctions blur for a more united society.

### Audience

Americans

### On-the-ground actions from transcript

- Embrace diversity in your community by fostering relationships with people from different backgrounds (exemplified).
- Challenge stereotypes and prejudices by engaging in open and honest dialogues with friends and family (implied).

### Whats missing in summary

A deeper exploration of how embracing diversity can lead to a more unified and understanding society, fostering empathy and collaboration among individuals.

### Tags

#AsylumSeekers #Fear #Diversity #Community #FutureSociety


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the fears of these asylum seekers, that group coming
up.
We're going to address some of those concerns, and then we're going to talk about what I
think the real fear is, because when you hear about it and you see it in the comments section
it's always the same thing.
There could be ISIS in that group, MS-13, they could be drug mules, they could have
diseases.
going to bankrupt us. We can't take that many people. It's going to overwork our
medical infrastructure. They could all be rapists and killers and murderers and
armed robbers. Why hadn't any of that happened yet? These giant groups of
asylum seekers have been coming up for 15 years. 15 years. You would think that if
that is a serious concern. I mean, there would be some kind of evidence of that
occurring so far. It's almost like those fears are unfounded. It's almost like
those fears are just parroting politician talking points, because that's
how politicians win elections now, isn't it? They get you kicking down. You're
better than those folks, vote for me. I'll make sure you stay better. That's what it
takes. It's that fear. Everybody's afraid of everything. So that's all a politician
needs to do is tap into that fear. That's how you win an election. You gather a group
of low information, middle income, middle Americans, and you tell them what to be afraid
of and how you're going to save them from it, and then you get the votes and you get
the power.
Then you can do whatever you want and they'll defend it.
That's what happens, but see, I don't think that's the real reason.
I don't think that's the real fear at all.
I think the real fear is something else, and it's so pervasive in our society that even
among friends, close friends, there's still that worry. I got a real tight
circle of friends, and I mean tight. You know, one of our kids is all of our kids
type of thing. You know, there's that joke. Friends help you move, real friends help
you move bodies. My friends would have a tarp if I needed one. One day we're all
sitting out in front of the house, standing around, drinking beer, looking
like King of the Hill.
Daughter walks up.
She's about 21 at the time.
She's like, I met a guy.
We all knew what she met.
It wasn't that she met a guy.
She met a guy she's serious about.
But we could all also tell she was holding something back.
We all started drinking a little bit heavier.
And all of a sudden, she blurts it out.
Well, he's not white, but he was in seventh group.
All of us went, Oh Lord, no.
And her worst fears were confirmed.
Of course, none of us were saying, Oh Lord, no,
because he wasn't white.
We were saying it because he was in Seventh Group.
If you're familiar with the community,
you probably already understand the reservation.
If you're not, let me clue you in.
Seventh Group is a very high-speed unit in the US Army.
They have a wild and crazy job,
therefore it attracts wild and crazy men.
And these guys don't come home and play bridge, you know?
They work hard, they party hard type of thing.
Not a place where you would expect
to find a lot of family men.
And that is the reputation of that unit.
If you're in seventh, don't take offense.
I understand it's a generalization
and that there are good family men in that unit.
However, you must admit that that reputation is well earned.
Name another outfit just had a sergeant get
top of like 90 pounds of coke in his bag.
I mean, come on, I love you guys, but it is what it is.
Even among that tight circle though, she felt like she had
to justify it.
Blew my mind when it happened.
Because standing there, you've got interracial marriages,
interracial kids, guys who married immigrants, pretty
diverse group as a whole but at that moment all white guys all white guys she
did she felt like she had to justify qualify he's not white but he was in
seventh he's part of your crazy club of stupid men doing stupid things I don't
know what that fears about I mean I do I understand it there's a lot of people
out there that look down on that. But I don't understand why. The skin tone
fundamentally change a person that much. I don't understand the fear of the
browning of America. Don't understand it because it doesn't make any sense
logically. And then I don't understand it because well it's inevitable. I mean I
don't know why you'd be afraid of something that is going to happen. This
isn't a it might happen in the future type of thing. It's gonna happen on a
a long enough timeline, it's going to happen everywhere.
Light will become darker.
Dark will become lighter.
Eventually we're all going to look like Brazilians.
I personally can't wait.
I mean, I'll be gone.
But can you imagine how much harder politicians are going to
have to work to keep us divided, to keep us kicking down?
It's gonna be a lot harder to say,
oh, you're better than those people
when they look just like you.
It's gonna be a lot harder to drum up support
for wars that aren't really necessary
because you're not killing some random person
that doesn't look like you.
In that situation, politicians might actually
had to start to lead. They might have to actually think of their constituents instead of themselves.
But that's a long way off. And maybe I'm wrong. You know, maybe America was not meant
to be a melting pot. Maybe it was meant to be a fruit salad. You know, a bunch of little
chunks, different colors, touching but not really mixing. Of course, you know when
somebody's eating a fruit salad, normally if you watch, they go through and they
pick the parts they want to devour first, maybe staying separated isn't the best
idea. Maybe the best thing for everybody is to blur those racial lines a little
bit. I don't know. It's just a thought. Y'all have a nice night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}