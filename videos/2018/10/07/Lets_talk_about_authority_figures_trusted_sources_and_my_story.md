---
title: Let's talk about authority figures, trusted sources, and my story....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fLSOMM1eCs4) |
| Published | 2018/10/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Viewers question his identity and credibility due to a contradiction between what he says and what they expect.
- People want to know his backstory to determine how much trust to place in his words.
- Beau rejects blind trust in authority figures and encourages fact-checking and critical thinking.
- He warns against blind obedience to authority, citing the dangers it poses.
- Beau mentions instances where people failed to fact-check, leading to disastrous consequences like the Iraq war.
- Referring to Milgram's experiments, Beau reveals how many individuals are willing to harm others under authority's influence.
- He stresses the importance of trusting oneself and thinking independently rather than relying solely on authority figures.
- Beau values dissenting opinions and encourages viewers to think for themselves.
- He believes that ideas should be judged based on their merit, not on the speaker's credentials.
- Beau concludes by advocating for fact-checking, trusting in personal judgment, and encouraging independent thinking.

### Quotes

- "That obedience to authority, that submission to authority. This person is an authority figure. They're a trusted source, so we're gonna believe what they say. It's not a good thing."
- "A lot of bad things have happened in history because people didn't do that."
- "Trust yourself. Trust yourself."
- "Ideas stand or fall on their own."
- "Don't trust your sources, trust your facts, and trust yourself."

### Oneliner

Beau warns against blind obedience to authority, advocates for critical thinking, and encourages viewers to trust themselves over external sources.

### Audience

Viewers

### On-the-ground actions from transcript

- Fact-check information before sharing or believing it (implied)
- Encourage critical thinking and independent judgment in your community (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the dangers of blind obedience to authority and the importance of critical thinking in evaluating information.

### Tags

#ObedienceToAuthority #CriticalThinking #FactChecking #IndependentJudgment #TrustYourself


## Transcript
Hey there internet people, it's Beau again.
Tonight, we're gonna talk about me, kinda.
Not really though, but a little bit.
After every video now, I get somebody going like,
dude, who are you?
They hear my voice and they hear what I say,
and there's that contradiction between what I say
and what they expect me to say,
and they wanna know the story.
Now, some people, they're just curious, and that's cool.
Other people want to kind of weigh how much trust
to put in what I say.
So they want to know my back story.
And yeah, there's a story.
Everybody's got one.
And some of the things that we've talked about,
I am an authority on.
As in, I get paid to talk about them.
But that's irrelevant.
doesn't matter.
Does not matter at all.
A lot of people are trying to figure out
whether or not to trust what I say.
Don't, I don't want you to.
I don't want you to.
That's a big problem.
That obedience to authority, that submission to authority.
This person is an authority figure.
They're a trusted source,
so we're gonna believe what they say.
It's not a good thing.
It's not a good thing.
You know, we've all got that one friend
or maybe a family member we can't politely delete
that shares
stuff that is obviously false
on Facebook.
What do you put in the comments section?
Sometimes, you know, some people put that hashtag, fake news.
Odds are though,
you probably say check your sources.
See, it's that subtle.
That obedience to authority,
that submission to authority, it is that subtle.
Check your sources.
You're not saying, you're not actually
saying the fact is wrong.
What you're saying is this authority isn't any good.
Go to a different one.
Trust them to give you your information.
Don't do that.
Check your facts.
And then once you check your facts,
run them through the filters.
You know, you've got the fact filter.
You've got the, is this piece of information true?
Then you've got the logic filter
that you kick in right behind it.
It is just because this fact is true
is what they're trying to portray true.
And then you run it through the moral and ethical filters.
Okay, so what they're trying to portray is true,
but the action, that subtle hint of an action
that they want me to take, is that moral and ethical?
A lot of bad things have happened in history because people didn't do that.
I mean, that's how we wound up in the Iraq war, you know.
We had a bunch of people with good resumes stand up there and say, here, this is true.
Check our sources.
Nobody checked the facts.
A lot of people died.
That obedience to authority, that submission to authority is deadly.
It can be deadly.
You know, there was a dude named Milgram back in the 60s, I think.
He conducted these experiments.
And what he found out, short version, was that about 60% of us, 6 out of 10 people watching
this video right now, would torture and electrocute somebody because a dude in a lab coat holding
a clipboard told him to.
conducted that experiment over and over again because the first time he did it
it's like there's no way this could be right because when they did the
predictions they thought it would be about 3% turned out to be 60 60 and they
did it over and over and over again and the average in the US I think was 61%
in the US and 66% outside of it. That's terrifying now he was trying to figure
out how the Holocaust happened. That submission to authority. That looking for an authority
figure. That trying to figure out who to trust. Trust yourself. Trust yourself. You know that's
one of the coolest things about the comments section of me is every once in a while I run
across one of those comments that says, I love every video you've done except this one.
Good. Good. I like you. You know why? Because if I'm sitting in a chair, you're not going
to shock me. You ran it through a filter, you don't like it. Fine, that's cool. I
don't want you to think like me. I want you to think for yourself. And in that
regard, my story, and yeah, there have been comments that I've made that have
probably given some of it away, but my story isn't that important. You know, my
resume is not that important. It doesn't matter. It does not matter because ideas
stand or fall on their own. If Einstein walked out and said hey the Sun is a
giant flashlight doesn't mean the Sun is a giant flashlight. Don't trust your
sources, trust your facts, and trust yourself. Just something to
about, you know, just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}