---
title: Let's talk about context....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iqNDL5FiLsg) |
| Published | 2018/10/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the importance of sound bites and context, using Kennedy's speech as an example.
- Kennedy's speech, often quoted for specific sound bites, is misunderstood without the full context.
- The speech was about the Soviet Union, not a vague conspiracy theory.
- The main aim of the speech was to encourage self-censorship among journalists, editors, and publishers.
- Beau criticizes the modern reliance on sound bites due to shortened attention spans from 24-hour news and social media.
- He points out the dangers of Twitter as a platform for substanceless communication.
- Beau questions the reasons behind U.S. involvement in Syria, attributing it to the U.S. starting the Civil War for ulterior motives.
- The motive may have been related to competing pipelines supported by different countries.
- Propaganda plays a significant role in shaping public perceptions and is on the rise.
- Beau urges viewers to look beyond sound bites and understand the interconnected nature of global events.

### Quotes

- "Context, it's really important."
- "If you want to understand what's going on in the world, you have to look beyond the sound bites."
- "Propaganda is coming into a golden age right now."
- "Most times the sound bite, just like in Kennedy's speech, it's the exact opposite of what's really being said and really being done."
- "Taking a whole bunch of information and getting down to what's actually important."

### Oneliner

Beau stresses the importance of context in understanding global events beyond misleading sound bites, cautioning against the rise of propaganda.

### Audience

Media Consumers, Critical Thinkers

### On-the-ground actions from transcript

- Question mainstream narratives (suggested)
- Research beyond headlines (exemplified)
- Analyze global events critically (implied)

### Whats missing in summary

The full transcript provides detailed insights on the manipulation of information through sound bites and the necessity of looking beyond them to grasp the true nature of global events.

### Tags

#Context #SoundBites #Propaganda #CriticalThinking #GlobalEvents


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about sound bites and context.
So I was actually planning on doing this video at some point in the future, but somebody
brought the speech up in the comments section the other day, so I figured I'd go ahead
and do it now.
Speech I was going to use to illustrate this is one of Kennedy's, he set it in, I want
1961, but it's pointed to as the reason he got killed. Now in this speech he says that
we are opposed around the world by a ruthless and monolithic conspiracy that relies primarily on
covert means. Man, who is he talking about? That's crazy. Who's coming after us? And in that same
speech another good sound bite is the very word secrecy is repugnant in a free
and open society man that's great that's good I like that so here's the thing
though those are the sound bites and that's why people point to it and they
They say, see, he was going to go after, pick your bogeyman.
Pick your conspiracy theory bogeyman, whatever it is.
The Illuminati, the Bilderbergs, whatever.
That's why they killed him, because he
was going to go after their ruthless and monolithic
conspiracy.
Of course, if you read the whole speech, it's really clear.
Context, it's important.
The conspiracy, well, that's the Soviet Union.
And that entire speech that included the phrase
that secrecy is repugnant in a free and open society,
the entire speech, the entire point of the speech
was to get journalists, editors, and publishers
to censor themselves.
In the same speech, he says, you should ask two questions.
Is it news?
And is it in the interest of national security?
See, the sound bite, what gets quoted, what gets used,
is the exact opposite of what the speech was really about
and what he really wanted to achieve with it.
Context, it's really important.
And today, the world runs on sound bites.
That attention span has gotten so short
because of 24-hour news and social media.
People tell me all the time I need to shorten my videos to
less than three minutes.
Can't do it, I'm too long-winded.
Twitter.
Twitter is nothing but visual sound bites.
And the problem with it is you don't have to have any
substance.
You don't have to say anything that matters.
You just have to be witty.
You don't have to defend what you say.
You just make your little tweet, and then your
supporters will defend it tell me it's not true with every politician, you know, even the ones you like
And that's dangerous that is very very dangerous
And I prove it why are we in Syria why did we get involved in Syria?
Well because the Civil War broke out
Why?
We started it
The U.S. started it. The U.S. and the United Kingdom got together and
they pretty much started that war. They started that civil war and they used it as justification as a pretext for
intervention
because they wanted to oust Assad.
Been ongoing. The plan had been in motion since I want to say 2006.
There's a lot of theories on why I leaned towards a pipeline theory.
There were two competing pipelines, one backed by our good friends and allies, the Saudis,
the journalist killers, and the others backed by Iran.
And they wanted the Saudi pipeline to go through.
They weren't certain that Assad was going to play ball, so they wanted to get rid of
him.
That's the context.
Everything else, it's a show, it's theatrics.
That's really what went down.
The fact that the U.S. and the U.K. pumped money into Syria to fund the propaganda to
start the civil war very well documented and was done through sound bites, you can't listen
to them.
You've got to look at the bigger picture.
You have to look at the bigger picture.
If you want to understand what's going on in the world, you have to look beyond the
sound bites.
are interconnected today, not just through communication, I mean that's instant
worldwide, but business. All those interests are linked. So if you want to
understand what's going on, never read a press release, never listen to a sound
by. You have to look at that bigger picture at all times. Propaganda is
coming into a golden age right now. It's infectious. You have entire news
networks that are devoted to supporting a single political party and everybody's
going to point to one or the other because there's a couple of them out
there. But you know they bring in the opposition. We're balanced. We're gonna
let this person talk from the other side. But they intentionally find the dumbest
person they can find. It's propaganda and you're inundated with it all day every
day. And most times the sound bite, just like in Kennedy's speech, it's the exact
opposite of what's really being said and really being done. Anyway, it's just a
just an observation. Most of my adult life has been spent in one way or
another processing information. Taking it and distilling it. Taking a whole bunch
of information and getting down to what's actually important. That's what I
still do with these videos, I guess. Anyway, it's just a thought. Y'all have a
Have a nice night, enjoy your weekend.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}