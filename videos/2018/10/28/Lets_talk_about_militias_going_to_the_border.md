---
title: Let's talk about militias going to the border....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wN-XFH5giKQ) |
| Published | 2018/10/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau Ginn says:

- Beau is on the Mexican border with his militia commander, tasked with protecting America.
- They won't stop refugees on the Mexican side as it violates U.S., Mexican, and international law.
- Using guns to stop refugees could result in committing a felony and losing gun rights.
- Sending back individuals at gunpoint from the American side is also against the law and goes against their core principles.
- Beau acknowledges that individuals entering the U.S. have rights, including due process.
- They are essentially backing up the Department of Homeland Security (DHS) despite being labeled potential terrorists or extremists in a DHS pamphlet.
- Beau expresses concern that their actions could have negative consequences if they follow through with their rhetoric.
- Beau questions the wisdom of their militia commanders and suggests they might be strategizing with "4D chess" to manage optics.
- Despite portraying themselves as protecting America, Beau acknowledges the grim reality of potentially harming unarmed asylum seekers following the law.
- Beau ends with a casual greeting to a Colonel, leaving the situation open-ended.

### Quotes

- "We're down here on the border protecting America with our guns, ready to kill a bunch of unarmed asylum seekers who are following the law."
- "Hey, Colonel!"

### Oneliner

Beau Ginn on the Mexican border with militia, raising concerns about the legality and ethical implications of their actions while potentially compromising their group's secrecy and intentions.

### Audience

Militia members, border patrol supporters

### On-the-ground actions from transcript

- Question the ethics and legality of their actions (implied)
- Reassess the group's strategies and potential consequences (implied)

### Whats missing in summary

The urgency of addressing the potential harm caused by militia groups operating at the border.

### Tags

#BorderSecurity #Militia #Ethics #LegalIssues #Immigration #HumanRights


## Transcript
Well, howdy there, Internet people, it's Bo Ginn.
Down here tonight on the Mexican border,
because my militia commander called me up,
told me to come on out, because we had to protect America.
So, yeah, I'm down here to do that.
Of course, we're not actually going to stop any of the refugees
on the Mexican side of the border,
because that would be a violation of U.S. law,
Mexican law, and international law.
And, you know, we're all really attached to our guns,
so we're not gonna do anything to make us commit a felony
and lose our gun rights.
And then we're not gonna do anything on the American side
of the border, either,
because that's also against the law
to send them back at gunpoint.
And more importantly, that'd be a violation
of our, like, core principles, you know,
upholding the Constitution and all of that.
And when people enter the United States,
they're, you know, have rights,
and one of those is due process.
So we wouldn't do that unless we were traitors, so we're not going to do that.
So I guess we're just back down here backing up DHS, you know, the government agency that
put out that pamphlet saying that pretty much every militia member was a potential terrorist
or extremist, and we're down here hanging out with them, showing them how our militia
works. Letting them take pictures of us. Man, that's going to go real bad if we ever actually
follow through with all that Molongwabe rhetoric, isn't it? They're going to know exactly who
we are and how we operate. I mean, our militia commanders aren't stupid or anything. I mean,
I'm sure they thought of this.
They're probably playing that 4D chess, you know.
They're trying to, you know,
make sure that the optics are good, we look good, you know,
because we're down here on the border
protecting America with our guns,
ready to kill a bunch of unarmed asylum seekers
who are following the law.
Hey, Colonel!

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}