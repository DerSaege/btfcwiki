---
title: 'Let''s talk about gun control (Part 5: Bumpstocks)....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HqVMiL7Qi0Y) |
| Published | 2018/10/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how bump stocks work to mimic fully automatic fire.
- Mentions the inaccuracy of fully automatic fire.
- Recounts the infamous use of bump stocks in the Vegas mass shooting.
- Points out that bump stocks may increase a shooter's lethality in enclosed spaces like schools.
- Disputes the notion that bump stocks save lives during mass shootings.
- Analyzes the necessity of bump stocks under the Second Amendment's intent.
- Describes the different perspectives among gun enthusiasts and Second Amendment supporters regarding bump stocks.
- Talks about the concept of suppressive fire and its relevance to the Second Amendment.
- States his opinion that bump stocks are not necessary in the context of insurgency against the government.
- Expresses reservations about banning bump stocks but acknowledges potential concerns.

### Quotes

- "It's not going to save any lives."
- "A lot of guys believe that to exercise the intent of the Second Amendment, which is to fight back against the government if need be, you'd need to be able to lay down suppressive fire."
- "No man, nothing should be banned."
- "You're not gonna see me at a protest trying to make sure that a bump stock doesn't get banned."
- "Nine of them are gonna be complete people you probably don't want to hang around."

### Oneliner

Beau explains bump stocks, disputes their life-saving potential, and examines their necessity under the Second Amendment, ultimately questioning the need for a ban.

### Audience

Firearm enthusiasts, Second Amendment supporters

### On-the-ground actions from transcript

- Question the necessity of bump stocks in relation to the Second Amendment (implied).
- Engage in respectful and informed debates about firearm regulations (implied).

### Whats missing in summary

In-depth exploration of the implications of bump stocks on mass shootings and insurgency tactics.

### Tags

#GunControl #SecondAmendment #BumpStocks #MassShootings #FirearmRegulations


## Transcript
Howdy there, internet people, it's Beau again.
So I guess we are on to part five of the gun conversation.
Had some questions about bump stocks,
three of them actually, we're gonna answer them.
Now the first one was, do bump stocks really save lives
during a mass shooting?
Well, talk about that.
The next one was, under the intent of the Second Amendment,
as I see it, are bump stocks necessary?
And then the third one was, should they be banned?
OK.
It took me a minute to figure out
where the idea that a bump stock would save lives
during a mass shooting came from.
What this person did is they used a very common gun control
tactic to espouse anti-gun control beliefs.
He's cherry picking data.
He's using one incident.
making that seem like it would be representative of mass shootings. So to
understand this, how does a bump stock work? It takes a semi-automatic rifle and makes
it mimic fully automatic fire. Makes it seem like it's a machine gun when it's
not. It doesn't actually change any of the mechanics of the weapon. It makes it
to where every time the trigger resets, it's getting pulled immediately, split
second later so it achieves rates of fire very comparable to a fully
automatic weapon with a military weapon okay so fully automatic fire is very
inaccurate you know if you let loose with a magazine you know full magazine
you dump 30 rounds downrange most of them are going to hit with your aiming
that. That's not how it works. Three to five rounds is about all you can shoot at a time
and still stay on target. Now I know somebody in the comments section is going to say, you
know, I can dump a magazine into a three by five card at 500 yards. Sure you can, but
for us mere mortals, doesn't happen that often. Okay, so it is inaccurate. The longer you
shoot the less accurate the fire. I understand a lot of you guys are not
shooters that are watching this, so what I want you to do is take your finger and
point it at a picture frame in the room, whatever. Get it good and center on that
picture frame and then just tilt it, tilt the tip of your finger half an inch up.
Now if you're across the room from this picture frame, if you were to follow the
line from your finger, you're no longer going to hit that picture. So when that
weapon bounces around because of the recoil the bullets are spreading out and
the further away the target the less accurate the fire because the further
that bullet travels on that trajectory the further off it's going to be. A half
inch here can be inches or feet further down. So what is the most famous use of a
bump stock in a mass shooting? Now to be honest it's actually the only one I know
know of. Vegas, right? The guy was letting loose with an entire magazine at a
time and firing a couple hundred yards. Yeah, in that instance it probably saved
lives. The fact that he used one made his fire so inaccurate. You have a whole
bunch of injured and I don't want to say I don't want to say not a lot of dead it
wasn't as bad as it could have been how about that if he had used semi-automatic
fire there would have been a lot more dead because of the range and because of
how he was firing that is true of that instance and that instance alone
Well, most mass shootings don't occur at a couple hundred yards, they occur at a couple
feet.
And being able to fire rounds that quickly in an enclosed space like most of them are
at a school or something like that, yeah, it's going to make the shooter a whole lot
more lethal.
It's not going to save any lives.
So that's, it's cherry pick data and it's bunk.
Whoever told you that probably knows they're misleading you.
Now, he may say, that's the only mass shooting that's
ever used one, so 100% of the time, it has saved lives.
And that's true, but it's still kind of misleading in the idea
that under normal circumstances for what we consider a mass
shooting, that it would help.
It won't.
OK, so the next question, is it necessary under the intent
the Second Amendment.
Okay, so you got the two groups. You got gun nuts and then you have Second Amendment supporters.
Gun nuts shall not be infringed. Love them. Bump stocks are the greatest thing ever.
You have to have one. It's our rights, it's our freedom. Okay.
Second Amendment supporters, they're divided.
They're divided and it's pretty clear. A lot of guys believe that
to exercise the intent of the Second Amendment, which is to fight back
against the government,
if need be, you would need to be able to lay down suppressive fire and what that means is putting a
whole bunch of bullets in one area to keep another guy's head down. So of course, since fully automatics
they're not actually banned, but they're effectively banned for all intents and purposes, you know,
most people can't get them. They're cost prohibitive. Anyway, this is kind of like the
poor man's full auto. It's kind of the way they're looking at it. Those who
don't think it's necessary understand that in an insurgency, which is how you
would fight back against the US government, you can't go toe-to-toe with
them, you need to be hit, run, and hide. Suppressive fires to keep
somebody's head down while your movement, while the rest of your team
moving. That's a sign of a running gun battle, a long gun battle. That means that
very shortly you're gonna get hit with air support. So it's not, to me, I don't
see it as being necessary. Also understand that in the event of an
insurgency the the $600 rifle you bought at Walmart will eventually capture a
military rifle so I don't see it as necessary under that intent so should
they be banned okay now ideologically no man nothing should be banned we're all
gonna sit around a campfire and love each other and everything's gonna be
fine. That's actually the society I want. In today's society, you're not going to see
me at a protest trying to make sure that a bump stock doesn't get banned. The only strong
argument I can come up with against banning bump stocks is back when I used to go to the
range all the time. I like to know who the dummies were, the people you don't want to
around. That was normally a pretty good indicator if they had one of those. Now
that's not foolproof, you know. There are decent guys that have them.
Most times they buy them as a joke, as a novelty, but I think most shooters would
tell you that if you took 10 bump stock owners and lined them up against the
wall, nine of them are gonna be complete people you probably don't want to hang
around so there you go so I hope that answers the questions and we will get to the other ones I
know there's a lot of them and a lot of them are really good questions we just haven't had time to
get to them yet because of the other stuff that's been going on so just something to think about y'all
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}