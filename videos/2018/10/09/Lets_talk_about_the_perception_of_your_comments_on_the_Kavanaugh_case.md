---
title: Let's talk about the perception of your comments on the Kavanaugh case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pfsgb7CySdQ) |
| Published | 2018/10/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a message from a young lady who is unable to face her father due to his comments, appreciating Beau's perspective.
- Urges men to reconsider their harmful comments and salvage relationships, rather than focusing on divisive topics like the Ford Kavanaugh case.
- Points out that one in five women have experienced sexual assault, and criticizes the common arguments used to doubt victims.
- Addresses the question of why victims wait to come forward, attributing it to fear of not being believed or facing consequences.
- Challenges the notion of victim blaming based on behavior, stating that regardless of circumstances, rape is never justified.
- Calls out the damaging impact of justifying inappropriate behavior, leading to underreporting of sexual assaults.
- Encourages men to have honest conversations with the women in their lives and apologize for harmful comments that perpetuate a culture of disbelief.

### Quotes

- "You don't get to rape her."
- "You've given them all the reason in the world to never come forward."
- "I got caught up in politics. I didn't care."

### Oneliner

Beau urges men to reconsider harmful comments, salvage relationships, and apologize for perpetuating a culture of disbelief towards sexual assault victims.

### Audience

Men and fathers

### On-the-ground actions from transcript

- Have honest and open conversations with the women in your life to understand the impact of harmful comments (suggested)
- Apologize for perpetuating a culture of disbelief and victim blaming (suggested)

### Whats missing in summary

The emotional impact and urgency conveyed by Beau's message can best be experienced by watching the full transcript.

### Tags

#GenderEquality #SexualAssault #Apology #Relationships #Conversations


## Transcript
Howdy there, internet people.
It's Beau again.
I'm gonna go ahead and warn you now,
this video's gonna have a little bit of a different tone
than most of my other videos.
To be honest, I got a private message and it tore me up.
Young lady said that she can't stand
to look her father in the eye anymore
because some of the comments that he's made
and that it was nice to hear a man say the things
she wished her father would say.
Somewhere out there,
there's a man who has unknowingly alienated his daughter.
So, he can talk with the boys
and be cool and make pizza cutter comments
about this Ford Cavanaugh case. Pizza cutter, all edge, no point.
It's no longer about this case.
It's about trying to salvage the relationships if they can be.
One out of five, gentlemen, one out of five women
that heard you talk have been sexually assaulted.
And that's the lowest ratio.
One out of five.
Their studies have actually put it much higher than that.
Let's run through your arguments real quick.
And we're going to talk about the perception
of what you said, what your daughter, her, or your sister,
Or maybe your son.
Why'd she wait so long to come forward?
Do you think that this is the first generation of women that had men like you in their life?
Think it's the first generation of women that hurt a bunch of men, excuse an attack,
or doubt the victim?
You.
You, you're the reason she waited.
And you're the reason that your daughter or sister will wait.
Because she won't be believed.
Or maybe it's even more simple than that.
Maybe it's just that she was 15 and had a party and didn't want to get in trouble.
She had a beer.
So she kept her mouth shut to avoid getting in trouble.
And then by the time she wouldn't get in trouble, it didn't really matter anymore.
Maybe that's why she waited.
And then, see, that leads right to the next argument.
She's 15 and drinking at a party.
What does that say about her?
It says she's exactly like every other teenage girl out there.
That's pretty common.
I'm sure your little angel did it too.
I know, not your daughter, right?
What's the perception of that?
She's drinking raper.
If you're going to ask that, why was she there doing that?
That's really what you're saying.
If you're trying to use that as an excuse, it's also a reason, isn't it?
You can get away with it that way.
I don't know if we believe her.
So if your daughter's out drinking, niece, whoever, she
gets attacked, she going to tell you?
No, she won't.
She won't come forward, maybe till years later.
Why?
Because you already justified it.
You already justified it.
What's your son here?
What is your son here?
Well, if she's drunk, she's kind of got it coming.
The perception of what you're saying, your arguments?
No.
And to be real clear on this, it doesn't matter if she's
got a heroin needle hanging out of her arm, works nights at
the strip club, and just robbed a liquor store.
You don't get to rape her.
I mean, this is a pretty simple concept.
It's not that we don't believe all sexual assault victims.
She's just not credible.
Not credible.
OK.
So you're pretty much the reason why
every young enlisted female that gets
raped in the military by some senior NCO
doesn't come forward. Congratulations.
I think a lot of men probably need to sit down and talk to the women in their lives
and try to explain some of their comments and some of their justifications for it.
I think that would probably be a real good idea.
Because you've given them all the reason in the world to never come forward
and you've given your sons a giant list of ways to get away with sexually assaulting people.
Because you know they're not going to be believed.
Well, if you're more credible than her, if you've got a better reputation, go to town.
Oh, she's been drinking too?
Even better.
You need to have that talk.
And my advice is rather than try to justify what you've said, just apologize.
apologize. Just say it. I got caught up in politics. I didn't care. I cared that
much about putting some police state goon on the Supreme Court that I
damaged our relationship.
Congratulations.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}