---
title: Let's talk about Trump , Obama , and monsters...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PJNF3tfo0RE) |
| Published | 2018/10/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President of the United States mocked sexual assault survivors on television, setting a shockingly low bar.
- The President's actions contribute to a concerted effort in the country to silence sexual assault claims.
- Referencing Trump's infamous "Grab-Em-By-The-P****" quote, Beau questions what it takes to stop such behavior.
- Beau contrasts Trump with Obama, acknowledging policy disagreements but noting the vast difference in character.
- He criticizes Trump for casting doubt on women coming forward with claims and talks about the presumption of innocence.
- Beau warns against dehumanizing perpetrators, stressing that they are people, not monsters.
- He draws a parallel between excusing Nazis as monsters and how society views rapists and assault predators.
- Beau underscores the danger of overlooking evil in plain sight due to political allegiance and nationalism.

### Quotes

- "There's a concerted effort in this country to silence anyone willing to come forth with a sexual assault claim, and it's being led by the President of the United States."
- "Honor, integrity, are those words that come to mind when you think of President Trump?"
- "The scariest Nazi wasn't a monster. He was your neighbor."
- "Scariest rapist or rape-apologist, well they're your neighbor too."
- "y'all have a good night."

### Oneliner

President Trump's actions mock sexual assault survivors and contribute to a dangerous culture of silencing victims, while Beau contrasts his character with Obama and warns against dehumanizing perpetrators.

### Audience

Activists, Advocates, Voters

### On-the-ground actions from transcript

- Challenge efforts to silence sexual assault survivors (implied)
- Advocate for the presumption of innocence and fair treatment for survivors (implied)
- Refrain from dehumanizing perpetrators and understand the importance of accountability (implied)

### Whats missing in summary

The emotional intensity and depth of analysis present in the full transcript.

### Tags

#SexualAssault #Presidency #Dehumanization #Accountability #SocialJustice


## Transcript
Well, howdy there, internet people, it's Bo again.
I just watched the President of the United States
mock sexual assault survivors on television.
Should have been shocking, but it wasn't.
I actually turned to my wife and said,
that wasn't as bad as I thought it was gonna be.
He set the bar that low.
He has set the bar that low.
I know somebody's going to say, no, he wasn't mocking sexual assault survivors.
He was just mocking one.
No, no, don't watch the clip on Fox News.
Go ahead and watch the whole thing and you'll see that he goes on to talk about how your
son may lose his job at IBM because some woman he never met makes up a claim about him.
There's a concerted effort in this country to silence anyone willing to come forth with
a sexual assault claim, and it's being led by the President of the United States.
I can't imagine why Mr. Grab-Em-By-The-Birds-Of-A-Feather type of thing, I guess.
You know, when that quote came up, everybody pointed to one word and used that to exonerate
him. Let. They let you do it. Let. So if a woman's almost passed out drunk, it's okay.
She lets you do it. Let. What would it take to not let somebody like him do that? I mean,
It's already done. What do you gotta do? Pepper spray him? Hit him in the throat? What is it?
How do you not let that happen? See, gentlemen, if you're gonna grab a woman like that, you
better be certain she wants you to do it. Let. Anyway, I get it, guys. I get it. After
After eight years of Obama, you wanted a man's man.
It's not really what you got though, is it?
Not even close.
You got some entitled, spoiled, temper tantrum-throwing, trust-fund baby billionaire who can't take
responsibility for his own actions.
That's what you got.
Honor, integrity, are those words that come to mind when you think of President Trump?
You know, I didn't like Obama's policies.
I didn't.
at all, but you know, if somebody came to me today and was like, Bo, you got to take
President Obama to church.
I wouldn't be worried.
I wouldn't be worried that he was going to say something embarrassing, make fun of
a handicapped person, groped the preacher's wife, wouldn't even cross my mind.
Can't say the same thing about President Trump.
So after he casts doubt on any woman willing to come forth with a claim, he goes on to
talk about the presumption of innocence, innocent until proven guilty and all that.
The man who led the charge and took out a full page ad demanding the execution of people
later turned out to be innocent and DNA evidence proved it.
That was in an actual court of law, not a job interview.
And that's what this is guys, it is a job interview.
You take all the information available and you take the best candidate.
Somebody doesn't get appointed to the highest court in the land simply because they are
not a sexual assault perpetrator. And then from there he goes on to talk about
what a wonderful guy Kavanaugh is. Maybe he is. Maybe he's a great guy. Wonderful.
Maybe he's a good father. I don't know. Never met the man. But see that speaks to
something that's really dangerous. It's a really dangerous thought. See when we
talk about rapists, sexual assault predators. They're monsters. Subhuman.
Look in the comment section. That's what they're called. That's dangerous. That is
dangerous because they're not monsters, guys. They're people. They are just people.
I've talked about this in other videos but it bears repeating. People said the
same thing about Nazis. They were monsters. They weren't. They're just people. Just
people. The scariest Nazi was not Hitler or Eichmann. The scariest Nazi was the good
German. The one that just stood there, didn't say anything. The one that didn't speak
up. The one that was in the stands, cheering a little bit, maybe to blend in. So blinded
By party politics and nationalism, they couldn't see evil on evil's face.
That's the one that really let it happen.
That's the scariest Nazi.
The scariest Nazi wasn't a monster.
He was your neighbor.
Scariest rapist or rape-apologist, well they're your neighbor too.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}