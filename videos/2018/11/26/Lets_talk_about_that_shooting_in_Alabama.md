---
title: Let's talk about that shooting in Alabama....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=db6dEBKhovg) |
| Published | 2018/11/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Police initially claimed they killed the shooter in Alabama, but later admitted the man did not shoot anyone.
- The man, a legally armed black individual, ran away from the scene of a shooting in a mall.
- Beau questions the rationality of shooting someone who fled from gunfire in a mall.
- He points out the tendency for people to defend police actions in such situations, even when the victim did nothing wrong.
- Beau delves into the man's military service history and addresses misconceptions surrounding his honorable discharge.
- He criticizes the media's portrayal of the man based on a single photo to incite negative opinions.
- Beau brings attention to the racial implications of being armed and black in America, referencing past incidents where armed black individuals were unjustly killed by police.
- He challenges biases and urges for a critical examination of the situation, asserting that being armed and black is not a crime.
- Beau addresses law enforcement directly, acknowledging the need for positive threat identification and a thorough assessment before resorting to lethal force.
- He calls for the acknowledgment and dissolution of biases to prevent further injustices and manipulation by those in power.

### Quotes

- "Being armed and black isn't a crime."
- "Nothing suggests this man did anything wrong."
- "Biases exist. If you don't acknowledge them, they will persist."
- "Time, distance, and cover. If he starts to point his weapon at the crowd, that's what we might call an indicator."
- "The establishment, the powers that be, will always be able to play us against each other."

### Oneliner

Police mistakenly shot a fleeing, unarmed man in Alabama, sparking racial bias debates and the need for unbiased threat assessment by law enforcement.

### Audience

Law Enforcement, Activists, Community Members

### On-the-ground actions from transcript

- Challenge biases within yourself and others (implied)
- Advocate for unbiased threat assessment training for law enforcement (implied)
- Support initiatives that address racial bias in policing (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of a shooting incident in Alabama, shedding light on racial biases, police actions, and the importance of acknowledging and overcoming personal biases to prevent injustice and manipulation.

### Tags

#PoliceShooting #RacialBias #LawEnforcement #CommunityPolicing #Justice


## Transcript
Well howdy there internet people, it's Bo again.
So today we're gonna talk about that shooting in Alabama
and there is a lot to talk about.
So we're just gonna kind of dive right into it.
Starts off, police say we killed the shooter.
And then the next day, okay, he didn't shoot anybody.
Well, okay.
I give the cops credit for doing that
When they shoot the wrong person,
they normally don't admit it, but they did in this case.
But even when they did, they went through
and they didn't really come clean.
What they said was, he may have been involved
in the altercation, and he was fleeing the scene.
When you say it like that, it sounds like
he did something wrong.
Let's take that out of PR speak real quick.
He might possibly, we don't know, have been involved in the altercation, which means he
could have been a victim.
We do know that he didn't shoot anybody and he ran away.
You mean to tell me this man ran away from somebody who opened fire in a mall?
I mean, of course you had to shoot him.
That's not completely rational behavior at all.
I know there's a bunch of internet tough guys that are going to say, well, if you're armed,
you're going to shoot back.
No, you're not.
Most of you aren't, 4%.
Most people are not going to engage if they can get away.
That's fact.
So far, there is nothing that has been released into the public narrative that suggests this
man did anything wrong, anything.
And it's part of a growing trend, and we're going to get to that.
But before we get to that, I want to talk about his military service, because that's
what's being used to pit everybody against each other.
I mean, well, against everybody except for the people who killed the wrong person.
See, his lawyer released a statement saying that he was honorably discharged.
family's lawyer said that he was honorably discharged. The Army said that
he didn't complete AIT. Well, okay. Because of that you have a bunch of
military scholars in the comments section of every article saying, see his
lawyer is already lying. If you don't complete AIT, and AIT is advanced
individual training, you get an ELS, not a discharge. ELS is entry-level
separation or enlistment level separation and that's true most times. You get an ELS
if you didn't serve 180 days. Anything after 180 days you get a discharge. Now
this guy was going to be a combat engineer 12 Bravo I want to say that's
14 weeks of training might be 16 either way it's not 180 days so if he was booted
out prior to completing AIT, he had to have gotten an enlistment-level separation, had
to have gotten an ELS, unless, of course, he got hurt and they didn't separate him because
they thought he was going to get better and it extended beyond 180 days, or he got recycled
and part of the training, or a hundred other things, or there's the option of it, well,
it's the Army.
Do you know any vet whose DD-214 is right the first time out the door?
I know a guy who never left Fort Benning that had a purple heart in his jacket.
I know that victory drive gets rough at night sometimes, but getting knocked out by a bouncer
doesn't really count as getting wounded by enemy action.
They argued with him when he tried to get it fixed.
They argued with him and it went on a while.
This guy's like, I never even deployed.
That's not what the records say.
It's the military.
It's the army.
It's a branch.
It's part of the government, it's a branch service, part of the government.
They're not always right with their paperwork.
So it is completely possible that this man never completed AIT and he got an honorable
discharge.
It is also completely possible that he got an ELS and his paperwork says that he was
honorably discharged.
So that's what his lawyer said.
Either one of those things is completely within the realm of possibility.
And now we've got to talk about that photo, because that's what the media is using to
inflame everybody.
And there he is.
He's a disgrace to the uniform.
Obviously not, quote, a real soldier.
Guarantee the people who are saying this, they've never seen the kill book.
You think real soldiers don't do things wrong?
What's more of a disgrace to the uniform?
Urinating on an enemy KIA and have your buddy taking a picture of it?
Or having your hat backwards in a bathroom?
This would not be the first time in history that a newly minted troop did something stupid.
It's part of being an E1.
I mean, yeah, he was out of regs, okay, in a bathroom.
This is not a huge mark against his character.
I mean, you have the right to do something stupid when you first join, everybody does.
So there's that, but before we go any further and start talking about the rest of this,
I want to point out that it doesn't matter if he was dishonorably discharged because
he punched a colonel in the face after driving drunk through the gate and crashing into an
MP's car.
didn't shoot anybody. Being armed and black is not a crime and right now I
know there's people going, Beau don't play the race car. Oh yeah we're gonna
play the race car because we just had another guy, good guy with a gun, stop a
shooting and then a cop showed up and killed him. Goes back further than that
too. We go to Philando Castile, another legally carrying gun owner. We go to
John Crawford didn't even have a gun had a BB gun in the store hadn't even bought
it yet. Cops killed him. Even if it had been a gun, it wouldn't have been illegal.
Ohio is an open-carry state. What do those people all have in common? I mean
other than the NRA not coming to help their families after this. What do they
all have in common. The NRA doesn't actually represent gun owners, not in
practice. They represent gun manufacturers. Now if you look like me
and something like this happens to you, they might say something, probably not,
because they're really more interested in keeping that Mola and Labe crowd
happy, you know, the same people that have the thin blue line sticker on their car.
They really want to keep them happy and keep them paying dues so they're not
going to do anything to upset the police.
Being armed in black isn't a crime. It's not. There is nothing that has been said
that suggests this man did anything wrong. And let me just go ahead and throw
some other stuff out there. Let's say, for example, he got into a fight with
somebody and that person pulled out a gun and started shooting and hit two
innocent people. Guess who still doesn't deserve to be shot? Let's say he pulled
out his gun. Still doesn't deserve to be shot. Fleeing the scene. What that tells
me is that he didn't point his weapon at the cops because they would have said
that he was running away. So let's really just run through what we know. A
legally carrying gun owner shot no one and ran away from somebody who was
That's what we know. That's what's entered the public narrative. Those are the facts we have.
If you have already decided that this guy did something worthy of his death, your biases are coming into play.
You can't tell me, oh, well, I trust the cops, the same guys who thought they killed the right person.
You think they got a good grasp on the situation, huh? Your biases are coming into play. It's what it is.
tone and the fact that he had his hat backwards.
Biases exist.
If you don't acknowledge them, they will persist.
You have got to really step back from this situation and look at it.
Nothing suggests this man did anything wrong.
If you're a cop and you're watching this, I imagine by this point you're probably pretty
mad and that's fine.
can be mad. I'm gonna throw you a bone though because there's obviously a
little bit of confusion. If you roll up on a scene and don't really know what's
going on, positive threat identification is really important. You use time,
distance, and cover to make that determination. That's how you figure
out if he's a good guy or a bad guy. Time, distance, and cover. If he starts to point
his weapon at the crowd, that's what we might call an indicator. Okay? If he's just running away,
he's just running away. Being armed and black is not a crime. And when you make that determination,
by the way, you want to use that and determine whether or not he's a good guy or a bad guy
before you shoot him since there is some confusion about that part. We as a
country have to start acknowledging our biases and when anytime a situation like
this comes up we see them in play. You should see them. If you're looking for
them you'll see them. If we don't start acknowledging these biases and letting
them fade away because we recognize they exist, the establishment, the powers that be, will
always be able to play us against each other.
Anyway, it's just a thought y'all have a good night

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}