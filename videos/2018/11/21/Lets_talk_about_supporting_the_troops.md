---
title: Let's talk about supporting the troops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cljonq0TBUA) |
| Published | 2018/11/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Dennis heard about a group in need north of Panama City without supplies after a hurricane.
- Terry contacted Beau to provide aid and they loaded up supplies from Hope Project.
- Beau encountered a lady filling in for David at the Hope Project and explained the situation.
- They discovered the community center in Fountain, Florida running low on food but with baby formula.
- Beau and others redistributed supplies between the locations to ensure both had what they needed.
- An 89-year-old man helped load supplies, showcasing incredible generosity and strength.
- All involved in the aid effort were veterans, showing their commitment to helping others in need.
- Beau points out the lack of support for veterans facing issues like delayed GI bill payments and homelessness.
- He criticizes the lack of attention to veteran suicides and the use of troops for political purposes.
- Beau challenges the notion of using veterans as an excuse to neglect other marginalized groups.
- He urges genuine care and involvement to truly support veterans and prevent unnecessary conflict.
- Beau advocates for prioritizing truth and understanding in supporting veterans and avoiding unnecessary wars.

### Quotes

- "If you really want to help veterans, stop turning them into combat veterans because some politician waved a flag and sold you a pack of lies that you didn't check out."
- "Before you can support the troops, you've got to support the truth."
- "If the only time you bring up helping veterans is as an excuse not to help somebody else, I want you to think back to that story."

### Oneliner

Beau showcases a community effort led by veterans to provide aid, while also challenging the true support needed for veterans beyond mere lip service.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Assist local aid efforts (exemplified)
- Support veterans facing issues like delayed GI bill payments and homelessness (exemplified)
- Advocate for truth and understanding in supporting veterans and preventing unnecessary conflicts (exemplified)

### Whats missing in summary

The importance of genuine care and involvement in supporting veterans and challenging the misuse of their service for political gains.

### Tags

#CommunityAid #VeteransSupport #TruthInSupport #PoliticalAccountability #AidEfforts


## Transcript
Well howdy there internet people, it's Bo again.
So today, I'm going to tell you a story.
And it's kind of a winding story,
and it's not going to make any sense in the beginning.
But stick with me, because there is a point.
So this guy named Dennis, he heard about a group of people
who got hit pretty bad.
They're north of Panama City, a little pocket of people
that don't have supplies, still from the hurricane.
They're still in a bad way.
So he tells this guy named Terry.
And this guy named Terry calls me.
So load up in the truck, head down there to Hope Project,
pick up supplies.
And David's not there that day, and there's
a lady filling in for him.
And any time I show up and David's not there,
I feel like I need to explain what I'm doing to whoever
is there, because this is a place where people come pick up
supplies for their family.
And then there's me, who shows up with a truck
and takes a ton and a half of supplies to go distribute.
So I felt like I needed to explain
why I'm taking a good portion of their inventory.
So anyway, I talked to her for a minute and load up.
They don't have any baby formula,
but they have everything else we need.
So load up the truck.
And as we're heading over there, we
swing by this community center in Fountain, Florida.
And they're doing the same thing.
They have a little relief station going on.
Get there, they have baby formula,
but they're almost out of food.
So we give them the food we had loaded up,
take the baby formula, head back to Hope Project,
explain what's going on, load our truck up again,
and then there's this guy, and he's like,
well, I got the truck.
Let's make sure the community center has food, too.
All right.
So we load it up, and this 89-year-old man
helps us load it, 89 years old,
and he helps us load a pallet and a half of food
into this guy's pickup truck.
I hope when I'm 89 years old, I end that spry.
But anyway, so we leave, help him unload it over at the
community center, and go on about our business.
So you got Dennis, Terry, David, the lady filling in for
David, the guy with the pickup truck, and the 89-year-old man.
They all have one thing in common.
They're all veterans, every single one of them.
Now, see, when things get bad, you can always count on
veterans to be there to lend a hand because they know what real bad is and I
know right now you're probably thinking well it's gonna be another one to
support the troops videos. Kind of, but not really. Supporting the troops. Right now
you got thousands that aren't getting their GI bill payments. Some of them on
the verge of homelessness because of it. No huge outcry. 20 suicides a day. No
No huge outcry, getting sent down to the border for some political stunt before the midterms
getting pulled off the border, before the caravan even shows up.
No huge outcry over getting used as a political tool.
Benefits getting whittled away constantly.
No huge outcry.
Getting shipped off to war under false pretenses.
huge outcry. Seems like the only time people bring up helping veterans is when
they don't want to help somebody else. You know, we can't help these migrants. We
have veterans we need to take care of. I don't see why we're giving these people
food stamps. We have veterans we need to take care of. I don't know why they
deserve a scholarship. What about our veterans?
It's interesting.
If the only time you bring up helping veterans is as an excuse not to help somebody else,
I want you to think back to that story.
You might be severely misunderstanding the character and nature of most veterans.
There aren't a whole lot of them that are going to say don't help someone, it's not
a common theme.
If you really want to help veterans, stop turning them into combat veterans because
some politician waved a flag and sold you a pack of lies that you didn't check out.
That might be one way.
Do you want to help veterans?
I actually care.
Get involved.
Before you can support the troops,
you've got to support the truth.
Anyway, it's just a thought.
a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}