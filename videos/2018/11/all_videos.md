# All videos from November, 2018
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2018-11-28: Let's talk about the arguments against asylum seekers.... (<a href="https://youtube.com/watch?v=5QoYFrUEUpk">watch</a> || <a href="/videos/2018/11/28/Lets_talk_about_the_arguments_against_asylum_seekers">transcript &amp; editable summary</a>)

Beau explains the legal and moral imperatives for supporting asylum seekers while challenging prevalent misconceptions about immigration and advocating for empathy and understanding.

</summary>

"It's cheaper to be a good person."
"Stop projecting your greed onto them. Maybe they just want safety."
"Accept some responsibility for your apathy and stay and fix it."
"I know killers, real killers. I don't know any that open fire on an unarmed crowd."
"Your canteen still be cool because you got ice in your veins."

### AI summary (High error rate! Edit errors on video page)

Correcting arguments against asylum seekers' entry after wild comments.
Referencing U.S. Constitution, international treaties, and protocols on refugees.
Explaining the legality of claiming asylum and entering a country.
Addressing the argument about sponsoring immigrant families.
Contrasting the focus on helping homeless vets with aiding asylum seekers.
Critiquing the emphasis on money and questioning who will pay for refugee assistance.
Comparing the cost of assisting asylum seekers to other government expenditures.
Disputing claims that asylum seekers come for benefits and asserting they seek safety.
Linking current immigration issues to past U.S. involvement in Central American countries.
Challenging the notion of staying in a dangerous country rather than seeking asylum.
Illustrating the scale of a tragedy like Syria happening in the U.S.
Mocking internet tough guys and their unrealistic bravado.
Offering a humorous take on potential survival scenarios.
Acknowledging his own skills but critiquing those who boast about combat abilities.
Condemning the idea of opening fire on unarmed individuals, especially women and children.
Expressing readiness for challenging situations with a touch of sarcasm.

Actions:

for advocates for humane immigration policies.,
Contact Office of Refugee Resettlement to offer support for asylum seekers (suggested).
Advocate for increased funding for refugee assistance programs (implied).
Educate others about the legal rights of asylum seekers and refugees (implied).
</details>
<details>
<summary>
2018-11-27: Let's talk about tear gassing toddlers.... (<a href="https://youtube.com/watch?v=toNqAAoofkI">watch</a> || <a href="/videos/2018/11/27/Lets_talk_about_tear_gassing_toddlers">transcript &amp; editable summary</a>)

Beau Ginn questions the use of tear gas on children, challenges perceptions of peace, and criticizes fear-mongering leadership.

</summary>

"What are you going to do when it doesn't happen?"
"We tear gassed kids like any other two-bit dictatorship."
"Because they don't like seeing their government behave like Saddam's."
"Sometimes, we tear gassed kids."
"Says they're better than us because I can't say we didn't earn it."

### AI summary (High error rate! Edit errors on video page)

Addressing tear gas used on children in the United States.
Criticizing violence and justification over trivial matters.
Asking how one might react if their child was tear-gassed.
Challenging the perceived peaceful nature of certain groups.
Mentioning heavy cartel presence in specific areas.
Implying potential violence against Border Patrol agents.
Questioning the nation's response to fear-mongering leadership.
Criticizing tear-gassing children and the impact on the nation's image.
Noting the troops' disapproval of government behavior.
Speculating on the lack of violence along the border.

Actions:

for advocates for justice,
Challenge the normalization of violence against children (implied)
Speak out against tear-gassing and violence (implied)
Support non-violent solutions to conflicts (implied)
</details>
<details>
<summary>
2018-11-26: Let's talk about that shooting in Alabama.... (<a href="https://youtube.com/watch?v=db6dEBKhovg">watch</a> || <a href="/videos/2018/11/26/Lets_talk_about_that_shooting_in_Alabama">transcript &amp; editable summary</a>)

Police mistakenly shot a fleeing, unarmed man in Alabama, sparking racial bias debates and the need for unbiased threat assessment by law enforcement.

</summary>

"Being armed and black isn't a crime."
"Nothing suggests this man did anything wrong."
"Biases exist. If you don't acknowledge them, they will persist."
"Time, distance, and cover. If he starts to point his weapon at the crowd, that's what we might call an indicator."
"The establishment, the powers that be, will always be able to play us against each other."

### AI summary (High error rate! Edit errors on video page)

Police initially claimed they killed the shooter in Alabama, but later admitted the man did not shoot anyone.
The man, a legally armed black individual, ran away from the scene of a shooting in a mall.
Beau questions the rationality of shooting someone who fled from gunfire in a mall.
He points out the tendency for people to defend police actions in such situations, even when the victim did nothing wrong.
Beau delves into the man's military service history and addresses misconceptions surrounding his honorable discharge.
He criticizes the media's portrayal of the man based on a single photo to incite negative opinions.
Beau brings attention to the racial implications of being armed and black in America, referencing past incidents where armed black individuals were unjustly killed by police.
He challenges biases and urges for a critical examination of the situation, asserting that being armed and black is not a crime.
Beau addresses law enforcement directly, acknowledging the need for positive threat identification and a thorough assessment before resorting to lethal force.
He calls for the acknowledgment and dissolution of biases to prevent further injustices and manipulation by those in power.

Actions:

for law enforcement, activists, community members,
Challenge biases within yourself and others (implied)
Advocate for unbiased threat assessment training for law enforcement (implied)
Support initiatives that address racial bias in policing (implied)
</details>
<details>
<summary>
2018-11-22: Let's talk about the judge overruling Trump's asylum ban.... (<a href="https://youtube.com/watch?v=PLIujWl-Wvk">watch</a> || <a href="/videos/2018/11/22/Lets_talk_about_the_judge_overruling_Trump_s_asylum_ban">transcript &amp; editable summary</a>)

A judge overruled Trump's asylum ban, exposing his attempt to act like a dictator by challenging the Constitution.

</summary>

"Crossing the border while seeking asylum in the middle of the desert and saying, hey, I'm claiming asylum. lawful, completely legal, completely moral."
"If it's your party, they can set fire to the Constitution and you'll roast a marshmallow over it."
"The three branches of government, that is the basis of the United States Constitution."

### AI summary (High error rate! Edit errors on video page)

A judge overruled Trump's asylum ban, revealing the legality of seeking asylum by crossing the border without permission.
News outlets labeled asylum seekers as "illegals" and the situation as an invasion, intentionally misleading the public.
Seeking asylum by crossing the border is lawful, legal, and morally sound.
Asylum is meant to provide people with the ability to seek safety.
Trump tried to change the law through a proclamation, attempting to act like a dictator.
Trump's actions were a direct challenge to the Constitution and an attempt to dictate law.
The judicial branch intervened to stop Trump's attempt, while Congress failed to act.
Trump's defenders may claim patriotism but failed to uphold the Constitution.
Trump's actions undermined Congressional authority and the foundation of the Constitution.
Blind partisanship has led to a disregard for the Constitution and the principles it upholds.

Actions:

for us citizens,
Stand up for the Constitution and demand accountability from elected officials (implied).
Educate others on the importance of upholding the Constitution and the separation of powers (implied).
</details>
<details>
<summary>
2018-11-21: Let's talk about supporting the troops.... (<a href="https://youtube.com/watch?v=cljonq0TBUA">watch</a> || <a href="/videos/2018/11/21/Lets_talk_about_supporting_the_troops">transcript &amp; editable summary</a>)

Beau showcases a community effort led by veterans to provide aid, while also challenging the true support needed for veterans beyond mere lip service.

</summary>

"If you really want to help veterans, stop turning them into combat veterans because some politician waved a flag and sold you a pack of lies that you didn't check out."
"Before you can support the troops, you've got to support the truth."
"If the only time you bring up helping veterans is as an excuse not to help somebody else, I want you to think back to that story."

### AI summary (High error rate! Edit errors on video page)

Dennis heard about a group in need north of Panama City without supplies after a hurricane.
Terry contacted Beau to provide aid and they loaded up supplies from Hope Project.
Beau encountered a lady filling in for David at the Hope Project and explained the situation.
They discovered the community center in Fountain, Florida running low on food but with baby formula.
Beau and others redistributed supplies between the locations to ensure both had what they needed.
An 89-year-old man helped load supplies, showcasing incredible generosity and strength.
All involved in the aid effort were veterans, showing their commitment to helping others in need.
Beau points out the lack of support for veterans facing issues like delayed GI bill payments and homelessness.
He criticizes the lack of attention to veteran suicides and the use of troops for political purposes.
Beau challenges the notion of using veterans as an excuse to neglect other marginalized groups.
He urges genuine care and involvement to truly support veterans and prevent unnecessary conflict.
Beau advocates for prioritizing truth and understanding in supporting veterans and avoiding unnecessary wars.

Actions:

for community members, activists,
Assist local aid efforts (exemplified)
Support veterans facing issues like delayed GI bill payments and homelessness (exemplified)
Advocate for truth and understanding in supporting veterans and preventing unnecessary conflicts (exemplified)
</details>
<details>
<summary>
2018-11-19: Let's talk about a native story in honor of Thanksgiving.... (<a href="https://youtube.com/watch?v=rSxupv4rKPE">watch</a> || <a href="/videos/2018/11/19/Lets_talk_about_a_native_story_in_honor_of_Thanksgiving">transcript &amp; editable summary</a>)

Beau uncovers the harsh reality behind the romanticized Native American narratives in mainstream media, urging reflection on fabricated tales and misrepresentations.

</summary>

"Most of what you know of Native stories is false. It's completely false, completely made up."
"A girl was kidnapped, held hostage, forced into marriage, which means raped, and then died an early death."
"There's not a whole lot of stories about natives just being native."
"It's a good time to bring it up and just kind of remember that most of what you know of Native culture, Native myths, Native stories is pretty much completely fabricated."
"Y'all have a nice night, it was just a thought."

### AI summary (High error rate! Edit errors on video page)

Shares a Native American story of Matoaka, not the typical Thanksgiving narrative.
Matoaka learned English, traded supplies, and was eventually kidnapped by colonists.
Forced into marriage to escape captivity, she was taken to England as a showcase of "civilized savages."
Matoaka died young, under suspicious circumstances—either poisoned or from disease.
Debunks the romanticized tale of Pocahontas saving John Smith's life.
Expresses how most Native stories in popular culture are fabricated and romanticized.
Criticizes the prevalent portrayal of natives in media with the white savior narrative.
Mentions a movie plot where a character leaves the tribe, becomes an FBI agent, and returns as a hero.
References Leonard Peltier's unjust imprisonment by the FBI.
Raises awareness about the misrepresentation of Native culture and myths in mainstream narratives.

Actions:

for history enthusiasts, educators, activists,
Research and share authentic Native American stories to counter misconceptions (suggested)
Support Native activists and causes to raise awareness about misrepresentations in media (implied)
</details>
<details>
<summary>
2018-11-14: Let's talk about police militarization.... (<a href="https://youtube.com/watch?v=LB3HUXdmid4">watch</a> || <a href="/videos/2018/11/14/Lets_talk_about_police_militarization">transcript &amp; editable summary</a>)

Beau dismantles the "warrior cop" mentality, exposing the dangers of police militarization and the consequences of prioritizing enforcement over public protection.

</summary>

"If you're a cop in an area where feeding the homeless is illegal, you're a bad cop."
"Being a warrior, everybody in the military is a warrior."
"They've bought into their own propaganda."
"There is no war on cops."
"You're the weapon if you're a warrior."

### AI summary (High error rate! Edit errors on video page)

Police militarization is a critical issue that affects communities, leading to concerns about the "warrior cop" mentality.
The misconception that being a warrior means being a killer is problematic and has resulted in a focus on militarizing law enforcement.
The shift towards viewing law enforcement as strictly enforcing the law, rather than protecting the public, raises moral questions for officers.
Enforcing unjust laws, such as those criminalizing feeding the homeless, can make a cop complicit in wrongdoing.
Propaganda within law enforcement, including inflated statistics on officer deaths, contributes to a heightened sense of danger and leads to hasty decision-making.
The belief in a "war on cops" is not grounded in reality, yet it influences the desire for militarization and warrior-like behavior among law enforcement.
The use of military equipment like flashbangs and MRAPs by SWAT teams is excessive and often lacks proper training, leading to dangerous outcomes.
Lack of intelligence work before operations results in SWAT teams making deadly mistakes, as seen in cases of wrong addresses and unnecessary force.
The discrepancy between perceived and actual capabilities of SWAT teams can have lethal consequences when faced with real resistance.
Beau advocates for a reevaluation of law enforcement's role as public servants and protectors rather than warriors.

Actions:

for law enforcement reform advocates,
Reassess law enforcement's role as public servants and protectors rather than warriors (implied)
Advocate for proper training and accountability in law enforcement (implied)
Support efforts to demilitarize police forces and redirect resources towards community policing (implied)
</details>
<details>
<summary>
2018-11-12: Let's talk about red flag gun seizures.... (<a href="https://youtube.com/watch?v=5JoyD1iPnQI">watch</a> || <a href="/videos/2018/11/12/Lets_talk_about_red_flag_gun_seizures">transcript &amp; editable summary</a>)

Beau explains the flaws in the execution of red flag gun seizures and suggests a more sensible approach to prevent harm and uphold due process, urging swift action from all sides.

</summary>

"The idea is sound, okay, the execution isn't."
"Cops are picking up these tactics, and they're using them in a police setting when they're not designed to be."
"That due process is important."
"You've got a good idea, guys."
"It's a good idea. The execution is bad and the execution can be fixed very easily."

### AI summary (High error rate! Edit errors on video page)

Explains red flag gun seizures, where local cops can temporarily seize guns if someone is deemed a threat.
Acknowledges the sound idea behind the concept of red flag gun seizures but criticizes its flawed execution.
Raises concerns about the violation of due process when guns are seized without proper legal procedures.
Criticizes the tactical implementation of red flag gun seizures, particularly the early morning raids by cops.
Points out the negative consequences of using military tactics in civilian settings by law enforcement.
Advocates for proper training for law enforcement officers using military equipment to prevent unnecessary harm.
Suggests a better approach to executing red flag gun seizures, such as detaining individuals on their way home for due process instead of surprise raids.
Emphasizes the importance of due process in red flag gun seizures to prevent wrongful confiscation of firearms.
Urges both pro-gun and gun control advocates to work together to improve the execution of red flag gun seizure laws.
Encourages swift action to address the flaws in red flag gun seizure implementation before more lives are lost.

Actions:

for advocates, law enforcement, gun owners,
Contact your representatives to push for immediate fixes in the flawed implementation of red flag gun seizure laws (suggested).
Advocate for proper training for law enforcement officers using military equipment (suggested).
Work towards ensuring due process in red flag gun seizures to prevent wrongful confiscations (suggested).
</details>
<details>
<summary>
2018-11-05: Let's talk about fear and a tale of two neighborhoods... (<a href="https://youtube.com/watch?v=1BMl6phxWpU">watch</a> || <a href="/videos/2018/11/05/Lets_talk_about_fear_and_a_tale_of_two_neighborhoods">transcript &amp; editable summary</a>)

Beau talks about fear, community action, and the importance of taking personal responsibility instead of relying on government or succumbing to irrational fears.

</summary>

"It's fear. It's fear."
"If you got a problem in your community, fix it, do it yourself."
"We've got to stop being afraid of everything."
"One of these migrants may be a murderer, maybe, there's 7,000 people, I'm sure somebody's done something wrong in that group."
"It's up to you, it is up to you."

### AI summary (High error rate! Edit errors on video page)

Describes running supplies to Panama City after a hurricane to ensure people get what they need.
Talks about visiting two neighborhoods: a rough one and a nice one.
Shares an encounter in the rough neighborhood where gang members help distribute supplies efficiently.
Narrates an incident in the nice neighborhood where a neighbor with a gun accuses him of looting.
Expresses frustration at the fear that pervades society, leading to irrational behaviors.
Criticizes the reliance on government and the lack of personal initiative in solving community problems.
Encourages individuals to take action locally rather than waiting for governmental intervention.
Emphasizes the importance of teaching children through actions, not just words.
Argues against demonizing migrants fleeing Central America and calls for empathy and understanding.
Concludes by urging people to take responsibility for solving problems instead of waiting for others.

Actions:

for community members, activists,
Support non-governmental organizations like Project Hope Incorporated by donating supplies or volunteering (suggested)
Take initiative in your community by identifying and addressing local issues without waiting for government intervention (exemplified)
</details>
<details>
<summary>
2018-11-03: Let's talk about how the troops on the border can defend American freedom.... (<a href="https://youtube.com/watch?v=kWWouKv51zY">watch</a> || <a href="/videos/2018/11/03/Lets_talk_about_how_the_troops_on_the_border_can_defend_American_freedom">transcript &amp; editable summary</a>)

A reminder to soldiers: implement vague orders wisely, defend American freedom by upholding laws, and beware of political misuse of your honor and uniform.

</summary>

"Order comes down, it's vague. Gets more specific until it gets to those guys on the ground. And they've got to figure out how to implement it."
"You want to defend American freedom, you keep that weapon slung and you keep it on safe."
"Honor, integrity, those things. It's bred into you, right?"
"You wouldn't circumvent the law, earn that trust, keep that weapon slung and keep it on safe."
"When that vague order comes down, you ask for clarification. Email. Clarification."

### AI summary (High error rate! Edit errors on video page)

Recounts a story from the past about a general ordering a lieutenant to take a hill crawling with enemies, resulting in success without casualties.
Explains the process of vague orders becoming specific on the ground and the importance of implementation.
Talks about the unique chance for soldiers at the border to defend American freedom by upholding specific laws and rules of engagement.
Warns against mission creep, using Afghanistan as an example, and the illegal request for U.S. soldiers to act as emergency law enforcement personnel.
Emphasizes the need to stay within the law, especially regarding non-combatants, and the potential consequences for enlisted soldiers if rules are broken.
Advises soldiers to keep their weapons safe and slung, not getting involved in illegal activities.
Encourages soldiers to know the laws of war, the real rules of engagement, and not to follow vague or illegal orders.
Urges those at the border or heading there to learn how to legally apply for asylum in the United States.
Criticizes the political use of soldiers by higher-ups, warning about the misuse of trust and honor for political gains.
Suggests requesting clarification on vague orders, following the law, and being prepared for a potential need for defense against illegal actions.

Actions:

for soldiers, military personnel,
Learn how to legally apply for asylum in the United States (suggested)
Request clarification on vague orders and follow legal guidelines (implied)
Keep weapons safe and slung, avoiding illegal activities (implied)
</details>
<details>
<summary>
2018-11-01: Let's talk about a Kennedy legend you've probably never heard and why it's important today... (<a href="https://youtube.com/watch?v=9Q2S1hLWmo4">watch</a> || <a href="/videos/2018/11/01/Lets_talk_about_a_Kennedy_legend_you_ve_probably_never_heard_and_why_it_s_important_today">transcript &amp; editable summary</a>)

Beau raises concerns about the suspension of habeas corpus, linking it to a legend about Green Berets and urging people to understand its significance in preserving the rule of law and national security.

</summary>

"The suspension of habeas corpus is though, that is a threat to national security. It's a threat to your very way of life."
"The suspension of habeas corpus is something that can't be tolerated."
"The suspension of habeas corpus is an affront to everything that this country has ever stood for."
"You need to look it up. You need to realize exactly what it means for that to be gone."
"You can watch this country turn into another two-bit dictatorship or you can kill this party politics that you're playing."

### AI summary (High error rate! Edit errors on video page)

Tells the legend of President Kennedy meeting with selected Green Berets in 1961, making them take an oath to fight back against a tyrannical government.
Mentions the secrecy and lack of evidence surrounding the oath and the Green Berets' activities.
Talks about the involvement of Green Berets in Kennedy's honor guard after his assassination.
Raises questions about the existence of a Special Forces Underground and its investigation by the Department of Defense.
Points out the annual visit by active-duty Green Berets to Kennedy's grave site for a brief ceremony.
Connects Kennedy's criteria for fighting tyranny, including the suspension of habeas corpus, to current political considerations.
Criticizes the idea of suspending habeas corpus as a threat to the rule of law and national security.
Challenges the argument for suspending habeas corpus in dealing with migrants and asylum seekers.
Urges people to understand the significance of habeas corpus and its potential removal from legal protections.
Raises concerns about the erosion of the rule of law and the danger of unchecked presidential powers.

Actions:

for americans, activists, citizens,
Educate yourself and others on the significance of habeas corpus and its importance in upholding the rule of law (implied).
Advocate for the protection of legal rights and civil liberties in your community and beyond (implied).
Stay informed about political decisions that could impact fundamental rights and freedoms (implied).
</details>
