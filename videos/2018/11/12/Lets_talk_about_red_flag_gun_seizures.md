---
title: Let's talk about red flag gun seizures....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5JoyD1iPnQI) |
| Published | 2018/11/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains red flag gun seizures, where local cops can temporarily seize guns if someone is deemed a threat.
- Acknowledges the sound idea behind the concept of red flag gun seizures but criticizes its flawed execution.
- Raises concerns about the violation of due process when guns are seized without proper legal procedures.
- Criticizes the tactical implementation of red flag gun seizures, particularly the early morning raids by cops.
- Points out the negative consequences of using military tactics in civilian settings by law enforcement.
- Advocates for proper training for law enforcement officers using military equipment to prevent unnecessary harm.
- Suggests a better approach to executing red flag gun seizures, such as detaining individuals on their way home for due process instead of surprise raids.
- Emphasizes the importance of due process in red flag gun seizures to prevent wrongful confiscation of firearms.
- Urges both pro-gun and gun control advocates to work together to improve the execution of red flag gun seizure laws.
- Encourages swift action to address the flaws in red flag gun seizure implementation before more lives are lost.

### Quotes

- "The idea is sound, okay, the execution isn't."
- "Cops are picking up these tactics, and they're using them in a police setting when they're not designed to be."
- "That due process is important."
- "You've got a good idea, guys."
- "It's a good idea. The execution is bad and the execution can be fixed very easily."

### Oneliner

Beau explains the flaws in the execution of red flag gun seizures and suggests a more sensible approach to prevent harm and uphold due process, urging swift action from all sides. 

### Audience

Advocates, Law Enforcement, Gun Owners

### On-the-ground actions from transcript

- Contact your representatives to push for immediate fixes in the flawed implementation of red flag gun seizure laws (suggested).
- Advocate for proper training for law enforcement officers using military equipment (suggested).
- Work towards ensuring due process in red flag gun seizures to prevent wrongful confiscations (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the flaws in the implementation of red flag gun seizures, the importance of due process, and the need for collaboration between different stakeholders to improve the system.

### Tags

#RedFlagLaws #GunControl #DueProcess #LawEnforcement #Advocacy


## Transcript
Well howdy there internet people, it's Beau again.
So tonight we're gonna talk about red flag gun seizures.
Seems timely considering we just had somebody
get killed in one.
So if you don't know what one of these things is,
it's where local cops say, hey, this person's a threat,
we need to take their guns, they get a court order
and they temporarily seize them.
Okay, this makes sense.
The idea is sound, okay, the execution isn't.
The idea is sound, and that's the reason I want to talk about it, because it is a good
idea in theory, but the way it's being executed makes it a bad idea, and it doesn't take much
to fix it.
So the way it's currently happening is cops say, this person is a threat.
They've made these Facebook posts.
We think they're going to be a mass shooter.
They go to a judge.
They get a rubber stamp, and then they go take his guns.
Okay, it doesn't matter how you feel about guns.
As the law currently stands in the United States,
it's an individual right.
It's supposed to be an inalienable right.
For them to be able to take it even temporarily
without due process is scary.
That's not a good thing.
Okay, now let's go to the tactical implementation of it,
how it's happening.
In this case, where the guy got killed,
cop showed up at 5 a.m.
Mmm, man, that sounds familiar.
sneaking up on your opposition early in the morning
when they're disoriented, not thinking clearly.
Yeah, it's a good tactic in combat
when the idea is to sneak in and kill them.
If you're wanting to have a civil conversation,
and I give these cops credit,
the two that showed up, from what I understand,
they just knocked on the door.
They didn't kick it in.
If you want to have a civil conversation,
perhaps that's not the best time.
This is something that's happened a lot lately.
The cops are mimicking military tactics
and they don't understand why they're used.
That tactic is used in the military
because people are disoriented.
They're not thinking clearly.
It gives you an edge when you go in to kill them.
Now, if you want to have a civil conversation,
especially with somebody who you believe
is predisposed to violence,
do you really want to have it
when they're not thinking clearly and they're disoriented,
perhaps a little agitated
because you drug them out of bed at 5 a.m.?
that make a lot of sense.
This is happening a lot, and it needs to stop.
Cops are picking up these tactics,
and they're using them in a police setting
when they're not designed to be.
And they're getting these toys from DOD.
They get the toy, and they don't get the training.
It's why you got babies getting maimed
by concussion grenades.
Happened in Georgia.
They don't know how to use it.
They get the toy, they don't get the training.
Now personally, I don't think they should have either,
But that's a subject for another video.
So that's one way that it gets implemented.
It gets executed.
The cops go in and they knock on the door.
That's great.
In some cases, they kick the door in.
Now put yourself in the shoes of the guy who's
getting his guns taken.
You've committed no crime.
You have no reasonable expectation for the police to
be kicking in your door.
Who do you think it is?
You don't think, oh, it's the cops.
You think it's an intruder.
How do you respond?
It's going to lead to more dead bodies.
So legal execution, not good.
Tactical execution, not good.
What if there was a way to fix it by just doing one thing?
So instead of going to this guy's house in the early
morning, why not?
I mean, if you know that he's a threat and you're certain
of this to want to deprive him of inalienable rights. You've got to know
something about him. You should know where he works at least. Why not pull him
over on the way home when he doesn't have an arsenal of weapons at his
disposal? Maybe he has one on him, but that's it. You're mitigating that
risk. More importantly, you can cuff him. Take him straight from there to his
hearing and give him due process and you're not giving up that element of
surprise see that's the reason they didn't want to do the they wanted to
get the guns before the hearing because they're worried about kind of tipping
their hand and if the guy was gonna commit a mass shooting you know okay
well you're gonna have your hearing next week and we'll take your guns then so he
does it before the hearing this way you don't lose that so you can pick them up
on their way home take them straight to their hearing because at that hearing
you may find out those Facebook posts that this whole thing is based on it's
not even his profile it's somebody else making them you find out he lost his
phone and somebody one of his friends is playing a joke on him and making the
post for him you could find out that the person who said all of this well they
They owe him 200 bucks and they don't want to pay it.
That due process is important.
So you've got a good idea, guys.
If you're in the gun control crowd, you need to be on the horn with your representative
to fix this immediately because the more bodies that pile up and there will be more, eventually
this law, these laws are just going to get thrown out.
Now, if you're in the pro-gun side, if due process is there, what's the problem?
Seriously.
You don't really want some loon out there running around saying he's going to kill
people while he's got his weapons at home, do you?
Makes you look bad.
You want to find a way to make this law work because the less of these shooters there are,
the better off you're going to be.
This is one of those things that everybody should be working for.
And it should be pretty easy to make it happen.
It's a good idea.
The execution is bad and the execution can be fixed very easily.
So anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}