---
title: Let's talk about a native story in honor of Thanksgiving....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rSxupv4rKPE) |
| Published | 2018/11/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares a Native American story of Matoaka, not the typical Thanksgiving narrative.
- Matoaka learned English, traded supplies, and was eventually kidnapped by colonists.
- Forced into marriage to escape captivity, she was taken to England as a showcase of "civilized savages."
- Matoaka died young, under suspicious circumstances—either poisoned or from disease.
- Debunks the romanticized tale of Pocahontas saving John Smith's life.
- Expresses how most Native stories in popular culture are fabricated and romanticized.
- Criticizes the prevalent portrayal of natives in media with the white savior narrative.
- Mentions a movie plot where a character leaves the tribe, becomes an FBI agent, and returns as a hero.
- References Leonard Peltier's unjust imprisonment by the FBI.
- Raises awareness about the misrepresentation of Native culture and myths in mainstream narratives.

### Quotes

- "Most of what you know of Native stories is false. It's completely false, completely made up."
- "A girl was kidnapped, held hostage, forced into marriage, which means raped, and then died an early death."
- "There's not a whole lot of stories about natives just being native."
- "It's a good time to bring it up and just kind of remember that most of what you know of Native culture, Native myths, Native stories is pretty much completely fabricated."
- "Y'all have a nice night, it was just a thought."

### Oneliner

Beau uncovers the harsh reality behind the romanticized Native American narratives in mainstream media, urging reflection on fabricated tales and misrepresentations.

### Audience

History enthusiasts, educators, activists

### On-the-ground actions from transcript
- Research and share authentic Native American stories to counter misconceptions (suggested)
- Support Native activists and causes to raise awareness about misrepresentations in media (implied)

### Whats missing in summary

Deeper insight into the impact of misrepresentation on Native communities and the importance of amplifying authentic voices.

### Tags

#NativeAmerican #Misrepresentation #Colonialism #History #Awareness


## Transcript
Well howdy there internet people, it's Beau again.
Since we are closing in on Thanksgiving, I figure it's a good time to tell a native story.
I'm not going to tell you the story of Thanksgiving. Pilgrims, John Smith, none of that. You know all that stuff.
I'm going to tell you a different one.
I'm going to tell you the story of a girl named Matoaka.
So when she's ten or twelve years old,
she meets this English explorer,
and she learns English.
It's cool because they actually found the guys' notes.
And there's stories.
And you can tell they were doing writing exercises and stuff.
Neat little thing.
Anyway, so because she learned English,
she's the one that goes to the fort.
And she trades and brings supplies
and that kind of stuff.
Her dad was a pretty powerful native.
So to keep him in check, the colonists kidnapped her.
and they held her hostage for a very long time.
So long, in fact, that when she was a little bit older,
talking mid to late teens,
one of the colonists kind of took a liking to her.
And in order to get out of being held hostage,
she was forced into marrying him.
And then he took her back to England,
kind of paraded her around as like an advertisement.
See, look, the colonies aren't that bad.
Here's a civilized savage.
And anyway, she died 19 or 21, somewhere in there.
One theory says that she just outlived her usefulness,
so he poisoned her.
Another version says that she contracted some disease.
Either way, there was definitely no happily ever
after in this story, not like Pocahontas.
That is actually the story of Pocahontas. The real story.
Some of it's debated, but that's the real story. Matoaka was her real name. Pocahontas,
it means like rebellious child. You know, that famous scene of her saving John Smith's life,
you know, in the painting, she's like this beautiful adult, native girl,
bare-breasted just, you know, covering him up so her dad doesn't kill him. That
probably never happened. It certainly didn't happen because, you know, they were
romantically involved or, well, let's hope not, because she was 10 or 12.
Most of what you know of Native stories is false. It's completely false, completely
made up. And when you think about it, when you take what actually happened, you
know, a girl was kidnapped, held hostage, forced into marriage, which means raped,
and then died an early death. All of a sudden, the fact that that got turned
into a love story, it makes sense why natives are like, don't wear that as a
Halloween costume. But that's what happens to every native story. There's
two storylines in popular culture when it comes to natives. There's the good
native, the understanding native, who understands that white people are there
you know to be their saviors and to civilize them, who buddies up to them and
somehow that relationship saves the tribe. Or somebody leaves the tribe or
or the Rez, goes and becomes civilized and then comes back and saves everybody.
There's not a whole lot of stories about natives
just being native. There's always that
interjection of the white savior. My favorite version
of the guy who leaves and then comes back is the one where
I can't remember the name of the movie now, but the guy he leaves becomes an FBI agent.
And he comes back, and it's at a reservation that is clearly meant to be Pine Ridge.
And he's the hero.
He saves the day.
And it's funny, if you ever talk to a Native activist and ask them how the FBI treats them,
especially one from Pine Ridge, you're probably going to get a different story.
You could ask Leonard Peltier how he feels about it, but you're going to have to write
him at Coleman Prison. It's a federal prison. The FBI put him there. He's been
there a couple decades for something. It's pretty clear he probably didn't do,
at least not to the extent they're saying. Anyway, it's this holiday is one
of those holidays. It's a good time to bring it up and just kind of remember
that most of what you know of Native culture, Native myths, Native stories is
pretty much completely fabricated. Not a whole lot of it's true in popular
Anyway, y'all have a nice night, it was just a thought.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}