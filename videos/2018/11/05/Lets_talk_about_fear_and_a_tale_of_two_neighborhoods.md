---
title: Let's talk about fear and a tale of two neighborhoods...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1BMl6phxWpU) |
| Published | 2018/11/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes running supplies to Panama City after a hurricane to ensure people get what they need.
- Talks about visiting two neighborhoods: a rough one and a nice one.
- Shares an encounter in the rough neighborhood where gang members help distribute supplies efficiently.
- Narrates an incident in the nice neighborhood where a neighbor with a gun accuses him of looting.
- Expresses frustration at the fear that pervades society, leading to irrational behaviors.
- Criticizes the reliance on government and the lack of personal initiative in solving community problems.
- Encourages individuals to take action locally rather than waiting for governmental intervention.
- Emphasizes the importance of teaching children through actions, not just words.
- Argues against demonizing migrants fleeing Central America and calls for empathy and understanding.
- Concludes by urging people to take responsibility for solving problems instead of waiting for others.

### Quotes

- "It's fear. It's fear."
- "If you got a problem in your community, fix it, do it yourself."
- "We've got to stop being afraid of everything."
- "One of these migrants may be a murderer, maybe, there's 7,000 people, I'm sure somebody's done something wrong in that group."
- "It's up to you, it is up to you."

### Oneliner

Beau talks about fear, community action, and the importance of taking personal responsibility instead of relying on government or succumbing to irrational fears.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Support non-governmental organizations like Project Hope Incorporated by donating supplies or volunteering (suggested)
- Take initiative in your community by identifying and addressing local issues without waiting for government intervention (exemplified)

### Whats missing in summary

The full transcript provides a detailed account of Beau's experiences in contrasting neighborhoods, shedding light on societal fears and the importance of community action.


## Transcript
Well howdy there internet people, it's Bo again.
So tonight we're gonna talk about fear
and we are going to talk about,
well I'm gonna tell you a tale of two neighborhoods.
Last couple weeks, every other day I've been running supplies
down to Panama City to the hurricane impact zone,
making sure, trying to make sure people get what they need.
Stop at a little supply depot, pick it up, drive down.
Been taking my kids with me and people have asked,
Why are you taking your kids?
You're going to some rough neighborhoods.
And if you've ever been there on vacation,
you're probably thinking rough neighborhood.
Well, if you went there on vacation,
you went to Panama City Beach.
Panama City's over the bridge,
and there are some rough neighborhoods there.
The other day, the first stop I made
was one of those neighborhoods.
When I turned the corner, I realized where I was at.
And it's the type of neighborhood people in my skin tone normally drive through speeding with their doors locked, or
they try to avoid it completely.
But I'm creeping down this road because they've cleared the debris in it, you know, you can
tell somebody went through with an axe and busted it up, and it's all piled on the side of the road.
If you've ever been in one of those neighborhoods, you know the addresses are normally painted on the curb, and they're
confusing to begin with.  begin with. It's a facility number, building number, and then a letter for the unit or
something like that. It's hard to find what you're looking for. So I'm driving real slow,
and about halfway down the street, four guys standing there, and one of them walks out
to the truck. These guys, they're obvious gang bangers. They're all sporting collars,
And he's like, homie, you lost, you know?
Obvious subtext of, white boy, you need to get out of here.
You're in the wrong place.
And I'm like, well, I'm looking for this address.
And he's like, oh, you're the baby formula man.
I'm like, yeah, today I am the baby formula man.
He sends one of his buddies to go get the girl that needs it,
and we're standing there talking.
One guy's standing there adjusting,
because he's carrying it and doesn't
have this holster ride or something.
Other guy's got his head in the back of the truck talking
to the boys.
My son's real impressed with the wind and how it smashed
the trees.
And this guy's like, nah, wind didn't smash the trees.
I smashed the trees with my axe.
My son's like, four-year-old, he's like, well, you made a
mess at my house.
And I asked him if they needed anything.
Three minutes, these guys gave me the diaper sizes of every
kid in the neighborhood.
Told me which kids needed infant formula, which kids
needed toddler mix, and charcoal would be really
helpful, because they got the girls out back, no power.
A guy down the road, nobody likes, needs a tarp.
Just doot, doot, doot, doot.
And I type it in my phone.
I run up, get it, bring it back to him.
Get there, open the back of that truck.
One guy hands it out.
Other people run it to where it needs to go.
Supply that whole neighborhood in like 10 minutes.
really impressive to be honest. Leave there and I drive up and I end up in a nice
neighborhood. Not like gated community nice, but a nice neighborhood, you know.
And the addresses are up on the house, all pretty. I find what I'm looking for
pretty easy, pull in the driveway, get in the back of the truck, pull out a case of
MREs and some water. Start walking up to the house and some idiot from next door
comes running out with his gun screaming you loot we shoot and we have words and
he goes back inside and I drop off the supplies get back in the truck and I'm
sitting there thinking you know this guy he thinks he's the neighborhood hero you
know because he's ready doesn't know his immediate neighbor the elderly couple
next door doesn't have food and water leave there go two streets north same
neighborhood, start to drive down the road, can't get down it, there's a tree
in it. So I grab a chainsaw and I start cutting it up. Guy walks out of his house
with his chainsaw, helps. We get it done in like 10 minutes. He's like, I didn't
want to be out here alone. He's like, you know, the city said that anybody caught
without working without a license get arrested.
Have you seen them?
It's been a few days since the hurricane.
He's like, no, I haven't seen them in four or five days.
Well, if you haven't seen them,
they're not gonna see you.
Clear your road.
What are you doing?
They're saying that because they're trying
to stop contractors and shady construction workers
from coming in and ripping people off.
But that's what it boils down to now. It's fear. It's fear. You know, that guy
that came out of his house, I'm sure, because that neighborhood already had
power, I'm sure he's sitting there reading the rumors on social media. You
know, there's looting in the streets. He's holed up. I've been down there.
That there's not looting in the streets. There's not it's I mean, I'm sure that it's happened
But it's been pretty isolated and I've been all over down there
The other guy is scared to cut up a tree without government permission, clear his own road.
The sons of the pioneers, right?
That's what we like to cast ourselves as.
We're Americans, the sons of the pioneers.
Are we anymore?
Or have we had it so good for so long that we've forgotten what that is and we've become
afraid of everything, everything. It's that fear of the other. That guy, he's not
from this neighborhood. Better get my gun. See, the sad part about that is, you know,
somebody who has not had a gun pointed at them before, that might stop them from
doing relief work. They might stop doing it. And that guy thinks he's protecting
people. He's helping. Because that's what it takes now, right? That's what it takes.
That God is the power of everything now. Because we're afraid of everything. So
violence becomes the answer to everything. Now there's a real fear.
There's genuine fear. If you're ever somewhere and you don't know why but the
hair in your neck starts to stand up, your subconscious has caught something,
then you need to pay attention to it and then there's a rational fear.
And that's what we're faced with now, and there's people to prey on it, and prey on
that self-centered nature that we've developed in this country.
You know, there are people in the comments section, some of these videos, that actually
believe that thousands of people fleeing Central America are really coming here
because they want to vote and influence our election and that's
really what they're doing. How egotistical do you have to be to believe that?
These people do not care about your politician. If they were going to wait
for a politician to better their life, they'd still be at home.
These people are on foot, walk a couple thousand miles to try to build a better life.
They don't care about your politician, dude.
And anybody willing to do that, yeah, they can live next to me.
They're not afraid.
They're willing to travel a couple thousand miles to come to a country that is certainly
welcoming to them right now to get somewhere safe and try to build a better
life. Sound like good people to me, but it's that fear, that fear. Everybody's had
to get you. Nobody's had to get you. You know, we are not the sons of the pioneers and the
daughters of Rosie the Riveter anymore because we're not going to fix the
problem ourselves. We're going to wait for somebody else to do it. Wait for
government, big daddy government. Need them for everything to cut a tree in our
road. It's insane. When we're leaving that day, we're in a small town and my
two-year-old spots a fire truck sitting out in front of the fire station and he
He loves them, and he's about ready to crawl out the window.
He said, oh, rescue team.
Then he notices that the fire station's damaged.
He's like, oh no, the rescue team's hurt.
Who's going to help?
My four-year-old doesn't miss a beat, us, and that's why I take them.
That is why I take them.
You can't tell a kid to be a good person.
We've got to show them.
Four-year-old understands that at some point and at some times, it's just us.
There is no big daddy government coming to help you.
If you're waiting for a politician, you need to understand you're waiting for the guy
that came out screaming, you loot, we shoot.
That's who you're hoping is going to solve your problem.
Because those people in D.C., they're convinced, they got everybody protected, meanwhile their
immediate neighbor doesn't have food and water.
That's who you're waiting for.
If you got a problem in your community, fix it, do it yourself.
Do not wait for a leader, don't wait for some savior to arrive, don't be afraid, put out
the flame of your foot, get involved.
want to make America great, it isn't going to happen in D.C. It's going to happen at
the local level. It's going to happen because we've reclaimed that initiative, that American
character, that, you know, Rosie the Reverend, you know. Instead, we're afraid of everything
and clinging to those guns.
Because that's what it takes to be a hero today.
That's how you become a hero.
It's on TV.
You've got to get in a gunfight, right?
That's what makes you tough, makes you a hero.
Let me ask you this.
At the end of the day, who's the better man?
I'm sure all those gang bangers had guns.
No stranger to violence.
They knew the diaper size of every kid in that neighborhood.
Or the guy screaming, you loot, we shoot,
didn't know his neighbor needed food and water.
Who's the better man?
Who's the hero in that story?
Gun doesn't make you a hero.
gun doesn't make you tough.
We've got a real problem in this country, and it really boils down to fear, it boils
down to fear.
We've got to stop being afraid of everything.
One of these migrants may be a murderer, maybe, there's 7,000 people, I'm sure somebody's
done something wrong in that group.
I mean, that's a pretty high probability right there.
So I guess we just write them all off then.
Let's do that with every group.
I'm sure there's somebody in your city
who's done something wrong.
We need to just write your whole city off.
Here's the thing guys, it's up to you, it is up to you.
If not you, who?
Who's going to solve the problems if it's not you?
You personally, not your representative, you.
Anyway, it's just a thought, just something to think about, and while I'm on this subject
I kind of want to point out that there are people still out there like that.
This little supply depot I've been going to, it's at a place called Project Hope Incorporated.
Under normal circumstances, these guys work with vets, and they use horses, kind of cool,
to provide a cathartic experience. Interesting little concept, and they do good work
according to everybody that I've talked to that has ever had anything to do with
them, they've turned their entire facility into a supply depot to help
people and need to think about that. Supply is coming in to a non-government
location, non-government people getting to where it needs to go. If government
I could help if government could really solve the problems.
I wouldn't be running supplies in from 30 or 40 miles away.
Anyway, y'all have a nice night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}