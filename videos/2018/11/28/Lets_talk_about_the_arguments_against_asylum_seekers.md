---
title: Let's talk about the arguments against asylum seekers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5QoYFrUEUpk) |
| Published | 2018/11/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Correcting arguments against asylum seekers' entry after wild comments.
- Referencing U.S. Constitution, international treaties, and protocols on refugees.
- Explaining the legality of claiming asylum and entering a country.
- Addressing the argument about sponsoring immigrant families.
- Contrasting the focus on helping homeless vets with aiding asylum seekers.
- Critiquing the emphasis on money and questioning who will pay for refugee assistance.
- Comparing the cost of assisting asylum seekers to other government expenditures.
- Disputing claims that asylum seekers come for benefits and asserting they seek safety.
- Linking current immigration issues to past U.S. involvement in Central American countries.
- Challenging the notion of staying in a dangerous country rather than seeking asylum.
- Illustrating the scale of a tragedy like Syria happening in the U.S.
- Mocking internet tough guys and their unrealistic bravado.
- Offering a humorous take on potential survival scenarios.
- Acknowledging his own skills but critiquing those who boast about combat abilities.
- Condemning the idea of opening fire on unarmed individuals, especially women and children.
- Expressing readiness for challenging situations with a touch of sarcasm.

### Quotes

- "It's cheaper to be a good person."
- "Stop projecting your greed onto them. Maybe they just want safety."
- "Accept some responsibility for your apathy and stay and fix it."
- "I know killers, real killers. I don't know any that open fire on an unarmed crowd."
- "Your canteen still be cool because you got ice in your veins."

### Oneliner

Beau explains the legal and moral imperatives for supporting asylum seekers while challenging prevalent misconceptions about immigration and advocating for empathy and understanding.

### Audience

Advocates for humane immigration policies.

### On-the-ground actions from transcript

- Contact Office of Refugee Resettlement to offer support for asylum seekers (suggested).
- Advocate for increased funding for refugee assistance programs (implied).
- Educate others about the legal rights of asylum seekers and refugees (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of legal, moral, and practical arguments in support of asylum seekers, along with a scathing criticism of anti-immigrant sentiments and internet bravado.

### Tags

#AsylumSeekers #Immigration #LegalRights #Refugees #CentralAmerica


## Transcript
Howdy there, internet people, it's Bo again.
So tonight we're going to talk about the arguments against
letting the asylum seekers in.
We're going to go through them, because yesterday in the
comments section it was wild.
Even though I tried to correct some things, people just didn't
get it, so we're going to go through it slowly in crayons.
OK, we want them to do it legally.
Pull out your constitution.
If you don't have one, Google it.
to Article 6, I think Section 2. I could be wrong. It's right in the beginning though,
first couple paragraphs. What you're going to find out is that the U.S. Constitution,
federal law, and international treaties are, quote, the supreme law of the land. The only
thing an international treaty does not supersede is the Constitution itself. Okay, so international
treaties that we sign are US law, they become US law.
Okay so now I want you to Google the 1967 protocols on refugees, which we are signatory
of.
Go to article 31, there it is.
People who enter a country illegally to claim asylum cannot be punished.
There is no such thing as entering a country illegally if you're claiming asylum, if you're
a refugee.
That is U.S. law.
If you let that sink in and you really think about it, you realize those trying to claim
asylum are following the law and those trying to stop them are violating it.
Interestingly enough, in that same treaty, you will find out that we can't refile them.
Send them back.
Okay, moving on, one of the other cute things was, where's yours?
Where's yours?
Where's the immigrant family you're sponsoring?
Okay, well first, I actually have done that.
But I don't think you need to to have an opinion on this, to be honest.
You know, we should ask.
Let's ask the homeless vet living at your house, oh, you don't have one?
Strange, kept bringing them up.
We need to save that money for those homeless vets.
Seems like the only time you guys ever bring up helping vets is when you don't want to
help somebody else.
Fine, you don't have a homeless vet.
Let's talk to the kid you adopted that was going to be aborted.
No?
Don't have one of them either, huh?
Interesting.
Seems a little hypocritical.
Just outside looking at.
But it's what it is.
It's the U.S.
We're always about money.
talking about money because it's our national character. And that's the other
thing, who's gonna pay for this? Office of Refugee Resettlement. They got a budget
500 million dollars a year, half a billion dollars. Budgets actually a
little bit more than that, but that's what they spent doing this.
Interestingly enough, Trump got them more money. I thought that was funny, I found
that out tonight. Their new budget's a little bit bigger. So I will give you this. That
is a lot of money to spend. But here's the thing. What did his little stunt down on the
border cost? Two billion. Four times as much. It's cheaper to be a good person. All right.
Next thing. Oh, well, they're coming here for benefits because we're Americans. Everything's
about money. Really, Mexico offered them benefits. They turned them down. So stop
projecting your greed onto them. Maybe they just want safety. All right, moving
on. This isn't our problem. Yes, it is. Yes, it is. 2009, we back to the coup in
Honduras that overthrew the government. And that's where most these people are
coming from. Now, Nicaragua, you can trace pretty much Nicaragua, Guatemala. You know
what? We'll do it the other way. Costa Rica and Belize are the only two
countries in that region that I can think of that we haven't recently messed
up. So we'll compromise. Costa Rican refugees and refugees from Belize,
they're not our problem. All the other ones are. Then my favorite, well if it
was me, I wouldn't put my kid in harm's way. I'd stay and fix my country. You put
their kid in harm's way because you wouldn't fix your country. You didn't
reel in your government when they were down there messing with their countries,
did you? You put their kid in harm's way. Accept some responsibility for your
apathy and stay and fix it. You know, I ran into you clowns in 2015 talking about
Syria. And I tried to figure out a way to put it into scale. So I did. I took the numbers from Syria
and I scaled them up so you could see what it would be like if it happened in the U.S.
A hundred million homeless, a hundred million people displaced, had to flee violence,
whatever. Three million dead. That's crazy. When we think about a tragedy in the U.S.,
we think about 9-11 or Katrina. Katrina, 1,800 people died. Three million.
See, we're Americans, we have no idea how to put that in scale. We don't know what that means.
So we'll do it this way. That's more people than U.S. soldiers died in the war. What war? All of them.
To include both sides of the Civil War. Every war in U.S. history. All added up. Times two.
It's more than that. But yes, Dwayne from
Best Buy, you'd be a survivor. You'd stay
and you'd be able to navigate the
militias and the government forces and
the ever-shifting alliances. You'd be able to
find food, water, shelter for your family.
You'd do all of that survive combat
against battle-hardened men who've been
fighting for 10 years in Iraq. You'd
survive and fix your country because
Because you're a super soldier, Dwayne from Best Buy.
As some of you may have picked up, I'm a pretty resourceful guy and I am better trained
than most.
I feel no shame when I say that if what happened in Syria, happened in the U.S., the crew
of the International Space Station will be able to track my progress by watching the
plumes of smoke as I burned rubber all the way to Canada.
trained people, you do one thing really well in combat. Die. That's it. And while
we're on that subject, one other thing with all you Internet tough guys out
there talking about what you would do. One gentleman said that he would open
fire, and the context was live ammunition. You would open fire on unarmed men,
women, and kids. Oh, you tough. You are tough, man. I know killers, real killers. I
I don't know any that open fire on an unarmed crowd.
You know, if through some twist of fate I end up down there guarding the border or something
because I'm forced to, I hope you're with me because it's hot in the desert and your
canteen still be cool because you got ice in your veins.
Tough guy.
You know who is impressed with statements like that?
People who have never fired a shot at someone.
who have never watched someone bleed out does not make you look tough makes you
look like an idiot anyway just a thought just a few actually y'all have a good
night and please forgive me for trouble in the comment section yesterday I do
this for free I want to have a little bit of fun sorry all right y'all have a
Good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}