---
title: Let's talk about police militarization....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LB3HUXdmid4) |
| Published | 2018/11/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Police militarization is a critical issue that affects communities, leading to concerns about the "warrior cop" mentality.
- The misconception that being a warrior means being a killer is problematic and has resulted in a focus on militarizing law enforcement.
- The shift towards viewing law enforcement as strictly enforcing the law, rather than protecting the public, raises moral questions for officers.
- Enforcing unjust laws, such as those criminalizing feeding the homeless, can make a cop complicit in wrongdoing.
- Propaganda within law enforcement, including inflated statistics on officer deaths, contributes to a heightened sense of danger and leads to hasty decision-making.
- The belief in a "war on cops" is not grounded in reality, yet it influences the desire for militarization and warrior-like behavior among law enforcement.
- The use of military equipment like flashbangs and MRAPs by SWAT teams is excessive and often lacks proper training, leading to dangerous outcomes.
- Lack of intelligence work before operations results in SWAT teams making deadly mistakes, as seen in cases of wrong addresses and unnecessary force.
- The discrepancy between perceived and actual capabilities of SWAT teams can have lethal consequences when faced with real resistance.
- Beau advocates for a reevaluation of law enforcement's role as public servants and protectors rather than warriors.

### Quotes

- "If you're a cop in an area where feeding the homeless is illegal, you're a bad cop."
- "Being a warrior, everybody in the military is a warrior."
- "They've bought into their own propaganda."
- "There is no war on cops."
- "You're the weapon if you're a warrior."

### Oneliner

Beau dismantles the "warrior cop" mentality, exposing the dangers of police militarization and the consequences of prioritizing enforcement over public protection.

### Audience

Law enforcement reform advocates

### On-the-ground actions from transcript

- Reassess law enforcement's role as public servants and protectors rather than warriors (implied)
- Advocate for proper training and accountability in law enforcement (implied)
- Support efforts to demilitarize police forces and redirect resources towards community policing (implied)

### Whats missing in summary

A deeper understanding of the implications of police militarization and the need for a fundamental shift in law enforcement culture.

### Tags

#PoliceMilitarization #LawEnforcement #CommunityPolicing #WarriorCop #Accountability


## Transcript
Well, howdy there internet people, it's Beau again.
So, I mentioned police militarization in that last video and man, my inbox blew up
with people asking me to talk about it, so here we are.
This video might get a little long, something I've thought about a little bit.
The problem starts with the idea of the warrior cop, okay?
And by that, they want to mimic the military.
what they think warrior means is killer. See, most of the military isn't killers.
Not half. Not a third. Not a quarter. Not ten percent. Four percent. Four percent of
the military are killers. You want to look into this topic. There's a book
called On Killing by Lieutenant Colonel Grossman. Highly recommend it. So they've
have equated killing with being a warrior. If you've done that, you're probably neither,
to be honest. A warrior, everybody in the military is a warrior. And I'll tell you
right now, those guys in civil affairs or the CBs, they've kept more bad guys off the
battlefield than any trigger puller. Those irrigation ditches for farmers, the bridges,
schools, that infrastructure, that keeps people off the battlefield, gives them
options. That's funny because we understand this during wartime. The
military gets it, but here at home, well, Flint's water will fix itself. We don't
really need to worry about education. It's not like crime and poverty and a
lack of options go hand-in-hand or anything.
But anyway, so they're warrior cops, and what they mean is they're killers.
And they want the toys to go with that.
So that's one issue.
The other issue that's happening at the same time is that they're law enforcement now.
They're law enforcement.
Your job is to enforce the law, not protect the public and that's changed.
Now what that means though, if you're no longer a public servant, your job is strictly to
enforce the law, means you're personally responsible on a moral level for every law in the books.
If you don't agree with it and you're willing to enforce it, well then you're just a hired
goon.
If you're a cop in an area where feeding the homeless is illegal, you're a bad cop.
Period.
Full stop.
If you're willing to enforce that law, you're a bad cop.
I know that's going to be hard for a lot of people to swallow, but it's the truth.
If your interest is no longer protecting the public and it's only enforcing the law, you're
You're just accepting orders and taking a paycheck to do it using violence.
You're a hired goon.
So you have those two issues that have shifted.
And then you have the propaganda that they've put out to justify them being warriors.
And I say propaganda because most of it's not true and we're going to go through it
right now.
You know, you hear about X number of law enforcement killed every year, and it's normally more
than a hundred.
And they get those numbers from the Officer Down Memorial page.
Go take a look and look at the cause of death.
When you think killed in the line of duty, man, you're thinking bullets, not cheeseburgers.
Cop dies of a heart attack on shift, well, he was killed in the line of duty.
They do anything they can to inflate those numbers.
Now the problem with that is, I mean, if you're going to run propaganda, fine, but don't believe
it yourself, and that's what's happened.
They've convinced themselves now that everybody's out to get them and they're in a hyper dangerous
job.
And because of that, they're real quick to pull a trigger, real quick.
And that leads to mistakes and unjustified killings.
I was part of the team of journalists that ripped apart that study by, I think it was
Washington Times, it might have been the Washington Post a couple years ago.
The idea behind the study was to dissect unarmed killings and find out how many they were.
Problem is they intentionally skewed the statistics because they counted anybody that the police
said they thought they were armed as armed.
So if you're holding a cell phone and a cop killed you,
and he said, well, I thought it was a gun,
you're armed in that study.
When you're not trying to skew the statistics, one out of 10,
one out of 10 people were killed while unarmed.
That's pretty high.
That is pretty high.
Now, I know there's some people saying, well, that's 10%.
That's not really that bad, is it?
Let's put it in another perspective.
Let's say you order a pizza once a week.
And five times a year, guy brings you the wrong order.
You're probably going to stop ordering from there.
You hold a pizza delivery driver more accountable
for the accuracy of your order than you do a cop
for the accuracy of his bullets.
But that's fitting, because being a pizza delivery driver
actually more dangerous than being a cop. Not a joke. Bureau of Labor Department
statistics. Being a cop, not really that dangerous. Not a lot of deaths really.
It's not even in the top 10. Loggers, fishermen, roofers, delivery drivers, all
have more dangerous jobs. But they don't know that. They've bought into their own
propaganda. So you hear things like, well, I want to go home at the end of my shift.
Why don't you do that and stay there? Your duty is not bounded by risk, my friend. We
know you want to go home at the end of your shift. Everybody does. That's completely understandable.
The unarmed people that were killed, they wanted to go home too.
You chose this profession.
They probably didn't choose to interact with you.
So you have this issue.
They believe, they truly believe that there's a war on cops.
There's not.
But they believe there is.
And they want to be warrior cops, and by that they mean killers.
So what do you need?
Well you need warrior toys, right?
You need flashbangs and suppressed MP5s.
You need MRAPs.
That SWAT team.
Now SWAT team's supposed to be a life-saving tool.
Really is.
It's why it was designed.
It's also supposed to be rarely used.
It's in the name.
That first letter, S, special.
It's not supposed to be used for every penny anti-drug dealer so you can look cool
in your Oakleys so you can be an operator. You know and it's funny with
those drug raids now a whole lot of them you're starting to see it in the
newspaper no drugs were found of course not you pulled up in an MRAP you ever
stood outside the thing when it was running you can hear it a mile away as
soon as you turn the corner they were flushing it. Now I personally don't have
a problem with that one because I don't really agree with Prohibition anyway but
That's neither here nor there. Let's talk about the flashbangs.
These are low-grade concussion devices
what they are. They call them distraction devices because they want to make it sound like it's a firecracker.
But these things can maim and they do
especially when they're misused and used by people that don't have training
and that's what happens. They get these toys but they don't get the training to
go with it.
So they throw them through windows into cribs or
They throw them in before a raid, they haven't hit the place with thermal, they have no idea where the people inside
are,
and that leads to problems too.
Or, yet another example, saw a video, they hit the guy with a concussion grenade, or with a flashbang,
and then they start giving him commands.
A lot of light, a lot of sound, blinds, deafens, and disorients.
He can't hear them.
He couldn't comply if he wanted to.
And we know what happens if you don't comply with a cop, right?
Comply or die.
That's what happened to him.
They killed him.
They got the toys.
They don't have the training.
And then when you look at the SWAT teams themselves, you know, almost every jurisdiction has one
now.
And when you look at them, when they're doing their training and their little promo videos,
And they look good, most of them got some guy from some high speed army unit to come
out and train them, and they're doing it man, they got it.
They're clearing the rooms, they're cutting the pie, they got it.
Of course, if you want to be an operator and you want to mimic those high speed teams,
do it.
You know how much intelligence work goes into one of those operations before one of those
shooters ever puts his foot on a door? Not a whole lot of times when you're
gonna see one of those units kicking a door on the wrong address or at the right
address but the guy that lived there moved two years ago because they don't
get their target location out of DMV records. I've known some criminals in my
day. Updating their driver's license address? Not really a high priority, guys.
Y'all need to knock that off. So there's that. They have the toys, they
don't have the training, but they think they do. And that leads to a discrepancy
between perceived capability and actual capability. And then that causes more
problems, because the perceived capability, when you say SWAT team, those
guys, they're operators, right? They got their Oakleys, they know what
they're doing. In real life, almost any time one of these small jurisdictions
with their SWAT team encounters real resistance, they get greased because they
ran into somebody with actual capability. It was like that case out there in
Texas. It encompasses almost all of this. Swat team kicks in the door, throws in a
flashbang, then they move in. One guy stops the whole team, kills three of
them. They were there to get his nephew, who wasn't home, was not there. And then
Because they did it the way they did and he was deafened, he couldn't hear him
identify themselves. He was found not guilty. Killed three of them. That's gonna
happen more and more because they're focusing their training on killing, not
being a warrior. There's a whole lot more to being an operator than just poking
holes in paper.
You want to be that, you're going to have to up your training a whole lot.
But there's no real need for it.
Again, police violence, violence against police, it's not a dangerous job.
There is no war on cops.
There should be no warrior cops.
Anyway, it's just a thought.
I could go on for hours on this topic, but I'm going to cut it short.
You guys really, law enforcement needs to rethink this.
The more they step away from their role as public servants and from their role as protectors
of the public and more into, we're just going to enforce whatever law we're told and we're
going to use these tools that we have, these weapons to do it.
There's going to be more and more fatalities.
For the record, guys, that M4, that MP5, that's a tool.
You're the weapon if you're a warrior.
And then when we're back to that topic of I want to go home at the end of my shift,
you want to be a warrior, maybe somebody in the comment section will acquaint you with
what the way of the warrior really is.
Anyway, y'all have a nice night.
the ball.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}