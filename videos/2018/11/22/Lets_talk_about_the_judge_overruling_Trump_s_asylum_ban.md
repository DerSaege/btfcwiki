---
title: Let's talk about the judge overruling Trump's asylum ban....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PLIujWl-Wvk) |
| Published | 2018/11/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A judge overruled Trump's asylum ban, revealing the legality of seeking asylum by crossing the border without permission.
- News outlets labeled asylum seekers as "illegals" and the situation as an invasion, intentionally misleading the public.
- Seeking asylum by crossing the border is lawful, legal, and morally sound.
- Asylum is meant to provide people with the ability to seek safety.
- Trump tried to change the law through a proclamation, attempting to act like a dictator.
- Trump's actions were a direct challenge to the Constitution and an attempt to dictate law.
- The judicial branch intervened to stop Trump's attempt, while Congress failed to act.
- Trump's defenders may claim patriotism but failed to uphold the Constitution.
- Trump's actions undermined Congressional authority and the foundation of the Constitution.
- Blind partisanship has led to a disregard for the Constitution and the principles it upholds.

### Quotes

- "Crossing the border while seeking asylum in the middle of the desert and saying, hey, I'm claiming asylum. lawful, completely legal, completely moral."
- "If it's your party, they can set fire to the Constitution and you'll roast a marshmallow over it."
- "The three branches of government, that is the basis of the United States Constitution."

### Oneliner

A judge overruled Trump's asylum ban, exposing his attempt to act like a dictator by challenging the Constitution.

### Audience

US citizens

### On-the-ground actions from transcript

- Stand up for the Constitution and demand accountability from elected officials (implied).
- Educate others on the importance of upholding the Constitution and the separation of powers (implied).

### Whats missing in summary

The full transcript provides in-depth analysis on the legality of seeking asylum, the implications of Trump's actions, and the importance of defending the Constitution.

### Tags

#Trump #AsylumBan #Constitution #Dictatorship #JudicialBranch


## Transcript
Well, howdy there, internet people, it's Beau again.
So we're gonna talk about the judge overruling
Trump's asylum ban, and it's really important.
There are a couple of components to this.
The first is, you know, most people watch these videos,
you guys already knew this,
but a large portion of the country found out today
that it is completely lawful to cross the border
without permission and claim asylum.
They don't know that and they thought otherwise because their news outlets that they choose
to watch called them illegals and called it an invasion and the President of the United
States intentionally misled the public.
They believed that this was illegal.
It's not.
Crossing the border while seeking asylum in the middle of the desert and saying, hey,
I'm claiming asylum.
lawful, completely legal, completely moral more importantly. The purpose of
asylum is to give people the ability to do just that. So there is that. There's a
whole bunch of people who should realize, most won't, but should realize that the
news service that they use intentionally misled them for the sake of
a partisan politics. Now, there's a much more important and much more significant
turn in this whole situation. See, this law, it was sent through the House of
Representatives and the Senate and then signed by a president into law. It went
through the system as laid out by the United States Constitution. Trump
Trump attempted to change that law by dictating a change in law.
There's a word for people who do that.
It's called dictator.
That is what he attempted to do.
Make no mistake about it.
This was an attempt to dictate law, issuing a proclamation that directly challenged a
standing law and basically said, well, it's not valid anymore.
direct defiance of the United States Constitution and how this government is supposed to work.
This was an attempt to dictate law, was an attempt to be a dictator.
It's not hyperbole, it's what he did.
This isn't a case of a president abusing the power of the executive order.
You know, this isn't slightly expanding or narrowing the scope of something or changing
a policy and in the end result is memos get changed.
This was an attempt to say this was legal and now it's not.
That's what he did.
That is a dictatorship.
The judicial branch did its job.
They stepped in and they stopped it as they should have.
Congress has not done its job.
This man is attempting to undermine the Constitution of the United States.
To me, what he did is appalling.
The funny thing is his defenders that will certainly show up in the comments section
will probably have photos of the Constitution or an American eagle or one of those stupid
red hats as their profile picture.
If you weren't appalled by this and want this man out of office because of it, you
don't care about the Constitution.
You don't.
The three branches of government, that is the basis of the United States Constitution.
Everything else doesn't really matter.
That's the basis, and he attempted to undermine it, attempted to circumvent it.
It was a direct challenge to Congressional authority.
It was a direct challenge to the Constitution of the United States, and people who fashioned
themselves patriots are mad at the judge who stopped him from betraying the Constitution.
That's how far this country has gone.
That's how red and blue, elephants and donkeys, that's how far it's gone.
If it's your party, they can set fire to the Constitution and you'll roast a marshmallow
over it and say well this is the way it's meant to be no it's not it's
treason. Anyway, you guys have a good day. Enjoy your holiday. It's just a thought.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}