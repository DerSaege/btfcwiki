---
title: Let's talk about how the troops on the border can defend American freedom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kWWouKv51zY) |
| Published | 2018/11/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts a story from the past about a general ordering a lieutenant to take a hill crawling with enemies, resulting in success without casualties.
- Explains the process of vague orders becoming specific on the ground and the importance of implementation.
- Talks about the unique chance for soldiers at the border to defend American freedom by upholding specific laws and rules of engagement.
- Warns against mission creep, using Afghanistan as an example, and the illegal request for U.S. soldiers to act as emergency law enforcement personnel.
- Emphasizes the need to stay within the law, especially regarding non-combatants, and the potential consequences for enlisted soldiers if rules are broken.
- Advises soldiers to keep their weapons safe and slung, not getting involved in illegal activities.
- Encourages soldiers to know the laws of war, the real rules of engagement, and not to follow vague or illegal orders.
- Urges those at the border or heading there to learn how to legally apply for asylum in the United States.
- Criticizes the political use of soldiers by higher-ups, warning about the misuse of trust and honor for political gains.
- Suggests requesting clarification on vague orders, following the law, and being prepared for a potential need for defense against illegal actions.

### Quotes

- "Order comes down, it's vague. Gets more specific until it gets to those guys on the ground. And they've got to figure out how to implement it."
- "You want to defend American freedom, you keep that weapon slung and you keep it on safe."
- "Honor, integrity, those things. It's bred into you, right?"
- "You wouldn't circumvent the law, earn that trust, keep that weapon slung and keep it on safe."
- "When that vague order comes down, you ask for clarification. Email. Clarification."

### Oneliner

A reminder to soldiers: implement vague orders wisely, defend American freedom by upholding laws, and beware of political misuse of your honor and uniform.

### Audience

Soldiers, Military Personnel

### On-the-ground actions from transcript

- Learn how to legally apply for asylum in the United States (suggested)
- Request clarification on vague orders and follow legal guidelines (implied)
- Keep weapons safe and slung, avoiding illegal activities (implied)

### Whats missing in summary

Importance of understanding and upholding legalities and regulations in military actions.

### Tags

#Soldiers #Military #DefendFreedom #LegalCompliance #AsylumApplications


## Transcript
Well howdy there internet people, it's Beau again.
I'm gonna start off tonight with an anecdote.
This story, it gets retold during every war
to make it seem like it happened in the war just before.
Now I'm sure if you went back to World War II,
there'd be guys swearing this happened during World War I.
The version I heard was in 1968 during the Tet Offensive,
there's a general and he sees this hill,
he's got, he has to have this hill.
The problem is it is crawling with VC and NVA, and they're looking at 50 to 60% US casualties
to take that hill.
But he's got a hat, so he calls down to battalion.
He says, I need that hill.
Battalion calls down to company.
You guys have to take this hill.
Company commander's like, 50 to 60% casualties.
But he's got him a new lieutenant, a bunch of green guys he don't really know.
If you're going to lose somebody, who are you going to pit?
Lieutenant, get over here.
You got to take that hill.
Lieutenant scurries off.
20 minutes later, they got the hill.
Didn't lose a man.
Nobody's even wounded.
Company commander's like, Matt, I look great.
Battalion commander, sir, we got it.
General's impressed.
So he hops in his Jeep.
He wants to meet this guy.
He finds that lieutenant.
He's like, son, how did you do that?
Lieutenant Luke said to him.
I said, Sergeant, take that hill.
And that's how it works.
Order comes down, it's vague.
Gets more specific until it gets to those guys on the ground.
And they've got to figure out how to implement it.
That's how it really works.
And that's an important little tidbit here in a minute.
Because we all know what's going on down at the border.
It doesn't matter how you feel about it.
We know what's going on, and it doesn't matter how you feel about it because those
war fighters have a very unique opportunity.
They have the chance to join an extremely exclusive club.
They can join those very few people that actually picked up a weapon and truly defended American
freedom.
I know what you're saying, all of that's defended American freedom, no, not really.
may have defended the American way of life, that may have acted as a deterrent, so other
countries didn't think about impinging on American freedom.
Very very few actually defended American freedom.
The last time it really happened, the last time we were truly at risk of invasion was
World War II, so 1945.
Prior to that, I would say the Union soldiers were fighting for the freedoms of Americans,
1865, prior to that, War of 1812, and then the Revolutionary War, four times in
American history, was truly about freedom in America. Those people on the border
have the opportunity to join that club. See, we all know what Mission Creep is.
Well, if you don't, Afghanistan is a perfect example. Why did we go into
Afghanistan. Well, to get those responsible for 9-11, of course. That's what we're told.
And then to fight the Taliban, because they kind of helped them, sort of, maybe.
Then to stabilize the country. Then to kind of protect the cash crop and help out the
the pharmaceutical industry a little bit. And then to bring the Taliban back into the
fold now, it's more than a decade and a half later, we're still there, still dying, nobody
knows why. That's mission creep. Now when DHS, when the Department of Homeland Security
called up DOD, they wanted U.S. soldiers to act as emergency law enforcement personnel.
The DOD rightfully told them to go somewhere else with that request.
It's illegal.
There are very, very specific instances where U.S. soldiers can operate domestically.
This isn't one of them.
But now you're heading down there.
I know what you're saying.
Well, we're doing surveillance, support, logistics, supply, okay?
Mission creep.
Why do they really want you there?
That vague order will come down, take that hill, only it's going to be stop them.
When that happens, who's going to implement it?
Lieutenant and Sergeant, right?
That's what's going to happen.
And when this administration leaves office, who's going to be prosecuted?
Think it's going to be some dude with stars on his shoulder?
No.
It's not going to be the guy that gave the vague order.
It's going to be the sergeant shoved his M4 in some kid's face.
You do not know the meaning of the word blue falcon, the term blue falcon, until you're
You're talking about a senior commander trying to cover his rear and the collateral damage
of a bunch of enlisted guys.
You want to defend American freedom, you want to defend the freedoms of Americans, you keep
that weapon slung and you keep it on safe.
Stay within the law.
You know what the laws of war are.
know what the UCMJ is. You know the real rules of engagement. Doesn't matter what
political rhetoric is said. You know what the real rules of engagement are. These
are non-combatants. You better remember that. You better act like it. You keep
that weapon on safe and you keep it slung. Don't get involved in this. It will
be you standing tall when it's over. See, here's the thing, and the one thing I
want to recommend to everybody, if you're already down there, if you're heading
down there, head over to the USCIS website and you figure out exactly how
to legally apply for asylum in the United States. What you're gonna find out,
what these people are doing, what these migrants, what this caravan is doing, is
completely legal. That's what you're going to find out. There is no law saying
they have to apply for asylum in the first country they come to. There is no
way for them to apply at an embassy. They have to be physically present in the
United States or at a port of entry. So what does that mean?
Republicans control the legislative and executive branch.
They could change the law if they wanted.
So what does it mean?
It means they don't want to.
They want to use you.
See, the American people trust you.
They trust that uniform.
Honor, integrity, those things.
It's bred into you, right?
You wouldn't do anything illegal.
You wouldn't circumvent the law, earn that trust, keep that weapon slung and keep it
on safe.
And if you think for a split second that the brass is going to have your back when this
is over, and they're going to say, oh yeah, we gave that order, first, following vague
orders, following illegal orders, it's not a defense.
If they were going to have your back, you wouldn't be down there.
They would have stopped this before it started.
The briefing slides, they got leaked.
It's not a threat.
We know it.
DOD says it's not a threat, but you're still down there.
Why?
Because the brass decided to allow you to be used as a political tool.
before the midterm elections and that's what this is about and it's your uniform,
your honor being used to legitimize them circumventing the law. That's what's
going on. It's just a thought but you really need to keep that weapon slung.
and keep it on safe. Don't do it. When that vague order comes down, you ask for
clarification. Email. Clarification. Something that's going to remain, something
you can use in your defense later. Because if you allow that mission creep
to happen, you're going to need it. You're going to need a defense. It would be better
to follow the law. Just an idea, just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}