---
title: Let's talk about a Kennedy legend you've probably never heard and why it's important today...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9Q2S1hLWmo4) |
| Published | 2018/11/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tells the legend of President Kennedy meeting with selected Green Berets in 1961, making them take an oath to fight back against a tyrannical government.
- Mentions the secrecy and lack of evidence surrounding the oath and the Green Berets' activities.
- Talks about the involvement of Green Berets in Kennedy's honor guard after his assassination.
- Raises questions about the existence of a Special Forces Underground and its investigation by the Department of Defense.
- Points out the annual visit by active-duty Green Berets to Kennedy's grave site for a brief ceremony.
- Connects Kennedy's criteria for fighting tyranny, including the suspension of habeas corpus, to current political considerations.
- Criticizes the idea of suspending habeas corpus as a threat to the rule of law and national security.
- Challenges the argument for suspending habeas corpus in dealing with migrants and asylum seekers.
- Urges people to understand the significance of habeas corpus and its potential removal from legal protections.
- Raises concerns about the erosion of the rule of law and the danger of unchecked presidential powers.

### Quotes

- "The suspension of habeas corpus is though, that is a threat to national security. It's a threat to your very way of life."
- "The suspension of habeas corpus is something that can't be tolerated."
- "The suspension of habeas corpus is an affront to everything that this country has ever stood for."
- "You need to look it up. You need to realize exactly what it means for that to be gone."
- "You can watch this country turn into another two-bit dictatorship or you can kill this party politics that you're playing."

### Oneliner

Beau raises concerns about the suspension of habeas corpus, linking it to a legend about Green Berets and urging people to understand its significance in preserving the rule of law and national security.

### Audience

Americans, Activists, Citizens

### On-the-ground actions from transcript

- Educate yourself and others on the significance of habeas corpus and its importance in upholding the rule of law (implied).
- Advocate for the protection of legal rights and civil liberties in your community and beyond (implied).
- Stay informed about political decisions that could impact fundamental rights and freedoms (implied).

### Whats missing in summary

The emotional weight and urgency conveyed by Beau regarding the potential threat of suspending habeas corpus and the need for active civic engagement and awareness to safeguard democratic principles.

### Tags

#HabeasCorpus #GreenBerets #CivilRights #PoliticalActivism #NationalSecurity #Democracy


## Transcript
Well howdy there internet people, it's Bo again.
So tonight I'm going to tell you a legend,
a conspiracy that has lasted decades.
You're going to get to hear Kennedy conspiracy theory
you've probably never heard before.
So the legend says that in 1961, in fall of 1961,
President Kennedy met in private with selected Green Berets, men
that he had personally chosen.
And he made him take an oath.
And the oath was that if the US government ever
became tyrannical, and he set out criteria,
that they would use their skills to help Americans fight back
against a tyrannical government.
And that they were to take this oath and pass it
on to other Green Berets so it would stay alive
and stay within the institutional memory.
That's cool.
That is a cool story, kind of heartwarming.
Inside the Army's most elite unit,
there is a secret organization dedicated to keeping you free from tyranny,
to liberate the oppressed.
It's kind of cool.
Now, is there any evidence to support this theory?
Well, October of 1961 is certainly fall of 1961,
And President Kennedy did, in fact, travel to Fort Bragg.
Fort Bragg happens to be home of the Green Berets.
That's public record, part of his travel records.
He did, in fact, meet with a list of men
that he had personally chosen in private.
Nobody knows what was said, though.
Two years later, Kennedy's assassinated, November 22.
46 Green Berets depart Fort Bragg and head to DC.
Twenty-one of them serve as Kennedy's honor guard.
Now that's weird.
No president before or after has had Green Berets as his
honor guard.
That is bizarre.
The other 25, we have no idea what they did.
Their orders aren't public.
That's pretty common if you're doing something sneaky.
You send a unit somewhere, half of them go do something very
public.
So everybody knows that's really why they're there.
The other half is off doing something sneaky.
Now according to the legend, they were with Jackie.
That's all it says.
Nobody really knows what they were doing.
We don't even know if that happened.
Now the honor guard part, there's a little bit of a
legend there too.
It says that the honor guard stayed with Kennedy's casket
after everybody left.
And a fellow named Command Sergeant Major Ruddy removed
his green beret and set it on the casket.
Another version of the story says all the other guys
followed suit and they put their green berets there too.
We don't know if that happened.
It was just them there, according to legend.
But do you know where Command Sergeant Major Barretti's beret is today?
JFK Presidential Library on display.
In fact, it is the only permanent military display in the entire facility, even though
Kennedy himself was in the Navy.
It's weird.
It is weird, isn't it?
You don't hear anything about it, nothing really surfaces, nothing becomes public, for
a couple of decades, and then some clown will run in some racist newsletter, leaks word
of something called the Special Forces Underground, and that sounds spooky, right?
DOD thought so too.
So they investigate it.
They send a team down to investigate, send a team up to brag, and well, what they found
out and what they released. You can't read the report, it's secret. But the blurb they
released was they've investigated it and it's not a racist organization. It's not
a racist organization. In order for them to determine whether or not it was racist, it
means it exists. It's kind of cool. Now, this guy that ran this newsletter, he was not a
Green Beret. Racism and the Green Berets do not mix. Even during segregation, the
Green Berets accepted black soldiers with open arms. There are a lot of
requirements to become a Green Beret. Skin tone isn't on the sheet. There's no
check, no box to check for that. But this guy wasn't a Green Beret. But he
taught at JFK. JFK, after Kennedy died, the Green Berets named their
training facility after him. So maybe he heard something. It's all kind of cool.
Then there is one other weird thing. To this day, a group of active-duty Green
Berets every year travels to Kennedy's grave site and a brief ceremony puts a
on it. No other president gets that kind of attention. Is it true? Maybe. Maybe it
is. Don't know. It's a cool story. It's a cool legend. So why am I telling you all
this? One of the criteria allegedly set out by Kennedy was the suspension of
habeas corpus. Something our current president is considering. It was one of
the criteria because the suspension of habeas corpus is the end of the rule of
law. That is what it is. There's no other way to put it. I know there's gonna be
some Trump supporters out there and they're gonna say well he's got to do
that to deal with the migrants. Really? If what they are doing is illegal and
And they are illegals, as he likes to say.
Why can't he just prosecute them?
I'll wait.
It's almost like what they're doing isn't illegal.
It's almost like that is exactly how you claim asylum,
and they are following the law.
And as his supporters like to contend,
If they don't have rights because they're not U.S. citizens,
why would he need to suspend habeas corpus?
It's because they do have rights, the second they set foot on American soil.
This migrant caravan, like those that have come for the last 15 years,
these asylum seekers, this isn't a national emergency.
The suspension of habeas corpus is though,
that is a threat to national security. It's a threat to your very way of life.
I want you to think back to everything that has happened over the last fifty years.
Are you telling me that a group of unarmed refugees
is more of a threat to national security than
the Soviet Union was during the Cold War, when you actually put this into perspective,
it's insane.
It's not silly.
It's insane.
It's crazy.
The suspension of habeas corpus is something that can't be tolerated, and the fact that
a sitting president refuses to remove that from the table of options, it's ludicrous.
is ludicrous. At this point you can support your country or you can support
your president. Those are your options. The suspension of habeas corpus is an
affront to everything that this country has ever stood for. The rule of law will
be erased completely. It's not hyperbole. There may be a lot of people out there
out there that don't really know what habeas corpus means. You need to look it up. You
need to look it up. You need to realize exactly what it means for that to be gone. You can
watch this country turn into another two-bit dictatorship or you can kill this party politics
that you're playing. This is dangerous. Anyway, it's just a legend, maybe. It's just a story.
It's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}