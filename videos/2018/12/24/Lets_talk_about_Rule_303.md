---
title: Let's talk about Rule 303....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HbGKzleLJVc) |
| Published | 2018/12/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the origins of "Rule 303" among military contractors, originating from a historical incident involving a soldier using his rifle caliber as justification for actions.
- Emphasizes that "Rule 303" meant having the means at hand and therefore the responsibility to act, not might makes right.
- Provides examples of how different situations have applied the concept of "Rule 303," including military interventions and law enforcement responses.
- Criticizes the excuses made by law enforcement officers who fail to act in critical situations, such as the Parkland shooting.
- Argues that duty is not bounded by risk and that officers have a responsibility to act, especially in protecting children.
- Challenges the notion that equipment superiority should deter action, stressing the importance of fulfilling one's duty regardless of perceived risks.
- Questions the message sent by prioritizing self-preservation over protecting those in need.
- Urges school resource officers and law enforcement personnel to reconsider their roles if they are not willing to lay down their lives to protect others.

### Quotes

- "Rule 303 meant having the means at hand and therefore the responsibility to act, not might makes right."
- "Your duty is not bounded by risk. Your duty is not to get up and go to work every day. The guy at Hardee's has that duty."
- "They care about the students. That's it."
- "The gun makes the man. Don't you think that might be reinforcing the thought process of those kids that go into schools and shoot the place up?"
- "If you cannot truly envision yourself laying down your life to protect those kids, you need to get a different assignment."

### Oneliner

Beau explains the concept of "Rule 303" and challenges law enforcement officers to fulfill their duty to protect without excuses or prioritizing self-preservation over others.

### Audience

Law enforcement officers

### On-the-ground actions from transcript

- Reassess your commitment to protecting others if you are in law enforcement and prioritize self-preservation over duty (implied).

### Whats missing in summary

The full transcript provides a detailed examination of the concept of "Rule 303," critiquing law enforcement responses to critical situations and urging officers to prioritize protecting others over self-preservation.

### Tags

#LawEnforcement #Responsibility #Duty #Protect #Rule303


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about rule 303
and military contractors all over the world just went oh god.
Don't worry, nobody's getting indicted.
Rule 303 was a very popular, is a very popular term
among a certain subset
of military contractors
because of the Iraq and Afghanistan wars, those contractors came into
contact with active duty military so the term made its way to there and then from
there it made its way into law enforcement. We need to talk about that.
We need to talk about rule 303 and what it means. The term originated with this
guy a very long time ago back when bolt-action rifles were a standard issue
for militaries all over the world and he's fighting an insurgency and some of
his men get ambushed and killed. Later on he finds some locals that have their
personal effects so he lines them up and shoots them. His commanders did not
look favorably on this action. He wound up in court standing tall and at one
point they asked him under what provision, what rule, did you feel you had the ability,
the authority or the responsibility to engage in what amounts to a summary execution.
And he said rule 303.
There is no rule 303.
It doesn't exist.
303 was the caliber of his rifle.
Now what he was saying was, I have the means at hand and therefore the responsibility to
act.
That's what he was saying.
Now today, as it has made its way through active duty and then the law enforcement,
it has kind of turned into might makes right.
That is not what that term meant.
He felt he had the duty to act.
felt he had that duty. Now anytime rule 303 is cited, the action is pretty much
always debatable as to whether or not what was done was right. In this case, we
now know, because of how insurgencies work, this was a really bad idea. This was
something bad to do. If these people were just random people who had found the
bodies and then looted them, he killed innocent people. And that would help the
insurgency grow. Their family members would probably take up arms. The theory
at the time was a little different. He was trying to send a message. We know now
that's not how you send it. Things have changed. Some more recent examples of
of rule 303 and one that everybody will know was there was an ambush that hit a
bunch of green berets in Western Africa. Everybody knows about it because Trump
dropped the ball when he was making the phone calls. So these guys they get
ambushed they got no air support. They're done. They are done. All of a sudden two
French Mirage 2000 jets fly in. They don't engage the enemy but they scare
them. They engage in a show of force and it worked. They didn't have the duty to
intervene but they had the means at hand and therefore the responsibility. Now
meanwhile there's a bunch of contractors working for Berry Aviation sitting in
their helicopters. They didn't have the means or they didn't have the duty, but
they had the means at hand and therefore the responsibility. Now I think the
official story is that some French helicopters came in and got them and that
may be what happened. I'm not sure, but anyway. Another example that people may
be familiar with, you may have saw the movie Tears of the Sun. It's about a
military team, they go in to get out a doctor. When they get there, they find a
bunch of refugees that are going to get slaughtered. So they make the attempt to
lead them to safety. They had no duty to do that, but they had the means at hand
and therefore the responsibility. And that's a highly fictionalized account of
something that actually happened, by the way. So, that's where the term comes from.
Now how does that relate to that Parkland shooting and the ruling that says law enforcement
has no duty to protect anybody? The officer that waited outside had no duty to enter.
Well, now that I'm starting to see that patch on law enforcement, I guess we should talk
about that, right?
Now there were two types of cops that sent messages or made comments after I mentioned
that ruling.
One type wanted to talk about the philosophical aspects of it, and we're going to talk about
that, but the tone of this video is not directed at you guys.
I want you all to know that.
There is a philosophical discussion to be had there, and it doesn't have to be confrontational.
Then there was the other batch of cops that wanted to offer excuses, and we're going
to address those excuses too.
The philosophical aspect is that if you create a legal obligation to act, you remove that
officer's ability to engage in self-preservation and assess risk.
Yeah, yeah that's what it means. That is exactly what it means.
Welcome to the profession of arms. Yeah that's what it means. Nobody cares if you
go home at the end of your shift. In that situation nobody cares. Your duty, your
responsibility, because you have the means at hand, is to act to protect those
kids. Your duty is not bounded by risk. Your duty is not to get up and go to
work every day. The guy at Hardee's has that duty. You chose a different
profession. So yeah, you shouldn't have the ability to assess risk and
determine whether or not you're gonna act. Rule 303, you have the means at hand,
you have the responsibility to act. Okay, let's go to the excuses. He wasn't paid
enough. That man made about a hundred K a year, a hundred thousand a year. What does
an E2 or E3 in Iraq make? About a fifth of that. Yeah, that argument, that dies
right there. The assignment was taken. You don't get to renegotiate salary once
you start getting shot at or somebody else starts getting shot at. He had to
make a split-second decision. No, he did not. No, he did not. Every time he heard a
a gunshot, and there were a lot of them, he had to make a decision. He never found his
courage. He never found the courage to enter. Now, I don't think he should be called a
coward for that simply because, as we've talked about in other videos, most people
wouldn't have entered. Four percent of the population are real killers. In this situation,
you could safely bump the percentage of people that would act up to 10 or 15 percent because
it's kids. People are more likely to go help kids, but still the majority of
people would not have entered despite what they may say. But he's not, he
was not the average person. He was the guy that were gone to that school every
day, ostensibly to protect those kids. When did school resource officers become
a thing and end up in every school after Columbine. It was a response to that. The
implied duty is that you are there to protect the kids. Nobody really wants you
there to pepper spray kids they get into a fight or to tase the autistic kid
that's having a disciplinary issue. Teachers and principals and coaches were
able to deal with that for a very long time without going to a gun belt. Not
really what you're there for. Then, my favorite excuse, well he only had a
pistol and the assailant had a rifle. Your duty is not bounded by risk and that
argument may work on the public. Hit mine because when they look at it they see
a rifle, a big gun and a pistol, a little gun. What caliber is in the MP5? Nine
millimeter, right? The weapon, the MP5, used by most hostage rescue teams who
encounter rifles all the time. They specifically chose a pistol caliber
weapon. Yeah, the rifle's more powerful so it can achieve a greater range. It can
shoot out to 300 yards. Great. Combat's taking place at 7. It doesn't matter. Both
of those weapons have the ability to kill at the range that combat was
taking place at. It's not gonna work with anybody that actually knows about this
stuff. It's a great it's a great propaganda piece for the public. It doesn't
fly. More importantly I want to talk about the message you're sending by
putting that out there. The gun makes the man, right? That's what you're saying.
Don't you think that might be reinforcing the thought process of those kids that
that go into schools and shoot the place up?
The gun makes the man.
That was a really poorly thought out excuse.
Here's the thing.
You've got to make a decision if you're a school resource officer.
If you are a cop in a school, you need to make the decision.
Are you expendable?
If not, quit.
Because nobody cares if you go home at the end of your shift after a school shooting.
They care about the students.
That's it.
You chose to join the profession of arms.
The likelihood of dying from a bullet, that's part of it.
That is part of it.
And you chose the one assignment in law enforcement where literally nobody cares about your life.
You know, most of them don't want to say that and yeah, they would prefer that you survive, but they don't care
Doesn't matter
So, you still want to wear that patch? Look
Look, this is something that despite my current demeanor,
you need to think about seriously.
If you cannot truly envision yourself
laying down your life to protect those kids,
you need to get a different assignment.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}