---
title: Let's talk about two psychological effects and first aid kits....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1lfWrmYPonc) |
| Published | 2018/12/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of psychological effects and first aid kits that could potentially save lives.
- Contrasts different levels of knowledge and confidence in handling first aid kits between himself, his son, and his wife.
- Explains the Dunning-Kruger effect in the context of expertise and confidence levels.
- Talks about the bystander effect and its impact on people's willingness to act in emergencies.
- Advocates for owning a first aid kit to combat the bystander effect and potentially save lives.
- Provides advice on what kind of first aid kit not to get and recommends a company named Bear Paw Tac Med for quality kits.
- Describes the contents and uses of different types of first aid kits offered by Bear Paw Tac Med.
- Suggests the importance of carrying tools you understand the theory of, even if you may not know how to use them practically.
- Addresses common questions about emergency medicine, including the safety of veterinary medications for humans.
- Emphasizes the importance of taking action in emergency situations rather than assuming someone else will help.

### Quotes

- "If you're walking down the street and you have a heart attack, you better hope there's only one other person on the street."
- "Sometimes, the actual expert is less confident in their knowledge than somebody that knows nothing."
- "Make sure that somebody else is helping before you just walk on by."

### Oneliner

Beau breaks down psychological effects, expert confidence, and the bystander effect in emergency situations, urging action and preparedness with quality first aid kits to potentially save lives.

### Audience

Health-conscious individuals

### On-the-ground actions from transcript

- Order a quality first aid kit from Bear Paw Tac Med (suggested)
- Educate yourself on emergency procedures and first aid techniques (implied)
- Familiarize yourself with the contents of your first aid kit (implied)

### Whats missing in summary

The detailed examples and scenarios presented by Beau to illustrate the psychological effects and the importance of being prepared with quality first aid kits. 

### Tags

#FirstAid #EmergencyPreparedness #Expertise #BystanderEffect #CommunityAid


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight, we're gonna talk about
a couple of psychological effects and first aid kits.
And this may be one of the few videos
that may actually save a life
just by watching it or sharing it.
Tall claim, right?
We'll get to that.
We're gonna talk about first aid kits
and use those to demonstrate some things
because I keep getting asked by people
what kind of first aid kit should I get.
Okay, so this is my son's first aid kit.
He's got basic first aid training,
a little bit of knowledge,
but he is supremely confident in that knowledge.
It certainly seems like he believes any kind of injury
can be solved with direct pressure,
which is a little scary, to be honest.
But a little bit of knowledge
and very, very confident in that knowledge.
And this is my kit.
I'm a wilderness first responder.
Well, I was.
My certification has actually lapsed.
It's a little bit more robust, but not much,
because that's kind of the point
of the Wilderness First Responder course.
When I started the course,
I was certain that by the time that it was over,
I was going to be able to treat somebody
that had been shot, impaled, and attacked
by a grizzly bear with a couple of Q-tips,
some sticks I found in the woods, some duct tape.
You know, it's kind of the MacGyver's
emergency medicine course.
Now by the time I graduated, what I realized was I can keep you alive until I get you to
somebody who knows what they're doing.
More knowledge, much less confident.
By now somebody has already figured out we are talking about the Dunning-Kruger effect.
Now my wife is, she's a board certified trauma nurse, works on ER and ICU, she's got advanced
training in mass casualty incidents, she's critical care, air transport,
qualify, that's ICU in the sky. She's a super nurse and when I told her, this is
her kit by the way, when I told her that I was gonna describe her that way she
laughed and she's like well I'm glad you think so and it's funny because she is an
expert but her confidence level isn't there and that's what happens.
Sometimes, the actual expert is less confident in their knowledge than somebody that knows
nothing.
We see it all the time in the comments section.
Some guy read four memes and all of a sudden he's an expert on quantum physics.
Not really the way it works.
Now the other effect I want to talk about is called the bystander effect.
And this is why I don't want to dissuade anybody from getting a first aid kit.
The bystander effect, to greatly oversimplify it,
is that if you're walking down the street
and you have a heart attack, you better
hope there's only one other person on the street.
Because the more people that are around,
the less likely each one of them is to act.
That diffusion of responsibility takes over.
And you start thinking, well, certainly somebody who knows
and will help, and you keep walking.
And it may not always be the case.
The cool thing is that just hearing that
and letting it sink in and registering that that is a psychological effect and that that
diffusion of responsibility can lead to nobody helping, kind of inoculates you against it.
And owning a first aid kit or a bug out bag or anything that requires you to envision
emergency scenarios does the same thing.
made the conscious decision to act ahead of time and that can save a life.
Now while I can't tell you what kind of kit, well I can't tell you what kind of kit to
get because I don't know your skill set, I don't know what you want it for, I can tell
you what not to get and we'll start there.
Don't get a first aid kit from Walmart or Target or any big box store.
What you're going to get inside of that is like 350 band-aids, a roll of gauze, a triangular
bandage and a cold pack.
Odds are if the injury can be treated with that, it probably would have got better on
its own.
So save that 30 bucks.
Now I can't tell you what kind of kit to get, but I can tell you the company to use.
And before we go any further, I want to say I'm not being paid for this, but in the interest
full disclosure, I should tell you I know the couple that runs it. They're both of
that's. The guy I've worked with on a couple of things that we're not gonna
talk about and I worked with both of them during Hurricane Harvey during
relief efforts. They are they are good people. In fact, after FEMA and the Red
Cross dropped the ball, if you got any supplies off any of those 18-wheelers,
Odds are it's because that couple took shifts sleeping so they could keep the
comms up and keep the trucks rolling and get them where they needed to go. They
are good people. The name of their company is Bear Paw Tac Med. Now I
happen to have the granddaddy of their kits. This monstrosity. And inside it you
find everything that comes pre-packed. You have the big bag and inside the big
bag you have little bags. You open it up there's everything you need to treat
trauma. This one is airways. Everything you need to secure an airway. It is
fantastic. Now this, the difference between this and the Super Nurse bag is
is a field surgery kit and the stuff to deliver a baby. Everything else it has. So do you need
this kit? Probably not. Unless you are, unless you have some medical training or you happen to be
in a field that is dangerous, this is probably not something you need. But they have a bunch of other
kits. They have, if you're an activist, they have street medic kits for protests
that are specifically designed and packed with stuff that will allow you to
treat anything you're likely to run into at a protest. They have individual
first-aid kits. They have a budget version of this which is really cool and
that's the other question. How much does this stuff cost? Super nurse kits, bags
like this they can run anywhere from 700 to a thousand bucks the the stomp bag
the granddaddy bag here I want to say it's 500 something like that they're
they're not cheap the other kits are are much less expensive and the odds are you
may not need all of this stuff not a lot of people know how to secure an airway
So you may not need it. There's a conventional wisdom says you don't carry
anything you don't know how to use. I kind of disagree with that to some extent.
You carry what you understand the theory of because at some point it may just be
you. There may be no other help coming. So let's say you you've never done a
a chest decompression but you understand the theory and you know how to do it, you've just
never done it. Maybe you carry it, maybe you carry what you need to do it because it's one of those
things that if you don't do it the guy's going to die. So it's not like you can hurt the patient
any more than death and if you're forced into that situation it's better to have the tools
and not need them, they need them and not have them. Now a couple other questions
I have gotten about emergency medicine that I feel like need to be addressed.
One of them, I'm not really going to answer the question that was asked, but
yes there is a thing called the Shelf Life Extension Program. It's a cool study.
If this is a topic that you're interested in, you might want to Google it. The other
one I'm definitely going to directly answer because the way it was phrased it kind of
scared me and it said, is it true that veterinarian meds are safe for people? No. No. Resounding
no. Some are. Which ones? We're not having that conversation. Because there's a whole
lot that goes into that, but the way that question was phrased made it sound like somewhere
out there, there was somebody saying that all vet meds are good for people. That's
not true. That is not true. In fact, there are some that are even the same type of
medicine but will kill a human because they have additives to make it work in
animals. So that's not something that you need to mess around with unless
you really know what you're doing. I'm gonna guess that if you're asking some
dude named Bo on the internet about it you probably just need to steer clear of
it that's and even the stuff that is safe also keep in mind these aren't
necessarily made in labs that are up to human standards not all of them anyway
some of them are but so even the stuff that is safe ostensibly I still don't
know that I would use it outside of a severe emergency. I'm talking like zombie
apocalypse emergency. I mean it's knowledge that's good to have but there
there's normally a better way to get the meds you need than PetSmart.
That can get real dangerous and real lethal real quick. So just keep
that in mind. But more importantly the bystander effect. It's not a matter of
of somebody else is going to help him. That may not happen. If not you, who? Make sure
that somebody else is helping before you just walk on by. Anyway, just a thought. Y'all
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}