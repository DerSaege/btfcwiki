---
title: Let's talk about Cyntoia Brown....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ljIWxvKdjOg) |
| Published | 2018/12/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about the Brown case in Tennessee where a 16-year-old girl killed a man 14 years ago and was sentenced to 51 years.
- Defense argues she was being sex trafficked by a guy named Cutthroat and killed the man in self-defense.
- Beau expresses doubt about the defense's story, questioning the rareness of Stockholm Syndrome leading to not turning a firearm on a captor.
- Beau expresses disbelief in the state's version of events and criticizes Tennessee's inclination towards punitive justice.
- Beau presents scenarios where, legally speaking, the girl could be seen as more guilty, but questions when she did something wrong in the eyes of Tennessee residents.
- Advocates for clemency for the girl based on her circumstances, including her age, developmental delays, and years already served in prison.
- Emphasizes the girl's rehabilitation efforts in prison and argues for sending her home.
- Questions the message the governor sends by not granting clemency, suggesting it could impact how traffickers view consequences in the state.
- Encourages the governor to make a decision that represents the values of the people of Tennessee.

### Quotes

- "You telling me a sex trafficker doesn't?"
- "Send a message that is very fitting for the people of Tennessee."
- "So rather than argue her innocence, I'm going to go a different way with it."

### Oneliner

Beau questions the justice system in Tennessee and advocates for clemency for a girl involved in a controversial case, raising doubts and proposing scenarios to challenge perceptions of guilt.

### Audience

Governor of Tennessee

### On-the-ground actions from transcript

- Grant clemency to the girl involved in the case (suggested)
- Advocate for fair treatment and rehabilitation of individuals involved in similar cases (exemplified)

### Whats missing in summary

Deeper insights into the nuances of the justice system and the impact of decisions on individuals caught in controversial cases.

### Tags

#JusticeSystem #Clemency #Tennessee #Advocacy #Rehabilitation


## Transcript
Well, howdy there, internet people.
It's Bo again.
So tonight, we're going to talk about the Brown case.
If you're not familiar with this case, it's a case in
Tennessee where a 16-year-old girl killed a man 14 years
ago.
She was sentenced to what amounts to 51 years after time
offered good behavior.
Her case is before the governor looking for clemency.
Now, the defense contends that she was being sex trafficked
by a guy named Cutthroat, and that the person she killed
was trying to buy her for the night.
She became in fear for her life, and she killed him.
Now, I have to say, I do not believe that story
beyond a reasonable doubt.
It is exceedingly rare for somebody to succumb
to the Stockholm Syndrome to the point
where they will carry a firearm
and not turn it on their captor.
It's rare.
It happens, but it's rare.
In fact, the reason most people know
the term Stockholm syndrome is because it did happen.
But it is rare, so it gives me a little bit of doubt.
Thing is, I don't have to believe her story
beyond a reasonable doubt.
I have to believe the states, and I don't.
But, we're talking about Tennessee.
I lived there.
It is a different kind of state, it's the volunteer state.
They are all about killing some people that need killing.
So rather than argue her innocence, I'm going to go a different way with it.
I'm going to present some scenarios and in each scenario she's going to be more guilty
than the last, legally speaking.
But you let me know when she did something wrong in the eyes of the people of Tennessee.
Let's start with her story.
She's being sex trafficked by a dude named Cutthroat.
Some guy takes her back to his house, gets aggressive, scares her, she kills him.
A dude trying to buy a 16-year-old girl.
Needed killing.
Moving on.
So let's say that he didn't put her in fear for her life.
He's still trying to buy a 16-year-old girl.
Okay, needed killin', movin' on.
Let's, you know, skip ahead a few phases here, because I actually had a bunch laid out, but
we don't need to go through all that.
Let's say cutthroat doesn't even exist.
It's just her out there hookin'.
Guy's still trying to buy a 16-year-old girl.
Needed killin'.
Movin' on.
say that she was flirting with him, and he decides to take her home.
So he's still trying to take advantage of a 16 year old girl.
Now at this point you might finally get some people that are like, well that's bad, but
it doesn't really warrant killing.
Eh, maybe that's true.
So let me remind you, she was 16, developmentally delayed, and she's already done 14 years
in prison.
Send her home.
That's clemency.
She deserves it.
And that's adding a whole lot more guilt than anything that's actually being alleged.
still deserves clemency. Now while she's been in prison, she's gotten her high
school diploma or an equivalency, got a degree, and mentors other prisoners. She's
a model of rehabilitation. Send her home. Now this is up to the governor. So to the
governor I have something I kind of want to say. You're eventually going to be
leaving office soon, right? You pardon this girl and send her home, what message
are you sending to traffickers in your state? You know if your victim kills
you, we're not gonna care. Man, that's a legacy. That is a message that needs to be
You know, rather than just be another politician, send a message that is very fitting for the
people of Tennessee.
Because like I said, it's a volunteer state.
People that will travel thousands of miles to kill people they think need killing.
You telling me a sex trafficker doesn't?
Anyway, just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}