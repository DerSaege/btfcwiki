---
title: Let's talk about how I was wrong about asylum seekers applying in the first country they come to....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t3_Nzzl7jd8) |
| Published | 2018/12/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Admits being wrong about asylum seekers applying for asylum in the first country they come to.
- Discovered a EU court ruling stating refugees must apply in the first member state they enter.
- Explains the Safe Third Country Agreement between the US and Canada.
- Emphasizes the importance of not getting legal information from memes.
- Clarifies that the treaty only applies to the US and Canada, not other countries.
- Mentions that Canada is considering scrapping the agreement due to the treatment of asylum seekers and refugees in the US.
- Trump tried to designate Mexico as a safe third country, but the attempt was laughed at globally.
- Points out the dangers and lack of safety standards in Mexico, making it unsuitable as a safe third country.
- Expresses astonishment at the widespread belief that asylum seekers must apply in the first country they arrive in.
- Concludes by stressing the importance of knowledge and not relying on memes for legal information.

### Quotes

- "Knowledge is power. Don't get your legal information from memes."
- "People that know immigration law were so dumbfounded by this widespread belief."
- "Unless you're moving along the U.S.-Canadian border. That's the only place this treaty applies."

### Oneliner

Beau admits being wrong, sheds light on asylum laws, and warns against relying on memes for legal knowledge, revealing the limited scope of the Safe Third Country Agreement.

### Audience

Legal enthusiasts, asylum advocates

### On-the-ground actions from transcript

- Read and research international treaties and laws (suggested)
- Educate others on the limitations of the Safe Third Country Agreement (implied)

### Whats missing in summary

Understanding the limitations of the Safe Third Country Agreement and the importance of seeking accurate legal information beyond social media memes.

### Tags

#AsylumSeekers #SafeThirdCountryAgreement #LegalInformation #ImmigrationLaw #KnowledgeIsPower


## Transcript
Well, howdy there, internet people, it's Bo again.
So tonight we're going to talk about how I was wrong.
I think it's important to admit when you are wrong.
You've probably seen it in the comments section.
You've got somebody saying, asylum seekers
have to apply for asylum in the first country they come to.
And if you've read our comments section,
I'm sure you have seen me say, cite that law.
Show it. Show me it. Show me the treaty. Without fail, they cannot."
So I got curious and I started looking, trying to figure out where this idea
came from, and I found an EU court ruling from
the high court there that basically said refugees have to apply in the first
member state they come to. If that was to be applied to the U.S.,
it would mean that if an asylum seeker entered in Texas, they had
to apply in Texas and they couldn't go to Louisiana to apply.
apply. That's what it would really mean, but I could see how somebody could be
confused by it. So that's what I was saying. You know, I was like, this is where
it came from. Not, it's not international law. Well, the other day I'm over on
YouTube and somebody comments and says, Bo, you're wrong. There is a treaty that
says that. And I'm like, great, another one. Click the little read more button.
And this guy's got a link. And I don't even have to read the link. I open it up
just have to read the title and sure enough I'm certain that is where it came
from. So we're going to talk about the Safe Third Country Agreement. Okay this
is a good example of why you should not get your legal information from memes.
You have to read the whole treaty or law or statute whatever and then you have to
read all the statutes and laws or treaties that that treaty cites to
really understand it. In most cases, in this case, all you had to do was read the full title.
It is the safe third country agreement between the United States and Canada. It applies to two
countries and two countries only. Basically what it's saying is that if Honduran, for example,
travels through the US, gets to the Canadian border, and tries to apply for asylum, they can't.
They have to apply in the US. If somebody took a flight into Canada and then went
to the US border to try to apply for asylum, they can't. They have to apply in
Canada. But that only applies to the US and Canada. Nowhere else in the world.
Nobody else is a signatory of this treaty. It's bilateral. Two countries. So
it has nothing to do with a Honduran coming up from the Mexican border.
Nothing. Now here's the funny part about this is while people in the US are
trying to expand this treaty and say look no they can't come here here's this
this treaty that Mexico didn't sign. The Canadians, the Conservative Party of
Canada, there's a whole bunch of parties in Canada that are trying to scrap it
because, to them, the United States is no longer a safe third country.
We have fallen out of the requirements because of our treatment of asylum seekers and refugees,
and because of our torture, actually.
Our stance on enhanced interrogation techniques, that also plays into it because that was a
requirement.
Yeah, the Canadians are trying to scrap it, but here's the other part that's really funny
about this.
Trump knows this.
President Trump is completely aware of this.
We know he's aware of it because he actually tried to get Mexico designated as a safe third
country and the entire world laughed at him.
This is a country where sex trafficking, human trafficking, there's been mass slayings of
migrants, people are targeted for their sexual orientation. It is not a safe
third country by any standards. Certainly not by the high standards set out by
that treaty. Okay, but the point I want everybody to take away from this,
especially if you've been walking around saying they have to apply in the first
country they come to, the real thing you need to really let kind of sink in is
that people that know immigration law were so dumbfounded by this widespread
belief that you have to apply in the first country you come to that they were
out there you know Sherlock Holmesing it up trying to figure out where the idea
came from because it's such a foreign concept it's so wrong that that people
were curious where the initial idea came from and they actually argue about it
debate it. Now, I have moved myself. I was wrong. I don't think that it came from the EU
High Court ruling. I think it came from the Safer Country Agreement.
Either way, it doesn't mean that you have to apply in the first country you come to.
Unless, unless you're moving along the U.S.-Canadian border. That's the only place this treaty
applies. Anyway, guys, knowledge is power. Don't get your legal information from
memes. Anyway, just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}