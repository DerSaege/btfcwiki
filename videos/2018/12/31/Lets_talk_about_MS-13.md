---
title: Let's talk about MS-13....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KM977L7pf0o) |
| Published | 2018/12/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- MS-13 has become a boogeyman, leading to calls for building a wall.
- Started in California among Salvadoran refugees as a street gang for protection.
- Civil war in El Salvador ended, and the US sent gang members back.
- The US military trained Ernesto Deris, turning MS-13 into a criminal enterprise.
- The School of Americas, linked to many atrocities, could be where Deris was trained.
- MS-13 is an American creation due to US foreign policy at every step.
- Beau questions the need for a wall and suggests curbing foreign policy instead.
- Criticizes the lack of scrutiny on those trained by the US military for violence.
- Urges stopping the problem at its source to prevent violence and fear.
- Beau advocates for a shift in approach to address the root causes of issues like MS-13.

### Quotes

- "MS-13 is an American creation, not just in the sense that it was founded here, but in the sense that every step of the way, it was American foreign policy that built it."
- "Tell me again why this is the reason we need a wall?"
- "Maybe a better idea would be to curtail our foreign policy instead of building a wall."
- "You never hear them calling for strict scrutiny or vetting of those people that the U.S. military trains to murder, torture, assassinate, and dominate a community by fear."
- "Stop the problem before it starts."

### Oneliner

MS-13, a product of American foreign policy, questions the need for a wall and advocates curbing violence at its source.

### Audience

Policy makers, activists, concerned citizens

### On-the-ground actions from transcript

- Advocate for changes in foreign policy to address root causes of issues like MS-13 (implied)
- Support organizations working to prevent violence and support communities affected by gang activity (suggested)

### Whats missing in summary

The full transcript provides a detailed historical context and analysis behind the creation and evolution of MS-13 through American foreign policy decisions. Watching the full video can offer a deeper understanding of the interconnected factors contributing to the gang's rise.

### Tags

#MS-13 #AmericanForeignPolicy #Immigration #ViolencePrevention #CommunityPolicing


## Transcript
Howdy there, internet people, it's Beau again.
So today we're going to talk about MS-13.
We need to talk about MS-13 because it has become the boogeyman.
That's why we got to build the wall, so let's talk about the boogeyman.
Now, what I'm saying here by calling them that, that is not to downplay
their viciousness or their propensity for violence or how efficient they are.
But it might be important to understand how they came to be.
So in order to understand that, we got to go back to 1979.
In 1979, there was a coup in El Salvador.
US government backed it.
It triggered a decade-long civil war.
That civil war created a bunch of refugees.
Those refugees wound up in California.
In California, they found themselves
to be a minority among immigrants.
So they all hung out together.
Kind of safety in numbers type of thing.
Of course, just like with all other minority immigrant groups, that turned into a street
gang.
MS-13 was born, and it was a street gang.
Not a big deal.
Then two things happened.
First was that the Civil War in El Salvador ended, and we sent a bunch of them home.
That sounds like not a big deal, except for the fact that part of the peace deal was that
the government down there could no longer use the army as police and they didn't have
any police departments.
So we sent a bunch of gang members to a country with no police.
So of course they carved it up and grew in power very, very quickly.
They didn't have any opposition.
And then something else happened.
Satan showed up.
Satan was his nickname.
His real name was Ernesto Deris.
And this man was brilliant.
Not applauding what he did with his brilliance, but he was brilliant.
He understood logistics.
He understood the importance of training.
He understood the importance of discipline.
He understood tactics, weapons, how to strike fear into the heart of a community.
He understood interrogations.
He understood everything that we taught him.
And by we, I mean the US military.
The leadership, the person that turned, who's credited with turning MS-13 from a minor street
gang into a transnational criminal enterprise, is a guy the US military trained.
Now records are fuzzy.
aren't certain of whether or not he was trained in El Salvador or he went to the
School of Americas. If you don't know what the School of Americas is, it is a
facility that the US government runs. Now, the School of America is closed, of course,
but it has been renamed the Western Hemisphere Institute for Security and
Cooperation, something like that. An organization with the same mission still
exists. So in this this place if you look at the graduates of this school you will
find out that they're linked to pretty much every horrible thing that has ever
happened in Central America in the last 50 years. Everything from the
assassination of priests to the disappearance of journalists, mass
executions, it's really bad and they're all trained by the US military. So now
Now it isn't certain that he attended the campus in Panama.
He may have gotten the training at like a pop-up satellite campus in El Salvador.
The records aren't really clear on that.
But to recap, the U.S. backed a coup and triggered a civil war.
The gang was founded in the United States.
It grew in power because of the United States' move to push a bunch of them to a country
with no police, and we trained the leadership that took it from a minor street gang in California
and turned it into a transnational criminal enterprise.
Tell me again why this is the reason we need a wall?
MS-13 is an American creation, not just in the sense that it was founded here, but in
the sense that every step of the way, it was American foreign policy that built it.
Maybe a better idea would be to curtail our foreign policy instead of building a wall.
The kind of knee-jerk reaction that leads people to think, oh, we need a wall, is the
kind of knee-jerk reaction that leads us to train people like Satan.
You know, it's funny, all of these people want really strict scrutiny and very thorough
vetting of people who want to come in and claim asylum.
And maybe that's a good idea.
But you never hear them calling for strict scrutiny or vetting of those people that the
U.S. military trains to murder, torture, assassinate, and dominate a community by fear.
That might be a better idea, just, you know, kind of stop the problem before it starts.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}