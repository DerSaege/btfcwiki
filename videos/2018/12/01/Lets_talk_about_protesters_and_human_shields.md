---
title: Let's talk about protesters and human shields....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KQXiKeKpI0A) |
| Published | 2018/12/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of protest and human shields in the context of current events in the U.S.
- Describes protesters upset with the government for not upholding rights and laws, resorting to violating international norms.
- Recalls a historical event, the Boston Massacre, where protesters were met with violence from militarized forces.
- Compares the recent events to the Boston Massacre, pointing out the use of human shields and the potential for harm.
- Criticizes those cheering on violence against protesters, questioning their support for undermining the Constitution.
- Raises concerns about the immoral act of using human shields and opening fire on unarmed civilians.
- Expresses disbelief at the debate surrounding these actions and advocates for upholding moral standards.
- Condemns betraying the values that America supposedly stood for and calls for reflection on the current state of the country.

### Quotes

- "You do not punish the child for the sins of the father."
- "You do not open fire on unarmed crowds."
- "That's America. That's how we make America great, betraying everything that this country stood for."

### Oneliner

Beau addresses the use of human shields in protests, drawing parallels to historical events and criticizing the immoral actions of opening fire on unarmed civilians.

### Audience

Activists, Protesters, Advocates

### On-the-ground actions from transcript

- Stand against the use of human shields in protests (implied)
- Advocate for non-violent methods of crowd control (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the current state of protests and the use of human shields, urging viewers to reconsider supporting actions that betray moral values.


## Transcript
Hey there internet people, it's Bo again.
So today we're going to talk about protest and human shields.
Because human shields is a term that's being used in relation to something happening in
the U.S., that's interesting.
Okay so I'm going to tell you a story.
You just follow along with me.
So there's these protesters.
They're upset because the government is not doing what it is supposed to do.
They are interfering with life, liberty, and the pursuit of happiness.
They're doing everything wrong, violating their own founding documents, trampling on
people's rights, not living up to the standard international custom, not living up to international
law, not living up to their own laws, basically changing things by proclamation.
So these protesters, they arrive and they are met by militarized goons.
So they throw rocks and sticks and right now you think you know what I'm talking about
And they throw snowballs.
And those militarized goons, they open fire on the crowd.
And we overthrew the government.
It sparked a revolution.
No I'm not talking about what happened on the border.
I'm talking about the Boston Massacre.
That's how far this country has fallen.
It was something that was the catalyst for a revolution.
And now people are cheering it on.
Well it's different.
You know, they didn't kill anybody.
You're right, they didn't.
But they could have.
They could have and they didn't care.
And also keep in mind, the protestors in Boston didn't have kids in the crowd.
Well they had human shields.
Those were just human shields.
Tenemos hijos!
That's not a human shield.
around the world, people still believe that Americans, the American servicemen, somebody
in American uniform will behave with honor and integrity.
What they're saying is, we've got kids here, what are you doing with their human shields?
It's a funny thing, it's a military term, it came into the popular culture during the
gulf war because Saddam put human shields at his radar stations do you know what the
military did in war they didn't bomb them because you don't open fire and harm the
human shield so let's say you're right what you're not but let's just say you are what
they did was still wrong in a wartime setting and you know most of you guys
are on the far right people cheering this on people cheering on someone
undermining the Constitution and open fire on civilians let me ask you this
How did you feel about it when it happened in Waco, Texas?
Any different?
You do not punish the child for the sins of the father.
You do not open fire on unarmed crowds.
I don't understand why this is a debate.
This is something that is plainly immoral.
So that's where we're at, doing anything we can to stop people from reaching safety
and freedom.
That's America.
That's how we make America great, betraying everything that this country stood for, at
least pretended to stand for.
it's pretty obvious you don't stand for much today anyway it's just a thought
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}