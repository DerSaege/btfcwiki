---
title: Let's talk about anti-feminist memes and the patriarchy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hQ1aNRBdQAY) |
| Published | 2018/12/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reacts to backlash over wearing a t-shirt with Disney princesses and feminist themes by discussing anti-feminist memes and the patriarchy.
- Describes two types of anti-feminist memes: one about women doing traditional household tasks and the other about feminism making women ugly.
- Criticizes the shallow mindset of valuing a woman based on her ability to cook, clean, or do laundry.
- Suggests that men who have issues with feminism may still be emotionally tied to their mothers rather than viewing women as independent individuals.
- Addresses the "feminism makes you ugly" meme, which objectifies and criticizes women based on physical appearance.
- Points out the hypocrisy of using a woman's attractiveness as a reason to discredit her feminist views.
- Counters the argument that feminism is unnecessary by showcasing the ongoing existence of patriarchy in society, citing the lack of female representation in positions of power.
- Challenges the notion that women are not promoted in the workforce due to working less or taking more days off, suggesting societal conditioning and gender biases as contributing factors.
- Supports different tactics in advocating for gender equality while acknowledging that radical feminists may not always use methods he agrees with.
- Emphasizes that women have the right to be themselves, pursue their goals, and not be defined by others' opinions or insecurities.

### Quotes

- "All women have to be two things, and that's it. Who and what they want."
- "What you think doesn't matter."
- "The fact that you don't find her attractive doesn't matter."
- "That's your hangup, not hers."
- "I believe in diversity of tactics, so whatever, they're drawing attention to a cause that still needs some attention."

### Oneliner

Beau addresses anti-feminist memes, patriarchy, and the importance of women defining themselves despite societal biases.

### Audience

Gender equality advocates

### On-the-ground actions from transcript

- Challenge anti-feminist attitudes and memes in your social circles (exemplified)
- Support diverse feminist tactics to raise awareness for gender equality (exemplified)
- Empower women to define themselves and pursue their goals (exemplified)

### Whats missing in summary

In-depth analysis and context on the ongoing challenges faced by women in the face of anti-feminist narratives and patriarchal structures.

### Tags

#Feminism #Patriarchy #GenderEquality #AntiFeminism #Empowerment


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about anti-feminist memes and the patriarchy.
We're going to do this because in that last video I wore that t-shirt with the Disney princesses all tatted up.
And for some reason that made people mad.
In the shares on Facebook, people loaded up the comments section with anti-feminist memes.
And we're gonna talk about them. They boil down to two types. You know, you've got the make me a sandwich, do my
laundry, clean  my house meme, and
then you have the feminism makes you ugly meme.
Okay, guys, if you are so shallow that you're going to marry a woman because she can cook,
I would hope that your palate is a little bit more refined than simply needing a BLT. That's a...
You're setting the bar pretty low.
I mean I would hope that you would need a little bit more skill in that
department but anyway that that's a beside the point. As far as cooking,
cleaning, doing your laundry, you probably already had a woman in your life that
did that for you and that was your mommy and that may be the key to
understanding this. See, my four-year-old can make his own sandwich. It may be that
the reason you have a problem with adult women has nothing to do with feminism
and it has to do with the fact that you're still a little boy. That would be
my guess. But let's go on. The feminism makes you ugly meme. Now if you haven't
seen one of these, what it is is they find a feminist that they don't
find attractive and then they cyber stalk her. They go back through and they
find her senior portrait from high school which you know she looks all
wholesome. The photo was taken when she was like 17. Go ahead and marinate on
that for a second. Their ideal of attractiveness is a teenager. Maybe
you're still a little boy. And the after photo is of course one of her in college
where she's probably put on the freshman 15, like most do. Maybe she dyed her hair
a wild color, got a piercing, whatever. So what's the point of the meme? Don't
listen to this woman because of how she looks. The objectification of women, guys,
that is not, that's not an argument against feminism. That's why feminism
exists. That's one of the key issues. And then there's always the added
thing of, you know, the caption says something like, don't let this happen to
your daughter. See in that after photo, she's an adult. She is an adult, yet she's
still property of some other man because she's not married yet, right?
So she still belongs to her dad. Still need that parental figure. Maybe
you're a little boy. Then it goes on, there are other comments about male
feminists and how they're all soft and they are soy boys. Okay, where's your
Medal of Honor at Mr. Meme Maker? Teddy Roosevelt is pretty much the pinnacle
of American masculinity. Medal of Honor recipient, Nobel Peace Prize, you know
I did a whole video on him. I will put it in the comments section.
He's also a founding feminist. Why? Because masculine men, they're not afraid of independent
women. Because they're not a little boy.
Now another argument against it is, you know, against feminism is that feminism is no longer
needed because the patriarchy is dead. And, yeah, one definition of patriarchy is a society in which
a male ruler inherits his title from his dad. That is one definition of patriarchy. You are correct.
Another is a society in which men will get most of the power and women are typically excluded to a
degree. Name the last female president. No. Vice president. No. There are 23 female
U.S. senators and that I think is a record. I think that's as high as it's
ever been. I could be wrong but I'm pretty sure that's a record. How many
would they need for equal representation in our representative
democracy. 50, twice as many. House of Representatives has 87 female
representatives, I believe. For there to be equal representation there they would
need to be 217, closing in on three times as many. What about governors?
There's six. How many would there need to be? 25, four times as many. When you get
It's mayors, state legislators, state senators, stuff like that.
The averages hover around 20 to 25 percent.
Certainly seems like men willed most of the power.
Anyway.
So patriarchy does still exist.
Now it's a loaded term, and it's a term a lot of people don't understand, but it is
still there.
And what happens is there's the rebuttal, oh no, no, no, Beau.
That's not why it's happening.
because they're not qualified. They're not qualified. And that's what they say. What
I hear is that those disparities exist in the private sector as well when it
comes to job promotion and stuff like that. That's what I hear. Oh no, no, no,
no. They don't get promoted because they don't work as hard or as long and they
take more days off. And that's what they say. And what I hear is that women have
been conditioned to put their husband's career first, or stay home with the kids.
Now this isn't to say that I agree with every tactic that radical feminists use, but they
never asked me.
I believe in diversity of tactics, so whatever, they're drawing attention to a cause that
still needs some attention.
At the end of the day, guys, all women have to be two things, and that's it.
Who and what they want.
What you think doesn't matter.
The fact that you don't find her attractive doesn't matter.
The fact that she intimidates you because she is more educated or because she makes
more money or because whatever doesn't matter.
That's your hangup, not hers.
Anyway, just a thought.
and y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}