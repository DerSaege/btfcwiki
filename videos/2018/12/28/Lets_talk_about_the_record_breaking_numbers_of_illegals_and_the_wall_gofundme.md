---
title: Let's talk about the record breaking numbers of illegals and the wall gofundme....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6LAhmB0vzFI) |
| Published | 2018/12/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Border Patrol uses the number of border apprehensions as a gauge for the influx of immigrants, which is flawed due to technological advancements.
- Despite claims of record-breaking illegal immigration, the numbers are actually at a 40-year low.
- Trump's immigration policies may not be the reason for the low numbers, as they have remained relatively consistent for the past eight years.
- The US is no longer a beacon of freedom and critical thinking, electing rulers instead of leaders.
- The government's plan to spend $5 billion on a wall to address a non-existent problem is irrational.
- Lack of critical thinking is evident as people donate to the GoFundMe for the wall without understanding government processes.
- Beau humorously suggests starting a GoFundMe to audit the funds raised for the wall through the IRS.
- Trying to influence politicians through private donations is concerning and hints at fascism.
- Beau challenges the idea of the wall being effective, pointing out flaws in the plan and how smugglers could adapt.
- The fear-mongering around issues like MS-13 is used by politicians to manipulate public opinion and gain support for ineffective policies.

### Quotes

- "We are people who elect rulers instead of leaders."
- "We are going to spend five billion dollars to address a problem that doesn't exist."
- "It shows, one, you don't know how the government works, and two, there's actually a word for a blending of private business interests and the government. It's called fascism."
- "So the wall, assuming it works, will actually pump more money into a criminal cartel that is destabilizing our next-door neighbor."
- "Nobody in DC is going to look out for you."

### Oneliner

Beau challenges the irrationality of spending $5 billion on a border wall for a non-existent issue, criticizing the lack of critical thinking in politics and society.

### Audience

Critical Thinkers, Voters, Activists

### On-the-ground actions from transcript

- Start educating yourself on government processes and policies (suggested)
- Support and donate to organizations working towards comprehensive immigration reform (implied)
- Engage in critical thinking and fact-checking before supporting political initiatives (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of the flaws in the proposed border wall and challenges the lack of critical thinking in politics and society.

### Tags

#Immigration #BorderWall #CriticalThinking #Government #Policy


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about the record-breaking
number of illegals coming into the United States.
With the wall being a hot topic, and especially with
that GoFundMe, we need to talk about it.
Now, there is no statistic on the number of illegals that
come into the country.
Doesn't exist.
Doesn't exist, because if they made it, we don't know.
So what Border Patrol has used for decades as a gauge is the number of border apprehensions,
the number of people they catch, and they kind of extrapolate from that.
Now there's a flaw in the methodology, like if you're comparing numbers from today to
40 years ago, I mean obviously there's more technology today, it's easier to catch them
today, they're catching a higher percentage today, but let's just set that aside, let's
forget about that for a second, because the numbers are at a 40-year low.
There are very few, historically speaking, there are very few people coming across that
southern border.
It's crazy.
And I know right now somebody is saying, well, that just shows that Trump's get tough on
illegal immigrants that that policy is working.
And that might be true if the numbers haven't been relatively the same for the last, I don't
know, eight years.
And right now there's somebody going, well, why would he campaign on it then?
To scare you.
And it worked.
Air-mongering because this country, and this is why we have fewer people coming here, because
we are no longer a beacon of freedom.
We are no longer the independent people, the critical thinkers that we once were.
We are people who elect rulers instead of leaders.
We are going to spend five billion dollars to address a problem that doesn't exist.
It's crazy, right?
And because the government won't come up with the money to do it, you've got people raising
the money on GoFundMe.
And this also leads to why people aren't coming here anymore, we don't critically think.
All of these patriots have never taken a civics course.
So you're going to give the government money, and they're going to do what you want.
Okay.
Well, see, I think that undue influence on these politicians, I think that's worrisome.
So I'm going to start my own GoFundMe, and we're going to donate the money to the IRS,
and we're going to have everybody who donated to the wall GoFundMe audit it.
Well, you can't do that.
You're right.
Under the system of government we have, we can't do that.
You also can't give the money to the government to have them do something for you.
It still has to go through Congress.
The fact that you're trying is pretty scary for two reasons.
It shows, one, you don't know how the government works, and two, there's actually a word for
a blending of private business interests and the government.
It's called fascism.
Fascism doesn't actually mean what it gets used as in internet debates.
It doesn't actually mean, oh, somebody was violent or somebody said something mean.
That's not what fascism is.
It's a real thing.
It is a real thing.
It doesn't just mean government by violence.
All governments rule by violence.
was very specific, everything in the state.
It's pretty terrifying.
So you wonder why people call you a fascist, that's probably why.
But moving on, let's say, because we need to address something else about the wall,
we're going to pretend for a moment that the country on the other side of this wall
is not ruled and heavily dominated
by a criminal enterprise that loves to build tunnels.
We're gonna forget about that.
We're gonna pretend that ladders and ropes don't exist.
We're gonna forget about the fact
that every immigration and smuggling expert
that has ever talked about it said the wall won't work.
We're gonna forget about all of this.
We're gonna pretend that the wall will work
and it's gonna stop people.
Okay?
Now, right now, a coyote to get you over a good one is about $1,500, one that's going
to get you to where you're going.
Now, I know everybody saw that expose, I'm sure, that it's between $4,000 and $10,000.
That's for a child.
That is for a child.
Most smugglers actually won't move anybody under 18, it changes the entire game.
And yeah, you can get some Ranchero to run you across the border when he gets off of
work for $500, but you're probably not going to get where you're going.
$1,500 is about what it's going to cost you.
So the wall's up.
It works, pretending it works.
So what do those smugglers do now?
They close up shop, right?
Of course not.
They team up with the cartel.
it's already more dangerous, but they team up with the cartel. Why? Because the
cartel dominates that country. They certainly have contacts in the passport
office. So for about a thousand bucks you get a Mexican birth certificate,
driver's license, and a passport. And right now everybody's like, but what's that
gonna do? They still got to get a visa to come here. They can't get over that
wall they're not coming here they're going to Canada I can go to Montreal or
Ottawa it's gonna cost them three hundred dollars for the plane ticket one
way and then seven bucks for the ETA that's the electronic travel
authorization so thirteen hundred and seven dollars and then they just walk
across the border into the US. See, Mexico has visa-free travel with Canada, that
open borders thing that so many people are scared of. So the wall, assuming it
works, will actually pump more money into a criminal cartel that is
destabilizing our next-door neighbor, one of our biggest trading partners, and make
it cheaper to get here illegally. Man, didn't really think that one through did
you? So there is no situation in which the wall will function the way the
government is telling you just like the idea that we need the wall when we're at
a 40-year low isn't true either. Nobody in DC is going to look out for you. This
isn't, they don't care and they use anything to scare you and because nobody
wants to think critically because we're all had become so caught up in this
partisan politics, the red and the blue, the donkey and the elephant, that nobody
thinks critically. How many people right now terrified of MS-13 coming to the
United States? Probably the same number of people that don't know it was started
in the United States. They've got you terrified of this foreign gang, right,
started in California. It's been here, has been for a very, very long time. It's
nothing new. It's just that this particular group of
politicians is using it to scare you. It's all it is. And because of that, you're
willing to give money that's left over from them taxing you to them to build a
wall that won't work will hurt the economy, make it cheaper for the people
to get here, fund the gangs, the criminal enterprises, and destabilize one of our
neighbors because that always works so well destabilizing these countries.
That's why most of these people are trying to still come here because
they're certainly not coming here for an education. Anyway, it's just a fault.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}