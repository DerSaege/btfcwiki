---
title: Let's talk about courage, Bundy Ranch, and Elf on the Shelf....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CB6fVkSiobM) |
| Published | 2018/12/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defines courage as recognizing danger and overcoming fear, not the absence of fear.
- Mentions the Bundy Ranch incident where ranchers faced off with the Bureau of Land Management.
- Declines to participate in the Bundy Ranch situation due to his disagreement with their tactics.
- Acknowledges Eamonn Bundy's courage for openly criticizing the President's immigration policy to his far-right fan base.
- Stresses that rural America supports freedom for all and believes in the Constitution.
- Shares a personal story about encountering racist remarks while shopping for Christmas decorations.
- Encourages those who usually mind their own business to start speaking out and getting involved in current issues.
- Urges people who believe in freedom for all to become vocal and counter the negative representation created by a vocal minority.
- Addresses the importance of diverse voices speaking up for freedom and equality.
- Encourages Southern belles and young Southern females to use platforms like YouTube to spread messages of freedom and equality.

### Quotes

- "Courage is recognizing a danger and overcoming your fear of it."
- "Somebody has got to start talking for those people who truly believe in freedom."
- "Be that megaphone standing up for freedom. Real freedom."
- "You are more important than you can possibly imagine at getting the message out to the people that need to hear it."
- "Start lending your voices. You can't just sit on the sidelines anymore."

### Oneliner

Beau challenges those who typically mind their business to start speaking out for real freedom and equality, urging diverse voices to counter the negative representation by a vocal minority.

### Audience

Southern Community Members

### On-the-ground actions from transcript

- Start speaking out on social media platforms (suggested)
- Use your voice to advocate for freedom and equality (implied)
- Encourage diversity in voices speaking out (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of courage, the Bundy Ranch incident, personal encounters with racism, and a call to action for individuals to use their voices to advocate for real freedom and equality.


## Transcript
Well, howdy there Internet people, it's Bo again.
So tonight we're gonna talk about
Bundy Ranch, courage, and Elf on the Shelf. I know those things don't sound like they have anything in common.
Courage. Today,
when people say courage, they think the absence of fear. That's not what courage is.
Courage is recognizing a danger and
overcoming your fear of it. That's what courage is. The absence of fear is
stupidity. Fear exists for a reason. It keeps you alive. Rational fear anyway. Now
a lot of people point to Bundy Ranch as a good example of courage. Now if
you're not familiar with it, some ranchers faced off with the Bureau of
land management.
That's basically what happened.
I was asked to go.
I had some friends ask me to go there and offer my advice.
I said, no, I didn't want to be a part of amateur hour at the
OK Corral.
And I thought the tactics that the Bundys were using were
reckless at best.
And I thought it would lead to unnecessary bloodshed.
I'm saying this because I want to firmly establish that I'm
not a fan of the Bundys.
Now most of you are kind of liberal.
You may not know this, but since that incident the Bundys have become social media phenomenons.
They are darlings of the far right.
And this week I watched Eamonn Bundy display true courage, real courage.
He went to his audience, his fan base of far right people, and openly and repeatedly called
out the President's immigration policy in no uncertain terms.
I actually encourage you to go look and see what he said.
I tend to focus on the legal and moral aspects of it.
He went to the religious aspects and it's rather interesting.
So to me that's courage and the reason I want to acknowledge that courage is because in
my comment section a lot I see people say things like, man I wish there were more people
from the south that thought like you.
I wish there were more people that looked like you that acted like you.
There are.
The majority of rural America supports freedom, real freedom, freedom for everybody.
There are people that truly believe in the Constitution, and the whole premise of the
Bill of Rights could be summed up with your right to be left alone.
But because of that, and because they embody that, they mind their own business.
A good example of that was today.
And what may be the greatest cliche ever,
um, my wife wanted pictures of the kids with Santa.
So we head to Bass Pro Shop, like good radnecks.
And we get up there and my wife also,
we need to get a new elf on the shelf.
If you don't know what this is,
it's a little elf that hangs out,
moves around your house at night,
and he reports back to Santa
how the kids are behaving. We need a new one. And, you know, so we're standing there, we've got all
the kids with us, and my wife walks over out on the line to go get a new elf on the shelf. And this
random woman, not a Bass Pro Shop employee, no reason to boycott them, looks at our family,
then looks at her and says, oh, well, you want these. And motions to the male light-skinned
on the shelf and my wife's like first I want you to picture a woman that would
marry me she's not exactly timid she's like I really don't think that matters
and the tone of her voice made other people in line kind of look over and the
woman's like oh yeah it matters and you can see people their faces everybody's
like now some of them were upset because the obvious racist connotations to what
the lady was saying. And some of them were probably upset at the idea of a woman telling
another family what kind of Christmas decoration they needed because we don't do that. We
let people run their own lives. Now my wife, she says some things that can't be repeated
in polite company and manages to make a point of showing the lady that she got the female
Latina off on the shelf which was entertaining and we did manage to get
out of the store with the pictures of the kids with Santa without ending up
on World Star. That's always a bonus. But the point is that crowd. Those people.
Those people that said nothing. You could tell by the look on their face they could
not believe that this woman would say this, all white, it's Bass Pro Shop, come on, and
you could tell people were upset, but nobody said anything because it's not what we do.
We mind our business.
Now that I've explained this, I want to issue a challenge to everybody that minds their
business.
Start lending your voices.
You can't.
can't just sit on the sidelines anymore. It's getting too dangerous and too crazy
out there for you to try to wait it out. You can't. You've got to get involved. You
know, I know people like us, the last thing in the world that you want to do
is start a Facebook page or a YouTube channel to tell people, other people, how
to live their lives. It's not what you do, that's a pastor's job. Do it. Do it. Be that
megaphone for people. Because here's the thing, it's this tiny vocal minority that's making
the rest of us look horrible. Somebody has got to start talking for those people who
truly believe in freedom. Freedom for all. Not freedom based on where you were born or
your skin color, or your socioeconomic class, freedom for all.
And we need a lot more voices doing it.
Another comment I see a lot in the comment section is, and it comes from black people,
my people have been saying this for years, some redneck comes along and says it and it
goes viral.
You know as well as I do that somebody that looks like us, sounds like us, can say things
and have it listened to, and they'd never listen to it if it came from a black person.
You know it.
Be that megaphone.
standing up for freedom. Real freedom. Not nationalism. Freedom. Anyway, and if you
are a Southern belle, if you are a young Southern female, for God's sake start a
YouTube channel. It's one thing I've noticed about that social media
platform. You are more important than you can possibly imagine at getting the
message out to the people that need to hear it anyways just a thought y'all
So have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}