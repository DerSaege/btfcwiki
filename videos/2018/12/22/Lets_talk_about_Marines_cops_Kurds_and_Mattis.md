---
title: Let's talk about Marines, cops, Kurds, and Mattis....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nCEToTsbcT8) |
| Published | 2018/12/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The theme of duty is prevalent in recent news, particularly with the ruling from a federal court in Florida stating that law enforcement has no duty to protect individuals.
- General Mattis's resignation is believed to be driven by his strong sense of duty, as described by other Marines who worked with him.
- Mattis's disagreement with President Trump on withdrawing from Syria stems from his belief that it leaves allies, like the Kurdish people, vulnerable.
- The Kurdish people, an ethnic minority spread across countries like Turkey, Syria, Iraq, and Iran, are seen as being left without support once again by the U.S.
- The U.S. historically has not supported Kurdish independence due to geopolitical reasons and the Kurds' potential to destabilize the region.
- The imminent U.S. withdrawal from Syria may present an uncertain but possibly opportune moment for Kurdish independence efforts.
- Law enforcement, as per existing case law, is not mandated to protect individuals, even in extreme scenarios like mass shootings.
- Beau underlines the idea that law enforcement is part of a government meant to control rather than protect citizens.
- Despite some officers' acts of heroism, there is no legal obligation for law enforcement to protect individuals.
- Beau concludes by urging people to be mindful of the government's role and the implications of legislation that may weaken citizens' ability to protect themselves.

### Quotes

- "Law enforcement is not there to protect you. We do not live under a protectorment."
- "Government is there to control you, not protect you."
- "Do some cops save lives? Yes, but they do it in spite of the law, not because of it."

### Oneliner

Beau delves into the concept of duty through the lens of recent events, from Mattis's resignation to the inherent lack of obligation for law enforcement to protect citizens, urging a reevaluation of governmental roles and citizen empowerment.

### Audience

Citizens, activists

### On-the-ground actions from transcript

- Connect with local community organizations for support and advocacy (implied)
- Educate others about the true role of law enforcement in society (implied)
- Support legislation that empowers citizens to protect themselves (implied)

### Whats missing in summary

The full transcript provides a comprehensive exploration of duty in the context of recent events, shedding light on the complex dynamics between government control and citizen protection.

### Tags

#Duty #LawEnforcement #Government #CitizenEmpowerment #KurdishIndependence


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about duty
because it is a overriding theme in the news,
in the last few major news items.
First, we have that ruling coming out of Florida
from the federal court saying that law enforcement
has no duty to protect you,
and we'll talk about that in a minute.
And then we have General Mattis's resignation,
which, I believe, stems from his sense of duty, and I know that sounds weird.
He quit because of his sense of duty.
I never met the man, but I know a lot of people who worked for him.
And first, let me say the memes are true by all accounts.
He is Mad Dog Mattis. He is General Chaos.
Chaos being his call sign.
He will knife-hand you to death.
due to death.
But something that I noticed was that when people would talk about him a little bit more
in depth and get past that bravado, they would talk about his honor, his integrity, and his
sense of duty.
And that is something that a politician would say about an outgoing Secretary of Defense.
However these weren't politicians, they were other Marines.
And the Marine Corps as an institution, honor, integrity, and a sense of duty, that's the
standard.
That's the norm.
So when you hear a Marine talk about another Marine's sense of honor and sense of duty,
you pay attention.
Because it's kind of like hearing an Olympic sprinter say, man, that guy's fast.
You know, it carries a little bit more weight.
And yeah, I do understand that there are Marines that have done dishonorable things.
However, as an institution, well, they're always faithful.
I'm certain that there are genuine geopolitical concerns that Madison Trump disagreed on.
I can't believe I'm going to say this.
For the first time, I agree with President Trump.
We should withdraw from Syria.
We never should have been there to begin with.
That revolution wasn't organic.
We pumped millions of dollars into feeding the propaganda that destabilized that country,
and this is the fallout.
But from Mattis's point of view, I believe that his sense of duty doesn't allow him
to continue because he feels we are leaving our allies twisting in the wind.
And in a lot of ways, he's right.
We are.
It's kind of like we were in a bar and picked a fight with a biker gang, and all of our
buddies joined in, and then we headed for the door.
But probably never should have picked a fight to begin with.
One of the groups of people that is being seen as being left twisting in the wind wears
They are the Kurdish people, they are an ethnic minority in the Middle East, they are very
unique because they describe themselves as Kurdish first rather than Shia or Sunni, they
are Kurds and they are spread out, they have no homeland of their own, but they are spread
out over a number of countries, most important of which are Turkey, Syria, Iraq, and Iran.
All of the countries you hear about in the news.
Because of that, and hopefully the Kurds will finally realize, the U.S. will never allow
them, under their banner, to achieve independence.
Because the Kurds are a ready-made insurgent force that can be used to destabilize any
of these countries at any point in time. It is not in the best interest of the United
States to have an independent Kurdistan. They didn't allow it after the first Gulf War.
They didn't allow it after the Iraq War. They didn't allow it when the KRG actually
attempted it. They wouldn't have allowed it in Assyria. So, the Kurds can look at it
is that they're being left twisting in the wind yet again, or they can look at it as
an opportunity to take the gloves off.
Because here's the thing, the US is leaving.
The US is leaving.
They can't re-intervene.
They can't go back in, not during this administration.
That would be the admission of a mistake.
We all know that Trump doesn't make mistakes.
He certainly wouldn't admit to it.
one on a global scale.
The U.S. will not get involved in that fight again.
So in a weird way, this may be the best shot for Kurdish independence because, well, they're
going to have to do it independently.
And there won't be a U.S. advisor keeping him on a leash.
But we'll have to see.
I would wager to guess that many of the Kurdish fighters, those men and women who have put
their lives on the line for a decade, fillets their duty to continue that fight.
as Matt has felt that he had a duty not to leave a man behind.
Now let's talk about that court ruling.
The court, federal court has ruled, and it's really just an affirmation of existing case
law that law enforcement has no duty to protect you.
Even the most, even in the most extreme circumstances, they have no duty to protect a citizen.
And those extreme circumstances were a classroom full of kids getting gunned down during a
mass shooting and the officer who failed to protect them literally waiting outside and
his entire scope of duty was that school.
Federal court has ruled he had no duty to protect them.
That is something that Americans need to keep in mind.
Law enforcement is not there to protect you.
We do not live under a protectorment.
We live under a government.
Government is there to control you, not protect you.
And law enforcement is the enforcement arm of that government.
Do some cops save lives?
Yes, but they do it in spite of the law, not because of it.
There is no duty for them to protect you.
That is something that many people in this country have forgotten and it's something
that people really need to keep in mind as they push for legislation that empowers the
government at the loss of the citizen and their ability to protect themselves.
It's something you really need to keep in mind, and this wasn't a surprise ruling.
This has been case law for a very, very, very long time.
So under federal law, that officer that everybody was outraged about that waited outside during
the Parkland shooting did nothing wrong.
He had no duty to intervene.
It's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}