---
title: Let's talk about hunting and Sex and the City....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=29xS9oOUZ30) |
| Published | 2018/12/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Dismisses the stereotype that everyone from the South hunts, noting he used to hunt but stopped due to early mornings.
- Distinguishes between hunters who eat what they kill and trophy hunters who go after exotic animals primarily for status.
- Criticizes wealthy trophy hunters who go on canned hunts, where guides do all the work and hunters simply take the shot.
- Points out that while some claim canned hunts support conservation efforts, the practice actually fuels a market for artifacts and poaching.
- Suggests investing the hunting money in local communities instead, promoting self-sufficiency and economic growth.
- Proposes alternatives like volunteering with anti-poaching forces or supporting advocates like Kristen Davis for animal conservation.
- Recommends supporting organizations like the David Sheldrick Wildlife Trust for elephant conservation efforts.

### Quotes

- "It's about proving you're a man."
- "Number one, that stops this from being just an advanced form of colonialism."
- "It's about killing. It's about proving you're a man."
- "So alternatively, if you are interested in this topic, my suggestion..."
- "They don't want aid, they want business."

### Oneliner

Beau dismisses hunting stereotypes, criticizes wealthy trophy hunters for fueling poaching markets, and suggests investing in local communities as an alternative to canned hunts.

### Audience

Conservationists, animal advocates

### On-the-ground actions from transcript

- Invest the hunting money in local communities to support self-sufficiency and economic growth (suggested).
- Volunteer with anti-poaching task forces to combat poaching (suggested).
- Support organizations like the David Sheldrick Wildlife Trust for elephant conservation efforts (suggested).

### Whats missing in summary

Beau's passion and detailed insights on the impact of canned hunting and the importance of community investment may be best understood by watching the full transcript.

### Tags

#Hunting #TrophyHunting #Conservation #CommunityInvestment #AnimalAdvocacy


## Transcript
Well, howdy there, internet people, it's Beau again.
So I had somebody ask me to talk about hunting.
Well, first, I want to point out that that
is extremely stereotypical.
Not everybody that sounds like me hunts.
Not everybody who lives in the South hunts.
Or people who live their entire lives in the South
have never gone hunting.
I'm not one of those people, but I'm sure they exist somewhere.
I used to hunt 10, 15 years ago, something like that.
I had an epiphany about the nature of hunting and I stopped and right now all the vegans are like, what?
No, no, no. I realized that it is always before the Sun comes up and I don't I don't want to get out of bed
That's that's really why I don't
Before we really get into this I want to say if you hunt and you eat that animal I'm not talking about you
I am NOT talking about you if you hunt and you eat that animal and you put that animal's head on the wall
I think it's a little weird, but I'm not really talking about you either. I'm talking about trophy hunters, and
it's become a trend among the wealthy. They go after exotic animals mostly. You can read that as
at risk, endangered, however you want to say it.
They want to go after the rare.
Now typically,
it happens in the US sometimes, but typically,
They pay tens of thousands of dollars to fly over to Africa and go on one of these
canned hunts. Now if you're not familiar with how these hunts work, basically some
would-be great white hunter hires a team of people that do all the work for him.
The guide finds the animal, which is typically an older sick animal, and the
supporters do everything else. The great white hunter, in this case, typically just walks
out and takes the shot and then brings back a artifact from that animal and we are all
supposed to be impressed with his marksmanship because he was able to hit something the size
of an elephant because it was actually an elephant. Now, the rebuttal from this crowd
is that these fees, these tens of thousands of dollars, they go to conservation efforts.
some truth to that. A huge portion of that money does go to conservation, but
that's only part of the story. That is only part of the story. The thing is,
what happens when a bunch of wealthy people start doing something? It becomes a
status symbol, right? Everybody else wants to do it to show that they can do it.
They're keeping up with the Joneses type of thing. So what if they don't have the
money for those fees. What happens then, there's a market that's not being met for
these artifacts and that's part of what goes on with poaching. So these canned
hunts actually help maintain the market and provide the drive for the market
that they're saying they're trying to combat. So it's not entirely true and
more importantly something that is definitely worth noting is that these
poachers are typically linked to some warlord in the area and the prophets
then turn into violence. So I've got a crazy idea. Just hear me out because if
it was about conservation they could do the safari with a cannon or an icon. Go
photographing it. We'll appreciate it in nature and you know all of that but
that's not what goes on because that's not what it's really about. It's about
killing something. It's about being the great white hunter. It's about proving
your masculinity in a completely canned hunt. Anyway, the thing is my alternative
satisfies everybody. First take the twenty or thirty thousand dollars you're
gonna spend on this hunt and just invest it in the community where you were gonna
go. Number one, that stops this from being just an advanced form of colonialism with some guy
coming over there to exploit the natural resources and leave. Number two, you may actually get your
money back. Number three, it helps them become more self-sufficient, helps their economies grow.
They don't want aid, they want business. So in exchange for that investment, I am certain that
somebody in the local community would be more than happy to take you on a photo safari if it's really
about conservation and adventure. But it's not. So let's keep going. It's about killing. It's about
proving you're a man. You want to hunt that dangerous game, right? Go volunteer with an
anti-poaching task force while you're over there. You want to hunt dangerous game? Hunt the most
dangerous animal of all. Some guy carrying an AK and a chainsaw out there to fill the
market you helped maintain and create. Now that may be a little bit militant for some
people. So alternatively, if you are interested in this topic, my suggestion would be to follow
Kristen Davis. That's Charlotte from Sex and the City. She is an amazing advocate for animals in
Africa, specifically elephants. And since my t-shirts have become such a hot topic as of late,
I will go ahead and tell you this one is from a fundraiser she did for the David Sheldrick
Wildlife Trust. If you happen to have some cash laying around, those guys have their stuff together
that they do a lot of good. The fifth column actually sponsored an elephant
from them a couple years ago named Bongo. Anyway, just a thought. Y'all have a good
night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}