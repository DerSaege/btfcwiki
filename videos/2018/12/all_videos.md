# All videos from December, 2018
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2018-12-31: Let's talk about MS-13.... (<a href="https://youtube.com/watch?v=KM977L7pf0o">watch</a> || <a href="/videos/2018/12/31/Lets_talk_about_MS-13">transcript &amp; editable summary</a>)

MS-13, a product of American foreign policy, questions the need for a wall and advocates curbing violence at its source.

</summary>

"MS-13 is an American creation, not just in the sense that it was founded here, but in the sense that every step of the way, it was American foreign policy that built it."
"Tell me again why this is the reason we need a wall?"
"Maybe a better idea would be to curtail our foreign policy instead of building a wall."
"You never hear them calling for strict scrutiny or vetting of those people that the U.S. military trains to murder, torture, assassinate, and dominate a community by fear."
"Stop the problem before it starts."

### AI summary (High error rate! Edit errors on video page)

MS-13 has become a boogeyman, leading to calls for building a wall.
Started in California among Salvadoran refugees as a street gang for protection.
Civil war in El Salvador ended, and the US sent gang members back.
The US military trained Ernesto Deris, turning MS-13 into a criminal enterprise.
The School of Americas, linked to many atrocities, could be where Deris was trained.
MS-13 is an American creation due to US foreign policy at every step.
Beau questions the need for a wall and suggests curbing foreign policy instead.
Criticizes the lack of scrutiny on those trained by the US military for violence.
Urges stopping the problem at its source to prevent violence and fear.
Beau advocates for a shift in approach to address the root causes of issues like MS-13.

Actions:

for policy makers, activists, concerned citizens,
Advocate for changes in foreign policy to address root causes of issues like MS-13 (implied)
Support organizations working to prevent violence and support communities affected by gang activity (suggested)
</details>
<details>
<summary>
2018-12-28: Let's talk about the record breaking numbers of illegals and the wall gofundme.... (<a href="https://youtube.com/watch?v=6LAhmB0vzFI">watch</a> || <a href="/videos/2018/12/28/Lets_talk_about_the_record_breaking_numbers_of_illegals_and_the_wall_gofundme">transcript &amp; editable summary</a>)

Beau challenges the irrationality of spending $5 billion on a border wall for a non-existent issue, criticizing the lack of critical thinking in politics and society.

</summary>

"We are people who elect rulers instead of leaders."
"We are going to spend five billion dollars to address a problem that doesn't exist."
"It shows, one, you don't know how the government works, and two, there's actually a word for a blending of private business interests and the government. It's called fascism."
"So the wall, assuming it works, will actually pump more money into a criminal cartel that is destabilizing our next-door neighbor."
"Nobody in DC is going to look out for you."

### AI summary (High error rate! Edit errors on video page)

Border Patrol uses the number of border apprehensions as a gauge for the influx of immigrants, which is flawed due to technological advancements.
Despite claims of record-breaking illegal immigration, the numbers are actually at a 40-year low.
Trump's immigration policies may not be the reason for the low numbers, as they have remained relatively consistent for the past eight years.
The US is no longer a beacon of freedom and critical thinking, electing rulers instead of leaders.
The government's plan to spend $5 billion on a wall to address a non-existent problem is irrational.
Lack of critical thinking is evident as people donate to the GoFundMe for the wall without understanding government processes.
Beau humorously suggests starting a GoFundMe to audit the funds raised for the wall through the IRS.
Trying to influence politicians through private donations is concerning and hints at fascism.
Beau challenges the idea of the wall being effective, pointing out flaws in the plan and how smugglers could adapt.
The fear-mongering around issues like MS-13 is used by politicians to manipulate public opinion and gain support for ineffective policies.

Actions:

for critical thinkers, voters, activists,
Start educating yourself on government processes and policies (suggested)
Support and donate to organizations working towards comprehensive immigration reform (implied)
Engage in critical thinking and fact-checking before supporting political initiatives (suggested)
</details>
<details>
<summary>
2018-12-24: Let's talk about the war on Christmas.... (<a href="https://youtube.com/watch?v=pj-2YGiYM7c">watch</a> || <a href="/videos/2018/12/24/Lets_talk_about_the_war_on_Christmas">transcript &amp; editable summary</a>)

Beau explains the war on Christmas, urging for actions that embody love and compassion, not just words.

</summary>

"There is definitely a war on Christmas."
"Be loving in truth and deed, not word and talk."
"Do you think people look at you and say, man, I want to be like them?"

### AI summary (High error rate! Edit errors on video page)

Explains the concept of the war on Christmas and clarifies that it is not a joke video.
Recounts the story of the Good Samaritan from the Bible, focusing on the message of loving thy neighbor.
Draws parallels between the actions of Christians during Christmas and the teachings of loving thy neighbor.
Criticizes the focus on building walls instead of helping those in need, contradicting the message of the Good Samaritan.
Emphasizes the importance of showing love through actions, not just words.
Questions whether one's actions make others want to emulate them or avoid them.
Notes that the war on Christmas is not about holiday cups but about embodying the spirit of love and compassion.
Extends warm holiday wishes to people of different religions, acknowledging their celebrations.

Actions:

for christians, holiday celebrators,
Extend warm holiday wishes to people of different religions (exemplified)
</details>
<details>
<summary>
2018-12-24: Let's talk about Rule 303.... (<a href="https://youtube.com/watch?v=HbGKzleLJVc">watch</a> || <a href="/videos/2018/12/24/Lets_talk_about_Rule_303">transcript &amp; editable summary</a>)

Beau explains the concept of "Rule 303" and challenges law enforcement officers to fulfill their duty to protect without excuses or prioritizing self-preservation over others.

</summary>

"Rule 303 meant having the means at hand and therefore the responsibility to act, not might makes right."
"Your duty is not bounded by risk. Your duty is not to get up and go to work every day. The guy at Hardee's has that duty."
"They care about the students. That's it."
"The gun makes the man. Don't you think that might be reinforcing the thought process of those kids that go into schools and shoot the place up?"
"If you cannot truly envision yourself laying down your life to protect those kids, you need to get a different assignment."

### AI summary (High error rate! Edit errors on video page)

Explains the origins of "Rule 303" among military contractors, originating from a historical incident involving a soldier using his rifle caliber as justification for actions.
Emphasizes that "Rule 303" meant having the means at hand and therefore the responsibility to act, not might makes right.
Provides examples of how different situations have applied the concept of "Rule 303," including military interventions and law enforcement responses.
Criticizes the excuses made by law enforcement officers who fail to act in critical situations, such as the Parkland shooting.
Argues that duty is not bounded by risk and that officers have a responsibility to act, especially in protecting children.
Challenges the notion that equipment superiority should deter action, stressing the importance of fulfilling one's duty regardless of perceived risks.
Questions the message sent by prioritizing self-preservation over protecting those in need.
Urges school resource officers and law enforcement personnel to reconsider their roles if they are not willing to lay down their lives to protect others.

Actions:

for law enforcement officers,
Reassess your commitment to protecting others if you are in law enforcement and prioritize self-preservation over duty (implied).
</details>
<details>
<summary>
2018-12-22: Let's talk about Marines, cops, Kurds, and Mattis.... (<a href="https://youtube.com/watch?v=nCEToTsbcT8">watch</a> || <a href="/videos/2018/12/22/Lets_talk_about_Marines_cops_Kurds_and_Mattis">transcript &amp; editable summary</a>)

Beau delves into the concept of duty through the lens of recent events, from Mattis's resignation to the inherent lack of obligation for law enforcement to protect citizens, urging a reevaluation of governmental roles and citizen empowerment.

</summary>

"Law enforcement is not there to protect you. We do not live under a protectorment."
"Government is there to control you, not protect you."
"Do some cops save lives? Yes, but they do it in spite of the law, not because of it."

### AI summary (High error rate! Edit errors on video page)

The theme of duty is prevalent in recent news, particularly with the ruling from a federal court in Florida stating that law enforcement has no duty to protect individuals.
General Mattis's resignation is believed to be driven by his strong sense of duty, as described by other Marines who worked with him.
Mattis's disagreement with President Trump on withdrawing from Syria stems from his belief that it leaves allies, like the Kurdish people, vulnerable.
The Kurdish people, an ethnic minority spread across countries like Turkey, Syria, Iraq, and Iran, are seen as being left without support once again by the U.S.
The U.S. historically has not supported Kurdish independence due to geopolitical reasons and the Kurds' potential to destabilize the region.
The imminent U.S. withdrawal from Syria may present an uncertain but possibly opportune moment for Kurdish independence efforts.
Law enforcement, as per existing case law, is not mandated to protect individuals, even in extreme scenarios like mass shootings.
Beau underlines the idea that law enforcement is part of a government meant to control rather than protect citizens.
Despite some officers' acts of heroism, there is no legal obligation for law enforcement to protect individuals.
Beau concludes by urging people to be mindful of the government's role and the implications of legislation that may weaken citizens' ability to protect themselves.

Actions:

for citizens, activists,
Connect with local community organizations for support and advocacy (implied)
Educate others about the true role of law enforcement in society (implied)
Support legislation that empowers citizens to protect themselves (implied)
</details>
<details>
<summary>
2018-12-21: Let's talk about what we can learn from internet tough guys.... (<a href="https://youtube.com/watch?v=mY1qVWuFZDY">watch</a> || <a href="/videos/2018/12/21/Lets_talk_about_what_we_can_learn_from_internet_tough_guys">transcript &amp; editable summary</a>)

Beau delves into the parallels between the internet tough guy and American foreign policy, addressing insulation from consequences and the role of individuals in driving global change.

</summary>

"When we see it on TV, we turn it off. That's happening somewhere over there."
"Money makes the world go round, right?"
"It does have to be somebody from here."
"At this stage in the game, the way everything is interconnected, any revolution that is not global, it's not a revolution."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Analyzing the internet tough guy as a symptom and analogy for American foreign policy.
The internet tough guy's behavior stems from being insulated from consequences, much like American foreign policy.
Americans can ignore horrible foreign policy because they are shielded by the military machine.
The insulation from consequences leads to a disconnect from global issues, viewing them as unrelated.
Addressing the mentality of not needing to help foreigners because one was born in the U.S.
Exploring the notion that caring from the top income earners could drive global change.
Money plays a pivotal role in influencing change due to the wealth in certain countries.
Acknowledging that injustices often arise from massive corporations driven by consumer choices.
Emphasizing that change starts with individual actions and consumer decisions.
Stating that any revolution in today's interconnected world must be global to truly bring change.

Actions:

for global citizens,
Choose consciously where to spend money to influence corporate behavior (implied)
Start making changes individually in consumption habits (implied)
</details>
<details>
<summary>
2018-12-21: Let's talk about revolution.... (<a href="https://youtube.com/watch?v=Db7TUd0A9-M">watch</a> || <a href="/videos/2018/12/21/Lets_talk_about_revolution">transcript &amp; editable summary</a>)

Beau delves into the implications of revolution, warning against violence in the US and advocating for new ideas to drive change.

</summary>

"A new form of government. Changing forms of government. Something like that."
"In the United States, a violent revolution will be horrible."
"You can put a bullet in it, or you can put a new idea in it."

### AI summary (High error rate! Edit errors on video page)

Addressing the comments section's interest in revolution, especially after the Yellow Vest Movement, and activists in the US seeking a revolution.
Defines a revolution as not just regime change but bringing a new idea to the table, like changing forms of government.
Notes that Western civilization's trend in revolutions is to decentralize power and give more power to more people.
Warns about the consequences of a violent revolution in the United States, citing ethnic, racial tensions, and cultural divides.
Mentions that the US is not self-reliant anymore and how a revolution interrupting transportation could lead to food and supply shortages.
Emphasizes that violent revolution isn't the only way to change minds; focusing on new ideas is key to the future.
Talks about the potential devastating impact of a violent revolution in the US and the misguided belief in readiness for such a scenario.

Actions:

for activists, thinkers, citizens,
Prepare emergency supplies and have a plan in case of disruptions in transportation (implied)
Engage in constructive dialogues and debates about new ideas for governance (implied)
</details>
<details>
<summary>
2018-12-16: Let's talk about two psychological effects and first aid kits.... (<a href="https://youtube.com/watch?v=1lfWrmYPonc">watch</a> || <a href="/videos/2018/12/16/Lets_talk_about_two_psychological_effects_and_first_aid_kits">transcript &amp; editable summary</a>)

Beau breaks down psychological effects, expert confidence, and the bystander effect in emergency situations, urging action and preparedness with quality first aid kits to potentially save lives.

</summary>

"If you're walking down the street and you have a heart attack, you better hope there's only one other person on the street."
"Sometimes, the actual expert is less confident in their knowledge than somebody that knows nothing."
"Make sure that somebody else is helping before you just walk on by."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of psychological effects and first aid kits that could potentially save lives.
Contrasts different levels of knowledge and confidence in handling first aid kits between himself, his son, and his wife.
Explains the Dunning-Kruger effect in the context of expertise and confidence levels.
Talks about the bystander effect and its impact on people's willingness to act in emergencies.
Advocates for owning a first aid kit to combat the bystander effect and potentially save lives.
Provides advice on what kind of first aid kit not to get and recommends a company named Bear Paw Tac Med for quality kits.
Describes the contents and uses of different types of first aid kits offered by Bear Paw Tac Med.
Suggests the importance of carrying tools you understand the theory of, even if you may not know how to use them practically.
Addresses common questions about emergency medicine, including the safety of veterinary medications for humans.
Emphasizes the importance of taking action in emergency situations rather than assuming someone else will help.

Actions:

for health-conscious individuals,
Order a quality first aid kit from Bear Paw Tac Med (suggested)
Educate yourself on emergency procedures and first aid techniques (implied)
Familiarize yourself with the contents of your first aid kit (implied)
</details>
<details>
<summary>
2018-12-15: Let's talk about what an iconic photo can tell us about Border Patrol.... (<a href="https://youtube.com/watch?v=yPAf6oc8Drc">watch</a> || <a href="/videos/2018/12/15/Lets_talk_about_what_an_iconic_photo_can_tell_us_about_Border_Patrol">transcript &amp; editable summary</a>)

Beau recounts a Vietnam War story, criticizes Border Patrol agents for inhuman actions, and urges them to quit before being held accountable.

</summary>

"You proud of yourself?"
"Your badge is not going to protect you from that."
"Just following orders isn't going to cut it."

### AI summary (High error rate! Edit errors on video page)

Recounts a story from the Vietnam War, referencing a Viet Cong trooper who continued fighting despite severe injuries by strapping a bolt to himself.
Mentions the iconic photo from the Battle of Saigon where American troops showed humanity by giving water to their opposition.
Criticizes the inhuman actions of Border Patrol agents who dump out water left by volunteers for migrants crossing dangerous routes.
Compares the Border Patrol agents' actions to torturing videos posted by teenagers and questions their morality.
Points out that deterrence methods like dumping water won't stop people seeking safety, freedom, and opportunities.
Urges Border Patrol agents to quit their jobs, as they will eventually be held accountable for their actions.
Emphasizes that blindly following orders is not an excuse, as history has shown that defense doesn't hold up.

Actions:

for border patrol agents,
Quit your job at Border Patrol (implied)
</details>
<details>
<summary>
2018-12-14: Let's talk about social issues and comic books.... (<a href="https://youtube.com/watch?v=GTo9Xs5gpWQ">watch</a> || <a href="/videos/2018/12/14/Lets_talk_about_social_issues_and_comic_books">transcript &amp; editable summary</a>)

Beau lays bare the longstanding tradition of addressing social issues in comics, challenging fans to see beyond the surface and confront their privilege amid societal changes.

</summary>

"Social issues have always been in comics. You were just too dense to catch it."
"If you have a privilege for so long, and you start to lose it, it feels like oppression."
"Two chunks of plastic have had more of an impact than the actual stories of people who underwent some pretty horrible things."
"There is no tyrannical, feminist regime coming for you."
"It's always been there. The reason you're noticing now is because the social issues being addressed are striking at the heart of the comic book industry's core audience which is white males."

### AI summary (High error rate! Edit errors on video page)

Using Elf on the Shelf to address social issues has sparked anger and controversy on social media, particularly among those who are critical of feminists addressing social issues.
Beau shares his perspective on how comic books like Punisher, G.I. Joe, X-Men, and Superman have always addressed social issues, despite some fans missing the underlying themes.
He points out that even seemingly masculine-themed comics like Punisher and G.I. Joe actually tackle social issues like PTSD, feminism, and political commentary.
Beau explains how characters in these comics represent deeper themes such as trauma, feminism, and societal commentary.
He contrasts the strong feminist undertones in G.I. Joe with the perception that comics should not address social issues.
Beau challenges the idea that addressing social issues in comics is a recent phenomenon, suggesting that it has always been present but may have gone unnoticed by some fans.
The backlash against using Elf on the Shelf for social commentary is juxtaposed with the lack of empathy towards real-life stories of hardship, drawing attention to the disparity in reactions.
He brings attention to how two plastic characters, Libertad and Esperanza, have sparked more reaction than the real-life stories of individuals facing hardships at the border.
Beau shares a true story behind characters Libertad and Esperanza, shedding light on the real struggles that inspired his use of Elf on the Shelf for social commentary.

Actions:

for comic book enthusiasts,
Acknowledge and appreciate the underlying social issues addressed in comic books (implied).
Engage in critical analysis of media content to understand deeper themes and messages (suggested).
</details>
<details>
<summary>
2018-12-13: Let's talk about how I was wrong about asylum seekers applying in the first country they come to.... (<a href="https://youtube.com/watch?v=t3_Nzzl7jd8">watch</a> || <a href="/videos/2018/12/13/Lets_talk_about_how_I_was_wrong_about_asylum_seekers_applying_in_the_first_country_they_come_to">transcript &amp; editable summary</a>)

Beau admits being wrong, sheds light on asylum laws, and warns against relying on memes for legal knowledge, revealing the limited scope of the Safe Third Country Agreement.

</summary>

"Knowledge is power. Don't get your legal information from memes."
"People that know immigration law were so dumbfounded by this widespread belief."
"Unless you're moving along the U.S.-Canadian border. That's the only place this treaty applies."

### AI summary (High error rate! Edit errors on video page)

Admits being wrong about asylum seekers applying for asylum in the first country they come to.
Discovered a EU court ruling stating refugees must apply in the first member state they enter.
Explains the Safe Third Country Agreement between the US and Canada.
Emphasizes the importance of not getting legal information from memes.
Clarifies that the treaty only applies to the US and Canada, not other countries.
Mentions that Canada is considering scrapping the agreement due to the treatment of asylum seekers and refugees in the US.
Trump tried to designate Mexico as a safe third country, but the attempt was laughed at globally.
Points out the dangers and lack of safety standards in Mexico, making it unsuitable as a safe third country.
Expresses astonishment at the widespread belief that asylum seekers must apply in the first country they arrive in.
Concludes by stressing the importance of knowledge and not relying on memes for legal information.

Actions:

for legal enthusiasts, asylum advocates,
Read and research international treaties and laws (suggested)
Educate others on the limitations of the Safe Third Country Agreement (implied)
</details>
<details>
<summary>
2018-12-13: Let's talk about Cyntoia Brown.... (<a href="https://youtube.com/watch?v=ljIWxvKdjOg">watch</a> || <a href="/videos/2018/12/13/Lets_talk_about_Cyntoia_Brown">transcript &amp; editable summary</a>)

Beau questions the justice system in Tennessee and advocates for clemency for a girl involved in a controversial case, raising doubts and proposing scenarios to challenge perceptions of guilt.

</summary>

"You telling me a sex trafficker doesn't?"
"Send a message that is very fitting for the people of Tennessee."
"So rather than argue her innocence, I'm going to go a different way with it."

### AI summary (High error rate! Edit errors on video page)

Talking about the Brown case in Tennessee where a 16-year-old girl killed a man 14 years ago and was sentenced to 51 years.
Defense argues she was being sex trafficked by a guy named Cutthroat and killed the man in self-defense.
Beau expresses doubt about the defense's story, questioning the rareness of Stockholm Syndrome leading to not turning a firearm on a captor.
Beau expresses disbelief in the state's version of events and criticizes Tennessee's inclination towards punitive justice.
Beau presents scenarios where, legally speaking, the girl could be seen as more guilty, but questions when she did something wrong in the eyes of Tennessee residents.
Advocates for clemency for the girl based on her circumstances, including her age, developmental delays, and years already served in prison.
Emphasizes the girl's rehabilitation efforts in prison and argues for sending her home.
Questions the message the governor sends by not granting clemency, suggesting it could impact how traffickers view consequences in the state.
Encourages the governor to make a decision that represents the values of the people of Tennessee.

Actions:

for governor of tennessee,
Grant clemency to the girl involved in the case (suggested)
Advocate for fair treatment and rehabilitation of individuals involved in similar cases (exemplified)
</details>
<details>
<summary>
2018-12-10: Let's talk about hunting and Sex and the City.... (<a href="https://youtube.com/watch?v=29xS9oOUZ30">watch</a> || <a href="/videos/2018/12/10/Lets_talk_about_hunting_and_Sex_and_the_City">transcript &amp; editable summary</a>)

Beau dismisses hunting stereotypes, criticizes wealthy trophy hunters for fueling poaching markets, and suggests investing in local communities as an alternative to canned hunts.

</summary>

"It's about proving you're a man."
"Number one, that stops this from being just an advanced form of colonialism."
"It's about killing. It's about proving you're a man."
"So alternatively, if you are interested in this topic, my suggestion..."
"They don't want aid, they want business."

### AI summary (High error rate! Edit errors on video page)

Dismisses the stereotype that everyone from the South hunts, noting he used to hunt but stopped due to early mornings.
Distinguishes between hunters who eat what they kill and trophy hunters who go after exotic animals primarily for status.
Criticizes wealthy trophy hunters who go on canned hunts, where guides do all the work and hunters simply take the shot.
Points out that while some claim canned hunts support conservation efforts, the practice actually fuels a market for artifacts and poaching.
Suggests investing the hunting money in local communities instead, promoting self-sufficiency and economic growth.
Proposes alternatives like volunteering with anti-poaching forces or supporting advocates like Kristen Davis for animal conservation.
Recommends supporting organizations like the David Sheldrick Wildlife Trust for elephant conservation efforts.

Actions:

for conservationists, animal advocates,
Invest the hunting money in local communities to support self-sufficiency and economic growth (suggested).
Volunteer with anti-poaching task forces to combat poaching (suggested).
Support organizations like the David Sheldrick Wildlife Trust for elephant conservation efforts (suggested).
</details>
<details>
<summary>
2018-12-08: Let's talk about anti-feminist memes and the patriarchy.... (<a href="https://youtube.com/watch?v=hQ1aNRBdQAY">watch</a> || <a href="/videos/2018/12/08/Lets_talk_about_anti-feminist_memes_and_the_patriarchy">transcript &amp; editable summary</a>)

Beau addresses anti-feminist memes, patriarchy, and the importance of women defining themselves despite societal biases.

</summary>

"All women have to be two things, and that's it. Who and what they want."
"What you think doesn't matter."
"The fact that you don't find her attractive doesn't matter."
"That's your hangup, not hers."
"I believe in diversity of tactics, so whatever, they're drawing attention to a cause that still needs some attention."

### AI summary (High error rate! Edit errors on video page)

Reacts to backlash over wearing a t-shirt with Disney princesses and feminist themes by discussing anti-feminist memes and the patriarchy.
Describes two types of anti-feminist memes: one about women doing traditional household tasks and the other about feminism making women ugly.
Criticizes the shallow mindset of valuing a woman based on her ability to cook, clean, or do laundry.
Suggests that men who have issues with feminism may still be emotionally tied to their mothers rather than viewing women as independent individuals.
Addresses the "feminism makes you ugly" meme, which objectifies and criticizes women based on physical appearance.
Points out the hypocrisy of using a woman's attractiveness as a reason to discredit her feminist views.
Counters the argument that feminism is unnecessary by showcasing the ongoing existence of patriarchy in society, citing the lack of female representation in positions of power.
Challenges the notion that women are not promoted in the workforce due to working less or taking more days off, suggesting societal conditioning and gender biases as contributing factors.
Supports different tactics in advocating for gender equality while acknowledging that radical feminists may not always use methods he agrees with.
Emphasizes that women have the right to be themselves, pursue their goals, and not be defined by others' opinions or insecurities.

Actions:

for gender equality advocates,
Challenge anti-feminist attitudes and memes in your social circles (exemplified)
Support diverse feminist tactics to raise awareness for gender equality (exemplified)
Empower women to define themselves and pursue their goals (exemplified)
</details>
<details>
<summary>
2018-12-06: Let's talk about treating everybody the same.... (<a href="https://youtube.com/watch?v=41RBjXaNC6s">watch</a> || <a href="/videos/2018/12/06/Lets_talk_about_treating_everybody_the_same">transcript &amp; editable summary</a>)

Beau advocates for treating everybody fairly over the superficial equality of treating them the same, pointing out the necessity of understanding and embracing cultural differences for true fairness.

</summary>

"The goal shouldn't be to treat everybody the same. It should be to treat everybody fairly."
"Doesn't take you long to realize treating everybody the same is not treating everybody fairly."
"We demonize education. We demonize learning about other cultures, other religions, whatever."
"In order to treat everybody fairly, you have to know where they're coming from."
"We demonize education. And it's something we need to work on."

### AI summary (High error rate! Edit errors on video page)

Advocates for treating everybody fairly rather than the same to build a just society.
Recounts a personal story about the director of photography for Olin Mills and the realization that treating everybody the same isn't always fair.
Mentions McNamara's morons, illustrating the consequences of treating everyone the same during the Vietnam War.
Shares a personal aversion to Arab males due to a cultural difference in personal space.
Points out the challenge of promoting fair treatment in the U.S. due to a lack of education and understanding of other cultures.
Emphasizes the importance of learning about others' backgrounds to treat everybody fairly.

Actions:

for americans,
Learn about different cultures and religions to understand and treat others fairly (implied).
Advocate for education and cultural understanding in communities (implied).
</details>
<details>
<summary>
2018-12-04: Let's talk about a cop asking me for training.... (<a href="https://youtube.com/watch?v=BmjB7TUroyE">watch</a> || <a href="/videos/2018/12/04/Lets_talk_about_a_cop_asking_me_for_training">transcript &amp; editable summary</a>)

Beau shares insights on critical police training principles, like time, distance, and cover, addressing issues of excessive force and misunderstandings in law enforcement.

</summary>

"Time, distance, and cover. That's exactly what it sounds like."
"Auditory exclusion is the hearing counterpart to tunnel vision."
"The fact that that cop exercised the spirit of the law, rather than the letter of the law, is the only reason he's alive."
"The most dangerous people on the planet are true believers."
"You need to stay away from ideologically motivated people."

### AI summary (High error rate! Edit errors on video page)

Recounts an encounter with cops during a training session where one suggested hitting a suspect in the knee with a baton for being more effective.
Shares his concern about the lack of response from other officers in the room when this suggestion was made.
Describes a cop who reached out to him after watching a video on police militarization, admitting to not knowing certain critical concepts like time, distance, and cover.
Explains the importance of time, distance, and cover in police work, citing examples like Tamir Rice and John Crawford where not applying this principle resulted in tragic outcomes.
Talks about auditory exclusion in high-stress situations and how it can lead to misunderstandings and potentially dangerous actions by law enforcement.
Advises cops to give suspects time to comply with commands and not resort to excessive force due to misinterpretations.
Warns against conflicting commands among cops during apprehensions, which can escalate situations and lead to excessive force.
Mentions the dangers of positional asphyxiation and the importance of understanding it to prevent unnecessary harm during arrests.
Urges cops to wear proper armor and stay behind cover during shootouts to avoid harming others due to stress-induced mistakes.
Shares a personal anecdote about the importance of understanding the spirit of the law over its letter and warns against enforcing unjust laws blindly.

Actions:

for law enforcement officers,
Implement training sessions on critical concepts like time, distance, and cover (suggested).
Advocate for proper armor and cover usage during operations (suggested).
Educate fellow officers on auditory exclusion and its effects in high-stress situations (suggested).
</details>
<details>
<summary>
2018-12-03: Let's talk about globalism.... (<a href="https://youtube.com/watch?v=18fkpRoBP1s">watch</a> || <a href="/videos/2018/12/03/Lets_talk_about_globalism">transcript &amp; editable summary</a>)

Beau breaks down globalism vs. nationalism, exposing them as tools to manipulate and divert attention from true accountability, advocating for a better world governance approach.

</summary>

"All a nationalist is, is a low-ambition globalist."
"It's a method of keeping you kicking down, keeping you in your place."
"I am neither. I'm not a nationalist. I am not a globalist."
"Just my opinion there. There are some pretty smart folks out there."
"You're just being played, and it's turned into a buzzword."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of globalism and how it's often used derogatorily, especially by nationalists.
Mentions that some extremists use "globalist" as a code word for being Jewish.
Describes globalism as a potential new world order with a single government ruling over everyone.
Compares nationalists to globalists, stating they both aim to control and motivate people through different means.
Criticizes the idea of representative democracy and questions if politicians truly represent the public.
Points out that both globalism and nationalism divert people from holding those in power accountable.
Expresses his stance of not being a nationalist or a globalist, advocating for better ways to govern the world.
Defines nationalists as low-ambition globalists who seek power within their own corner of the world.

Actions:

for critical thinkers,
Challenge derogatory terms and stereotypes in your community (suggested).
Question politicians' accountability and representation (implied).
</details>
<details>
<summary>
2018-12-02: Let's talk about courage, Bundy Ranch, and Elf on the Shelf.... (<a href="https://youtube.com/watch?v=CB6fVkSiobM">watch</a> || <a href="/videos/2018/12/02/Lets_talk_about_courage_Bundy_Ranch_and_Elf_on_the_Shelf">transcript &amp; editable summary</a>)

Beau challenges those who typically mind their business to start speaking out for real freedom and equality, urging diverse voices to counter the negative representation by a vocal minority.

</summary>

"Courage is recognizing a danger and overcoming your fear of it."
"Somebody has got to start talking for those people who truly believe in freedom."
"Be that megaphone standing up for freedom. Real freedom."
"You are more important than you can possibly imagine at getting the message out to the people that need to hear it."
"Start lending your voices. You can't just sit on the sidelines anymore."

### AI summary (High error rate! Edit errors on video page)

Defines courage as recognizing danger and overcoming fear, not the absence of fear.
Mentions the Bundy Ranch incident where ranchers faced off with the Bureau of Land Management.
Declines to participate in the Bundy Ranch situation due to his disagreement with their tactics.
Acknowledges Eamonn Bundy's courage for openly criticizing the President's immigration policy to his far-right fan base.
Stresses that rural America supports freedom for all and believes in the Constitution.
Shares a personal story about encountering racist remarks while shopping for Christmas decorations.
Encourages those who usually mind their own business to start speaking out and getting involved in current issues.
Urges people who believe in freedom for all to become vocal and counter the negative representation created by a vocal minority.
Addresses the importance of diverse voices speaking up for freedom and equality.
Encourages Southern belles and young Southern females to use platforms like YouTube to spread messages of freedom and equality.

Actions:

for southern community members,
Start speaking out on social media platforms (suggested)
Use your voice to advocate for freedom and equality (implied)
Encourage diversity in voices speaking out (implied)
</details>
<details>
<summary>
2018-12-01: Let's talk about protesters and human shields.... (<a href="https://youtube.com/watch?v=KQXiKeKpI0A">watch</a> || <a href="/videos/2018/12/01/Lets_talk_about_protesters_and_human_shields">transcript &amp; editable summary</a>)

Beau addresses the use of human shields in protests, drawing parallels to historical events and criticizing the immoral actions of opening fire on unarmed civilians.

</summary>

"You do not punish the child for the sins of the father."
"You do not open fire on unarmed crowds."
"That's America. That's how we make America great, betraying everything that this country stood for."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of protest and human shields in the context of current events in the U.S.
Describes protesters upset with the government for not upholding rights and laws, resorting to violating international norms.
Recalls a historical event, the Boston Massacre, where protesters were met with violence from militarized forces.
Compares the recent events to the Boston Massacre, pointing out the use of human shields and the potential for harm.
Criticizes those cheering on violence against protesters, questioning their support for undermining the Constitution.
Raises concerns about the immoral act of using human shields and opening fire on unarmed civilians.
Expresses disbelief at the debate surrounding these actions and advocates for upholding moral standards.
Condemns betraying the values that America supposedly stood for and calls for reflection on the current state of the country.

Actions:

for activists, protesters, advocates,
Stand against the use of human shields in protests (implied)
Advocate for non-violent methods of crowd control (implied)
</details>
