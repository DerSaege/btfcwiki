---
title: Let's talk about a cop asking me for training....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BmjB7TUroyE) |
| Published | 2018/12/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts an encounter with cops during a training session where one suggested hitting a suspect in the knee with a baton for being more effective.
- Shares his concern about the lack of response from other officers in the room when this suggestion was made.
- Describes a cop who reached out to him after watching a video on police militarization, admitting to not knowing certain critical concepts like time, distance, and cover.
- Explains the importance of time, distance, and cover in police work, citing examples like Tamir Rice and John Crawford where not applying this principle resulted in tragic outcomes.
- Talks about auditory exclusion in high-stress situations and how it can lead to misunderstandings and potentially dangerous actions by law enforcement.
- Advises cops to give suspects time to comply with commands and not resort to excessive force due to misinterpretations.
- Warns against conflicting commands among cops during apprehensions, which can escalate situations and lead to excessive force.
- Mentions the dangers of positional asphyxiation and the importance of understanding it to prevent unnecessary harm during arrests.
- Urges cops to wear proper armor and stay behind cover during shootouts to avoid harming others due to stress-induced mistakes.
- Shares a personal anecdote about the importance of understanding the spirit of the law over its letter and warns against enforcing unjust laws blindly.

### Quotes

- "Time, distance, and cover. That's exactly what it sounds like."
- "Auditory exclusion is the hearing counterpart to tunnel vision."
- "The fact that that cop exercised the spirit of the law, rather than the letter of the law, is the only reason he's alive."
- "The most dangerous people on the planet are true believers."
- "You need to stay away from ideologically motivated people."

### Oneliner

Beau shares insights on critical police training principles, like time, distance, and cover, addressing issues of excessive force and misunderstandings in law enforcement.

### Audience

Law Enforcement Officers

### On-the-ground actions from transcript

- Implement training sessions on critical concepts like time, distance, and cover (suggested).
- Advocate for proper armor and cover usage during operations (suggested).
- Educate fellow officers on auditory exclusion and its effects in high-stress situations (suggested).

### Whats missing in summary

Importance of understanding critical training principles and the potential consequences of not applying them in law enforcement scenarios.

### Tags

#PoliceTraining #LawEnforcement #AuditoryExclusion #UseOfForce #SpiritVsLetterOfLaw


## Transcript
Well, howdy there, internet people.
It's Bo again.
If you know me, you're already laughing, waiting for the story.
If you don't know me, you need to know I used to train law
enforcement.
One day, I was given a class, and I had a cop ask about
hitting a suspect in the knee with a baton, saying that it
was probably more effective.
And I explained, you've got legal, tactical, and medical
The legal and medical liability on that is going to eat you alive.
You don't want to do anything that could be permanently disabling, like hit a joint, unless
your wife is truly in danger.
And he says, well, we write the report.
If you don't know what that phrase means, it means that the cops can pretty much say
whatever they want in the police report and it will be believed, even if they are lying.
It bothered me that he said it.
It bothered me more that I got a room full of cops and nobody said anything against it.
In fact, most of them laughed.
I never taught another class for law enforcement again.
That was 12 years ago.
In that time, I have had a number of officers ask me for training, and they normally get
pretty colorful responses.
This is a little bit different.
A cop watched that police militarization video and he, I don't want to put his details
out there.
He's been a cop for more than five and less than ten years.
Said he'd never heard the phrase time, distance, and cover.
And I guess in the comments section I talked about auditory exclusion.
He'd never heard of that either.
He Googled it and said that now he feels a little bad.
Now what that means is that over the course of his career, he has treated someone more
roughly than he should have, simply because he did not know.
He ends his message by saying, Bo, you're wrong.
We do need militarized training, just not the kind we're getting.
I want to know what you know so I don't hurt anybody.
Okay.
Okay, yeah. You sir, you have my attention. Now you said that if I made this video, you
would share it with every cop that you know. I'm going to advise you against that. I'm
going to advise you not to do that. What you're doing is not going to be received well by
your brethren in the thin blue line. Okay. All right, time, distance, and cover. Now
if you're a civilian, if you're not a cop, you need to watch this anyway because you
were going to become a little bit more critical of law enforcement. Keep in mind this is the
stuff that was being taught a decade ago. So if you see this stuff being inappropriately
applied today, it is entirely on the department. This isn't new research. This has been around
for at least 10 years.
Okay, time, distance, and cover.
That's exactly what it sounds like.
Time, if you roll up on a scene,
you don't know what's going on,
you take a little bit of time to watch the suspect,
you keep your distance, give him some breathing room,
and you stay behind cover
in case he does start shooting at you.
Why do you wanna do this?
How does this work in real life?
How does it play out in real police work?
Go look at the shooting of Tamir Rice.
There's a kid playing in a park.
Cops roll up, pull up feet away from him, get out of the car and kill him, gun him down.
Time, distance, and cover would have saved his life.
John Crawford, guy inside a Walmart, picks up a BB gun from the store that wasn't in
the box, calls his, I think, girlfriend to ask if he could buy it for the kid.
Some idiot walking by calls the cops to make it seem like there's an active shooting going
on. Cops show up, run inside, ignore all of the counter indications, like the fact
that nobody in the Walmart is diving for cover, there are no gunshots, nobody seems
startled at all, except for those people that see the cop running with his
rifle, runs back to sporting goods, rounds the corner, pop-pop, kills him. Time,
distance, and cover would have saved his life. The list of victims goes on and
on and on. Most bad shoots are because this was not applied. Time, distance, and
cover, please start to use it. Auditory exclusion. Auditory exclusion, you guys
are trained in tunnel vision, right? You've heard that term. Your body has
certain biological responses to high stress situations and adrenaline. You're
familiar with tunnel vision because they talk about it and they say yeah you got
to look left or right left to right you got to be very you know aware of it when
you're in a high-speed chase otherwise you'll get tunnel vision. Auditory
exclusion is the hearing counterpart to that. In high stress situations you can
suffer temporary hearing loss or limited hearing. Example of it from your world
guys, walk out to your car, turn the siren on and just sit there. It is loud, it is
annoying, right? When you're in a chase, you hear it? Not really. But you hear that
radio. That is Auditory Exclusion at Work. Okay, so what this means is, let's say you
kick in the door on the house. You got your black mask on, your Oakleys because
you're an operator. You kick in the door and there's that guy standing at his kitchen sink,
confused, traumatized. That's why you do it. That's why you dress that way. It's intimidating.
It is shocking. It is traumatic. It is high stress. He's standing there at the kitchen
sink and you're telling him to put his hands on his head and he's not complying. Well,
obviously resisting. No, he can't hear you. Give him a few seconds, he'll hear it.
Another one of these biological responses is, let's say you roll up on a kid selling
a dime bag. He takes off running. That's completely normal. That is completely normal. That's
not a reason to shoot him. Fight, flight, or freeze. That is, you can't kill somebody
because they succumb to a biological response.
So he takes off running, you give chase, you catch him in an alley, and he's standing there,
and you're yelling at him, put your hands on your head, and he's not doing it.
Give him a few seconds.
All he hears right now is his heart.
He's not, you know, there's no reason to kill him.
To add another dimension to this scenario, your partner's with you, not a cop shows
up, whatever.
yelling at him put your hands on your head get on the ground you're already
messing up you are already messing up if you got a bunch of cops one person
speaks one person gives commands why because two could give conflicting
commands or they could give commands that cannot be complied with at the same
time then somebody in the group of cops believes that he's resisting an
excessive force ensues one person speaks continue this scenario see you got a guy
Maybe he resisted a little bit. Then he ran. What do you do? You got him now, right? Cuff him.
What happens next? He falls, right? Throw him on the ground. Boom! And everybody jumps on him.
And then you're surprised when he dies. You didn't beat him to death. What happened?
It's a misnomer because it isn't normally a lack of oxygen that does it. But if you want to look
look into the research on it and understand the theory, it's called
positional asphyxiation. For you guys, this is what you do. Go run, get winded.
For you guys, most of y'all, that's running out to the mailbox. When you get there,
put your hands in your pockets, lock your elbows, and don't move your feet. Don't
rock back and forth, nothing. Just freeze. It hurts, right? Because your heart's still
So if somebody ran, exerted themselves while they were resisting, now they can't move,
their hands are cuffed, you're laying on them, and they have the added stress that they're
going to jail.
Somebody Eric Garner size, they might die.
Positional asphyxiation.
The next bit is wear your vest.
Wear your vest.
There are a lot of critics of police departments that don't want them wearing plate carriers.
I do.
I want you to have top-of-the-line armor, because it makes you act a little bit less
like a child afraid of their own shadow.
When you're scared, you do stupid things.
You make mistakes.
Wear your vest.
If you are one of the extremely few cops that gets in a shootout, stay behind cover.
Every video I see of you guys in a shootout, you star-ski and hutch it out in the middle
of the road popping off rounds.
You don't hit the guy.
Why?
Because you're stressed.
You're moving.
You're scared.
You're inaccurate because you don't train.
Fire 48 rounds, suspect hit three times.
That's 45 rounds that could have killed some kid, could have killed some single mother,
could have killed some elderly guy.
Stay behind cover.
I'm not telling you this because I give care if you get shot.
I don't want you shooting somebody else because you're scared.
Now the last bit, it's not like the other stuff.
The other stuff is very researchable.
This is more personal advice.
This is more personal advice, it's something I have noticed over the years, and it's something
that you should probably be aware of.
To understand, I need to tell you a story.
Six weeks ago, more or less, a friend of mine is down in the hurricane impact zone.
He's a vet, and he is eight up.
He looks a lot like me, only with long hair, looks homeless.
He's eight up, though.
He has seen more than most, more than anybody really should.
He's got some severe PTSD, and he's got problems with authority.
He's down there cooking food and distributing it, and I can't remember if it was because
he was cooking food without a license or he wasn't supposed to be at the park that he
was distributing it at.
Either way, sheriff's deputy rolls up, and this cop's like, you can't do this.
He's like, I assure you, I can, look, I'm doing it.
And the cop's like, you know, look, you keep doing this,
I gotta take you to jail, you need to shut this down.
The guy goes like this, holds his hands out to be cuffed,
and then puts his hands behind his back.
Cop doesn't want to be the sheriff nodding him.
He's like, look, I get off in four hours,
I got the call, you can't be here
when the next shift arrives.
I'll be back at the end of my shift,
if you're still here, I gotta take you in.
And he left.
The fact that that cop exercised the spirit of the law, rather than the letter of the
law, is the only reason he's alive.
This guy did not put his hand at the small of his back to be cuffed.
That's where his weapon was.
Somewhere in North Florida, there's a deputy that has no idea he was seconds away from
from having his head turned into a canoe.
It's an unjust law.
It's plainly unjust law.
This guy understood that, went with the spirit of the law.
This was to protect people from, you know,
unsanitary conditions.
Chose not to enforce it during the hurricane.
Saved his life.
There are a lot of unjust laws out there.
When you run across somebody that is ideologically
motivated, and they are breaking the law, but the crime has no victim, keep rolling.
Keep rolling.
The most dangerous people on the planet are true believers.
And now that the legislation or legislative branches are passing all these laws that are
putting you face to face with these guys, you need to be aware of it.
Trust me, the riddled vet who's feeding someone after a hurricane, they're going to kill you
so much faster than any drug dealer you have ever met.
You need to stay away from ideologically motivated people.
You need to stay away from the do-gooders.
Because most of the do-gooders today, they're doing it out of anger.
Anyway, just a thought.
To the guy who sent me this message, he said you were going to approach your chief and
try to get him to give you advanced training on this kind of stuff.
I'm going to advise you against that.
You're going to lose your job.
I looked into your department, it's small, going forward and asking for reform, you're
going to get marked.
You need to have somebody outside of that department raise a stink and ask for it.
If the chief goes for it, send me a message, I will come up and give the classes for free.
Anyway, y'all have a nice night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}