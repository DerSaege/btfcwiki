---
title: Let's talk about revolution....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Db7TUd0A9-M) |
| Published | 2018/12/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the comments section's interest in revolution, especially after the Yellow Vest Movement, and activists in the US seeking a revolution.
- Defines a revolution as not just regime change but bringing a new idea to the table, like changing forms of government.
- Notes that Western civilization's trend in revolutions is to decentralize power and give more power to more people.
- Warns about the consequences of a violent revolution in the United States, citing ethnic, racial tensions, and cultural divides.
- Mentions that the US is not self-reliant anymore and how a revolution interrupting transportation could lead to food and supply shortages.
- Emphasizes that violent revolution isn't the only way to change minds; focusing on new ideas is key to the future.
- Talks about the potential devastating impact of a violent revolution in the US and the misguided belief in readiness for such a scenario.

### Quotes

- "A new form of government. Changing forms of government. Something like that."
- "In the United States, a violent revolution will be horrible."
- "You can put a bullet in it, or you can put a new idea in it."

### Oneliner

Beau delves into the implications of revolution, warning against violence in the US and advocating for new ideas to drive change.

### Audience

Activists, thinkers, citizens

### On-the-ground actions from transcript

- Prepare emergency supplies and have a plan in case of disruptions in transportation (implied)
- Engage in constructive dialogues and debates about new ideas for governance (implied)

### Whats missing in summary

Detailed examples and historical contexts mentioned by Beau to support his insights on revolutions and violence.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about revolution.
And we're going to talk about it because it keeps coming up
in the comments section.
And because of the Yellow Vest Movement, there are
activists in the United States wanting to bring that
movement here and hoping that it starts a revolution.
First, we've got to talk about what a revolution is.
And is the Yellow Vest Movement a revolution?
It's not, it's not, guys.
It started as a riot.
And it was what all riots are,
the unheard, the voiceless, lashing out violently.
Some riots have staying power.
They become rebellions.
It's short-lived, and it's looking for reform.
A lot of revolutions are really just a scheme to put somebody else new in charge, what gets
termed a revolution.
It's not really a revolution, that's regime change.
And that isn't always a good thing.
You can look to Egypt as a good example of that, where they got the guy they didn't
like out and the person they got in, well they didn't like him either. So we have
to really define what a revolution is. Now we also have to talk about outside
influence when we're talking about these things. You know, is a foreign government
helping? Is that necessarily bad? Now right now, because of everything that
happened in our last election, Americans are like, of course it's bad. And sometimes
Sometimes it is, sometimes it is.
I would say that the US involvement in Iran in overthrowing their government was a horrible
thing for the Iranian people.
It directly led to the regime they have in power today.
What about the French helping the colonists during the US revolution?
Probably don't think that was a bad thing.
And that was a whole lot more than Facebook posts.
That was ships and guns.
Nations will act in their own best interest, just like any people.
So an outside nation assisting doesn't necessarily mean that it's bad for the people of that
country.
What it means is that the goals of the revolution are in line with the goals of the people in
power in the other country.
That's really all it means.
Maybe it's good, maybe it's bad.
So we've talked about what a revolution isn't.
What is a revolution?
A revolution is bringing a new idea to the table.
That's what it is.
A new form of government.
Changing forms of government.
Something like that.
And throughout Western civilization, the overwhelming trend of revolutions has been to decentralize
power and to give more power to more people. I would suggest that if a revolution occurred
in the United States, that would be the direction it would take. That is the trend of Western
civilization. Now there have been revolutions that have taken a step back from that, but
they're normally pretty short lived when you compare it to the timeline of Western civilization.
The other thing you have to kind of note when you're talking about a revolution in the US
is that you're kissing the United States goodbye.
It won't survive.
And not just in the form of, well, this form of representative democracy won't survive.
But the country itself, the borders will change.
Because when large, culturally diverse, ethnically diverse countries, especially those that have
jurisdictions within them that have power like states when they have
revolutions they they don't survive intact the last major country to have a
revolution we don't refer to it as a revolution we call it the breakup of the
Soviet Union because that is typically what happens so what would a revolution
the US be like. Horrible. All of the different gripes, the polarization in this country,
the ethnic tensions, the racial tensions, the cultural divide, I mean let's be honest,
people from Massachusetts, they should probably already need a passport to come to Mississippi.
It's different.
The odds of building a popular revolution where those groups of people line up and they
agree, it's pretty slim.
It is pretty slim, especially if you're talking about one that centers on violence, and it's
the United States.
At this point in time, at this point in history, a revolution in the United States would be
violent and it would be very violent.
And all of that stuff we just talked about, it's all going to come to a boil.
People always think, well, it can't happen here like that when you talk about mass murder
and violence in revolutions, genocide.
That's something that just happens in those other countries, dominated by people that
don't look like the majority of the U.S., right?
It's just where it happens.
It doesn't happen in Europe, except that it did in the last country that was like the
United States in the 90s, when Yugoslavia broke up.
Same situation, ethnic divides, cultural divides, jurisdictions and land that were predominantly
populated by one type, yeah, that's what happened and for those that don't know, you may not
know that Yugoslavia is what became Bosnia, Serbia, that's what you can expect and if
If you don't know, it got bad, real bad.
And this wasn't a country of those other people, white Europeans.
And there were horrible atrocities that happened during that, and that's really what you
could expect if there was a violent revolution in this country.
The other thing is, you have to keep in mind that the U.S. is not, we're not self-reliant
anymore.
And one of the things that happens during a revolution, especially one like what would
happen in the U.S., which would have to be fought with an insurgency, is that transportation
gets interrupted and people are like, well that's not a huge deal, oh it's a very huge
deal.
If the trucks stop rolling, those 18-wheelers, day one, there's shortages of some kinds
of food.
By day three, there's no food in the stores.
We operate on a zero inventory.
Most big stores, most grocery stores, don't have a back room anymore.
What's on the shelf is that.
Because our transportation system is so reliable, these companies have timed it now to where
As soon as that food shows up, it's on the shelf.
That way they don't have to waste money
dealing with the back room.
It's great for the company.
It is bad if you're talking about an interruption in supply.
So there's no food after three days.
Gas stations, most gas stations need to be filled up every 2
and 1 half days.
Hospitals, they run the same way now.
They don't carry massive inventories of supplies.
So it would get bad.
It would get bad.
Anything that ever triggers the trucks to stop rolling in this country will bring it
to its knees.
So if you're going to change the government, yeah, it can be done with bullets and lamp
posts. If you don't get the imagery, don't Google it. There's two ways to change
somebody's mind. You can put a bullet in it, or you can put a new idea in it. And
that's probably what we need to focus on. In the United States, a violent
revolution will be horrible.
And most of the people who think that they are ready for it, you've got $15,000 worth
of guns, 50,000 rounds of ammo for each one of them, you will die to a.30 bullet.
It doesn't work the way most people think.
Anyway, so what kind of governments could give more power to more people, continue that
trend of Western civilization?
I'm actually interested to hear what y'all say.
Anyway, it's just a thought.
Have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}