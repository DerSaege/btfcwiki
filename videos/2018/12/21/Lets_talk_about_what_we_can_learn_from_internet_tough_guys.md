---
title: Let's talk about what we can learn from internet tough guys....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mY1qVWuFZDY) |
| Published | 2018/12/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the internet tough guy as a symptom and analogy for American foreign policy.
- The internet tough guy's behavior stems from being insulated from consequences, much like American foreign policy.
- Americans can ignore horrible foreign policy because they are shielded by the military machine.
- The insulation from consequences leads to a disconnect from global issues, viewing them as unrelated.
- Addressing the mentality of not needing to help foreigners because one was born in the U.S.
- Exploring the notion that caring from the top income earners could drive global change.
- Money plays a pivotal role in influencing change due to the wealth in certain countries.
- Acknowledging that injustices often arise from massive corporations driven by consumer choices.
- Emphasizing that change starts with individual actions and consumer decisions.
- Stating that any revolution in today's interconnected world must be global to truly bring change.

### Quotes

- "When we see it on TV, we turn it off. That's happening somewhere over there."
- "Money makes the world go round, right?"
- "It does have to be somebody from here."
- "At this stage in the game, the way everything is interconnected, any revolution that is not global, it's not a revolution."
- "It's just a thought."

### Oneliner

Beau delves into the parallels between the internet tough guy and American foreign policy, addressing insulation from consequences and the role of individuals in driving global change.

### Audience

Global citizens

### On-the-ground actions from transcript

- Choose consciously where to spend money to influence corporate behavior (implied)
- Start making changes individually in consumption habits (implied)

### Whats missing in summary

The full transcript provides a comprehensive analysis of the internet tough guy phenomenon, drawing parallels to American foreign policy and advocating for individual responsibility in driving global change.

### Tags

#InternetToughGuy #AmericanForeignPolicy #GlobalChange #CorporateResponsibility #ConsumerChoices


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the internet tough guy
and what we can learn from it.
I know right now you're like, we can't learn anything from them.
We can't.
We can because they are a symptom.
And they're also a perfect analogy.
Why is the internet tough guy the way he is?
Why does he feel like he can get online and just be a pizza
cutter, all edge, no point, and berate people and do whatever he wants because he can, right?
He's insulated from the consequences of his actions.
Insulated, he's behind that machine, in many cases hiding behind that machine.
Who's that sound like?
What does that sound like?
It's American foreign policy.
The reason Americans can back, sometimes just ignore, horrible foreign policy is because
they're insulated.
They're hiding behind that machine, but rather than a computer, it's the greatest military
machine the world's ever known.
They don't suffer the consequences or repercussions of their actions.
That's where it comes from.
It is that insulation.
We've had it so good for so long that it's not real.
When we see it on TV, we turn it off.
That's happening somewhere over there.
Doesn't have anything to do with us because it never does.
It never reaches here and when it does, man, we go around the world invading countries.
them had nothing to do what happened. And then in the same vein you've got that
other person. Well I was born here. I don't know why we need to help these
foreigners. You're right, you were born here. I don't really know what that means.
I don't know that the GPS coordinates of your mother's vagina at the time you
were born, somehow give you magical powers that make you more worthy of a
human being. I didn't know that beyond America's borders lived some kind of
lesser people. And then you got the other kind of person. You got the person that's...
I don't know why it always has to be an American that helps. Why is this even our
problem. You know, I didn't do this. And then there's some fairness to that. But
it may not always be the American that's doing it, but it is normally. When you're
talking about injustice in the world, it normally stems from the wealthy. That
that play for power. If we could just get them to care then all of a sudden be a whole lot easier
to affect change wouldn't it? If we could just get like the top 15 percent of income earners in the
world, get them caring and then change should be easy. $250 a week. If you make more than $250 a
week, you are that top 15%. If you make $615 a week, well, you're in the top 1% of global
income earners. That's why it has to start here. Money makes the world go round, right?
Because of the abundance that this country has and the wealth that has accumulated here,
It is the machinery for change.
It does have to be somebody from here.
And in a way, it is our fault.
It is our problem.
More so than a lot of people realize.
A lot of the injustice in the world is caused by massive corporations trying to feed this
market.
Every time we go to the Walmart or the grocery store or the gas station, we are telling those
massive corporations what kind of world we want we're voting with our debit
cards you know and if you want to change it has to start with us has to start
somewhere has to start with us with you the person watching this video it's got
to start with you and you know there's a lot of talk right now about the yellow
movement, calling it a revolution. Here's the thing, at this stage in the game, the
way everything is interconnected, any revolution that is not global, it's not
a revolution. But that's a subject for another video. Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}