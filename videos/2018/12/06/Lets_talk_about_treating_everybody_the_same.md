---
title: Let's talk about treating everybody the same....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=41RBjXaNC6s) |
| Published | 2018/12/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Advocates for treating everybody fairly rather than the same to build a just society.
- Recounts a personal story about the director of photography for Olin Mills and the realization that treating everybody the same isn't always fair.
- Mentions McNamara's morons, illustrating the consequences of treating everyone the same during the Vietnam War.
- Shares a personal aversion to Arab males due to a cultural difference in personal space.
- Points out the challenge of promoting fair treatment in the U.S. due to a lack of education and understanding of other cultures.
- Emphasizes the importance of learning about others' backgrounds to treat everybody fairly.

### Quotes

- "The goal shouldn't be to treat everybody the same. It should be to treat everybody fairly."
- "Doesn't take you long to realize treating everybody the same is not treating everybody fairly."
- "We demonize education. We demonize learning about other cultures, other religions, whatever."
- "In order to treat everybody fairly, you have to know where they're coming from."
- "We demonize education. And it's something we need to work on."

### Oneliner

Beau advocates for treating everybody fairly over the superficial equality of treating them the same, pointing out the necessity of understanding and embracing cultural differences for true fairness.

### Audience

Americans

### On-the-ground actions from transcript

- Learn about different cultures and religions to understand and treat others fairly (implied).
- Advocate for education and cultural understanding in communities (implied).

### Whats missing in summary

The full transcript provides a deeper exploration of the importance of education, cultural understanding, and fairness in treating others, offering valuable insights into creating a just society.

### Tags

#Fairness #CulturalUnderstanding #Education #SocialJustice #Equality


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're gonna talk about treating everybody the
same, because that's the goal, right?
That is how we are going to build a just society.
We're gonna treat everybody the same.
I used to think that way, and then I met this guy.
That's funny, because he wasn't a philosopher.
He wasn't a social justice advocate, anything like that.
He was the director of photography for Olin Mills.
Now, don't laugh.
I know what the photos looked like in the 70s and 80s.
It looked that way because that's how we looked
and how we dressed and what we were buying.
This guy was a genius, not just creatively,
but in other ways as well, as you're about to find out.
We're standing in the food court of a mall in Albany, Georgia,
and somebody had said that to him earlier in the day.
I'm like, I treat everybody the same.
And he was kind of venting about it, and he's like, man,
I despise that.
It rubs me the wrong way when people say that.
I'm like, treating everybody the same,
that that's a bad thing to you.
And he kind of looked at me, and he started cussing.
And it surprised me, not because I'm a stranger to
course language.
I'm not.
I don't use it in the videos, because it's not appropriate
everywhere. But I had never heard this man cuss before or after. And when he got
done he was like, and if I said that to you it wouldn't bother you at all. Like
no, it would surprise me but it wouldn't really bother me. What if I said it to
Gina? Well, that was a photographer who worked for him. He'd probably be in HR
the next day. He's like, well I'd probably be in the unemployment line but that's
That's not the point.
The point is that's treating everybody the same.
The goal shouldn't be to treat everybody the same.
It should be to treat everybody fairly."
Okay, and I said it like that.
I knew that he had made a great point, but I didn't really grasp it at the time.
Took me a while to really think about it.
If you watch enough of these videos, you know I like to relate things to events that have
happened in history so people can kind of research it and get a good grasp on
it on their own. And this was a hard one. This was a hard one until I thought of
McNamara's morons. Now if you're gonna research it, don't use that term. Use
Project 100,000. McNamara's morons was kind of a nickname. During the Vietnam
War, the US Army needed troops. They needed people. They needed bodies. You
You know, people weren't signing up.
So McNamara got this idea.
Under the guise of treating everybody the same and offering more opportunities to those
less fortunate, he waived enlistment standards, minimum enlistment standards.
Now if you don't know, you don't have to be a genius to get into the army.
Now that's not saying that everybody who joins the army is dumb.
It's saying that you don't need an IQ of 140 to get in.
If you can't cut minimum enlistment standards, you are certainly not going to be able to
cut combat in Vietnam.
The proof of this is the fact that people in this project died at a rate three times
as high as your average soldier.
Doesn't take you long to realize treating everybody the same is not treating everybody
fairly.
an example for my personal life, I had a strong aversion to Arab males. And I know
right now somebody going, see Muslims, bad people. No, it wasn't that. It was that
they were space invaders, not like aliens, like personal space invaders. They stood
really close to you. It took me a while to even realize what it was that bothered me.
It took me a little bit longer to realize it was just a cultural thing. And if I
held them to the same standard treated everybody the same as I would somebody
from down here wouldn't really be fair I mean you know my people we we shake
hands leaning forward because we can't stand too close to each other you know
somebody might think we were gay and that's just a horrible horrible thing
so there's a couple of examples the problem is this concept of treating
everybody fairly rather than treating everybody the same is never gonna catch
on in the U.S. because we demonize education. We demonize learning about other cultures,
other religions, whatever. We're the U.S. We've got the greatest culture in the world.
Baseball games and hot dogs, I guess. In order to treat everybody fairly, you have to know
where they're coming from, which means you have to know a little bit about them, about
their culture, about their religion, about who they are as a people.
Since we don't do that, since we look down on that, it won't catch on.
It's something we need to own as Americans, because it is an American thing.
You know, most languages don't have a word comparable to nerd.
They don't have a word that means you're smart, that's bad, doesn't exist in most
languages. That's purely an American thing. We demonize education. And it's
something we need to work on. Anyway, it's just a thought. Y'all have a good
Good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}