---
title: Let's talk about globalism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=18fkpRoBP1s) |
| Published | 2018/12/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of globalism and how it's often used derogatorily, especially by nationalists.
- Mentions that some extremists use "globalist" as a code word for being Jewish.
- Describes globalism as a potential new world order with a single government ruling over everyone.
- Compares nationalists to globalists, stating they both aim to control and motivate people through different means.
- Criticizes the idea of representative democracy and questions if politicians truly represent the public.
- Points out that both globalism and nationalism divert people from holding those in power accountable.
- Expresses his stance of not being a nationalist or a globalist, advocating for better ways to govern the world.
- Defines nationalists as low-ambition globalists who seek power within their own corner of the world.

### Quotes

- "All a nationalist is, is a low-ambition globalist."
- "It's a method of keeping you kicking down, keeping you in your place."
- "I am neither. I'm not a nationalist. I am not a globalist."
- "Just my opinion there. There are some pretty smart folks out there."
- "You're just being played, and it's turned into a buzzword."

### Oneliner

Beau breaks down globalism vs. nationalism, exposing them as tools to manipulate and divert attention from true accountability, advocating for a better world governance approach.

### Audience

Critical Thinkers

### On-the-ground actions from transcript

- Challenge derogatory terms and stereotypes in your community (suggested).
- Question politicians' accountability and representation (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of globalism and nationalism, urging viewers to question power structures and strive for better governance models.

### Tags

#Globalism #Nationalism #PowerStructures #Accountability #Governance


## Transcript
Well howdy there internet people, it's Beau again.
Today we're going to talk about globalism.
I see, keep saying it in the comments section.
This is part of the globalist agenda.
And it's always a nationalist saying it.
So we're going to talk about globalism, what it is
and what it means when people say it.
Now if you're dealing with somebody of the Nazi ilk
and they say globalist, what they mean
you're a Jew or you're supporting the Jewish agenda. Really what it means
within that community. Kind of code word. Now that's not most people who use the
term though. That's a small subset. Most people mean that you're part of
a new world order or you support it or you don't understand it because it's
going to bring about a global government that's going to have control over that
That entire jurisdiction, one government, is going to have this elite group of people
up at the top, and they're going to make decisions for everybody, and they're going to make whatever
rules they want to, and if you don't abide by it, you're going to have violence visited
upon you.
That sounds horrible.
What's a nationalist?
The exact same thing only on a smaller scale.
It is.
It's the exact same thing.
I know somebody's going to say, well, no, we've got a representative democracy.
Do you?
Your senator represents you?
You really believe that?
Do you believe that those people up on Capitol Hill are truly your peers and they're representative
of the public?
You don't believe that any more than I do.
Both of them, both globalism and nationalism, it's just a method of motivating people through
different means to kick down instead of punching up.
You're going to be mad at the foreigner who's now working in the factory in Bangladesh because
that factory used to be in the US.
You're mad at them because you're a nationalist, the other.
Those other people, they took your job.
You're not mad at the CEO that shipped it over there, huh?
Not mad at the Senator who took a bribe, I mean, campaign contribution to help make
it happen.
It's a method of keeping you kicking down, keeping you in your place.
what it is. I am neither. I'm not a nationalist. I am not a globalist. I think that there are
probably better ways to run the world than in trusting the few to make decisions for everybody.
Just my opinion there. There are some pretty smart folks out there. I think they can come up
with something better than that. So at the end of the day, when you get called this,
You just need to remember, all a nationalist is, is a low-ambition globalist.
Somebody who can't fathom taking control of the entire world.
They just want it to be their little corner of the world.
That's why all of the heads of states, you know, people support them and then they get
up there and these nationalists are like, well, he's turned into a globalist.
He's bought into the globalist agenda.
Of course he did, because it's all about power.
And once you have reached the pinnacle of power in your country, well, you want more.
Where are you going to get it?
You're just being played, and it's turned into a buzzword, and it's so funny to watch
people with identical ideologies to the one they're criticizing, use it as an insult.
If you can give me a difference between a Nationalist and a Globalist, I am all ears.
Anyway, just a thought, y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}