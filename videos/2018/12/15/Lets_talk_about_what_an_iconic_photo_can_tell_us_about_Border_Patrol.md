---
title: Let's talk about what an iconic photo can tell us about Border Patrol....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yPAf6oc8Drc) |
| Published | 2018/12/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts a story from the Vietnam War, referencing a Viet Cong trooper who continued fighting despite severe injuries by strapping a bolt to himself.
- Mentions the iconic photo from the Battle of Saigon where American troops showed humanity by giving water to their opposition.
- Criticizes the inhuman actions of Border Patrol agents who dump out water left by volunteers for migrants crossing dangerous routes.
- Compares the Border Patrol agents' actions to torturing videos posted by teenagers and questions their morality.
- Points out that deterrence methods like dumping water won't stop people seeking safety, freedom, and opportunities.
- Urges Border Patrol agents to quit their jobs, as they will eventually be held accountable for their actions.
- Emphasizes that blindly following orders is not an excuse, as history has shown that defense doesn't hold up.

### Quotes

- "You proud of yourself?"
- "Your badge is not going to protect you from that."
- "Just following orders isn't going to cut it."

### Oneliner

Beau recounts a Vietnam War story, criticizes Border Patrol agents for inhuman actions, and urges them to quit before being held accountable.

### Audience

Border Patrol agents

### On-the-ground actions from transcript

- Quit your job at Border Patrol (implied)

### Whats missing in summary

The emotional impact of Beau's storytelling and his powerful condemnation of inhumane actions by Border Patrol agents.

### Tags

#VietnamWar #Humanity #BorderPatrol #Migrants #Accountability


## Transcript
Well, howdy there internet people it is Bo again
I know what people are expecting me to talk about but before we talk about that
I want to talk about a story from the Vietnam War
Happened there in the Battle of Saigon
There's a Viet Cong trooper and he's hurt bad bad
His belly's opened
So to keep everything where it's supposed to be kind of
He straps a bolt to himself, goes on fighting for three days like that, killing American troops.
Eventually, the GIs catch up to him, produce one of the most iconic photos of the Vietnam War.
And there they are, three of them.
M16's at the low ready, right? Ready to kill this guy.
No. Hold one of these. Hold one of these.
hold one of these because even in the middle of the worst year, the worst fighting, of the worst
war this country's ever seen, they still had the humanity to give their opposition water.
I know right now somebody's going uh Bo that that's from a movie. No Coppola put that in his movie
because he saw the photo. The photo was taken by a guy named Philip J. Griffiths.
Actually happened.
I think people would expect me to rail about the 90 minutes that this little girl waited for medical
attention because a lot can happen in 90 minutes. You can pump a lot of fluid into somebody in 90
minutes. In an emergency medicine situation 90 minutes is the difference
between life and death. But that's not what I want to talk about. I want to talk
about the inhuman garbage that works for Border Patrol that goes around and
dumps these out. See volunteers leave canteens little stashes of water along
these routes, and Border Patrol dumps them out. They actually take videos of it because it's funny.
When this broke, and I saw those videos the first time was a while ago, I couldn't help but be
reminded of those videos that get posted to YouTube by teenagers where they're torturing
autistic kid in the class or whatever. I guess those kids don't change. They grow
up but join Border Patrol. And I know right now somebody's out
there going, well no it's to deter the bad elements. Well genius, the bad
elements, the healthy people, the men you're worried about, they can make it
three days without water. Who you're killing and if you've done this, yeah
Yeah, you've killed somebody, maybe not this girl, but somebody else, her bones are out
in the desert.
Who you're killing?
Well it's the elderly, the sick, and the kids.
You proud of yourself?
Here's the thing, you cannot deter the drive for safety.
wall, no razor wire, no machine gun nests, no landmines, is going to stop the drive
for safety, for freedom, for opportunity. It's not gonna happen. The human spirit
will find a way. You're gonna build a 30-foot wall, 35-foot ladders, they're 450
bucks on Amazon. If you got Prime, they ship for free. You're gonna need two. Fixed
fortifications or monuments of the stupidity of man. If you work for border
patrol you need to quit because eventually this administration is going
to leave office and you are going to be held accountable. Dumping these out in
the statute what that's called is depraved indifference. Your badge is not
going to protect you from that. You need to quit. You need to turn in your
resignation. Just following orders isn't going to cut it. Last time people used
that defense, we hung them. And we don't do that anymore, sadly.
Anyway, this is just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}