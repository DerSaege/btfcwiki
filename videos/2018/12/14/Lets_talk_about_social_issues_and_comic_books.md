---
title: Let's talk about social issues and comic books....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GTo9Xs5gpWQ) |
| Published | 2018/12/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Using Elf on the Shelf to address social issues has sparked anger and controversy on social media, particularly among those who are critical of feminists addressing social issues.
- Beau shares his perspective on how comic books like Punisher, G.I. Joe, X-Men, and Superman have always addressed social issues, despite some fans missing the underlying themes.
- He points out that even seemingly masculine-themed comics like Punisher and G.I. Joe actually tackle social issues like PTSD, feminism, and political commentary.
- Beau explains how characters in these comics represent deeper themes such as trauma, feminism, and societal commentary.
- He contrasts the strong feminist undertones in G.I. Joe with the perception that comics should not address social issues.
- Beau challenges the idea that addressing social issues in comics is a recent phenomenon, suggesting that it has always been present but may have gone unnoticed by some fans.
- The backlash against using Elf on the Shelf for social commentary is juxtaposed with the lack of empathy towards real-life stories of hardship, drawing attention to the disparity in reactions.
- He brings attention to how two plastic characters, Libertad and Esperanza, have sparked more reaction than the real-life stories of individuals facing hardships at the border.
- Beau shares a true story behind characters Libertad and Esperanza, shedding light on the real struggles that inspired his use of Elf on the Shelf for social commentary.

### Quotes

- "Social issues have always been in comics. You were just too dense to catch it."
- "If you have a privilege for so long, and you start to lose it, it feels like oppression."
- "Two chunks of plastic have had more of an impact than the actual stories of people who underwent some pretty horrible things."
- "There is no tyrannical, feminist regime coming for you."
- "It's always been there. The reason you're noticing now is because the social issues being addressed are striking at the heart of the comic book industry's core audience which is white males."

### Oneliner

Beau lays bare the longstanding tradition of addressing social issues in comics, challenging fans to see beyond the surface and confront their privilege amid societal changes.

### Audience

Comic book enthusiasts

### On-the-ground actions from transcript

- Acknowledge and appreciate the underlying social issues addressed in comic books (implied).
- Engage in critical analysis of media content to understand deeper themes and messages (suggested).

### Whats missing in summary

Exploration of the nuanced social commentary embedded in popular culture and its reflection on societal norms and values.

### Tags

#Comics #SocialIssues #Feminism #Privilege #CriticalAnalysis #PopularCulture


## Transcript
Well howdy there internet people, it's Bo again.
Some people are mad about how I'm using Elf on the Shelf
to address social issues.
Let's see it in the shares of the Facebook posts.
I can read the comments sections.
And it's funny to me because it comes on the hills
of that feminism video and everybody's saying
that one of the reasons everybody's mad at feminists
is because they inject themselves in comic books
and address social issues, okay.
So let's talk about comic books tonight, that seems weird.
I didn't read many when I was a kid, the only two I read with any regularity were Punisher
and G.I. Joe, go figure.
Now if there are two comic books that are completely masculine themed and have absolutely
no room for social issues or feminism, it's those two.
What's Punisher about?
now somebody saying well it's a guy whose family gets murdered and then he
goes on the warpath looking for vengeance and punishing criminals. It's
one way to look at it. Another way is that he's a vet from Vietnam comes back
he's suffered the trauma of war all he wants to do is spend time with his
family they get gunned down so he suffers another trauma so he goes to
what he knows. It's almost like the entire series is actually about how
certain types of trauma, illicit patterns of behavior that can't be turned off.
That would especially be true if that character had some severe psychological issues, nightmares,
stuff like that throughout the entire series.
Almost like that comic is about PTSD or other kinds of trauma.
What about GI Joe?
Who's the only competent person on the Cobra side?
Baroness, the female.
What about the women in G.I. Joe on the good guy side?
All pretty strong women, right?
Don't find a lot of weak women there.
Feminism is actually a very strong undercurrent in it.
Extremely strong.
And they address social issues throughout the entire series.
And it's funny, because in the 80s, there was the cartoon
that everybody watched.
A lot of people write it off as Cold War propaganda.
And if you pay attention, those writers,
extremely anti-establishment.
Um, Cobra worked with indigenous groups,
accepted people of all ethnicities,
every socioeconomic class, from the white trash
in the swamps of Louisiana to the European aristocracy.
Their goals included stuff like ending
Federal Reserve, embracing renewable energy, they embraced science of all kinds.
They actually used the bad guys to address a lot of social issues in the cartoon.
And then the reboot, GI Joe Renegades, the GI Joe team is now outlaws because the legal
system has failed, and their opponent, Cobra, is now no longer a terrorist organization,
it's a defense contracting firm.
If you pay attention, that's Halliburton, always addressing social issues.
What about X-Men?
I never read it with any kind of regularity, but I know enough about it to know that the
civil rights of the mutants are central to the plot so much so that there's a lot of
people that think it's an allegory for being gay and that there's another group
of people who think it's an allegory for the American Civil Rights Movement
because there was the group that wants to be militant and a group that wants to
play within the rules. I don't see why it couldn't be both but I haven't read
that. I don't know the details to the story. What about the granddaddy of them
all, Superman. The refugee alien living under a false identity, taken at an
American job. Those to the theories of the time that immigrants were the
bravest and boldest of their village. They were people that would not settle
for a substandard life and would risk everything in the pursuit of freedom and
opportunity and that they made the country better. Yeah guys, social issues
have always been in comics. You were just too dense to catch it. It's always been
there. The reason you're noticing now is because the social issues being
addressed are striking at the heart of the comic book industry's core audience
which is white males.
You know, if you have a privilege for so long,
and you start to lose it, it feels like oppression.
It's not.
There is no tyrannical, feminist regime coming for you.
Yeah, I'm using Elf on the Shelf in a way
that I'm certain the creators did not intend.
That is true.
But at the same time, the responses,
I love it, it cracks me up.
You guys, you come to the page, and y'all are all
internet tough guys, I would stay and fix my country.
I'd fight the gangs.
Meanwhile, you're having a mental breakdown over two
plastic characters, because suddenly they're named.
Those two characters are now more human than the actual
humans who are going through this down at the border,
you can't simply write them off as illegals. That in itself is social commentary about
the state of this country. Two chunks of plastic have had more of an impact than the actual
stories of people who underwent, who went through some pretty horrible things. And now here's
Here's the part where this is going to get really messed up.
Libertad and Esperanza, that's a true story.
That's a true story.
There's obviously been some changes.
Their names and the inciting incident, that's changed.
But this is more or less a very true story.
This is what happens on a daily basis.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}