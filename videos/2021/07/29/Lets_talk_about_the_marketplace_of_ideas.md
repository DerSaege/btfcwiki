---
title: Let's talk about the marketplace of ideas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KDXPfUKTwQg) |
| Published | 2021/07/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- GOP members held a press conference outside the DOJ, but it was disrupted by protesters with signs and whistles.
- The GOP members left without the press conference happening, leading to cries of First Amendment rights violation.
- Beau clarifies that the First Amendment protects people from government censorship, not from being challenged in public.
- Free speech goes beyond the First Amendment, allowing for open debate and discourse in the public square.
- The incident at the DOJ was an example of the marketplace of ideas in action, where dissenting voices countered the GOP speakers.
- Beau addresses the notion of the "intolerant left" and the tolerance paradox, stating that free speech shouldn't incite violence.
- He points out the perceived hypocrisy of labeling the left as intolerant for countering harmful speech.
- Acknowledging conservatives for raising the hypocrisy suggests an understanding that principles like free speech are not tied to a specific political ideology.
- Beau leaves with a reflective thought on the dynamics of free speech and tolerance in public discourse.

### Quotes

- "What you witnessed was free speech in action. That's the marketplace of ideas."
- "The whole idea behind the tolerant left has to do with this premise that you can't allow free speech to become an incitement to violence."
- "In order for this hypocrisy to exist, there has to be an acknowledgement that free speech, the First Amendment, most of the principles in the Bill of Rights are left."

### Oneliner

Beau breaks down the concept of free speech, the marketplace of ideas, and the tolerance paradox, addressing cries of First Amendment violations and perceived hypocrisy in public discourse.

### Audience

Online activists

### On-the-ground actions from transcript

- Counter harmful speech with more speech by engaging in constructive debate and discourse (implied).
- Advocate for inclusive platforms for open dialogues to foster a diverse range of voices (implied).

### What's missing in summary

Beau's engaging delivery and nuanced breakdown of free speech nuances can be best appreciated in the full transcript.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about free speech,
the First Amendment, the marketplace of ideas,
and all that stuff, and the tolerant left.
We're going to do this because of yesterday's events
and the discussions that followed.
If you don't know what happened, the GOP goofball group
of Gomer, Gosser, Gates, and Green decided to hold a press
conference outside of DOJ on the sidewalk.
It abruptly ended before it really started when people
showed up with signs that were, let's just say,
less than flattering about those speaking.
And people started blowing whistles and asking questions
the speakers apparently didn't want to answer. They wound up leaving. The press
conference never occurred. This of course prompted cries about the First
Amendment being violated. No it wasn't. Almost anytime somebody says my First
Amendment rights were violated, they weren't. The First Amendment protects
people from the government, from government censorship, and that's one of
the elements and that's the one that most people often point to. I would just
remind everybody it's designed to protect the people from Congress, not the
other way around. These were all members of Congress acting as agents of
government in the public square. There was no First Amendment violation. Now
Now you can talk about the idea of free speech itself because free speech is a concept beyond
the First Amendment.
Free speech says that, well, you should be able to say pretty much anything and that's
going to foster debate and discussion and it's going to propel society forward and you'll
be able to solve things with debate rather than violence.
And yeah, that's cool.
especially when it takes place in the public square, which is where this did.
What you witnessed was free speech in action. That's the marketplace of ideas.
Nobody there was buying what Gates and his crew were selling. That's how it
works. Nobody has to listen. Nobody has to avoid using their own speech to allow
you to put yours out. If you want to have an event that is regulated and other
people can't talk, you should probably have it in a private venue, not in the
literal public square. What you saw was free speech in action. And then it went
to the intolerant left was the discussion, that was the talking point.
The idea behind this is to point to a hypocrisy, a perceived hypocrisy with the left and not
allowing them to speak.
There's a thing called the tolerance paradox and basically what it says is if you have
people who are using free speech to do things that are going to lead to people being hurt,
can't be tolerant of that. The whole idea behind the tolerant left has to do with
this premise that you you can't allow free speech to become an incitement to
violence. That's the concept. Now that's not what was going on here. Nobody
censored, the government didn't come in and censored this speech. The marketplace
of ideas, the thing that free speech advocates like to point to, decided that it was garbage
and didn't need to be on the market, and they used their own speech to counter it.
But any time that perceived hypocrisy of the intolerant left comes up, I do always want
to acknowledge conservatives for being honest, because in order for this hypocrisy to exist,
There has to be an acknowledgement that free speech, the First Amendment, most of the principles
in the Bill of Rights are left.
Otherwise it wouldn't be a talking point, right?
That's where the hypocrisy comes from.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}