---
title: Let's talk about Pelosi, college debt, and do some supposing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eENhNkIfPSA) |
| Published | 2021/07/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques Speaker Pelosi's stance on forgiving college debt and tax money allocation.
- Raises concerns about paying for corporate bailouts, mass incarceration, surveillance state, and military weapons for law enforcement.
- Points out issues with large corporations using SNAP as a subsidy to pay poverty wages, perpetuating poverty and class division.
- Emphasizes that universal access to education is opposed by those in power to maintain the class divide.
- Shares a personal story of a disadvantaged high school student with Ivy League potential but lacking financial means.
- Questions the unfair advantage given to wealthy Ivy League students in the job market, perpetuating the cycle of wealth.
- Advocates for tax money being used in the public interest to benefit society as a whole.
- Stresses the importance of universal higher education, regardless of degree type, for the country's advancement and societal benefit.

### Quotes

- "Universal access to higher education benefits all of society."
- "Higher education is good for the country."
- "Universal education, universal higher education, is something this country needs."

### Oneliner

Beau critiqued Speaker Pelosi's stance on forgiving college debt, advocating for tax money in the public interest and universal higher education.

### Audience

Taxpayers, Education Advocates

### On-the-ground actions from transcript

- Advocate for tax allocation towards public interest needs (implied)
- Support initiatives for universal higher education (implied)

### Whats missing in summary

The full transcript provides a thorough breakdown of the implications of tax money allocation, higher education accessibility, and class division in society. Viewing the full transcript offers a comprehensive understanding of Beau's critical analysis.

### Tags

#TaxMoney #HigherEducation #ClassDivide #PublicInterest #Advocacy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to do some supposing
with Speaker Pelosi
because she was talking about
forgiving college debt
and she said something that, wow, it rubbed me wrong.
She said suppose
that your child doesn't want to go to college right now
but your tax money
is going to
relieve the obligation of that debt for other people.
Okay,
alright, fair enough.
That's how we're going to do it.
Okay, suppose
that
I don't want to pay for corporate bailouts.
Suppose I don't want to pay for mass incarceration.
Suppose I don't want to pay for surveillance state. Suppose I don't want to pay for military
weapons that end up in the hands of local law enforcement.
Suppose I have no problem paying for something like SNAP
but I do have a problem with that being used as a corporate subsidy so large
corporations can pay their employees poverty wages, therefore continuing the
cycle of poverty
and reinforcing that class divide and that is what this is about.
Make no mistake about it.
The reason those in power are bettors,
don't want universal access to education,
is because it reinforces a class divide.
And the thing is,
you don't need an Ivy League degree to know this.
Pelosi,
while she's saying
that it may not be fair to some people,
she tells a story about a high school student that came into her office
that had the grades to go to an Ivy League school but didn't have the
financial ability.
So what happens there?
What happens in that case?
This is a person who was probably disadvantaged,
didn't have the advantages
that many people who go to Ivy League schools had,
but they excelled.
This is a driven person
but they don't go to one of those schools.
They go to a public school.
Then,
when
they go to apply for a job,
who gets the job?
This student
or the Ivy League student?
Could afford to go.
The children of the wealthy.
Some senators can't.
So then that cycle continues. It reinforces the class. Those six and
seven figure jobs,
they are reserved
for the children of the wealthy.
So they stay wealthy.
So that class divide continues.
Suppose
I don't want my tax dollars
going to pay the salaries of those people attempting to perpetuate that
class divide.
The test
when it comes to taxes,
it's really simple,
promote the general welfare.
Is that use of money?
Is it in the public interest?
If you are going to force people to pay money into the government,
that money should be used in the public interest.
The test is not whether it benefits an individual person who paid in money.
That's not how it works. Does it benefit society as a whole?
Higher standards of education does.
Universal access to higher education
benefits all of society and it doesn't matter if it's a STEM degree or a liberal
arts degree.
Higher education is good for the country.
You don't need an Ivy League degree to know that.
But the people that are going to make the decision,
they're the ones who can afford the Ivy League degree.
That they didn't have to compete
with the poor kids.
The kids who couldn't afford
to get into the school,
well, they don't even have to worry about competing for them for acceptance
because there's that class barrier.
Universal education, universal higher education,
is something this country needs.
Desperately needed
in the United States.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}