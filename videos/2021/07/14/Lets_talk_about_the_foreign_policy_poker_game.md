---
title: Let's talk about the foreign policy poker game....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=76hMRb3-r_A) |
| Published | 2021/07/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A viewer shared a story of a heated argument with his dad over Trump's decision to leave Afghanistan.
- The viewer initially disagreed but later found Beau's video echoing his dad's opinion.
- Beau clarifies that being anti-war and anti-imperialism is morally right but irrelevant in foreign policy.
- Foreign policy is about power, not morality, ethics, or ideology.
- He compares foreign policy to an international poker game where everyone cheats, and every move impacts the next.
- Beau explains that foreign policy changes rapidly, rendering old slogans ineffective quickly.
- The U.S.'s decision to leave Afghanistan may lead to severe consequences for civilians due to the power shift.
- Trump's mistake was setting a hard date for leaving, prompting opposition to wait until the U.S. exits.
- Beau commends the opposition's strategic restraint and knowledge despite common misconceptions.
- The absence of a regional security force post-U.S. withdrawal could escalate conflict.
- The failure to establish a security force might result in increased casualties after the U.S. leaves.
- Being anti-war is still valid, but Beau stresses the importance of preventing conflicts before they start.
- The aftermath of U.S. interventions often leaves chaos, reinforcing the need to prevent involvement.

### Quotes

- "Being against war, being against imperialism, military adventurism, those are the right stances."
- "Foreign policy is about power. It's not the way it should be, but it's the way it is."
- "The reality is, yeah, it's probably going to get real bad over there when the U.S. finally pulls out."
- "You want to be effective at being anti-war, you got to stop them before they start."
- "Doesn't mean that you're wrong. You are without a doubt 100% correct."

### Oneliner

Beau clarifies that being anti-war is right morally but irrelevant in foreign policy, urging prevention rather than intervention to avoid chaos.

### Audience

Policymakers, Activists, Citizens

### On-the-ground actions from transcript

- Prevent conflicts before they escalate by advocating for diplomatic solutions (implied).
- Support organizations working towards peacebuilding and conflict prevention (implied).

### Whats missing in summary

The full transcript provides a comprehensive understanding of the intricate dynamics of foreign policy and the consequences of military interventions.

### Tags

#ForeignPolicy #AntiWar #Prevention #ConflictResolution #USIntervention #PowerDynamics


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about a little update
on what's going on over there.
I'm going to do this because a couple days ago I got a very long,
very genuine message explaining stuff that occurred over a year and a half.
Basically a year and a half ago, one of y'all,
he gets into a discussion with his dad, an argument,
because it was about the time Trump announced that he was leaving Afghanistan
and his dad was like, he shouldn't have done that,
that's going to cost the civilians there.
And his son, he's like, no, and it turns into an argument, a big one,
like slamming doors, leaving the house kind of argument.
The next day, the son flips on YouTube, sees a video from me,
which basically said the same thing his dad did.
And he left me some colorful comments, apparently, which he apologized for.
I'd point out that we talk about a lot of hot button stuff on this channel,
it's totally okay to get mad, it's fine, you don't have to apologize for that.
And it goes on to ask if he's wrong, if being anti-war is wrong,
because it looks like that position is going to cost civilians in Afghanistan.
No, no, that is not what this means, absolutely not.
Being against war, being against imperialism, military adventurism,
those are the right stances.
Those are the right stances morally, ethically, ideologically,
those are the right stances.
But this isn't about morality, it's foreign policy.
Morality, ideology, ethics, these things don't have anything to do with foreign policy.
Foreign policy is about power.
It's not the way it should be, but it's the way it is.
And, you know, people, and I'm sure I've used the analogy myself,
but people often say that foreign policy is like an international chess game.
No, it's not. Chess has rules.
Chess has rules, and one game doesn't impact the next.
The game you're playing now isn't influenced by the preceding game.
It is not chess.
It's nothing that simple.
It's more like an international poker game, and everybody's cheating.
That's more fitting, because every card that's dealt changes the options on the table.
You don't know what's in everybody's hand, you don't know what they're up to,
you don't know what's been slid under the table,
and each hand, it impacts the next one, because the balance of power shifts.
There's chips that get moved around the table.
That's the reality of it.
It's never about morality or ethics or ideology.
That's just how it gets framed, so it can be sold to people like us.
But that's not what it's about. It's about power.
One of the things that's really important to note
when you're talking about foreign policy is that it changes constantly.
So talking points, slogans that are good today, three weeks from now,
that idea is garbage. It's not going to work.
Because it's a very fluid thing. It changes very, very quickly.
If you are anti-war, anti-imperialist, whatever, why?
You're going to say, well, because it's wrong. Sure, but why?
Because it impacts the civilians.
The moves that have been made since 2019 or so,
they've led to this, and there aren't many more cards coming up.
The reality is, yeah, it's probably going to get real bad over there
when the U.S. finally pulls out.
The mistake, the card that got dealt, that set all of this in motion,
is exactly what your dad said.
It's not that Trump wanted to leave Afghanistan. That was the right move.
It was saying that he wanted to leave.
It was putting a hard date on it.
Because then the opposition over there, they just fold it.
We'll play the next hand once the U.S. gets up from the table.
And that's what's happening.
There were some other cards that came up along the way,
such as the opposition over there showing restraint,
which I don't know that many people predicted.
As U.S. troop levels dropped there,
there was an increasing likelihood
that the opposition there go on the offensive with the U.S. there.
But they were smart. They were smart.
Contrary to popular opinion and the way they are often portrayed,
the opposition there is not ignorant, unlearned, or unread.
They know what the Tet Offensive is.
One of the questions when we're talking about the regional security force
that needed to get in place before we left,
one of the questions that kept coming up was,
why does a couple thousand troops matter?
If it's only a couple thousand troops, you can remove them
and it won't affect the balance of power.
No, those troops were chips.
They were an anti in the next game.
If the opposition had pulled a Tet Offensive,
a couple thousand troops, that's a speed bump.
They would have rolled right over them.
But having them there was saying the U.S. was in the next hand.
A couple thousand troops come home horizontal
probably reinvigorate the war effort.
Without that token security force there,
there's nothing holding them back.
There's nothing holding them back.
The current administration, the Biden administration,
they attempted to get one.
Doesn't look like it's going to happen
because of other poker games going on.
They looked to a couple of different countries to try to get one,
but it doesn't look like it's going to happen.
So yeah, the most likely scenario is that at three to six months
after U.S. withdrawal, there will probably be more lost
than in the last two years there, three years there.
That's the reality.
That is the reality because the U.S.
is really good at getting into stuff like this.
Really bad at getting out.
The U.S. military can evaporate any other conventional military
on the planet in very short order.
After that, we don't have a clue anymore.
We don't know what to do.
This reality, what's going to happen,
the footage that has already started to come out
and what will come out,
I understand what you're saying right now is a warm up.
It's not even close to what's likely to occur.
This doesn't mean that being anti-war is wrong.
Not at all.
It shows the stakes of not pulling it off.
Everybody today says we never should have been there.
Yeah, but 20 years ago, the same people saying
we never should have been there, they voted for it.
They were in favor of it.
Cards got dealt.
Positions changed.
You want to be effective at being anti-war,
you got to stop them before they start.
Because once you're through the door,
once the U.S. has inserted itself somewhere,
normally when it leaves, it leaves a giant mess.
And that's what's going to happen here.
It doesn't mean that you're wrong.
You are without a doubt 100% correct.
Being anti-war, being anti-imperialist,
anti-military adventurism, that's the right stance.
Just got to be effective at it.
What you're going to see are the stakes of the entire country
not being anti-war enough.
Doesn't mean that that's the wrong stance.
It most certainly is the right one.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}