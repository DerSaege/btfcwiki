---
title: Let's talk about the separation of church and state....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9uyYh4I0Hp4) |
| Published | 2021/07/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses assumptions about himself and why he remains vague about certain topics.
- Responds to a message accusing him of dismissing concerns about Biden seizing Bibles.
- Explains why he doesn't believe the fear-mongering scenario of the government taking Bibles is valid.
- Points out that faith and church are not dependent on physical Bibles.
- Talks about the separation of church and state and how it doesn't mean excluding Judeo-Christian principles from government.
- Suggests actions based on Christian morals like feeding the hungry and implementing universal healthcare.
- Expresses his belief that many Americans, regardless of religion, support these principles and morals.
- Notes the potential resistance to these principles from those who claim to value them the most.
- Stresses the importance of focusing on teachings rather than symbols like a profile picture.
- Clarifies that he is not affiliated with any political party but leans towards principles and teachings that seem to be more in line with Democrats.

### Quotes

- "I'm pretty sure the only person that can destroy your faith is you."
- "Because without the Bible, the church doesn't exist, right?"
- "When you boil them down, they're kind of all the same. They just say be a good person."
- "You going to lose a prop?"
- "I'm demon-crats."

### Oneliner

Beau addresses misconceptions, dismisses fear-mongering about Biden seizing Bibles, and advocates for actions based on Christian morals, leaning towards principles that seem more in line with Democrats.

### Audience

Socially conscious individuals

### On-the-ground actions from transcript

- Support feeding the hungry and expanding SNAP (exemplified)
- Advocate for universal healthcare to heal the sick (exemplified)
- Be kind to immigrants and ease up on immigration laws (exemplified)
- Focus on teachings rather than symbols like a profile picture (exemplified)

### Whats missing in summary

The full transcript provides a deep dive into addressing misconceptions, clarifying the separation of church and state, and advocating for actions rooted in Christian morals within a political context.

### Tags

#Misconceptions #FearMongering #ChristianMorals #Democrats #SeparationOfChurchAndState


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about, well we're going to talk about a message I got.
You know people make a lot of assumptions about me because I'm vague about a lot of
stuff and I don't go into a lot of different things.
And I tend to just allow people to assume whatever they want because generally speaking
what people think of me isn't really my business.
There are a lot of things that aren't my business.
One of them is religion.
Somebody's religion isn't my business.
So it rarely comes up.
Unless somehow it gets injected into the public sphere, I don't really talk about it.
It's pretty rare that that's a topic on this channel.
But it's going to be one today.
So here's the message.
The way you dismissed concerns over sleepy Joe Biden seizing our Bibles is what makes
you irredeemable scum just like the rest of the Democrats.
Demon-crats, I'm sorry.
You said you didn't run the numbers and just laughed it off, making no attempt to address
this scary possibility.
Religion could take away our Bibles and destroy our church and faith.
What about the separation between church and state you speak of?
What that really means is you don't want Judeo-Christian principles and morals in government.
Just say that, loser.
This is something else.
I'll own part of this.
I did not address the possibility that has apparently scared people of the US government
coming to take your Bibles.
I have also not addressed the scary possibility of being slapped by Bigfoot or being eaten
by the Loch Ness Monster.
Probably not going to run the numbers on those either.
Because it's not going to happen.
This is ridiculous.
It's fear-mongering.
It's not really a thing.
No, I'm not going to address it.
I'm not going to run the numbers on it and figure out the logistics for doing it.
But since we're talking about this, irredeemable scum, even by the great Redeemer, just wondering.
Biden could take away our Bibles and destroy our church and faith.
You are giving that man a lot of power.
Joe Biden can destroy your faith?
I'm pretty sure he can't.
I'm pretty sure the only person that can destroy your faith is you.
Destroy our church.
Because without the Bible, the church doesn't exist, right?
Why don't you take the Glock off the Bible that you have in your profile picture and
actually open the book.
Between Acts and Revelations, you're going to find some letters.
Those letters were sent between early churches.
The letters were sent between early churches before the Bible was compiled.
Follow me on that.
Putting it together, the Bible actually isn't a requirement for a church.
Churches existed before the Bible did.
Just putting that out there.
And then there's this other bit here.
What about the separation of church and state you speak of?
What that really means is you don't want Judeo-Christian principles and morals in government.
Nah, that's not what that means.
That's not what that means at all.
Morals are personal.
Morals are personal.
Your religion may influence your morals, but they're personal.
I'm actually okay with Christian morals.
If you're actually talking about the principles that are contained in the book You're Scared
of Losing, which I'm fairly certain you've never read, let's do it.
I'll support it.
Let's start by feeding the hungry.
Let's expand SNAP.
Cool with that?
Think Mitch McConnell will support it, even with as much as his state relies on it?
Probably won't.
I bet the demon-crats would, though.
I bet they would.
Let's heal the sick.
Universal health care.
How much Republican support you think you're going to get for that?
Probably not a lot.
Let's be kind to the stranger on our land.
Let's ease up on our immigration laws.
Who do you think's going to support it?
Judeo-Christian.
You know, after they left Egypt, they were rewarded.
The slaves, they were rewarded.
It was seen as a compensation for what happened when they built that country.
You down for reparations?
Probably not.
Most of the United States would be just fine with a lot of the principles and morals that
are created by Christianity.
Even people of other religions.
Because when you boil them down, they're kind of all the same.
They just say be a good person.
That's really what it boils down to.
I'm fairly certain that most of America would be supportive of this.
The thing is, you would actually get the most resistance in the United States from the people
who say that they want it the most.
Because I got a feeling they don't care about the principles, the teachings.
They care about the symbols.
Like that stupid profile picture.
What are you worried about losing it for?
You going to lose a prop?
Based on this message, I'm fairly certain you don't read it.
So anyway, aside from that, I would like to point out that I'm not a member of a political
party.
I'm not a Democrat.
But if I'm walking in and just looking and seeing which party actually tries to abide
by the principles, the teachings, I'm pretty sure it would be Democrat.
I'm demon-crats.
I'm sorry.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}