---
title: Let's talk about community networks from Yellowstone to Yukon....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=u1sRwi888Pg) |
| Published | 2021/07/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Community networks start slow and small, like with his attempt to start one at a barbecue where only three people showed up for the first meeting out of forty interested individuals.
- Despite starting small, community networks can accomplish great things, as seen in the Yellowstone to Yukon conservation effort.
- Yellowstone to Yukon aimed to protect and connect a 2,000-mile stretch of land based on animal migration patterns, not geographical boundaries.
- The initiative initially started with only 10% of the land protected and 5% connectivity but has since doubled in protected land and increased connectivity to 30%.
- Yellowstone to Yukon has over 200 partners, including private individuals, conservation groups, and even a mining group in Canada.
- The project defied traditional conservation boundaries like state lines and international borders to protect the interconnected ecosystem.
- Beau encourages building community networks with few people, as even with three individuals, projects can be initiated and successes can attract more participants.
- Real and deep change requires thinking beyond one's lifetime and starting initiatives now, despite time constraints.
- The best time to make a change was in the past, but the next best time is now.
- Building momentum and showing early successes can attract more people to join and contribute to the community network.

### Quotes

- "The best time to plant a tree was 20 years ago. The next best time is right now."
- "Real and deep change requires thinking beyond your lifetime."
- "Just because it is small doesn't mean it can't accomplish things."
- "The more people you have, the more effective it will be."
- "People will be willing to join something that is already moving and already succeeding."

### Oneliner

Community networks start small but can achieve great things, like the Yellowstone to Yukon conservation effort, by defying traditional boundaries and building momentum slowly.

### Audience

Community organizers

### On-the-ground actions from transcript

- Start a community network with a small group of like-minded individuals (suggested)
- Initiate projects within the community network to show early successes (suggested)
- Hold events like barbecues to attract more participants to the community network (suggested)

### Whats missing in summary

Building community networks takes time and dedication, but starting small can lead to significant accomplishments in the long run.

### Tags

#CommunityNetworks #ConservationEfforts #BuildingMomentum #RealChange #CommunityOrganizers


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk
about how community networks are kind of slow going at first, small, and then we're
going to talk about a network that isn't so small, running from Yellowstone to
Yukon. Because I got a message from somebody who's like, you know, hey I love
the idea of community networks. I love all of these philosophies and ideas that
you never actually name and I wanted to start one. So I held a barbecue for
vaccinated people, had 40 people show up, about 10 said they were interested in
starting a community network, and then when we had the first meeting for the
network, three of them showed up. Yeah, that sounds about right. That sounds about right.
Goes on to say that, wish they had started it sooner, because they only have
like 20 good years left in them. Yeah, yeah, good ideas are always better if they
started, you know, a while ago. But just because it is small doesn't mean it
can't accomplish things. Just because it will probably be slow-moving doesn't
mean it can't accomplish things. Back in around 2000, somewhere in there, I heard
about a conservation effort called Yellowstone to Yukon, and it had been
around for like a decade before, and it was wild, pie in the sky type of stuff.
The idea behind it was to connect and protect a stretch of land running from
Yellowstone to Yukon. You're talking about a 2,000 mile stretch of land.
That's pretty ambitious, especially given that at the time, about only 10% of that
land was protected and connectivity was about 5%. When you really think about it,
it's wild. What they did was, rather than looking at geographic features and where
stuff already existed, they looked at where animals went and they used that to
determine the areas they wanted to protect. So rather than just thinking of
it like a park, you're protecting this interconnected ecosystem that is 2,000
miles long. Yeah, and it started with 10% of the land protected, 5% connectivity.
Today, they've got about double that amount of the land protected and
connectivity is up to 30%. The project is ongoing. The people who started it, they
will be gone before it's finished, but because they built the infrastructure,
because they had the good idea, because they got it moving, it'll outlive them. It will
continue to grow even after they're gone. Today, this little initiative, which is
called Yellowstone to Yukon, I want to say they have 200 partners, maybe more.
It's measured in the hundreds and it's not all government. It's not all
government. Some of it's private individuals, some of it's conservation
groups. There's a mining group that is helping up in Canada. The idea, though, is
to protect this ridge of land. Wildly ambitious, and especially for the time.
You got to think, this disregards things that would normally matter when you're
talking about conservation. You know, things like state lines or international
borders, because the animals didn't care about it, neither did the conservationists.
So they came up with this goal and they started working towards it. And I'm sure
over time it's gotten easier. They've been able to get more and more done
because they have more resources, they get more partners. The same thing will
happen with your community network. So you have three people. Yeah, four's better.
Five's better than that. The more people you have, the more effective it will be.
But even with just three people, you can certainly do some of the projects, get
some wins under your belt, and then hold another barbecue. People will be willing
to join something that is already moving and already succeeding. Most people will
pitch in. Yeah, so you only have a limited amount of time left. That's true for
everybody. If you want real change, you want deep change, you have to think
beyond your lifetime. The best time to plant a tree was 20 years ago. The next
best time is right now. Anyway, it's just a thought. You have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}