---
title: Let's talk about Huckabee Sanders and giving up on freedom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pFWzfCdhya0) |
| Published | 2021/07/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into Sarah Huckabee Sanders' statements, the implications, and how some Americans unknowingly surrender their freedom.
- Sarah Huckabee Sanders expressed her desire to be the governor of Arkansas and announced her opposition to mask and vaccine mandates.
- Beau agrees with limited mandates due to valuing freedom and people's ability to self-govern.
- The past year has made Beau question people's self-governing ability due to their actions during the pandemic.
- Beau points out the contradiction in authoritarian followers' interpretation of no mandates as a lack of importance rather than government non-interference.
- Many Trump supporters are aware of his vaccination status and Fox News' vaccine passport but still resist mandates due to their perceived connection with Trump's stance.
- Beau argues that freedom comes with responsibility for one's actions and shared responsibility for others.
- People who submit to authoritarianism often neglect personal responsibility and allow leaders to dictate their choices.
- Beau criticizes those who prioritize perceived wishes of leaders over their own well-being by refusing vaccines or masks.
- Beau warns of the danger of public figures like Sarah Huckabee Sanders making firm statements against mandates in a dynamic public health crisis.
- Beau believes that all public officials should prioritize ending the public health crisis, labeling such statements as hindrances to this goal.
- Beau questions the real intent behind Sanders' statement, suggesting that it aimed to rally her base rather than contribute to resolving the public health crisis.

### Quotes

- "If you hear, we're not going to mandate vaccines, we're not going to mandate masks, and take that to mean is you don't need to wear one, you have given up on everything you say you believe in."
- "You have given up on freedom. You have given up on self-determination. You have given up on the very principles you scream about."
- "People who scream about freedom and have the Constitution as a profile pick have forgotten that they need to provide for the common defense and promote the general welfare."
- "Every candidate right now, everybody in public office right now should have one goal. That is to end the public health issue."
- "Freedom is something that carries responsibility with it. Responsibility for your own actions, shared responsibility for your neighbors."

### Oneliner

Beau warns against sacrificing freedom and personal responsibility in blindly following anti-mandate stances amidst the public health crisis.

### Audience

Americans

### On-the-ground actions from transcript

- Wear a mask and get vaccinated to protect yourself, your family, and your community (implied)
- Prioritize ending the public health crisis as the primary goal for all public officials (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the dangers of authoritarianism and blind obedience, especially in the context of public health crises and political manipulation. Watching the full transcript can offer a deeper understanding of these complex issues. 

### Tags

#Freedom #PublicHealth #Authoritarianism #Responsibility #Vaccination #Mandates


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Sarah Huckabee Sanders'
statements, what they mean, the impacts,
and how some people in the United States
have given up their freedom without ever realizing
that it happened.
So what did she say?
We'll start there.
She wants to be governor of Arkansas.
She came out and said that if she was elected,
she wasn't going to have mask mandates or vaccine mandates.
Now, philosophically, I agree with that.
I don't believe in mandating much because I value freedom.
I value that, and I believe that people have the ability
to self-govern.
Incidentally, nothing has shaken my belief in that
more than the last year.
We need to stop saying, avoid it like the plague,
because it turns out people don't do that,
not if they're authoritarians.
See, that simple statement, I'm not
going to mandate these things, that
seems like a pro-freedom statement.
It really does.
It's to those who actually value freedom.
But if you're an authoritarian, if you have farmed out
your self-determination and handed it over
to your betters, such as Republicans endorsed by Trump,
what does that statement really mean?
Does it mean, oh, the government isn't
going to force me to do that?
Or does it mean that I don't need
to do that because the government doesn't see it as important?
See, this is the double-edged sword of authoritarianism.
Most people who support Trump, they
are fully aware of the fact that he's been vaccinated.
Most people who watch Fox News, they
saw the coverage of Fox Corporation's vaccine passport
type thing, the very thing that Fox News rails against.
They know this.
But if dear leader says, or his subordinates,
those who he endorses, kind of likes, likes for the moment,
liked at one time, whatever, if they say, oh, we're not
going to mandate it, what that really means
is it's not important because they have farmed out
their self-determination.
If dear leader isn't going to order me to do it,
I shouldn't because they've given up.
People who scream about freedom and have the Constitution
as a profile pick have forgotten that they
need to provide for the common defense
and promote the general welfare.
And for those of you with a We the People profile pick,
those are quotes.
I'm fairly certain you all haven't read it.
Freedom is something that carries responsibility with it.
Responsibility for your own actions,
shared responsibility for your neighbors.
If you want to be free, you have to regulate yourself.
You have to govern yourself.
You can't allow the government to determine what is right
and what is wrong, what you should do
and what you shouldn't.
If you want to be free, there's an ethical and moral compass
that goes along with that.
The problem is you have so many people
that have bought into the authoritarian system
of government.
They believe that if the government, if Trump,
isn't going to tell me I have to go do this,
then I don't need to.
I shouldn't, in fact, because it might upset, dear leader,
I'll fail that purity test.
So they don't get vaccinated.
They don't wear a mask.
That's the cost of authoritarianism.
That's the cost of falling for a cult of personality
to the point where you allow their perceived wishes
to determine what you do with your own life,
to determine whether or not you are going to act
in your own self-interest.
Aside from that, there's the obvious impacts
of this to those who have fully bought in to Trumpism.
They see this as a command not to get vaccinated
because only the government can tell them what to do,
because only dear leader has authority over their body.
And since he's not going to mandate it,
since his subordinates aren't going to mandate it,
well, then they shouldn't do it.
And if they do, well, they're bad Trump supporters.
Aside from that, crew, there's something else
that people need to note.
You ever heard sound bites from diplomats,
professional diplomats?
No, because normally what they say
is pretty boring, because they don't say stuff like this.
They talk about accomplishments.
They talk about what they want to accomplish.
They talk about the goals.
They don't talk about what they're
going to do because they are playing that international poker
game where everybody's cheating.
And as soon as they say what they're going to do,
everybody else at the table, well, they
change what they're going to do.
That's another card on the table face up.
So they don't broadcast their plans.
They keep their options open because the situation
is dynamic.
If a candidate locks themself into a position of we
are not going to mandate masks, we
are not going to mandate vaccines,
in a situation this dynamic, they
have absolutely no business making decisions.
What is the current trend of the public health issue?
It's getting more contagious.
What if it also starts to get more lethal?
If that occurs, rest assured, because of this statement,
this hard line statement, we're not going to do this,
if she is in office, she will hesitate to mandate masks.
And when she does, people will be lost.
You want to talk about philosophy
and the pragmatic realities.
That's where they intersect.
That is where the rubber meets the road.
Every candidate right now, everybody in public office
right now should have one goal.
That is to end the public health issue.
What was served by this statement by Sarah Huckabee
Sanders, other than herself?
Nothing.
It didn't further the goal of ending the public health issue.
It made it harder to accomplish that goal.
People like this have no business in office.
The goal of this statement was to get people mad at her
for saying it, so her base would rally around her,
so she could get some support.
So those authoritarians will rally around her, support her,
and in the process, I'm not going to get vaccinated.
That's just doing what they want.
You could do it voluntarily.
You know it's in your own self-interest.
This is spreading among the unvaccinated.
It's in your self-interest.
But you are willing to sacrifice yourself
for somebody who literally does not care about you,
because you have given up on freedom.
You have given up on self-determination.
You have given up on the very principles you scream about.
You have a responsibility to yourself, to your family,
to your neighborhood.
Wear a mask.
Get vaccinated.
Don't let some authoritarian tell you
what's in your own best interest.
If you hear, we're not going to mandate vaccines,
we're not going to mandate masks,
and take that to mean is you don't need to wear one,
you have given up on everything you say you believe in.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}