---
title: Let's talk about executive privilege and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RbAiVv6wtoU) |
| Published | 2021/07/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Executive privilege allows the executive branch to keep information confidential, including the deliberative process and presidential communication.
- The Department of Justice and the Office of Legal Counsel have decided not to protect testimony about Trump's attempts to overturn the election under executive privilege.
- People cleared to testify are related to investigating Trump's alleged use of DOJ to overturn the election, not directly tied to the committee investigating the events of January 6th.
- Allowing testimony from high-level individuals may pave the way for accountability, although real accountability remains uncertain due to the strong drive to protect the institution of the presidency in DC.
- The decision to allow testimony provides a glimmer of hope for potential accountability, although it remains to be seen how it will unfold.
- Trump may try to stop the testimony through a lawsuit, but it is unlikely to succeed given the circumstances deemed to be in the public interest by DOJ and the Office of Legal Counsel.

### Quotes

- "The first rule of working in the White House is to not talk about working in the White House."
- "Real accountability may not be coming."
- "Them allowing the testimony allows for a ray of hope that real accountability may occur."
- "This is paving the way for testimony from high-level people who had conversations with Trump."
- "I doubt that suit [by Trump] would be successful."

### Oneliner

Executive privilege explained, potential accountability ahead, but real consequences uncertain.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Contact your representatives to express the importance of accountability in government actions. (exemplified)
- Stay informed about the developments in the investigations and hold officials accountable for transparency. (exemplified)

### Whats missing in summary

The full transcript provides a detailed explanation of executive privilege and its implications, offering insights into potential accountability in government actions and the challenges surrounding it.


## Transcript
Well, howdy there internet people. It's Beau again. So today we're going to talk about
executive privilege and what it means and we're going to clear up some confusion about
what's happening. Okay, so first, what is executive privilege? Basically, it's the ability
of the executive branch to keep things quiet. The deliberative process is one facet of it
and the idea behind it is that it's in the public's interest for people within the
executive branch to be able to speak freely. And so those records shouldn't become public.
They're protected. Now the other side to it, another facet, is presidential communication,
which is more rooted in the separation of powers. So the first rule of working in the
White House is to not talk about working in the White House. You want to keep the deliberations,
the conversations quiet. The reason Trump was in scandal after scandal after scandal
was because the people he brought in couldn't keep their mouth shut. That had a whole lot
to do with it. The Department of Justice and the Office of Legal Counsel has decided that
testimony from DOJ about Trump's attempts to overturn the election is not going to be
protected by executive privilege. The general idea is that executive privilege is there
to protect public interest and that the extraordinary circumstances suggest that public interest
lies in knowing what happened. Now the confusion that is widespread right now is that this
is clearing the way for people to testify before the committee that's investigating
what happened on the 6th. It may do that, but that's not what was actually cleared.
They were cleared to testify before two other committees, one in the House, one in the Senate,
looking into Trump's alleged attempts to use DOJ to overturn the election. It's an
entirely separate scandal from the one that is dominating the headlines. That being said,
them giving the ability to provide unrestricted testimony to those committees probably means
that the committee investigating the 6th is going to get the same ability. So what does
all this mean? It means we're going to see some high-level people testify about what
occurred. Does this mean that accountability is coming? I don't know. I don't know.
I've been one of those people who is less than hopeful of real accountability for this
because the drive to protect the institution of the presidency is very strong in DC. Many
people want to continue the illusion that the office of the presidency has this just
unimpeachable integrity. So real accountability may not be coming. That being said, I didn't
expect this. I expected executive privilege to be invoked and these hearings to go nowhere.
So them allowing the testimony allows for a ray of hope that real accountability may
occur. We'll have to wait and see, but the key takeaways here are that this is paving
the way for testimony from high-level people who had conversations with Trump and that
it is not necessarily related to the committee that's investigating the 6th. This is a separate
scandal but at the same time, given that they granted it for this, they will probably also
grant it for the 6th. We'll just have to wait and see. I would imagine it would carry over.
Trump himself could probably sue to stop this and probably will. I doubt that suit would
be successful. Not with DOJ and the Office of Legal Counsel saying that the extraordinary
circumstances lead to it being in public interest for this information to become public. I have
to admit I'm curious what it is. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}