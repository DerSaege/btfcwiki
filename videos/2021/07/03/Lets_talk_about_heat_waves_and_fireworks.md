---
title: Let's talk about heat waves and fireworks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vAsRg7_z7ds) |
| Published | 2021/07/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses two public service announcements related to the ongoing drought and extreme heat.
- He clarifies that he is not a former firefighter and shares a firefighter's message about avoiding fireworks during a drought.
- Beau urges viewers to drink plenty of water to stay hydrated in the heat, stressing the importance of water over other beverages.
- He advises against strenuous activities in hot weather, as the body can struggle with the additional heat.
- Beau suggests using a kiddie pool or seeking air-conditioned spaces to cool off during power outages after hurricanes.
- Encourages checking on vulnerable individuals, especially the elderly, who may struggle in extreme heat.

### Quotes

- "Water. Water. First and foremost, water."
- "Your body is going to be a whole lot more taxed by that additional heat."
- "It's not a joke."
- "The heat, it's a literal killer."
- "It literally might save a life."

### Oneliner

Beau shares vital tips on staying safe during a drought, managing extreme heat, and checking on vulnerable individuals in a public service announcement.

### Audience

Community members in drought-stricken areas

### On-the-ground actions from transcript

- Drink plenty of water constantly (implied)
- Encourage others to avoid using fireworks in drought-stricken areas (implied)
- Avoid strenuous activities in extreme heat (implied)
- Provide assistance to vulnerable individuals, especially the elderly, during hot weather (implied)

### Whats missing in summary

Beau's compassionate delivery and practical advice are best experienced through watching the full video.

### Tags

#HeatSafety #DroughtPrevention #CommunityCare #PublicServiceAnnouncement #Vulnerability #WaterSafety


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today is going to be a public service announcement day
because I got a couple of questions and they're both kind of related.
So I'm just going to...
We're going to run through that today because it seems like it might be important.
The first is from a firefighter.
And just in case anybody else is under this impression,
I am not a former firefighter.
I don't really know where that came from, but no, that's not me.
He wanted me to pass along that we are in the middle of a drought.
If you are in a drought-stricken area, please do not use fireworks right now.
I don't feel like anybody watching this channel really needs to be told this,
but perhaps you can encourage others to behave a little bit more responsibly.
If you just feel like you absolutely have to do it because America and freedom and all of that,
do it in a parking lot, somewhere where there isn't something that's going to burst into flames.
And please don't do anything that flies.
That's how a lot of it starts.
I know there's a lot of jokes right now about the fact that some areas have banned fireworks
but have said that they're not going to be enforcing it.
They're not going to be enforcing it because they don't have the resources.
They also don't have the resources to deal with a fire.
So please encourage your friends and family to behave responsibly.
You don't want to be the reason somebody loses their home.
The next is, hey Bo, you're from Florida.
Please tell us how to deal with the heat.
Water.
Water. First and foremost, water.
Drink lots and lots and lots of water constantly.
Not all liquid is the same.
Water.
Drinking a Coke and some coffee is not the same as drinking a liter of water.
Drink water.
Don't exert yourself.
I don't care if you normally run every day.
Now's not the time to do it.
You may not really feel the difference between 90 degrees and 105.
To you it's just hot.
Your body does.
Your body is going to be a whole lot more taxed by that additional heat.
Don't exert yourself right now.
Number three is also water.
Something that is really useful after a hurricane and all the electric is out down here
is a kiddie pool to help cool off.
Take a bath.
Take a shower.
Something like that.
Another option is to exercise any opportunity you have to cool off using somebody's AC.
Because even though your government and your electric company may be conducting brownouts in residential areas,
they're probably not doing it at the grocery store.
Because there's food there that they're not going to want to spoil.
So maybe it's time to go for a grocery trip.
So maybe it's time to go for a grocery trip and take your time.
Cool off.
But don't exert yourself walking there.
And last and probably the most important, check on your old folks.
Check on your old folks or anybody else that may have trouble getting around,
may not be capable of adjusting their environment.
The heat, it's a literal killer.
We're talking, I want to say, 100 people already in the Pacific Northwest.
So take it seriously.
It's not a joke.
You know, I'd like to say we're used to it in Florida, but we're not.
We've just gotten a system and our electric comes back up really quickly here after storms.
So I can tell you right now, I would not want to be in 105 or 108 degree weather
without AC for an extended period of time.
Check on people that can't modify their environment.
It literally might save a life.
So anyway, it's just a thought. You have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}