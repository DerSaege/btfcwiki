---
title: Let's talk about other channels not doing something....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hEdOxn0G0BQ) |
| Published | 2021/07/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Addresses a topic he usually avoids discussing: other people on YouTube and their engagement in charity and mutual aid.
- Expresses his reluctance to join such dialogues due to viewing it as a waste of resources, but acknowledges the necessity this time.
- Talks about channels on YouTube that are left-leaning or anti-authoritarian but do not participate in charity or mutual aid.
- Mentions being sent screenshots of other channels engaging in these activities, which was meant to make him "look good" but didn't.
- Points out a commonality among channels engaging in mutual aid: many were journalists or organizers before YouTube.
- Emphasizes the importance of having a network and team for effective mutual aid efforts.
- Suggests reaching out to channels not engaged in mutual aid to offer assistance or collaboration.
- Acknowledges that some may be engaging in mutual aid for self-serving reasons, but underscores the importance of the end goal of helping those in need.
- Urges not to let ideological differences hinder collaboration for the greater good.
- Encourages utilizing resources from different channels for mutual aid efforts, regardless of their motivations.

### Quotes
- "I'm willing to bet almost all of the channels will. You just got to ask. Reach out to them."
- "You do not have the luxury of being ideologically pure. You need the resources and those channels can help you get them."
- "If you're concerned about it, you probably have that network. So you could team up."

### Oneliner
Beau addresses the lack of mutual aid engagement in certain YouTube channels, urging collaboration for the greater good.

### Audience
YouTube content creators

### On-the-ground actions from transcript
- Reach out to YouTube channels not engaged in mutual aid to offer collaboration (suggested)
- Team up with channels for mutual aid efforts (implied)

### Whats missing in summary
The full transcript provides a detailed insight into the importance of collaboration and utilizing networks for mutual aid efforts, urging individuals to look beyond ideological differences for the greater good.

### Tags
#YouTube #MutualAid #Collaboration #CommunitySupport #Resources


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about something I pretty much never talk about.
Other people on YouTube.
Um...
I generally do not get involved in conversations like this because I...
for me, I don't think I have anything to contribute
and I generally view it as a waste of resources.
I don't like waste.
That being said, in this case,
I think if I don't say something,
there may be a waste of resources.
So I kind of feel compelled to, uh...
to say something about it.
The conversation is about
channels on YouTube that are left-leaning
or anti-authoritarian or both
who don't
engage in
charity, mutual aid, mutual assistance.
These are different concepts but
the lines are fuzzy
unless you're actively involved.
Um...
This channel and a few others were held up as, see, look, they can do it.
And people sent me some screenshots of it.
And I know it was meant to make me feel good.
It didn't. It didn't.
I'm also responding because I don't think the other people
that are being talked about are going to respond for the same reason that I don't want to.
So like Taylor Swift, I would very much like to be excluded from this narrative.
At the end of the day,
if you look at that list
of the channels that do it,
the good channels, the channels that aren't, quote, trash,
um...
they all have one thing in common, at least those that I know. I don't know everybody on the list.
They were all
journalists, organizers
before they were on YouTube.
And I think people recognize that and they see the pattern but they
put that up to, uh...
this just shows they're more committed.
It's not the case.
It's not the case. One of the reasons I feel bad
when I see stuff like this.
Beau did this. No, I didn't. Twenty people y'all never heard of did that.
I was the shipping clerk.
If you're engaged in mutual aid, mutual assistance, you know you can't do it by yourself.
You have to have a team.
You have to have a network.
There's got to be other people.
Um...
the
histories
of the channels that do it,
they have that network.
It already exists.
Not everybody on YouTube
came from that space.
They came from the academic space,
the debate space, the tech space.
They may not have the network
necessary to do it.
So it may not be that they don't care
or that they wouldn't do it.
They don't have the
personnel
to do it.
So
what I'm going to suggest
is that if this is a concern of yours,
reach out to them.
Take a seat if they'll help. I'm willing to bet they will.
I don't think that all of these people
are just self-serving.
I think they may be missing a piece of what they need to do it.
If you're concerned about it, you probably have that network.
So you could team up.
You could reach out.
And sure,
there will be some people,
undoubtedly,
that are just doing it
because they're self-serving.
To get credibility.
Because if
they are self-serving, they'll know that this will make them look good.
And that's fine.
I know you, if you're actively involved in this,
you may not want to do that.
You may not want to boost the profile of somebody who is self-serving
by allowing them to
take credit
for your actions.
Because that's what's going to happen.
But I want to point something out.
If you're engaged
in this kind of stuff, if you're engaged in mutual aid of this type, who are you helping?
The homeless, people in poverty,
people who are hungry,
people who are in a bad way.
The neediest of the needy.
You do not have the luxury
of being ideologically pure.
You need the resources
and those channels can help you get them.
It doesn't matter why they're doing it.
In a lot of cases, with the stuff that
gets addressed
through this method,
it's war.
War on poverty, homelessness, hunger. It is war.
Wars require resources.
Yeah, I know you may not like it.
But it doesn't matter.
It doesn't matter why they help.
Just that they do.
And I'm willing to bet
almost all
of the channels will.
You just got to ask. Reach out to them.
I don't think
that the six or seven channels that got listed
are exceptional.
I think we just had the networks.
So, uh,
I kind of feel like some of the other people on YouTube are taking heat
for something maybe they shouldn't.
It's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}