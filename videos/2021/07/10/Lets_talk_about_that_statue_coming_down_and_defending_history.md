---
title: Let's talk about that statue coming down and defending history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fwNzq6BIng4) |
| Published | 2021/07/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau has been waiting to talk about a specific statue in Charlottesville since 2017, which has now been removed.
- The statue of Robert E. Lee was defended in the past under the guise of defending history, but its true history isn't accurately represented by that defense.
- The statue was commissioned around the same time as the film "Birth of a Nation," which glorified the Ku Klux Klan.
- The motivations behind the statue's commissioning aren't entirely clear, but it was part of a broader context that included segregation and oppression.
- Washington Park, another project by the same commissioner, was also intertwined with racial segregation.
- The statue symbolized more than just the Civil War; it had ties to celebrating slavery and segregation.
- Statues are not just historical artifacts; they are political statements that convey specific messages.
- Defending a symbol without fully understanding its implications may mean upholding problematic ideas and values.
- Beau suggests that it's sometimes better to let go of symbols that represent old and harmful ideas rather than keeping them alive.
- Beau quotes Robert E. Lee, who himself believed Confederate monuments should not exist.

### Quotes

- "Statues are not really history. It's art. Sometimes art's good, sometimes it's bad. But all art is political and it conveys a message."
- "Be certain you are aware of the ideas contained in that symbol."
- "When you're talking about ideas that old and that wrong, maybe it's best to let them go."
- "When somebody has emotionally manipulated you into defending a symbol, be certain you know what that symbol represents."
- "I think it's wiser not to keep open the sores of war."

### Oneliner

Beau has been waiting to address the removal of a statue in Charlottesville, shedding light on its history and the importance of understanding the messages behind symbols.

### Audience

History enthusiasts, activists

### On-the-ground actions from transcript

- Research the history and context behind statues and monuments in your community (implied)
- Engage in dialogues about the implications of symbols and their historical significance (implied)
- Advocate for the removal of statues that glorify oppressive histories (implied)

### Whats missing in summary

Deeper insights into the impact of symbols on society and the importance of acknowledging and understanding their true meanings.

### Tags

#Charlottesville #RobertELee #StatueRemoval #Symbolism #HistoryLessons #Segregation


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about something
I have been waiting years to talk about,
literally years since 2017.
I've been waiting to talk about this,
but it took a while for all of the pieces
to get into place to talk about it
in the way that I want to.
But we can do that now.
By the time y'all watch this,
the statue in question is gone.
It's been removed.
Now of course I am talking about the statue in Charlottesville,
the one that caused all those problems that led to loss.
It's gone.
It's been removed.
Back then, that statue of Robert E. Lee,
people would say, we have to defend history.
That's the battle cry.
That was the idea that caused all of this.
But that statue is now gone.
Let's talk about that history.
Because here's the thing,
if you ask people who were supportive of keeping that statue,
what does it represent?
They'd say something like,
it commemorates the Civil War or something like that.
I'm going to suggest that the acres upon acres of graves
do that for us.
That's enough of a monument
that we don't need the statue.
I'd also point out that, no, that's wrong.
That's not why that statue was commissioned.
That statue was not commissioned two years
after the end of the Civil War.
It was commissioned two years after Birth of a Nation,
a film, a movie.
If you're not familiar with it, it popularized.
A bunch of guys riding around in white sheets
and led to the peak of that little group.
That's the history.
That's the contemporary history of when that was commissioned.
That's what was going on.
That's why the populace wanted it.
That's why it resonated.
That's the history of it.
Now, do we know for certain
that the motivations of the person who commissioned it
were to tap into those feelings?
We don't.
We don't. I don't anyway.
I've looked.
It's hard to find anything that is really specific
about his motivations.
And he commissioned other stuff, Lewis and Clark,
Booker T. Washington, and right then somebody's going to say,
see, look, it's not about race.
It's not about oppression because he, Washington Park.
Yeah, sure, you could read it like that
or you could read it as separate but equal
because that's what it was.
Those parks are an ongoing reminder of segregation
because that's why Washington Park exists.
It's literally in the deed.
It's a playground for the black citizens of Charlottesville.
Terminology is a little different
but that's what it says.
That's the history.
That's what that statue represents.
That symbol.
Even in the initial response of it being about the Civil War,
it glorified the defense of slavery.
The contemporary history was about guys in white sheets
and then today it's an ongoing reminder of segregation.
That's the history.
That is the actual history.
Here's the funny part about it.
The statue's gone.
You didn't need the statue to learn about that.
Didn't need to be there because it's not.
And for many people who defended that statue,
this is the first time you're hearing this.
Statues are not really history.
It's art.
Sometimes art's good, sometimes it's bad.
But all art is political and it conveys a message.
And the message at that time, combined with Washington Park,
you better stay in your place.
That's the message.
And I'm wondering if those who wanted to defend history
were defending that message.
I think a lot of them were.
When somebody has emotionally manipulated you
into defending a symbol, be certain you
know what that symbol represents.
Be certain you are aware of the ideas contained in that symbol.
Because a lot of times in the United States,
the symbol-minded will hold a symbol higher
than the ideas and values it is supposed to represent.
And when you're talking about ideas that old and that wrong,
maybe it's best to let them go.
I think it's wiser not to keep open the sores of war,
but to follow the examples of those nations who
endeavored to obliterate the marks of civil strife,
to commit to oblivion the feelings engendered.
That's Robert E. Lee, by the way,
making it really clear that he didn't
think there should be Confederate monuments.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}