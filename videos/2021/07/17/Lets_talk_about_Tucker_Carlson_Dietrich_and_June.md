---
title: Let's talk about Tucker Carlson, Dietrich, and June....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xmsuhS-RgNo) |
| Published | 2021/07/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A plea to Tucker Carlson to publicly acknowledge his vaccination status to influence hesitant viewers like June.
- Drawing attention to the political divide in vaccination rates based on political affiliation, not medical guidance.
- Trump's initial downplaying of the pandemic and its impact on shaping conservative media's response.
- Criticizing modern news shows for not issuing corrections or admitting when they were wrong, contributing to misinformation.
- Urging people to seek medical advice from professionals rather than relying on screen personalities.
- Offering a firsthand account from a woman who worked in a COVID ward to help persuade vaccine skeptics.
- Calling out wealthy individuals and politicians who spread vaccine misinformation while being vaccinated themselves.
- Encouraging everyone to get vaccinated for the well-being of themselves and their loved ones.

### Quotes

- "It shows that it's purely political."
- "Don't take any advice from people on a screen, myself included."
- "Almost all of them are vaccinated. It's not that they believe any of this stuff they're saying."

### Oneliner

Beau sheds light on the political divide influencing vaccine hesitancy and urges seeking medical advice over media influence.

### Audience

General public, Vaccine skeptics

### On-the-ground actions from transcript

- Contact a doctor or nurse for advice on vaccines (implied)
- Listen to stories from healthcare professionals to understand the importance of vaccination (implied)

### Whats missing in summary

The emotional impact of stories from healthcare professionals on vaccine hesitancy. 

### Tags

#Vaccines #PoliticalDivide #Misinformation #PublicHealth #COVID19


## Transcript
Well howdy there internet people, it's Bo again. So today we're going to talk about Tucker Carlson,
we're going to talk about Dietrich, and we're going to talk about June.
I was on Twitter and I saw somebody reach out to Tucker Carlson and it says,
Hey Tucker Carlson, my mother-in-law watches your show every night and she is hesitant,
understatement to take the vaccine. Could you please say on air that you have taken it. I believe
it would change her mind and I think this could save her life. Her name is June if you want to
say hi. If you look under this it's just comment after comment of people in the same situation.
I've got a feeling Tucker's not going to get on air and admit he's taken a vaccine.
So let me give this a shot.
Miss June, down below in the comments section, the very first comment has two links.
The first link is to a video, it's a short video, it's kind of funny, and it's going
over two maps.
The first map is the 2020 election, and it shows who won which state by how much.
The second map that they talk about, well, that's vaccination rates, and this weird thing
happens.
If Joe Biden captured like 68% of the vote, the vaccination rate, it's about 68%.
If Trump captured 68% of the vote, about 68% of people remain unvaccinated.
It shows that it's purely political.
It is a political divide.
It's not one about medicine.
It's political.
So how did we end up here?
How did this happen?
When this first started, Trump didn't think it was going to be a big deal.
And that's okay.
A whole lot of people didn't.
The second link down there, that's my first video on the topic.
Summarizing, don't even worry about this.
We have like teams of people who specialize in stopping stuff like this.
Y'all chill out.
It's basically what the video says.
The difference is, I don't have a problem admitting I'm wrong.
Trump does.
He downplayed it.
And this isn't me saying that, it's him saying that.
He told Bob Woodward that.
He said he was trying to downplay it.
And when he did that, the media networks that cater to conservatives, well, they followed
his lead.
They downplayed it as well.
And that's why we have that political divide, because we're in echo chambers now.
And they don't want to admit they were wrong either.
The people on these news shows today, they are not like the people from Bob Woodward's
generation.
They don't issue corrections or retractions.
They don't admit they were wrong.
They try to twist whatever it was they said at the time to make it seem accurate today.
They're going to continue to lean into this, but that map, it shows that it's purely political.
It's not a medical thing.
It's politics, and it all boils down to not wanting to admit that they were wrong.
It's really that simple.
Now, if you have questions about the vaccine, if you have questions about medicine in general,
my advice is don't take any advice from people on a screen, myself included.
Talk to your doctor.
Call your doctor.
Better yet, call your doctor's nurse because, and that's not a slight against doctors, nurses
do a lot of patient education.
better at explaining stuff. Talk to them and see what they have to say. And if you
don't have medical professionals that you feel comfortable talking to, have
Dietrich reach out, I will put you on the phone with a woman who ran a COVID ward.
I guarantee you 15 minutes after hearing those stories you will go get your shot.
All these rich folk you see on TV, all these politicians who are leaning
into the idea of not needing a vaccine or spreading misinformation or whatever,
positive. Almost all of them are vaccinated. It's not that they believe
any of this stuff they're saying. They just don't want to admit they were wrong
publicly. That's all it boils down to. You have people in your life that care about
you. Go get your shot. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}