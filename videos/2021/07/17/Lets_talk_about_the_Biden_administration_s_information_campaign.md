---
title: Let's talk about the Biden administration's information campaign....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=M10B1htACU0) |
| Published | 2021/07/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau supports mitigation efforts like wearing masks and getting vaccinated.
- The Biden administration is pressuring social media platforms to remove people spreading disinformation.
- Beau questions the constitutionality of the Biden administration's actions.
- Beau believes that although constitutional, the idea of silencing opposing views is a bad precedent.
- He warns against the dangers of setting a precedent where the executive branch can censor information.
- Beau advocates for counter messaging and providing factual information as a better approach.
- He expresses concern about the lasting impact and dangers of granting such power to control information.
- Beau believes an effective counter messaging campaign is more beneficial than just removing content.
- He stresses the importance of convincing people through ideas rather than silencing opposition.
- Beau acknowledges the potential consequences of allowing such censorship power to be established.

### Quotes

- "It's counter messaging. It's putting out the facts. It's putting out the information."
- "The answer to this isn't necessarily getting rid of this content. It's counter messaging."
- "We have to actually convince people that this is the right thing to do, and not just take away the opposing view."

### Oneliner

Beau supports mitigation efforts but questions the Biden administration's approach to combating disinformation on social media, advocating for counter messaging over censorship.

### Audience

Social media users

### On-the-ground actions from transcript

- Advocate for counter messaging with factual information (implied)
- Encourage open debate and education on public health issues (implied)

### Whats missing in summary

Beau's detailed analysis and concerns about the potential long-term consequences of allowing the executive branch to control information.

### Tags

#BidenAdministration #Disinformation #CounterMessaging #PublicHealth #SocialMedia


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the Biden administration and their
recent attempt to mitigate information that is going out
in relation to public health. Now, before we get into this, I feel like I
should remind people I have hours of content dedicated to
supporting mitigation efforts, from wearing a mask to
getting your shots. I have an entire playlist
about public health. Shaved my beard on camera to convince one person
to wear a mask. I am very much in favor of mitigation efforts.
The Biden administration appears to be putting pressure on
large social media platforms in an attempt to get them to
remove people who are spreading disinformation, the people who are
basically my opposition at this point.
Now, this has sparked a whole lot of conversation,
so we're going to go through each step of it.
First is what the Biden administration did spying. No, absolutely not.
That's silly. It is absolutely not spying.
If you produce something for public consumption,
people watching it is not spying. You're not spying on me right now.
The stuff that is being discussed is in that same vein.
It was intentionally put out. It's not spying.
Do I, Beau, private citizen, do I think it's a good idea if
the people who are putting this information out there,
if they don't have a way to do it anymore? Yeah, I do.
I do. Is what the Biden administration
did constitutional? Barely. Barely, in my opinion.
You know, under Trump, once a week I had to make a video
saying this is unconstitutional. You can't do this.
The Biden administration is inches away from one of those videos.
You can't separate Joe Biden the man from the presidency.
He is an agent of government. So for the executive branch,
for his administration to say this right here,
we're not going to talk about it, it's disinformation.
That, under normal circumstances, I don't believe that's constitutional.
The thing that kind of sways it here is looking at it through the lens of
promoting the general welfare in the preamble, the lens with which you're
supposed to view the Constitution. That certainly applies here.
So I do think it's just on this side of constitutionality, but it is,
it's really close. There are little things in the chain of events
that if the facts were just a little different,
I think it would make it unconstitutional. And to be clear,
this is an argument that there's an opposing argument for.
This is something that would have to be hashed out in court.
Now,
even though it's constitutional, in my opinion,
does that mean it's a good idea? No. No, I don't think it is.
I think it's a really bad idea.
And obviously, I would love for the people, I have to counter message
constantly, I would love for them to not have a way
to put out their message. But the idea that the executive branch
can just say, this is disinformation, it can't be out there,
that's a really bad precedent to set. What's another term for disinformation?
Two words, fake news.
If this precedent is set, it doesn't end with the Biden administration.
The next administration would have that same sway.
That's dangerous. That's dangerous. In this particular instance,
yeah, I agree with it. I think it's a good idea.
But once that precedent is set, it doesn't go away.
And I think we're flirting with disaster here.
The answer to this isn't necessarily getting rid of this content.
It's counter messaging. It's putting out the facts.
It's putting out the information. Yeah, it's hard. It's a fight.
And it goes on. And during that time, there are people who probably
would have got their vaccines if the disinformation wasn't out there.
Yeah. But I think running the risk of handing that kind of power
to somebody like Trump, I think that's a big concern.
An effective counter messaging campaign would be more beneficial to the country
than just taking it off of the major platforms.
Because we've seen this in other situations.
It just goes to lesser platforms. It's still out there.
And with the prevalence of it right now, even if it's gone,
they're still going to believe it. The counter messaging campaign
is still going to have to occur. The work is still going to have to be done.
I'm not one of those people who is...
somebody who says we need to meet them in the marketplace of ideas very often.
But in this case, I think that's the most effective route.
We have to actually convince people that this is the right thing to do,
and not just take away the opposing view.
Now, all that being said, if Twitter and Facebook decided to do it on their own,
I would applaud it. It's not what's happening so much as how it's happening.
It's the administration's involvement that gives me pause.
So this is definitely something that everybody needs to consider.
It doesn't matter where you stand on this topic.
If you think they need to be gone, that's fine.
But definitely weigh the fact that if this precedent is set,
the next occupant of the Oval Office is going to have that same power.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}