---
title: The roads with Isiah Holmes, journalist on a list....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wifd7x2tA2E) |
| Published | 2021/07/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing Isaiah Holmes, a journalist with the Wisconsin Examiner from Milwaukee, Wisconsin, who was recently placed on a list by the Wauwatosa Police Department.
- The list included over 200 individuals, including elected officials, lawyers, activists, and Isaiah Holmes, with detailed personal information and connections to their social media profiles.
- The list was shared with the FBI and the Milwaukee Police Department, raising concerns about surveillance and potential infringement on constitutional rights.
- The Wauwatosa Police Department claimed the list was a tool for investigating protests, including individuals who were witnesses or bystanders, not just protesters or suspects.
- Isaiah Holmes had a history of being monitored by law enforcement since his teenage years, including being followed and experiencing odd social media occurrences.
- There are talks of legal action and civil rights implications following the exposure of the list and other questionable actions by the Wauwatosa Police Department.
- Law enforcement responses to ongoing protests in Milwaukee, Wauwatosa, and Kenosha varied, with some departments being hostile and others more restrained.
- The determination and organizing efforts of protest groups like the People's Revolution in Milwaukee have continued despite challenges and lack of media coverage.

Isaiah Holmes says:

- Isaiah Holmes, a journalist, was placed on a list by the Wauwatosa Police Department as part of a surveillance effort targeting protesters and individuals associated with protests in Milwaukee and Wauwatosa.
- The list included detailed personal information about individuals, such as their names, ages, race, gender, affiliations, and even vehicle information.
- Despite being labeled as a journalist, Isaiah was included on the list as a potential witness to protest events, raising concerns about the chilling effect on press freedom and free speech.
- There are suspicions of deeper surveillance and privacy breaches, including tampering with phones and intelligence gathering on lawyers, lawmakers, and journalists involved in the protests.
- Isaiah's history with journalism and experiences of being monitored by law enforcement since his teenage years have added to concerns about privacy and constitutional rights violations.
- The ongoing determination of protest groups like the People's Revolution in Milwaukee and evolving responses from law enforcement indicate a complex and challenging environment for activism and civil rights.
- Isaiah encourages people to be aware of privacy measures like VPNs and password protection, stressing the importance of understanding and defending constitutional rights even if they seem unaffected by surveillance.

### Quotes

- "Even if law enforcement is routinely violating and disregarding the constitution, that matters even if it doesn't negatively impact you, because someday it will impact you and people you care about." - Isaiah Holmes
- "People really need to understand that these things are real and they have real consequences." - Isaiah Holmes
- "Always be informed about privacy. If it means paying for a little bit better antivirus, go for it." - Beau
- "You do lock your doors, right? That means you have things to hide, even if it's seemingly innocuous. It matters." - Beau
- "If it doesn't negatively impact you, someday it will impact you and people you care about." - Isaiah Holmes

### Oneliner

Isaiah Holmes and Beau expose the surveillance tactics of the Wauwatosa Police Department targeting protesters and journalists, raising concerns about privacy, constitutional rights, and the chilling effect on press freedom.

### Audience

Journalists, activists, civil rights advocates

### On-the-ground actions from transcript

- Contact civil rights organizations for support and legal advice (implied)
- Stay informed about privacy measures like VPNs and password protection (implied)
- Support independent media outlets covering civil rights issues (exemplified)

### Whats missing in summary

The full transcript provides a detailed account of the surveillance tactics used by the Wauwatosa Police Department, the ongoing determination of protest groups in Milwaukee, Wauwatosa, and Kenosha, and the broader implications for privacy, constitutional rights, and press freedom. Viewing the full transcript offers a comprehensive understanding of these complex issues.

### Tags

#Surveillance #CivilRights #Protests #Journalism #Privacy #ConstitutionalRights #Activism #Wauwatosa #Milwaukee


## Transcript
Well, howdy there, internet people.
It's Bo again.
And today we have an interesting person with us.
We have Isaiah Holmes,
who has had a unique thing happen to him recently.
He's a journalist, but I'll let him tell you
about who he is.
Isaiah.
Hey, Bo, how's it going?
Yeah, so my name's Isaiah Holmes, obviously.
I'm a journalist.
Right now I'm with the Wisconsin Examiner.
And I live in Milwaukee, Wisconsin.
I'm born and raised in Milwaukee, Wisconsin.
Very proud.
And prior to that, I was actually,
I freelanced at the Fifth Column News for a while,
Pontiac Tribune for a while.
Before that, I was at Cop Block.
That was my first little blogging thing.
And since then, I just kind of moved up the ranks
through the local media and everything.
And now I kind of have a solid gig
that I'm really fortunate for at the Examiner.
Also had a couple of things out in like the Guardian
and Al Jazeera, just miscellaneous stuff.
And I have a whole variety of topics I like to cover
from water quality and the environment to policing.
And I like lacrosse.
That's about it.
All right.
And so something interesting just happened with you.
I saw your name on a list.
So what happened?
Yeah, so that situation is,
so this list was a list of essentially protesters
that was created by the Wauwatosa Police Department.
So I live in Milwaukee and Wauwatosa is a suburb,
city right next to Milwaukee.
That's part of the same county, Milwaukee County.
And basically, just a little context,
Milwaukee is one of, if not the most segregated city
in the country and segregated county.
And Wauwatosa is one of those legacy cities
that has a really long kind of enduring history
with like, because of the red lining and everything,
there are literally streets like here at 60th Street
that are literally not only borders
between Wauwatosa and Milwaukee,
but also like even like the demographics change drastically
from one side of the street versus the other,
even your life in Milwaukee,
your life expectancy can vary by up to 12 years
based off of what zip code you live in.
That's how segregated and segregated Milwaukee is.
And yeah, just basically segregated the area is.
So just a little kind of historical context there.
So, during the protests of 2020,
I was out working for the Wisconsin Examiner
covering the protests that were in Milwaukee and Wauwatosa.
I was also in Kenosha as well.
And this list was a list that was created way,
basically right when all the protests started,
really even before they kind of even got to Wauwatosa yet,
because it took a couple, back last summer,
it took, I'd say almost a month for the protesters
who kind of formed in Milwaukee
to really start to pay attention
to what was going on in Wauwatosa.
So they created this list very, very early on.
It's a list of over 200 people,
a couple of elected officials like state representatives,
both Democrats, lawyers who have represented families
of people killed by police against this department
and are pushing lawsuits against the department.
Many people from various activist groups in the area,
one group called the People's Revolution,
another group called the Community Task Force,
another group called the Milwaukee Alliance
Against Racist and Political Repression,
and me.
And I was put on there with the context of,
I am a journalist with the Wisconsin Examiner.
It said Wisconsin Examiner right next to my name.
The list generally followed kind of the same,
similar, like a kind of a similar theme for everyone.
But for me, for some people it had more information,
for others it had different information,
but for me it had my name, my age,
my race and gender, of course.
It had Wisconsin Examiner right next to my name.
It had my birth date.
It had my driver's license picture
and my Facebook profile picture at the time.
It had a URL link to my personal Facebook page
and then it had another URL link
to our company Facebook page as Wisconsin Examiner.
And then there was a separate section
where it had my car, make and model,
the year, the VIN number, all that stuff.
And they shared that list at some point,
not only with the FBI, but also
with the Milwaukee Police Department,
which has one of the areas
kind of intelligence fusion centers.
So that's kind of the general context
of this list and this situation.
Okay, so there's a couple of interesting things about this
when in comparison to other stuff,
because a lot of people may not know this,
but if you're a journalist, especially if you're freelance,
you end up on these lists.
Your names pop on these lists all the time,
but you, it actually said you were a journalist beside it.
Like that was information that was included in it.
Do they, have they issued a response now that this is open?
Why are they saying that this was created
and why were you on it?
Yeah, so they said that the list,
to use their phrasing, a spokesperson for the department
who is a sergeant in the department,
he said that the list was a tool
that the investigative division within their department,
which has their detectives along with this other unit
of detectives called the,
that call themselves the Special Operations Group.
The list was created as a tool for these individuals
to investigate the protests basically,
and people were included on it,
whether it didn't matter who you were,
whether you were a, according to them,
whether you were a protester,
whether you were a victim of a protest,
whether you were a suspect that they were looking for,
whether you were just kind of a bystander,
they kind of made it seem like essentially anyone
who had contact with the protests
could have been put on this list
because in their view as law enforcement,
criminal activity was occurring at the protests.
And they said I was included on it
because I was out covering the protests
and thus I was a witness to some of the protest events,
whether or not they were illegal.
They said that I was never regarded as a suspect
in by any regards.
And they also said that I was never regarded
as someone who was actually participating in the protests,
but just by virtue of covering the protests and being there
and seeing, say, them walking in the street
or whatever activity they regarded as criminal at the time,
they said that I could have been
a potential witness for that,
which I've never been approached by law enforcement.
I've never had law enforcement come to my house
asking me questions.
I've never been contacted.
It's really, and that's if you take their official response
at face value and believe it, you know what I mean?
And I think the important thing is that
even if you believe their on-the-record response
saying that basically anyone could be put on this list
and it was made essentially
as an investigative reference tool,
that's still questionable on a constitutional basis
because there's the press,
there's a first-minute right to a free press
and our ability to cover the news.
And there's also a first-minute right to free speech,
and this could be seen as a chilling effect.
Oh, absolutely.
I mean, I've had stuff similar to this occur,
but it was kind of one of those things
like once they found out I was a journalist,
like that was kind of the end of the conversation.
I've never, I don't know that I've ever seen it
with a local department targeting somebody,
or I don't wanna say targeting,
gathering information on somebody
who they know was a journalist
because, you know, sure, it's an investigative tool.
If you're talking about a list of people who were there,
this person works at Washington Examiner,
maybe he saw something.
Yeah, I mean, I could buy that,
but why do they have your VIN number?
Right, right.
Right, right, exactly, right.
And then they say,
well, we were just trying to place cars to people
or whatever, which is weird too,
because I always kind of made it a habit
of kind of parking like not where everyone
would park their cars for the protest,
because in Milwaukee, you know,
the protests were very organized
in that there were marchers and they were kind
of these organized car caravans
that would kind of briefly block traffic
so that you don't have another Charlottesville incident,
you know, of someone plowing through protesters
walking in the street.
And I would always, so basically,
like everyone would kind of gather
in say a park or a parking lot or something,
and I would always park my car
like around the corner or whatever,
you know what I mean?
So it's not, I mean, like they were driving
through a parking lot and just getting my information.
So yeah, and then other, like I mentioned,
a couple of people had kind of this phone information
that was pulled on them, which seems to,
and it's only a few of them,
and one of them is one of these lawyers
who was representing cases against the department.
It makes you wonder, were they,
was there some sort of like deeper, you know,
telephonic surveillance going on?
And I'll be honest with you,
a lot of people, myself included,
have suspected that that was the case at,
you know, different points over the last year.
Yeah, I mean, yeah, generally speaking, you know,
I don't, you know, I mean, now I'm just the dude on YouTube,
but you know, when I was actually out doing stuff
where there would be opposition,
people that didn't want that information public,
I just assumed that at all times
my phone was completely compromised.
Right.
And it's not out of, you know, some tinfoil hat,
it's that it happens.
It occurs at times, and you have to be careful.
So the fact that they hit some lawyers
and some lawmakers and some journalists,
I'm wondering, has there been any talk of a suit
or anything like that about this?
Yeah, there's been talks of a,
one of the lawyers who's actually on the list
has called for a civil rights probe of the Wauwatosa
Police Department.
And, you know, it's not just, you know,
just this thing, you know,
this is one thing in a slew of issues
that have happened with this department.
So just to briefly go over a couple of things,
you know, as the protests,
as their actions against the protests started to escalate,
you know, they started, you know, spike stripping cars,
taking pictures of people.
They took several pictures of me,
several pictures of ACLU legal observers.
And we don't know who saw those pictures,
where they were shared.
And they conducting kind of like random kind of arrests
of people kind of just picking a person out of the crowd
and arresting them, seizing phones of people,
especially during the curfew in October,
they were seizing people's phones.
They seized the phones belonging to the family
of one of the people that one of their officers killed,
which actually triggered these protests.
And that was that particular officer's third kill
over a five-year period.
And he was the only officer to be involved
in kind of those repeated fatal incidences.
Usually Wauwatosa police shootings
don't end up actually being fatal.
So people were protesting over that.
And once the curfew happened,
they bared down on a car caravan with that boy's mother
and his sisters in the car
with their special operations group
and their US Marshals Task Force, and they arrested them.
The mother was put in the hospital
because she was injured and they took their phones.
And the sister's phone was held for over 20 days.
As far as anyone can tell from me to the lawyers,
there weren't any warrants issued
for this phone or anything.
And they claimed they didn't try to access it,
but there was some evidence kind of to the contrary of that.
So another thing that that...
Hey, you're locking up.
A person who'd been going to the protests
who works in a separate state legislature's
kind of staff office.
And one moment.
And the last person was actually the mayor
of the city of Wauwatosa.
And these people were put on this high value target list,
which is different from this list that I was put on.
Okay, let me interrupt you for a second.
We had some technical...
You kind of locked up.
I'm not sure if it came through.
The last thing that I know got picked up was
they said they didn't try to access her phone,
but there was some evidence that they did.
And then I lost the feed for a second.
Okay, yeah, so I could just kind of pick up from there
and kind of just finish it up.
Okay.
All right, I'll just do that now.
So yeah, so yeah, picking up from where the phone...
Yeah, so there's some evidence
that maybe the phone had been tampered with.
And then separately, another list had been created
by a special operations group detective,
which was dubbed the higher value target list,
which had a protest leader on it,
two AS state representative, another person who worked
for a separate state legislature or a legislator,
and then the mayor of the city of Wauwatosa.
And they were all put on this list
for their presumed support of Black Lives Matter protests
and communicating with Black Lives Matter protests.
The lawyers were mentioned on that higher value target list,
which is different from this list that I was put on.
So yeah, there's serious talks about
kind of the civil rights implications
of all these revelations and whether or not
some sort of legal action can follow soon.
So there was a list called a higher value target list?
Yes.
Yeah.
So the people that were on the list that you were on,
I'm gonna guess that these were people
who frequented the protest.
Like aside from those that are normally,
or that we would view as shocking,
those who were going to the protest often,
but maybe not leaders, is that who's on that list?
Yeah, and even surprisingly, there are people
who only went to a couple of demonstrations
who were on this list, who only went to a couple
of Black Lives Matter related demonstrations.
But during that summer of protest and once COVID,
because of COVID and people were put out of work,
there was also a pretty significant labor movement
in Milwaukee of workers, laid off restaurant workers,
who were calling for benefits and just getting the story
about the conditions of their restaurants and everything.
And some of them also went to these
Black Lives Matter protests, though not many.
They're on this list.
And like I said, there are people from various organizations
and groups who are on this list,
but sometimes not the whole organization.
So it really, and once again,
there's nearly 200 people on this list, I believe.
So it's just this huge dragnet.
And then again, the reasoning was anyone could potentially
end up on this list, witnesses, victims, suspects, anyone.
Are there any examples, have you been able to see anybody
on the list that was a victim of something?
No.
I didn't think so, yeah.
No, they're all people who were,
even if they went to only a couple of protests,
they're all people who, to be clear,
the Wauwatosa Police Department is backing up
on the notion that this is a protester list,
even though when we first found out that a list existed,
it was because we got an email,
we obtained an email from them through open records,
which mentioned that it existed,
and mentioned that it was being updated and et cetera.
It was called the protester list in that email.
But now the Wauwatosa Police Department
is trying to back up and say,
well, that's kind of an over generalization
of what that list is.
But yeah, there's only people involved in the protests
who are on it.
Yeah, I mean, that sounds like a good PR spin of,
oh, it could be anybody,
but unless they can point to somebody on that list
that actually was a victim
or somebody who was completely uninvolved,
that it just sounds like empty PR.
Exactly.
Well, so if I'm not mistaken,
this is the first time this has happened to you?
That I know of, that I know of,
but I've had, my history with journalism
kind of goes back to my time as a teenager.
The first thing I did while I was still,
I went to high school in Wauwatosa.
And one of the things I did around my senior year
was I made a documentary.
I started filming a documentary.
It took a few years to finish,
but I made a documentary called Speak, Front and Enter,
which is on YouTube.
It's about the Wauwatosa Police Department
and like a crackdown they were doing against teenagers
around my age group at the time.
And some of the tactics they were using
were kind of heavy handed.
And I made a film about that.
So they, I feel like have kind of known me for a long time.
I've been called out by officers at the protests by my name.
And even back when I was a freelancer
and delivering pizzas and freelancing at the same time,
even back in those days, let's say I'm on a delivery,
I would get followed by Tosa police.
And I even had odd social media things happen.
And so there is always a,
and even when I was a teenager,
and because of the crackdown,
lots of kids have cops around their block being followed.
They would stop you and know who your friends were
and all that other stuff.
So there is always a sense that maybe I could be
on some paperwork somewhere in that department.
But in terms of seeing me put on a list as a journalist,
just for doing my job, credentialed journalist on salary
with, and then to share that with the FBI
for whatever purpose,
and you share it with the department
that runs one of the state's two fusion centers.
Yeah, that's a first for me.
It's kind of hard to process.
I gotta admit, I'm proud of you.
No.
No.
No.
No.
No.
So your coverage of the events,
I'm assuming people can find that over on the Examiner,
Wisconsin Examiner?
Absolutely, yeah.
So you can find my coverage of the Wabatosa protests
and the subsequent kind of open records fallout
and all the reveals and everything
on the Wisconsin Examiner.
Urban Milwaukee also aggregates out a lot of our stuff.
We also have a YouTube where we captured a lot of the events
and protests in Wabatosa and also some in Kenosha as well.
So that's under Wisconsin Examiner.
So yeah, absolutely.
Feel free to check out the stuff,
share it around, sharing is caring.
So you're probably pretty plugged in up there.
Since the election, has determination faded
or are we looking at,
do you think there's gonna be more, I guess?
Well, I can say in terms of the protests,
the protests in Milwaukee never stopped.
The protest group, which calls itself
the People's Revolution or TPR,
they've been marching for over 400 days at this point.
And they don't seem to be willing to stop that at all.
And they're doing all kinds of actions.
So the organizing is continuing straight,
that straight line reaching back over a year ago
at this point to the George Floyd marches.
Then there's all kinds of other organizing going on as well.
So even if local media doesn't cover it very well
or people don't realize that it's happening,
it's still happening.
That determination is there.
The organizing is there and it's evolving and changing.
They're finding ways to keep this going.
If there's not a lot of interest in marching in the streets,
then they move to call blasts of state legislatures.
It's really triggered this very interesting
kind of decentralized movement in Southeast Wisconsin.
And how is law enforcement responding to that?
You know, it depends.
It depends.
The Wauwatosa Police Department was very hostile,
not only in their physical actions, but also their rhetoric.
Their police union straight up saying,
we don't think that people who are protesters
or people who are from the city of Milwaukee
should be involved in Wauwatosa city government.
We don't think that we should have this rate,
this committee on racial equity issues, et cetera, et cetera.
So the Wauwatosa Police Department
was very hostile about that.
The Milwaukee Police Department on the other hand,
they had their couple of days where back in the summer
where they were kind of being heavy handed to the protesters
but over time they stopped doing that.
You don't see riot cops responding in Milwaukee anymore.
Just the news and the press, I guess,
was just too much for them at the time.
And they just decided to leave the protesters alone
right now.
And then in Kenosha, it's kind of a separate story.
The Kenosha Police Department,
because of everything that happened down there
is very defensive and they should be.
One article we recently published kind of showed
that they regarded some of the militias
that went down there as friendly
and were possibly even willing to bump shoulders if need be.
And I think that the Kenosha Police Department
is kind of in a defensive position right now
because of the issue with Kyle Rittenhouse
and they don't want things to flare up again down there.
And also the activist scene in Kenosha is very, very new.
It didn't really exist before
the Jacob Blake shooting last year.
So they're still working out down there.
But, and then going back to Wauwatosa,
there's a new police chief.
The police chief that had led Wauwatosa for 31 years
and led Wauwatosa while it was cracking down
these protests retired a couple of months back
and they're welcoming in a new police chief
at the end of this month.
And that police chief is very conscious
that community trust has been completely shattered
and he has to do a lot of work
even in calling people's minds
about these various intelligence reports
that the department was building and making about people.
So yeah, it's been kind of this evolving kind of response
from law enforcement, I think.
And by the way, I mean like who knows
what's happening in the background.
So with Milwaukee, they stopped the overt stuff
but were they still intelligence gathering,
crunching numbers, making lists, who knows?
Who knows?
Yeah, I would imagine they were,
but again, some of it is to be expected.
It doesn't make it right, but some of it is to,
you know it's gonna happen.
So, all right.
So, I mean, that gives us a pretty good frame.
And it's one of those things that when we hear about it
in the media, a lot of times people are like,
wow, that's isolated.
It's really not, it's incredibly common.
I mean, it's so common in fact that,
you know, we were all talking about it
and we were joking about it.
You know, I was talking to some of the people
that you've worked with before
and I was like, hey, look, he made it, you know.
But it's a risk, just remember, you know,
track phones are cheap or whatever they're called now,
straight talk.
So.
Absolutely, VPN, friend,
always be informed about privacy.
If it means paying for a little bit better antivirus,
go for it, all that, you know.
And yeah, like I will say, you know,
people in Wauwatosa want to know
that they're not alone.
They're not alone.
They're not alone.
And so, you know,
I think that's a big part of it.
Yeah, like I will say, you know, people in Wauwatosa,
once they found out about all these tactics in the list,
you know, they weren't necessarily surprised,
but it's another thing to suspect it
based off of what you're experiencing
and just kind of common sense.
It's another thing to really see it, you know,
and to see your name on it and see your face
looking back at you on a piece of paper
that went who knows where.
Yeah, people really need to kind of
not only understand that these things are real
and they have real consequences,
but also get over the idea of like,
well, if it doesn't affect me, I don't care.
You know, even if they put me on a list,
I don't care, I have nothing to hide.
Actually, you do have a lot to hide.
That's why you lock your doors.
You do lock your doors, right?
You do pass code protect your phone.
That means you have things to hide,
even if it's, you know, seemingly to you,
like, you know, like this innocuous thing, it matters.
And people need to understand,
especially from the aspect of the constitution,
that if they violate, if law enforcement
is routinely violating and disregarding the constitution,
that matters even if it doesn't negatively impact you,
because someday it will impact you
and people you care about.
Well said, well said.
Okay, you know what?
That's where we're going to end it,
because that's a good ending.
So everybody, this is Isaiah Holmes.
You can check him out at the Wisconsin Examiner.
Yep.
And you can follow him on Twitter, I'm sure.
What's your handle?
One sec, I believe it's Isaiah Holmes 8.
Isaiah Holmes 8, I-S-I-A-H-H-O-L-M-E-S,
just like Sherlock, and then the number eight.
All right, okay.
Everybody, that's it for today.
It's just a thought.
Y'all have a good day.
Thanks.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}