---
title: Let's talk about Legos, being your own worst enemy, and the Block 19....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=b_c8fB8m3Xw) |
| Published | 2021/07/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mention of discussing consequences of actions, being one's own worst enemy, Legos, upcoming legislation, and a concerning issue.
- Introduction to the Block 19, a kit designed to make a Glock look like it's made out of Legos.
- Noting the cease and desist order from Lego and the potential compliance from the company.
- Criticism towards the gun crowd for being their own worst enemy and making guns look like toys.
- Reflection on the phrase "guns are not toys" and questioning the motive behind making firearms appear as toys.
- Connecting the issue to identity politics and the consequences of such actions.
- Concerns about potential increase in accidental shootings and plausible deniability for cops shooting kids with toy-like guns.
- Prediction of future legislation limiting firearm finishes and the possible impact on gun control.
- Critique on marketing a kit that makes a firearm resemble a toy with no practical purpose.
- Addressing the inevitable persecution claims from the gun crowd when legislation is enacted.
- Criticism of treating guns as more than just tools and integrating them into one's identity.
- Commentary on toxic masculinity and the need for a mindset shift regarding firearm ownership.
- Personal reflection on happiness, security, and calmness post distancing from firearm ownership.

### Quotes

- "The gun crowd, you are your own worst enemy."
- "There are consequences to your actions."
- "Making it part of your identity. You know, making it something, a substitute for masculinity. It's very toxic."
- "I'm not saying that you should sell off all your firearms and give up your identity or your hobby or whatever."
- "Anyway, it's just a thought."

### Oneliner

Beau addresses the concerning trend of making guns look like toys, predicting consequences and legislation while criticizing the gun crowd's identity association with firearms.

### Audience

Gun Owners

### On-the-ground actions from transcript

- Contact legislators to advocate for responsible firearm legislation (implied).
- Educate gun owners on the importance of treating firearms as tools, not toys (implied).

### Whats missing in summary

Deeper insights into the impact of identity politics on gun ownership.

### Tags

#GunControl #IdentityPolitics #Legislation #Consequences #FirearmSafety


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a whole bunch of stuff.
We're going to talk about the consequences of your actions.
Being your own worst enemy.
Legos.
Some legislation that even though it probably isn't written yet, it's coming and it's definitely going to pass.
And something that I am convinced is a real issue.
We're going to do all this by talking about the Block 19.
No, I didn't say that wrong.
The Block 19, not the Glock 19.
The Block 19.
It's a kit designed to go around a Glock and make it look like it's made out of Legos.
Make it look like a toy.
The gun crowd is its own worst enemy.
Now Lego found out about this and they sent a cease and desist order of course.
And it does appear that the company is going to comply with that.
I'm not going to trash the company.
Because that's a symptom.
It's a symptom of the disease.
The gun crowd, you are your own worst enemy.
When you were learning to shoot, what is something I guarantee somebody told you.
Your dad, your granddad, your mom.
Whoever taught you how to shoot said the phrase, at some point, guns are not toys.
Why on earth would you...
Why would you want to make one look like a toy?
To own the libs, right?
Because it's part of that identity.
Being edgy is not a substitute for having a personality.
I get it.
It's a novelty item.
I understand that.
But there are consequences.
People who handle lethal implements should understand that.
There are consequences to your actions.
Now the gun control crowd, they have already latched on to the idea that this would probably
increase accidental shootings.
And yeah, I mean that makes complete sense to me.
A kid finds something that looks like a toy.
Yeah.
That probably happened.
I'm going to give you another one.
This just gave plausible deniability to any cop who shoots a kid with a toy gun.
I didn't know.
You know, they're making them look like toys now.
I thought it was real.
Can't tell.
And the thing about that is that these will be conservative kids.
Because you know, liberals aren't letting their kids run around in the streets playing
with toy guns.
Own worst enemy.
Because of the actions.
I am certain legislation is going to be proposed that will limit finishes on firearms now.
They'll all be gunmetal blue, stainless nickel, something like that.
And it'll pass.
Because all they've got to do is show that image.
It's a tool, right?
That's what the gun crowd always says.
It's a tool.
Guns don't kill people.
People kill people.
Guns just a tool.
Okay.
Yeah.
So market it to me.
Market this kit to me.
Explain to me why I need a tool that looks like a toy.
No reason.
No reason.
It's a novelty thing.
I get it.
And yeah, I mean good marketing.
That whole thing about making it painful to tread on the Second Amendment.
Yeah, that's cute.
I mean, I always find the idea of an unsecured firearm on the floor getting stepped on, I
always find that funny.
When this legislation comes, and you know it's going to, the gun crowd will inevitably
scream persecution.
What did you expect to happen?
I mean seriously, where did you expect this to go?
You made a firearm look like a toy.
There are a lot of things that the gun crowd does that furthers the drive for gun control
in this country.
And the number one thing is not treating it like a tool.
Making it part of your identity.
You know, making it something, a substitute for masculinity.
It's very toxic.
It really is.
It corrupts your whole mindset.
You know, I have, I'm actually a whole lot happier.
Feel more secure.
Feel more calm.
Now that I don't own.
I'm not saying that you should sell off all your firearms and give up your identity or
your hobby or whatever.
But I have to think that if you didn't realize this was a really bad idea, perhaps your judgment
isn't quite what it should be.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}