---
title: Let's talk about Trump's coup denial....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1m6wvTcNWMQ) |
| Published | 2021/07/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring former President Donald Trump's response to reports about General Milley's concerns.
- Trump's statement mocked for resembling a transcript of a not-so-smart criminal in an interrogation room on TV.
- Trump denies discussing a coup and claims the election was his form of coup.
- Trump criticizes General Mark Milley, citing conflicts with General James Mattis and Obama.
- Former President Trump acts authoritarian, believing he knows more than experts, and criticizes Milley's actions.
- Trump attempts to sow dissension between generals, particularly targeting Milley.
- Contrasting leadership styles between Mattis and Milley likely led to conflicts.
- Milley's background in Special Forces indicates more than just being a tough guy - he's also a weaponized educator.
- Milley understands the dynamics of a coup and was reportedly concerned about Trump staging one.
- It's vital to recognize the seriousness of Milley's concerns and actions against a potential coup.

### Quotes

- "Former President Trump behaves like the typical authoritarian with a god complex."
- "Milley was concerned about the former president staging a coup against the United States."
- "One of the most vital functions of Special Forces is to be a teacher."

### Oneliner

Beau delves into Trump's response to Milley's concerns, showcasing authoritarian behavior and Milley's serious worries about a potential coup.

### Audience

Politically conscious individuals

### On-the-ground actions from transcript

- Contact local representatives to voice concerns about potential political crises (implied)
- Support efforts to uphold democratic principles in government (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's response to General Milley's concerns, shedding light on authoritarian behavior and the seriousness of potential coup attempts.

### Tags

#Trump #GeneralMilley #Authoritarianism #Coup #Democracy


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the former president Donald J. Trump's
response to the reports about General Milley's concerns.
He issued a pretty lengthy statement and it is being widely mocked as reading like
a transcript of the statement from the not so smart criminal in the
interrogation room during a, you know, TV criminal procedure show.
So it says, it starts off with his typical lies about the election, you know,
and then it goes on to say, I never threatened or spoke about to anyone a
coup of our government, so ridiculous.
Sorry to inform you but an election is my form of coup.
Yeah, we know that, we saw the attempt to be of the legal system.
We're very well aware of that.
And if I was going to do a coup, one of the last people I would want to do it with is
General Mark Milley.
That's funny and we're going to come back to that.
He got his job only because the world's most overrated General James Mattis
could not stand him, had no respect for him, and would not recommend him.
To me, the fact that Mattis didn't like him, just like Obama didn't like him,
and actually fired Milley, was a good thing, not a bad thing.
I often act to counter people's advice who I don't respect.
That's widely known in technical terms as being petty, by the way.
But here we see the former president behaving like the typical
authoritarian good with a guide complex who believes he knows more than all of the experts.
I don't know that President, former President Trump, is in a position to rate generals.
And then he goes on, says he lost respect for Milley because he apologized for walking
with Trump through that park, and then dog whistles about Confederate base names,
and talks about how Milley is in favor of changing them.
The interesting part about this is it certainly appears that the former
president is attempting to sow dissension between the generals.
Sure, why not? I can actually believe that there were some personality conflicts
between the two. Mattis is a conventional warrior.
Conventional. Lines, retreat, advance, very more akin to what you see in movies.
Milley, early in his career, was with Fifth Group. Unconventional.
That probably heavily influenced the way he thinks.
Those two types of leaders, they typically clash.
They typically have conflict. So I can believe that part.
But that's actually the funny part about this.
And it's humorous, but it's also something that's worth noting.
If you look at Milley's uniform, you see that long tab on his shoulder, right?
It says Special Forces. To the average American, and I'm sure to Trump,
that just means tough guy. And yeah, that's true, no doubt, but it's not all being Rambo.
One of the most important functions of the Special Forces is to be a teacher.
They're weaponized educators. They can go in and train local troops to politically
realign a geographic area through unconventional means and force.
One of those means would be a coup.
At least Trump's stellar reputation for being able to choose the right person
for the job is still intact.
The one person he wouldn't want to do a coup with is probably the most qualified person
he has ever met to do one with.
And that's funny, but it's also important to note.
Milley understands the conditions. Milley understands the dynamics of a coup.
This isn't somebody who is untrained or uneducated on this topic.
He's very well aware of what it takes and what it looks like, and he was concerned about it.
He was concerned about the former president staging a coup against the United States,
so much so that he is reported to have conversations with his aides trying to figure out how to stop it.
Don't let this story just slide by. This is important.
Somebody who knew what they were talking about was really concerned about it.
Somebody who knew what they were talking about and was very close to what was going on
was concerned enough about it to have had conversations with aides to stop it.
We can't let this get overshadowed.
This needs to be remembered because the former president still has a lot of influence in the Republican Party.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}