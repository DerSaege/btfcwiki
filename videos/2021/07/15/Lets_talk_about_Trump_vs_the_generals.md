---
title: Let's talk about Trump vs the generals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Dym0UN2UB1M) |
| Published | 2021/07/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Recalls a friend asking if Trump will try to stay in power, leading to jokes until January 6th.
- Friend apologizes after reading about events in the Trump administration from a book.
- Details generals at the highest level discussing how to prevent a Reichstag moment.
- These private conversations reveal how close the U.S. came to a serious threat.
- Emphasizes the gravity of the situation, with generals not making political statements publicly.
- Mentions a key figure, indicated by a "funny little green hat," leading most of the concerning talks.
- Points out the major influence this person holds over a political party in the U.S.
- Stresses that the outcome of January 6th could have altered the course of future elections significantly.
- Expresses hope for others to realize the seriousness of past events and prevent similar occurrences.
- Concludes with a reminder that despite believing such events cannot happen, they nearly did.

### Quotes

- "That's how close we came."
- "It's not over."
- "You can say it can't happen here, but the reality is it almost did."
- "Have a good day."

### Oneliner

Beau details the close call the U.S. faced with potential authoritarianism, urging vigilance to prevent similar threats in the future.

### Audience
Citizens, voters, activists

### On-the-ground actions from transcript
- Spread awareness about the risks of authoritarianism in politics (implied).
- Engage in critical thinking and analysis of political events to prevent similar threats (implied).

### Whats missing in summary
The emotional intensity and urgency conveyed by Beau during the recount of past events. 

### Tags
#USPolitics #Authoritarianism #Threat #Vigilance #Awareness


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about how close we came
and how close we are.
You know, a very early video on this channel,
it's titled something like, let's talk about a Kennedy
legend you've probably never heard before, something
like that.
And before the end of last year, a friend of mine,
he asked me about that video.
And he asked me about a few others.
And he said, you really think he's
going to try to do something, don't you?
I'm like, yeah, yeah, I do.
You really think he's going to try to stay in power?
Yeah, yeah, I do.
And he laughed.
Other people laughed.
A lot of jokes at my expense.
Then the sixth happened, joke stopped.
Tonight, that person just called to apologize because he read some
excerpts from an upcoming book called I Alone Can Fix This, and it details
things that happened in the Trump administration behind closed doors.
One of the things it reports is that generals at the highest level of the U.S. military
were having conversations in private about how to stop a Reichstag moment.
If you're not familiar with the term, that's the moment that that German guy from World War II,
that's the moment he took over. That's the moment he really got power.
Generals at the highest levels were trying to figure out how to stop it from happening here.
They may try, but they can't do it without the military. There's another word in there.
These are the same people were fighting during World War II. Quotes.
That's how close we came. That is how close we came. These weren't generals
out making political statements. These weren't generals having public
conversations that got leaked. This was in private trying to figure out how to
deal with it. That's how close we came. I would note that the person that seems to
have been leading most of the conversations, well he's got one of those
funny little green hats. How close are we? The person that they were worried
about. The person that they had private meetings about trying to figure out how
to stop him has major influence over one of the political parties in the United
States. That's how close we are. It's not over and I think the most important
thing to remember as we head into the midterms is that if the 6th had gone a
different way, we wouldn't be talking about the midterms because we wouldn't
be having them.
This is a person, this is a person who called, was somebody who, I don't want to
I was a Trump supporter, but didn't see the danger on danger's face.
I dealt with the mind.
We can hope that others wake up to what was going on.
Realize that all of those characteristics were met.
every box was checked that's what was happening you can say it can't happen
here but the reality is it almost did anyway it's just a thought you have a
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}