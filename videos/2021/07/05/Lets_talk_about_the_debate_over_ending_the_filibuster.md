---
title: Let's talk about the debate over ending the filibuster....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=J6QRjslRy9c) |
| Published | 2021/07/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the debate around ending the filibuster and its long-term impacts.
- The filibuster is a Senate rule that makes passing legislation harder by requiring more than a simple majority.
- McConnell uses the filibuster to obstruct the Democratic Party's agenda, despite them being in power.
- Some want to eliminate the filibuster, while others fear it could backfire during Republican control.
- Concerns about midterm losses for the President's party and potential legislative advantages for the opposition.
- Beau suggests that midterm losses are due to a team mentality and lack of participation rather than filibuster concerns.
- The legacy of Trump's executive orders could easily be undone, unlike lasting legislation.
- Democrats face challenges delivering on promises due to Senate hurdles created by the filibuster.
- Removing the filibuster could enable the Democratic Party to pass ambitious legislation and secure wins.
- Beau acknowledges the gamble in deciding whether to keep or eliminate the filibuster.

### Quotes

- "It's politics. It's trying to make the right play."
- "Even those who normally might show up in the midterms, maybe they don't, because they don't see the benefit."
- "There isn't a guaranteed right answer here."
- "If the Democratic Party doesn't move some pretty substantial legislation forward pretty quickly, they're going to have issues in the midterms."
- "It's just a thought."

### Oneliner

Beau presents both sides of the filibuster debate, weighing its impact on legislation and political strategies.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Educate yourself on the filibuster debate and its implications (implied)
- Stay informed about current political developments and how they may affect you (implied)

### Whats missing in summary

Deeper insights into specific legislative impacts and potential outcomes.

### Tags

#Filibuster #Senate #Legislation #Politics #Debate


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the filibuster and the discussion over ending it.
Getting a lot of questions about the different positions that people are taking
and the long-term impacts from either ending it or not ending it.
I'm going to try to set my opinion aside and just present both arguments.
Now, with any discussion there's always more than two sides.
There's always more than two arguments.
In this case, though, when you strip away the historical context that gets used as talking points,
at the end of the day, it's real politics.
It is just political and all of the other stuff, it doesn't really matter. It's window dressing.
So, if you're not familiar with what the filibuster is, it's a rule in the Senate.
It's not a constitutional thing. It's just a rule in the Senate.
Its pure political impact is to make it harder to get stuff through the Senate.
A simple majority isn't enough.
Because of this, right now, McConnell is able to be McConnell, to do what he's done his entire career,
which is play the obstructionist.
He's able to slow, and in some cases stop, the Democratic Party's agenda,
even though they're the party in power.
Because of this, some people want to get rid of the filibuster.
Those opposed to the idea are generally coming from the historical perspective,
and the reality that the party in power, the President's party, typically loses seats in Congress during the midterms.
So the worry is that they're going to create this new tool, make it easier to get stuff through,
and then just hand it to the Republican Party.
There's a lot of historical basis for that.
Now, people attribute this factor to a lot of different things.
I think it's way simpler than most.
I think the reason the party in power loses seats during the midterms is due to a team mentality.
They feel safe. They feel comfortable.
Their person is in power. Their person is in the White House.
So they don't show up for the midterms, and then they're surprised when their team loses seats.
So that's their concern.
Now, a follow-on concern from that side, the side that doesn't want to get rid of it,
is that if they do, when the Republican Party eventually does take power again,
they'll be able to push through legislation a lot faster, a lot easier.
Trump, as far as a legislative legacy, he's not really going to have one.
He's going to have a legacy, and he definitely made his mark when it comes to American politics.
But as far as things that he's going to accomplish that last, there's not going to be one,
because he pretty much did everything through executive order,
which means it's been really easy for the Biden administration to unwind a lot of what he's done.
If he had been able to get it done through law, through legislation that went through the House and the Senate
and was signed into law, executive order doesn't cut it.
You can't undo it that way.
So that's the other accompanying argument.
These are the reasons to not get rid of the filibuster.
The flip side to this is that it may be a self-fulfilling prophecy.
The American people, those who showed up in pretty big numbers to get rid of Trump,
they want results, and the Democratic Party is not delivering.
It's not delivering what a lot of people want, because they can't get it through the Senate.
So with that in mind, even those who normally might show up in the midterms,
maybe they don't, because they don't see the benefit.
The Democratic Party isn't doing what they said they're going to do anyway.
They're not doing what I want them to do. They can't get anything through.
If they were to get rid of the filibuster, they might be able to push through ambitious legislation
that would guarantee them a win, that would guarantee a series of wins, to be honest.
That's the other side of the coin.
Now, I've made my opinion pretty clear on this in the past.
I'm trying not to provide it today, but those are the two sides.
And I think that if people are going to talk about this,
you have to know where the other person's coming from.
One is playing this conservatively, and one is going for the win.
That's the difference.
Now, realistically, my opinion aside, either way is a gamble.
There isn't a guaranteed right answer here.
It's politics. It's trying to make the right play.
There's a lot of guesswork.
So I'm not sure that people should be getting as emotional and as fiery over this topic as they are,
because there's no way to know who's right,
who is going to have the right take for a while. We're talking years.
I think that if the Democratic Party doesn't move some pretty substantial legislation forward pretty quickly,
they're going to have issues in the midterms and probably in 2024.
But the counter argument to that is that that might happen even if they get rid of the filibuster.
And then they've just handed Trump's clone this tool.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}