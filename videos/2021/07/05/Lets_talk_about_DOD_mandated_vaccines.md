---
title: Let's talk about DOD mandated vaccines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OIP_lio-bBA) |
| Published | 2021/07/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Representative Massey and other Republicans introduced a bill to prohibit the US Department of Defense from requiring personnel to get the vaccine.
- The argument that troops should have bodily autonomy in decision-making doesn't hold as the military operates under strict hierarchy with no room for personal choice.
- Morality doesn't play a significant role in the military; readiness is the paramount concern.
- Unvaccinated troops could significantly impact readiness in various operations, including unconventional conflicts, humanitarian missions, and facing near peers.
- Biden is unlikely to sign such a bill, and even if passed, the military tradition suggests they may ignore it for the sake of readiness.

### Quotes

- "Morality and war-making capabilities really don't blend well."
- "The only legitimate reason to support the bill is the bodily autonomy argument, but that's literally it out because it's the military."
- "The US military has a long tradition of ignoring politicians who think they know better, who refuse to embrace science, and who try to degrade military readiness."

### Oneliner

Beau explains why the bill prohibiting mandatory vaccines for military personnel is impractical, focusing on hierarchy, readiness, and potential impacts on operations.

### Audience

Military personnel, policymakers.

### On-the-ground actions from transcript

- Contact military representatives to advocate for prioritizing readiness over political stunts (implied).
- Support efforts to ensure military personnel are adequately vaccinated to maintain operational readiness (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of prohibiting mandatory vaccines for military personnel, offering insights into the hierarchy, readiness concerns, and potential operational impacts.

### Tags

#Military #Vaccines #Readiness #Government #Hierarchy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about
Representative Massey's little bill,
and we'll go through the discussion that it has prompted.
We'll go through the points and counterpoints
that are being made and explain why they really don't matter
at the end of the day, why that's not the discussion
should be taking place, because there's really only one thing that matters when it comes to this.
We'll get to all of that. While the discussion as it's taking place is entertaining, it is fun to
watch the mental gymnastics, it doesn't actually further anything. If you don't know what I'm
talking about, Representative Massey and like two dozen other Republicans introduced a bill that
that would prohibit the United States Department of Defense
from requiring that its personnel get the vaccine.
OK.
So when this is said, obviously, the first thing that pops up
is you do know that the military requires
like a dozen vaccines, right?
And yeah, that's fun.
I mean, it's fun to say.
It is fun to watch people respond to that.
is entertaining. But at the end of the day it's a what about-ism. It doesn't actually further the
discussion and it doesn't demonstrate why they're wrong. It just points out a hypocrisy. It doesn't
move the conversation along. So at the end of the day it's fun but it doesn't matter. The next thing
is that troops should have the bodily autonomy to make this decision for
themselves. No, it's not how it works. That is not how it works. I generally agree
with that argument when it comes to making this mandatory. I don't agree with
it. I don't believe the government should be able to come in and say you have to
do this with your body. That's wrong. I'll argue with you all day saying you need
go get one, but I don't think it should be done by force. If you go back and watch those videos,
you'll also hear me say that if you don't get one, certain opportunities won't be open to
you. The U.S. military is one of them. Okay? Okay. First, the idea that those in the military
have the same level of personal choice as those outside of it is ridiculous. The United States
military, every military all over the world, is authoritarian by nature. It functions under a strict
hierarchy. If that hierarchy breaks down, it fails to work. That's why when major militaries go into
countries overseas, the very first thing they attempt to do is take out command and control.
They try to disrupt that hierarchy. As far as bodily autonomy in the military, that's not really
a thing. If it was, how many troops do you think would PT? It's not a thing. It
doesn't work like that. And when you say this, you have somebody say, well show me
where that's written. Okay, the answer is Form DD4, Part C, Section 9. It's the
enlistment papers. Actually has a whole section on the fact that if you sign
this, you are going to be required to do things that civilians don't. There's a
whole part of the contract about this actually. Now the moral argument, I agree
with it, it's sound, bodily autonomy and all of that. I will suggest that those who know
what makes the grass grow green, I'm gonna suggest that they might have a
different moral compass because they have to because of what they have
volunteered to do. When it comes to the military and military personnel, morality
doesn't matter. Morality and war-making capabilities really don't blend well.
Those two things have nothing to do with each other. To prove this, if you've
watched this channel for any length of time, you know that when it comes to
people of different orientations and identities that I think they should be
able to do whatever they want. I don't think it should be held against them.
It's not wrong. They're not hurting anybody. That's not a negative mark in my
opinion. So morally they should be able to serve, but when that conversation came
up, that's not how I addressed it because morality doesn't matter. What matters is
readiness. And when it comes to trans troops, study after study has shown it
doesn't negatively impact readiness. So they can serve. That's the overriding
question. That's the actual question on the table. So how does having unvaccinated
troops impact readiness? Negatively, a lot. By however many percent choose not to
get it. Let's suggest that only the MAGA loyal refused to get the vaccine. So you're talking
about 20% or so. We'll go through the three main styles of operations that have been recently
conducted. Now if I was to go through and list all of the negative ways this would impact readiness,
this video would be like five hours long. I'm just going to focus on three general types that people
can easily relate to. First, let's talk about an unconventional conflict. So like those that we've
been fighting for the last two decades, you have troops that are out in forward bases, right?
right? And they train local forces. So if the opposition wants to degrade U.S. warfighting
capability by 20%, all they have to do is get one of them that has it in that training
group. That's it. Because then one of the U.S. troops takes it back to the base and
those close quarters it spreads and reduces capability. And that's just
scratching the surface. That hasn't even factored in the macho guys who pretend
they aren't sick and still go out, but because they are sick, well they're not
as sharp, so they make mistakes that cost other people. That doesn't factor in the
drain on transportation because these guys have to get airlifted out. It
It doesn't factor in the drain on medical supplies
because they have to be treated.
It impacts readiness in a huge way in that situation.
Now let's talk about humanitarian operations.
US troops go down and elements show up to, I don't know,
vaccinate people, you know, something they do,
or distribute relief supplies, winning hearts and minds
and all of that stuff, right?
What happens when somebody on that deployment is sick
and brings that to that population?
That hearts and minds mission just
went sideways when it triggers an outbreak, right?
Completely undermines the entire mission in that instance.
Now, let's suggest hypothetically the US might
be regearing and retooling to face off
against near peers, as is actually happening.
These countries, well, they're more technologically advanced.
So they don't have to go the low tech route of getting
somebody into a training group.
They can just release it, because they have the
technology to do it.
And because it exists in the wild, it won't even look like
something designed to degrade capabilities.
It wouldn't be seen as a precursor to an attack.
This list goes on and on and on.
I don't think you can give me a viable reason
to support the bill, though, other than MAGA.
It doesn't exist, because the only legitimate one
is the bodily autonomy argument, but that's literally
it out because it's the military. So I will be honest if I was tasked with
disrupting or degrading a major military's combat effectiveness I'd be
bribing politicians to introduce this kind of legislation because that's the
only thing it does, all it does is degrade US readiness, support the troops.
Right, guys?
Okay.
But I do have some good news after all of this, first, Biden isn't going to sign
this, this is a joke, it's more of a political stunt for a very small base of
people, okay, let's say though, hypothetically, that at some point in the
future, something like this does pass and it gets signed. The military will
ignore it. The US military has a long tradition of ignoring politicians
who think they know better, who refuse to embrace science, and who try to degrade
military readiness. And I know somebody's gonna say, well that's not what the
founders intended, the founders did it. There were prohibitions against certain
kinds of inoculations during the Revolutionary War. George Washington
attempted to abide by these until it impacted readiness and then he went
ahead and did it anyway. People talk about how bad Valley Forge was. One of
the reasons is that a lot of the troops were getting over their inoculations. I
I would point out that it knocked the infection rates from 20% to 1% and made him combat effective again.
So even if something like this passes, it is worth noting.
Sure, maybe the military can't say you have to get out if you don't get this vaccine.
Maybe they can't say that.
say that, but they can't say you're not worldwide deployable because you're not.
And if you're not worldwide deployable, well, you can't advance.
You can't get promoted.
So you need to get out.
They will find a way around it because it is that important to U S readiness.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}