---
title: Let's talk about the New York Times documentary about the 6th....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vqt7oRzn2X8) |
| Published | 2021/07/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New York Times documentary details events of January 6th Capitol riot from start to finish, providing valuable context.
- The documentary, "A Day of Rage, How a Mob Stormed the Capitol," shows the layout of the Capitol and what led to the notable events.
- Department of Homeland Security warned of more incidents in August due to certain rhetoric and baseless theories.
- People in office and out continue to echo baseless claims and downplay the events of January 6th.
- Some in Washington are trying to pretend the Capitol riot wasn't a big deal to protect their political careers.
- Continuing to push baseless claims may lead to more events, damaging the Republican Party and risking safety.
- Officials seem more focused on their political careers than on addressing the underlying issues.
- The longer baseless claims go unchallenged, the more damaging it is to the Republican Party.
- Addressing the baseless claims is necessary to prevent the cycle from continuing.
- Someone in the Republican Party needs to have the courage to denounce the lies and address the situation.

### Quotes

- "They're putting their careers above their party, above their country, and at this point above the safety of their constituents."
- "If that doesn't happen, people will still believe it. And this cycle will continue."

### Oneliner

New York Times documentary provides context on Capitol riot while warning of potential future incidents due to baseless claims and rhetoric, urging action to break the cycle.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Challenge baseless claims and rhetoric (suggested)
- Call for accountability within the Republican Party (suggested)

### Whats missing in summary

The emotional impact and urgency conveyed by Beau's call for addressing baseless claims and protecting democracy. 

### Tags

#CapitolRiot #BaselessClaims #RepublicanParty #DepartmentOfHomelandSecurity #PoliticalAccountability


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about the
New York Times documentary that was recently released. It's about 40 minutes long and it
is well worth your time. Over those 40 minutes, the documentary takes you through the events
of the 6th, start to finish. It carries you through it. And it is a good briefing. It
does a really good job. If you have never been to the Capitol, if you're not familiar
with the layout, you don't know how big it really is. It's worth watching this just for
that. They do a really good job of detailing the situation and showing what led up to some
of the more notable events and what may have contributed to them occurring. It's called
A Day of Rage, How a Mob Stormed the Capitol. It's 40 minutes of your time and it will provide
you with a lot of context to the events of that day and to events that may be coming
later. As you're watching it, listen. Listen to what's being said, not by the narrator,
but by the people in the crowd. And listen to what they say in comparison to the Times
Trump tweet stuff. It's worth watching. As you're watching it, remember that the Department
of Homeland Security put out a bulletin saying that a convergence of certain rhetoric and
certain baseless theories is likely to lead to more incidents this summer, in August.
And as you think about that, think about those who are currently in office and those outside
of office who are continuing to echo those baseless claims, who are continuing to downplay
the events you can see with your own eyes in this documentary. There are a lot of people
in Washington who are very committed to pretending like what you will watch did not happen. Like
it wasn't a big deal. Like it wasn't really spurred on by what spurred it on. They don't
want to admit it because it's damaging to their political careers. That's the reality.
That's what they're putting the importance on. When you take that in combination with
the DHS bulletin and the likelihood that continuing these claims, continuing to play into this,
is likely to spur more events, you have to wonder what they're even doing up there. Because
at this point, they're putting their political careers above their party because the longer
this goes on, the longer they drag this out, the more damaging it is to the Republican
Party. They're putting their careers above their party, above their country, and at this
point above the safety of their constituents. DHS is telling them it's going to get worse.
More could happen come August. Yet you really don't see a lot of them walking back claims
and trying to diffuse the situation. To the contrary, it seems like they just want it
to go away. Pandora's box is opened here. It's not going to go away unless these baseless
claims are addressed. Unless there is somebody in the Republican Party that has the courage
to come out and say, no, none of that was true. He lied. He lied. If that doesn't happen,
people will still believe it. And this cycle will continue. If you've got the 40 minutes,
it's worth watching. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}