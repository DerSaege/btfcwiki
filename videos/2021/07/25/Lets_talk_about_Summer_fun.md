---
title: Let's talk about Summer fun....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GD-FU-9QTkc) |
| Published | 2021/07/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Family summer fun turned serious when a child almost drowned.
- Child rescued by family members after not responding in the water.
- Quick action taken to clear child's lungs and call 911.
- EMS arrived remarkably fast, within five to six minutes.
- Child needed to be checked out despite appearing fine.
- Importance of knowing CPR and taking classes emphasized.
- Acknowledgment that sometimes it's up to individuals to act quickly.
- Encourages getting certified in CPR as it can save a life.
- Stress on the need for quick response in emergencies.
- Reminder that knowledge and skills can alter outcomes significantly.
- Despite being okay, the child still needed medical evaluation.
- Urges people to be prepared for situations when help may not arrive in time.
- Knowledge is emphasized as a valuable tool that can be carried for a lifetime.
- Encouragement to prioritize learning life-saving skills.
- Closing thought on the importance of being prepared for emergencies.

### Quotes

- "Knowledge weighs nothing. You will carry it with you the rest of your life."
- "If you have the ability, please, please take a course."
- "There is no response that is going to get there fast enough."
- "A little bit of knowledge can greatly alter the outcome of events."
- "Have a good day."

### Oneliner

Family summer fun turned serious as Beau recounts a near-drowning incident, urging the importance of quick action, CPR knowledge, and individual preparedness for emergencies.

### Audience

Parents, caregivers, community members

### On-the-ground actions from transcript

- Take a CPR class (suggested)
- Get certified in CPR (suggested)
- Stay prepared for emergencies (implied)

### Whats missing in summary

The full transcript provides a detailed account of a near-drowning incident during a family summer outing, stressing the significance of CPR knowledge, quick response in emergencies, and individual preparedness for unforeseen situations.

### Tags

#SummerFun #FamilySafety #CPR #EmergencyPreparedness #LifeSavingSkills


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about summer fun
and how to have summer fun
because my family had some summer fun recently
and it definitely seems like a story that should be shared.
My family meets up with a couple others,
go do the Florida thing.
Everybody goes swimming, enjoy the sun, the water,
even had hot dogs, all American day.
Whole bunch of people there,
everybody laughing, swimming, playing.
You know, at one point,
one of my boys swims up to his friend,
a little bit younger than him,
starts talking to him,
but he's got his head down in the water looking down below,
so he doesn't answer.
So my son lifts his head up out of the water and it's limp.
His dad sees this,
snatches him up, slings him up on shore to a mom,
gets out, calls 911.
My wife lays him down, starts to check him
and then starts pumping
on his little itty bitty five-year-old chest.
Doesn't take long to clear his lungs.
Water comes out, he starts coughing, crying
and has to throw up.
And I would imagine that there's not an adult there
that had ever been happier to hear a child start crying.
Now, EMS shows up five to six minutes,
which in my area, that is amazingly fast.
That is ridiculously fast.
And they check him and his lungs do sound clear.
Everything seems fine.
So all he wants to do is get back in the water,
but he's got to get in the ambulance.
If you don't know,
if you ever have a near-drowning experience,
you still need to go get checked out,
even if you feel fine, especially if it is salt water.
So once the door is on that ambulance shut
and it starts pulling down the road,
the adrenaline is gone and the mom starts crying
because it was that close.
It was that close.
Now his best guess is that he hit his head
on something underwater.
And it took all of 30 seconds to be noticed.
And that got the 911 call,
which got an ambulance in a ridiculously fast amount of time,
five to six minutes.
I would point out that by conventional wisdom,
five to six minutes is two to three minutes too long.
Would you know what to do?
Would you be able to start pumping on that itty bitty five-year-old chest?
If not, it's probably a good idea to take a class, CPR classes.
They don't take long.
In fact, I think they even offer some of them online now.
We have to acknowledge that at some points in time,
it's just us.
It is just us.
There is no response that is going to get there fast enough.
It's just us.
If you have the ability, take a class.
Could save a life.
And again, it doesn't take long.
I promise you, I will be getting recertified
because I am sure that I could use a refresher.
And things have changed since I last took one 15 years ago.
Things can happen very quickly.
And a little bit of knowledge can greatly alter the outcome of events.
So just so everybody knows, he is okay.
In fact, I got pictures sent to my phone of him playing on a phone in the ambulance.
But it didn't matter.
He still needed to go get checked out.
A lot of times, we are so used to the comforts of society
and the ability to call on others that we forget
there are some times when they can't get there fast enough.
If you have the ability, please, please take a course.
Please get that knowledge.
Knowledge weighs nothing.
You will carry it with you the rest of your life.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}