---
title: Let's talk about blame for the 6th and the National Guard....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lnRq3EKGFsQ) |
| Published | 2021/07/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Politicians are questioning the National Guard's actions and decisions on January 6th.
- The scrutiny aimed at the National Guard is unwarranted, as they were following guidance.
- Orders from Acting Secretary of Defense limited the National Guard's capabilities on that day.
- The guidance restricted the National Guard from having necessary tools and engaging with protesters.
- The ultimate responsibility for the failures on January 6th lies with the White House at that time.
- The guidance was intended to prevent an overreaction by the National Guard.
- President Trump's directive to protect demonstrators influenced the restrictive guidance.
- The American people are more concerned about how the events started rather than operational breakdowns.

### Quotes

- "Nobody wearing a National Guard uniform is responsible for any of the failures or breakdowns that occurred on the 6th."
- "There's nothing the National Guard could have done. They had orders."
- "The ultimate responsibility for the failures that occurred related to the National Guard? You need to look to the White House at the time."
- "The American people do not care about this. This is not what people are concerned with."
- "You want to know who's ultimately responsible, it's President Trump."

### Oneliner

Politicians unfairly scrutinize the National Guard; ultimate responsibility for Jan 6 failures lies with the White House, not the Guard.

### Audience

Politically aware citizens

### On-the-ground actions from transcript

- Seek accountability from those in power (implied)
- Advocate for transparent investigations into the events of January 6th (implied)

### Whats missing in summary

Detailed insights on the specific actions and decisions made by the National Guard on January 6th

### Tags

#NationalGuard #January6 #Responsibility #Accountability #PoliticalScrutiny


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the National
Guard in relation to the Sixth.
We're going to do this because there's
a whole bunch of politicians who are now going back and forth
asking questions in the form of soundbites
about the National Guard, why they weren't in certain places,
what took so long, going back and forth.
And all of the scrutiny is directed at the National
Guard.
Now, normally, when politicians start doing this,
somebody within that agency, the one that's under scrutiny,
they take the fall.
They end up taking the rap.
And nine times out of 10, it wasn't their fault.
The National Guard isn't used to this.
And I have a feeling that right now there's
some colonel somewhere that has absolutely no clue
that if this continues, they'll end up taking the rap,
because they didn't have anything to do with it.
They didn't make these decisions.
I'm going to go ahead and get out in front of all of this
and tell you right now, nobody wearing a National Guard
uniform is responsible for any of the failures or breakdowns
that occurred on the 6th.
Bold claim, right?
I'll read you the orders in a second.
Before I do, I want to point out that the American people do
not care about operational intelligence, logistical, and planning breakdowns for
the National Guard. That's not what people want to know about when it comes
to the Six. They don't want to know why it wasn't stopped quickly. They want to
know what started it. So unless there was some concerted effort to degrade the
capabilities of the National Guard in furtherance of, I don't know,
So hypothetically speaking, a coup attempt, we don't care.
But who is ultimately responsible?
The National Guard, they have these crazy things called orders.
They get guidance.
So this is from Acting Secretary of Defense Christopher C. Miller.
It's dated January 4th, 2021.
is to the Secretary of the Army. This memorandum responds to your January 4, 2021 memorandum
regarding the District of Columbia request for District of Columbia National Guard support
in response to planned demonstrations from January 5 to 6, 2021. You are authorized to
approve the requested support subject to my guidance below and subject to consultation
with the Attorney General as required by Executive Order 11485.
So what's that guidance, right? Because that may have something to do with it. I mean,
this is coming from Acting Sec. Def. Okay. Without my subsequent personal authorization,
DC National Guard is not authorized the following. To be issued weapons, ammunition, bayonets,
batons or ballistic protection equipment such as helmets and body armor. So even if they were at
the Capitol before it started, they would have been subject to this guidance. They wouldn't have
the tools to do the job. The reality is there were a few hundred, I want to say 340, could be wrong
on that, but more than 300 National Guard troopers in DC at the time. They were there assisting Metro
PD, I believe, so they may not have had the jurisdictional capability to go to the Capitol.
But they didn't have the tools necessary to respond, so they had to go back to the armory.
That's part of the delay, but they were just following the guidance.
It goes on, to interact physically with protesters except when necessary in self-defense or defense
of others consistent with the D.C. National Guard rules for the use of force, meaning that in the
beginning when there's that giant crowd and they're shaking the gates and everything,
they wouldn't have been able to do anything. The National Guard would have had to have
uh, waited for things to actually turn violent before acting, and then they didn't have the
the tools to do the job, so they had to go back to the armory.
To employ any riot control agents, can't do that either.
To share equipment with law enforcement,
instead of the idea that because the Capitol Police didn't
have the tools that they needed, National Guard
would have been able to provide them.
Nope, not according to the orders.
To use intelligence, surveillance,
and reconnaissance assets, or to conduct incident awareness
and assessment activities to employ helicopters
or any other air assets to conduct searches, seizures,
arrests, or other similar direct law enforcement activity
to seek support from any non-DCNG National Guard units."
So at the end of the day, reading this guidance,
which came from the Secretary of Defense
to the Secretary of the Army and then down to the National Guard,
There's nothing the National Guard could have done.
They had orders.
Now the question is, why did this guidance go out like this?
Why was it so focused on limiting the capabilities
of the National Guard?
If only Christopher Miller had, you know, said why.
He did, the day before the date on this memo,
January 3rd, he met with the President of the United States,
Donald Trump at the time, the Commander-in-Chief.
The Commander-in-Chief told him to fill the request and do whatever was necessary to protect
the demonstrators who were exercising their constitutionally protected rights.
So this guidance is designed to limit a possible overreaction by the National Guard.
That's why it's set up the way it is.
the orders that came from Commander-in-Chief. That's what he was told to do, so that's
what he did. You want to know who is ultimately responsible for the failures that occurred
related to the National Guard? You need to look to the White House at the time. I'm
going to reiterate, the American people do not care about this. This is not what people
are concerned with. They don't want to know the mundane stuff like this. They
want to know how this started. Everybody who watched understood that there were
logistical failures. We all know that and I would imagine that the National
Guard is going to hot wash this and correct those problems but to be honest
I don't see many of them that actually had anything to do with the National
Guard. It was influenced from outside. But what matters is this guidance. That
answers all of the questions that are being thrown around right now. Why
weren't they already at the Capitol? Doesn't matter because even if they were
they wouldn't have been able to do anything. And this guidance came from the
acting Secretary of Defense based on an order from the commander-in-chief. You
want to know who's ultimately responsible, it's President Trump. I think what the
American people want to know is whether or not that same person who is
responsible for slowing down the response, the same person who delayed
being able to stop it, is also really responsible for starting it. Hanging out
some National Guard commander hanging them out to dry for this. The idea that
that's something that may be being toyed with is absolutely appalling. Anyway, it's
just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}