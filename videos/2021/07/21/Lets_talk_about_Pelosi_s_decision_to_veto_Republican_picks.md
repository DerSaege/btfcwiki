---
title: Let's talk about Pelosi's decision to veto Republican picks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mZVt82j5QVo) |
| Published | 2021/07/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Nancy Pelosi stopped certain Republicans from being on a committee looking into the events of the 6th.
- Republicans submitted names of people who actively supported undermining the election.
- Beau believes Pelosi's decision should be analyzed beyond a partisan view.
- He draws a historical analogy to the development of factions in movements seeking to change governments.
- Beau explains the roles of the military and political wings in such movements.
- He mentions how politicians were likely to justify, excuse, downplay, and cover up what happened on the committee.
- Beau asserts that the ultimate goal is to install an authoritarian leader by undermining democracy.
- He suggests that politicians may not all fully support the movement's goals but might be appealing to certain voter bases.
- Beau argues that those ideologically or politically involved in undermining democracy should not be on the committee.
- He mentions that disrupting the hearings could be a beneficial move for the political wing of the movement.

### Quotes

- "It was an attempt to undermine the Constitution."
- "Deny the opposition that platform."
- "But her decision to stop those who are at least willing to appear to be ideologically
  aligned with those who attempted to stop the election, stop the certification, her decision
  to bar them from being on this committee is the right one."

### Oneliner

Nancy Pelosi's decision to exclude Republicans from a committee investigating the events of January 6th is a strategic move to prevent undermining democracy by denying a platform to those seeking to justify and cover up the attack.

### Audience

Political analysts, concerned citizens

### On-the-ground actions from transcript

- Disrupt hearings (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the potential motives and actions of politicians involved in undermining democracy, urging caution and vigilance against authoritarian movements.

### Tags

#NancyPelosi #RepublicanParty #UnderminingDemocracy #Authoritarianism #PoliticalAnalysis


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Nancy Pelosi's decision to stop certain Republicans from
being on the committee that's going to look into the activities of the 6th and whether
or not that was the right move.
There's a lot of people talking about it, but they're talking about it from an American
political viewpoint.
They're looking at it through a partisan lens.
What is best for each party?
And that's the framing.
I think that's the wrong framing.
We're going to talk about it through a very different viewpoint, a very different lens,
and we're going to see if she made the right decision.
If you don't know what went down, there's going to be a committee to look into these
events.
Pelosi was like, hey Republicans, give me five names, and a lot of the people they submitted
were active supporters of undermining the election.
So she exercised veto power and said no.
So we're not going to look at it through a partisan lens.
We're going to address some historical realities, and I know people are going to say that it's
really early to make this analogy.
Yeah, it is, admittedly.
It is early to make this analogy.
I agree.
However, the time to stop a war is before it starts, the time to close Gitmo is before
it opens, and so on and so forth.
The time to stop this is now.
Historically, when a demographic decides they want to get rid of the current system of government
and put in a new one, two factions develop.
A military wing, that's the one everybody knows about, and a political wing.
The military wing, those are the people that go out and use violence or the threat of violence
to influence those beyond the immediate area to achieve a political, religious, ideological,
or monetary goal.
Everybody knows about those folks.
They're the ones in the news all the time.
And then there's the political wing.
The political wing, their job is to, well, justify, excuse, downplay, and cover up for
the military wing.
This is a truism.
This occurs all over the world.
This is something that happens.
You know, I apologize.
If you live in Ireland or the UK, understand I use that conflict as an example because
in the United States it got romanticized, and Americans who have never had to deal with
this firsthand, they can relate to it.
That's the reason I use it so often.
In Ireland, you had Sinn F??in, right?
That was it often called the political wing of, right?
In the movie Patriot Games, there was that line, the guy he argued with in the bar said,
cut off your money, you'll be throwing rocks in the street, something like that.
It's the political wing.
They justify, they fundraise, they downplay.
You know, they come out and make statements.
I'm like, you know, I can't condone what happened, but I understand where the hatred comes from.
And the only way to get through this is to whatever the military wing's goal is.
It is a truism.
It's something that occurs constantly.
What do you think these politicians, these Republican politicians were going to do on
this committee?
The exact same thing.
The exact same thing.
They're going to justify, excuse, downplay, and attempt to cover up what happened.
Understand these people voted to undermine the election.
It was an attack on American democracy.
They want to replace the current system of government.
They want to get rid of the Constitution.
It's what they want to do.
The overreaching goal of this is to install a strongman leader, an authoritarian.
Now do all of these politicians, do they support this?
I don't know.
I don't know their motives.
I would imagine that many of them are just trying to appeal to a certain voter base.
And that's how it always starts.
In the beginning, when these movements form all over the world, it's not like they have
a meeting and everybody says, oh, I'm going to be on the political side, I'm going to
be on the military side.
That's not what happens.
People who are loosely ideologically aligned start moving in the same direction and covering
for each other because they're kind of on the same team.
Think of it like normal politics, but with, you know, the wild stuff happening on the
side.
We know it occurs the way people will justify or excuse a politician who gets involved in
a scandal who's on their team.
It's the exact same thing.
There's no coordination in the beginning.
It just progresses.
And eventually it becomes more formalized.
Right now we are in the beginning stages of this.
I think the most beneficial thing that somebody could do if they wanted to be a leader in
the political wing of this movement would be to disrupt these hearings.
So those who have shown themselves to either be ideologically aligned or are willing to
do whatever for votes, they shouldn't be on it.
They shouldn't be given that opportunity.
They shouldn't be put in a position where they can solidify leadership.
What's the right move from a military standpoint?
Please understand that things in the United States have evolved and are still at the point
where what is best from a military standpoint is a valid discussion when you're talking
about committee assignments in Congress.
If you think this is over, it is not.
It's still moving.
They're still trying to invigorate this change of government to an authoritarian system.
Whether or not these particular politicians understand that, want to be a part of that,
or just trying to get reelected, I don't know.
And honestly, I don't care.
But her decision to stop those who are at least willing to appear to be ideologically
aligned with those who attempted to stop the election, stop the certification, her decision
to bar them from being on this committee is the right one.
It may not be the right one from a partisan standpoint.
It may make it look like the whole committee is just a partisan witch-hunter or whatever.
I think they're going to have to make the case, actually say what's going on, point
out the historical realities, stop treating Americans like they can't understand anything.
The American people are not... it's not like they're incapable of understanding these things.
The information just has to go out there and people have to be willing to call it what
it is.
This was an attempt to undermine the Constitution.
It was an attempt to change the system of government of this country.
That was the goal.
They wanted to stop the election.
They wanted to overturn it.
Those people who supported this idea, they're not gone.
Those politicians who would be willing to play into it, to get votes, to secure their
own short-term political futures, they're not gone either.
So maybe she handed Republicans a gift by making them reconsider who they're going to
put on this committee.
Maybe that was a political gift.
Maybe it's good red meat for Fox News to scream about.
They were going to do that anyway.
And had these people been allowed on this committee, it would have turned into a circus.
From a military standpoint, this was the right move.
Deny the opposition that platform.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}