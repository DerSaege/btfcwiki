---
title: Let's talk about Tom Barrack and influence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WB1OVMCZI0s) |
| Published | 2021/07/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the context of Tom Barrack being indicted for allegedly operating on behalf of the United Arab Emirates, a close associate of former President Donald Trump.
- Addresses the questions of how this situation is not considered treason or espionage.
- Differentiates between the common usage and legal definition of treason in the U.S., stating that the allegations do not meet the narrow legal criteria for treason.
- Explores the concept of espionage, mentioning that if the allegations are true, Barrack was acting as an agent of influence for a foreign power.
- Emphasizes the significance of agents of influence in shaping policy and public opinion.
- Notes that while common parlance may label the actions as espionage, the legal statutes have specific requirements that may not be met based on the released information.
- Raises the point of potential plea deals based on the volume of emails and communications possessed by federal authorities.
- Stresses the gravity of the situation, considering the direct access Barrack had to the President and the implications of his alleged actions.
- Comments on the importance of transparency regarding working for foreign powers, especially in close proximity to policy-making circles.
- Anticipates that the situation will likely evolve into a major story, given the implications.

### Quotes

- "An agent of influence having direct access to the President of the United States, that's kind of a big deal."
- "I'm not certain how this is going to play out, but I can assure you that if you were a counterintelligence officer and you found out that somebody who was operating on behalf of a foreign power had direct access to the President of the United States and to other government officials who were operating in the region of that foreign power, you'd be very concerned."
- "This is a big deal."

### Oneliner

Beau explains why Tom Barrack's actions, though not meeting the legal definitions of treason or espionage, could still have significant implications if proven true.

### Audience

Political analysts and concerned citizens.

### On-the-ground actions from transcript

- Monitor developments in the case and stay informed about updates (implied).
- Advocate for transparency and accountability in relationships with foreign entities (implied).

### Whats missing in summary

Insights on the potential broader impact of individuals acting as agents of influence within political circles.

### Tags

#TomBarrack #DonaldTrump #AgentOfInfluence #Treason #Espionage #ForeignPolicy


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Tom Barak because as soon as news broke, I started getting
questions.
Two questions kept popping up over and over again, and we're going to get into those.
For context, Tom Barak is the person who I believe Roger Stone described as the best
friend of former President Donald Trump, and I think he said he was the only person Trump
viewed as a peer.
Somebody who had a lot of access to the former president.
He has been indicted, and it is alleged that he was operating on the behalf of the United
Arab Emirates.
At this point, before we get into the questions, we need to acknowledge these are allegations.
Innocent unless proven guilty and all that stuff.
So the questions that have come in after just a fraction of the evidence has been released,
how is this not treason, and how is this not espionage?
In common conversation, treason loosely means betraying the United States.
When it comes to the law, treason in the United States is very, very specific.
In the U.S., the laws are very narrow.
It's not like other countries.
You have to try to commit treason in the U.S.
It's stuff like levying war against the United States, adhering to an enemy, stuff like that.
So it's not treason.
It's not treason statutorily.
You want to say it's treasonous?
Sure, in common conversation.
But as far as charges, nothing I have seen even comes close to that level.
How is this not espionage?
Again, same thing.
You have the common conversation use of the term espionage, and then you have the statute.
Common conversation means it's the intelligence world.
If the allegations are true, it appears to me that Barrack was acting as an agent of influence.
That is definitely part of the intelligence world, an underappreciated endeavor, if you
ask me.
It doesn't get a lot of press because, you know, there's no bungee jumping off of reservoirs
or skydiving or hanging out at casinos, but it's far more productive than any of that
stuff.
Acting as an agent of influence is where you are employed or directed by foreign power
to influence policy, public opinion, stuff like that.
We've talked about it over and over again on this channel.
Intelligence work is about determining other countries' intent.
It's a whole lot easier to do that if you can help guide their intent, if you can help
tell them what they're going to do.
So agents of influence are incredibly important.
Based on his actions, yeah, that's what it looks like if the allegations are true.
So sure, in common conversation, espionage fits.
Statutorily, there is no requirement for it to be a hostile nation, but I think there's
a part of the elements of the offense that require the transmission of classified material
or information related to national defense.
So far in what has been released, I haven't seen any of that.
So that's probably why that isn't what was charged.
That being said, it is reported that the number of emails that the feds possess, the number
of communications, is in the tens of thousands.
So they may have information like that, and they're just holding it in reserve to secure
a plea deal.
You know, you better plead guilty to this, or we're going to talk about this specific
email or text message or whatever.
So how serious is this?
How much does this matter?
A lot. An agent of influence having direct access to the President of the United States,
that's kind of a big deal.
And that does appear to be the situation, assuming the allegations are true.
But we're going to have to wait and see on this.
Operating on behalf of the United Arab Emirates, in and of itself, not a huge deal.
Not declaring it, not saying, hey, I'm working for them.
That's important, especially if you're going to have direct contact with major shapers
of policy.
I'm not certain how this is going to play out, but I can assure you that if you were
a counterintelligence officer and you found out that somebody who was operating on behalf
of a foreign power had direct access to the President of the United States and to other
government officials who were operating in the region of that foreign power, you would
be very concerned.
This is a big deal.
This will develop into a very large story, I would assume.
So that's where we're at.
Definitely not treason, probably not espionage, based on anything we've seen.
But it does appear to me that if these allegations are true, that he was acting as an agent of
influence for a foreign power, which is, you can safely say espionage in common conversation.
It wouldn't be that statutorily.
But again, they're allegations at this point.
Let's see what happens.
I'm going to suggest that there are a whole lot of people who probably came in during
the Trump administration who have no idea how any of this stuff works.
I would imagine that some of them, a lot of them, probably acted as agents of influence
without actually knowing what they were doing, without even knowing that it's wrong.
Now, to be clear, that's not going to matter when it comes to federal court.
It's just a matter of doing it or not doing it.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}