---
title: Let's talk about Trump's buyer's remorse over Kavanaugh....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ve2FpqlvjCU) |
| Published | 2021/07/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Donald J. Trump expresses buyer's remorse regarding Supreme Court Justice Kavanaugh.
- Trump saved Kavanaugh's life, suggesting he couldn't even get a job at a law firm without him.
- Trump expresses disappointment in Kavanaugh's lack of courage and inability to make great decisions.
- Trump is surprised by Kavanaugh not overturning the election, considering it a betrayal.
- The lifetime appointment of Supreme Court justices prevents influence and pressure from those who nominated them.
- Setting a mandatory retirement age might be a good idea, but the lifetime appointment serves a purpose.
- Removing the ability for elected officials to pressure justices ensures brave decisions.
- Despite Trump's regrets, Kavanaugh is likely to remain in his position for a long time.
- The system of lifetime appointments stops elected officials from being able to remove justices easily.
- Trump's pattern of making bad hires shows his need for a good HR director.

### Quotes

- "Who would have had him? Nobody, totally disgraced. Only I saved him."
- "The fact that the president once again made a bad hire, the former president made a bad hire, hired somebody he regrets."
- "That's why it exists."
- "If we have learned anything from the former president, it's that he really needs an HR director."
- "Have a good day."

### Oneliner

Former President Trump expresses buyer's remorse over Supreme Court Justice Kavanaugh, revealing his disappointment in Kavanaugh's decisions and the purpose behind lifetime appointments.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact your representatives to advocate for reforms in the appointment process (suggested).
- Join advocacy groups working towards setting a mandatory retirement age for Supreme Court justices (suggested).

### Whats missing in summary

Insights on the potential impact of Trump's hiring decisions and the importance of informed appointments to key positions.

### Tags

#DonaldJTrump #SupremeCourt #LifetimeAppointments #Kavanaugh #PoliticalInsights


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about former President Donald J. Trump's buyer's remorse
when it comes to Supreme Court Justice Kavanaugh because Trump's not happy.
The book came out, landslide, it's got some pretty entertaining quotes in it.
We're going to talk about the quotes, partially because it's funny, and also because it helps
answer the single most common question I get about the Supreme Court.
Okay, so, what did Trump say?
Where would he be without me?
I saved his life.
He wouldn't even be in a law firm.
Who would have had him?
Nobody, totally disgraced.
Only I saved him.
To be clear, the former President of the United States is suggesting that he nominated a person
who was totally disgraced, who couldn't get a job at a law firm, to a lifetime appointment
on the Supreme Court.
That may not have been wise, just throwing that out there.
He goes on.
I can't even believe what's happening.
I'm very disappointed in Kavanaugh.
I just told you something I haven't told a lot of people.
In retrospect, he just hasn't had the courage you need to be a great justice.
I'm basing this on more than just the election.
Of course you are.
You know, Kavanaugh did not overturn the election for the former President, and this of course
is seen as a betrayal by Trump.
Two people who I believe are wholly unfit for the offices that they held or currently
hold, provided a great civics lesson.
The single most common question I get about the Supreme Court is why are these appointments
for life?
This is why.
This is why.
I'm surprised nobody on Trump's team briefed the President on the fact that Supreme Court
justices tend to not grant favors to the people who appointed them.
The lifetime appointment makes sure that once a judge is appointed, those who nominated
or voted to confirm, they don't have much influence.
They can't really, they can't pressure the justices.
That's the point of it.
Now, you can make the argument that maybe we should set a mandatory retirement age,
and I think that might be a good idea, but the inability to remove them, or as difficult
as it is, that serves a purpose.
It stops people like Trump from being able to pressure the justices into making brave
decisions.
Now realistically, we're stuck with Kavanaugh for a long time.
It's unlikely that he's going to leave any time soon, but if you ever wondered why
they're set up to be lifetime appointments, this is why.
It removes most of the ability of elected officials to pressure them, because it's
not like they can say, well, if you don't do this, we're just going to remove you.
No, that's super hard.
You're not going to be able to swing that, and it'll be really apparent why you're
doing it if you try.
So that's why it exists.
The fact that the president once again made a bad hire, the former president made a bad
hire, hired somebody he regrets, and I'm sure soon he will say he doesn't even know
the guy, that's not a surprise.
If we have learned anything from the former president, it's that he really needs an
HR director, because he doesn't really have good instincts when it comes to picking
people.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}