---
title: The Roads with Alexandra Hunt, candidate for US House....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vRvrmbHXf_8) |
| Published | 2021/07/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Introduces the guest, Alexandra Hunt, who is running for Congress in PA3, a Democratic district in Philadelphia.
- Alexandra shares her background in health care and public health, her motivation for running for Congress, and her challenging incumbent who does not support progressive policies.
- Alexandra talks about her journey through college, working as a stripper and server to pay bills, and obtaining master's degrees while working full time.
- The importance of harm reduction in criminal justice reform and decriminalizing drug use and sex work.
- Restorative justice, based on Alexandra's personal experience as a survivor of sexual assault, focuses on accountability and understanding harm.
- Alexandra's platform includes justice for tribal nations, housing rights, gentrification, economic justice, wealth tax, environmental justice, and ending food insecurity.
- Her commitment to representing the will of the people through town halls, accessibility, and engagement with the community.
- The tipping point for Alexandra's decision to run for office was witnessing the government's failure during the pandemic and the lack of support for vulnerable communities.
- Alexandra's grassroots campaign approach to avoid corporate influence and stay true to ethical principles.
- Soccer is a passion outside of politics that Alexandra expresses enthusiasm for.

### Quotes

- "Harm reduction is decriminalizing drug use, the same way that I have a platform to decriminalize sex work."
- "Justice, to me, is not putting someone away in prison. Justice, to me, is a person understanding the harm that they have done."
- "Every person should be housed and be able to stay in their family home without threat of violence or new construction pushing them out."
- "The money that comes earlier allows you to play out your field game. And so it's hard."
- "My big game plan was just to have as much courage as I could and just be OK with being the only person in a room for a bit."

### Oneliner

Beau talks with Alexandra Hunt about her journey from health care to Congress candidacy, focusing on progressive platforms like harm reduction, restorative justice, and grassroots campaign ethics.

### Audience

Voters in PA3 district

### On-the-ground actions from transcript

- Attend town halls and community events to voice concerns and needs (exemplified)
- Support grassroots political campaigns financially or through volunteer work (exemplified)

### Whats missing in summary

The full transcript captures Alexandra Hunt's diverse platform and personal journey towards running for Congress, offering insights into her motivations, advocacy for progressive policies, and dedication to grassroots campaigning.

### Tags

#AlexandraHunt #Congress #PA3 #ProgressivePlatform #GrassrootsCampaign #HarmReduction #RestorativeJustice


## Transcript
Well, howdy there, Internet people.
It's Beau again.
And today we are going to be talking with Alexandra Hunt.
And we're going to go over her path, her roads that
led her to where she is and where she is wanting to go,
which is the house.
So Alexandra, tell us a little bit about yourself.
Hi there.
My name is Alexandra Hunt.
I am running for Congress in PA3.
It is the most Democratic district in the nation.
It's located in Philadelphia.
And I'm running against an incumbent who does not support
Medicare for All or the Green New Deal.
I am challenging him from the left.
And we launched in February.
Essentially, prior to that, I was
working in cancer therapy doing research
and have a public health background.
And our government did not show up for us
enough during this pandemic to see us through
and to lead us into a viable future.
So that was when I decided to run.
OK.
All right.
And so before you were in health care, what did you do?
So before I was in health care, I've
been on quite a journey as a student.
I started in college at University of Richmond.
And during that time, I was working two jobs.
I worked as a stripper.
And I worked as a server to pay my bills.
Then straight from there, I went into a master's program
located in Philly at Drexel.
And I got my master's of science in interdisciplinary health
sciences.
I went into the workforce and just
found that I wasn't quite getting the footing that I
wanted, even having a master's degree.
So I pursued a master's of public health
while working full time.
And I just graduated with that in August 2020.
And I'm now working full time and running a campaign.
Congratulations.
Sounds wonderful.
So obviously, somebody is going to say something about that.
Because I mean, you were a server.
You did something that wasn't illegal.
And you didn't hurt anybody.
And you did that other thing, too,
that is also exactly the same.
OK.
So now that we've covered that, let's
talk about your platform and what you want to do.
I saw something in here about harm reduction.
Yeah.
So a big part of my platform is about criminal justice reform.
And ending the war on drugs is a major issue
that we are so far behind.
It's contributed to mass incarceration and just people
living in cages because of the war on drugs.
And drug use, if you take a step back,
it's really not a place for government.
And it's not criminal activity.
It's not breaking the law, or it shouldn't be.
So harm reduction is decriminalizing drug use,
the same way that I have a platform
to decriminalize sex work.
They really go hand in hand.
And it's just a platform supporting and uplifting
bodily autonomy.
So harm reduction would, part of that platform,
is to push us towards a safer supply program, which
is something that they're running in Canada.
And it uses our pharmaceutical company,
our pharmaceutical industry, to create safe drugs
so that people can use them safely and not overdose.
So at the end of the day, this part of your platform
sounds a whole lot like no victim, no crime kind of thing.
Yeah.
Yeah.
And I have issues with the word crime and criminal.
But sometimes things happen where there is harm done.
And that's exactly where restorative justice
needs to play a role.
Outside of that, we really don't need policing.
OK.
Let's jump into restorative justice.
This is a term that those outside of the left
may not have heard.
So give us a basic summary.
I am a survivor of two different sexual assaults.
And that was really how I first was introduced
to restorative justice.
And justice, to me, is not putting someone away in prison.
Justice, to me, and to restorative justice,
is a person understanding the harm that they have done,
the harm that they have inflicted
on a person or a community, and taking accountability
for that, taking responsibility for that,
and taking measures to ensure that that harm will never
be inflicted again.
And putting someone in prison, they don't learn any better.
And their quality of life drastically drops.
And it's just a form of punishment.
It's not a form of justice.
That's right.
Your platform overall, that word justice
appears like every other line.
No.
I believe it.
Yeah, it's a really common theme here.
So we have, let's see.
Justice for tribal nations.
With where you're heading, how are you
going to be able to influence that?
How are you going to be able to help engage
in tribal empowerment?
Well, I've done some work on the Oglala Lakota Reservation
out in South Dakota.
And they are so deprived and neglected of resources.
And there is a current relationship
of tribes in the United States government
that is paternalistic.
And the United States government feels
they need to come in and tell tribes how they should
and shouldn't live.
And it's really robbed them of their culture.
And so in order to provide justice for them,
they should have the means to live and the means to thrive
and then empower them to restore their culture
and live life as best they can.
OK.
You also have a lot of stuff about housing and tenants'
rights and ending gentrification and segregation
is on the platform, which I'd love to hear your take on that.
And how it explained is how it's gone legally,
but that doesn't really necessarily mean
that it's gone.
Right.
And that really comes from how certain neighborhoods
in Philadelphia, and this is true in many different cities,
that you can walk through the city neighborhood
by neighborhood, street by street,
and see different environments.
And that comes from the investment
into certain neighborhoods and the divestment
or just lack of investment completely
to other neighborhoods.
And the divide there is traditionally, historically,
along race lines.
And so we need to address that and make sure
that we are investing in all our neighborhoods
without pushing people out of them.
Every person should be housed and be
able to stay in their family home
without threat of violence or new construction
and development, raising taxes, and pushing them out.
So that's a big issue here in Philadelphia.
Gentrification is huge, and really nobody's
talking about it.
Right.
So this is very much a housing for all platform.
One of the other things that I heard or I saw
was something about just cause for evictions.
But it didn't say what those just causes would be.
So I'm curious as to what they are.
I haven't developed that as much.
I was working with a rep from the National Homes Guarantee.
I haven't developed that stance as much.
OK.
And then under economic justice, so we've
got the traditional left-leaning platform here.
You've got strong union rights, wealth tax, pay equity.
So how does the wealth tax function in your mind?
Like when it goes into effect, what's it going to do?
It's going to tax the wealthy.
The way that our taxes are set up,
the more you make, the bigger a tax break you get.
And so it's pulling more money from people
who need it more to survive and have a dignified everyday life.
And it's pulling less money from people who have extra
and surplus of wealth.
And so it's going to be targeting those people
with that surplus because they really don't need
to be 100,000 billionaire.
Right.
OK.
In the environmental justice thing,
obviously there's support for the Green New Deal.
And then there's conserving our land,
protecting endangered species.
And then there's environmental justice for Philly
and ending food insecurity.
So what is that?
One of the neglected issues is that as we face our climate
emergency and looming climate crisis,
our food supply is threatened.
And of course, in an unequal society,
the people who get threatened first
are the people who have the least access to food.
And so we need to end food deserts.
We also need to address the climate crisis
and head towards green infrastructure, green jobs,
and starting to restore our environment.
It might be too late at this point,
but that doesn't mean that we should stop fighting for it
and trying to slow it down.
So there needs to be an acknowledgment
that our food supply, our food chain,
is being threatened by the climate crisis.
And we need to be prepared for that.
And we need to be prepared to fight the climate crisis
and that in Philly, the people who
are first to be without food, it's a big issue here in Philly.
We have big food insecurity, are the people
who have the least access to it, whether it's
by transportation or affordability.
OK.
All right, that makes sense.
So when you're in the House, you are representing your area.
I don't want to say a small district, because it's not,
but geographically, it's a small area.
How do you plan on actually representing the will
of the people from Philly?
How are you going to know what they want?
How are you going to stay in contact?
Town halls where people get to speak instead
of just being spoken to, having easy accessibility,
calling hours to just hear from the district,
and providing that dialogue, going to events,
being present, hearing what the struggles are.
The same way that I am out and about in community today,
and even before I was a candidate,
I would continue to do that through representing
and serving PA3.
That's how you learn what the community needs are
and the community solutions.
So when it comes to your background in health care,
when you say that the pandemic kind of helped prompt this,
prompt your run, is there anything specific that kind
of pushed you out there?
I was so angry and frustrated with our government
as just the whole way through the pandemic.
The fact that it failed to be mitigated,
and then the talk about saving our economy instead
of saving lives, and how if we had just put value
and not treated people as disposable,
we could have reduced our deaths from COVID-19
by hundreds of thousands.
And I did as much as I could to help my community.
I was at testing sites, and then later
at vaccine distribution sites.
I was getting out menstrual items
to school-age children who that was their only supply,
was going to school and getting those items.
And I was at a food distribution site.
And one of the times that I was at a food distribution site,
I just looked at how long the line was.
And it wrapped around the building and down the street.
It was extraordinary.
And it was just one food distribution site
compared to the hundreds of pop-up ones
that we had going during the pandemic.
And that's how many people in Philly, in the United States,
in one of the wealthiest countries in the world,
were without food.
And our government wasn't doing anything about it.
And so that was what my tipping point was, was I'm going to run
and I'm going to try to get people the resources
and the help that we need.
It almost sounds like you're running reluctantly,
like this isn't something you actually wanted to do,
that you're just like, nobody else is going to do it.
I don't think this was in my life plan.
It was never a goal of mine.
I'm not running reluctantly.
There's good and bad to running for office.
And I definitely chose to do this.
But I always thought of politics as a very dark, dark place.
And politicians, and politicians, and politicians
were just full of greed and selfishness
and serving personal interests.
And it really wasn't until Bernie Sanders
that I had kind of an awakening, like, oh, American politics
could actually be meant for somebody like me,
not thinking that I would ever run for office.
And then the squad stepping up and standing
for this progressive platform that I really believe in.
And then with the pandemic and no one else is doing this,
our representative certainly isn't fighting for us.
I'm going to put myself out there.
I'm going to throw my hat into the ring.
So you're going to be facing a primary.
How do you think it's going to go?
I mean, obviously, you know it's going to be tough.
You know there's going to be stuff that's slung out there.
And you're right.
You know, you're saying I view politics as this dark space.
And having covered it for years, it totally is.
They're going to drag out everything that has ever
happened in your entire life because they
can paint you that way.
That they can do the whole she's a bartender thing
like they did with AOC.
And you're not naive.
You know that's going to happen.
So I'm curious, how did you steel yourself for that?
How did you ready yourself because you know this is coming?
You know it's going to happen.
It's just worth it.
Is that what I'm hearing?
Yeah, it's absolutely worth it.
I mean, I had to sit down and go through my life
and pick apart all the different pieces of me,
things that I used to be ashamed of,
or things that I didn't necessarily
want everyone to know.
And just outed myself.
Just I'm going to put this out in the open.
I'm going to be completely vulnerable.
And people will either take it or leave it.
And that is the most that I can do.
And so I've been hoping that I can kind of soften anything
that they're going to use against me,
because it's already out there.
And people are like, oh, yeah, we heard that from her.
But that doesn't mean that the machine in Philly
doesn't play dirty.
And I know that they do.
When I first launched, I had a reporter approach me and say,
Dwight Evans knows where all the bodies are buried.
How do you plan on navigating that?
And it's like, yeah, I know I'm up against a deeply entrenched
person in establishment politics.
But we have to try.
Because if we don't try, if we don't speak up,
if we don't do anything, we are also complacent with the status
quo, and nothing will change.
Right.
Yeah.
I mean, it's just I'm picturing the campaign.
And I'm just, I know it's coming.
And the reality of it is, from where I'm sitting,
this is not something that matters.
This is not something that matters.
This isn't a scandal.
This is something that a whole lot of people do.
And I don't see it as a black mark against you.
But I also am pretty familiar with the machines up in that area.
So I think you're right.
I think getting it out early and just owning it,
I think that's the right move.
I found out about it on TikTok.
That's how I found out.
And I think that's a smart move.
But back to your platform.
You have a section on women's rights,
and it has reproductive rights in there.
And supports every person's religious freedom and right
to choose, which is why she's a proponent
of reproductive justice and advocates
for inclusive reproductive legislation.
Fight to repeal the Hyde Amendment,
oppose efforts, expand WIC.
It's a solid platform.
This section right here is incredibly specific.
Is there anything that you would want to add to this?
The same way that dismantling white supremacy
is interwoven throughout many different pieces
of the platform, women's rights and dismantling the patriarchy
is interwoven throughout the platform.
So in criminal justice reform and economic justice,
those are big issues that also pertain to women's rights.
And one of the big pieces, one of the big pillars
of my platform is education.
And that has really been a way to,
I think education is the key to our democracy.
And that if we can fix our education system
and get it to a place where it's equitable
and it's teaching true American history
and really what has happened in our past,
and it provides a platform and opportunity
for where people can go, that that
is going to be our key to moving forward as a country
and as a civilization.
Now, you have the ending gun violence section here.
And first, the fact that it has the part
about closing the DV loopholes and disarming domestic abusers,
that's fantastic, because it's something
that's missing from a lot of people
who are supportive of gun control.
This isn't on there.
There's a section in here that cracked me up.
So it is ending gun violence.
That's your main category here.
And demilitarize the police is in there.
Yeah.
Yep, I think it contributes.
And it seems so hypocritical to me
that we point fingers at different neighborhoods
and different people who are suspected of gang involvement.
And then we militarize our police up to their necks
and just accept that they kill civilians.
And so if we want to end gun violence,
we have to address every single area
that happens in our society, including police.
That's what accountability looks like.
So that is a very neglected piece of many, many politicians,
many, many Democrats' platform.
But it needs to be part of it.
Yeah.
And for those who are listening who aren't going to go look it
up later, it does talk about the 1033 program
and shutting that down.
This is in here.
The overall platform for ending gun violence
is universal background check, safe gun storage, ghost guns,
demilitarize the police, and disarm domestic abusers.
So that's kind of the overview of that.
So let me ask you this.
Let's say you win.
You're heading up to DC.
And then you're there with not just one machine,
but hundreds of them.
And it doesn't sound like politics is something
that you sought to get into.
It wasn't part of your life plan.
How do you think that adjustment's going to go?
Oh, I don't foresee it being a very good time.
I do not have one of the things that my father said to me
when I told him that I was going to run for office.
He was like, he supports it.
But he said, Alexandra, you're not political.
And I'm really not.
I say exactly what's on my mind.
And I tell it how it is.
And so I'm not going to be playing those games in DC.
And I don't think that the people who
have been there for years are going to like that very much.
But when I make it to Washington and make it to Congress,
I have a job to do.
And that is to get the platform that I ran on passed.
Because that's why people voted me in.
And then we would have done our job.
That's my agenda.
It's not smooth talking and buttering up
different representatives.
It's just, how are we going to get the job done?
Power mapping Congress, where do we
need to put pressure here, there, get this passed, get out.
You actually put way more thought into that
than I expected to hear in that answer.
So you do have a plan.
You actually, OK.
So let's see here.
I'm sorry.
I think I might have missed.
You already went to the education thing.
Kind of threw me through a loop.
No.
No.
That's OK.
Education's a big, I'm the daughter of two teachers.
And as I said, I think education plays a bigger role
than we really give attention or funding to.
I think education, or the divestment from education,
was the basis for our January 6th campaign.
It was for our January 6th insurrection.
So we can talk about education for a while.
I'm happy to.
I would actually love to hear a correlation
connection between the 6th and defunding schools.
Yeah, go off.
I want to hear this.
Have you heard it before?
I haven't heard you say it, no.
Yeah, well, January 6th was essentially,
we saw the polarization that has happened
and how the Republican Party has been fed
this propaganda of what they, I don't know if you watched,
I mean, the media had people who were breaking
into different chambers and whatnot in the Capitol
building.
But if you watch the people marching there and the things
that they were saying, they really
thought that they were about to do something very heroic.
And they were defending our country.
And that is because they had been fed these, essentially,
lies about what our country is, what our country has done,
and where we came from.
When I, as I said, my parents are both teachers.
And my father is my American history teacher.
And he provided a very well-rounded picture
of American history that I didn't
realize I should be grateful for until I ended up
at school at University of Richmond,
which is in Richmond, Virginia.
And I heard people referring to the Civil War
as the War of Northern Aggression.
And so it's things like that that
contributes to our polarization and the discrimination
and bigotry that can lead up to something like the January 6th
insurrection.
Yeah, people can exploit the ignorance on different topics.
And push them different directions, yeah.
So how are you going to fund this task of taking down
the machine there?
For the campaign?
Well, we are running a fully grassroots campaign, which
makes it very, very hard.
But the purpose of it is to demonstrate to voters,
we cannot be bought.
We will not sell out to corporate donors.
We're not going to sell out to a corporation.
And it's still very, very hard.
You have to go person to person, dollar by dollar,
and just raise as much money.
And the money that comes earlier allows you to kind of play out
your field game.
And so it's hard.
I can't express that enough.
It is so hard to run a fully grassroots campaign.
But the ethics and morality behind it is why I'm doing it.
Right.
I mean, it would definitely be easier to stick your hand out
to some corporate donors.
But at the same time, politics is a dark place.
And anybody's going to want something in return.
So out of your platform here, there's
one of the things I've noticed about most people who
enter politics is that, aside from their platform,
they have something they're really passionate about.
Maybe they've included it, but they didn't express it fully.
What is it out of all of these topics, what
are you most passionate about?
What are you going to fight for?
Oh, I thought we were saying something
that's not on the platform that we are most passionate about.
OK, what are we going to fight for?
Ending mass incarceration and fighting for freedom
in a time of mass incarceration, education,
and making sure it is opportunistic and equitable
for all people in the country.
We're going to fight for housing and universal health care.
And we're going to fight for racial justice.
So now I want to hear what's not on the platform
that you're really passionate about,
because it looked like you had an answer for that.
Soccer.
Soccer?
Yeah, I thought we were going to talk about soccer for a second.
I got excited.
Anyone who knows me and talks about running for office
and just building a grassroots movement,
I am constantly comparing things to the soccer field
and I think that soccer could change the world.
Or football.
I know it's not very popular in America,
but it's been something that has connected me
to an international group.
And it's a big love of mine.
All right.
So is there anything you feel like we missed
or anything you definitely want to cover?
We'll have the links to your site
so people can review the platform.
And I'm sure your donation links are on there too.
But is there anything that you specifically
want to say to people who are watching, who are listening,
who may live in your district?
When I first launched this campaign, it was small.
A few people knew about it.
But we were determined to do it and to run as hard as we could.
And a couple of people, after we had launched,
approached me and said, what is your big game plan?
And really, my big game plan was just
to have as much courage as I could
and just be OK with being the only person in a room
for a bit and see if this vision clicked with people.
This platform clicked with people, and it's been clicking.
And as we grow our movement and the volunteer support
and the monetary support and just the general outpouring
of support, it is so humbling.
And I am so grateful for people like you
who help us build our platform and just anyone
who is contributing and is doing so much for us.
And I'm so grateful for that.
And I'm so grateful for the people
who are contributing in any way that they can.
It's really deeply appreciated.
All right.
OK.
So unless there's anything else, we're going to wrap it up.
And so this is Alexandra Hunt running for PA3, right?
PA3 in 2022.
Thank you.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}