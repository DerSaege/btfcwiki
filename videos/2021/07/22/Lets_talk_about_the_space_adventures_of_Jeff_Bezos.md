---
title: Let's talk about the space adventures of Jeff Bezos....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nAr5ZJFWgbY) |
| Published | 2021/07/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls childhood fascination with a giant rocket in Florida and the idealistic nature of space travel.
- Expresses initial concern over billionaires turning space exploration into a hobby.
- Mentions the "overview effect" experienced by astronauts, where they realize the fragility of Earth from space.
- Comments on Jeff Bezos going to space and his subsequent interview about climate change.
- Criticizes Bezos' idea of polluting space by moving heavy industry there.
- Emphasizes the importance of not repeating past mistakes like burning stuff or burying toxic waste.
- Advocates for finding a balance with Earth and changing the profit-driven mindset to push humanity forward.

### Quotes

- "Let's not pollute space. Let's not move all heavy industry."
- "Maybe the best idea is to figure out how to live in some kind of balance with earth."
- "That personal profit motivator rather than the drive to push humanity forward, that is something we have to change."

### Oneliner

Beau questions the wisdom of polluting space with heavy industry and advocates for prioritizing Earth's protection over interplanetary endeavors.

### Audience

Activists, Environmentalists, Space enthusiasts

### On-the-ground actions from transcript

- Advocate for environmental protection on Earth (implied)
- Educate others about the importance of sustainability (implied)
- Support initiatives that aim to protect the planet (implied)

### Whats missing in summary

Beau's emotional delivery and personal connection to the topic might be best experienced by watching the full video.

### Tags

#SpaceExploration #ClimateChange #EnvironmentalProtection #Sustainability #Billionaires #ProfitMotivation


## Transcript
Well howdy there internet people, it's Bo again.
So today we are going to
talk about Jeff Bezos and his space adventure.
You know I have to admit
I love the idea of space travel.
You know when I was a kid
we used to go from
Tennessee
down to Florida all the time and traveling along the roads
there's this giant rocket
and back then you could actually walk underneath it. I guess now there's chain
link up around it and you can't do that.
But it was really impressive to me.
I always pictured
the people that would go into space would be
idealistic in nature. People wanting to push humanity forward
to boldly go where
humanity hasn't gone before.
It's what I pictured.
I was picturing Star Trek
basically.
I was a little concerned
when a bunch of billionaires decided to make it their
newest hobby.
But then again
that idealistic nature that I have
there's this thing called the overview effect
that astronauts
succumb to or experience whatever.
They get up there
and they see the planet just suspended in nothing
and they realize the fragility of it all.
I'm thinking you know maybe it'll do them some good. Maybe they'll get up there
and
it will change them.
It'll be a
profound experience in their life.
So
Bezos goes up there
and
when he comes back he gives an interview and all I see
is we need to end climate change and
protect this beautiful gem of an earth.
I'm thinking wow
it happened. That's cool.
And then it's we need to protect this beautiful gem of an earth. Let's pollute
space.
Yeah, I'm picturing Star Trek. He's picturing episodes of Futurama.
No, Jeff.
Let's not pollute space. Let's not move all heavy industry. If you don't know the
quote it's
we need to take all heavy industry, all polluting industry and move it to space
and keep earth as this beautiful gem of a planet that it is.
No. I'm going to go ahead and just say no. That's a bad idea.
That's a bad idea.
I
understand that there was a time when people thought just burning stuff would
be okay.
We just push it out into the air and it disappears and everything's fine. Turned out that was wrong.
I can remember when people
believed that it was okay to just bury toxic waste and everything would be fine.
Turns out that that's not exactly the case either.
Perhaps
polluting space isn't a great idea either. There may be negative
consequences to that that we don't know about given the fact that we don't know
a whole lot about space.
Maybe that's not the best idea.
Maybe the best idea is to figure out how to
live in some kind of balance
with earth
or whatever planet we happen to be on.
That might be a better idea.
The
profit motivator
that exists in
most of the world,
that personal profit motivator rather than
the drive to push humanity forward,
that is something we have to change.
We have to change if we're going to make it.
This is a person who, in theory,
should have had
an experience that
altered
the way they view humanity itself, the way they view everything.
And instead he just picked a new spot to mess up.
It might be better
if we
figure out how to not mess up this planet
before we travel to other ones.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}