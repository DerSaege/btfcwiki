---
title: Let's talk about how we're interviewing people....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mjmh3hQ0PHM) |
| Published | 2021/07/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of interviews and explains the two series on his channel, one featuring interesting people and the other political candidates running in 2022.
- The purpose of the political candidate interviews is to allow them to talk about their platform and policies, informing viewers who may vote in 2022.
- Beau aims to determine if candidates genuinely believe in what they are saying or if they are just following instructions.
- He does not plan to push back on candidates' positions unless something objectively false is stated, focusing on letting candidates speak and viewers access unfiltered information.
- Beau uses the example of an interview with Erica Smith, a U.S. Senate candidate from North Carolina, to illustrate the approach of allowing candidates to present their platforms without engaging in debates or trying to change their positions.
- In interviews with interesting people like Chelsea Manning, Beau focuses on topics beyond what people might expect, aiming to show different aspects of their personalities beyond the usual questions.
- The goal of both types of interviews is to provide information and better inform viewers without getting into confrontational or sensationalist questioning.
- Beau's approach to interviews is about allowing guests to share their thoughts and projects rather than creating conflict or controversy.

### Quotes

- "The goal is to inform those people who choose to vote in 2022."
- "It's more about getting information out."
- "They'll be pretty friendly chats."

### Oneliner

Beau introduces two interview series on his channel—one featuring political candidates and the other interesting people—to inform viewers without engaging in debates or sensationalism, focusing on letting guests share their views and projects.

### Audience

Content Creators, Viewers

### On-the-ground actions from transcript

- Watch and share Beau's interview videos to better understand political candidates' platforms and learn about interesting people (implied).

### Whats missing in summary

The full transcript provides a detailed insight into Beau's approach to interviews, focusing on information dissemination and showcasing different aspects of guests beyond typical questions.

### Tags

#Interviews #Information #PoliticalCandidates #ContentCreation #CommunityInforming


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about interviews and how we're doing them.
So over on the other channel, on the Roads with Beau, we have launched, well it's really
two series of interviews, but it probably looks like one right now.
One is of interesting people, and the other is going to be of political candidates who
are running in 2022.
And I've already started getting questions about why we're doing it the way we are.
So we'll answer those real quick.
Those interviews with the political candidates, I got a couple variations all asking the same
thing.
Hey, I'm surprised you didn't push back on her gun control stance.
Why?
Because you know mine, right?
The interviews with the candidates, it's to get them to talk about their platform, their
policies, what they want to do, and find out a little bit about them.
The goal is to inform those people who choose to vote in 2022.
So if you want to find out what their policies are, you can watch the video, you can hear
them talk about it.
Most importantly, you'll be able to determine whether or not the candidate themselves knows
their platform, whether they actually believe what they're saying.
It'll become really apparent if they are just doing what they're told.
We'll see if they actually believe what they're saying.
For me to push back on any particular position, probably not going to happen.
If something is said that is objectively false, yeah, maybe, maybe, or maybe just provide
commentary afterward.
The goal is to allow them to talk, to get that idea out, to allow people access to the
information they need.
Because a lot of times when you're talking about political candidates, you get something
that is very managed.
You get sound bites.
You get press releases.
You don't get to hear them talk about it very often.
And that's the goal.
So there's not going to be a lot of debate.
I'm not going to push when it comes to gotcha questions and stuff like that.
That's not part of the plan.
Now when we did this with Erica Smith, she's running for Senate in North Carolina, U.S.
Senate to represent North Carolina.
It was very clear that she knew her platform.
Yeah, she's in favor of gun control.
It's in her platform.
That wasn't a surprise.
So there's no reason to try to debate that.
I doubt the three-term state senator from North Carolina is going to change their position
because Bo said so.
Doesn't seem like a good use of time.
The idea is to get the information to y'all.
Now with the interesting people thing, yeah, we interviewed Chelsea Manning and didn't
ask a single question most people would ask of Chelsea Manning.
We talked about her new upcoming project on YouTube, her new channel.
And that's going to be the goal of those.
There are a lot of people who, because of their actions, because of things that they've
done, they have a perceived personality.
Oftentimes that's not actually their personality.
They may be very different.
But if the questions are always focused on the same topics, well you never get to see
that.
So that's what those will be like.
They'll be pretty friendly chats.
In these interviews, I wouldn't expect to see a lot of debates or sparks fly or gotcha
questions because I'm not planning on having any.
It's more about getting information out.
So this hopefully will help better inform those people who are trying to make sense
of things.
It's the goal.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}