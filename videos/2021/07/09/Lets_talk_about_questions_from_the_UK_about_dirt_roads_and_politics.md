---
title: Let's talk about questions from the UK about dirt roads and politics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tPCoFAwa4Fc) |
| Published | 2021/07/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the fascination with dirt roads and pickup trucks in the United States.
- Addresses the misconception that dirt roads are just a movie trope or a political statement.
- Notes that a significant percentage of Americans live on dirt roads, especially in rural areas.
- Points out that fixing infrastructure won't end the U.S. fascination with SUVs and large trucks.
- Describes who typically drives SUVs and large trucks, debunking stereotypes.
- Talks about the identity influence and stereotype adoption in the U.S.
- Mentions the confusion around the term "dirt road Democrat" and the political leanings of rural vs. suburban areas.
- Explains why economically left-leaning rural areas often vote Republican due to social conservative issues.
- Reveals that many Americans vote based on perceived identity rather than actual beliefs.
- Suggests that reaching out to real rural Americans could benefit the Democratic Party economically.
- Comments on the elitism within the left-leaning party and the disconnect with rural Americans.
- Wraps up by acknowledging the confusing nature of U.S. politics from an outside perspective.

### Quotes

- "Welcome to the US."
- "Welcome to the United States."
- "The people who you would think drive the giant truck stopped."

### Oneliner

Beau explains the U.S. fascination with dirt roads, pickup trucks, and the political identity confusion between rural and suburban areas, shedding light on voting trends and stereotypes.

### Audience

American voters

### On-the-ground actions from transcript

- Reach out to real rural Americans to understand their economic concerns (suggested)
- Challenge stereotypes and misconceptions surrounding rural areas and their political leanings (implied)

### Whats missing in summary

The full transcript delves into the intersection of identity, politics, and stereotypes in the U.S., offering insights into rural voting trends and the disconnect between economic beliefs and political affiliations.

### Tags

#US #Politics #RuralAreas #Identity #Stereotypes #VotingTrends


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about dirt roads and pickup trucks.
Every once in a while I get a question from somebody from outside the United States, and
it makes me realize exactly how backwards the U.S. is in a whole lot of ways.
This is one of those instances, the question, a series of questions, has to do with dirt
roads.
Someone from the UK heard the term dirt road democrat, and they had a whole bunch of questions
about it.
Now, as you listen to this, especially if you live on a dirt road, understand the questions
are purely logical.
It makes sense every step of the way.
It's just the exact opposite of how the United States works.
Okay, so the first question was, you know, I kind of thought that dirt roads were a trope,
it was something in movies, it wasn't real, or something that politicians just say to
appear more rural than they are.
Remember that appear more rural than they are part, because that's going to be really
funny here in a minute.
So are there really that many?
Yes, I would say 90% of the people I know live on dirt roads.
They are that common, especially in more rural areas.
So the next part of this is, if the United States fixed its infrastructure, meaning paved
roads, would that end the United States' fascination and use of SUVs and large trucks
to include six-wheel trucks?
No, of course not, because generally speaking, the people who drive SUVs have never had dirt
under their tires.
That's not actually who buys them.
When it comes to large trucks, probably half of them are bought by people in the suburbs.
When it comes to four-wheel drive, I would say half have never been off-road, and half
of the half that has been off-road did so for recreational purposes, not because they
are actually that rural.
When it comes to six-wheel trucks, duallys, most of those are actually owned by country
people.
These are people that own farms, these are people that have giant loads they need to
haul or they're pulling something.
However, that other third is bought by people in suburbs who want to appear more rural than
they are, because in the United States, things are heavily influenced by identity, and people
like to lean into stereotypes to show who they are, rather than just living their lives.
They adopt a stereotype and tend to try to fit into it.
A lot of people do this.
So no, that's not going to change, because that's not who uses most of them.
Most people on dirt roads wait for this.
A lot of them drive sports cars, like Chargers, Challengers, Camaros, stuff like that.
I know, it doesn't make any sense.
Welcome to the US.
And then it gets better.
The idea of a dirt road Democrat itself was confusing, because it's always portrayed as
Republicans living in rural areas, because people in the country are right-wing.
That's also a little backwards and confusing.
So actual rural people, people who live way out in the country, generally lean left economically
in the United States.
And there are people in the cities right now going, what?
No really, where are you more likely to hear the phrase, I gotta drop my son off at the
union meeting and then head up to the co-op?
Yeah, that's it.
Economically, rural areas lean left, and they live their lives generally by left ideas.
Now we're talking about actual rural areas, not the suburbs who like to pretend to be
rural.
So why does that area, why do those states, tend to vote Republican?
Because the Republican Party at the federal level has made issue of school board policies
and funding of local police departments and stuff like this, and they use those ideas,
socially conservative ideas, to get rural Americans to vote against their own economic
interests.
The entertaining part about this is that in the United States, most of the stuff that
they use to motivate people to vote for them at the federal level is actually decided at
the county level.
The idea of red states and blue states is also not really that accurate.
Wyoming, which is about the reddest state there is, I want to say one out of three is
a Democrat.
They're not, they aren't monoliths, and this is especially true in the southern US where
a lot of the dirt roads are.
So what you have in the United States is people voting for politicians based on a perceived
identity that they want to cast to other people, rather than their actual beliefs.
And this happens a lot.
So to answer the questions, no, it's not really going to help the climate to pave roads in
the south, because that's not generally who is driving the large trucks and SUVs, it's
people who want to appear like they live there when they don't.
And a lot of the SUVs are actually driven by families in the US, like suburban families.
There's a joke here in the US about it being soccer moms.
I would imagine that US politics is really confusing from outside.
But yeah, if the Democratic Party ever tried and actually reached out to real rural Americans,
we're talking about farmers and people who actually live in rural areas, not those who
just live in southern cities, but live in rural areas, they could make headway.
The problem is, and this is going to be entertaining, the party that is most left-leaning economically
in the US, because they're not actually left, but the one that is most left, tends to be
elitist a little bit, and they don't reach out to rural Americans.
Which yeah, so the left party is elitist, and the right-wing party campaigns on stuff
that has absolutely nothing to do with the federal level in most cases.
The people who you would think drive the giant truck stopped.
Welcome to the United States.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}