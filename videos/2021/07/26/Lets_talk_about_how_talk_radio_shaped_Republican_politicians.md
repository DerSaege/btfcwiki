---
title: Let's talk about how talk radio shaped Republican politicians....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=go10YNi2CsI) |
| Published | 2021/07/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into conservative talk radio and its impact on his views.
- He mentions the repetitive nature of conservative talk radio shows, with hosts becoming increasingly extreme.
- The divergence within the Republican party regarding vaccines is compared to conservative talk radio sensationalism.
- Beau believes that senior Republicans' shift on vaccines is more about political tactics than genuine belief.
- He speculates that the abrupt reversal on vaccines may be influenced by statistics and polling rather than a change of heart.
- The correlation between Biden voters and vaccine rates is discussed as a potential factor in the Republican shift.
- Beau expresses skepticism about politicians truly caring for their constituents and suggests their actions are driven by political considerations.

### Quotes

- "There's nothing that will convince you you are not a conservative faster than listening to conservative talk radio."
- "It's really easy for somebody at the top like McConnell to shift from 'it's a personal choice' to 'you need to go get your vaccine.'"
- "I don't believe it's an actual change of heart. I think it has to do with statistics and polling."

### Oneliner

Beau analyzes conservative talk radio influence, Republican stance on vaccines, and political motivations behind the shift.

### Audience

Political observers, voters

### On-the-ground actions from transcript

- Pay attention to political rhetoric and analyze it critically (suggested)
- Stay informed about political strategies and motivations (implied)

### Whats missing in summary

Deeper insight into the influence of media on political ideologies and decision-making processes.

### Tags

#ConservativeTalkRadio #RepublicanParty #Vaccines #PoliticalStrategy #MediaInfluence


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about talk radio, conservative talk radio,
and whether or not Republicans are inadvertently handing Democrats an edge in the midterm elections.
I used to listen to a lot of conservative talk radio.
In fact, I'm certain they wouldn't be delighted to know this, but there are a lot of major
conservative personalities who are in no small way responsible for the way I argue stuff.
It's a lot like that joke about Catholic school.
There's nothing that will make you certain you're not Catholic faster than going to Catholic
school.
If you are a thinking person, there is nothing that will convince you you are not a conservative
faster than listening to conservative talk radio.
See because it's the same thing every day.
Used to be.
Limbaugh, you know, he would start stuff off, and it seemed like his show was show prep
for all of the other hosts.
It seems like they would cover the exact same talking points, the exact same situations
and news events.
They would just each be a little bit more extreme than the preceding one.
And it makes sense because that's the only way a newcomer is going to establish their
name.
When you have a force like Limbaugh dominant in the field, why would anybody listen to
you if you're just going to say the exact same thing he said?
Makes sense, right?
So that's why they did it.
And that sensationalism and the fear-mongering is definitely something that made me realize
this was not something that I wanted to be associated with.
So the question that came in was whether or not the divergence that is developing between
senior Republicans when it comes to their reversal on vaccines, the divergence between
them and the younger crop, whether or not that's something that Democrats can exploit
in the midterms.
I don't think it is, but I like the way you're thinking.
And then the second part to the question was whether or not I knew why there was a reversal.
Okay, so as far as the divergence, it is exactly like conservative talk radio because many
politicians today, they use the same tactic.
They become more sensationalized in hopes of grabbing sound bites and getting on the
news and then therefore they can advance themselves.
They don't actually believe the stuff they're saying, most of them.
So it starts with the people who actually control the party, somebody like McConnell.
Now he comes out and he says vaccines are a personal choice.
I don't think this is government's business.
The copy of him, well he says, you know, it's the Democratic agenda for you to do this.
You don't want them to control you, so do the exact opposite.
That way they won't control you if you just always do the opposite of what they say.
They won't have any influence over what you do.
That's a way to not give them the power, guys.
And then the copy of the copy, well they're going to say something like, you know, how
do we even know it works?
They're experimenting.
And you get far enough down the line and you find people saying they're going to take your
guns and bibles.
No joke, that actually happened.
So it's really easy for somebody at the top like McConnell to shift from it's a personal
choice, the government shouldn't be involved in this, to the government shouldn't be involved
in this, you need to go get your vaccine.
That's an easy, easy, easy transition.
Doesn't require a lot.
However the further down you go, the copy of the copy of the copy, the harder it is
to walk it back.
It takes more time.
I would imagine that over time we're going to see those people who are rational slowly
move their positions to align with Republican leadership.
And I would imagine that would happen before the midterms.
So I don't think it's going to be exploitable.
Now do I know why there was such an abrupt reversal?
I have a theory.
It's not a theory.
I don't have enough evidence to call this a theory.
I have a guess.
How about that?
I have a guess.
My guess is that they understood what was outlined in the video I did to Jim.
The video I made for Jim, I pointed out the correlation between those who voted for Biden
and the vaccine rates.
The percentages are pretty close to each other.
That's correlation, it's not causation.
But if you're looking at polls from a political standpoint, you're going to notice that.
My guess is they understand that many tight races are decided by a pretty limited number
of votes.
And if it is in fact a pandemic of the unvaccinated, and it is their constituents who are
unvaccinated, it may swing races.
Not just due to the votes lost due to negative outcomes from the public health thing, but
also their families may not be real happy about that.
I don't believe it's an actual change of heart.
I think it has to do with statistics and polling.
I think that's the primary motivation.
I don't have any evidence for that.
Not really.
Other than the timing of the events and when a lot of this information came out, I don't
have correlation.
I don't have causation.
So do I know why that reversal happened?
No.
But that's my guess.
So when it comes to the gap between senior leadership and those on the bottom, these
sensationalized people will probably eventually walk back their comments.
And yeah, I think it's all political.
I still don't think they actually care about their constituents.
I think if they believed it would hurt Biden more than it would hurt them, they would still
be telling people that vaccines are going to lead to their Bibles and guns being taken.
So anyway, it's just a thought. So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}