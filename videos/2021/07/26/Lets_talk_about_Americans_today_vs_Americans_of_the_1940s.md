---
title: Let's talk about Americans today vs  Americans of the 1940s....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6HQSMpS8KaU) |
| Published | 2021/07/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the concept of the "greatest generation" from the 1940s in American society.
- Addressing memes that suggest today's Americans are not up to the same task as the Americans from the 1940s.
- Disputing the idea that the 1940s generation was uniquely tough and downplaying the emotional and psychological impacts of war.
- Pointing out the historical inaccuracy in suggesting that liberals couldn't have participated in the same actions as the generation from the 1940s.
- Emphasizing that the greatness of that generation was not due to their going to war, but their unity and willingness to sacrifice for a common goal.
- Drawing parallels between the inconveniences that generation endured during wartime and the minor inconveniences asked of Americans today, like wearing masks.
- Stating that true patriotism is about putting the well-being of others above oneself and making sacrifices for the greater good.
- Encouraging people to wear masks, get vaccinated, and do their part in stopping the current crisis to truly honor the spirit of the "greatest generation."

### Quotes

- "Patriotism is having a loyalty to a geographic area and having a desire to protect those within it, putting that group of people above yourself."
- "If you want to claim to be the heir of those Americans, you need to do your part now."
- "The greatest generation was the greatest, not because it fought World War II, but because the entire country came together to stop something that needed to be stopped."

### Oneliner

Exploring the concept of the "greatest generation" from the 1940s and urging people to emulate their unity and sacrifices by wearing masks and getting vaccinated.

### Audience

Americans

### On-the-ground actions from transcript

- Wear a mask, get your shots, and go get your vaccine (exemplified)
- Put the well-being of others above yourself by following public health guidelines (exemplified)

### Whats missing in summary

The full transcript delves deeper into the comparison between past sacrifices and present challenges, urging individuals to embody the spirit of unity and sacrifice seen in the "greatest generation" by taking necessary public health measures.

### Tags

#GreatestGeneration #Unity #Sacrifice #Patriotism #PublicHealth #COVID19 #Vaccination #AmericanSociety


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk
about the greatest generation. There are memes that suggest that Americans of the
1940s, they were the pinnacle, they were the peak of American society and that
today's Americans, well they're just not up to the task. The thing is there's a
lot of truth to that meme, to that idea. There's a lot of truth to it. Of course
it's not in the way that it often gets portrayed if you want to be historically
accurate, but we're going to talk about that concept today. Those great patriots
of the 1940s. We're going to talk about the memes that are often shared and then
we're going to talk about the reality and where the truth lies. So what are the
memes? There's two versions. The first is something like, today's generation could
never handle storming the beaches of Normandy or fighting at Iwo Jima. This
one is just ridiculous on its face. Yeah, the generation that was at war for 20
years, they couldn't handle that. That doesn't make any sense. Aside from
that, saying it like that, well they handled it. Like they were just so tough
it didn't cause a severe emotional and psychological impact on them. I would
suggest that's downplaying it and I would suggest that it's historically
inaccurate because it is. They didn't handle it because they were so tough.
Some of them survived. Same would happen today. The other version of it is, can you
imagine liberals storming the beaches in Normandy? Again, this one's a little weird
to me. I mean, could you imagine the people who elected FDR four times? A guy
who had a vice president that was pretty much a socialist, like a real one, not the
way people use the term today. Could you imagine them doing exactly what they did?
I mean, that doesn't even make sense. That doesn't make sense. That generation, that
group of people, they elected FDR four times. They were pretty liberal. So
where's the truth lie? The truth lies in the name itself. Why is it the greatest
generation? Is it because they went to war? No, we're the United States. Every
generation goes to war. Yeah, think about that for a while. It's because the entire
country saw something sweeping the globe, they wanted to stop it, and they pulled
together to do so. They put up with a lot of inconveniences to accomplish this. Can
you imagine Americans today being told that they were gonna ration gas, coffee,
meat, dairy, firewood, nylon? There'd probably be protests out in the street.
There'd probably be protests out in the street. Can you imagine them being told that they
needed to be ready to turn off their lights at night to protect from air
raids? You would probably have protests that were anti-blackout protests, because
they're not up to it. They aren't up to it. This is a generation that had posters
up telling them not to jackrabbit start, which is, you're at a red light, it turns
green, you know how you get up to the speed limit as quickly as possible? Well,
you're not supposed to do that, because if you do that, well, it burns more fuel, and
then inadvertently that helps the opposition. They were willing to put up
with that minor inconvenience, so that bad thing didn't spread. I can't think of
a more fitting analogy to wearing a mask. A lot of Americans aren't up to it. It's
that simple. They pulled together. They saw something that needed to be
stopped, and everybody put up with the minor inconveniences, pitched in, and did
it. Yeah, there are a lot of Americans today who are not up to the task, and as
is typically the case, it is those people who want to latch on to that imagery the
most. Those people who want to claim to be the philosophical and spiritual heirs
of the greatest generation. Those are the ones that wouldn't put up with the minor
inconveniences. Those are the ones that wouldn't pitch in and do their part.
Patriotism is not waving a flag, singing a song, or standing up and put your hand
over your heart. Patriotism is having a loyalty to a geographic area and having
a desire to protect those within it, putting that group of people above
yourself. That's patriotism. You want to be a patriot? You want to emulate the
greatest generation? Wear a mask. Get your shots. Go get your vaccine. That's how you
can be like them. I'd point out that there was a race to get vaccines for
troops, and they were ready to go into arms in 1944. The greatest generation was
the greatest, not because it fought World War II, but because the entire country
came together to stop something that needed to be stopped. Everybody pitched
in. Everybody did their part. If you want to claim to be the heir of those
Americans, you need to do your part now. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}