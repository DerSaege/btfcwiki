---
title: Let's talk about what makes a good conservative....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Br3WxNbaU7g) |
| Published | 2021/07/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responding to a comment questioning his anti-conservative bias, Beau dives into what he believes makes a good conservative.
- He differentiates between his objective coverage of events and his personal philosophy, which leans towards a society with fairness, freedom, and cooperation.
- Beau believes his viewers, who are primarily progressive and anti-authoritarian, want a world where everyone can thrive without hierarchy.
- Exploring the idea of a good conservative, Beau sees them as cautious individuals who still push for progress and increased freedom but at a slower pace.
- He criticizes conservatives motivated by fear, who want to halt progress instead of moving through challenges with courage.
- Beau defines a good conservative as someone who ensures progress continues without crashing into unprepared situations, acknowledging that such individuals are becoming rarer, especially in political office.

### Quotes

- "Courage isn't the absence of fear. Courage is moving through the fear."
- "A good conservative is somebody who is cautious, but still moving forward with the rest of humanity."
- "Humanity is very biased towards liberals. It becomes more liberal as time progresses."
- "The problem is when they stop. Just don't stop, because humanity's not going to."
- "So it's not somebody who stops. It's certainly not somebody who wants to go backwards and try stuff again."

### Oneliner

Addressing anti-conservative bias accusations, Beau defines a good conservative as cautiously progressive, ensuring humanity's continual forward movement.

### Audience

Progressive viewers

### On-the-ground actions from transcript

- Engage in cautious but progressive actions in your community (implied)

### Whats missing in summary

Beau's detailed analysis and perspective on the characteristics of a good conservative

### Tags

#Conservatism #Progressive #Anti-authoritarian #Cautious #Courage


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about good conservatives and what I think they are.
Because a comment showed up on a video.
It says, it occurs to me how much anti-conservative bias you're letting show.
I'm not surprised at all that you have it, but a bit surprised you show it so clearly.
Anyway, I'm going to assume there's also nuance here.
So is there in your mind a good side to conservatives or conservatism?
And perhaps subdivide it into say Republican politicians and conservative non-politicians
or further subdivide as you see fit.
Would love to see a video on this.
I don't know how it's surprising, to be honest.
I mean I do, because if you mainly watch videos where I'm just doing coverage of events, I
try to be pretty objective in my coverage of stuff.
So I don't let my personal philosophy interject into that too much.
I don't try to frame reality around the way I believe things should work.
However, when it comes to my personal philosophy, I'm pretty open about it.
I want a society where everybody gets a fair shake.
I want a society where there's the maximum amount of freedom for the maximum amount of
people with the maximum amount of cooperation between them because the personal profit motivator
is gone.
I want a society that doesn't have upper and lower class.
There's no hierarchy keeping people down.
That's what I want.
And I'm really open about that.
I think most of the people watching this channel are capable of imagining all the people living
together.
And they're okay with it.
They're comfortable with it.
And that's the society they want as well.
And I have some evidence to back this up.
The day I talked about the Purge movie, I tweeted something out asking, what would you
do if everything was legal for 24 hours?
Hundreds of responses.
I read them all.
Less than 1% had a victim of some sort other than like a multi-billion dollar corporation.
Most people watching this channel, that's the world they want.
They are really progressive.
They're very anti-authoritarian.
They're not conservative.
So to us, if somebody like me was able to snap their fingers and tomorrow we live in
that world, we'd be ready for it.
We'd be able to prosper, do well.
Even those who say, well I don't know if I want it to that extreme, they'd do fine.
However, what if that question was asked by Ted Cruz on Twitter?
I'm willing to bet the responses would be pretty different.
I don't think people would say that they would remove anti-homeless architecture, plant community
gardens, engage in stuff that is trying to level the playing field, trying to move humanity
forward.
There'd probably be victims.
So what is a good conservative?
That's the question.
This all boils down to what I think a good conservative is.
Somebody who slows people like me down.
Because when I think about my personal philosophy, I'm thinking about humanity as a whole.
And the trend of humanity is to get more freedom for more people over time.
That's the trend.
Humanity is very biased towards liberals.
It becomes more liberal as time progresses.
That's the nature of humanity.
Since we came out of the caves, we overcame fear.
So what's a good conservative?
Especially given the fact this video was, or this comment was left on the video when
I was talking about fear mongering.
Courage isn't the absence of fear.
Courage is moving through the fear.
A good conservative is somebody who is cautious, but still moving forward with the rest of
humanity.
Still trying to achieve more freedom for more people to level the playing field.
They're just doing it slowly.
That's a good conservative to me.
Not somebody who wants to stop.
The world doesn't stop.
Humanity doesn't stop.
We're going to push forward.
So it's not somebody who stops.
It's certainly not somebody who wants to go backwards and try stuff again.
Try a bunch of policies that were attempted in the 30s and 40s and failed miserably.
A lot of the conservative movement today is motivated by fear.
Rather than trying to push through the fear, they just want to stop.
They want the world to freeze because it's scary, because things are new.
I don't think that's conservative.
I think that's freezing.
I think that's freezing.
I think that's wishing that things would stop.
They're not going to.
Humanity has proven that time and time again.
Yeah, there are those people, those historical figures who drug us back, but they don't last
long.
The long-term trend is more freedom for more people.
That's how humanity progresses.
A good conservative is somebody who doesn't try to stop that, but just tries to make sure
people like me don't crash headlong into something that humanity's not ready for yet.
That's a good conservative.
Those people are becoming fewer and further between, especially in political office.
You want to talk about Republican politicians, to me that doesn't really have much to do
with conservatism anymore.
I'm pretty open about the fact that I think most Democrats are conservative, because I'm
on the other end of the spectrum.
The only reason it's surprising is because you may not watch the philosophical videos,
those that address those topics.
In a way, this is a compliment to me, because I do try to not allow my personal philosophy
to impact my coverage of actual events.
I think that may also play into a lot of issues with the conservative movement today.
They want to frame reality around the way they think it should be, rather than dealing
with the objective facts as they are.
So it's not to say, and I've said it before, I don't believe that conservatives are evil.
They're just more motivated to be cautious for whatever reason.
And as long as they're cautious and still moving forward, there's nothing inherently
wrong with that.
The problem is when they stop.
Just don't stop, because humanity's not going to.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}