---
title: Let's talk about politicians writing unconstitutional laws....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MdCrEy95wxs) |
| Published | 2021/07/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a law in Tennessee requiring businesses to post signs allowing individuals to use the bathroom corresponding to their identity.
- Skips moral arguments and focuses on economic impact and constitutional issues.
- ACLU challenges the law in court, and a federal judge issues an injunction deeming it likely unconstitutional.
- Questions why groups using the Constitution as a backdrop keep writing unconstitutional legislation.
- Argues that voters should have access to information about lawmakers who support unconstitutional laws.
- Advocates for consequences for lawmakers writing bills found to be unconstitutional, such as being barred from public office.
- Suggests that legislation constantly challenging constitutional limits should be a warning sign.
- Emphasizes the importance of upholding the basic principles of the Constitution.

### Quotes

- "Why is it that a group of people who often use the Constitution as their backdrop keep writing legislation that is unconstitutional?"
- "You can't pretend to be a patriot and uphold the basic principles of this country if you are actively trying to undermine them."
- "If you write a bill that is found to be unconstitutional, you're barred from public office."
- "The fact that we have so much legislation headed to the Supreme Court with the express purpose of testing the limits of those hard limits, trying to push them as far as they can go, that should be a warning sign."
- "Maybe we should know the legislators who are out there who don't actually support the Constitution."

### Oneliner

Beau questions the logic behind lawmakers writing unconstitutional legislation and advocates for consequences for those who do, urging voters to have access to information about such actions.

### Audience

Voters, Constitution supporters

### On-the-ground actions from transcript

- Advocate for transparency in lawmakers' actions by supporting initiatives that track and make public information about unconstitutional legislation (suggested).
- Stay informed about laws and bills being introduced in your area and hold lawmakers accountable for upholding constitutional principles (implied).

### Whats missing in summary

The full transcript provides a detailed exploration of the impact of unconstitutional legislation on the Constitution's integrity and suggests accountability measures for lawmakers who introduce such bills.

### Tags

#Constitution #UnconstitutionalLegislation #Accountability #Transparency #Lawmakers


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a
law in Tennessee
that has been halted.
We're going to talk about some information that I think voters should have better access to.
And we're going to talk about a law I think should exist.
So, about a month ago I did a video
about a new law in Tennessee that required signage
related to bathrooms.
Basically,
the state government there decided to require
businesses
to post up signs
saying in essence
we allow people to use the bathroom that corresponds to their identity.
Different phrasing on the sign, obviously.
I decided to forego
talking about the moral arguments there because generally speaking moral
arguments do not work on bigots.
I decided to talk about the economic issues
and how it would impact areas surrounding military installations.
The ACLU decided to talk about the constitutional issues
and they took it to court.
And rightfully so,
a federal judge issued an injunction because they found it to be likely
unconstitutional.
It is, it's plainly unconstitutional.
It's the government forcing speech onto private entities.
So it's the right move by the judge,
but it got me thinking.
It got me thinking.
Why is it that
a group of people who often use the Constitution as their backdrop
or in their marketing,
why do they keep writing legislation that is unconstitutional?
That seems odd to me.
It also seems like
it's information
that maybe the public should have access to.
I think records should be kept.
If you write a law
or vote in favor of a law
that is found to be unconstitutional,
I think that's information that voters should have. I think people should know
that
because there's absolutely no way you're upholding your oath.
You can't support and defend the Constitution
if you are writing laws to undermine it.
If you are supporting legislation
that undermines it.
The Constitution is a pretty simple document.
The Bill of Rights could be summed up with
it's your right to be left alone.
And yet,
constantly
there's legislation introduced
that attempts to undermine it,
undercut these rights.
Your inalienable rights.
Seems like something that
voters should know about.
You know, the Bill of Rights, these were hard limits.
You know,
this was,
the government shall not do this.
And yet,
there's constant
introduction of legislation
that is unconstitutional.
You can't pretend to be a patriot
and uphold the basic principles of this country
if you are actively trying to undermine them.
So I think this information should be out there.
Further,
I think
there should be a law
that if you write a bill
that is found to be unconstitutional,
you're barred from public office.
Because you obviously don't take your job seriously.
You obviously don't take your oath seriously.
So maybe that should happen.
I would imagine
that
a lot of these bills
that are being introduced
at various levels,
that are really just designed to create headlines
and feed into the culture war,
I imagine they'd stop real quick.
Because they're pretty much all unconstitutional.
So the politician can't say,
oh well we tried to do that thing
that's blatantly unconstitutional
because we love the Constitution.
They can't sell that anymore.
Because if they do it,
well they can't run for office again.
I think that we would suddenly find
people brushing up
on the Constitution
and changing their rhetoric really quickly.
At the end of the day,
the Constitution isn't complicated.
And the fact that we have so much legislation
headed to the Supreme Court
with the express purpose
of testing the limits
of those hard limits,
trying to push them as far as they can go,
that should be a warning sign.
Realistically, if you write legislation
and it makes it to the Supreme Court,
that should be embarrassing.
Even if it's found to be constitutional,
it should be embarrassing
because it was probably written in a way
that allowed for the challenge.
So maybe we should keep better records of this.
Maybe we should know
the legislators who are out there
who don't actually support the Constitution,
who want to infringe on your rights,
and who introduce legislation to do that.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}