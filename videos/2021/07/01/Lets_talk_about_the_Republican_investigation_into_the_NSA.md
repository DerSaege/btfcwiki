---
title: Let's talk about the Republican investigation into the NSA....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NX6S8aLWHGI) |
| Published | 2021/07/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans refused to conduct an investigation into the events of January 6th on Capitol Hill, despite video evidence.
- Instead, they have chosen to launch an investigation into the NSA, citing Tucker Carlson's unbacked claims.
- Devin Nunes, known for his Twitter cow feud, will lead this investigation into the NSA.
- Beau questions Nunes' credibility as a defender of the NSA leading this investigation.
- Beau suggests that if the Republican Party genuinely cares about civil liberties, legislation is needed rather than just an investigation.
- He advocates for strong safeguards to protect Americans' rights and potentially rolling back intrusive NSA programs.
- Beau sees this investigation as a distraction and misinformation campaign aimed at the Republican base.
- He believes it's an attempt to shift focus from the events of January 6th.
- Beau suspects the timing of this investigation announcement isn't coincidental and vows to follow its progress.

### Quotes

- "Contrary to popular perception, the NSA is not and never was recording or listening to millions of Americans' phone calls." - Kevin Nunes
- "If you really believe that this occurred, we don't need an investigation. We need legislation." - Beau

### Oneliner

Republicans deflect from January 6th with an NSA investigation based on Tucker Carlson's claims, prompting Beau to call for legislation over investigations.

### Audience

Activists, Civil Liberties Defenders

### On-the-ground actions from transcript

- Contact local representatives to advocate for legislation protecting civil liberties (implied)
- Join civil rights organizations pushing for stronger safeguards against government intrusion (implied)

### Whats missing in summary

Full context and depth of Beau's analysis and skepticism towards the Republican-led investigation into the NSA.

### Tags

#CapitolHill #NSA #Investigation #CivilLiberties #RepublicanParty


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the other investigation
that is going to take place up on Capitol Hill,
if you don't know.
After Republicans voted against having an investigation
and hearings and everything into the events of the 6th,
something that we all know actually happened,
something that occurred, something that's like on video,
after they said that that wasn't important,
we don't need to look into that,
they have decided that they will be launching an investigation
into the NSA.
Part of the reasoning is Tucker Carlson's claims,
which as of time of filming, we have still not seen
any evidence to back up.
To make it more interesting,
they have chosen Devin Nunes to lead up this investigation.
And I mean I guess that makes sense, he did in fact
show that he knows a whole lot about the internet
when he decided to go to war with a Twitter cow,
and if you do not get that reference,
just be glad you have a life outside the internet.
So he will be looking into the NSA.
I have some things to say about that.
Quotes,
it's more important than ever that the US maintain
strong intelligence capabilities throughout the world,
but for the last year, various groups have sought
to curtail our intelligence activities
based on selectively presented, maliciously leaked documents.
We cannot allow erroneous reports
about our intelligence programs to force us back
to a pre-911 posture.
Contrary to popular perception,
the NSA is not and never was recording
or listening to millions of Americans' phone calls.
In short, unless you're talking to a foreign-based terrorist,
the NSA is not monitoring you.
Kevin Nunes.
Somebody that is that much of a defender of the NSA
participating in this leads me to believe
that they know that this is a farce,
that it's a joke, it's another misdirection.
Now maybe I'm wrong.
Maybe the Republican Party has suddenly decided
to care about civil liberties
now that it's their buddies getting pinged in theory
without evidence.
Maybe that's happening.
But you know what?
If that's the case,
if you really believe that this occurred,
we don't need an investigation.
We need legislation.
Because an investigation just leads people like me
to believe that you'd be okay
with it being done to us common folk,
that the real trouble is that it was done
to Tucker Carlson, a political ally.
And therefore, you just want to turn the heat up on the NSA.
That's what it seems like.
Now if the Republican Party wants to, I don't know,
install some real hard safeguards
to make sure the rights of Americans are protected,
I'm all for it.
They want to roll back some of these programs,
I am all for it.
They want to roll back a lot of these programs,
I am all for it.
What I'm not for is another distraction,
another campaign of misinformation,
another deception aimed at their base.
And that's what this appears to be shaping up to be.
Seems like another way to attempt to deflect attention
away from the events of the 6th.
That's what it looks like.
I don't think that the timing of this announcement
is a coincidence.
I will definitely be keeping up with this investigation.
And we'll see what is discovered about Tucker Carlson's claims.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}