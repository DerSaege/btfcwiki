---
title: Let's talk about why Republicans voted against the committee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nXMJ_E7DgBo) |
| Published | 2021/07/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US House of Representatives voted on forming a committee to look into the events of January 6th at the Capitol.
- Only two Republicans voted in favor of investigating the Capitol incident, raising eyebrows.
- Some Republicans claimed it was the work of left-wing agitators, BLM, or the FBI to deflect blame.
- Beau questions why Republicans who made these claims wouldn't want an investigation to reveal the truth.
- Republicans voting against the investigation leads to only two conclusions: they believe the claims or they were lying.
- Beau suggests that Republicans on the committee may work to undermine it and turn it into a circus.
- Republican leaders aim to keep their base in the dark about what truly happened on January 6th.
- Enabling Trump and failing to serve as a checks and balance against executive power started the chain of events leading to January 6th.
- Republicans want to keep their constituents confused to manipulate them easily.
- Beau concludes by raising awareness of the Republican Party's actions regarding the Capitol incident.

### Quotes

- "Either they believe that these things happened and they're okay with it, or they know they were lying."
- "Because if they accept reality, well, they might be pretty mad with the Republican Party."
- "They want to keep their constituents in an echo chamber of confusion."

### Oneliner

The US House vote on investigating January 6th reveals Republican reluctance to uncover the truth, leading to manipulation of their base by keeping them in the dark.

### Audience

American voters

### On-the-ground actions from transcript

- Reach out to your representatives and demand transparency and accountability in investigating the events of January 6th (suggested).
- Stay informed about political events and hold elected officials accountable for their actions (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the Republican Party's stance on investigating the events of January 6th and their potential motives for avoiding transparency.

### Tags

#USPolitics #January6th #RepublicanParty #Accountability #Transparency


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the vote.
The vote took place in the US House of Representatives over forming a committee to look into what
happened on the 6th to find out what happened at the Capitol.
Now if you watch this channel with any regularity, that vote probably went exactly as you would
have predicted it.
But if you're part of that Republican base, you're part of that group of people that is
heavily swayed by soundbites, I bet you have some serious questions.
And if you don't, you should.
Because only two Republicans voted in favor of investigating what happened that day.
That seems odd.
That should raise your eyebrows a little bit.
Because there are Republicans in the House who floated some pretty wild claims.
Who said it was the work of left-wing agitators.
It was BLM.
It was the FBI.
Now if you actually believe that these things are possibilities, wouldn't you want an investigation?
Wouldn't you want to look into this to kind of undermine that narrative that's out there?
Because if you truly believe this is what happened, you would want people to know it.
You would want people to say, hey, you know, all that stuff that we heard, that's not true.
It wasn't these people riled up by Trump.
It was this other thing.
This is what caused it.
And they've made these claims in an attempt to deflect blame repeatedly.
But when it comes time to actually vote to investigate it, they vote no?
Man.
That really leads you to only one of two conclusions, really.
Either they believe that these things happened and they're okay with it, or they know they
were lying.
They know that they were just trying to con you.
So they don't want an investigation because they don't want to get busted.
They don't want to be shown to be wrong.
Those are really the only two options.
I'm open to hearing a third, but I can't think of one because the way the committee is made
up, it's going to have Republicans on it who are going to be able to ask questions.
And that brings us to something else.
Because so few Republicans are concerned about what happened and want the American people
to know what happened, there will be somebody on this committee who is opposed to it being
formed, who voted against it, and they'll be on it.
I'm willing to bet that person will work to undermine it, do everything they can to turn
it into a circus, a show, to deflect blame, to continue to attempt to con their base.
Because the one thing that can't happen for the Republican Party is for the truth to come
out about the sixth.
Their base can't know what occurred.
At the very least, Republican leaders, they have to give their base talking points, plausible
deniability, something to blame it on.
Because if they accept reality, well, they might be pretty mad with the Republican Party.
Because this chain of events, it starts with those in the House and the Senate enabling
Trump.
That's where it begins.
Now there's no way that way back then they could have known what was going to happen
on the sixth.
But that's the chain of events.
That's where it really started.
When those in Congress failed to serve as a check against executive power, when those
in Congress failed to correct the former president when he said things that were against the
core principles of this country, that's where it started.
And that's why they don't want their base to know.
All but two Republicans voted against finding out what happened on the sixth, even though
it's Republicans who are claiming we don't know what really happened.
Now we know.
They just don't want their constituents to know.
They want to keep their constituents in an echo chamber of confusion.
Because if they're confused, well, they're easier to manipulate.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}