# All videos from July, 2021
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2021-07-31: Let's talk about criticism of the Capitol cops.... (<a href="https://youtube.com/watch?v=rB8NYIgDC3A">watch</a> || <a href="/videos/2021/07/31/Lets_talk_about_criticism_of_the_Capitol_cops">transcript &amp; editable summary</a>)

Beau examines the criticism faced by officers, challenges misconceptions about PTSD, and calls out reckless messaging undermining mental health support.

</summary>

"Real warriors want real opposition."
"The toughest people I know all have PTSD."
"Needing help does not make you weak."
"They reinforce the idea that needing help makes you weak."
"This is a very reckless set of messaging."

### AI summary (High error rate! Edit errors on video page)

Exploring the criticism of officers' testimony regarding the events of January 6th and the implications of this criticism on their emotional state and future outcomes.
Initially found it amusing when critics like Tucker Carlson questioned the officers' courage, but shifted perspective after understanding the implications.
Observing how individuals, even those traditionally anti-police, defended the officers against accusations of weakness.
Acknowledging the importance of facing worthy opponents in warrior culture, not weak ones.
Noting the impact of criticism on officers' emotional and psychological well-being, especially when coming from influential platforms like Fox News.
Sharing stories of military veterans who developed PTSD from specific moments, illustrating the complex nature of psychological trauma.
Criticizing pundits like Tucker Carlson for downplaying the severity of potential PTSD triggers and emotional distress in combat situations.
Challenging the misconception that toughness and courage prevent individuals from experiencing PTSD.
Emphasizing the significance of seeking help for PTSD and the bravery it takes to confront mental health challenges.
Critiquing the reckless messaging of pundits who prioritize talking points over the well-being of those affected by trauma.

Actions:

for advocates for mental health,
Support organizations aiding individuals with PTSD (implied)
Advocate for mental health awareness in communities (implied)
Challenge stigmas around seeking help for PTSD (implied)
</details>
<details>
<summary>
2021-07-30: Let's talk about the DOJ's notes of the Trump conversation.... (<a href="https://youtube.com/watch?v=a8WXGkpTNP4">watch</a> || <a href="/videos/2021/07/30/Lets_talk_about_the_DOJ_s_notes_of_the_Trump_conversation">transcript &amp; editable summary</a>)

Former President Trump manipulated his base by pushing baseless election fraud claims, knowing they wouldn't demand evidence.

</summary>

"If you believed this stuff, you were tricked."
"Those people that went up there on the 6th, they were tripped, they got duped."
"He just needed somebody to say it and then he and his willing accomplices in Congress, well they would handle the rest."

### AI summary (High error rate! Edit errors on video page)

Handwritten notes released to an oversight committee detail a conversation between former President Donald Trump and top officials in the Department of Justice.
Trump suggests that the election was corrupt and asks officials to leave the rest to him and a congressman.
The implication is a coordinated effort to overturn the election without evidence between the White House and Republicans in Congress before January 6.
Trump understands his base doesn't require evidence; he just needs the claim out there to repeat it and have his followers believe it.
Supporters of Trump are reminded that there has been no evidence to support claims of election fraud for months.
People who believed in these claims were tricked into risking themselves for Trump's ego and desire to stay in power.
Beau suggests that those who tricked people into believing election fraud might mislead them on other issues, getting them to act against their own interests.
Trump's request for officials to "just say" the election was corrupt is seen as an attempt to manipulate them into believing it without proof.
Trump's base is willing to follow along without demanding evidence, allowing him and his allies in Congress to handle the rest.
Supporters of Trump should weigh heavily the manipulation and lack of evidence behind his claims.

Actions:

for supporters of trump,
Revisit beliefs on election fraud claims and seek evidence to inform future decisions (implied).
</details>
<details>
<summary>
2021-07-30: Let's talk about masks being back in style.... (<a href="https://youtube.com/watch?v=AMApHMPCmTg">watch</a> || <a href="/videos/2021/07/30/Lets_talk_about_masks_being_back_in_style">transcript &amp; editable summary</a>)

Beau reminds fully vaccinated individuals to continue following COVID-19 precautions not just for themselves but for those who can't, stressing collective responsibility and protection.

</summary>

"Your actions are there to protect the people who refuse to protect themselves."
"Stay on your guard to protect those around you."

### AI summary (High error rate! Edit errors on video page)

Masks and mandates are making a comeback in several places, with more to come.
The messaging around these precautions might be off, so Beau offers his perspective.
Fully vaccinated individuals have a low risk of negative outcomes due to the effectiveness of vaccines.
However, some vaccinated people may become lax with precautions.
Beau acknowledges that some individuals may think those facing consequences chose not to get vaccinated or wear masks.
Precautions taken by vaccinated individuals mainly protect those who can't get vaccinated.
Despite being protected, vaccinated individuals could still transmit the virus.
Beau encourages people to continue following guidelines to protect those who can't get vaccinated due to medical conditions.
He stresses the importance of considering those who are at risk through no fault of their own.
Historical trends show peaks and valleys in response to events like the current situation.
Beau advocates for basic protective measures like handwashing, avoiding face-touching, and wearing masks.
While social distancing isn't emphasized in current guidance, Beau suggests it's still a good idea based on data.
Even though it may be frustrating to protect those who refuse precautions, Beau urges doing it for those who can't protect themselves.
He concludes by reminding viewers to stay vigilant and protect those around them.

Actions:

for fully vaccinated individuals,
Continue wearing masks and following COVID-19 guidelines to protect those who can't get vaccinated (suggested).
Practice social distancing and other basic protective measures even if not explicitly advised (suggested).
</details>
<details>
<summary>
2021-07-29: Let's talk about the marketplace of ideas.... (<a href="https://youtube.com/watch?v=KDXPfUKTwQg">watch</a> || <a href="/videos/2021/07/29/Lets_talk_about_the_marketplace_of_ideas">transcript &amp; editable summary</a>)

Beau breaks down the concept of free speech, the marketplace of ideas, and the tolerance paradox, addressing cries of First Amendment violations and perceived hypocrisy in public discourse.

</summary>

"What you witnessed was free speech in action. That's the marketplace of ideas."
"The whole idea behind the tolerant left has to do with this premise that you can't allow free speech to become an incitement to violence."
"In order for this hypocrisy to exist, there has to be an acknowledgement that free speech, the First Amendment, most of the principles in the Bill of Rights are left."

### AI summary (High error rate! Edit errors on video page)

GOP members held a press conference outside the DOJ, but it was disrupted by protesters with signs and whistles.
The GOP members left without the press conference happening, leading to cries of First Amendment rights violation.
Beau clarifies that the First Amendment protects people from government censorship, not from being challenged in public.
Free speech goes beyond the First Amendment, allowing for open debate and discourse in the public square.
The incident at the DOJ was an example of the marketplace of ideas in action, where dissenting voices countered the GOP speakers.
Beau addresses the notion of the "intolerant left" and the tolerance paradox, stating that free speech shouldn't incite violence.
He points out the perceived hypocrisy of labeling the left as intolerant for countering harmful speech.
Acknowledging conservatives for raising the hypocrisy suggests an understanding that principles like free speech are not tied to a specific political ideology.
Beau leaves with a reflective thought on the dynamics of free speech and tolerance in public discourse.

Actions:

for online activists,
Counter harmful speech with more speech by engaging in constructive debate and discourse (implied).
Advocate for inclusive platforms for open dialogues to foster a diverse range of voices (implied).
</details>
<details>
<summary>
2021-07-29: Let's talk about Pelosi, college debt, and do some supposing.... (<a href="https://youtube.com/watch?v=eENhNkIfPSA">watch</a> || <a href="/videos/2021/07/29/Lets_talk_about_Pelosi_college_debt_and_do_some_supposing">transcript &amp; editable summary</a>)

Beau critiqued Speaker Pelosi's stance on forgiving college debt, advocating for tax money in the public interest and universal higher education.

</summary>

"Universal access to higher education benefits all of society."
"Higher education is good for the country."
"Universal education, universal higher education, is something this country needs."

### AI summary (High error rate! Edit errors on video page)

Critiques Speaker Pelosi's stance on forgiving college debt and tax money allocation.
Raises concerns about paying for corporate bailouts, mass incarceration, surveillance state, and military weapons for law enforcement.
Points out issues with large corporations using SNAP as a subsidy to pay poverty wages, perpetuating poverty and class division.
Emphasizes that universal access to education is opposed by those in power to maintain the class divide.
Shares a personal story of a disadvantaged high school student with Ivy League potential but lacking financial means.
Questions the unfair advantage given to wealthy Ivy League students in the job market, perpetuating the cycle of wealth.
Advocates for tax money being used in the public interest to benefit society as a whole.
Stresses the importance of universal higher education, regardless of degree type, for the country's advancement and societal benefit.

Actions:

for taxpayers, education advocates,
Advocate for tax allocation towards public interest needs (implied)
Support initiatives for universal higher education (implied)
</details>
<details>
<summary>
2021-07-28: Let's talk about executive privilege and Trump.... (<a href="https://youtube.com/watch?v=RbAiVv6wtoU">watch</a> || <a href="/videos/2021/07/28/Lets_talk_about_executive_privilege_and_Trump">transcript &amp; editable summary</a>)

Executive privilege explained, potential accountability ahead, but real consequences uncertain.

</summary>

"The first rule of working in the White House is to not talk about working in the White House."
"Real accountability may not be coming."
"Them allowing the testimony allows for a ray of hope that real accountability may occur."
"This is paving the way for testimony from high-level people who had conversations with Trump."
"I doubt that suit [by Trump] would be successful."

### AI summary (High error rate! Edit errors on video page)

Executive privilege allows the executive branch to keep information confidential, including the deliberative process and presidential communication.
The Department of Justice and the Office of Legal Counsel have decided not to protect testimony about Trump's attempts to overturn the election under executive privilege.
People cleared to testify are related to investigating Trump's alleged use of DOJ to overturn the election, not directly tied to the committee investigating the events of January 6th.
Allowing testimony from high-level individuals may pave the way for accountability, although real accountability remains uncertain due to the strong drive to protect the institution of the presidency in DC.
The decision to allow testimony provides a glimmer of hope for potential accountability, although it remains to be seen how it will unfold.
Trump may try to stop the testimony through a lawsuit, but it is unlikely to succeed given the circumstances deemed to be in the public interest by DOJ and the Office of Legal Counsel.

Actions:

for politically engaged citizens,
Contact your representatives to express the importance of accountability in government actions. (exemplified)
Stay informed about the developments in the investigations and hold officials accountable for transparency. (exemplified)
</details>
<details>
<summary>
2021-07-28: Let's talk about Trump failing a political test in Texas.... (<a href="https://youtube.com/watch?v=Wm1rT21F9o4">watch</a> || <a href="/videos/2021/07/28/Lets_talk_about_Trump_failing_a_political_test_in_Texas">transcript &amp; editable summary</a>)

Trump's failed political test in Texas signals a shift away from Trumpism within the Republican Party, challenging the belief that his endorsement guarantees success.

</summary>

"The moment of Trumpism is fading. It is passing. It was a losing proposition."
"This should be a wake-up call to everybody in the Republican Party and to those people who are conservatives."
"Trump's political fortunes when it comes to elections, they really haven't been that good."
"The victory of the candidate without Trump's endorsement over the one with it can't be overlooked."
"The influence and staying power of the former president in shaping election outcomes are now being questioned."

### AI summary (High error rate! Edit errors on video page)

Trump failed a political test in the Texas 6th congressional district, where two Republican candidates, one endorsed by Trump, were almost identical in policy.
Despite the other candidate raising more money, the one without Trump's endorsement won the election, raising questions about Trump's influence over voters.
The Republican Party is currently heavily reliant on Trump's endorsement for primaries, but this election in Texas indicates that having it may not guarantee success in the general election.
Trump's endorsement did not swing the election in Texas, suggesting a potential shift away from Trumpism within the Republican Party.
This outcome serves as a wake-up call for the Republican Party and conservatives, signaling the fading moment of Trumpism as a winning strategy.
Trump's political fortunes in elections have not been consistently successful, as seen in this Texas election with two similar Republican candidates.
The victory of the candidate without Trump's endorsement over the one with it underscores the changing dynamics within the Republican Party.
This election result should prompt reflection on the effectiveness of Trump's influence and the direction of the Republican Party moving forward.
The influence and staying power of the former president in shaping election outcomes are now being questioned.
The Texas election outcome challenges the notion that Trump's endorsement is a guaranteed path to victory in Republican primaries.

Actions:

for republican party members,
Reassess reliance on Trump's endorsement for election success (implied)
Engage in critical reflection on the future direction of the Republican Party (implied)
Support candidates based on policy and merit rather than endorsements (implied)
</details>
<details>
<summary>
2021-07-27: Let's talk about the Love America Act.... (<a href="https://youtube.com/watch?v=AOvecN0IlBg">watch</a> || <a href="/videos/2021/07/27/Lets_talk_about_the_Love_America_Act">transcript &amp; editable summary</a>)

Beau explains the flaws in the Love America Act, advocating for acknowledging the US's racist past to work towards a more inclusive future.

</summary>

"You want them to love America? Give them an America worth loving."
"Students should absolutely read these things. Not because they won't show that the US was racist when it was founded. It's definitely going to show that because it was."
"The United States isn't perfect. It's not a finished project."

### AI summary (High error rate! Edit errors on video page)

Introduces a proposed legislation called the Love America Act by Senator Hawley, sparking controversy.
Points out the contradiction between the reasons stated for the legislation and its actual content.
Explains the four-step plan in the Love America Act to ensure American students don't view the US as inherently racist.
Criticizes the Act's approach of having students memorize patriotic documents without acknowledging the racist history.
Emphasizes that studying the founding documents of the US reveals its inherent racism due to slavery.
Argues that rather than hiding the country's racist past, students should understand it to strive for a more inclusive future.
Believes that politicians lying about historical truths hinders the country's progress in learning from its past.
Suggests that reading and understanding these documents can provide insight into the current societal issues stemming from past racism.
Acknowledges that the US is not a perfect nation but a work in progress towards fulfilling its promises of freedom and equality.
Encourages students to read these documents not to deny the country's racist history but to grasp where improvements are needed for a more inclusive society.

Actions:

for students, educators, activists,
Encourage students to study and understand the founding documents of the US (suggested).
Promote critical thinking and historical awareness in educational curriculums (implied).
</details>
<details>
<summary>
2021-07-27: Let's talk about my motives and people's rivers.... (<a href="https://youtube.com/watch?v=hIokMwSasaE">watch</a> || <a href="/videos/2021/07/27/Lets_talk_about_my_motives_and_people_s_rivers">transcript &amp; editable summary</a>)

Beau addresses his motivation for discussing critical topics, aiming to break through fear and misinformation to help individuals move forward positively.

</summary>

"It's symbolic like anything else. You get one little hole in that dam. And the desire to live their life, it will come back."
"We have to move forward as individuals."
"Imagine how sad a life is that has come to the point where all forward movement has stopped because of hatred, because of bigotry, because of paranoia, because of fear."
"I want to do my part to help drill little holes in hopes that once they get that shot in their arm, that the rest of the fear will slip away."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau addresses a question about why he talks about certain topics even though those individuals may not watch his channel initially. He points out that they do watch based on their comments and engagement.
His motivation stems from a personal desire for his wife to not have to deal with certain issues, along with a broader mental health aspect.
Beau uses the analogy of life being a river that should keep moving forward but is sometimes stopped by fear, paranoia, and wild theories.
He mentions people in the country expecting Biden and Harris to resign, leading Trump to become president, showcasing the impact of misinformation and fear.
Beau believes that breaking through fear and getting people to take vaccines can help dismantle other baseless theories and fears, allowing society to move forward.
He stresses the importance of individuals overcoming their fears and biases for societal progress.
Despite ideological differences, Beau encourages challenging others' beliefs to help them move forward in life.
His efforts are aimed at helping individuals overcome hatred, bigotry, and fear to resume forward movement in their lives.
Beau acknowledges the sadness of lives stalled due to fear and prejudice, and he hopes to contribute by breaking down those barriers.
He concludes by expressing his desire to play a part in helping people overcome their fears and move forward positively.

Actions:

for community members,
Challenge beliefs respectfully to help individuals overcome fears (exemplified)
Encourage open dialogues about vaccines and mental health (exemplified)
</details>
<details>
<summary>
2021-07-26: Let's talk about how talk radio shaped Republican politicians.... (<a href="https://youtube.com/watch?v=go10YNi2CsI">watch</a> || <a href="/videos/2021/07/26/Lets_talk_about_how_talk_radio_shaped_Republican_politicians">transcript &amp; editable summary</a>)

Beau analyzes conservative talk radio influence, Republican stance on vaccines, and political motivations behind the shift.

</summary>

"There's nothing that will convince you you are not a conservative faster than listening to conservative talk radio."
"It's really easy for somebody at the top like McConnell to shift from 'it's a personal choice' to 'you need to go get your vaccine.'"
"I don't believe it's an actual change of heart. I think it has to do with statistics and polling."

### AI summary (High error rate! Edit errors on video page)

Beau delves into conservative talk radio and its impact on his views.
He mentions the repetitive nature of conservative talk radio shows, with hosts becoming increasingly extreme.
The divergence within the Republican party regarding vaccines is compared to conservative talk radio sensationalism.
Beau believes that senior Republicans' shift on vaccines is more about political tactics than genuine belief.
He speculates that the abrupt reversal on vaccines may be influenced by statistics and polling rather than a change of heart.
The correlation between Biden voters and vaccine rates is discussed as a potential factor in the Republican shift.
Beau expresses skepticism about politicians truly caring for their constituents and suggests their actions are driven by political considerations.

Actions:

for political observers, voters,
Pay attention to political rhetoric and analyze it critically (suggested)
Stay informed about political strategies and motivations (implied)
</details>
<details>
<summary>
2021-07-26: Let's talk about Americans today vs  Americans of the 1940s.... (<a href="https://youtube.com/watch?v=6HQSMpS8KaU">watch</a> || <a href="/videos/2021/07/26/Lets_talk_about_Americans_today_vs_Americans_of_the_1940s">transcript &amp; editable summary</a>)

Exploring the concept of the "greatest generation" from the 1940s and urging people to emulate their unity and sacrifices by wearing masks and getting vaccinated.

</summary>

"Patriotism is having a loyalty to a geographic area and having a desire to protect those within it, putting that group of people above yourself."
"If you want to claim to be the heir of those Americans, you need to do your part now."
"The greatest generation was the greatest, not because it fought World War II, but because the entire country came together to stop something that needed to be stopped."

### AI summary (High error rate! Edit errors on video page)

Exploring the concept of the "greatest generation" from the 1940s in American society.
Addressing memes that suggest today's Americans are not up to the same task as the Americans from the 1940s.
Disputing the idea that the 1940s generation was uniquely tough and downplaying the emotional and psychological impacts of war.
Pointing out the historical inaccuracy in suggesting that liberals couldn't have participated in the same actions as the generation from the 1940s.
Emphasizing that the greatness of that generation was not due to their going to war, but their unity and willingness to sacrifice for a common goal.
Drawing parallels between the inconveniences that generation endured during wartime and the minor inconveniences asked of Americans today, like wearing masks.
Stating that true patriotism is about putting the well-being of others above oneself and making sacrifices for the greater good.
Encouraging people to wear masks, get vaccinated, and do their part in stopping the current crisis to truly honor the spirit of the "greatest generation."

Actions:

for americans,
Wear a mask, get your shots, and go get your vaccine (exemplified)
Put the well-being of others above yourself by following public health guidelines (exemplified)
</details>
<details>
<summary>
2021-07-25: Let's talk about blame for the 6th and the National Guard.... (<a href="https://youtube.com/watch?v=lnRq3EKGFsQ">watch</a> || <a href="/videos/2021/07/25/Lets_talk_about_blame_for_the_6th_and_the_National_Guard">transcript &amp; editable summary</a>)

Politicians unfairly scrutinize the National Guard; ultimate responsibility for Jan 6 failures lies with the White House, not the Guard.

</summary>

"Nobody wearing a National Guard uniform is responsible for any of the failures or breakdowns that occurred on the 6th."
"There's nothing the National Guard could have done. They had orders."
"The ultimate responsibility for the failures that occurred related to the National Guard? You need to look to the White House at the time."
"The American people do not care about this. This is not what people are concerned with."
"You want to know who's ultimately responsible, it's President Trump."

### AI summary (High error rate! Edit errors on video page)

Politicians are questioning the National Guard's actions and decisions on January 6th.
The scrutiny aimed at the National Guard is unwarranted, as they were following guidance.
Orders from Acting Secretary of Defense limited the National Guard's capabilities on that day.
The guidance restricted the National Guard from having necessary tools and engaging with protesters.
The ultimate responsibility for the failures on January 6th lies with the White House at that time.
The guidance was intended to prevent an overreaction by the National Guard.
President Trump's directive to protect demonstrators influenced the restrictive guidance.
The American people are more concerned about how the events started rather than operational breakdowns.

Actions:

for politically aware citizens,
Seek accountability from those in power (implied)
Advocate for transparent investigations into the events of January 6th (implied)
</details>
<details>
<summary>
2021-07-25: Let's talk about Summer fun.... (<a href="https://youtube.com/watch?v=GD-FU-9QTkc">watch</a> || <a href="/videos/2021/07/25/Lets_talk_about_Summer_fun">transcript &amp; editable summary</a>)

Family summer fun turned serious as Beau recounts a near-drowning incident, urging the importance of quick action, CPR knowledge, and individual preparedness for emergencies.

</summary>

"Knowledge weighs nothing. You will carry it with you the rest of your life."
"If you have the ability, please, please take a course."
"There is no response that is going to get there fast enough."
"A little bit of knowledge can greatly alter the outcome of events."
"Have a good day."

### AI summary (High error rate! Edit errors on video page)

Family summer fun turned serious when a child almost drowned.
Child rescued by family members after not responding in the water.
Quick action taken to clear child's lungs and call 911.
EMS arrived remarkably fast, within five to six minutes.
Child needed to be checked out despite appearing fine.
Importance of knowing CPR and taking classes emphasized.
Acknowledgment that sometimes it's up to individuals to act quickly.
Encourages getting certified in CPR as it can save a life.
Stress on the need for quick response in emergencies.
Reminder that knowledge and skills can alter outcomes significantly.
Despite being okay, the child still needed medical evaluation.
Urges people to be prepared for situations when help may not arrive in time.
Knowledge is emphasized as a valuable tool that can be carried for a lifetime.
Encouragement to prioritize learning life-saving skills.
Closing thought on the importance of being prepared for emergencies.

Actions:

for parents, caregivers, community members,
Take a CPR class (suggested)
Get certified in CPR (suggested)
Stay prepared for emergencies (implied)
</details>
<details>
<summary>
2021-07-24: Let's talk about the Cleveland Guardians.... (<a href="https://youtube.com/watch?v=r7c8qJ3KjU8">watch</a> || <a href="/videos/2021/07/24/Lets_talk_about_the_Cleveland_Guardians">transcript &amp; editable summary</a>)

Beau addresses the controversy around sports team name changes and criticizes conservative tactics of outrage without solutions or policy.

</summary>

"I understand that can cause a lot of stress and a lot of anxiety."
"They want to maintain the status quo and the world is changing."
"All they have is stirring up outrage and fear because they have turned into shock jocks."
"They are hilariously wrong."
"Because they don't have solutions, they don't have policy."

### AI summary (High error rate! Edit errors on video page)

A baseball team's name change is the new front in the culture war, with Ted Cruz and former President Trump lamenting the Cleveland Indians becoming the Cleveland Guardians.
Beau addresses a message accusing him of deleting a video on army helicopters named after Indians due to Trump supporting the practice, calling out the sender as disingenuous.
He clarifies that the video was not deleted but is hard to find because the title does not mention Indians or helicopters, urging the sender to watch it.
Beau explains that when the military names helicopters after native groups, it is to honor them, not to create caricatures like sports mascots.
He humorously notes Trump's focus on sports team names in his post-presidency phase, finding it entertaining but also acknowledging the anxiety name changes can cause.
Beau jests about his own experience with sports teams changing names, illustrating the frivolity of the culture war and conservatives' lack of substantial policy.
He points out that conservatives rely on outrage and fear-mongering to stay relevant, lacking real solutions or policies.
The Cleveland Indians have had several name changes in the past, leading Beau to suggest that one more change won't hurt.
Beau criticizes politicians and media personalities for focusing on trivial issues like team names rather than addressing real problems.
He concludes by remarking on the conservatives' reliance on provoking outrage over insignificant matters to maintain power.

Actions:

for activists, sports fans,
Share educational content on the history and significance of indigenous names in sports teams (suggested).
Challenge fear-mongering and outrage tactics by promoting informed and constructive dialogues (implied).
</details>
<details>
<summary>
2021-07-24: Let's talk about Governor Ivey pointing fingers..... (<a href="https://youtube.com/watch?v=fepwzU7GYns">watch</a> || <a href="/videos/2021/07/24/Lets_talk_about_Governor_Ivey_pointing_fingers">transcript &amp; editable summary</a>)

Beau questions the persuasive power of blaming the unvaccinated, speculating on a narrative shift rather than increased vaccination rates in Alabama.

</summary>

"Will it work on the others? You've disappointed grandma? Maybe."
"Blame the unvaccinated people. Don't blame the people who downplayed it."
"Changing the narrative. And I think that's really what it's about."

### AI summary (High error rate! Edit errors on video page)

Analyzing the effectiveness of the governor of Alabama's statement blaming the unvaccinated for COVID-19.
The governor's message may not be persuasive due to varying motivations among individuals.
Some unvaccinated individuals may only comply if mandated due to a belief in authoritarianism.
The statement's effectiveness may lie in changing the narrative rather than increasing vaccination rates.
Beau questions whether the governor's goal was to shift blame onto the unvaccinated rather than encourage vaccination.
The approach of blaming the unvaccinated might not significantly impact vaccination rates in Alabama.
Southern cultural dynamics and historical context are considered in assessing the statement's potential impact.
The statement may resonate more with those who already follow an authoritarian leader.
Beau suggests that the goal was to divert attention and shift the narrative away from other responsible parties.
Changing the narrative and blaming specific groups might be the main intent behind the governor's statement.

Actions:

for social commentators,
Challenge narratives about blame (implied)
Encourage critical thinking about shifting blame (implied)
</details>
<details>
<summary>
2021-07-23: Let's talk about community networks from Yellowstone to Yukon.... (<a href="https://youtube.com/watch?v=u1sRwi888Pg">watch</a> || <a href="/videos/2021/07/23/Lets_talk_about_community_networks_from_Yellowstone_to_Yukon">transcript &amp; editable summary</a>)

Community networks start small but can achieve great things, like the Yellowstone to Yukon conservation effort, by defying traditional boundaries and building momentum slowly.

</summary>

"The best time to plant a tree was 20 years ago. The next best time is right now."
"Real and deep change requires thinking beyond your lifetime."
"Just because it is small doesn't mean it can't accomplish things."
"The more people you have, the more effective it will be."
"People will be willing to join something that is already moving and already succeeding."

### AI summary (High error rate! Edit errors on video page)

Community networks start slow and small, like with his attempt to start one at a barbecue where only three people showed up for the first meeting out of forty interested individuals.
Despite starting small, community networks can accomplish great things, as seen in the Yellowstone to Yukon conservation effort.
Yellowstone to Yukon aimed to protect and connect a 2,000-mile stretch of land based on animal migration patterns, not geographical boundaries.
The initiative initially started with only 10% of the land protected and 5% connectivity but has since doubled in protected land and increased connectivity to 30%.
Yellowstone to Yukon has over 200 partners, including private individuals, conservation groups, and even a mining group in Canada.
The project defied traditional conservation boundaries like state lines and international borders to protect the interconnected ecosystem.
Beau encourages building community networks with few people, as even with three individuals, projects can be initiated and successes can attract more participants.
Real and deep change requires thinking beyond one's lifetime and starting initiatives now, despite time constraints.
The best time to make a change was in the past, but the next best time is now.
Building momentum and showing early successes can attract more people to join and contribute to the community network.

Actions:

for community organizers,
Start a community network with a small group of like-minded individuals (suggested)
Initiate projects within the community network to show early successes (suggested)
Hold events like barbecues to attract more participants to the community network (suggested)
</details>
<details>
<summary>
2021-07-23: Let's talk about Huckabee Sanders and giving up on freedom.... (<a href="https://youtube.com/watch?v=pFWzfCdhya0">watch</a> || <a href="/videos/2021/07/23/Lets_talk_about_Huckabee_Sanders_and_giving_up_on_freedom">transcript &amp; editable summary</a>)

Beau warns against sacrificing freedom and personal responsibility in blindly following anti-mandate stances amidst the public health crisis.

</summary>

"If you hear, we're not going to mandate vaccines, we're not going to mandate masks, and take that to mean is you don't need to wear one, you have given up on everything you say you believe in."
"You have given up on freedom. You have given up on self-determination. You have given up on the very principles you scream about."
"People who scream about freedom and have the Constitution as a profile pick have forgotten that they need to provide for the common defense and promote the general welfare."
"Every candidate right now, everybody in public office right now should have one goal. That is to end the public health issue."
"Freedom is something that carries responsibility with it. Responsibility for your own actions, shared responsibility for your neighbors."

### AI summary (High error rate! Edit errors on video page)

Beau delves into Sarah Huckabee Sanders' statements, the implications, and how some Americans unknowingly surrender their freedom.
Sarah Huckabee Sanders expressed her desire to be the governor of Arkansas and announced her opposition to mask and vaccine mandates.
Beau agrees with limited mandates due to valuing freedom and people's ability to self-govern.
The past year has made Beau question people's self-governing ability due to their actions during the pandemic.
Beau points out the contradiction in authoritarian followers' interpretation of no mandates as a lack of importance rather than government non-interference.
Many Trump supporters are aware of his vaccination status and Fox News' vaccine passport but still resist mandates due to their perceived connection with Trump's stance.
Beau argues that freedom comes with responsibility for one's actions and shared responsibility for others.
People who submit to authoritarianism often neglect personal responsibility and allow leaders to dictate their choices.
Beau criticizes those who prioritize perceived wishes of leaders over their own well-being by refusing vaccines or masks.
Beau warns of the danger of public figures like Sarah Huckabee Sanders making firm statements against mandates in a dynamic public health crisis.
Beau believes that all public officials should prioritize ending the public health crisis, labeling such statements as hindrances to this goal.
Beau questions the real intent behind Sanders' statement, suggesting that it aimed to rally her base rather than contribute to resolving the public health crisis.

Actions:

for americans,
Wear a mask and get vaccinated to protect yourself, your family, and your community (implied)
Prioritize ending the public health crisis as the primary goal for all public officials (implied)
</details>
<details>
<summary>
2021-07-22: Let's talk about what we can learn from security failures.... (<a href="https://youtube.com/watch?v=-rm32g1CNR4">watch</a> || <a href="/videos/2021/07/22/Lets_talk_about_what_we_can_learn_from_security_failures">transcript &amp; editable summary</a>)

Beau explains how maintaining security posture post-event applies to public health measures, urging individuals to go beyond guidelines as attention wanes.

</summary>

"The job's not over just because the main event is."
"You should probably elevate your security posture a little bit when it comes to this."
"That chain is broke."
"The job isn't over until the client is secure."
"A whole lot of people aren't paying attention."

### AI summary (High error rate! Edit errors on video page)

Shares insights from his experience as a security consultant and being part of protective details.
Explains the concept of inner and outer rings in security details.
Describes how the outer ring, often comprising inexperienced individuals like local cops, serves as a deterrent.
Notes a common breakdown in security during the post-event meet and greet phase.
Emphasizes the importance of maintaining security posture even after the main event is over.
Draws parallels between security details and the public health issue in the United States.
Advises individuals to elevate their security posture regarding public health guidelines.
Urges people to go beyond the standard guidance to account for those who are not following protocols.
Warns of a potentially rough fall if attention to public health measures wanes.
Encourages staying vigilant and proactive until the situation is under control.

Actions:

for individuals,
Elevate your security posture regarding public health guidelines (suggested).
Go beyond standard guidance and take additional precautions, like wearing masks inside all buildings (suggested).
</details>
<details>
<summary>
2021-07-22: Let's talk about the space adventures of Jeff Bezos.... (<a href="https://youtube.com/watch?v=nAr5ZJFWgbY">watch</a> || <a href="/videos/2021/07/22/Lets_talk_about_the_space_adventures_of_Jeff_Bezos">transcript &amp; editable summary</a>)

Beau questions the wisdom of polluting space with heavy industry and advocates for prioritizing Earth's protection over interplanetary endeavors.

</summary>

"Let's not pollute space. Let's not move all heavy industry."
"Maybe the best idea is to figure out how to live in some kind of balance with earth."
"That personal profit motivator rather than the drive to push humanity forward, that is something we have to change."

### AI summary (High error rate! Edit errors on video page)

Recalls childhood fascination with a giant rocket in Florida and the idealistic nature of space travel.
Expresses initial concern over billionaires turning space exploration into a hobby.
Mentions the "overview effect" experienced by astronauts, where they realize the fragility of Earth from space.
Comments on Jeff Bezos going to space and his subsequent interview about climate change.
Criticizes Bezos' idea of polluting space by moving heavy industry there.
Emphasizes the importance of not repeating past mistakes like burning stuff or burying toxic waste.
Advocates for finding a balance with Earth and changing the profit-driven mindset to push humanity forward.

Actions:

for activists, environmentalists, space enthusiasts,
Advocate for environmental protection on Earth (implied)
Educate others about the importance of sustainability (implied)
Support initiatives that aim to protect the planet (implied)
</details>
<details>
<summary>
2021-07-21: Let's talk about Tom Barrack and influence.... (<a href="https://youtube.com/watch?v=WB1OVMCZI0s">watch</a> || <a href="/videos/2021/07/21/Lets_talk_about_Tom_Barrack_and_influence">transcript &amp; editable summary</a>)

Beau explains why Tom Barrack's actions, though not meeting the legal definitions of treason or espionage, could still have significant implications if proven true.

</summary>

"An agent of influence having direct access to the President of the United States, that's kind of a big deal."
"I'm not certain how this is going to play out, but I can assure you that if you were a counterintelligence officer and you found out that somebody who was operating on behalf of a foreign power had direct access to the President of the United States and to other government officials who were operating in the region of that foreign power, you'd be very concerned."
"This is a big deal."

### AI summary (High error rate! Edit errors on video page)

Explains the context of Tom Barrack being indicted for allegedly operating on behalf of the United Arab Emirates, a close associate of former President Donald Trump.
Addresses the questions of how this situation is not considered treason or espionage.
Differentiates between the common usage and legal definition of treason in the U.S., stating that the allegations do not meet the narrow legal criteria for treason.
Explores the concept of espionage, mentioning that if the allegations are true, Barrack was acting as an agent of influence for a foreign power.
Emphasizes the significance of agents of influence in shaping policy and public opinion.
Notes that while common parlance may label the actions as espionage, the legal statutes have specific requirements that may not be met based on the released information.
Raises the point of potential plea deals based on the volume of emails and communications possessed by federal authorities.
Stresses the gravity of the situation, considering the direct access Barrack had to the President and the implications of his alleged actions.
Comments on the importance of transparency regarding working for foreign powers, especially in close proximity to policy-making circles.
Anticipates that the situation will likely evolve into a major story, given the implications.

Actions:

for political analysts and concerned citizens.,
Monitor developments in the case and stay informed about updates (implied).
Advocate for transparency and accountability in relationships with foreign entities (implied).
</details>
<details>
<summary>
2021-07-21: Let's talk about Pelosi's decision to veto Republican picks.... (<a href="https://youtube.com/watch?v=mZVt82j5QVo">watch</a> || <a href="/videos/2021/07/21/Lets_talk_about_Pelosi_s_decision_to_veto_Republican_picks">transcript &amp; editable summary</a>)

Nancy Pelosi's decision to exclude Republicans from a committee investigating the events of January 6th is a strategic move to prevent undermining democracy by denying a platform to those seeking to justify and cover up the attack.

</summary>

"It was an attempt to undermine the Constitution."
"Deny the opposition that platform."
"But her decision to stop those who are at least willing to appear to be ideologically
aligned with those who attempted to stop the election, stop the certification, her decision
to bar them from being on this committee is the right one."

### AI summary (High error rate! Edit errors on video page)

Nancy Pelosi stopped certain Republicans from being on a committee looking into the events of the 6th.
Republicans submitted names of people who actively supported undermining the election.
Beau believes Pelosi's decision should be analyzed beyond a partisan view.
He draws a historical analogy to the development of factions in movements seeking to change governments.
Beau explains the roles of the military and political wings in such movements.
He mentions how politicians were likely to justify, excuse, downplay, and cover up what happened on the committee.
Beau asserts that the ultimate goal is to install an authoritarian leader by undermining democracy.
He suggests that politicians may not all fully support the movement's goals but might be appealing to certain voter bases.
Beau argues that those ideologically or politically involved in undermining democracy should not be on the committee.
He mentions that disrupting the hearings could be a beneficial move for the political wing of the movement.

Actions:

for political analysts, concerned citizens,
Disrupt hearings (implied)
</details>
<details>
<summary>
2021-07-20: The roads with Kim Kelly and the union.... (<a href="https://youtube.com/watch?v=vjsn2Tbl7ak">watch</a> || <a href="/videos/2021/07/20/The_roads_with_Kim_Kelly_and_the_union">transcript &amp; editable summary</a>)

Beau and Kim Kelly shed light on the underreported coal miner strike in Alabama and the community's resilience against corporate oppression.

</summary>

"One day longer, one day stronger."
"It's been my personal crusade to get people to pay attention to this strike."
"These folks just want to go back to work and make their money and live their lives."

### AI summary (High error rate! Edit errors on video page)

Introduction to guest Kim Kelly, a freelance labor reporter with a book coming out soon.
Kim Kelly shares her background, living in South Philadelphia, and reporting on strikes in Alabama.
Kim Kelly recounts her involvement with the Amazon warehouse workers' unionizing efforts.
Details about the coal miner strike at Warrior Met Coal in Brooklyn, Alabama, due to unfair labor practices.
Description of the strike, lack of mainstream media coverage, and the support network established by the community.
Insights into the disturbing tactics used by Warrior Met, including vehicular attacks on strikers.
Kim Kelly's reflections on the stark contrast between the supportive community efforts and the violent attacks.
Overview of the unique structure of the picket lines at Warrior Met Coal in Brookwood.
Beau and Kim Kelly discussing the lack of political support for the striking miners from both Democrats and Republicans.
Information about upcoming events organized by the striking miners and their families to protest in New York City.

Actions:

for advocates for workers' rights,
Support the striking miners by donating to the strike fund or contributing to the strike pantry (suggested).
Amplify information about the strike and the community support efforts on social media (exemplified).
</details>
<details>
<summary>
2021-07-20: Let's talk about how to trust the experts when they disagree.... (<a href="https://youtube.com/watch?v=a_CO-lGefCM">watch</a> || <a href="/videos/2021/07/20/Lets_talk_about_how_to_trust_the_experts_when_they_disagree">transcript &amp; editable summary</a>)

Beau addresses the importance of trusting experts and consensus, particularly on the topic of wearing masks in schools, stressing the need to rely on specialization and the overwhelming consensus among medical professionals.

</summary>

"Specialization is something that humans do very well."
"There isn't a real argument when it comes to this topic."
"Everybody knows, because it's a painfully obvious truth."
"When kids go back to school, wash your hands. Don't touch your face. Wear a mask."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addresses the topic of experts, consensus, and new guidance, specifically regarding wearing masks in schools.
Responds to a message pointing out that he is not a medical expert but has some medical training.
Acknowledges the challenges of determining which expert to trust when there are disagreements within a field.
Notes that true consensus among experts requires statements that are painfully obvious and inarguable.
Mentions the guidance from the Academy of American Pediatrics recommending kids wear masks when they return to school.
Comments on the tendency for people to question experts in fields where they lack training.
Emphasizes the importance of specialization and relying on the expertise of others.
Stresses that there is overwhelming consensus among medical professionals on the effectiveness of masks and vaccines.
Encourages kids to follow basic hygiene practices when they return to school.
Concludes by sharing his thoughts on the matter and wishing everyone a good day.

Actions:

for parents, educators, policymakers,
Trust the guidance provided by medical professionals when it comes to wearing masks and vaccines (implied).
Encourage children to practice good hygiene habits, such as washing hands, not touching their faces, and wearing masks when they return to school (implied).
</details>
<details>
<summary>
2021-07-20: Let's talk about closing Guantanamo.... (<a href="https://youtube.com/watch?v=C-envUtGol4">watch</a> || <a href="/videos/2021/07/20/Lets_talk_about_closing_Guantanamo">transcript &amp; editable summary</a>)

Beau questions the feasibility of closing Guantanamo due to political risks, stressing the importance of standing by principles despite past violations and fear-driven decisions.

</summary>

"If what we believe to be right and good and true is right and good and true, we have to stand by those principles no matter how hard it is."
"This place exists because the United States violated its own principles."
"It's not an intelligence thing."

### AI summary (High error rate! Edit errors on video page)

A facility is challenging to close, and there are lessons to learn from it.
Guantanamo Bay, where the United States detained people for about 20 years, is the focus.
The Trump administration showed no interest in addressing the Guantanamo issue.
The Biden administration approved a deal to let one person leave Guantanamo for Morocco.
Some detainees in Guantanamo were based on inaccurate information, faced harsh conditions, and many were never charged.
There is no military or intelligence application for keeping most detainees in Guantanamo.
Politics, not military necessity, is the main reason for the delay in closing Guantanamo.
The fear of potential political fallout if a released detainee commits an offense is a significant barrier to closing Guantanamo.
The facility exists because the US violated its own principles, and the decisions made have continued the cycle.
Beau questions if it is politically feasible to close Guantanamo due to the risks involved.
He stresses the importance of standing by principles, even when it's difficult and fear-driven decisions lead to violations.

Actions:

for politically-aware individuals.,
Advocate for the closure of Guantanamo and the fair treatment of detainees (implied).
Support politicians willing to prioritize principles over political fallout in addressing Guantanamo (implied).
</details>
<details>
<summary>
2021-07-19: Let's talk about wolves, Wisconsin, and tough guys.... (<a href="https://youtube.com/watch?v=Al_sTUSa4Eg">watch</a> || <a href="/videos/2021/07/19/Lets_talk_about_wolves_Wisconsin_and_tough_guys">transcript &amp; editable summary</a>)

Beau criticizes the detrimental impact of wolf hunting on Wisconsin's population and ecosystem, urging a shift towards responsible conservation efforts over macho displays.

</summary>

"You aren't on foot carrying a flintlock anymore in a wild and untamed area."
"This could have been one of the great success stories of American conservation."
"It's not all about destruction."
"The days of that man versus nature contest, man versus wolf, it's over."
"Pretending you're a mountain man."

### AI summary (High error rate! Edit errors on video page)

Eight months ago, Beau expressed concern about the removal of gray wolves from the Endangered Species Act list, fearing the lack of federal protections.
The University of Wisconsin study reveals a devastating decrease in the gray wolf population in Wisconsin by a third, taking it back to levels from a decade ago.
Beau criticizes the inability of the state to protect the wolves, attributing the population decline to the desire of some individuals to seem tough.
He stresses the ecological repercussions of losing a third of the wolf population, disrupting packs and hindering recovery efforts.
Despite the state considering another hunt, Beau argues that science clearly indicates the unsustainability of such actions if the goal is to preserve the wolf population.
Beau condemns the prioritization of economic interests and macho ideals over conservation efforts, setting back decades of progress in less than a year.
He suggests that those engaging in wolf hunting for sport should find a more constructive hobby and recognize the creative aspect of masculinity.
Beau points out that modern advancements have removed the genuine challenge of man versus nature, reducing it to a superficial game of make-believe.
He concludes by urging people to reconsider their actions and embrace a more responsible approach to wildlife conservation.

Actions:

for conservationists, wildlife enthusiasts,
Advocate for stricter wildlife protection laws in your state (implied)
Support local conservation efforts and organizations (implied)
</details>
<details>
<summary>
2021-07-19: Let's talk about 8 months, justice, and fairness.... (<a href="https://youtube.com/watch?v=RgFXRd_qRoM">watch</a> || <a href="/videos/2021/07/19/Lets_talk_about_8_months_justice_and_fairness">transcript &amp; editable summary</a>)

Beau addresses the controversy of an eight-month sentence, questioning the purpose of prisons and advocating for a justice system not based on vengeance.

</summary>

"I don't want to live in a society where the justice system is based on vengeance."
"We have to determine as society what we want our prisons to be, what we want our justice system to be."
"If it's about rehabilitation, if it's about restorative justice, then it's completely reasonable."
"Make sure that Mark and Malik should be at home. Not that Florida man should do 20 years."
"Not just that you're angry. Not just that you want punishment."

### AI summary (High error rate! Edit errors on video page)

Explaining the controversy surrounding an eight-month sentence given to a Florida man for actions at the Capitol on January 6th.
Clarifying that the person received the sentence for being a "selfie seditionist" and not for engaging in violent acts or causing damage.
Mentioning that the sentence was relatively light due to the defendant pleading guilty early and having no significant criminal history.
Addressing the question of fairness and justice in the sentencing, pointing out the potential consequences beyond just the time served.
Questioning the purpose of prisons and the justice system – whether they are meant for punishment, rehabilitation, or simply separation from society.
Rejecting the idea of longer sentences based on unfair past judgments, advocating for a system that focuses on restorative justice.
Expressing concern over mass incarceration rates in the United States and the need to reconsider who gets locked up and for how long.
Encouraging a deeper reflection on personal beliefs when discussing sentences and justice, rather than reacting out of anger or a desire for revenge.

Actions:

for justice reform advocates,
Advocate for restorative justice practices and rehabilitation programs in your community (suggested)
Support organizations working towards reducing mass incarceration rates and promoting fair sentencing policies (suggested)
</details>
<details>
<summary>
2021-07-18: The roads with Isiah Holmes, journalist on a list.... (<a href="https://youtube.com/watch?v=wifd7x2tA2E">watch</a> || <a href="/videos/2021/07/18/The_roads_with_Isiah_Holmes_journalist_on_a_list">transcript &amp; editable summary</a>)

Isaiah Holmes and Beau expose the surveillance tactics of the Wauwatosa Police Department targeting protesters and journalists, raising concerns about privacy, constitutional rights, and the chilling effect on press freedom.

</summary>

"Even if law enforcement is routinely violating and disregarding the constitution, that matters even if it doesn't negatively impact you, because someday it will impact you and people you care about." - Isaiah Holmes
"People really need to understand that these things are real and they have real consequences." - Isaiah Holmes
"Always be informed about privacy. If it means paying for a little bit better antivirus, go for it." - Beau
"You do lock your doors, right? That means you have things to hide, even if it's seemingly innocuous. It matters." - Beau
"If it doesn't negatively impact you, someday it will impact you and people you care about." - Isaiah Holmes

### AI summary (High error rate! Edit errors on video page)

Introducing Isaiah Holmes, a journalist with the Wisconsin Examiner from Milwaukee, Wisconsin, who was recently placed on a list by the Wauwatosa Police Department.
The list included over 200 individuals, including elected officials, lawyers, activists, and Isaiah Holmes, with detailed personal information and connections to their social media profiles.
The list was shared with the FBI and the Milwaukee Police Department, raising concerns about surveillance and potential infringement on constitutional rights.
The Wauwatosa Police Department claimed the list was a tool for investigating protests, including individuals who were witnesses or bystanders, not just protesters or suspects.
Isaiah Holmes had a history of being monitored by law enforcement since his teenage years, including being followed and experiencing odd social media occurrences.
There are talks of legal action and civil rights implications following the exposure of the list and other questionable actions by the Wauwatosa Police Department.
Law enforcement responses to ongoing protests in Milwaukee, Wauwatosa, and Kenosha varied, with some departments being hostile and others more restrained.
The determination and organizing efforts of protest groups like the People's Revolution in Milwaukee have continued despite challenges and lack of media coverage.
Isaiah Holmes, a journalist, was placed on a list by the Wauwatosa Police Department as part of a surveillance effort targeting protesters and individuals associated with protests in Milwaukee and Wauwatosa.
The list included detailed personal information about individuals, such as their names, ages, race, gender, affiliations, and even vehicle information.
Despite being labeled as a journalist, Isaiah was included on the list as a potential witness to protest events, raising concerns about the chilling effect on press freedom and free speech.
There are suspicions of deeper surveillance and privacy breaches, including tampering with phones and intelligence gathering on lawyers, lawmakers, and journalists involved in the protests.
Isaiah's history with journalism and experiences of being monitored by law enforcement since his teenage years have added to concerns about privacy and constitutional rights violations.
The ongoing determination of protest groups like the People's Revolution in Milwaukee and evolving responses from law enforcement indicate a complex and challenging environment for activism and civil rights.
Isaiah encourages people to be aware of privacy measures like VPNs and password protection, stressing the importance of understanding and defending constitutional rights even if they seem unaffected by surveillance.

Actions:

for journalists, activists, civil rights advocates,
Contact civil rights organizations for support and legal advice (implied)
Stay informed about privacy measures like VPNs and password protection (implied)
Support independent media outlets covering civil rights issues (exemplified)
</details>
<details>
<summary>
2021-07-18: Let's talk about Legos, being your own worst enemy, and the Block 19.... (<a href="https://youtube.com/watch?v=b_c8fB8m3Xw">watch</a> || <a href="/videos/2021/07/18/Lets_talk_about_Legos_being_your_own_worst_enemy_and_the_Block_19">transcript &amp; editable summary</a>)

Beau addresses the concerning trend of making guns look like toys, predicting consequences and legislation while criticizing the gun crowd's identity association with firearms.

</summary>

"The gun crowd, you are your own worst enemy."
"There are consequences to your actions."
"Making it part of your identity. You know, making it something, a substitute for masculinity. It's very toxic."
"I'm not saying that you should sell off all your firearms and give up your identity or your hobby or whatever."
"Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Mention of discussing consequences of actions, being one's own worst enemy, Legos, upcoming legislation, and a concerning issue.
Introduction to the Block 19, a kit designed to make a Glock look like it's made out of Legos.
Noting the cease and desist order from Lego and the potential compliance from the company.
Criticism towards the gun crowd for being their own worst enemy and making guns look like toys.
Reflection on the phrase "guns are not toys" and questioning the motive behind making firearms appear as toys.
Connecting the issue to identity politics and the consequences of such actions.
Concerns about potential increase in accidental shootings and plausible deniability for cops shooting kids with toy-like guns.
Prediction of future legislation limiting firearm finishes and the possible impact on gun control.
Critique on marketing a kit that makes a firearm resemble a toy with no practical purpose.
Addressing the inevitable persecution claims from the gun crowd when legislation is enacted.
Criticism of treating guns as more than just tools and integrating them into one's identity.
Commentary on toxic masculinity and the need for a mindset shift regarding firearm ownership.
Personal reflection on happiness, security, and calmness post distancing from firearm ownership.

Actions:

for gun owners,
Contact legislators to advocate for responsible firearm legislation (implied).
Educate gun owners on the importance of treating firearms as tools, not toys (implied).
</details>
<details>
<summary>
2021-07-17: Let's talk about the Biden administration's information campaign.... (<a href="https://youtube.com/watch?v=M10B1htACU0">watch</a> || <a href="/videos/2021/07/17/Lets_talk_about_the_Biden_administration_s_information_campaign">transcript &amp; editable summary</a>)

Beau supports mitigation efforts but questions the Biden administration's approach to combating disinformation on social media, advocating for counter messaging over censorship.

</summary>

"It's counter messaging. It's putting out the facts. It's putting out the information."
"The answer to this isn't necessarily getting rid of this content. It's counter messaging."
"We have to actually convince people that this is the right thing to do, and not just take away the opposing view."

### AI summary (High error rate! Edit errors on video page)

Beau supports mitigation efforts like wearing masks and getting vaccinated.
The Biden administration is pressuring social media platforms to remove people spreading disinformation.
Beau questions the constitutionality of the Biden administration's actions.
Beau believes that although constitutional, the idea of silencing opposing views is a bad precedent.
He warns against the dangers of setting a precedent where the executive branch can censor information.
Beau advocates for counter messaging and providing factual information as a better approach.
He expresses concern about the lasting impact and dangers of granting such power to control information.
Beau believes an effective counter messaging campaign is more beneficial than just removing content.
He stresses the importance of convincing people through ideas rather than silencing opposition.
Beau acknowledges the potential consequences of allowing such censorship power to be established.

Actions:

for social media users,
Advocate for counter messaging with factual information (implied)
Encourage open debate and education on public health issues (implied)
</details>
<details>
<summary>
2021-07-17: Let's talk about Tucker Carlson, Dietrich, and June.... (<a href="https://youtube.com/watch?v=xmsuhS-RgNo">watch</a> || <a href="/videos/2021/07/17/Lets_talk_about_Tucker_Carlson_Dietrich_and_June">transcript &amp; editable summary</a>)

Beau sheds light on the political divide influencing vaccine hesitancy and urges seeking medical advice over media influence.

</summary>

"It shows that it's purely political."
"Don't take any advice from people on a screen, myself included."
"Almost all of them are vaccinated. It's not that they believe any of this stuff they're saying."

### AI summary (High error rate! Edit errors on video page)

A plea to Tucker Carlson to publicly acknowledge his vaccination status to influence hesitant viewers like June.
Drawing attention to the political divide in vaccination rates based on political affiliation, not medical guidance.
Trump's initial downplaying of the pandemic and its impact on shaping conservative media's response.
Criticizing modern news shows for not issuing corrections or admitting when they were wrong, contributing to misinformation.
Urging people to seek medical advice from professionals rather than relying on screen personalities.
Offering a firsthand account from a woman who worked in a COVID ward to help persuade vaccine skeptics.
Calling out wealthy individuals and politicians who spread vaccine misinformation while being vaccinated themselves.
Encouraging everyone to get vaccinated for the well-being of themselves and their loved ones.

Actions:

for general public, vaccine skeptics,
Contact a doctor or nurse for advice on vaccines (implied)
Listen to stories from healthcare professionals to understand the importance of vaccination (implied)
</details>
<details>
<summary>
2021-07-16: Let's talk about real-world numbers in Los Angeles.... (<a href="https://youtube.com/watch?v=dRYevumAkog">watch</a> || <a href="/videos/2021/07/16/Lets_talk_about_real-world_numbers_in_Los_Angeles">transcript &amp; editable summary</a>)

Beau talks about the effectiveness of vaccines in preventing hospitalizations, revealing that in LA County, all COVID patients admitted were not fully vaccinated, settling the debate on vaccine efficacy.

</summary>

"Real world right there. That's real world."
"The question about whether or not it works is over. It does."
"They are hoping that everything goes wrong and that their constituents suffer."

### AI summary (High error rate! Edit errors on video page)

Talking about the real-world numbers in LA related to the current public health issue.
LA County is experiencing an uptick in new cases for five consecutive days.
The percentage of hospitalizations remains low despite the increase in cases.
Out of those hospitalized, none are fully vaccinated with J&J, Pfizer, or Moderna vaccines.
The effectiveness of vaccination in preventing hospitalizations is evident in LA County.
There may be rare breakthrough cases or individuals who did not complete their vaccine series.
The debate about the effectiveness of vaccines is settled; they work.
Politicians, particularly Republicans, may have a vested interest in vaccine failure for political gain.
Republicans are hoping for failures to regain power, even if it means their constituents suffer.
The reality in LA County shows that every COVID patient admitted was not fully vaccinated.

Actions:

for public health officials, policymakers,
Get vaccinated with J&J, Pfizer, or Moderna vaccines (implied)
Advocate for vaccination to prevent hospitalizations (implied)
Combat misinformation about vaccine effectiveness (implied)
</details>
<details>
<summary>
2021-07-16: Let's talk about Democrats already being dissatisfied with Biden.... (<a href="https://youtube.com/watch?v=On-F1nsn79E">watch</a> || <a href="/videos/2021/07/16/Lets_talk_about_Democrats_already_being_dissatisfied_with_Biden">transcript &amp; editable summary</a>)

Democrats dissatisfied with Biden; Republicans blindly loyal to Trump - beware the cult of personality.

</summary>

"If you don't disagree with the man, if you truly see him as a savior, you really need to examine the situation a little closer because you may have been conned."
"Because let's be honest, the stuff the liberals use to make fun of him, he said it, but he was joking."
"He messed up a lot of stuff and he just said that he didn't."
"Politicians lie. We know that, right? Why is he somehow accepted from that rule?"
"And if you can't even entertain the possibility that you were conned, it's a guarantee that you were."

### AI summary (High error rate! Edit errors on video page)

Democrats are dissatisfied with President Joe Biden's performance, presenting an opening for the Republican Party and Trump in 2024.
A Twitter survey revealed registered Democrats' discontent with Biden, including issues like not supporting Medicare for all, a $15 minimum wage, and student loan forgiveness.
Republicans tend to unquestionably support Trump, unlike Democrats with Biden.
Trump supporters are seen as being part of a cult of personality rather than a political party, never disagreeing with him.
Beau questions the blind loyalty to Trump, pointing out his numerous mistakes and the disconnect between his words and actions.
He urges supporters to critically analyze their beliefs and not blindly follow Trump's rhetoric.
The possibility of being tricked by Trump as a politician is raised, encouraging supporters to re-evaluate their unwavering support.

Actions:

for voters,
Examine your political beliefs critically (implied).
Question blind loyalty to political figures (implied).
Re-evaluate support for politicians based on actions, not just words (implied).
</details>
<details>
<summary>
2021-07-15: Let's talk about Trump's coup denial.... (<a href="https://youtube.com/watch?v=1m6wvTcNWMQ">watch</a> || <a href="/videos/2021/07/15/Lets_talk_about_Trump_s_coup_denial">transcript &amp; editable summary</a>)

Beau delves into Trump's response to Milley's concerns, showcasing authoritarian behavior and Milley's serious worries about a potential coup.

</summary>

"Former President Trump behaves like the typical authoritarian with a god complex."
"Milley was concerned about the former president staging a coup against the United States."
"One of the most vital functions of Special Forces is to be a teacher."

### AI summary (High error rate! Edit errors on video page)

Exploring former President Donald Trump's response to reports about General Milley's concerns.
Trump's statement mocked for resembling a transcript of a not-so-smart criminal in an interrogation room on TV.
Trump denies discussing a coup and claims the election was his form of coup.
Trump criticizes General Mark Milley, citing conflicts with General James Mattis and Obama.
Former President Trump acts authoritarian, believing he knows more than experts, and criticizes Milley's actions.
Trump attempts to sow dissension between generals, particularly targeting Milley.
Contrasting leadership styles between Mattis and Milley likely led to conflicts.
Milley's background in Special Forces indicates more than just being a tough guy - he's also a weaponized educator.
Milley understands the dynamics of a coup and was reportedly concerned about Trump staging one.
It's vital to recognize the seriousness of Milley's concerns and actions against a potential coup.

Actions:

for politically conscious individuals,
Contact local representatives to voice concerns about potential political crises (implied)
Support efforts to uphold democratic principles in government (implied)
</details>
<details>
<summary>
2021-07-15: Let's talk about Trump vs the generals.... (<a href="https://youtube.com/watch?v=Dym0UN2UB1M">watch</a> || <a href="/videos/2021/07/15/Lets_talk_about_Trump_vs_the_generals">transcript &amp; editable summary</a>)

Beau details the close call the U.S. faced with potential authoritarianism, urging vigilance to prevent similar threats in the future.

</summary>

"That's how close we came."
"It's not over."
"You can say it can't happen here, but the reality is it almost did."
"Have a good day."

### AI summary (High error rate! Edit errors on video page)

Recalls a friend asking if Trump will try to stay in power, leading to jokes until January 6th.
Friend apologizes after reading about events in the Trump administration from a book.
Details generals at the highest level discussing how to prevent a Reichstag moment.
These private conversations reveal how close the U.S. came to a serious threat.
Emphasizes the gravity of the situation, with generals not making political statements publicly.
Mentions a key figure, indicated by a "funny little green hat," leading most of the concerning talks.
Points out the major influence this person holds over a political party in the U.S.
Stresses that the outcome of January 6th could have altered the course of future elections significantly.
Expresses hope for others to realize the seriousness of past events and prevent similar occurrences.
Concludes with a reminder that despite believing such events cannot happen, they nearly did.

Actions:

for citizens, voters, activists,
Spread awareness about the risks of authoritarianism in politics (implied).
Engage in critical thinking and analysis of political events to prevent similar threats (implied).
</details>
<details>
<summary>
2021-07-14: Let's talk about the separation of church and state.... (<a href="https://youtube.com/watch?v=9uyYh4I0Hp4">watch</a> || <a href="/videos/2021/07/14/Lets_talk_about_the_separation_of_church_and_state">transcript &amp; editable summary</a>)

Beau addresses misconceptions, dismisses fear-mongering about Biden seizing Bibles, and advocates for actions based on Christian morals, leaning towards principles that seem more in line with Democrats.

</summary>

"I'm pretty sure the only person that can destroy your faith is you."
"Because without the Bible, the church doesn't exist, right?"
"When you boil them down, they're kind of all the same. They just say be a good person."
"You going to lose a prop?"
"I'm demon-crats."

### AI summary (High error rate! Edit errors on video page)

Addresses assumptions about himself and why he remains vague about certain topics.
Responds to a message accusing him of dismissing concerns about Biden seizing Bibles.
Explains why he doesn't believe the fear-mongering scenario of the government taking Bibles is valid.
Points out that faith and church are not dependent on physical Bibles.
Talks about the separation of church and state and how it doesn't mean excluding Judeo-Christian principles from government.
Suggests actions based on Christian morals like feeding the hungry and implementing universal healthcare.
Expresses his belief that many Americans, regardless of religion, support these principles and morals.
Notes the potential resistance to these principles from those who claim to value them the most.
Stresses the importance of focusing on teachings rather than symbols like a profile picture.
Clarifies that he is not affiliated with any political party but leans towards principles and teachings that seem to be more in line with Democrats.

Actions:

for socially conscious individuals,
Support feeding the hungry and expanding SNAP (exemplified)
Advocate for universal healthcare to heal the sick (exemplified)
Be kind to immigrants and ease up on immigration laws (exemplified)
Focus on teachings rather than symbols like a profile picture (exemplified)
</details>
<details>
<summary>
2021-07-14: Let's talk about the foreign policy poker game.... (<a href="https://youtube.com/watch?v=76hMRb3-r_A">watch</a> || <a href="/videos/2021/07/14/Lets_talk_about_the_foreign_policy_poker_game">transcript &amp; editable summary</a>)

Beau clarifies that being anti-war is right morally but irrelevant in foreign policy, urging prevention rather than intervention to avoid chaos.

</summary>

"Being against war, being against imperialism, military adventurism, those are the right stances."
"Foreign policy is about power. It's not the way it should be, but it's the way it is."
"The reality is, yeah, it's probably going to get real bad over there when the U.S. finally pulls out."
"You want to be effective at being anti-war, you got to stop them before they start."
"Doesn't mean that you're wrong. You are without a doubt 100% correct."

### AI summary (High error rate! Edit errors on video page)

A viewer shared a story of a heated argument with his dad over Trump's decision to leave Afghanistan.
The viewer initially disagreed but later found Beau's video echoing his dad's opinion.
Beau clarifies that being anti-war and anti-imperialism is morally right but irrelevant in foreign policy.
Foreign policy is about power, not morality, ethics, or ideology.
He compares foreign policy to an international poker game where everyone cheats, and every move impacts the next.
Beau explains that foreign policy changes rapidly, rendering old slogans ineffective quickly.
The U.S.'s decision to leave Afghanistan may lead to severe consequences for civilians due to the power shift.
Trump's mistake was setting a hard date for leaving, prompting opposition to wait until the U.S. exits.
Beau commends the opposition's strategic restraint and knowledge despite common misconceptions.
The absence of a regional security force post-U.S. withdrawal could escalate conflict.
The failure to establish a security force might result in increased casualties after the U.S. leaves.
Being anti-war is still valid, but Beau stresses the importance of preventing conflicts before they start.
The aftermath of U.S. interventions often leaves chaos, reinforcing the need to prevent involvement.

Actions:

for policymakers, activists, citizens,
Prevent conflicts before they escalate by advocating for diplomatic solutions (implied).
Support organizations working towards peacebuilding and conflict prevention (implied).
</details>
<details>
<summary>
2021-07-13: The Roads with Alexandra Hunt, candidate for US House.... (<a href="https://youtube.com/watch?v=vRvrmbHXf_8">watch</a> || <a href="/videos/2021/07/13/The_Roads_with_Alexandra_Hunt_candidate_for_US_House">transcript &amp; editable summary</a>)

Beau talks with Alexandra Hunt about her journey from health care to Congress candidacy, focusing on progressive platforms like harm reduction, restorative justice, and grassroots campaign ethics.

</summary>

"Harm reduction is decriminalizing drug use, the same way that I have a platform to decriminalize sex work."
"Justice, to me, is not putting someone away in prison. Justice, to me, is a person understanding the harm that they have done."
"Every person should be housed and be able to stay in their family home without threat of violence or new construction pushing them out."
"The money that comes earlier allows you to play out your field game. And so it's hard."
"My big game plan was just to have as much courage as I could and just be OK with being the only person in a room for a bit."

### AI summary (High error rate! Edit errors on video page)

Introduces the guest, Alexandra Hunt, who is running for Congress in PA3, a Democratic district in Philadelphia.
Alexandra shares her background in health care and public health, her motivation for running for Congress, and her challenging incumbent who does not support progressive policies.
Alexandra talks about her journey through college, working as a stripper and server to pay bills, and obtaining master's degrees while working full time.
The importance of harm reduction in criminal justice reform and decriminalizing drug use and sex work.
Restorative justice, based on Alexandra's personal experience as a survivor of sexual assault, focuses on accountability and understanding harm.
Alexandra's platform includes justice for tribal nations, housing rights, gentrification, economic justice, wealth tax, environmental justice, and ending food insecurity.
Her commitment to representing the will of the people through town halls, accessibility, and engagement with the community.
The tipping point for Alexandra's decision to run for office was witnessing the government's failure during the pandemic and the lack of support for vulnerable communities.
Alexandra's grassroots campaign approach to avoid corporate influence and stay true to ethical principles.
Soccer is a passion outside of politics that Alexandra expresses enthusiasm for.

Actions:

for voters in pa3 district,
Attend town halls and community events to voice concerns and needs (exemplified)
Support grassroots political campaigns financially or through volunteer work (exemplified)
</details>
<details>
<summary>
2021-07-13: Let's talk about Trump's buyer's remorse over Kavanaugh.... (<a href="https://youtube.com/watch?v=ve2FpqlvjCU">watch</a> || <a href="/videos/2021/07/13/Lets_talk_about_Trump_s_buyer_s_remorse_over_Kavanaugh">transcript &amp; editable summary</a>)

Former President Trump expresses buyer's remorse over Supreme Court Justice Kavanaugh, revealing his disappointment in Kavanaugh's decisions and the purpose behind lifetime appointments.

</summary>

"Who would have had him? Nobody, totally disgraced. Only I saved him."
"The fact that the president once again made a bad hire, the former president made a bad hire, hired somebody he regrets."
"That's why it exists."
"If we have learned anything from the former president, it's that he really needs an HR director."
"Have a good day."

### AI summary (High error rate! Edit errors on video page)

Former President Donald J. Trump expresses buyer's remorse regarding Supreme Court Justice Kavanaugh.
Trump saved Kavanaugh's life, suggesting he couldn't even get a job at a law firm without him.
Trump expresses disappointment in Kavanaugh's lack of courage and inability to make great decisions.
Trump is surprised by Kavanaugh not overturning the election, considering it a betrayal.
The lifetime appointment of Supreme Court justices prevents influence and pressure from those who nominated them.
Setting a mandatory retirement age might be a good idea, but the lifetime appointment serves a purpose.
Removing the ability for elected officials to pressure justices ensures brave decisions.
Despite Trump's regrets, Kavanaugh is likely to remain in his position for a long time.
The system of lifetime appointments stops elected officials from being able to remove justices easily.
Trump's pattern of making bad hires shows his need for a good HR director.

Actions:

for political observers,
Contact your representatives to advocate for reforms in the appointment process (suggested).
Join advocacy groups working towards setting a mandatory retirement age for Supreme Court justices (suggested).
</details>
<details>
<summary>
2021-07-13: Let's talk about Democrats walking out in Texas.... (<a href="https://youtube.com/watch?v=KVBR2vmJu1o">watch</a> || <a href="/videos/2021/07/13/Lets_talk_about_Democrats_walking_out_in_Texas">transcript &amp; editable summary</a>)

The Republican Party in Texas is pushing a voting restrictions bill, prompting Democratic legislators to flee the state in a procedural move akin to a filibuster, hoping to buy time for federal intervention.

</summary>

"It's a filibuster. It's a procedural trick to slow down legislation."
"This is an Alamo moment. They know they're not going to win."
"But while it is entertaining and there is lots of high drama, just remember there isn't a whole lot of difference other than travel expenses between this and the filibuster."

### AI summary (High error rate! Edit errors on video page)

The Republican Party in Texas is pushing forward a voting restrictions bill to make voting harder and more inconvenient, under the guise of election security.
Democratic politicians fled the state, denying Republicans the two-thirds majority needed to move forward with the bill.
The governor of Texas could send law enforcement to bring back the legislators who fled, but doing so might strengthen the Democrats' position.
Ultimately, the Democrats' actions in Texas amount to a filibuster, a procedural move to slow down legislation.
While the Democrats may not completely stop the bill, their goal might be to buy time for federal legislation to counteract state voting restrictions.
Beau compares the situation in Texas to a holding action, like the Alamo, acknowledging that the Democrats may not win but are aiming to aid federal efforts in passing voting rights legislation.

Actions:

for texans, voters, politically active,
Support and amplify efforts for federal voting rights legislation (implied)
Stay informed and engaged with political developments in Texas and nationally (implied)
</details>
<details>
<summary>
2021-07-12: Let's talk about what makes a good conservative.... (<a href="https://youtube.com/watch?v=Br3WxNbaU7g">watch</a> || <a href="/videos/2021/07/12/Lets_talk_about_what_makes_a_good_conservative">transcript &amp; editable summary</a>)

Addressing anti-conservative bias accusations, Beau defines a good conservative as cautiously progressive, ensuring humanity's continual forward movement.

</summary>

"Courage isn't the absence of fear. Courage is moving through the fear."
"A good conservative is somebody who is cautious, but still moving forward with the rest of humanity."
"Humanity is very biased towards liberals. It becomes more liberal as time progresses."
"The problem is when they stop. Just don't stop, because humanity's not going to."
"So it's not somebody who stops. It's certainly not somebody who wants to go backwards and try stuff again."

### AI summary (High error rate! Edit errors on video page)

Responding to a comment questioning his anti-conservative bias, Beau dives into what he believes makes a good conservative.
He differentiates between his objective coverage of events and his personal philosophy, which leans towards a society with fairness, freedom, and cooperation.
Beau believes his viewers, who are primarily progressive and anti-authoritarian, want a world where everyone can thrive without hierarchy.
Exploring the idea of a good conservative, Beau sees them as cautious individuals who still push for progress and increased freedom but at a slower pace.
He criticizes conservatives motivated by fear, who want to halt progress instead of moving through challenges with courage.
Beau defines a good conservative as someone who ensures progress continues without crashing into unprepared situations, acknowledging that such individuals are becoming rarer, especially in political office.

Actions:

for progressive viewers,
Engage in cautious but progressive actions in your community (implied)
</details>
<details>
<summary>
2021-07-12: Let's talk about politicians writing unconstitutional laws.... (<a href="https://youtube.com/watch?v=MdCrEy95wxs">watch</a> || <a href="/videos/2021/07/12/Lets_talk_about_politicians_writing_unconstitutional_laws">transcript &amp; editable summary</a>)

Beau questions the logic behind lawmakers writing unconstitutional legislation and advocates for consequences for those who do, urging voters to have access to information about such actions.

</summary>

"Why is it that a group of people who often use the Constitution as their backdrop keep writing legislation that is unconstitutional?"
"You can't pretend to be a patriot and uphold the basic principles of this country if you are actively trying to undermine them."
"If you write a bill that is found to be unconstitutional, you're barred from public office."
"The fact that we have so much legislation headed to the Supreme Court with the express purpose of testing the limits of those hard limits, trying to push them as far as they can go, that should be a warning sign."
"Maybe we should know the legislators who are out there who don't actually support the Constitution."

### AI summary (High error rate! Edit errors on video page)

Explains a law in Tennessee requiring businesses to post signs allowing individuals to use the bathroom corresponding to their identity.
Skips moral arguments and focuses on economic impact and constitutional issues.
ACLU challenges the law in court, and a federal judge issues an injunction deeming it likely unconstitutional.
Questions why groups using the Constitution as a backdrop keep writing unconstitutional legislation.
Argues that voters should have access to information about lawmakers who support unconstitutional laws.
Advocates for consequences for lawmakers writing bills found to be unconstitutional, such as being barred from public office.
Suggests that legislation constantly challenging constitutional limits should be a warning sign.
Emphasizes the importance of upholding the basic principles of the Constitution.

Actions:

for voters, constitution supporters,
Advocate for transparency in lawmakers' actions by supporting initiatives that track and make public information about unconstitutional legislation (suggested).
Stay informed about laws and bills being introduced in your area and hold lawmakers accountable for upholding constitutional principles (implied).
</details>
<details>
<summary>
2021-07-11: Let's talk about me being "giddy like a schoolgirl" over corporate tokens.... (<a href="https://youtube.com/watch?v=80LnRJ767VM">watch</a> || <a href="/videos/2021/07/11/Lets_talk_about_me_being_giddy_like_a_schoolgirl_over_corporate_tokens">transcript &amp; editable summary</a>)

Beau explains how token gestures of progressiveness in media combat fear-mongering and pave the way for change by fostering familiarity and reducing resistance.

</summary>

"Every time this happens, there's an orchestrated outrage. Right? Why are they doing this? Because they know how effective it is."
"Familiarity, because it's everywhere. You walk into a gas station to buy a Coke, there is an advertisement for Coke on the cooler."
"Their motive as to why they're doing it doesn't mean that it's not effective."
"The tide is turning. It is more profitable to do this than to not."
"And that exposure, well, it breeds familiarity. Makes them less afraid, which means they will mount less resistance to change when it comes."

### AI summary (High error rate! Edit errors on video page)

Explains his excitement when major companies show token gestures of progressiveness by changing the race or gender of characters in media.
Compares this strategy to record companies paying radio stations to play their music for familiarity.
Argues that conservatives fear these changes because they effectively combat fear-mongering and "othering."
Notes that companies implement these changes for profit, not social change, similar to McDonald's and Coke.
Emphasizes that exposure breeds familiarity and reduces fear of the unknown.
States that conservatives use fear to motivate their base and uphold the current system by keeping people separate.
Believes that familiarity with different ideas and demographics leads to less resistance to change.
Sees these token gestures as effective in gradually shifting perspectives and making change less scary.
Expresses optimism about these gestures indicating a shift in the market towards inclusivity and progressiveness.
Concludes by suggesting that exposure and familiarity through media can lead to greater acceptance and less resistance to change.

Actions:

for media consumers, progressives,
Support media that showcases diversity and inclusivity (implied)
Advocate for gradual exposure to different ideas and demographics through media (implied)
</details>
<details>
<summary>
2021-07-11: Let's talk about Republican public health strategies.... (<a href="https://youtube.com/watch?v=JeHRTT_RfTU">watch</a> || <a href="/videos/2021/07/11/Lets_talk_about_Republican_public_health_strategies">transcript &amp; editable summary</a>)

Beau explains Republican politics on public health statements, addressing fear-mongering and cold motives, expressing concern over conservative friends falling prey to anti-vaccine rhetoric.

</summary>

"The paranoia and fear that has been stoked within the Republican Party is unbelievable."
"It's got to make up for the loss of voters that they're going to have."
"You have to be pretty far gone to willingly put yourself at risk to own the libs."
"I'm sure you know some who have fallen prey to this."
"I wish I had advice for you, because those cheers and the rhetoric that's being used, I don't know how to overcome it."

### AI summary (High error rate! Edit errors on video page)

Explains Republican politics on public health statements, focusing on recent events.
Mentions Cawthorn's claim about Biden's door-to-door vaccine machinery being used for gun and Bible confiscation.
Addresses the paranoia and fear within the Republican Party.
Debunks the feasibility of door-to-door gun confiscation.
Talks about the CPAC convention where not meeting vaccination goals was cheered.
Analyzes the cold and calculating motives of some Republican leaders.
Expresses concern over conservative friends falling prey to anti-vaccine rhetoric.
Condemns risking health to "own the libs."

Actions:

for conservative friends,
Reach out to conservative friends and provide them with accurate information about vaccines (implied).
</details>
<details>
<summary>
2021-07-10: Let's talk about that statue coming down and defending history.... (<a href="https://youtube.com/watch?v=fwNzq6BIng4">watch</a> || <a href="/videos/2021/07/10/Lets_talk_about_that_statue_coming_down_and_defending_history">transcript &amp; editable summary</a>)

Beau has been waiting to address the removal of a statue in Charlottesville, shedding light on its history and the importance of understanding the messages behind symbols.

</summary>

"Statues are not really history. It's art. Sometimes art's good, sometimes it's bad. But all art is political and it conveys a message."
"Be certain you are aware of the ideas contained in that symbol."
"When you're talking about ideas that old and that wrong, maybe it's best to let them go."
"When somebody has emotionally manipulated you into defending a symbol, be certain you know what that symbol represents."
"I think it's wiser not to keep open the sores of war."

### AI summary (High error rate! Edit errors on video page)

Beau has been waiting to talk about a specific statue in Charlottesville since 2017, which has now been removed.
The statue of Robert E. Lee was defended in the past under the guise of defending history, but its true history isn't accurately represented by that defense.
The statue was commissioned around the same time as the film "Birth of a Nation," which glorified the Ku Klux Klan.
The motivations behind the statue's commissioning aren't entirely clear, but it was part of a broader context that included segregation and oppression.
Washington Park, another project by the same commissioner, was also intertwined with racial segregation.
The statue symbolized more than just the Civil War; it had ties to celebrating slavery and segregation.
Statues are not just historical artifacts; they are political statements that convey specific messages.
Defending a symbol without fully understanding its implications may mean upholding problematic ideas and values.
Beau suggests that it's sometimes better to let go of symbols that represent old and harmful ideas rather than keeping them alive.
Beau quotes Robert E. Lee, who himself believed Confederate monuments should not exist.

Actions:

for history enthusiasts, activists,
Research the history and context behind statues and monuments in your community (implied)
Engage in dialogues about the implications of symbols and their historical significance (implied)
Advocate for the removal of statues that glorify oppressive histories (implied)
</details>
<details>
<summary>
2021-07-10: Let's talk about Boebert and Republicans suggesting CRT is true.... (<a href="https://youtube.com/watch?v=f9RrZ56O1Pg">watch</a> || <a href="/videos/2021/07/10/Lets_talk_about_Boebert_and_Republicans_suggesting_CRT_is_true">transcript &amp; editable summary</a>)

The focus should be on substantial issues like Republicans suggesting CRT is true, not on personal educational backgrounds, as education level doesn't always correlate with intelligence.

</summary>

"Equating intelligence or education level to credentialing isn't a good idea."
"Educational credentials and intelligence or how smart you are, they don't go together."
"Alienating rural Americans and probably other demographics."
"Education level doesn't always correlate with intelligence or capability."
"It's more effective to focus on substantial issues rather than personal educational backgrounds."

### AI summary (High error rate! Edit errors on video page)

The Zen Education Project's pledge for teachers caused a stir, misconstrued by conservative outlets as CRT support.
Representative Lauren Boebert suggested firing 5,000 teachers over the pledge, sparking backlash.
Boebert's educational background being a high school dropout was criticized by Democrats.
Equating intelligence or education level to credentials is not a good idea, as intelligence varies widely.
The focus should be on Republicans suggesting CRT is true, not on Boebert's educational history.
Calling for firings over free speech rights may be a First Amendment issue.
The pledge was about refusing to lie to students about US history, not specifically about teaching CRT.
Alienating rural Americans and other demographics with elitist attacks based on education level is a concern.
Education level doesn't always correlate with intelligence or capability.
It's more effective to focus on substantial issues rather than personal educational backgrounds.

Actions:

for educators, politicians, activists,
Refuse to lie to young people about US history and current events, regardless of the law (suggested).
Be cautious of equating educational credentials with intelligence (implied).
</details>
<details>
<summary>
2021-07-09: Let's talk about questions from the UK about dirt roads and politics.... (<a href="https://youtube.com/watch?v=tPCoFAwa4Fc">watch</a> || <a href="/videos/2021/07/09/Lets_talk_about_questions_from_the_UK_about_dirt_roads_and_politics">transcript &amp; editable summary</a>)

Beau explains the U.S. fascination with dirt roads, pickup trucks, and the political identity confusion between rural and suburban areas, shedding light on voting trends and stereotypes.

</summary>

"Welcome to the US."
"Welcome to the United States."
"The people who you would think drive the giant truck stopped."

### AI summary (High error rate! Edit errors on video page)

Explains the fascination with dirt roads and pickup trucks in the United States.
Addresses the misconception that dirt roads are just a movie trope or a political statement.
Notes that a significant percentage of Americans live on dirt roads, especially in rural areas.
Points out that fixing infrastructure won't end the U.S. fascination with SUVs and large trucks.
Describes who typically drives SUVs and large trucks, debunking stereotypes.
Talks about the identity influence and stereotype adoption in the U.S.
Mentions the confusion around the term "dirt road Democrat" and the political leanings of rural vs. suburban areas.
Explains why economically left-leaning rural areas often vote Republican due to social conservative issues.
Reveals that many Americans vote based on perceived identity rather than actual beliefs.
Suggests that reaching out to real rural Americans could benefit the Democratic Party economically.
Comments on the elitism within the left-leaning party and the disconnect with rural Americans.
Wraps up by acknowledging the confusing nature of U.S. politics from an outside perspective.

Actions:

for american voters,
Reach out to real rural Americans to understand their economic concerns (suggested)
Challenge stereotypes and misconceptions surrounding rural areas and their political leanings (implied)
</details>
<details>
<summary>
2021-07-09: Let's talk about how we're interviewing people.... (<a href="https://youtube.com/watch?v=mjmh3hQ0PHM">watch</a> || <a href="/videos/2021/07/09/Lets_talk_about_how_we_re_interviewing_people">transcript &amp; editable summary</a>)

Beau introduces two interview series on his channel—one featuring political candidates and the other interesting people—to inform viewers without engaging in debates or sensationalism, focusing on letting guests share their views and projects.

</summary>

"The goal is to inform those people who choose to vote in 2022."
"It's more about getting information out."
"They'll be pretty friendly chats."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of interviews and explains the two series on his channel, one featuring interesting people and the other political candidates running in 2022.
The purpose of the political candidate interviews is to allow them to talk about their platform and policies, informing viewers who may vote in 2022.
Beau aims to determine if candidates genuinely believe in what they are saying or if they are just following instructions.
He does not plan to push back on candidates' positions unless something objectively false is stated, focusing on letting candidates speak and viewers access unfiltered information.
Beau uses the example of an interview with Erica Smith, a U.S. Senate candidate from North Carolina, to illustrate the approach of allowing candidates to present their platforms without engaging in debates or trying to change their positions.
In interviews with interesting people like Chelsea Manning, Beau focuses on topics beyond what people might expect, aiming to show different aspects of their personalities beyond the usual questions.
The goal of both types of interviews is to provide information and better inform viewers without getting into confrontational or sensationalist questioning.
Beau's approach to interviews is about allowing guests to share their thoughts and projects rather than creating conflict or controversy.

Actions:

for content creators, viewers,
Watch and share Beau's interview videos to better understand political candidates' platforms and learn about interesting people (implied).
</details>
<details>
<summary>
2021-07-08: The roads with Erica D. Smith candidate for Senate..... (<a href="https://youtube.com/watch?v=HPPIlnZtCvU">watch</a> || <a href="/videos/2021/07/08/The_roads_with_Erica_D_Smith_candidate_for_Senate">transcript &amp; editable summary</a>)

Former state Senator Erica Smith outlines her trajectory from engineer to public servant, advocating for income equality, healthcare access, environmental justice, and more in her run for the US Senate.

</summary>

"We're running a movement to create real structural change so that our government works for all of us, not just the wealthy, not just the well-connected, but all of us."
"Policy is personal for me, Bo. That's why I fight so hard."
"Healthcare is a basic human right. No one should have to ration their insulin just to put food on the table."
"Common sense protections for our community."
"We are truly one of us for all of us."

### AI summary (High error rate! Edit errors on video page)

Beau interviews former state Senator Erica Smith, a candidate for Senate in North Carolina, to learn about her background and platform.
Erica describes her upbringing on a family farm in North Carolina and her trajectory from engineer to public school educator to state senator.
She shares her experiences in Uganda on a mission trip and the impact it had on her perspective on community building and service.
Erica outlines her priorities if elected to the US Senate, focusing on income inequality, rural healthcare, and environmental justice through the Green New Deal.
She advocates for universal broadband access, Medicare for all, and addressing the challenges faced by rural hospitals.
Erica stresses the importance of canceling student loan debt, raising the minimum wage, and supporting the PRO Act for workers' rights.
She details her stance on gun control, including banning assault rifles and implementing common sense policies to prevent gun violence.
Erica also addresses the pre-funding mandate for the US Postal Service and proposes postal banking as a solution.
She concludes by inviting viewers to learn more about her platform and support her campaign.

Actions:

for voters in north carolina,
Support Erica Smith's campaign by visiting www.ericaforus.com (implied)
Learn more about her platform and policy proposals (implied)
Invest in politicians who champion causes that support working families (implied)
</details>
<details>
<summary>
2021-07-08: Let's talk about being happy and Biden's door-to-door salespeople.... (<a href="https://youtube.com/watch?v=TxIzZA5oaGU">watch</a> || <a href="/videos/2021/07/08/Lets_talk_about_being_happy_and_Biden_s_door-to-door_salespeople">transcript &amp; editable summary</a>)

Beau challenges harmful rhetoric on political differences, rejects authoritarian tactics, and advocates for peace and critical thinking in the face of escalating tensions.

</summary>

"Nobody wants to go to work not like that."
"That rhetoric needs to stop."
"Because eventually it's going to go too far."

### AI summary (High error rate! Edit errors on video page)

Blue collar workers aren't happy going to work, even if they take pride in their profession and accomplishments.
Rejects the notion of being happy to harm others based on political differences or engaging in civil war.
Disagrees with the idea of Democrats forcing experimental gene therapy through door-to-door efforts.
Believes differing political opinions do not make someone evil, but rather misguided, misinformed, or propagandized.
Criticizes those who profit from pushing civil war rhetoric and inciting violence while staying safe themselves.
Expresses a desire for peace, staying where he is, enjoying simple pleasures like being known for planting pumpkins.
Distinguishes between door-to-door salesmen and authoritarians who use violence to force compliance.
Urges people to question misinformation and propaganda surrounding vaccines and door-to-door initiatives.
Encourages speaking to veterans about the realities of conflict and the lack of happiness in such situations.
Warns against escalating dangerous rhetoric that could lead to real harm, with instigators remaining safe and profiting.

Actions:

for critical thinkers,
Talk to veterans about the realities of conflict (suggested)
</details>
<details>
<summary>
2021-07-07: Let's talk about San Jose's new gun control initiative.... (<a href="https://youtube.com/watch?v=-zzt-iSMcyE">watch</a> || <a href="/videos/2021/07/07/Lets_talk_about_San_Jose_s_new_gun_control_initiative">transcript &amp; editable summary</a>)

San Jose's firearm ownership fee initiative sheds light on the potential racial impacts of colorblind laws and the importance of addressing systemic issues for a fair society.

</summary>

"Systemic issues like institutionalized racism and inherited disadvantages could make it harder for poor individuals to afford firearm ownership."
"Laws like this can have unintended racial consequences and may be selectively enforced."
"Acknowledging the intersection of race with the law is vital for building a fair society."
"Building a country where everyone gets a fair chance requires confronting and addressing systemic issues."
"Ignoring racial issues may indicate a desire to maintain existing discriminatory systems."

### AI summary (High error rate! Edit errors on video page)

San Jose is planning to impose a fee on firearm ownership, but legal challenges are expected due to charging money to deter a constitutional right.
The initiative aims to deter gun ownership and offset violence costs, but the impact may disproportionately affect certain demographics, particularly the poor.
Systemic issues like institutionalized racism and inherited disadvantages could make it harder for poor individuals to afford firearm ownership, leading to violations of the law.
The law is colorblind on the surface, but its enforcement could have racist impacts due to existing conditions and systemic issues in the country.
Beau suggests that laws like this can have unintended racial consequences and may be selectively enforced, showing how colorblind laws can lead to discriminatory outcomes.
Acknowledging the intersection of race with the law is vital for building a fair society, whether through evidence-based arguments or storytelling narratives.
Building a country where everyone gets a fair chance requires confronting and addressing systemic issues and acknowledging the truth, whether through data or personal stories.
Beau challenges the reluctance to openly address racial issues and suggests that ignoring these issues may indicate a desire to maintain existing discriminatory systems.
Encourages critical thinking about how race intersects with the law and the importance of discussing theories that illuminate racial disparities.

Actions:

for policy advocates, activists,
Address systemic issues in your community through advocacy and support for marginalized groups (exemplified)
Educate others on the impact of colorblind laws on different demographics (exemplified)
</details>
<details>
<summary>
2021-07-07: Let's talk about Chip Roy's 18 months of nothing getting done.... (<a href="https://youtube.com/watch?v=VO4Aj-bZrMs">watch</a> || <a href="/videos/2021/07/07/Lets_talk_about_Chip_Roy_s_18_months_of_nothing_getting_done">transcript &amp; editable summary</a>)

Beau criticizes Chip Roy's political gamesmanship in obstructing progress for 18 months, exposing the real-life consequences of delaying critical infrastructure improvements.

</summary>

"He's saying the good people of Bandera and Comfort, well they're just too slow-witted to figure out that we're obstructing it because they know the Democrats are in power."
"Want your neighbors to suffer so you can get a boost at the polls. That is wild."
"I think we need a lot of these politicians to be as comfortable as they are, to believe that their constituents are that ignorant."

### AI summary (High error rate! Edit errors on video page)

Beau criticizes Chip Roy's statement on obstructing progress for political gain.
Chip Roy expressed satisfaction in causing chaos and hindering progress for 18 months.
The infrastructure bill is being delayed due to political gamesmanship.
Beau points out the real-life consequences of delaying infrastructure improvements.
Chip Roy is banking on the ignorance of his constituents to support his strategy.
Beau advocates for bipartisan cooperation to address critical infrastructure issues.
The current compromise on the infrastructure bill is deemed insufficient by Beau.
Beau stresses the urgent need to address the neglected infrastructure problems in the US.
Politicians' neglect has led to major infrastructure challenges facing the country.
Beau questions the ethics of prioritizing political gain over addressing national issues.

Actions:

for politically conscious individuals,
Contact your representatives to prioritize bipartisan cooperation for addressing infrastructure issues (implied).
Join local advocacy groups pushing for comprehensive infrastructure solutions (implied).
</details>
<details>
<summary>
2021-07-06: Let's talk about sympathy for Republicans.... (<a href="https://youtube.com/watch?v=ElDWx21-M-c">watch</a> || <a href="/videos/2021/07/06/Lets_talk_about_sympathy_for_Republicans">transcript &amp; editable summary</a>)

Beau expresses sympathy for Republican politicians but warns them to unite against Trump's authoritarianism before it's too late.

</summary>

"You're not representing the people you're sent there to represent."
"Authoritarians don't care about loyalty. They care about obedience."
"Recognize what he is and what he's going to do to your party and to this country."
"He will pick you off one by one and replace you with the obedient."

### AI summary (High error rate! Edit errors on video page)

Expresses sympathy for Republicans, specifically politicians, for being in a difficult situation.
Criticizes Republican politicians for blindly following Trump, even though he doesn't support representative democracy.
Points out that politicians are motivated by money and power, which are now controlled by Trump.
Warns politicians that their loyalty to Trump won't protect them in the end.
Urges Republicans to unite against Trump to protect their party and country.
Emphasizes the consequences of not standing up against authoritarianism.

Actions:

for politically aware individuals,
Unite against authoritarianism to protect democracy (suggested)
Recognize the true motives of politicians and hold them accountable (implied)
</details>
<details>
<summary>
2021-07-06: Let's talk about DARPA, ARPA-H, and Republican objections.... (<a href="https://youtube.com/watch?v=gT7HXaDJnAs">watch</a> || <a href="/videos/2021/07/06/Lets_talk_about_DARPA_ARPA-H_and_Republican_objections">transcript &amp; editable summary</a>)

Beau explains the need for replicating DARPA's culture in the new health agency to drive innovation and save lives effectively.

</summary>

"If they want the health version to have the impacts that DARPA does, it's got to have that culture."
"It has to have the leaders that understand what the goal is."
"If they can duplicate the fluidity, the dynamic nature of DARPA, I have no doubt this agency will create amazing breakthroughs and save lives."

### AI summary (High error rate! Edit errors on video page)

Explains the Advanced Research Projects Agency for Health, Biden's new agency for advanced health research.
Republicans have concerns that if it's rolled into NIH, it will adopt NIH's culture and undermine its purpose.
Draws parallels between the new health agency and DARPA, the Defense Advanced Research Projects Agency, known for groundbreaking advancements.
DARPA's unique organizational culture and operational methods lead to significant technological breakthroughs.
DARPA operates with a small team of 200 employees, offering leaders in the field autonomy, urgency, and a fixed timeline to solve problems.
The agency looks at long-term solutions, fostering incidental developments while focusing on major challenges.
DARPA's approach includes funding external projects and maintaining a nimble, unorthodox behavior uncommon in government agencies.
Beau stresses the importance of replicating DARPA's culture and dynamic nature for the new health agency to achieve impactful breakthroughs.
Emphasizes the need for autonomy, different culture, and visionary leadership to ensure the new agency's success in saving lives.
Urges for the replication of DARPA's fluidity and dynamic approach in the new health agency to drive innovation and positive outcomes.

Actions:

for health policymakers and researchers.,
Ensure that the new health agency maintains autonomy and a distinct culture (implied).
Advocate for visionary leadership that understands the agency's goals (implied).
Support efforts to replicate DARPA's dynamic and fluid operational approach in the new health agency (implied).
</details>
<details>
<summary>
2021-07-05: Let's talk about the debate over ending the filibuster.... (<a href="https://youtube.com/watch?v=J6QRjslRy9c">watch</a> || <a href="/videos/2021/07/05/Lets_talk_about_the_debate_over_ending_the_filibuster">transcript &amp; editable summary</a>)

Beau presents both sides of the filibuster debate, weighing its impact on legislation and political strategies.

</summary>

"It's politics. It's trying to make the right play."
"Even those who normally might show up in the midterms, maybe they don't, because they don't see the benefit."
"There isn't a guaranteed right answer here."
"If the Democratic Party doesn't move some pretty substantial legislation forward pretty quickly, they're going to have issues in the midterms."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Exploring the debate around ending the filibuster and its long-term impacts.
The filibuster is a Senate rule that makes passing legislation harder by requiring more than a simple majority.
McConnell uses the filibuster to obstruct the Democratic Party's agenda, despite them being in power.
Some want to eliminate the filibuster, while others fear it could backfire during Republican control.
Concerns about midterm losses for the President's party and potential legislative advantages for the opposition.
Beau suggests that midterm losses are due to a team mentality and lack of participation rather than filibuster concerns.
The legacy of Trump's executive orders could easily be undone, unlike lasting legislation.
Democrats face challenges delivering on promises due to Senate hurdles created by the filibuster.
Removing the filibuster could enable the Democratic Party to pass ambitious legislation and secure wins.
Beau acknowledges the gamble in deciding whether to keep or eliminate the filibuster.

Actions:

for politically engaged individuals,
Educate yourself on the filibuster debate and its implications (implied)
Stay informed about current political developments and how they may affect you (implied)
</details>
<details>
<summary>
2021-07-05: Let's talk about DOD mandated vaccines.... (<a href="https://youtube.com/watch?v=OIP_lio-bBA">watch</a> || <a href="/videos/2021/07/05/Lets_talk_about_DOD_mandated_vaccines">transcript &amp; editable summary</a>)

Beau explains why the bill prohibiting mandatory vaccines for military personnel is impractical, focusing on hierarchy, readiness, and potential impacts on operations.

</summary>

"Morality and war-making capabilities really don't blend well."
"The only legitimate reason to support the bill is the bodily autonomy argument, but that's literally it out because it's the military."
"The US military has a long tradition of ignoring politicians who think they know better, who refuse to embrace science, and who try to degrade military readiness."

### AI summary (High error rate! Edit errors on video page)

Representative Massey and other Republicans introduced a bill to prohibit the US Department of Defense from requiring personnel to get the vaccine.
The argument that troops should have bodily autonomy in decision-making doesn't hold as the military operates under strict hierarchy with no room for personal choice.
Morality doesn't play a significant role in the military; readiness is the paramount concern.
Unvaccinated troops could significantly impact readiness in various operations, including unconventional conflicts, humanitarian missions, and facing near peers.
Biden is unlikely to sign such a bill, and even if passed, the military tradition suggests they may ignore it for the sake of readiness.

Actions:

for military personnel, policymakers.,
Contact military representatives to advocate for prioritizing readiness over political stunts (implied).
Support efforts to ensure military personnel are adequately vaccinated to maintain operational readiness (implied).
</details>
<details>
<summary>
2021-07-04: Let's talk about the New York Times documentary about the 6th.... (<a href="https://youtube.com/watch?v=vqt7oRzn2X8">watch</a> || <a href="/videos/2021/07/04/Lets_talk_about_the_New_York_Times_documentary_about_the_6th">transcript &amp; editable summary</a>)

New York Times documentary provides context on Capitol riot while warning of potential future incidents due to baseless claims and rhetoric, urging action to break the cycle.

</summary>

"They're putting their careers above their party, above their country, and at this point above the safety of their constituents."
"If that doesn't happen, people will still believe it. And this cycle will continue."

### AI summary (High error rate! Edit errors on video page)

New York Times documentary details events of January 6th Capitol riot from start to finish, providing valuable context.
The documentary, "A Day of Rage, How a Mob Stormed the Capitol," shows the layout of the Capitol and what led to the notable events.
Department of Homeland Security warned of more incidents in August due to certain rhetoric and baseless theories.
People in office and out continue to echo baseless claims and downplay the events of January 6th.
Some in Washington are trying to pretend the Capitol riot wasn't a big deal to protect their political careers.
Continuing to push baseless claims may lead to more events, damaging the Republican Party and risking safety.
Officials seem more focused on their political careers than on addressing the underlying issues.
The longer baseless claims go unchallenged, the more damaging it is to the Republican Party.
Addressing the baseless claims is necessary to prevent the cycle from continuing.
Someone in the Republican Party needs to have the courage to denounce the lies and address the situation.

Actions:

for politically engaged individuals,
Challenge baseless claims and rhetoric (suggested)
Call for accountability within the Republican Party (suggested)
</details>
<details>
<summary>
2021-07-03: Let's talk about other channels not doing something.... (<a href="https://youtube.com/watch?v=hEdOxn0G0BQ">watch</a> || <a href="/videos/2021/07/03/Lets_talk_about_other_channels_not_doing_something">transcript &amp; editable summary</a>)

Beau addresses the lack of mutual aid engagement in certain YouTube channels, urging collaboration for the greater good.

</summary>

"I'm willing to bet almost all of the channels will. You just got to ask. Reach out to them."
"You do not have the luxury of being ideologically pure. You need the resources and those channels can help you get them."
"If you're concerned about it, you probably have that network. So you could team up."

### AI summary (High error rate! Edit errors on video page)

Addresses a topic he usually avoids discussing: other people on YouTube and their engagement in charity and mutual aid.
Expresses his reluctance to join such dialogues due to viewing it as a waste of resources, but acknowledges the necessity this time.
Talks about channels on YouTube that are left-leaning or anti-authoritarian but do not participate in charity or mutual aid.
Mentions being sent screenshots of other channels engaging in these activities, which was meant to make him "look good" but didn't.
Points out a commonality among channels engaging in mutual aid: many were journalists or organizers before YouTube.
Emphasizes the importance of having a network and team for effective mutual aid efforts.
Suggests reaching out to channels not engaged in mutual aid to offer assistance or collaboration.
Acknowledges that some may be engaging in mutual aid for self-serving reasons, but underscores the importance of the end goal of helping those in need.
Urges not to let ideological differences hinder collaboration for the greater good.
Encourages utilizing resources from different channels for mutual aid efforts, regardless of their motivations.

Actions:

for youtube content creators,
Reach out to YouTube channels not engaged in mutual aid to offer collaboration (suggested)
Team up with channels for mutual aid efforts (implied)
</details>
<details>
<summary>
2021-07-03: Let's talk about heat waves and fireworks.... (<a href="https://youtube.com/watch?v=vAsRg7_z7ds">watch</a> || <a href="/videos/2021/07/03/Lets_talk_about_heat_waves_and_fireworks">transcript &amp; editable summary</a>)

Beau shares vital tips on staying safe during a drought, managing extreme heat, and checking on vulnerable individuals in a public service announcement.

</summary>

"Water. Water. First and foremost, water."
"Your body is going to be a whole lot more taxed by that additional heat."
"It's not a joke."
"The heat, it's a literal killer."
"It literally might save a life."

### AI summary (High error rate! Edit errors on video page)

Beau addresses two public service announcements related to the ongoing drought and extreme heat.
He clarifies that he is not a former firefighter and shares a firefighter's message about avoiding fireworks during a drought.
Beau urges viewers to drink plenty of water to stay hydrated in the heat, stressing the importance of water over other beverages.
He advises against strenuous activities in hot weather, as the body can struggle with the additional heat.
Beau suggests using a kiddie pool or seeking air-conditioned spaces to cool off during power outages after hurricanes.
Encourages checking on vulnerable individuals, especially the elderly, who may struggle in extreme heat.

Actions:

for community members in drought-stricken areas,
Drink plenty of water constantly (implied)
Encourage others to avoid using fireworks in drought-stricken areas (implied)
Avoid strenuous activities in extreme heat (implied)
Provide assistance to vulnerable individuals, especially the elderly, during hot weather (implied)
</details>
<details>
<summary>
2021-07-02: Let's talk about the latest Purge movie.... (<a href="https://youtube.com/watch?v=BtnMG4FlqbE">watch</a> || <a href="/videos/2021/07/02/Lets_talk_about_the_latest_Purge_movie">transcript &amp; editable summary</a>)

Beau talks about the political nature of "The Purge" franchise, pointing out its intentional sociopolitical commentary intertwined with horror elements, and how all art carries political undertones.

</summary>

"All art is political in some way."
"The entire franchise is political in nature."
"Life imitates art."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of discussing the political nature of "The Purge" franchise.
Describes the general premise of "The Purge" movies where all crime is legal for a set period every year.
Talks about the latest installment of the franchise taking place along the southern border of the United States, focusing on a group wanting to "take America back."
Points out the obvious parallels in the movie to Trumpism, describing it as ham-fisted and not subtle.
Explains that horror movies, including "The Purge" franchise, often contain sociopolitical commentary exploring our darkest fears.
Mentions the economic disparity commentary present throughout the entire franchise and in horror movies in general.
Shares examples of political commentary in horror films regarding environmental issues, greedy politicians, and the military-industrial complex.
States that all art is political in some way and that pretty much everything today is political.
Suggests that those who dislike the political commentary in movies may refuse to see it or pretend it's a joke.
Raises the point that the entire "Purge" franchise has always been political, with intentional political commentary in the latest installment.
Notes that despite being too on the nose, the political parallels in the latest movie were filmed before certain real-life events occurred.
Concludes by speculating that there may be backlash on social media due to the political nature of the latest "Purge" movie.

Actions:

for movie enthusiasts, political analysts.,
Watch and analyze movies with a critical eye for sociopolitical commentary (implied).
Engage in respectful debates and dialogues about the political themes in art (implied).
</details>
<details>
<summary>
2021-07-02: Let's talk about the DHS bulletin.... (<a href="https://youtube.com/watch?v=3Drr7EKBGOI">watch</a> || <a href="/videos/2021/07/02/Lets_talk_about_the_DHS_bulletin">transcript &amp; editable summary</a>)

Beau warns of the escalating risk of violence fueled by baseless claims and rhetoric, challenging politicians to prioritize debunking falsehoods over political gains.

</summary>

"There is no way to misread it."
"The Department of Homeland Security is saying this is a risk. It's a danger. It's a problem."
"Let's see who is willing to put their country above their own political careers."
"Maybe they didn't understand that their words were going to have consequences, that people might act on them."
"At the end of the day, there's no excuse this time."

### AI summary (High error rate! Edit errors on video page)

Beau addresses a bulletin released by the Department of Homeland Security before the end of June, warning about the increased likelihood of violence fueled by baseless claims and rhetoric.
The bulletin explicitly states that the convergence of rhetoric and baseless claims is a significant factor in escalating violence.
The bulletin predicts a peak in likelihood of violence around August, associated with a theory suggesting Trump will be reinstated, which Beau clarifies is not true.
Beau criticizes politicians who continue to perpetuate misinformation and baseless claims despite the clear warning from the DHS.
He challenges politicians to prioritize the country over their political careers, urging them to debunk false claims and step away from dangerous rhetoric.
Beau points out that with the knowledge of the bulletin and the events of January 6th, politicians can no longer claim ignorance about the potential consequences of their words.

Actions:

for politically engaged individuals,
Monitor politicians perpetuating baseless claims (implied)
Challenge politicians spreading misinformation (implied)
</details>
<details>
<summary>
2021-07-01: Let's talk about why Republicans voted against the committee.... (<a href="https://youtube.com/watch?v=nXMJ_E7DgBo">watch</a> || <a href="/videos/2021/07/01/Lets_talk_about_why_Republicans_voted_against_the_committee">transcript &amp; editable summary</a>)

The US House vote on investigating January 6th reveals Republican reluctance to uncover the truth, leading to manipulation of their base by keeping them in the dark.

</summary>

"Either they believe that these things happened and they're okay with it, or they know they were lying."
"Because if they accept reality, well, they might be pretty mad with the Republican Party."
"They want to keep their constituents in an echo chamber of confusion."

### AI summary (High error rate! Edit errors on video page)

The US House of Representatives voted on forming a committee to look into the events of January 6th at the Capitol.
Only two Republicans voted in favor of investigating the Capitol incident, raising eyebrows.
Some Republicans claimed it was the work of left-wing agitators, BLM, or the FBI to deflect blame.
Beau questions why Republicans who made these claims wouldn't want an investigation to reveal the truth.
Republicans voting against the investigation leads to only two conclusions: they believe the claims or they were lying.
Beau suggests that Republicans on the committee may work to undermine it and turn it into a circus.
Republican leaders aim to keep their base in the dark about what truly happened on January 6th.
Enabling Trump and failing to serve as a checks and balance against executive power started the chain of events leading to January 6th.
Republicans want to keep their constituents confused to manipulate them easily.
Beau concludes by raising awareness of the Republican Party's actions regarding the Capitol incident.

Actions:

for american voters,
Reach out to your representatives and demand transparency and accountability in investigating the events of January 6th (suggested).
Stay informed about political events and hold elected officials accountable for their actions (implied).
</details>
<details>
<summary>
2021-07-01: Let's talk about the Republican investigation into the NSA.... (<a href="https://youtube.com/watch?v=NX6S8aLWHGI">watch</a> || <a href="/videos/2021/07/01/Lets_talk_about_the_Republican_investigation_into_the_NSA">transcript &amp; editable summary</a>)

Republicans deflect from January 6th with an NSA investigation based on Tucker Carlson's claims, prompting Beau to call for legislation over investigations.

</summary>

"Contrary to popular perception, the NSA is not and never was recording or listening to millions of Americans' phone calls." - Kevin Nunes
"If you really believe that this occurred, we don't need an investigation. We need legislation." - Beau

### AI summary (High error rate! Edit errors on video page)

Republicans refused to conduct an investigation into the events of January 6th on Capitol Hill, despite video evidence.
Instead, they have chosen to launch an investigation into the NSA, citing Tucker Carlson's unbacked claims.
Devin Nunes, known for his Twitter cow feud, will lead this investigation into the NSA.
Beau questions Nunes' credibility as a defender of the NSA leading this investigation.
Beau suggests that if the Republican Party genuinely cares about civil liberties, legislation is needed rather than just an investigation.
He advocates for strong safeguards to protect Americans' rights and potentially rolling back intrusive NSA programs.
Beau sees this investigation as a distraction and misinformation campaign aimed at the Republican base.
He believes it's an attempt to shift focus from the events of January 6th.
Beau suspects the timing of this investigation announcement isn't coincidental and vows to follow its progress.

Actions:

for activists, civil liberties defenders,
Contact local representatives to advocate for legislation protecting civil liberties (implied)
Join civil rights organizations pushing for stronger safeguards against government intrusion (implied)
</details>
