---
title: Let's talk about the DOJ's notes of the Trump conversation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=a8WXGkpTNP4) |
| Published | 2021/07/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Handwritten notes released to an oversight committee detail a conversation between former President Donald Trump and top officials in the Department of Justice.
- Trump suggests that the election was corrupt and asks officials to leave the rest to him and a congressman.
- The implication is a coordinated effort to overturn the election without evidence between the White House and Republicans in Congress before January 6.
- Trump understands his base doesn't require evidence; he just needs the claim out there to repeat it and have his followers believe it.
- Supporters of Trump are reminded that there has been no evidence to support claims of election fraud for months.
- People who believed in these claims were tricked into risking themselves for Trump's ego and desire to stay in power.
- Beau suggests that those who tricked people into believing election fraud might mislead them on other issues, getting them to act against their own interests.
- Trump's request for officials to "just say" the election was corrupt is seen as an attempt to manipulate them into believing it without proof.
- Trump's base is willing to follow along without demanding evidence, allowing him and his allies in Congress to handle the rest.
- Supporters of Trump should weigh heavily the manipulation and lack of evidence behind his claims.

### Quotes

- "If you believed this stuff, you were tricked."
- "Those people that went up there on the 6th, they were tripped, they got duped."
- "He just needed somebody to say it and then he and his willing accomplices in Congress, well they would handle the rest."

### Oneliner

Former President Trump manipulated his base by pushing baseless election fraud claims, knowing they wouldn't demand evidence.

### Audience

Supporters of Trump

### On-the-ground actions from transcript

- Revisit beliefs on election fraud claims and seek evidence to inform future decisions (implied).

### Whats missing in summary

Full context and emotional impact of Beau's analysis and warning against being manipulated by baseless claims.

### Tags

#Trump #ElectionFraud #Manipulation #Politics #BaselessClaims


## Transcript
Well, howdy there, internet people, it's Bo again.
So I guess today we're gonna talk about
some handwritten notes.
The notes were released to an oversight committee.
They're detailing a conversation between
the former president of the United States, Donald Trump,
and two of the top people in the Department of Justice.
The Department of Justice person says,
understand that the DOJ can't and won't snap its fingers and change
the outcome of the election.
It doesn't work that way.
To which president Trump replies, don't expect you to do that.
Just say that the election was corrupt and leave the rest to me and the
R congressman.
Now, what many people are pointing to is the obvious implication that there
was a coordinated effort in the days leading up to the 6th between the White
House and Republicans in Congress and that effort was designed to overturn the
election without evidence. That's the implication. And yeah, I mean that's
definitely worth talking about. That seems noteworthy and seems newsworthy.
There's something else about this though, that kind of caught my ear, just say
that it was corrupt and leave the rest to me. Trump is aware that his base
doesn't require evidence. He's not worried that he's gonna have to prove
that there was some problem with the election. He just needs it said because
Because as long as it's said, then he can repeat the claim and his base, his followers,
those people he's tricked, those people he's duped, they're just going to follow along.
He just needs it said because they don't require evidence.
If you are still somebody who supports the president who believes these claims, please
understand it has been months and there's still no evidence.
There wasn't evidence way back then, before the 6th, before he duped a whole bunch of
people into risking themselves for him, for his ego, for his desire to stay in power.
There wasn't evidence then, and there's not now.
You were tricked.
If you believed this stuff, you were tricked.
I'm going to suggest that the people who tricked you about something like this, the people
who sought to make you their unwitting accomplice in shredding the Constitution, in destroying
the representative government that the United States has, that they would probably trick
you about other stuff.
They would probably mislead you about other stuff.
They would probably use rhetoric to cast doubt on things, to get you involved in a culture
war, to get you voting against your own interests for their benefit.
Because that's what happened.
Those people that went up there on the 6th, they were tripped, they got duped, and they
put themselves at risk for the benefit of somebody who does not care about them.
line that isn't getting much attention in this is I don't know if you follow
the internet the way I do. You know people that have mentioned it they're
kind of looking at it as like wow Trump thinks that the DOJ needs to look at the
internet that's not what it is. I don't think that's what he's saying. I think
he's saying that he wants these two officials to look at it because Trump
himself has such a superiority complex that he believes that if they look at it
well they'll believe it too because he views everybody as a mark, somebody to be
tricked and manipulated. That's the way it seems to me. I can't prove that but
that's how it reads to me. What we can say for certain is that he didn't want
evidence because he knew his base wouldn't demand it. He just needed
somebody to say it and then he and his willing accomplices in Congress, people
who are probably still there, well they would handle the rest. That should
definitely weigh pretty heavily in the minds of his supporters. Anyway, it's just
That's a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}