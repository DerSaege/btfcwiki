---
title: Let's talk about real-world numbers in Los Angeles....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dRYevumAkog) |
| Published | 2021/07/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about the real-world numbers in LA related to the current public health issue.
- LA County is experiencing an uptick in new cases for five consecutive days.
- The percentage of hospitalizations remains low despite the increase in cases.
- Out of those hospitalized, none are fully vaccinated with J&J, Pfizer, or Moderna vaccines.
- The effectiveness of vaccination in preventing hospitalizations is evident in LA County.
- There may be rare breakthrough cases or individuals who did not complete their vaccine series.
- The debate about the effectiveness of vaccines is settled; they work.
- Politicians, particularly Republicans, may have a vested interest in vaccine failure for political gain.
- Republicans are hoping for failures to regain power, even if it means their constituents suffer.
- The reality in LA County shows that every COVID patient admitted was not fully vaccinated.

### Quotes

- "Real world right there. That's real world."
- "The question about whether or not it works is over. It does."
- "They are hoping that everything goes wrong and that their constituents suffer."

### Oneliner

Beau talks about the effectiveness of vaccines in preventing hospitalizations, revealing that in LA County, all COVID patients admitted were not fully vaccinated, settling the debate on vaccine efficacy.

### Audience

Public health officials, policymakers

### On-the-ground actions from transcript

- Get vaccinated with J&J, Pfizer, or Moderna vaccines (implied)
- Advocate for vaccination to prevent hospitalizations (implied)
- Combat misinformation about vaccine effectiveness (implied)

### Whats missing in summary

The detailed breakdown of LA County's numbers and the implications for vaccine effectiveness.

### Tags

#Vaccines #PublicHealth #LA #COVID-19 #Statistics


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about LA
and real world numbers.
Because people talk about the statistics,
but most of those are kind of clinical.
We need real world numbers and we can get them from LA
because of what's going on there right now.
LA is having an uptick in the public health issue.
That's what's going on there.
And the Department of Health Services there in LA County
was nice enough to put their numbers out there.
And what we want to know is even though
there's a thousand or so new cases a day,
only some of those end up hospitalized.
And that's what we want to look at.
We want to look at the number that were hospitalized
and find out exactly what percentage
of those are fully vaccinated.
And that in and of itself will give us a pretty good measure
of how effective it is.
So again, there's a lot of numbers here.
So stick with me on this one.
Okay.
So LA County's topping a thousand new cases
for the fifth straight day, five days in a row.
Okay.
And then, you know, the percentage of hospitalizations
that remains low.
Okay. So here we go.
Here's the numbers that matter.
Out of those hospitalized, how many are fully vaccinated?
None.
None.
Not a single one.
Quote, to date, we have not had a patient admitted
to a Department of Health Services hospital
who has been fully vaccinated
with either the J&J, Pfizer or Moderna vaccine.
Every single patient that we've admitted
for COVID is not yet fully vaccinated.
Real world right there.
That's real world.
That's the effectiveness of it.
You can sit there and you can argue about it all you want.
You can argue about whether or not it works,
but this trend, it exists pretty much everywhere.
Yeah. There's a rare breakthrough.
There's also those people who got one shot
and didn't get the next one.
And well, that doesn't work as well.
That's why they're scheduled the way they are.
The debate over whether or not it works is over.
Now the only debate is whether or not
you're going to listen to a bunch of politicians
who have a vested interest in not meeting vaccine goals,
who are staking their political futures
on things going bad for Americans.
That's how Republicans plan to win.
They want things to fail.
They are hoping that everything goes wrong
and that their constituents suffer
so they will put the Republican party back in power.
That's the reality of the situation.
The reality of the numbers is that in LA County
where there's almost two dozen healthcare facilities
that are under this little umbrella,
not a single person admitted was fully vaccinated.
The question about whether or not it works is over.
It does.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}