---
title: Let's talk about Democrats already being dissatisfied with Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=On-F1nsn79E) |
| Published | 2021/07/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Democrats are dissatisfied with President Joe Biden's performance, presenting an opening for the Republican Party and Trump in 2024.
- A Twitter survey revealed registered Democrats' discontent with Biden, including issues like not supporting Medicare for all, a $15 minimum wage, and student loan forgiveness.
- Republicans tend to unquestionably support Trump, unlike Democrats with Biden.
- Trump supporters are seen as being part of a cult of personality rather than a political party, never disagreeing with him.
- Beau questions the blind loyalty to Trump, pointing out his numerous mistakes and the disconnect between his words and actions.
- He urges supporters to critically analyze their beliefs and not blindly follow Trump's rhetoric.
- The possibility of being tricked by Trump as a politician is raised, encouraging supporters to re-evaluate their unwavering support.

### Quotes

- "If you don't disagree with the man, if you truly see him as a savior, you really need to examine the situation a little closer because you may have been conned."
- "Because let's be honest, the stuff the liberals use to make fun of him, he said it, but he was joking."
- "He messed up a lot of stuff and he just said that he didn't."
- "Politicians lie. We know that, right? Why is he somehow accepted from that rule?"
- "And if you can't even entertain the possibility that you were conned, it's a guarantee that you were."

### Oneliner

Democrats dissatisfied with Biden; Republicans blindly loyal to Trump - beware the cult of personality.

### Audience

Voters

### On-the-ground actions from transcript

- Examine your political beliefs critically (implied).
- Question blind loyalty to political figures (implied).
- Re-evaluate support for politicians based on actions, not just words (implied).

### Whats missing in summary

The full transcript provides a thought-provoking analysis of blind loyalty to political figures and the dangers of following them without question.

### Tags

#Politics #Democrat #Republican #Trump #CultOfPersonality


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Democrats
already being dissatisfied
with President Joe Biden's performance.
There might be an opening for the Republican Party here.
There might be an opening for Trump
to stage that comeback in 2024.
So I was trying to figure out what it was.
Four registered Democrats, I put this on Twitter,
four registered Democrats,
name some things about Biden
you don't like or are unhappy with.
And did you know, in less than an hour,
it had 300 responses.
And it's stuff like, you know,
not supporting Medicare for all,
I wish he was the Joe Biden from the Republican memes,
not supporting a $15 minimum wage,
not doing enough for student loan forgiveness.
Not, it's a list.
It's a list.
Democrats, they disagree with Joe Biden on a whole lot.
See, that doesn't happen with Republicans.
That doesn't happen with Trump.
Name something you disagree with Trump about.
Bet you can't.
I bet you can't.
You agree with everything he did, right?
Makes him a better president, doesn't it?
What about things he said?
Agree with most of that too, right?
Because let's be honest,
the stuff the liberals use to make fun of him, he said it,
but he was joking.
And if he wasn't joking, that's not really what he meant.
And if he did mean it, it's out of context.
And if it is in context and what he meant,
let's be honest, he's right and you're wrong.
Because he's infallible, right?
It certainly seems like a lot of Trump supporters
aren't really part of a political party anymore.
They're part of a cult of personality.
If a president like Trump,
who is consistently rated as one of the worst presidents
in history by subject matter experts
in whatever the field being rated is,
if you don't disagree with him on anything,
that may be something you want to look at.
The fact that you don't disagree with him on anything
at any point in time, that everything he did was right,
maybe that doesn't mean that y'all share the same positions.
Maybe that means that he has got you to the point
where you will support anything that he does.
Because you can't question dear leader, right?
God, Emperor Trump, he's infallible.
This is something that really needs to be examined
because the reality is that man made a lot of mistakes.
He messed up a lot of stuff and he just said that he didn't.
And people believe him because of that cult of personality.
I mean, you think about it.
Some of the most respected and decorated generals
in the military, you had people turning their backs on them
because Trump said that they weren't any good.
But Trump doesn't really know a whole lot about that stuff.
I mean, he does, of course, because he's infallible.
There are a lot of people who really need to examine
what they believe and not use the lens of what Trump believes
or what he says he believes.
Because what he says at rallies,
what he says through his social media when he has it,
it almost never lines up with his actions
or his statements in private.
There is a possibility.
You have to entertain the possibility
that you were tricked.
He's a politician.
Politicians lie.
We know that, right?
Why is he somehow accepted from that rule?
Because you bought into it.
Because you fell under the spell, right?
You really need to look at it.
If you don't disagree with the man,
if you truly see him as a savior,
you really need to examine the situation a little closer
because you may have been conned.
And if you can't even entertain the possibility
that you were conned, it's a guarantee that you were.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}