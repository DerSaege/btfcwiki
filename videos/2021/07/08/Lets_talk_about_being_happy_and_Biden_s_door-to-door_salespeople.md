---
title: Let's talk about being happy and Biden's door-to-door salespeople....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TxIzZA5oaGU) |
| Published | 2021/07/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Blue collar workers aren't happy going to work, even if they take pride in their profession and accomplishments.
- Rejects the notion of being happy to harm others based on political differences or engaging in civil war.
- Disagrees with the idea of Democrats forcing experimental gene therapy through door-to-door efforts.
- Believes differing political opinions do not make someone evil, but rather misguided, misinformed, or propagandized.
- Criticizes those who profit from pushing civil war rhetoric and inciting violence while staying safe themselves.
- Expresses a desire for peace, staying where he is, enjoying simple pleasures like being known for planting pumpkins.
- Distinguishes between door-to-door salesmen and authoritarians who use violence to force compliance.
- Urges people to question misinformation and propaganda surrounding vaccines and door-to-door initiatives.
- Encourages speaking to veterans about the realities of conflict and the lack of happiness in such situations.
- Warns against escalating dangerous rhetoric that could lead to real harm, with instigators remaining safe and profiting.

### Quotes
- "Nobody wants to go to work not like that."
- "That rhetoric needs to stop."
- "Because eventually it's going to go too far."

### Oneliner
Beau challenges harmful rhetoric on political differences, rejects authoritarian tactics, and advocates for peace and critical thinking in the face of escalating tensions.

### Audience
Critical Thinkers

### On-the-ground actions from transcript
- Talk to veterans about the realities of conflict (suggested)

### Whats missing in summary
The emotional impact and nuances in Beau's delivery.

### Tags
#Peace #CriticalThinking #RejectViolence #CommunitySafety #PoliticalRhetoric


## Transcript
Well howdy there internet people, it's Beau again.
So today
we're going to talk about
being happy
and uh...
what it takes.
You know, I've got a lot of blue collar friends.
A lot of people
who are really good at what they do
and uh...
most of them aren't happy
when they go to work.
If it's their profession,
most times
they're not excited. They don't get up and say, yay, I get to go frame a house today.
Now they may be excited about the money
and they may take pride in what they do. They may be proud of what they're able to
accomplish.
But most of them
aren't happy
going to work.
So I got a message.
Beau,
I know if civil war broke out
we'd be happy
to put a bullet in one another.
But I'm curious.
Now that the Democrats, sorry,
now that the fascistic Democrats
are planning on sending cadres door-to-door
so you will take experimental gene therapy,
what's the anti-authoritarian say?
I've never seen anybody
get this much wrong
in such a short space.
Nah, man. I don't want to shoot you.
I would not be happy to go to work.
No, absolutely not. I don't believe that most people who have
differing political opinions
are evil.
Misguided? Sure. Misinformed? Yeah.
Propagandized? Absolutely.
But not evil.
I understand
that there's a whole lot of people
who are getting a lot of prestige
and making a whole lot of money
pushing that kind of rhetoric,
that civil war rhetoric.
It needs to stop.
Do you know where they're going to be if it ever pops off?
The exact same space,
making a whole bunch of money,
safely on set with security.
It's not going to be them leaving their family for years on end.
No, I would not be happy.
Years on end, yeah.
There's no rotating home
in a situation like this.
The war is at home.
No, I would not be happy. What would make me happy is to stay
where I am.
Take a dip in a pond,
play with my horse, and be known as the guy who keeps planting too many pumpkins.
That's what would make me happy.
This needs to stop.
Okay, but as far as the rest of this
experimental gene therapy,
misinformation, propagandized,
really look into this. That's not what it is.
But that's also not the real question.
The question is
door to door.
Door to door to get you to take the vaccine.
I get it.
I mean, imagine it.
You're sitting there,
eating dinner,
minding your own business at home,
and somebody comes and knocks on the door.
You're not expecting anybody, so you're annoyed.
You get up, you open the door,
and they're like, Sir,
we're from Greengrass Incorporated.
Can we show you a lawn treatment?
Willing to bet you don't want to shoot that person.
Willing to bet you don't view that as authoritarian.
Because it's not.
Door to door salesmen
are not authoritarian.
Authoritarians are the people who
attempt to
use violence,
use the power of the state,
that inherent violence,
to get you to do something,
to force you to do something.
Not trying to convince you something is a good thing.
Those aren't the same.
But those people
who keep pushing that idea of civil war, and you should be happy about it,
they're pushing that propaganda.
Because it makes them money.
It makes them money
to say stuff like that.
It increases their social cred.
Dude,
I want you
to actually talk to the vets in your life.
Talk to people who have been in situations like that.
Ask any of them
if they were actually happy when they had to get on that plane.
Willing to bet
they're going to say no.
Nobody wants to go to work
not like that.
That rhetoric needs to stop.
Because eventually
it's going to go too far.
And it's not going to be rhetoric anymore.
And the people who started it,
just like always,
they're going to be safe somewhere else.
Making money off of it.
While you are away from your family.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}