---
title: The roads with Erica D. Smith candidate for Senate.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HPPIlnZtCvU) |
| Published | 2021/07/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau interviews former state Senator Erica Smith, a candidate for Senate in North Carolina, to learn about her background and platform.
- Erica describes her upbringing on a family farm in North Carolina and her trajectory from engineer to public school educator to state senator.
- She shares her experiences in Uganda on a mission trip and the impact it had on her perspective on community building and service.
- Erica outlines her priorities if elected to the US Senate, focusing on income inequality, rural healthcare, and environmental justice through the Green New Deal.
- She advocates for universal broadband access, Medicare for all, and addressing the challenges faced by rural hospitals.
- Erica stresses the importance of canceling student loan debt, raising the minimum wage, and supporting the PRO Act for workers' rights.
- She details her stance on gun control, including banning assault rifles and implementing common sense policies to prevent gun violence.
- Erica also addresses the pre-funding mandate for the US Postal Service and proposes postal banking as a solution.
- She concludes by inviting viewers to learn more about her platform and support her campaign.

### Quotes

- "We're running a movement to create real structural change so that our government works for all of us, not just the wealthy, not just the well-connected, but all of us."
- "Policy is personal for me, Bo. That's why I fight so hard."
- "Healthcare is a basic human right. No one should have to ration their insulin just to put food on the table."
- "Common sense protections for our community."
- "We are truly one of us for all of us."

### Oneliner

Former state Senator Erica Smith outlines her trajectory from engineer to public servant, advocating for income equality, healthcare access, environmental justice, and more in her run for the US Senate.

### Audience

Voters in North Carolina

### On-the-ground actions from transcript

- Support Erica Smith's campaign by visiting www.ericaforus.com (implied)
- Learn more about her platform and policy proposals (implied)
- Invest in politicians who champion causes that support working families (implied)

### Whats missing in summary

Details on Erica Smith's advocacy for cancelling student loan debt and raising the minimum wage are not fully captured in the summary.

### Tags

#NorthCarolina #USsenate #IncomeInequality #HealthcareAccess #EnvironmentalJustice #PolicyAdvocacy


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we are joined by former state Senator Erica Smith, who is a candidate
for a Senate in North Carolina.
And we're going to talk to her and find out a little bit about her and her
platform and what she wants to do.
So tell us a little bit about yourself.
Yes, Bo, I call myself a dirt road Democrat.
I grew up in the eastern part, northeastern part of North Carolina.
I grew up on the family farm after my dad served in the military, United States
Air Force for more than two decades.
He retired and we moved back home to the family farm in the late eighties and
really, really struggled to hold onto that family farm.
And so it's that hard work ethic that I gained from the family farm,
harnessing the available resources to build a quality of life is what put me
on my trajectory of serving and making sure that we were investing in community.
So professionally, I've been an engineer for 13 years.
Um, after being an engineer for 13 years, I transitioned, I went on a
mission trip to Uganda after I got my master's degree in religious studies.
And I really became invested in placing service over self, understanding
the capacity that we have to really build, uh, and invest in the
infrastructure so that all communities can thrive.
So for the last 17 years, I've been a public school educator and ordained clergy.
I was a party officer for 12 years, and then I started, um, in my elected
service as a school board member for six years, and then in the North
Carolina state Senate for three terms.
Now I am endeavoring, um, to take our voices and our values.
And I say our a lot because we're not running a campaign.
We're running a movement.
We're running a movement to create real structural change so that our
government works for all of us, not just the wealthy, not just the
well-connected, but all of us.
And so on that trajectory, we are fighting really hard to become North
Carolina's next to US Senator.
Wow.
Something I didn't expect to hear.
And I don't have it in my notes.
You went to Uganda?
Yes, I went to Uganda on a mission trip and it was there in Uganda.
Um, I realized that, you know, we can send technology to Jupiter and
back and every other planet, but we can't invest in that in developing
countries beyond our border or even the rural parts of the state within our
border, uh, that coincided with me relocating back to North Carolina after,
uh, working for Boeing in Seattle, Washington, and also the United States
patent and trademark office in Northern Virginia.
I transitioned and moved back home because my dad had early onset
of Alzheimer's and dementia.
I went from doing high speed research and, um, internet research on two
platforms to when I moved back home to the family farm, my parents
were still on dial up.
It was incredible.
I had to suspend my graduate program.
That's why I fight so hard.
Policy is personal for me, Beau.
That's why I fight so hard for universal broadband.
I mean, we have entire communities who are disconnected from e-commerce,
from remote learning, virtual learning, from telemedicine, because
the broadband isn't strong enough.
And so after that mission trip, I said, Hey, you know, I want to really
work toward community building.
And I couldn't, you know, do that as an engineer, but being in the community,
being a public school educator empowered me to be able to build coalitions and,
um, do a lot of work around, um, you know, creating that structural change,
particularly for marginalized communities and those folks who have been
left out unheard and underserved.
Wow.
That's, that's something else.
All right.
So you you've been in North Carolina, you've been in government there, and
now you're going to the federal level.
That that's the goal.
What are the, uh, the main things you're going to want to accomplish up there?
Well, we have three large buckets of priorities.
The first one is to address the extreme income inequality that's
existed in this nation.
You know, this, this pandemic revealed just how rigged our economy is.
When you look at having food lines further than the I can see bow, but yet
at the same time, we have millionaires and billionaires, tripling, doubling,
quadrupling their net worth in a pandemic.
And so, um, we're looking at so many rural towns that I've represented and
even some pockets of urban centers, um, that we travel, they've been hollowed
out by monopolies and, um, they've been left empty and we've had, you know,
struggles to connect struggles to have the infrastructure investments so that
all parts of the state in the nation can grow.
So that's why I've worked my way.
You said, you know, from local leadership to federal leadership.
I'm one of those candidates who understands, you know, the importance of a work ethic,
but also gaining the tools and learning the rules and also paying your dues.
I am working that way from local elected office to state office
to now congressional level office.
Um, that's been, you know, empowering for us because we have built coalitions
for the last 20 years of black, white, and Brown, who are very supportive
and want someone who's going to fight for them.
Once someone who's going to place people, um, over politics and prioritize
people over corporate profits and prioritize communities over corporations.
And they have that champion in me.
That sounds wonderful.
One of the, uh, one of the things that I saw on your campaign stuff
was the thing about rural healthcare.
And I'll tell you, and the people on this channel know that this, this is an issue
that, that definitely touched close to me during the onset of the pandemic,
because I mean, this channel helped go out and get like medical supplies
for these hospitals because they just couldn't get them.
Um, I I'm curious, what, what are some of the specifics on that?
Like, how do we, uh, how do you plan to address it?
Because that is, uh, that's a big question.
How do you plan to address it?
Because that is, uh, that's something else.
And I never realized how bad it was until the pandemic.
Yes.
You know, and that's what it is.
The pandemic revealed just how broken our healthcare system is
that prioritizes profits over people.
The reason why many of these rural hospitals are failing, and that's
part of the income inequality bucket that we have, we also are supporting
Medicare for all, um, and then the green new deal.
And I'll talk about that a little bit later.
Um, when we look at Medicare for all, we know that about 600,000 Americans
died, um, in this, uh, pandemic and, uh, almost a third of them, uh, more
than 200,000 died because they did not have health insurance or
they were under insured.
And so what we know is that if we have the political will, we
can provide healthcare for all.
And we can do that by making sure, um, we were fighting for people to be
able to get tested, even if they didn't have insurance and did not have to pay
and to be able to get that treatment.
And so we can do it in a pandemic.
We can do it as the, one of the wealthiest countries on the planet.
And, um, to be the only developed country that does not have a
universal healthcare, there's a problem.
This system is rigged.
It's broken and it's profiting off of a misery economy.
And so we need to provide for Medicare for all, because I believe that
healthcare is a basic human right.
No one should have to ration their insulin just to put food on the table.
I have a personal experience.
My family almost went bankrupt when I gave birth to a medically fragile son
and we had to get rid of our assets.
Um, we almost lost our home.
We had to get rid of the spare vehicle just for him to qualify for Medicaid
cap C and even with qualifying for Medicaid cap C, it did not provide for
the basic needs that we needed.
And so Bo, you started this question by highlighting how difficult it was to
get resources to these rural hospitals.
And it's because, you know, a state like North Carolina, these rural
hospitals are closing their doors and they're suffering because we have not
expanded Medicaid in North Carolina.
And we are denying almost 600,000 North Carolinians adults
who do not have healthcare.
Many of them are working adults.
It's a big problem.
That's why I'm fighting so hard, but you are absolutely right.
These rural hospitals are under-resourced and, and, you know, anytime you have
to drive two and a half hours, we were in Clay County last night in North Carolina.
And that's six hours from where I live.
And I, you know, I live in Northeastern North Carolina.
And when we got to Clay, we had a room full, almost standing room in one of the
reddest counties in the state because people want someone who's going to champion those
issues that are affecting them the most.
And that's looking at having to drive two and a half hours from Clay County, either
go to Tennessee, Chattanooga, or go to Asheville, North Carolina, in order to
deliver a baby because all of the labor and delivery wards and wings are closing at
these hospitals.
They can't keep the doors open.
And so we need to really, really look at how we can invest in making sure people have
universal health care.
We need to stop the profiteering by the pharmaceutical companies and the hospital
CEOs.
And we really, truly need to invest so that everyone can have health care.
You shouldn't have to drive, you know, an hour just to get medical services.
All right.
Now, everybody knows, or at least has a general idea of what the Green New Deal is.
I saw a red deal and a blue deal in your campaign stuff.
Why don't you tell us a little bit about that?
Yes, the blue deal deals with the oceans.
I am an environmental justice advocate.
I got my start in the sixth grade.
I live next door to Warren County in Northampton County, and Warren County was the
birthplace of the environmental justice movement.
For those of you who can recall that there was toxic PCB dumping in black communities and
poor black community in Warren County.
And that was the birthplace of the environmental justice movement.
That was when I gained my political consciousness to fight for clean air and clean water for
everyone, and especially in communities that are underrepresented, like marginalized
communities.
The red deal is a strong deal that comes from a platform of understanding racial justice,
particularly for indigenous people and marginalized communities.
The blue deal focuses on clean water and our oceans.
We have enormous problems in North Carolina with, first of all, the flooding and the
contamination of our groundwater from runoff from hog lagoons and waste sites because these
500 year floods have been coming every other year in North Carolina because we've had
extreme problems with global warming.
And we need to address this climate crisis with the bold initiative of the Green New
Deal.
For me, the Green New Deal is strong in that it creates millions of good paying jobs,
creating the energy system of the future.
When you can look at these good paying jobs that will elevate families out of poverty,
these are jobs that don't pay a starvation wage.
They pay above $15 an hour.
And so that's why we're really fighting hard for these strategic infrastructure investments
that promote racial reckoning with what has happened with marginalized communities with
the red deal, but also understanding the blue deal and the need to protect our water.
In North Carolina as well, I have fought as a state senator to address the issue of Gen
X and emerging compounds, chlorofluorocarbons that are in our water and it's contaminating
our water sources, especially when it comes to our military families.
This is untenable and we have to fight for those who don't have a voice and who deserve
to have a better quality of life.
We need to clean it up.
And so we see the blue deal, the green deal and components of the red deal as effective
in doing that.
Wow.
It's going to be so fun listening to this because you talk so fast and I talk so slow.
I'm sorry, I have a lot to say and I try to get it in really fast.
As a teacher, we had to teach to a pacing guide and make sure we covered all the objectives
for our students.
So I've gotten a lot of practice in the last 17 years, talking fast and getting all the
information out.
No, it's, I don't know if it's intentional or not, but it definitely shows you know your
material.
I mean, like there's no doubt about that.
You're not reading it that quickly.
That's for sure.
We've been working on these issues for so long.
It's part of our life story.
It's about creating a government that works for us.
You know, one of the things when I was going through getting ready for this, I was looking
at your campaign stuff and you don't pull punches like it.
You know, there was there's a line here.
It's our country exists on stolen ground, like flat out.
Boom.
Here you go.
This is this part of it.
We're going to go ahead and just throw this out here and then you go into talk about the
red deal.
I'm curious with the current political climate being that up front and just saying this is
what we're going to do.
Medicare for all, you know, everybody supports it, but it runs into a lot of well, not everybody
supports it, but it has overwhelming support among the American people.
But when you get up to Capitol Hill and that that's a tough sell.
So how are you envision envisioning doing it?
I mean, you have statehouse experience.
I'm just wondering.
I mean, that's going to be tough.
It's going to be hard.
You know what?
I'm reading President Obama's last book, and I believe there is a line in there that that
David Axelrod share with him that hard thing is hard to do or hard things are hard to do.
And we recognize that this is hard.
But you are right, though.
The overwhelming vast majority of Americans support Medicare for all.
They understand its importance and taking the profits and making sure that you're investing
them in the health and well-being of people and not hospital CEOs and pharmaceutical executives.
The reason why there's such inaction is because many of our politicians have been bought out
by corporate interests.
They have been influenced by all of the corporate PAC money they receive from pharmaceutical
companies.
And so we you know, we believe in calling a spade a spade and talking truth and speaking
truth to power.
And that is our economy is rigged.
And that rigging is perpetuated and continues because it protects wealthy.
It protects wealthy people.
It protects wealthy establishment politicians who don't want to upset the status quo and
create the structural change that will lift millions of Americans out of poverty.
And so to embrace another straight to the point, straight, no chaser concept is my role
model, Representative Shirley Chisholm, who said you have to be unbought and unbossed.
This is why, Bo, we are a campaign that's entirely people powered through grassroots
donations.
We have taken the no corporate money pledge, no money from fossil fuel, gas and oil CEOs,
no corporate PAC money.
We only accept money from our only boss.
And that's going to be the voters and the people of North Carolina and the people of
this nation who want to champion who is not going to be bought off and not fight for the
issues that we know will be effective in making sure that everyone has universal health care.
It is going to be difficult to do, but there are a number of us now who are fighting for
that.
And I say that when we pull the progressives together who are fighting for this issue,
many people it's been easier to talk about Medicare for all after the pandemic.
And we've seen what can be done.
We also support UBI, universal basic income.
No one could imagine before the pandemic that Americans could get a monthly check or a stimulus
check to get through hard economic times.
And when we look at it, in every economic crisis, our government has intervened.
They have bailed out the automotive industry, bailed out banks, bailed out corporate farmers.
Where is Main Street's bailout?
Where is the average working families bailout?
That bailout is UBI.
I, as a theologian, I follow Dr. Martin Luther King's teachings.
And I know when he was talking about eradicating poverty, he said the best way to do it is
through a guaranteed basic income.
That is a monthly check.
For me, our policy is $1,000 a month to every adult 18 years and older in this country.
And it helps to work toward changing the socioeconomic status.
When I talk about income inequality, it's about counseling student loan debt, taking
that burden that's been placed on the poor.
The poor are paying more because of tax loopholes that we have in our IRS code, where the wealthiest
are not paying their fair share.
They can certainly afford that 1%, that 2% to make sure we can invest in those programs
that are going to create that structural change, lifting generations out of poverty.
This is a moral movement for us.
We don't call ourselves a campaign.
We are working to make our government invest in the people of this nation.
All right.
And as far as working class people, when I was kind of digging into you, I know that's
got to be weird to have people say that, but you went to college on a union scholarship,
right?
Yes.
I can assume you support the PRO Act.
Absolutely.
I support the PRO Act.
Unequivocally, I support the PRO Act.
And I see that in building the 21st century middle class.
We can do that through strong union jobs.
I've been a member of the union in every job where I have worked.
It was because my dad, as a male handler, after he retired from the United States Air
Force, we lost the family farm.
We went in the red in the late 80s.
That was very traumatic for us, particularly when my dad was suffering from PTSD.
And then to have that burden, black farmers were really left out in much of the USDA aid.
And so we were fighting decades and generational fights for equity.
And so that's one of the issues that I wanted to highlight.
But then he had to return to work for federal service to the post office because he had
to support his family.
It was six children and my mother.
And he did a phenomenal job making sure that we were taken care of.
And so it was through his employment with the US Postal Service that my twin sister
and I, it's six children, four of us were in college at the same time.
And so I would not have been able to go had the male handlers, local 305, not awarded
me and my twin sister financial aid and financial scholarships for us to be able to afford to
go to college.
And so I'm very grateful for the impact that unions have had in building strong families
and strong communities.
I was a member of the union as an engineer.
I was a member of SPIA, which is a sub organization of international machinists as a mechanical
engineer for Boeing.
And understanding how we can improve the workplace and protect workers through our unions.
I definitely see the strength in the PRO Act and being really effective, particularly,
especially as necessary to protect essential workers and workers who are coming back from
a pandemic and need those protections in the workplace.
Okay.
All right.
So, so you really did some research on me.
Thank you.
I do.
I like to be prepared and I, you know, and like, I know the, the, the answer to the next
question I'm going to ask, and this, just for anybody else who's watching, this is like
a normal viewer of the channel.
This may be like the only issue where you're like, wow, I don't agree with that.
But I mean, nobody, nobody is going to agree on everything.
So let's talk about gun control.
You're in North Carolina.
That's that's going to be a hard sell.
I don't have the specifics.
I know.
I don't have the specifics on what it was you're looking at.
And just to be clear, if you don't know, it's not like I'm one of the, the, the, the far
right.
Nothing is out of bounds.
I didn't do anything.
I was just, I saw it as a bullet point in your campaign stuff.
And I was wondering what it meant.
What is gun control to you?
For me, gun control is common sense policies and legislation to protect innocent people
from being harmed through gun violence.
We've had an enormous uptick and increase in gun violence across this nation.
There have been so many mass shootings, more mass shootings than we have days of the calendar
year and something needs to be done.
I am for common.
Let me lay this out this way.
First, let me share with you that hunting heritage is very important in my family.
I grew up in the rural part of the state.
I have one of my sisters who is an Abbott hunter.
She prefers steel hunting.
I have taken the concealed carry class myself.
I have not yet purchased a gun.
I support that common sense legislation that many, many, the vast majority of gun owners
support.
And that is making sure that you're banning assault rifles.
When we go hunting, when my family members go hunting, they don't hunt with an assault
rifle.
If you've got a hunt with an assault rifle, then you don't need to be hunting.
And so we look at banning assault rifles, bump stocks, rapid fire ammunition, at least
putting a limitation on that, universal background checks with the adequate waiting period.
If that's 10 days, then it's 10 days to make sure that you are not allowing anyone who
should not own a gun to fall through the cracks.
Red flag laws that will be provided to keep individuals and gun owners safe when they
should not have a gun because of mental incapacitation or because they're sick or other challenges
that for periods of time would not make it safe for them to have a gun or safe for the
public.
And so these are common sense issues, common sense policy and reforms that many, many Americans
support.
And so that's what gun reform and gun policies, common sense protections for our community.
I believe that we can use the advancement of smart gun technology for all of those occurrences
of children getting access to guns when they shouldn't, because people are not always following
the protocols and the rules.
But I know that in this 21st century, we can look at some innovative strategies like smart
gun technology to prevent people from being harmed from unsafe practices.
Well in many ways, gun owners are their absolute own worst enemy for not doing the stuff that
they should do as far as firearms and stuff like that.
But one thing that I have to do, because when I, anytime I talk to anybody who's going to
have any influence when we're talking about gun control, please, please look into closing
domestic violence loopholes.
They are like these, there's laws on the books, but they exist with a whole bunch of loopholes.
And when you look at the statistics, that is absolutely going to be the most effective
thing.
And that's something that it seems like it should be on somebody's national platform.
And I don't see it very often.
So honestly, when going through your entire platform, that was really the, that's the
hard question, like for the audience that you're going to be talking to, that's the
tough one.
Everything else is definitely in line with what most people watching this channel believe.
And to be honest, it's probably 50 50 on the guns.
So when it comes to student loan, it says canceling student loan debt, right?
That was the thing.
Now, how is that going to work?
Is this a thing where we'll end up with federally funded universities?
Or are we just talking about what's incurred and try to reset everything?
We're talking about what's incurred and definitely reset.
That is a start.
When you look at student loan debt, it unnecessarily and it has a disparate impact on poor people
and minorities.
As black women, black women are one of the most educated demographics, but they carry
the highest in student loan debt.
And so when I'm talking about canceling student loan debt, this is a tool that will help lift
generations out of poverty.
You look at the ways that people were able to build wealth in this country.
If you were not born with a silver spoon or didn't come from a family with money, you
had to get a college degree in order to earn in higher salary brackets.
That is unfair when you have generational poverty.
That's our that our systemic holdovers from Jim Crow, from segregation, from slavery,
and slavery in this country.
As a black woman, Bo, let me share this statistic with you.
As a black woman, I only earned 62 cents on the dollar of a white male.
And so when my earnings are capped because of systemic discrimination and inequities
and pay across genders and across demographics, then you're placing me in a further hole of
poverty.
And then when when I and those student loans are because my family, you know, doesn't have
the generational wealth to just write out a tuition check, then you're adding layers
of inequality.
So I believe that one of the most structural ways that we can impact lifting people out
of poverty is to cancel student loan debt.
That is a start.
And so beyond that, I do believe that there should be free community college and we need
to look at some of our state colleges and making sure tuition is as low and as affordable
as possible.
But I am for counseling student loan debt.
And I am for a form of free college for so many students, because I believe what Nelson
Mandela said, a quality education is the greatest civil rights tool of the 21st century.
Absolutely, absolutely.
So coming from a postal family, what do you think?
I didn't know that we shared that in common.
Oh, no, I was I was just wondering what you think of the pre funding mandate.
I thought you were saying you're from a postal family as well.
I believe that this pre funding mandate, this is why this is what has led to the US Postal
Service getting into such extreme financial problems and hardships.
We don't require that for any other governmental entity.
And so we need to remove that.
I also am supporting a policy that's postal banking.
In many of these rural communities, as I shared earlier, the banks are closing their banks
are closing their doors.
One of the biggest issues that I get from seniors that I talked to is that they want
face to face banking, they want personal banking, they don't trust the internet, they don't
trust you know, those who can get a broadband connection, they don't want to do those transactions
online because of identity theft and the issues around cybersecurity.
And so I feel that is a win win every town that we travel through has a post office.
And so we are promoting a policy that talks about postal banking services.
And that will be a way to add the additional services and have the increased the increased
operations for our postal service.
It's it's a wonder it survived in the previous administration with the attacks on the postal
service.
But I see that as two things postal banking, but also this pre funding requirement.
We should end that and we should look at ways to strengthen our post office because it is
a life saving tool in many of our communities across this rural state in North Carolina,
but also across this nation.
Yeah, so for listeners overseas, yeah, you know how y'all can go to the post office and
do a whole lot of your banking needs.
For some reason, we don't do that here in the US.
I mean, this is this is standard.
It's being done, why not do more of it?
Yeah, I mean, that that's, it's one of those things that the facilities are there and they're
trusted people because of having to handle the mail, it seems like it would just go hand
in hand.
Yeah, so when it comes to your platform, I think we've gone over most of it.
Is there anything you want to get out?
Um, I talked a lot about the pro act, but I also want to stress a $15 minimum wage as
the floor if it were indexed with inflation, it would actually be 22 around that neighborhood
per hour.
And I just see that raising the wage, people are trying to survive off of starvation wages.
There's a lot of discussion around this time about, you know, businesses not having workers
and I don't think we have a worker shortage.
I feel like we have a wage shortage bow.
And you know, people the problem, if people are getting paid more $300 a week to stay
at home, then the problem is the starvation wages that people have had to be faced with.
And a lot of workers have not been able to return because they don't have the money or
they don't have access to childcare.
And so those are issues as a working mother.
I had to pay when I first started out, my children were young.
When I was working as an engineer, I had a my two sons, my older sons are two months
apart.
I mean, 15 months apart.
And even with their age difference, you know, I was paying $1,100 a month in 1993-94, $1,100
a month in childcare alone.
So I never really saw my paycheck bow, it just circulated through the local economy.
That's all I did.
And so when you look at the cost of childcare, that's really important for us.
And so we will be sharing, you know, our policies.
We have a series called policy is personal.
And for me, having to navigate these broken policies is why I fight so hard, because I
know that the, you know, the status quo that disenfranchises so many groups of people is
only been the status quo, because there's been no one to look at reengineering those
policies, so that they work more effectively.
They work as well in the rural pockets, as well as the urban centers, and some of the
work that I've done as a state senator, the district that I represented became less economically
distressed because of reengineering those tools for strategic and targeted economic
investments, and investments in infrastructure.
And that's how you do the work of elevating communities out of poverty.
So I hope that people will go to our website, www.ericaforus.com.
We are truly one of us for all of us.
We are one of us who will fight for us, and one of us who will change.
We are the only progressive in this race, Bo, and we are the only ones offering a platform
that is big enough, that is bold enough to solve the problems that working families are
facing, not only across the state, but across this nation.
And so we would like for you to learn more about us, go to our website, do your research,
invest in our campaign.
We don't take money from corporations and corporate PACs, but I guarantee you, if you
don't invest in politicians that you want to support who are championing our causes,
then corporations will have their way and buy all.
I think I'm losing you.
Speaking of broadband internet in rural areas.
Okay, are you back?
Okay.
We are People For Us Movement, and we're looking forward to representing millions of Americans
in the US Senate.
We know that together we can do this.
Thank you.
All right.
One of the cool things, for those that are wanting to check it out a little bit further,
on her website, the platform's actually there.
We talk a lot about policy and actually having a platform to stand on.
It's all listed.
You can go through each item and kind of take a look.
Now are you still there?
Or have I lost you completely?
This certainly shows the importance of the broadband internet in rural areas that she
was talking about.
I guess that's going to be the end of this interview.
She's running in North Carolina, and go check her out.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}