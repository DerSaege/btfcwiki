---
title: Let's talk about 8 months, justice, and fairness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RgFXRd_qRoM) |
| Published | 2021/07/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explaining the controversy surrounding an eight-month sentence given to a Florida man for actions at the Capitol on January 6th.
- Clarifying that the person received the sentence for being a "selfie seditionist" and not for engaging in violent acts or causing damage.
- Mentioning that the sentence was relatively light due to the defendant pleading guilty early and having no significant criminal history.
- Addressing the question of fairness and justice in the sentencing, pointing out the potential consequences beyond just the time served.
- Questioning the purpose of prisons and the justice system – whether they are meant for punishment, rehabilitation, or simply separation from society.
- Rejecting the idea of longer sentences based on unfair past judgments, advocating for a system that focuses on restorative justice.
- Expressing concern over mass incarceration rates in the United States and the need to reconsider who gets locked up and for how long.
- Encouraging a deeper reflection on personal beliefs when discussing sentences and justice, rather than reacting out of anger or a desire for revenge.

### Quotes

- "I don't want to live in a society where the justice system is based on vengeance."
- "We have to determine as society what we want our prisons to be, what we want our justice system to be."
- "If it's about rehabilitation, if it's about restorative justice, then it's completely reasonable."
- "Make sure that Mark and Malik should be at home. Not that Florida man should do 20 years."
- "Not just that you're angry. Not just that you want punishment."

### Oneliner

Beau addresses the controversy of an eight-month sentence, questioning the purpose of prisons and advocating for a justice system not based on vengeance.

### Audience

Justice reform advocates

### On-the-ground actions from transcript

- Advocate for restorative justice practices and rehabilitation programs in your community (suggested)
- Support organizations working towards reducing mass incarceration rates and promoting fair sentencing policies (suggested)

### Whats missing in summary

A deeper understanding of the nuances of justice reform and the implications of different sentencing approaches.

### Tags

#JusticeReform #PrisonSystem #FairSentencing #RestorativeJustice #MassIncarceration


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about a sentence.
We're gonna talk about an eight month sentence
that has prompted so many different discussions.
And we're gonna try to hit a couple of them in one video.
Because it has sparked conversations about fairness,
justice, what our justice system is supposed to be,
and what people can expect.
Let's start with that last one first,
because there's a lot of people who
are viewing an eight month sentence.
And if you don't know what I'm talking about,
the first person, of course it's a Florida man,
who was sentenced for a felony charge for their actions
on the 6th at the Capitol got eight months.
And people are like, is this all they can expect?
No, no.
The court basically said this person was a selfie seditionist.
They didn't break anything, didn't
engage in any acts of violence.
They walked inside, took some selfies,
and were there about 15 minutes, and then left.
They're getting eight months for that.
Most people will not get that sentence,
because this person pled guilty very early in the court
looks favorably upon that, because it shows
acceptance of responsibility.
This person had no criminal history to speak of.
And this person didn't engage in any of the more serious acts.
And because we do not engage in collective punishment
in this country, at least we're not supposed to,
his sentence was relatively light.
This isn't what people are looking at.
They're looking at much, much more.
His was the exception.
Eight months is the floor.
That's where they're going to start at bare minimum.
Now from there, we have the question of fairness
and whether or not it's justice.
To me, what's actually going to happen to this person?
Eight months, so they'll go away for eight months.
During this time, they'll lose their job.
Probably lose their car, place of residence,
definitely be financial strain, and be
saddled with a felony conviction for the rest of their life.
That kind of seems reasonable to me.
That kind of seems reasonable to me.
But then you have the question of fairness,
because people can point to those
who voted before they were all the way off parole
in the time they're looking at.
Or they can point to Mark, who got three years for two plants.
Or they can point to Malik, who got five years for the same two
plants, but he got sentenced to more time
because his name's Malik.
And there's a discrepancy.
And it begs for a what aboutism type of argument.
I really don't like those arguments,
because they come across to me as the eight month
sentence should be longer.
But that's not what it says to me.
See, we have to determine as society
what we want our prisons to be, what we want our justice
system to be.
Is it restorative?
Is it rehabilitative?
Are we just separating people until they're no longer
a threat to the rest of society?
Or is it punishment?
And I think for a lot of people, it's punishment.
That's not how I view it.
I can't look at Mark and Malik's sentence
and say because they got an unjust sentence,
this person should go away for longer,
because that's not what it says to me.
Eight months seems reasonable for this.
So for Mark and Malik to have done nothing to anybody,
that means they should be at home.
Means they should be at home.
I don't want to live in a society
where the justice system is based on vengeance.
That doesn't seem like justice to me.
It seems like it's just retribution.
And I'm sure there's a place for that,
but it's not the world I want to live in.
To me, these other sentences, these other things
that are being held up, to me, they're plainly unjust.
So I can't use them as an example
to say this person needs a longer sentence.
I can use the eight month sentence
to say these people should be at home
because they weren't really a threat to society.
We have a mass incarceration issue in this country.
We have higher rates of incarceration
than pretty much anywhere in the world.
And it's probably due to locking people up
that don't really need to be locked up
or locking them up for too long.
I get the whataboutism, and it makes sense
because it isn't fair.
When that argument is made,
make sure that it aligns with what you really believe.
Not just that you're angry.
Not just that you want punishment.
Because yeah, if it's about vengeance,
nine, eight months, that's not enough.
But if it's about rehabilitation,
if it's about restorative justice,
if it's about these other things,
then it's completely reasonable.
If you don't want a society based on vengeance,
when you make this comparison,
make sure that you're making it clear
that Mark and Malik should be at home.
Not that Florida man should do 20 years.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}