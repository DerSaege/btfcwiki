---
title: Let's talk about the Cleveland Guardians....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=r7c8qJ3KjU8) |
| Published | 2021/07/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A baseball team's name change is the new front in the culture war, with Ted Cruz and former President Trump lamenting the Cleveland Indians becoming the Cleveland Guardians.
- Beau addresses a message accusing him of deleting a video on army helicopters named after Indians due to Trump supporting the practice, calling out the sender as disingenuous.
- He clarifies that the video was not deleted but is hard to find because the title does not mention Indians or helicopters, urging the sender to watch it.
- Beau explains that when the military names helicopters after native groups, it is to honor them, not to create caricatures like sports mascots.
- He humorously notes Trump's focus on sports team names in his post-presidency phase, finding it entertaining but also acknowledging the anxiety name changes can cause.
- Beau jests about his own experience with sports teams changing names, illustrating the frivolity of the culture war and conservatives' lack of substantial policy.
- He points out that conservatives rely on outrage and fear-mongering to stay relevant, lacking real solutions or policies.
- The Cleveland Indians have had several name changes in the past, leading Beau to suggest that one more change won't hurt.
- Beau criticizes politicians and media personalities for focusing on trivial issues like team names rather than addressing real problems.
- He concludes by remarking on the conservatives' reliance on provoking outrage over insignificant matters to maintain power.

### Quotes

- "I understand that can cause a lot of stress and a lot of anxiety."
- "They want to maintain the status quo and the world is changing."
- "All they have is stirring up outrage and fear because they have turned into shock jocks."
- "They are hilariously wrong."
- "Because they don't have solutions, they don't have policy."

### Oneliner

Beau addresses the controversy around sports team name changes and criticizes conservative tactics of outrage without solutions or policy.

### Audience

Activists, Sports Fans

### On-the-ground actions from transcript

- Share educational content on the history and significance of indigenous names in sports teams (suggested).
- Challenge fear-mongering and outrage tactics by promoting informed and constructive dialogues (implied).

### Whats missing in summary

The full transcript provides a detailed insight into the frivolity of the culture war and the tactics used by conservatives to provoke outrage without offering real solutions. Viewing the full transcript will give a deeper understanding of these dynamics. 

### Tags

#Sports #Conservatives #CultureWar #NameChange #Outrage #Policy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a baseball team changing its name,
because apparently that's the newest front in the culture war.
I know that at least Ted Cruz and
former President Trump have lamented the changing of the name of
the Cleveland Indians to the Cleveland Guardians.
I know this because it prompted a deluge of messages to me.
I'm going to read my favorite and go from here.
I noticed you deleted your video on army helicopters being named after Indians now
that Trump also supports naming things after Indians.
You are a prime example of disingenuous liberals who hate history.
Quick, hit delete because you won't be able to make fun of this the way you do
others, and you're wrong about a lot.
Okay, first, that video wasn't deleted.
I'll put it down below.
It just doesn't have Indian or native or helicopter in the title, so
it's hard to find.
You really should watch it because you definitely did not get the point
of that video.
I would point out that there are a couple of major themes in it.
The first is that when the military names helicopters after native groups or
native leaders, it is legitimately done to honor them,
not to turn them into a cartoonish mascot, which is a line in the video
where I'm referencing the Cleveland Indians.
I think that's funny.
And the other theme is that people who look like you and
me really don't have a vote in this.
Now, I do have to admit, I am delighted that we have now
entered the post-presidency phase of Donald Trump,
where he is yelling at clouds about sports teams names.
I find that entertaining.
But at the same time, while he's definitely wrong in this case,
I do understand how unnerving it can be for a sports team to change its name.
I understand that that can cause a lot of stress and a lot of anxiety,
because I felt that way when the Rustlers changed their name to the Lakeshores.
And then when the Lakeshores changed their name to the Bluebirds, and
when the Bluebirds changed their name to the Broncos, and
the Broncos changed their name to the Nats.
It really bothered me when that happened.
And if you do not get that joke,
I don't care what you think about the name of the team in Cleveland.
Yes, politicians who are supposed to be helping a state or
a person who raised tens of millions of dollars to challenge an election and
then didn't, their major concern right now is
the name of a sports team that has repeatedly changed its name.
The culture war is what conservatives have now.
It's stuff like this, completely without substance.
And most times, if you ever actually looked into it, they are hilariously wrong.
All of those names that I recited,
those are all the former names of the Cleveland Indians.
I don't think one more name change is gonna hurt, to be completely honest.
But this is what they have.
They have stuff like this because they don't have policy.
Because all they have is stirring up outrage and
fear because they have turned into shock jocks.
They have turned into talk radio hosts whose primary mission
is fear mongering and stirring discontent and anger.
Because they don't have solutions, they don't have policy.
They want to maintain the status quo and the world is changing.
And the only way they can retain power and
continue to get donations for their pack is to stay relevant
by provoking outrage over nothing.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}