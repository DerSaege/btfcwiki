---
title: Let's talk about Governor Ivey pointing fingers.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fepwzU7GYns) |
| Published | 2021/07/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the effectiveness of the governor of Alabama's statement blaming the unvaccinated for COVID-19.
- The governor's message may not be persuasive due to varying motivations among individuals.
- Some unvaccinated individuals may only comply if mandated due to a belief in authoritarianism.
- The statement's effectiveness may lie in changing the narrative rather than increasing vaccination rates.
- Beau questions whether the governor's goal was to shift blame onto the unvaccinated rather than encourage vaccination.
- The approach of blaming the unvaccinated might not significantly impact vaccination rates in Alabama.
- Southern cultural dynamics and historical context are considered in assessing the statement's potential impact.
- The statement may resonate more with those who already follow an authoritarian leader.
- Beau suggests that the goal was to divert attention and shift the narrative away from other responsible parties.
- Changing the narrative and blaming specific groups might be the main intent behind the governor's statement.

### Quotes

- "Will it work on the others? You've disappointed grandma? Maybe."
- "Blame the unvaccinated people. Don't blame the people who downplayed it."
- "Changing the narrative. And I think that's really what it's about."

### Oneliner

Beau questions the persuasive power of blaming the unvaccinated, speculating on a narrative shift rather than increased vaccination rates in Alabama.

### Audience

Social commentators

### On-the-ground actions from transcript

- Challenge narratives about blame (implied)
- Encourage critical thinking about shifting blame (implied)

### Whats missing in summary

The detailed nuances of Beau's analysis can be best understood by watching the full video.

### Tags

#COVID-19 #Vaccination #NarrativeShift #Blame #Persuasion


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
the governor of Alabama statement because I've got some questions about it.
And we're going to talk about whether or not it is going to be effective at
motivating people and whether or not it's going to be effective at what she
wanted it to be effective at. So if you don't know what she said, she said folks
are supposed to have some common sense but it's time to start blaming the
unvaccinated folks, not the regular folks. It's the unvaccinated folks that are
letting us down. The question that I've gotten is, is this going to be persuasive?
I don't know. Generally speaking, if you're trying to motivate somebody, you
have M.U.R.R.I.E. M.U.R.R.I.E., an acronym. You have five ways you can do it. Monetary,
addiction, religion, ideology, or ego. Those five things. Governor Ivey is an
older woman in the South. This statement, you let grandma down. That's kind of how
it comes across. Is it going to be effective? I don't know. I don't know. I
truly believe that a large portion of those who are unvaccinated have
fallen under the spell of authoritarianism to the degree that they
won't do it unless they're mandated. Unless they are told to, they won't do it
because they have already accepted the idea of a strong person leader of an
authoritarian. So they need to be commanded. I think that's a lot of
them. Will it work on the others? You've disappointed grandma? Maybe. Maybe. It
might be effective. It's not the route I would go, but as we've talked about
before, when you're structuring arguments to persuade people, it's part messenger,
part the way you've structured it. You know, there's a different combination
when it comes to each person. One argument won't work for everybody, won't
persuade everybody. One motivation doesn't cover everybody. And yeah, this is
why you'll find several videos on the same topic over time from me, hitting
different motivations. Do I think it'll be effective? I don't know. Maybe for some.
Maybe for some, but I tell you what it will be effective at. Changing the
narrative. And I think that's really what it's about. I don't think it has anything
to do with her convincing people to go get vaccinated. I don't think that was
her goal. I think her goal was to change the narrative. Blame the unvaccinated
people. Don't blame the people who downplayed it. Don't blame the people who
signed stuff stopping vaccine passports. Don't blame the politicians. Blame those
who followed them. That, to me, I think that's the real intent here. Do I think
that's going to be effective? Oh yeah, because I didn't get a single question
about that. So it will shift the narrative. So I think that's that part
of it will work. As far as it raising the just completely disappointing
vaccination rates in Alabama, probably not. Not by much. There was a time when a
woman of her age saying something like that, telling a bunch of Southern men
they don't have any common sense and that they're letting people down, there
was a time when that would have been effective. I don't know that that time is
now. I think that may have passed for a whole lot of people, particularly the
demographic that isn't vaccinated, because they operate from a place of
obedience and anger. So I don't know that it's going to help. But again, I don't
think that was a goal. I think the goal was to change the narrative, change the
song, get people thinking about something else and blaming somebody else. It's an
interesting choice of words. We need to blame these people. Shifting the blame
quite literally. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}