---
title: The roads with Kim Kelly and the union....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vjsn2Tbl7ak) |
| Published | 2021/07/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduction to guest Kim Kelly, a freelance labor reporter with a book coming out soon.
- Kim Kelly shares her background, living in South Philadelphia, and reporting on strikes in Alabama.
- Kim Kelly recounts her involvement with the Amazon warehouse workers' unionizing efforts.
- Details about the coal miner strike at Warrior Met Coal in Brooklyn, Alabama, due to unfair labor practices.
- Description of the strike, lack of mainstream media coverage, and the support network established by the community.
- Insights into the disturbing tactics used by Warrior Met, including vehicular attacks on strikers.
- Kim Kelly's reflections on the stark contrast between the supportive community efforts and the violent attacks.
- Overview of the unique structure of the picket lines at Warrior Met Coal in Brookwood.
- Beau and Kim Kelly discussing the lack of political support for the striking miners from both Democrats and Republicans.
- Information about upcoming events organized by the striking miners and their families to protest in New York City.

### Quotes

- "One day longer, one day stronger."
- "It's been my personal crusade to get people to pay attention to this strike."
- "These folks just want to go back to work and make their money and live their lives."

### Oneliner

Beau and Kim Kelly shed light on the underreported coal miner strike in Alabama and the community's resilience against corporate oppression.

### Audience

Advocates for workers' rights

### On-the-ground actions from transcript

- Support the striking miners by donating to the strike fund or contributing to the strike pantry (suggested).
- Amplify information about the strike and the community support efforts on social media (exemplified).

### Whats missing in summary

In-depth analysis of the cultural and political dynamics influencing support for the striking miners.

### Tags

#LaborRights #CommunitySupport #AlabamaStrike #CorporateOppression #Unionizing


## Transcript
Well, howdy there, internet people.
It's Bo again.
And today we are going to be talking with Kim Kelly, one of the interesting
people we're going to have on the channel.
And she has some news for us.
Kim, why don't you tell us a little bit about yourself?
Hello.
Thanks so much for having me.
It's great to meet you guys.
My name's Kim.
I live in South Philadelphia.
I'm from the middle of nowhere, New Jersey.
Um, let's see.
I'm a freelance or independent labor reporter.
However you want to say that.
I used to be the heavy metal editor at Vice up until 2019 when it got laid off.
And by then my interest has shifted so much through the course of unionizing
advice that I was like, you know what, maybe I'll just jump into this whole
other world and see how that works out.
And so far so good.
You know, I've been reporting on all kinds of strikes and organizing campaigns.
And I have a book coming out in April and pre-order school
live for that on Thursday.
You can find more about that on my Twitter or my Patreon,
Patreon, however you say that.
But, um, yeah, I'm a little, obviously this past year I haven't really been
anywhere, but I spent a lot of time in Alabama and literally only Alabama since
February, and I've been focusing my reporting work on all kinds of interesting
things that are happening there.
And I'm excited to tell you guys all about it.
Okay.
So her Twitter handle is at Grim Kim, right?
Yep.
College radio DJ name that will never die.
Nice.
And, uh, and then you have, what's, what's the name of the book?
It's called Fight Like Hell, the Untold History of American Labor.
So you can guess what it's about.
Nice.
Okay.
So what were you doing in Alabama?
Well, I first went down there in February because this digital media
outlet, More Perfect Union had reached out to me and said, you know, there's,
you know, we noticed that there's this Amazon, uh, warehouse full of workers
who are trying to organize and trying to unionize down there, and we think
that's going to be kind of interesting.
Do you want to go down there and do some video content for us?
And at first I was like, ah, I don't know about all that, cause I'm not
really a video guy, or at least up until that point hadn't been a video guy.
I'm a writer, but I thought it was a really important story.
And I was like, you know what?
Screw it.
Let's go see what's going on.
So I went down there for about a month, two weeks, a while, for a while.
And I think I was probably one of the first, if not the first
meeting people down there.
And so I had a lot of time to get to know folks who are organizing, get to
know the workers, get to know the union people and get really invested.
And I ended up going back again for another couple of weeks later in that
campaign when things were heating up.
And then once their election had concluded and it became pretty apparent
that they're going to need to appeal and rerun it because Amazon's union
busting campaign was, well, if you've got the richest guy in the world at
the helm, you can get away with a lot of dirty, which they did.
So I went back down there a third time because they're having kind of a
campaign closing rally and I wanted to see my friends.
And while I was there, I heard, I knew kind of in the back of my head that
there is a coal miner strike happening about 16 miles outside of Bessemer in
Brooklyn, in a more rural area.
And my friend Chris, who was down there documenting the Amazon stuff from the
jump, he was like, oh, let's go out there.
Let's get some Krispy Kremes.
Let's bring some donuts out and go say hi and just support the workers on the
line. And we went out there and got to know some folks there.
And I was so interested in what was going on that I ended up getting even
more invested. And I've been back there two more times since just to report on
what's happening and work on this mini documentary series on it for the Real
News. And I'm just, man, I'm all in.
Like I text a bunch of them.
We have like a group chat.
Like it's a big part of my life at this point.
And yeah, it's been a while.
It is funny because literally Alabama is the only other place I've gone besides
my South Philly neighborhood since like early 2020.
And I'm not I'm hoping I get to go back soon.
It gets really hot there, though, so we'll see.
I'm just hoping if everything wraps up and they win a good contract and everyone's
good. But if I have to go down there and sweat my *** up again, I will happily do
it.
All right.
And the so tell us about the situation at the mine, because our viewers are
familiar with it. We covered it a little bit and I actually was trying to get up
there to their fundraiser and couldn't.
So we had to do everything online.
But so they're probably very curious about what's going on and what's happening
next. Right.
So, you know, the basics since April 1st, 1100 coal miners at Worry Mac Coal in
Brooklyn, Alabama, had been out on strike, an unfair labor practices strike,
basically because the company was not bargaining in good faith.
They're working on a new contract because their previous contract, which is
terrible, which they only accepted because the mine had gone into bankruptcy right
before Worry Mac came in.
And they were kind of presented with this devil's bargain of like, look, here's
the contract we're going to offer you.
Once we get on our feet and make some profits and get established, we'll take
care of you. And they took it.
And five years later, that is not what's happening.
So these folks have been on strike for a very long time now.
I think it's the April, May and June.
We're in the middle of July, three and a half months, three and a half months.
And it's been getting pretty ugly.
You know, there hasn't been a ton of major changes.
A ton of mainstream media coverage of it.
Lord knows I've been trying.
It's kind of become my personal crusade in a way to get people to pay attention to
this strike. But it's been difficult.
And these folks are kind of, you know, you're never on your own in the labor
movement. You know, labor leaders and rank and file folks from across the country
and in the community have been pitching in and sending in donations and coming up
to rallies. They've built a really robust, really mutual aid network, specifically
the auxiliary, which is primarily the spouses and retired members have built up a
really formidable strike pantry.
And they've been feeding people and getting school supplies for kids and taking care
of pregnant people. It's been really beautiful.
But the company has seen this and, you know, a lot of folks down there, the way
that they describe it is, you know, Warrior Met is trying to starve us out.
You know, they brought in scabs.
They have management and bosses working inside the mines, which I'm sure has been a
rude awakening for them.
They have engaged private armed security to patrol the picket lines and keep an eye on
things. They're flying drones over the picket lines.
And the most sobering and really horrifying aspects of the company's participation,
their attitude towards the strikers, has been this really, really awful uptick in
violence, specifically in vehicular attacks.
I think we are now up to four, four instances of cars and trucks being driven into the
picket lines and, you know, hitting people or hitting burn barrels that fly into people.
And folks have been sent to the hospital.
You know, the most recent incident, coal miners wife was hit and she was injured.
And the thing about that that kills me is that she, her husband, about a few weeks ago,
he was hit, too, went to his, he had to go to the hospital for a torn meniscus.
So like this entirely entire family is hurting.
And there's a lot of these families.
And, you know, it's been the vehicular attack part has been hard for me just as someone
who's invested and cares about these folks and since and also as someone who was at
Charlottesville's Unite the Right rally in 2017 and barely made it out alive.
Just seeing this tactic that has been embraced by fascists, seeing it used pretty much with
impunity by these company bosses and company employees against these striking workers.
It's just it sends a chill down your spine.
And I don't know how I worry about what's going to happen because these picket lines,
it's not, you know, there's 1100 workers.
That's a lot of people.
But the company early on, they acquired an injunction from the court that restricted
the amount of people that are allowed on the line.
At first it was six and they got up to 10.
That's still not very many.
And the line itself, it's not, you know, one of those big continuous like signs up triumph
for looking picket line.
Okay, and we are back after a short technical interruption for once.
It wasn't my Internet out in the boonies that crashed, but we are back right now.
So you were at the part where you were telling us what the line looks like and how it may
not be what we are picturing.
Right.
So when you hit the words picket line, there's this idea of a literal line of people walk
around in a circle holding up signs in front of a business or some kind of other establishment.
Right.
That's not what it looks like in Brookwood at the Warrior Met strike.
The Warrior Met coal mine is very big.
There's multiple entrances.
There's not a lot of through traffic, you know, not people will drive by and see you
on the line, but it's not quite the same thing as what you might be used to seeing in a town
or a bigger city.
There are 12 different outposts that are scattered around the outside of the mine, you know,
covering all the entrances.
And because there's a court injunction, thanks to the company, there aren't, you know, big
groups of people out there.
There could be because there's 1100 people on strike, but because of this injunction,
there are only legally allowed to be 11 people on or no 10 people on the line on in each
you know, in each section.
And actually, you know, I should that might not even be.
You're editing this stuff, right?
Yeah, probably.
Yeah.
Well, definitely.
I'm just thinking about it.
Like, this is I'm just talking to you, I'm just thinking like there's an injunction says
you can only have 10 people on the line.
And I'm thinking back to who, you know, because I visited a bunch of these different little
stations, and there's never 10 people on them.
So maybe it is 10 people total for all of those different outposts, which is even more
fucked up than I thought.
Man, I worry about the worst.
You can even keep that in me just like thinking through this.
But yeah, that I mean, it makes sense.
And a lot of folks have had their spouses and their families come out to support because
I don't think they're technically fall under the injunction because they're not employees.
They're not part of the union.
But the more that happens, one of the women I talked to a lot Hayden, she was telling
me that, you know, they've had t-ball games out there, people bringing the kids out and
having families there.
And it's, it's really nice atmosphere.
I've been down there a few times and, you know, brought treats and talk to the guys,
people sitting there.
But, you know, I'd already mentioned the vehicular attacks, but just the contrasting that environment
and that scene of like kids playing t-ball and coming out to see their dads and their
moms in the picket line with the idea of these truck attacks, like there's kids out there.
You know, they're the company has really shown no interest in recognizing that, you know,
it's not just the strikers who they're attacking.
It's potentially their families as well.
Like, as I said, a person's wife was just hit.
She's not in the union.
She doesn't work at Warrior Met, but she was targeted by association.
And, you know, she's, she's paid the price for supporting her husband, supporting her,
you know, her union family.
So it's, it's, it's pretty wild, you know, when you don't have cell phone service and
these things are happening, that can be pretty scary.
Like if I had, if my person was on strike and they're out on the picket line, I knew
this was going on.
I would be sitting at home terrified, just hoping that they make it home safe, which
is something that, you know, if you're married to a coal miner, that's a feeling you're already
accustomed to, but this is a whole other level.
Right.
Right.
And I've seen some of the, some of the incidents that have been caught on film and it isn't,
you know, when you're watching something like this, they can always frame it.
Well, this was an accident.
This wasn't.
It seems really hard to do that in these cases.
It's literally like, it's, you can't turn it up.
Like a big black pickup truck driving into a line of people, driving off of a road, turning
off for the road specifically to drive into these people.
And when they feel the impact and see a person in front of the windshield, continuing to
drive, I don't think you can spin that.
Right.
It's going to be real hard because eventually all of this stuff's going to end up in court
and the defense is going to have a hard time with that one, I think.
And I don't think Alabama has one of those laws about, I don't know the mechanics of
it, but there's a bunch, we know this, there's a bunch of laws that have been introduced
and maybe even passed across the country post 2017 that basically allow people to drive
into protesters and say that that's fine legally.
You can do that.
You can just try to murder people, I guess, America.
I don't think Alabama has one of those on the books, but that's something I've been
hearing from the folks there.
Like, you know, the response to their concern over these attacks is, oh, well, y'all were
in the road.
Get out of the way.
You know, you shouldn't be out there.
Like, yeah, I don't think that's, that warrants, you know, you running over someone's wife.
I don't know.
Maybe that's just me.
That's what you get.
Yankee liberal.
I've learned, I've spent a lot of time in Alabama.
I'm from Jersey, man.
I get it.
Oh, okay.
So what's happening next and what, there's some, there's some trips going on and stuff
like that.
Tell us about that.
Yes, it's super exciting.
They, so they, I guess it was maybe a trial balloon, a handful of minors, I think it was
about 14 minors from Brookwood and then local HUMWA officials.
They came up, it was only a few weeks ago, I think earlier, later in June, they came
up to New York city and they visited these three locations for these three different
venture capital funds, chief among them, BlackRock, who, you know, who support Warrior Met, who
fund Warrior Met, who have allowed Warrior Met to pay their executives millions of dollars
while these folks are working six hours a day, 12, no, six days a week, 12 hours a day
underground for like $22 an hour.
So they went up there and they had a little protest.
It got kind of rained out.
It was, it was really cold.
It was weird for June in New York, but you know, they, they had local union members come
out and rally with them.
It was really cool to see.
And now in later in July on the 28th, to be exact, they're coming back, they're coming
back to BlackRock and this time they're bringing their families, they're bringing a whole lot
of their friends.
I can say that I chat with the UMWA press guy pretty often about all this stuff.
It sounds like they've got at least six buses chartered full of striking miners and their
families who are coming up to New York city to show up in front of BlackRock and be like,
look, here's who you're hurting.
Here's what you're doing.
Here's what you're supporting.
We're not going to lie here and take this.
You know, they're asking for folks in New York and surrounding areas and the union world
and just people who support them in general to come out and show their support.
It should be a pretty big event.
Like I'm going up there again.
I'm really excited, especially because this time the wives are coming and you know, I'm
a little biased, I guess, but they have just been so incredible in the support networks
they've built and the mutual aid sort of effort they put together.
The amount of work they put into getting the strike out there.
Hayden Wright, who's, she's my girl.
She's been doing tons of interviews.
She's been kind of prodding her husband into doing interviews, really just getting the
word out and having those women up there, I think it's going to just ramp things up
to like a whole new level.
You know, there is this long history of women, lots of people, of course, but women specifically
in the coal mining world who have shown up and shown out for the striking miners, even
if they weren't working the mines themselves, they and their families are so closely tied
to it that it was their fight too.
Now this particular episode makes me think of like in 1983 when we had the great Arizona
copper strike and the women's auxiliary there was super active.
Those women went on a speaking tour across the country to bring attention to their plight,
to what their bosses were doing, to what their folks were fighting for.
And they went to New York and they went after Wall Street too.
And so these women from Brooklyn, Alabama, they're following in those footsteps.
They're following in the footsteps of Mother Jones, who they love to invoke.
You know, they're really bringing the fight to the front doors of these venture capitalists
who are profiting off of their labor.
And I can't wait to see it.
Yeah, it's one of the things that I love about being down here.
Like you keep saying that, you know, they've built these mutual aid networks and they have
and they exist and very, very few of them would ever use that terminology, you know,
like it's just the way it is.
And then when it swings into action, you have especially, you know, leftists from urban
areas who see it and they're like, you know, y'all built this great network.
Like, nah, we all just went to high school together, you know.
And there's this family ties and those church ties and that, you know, there's nobody else
around within five miles.
So we're friends kind of ties.
That's kind of the way I grew up too.
They wouldn't call it mutual aid, but it is, I will say just personally, it has been very
surreal and very funny to hear some of these older heads, like the older dudes, like yelling
about hating socialism and all that communism.
But then they go to their, you know, they pick up their strike check and they pick up
their free groceries at their union rally, you know, while they're on strike for the
labor union.
But they hate socialism.
But like, do they hate socialism or do they hate what someone told them socialism is?
I think, you know, that's a conversation.
I have a lot when people are like, you know, these leftists and I'm like, dude, you just
left the co-op and you're heading to the union meeting.
What are you, you're not, you actually are a leftist.
Yeah, like we all hate the government.
We all hate the boss that you're halfway there.
So what can people do to help?
Right.
So they, speaking of the strike pantry, the mutual aid effort, they do collect donations
for that.
There's Hayden Wright, at Hayden Wright on Twitter.
She's been, she's the mastermind.
She obviously tons of people are involved and are, you know, it's definitely a community
effort, but she's really good at social media and she's been kind of marshaling the effort
online.
You can find info on how to dotate on her Twitter.
And then the UMWA has set up an official strike fund that, you know, folks can donate there.
And that goes into the, to keep funding the strike, those strike checks.
And then these folks, they're not getting, they're not getting rich off of these checks.
It's like, I think about 600, $650 every two weeks, assuming that you put in your time
in the picket line.
And I think about 80%, if not more of the workers here are parents and schools coming
up then there, there's definitely an economic need there.
So if you have the ability to donate or just have the ability to signal boost and share
that link around so people who do have the ability to donate can ship in, it'll go a
long way because man, last time I was down there, the ladies were showing me around their,
you know, their hall, like their storeroom for all the donations, all the things they've
purchased for the strike, the strike pantry.
And they had a whole section devoted to school supplies because they're already planning
for September if they need to.
You know, they told me that they're already kind of putting together plans to do a toy
drive for Christmas.
Like they expect this to be a very long haul.
They're hoping it won't be because these folks just want to go back to work and make their
money and live their lives.
You know, being on strike isn't a walk in the park, especially if, you know, most media
in the world and the country isn't paying attention.
Even the people that you would hope or expect, like, I will tell you, this is what kind of
kills me.
I know for a fact that a lot of these folks are conservative or they consider themselves
to be conservative on the right side, on the right politically.
And they watch Fox News, they watch Tucker Carlson and all that.
One of my girls sends me Sean Hannity videos all the time.
And this is exactly the kind of story.
I mean, Tucker Carlson, hopefully rots in hell as soon as possible.
But this story of like these blue collar American working men and women going up against these
Wall Street elites, you know, that's exactly the kind of thing that he and his fan base
thrive on.
But their union and that's too much of a commie pinko thing for them to even look at.
So it's completely ignored them, even though like all these Republican politicians who
love to trot out coal miners for photo ops and talk about how they're going to save their
industry.
They're not showing up.
They're not even sending tweets.
They're not sending money.
They're not doing a damn thing to help these people, a lot of whom voted for them.
And it's like I my heart really goes out to them because they kind of they're kind of
backed in a corner.
And it's it's interesting to be a labor reporter and to go to a place like this and kind of
have to recalibrate the way that I understand and view like the culture and like the political
landscape, because it's I definitely haven't been around that many Trump supporters since
last month was my grandma's for Christmas.
So it's been an adjustment, but, you know, these folks are still hurting and I think
they they deserve our help.
Right.
And yeah, and that's what it is.
It's such a it's a very common theme on on my channel because it boils down to a lot
of rural people when it comes to economic policies.
They are left, but they're socially conservative.
And the the right wing in the United States drags out, you know, the symbol of the Bible
and the flag.
And it motivates them and it it it drives them and they end up voting against their
own economic interest.
But in this case, I haven't seen.
This is also an example of when the Democratic Party could, in theory, reach out, you know,
show up, give some support.
And you don't see that either.
I will say I did hear an anecdote from someone who was actually involved in Strikefest.
So he was he was pretty involved in all this.
He told me that someone from Bernie Sanders's orbit had reached out and said, you know,
the senator wants to send she sends a pizza.
She said, can you send something to support?
And it sounds like they reached out to one of the local union officials.
And the guy was like, oh, we don't we don't want that done here.
Like we don't want Bernie to say anything.
And it's like, I OK, if you I get it conceptually, but like, bro, you really need all the help
you can get.
My guy like it is like it's the but none of the other Democrats even tried.
And it's like maybe they're afraid because it's fossil fuel related.
Maybe they're afraid they wouldn't be welcomed, though.
I will say more locally, the Alabama Democrats have they don't they've collected a whole
lot of donations and donated a ton of stuff to the strike pantry.
So on a more local level, I think there's those those divisions, partisan divisions
or whatever, a little more fluid.
But yeah, you're not going to get a congressional delegation to Brookwood, even though it's
only 16 miles outside of Bessemer.
And like half of Congress showed up there when Amazon is organizing.
So it's like, yeah, there's a golden opportunity here to show like a bunch of people who think
you suck that that you actually care about them.
But as per usual, the Democrats are whiffing it.
Right, right.
In this regard, they definitely are.
And it's it's it's sad, but it is what it is.
So your book is on preorder now, you said.
It's the link comes out on Thursday and I'm talking to you on Wednesday.
So I can I can send it to you.
I'm going to put it all over the Internet.
It's going to I guess I can say it'll be out on Simon and Schuster's One Signal imprint
on April 26, 2022.
So just in time for May Day.
Yeah, I've got I mean, I still write in the damn thing, but now there's a little more
pressure.
Right.
So yeah, definitely send it because we're going to have to do the editing on this one.
So oh, that's right.
It'll be live.
It'll be live.
Maybe maybe I should re-say that in a way that is that.
It's probably fine.
Yeah, it'll be fine.
I don't worry about it.
It's a very informal channel.
It's all right.
I've got my like, well, I'm going to make sure I got my ring light.
I got to make sure I have my sound bites ready.
Like, I don't know, man, spent half my life on Zoom trying to talk to people for the past
year.
But yeah, this is much more fun than having to explain what like what a union is or like
why we should actually care about people, even if they voted for something.
Oh, man, I've gotten some of the wildest like commentary because I've been doing all this
work in Alabama and there are so many conceptions about the Deep South and about Alabama and
like sure, some of them are true.
I'm not here to I'm not the Alabama whisperer, but I've learned a lot being there and I've
about the history and about the folks who are there now and the folks who are there
now trying to make things better for themselves and their communities and for everyone.
Like it's you right off the South, you right off rural areas at your peril.
You know, right.
And it's it it happens there.
There's so many misconceptions and stereotypes about this kind of stuff.
And then people have like anecdotal things, you know, over the last two months or so,
I've had three or four people like, you know, when I was traveling in the South, you know,
they invited me to go to this organization, this meeting.
And I'm like, OK, and they're assuming that it's the Klan.
And I'm like, yeah, they were probably inviting you to a church group if they were inviting
you to the Klan.
That says a whole lot about you because I've lived down here a long time and nobody's ever
offered to take me.
Oh, man, it's wild.
I've traveled a whole bunch because I used to tour with heavy metal bands.
So I've kind of been everywhere in the country and I used to tour specifically with Southern
heavy metal bands.
So I got kind of a crash course in getting rid of some of those liberal Yankee ideas
I might have had when I was younger.
And I just wish more people could have that experience, if not touring with a heavy metal
band, but like paying attention to other parts of the country that maybe you're not as familiar
with being like, oh, they have people there.
Right.
Amazing.
Who knew?
Right.
So, yeah.
Okay.
So can you think of anything else?
Anything that anybody needs to know, information that they need to have?
I mentioned Hayden's Twitter.
There've been, oh, the Valley Labor Report.
They're great.
They're based in Alabama.
Those guys have been on top of the strikes since the beginning.
They planned that Alabama Strike Fest that I know you took part in and your wonderful
reader or watchers, viewers contributed a lot of support too.
That was great.
They've been, they're really, really great.
They need, they deserve a ton of support for doing what they do, where they do it and amplifying
these worker struggles in the deep South.
And they're a talk radio show.
So they're always fun to listen to.
So yeah, keep an eye out for them and for their coverage.
UMWA is, you know, they're as much fun maybe message-wise, like me or Valley Labor Report,
but they've got a lot of good info too.
Yeah.
It's funny how social media has become such a big, you know, information, informational
aspects of the strikes.
I mean, that's, we've seen that happening a lot over the past few years, obviously,
but this is more of a Facebook crowd, but Twitter's where all the action is in terms
of people paying attention.
And I made a bunch of the Miners Facebook groups and it's like a whole other world,
but Twitter's a good place to keep an eye on the developments, I would say.
Yeah.
Yeah.
It's one of those, Twitter is definitely a mixed bag because you can get information
so much faster, but there's a lot of other stuff that you have to sort through.
I will say, Coal Miner Facebook is a trip.
Man, there's, that's the thing, like there's, I'm a reporter and stuff, but I'm also very
biased and very sympathetic.
And like, there's a bunch of stuff that I've seen that I can maybe report on, you know,
there's, these are coal miners, they're in the UMWA, there's a long history of fighting
back against bullshit.
And you know, they're not taking any of this lying down, but they're not driving trucks
in anybody either.
So, you know, we'll just leave it at that.
Right.
That tends to be how it works.
It's normally defensive.
Yeah.
So.
Nice.
So
coming into my mentions and telling me the coal is bad. I know. It's not, you know, you don't need
to tell me. It's so weird. Some of the quote tweets I've gotten are like, oh, well, why are we
supporting coal miners? They're destroying the planet. Like, yeah, these specific people have,
they're specifically signed up to destroy the planet. That's what they want to do for themselves
and their kids. They're not just taking the only decent job within a 50 mile radius that three
generations of their family was part of that offers them a union and a decent living. No,
they want to set the world on fire specifically. I hope I can say swear words on here because I've
said a bunch. I think Carrie, you're probably going to clean that up. I think when it gets
edited, some of it will probably be bleeped out or it'll just be dead air. I don't know how it's
going to work. We'll figure it out too, but I feel animated about these things. It's fine. It's fine.
You're not offending me or anybody really. So family friendly show. Yeah. All right. I think
that's all I got, you know, keep an eye out and you know, one day longer, one day stronger.
All right. Okay, everybody. So that's the show. It's just a thought y'all. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}