---
title: Let's talk about closing Guantanamo....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=C-envUtGol4) |
| Published | 2021/07/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A facility is challenging to close, and there are lessons to learn from it.
- Guantanamo Bay, where the United States detained people for about 20 years, is the focus.
- The Trump administration showed no interest in addressing the Guantanamo issue.
- The Biden administration approved a deal to let one person leave Guantanamo for Morocco.
- Some detainees in Guantanamo were based on inaccurate information, faced harsh conditions, and many were never charged.
- There is no military or intelligence application for keeping most detainees in Guantanamo.
- Politics, not military necessity, is the main reason for the delay in closing Guantanamo.
- The fear of potential political fallout if a released detainee commits an offense is a significant barrier to closing Guantanamo.
- The facility exists because the US violated its own principles, and the decisions made have continued the cycle.
- Beau questions if it is politically feasible to close Guantanamo due to the risks involved.
- He stresses the importance of standing by principles, even when it's difficult and fear-driven decisions lead to violations.

### Quotes

- "If what we believe to be right and good and true is right and good and true, we have to stand by those principles no matter how hard it is."
- "This place exists because the United States violated its own principles."
- "It's not an intelligence thing."

### Oneliner

Beau questions the feasibility of closing Guantanamo due to political risks, stressing the importance of standing by principles despite past violations and fear-driven decisions.

### Audience

Politically-aware individuals.

### On-the-ground actions from transcript

- Advocate for the closure of Guantanamo and the fair treatment of detainees (implied).
- Support politicians willing to prioritize principles over political fallout in addressing Guantanamo (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the challenges in closing Guantanamo Bay and the political implications surrounding the issue.

### Tags

#GuantanamoBay #Politics #HumanRights #UnitedStates #Principles


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about why a certain facility is
so hard to close and the lessons that we can learn from it.
You know, for a while, for years, this was a big topic.
It was discussed a lot.
It was in the news cycle all the time.
When are we going to shut this place down?
What are we going to do with these people?
There were a lot of people who were very interested
enclosing this chapter of American history. Then under the Trump administration it fell out of the
news cycle because everybody knew the Trump administration had absolutely no interest in
rectifying the situation. The Biden administration has approved a deal that's going to let one person
leave this place. They're basically headed to Morocco. They're gonna be on
parole, kind of. In the process of approving this deal, they have expressed
lukewarm interest in shutting down Guantanamo. Now when they say that, I'm
certain they just mean the more notorious part of Guantanamo, not the
entire facility. If you don't know, Guantanamo is where the United States
warehoused people it believed to be high value for about 20 years. Some of the
people that were there were led there based on less than accurate information.
Some pretty horrible stuff happened there. They were held there for a very
long time, many of whom never had charges. Now, the question that kind of caught my
interest about this is, what's the military application for keeping these
people? Oh, that's easy. There's not one. There's not one and there hasn't been one
for a very, very long time. There's no intelligence gathering going on anymore.
These people have been out of circulation so long, they don't even know
the names of the people on the field. There's not a military or intelligence
application anymore. At its peak, this place held 600 to 700 people. Today
there's less than 40. Of those, about a dozen, I think the actual number is 11,
they're not going anywhere. Like even if they leave Guantanamo, they're just
going into another cell. They will probably never see a free day again. The
remainder from an intelligence or military standpoint, yeah they could
probably be released the same way this person is. You know, an intelligence
agency in their home country say, yeah we'll monitor him, he's gonna come here
and clean pools for his brother, and that's really what this guy's going to
do in Morocco, and US intelligence monitors them as well. That could be done.
So why isn't it happening? It has nothing to do with the military intelligence. It
is politics. What if something goes wrong? What if one of these people that's
released, well what if they do something? That politician that approved that
release and everybody who had anything to do with that decision, their careers
are over. That's why it's taken so long. And that political math, it's going to be
valid today and valid 20 years from now. That's what's taking so long. Because
whoever releases them has to believe that these people who have been held for
a very long time without charges, that they're not going to do anything. They're
putting their political futures in the hands of those being released. It seems
really unlikely. It seems really unlikely. That's the holdup. It's not a military one
or an intelligence one anymore. This is like any situation. When the gloves come off, it's
really hard to put them back on because the law of unintended consequences. This place
exists because the United States violated its own principles. That's why
it exists. And once that happened, well, those decisions keep getting made. How do
you close places like this? You never let them open. It's one of those Pandora
box things. The political reality is I don't know how it could be closed.
Doesn't matter if it's moral or if it's the right thing to do. I don't know
politically how it would work out because that's going to require a
very brave politician. That's going to require them to care more about the
principles of the United States than the political fallout that could occur.
I mean, regardless of what led them there, regardless of the fact that
they were held without charges, regardless of the absolutely inexcusable
stuff that occurred down there, they aren't necessarily good people. They may
do something bad, which would impact the political futures of those who release them.
It's a realistic possibility.
And because it's a realistic possibility, when it comes to politics, certainty is more
important than morality.
I don't see the path to closing this place, because we're not going to bring them to
the US.
I mean, that might give them access to US courts.
And given some of the stuff that went on, I mean that doesn't seem like a good idea
from the government standpoint.
Just letting them go and letting them go home, I mean that's begging for a political disaster.
There aren't a lot of options on the table politically when you're talking about reality
and how things actually work.
Because once those ideals get violated, it's hard to fix it.
that's where we're at. But make no mistake, the people down there now, it's
not an intelligence thing. And with the exception of that dozen or so that is
never leaving, there's probably not a legitimate reason to hold any of them
outside of that 11 or 12. Those people are, those people are
ever going anywhere. So that's the reality of it. Now should this place be
closed? Yeah. How it's gonna happen? I don't have a clue. I don't have a clue. I don't
see a realistic path forward. The key takeaway, the real lesson here, is that if
what we believe to be right and good and true is right and good and true, we have
to stand by those principles no matter how hard it is. And we didn't. We let fear
get the better of us, we violated our own principles, and here we are 20 years
later, confused as to how to get them back. Anyway, it's just a thought. Y'all
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}