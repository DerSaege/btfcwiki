---
title: Let's talk about how to trust the experts when they disagree....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=a_CO-lGefCM) |
| Published | 2021/07/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the topic of experts, consensus, and new guidance, specifically regarding wearing masks in schools.
- Responds to a message pointing out that he is not a medical expert but has some medical training.
- Acknowledges the challenges of determining which expert to trust when there are disagreements within a field.
- Notes that true consensus among experts requires statements that are painfully obvious and inarguable.
- Mentions the guidance from the Academy of American Pediatrics recommending kids wear masks when they return to school.
- Comments on the tendency for people to question experts in fields where they lack training.
- Emphasizes the importance of specialization and relying on the expertise of others.
- Stresses that there is overwhelming consensus among medical professionals on the effectiveness of masks and vaccines.
- Encourages kids to follow basic hygiene practices when they return to school.
- Concludes by sharing his thoughts on the matter and wishing everyone a good day.

### Quotes

- "Specialization is something that humans do very well."
- "There isn't a real argument when it comes to this topic."
- "Everybody knows, because it's a painfully obvious truth."
- "When kids go back to school, wash your hands. Don't touch your face. Wear a mask."
- "Y'all have a good day."

### Oneliner

Beau addresses the importance of trusting experts and consensus, particularly on the topic of wearing masks in schools, stressing the need to rely on specialization and the overwhelming consensus among medical professionals.

### Audience

Parents, educators, policymakers

### On-the-ground actions from transcript

- Trust the guidance provided by medical professionals when it comes to wearing masks and vaccines (implied).
- Encourage children to practice good hygiene habits, such as washing hands, not touching their faces, and wearing masks when they return to school (implied).

### Whats missing in summary

Beau's engaging delivery and perspective on trusting experts and consensus can be best appreciated by watching the full video to grasp the nuances of his arguments.

### Tags

#Experts #Consensus #Masks #Schools #Trust #Specialization #Hygiene #MedicalProfessionals


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about experts and consensus
and some new guidance.
Before we start, I want to point out
that I had no intention of making a video about this
because it was kind of obvious where I would stand on it.
Seemed a little pointless.
But I got a message.
So I feel like it brings up a very good point.
It's something that maybe gets overlooked.
The message says, Beau, before you
make your video about the new requirement of kids
wearing masks, there's another term here,
masks in school come fall, please
remember that you are not a medical expert.
That's true, I'm not.
And that you have no medical training,
that's actually not true.
I do have some.
But nowhere near enough to argue with the experts
in their field.
You always say to trust the experts,
but I've seen you argue with people in your field.
Which expert do I trust, Beau?
Which one?
That's a gotcha question right there.
I am absolutely stumped because you're right.
People who are in the same field that
have a lot of knowledge, experience, expertise,
whatever, they do.
They argue a lot.
That's true.
That's absolutely correct.
In my field, to get everybody to agree so there isn't
an argument about it, it would have
to be a statement like, invading Middle Eastern country
acts is likely to cause pockets of irregular resistance
or something like that.
It would have to be a statement so obvious that you don't
actually need an expert for it.
It would have to be a statement that is so painfully obvious
that it should just seem like common sense.
If you want to get that kind of consensus, people in my field,
that's what it had to be.
It would have to be a statement that is just inarguable.
Something that is absolutely true no matter what.
That's the only way you're going to get that without argument.
If you want a real consensus, we're talking like 99.9%,
the kind of consensus that exists
among medical professionals when it
comes to masks and vaccines, that's what you would need.
Something inarguable.
Something that experts couldn't find any divergence on.
Just everybody knows it's true.
That's the only way you get consensus
like that among experts is if it's just kind of obvious.
So the Academy of American Pediatrics,
American Academy of Pediatrics, or something like that,
they put out guidance saying that when kids go back
to school, they should wear masks.
That means that when kids go back to school,
they should wear masks.
That's kind of painfully obvious.
The desire of people to question experts in their field,
when they have no or little training in that field,
it's something new.
I mean, it's always happened.
But the idea that those concerns should
be given as much weight as the people who dedicated their life
to the study of a subject, that's new.
That's new.
People didn't used to expect that.
And the thing is, people will do it
when it comes to political issues,
but not with everyday issues.
You know, if you take your car to the mechanic,
and the mechanic's like, hey, it's not your battery.
You need a new alternator.
Do you believe them, or do you argue with them and say,
well, you know, I read on the internet that it could be.
I mean, really, if your plumber says you need to do this,
do you argue?
Specialization is something that humans do very well.
The benefit of specialization means
that you don't have to know everything,
that you can rely heavily on the work of others.
And that's one of the things that moves society
as a whole along.
There isn't a real argument.
There isn't a real argument when it comes to this topic.
There's no both sides on this.
Medical professionals, medical experts,
overwhelmingly have a consensus that masks and vaccines work.
The fact that some news outlet desperate for ratings
put up the one outlier doesn't mean there's
an actual debate in the field.
Everybody knows, because it's a painfully obvious truth.
So yeah, kids, when you go back to school, wash your hands.
Don't touch your face.
Wear a mask.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}