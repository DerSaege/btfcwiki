---
title: Let's talk about DARPA, ARPA-H, and Republican objections....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gT7HXaDJnAs) |
| Published | 2021/07/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Advanced Research Projects Agency for Health, Biden's new agency for advanced health research.
- Republicans have concerns that if it's rolled into NIH, it will adopt NIH's culture and undermine its purpose.
- Draws parallels between the new health agency and DARPA, the Defense Advanced Research Projects Agency, known for groundbreaking advancements.
- DARPA's unique organizational culture and operational methods lead to significant technological breakthroughs.
- DARPA operates with a small team of 200 employees, offering leaders in the field autonomy, urgency, and a fixed timeline to solve problems.
- The agency looks at long-term solutions, fostering incidental developments while focusing on major challenges.
- DARPA's approach includes funding external projects and maintaining a nimble, unorthodox behavior uncommon in government agencies.
- Beau stresses the importance of replicating DARPA's culture and dynamic nature for the new health agency to achieve impactful breakthroughs.
- Emphasizes the need for autonomy, different culture, and visionary leadership to ensure the new agency's success in saving lives.
- Urges for the replication of DARPA's fluidity and dynamic approach in the new health agency to drive innovation and positive outcomes.

### Quotes

- "If they want the health version to have the impacts that DARPA does, it's got to have that culture."
- "It has to have the leaders that understand what the goal is."
- "If they can duplicate the fluidity, the dynamic nature of DARPA, I have no doubt this agency will create amazing breakthroughs and save lives."

### Oneliner

Beau explains the need for replicating DARPA's culture in the new health agency to drive innovation and save lives effectively.

### Audience

Health policymakers and researchers.

### On-the-ground actions from transcript

- Ensure that the new health agency maintains autonomy and a distinct culture (implied).
- Advocate for visionary leadership that understands the agency's goals (implied).
- Support efforts to replicate DARPA's dynamic and fluid operational approach in the new health agency (implied).

### Whats missing in summary

The full transcript provides a detailed comparison between DARPA and the new health agency, underscoring the importance of organizational culture in driving innovation and breakthroughs in health research.

### Tags

#HealthResearch #Innovation #DARPA #OrganizationalCulture #Autonomy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the Advanced Research Projects Agency for Health.
This is Biden's new agency.
It's supposed to provide a lot of advanced research for health-related issues.
Republicans have come forward with a concern.
And while I'm fairly certain their concern is politically motivated and it's an attempt
to undermine the National Institutes for Health, they're also kind of onto something.
I don't believe that they're coming from the same place I am, but they're kind of onto
something.
The concern is that if it's rolled into NIH, it will adopt NIH's culture.
The idea of this agency is to mimic DARPA, the Defense Advanced Research Projects Agency.
Now a lot of people are familiar with that name, but they may not be familiar with the
accomplishments of this agency or how it works.
It's very different.
So let's point out, first, you probably wouldn't be watching me without it, because the internet
itself has roots in DARPA.
GUI, the Graphical User Interface, I think is what the acronym stands for, has its roots
in DARPA.
The mouse has its roots in DARPA.
GPS has its roots in DARPA.
Satellite constellations have their roots in DARPA.
This doesn't even touch on stealth aircraft or stealth ships or unmanned vehicles or all
the stuff we don't know about.
Those creepy little robot dogs that every time I see I'm simultaneously amazed by the
technology and also trying to figure out how to destroy it when Skynet takes over, they
have a tie to DARPA.
Siri, can you think of anything else?
Siri's a spin-off of a DARPA project.
The way they accomplish this is kind of unique.
DARPA has like 200 employees, an agency with 200 employees can claim ties to some of the
most advanced technological breakthroughs of the last half century.
It's because of the organizational culture, because of how they operate, because of what
they do.
It doesn't function like a normal agency.
They even have legislated exemptions on how they hand out contracts.
Because the way it kind of works is a DARPA employee shows up and they're like, hey you,
genius, leader in your field, come with me.
This is your team, this is your budget, this is your problem, you have three years.
It's kind of how it works.
So it creates a sense of urgency.
And the problem isn't an immediate one.
They're looking 10 or 20 years down the road, trying to solve that.
So along the way, they incidentally develop other stuff.
A good example would be, hey, we want you to develop secure communications.
So along the way, the Onion Router gets developed.
That actually happened.
The Tor also has its roots in DARPA.
That's the idea.
They also fund projects that are ongoing outside the agency.
Hey you're working on this thing, here's some money, we want to know about this along the
way.
You know, it's often said you can't solve problems by throwing money at it.
That's not true if the problem is a lack of funding, which in health research is kind
of the case a lot of times.
If they want the health version to have the impacts that DARPA does, it's got to have
that culture.
It has to be that nimble.
It has to be able to behave in a pretty unorthodox manner when compared to other government bureaucracies.
So while Republicans are probably just coming from the normal space they've been coming
from, they're kind of right on this one.
This agency, it can be rolled into NIH, but it has to be really autonomous.
It has to have an entirely different culture.
It has to have the leaders that understand what the goal is.
And if they can duplicate that, if they can duplicate the fluidity, the dynamic nature
of DARPA, I have no doubt this agency will create amazing breakthroughs and save lives.
And that's the goal.
So it may be wise to make certain that this new agency mimics more than just the name
of DARPA.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}