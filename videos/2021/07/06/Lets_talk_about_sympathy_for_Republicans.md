---
title: Let's talk about sympathy for Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ElDWx21-M-c) |
| Published | 2021/07/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses sympathy for Republicans, specifically politicians, for being in a difficult situation.
- Criticizes Republican politicians for blindly following Trump, even though he doesn't support representative democracy.
- Points out that politicians are motivated by money and power, which are now controlled by Trump.
- Warns politicians that their loyalty to Trump won't protect them in the end.
- Urges Republicans to unite against Trump to protect their party and country.
- Emphasizes the consequences of not standing up against authoritarianism.

### Quotes

- "You're not representing the people you're sent there to represent."
- "Authoritarians don't care about loyalty. They care about obedience."
- "Recognize what he is and what he's going to do to your party and to this country."
- "He will pick you off one by one and replace you with the obedient."

### Oneliner

Beau expresses sympathy for Republican politicians but warns them to unite against Trump's authoritarianism before it's too late.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Unite against authoritarianism to protect democracy (suggested)
- Recognize the true motives of politicians and hold them accountable (implied)

### Whats missing in summary

Insight into the urgency of standing up against authoritarian leaders like Trump.

### Tags

#Republicans #Trump #Authoritarianism #Unity #Democracy


## Transcript
Well howdy there, Internet people. It's Beau again.
So today we're going to talk about sympathy for Republicans.
Because I really do have it.
I have a lot of sympathy for the situation they're in right now.
I'm not talking about rank and file Republicans. I'm talking about politicians.
Because I still don't think they get it.
After all this time,
they don't get it. I read article after article this week, so many that it almost
seemed coordinated, that basically said if you're a Republican politician and
you disagree with Trump,
well your political career is over. And so Republican politicians have been
playing this game. And it makes sense to do that in a representative democracy.
You back the popular person. And that's normally a smart political play.
The thing is, Trump doesn't support representative democracy.
He's made that really clear. He's an authoritarian. A role.
But they're still playing by that other game.
They're still playing by the rules to a representative democracy.
They just do whatever he says because he's the popular person.
They have to do whatever he says. They can't disagree with him or they'll lose their seat.
Why does their seat matter?
If you were to ask one of these politicians in Congress,
why are you running for Congress? They'll give you the canned answer.
I'm there to represent the people of the great state of whatever.
Right? That's what they'll say.
But we know that's not true. Because that's not what they're doing.
They can't do that because they just have to do whatever Trump says.
They're not representing the people of anywhere. They're doing Trump's bidding.
Following his orders. Marching in step because they don't want to offend him.
So their canned answer just doesn't make any sense, now does it?
But let's pretend for a second that we're not a bunch of naive children.
You know, let's drop the facade for a moment.
That's not really why they're running anyway.
Why are they really running? Money and power, right?
That's why politicians want to get to Capitol Hill.
Money and power. And in a representative democracy as it was functioning in the United States,
the money grows from the power. See? There's that problem again.
They don't have any anymore. They just have to do whatever Trump says.
And that power flowed to Trump.
Guess what's going to happen with that money?
How long until the contributors realize,
I don't need to buy a senator. I don't need to buy a congressman.
I just need to get access to Trump.
Then the contributors start going his way.
You're already seeing it happen. That's why his PAC is making so much money.
There is no motive to continue to do whatever Trump says.
There's no self-interest in that.
Because your seat that you are trying so desperately to save, it means nothing.
Even if you are idealistic and you are there to represent the will of the people of your district or state or whatever,
yeah, you can't do it. Because you have to do whatever Trump says.
Now for the 99% of people up there, you're not going to make the money anymore.
You don't have that influence. Trump took it.
And you let him. Because you didn't recognize what he was.
Even when he told you, he made it really clear who he was.
And you ignored it. And I know there's some that are saying,
no, see, I am a MAGA loyalist. I'll be fine.
No, you won't. No, you won't.
Eventually you will do something that will incur his displeasure.
And that's it.
Since I know most of these politicians don't own history books,
I would suggest that they hop online and look for images of the authoritarians of the past.
Look for images of their inner circle.
When they came to power.
And then look for images of their inner circle at the end of their reign.
What you're going to find out is that there's only one face that's the same in both photos.
Doesn't matter if you're loyal.
Because authoritarians don't care about loyalty.
They care about obedience.
They're not the same.
And eventually something minor will irk him.
And that's it. You're done.
But it doesn't matter because you can't do anything anyway.
You can't represent the people you're sent there to represent.
And you can't feather your own nest.
I don't know why you're putting up with this headache, to be honest.
There doesn't really seem to be any clear motive.
You know, there's really only one thing that the Republicans can do in this case.
And that's unite against this man of style and taste.
Come together.
Recognize what he is and what he's going to do to your party and to this country.
Because if you don't unite now, you're going to fall separately later.
He will pick you off one by one and replace you with the obedient.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}