---
title: Let's talk about criticism of the Capitol cops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rB8NYIgDC3A) |
| Published | 2021/07/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the criticism of officers' testimony regarding the events of January 6th and the implications of this criticism on their emotional state and future outcomes.
- Initially found it amusing when critics like Tucker Carlson questioned the officers' courage, but shifted perspective after understanding the implications.
- Observing how individuals, even those traditionally anti-police, defended the officers against accusations of weakness.
- Acknowledging the importance of facing worthy opponents in warrior culture, not weak ones.
- Noting the impact of criticism on officers' emotional and psychological well-being, especially when coming from influential platforms like Fox News.
- Sharing stories of military veterans who developed PTSD from specific moments, illustrating the complex nature of psychological trauma.
- Criticizing pundits like Tucker Carlson for downplaying the severity of potential PTSD triggers and emotional distress in combat situations.
- Challenging the misconception that toughness and courage prevent individuals from experiencing PTSD.
- Emphasizing the significance of seeking help for PTSD and the bravery it takes to confront mental health challenges.
- Critiquing the reckless messaging of pundits who prioritize talking points over the well-being of those affected by trauma.

### Quotes

- "Real warriors want real opposition."
- "The toughest people I know all have PTSD."
- "Needing help does not make you weak."
- "They reinforce the idea that needing help makes you weak."
- "This is a very reckless set of messaging."

### Oneliner

Beau examines the criticism faced by officers, challenges misconceptions about PTSD, and calls out reckless messaging undermining mental health support.

### Audience

Advocates for mental health

### On-the-ground actions from transcript

- Support organizations aiding individuals with PTSD (implied)
- Advocate for mental health awareness in communities (implied)
- Challenge stigmas around seeking help for PTSD (implied)

### Whats missing in summary

In-depth personal stories and detailed analysis of the impact of criticism on officers' emotional well-being.

### Tags

#PTSD #MentalHealthAwareness #PoliceCriticism #CombatTrauma #RecklessMessaging


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about the testimony of the officers who were there
on the 6th, the criticism of that testimony and of their emotional state,
and the implications of that criticism and what may happen because of it.
Now, I have to be honest, when I first heard the criticism coming from people
like Tucker Carlson about those officers, I found it funny.
I found it funny because I'm working on a couple of large projects right now.
By the way, y'all may only get one video a day this week.
And I was around a bunch of people who are anti-cop.
And by anti-cop, I don't mean they're police accountability activists.
I mean they're anti-cop.
But after Tucker implied that these officers were weak and lacked what it
took and all of that stuff, you had people, had a guy with a 1312 tattoo
taking up for the cops because he's a warrior, right?
And having a badge, that doesn't get you any respect from him.
Standing fast against 101 odds does.
If you get into warrior culture, the reality is you don't want to face a weak opponent.
You want to be pitted against the best.
Think about the movies you watch, right?
The general comes in to give that speech, and he's always like,
this is the weakest, most disorganized opposition the United States has ever faced.
No, you never hear that because they're dedicated, right?
Real warriors want real opposition.
Because I realized that Fox News has a lot of vets that watch it,
I don't think it's funny anymore, especially now that President Trump,
former President Trump, is reported to have said that they were wimps,
not his term, and that basically attacked their courage and character.
It means that this type of criticism of these officers is going to continue
on Fox News, where there's a bunch of vets and right-wing pundits,
many of whom will promote doing 22 push-ups a day to raise awareness of the issues
that people with PTSD face and the negative impacts of that.
They will mock these officers' emotional and psychological state.
Because it's not like it was real, you know?
It's not like they were getting shot at.
Yeah, okay.
So I got two stories for you.
I know a guy, he did more than 20 years in the Army, real warrior, real warrior.
Lots of engagements, lots of firefights over the course of his career.
He has PTSD.
Of course he does, because he was in lots of firefights.
He'll be the first to tell you that and why he has it.
He is certain that he has it because of one mortar, a mortar, the day he was leaving.
And he will be the first to tell you.
Objectively, it was nowhere near him, but he just pictured it being walked
in on top of him.
And he says that it's the only time he let the fear get the better of him.
And he is certain that that is what caused it.
Another guy, he did six or eight years enlisted.
Then he got out and went to college.
During those six or eight years, he had a couple of deployments
and he saw some stuff.
He had no issues, none, none whatsoever.
After college, he went back as an officer and he developed PTSD.
And his is pretty debilitating.
It takes place in like flashbacks like you would see in a movie.
And his is from something he said over the radio that had a negative outcome
on the other end.
I'm going to suggest that people like Tucker Carlson are not in a position
to judge whether or not somebody's PTSD, their psychological state,
their emotional state is appropriate when it comes to combat.
And I know this same crowd, they're going to say, hey,
it wasn't really combat.
In those movies that you watch, because you have never dealt with it yourself,
in the movies, how do they let the viewer know that it was really tough combat?
It devolved into hand to hand because it's chaotic, traumatic maybe.
These pundits, these people are setting the stage
and they're casting the idea that, well, these cops,
they're just not tough enough.
That's the reason they're like that.
Yeah, Department of Defense, veterans groups,
they have worked really hard to not equate toughness, masculinity,
with needing help for PTSD.
It's been a battle for years.
Undermine that and you're not going to be doing 22 push-ups a day.
You're going to be doing 25.
People like Tucker Carlson, who have no idea what they're talking about,
are not in a position to judge whether or not a certain event was enough
to cause PTSD, whether or not it was enough to cause emotional or
psychological distress.
They have no clue.
They do not know.
The idea that people who are tough, people
who have the right character or right level of courage, don't get PTSD,
that's garbage.
It's not true.
And you're not going to hear that from anybody who has any clue what they're
talking about.
You're just going to hear from a bunch of wannabe tough guys
who are not going to be able to do what they're supposed to do.
And they're not going to be able to do what they're supposed to do.
You're just going to hear from a bunch of wannabe tough guys on TV.
The toughest people I know all have PTSD.
And for many of them, the toughest thing they ever did
was admit it and get help because that image exists.
I mean, setting aside the hypocrisy of the law and order crowd,
criticizing these cops for, I don't know,
being scared when they were outnumbered 101, set that aside for a second.
This is a very reckless set of messaging.
These people care about talking points.
They don't care about their viewers.
That's pretty clear.
Because you reinforce the idea that needing help makes you weak
and people don't get help.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}