---
title: Let's talk about San Jose's new gun control initiative....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-zzt-iSMcyE) |
| Published | 2021/07/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- San Jose is planning to impose a fee on firearm ownership, but legal challenges are expected due to charging money to deter a constitutional right.
- The initiative aims to deter gun ownership and offset violence costs, but the impact may disproportionately affect certain demographics, particularly the poor.
- Systemic issues like institutionalized racism and inherited disadvantages could make it harder for poor individuals to afford firearm ownership, leading to violations of the law.
- The law is colorblind on the surface, but its enforcement could have racist impacts due to existing conditions and systemic issues in the country.
- Beau suggests that laws like this can have unintended racial consequences and may be selectively enforced, showing how colorblind laws can lead to discriminatory outcomes.
- Acknowledging the intersection of race with the law is vital for building a fair society, whether through evidence-based arguments or storytelling narratives.
- Building a country where everyone gets a fair chance requires confronting and addressing systemic issues and acknowledging the truth, whether through data or personal stories.
- Beau challenges the reluctance to openly address racial issues and suggests that ignoring these issues may indicate a desire to maintain existing discriminatory systems.
- Encourages critical thinking about how race intersects with the law and the importance of discussing theories that illuminate racial disparities.

### Quotes

- "Systemic issues like institutionalized racism and inherited disadvantages could make it harder for poor individuals to afford firearm ownership."
- "Laws like this can have unintended racial consequences and may be selectively enforced."
- "Acknowledging the intersection of race with the law is vital for building a fair society."
- "Building a country where everyone gets a fair chance requires confronting and addressing systemic issues."
- "Ignoring racial issues may indicate a desire to maintain existing discriminatory systems."

### Oneliner

San Jose's firearm ownership fee initiative sheds light on the potential racial impacts of colorblind laws and the importance of addressing systemic issues for a fair society.

### Audience

Policy advocates, activists

### On-the-ground actions from transcript

- Address systemic issues in your community through advocacy and support for marginalized groups (exemplified)
- Educate others on the impact of colorblind laws on different demographics (exemplified)

### Whats missing in summary

The full transcript provides a deeper exploration of how systemic racism intersects with laws, urging for critical reflection and open discourse on addressing racial disparities in society.

### Tags

#SystemicRacism #GunControl #ColorblindLaws #CommunityAdvocacy #FairSociety


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about San Jose's initiative,
what they're hoping to accomplish with it,
why it is unlikely to make it through the legal challenges that are sure to follow,
and then we're going to use it to illustrate two different things.
I will go ahead and tell you now, no matter how you feel about what I'm saying,
watch this video to the end.
Okay, so what is San Jose trying to do, if you don't know?
They are going to impose a fee, a tax.
They're going to take money if you decide to own a firearm there.
Now, the reason this is unlikely to make it through the legal challenges is because,
generally speaking, when you take an enumerated constitutional right
and charge money in the hopes of deterring people from exercising that right,
that is something that the Supreme Court refers to in technical legal jargon as totally uncool.
It's not going to make it.
I would be super surprised if this was upheld.
Okay, given the fact that it's unlikely to make it
and nobody has to get politically charged about it,
it provides us an opportunity to showcase two different things.
The first is, what is this actually going to do?
Well, it's going to deter gun ownership and provide the means to, you know,
offset the cost of the violence.
Deter ownership, will it though? Of who?
Rich folk? Poor folk?
Disproportionately, what do they look like?
Do they look like me? No.
Most people who watch this channel,
you would acknowledge that there are systemic issues in this country
that have kept certain demographics down, right?
I think that's fair to say that most of you would acknowledge that.
That there is an inherited disadvantage that comes along with it
because of systemic racism, because of institutionalized racism.
So what happens here? Poor people can't afford it.
Poor people end up violating this law.
Who does that disproportionately impact?
Aside from that, who's more likely to feel like they need a firearm?
Rich folk who live in a gated community with private security?
Or people in a working-class neighborhood?
Or people in a neighborhood where when you call the cops, it takes them 20 minutes to show up?
Who is going to be disproportionately impacted by this?
The law itself isn't racist, right?
It's pretty straightforward. It applies to everybody.
But because of the conditions that exist,
because of the systemic issues in this country that are already present,
it's going to impact certain groups more than others through no fault of their own.
And you're talking about taking away something that is a constitutional right.
That seems wrong to me. That seems wrong to me.
This is a good example of how colorblind laws end up having pretty racist impacts over time.
And I'm pretty sure that's what would happen here.
Now here's the twist to this video.
Right now, there are a whole bunch of right-wing gun guys going,
yeah, Beau, you tell them liberals.
Yeah, because you know this is true.
You know this is what's going to happen.
That's how it's going to play out.
There will even be selective enforcement of these laws.
So it goes to show that colorblind laws can have racist impacts.
Inherited disadvantage, power structures, institutionalized racism, right?
All these terms.
Now I've done a bad job of showcasing it because I'm not an expert on this subject.
But do you know what theory helped me become really critical of the way race intersects with law?
Maybe don't ban it because you just acknowledged it's real.
You know it's true.
Maybe it's something that needs to be taught.
Because if we're going to try to build a country that at some point,
everybody does get a fair shake, we're going to have to acknowledge this stuff.
And it doesn't matter if it's done through evidence when you're pointing to
disproportionate income levels and stuff like that.
Or if it's done through anecdotal stuff and storytelling and narrative.
You end up in the same spot.
Because it's truth.
You cheered this on because you knew it was true.
This presentation is very much rooted in the theory that you don't want discussed.
Why would you not want that discussed?
Unless the real desire is to see those systems upheld.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}