---
title: Let's talk about Chip Roy's 18 months of nothing getting done....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VO4Aj-bZrMs) |
| Published | 2021/07/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau criticizes Chip Roy's statement on obstructing progress for political gain.
- Chip Roy expressed satisfaction in causing chaos and hindering progress for 18 months.
- The infrastructure bill is being delayed due to political gamesmanship.
- Beau points out the real-life consequences of delaying infrastructure improvements.
- Chip Roy is banking on the ignorance of his constituents to support his strategy.
- Beau advocates for bipartisan cooperation to address critical infrastructure issues.
- The current compromise on the infrastructure bill is deemed insufficient by Beau.
- Beau stresses the urgent need to address the neglected infrastructure problems in the US.
- Politicians' neglect has led to major infrastructure challenges facing the country.
- Beau questions the ethics of prioritizing political gain over addressing national issues.

### Quotes

- "He's saying the good people of Bandera and Comfort, well they're just too slow-witted to figure out that we're obstructing it because they know the Democrats are in power."
- "Want your neighbors to suffer so you can get a boost at the polls. That is wild."
- "I think we need a lot of these politicians to be as comfortable as they are, to believe that their constituents are that ignorant."

### Oneliner

Beau criticizes Chip Roy's political gamesmanship in obstructing progress for 18 months, exposing the real-life consequences of delaying critical infrastructure improvements.

### Audience

Politically conscious individuals

### On-the-ground actions from transcript

- Contact your representatives to prioritize bipartisan cooperation for addressing infrastructure issues (implied).
- Join local advocacy groups pushing for comprehensive infrastructure solutions (implied).

### Whats missing in summary

Beau's passionate call for politicians to prioritize the country's needs over political gain.

### Tags

#Politics #Infrastructure #Bipartisanship #PoliticalGamesmanship #CommunityAction


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Biden and infrastructure and 18 months with Chip Roy.
I'll tell you that's a name you can trust right there, doesn't it just sound like it?
Chip Roy.
We're going to do this because a video came out old Chip and I imagine he's not too happy
about that because he sure said the quiet part aloud in this one.
That's the thing, this is the problem.
I actually say thank the Lord, 18 more months of chaos and the inability to get stuff done,
that's what we want.
Wow.
I mean that's pretty wild but at the same time, you know, fiery rhetoric and all of
that, it could just be hyperbole or anything.
I mean it's not like he said it twice.
Honestly, right now for the next 18 months our job is to do everything we can to slow
all of that down to get to December of 2022, the midterms.
Now when he says political chaos, when he says chaos he's talking about political chaos
up on Capitol Hill and when you frame it like that, that's not so bad.
I mean that's just politics as usual, right?
Doesn't really matter except the context of this is the infrastructure bill.
That's what's being discussed here.
Holding that up for 18 months.
The chaos on Capitol Hill translates to chaos out here.
We got bridges cracking, we have buildings crumbling, we have pipelines getting hacked,
we are losing people in the heat, we are losing people in the cold, we have water management
issues.
He wants to wait a year and a half to address it.
To start to address it.
It should have been addressed years ago.
And see, the part he managed not to say aloud is that for this little strategy to work,
to just obstruct everything, play into the chaos, make sure nothing gets done, he's counting
on the ignorance of his constituents.
He's saying the good people of Bandera and Comfort, well they're just too slow-witted
to figure out that we're obstructing it because they know the Democrats are in power.
That's the unspoken part of that.
Because this strategy doesn't work otherwise.
The thing is, when it comes to this, when it comes to infrastructure, it's needed.
It's needed.
This compromise that's being worked out, it's already whittled it down to where it's nothing
but a band-aid.
It's not actually going to solve the problem.
The US has major infrastructure issues and they're going to have to be addressed.
So Republicans could, theoretically, you know, do their job, look out for their constituents,
come together with Democrats and build a bigger infrastructure bill.
One that they can even share in the credit for.
Actually do something to help your country, your neighbors.
Because the way it stands, yeah I understand that we have brownouts and blackouts and we
have problems of all sorts, but you're just going to have to deal with that for the next
year and a half because it's going to help me a couple points in the polls.
Want your neighbors to suffer so you can get a boost at the polls.
That is wild.
That is wild.
The US has an infrastructure bill that is coming due.
It's huge.
It is huge because it's been neglected.
Because politicians of the past have done stuff like this and put it off for 18 months
and then, well, it really didn't matter to my constituents for the last year and a half.
Maybe I don't need to do anything about it.
That's why we're so behind.
That's why everything is in chaos.
That's why we have the issues we do.
This is sad, but in a way it's probably a good thing that it happened.
Because I think we need a lot of these politicians to be as comfortable as they are, to believe
that their constituents are that ignorant.
So they say this stuff aloud because I, for one, don't believe the people of Comfort and
Bandera are ignorant.
I think they can see this and I think if they listen to this and they look at these quotes,
they're going to understand.
He's saying, yeah, I'm going to let the country suffer because it helps my party.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}