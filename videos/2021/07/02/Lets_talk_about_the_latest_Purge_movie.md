---
title: Let's talk about the latest Purge movie....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BtnMG4FlqbE) |
| Published | 2021/07/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Introduces the topic of discussing the political nature of "The Purge" franchise.
- Describes the general premise of "The Purge" movies where all crime is legal for a set period every year.
- Talks about the latest installment of the franchise taking place along the southern border of the United States, focusing on a group wanting to "take America back."
- Points out the obvious parallels in the movie to Trumpism, describing it as ham-fisted and not subtle.
- Explains that horror movies, including "The Purge" franchise, often contain sociopolitical commentary exploring our darkest fears.
- Mentions the economic disparity commentary present throughout the entire franchise and in horror movies in general.
- Shares examples of political commentary in horror films regarding environmental issues, greedy politicians, and the military-industrial complex.
- States that all art is political in some way and that pretty much everything today is political.
- Suggests that those who dislike the political commentary in movies may refuse to see it or pretend it's a joke.
- Raises the point that the entire "Purge" franchise has always been political, with intentional political commentary in the latest installment.
- Notes that despite being too on the nose, the political parallels in the latest movie were filmed before certain real-life events occurred.
- Concludes by speculating that there may be backlash on social media due to the political nature of the latest "Purge" movie.

### Quotes
- "All art is political in some way."
- "The entire franchise is political in nature."
- "Life imitates art."

### Oneliner
Beau talks about the political nature of "The Purge" franchise, pointing out its intentional sociopolitical commentary intertwined with horror elements, and how all art carries political undertones.

### Audience
Movie enthusiasts, political analysts.

### On-the-ground actions from transcript
- Watch and analyze movies with a critical eye for sociopolitical commentary (implied).
- Engage in respectful debates and dialogues about the political themes in art (implied).

### Whats missing in summary
The full transcript provides a comprehensive analysis of the political themes present in horror movies, urging viewers to recognize and appreciate the intentional sociopolitical commentary woven into artistic creations.

### Tags
#ThePurge #PoliticalCommentary #HorrorMovies #Art #SociopoliticalThemes


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the Purge
and how everything is political.
We're going to do this because we're going to try to head something off
that happens anytime something like this occurs.
So, the latest installment
of this movie franchise.
It's pretty political.
If you're not familiar with the series of movies,
basically for a set period each year,
well, all crime is legal. Everything.
And people go wild.
That's the general storyline.
The latest installment
takes place along the southern border of the United States.
And it
highlights a group of people who, well, they just
want to take America back.
They want to make it great again. They want to purify it.
A bunch of bigots.
And they,
well, they're not ready for it to be over.
So they mount their own little insurrection
and keep it going. And it's coordinated.
The parallels are
pretty obvious.
And people are describing it as ham-fisted
because it is
not subtle.
It is definitely a shot at Trumpism.
Now, anytime something like this happens,
you have that group of people that comes out and says,
great, they have to make everything political.
In times like this, everything does
become political, but I would point out
that
horror movies in general explore our darkest fears.
That's what they're about.
And
they often contain sociopolitical commentary. This is especially true of
the Purge franchise.
Throughout it,
there is definitely
commentary on economic disparity
between the haves and the have-nots.
It exists. It's there throughout the entire franchise of movies.
It is very political.
And it's not limited to this franchise.
So when you hear people say, oh, horror movies are woke now.
They're politically aware.
Just go ahead and point out that
pretty much forever
they have been.
Not all, but most.
There's commentary about the environment,
toxic sludge,
commentary about greedy politicians who don't want to close the beaches,
commentary about the military-industrial complex,
and how when it finds something that's a danger to humanity, rather than eliminate it,
it tries to weaponize it.
It's there
throughout everything.
The
Umbrella Corporation
would be a good example.
Obviously, commentary exists.
It always has been.
Those who don't like the commentary,
who don't want to see it, who want to reject
that commentary,
they
just
refuse to see it, or they pretend like they don't.
They pretend like it's a joke.
All art
is political in some way.
Pretty much everything today is political.
If you can consider
horror films art.
But because of the nature
of this film
and
the political commentary it is providing,
I would imagine there's going to be a whole lot of upset people on social media.
So, it may be worth remembering
that the entire franchise
is political in nature.
It provided political commentary from the beginning.
And
the commentary that is in the
latest installment
is certainly not an accident.
It's a little too
on the nose, to be honest.
The parallels to the sixth
are really easy to see.
But the interesting part
is that it was filmed
before the sixth ever happened.
Because,
I guess life imitates art.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}