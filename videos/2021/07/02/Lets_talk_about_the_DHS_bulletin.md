---
title: Let's talk about the DHS bulletin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3Drr7EKBGOI) |
| Published | 2021/07/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses a bulletin released by the Department of Homeland Security before the end of June, warning about the increased likelihood of violence fueled by baseless claims and rhetoric.
- The bulletin explicitly states that the convergence of rhetoric and baseless claims is a significant factor in escalating violence.
- The bulletin predicts a peak in likelihood of violence around August, associated with a theory suggesting Trump will be reinstated, which Beau clarifies is not true.
- Beau criticizes politicians who continue to perpetuate misinformation and baseless claims despite the clear warning from the DHS.
- He challenges politicians to prioritize the country over their political careers, urging them to debunk false claims and step away from dangerous rhetoric.
- Beau points out that with the knowledge of the bulletin and the events of January 6th, politicians can no longer claim ignorance about the potential consequences of their words.

### Quotes

- "There is no way to misread it."
- "The Department of Homeland Security is saying this is a risk. It's a danger. It's a problem."
- "Let's see who is willing to put their country above their own political careers."
- "Maybe they didn't understand that their words were going to have consequences, that people might act on them."
- "At the end of the day, there's no excuse this time."

### Oneliner

Beau warns of the escalating risk of violence fueled by baseless claims and rhetoric, challenging politicians to prioritize debunking falsehoods over political gains.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Monitor politicians perpetuating baseless claims (implied)
- Challenge politicians spreading misinformation (implied)

### Whats missing in summary

Importance of holding politicians accountable for their words and actions. 

### Tags

#DepartmentOfHomelandSecurity #BaselessClaims #PoliticalRhetoric #ViolenceRisk #Accountability


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about a bulletin that the Department of Homeland Security put
out before the end of June. We're going to talk about it for two reasons. One is, I mean,
you should know about this, I think, and it's kind of being overshadowed by a lot of the
other news, the news of the hearing and the Trump Organization indictment and all of that
stuff. So I don't think it got the airtime it should have. Aside from that, I was having
a conversation with somebody and I was venting my frustrations about politicians who leaned
in to the baseless claims, who used that rhetoric. And he said, you know, this isn't everybody's
area. Not everybody's going to connect and say, hey, you know, if I say this it might
inflame people and cause things to happen. Fair enough. Fair enough. I disagree. I think
most reasonable people would know that that kind of rhetoric could lead to bad things.
So DHS released a bulletin. The bulletin says flat out the convergence of rhetoric and these
baseless claims are fueling an increased likelihood of violence. There's no way to misread it.
There is no way to misread it. They're suggesting that the increased likelihood is going to
peak around August, which is a date associated with one of these theories, suggesting that
Trump will be reinstated. Just so you know, that's not a thing. So maybe they didn't know
before, but those in office, they know about this bulletin or they should know. It's their
job. So any rhetoric that furthers these theories, that plays into them, that leans into them,
that refuses to address them and debunk them the way they should be, at this point, there's
no excuse. You have the Department of Homeland Security telling you this is fueling this.
And if it continues, we're likely to see bad things happen in August. It doesn't get any
more clear than that. So as we move through the month, let's keep an eye on the politicians
that continue to lean into the misinformation, that continue to humor those who are promoting
these baseless claims and these baseless theories. Because, sure, fine, I'll play your silly
little game. Maybe they didn't know before. Maybe they didn't understand that their words
were going to have consequences, that people might act on them. They do know now because
they saw the sixth and they have the bulletin. So, at the end of the day, there's no excuse
this time. The Department of Homeland Security is saying this is a risk. It's a danger. It's
a problem. Let's see who is willing to put their country above their own political careers.
Who's willing to back off this rhetoric? Who's willing to debunk these claims? Anyway, it's
just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}