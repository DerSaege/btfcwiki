---
title: Let's talk about Republican public health strategies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JeHRTT_RfTU) |
| Published | 2021/07/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains Republican politics on public health statements, focusing on recent events.
- Mentions Cawthorn's claim about Biden's door-to-door vaccine machinery being used for gun and Bible confiscation.
- Addresses the paranoia and fear within the Republican Party.
- Debunks the feasibility of door-to-door gun confiscation.
- Talks about the CPAC convention where not meeting vaccination goals was cheered.
- Analyzes the cold and calculating motives of some Republican leaders.
- Expresses concern over conservative friends falling prey to anti-vaccine rhetoric.
- Condemns risking health to "own the libs."

### Quotes

- "The paranoia and fear that has been stoked within the Republican Party is unbelievable."
- "It's got to make up for the loss of voters that they're going to have."
- "You have to be pretty far gone to willingly put yourself at risk to own the libs."
- "I'm sure you know some who have fallen prey to this."
- "I wish I had advice for you, because those cheers and the rhetoric that's being used, I don't know how to overcome it."

### Oneliner

Beau explains Republican politics on public health statements, addressing fear-mongering and cold motives, expressing concern over conservative friends falling prey to anti-vaccine rhetoric.

### Audience

Conservative friends

### On-the-ground actions from transcript

- Reach out to conservative friends and provide them with accurate information about vaccines (implied).

### Whats missing in summary

The full transcript provides an in-depth analysis of recent events exposing the manipulation of fear and misinformation in Republican politics regarding public health.

### Tags

#RepublicanPolitics #PublicHealth #Vaccines #FearMongering #CPAC #ConservativeFriends


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk a little bit
about Republican politics when it comes to public health
and why they're making some of the statements
they are making, because there were
two events that happened over the last couple of days.
And to be honest, it kind of blew my mind.
Things that occurred that, even as cynical as I am,
things I did not have on my bingo card.
The first was Cawthorn coming out
and saying that the machinery that
would be developed by Biden's little door-to-door crew
for the vaccines, that same machinery, well,
that could be used to come take your guns and Bibles.
No joke, he said this.
Before we get into this, I do want
to point out that the US government sends somebody
to your door pretty much every day,
sends somebody to your house to deliver your mail.
The paranoia and fear that has been
stoked within the Republican Party is unbelievable.
Now, I have to admit, I have never
run the numbers on attempting to go door-to-door to take
people's Bibles.
That's a new one.
I haven't worked out the logistics on that.
But I did do it when it comes to gun confiscation, door-to-door.
We ran the numbers on this channel
just to see if it was feasible.
It would take about 100 years.
It's not a thing.
That's not going to happen.
And if you have people in authority using that fear
to terrify you into not doing what
is in your best interest as far as your health,
you really have to question their motives.
So you had that.
And then at the CPAC convention, that's
a major conservative convention if you don't know,
it was announced that, well, the US, they're not
meeting their vaccination goals, and applause and cheers
broke out.
Didn't see that coming.
The people who have taken control of the Republican Party
have done the math.
If the US doesn't meet its vaccination goals,
well, that's going to make Biden look bad.
And the more hospitalizations there are,
the more loss there is, well, the worse he's going to look.
That's something else.
And what's important to note is that the rhetoric
is aimed at their constituents, their supporters.
Those are the people that they're convincing not
to get their shot.
Those are the people that will be lost.
I guess they imagine it's going to hurt Biden pretty bad
in the polls, because it's got to make up
for the loss of voters that they're going to have.
I have met some very cold and very calculating people
in my day.
This is a whole new level.
This is a whole new level.
I'm sure that you have conservative friends
in your life who you have tried to reach out to,
who have fallen prey to this.
Now, sure, there are other reasons for not
wanting to get your shot.
But I'm sure you know some who have fallen prey to this.
I wish I had advice for you, because those cheers
and the rhetoric that's being used,
I don't know how to overcome it.
You have to be pretty far gone to willingly put yourself
at risk to own the libs, which is what's happening.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}