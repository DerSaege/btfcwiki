---
title: Let's talk about me being "giddy like a schoolgirl" over corporate tokens....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=80LnRJ767VM) |
| Published | 2021/07/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains his excitement when major companies show token gestures of progressiveness by changing the race or gender of characters in media.
- Compares this strategy to record companies paying radio stations to play their music for familiarity.
- Argues that conservatives fear these changes because they effectively combat fear-mongering and "othering."
- Notes that companies implement these changes for profit, not social change, similar to McDonald's and Coke.
- Emphasizes that exposure breeds familiarity and reduces fear of the unknown.
- States that conservatives use fear to motivate their base and uphold the current system by keeping people separate.
- Believes that familiarity with different ideas and demographics leads to less resistance to change.
- Sees these token gestures as effective in gradually shifting perspectives and making change less scary.
- Expresses optimism about these gestures indicating a shift in the market towards inclusivity and progressiveness.
- Concludes by suggesting that exposure and familiarity through media can lead to greater acceptance and less resistance to change.

### Quotes

- "Every time this happens, there's an orchestrated outrage. Right? Why are they doing this? Because they know how effective it is."
- "Familiarity, because it's everywhere. You walk into a gas station to buy a Coke, there is an advertisement for Coke on the cooler."
- "Their motive as to why they're doing it doesn't mean that it's not effective."
- "The tide is turning. It is more profitable to do this than to not."
- "And that exposure, well, it breeds familiarity. Makes them less afraid, which means they will mount less resistance to change when it comes."

### Oneliner

Beau explains how token gestures of progressiveness in media combat fear-mongering and pave the way for change by fostering familiarity and reducing resistance.

### Audience

Media consumers, Progressives

### On-the-ground actions from transcript

- Support media that showcases diversity and inclusivity (implied)
- Advocate for gradual exposure to different ideas and demographics through media (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of how exposure to diversity in media can combat fear and resistance, leading to societal change.

### Tags

#Media #Progressiveness #Diversity #FearMongering #Inclusivity


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about why I get
giddy like a schoolgirl on Twitter, quote.
I got a message and it starts off with,
you're one of the smartest people on YouTube.
I really hope that's not true,
we're in a lot of trouble if it is.
But they noticed that I get, quote,
giddy like a schoolgirl
anytime some major company extends
the smallest token of progressive ideas,
such as switching the race or gender
of a character in a movie or TV show.
And the person wanted to know
if this was just some flaw in my personality
or if I had some reasoning behind it.
I do have reasoning behind it.
Did you know that record companies
back in the day used to pay radio stations
to play their music?
You probably did.
Why?
Why'd they do that?
Because if you became familiar with one of the songs,
well you might buy the whole album, right?
I want you to think of the people you know
who live in the United States
who have never had McDonald's or a Coke.
Willing to bet, that's a really short list.
Right?
Why?
Familiarity, because it's everywhere.
You walk into a gas station to buy a Coke,
there is an advertisement for Coke on the cooler.
Just so those people walking by see it.
Because everybody knows that name.
Because they've been exposed to it.
And humans are weird.
We fear the unknown.
Those things that we don't know, it's scary.
We don't want to try that.
And if something is scary, well, you keep it over there.
If you want to understand my reasoning behind it,
don't ask me why I support it.
Ask me why conservatives fear it.
Every time this happens,
there's an orchestrated outrage.
Right?
Why are they doing this?
Because they know how effective it is.
Now don't get me wrong.
I understand these companies,
they're not doing it to achieve social change.
They're doing it for dollars and change.
But so is McDonald's.
So is Coke.
Their motive as to why they're doing it
doesn't mean that it's not effective.
Conservatives motivate their base through fear.
Build a wall or else those people will get you.
Better watch your daughter when she goes into the bathroom.
Those people will get you.
It's what it's all about.
It's all about fear.
Fear of the other, the unknown.
Every time a show, a book, a movie, a comic book,
whatever, every time the character gets shifted a little,
it helps breed that familiarity.
People are scared of what they don't know about.
Ignorance causes fear.
They don't know about something,
so they're afraid of it.
They get exposed to it, well they're less afraid.
If they become familiar with it,
it's not scary at all.
Which means it's really hard to fear-monger with it.
Conservatives can't other them anymore.
Can't use them as the boogeyman.
Yeah, that's my reason.
And it's human nature.
You're afraid of things you don't know.
Things you don't understand.
Conservatives and those who want to uphold the current system,
they use that to other people.
To keep them over there.
To keep everybody separate.
Don't do that.
They're different than you.
Don't even be friends with them.
This same thing, this same concept,
also applies to ideas.
Little by little, you become familiar with something.
And it's not scary anymore.
So yeah, I do, definitely.
I don't know that I get giddy like a schoolgirl,
but I'm very supportive when companies do this.
Because while it isn't revolutionary,
it isn't immediate, it is effective.
It works.
And yeah, they're doing it to line their own pockets.
Good, good.
Because that means that they ran the numbers
and they know that's where the market is.
The tide is turning.
It is more profitable to do this than to not.
Companies that are multi-billion dollar,
multinational companies, they don't engage in stuff like this
without running the numbers first.
It means we're winning.
It means we're winning.
So yeah, I mean, it's silly.
These token gestures coming from these large companies,
they don't mean much to those who are actively
trying to pursue real change.
But every time they do it, you're
going to face less resistance.
Because somebody, somewhere became more familiar
with that demographic or became more familiar
with that concept, that idea, that cause.
So it's not as scary anymore.
Yeah, and I'm sure this has to do with the wonder years,
because I was super excited about that.
I'll probably do a whole video on that all by itself.
Because if that is done right, that can make people like me,
meaning the way I look, roughly my age,
that can expose them to stuff that realistically, they
probably never would.
They'd never be exposed to it without it.
It all depends on how it's done.
So and that exposure, well, it breeds familiarity.
Makes them less afraid, which means
they will mount less resistance to change when it comes.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}