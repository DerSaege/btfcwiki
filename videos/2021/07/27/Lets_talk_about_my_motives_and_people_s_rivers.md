---
title: Let's talk about my motives and people's rivers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hIokMwSasaE) |
| Published | 2021/07/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses a question about why he talks about certain topics even though those individuals may not watch his channel initially. He points out that they do watch based on their comments and engagement.
- His motivation stems from a personal desire for his wife to not have to deal with certain issues, along with a broader mental health aspect.
- Beau uses the analogy of life being a river that should keep moving forward but is sometimes stopped by fear, paranoia, and wild theories.
- He mentions people in the country expecting Biden and Harris to resign, leading Trump to become president, showcasing the impact of misinformation and fear.
- Beau believes that breaking through fear and getting people to take vaccines can help dismantle other baseless theories and fears, allowing society to move forward.
- He stresses the importance of individuals overcoming their fears and biases for societal progress.
- Despite ideological differences, Beau encourages challenging others' beliefs to help them move forward in life.
- His efforts are aimed at helping individuals overcome hatred, bigotry, and fear to resume forward movement in their lives.
- Beau acknowledges the sadness of lives stalled due to fear and prejudice, and he hopes to contribute by breaking down those barriers.
- He concludes by expressing his desire to play a part in helping people overcome their fears and move forward positively.

### Quotes

- "It's symbolic like anything else. You get one little hole in that dam. And the desire to live their life, it will come back."
- "We have to move forward as individuals."
- "Imagine how sad a life is that has come to the point where all forward movement has stopped because of hatred, because of bigotry, because of paranoia, because of fear."
- "I want to do my part to help drill little holes in hopes that once they get that shot in their arm, that the rest of the fear will slip away."
- "Y'all have a good day."

### Oneliner

Beau addresses his motivation for discussing critical topics, aiming to break through fear and misinformation to help individuals move forward positively.

### Audience

Community members

### On-the-ground actions from transcript

- Challenge beliefs respectfully to help individuals overcome fears (exemplified)
- Encourage open dialogues about vaccines and mental health (exemplified)

### Whats missing in summary

The full transcript captures Beau's passionate plea for individuals to overcome fear and biases to progress positively in society.

### Tags

#Motivation #Fear #Misinformation #PublicHealth #CommunityProgress


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about my motives, I guess.
Got an interesting question.
And basically, it was, hey, you know,
at first I didn't understand why you were talking
about some of this stuff, because it's not
like those people would be watching this channel.
And then I realized that, hey, they
do because they leave comments and thumbs down videos
and stuff like that.
Yeah, that's true.
And you ought to see my inbox.
They definitely watch.
So the person realized that they do watch.
But then they wanted to know why I cared.
And more specifically, why I put so much effort
into the public health stuff.
There is the public health aspect to it.
And you know, my wife has to deal with this stuff.
And I would really like for her to not
have to deal with it anymore.
That would be a personal motivation for me.
But aside from that, there's a mental health aspect to it.
You know, your life, everybody's life, it's a river.
It is a river.
It will keep moving forward unless you
allow it to be stopped.
And for a lot of people, it has stopped.
Their life, their forward movement,
everything that they could become, it has stopped.
It's turned into a reservoir.
There's a dam built of fear and paranoia.
And it's on a foundation of wild theories and baseless claims.
You know, there are people in this country right now
that honestly expect Biden and Harris to resign in a few days.
And that means that Trump will become president.
Just so you know, that's not what
happens if they resign, Pelosi becomes president.
But they believe this because of this wall they've built.
And it is holding them back.
They're awaiting months for their savior to be reinstated.
Their entire life has stopped.
No forward movement.
So what's this have to do with the vaccines and masks
and stuff like that?
It's symbolic.
It's symbolic like anything else.
You get one little hole in that dam.
And the desire to live their life, it will come back.
If you can help them break through that fear,
a lot of that other stuff will come tumbling down.
The hatred, the paranoia, the fear, all of that stuff.
It is stopping their lives.
And it's worth the effort to me to try
to help get that hole built. Because if they voluntarily
make the decision to go and get the vaccine,
they decide, hey, you know, this is real
and I need to take it seriously.
It helps undermine all of those other baseless theories,
all of those other wild claims, all of the stuff
that builds that wall of fear.
And if we want to move forward as society,
we have to move forward as individuals.
So all of these people who have built these little reservoirs
that are voluntarily stopping their lives,
who have allowed fear to just overcome them,
they're holding everybody else back.
Yeah, the public health thing is important to me,
but the mental health thing, the health of the country,
the world, it all relies on breaking this stuff down
and getting people to move past their own fears,
their own biases, their rejection of fact,
science, history, reality in general, in many cases.
We got to help them push through.
Because the people in their in-group,
they're not going to because they are also stopped.
They also have that dam built.
It's one of those things, if not you, who?
Who else is going to do it?
I put a lot of effort into it.
I go to, I try to address the topics from different angles
in hopes that y'all can use those arguments with people
in your life to help them tear down their dams
so they can get their life moving again.
I know that in many ways they're the opposition ideologically,
but I want you to imagine how sad a life is
that has come to the point where all forward movement has stopped
because of hatred, because of bigotry,
because of paranoia, because of fear.
It's sad.
No other word for it.
It's sad.
So I want to do my part to help drill little holes in hopes
that once they get that shot in their arm,
that the rest of the fear will slip away,
that it'll all come tumbling down,
and they can start moving forward again.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}