---
title: Let's talk about Trump, kingmaker of the Republican party....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JO-3qSgkNNM) |
| Published | 2021/08/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump's influence as a kingmaker in the Republican Party is in question after his endorsed candidate in Texas lost.
- Trump's PAC invested $350,000 in advertising for his endorsed candidate in Ohio, indicating that his name alone may not be enough to secure victories.
- The endorsement from Trump may not hold as much weight as previously thought, with the need for financial backing to boost candidates.
- The brand of authoritarianism known as Trumpism still holds strong within the Republican Party, despite Trump's decreasing personal influence.
- Republicans must reclaim control of their party from the Trump image and brand, which may be doing long-term damage to both the party and the country.
- Trump, although projecting an image of political strength, may not be as influential as he portrays himself to be, often losing more than winning.
- The glitzy image associated with Trump is perceived as more of a branding tactic rather than a true representation of political power.
- The election results suggest that Trump's influence may be more about financial contributions rather than genuine endorsement power.
- The Republican Party needs to break free from the dominance of the Trump brand and regain control to prevent further damage.
- Trump's influence is shifting towards that of a mega donor rather than a political influencer.

### Quotes

- "Trump isn't the kingmaker, he's just shaping up to be yet another mega-donor."
- "The brand of authoritarianism, Trumpism, that he founded, it is still widespread within the Republican Party."
- "He isn't the political juggernaut that he likes to pretend that he is."
- "The glitz, the gold, it's not real. It's just a brand. It's an image."
- "Trump isn't the influencer. He's a mega donor. That's it."

### Oneliner

Former President Trump's influence as a kingmaker in the Republican Party is questioned as financial backing seems more critical than his endorsements, perpetuating the damaging influence of Trumpism within the GOP.

### Audience

Republican Party Members

### On-the-ground actions from transcript

- Reclaim control of the Republican Party from the dominating influence of the Trump brand (implied).
- Focus on rebuilding the party's image and platform independent of Trump's influence (implied).
- Work towards reducing the prevalence of Trumpism within the Republican Party (implied).

### Whats missing in summary

Insights on the potential strategies for Republicans to counter the lingering influence of Trumpism within the party.

### Tags

#RepublicanParty #Trump #Trumpism #Influence #PoliticalStrategy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about former President Trump,
kingmaker of the Republican Party, influencer extraordinaire.
Earlier this week, we talked about how Trump's candidate in Texas,
the one he endorsed, lost in a Republican-only election.
That definitely called his status as kingmaker into question.
It made people wonder if his endorsement is really as valuable as he pretends it is.
All eyes, for people who were watching this, everybody shifted and started looking at Ohio,
where there's another election coming up, a primary,
and one of the candidates is endorsed by Trump.
This really interesting thing happened.
The day Trump's candidate in Texas lost the Trump PAC,
they made a $350,000 ad buy for their candidate in Ohio, for the Trump-endorsed candidate.
What this means is that even Trump's team knows his name isn't enough to win,
but they're going to try to brand it that way.
So they're going to spend money, and we know how much Trump loves to spend money,
they're going to spend money to try to make sure that his endorsement matters.
But the reality is, at this point, we kind of now know that it's not his endorsement that matters.
If the PAC is having to throw money into providing advertising for the candidates,
the endorsement isn't that important.
Trump isn't the kingmaker, he's just shaping up to be yet another mega-donor.
Candidates within the GOP should probably consider whether or not it is worth marrying themselves off
to Trump's platform, because it may not actually elevate their station.
One of the things that we should definitely note as we talk about the former president's
lessening influence is that while he as a person,
he is losing influence within the Republican Party, he isn't as strong as he likes to pretend to be.
The brand of authoritarianism, Trumpism, that he founded,
it is still widespread within the Republican Party.
That's not going away anytime soon, and it is causing long-term damage,
not just to the Republican Party, but to the country as a whole.
And it's damage that is going to take time to repair, even if it was to stop today.
One of the things that Republicans have to do is retake control of their party,
because they don't have control anymore.
They're beholden to this image, this brand of Trump the undefeatable,
when the reality is he loses more than he wins.
He isn't the political juggernaut that he likes to pretend that he is.
He's a branding expert.
It's what he's always been.
It's what he always did.
He just took the tactics that he used in the business world and shifted them to politics.
The glitz, the gold, it's not real.
It's just a brand. It's an image.
And the results that are coming out of these elections are showing that.
Even if his candidate wins in Ohio, we don't know if it's because of Trump's endorsement
or it's because of a $350,000 ad buy.
Trump isn't the influencer.
He's a mega donor. That's it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}