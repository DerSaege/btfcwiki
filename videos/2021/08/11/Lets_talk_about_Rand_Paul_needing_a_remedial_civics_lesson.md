---
title: Let's talk about Rand Paul needing a remedial civics lesson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ag_NJEc5cI4) |
| Published | 2021/08/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Rand Paul faces a suspension from YouTube for spreading medical misinformation, not for undermining public health efforts.
- Paul falsely claims that YouTube is violating his freedom of speech, failing to understand that the First Amendment protects people from Congress, not Congress from people.
- Beau points out that if masks were truly ineffective, the only remaining solution for public health in the US would be mandatory vaccines and vaccine passports.
- He criticizes politicians who prioritize sound bites over their own beliefs, like Senator Paul who claims to be anti-establishment but is actually part of the government.
- Beau addresses the harmful impact of spreading misinformation about masks on public health efforts and the hypocrisy of those who claim to champion the Constitution but only want it applied when convenient.

### Quotes

- "The intent of the First Amendment is to protect the people from Congress. That's you, Senator."
- "You are the establishment. You're not anti-establishment. You're not anti-government."
- "Many of those who pretend to champion the Constitution don't really want it applied."
- "There is no debate over whether or not masks work. They do."
- "An attempt to force them to carry a message from the government, that's you, Senator Paul, that's a violation of the First Amendment."

### Oneliner

Beau explains the First Amendment to Senator Rand Paul and criticizes politicians who spread misinformation while claiming to champion the Constitution.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Fact-check information shared by politicians and public figures (implied)
- Support platforms that combat misinformation (implied)

### Whats missing in summary

In-depth analysis and context on the relationship between politicians, freedom of speech, and public health crises.

### Tags

#FirstAmendment #Misinformation #PublicHealth #Politics #FreedomOfSpeech


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Senator Rand Paul
and constitutionally protected speech
and senators who need a remedial civics lesson.
Okay, so if you don't know,
Rand Paul won't be watching this video.
He's having to take a break from YouTube for seven days
because he actively attempted to undermine the public health effort in the United States.
I mean, that's not why YouTube suspended him.
They suspended him over medical misinformation or something like that.
When this happened, he of course went crying to the press,
cancel culture and whatnot, right?
He said, YouTube now thinks they are smart enough and godly enough
that they can oversee speech, even constitutionally protected speech.
Okay, have a seat, Senator.
Take out your social studies book and flip to the back
where the copy of the Constitution is and read with me,
starting at the First Amendment.
Congress shall make no law respecting an establishment of religion
or prohibiting the free exercise thereof
or abridging the freedom of speech or of the press
or the right of the people to peaceably assemble
and to petition the government for a redress of grievances.
That first line, Congress shall make no law.
Congress.
The intent of the First Amendment is to protect the people from Congress.
That's you, Senator.
You, your Congress.
I know it's confusing.
It's not designed to protect Congress from the people.
The argument that a lot of politicians are currently making
that they are being censored and it's against their First Amendment rights
or whatever, it's not accurate.
When you slap Senator on something and you use the power of your office
to put out a message and you want to force people to carry it,
that violates the intent of the First Amendment.
It doesn't violate the letter of it, though,
because you didn't create a law.
You didn't make it a provision.
But that pressure that's coming from your office
is certainly against what the founders intended.
Okay.
So moving on from this, let's just take a quick moment
to pretend for a second that what he said about masks
being ineffective is true.
It's not.
To be super clear on this, there are reams upon reams
of peer-reviewed studies that show that they are.
But let's pretend for a second that they are.
Then what's the only tool left in the toolbox
to manage the public health issue in the United States?
Mandatory vaccines and vaccine passports.
You okay with that, small government senator?
Libertarian leaning?
Is that cool with you?
So eager to get a sound bite that you're undermining
your own stated beliefs.
But that's the thing.
They're just stated beliefs.
It's a brand.
The problem is, is that a lot of these politicians
have fallen to the point that they believe their own propaganda.
Senator Paul, you are the establishment.
You're not anti-establishment.
You're not anti-government.
You are the establishment.
You're part of a political dynasty.
Stop pretending that you're some kind of dissident,
that you are persecuted.
I hate to be the one to break it to you,
but with all of your anti-government rhetoric,
I would like to point out you are quite literally
the government.
The persecution complex that is developing among Republicans
who are finally being called out on their lies is amazing.
There is no debate over whether or not masks work.
They do.
Different types work to different degrees.
An N95 is better than a cloth mask.
There's stuff like that.
There's room for discussion,
but the idea that they don't work, that's just garbage.
And yeah, it is harmful to the efforts to mitigate
the surging public health crisis in the United States.
And it also shows yet again that many of those
who pretend to champion the Constitution
don't really want it applied.
YouTube is a private entity.
An attempt to force them to carry a message
from the government, that's you, Senator Paul,
that's a violation of the First Amendment.
The whole idea was so the government
couldn't make people say what they wanted.
That's the intent.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}