---
title: Let's talk about hurricanes and vaccines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pZn_7pc9Z-g) |
| Published | 2021/08/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden's statement about getting vaccinated in hurricane-prone areas has sparked controversy.
- Biden's message is not fear-mongering but a statement of fact about the impact of hurricanes on medical infrastructure.
- In a pre-pandemic video, Beau discussed the consequences of being unvaccinated during a natural disaster like Hurricane Michael.
- Hurricane damage to hospitals reduces medical capacity, making vaccination even more critical in hurricane-prone states.
- The strain on medical infrastructure in hurricane-prone areas is already significant.
- Beau stresses the importance of vaccination as part of hurricane preparedness in states like Florida, Georgia, Alabama, and others.
- The reality is that without vaccination, individuals may face dire consequences during a hurricane due to reduced medical capacity.
- Beau underscores that the pandemic will persist until enough people are vaccinated, regardless of personal preferences.
- He warns that being unvaccinated during a hurricane can lead to severe consequences and loss.
- Vaccination is a vital component of preparedness for natural disasters in areas prone to hurricanes.

### Quotes

- "It's just reality."
- "It isn't fear-mongering."
- "Yeah, you need to get your shots."
- "This is going to continue until we have enough people vaccinated."
- "Add getting vaccinated to your hurricane preparedness plan."

### Oneliner

President Biden's vaccination advice in hurricane-prone regions is a factual warning, not fear-mongering, stressing the critical role of vaccination in hurricane preparedness amidst strained medical infrastructure.

### Audience

Residents in hurricane-prone areas

### On-the-ground actions from transcript

- Get vaccinated as part of your hurricane preparedness plan (suggested)
- Recognize the importance of vaccination in areas vulnerable to hurricanes (implied)

### Whats missing in summary

The full transcript provides additional context on the necessity of vaccination in hurricane-prone regions and the potential consequences of being unvaccinated during a natural disaster.

### Tags

#Vaccination #HurricanePreparedness #MedicalInfrastructure #PublicHealth #CommunitySafety #RealityBasedInformation


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about something that President Biden said
because it has apparently rubbed people the wrong way.
So we're going to talk about that.
We're going to talk about statements of fact.
And we're going to talk about an old video.
Biden basically said that if you live in a hurricane-prone area,
you need to go get your shots.
That's what he said.
That has led to a lot of messages, and this is one.
Beau, I know you're very pro-vax.
Even though I'm not, I respect your views on it
because I watched your vaccine and jaywalking video.
You were very fair.
I think the only thing you hate more than the unvaxed is fear-mongering.
Can you please address Biden's fear-mongering about hurricanes and vaccines?
It's like he saw where the outbreaks were
and found something to scare people in those areas.
I know the video you're talking about.
I know the video you're talking about.
It was made before all this started.
It was made before all this started.
And in it, I say that if you choose to be unvaccinated,
you kind of have to operate under the assumption
that modern medical infrastructure will always be available to you
and that you probably can't envision a scenario
in which it wouldn't be available to you.
I then provided an example of when it wouldn't be available.
Hurricane Michael.
In the video you're referencing here.
After Michael, the hospitals in this area were down for months.
They didn't have capacity.
One of the metrics that people are talking about right now
is how many beds are left, right?
If hospitals have their roofs ripped off,
if they're full of water, if they are damaged, don't have power,
any of this stuff, the number of beds goes down.
What Biden said, sure, it's scary, but it's not fear-mongering.
It is a statement of fact.
If we are hit with a major hurricane in Florida right now,
we are at the brink.
Anything that reduces capacity is bad.
If there's a major hurricane and the medical infrastructure is damaged,
and that is the point in time when you need hospitalization,
you will cease to be.
Scary?
Sure.
Fear-mongering?
No.
It's just reality.
The consequences of the choice to not be vaccinated.
It isn't something that's not true.
It's not something that's being spun.
It is something that was in the video you said was very fair.
It isn't fear-mongering.
If you live in Florida, Georgia, Alabama, Mississippi, Louisiana,
Texas, South Carolina, any of the places where hurricanes are likely to hit,
yeah, you need to get your shots.
The medical infrastructure in these areas is already taxed.
It is at the brink.
Anything that disrupts how it's running right now
is going to cause substantial loss.
There will be a whole lot of people wishing they had that shot if it happens.
Aside from that, if you end up in a shelter, in a confined space,
a whole lot of other people, you're probably going to wish you were vaccinated.
Yeah.
Scary?
Sure.
Fear-mongering?
Absolutely not.
It's just reality.
We are in a pandemic.
It doesn't matter if you don't like it.
It doesn't matter if you don't want to wear a mask.
Doesn't matter how much you whine and cry about it.
This is going to continue until we have enough people vaccinated.
The virus doesn't care that you're tired of it.
It doesn't care if you're fatigued.
This will go on.
And yeah, if a hurricane hits and reduces capacity,
damages medical infrastructure, it is going to cause loss
because the system is already at its breaking point.
If you live in one of those areas, yeah, I mean, I think it's a funny way to say it,
but add getting vaccinated to your hurricane preparedness plan.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}