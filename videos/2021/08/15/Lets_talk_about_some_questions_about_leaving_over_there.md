---
title: Let's talk about some questions about leaving over there....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6mYE6xQPoFw) |
| Published | 2021/08/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the differences between the withdrawal in Afghanistan and Vietnam, mentioning that the chaos on the ground may be the only similarity.
- Talks about completing the mission in Afghanistan and how mission creep led to the extended stay of the US military.
- Emphasizes the importance of defining clear goals and ensuring the military sticks to them to avoid mission creep.
- Points out that the US military is not designed for nation-building and suggests the need for separate departments for fighting and building.
- Mentions the failure of using the military for functions other than war, leading to the situation in Afghanistan.
- Addresses why the Afghan national government couldn't hold its own, attributing it to lack of experience and being outmatched by the opposition.
- Expresses doubt about fixing the situation in Afghanistan and suggests that foreign nations, other than the US, could provide token security forces.
- Advises on ensuring clear and publicly known goals for any proposed intervention in the future.

### Quotes

- "Make sure that the goal is very well defined and that the military sticks to that goal."
- "The US military is not actually designed for nation building."
- "They're outmatched."
- "The only thing that could make things better for the people in Afghanistan is for a foreign nation, not the United States."
- "You want to get further down the line? Maybe some of the US military budget could be shifted to infrastructure development."

### Oneliner

Beau explains the differences in withdrawal, mission completion, and military limitations in Afghanistan, stressing clear goals and potential solutions beyond US involvement.

### Audience

Policy analysts, activists.

### On-the-ground actions from transcript

- Advocate for clear and publicly known goals in any proposed intervention (implied).
- Push for a shift of some of the US military budget towards infrastructure development in conflict zones (implied).

### Whats missing in summary

In-depth analysis of specific actions needed to support nations recovering from conflict.

### Tags

#Afghanistan #USMilitary #NationBuilding #ForeignIntervention #ClearGoals


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to go through questions
that were prompted by yesterday's video.
We're gonna answer some questions about over there.
The first one is, it has to do with an analogy.
The next one is, and I love the way it was phrased,
what are the lessons learned
so we can complete the mission next time?
And the last one is, why can't they hold their own?
Okay, so the one about the analogy was asking
why I don't compare the withdrawal in Afghanistan to Vietnam.
I don't think that's as good an analogy as a lot of people do.
I believe there are some people in the United States
who are actively hoping for a Saigon moment
because it's good for them politically.
Even if that moment occurs,
the lead up to the two events is very, very different.
I don't think it's a good analogy really,
other than the chaos that could occur on the ground.
Aside from that, it's not really the same.
Now, what are the lessons learned
so we can complete the mission next time?
We completed the mission.
The United States completed the mission.
The mission upon entering Afghanistan
was to deny a territorial safe haven to an opposition group.
That mission was accomplished in like the first 18 months,
and then mission creep set in.
People who wanted to stay there,
people who were profiting from it,
people who could find some way to use that to their own benefit.
They started tasking the military with more and more stuff.
The next thing you know, we're there for 20 years.
You want a lesson learned?
Make sure that the goal is very well defined
and that the military sticks to that goal.
Generally speaking, when you're talking about people
that are going into these countries,
they're 18 to 24 years old,
and they're trained to do one job really well.
18 months later, some 20-year-old infantry guy
is suddenly having to deal with public affairs,
civil affairs, facilitating trade,
and engaging in missions that aren't theirs.
An infantry person, what's their job?
Take it and hold it.
That's their job.
And then they're being forced into other roles.
That doesn't work very well.
The US military is not actually designed for nation building.
It's not really good at it.
It doesn't have a good success record.
There are a lot of advisors who will say
that we have need for something else.
In fact, that's the best way I've heard it put.
We need a Department of War
and a Department of something else.
The people to do the fighting
and the people to do the building.
You've heard me reference that idea
with the world's policemen and the world's EMT, right?
It's the same premise.
But there's an advisor whose videos are on YouTube,
and I cannot remember his name,
who did a really good presentation
explaining exactly why using the military
for any function other than war
generally leads to failure.
That's kind of what happened here.
The initial mission of denying that territorial safe haven,
that was accomplished very early on.
And those who wanted to stay,
well, they said, well, they could come back.
Sure, they could.
But at that moment, once they had generally fled,
I'm sure there were remnants,
but once they had generally fled
and they had been disrupted,
the United States was occupying an area
of very little interest to the actual mission,
meaning it was using resources
and getting bogged down with no real benefit.
And then as mission creep set in
and it started changing to nation-building
and bringing democracy to Afghanistan,
it didn't work out.
It might have been better to have had less soldiers
and more engineers and teachers.
Now, the other question, and the last one,
is why can't they hold their own?
You know, they were trained for 20 years.
The national government can't hold its own
for a really simple reason.
They were trained. It was a job.
They were the military.
They came in, they learned how to fight,
and they were trained by the US military
to fight like the US military.
However, the US military continued to do
most of the heavy lifting,
so they didn't get a lot of experience.
So what you have now are people who are trained
pretty well to fight a conventional conflict
that requires a lot of technology,
which they no longer have,
a lot of air support, which they no longer have,
a lot of surveillance and intelligence,
which they no longer have,
up against a group of people
who didn't have that kind of training regimen.
It was baptism by fire.
Somewhere in Afghanistan, there's no doubt in my mind,
there is a 36-year-old commander
who has been fighting since he was 16.
20-year veteran.
You know, when you say that in the United States,
what that means is they were in the military for 20 years.
There, they were actively fighting for 20 years.
The national government,
the people that make up the military
of the national government,
understand that you cannot make a single mistake
against somebody like that.
They're outmatched.
It is incredibly unlikely
that they'll be able to hold their own,
and it has nothing to do with determination
or their willingness to support democracy
or wanting their country to be free or anything like that.
They're outmatched.
I don't think that there should be any blame
put on the national military there.
The US did not do a good job
readying them for a US departure.
The US used them as support in their own efforts.
The US still did most of the heavy lifting,
so they're not ready to take over.
You know, and then something that is underlying
all of these questions is how can we fix it?
I don't think there's any fixing this one.
The milk is spilt.
At this point, the only thing that could make things better
for the people in Afghanistan is for a foreign nation,
not the United States.
Any country other than the US, right,
willing to step up and provide that token security force
and back it up, put that chip in the game.
Aside from that, I don't think there is a solution.
But if you want to file something away,
next time there's a proposed intervention,
make sure that the goals are clear and publicly known
and make sure that the US sticks to them.
That is the takeaway.
You want to get further down the line?
Sure.
Maybe some of the US military budget
could be shifted to infrastructure development
and the type of stuff that can be done
to help a nation recover,
but it's not being done by people in the same uniforms
as those who may have helped cause the destruction.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}