---
title: Let's talk about the future of Afghanistan and this morning's events....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DrAkRnWuhng) |
| Published | 2021/08/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Correcting misconceptions about recent developments in Afghanistan.
- An opposition group, ISK, not the Taliban, made a move on the airport.
- ISK and the Taliban are not allies; they are more likely to shoot at each other than work together.
- ISK's move was a direct challenge to the de facto government, not just the West.
- ISK's goal is to show they are still relevant before the West completely leaves.
- ISK is not incredibly capable; estimates suggest they have around 1000 members globally.
- The future options for Afghanistan include power-sharing or civil war among factions.
- Maintaining a "light footprint" in Afghanistan is not a viable solution according to Beau.
- Beau warns against the US dropping a foreign operating base in Afghanistan as it could incite conflict with factions.
- Surprisingly, the withdrawal did not severely degrade US and Western intelligence gathering capabilities in Afghanistan.
- The factionalization in Afghanistan may lead to conflict unless the de facto government acts quickly to eliminate opposition.
- Beau believes conflict is likely, and the US needs to expedite its withdrawal from Afghanistan.

### Quotes

- "This is proof the US needs to maintain a light footprint."
- "We don't need to stay. We don't need a light footprint."
- "It's more of a reason to leave."

### Oneliner

Beau corrects misconceptions about Afghanistan, warns against maintaining a light footprint, and advocates for a swift US withdrawal due to factionalization and potential conflict.

### Audience

Policymakers, activists, concerned citizens

### On-the-ground actions from transcript

- Advocate for swift US withdrawal from Afghanistan (implied)
- Stay informed about the situation in Afghanistan and support efforts towards peacebuilding (implied)

### Whats missing in summary

The nuances and detailed analysis provided by Beau on the situation in Afghanistan, including the impact of factionalization and the need for a swift US withdrawal.

### Tags

#Afghanistan #USwithdrawal #Factionalization #Conflict #Misconceptions


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about the recent developments
over there, what they mean, and what they kind of foreshadow.
We're also going to correct some misconceptions that
came out through the media, because there was some stuff
said on major networks that is less than accurate.
And by that, I mean not accurate at all.
And then we're going to talk about the options
for the future and whether or not
the desire to maintain a, quote, light footprint
is all of a sudden viable.
OK, so if you don't know what happened,
an opposition group in Afghanistan
made a move on the airport.
It was not the de facto government.
It was not the Taliban.
It was a group ISK.
And when people hear that, IS, they
think of the group in Iraq.
They're not the same.
The group ISK is not as capable as the group that was in Iraq.
In fact, the group in Iraq at one time was like, yeah,
we don't know those people.
They're not part of us.
They are not friendly with the de facto government.
That's something that I have seen said numerous times,
that they're allies, that they're friends.
That is flatly not true.
At times, they have worked together
because they have similar goals, kind of,
but they're not friends.
They are more likely to shoot at each other than work together.
In fact, the leadership of ISK are literal traitors
to the de facto government.
They left the de facto government back in 2015.
They're not friends.
They're not partners.
They're not brothers.
OK.
This move, although it was directed at the West,
it was a direct challenge to the de facto government.
You know, a whole lot of people are
watching that video from 2019.
That video talks about this group
making a move to show they're still relevant when
the deal first got made.
Right?
This is the same thing.
This is them trying to show that they're still relevant
before the West is completely gone,
to show they still have a chip in the game.
They are not incredibly capable.
I want to say the highest estimate I've seen
says that they've got like 1,000 people, which
when you're talking about worldwide, yeah, that's a lot.
Like if you're looking at the scope of groups like this
on a global scale, that's a lot.
When you're talking about it for that country, it's not.
I've seen some estimates saying that soon they'll have 10,000.
I am skeptical of those estimates.
So this was a direct challenge to the de facto government,
to the Taliban.
OK.
So what does this mean for the future of the country?
That's the real question, right?
It means that there's a couple of options
that we can foresee.
One is that because there are several factions now,
you have the de facto government,
you have the government in exile under the former vice
president of the country, I guess now the acting president,
and you have this IS group.
There are a couple of smaller factions
that really haven't made the news as well.
So the de facto government has the option
of establishing a power sharing form of government
with these factions.
If that happens, this group, this IS group,
in comparative political power, they're
going to be like the Libertarian or Green Party in the United
States.
People are going to know they exist,
but they don't really swing anything.
So that's one option that could occur.
The other is that the de facto government attempt
to eliminate the other factions, in which case
the country will devolve into civil war.
Those are the two futures.
There have been those in the United States
who have taken this moment to suggest
that this is proof the US needs to maintain a light footprint.
They are flatly wrong.
In fact, they're completely wrong.
In fact, anybody who is suggesting that is somebody
that you never need to listen to again
when it comes to this topic.
If that plan is enacted and the US drops a fire
base in the middle of the country,
a foreign operating base, that location
becomes a lightning rod for every faction.
Because if they want to show that they're in the game,
they've got to take a swing at the US.
That's a horrible idea.
Now, aside from that, there is one surprising development.
The advanced warning.
Most people, myself included, believe that the withdrawal
would severely degrade US intelligence gathering
capabilities and Western intelligence gathering
capabilities within the country.
That apparently hasn't happened, which means we don't
need a footprint at all.
It's not like the country, no matter who controls it,
has any significant air defenses.
There can be insertions and extractions.
So that's a quick look at the future there.
Nothing changes. We still need to get out.
This isn't a reason to stay.
It's more of a reason to leave.
My guess is that the factionalization will probably
cause conflict unless the de facto government moves very,
very quickly to ventilate all of their opposition.
And I don't know that they're going to do that.
So there's probably going to be conflict.
The US needs to get out even faster.
It doesn't change that.
There isn't anything that's going
to change that at this point.
We don't need to stay.
We don't need a light footprint.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}