---
title: Let's talk about hindsight and how it could've been different....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LXqhfSn8aTE) |
| Published | 2021/08/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hindsight is 20-20, and it's easy to make decisions after knowing the outcome.
- Most people claiming they could have done things differently are merely speculating.
- It's vital to base decisions on estimates and information available at the time, not just what happened.
- Holding onto Bagram, an air base, is debated as a potential game-changer.
- Moving operations to Bagram is seen as more defensible, but the logistics pose significant challenges.
- The focus should be on saving lives and facilitating the evacuation, not on hindsight or hypothetical decisions.
- The preservation of human life is the top priority in the current situation in Afghanistan.
- The withdrawal from Afghanistan was negotiated long ago and was never going to be a clear victory.
- It's critical to prioritize saving lives rather than getting caught up in hindsight debates.
- The focus should be on the immediate task of evacuating people safely.

### Quotes

- "Hindsight is 20-20. That's not an encouragement to provide it."
- "The preservation of human life. Period. Full stop."
- "There is no way that this was going to turn into a US victory."
- "It's distracting from the actual important thing, which is saving lives."
- "Y'all have a good day."

### Oneliner

Hindsight is 20-20, but saving lives amid crisis is what truly matters now in the Afghan situation.

### Audience

Policy analysts, decision-makers, humanitarian organizations.

### On-the-ground actions from transcript

- Coordinate with humanitarian organizations to facilitate the safe evacuation of individuals from conflict zones (implied).
- Prioritize the preservation of human life by supporting efforts to save lives in crisis situations (implied).

### Whats missing in summary

The emotional weight of navigating complex decisions under pressure and the importance of focusing on immediate actions rather than hindsight debates.

### Tags

#Hindsight #Afghanistan #HumanitarianCrisis #Evacuation #Priority


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about hindsight.
Because a lot of people seem to be misunderstanding that phrase,
hindsight is 20-20. Maybe they don't get the meaning of that.
Because there's a whole lot of people providing theirs,
saying that if I was in charge, I would have done it this way.
I would have done that.
I, and I alone, would have found some way to snatch victory from the jaws of defeat.
Most of them, in fact all of them to be honest, as far as I know, are just making it up.
Making it up.
Unless whoever this commentator is can point to an estimate that they made
before the national government started falling.
That says they believed the national government was going to fall in two weeks.
What they're saying is based solely on the fact that they know what happened.
It's easy to make decisions after the fact.
Okay, if you watch this channel and you only get this kind of news from here,
you probably have a pretty negative view of how long
the national government was going to last, right?
Because I was incredibly skeptical of the official estimates.
However, it is really important to note that most estimates
had the national government lasting 60 days from now.
They're still in power 60 days from now.
That is the overwhelming majority of estimates.
That's the low end of the overwhelming majority of estimates.
Okay, so let's say hypothetically speaking that somebody did send up
an estimate that was 30 days or two weeks right on the money, right?
If you were the person making decisions, is that the one you would go with?
Would you go with the outlier or would you go with the giant stack of estimates
that said 90 days, realistically?
As far as I know, there is not a single person that can point to their estimate
saying the national government was only going to last two weeks.
Without knowing that ahead of time, you wouldn't base your decisions on the idea
that the national government was going to collapse in two weeks
because you didn't know.
Even with that in mind, most of the I would have done this things,
they don't even make sense.
They're penguin comments, right?
Holding the penguin under the arm.
It's stuff like that.
There is one question that keeps getting brought up
that I think in the future is going to be debated.
I think in the future, people are going to be like, what about this?
This is something that could have been a game changer.
So we're going to go through it and it's about holding on to Bagram,
which is an air base.
The idea is that it is incredibly defensible.
So we're going to kind of go through the discussion about holding this on
or holding on to this and see if that was the decision that might have been made.
OK, so the first thing that you have to know about Bagram
is that it isn't right by the embassy.
It's an hour and a half away down a pretty notorious road.
In fact, if you want to know how notorious I have a tweet where I mention this,
go read the comments from the people who have been on it.
Most of them don't seem to think this is a good idea because they've driven it.
OK, so the first scenario is that for whatever reason,
you know that this is going to happen.
So you're going to hold on to both of them, the capital and Bagram.
Realistically, you don't have the troops to defend it.
The idea that Bagram and the capital can be held, that's not a thing.
To be completely honest, we're not holding the airport now.
They're not really trying to dislodge us.
So you can't hold them both.
OK, so then let's close up shop in the capital.
So, all right, so how do you start that?
You surrender the embassy, yank that American flag down,
and right now people are getting tense because they don't like the idea of surrender.
They're looking for a victory in a withdrawal.
Doesn't exist.
So you close down the embassy, close up shop,
and you move it to Bagram, an hour and a half outside of town.
What about all those American civilians?
What do they do?
When it's time to leave because things go bad,
well, they have to drive an hour and a half across a pretty notorious piece of road.
And if you're planning this and you don't know what's going to happen,
you have to assume that there's going to be conflict along that road.
And all you can picture in your head is a bunch of videos of American civilians
surfacing online holding newspapers.
Okay, so we can't do it that way.
We're not going to leave the civilians in the capital city
and maintain our presence at Bagram.
Let's move the civilians there.
Makes sense.
We can do it that way.
All right, so we move them there.
All the civilians that didn't really want to leave or for whatever reason didn't,
they're now at Bagram.
Most of them didn't want to leave because they got a job there.
So now we're back to them driving down that notorious piece of road.
We're back to them holding newspapers.
Doesn't seem to be good either.
And then you get to the idea of what about all of the Afghan nationals who want to leave?
They have to make it down that road too.
There is a lot of wisdom in the idea of shifting operations to Bagram
because it is way more defensible, way more defensible.
But unless you know the exact scenario that is going to play out,
and you wouldn't when you made this decision,
most things are going to tell you not to do it.
The difference between this and most of the what I would have done it this way
is that in the future, this is going to be debated.
In the future, years from now, people are going to talk about this
and it might shift the way the United States does stuff.
Because maybe the idea of remote working, you know, these contractors,
maybe they could have worked from Bagram.
The embassy could have been there.
Maybe we could have used helicopters.
There's a whole bunch of stuff that can be hot washed afterward.
But unless you know the exact scenario that's going to play out,
and you don't, nobody did, you wouldn't foresee this as a good move.
Because of the road, they would have to be trampled.
And the fact that people are saying this and pushing this idea on the day
that a group made a move at the airport, that doesn't make a lot of sense.
And it doesn't put a whole lot of faith, for me anyway,
in their ability to gauge risk.
Because that group, they would definitely be stopping some cars on that road.
Hindsight is 20-20. That's not an encouragement to provide it.
That's a way of saying that it's easy to know what's going to happen
once you already saw it happen.
There are those who are saying that, well, intelligence should have known
this was going to play out.
And yeah, there were a lot of signs.
I mean, you know that if you watch this channel.
At the same time, it's not a crystal ball.
It's using the information you have with and providing your best estimate.
That's why they're called estimates.
They're not accurate.
They're not going to be accurate all the time.
Even if you're really good, you're going to get stuff wrong.
I don't think that the I would have done it this way type of stuff
is beneficial at all.
And I don't think it should carry any weight unless the person can point
to them previously saying that the national government was going
to fall in two weeks.
Because that changed everybody's operational template.
Nobody expected that.
Nobody saw that coming, at least as far as I know.
And unless you know that, the moves that people are suggesting
don't make sense.
Most of them don't make sense, even if you did know it.
So when it comes to this, when it comes to this topic,
there is one thing that's important right now.
The preservation of human life.
Period.
Full stop.
That's it.
That's all that matters is getting people out.
There is no way that this was going to turn into a US victory.
It was a withdrawal.
It was a withdrawal that was negotiated a long time ago.
There isn't a way to walk away with this waving the American flag
and having ticker tape parades.
It's not how it's going to look.
It was never going to look that way.
The hindsight stuff isn't important because it's distracting
from the actual important thing, which is saving lives.
And that's what needs to be occurring right now.
That's it.
Nothing else matters.
Believe me when I say that people that really understand this stuff,
they're going to be debating, dissecting, and discussing this
for the next 20 years.
You're probably not going to find a lot on your favorite network
that's going to matter.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}