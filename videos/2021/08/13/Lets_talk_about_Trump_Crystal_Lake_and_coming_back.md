---
title: Let's talk about Trump, Crystal Lake, and coming back....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XuMQHX7zzlA) |
| Published | 2021/08/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reporting live from Camp Crystal Lake on Friday the 13th, waiting for the return of a mythical figure to set things right.
- Former President Trump could have debunked false claims but didn't, benefiting politically.
- Those who believed in the false claims saw their family and social life suffer without concern from Trump.
- People who promoted Trump's return are the same advising against masks and vaccines for political gain.
- Republican leaders who remain silent are willing to sacrifice followers for political gain.
- If leaders don't encourage measures to protect health, they don't care about their followers.
- Leadership of the Republican party and the mythical figure walking out of the lake both end with people harmed.
- Advice to wash hands, wear masks, get vaccinated, and protect oneself despite misleading leadership.
- Leaders who allowed false beliefs without correction show their lack of care for their followers.
- Encouragement to take note of the character of leaders who mislead or fail to correct misinformation.

### Quotes

- "Leaders who allowed false beliefs without correction show their lack of care for their followers."
- "Those who promoted Trump's return are the same advising against masks and vaccines for political gain."

### Oneliner

Former President Trump could have debunked false claims, but his silence harmed followers, while Republican leaders prioritize political gain over followers' well-being.

### Audience

Followers of political leaders

### On-the-ground actions from transcript

- Stay at home as much as you can to protect yourself and your family (implied)
- Wash your hands regularly to prevent the spread of illness (implied)
- Wear a mask when in public spaces to protect yourself and others (implied)
- Get vaccinated to safeguard your health and the health of your community (implied)

### Whats missing in summary

The full transcript delves into the consequences of following misleading leaders and the importance of prioritizing health over political allegiances.

### Tags

#Politics #Misinformation #Health #Leadership #Accountability


## Transcript
Well, howdy there, Internet people.
It's Beau again.
And today we're reporting live from Camp Crystal Lake
here on Friday the 13th,
where we await the return of a mythical figure,
the resurrection of somebody
who's going to come back like the myths of old
and set things right.
He's gonna be reinstated, right?
Retake the White House.
We all know that's gonna happen,
except for the fact that we all know it's not going to happen.
It's not true.
It never was.
Everybody knows that, with the exception
of a small group of people who truly bought into it.
But even if you aren't part of that group of people,
there's something here that you really need to recognize.
At any point in time, the former president,
former President Trump, could have walked out
and said none of this was true.
He could have come out and said,
no, that's ridiculous, that's not a thing.
But he never did.
Years, and he never did.
He leaned into it
because he knew it was good for him politically.
Now, if you bought into this,
over the last couple of years,
you have watched your family life
and your social life perish.
And the person who you put so much faith in
never told you that it was pointless.
He never expressed the smallest amount of concern
for the upheaval it caused in your life.
Now, the other side to this is,
please understand the people who led you to believe
that Trump was going to walk out of that lake today,
they're the same people telling you
that you don't need to wear a mask.
It's not that big of a deal.
You shouldn't get vaccinated.
All of that, the same people.
Why?
Because just like before,
your family life perishing,
your social life going under,
that was good for them politically.
This is the same situation.
They are willing to throw you under
for their own political advancement.
And this applies to every single Republican party member
who hasn't openly and forcefully spoke out.
Both scenarios,
whether you're talking about the silly theories
or whether you're talking about the public health stuff
that is very real and needs real attention.
If they're not out there actively encouraging you
to take the right measures to protect yourself,
to protect your family,
they don't care about you.
You don't owe them any loyalty.
They haven't shown any to you.
There's only one similarity
between the leadership that has recently been displayed
by the Republican party
and the guy that walks out of the lake in these movies.
They both end up with people gone.
Wash your hands.
Don't touch your face.
Wear a mask.
Get vaccinated.
Stay at home as much as you can.
Do the stuff you need to to protect your family,
to protect yourself.
Don't listen to these people who have proven for years
that they do not care about you.
And even if they didn't mislead you personally,
take this as a note of their character.
They allowed this segment of Americans
to believe these lies, and they never corrected them.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}