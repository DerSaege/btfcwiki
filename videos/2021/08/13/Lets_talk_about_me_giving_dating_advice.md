---
title: Let's talk about me giving dating advice....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=m0XX6OxfTNs) |
| Published | 2021/08/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing dating advice on whether to date a trans woman.
- Advising against seeking approval from others in matters of love.
- Encouraging the listener to make their own decisions in relationships.
- Hinting at the influence of the listener's friends on their question.
- Mentioning the importance of individual choice in love.
- Drawing parallels with the movie "A Bronx Tale" to illustrate dynamics at play.
- Urging the listener to prioritize their feelings and connection with their partner.
- Sharing empathy for young men facing societal pressures.
- Referencing a "door lock test" as a measure of true love.
- Concluding with a message to prioritize love over conformity.

### Quotes

- "Never seek approval from anybody when it comes to who you love. Nobody's opinion matters. Nobody's."
- "It's just you and the other person."
- "Fall in love. Don't fall in line."

### Oneliner

Beau advises against seeking approval from others in matters of love, stressing that it's solely about the individuals involved, not anyone else.

### Audience

Young adults seeking dating advice.

### On-the-ground actions from transcript

- Fall in love based on genuine connection and not societal expectations (implied).

### Whats missing in summary

The emotional depth and personal anecdotes shared by Beau in the full transcript. 

### Tags

#DatingAdvice #Relationships #Love #Authenticity #IndividualChoice


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, I'm going to give some dating advice.
This channel is not going to turn into Dear Abby,
but I want to answer this one.
Beau, not your normal type of question,
but I didn't have a dad in a mostly not creepy
parasocial way.
I look up to you for that male role model.
I met this woman.
She's wonderful and hot AF.
We've really clicked.
We've gone out with friends twice and just us once.
To get to the point, would you date a trans woman?
I don't know.
I think my wife would get really mad if I started doing that.
Besides, I don't know why you're trying to set her up with me.
She sounds perfect for you.
Dad jokes aside, there's something else here.
Judging by that message, you've watched this channel a lot.
You knew what I was going to say.
I mean, it's not like I'm guarded on this topic.
You just needed to hear it, right?
I have to assume that's because you have somebody else, maybe
one of your friends, whispering in your ear.
If you want some real fatherly advice,
never seek approval from anybody when
it comes to who you love.
Nobody's opinion matters.
Nobody's.
That goes for your parents, your sister, your brother,
your best friend, your aunt, your uncle.
Nobody, nobody should influence that decision.
Just you and the other person.
That's it.
I would go watch the movie A Bronx Tale.
It's not about this topic, but it kind of is in a different way.
My guess is there are some of the same dynamics at play.
And like the guy says in that movie,
your friends are something I can't say on this channel,
but that's probably what they are.
And at the end of it, they're not going to be around.
It's just you and her.
There is no reason to seek approval
from anybody on this topic.
Nobody else has to deal with it at all.
It's just you.
On a related note, I do feel sorry for young men
today, because I'll tell you that door lock test,
man, never let me down.
Anyway, fall in love.
Don't fall in line.
It's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}