---
title: Let's talk about personal responsibility in today's world....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=izFoszGfWRo) |
| Published | 2021/08/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines the concept of personal responsibility deeply entrenched in American politics.
- Personal responsibility is seen as a way to reduce government intervention and increase individual freedom.
- Criticizes individuals who refuse to wear masks or get vaccinated under the guise of personal responsibility.
- Points out the contradiction in claiming personal responsibility while not taking necessary public health measures.
- Expresses disappointment in small government conservatives and libertarians for not advocating for responsible behavior during the pandemic.
- Notes that individuals leaving the Republican Party may not find solace in libertarianism due to lack of distinguishable differences.
- Critiques libertarians for failing to provide a viable alternative and for echoing Republican talking points.
- Argues that the refusal to wear masks or get vaccinated undermines the philosophy of self-governance.
- Suggests that libertarians could have gained more support during the pandemic by promoting responsible behavior.
- Emphasizes the need to act on personal responsibility by wearing masks and getting vaccinated without waiting for government mandates.

### Quotes

- "You don't need to make me wear a mask because I'm already doing it, because I'm exercising that personal responsibility."
- "They're not looking for a new philosophy. They're just tired of seeing funeral processions."
- "But as simple as that philosophy is, you have to act on it."
- "No, you don't need the government to tell you to do that because the government is a child of the people, not the other way around."
- "It makes it seem that you don't believe your ideology, in the best case."

### Oneliner

In a critical analysis of personal responsibility, Beau challenges the failure of small government conservatives and libertarians to advocate for responsible behavior during the pandemic, undermining the core principles they claim to uphold.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Wear a mask and get vaccinated (implied)
- Advocate for responsible behavior in the community (implied)
  
### Whats missing in summary

The full transcript provides a comprehensive breakdown of how personal responsibility intersects with political ideologies and public health crises.

### Tags

#PersonalResponsibility #AmericanPolitics #COVID19 #SmallGovernment #Libertarians


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about personal responsibility.
It's a concept that is very entrenched in American politics.
It informs and guides a lot of political discussions in the United States.
It is the cornerstone of a lot of different schools of thought.
Personal responsibility.
The general idea is that if we exercise personal responsibility, we will end up being more free.
Because if we, as individuals, do what's right, then there will be less need for government intervention.
There will be less laws, and every law is contrary to human liberty, and so on and so forth.
So we become more free.
You can swing your fist however you want, as long as it doesn't hit somebody else's nose.
Foundational element of small government conservatives, of libertarians,
of the libertarian side of the right wing.
You know, it's beautiful in its simplicity.
But I got a nagging question. Where is it at? Right now, where is it at?
Because I don't see it.
I hear the same thing from the small government conservatives,
from the libertarians, that I hear from the Republicans.
You can't make me wear a mask.
If you're of that personal responsibility bent,
if that's part of your school of thought, that's only half the sentence.
You don't need to make me wear a mask because I'm already doing it,
because I'm exercising that personal responsibility.
Because I understand that when I'm walking around unmasked, unvaccinated,
that the stuff that's coming out of my mouth every time I cough or sneeze,
that that is hitting somebody else's nose.
Where's it at?
I don't see it.
I don't see anybody advocating for that advanced citizenship from that side.
And it's sad because this is a golden moment
for the small government conservatives, for the libertarians.
They could be scooping up so many disaffected Republicans right now,
but they won't because they're just using the same contrarian talking points.
There's no difference.
Those who are leaving the Republican Party because of the talking points
of the politicians here in Florida, they're not going to go to the libertarians.
They don't want to go to the Democrats,
but that's where they're going to end up because there is no discernible difference.
Because you're not giving them anything to support.
You're just parroting the same talking points as the Republicans.
You're not giving them something new.
You're not saying, hey, you know, we don't need this.
We can do it ourselves.
They're not looking for a new philosophy.
They're not looking for a new school of thought.
They're just tired of seeing funeral processions.
And you're not giving them anything that's going to stop that.
In fact, they probably see you as making the problem worse.
And it also goes against the whole idea.
For years, libertarians have been out there doing outreach,
trying to be like, no, we can build the roads.
We don't need the government.
We can handle this ourselves.
But they're not going to remember that.
What they're going to remember is that in the middle of a pandemic,
you were screaming you weren't going to wear a mask.
Making the problem worse, not better.
It's really hard for somebody to look at that school of thought
and think, hey, yeah, we can self-govern.
When you have people saying that they're not going to wear masks,
that they're not going to get vaccinated,
because that doesn't warm people to the idea of self-governing.
It convinces them that we definitely need a government
because there's some people out there that just aren't thinking right.
Never seen anything so self-defeating in my life.
And really, this could have been the period
during the end of the Trump years and right now,
the Libertarian Party could have came into its own.
Could have became a real force instead of just a spoiler party.
But as simple as that philosophy is, you have to act on it.
You have to exercise that personal responsibility.
If that's your school of thought, own it.
Yeah, you need to wear a mask. You need to get vaccinated.
No, you don't need the government to tell you to do that
because the government is a child of the people, not the other way around.
We should be leading it.
But that's not what's happening.
The standard line is, I'm not going to do anything
the government doesn't make me do,
which is the exact opposite of what the philosophy is supposed to be.
It makes it seem that you don't believe your ideology, in the best case.
Or in the worst case, that in one of the first really visible public tests
of your ideology, of your school of thought, that it failed miserably
because it couldn't even encourage wearing a mask.
Couldn't encourage the basic, basic mitigation efforts
that we all know are necessary.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}