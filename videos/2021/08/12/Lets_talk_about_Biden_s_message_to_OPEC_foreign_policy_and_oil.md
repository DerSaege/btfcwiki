---
title: Let's talk about Biden's message to OPEC, foreign policy, and oil....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-AvORj6UiNc) |
| Published | 2021/08/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden asked OPEC to increase oil production, prompting backlash from people advocating for America to use its own oil reserves.
- The United States has the 11th largest oil reserves globally, about 35 billion barrels, which is approximately 2% of the world's oil supply.
- If the US were to solely rely on its own oil reserves and maintain current consumption levels, it could run out of oil before the end of Biden's second term.
- Comparatively, Saudi Arabia, with the second-largest known oil reserves, has enough oil to last for over two centuries.
- If the US depleted its own oil reserves in about five years, it would become dependent on foreign oil, contrary to the America first policy.
- The US military, although not involved in pillaging, could politically influence countries to ensure a steady oil supply.
- A reliance on only domestic oil could jeopardize the US's superpower status as OPEC could cut off the oil supply.
- Maintaining a policy of using others' oil reserves first was enforced from 1975 to 2015, suggesting a strategic approach.
- Beau implies that transitioning to alternative energy sources like electric cars could be a wise long-term decision considering the limited domestic oil reserves.
- The concept of using foreign oil before exhausting domestic reserves is a fundamental but often overlooked aspect of US foreign policy.

### Quotes

- "America first and all that."
- "I'm not saying it's right. I'm saying it's the way it is."
- "If you absolutely want to destroy the United States, go right ahead."
- "It might be a little bit more of a motivating factor."
- "Y'all have a good day."

### Oneliner

President Biden's call for increased oil production unveils the dilemma of US oil reserves and foreign dependency, posing risks to national security and superpower status, urging consideration for alternative energy solutions. 

### Audience
Policy Analysts, Energy Activists

### On-the-ground actions from transcript
- Advocate for sustainable energy solutions in your community (implied).
- Support policies that encourage the transition to alternative energy sources (implied).

### Whats missing in summary
Further insights into the implications of foreign oil dependency and the necessity for strategic energy transitions.

### Tags
#ForeignPolicy #OilReserves #USDependency #AlternativeEnergy #NationalSecurity


## Transcript
Well howdy there internet people. It's Beau again.
So today we're going to talk about foreign policy and oil.
We're going to do this because President Biden issued a pretty routine run-of-the-mill statement to OPEC
asking them to pump out more oil.
When he did this, a whole lot of people expressed their displeasure.
They suggested that we shouldn't be buying oil from other countries, that we should use our own.
America first and all that.
After all, we do have the 11th largest oil reserves in the world.
Why are we dealing with those people?
America first.
When you say it like that, 11th largest known oil reserves, that sounds like a lot.
It does.
I mean it sounds like a lot.
Even if you put it into barrels, that sounds like a lot because it's like 35 billion barrels of oil.
That's about 2% of the world's supply.
So let's say we exercise this plan and we stop importing oil
and we just use our own.
We just use our own reserves and we keep up current levels of consumption.
We would run out of oil before the end of Biden's second term, assuming he has one.
We have a little less than five years.
Our oil reserves in the United States are just a little less than five years of our annual consumption.
For comparison, Saudi Arabia, which is second, I believe, in known oil reserves,
they have enough to run their country for little more than two centuries.
So if we were to exercise this make America great, America first plan,
in five years, the United States would actually be dependent on foreign oil.
We would run out of our own, right?
So then, well, we would truly be at the mercy of these oil producing nations.
We're not right now.
It may seem like it because we buy a lot from them and they can charge us for it.
But they can't charge too much because it's foreign policy.
Not about right and wrong, not about good and evil, not about ideology.
It's about power, right?
See, we have this wild thing called the U.S. military.
And while the U.S. does not pillage, that's a war crime, we don't do that.
We don't go into foreign countries and just take their oil.
That would be wrong.
The U.S. military is more than capable of going into a country and politically realigning it.
That means getting rid of the leadership and installing a new leadership that would sell us oil.
If we don't have the U.S. military because we have exhausted our own reserves,
well, we can't do that.
And the United States is done.
It is no longer a superpower because other nations, OPEC, can just turn off our oil supply.
And then we can't go politically realign them.
I mean, do you have any idea what the miles per gallon on an Abrams is?
I think it's like six.
The U.S. military needs oil.
What I'm describing right now is a war crime.
I'm not saying it's right.
I'm saying it's the way it is.
And the America first approach of using our own reserves first so we don't buy it from
other countries, I mean, sure, if you absolutely want to destroy the United States, go right ahead.
I mean, that's a great idea.
But perhaps if your favorite talking head was saying this,
maybe you need a different favorite talking head because this is pretty simple to understand.
It's not even a secret.
Right now, this is kind of an unspoken foreign policy.
It wasn't always unspoken.
In fact, from 1975 to 2015, it was more or less illegal to export oil because our overall
strategy is to use everybody else's first.
And as you think about these numbers and understand five years,
maybe allow that to influence your decision when it comes to things like electric cars
and moving towards other forms of energy.
It might be a little bit more of a motivating factor.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}