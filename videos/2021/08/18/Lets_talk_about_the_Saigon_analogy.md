---
title: Let's talk about the Saigon analogy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AI7FTx2_lB0) |
| Published | 2021/08/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Saigon analogy and why it's flawed.
- Points out the analogy's emotional manipulation rather than factual accuracy.
- Contrasts the military end of Vietnam with Afghanistan's political end.
- Argues that Afghanistan's loss was a political defeat, not a military one.
- Criticizes the failure to win the peace in Afghanistan.
- Analyzes America's approach to foreign policy and empire-building.
- Suggests that American foreign policy needs to change to prioritize aiding countries to stand on their own.
- Addresses the limitations of American military dominance in shaping global influence.

### Quotes

- "We lost the peace."
- "The attempt to tie this to a military defeat, to make it look like a failed war rather than a failed peace, is an attempt to avoid these questions."
- "Eventually, the United States is going to have to realize that that form of empire building, those days are done."
- "Empire is bad."
- "If the United States wants a place as a world leader in the future, it's not going to be able to rely on the military."

### Oneliner

Beau explains the flawed Saigon analogy, criticizes America's approach to foreign policy, and calls for a shift towards helping countries stand independently.

### Audience

Policy Makers, Activists, Citizens

### On-the-ground actions from transcript

- Challenge and advocate for a shift in American foreign policy towards helping countries stand on their own (implied).
- Support organizations working towards building sustainable independence in countries affected by U.S. interventions (implied).

### What's missing in summary

The full transcript provides in-depth analysis and historical context behind America's foreign policy failures, encouraging a reevaluation of imperialistic practices and advocating for a more sustainable approach to global influence. 

### Tags

#ForeignPolicy #SaigonAnalogy #AmericanEmpire #MilitaryDominance #PoliticalDefeat


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the Saigon analogy.
Because I got a message I'm really glad I got.
There's a concept that I wanted to talk about but honestly couldn't figure out a way to talk about it
because it's an incredibly complex topic and I couldn't figure out a way to frame it
without getting too far into the weeds of foreign policy.
But then I got this message.
Beau, I'm 22 so unlike you I don't have a clear memory of Vietnam.
I don't know how old you think I am but I don't have a clear memory of Vietnam either.
I heard you say that the Saigon analogy was a bad analogy yet I kept hearing people make it.
So I started reading.
After 30 minutes of reading I realized it doesn't make any sense.
So why do Republicans keep making this analogy?
I'd point out now it's not just Republicans.
There's a lot of people making this analogy because they've heard it
and there are some superficial similarities.
But that's not the question.
The question is why?
Why was this analogy made?
Because they wanted feelings not facts.
They wanted to evoke emotion.
This general concept and theme that existed in the 1980s, they want to tap into it
and they want to tie Afghanistan to Vietnam.
Okay.
So if you don't know, there's a whole bunch of reasons it's not a good analogy.
The easiest one to point to is a timeline.
Okay.
Let's just start with this.
Direct military involvement in Vietnam ended in 1973.
Troops out in March of 1973.
If you were to type into Google Vietnam troop levels by year, the chart would end in 1973.
Saigon fell in 1975.
That alone should tell you there's probably some pretty significant differences
between the two.
But they want to make it look as though Afghanistan was a military defeat,
not a political one.
They want to make it look as though it was a military defeat so they can tap
into the kind of emotion that existed in the 1980s and early 90s.
You may not remember it.
Well, I know you don't, but if you go back and watch movies from that time,
there's a whole bunch of them where they find a way to go back to Vietnam and get even.
The general idea was that we could do it.
We could do it.
We'd go back.
They're going to let us win this time, sir?
It was there.
They want to make Afghanistan look like a military defeat rather than a political one.
We didn't lose the war in Afghanistan.
We lost the peace.
The strongest evidence of this is the fact that in order to effect a withdrawal,
we had to send troops into the country because we didn't have enough troops there
to secure a pretty minimal amount of space.
Why?
Because the fighting was kind of over.
It wasn't over.
It still existed, and we'll get to why.
But the real war part of it...
Well, that was done.
We were fighting the peace.
But if we acknowledge that we lost the peace rather than lost the war,
that it was a political defeat rather than a military one,
that raises a lot of uncomfortable questions.
So we want to avoid that discussion.
Okay.
So the United States, it's incredibly powerful.
Militarily, there is not a country on the planet
that can stand up to it in open conflict.
Not one.
Whichever one you're going to say, it can't.
Okay.
There is not a country on the planet that can meet the United States in open conflict.
So wars are over very quickly.
But because other countries know they can't meet us in open conflict,
the peace becomes a little fuzzy.
There's a lot of low-intensity conflict that goes on along the way.
Because of that, the United States really never shifts gears
into a true reconstruction process.
They still use military doctrine, when maybe they shouldn't.
But that's not the real issue either.
I mean, it doesn't help, but that's not the real issue.
Why did the opposition make such quick gains?
Because Afghanistan couldn't handle it.
The national government just wasn't up to it.
After 20 years, they weren't up to it.
After all we put into training them and equipping them and getting them ready,
they just weren't up to it.
Of course they weren't, because they were never supposed to be.
What's our goal when we go into these countries today?
Is it to get it to where these countries can stand on their own
and they don't need us?
Of course not.
Of course not.
How does that further the American sphere of influence?
That's not what we do.
We try to get them to where they're strong enough to maintain control
of their country for the most part and help us out a little bit,
but they still need the United States.
If we made them too strong, well, they might just decide to align with China.
So we never get them to where they can stand on their own.
And then when we leave, they collapse.
Why?
Because we never got them to where they could stand on their own.
This is a byproduct of the pursuit of American influence, American empire.
This will play out over and over again until we acknowledge
that we have to win the peace.
And in order to win the peace, we have to get it
to where the national governments in any country that we invade
can stand on its own without us.
And yes, that means that they may decide that they don't
want to have anything to do with us.
That's the only way you're going to win.
The attempt to tie this to a military defeat,
to make it look like it was a failed war rather than a failed peace,
is an attempt to avoid these questions.
Because if we start asking ourselves this,
we have to adjust all of our foreign policy.
Politicians have to change their talking points.
None of them are going to be able to say,
well, I'm part of the party that wins wars, which that in and of itself
is funny because historically speaking, the United States
doesn't win wars that have been politicized.
By the nature of the way the United States works,
there has to be a united front.
If it becomes politicized, divided, we don't win them.
But if there is one thing that history has taught us,
it's that we don't learn anything from history.
This idea of we lost the war in Afghanistan,
and therefore we need to blame this politician, this general,
this national government, this goes beyond failed strategy
to failed doctrine.
And the way the United States behaves overseas,
attempting to maintain spheres of influence
by keeping other countries weak.
We wanted the national government in Afghanistan
to be just strong enough to do what we told them.
When we left, they weren't strong enough to stand on their own.
That's American foreign policy.
That's a failure of American foreign policy,
and it's not an isolated incident.
Eventually, the United States is going to have to realize
that that form of empire building, those days are done.
Those days are done.
You can't do it like that anymore.
Understand, empire is bad.
In all of its forms, empire is bad.
The current way of doing it, allowing a little bit
of autonomy, allowing a little bit of control,
allowing a little bit of autonomy,
but not strong enough to stand on their own,
yeah, that's better than the age of imperialism.
But we're going to have to enter a new phase where
it's even softer.
The way that the United States is
going to be able to exert influence in the future,
once it grows up and out of this phase,
is going to be by helping countries develop to the point
where they can stand on their own.
Not to the point where they're so weak
they're dependent on the United States,
but to the point where they can stand on their own
and they align with the United States,
not out of fear of collapse, but because the United States
is leading the world the right way.
Pipe dream, right?
If we keep politicizing these events,
it will stay a pipe dream.
If the United States wants a place
as a world leader in the future, it's
not going to be able to rely on the military.
The American century of American military dominance, it's over.
Not in the sense of we're not strong anymore,
but in the sense of we're not strong enough
anymore, but in the sense of that military might doesn't
mean what it used to.
The world's moved beyond that.
When we go into these countries, the goal is to keep them weak.
The goal is to keep them dependent on the United States.
We can't act surprised that they can't stand on their own.
We never wanted them to.
It was never part of the doctrine.
But if we admit that, we have to acknowledge American empire.
So the people in DC, they will do anything
they can to avoid admitting that we won the war and lost the peace.
Anyway, it's just a thought.
You have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}