---
title: Let's talk about balance and advice for the unvaccinated....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CcnCYdZw72k) |
| Published | 2021/08/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing moral and ethical responsibilities by providing a wide range of viewpoints.
- Acknowledging being informed of neglecting duties and aiming to remedy that.
- Being advised by someone to advise anti-vax people despite being pro-vax.
- Explaining the importance of filling out certain documents for emergency preparedness.
- Providing instructions on filling out an Emergency Child Care Plan for children.
- Emphasizing not to include dogs in the Child Care Plan, as it is for human children.
- Mentioning the importance of filling out a will and power of attorney document.
- Cautioning against designating an unvaccinated individual as a power of attorney.
- Recommending discussing Do Not Resuscitate/Do Not Intubate (DNR-DNI) decisions with family.
- Instructing to designate a location for storing vital documents and records.
- Listing documents to store, including insurance policies, medical records, tax records, and more.
- Advising to include copies of last month's bills for someone taking over financial responsibilities.
- Suggesting deleting internet history as an additional precaution.

### Quotes

- "You're going to fill out this form and you're going to discuss this plan with your children. Don't make us do it."
- "Do not put your dogs on this. This is for human children."
- "You are asking somebody to take over your financial responsibilities in the event of an emergency. You want to make it as easy as possible for the person you are dumping this on."

### Oneliner

Beau addresses moral responsibilities, advises on emergency preparedness documents, and stresses the importance of comprehensive planning and communication for unforeseen circumstances.

### Audience

Parents, caretakers, and individuals needing guidance on emergency preparedness.

### On-the-ground actions from transcript

- Fill out an Emergency Child Care Plan with your children (suggested).
- Fill out a will, power of attorney, and DNR-DNI documents (suggested).
- Designate a secure location for storing vital documents and records (suggested).
- Include copies of last month's bills in the designated location for financial continuity (suggested).
- Delete internet history as a precaution (suggested).

### Whats missing in summary

Detailed step-by-step instructions for emergency preparedness planning and document organization.

### Tags

#MoralResponsibilities #EmergencyPreparedness #DocumentOrganization #Communication #ComprehensivePlanning


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about moral and ethical responsibilities and making
sure that we provide as wide a range of viewpoints as possible.
So, everybody pull up a seat.
We're going to do this
because I was informed that I have been derelict in my duties here. I have been
remiss, and I don't want to be like that, so I would like to remedy that now.
I was informed by command via Twitter
that
I know you're pro-vax, but I feel required to inform you
that you have an ethical and moral responsibility to advise anti-vax people
as well.
Sure.
I got you.
I'm assuming this is advice
beyond, you know, go get vaccinated,
wear a mask, and undertake all the mitigation efforts that might protect
you.
You want something other than that?
Fine. Fine.
What, Murphy?
Yes, you still have to fill it out, too.
I know you filled it out before. We didn't expect you to come back. We threw
it away.
Okay, on the desk in front of you is a packet. If you arrived late and you do
not have a packet, you can get most of these forms from NOLO.com or.org or
whatever it is.
That is N-O-L-O
dot whatever.
All right.
Open up the packet. First
form in the packet is the Emergency Child Care Plan. This is the document
that lets us know what you would like done with your children, with your
offspring.
You're going to fill out this form
and you're going to discuss this plan with your children. Don't make us do it.
All right. I have to say this part of the briefing because of you.
Do not put your dogs on this. This is for human children. While it is advisable
to have a plan for your four-legged friends, they do not go on this document.
The next document
is your will.
Fill it out as you will.
The next document
is a power of attorney.
This is a person that you are designating
to take care of your responsibilities in the event that you become
incapacitated for an extended period of time.
Do not designate your battle buddy.
Do not designate somebody who's around you all the time who is also
unvaccinated.
They may not be up to the task.
The next document is DNR-DNI. Talk it over with your family. Decide if it's
right for you.
In addition to all of this, when you get home tonight,
you are going to get a shoe box, a filing cabinet,
a litter box, inside of a microwave.
Doesn't matter. You're going to designate a location
to put everything that I'm about to list.
First and foremost is your life insurance policy.
Next is copies of all of the documents you are about to create.
After that,
homeowners insurance, car insurance, that kind of stuff.
Copies of your medical and vaccination records.
I forgot that was on the list.
Tax records go in this box.
Court orders such as child support, custody documents, so on and so forth.
You also want to put in there your social security cards, marriage licenses,
birth certificates, that stuff as well.
Passports, vehicle titles and registrations.
Anything that people are going to need.
Any kind of document that is important.
If you've ever said, oh, I don't want to lose this, it goes in this location.
On top of that, you would like to put copies of all of last month's bills in there.
You are asking somebody to take over your financial responsibilities in the event of an emergency.
You want to make it as easy as possible for the person you are dumping this on.
Any questions?
No? Good.
You probably also want to delete your internet history.
But it's just a thought.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}