---
title: Let's talk about the hearing and dueling definitions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4Xi58mf5TJE) |
| Published | 2021/08/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of dueling definitions when experts disagree.
- Describes a scenario where two friends, one from the military and the other a lawyer, disagreed over the use of a term during hearings.
- The military friend argued against using the term "terrorist," proposing "coup" or "self-coup" instead.
- The lawyer, however, was correct in using the legal statute's definition, which differs from the academic perspective.
- Beau suggests that both friends were right from their respective standpoints due to a misunderstanding of definitions.
- Points out the broad and vague nature of the federal statute on what constitutes an act of terrorism.
- Emphasizes that regardless of the semantic debate, the core issue was an attempt to influence the government through violence or threat during the election overturn.
- Advocates for focusing on the critical aspect of the situation, which was the attack on vital US institutions by violence or the threat of violence.

### Quotes

- "When you run into this issue where you have two experts who disagree, maybe start from the position that they're both right, but there's a misunderstanding of what they're talking about."
- "This is a semantic argument. People who are experts, who are very well informed about a topic, generally they also have huge egos."
- "That's the main point is that the institutions that the United States relies on were under direct attack."

### Oneliner

Beau dives into dueling definitions between experts, showcasing a disagreement between a military friend and a lawyer over a term during hearings, illustrating the broader issue of influencing the government through violence.

### Audience

Analytical thinkers

### On-the-ground actions from transcript

- Organize educational sessions to clarify differing definitions and interpretations within specific fields (suggested)
- Engage in respectful dialogues to bridge understanding gaps between conflicting perspectives (implied)

### Whats missing in summary

Beau's engaging delivery and nuanced analysis can best be appreciated by watching the full transcript.

### Tags

#Definitions #ExpertDisagreement #LegalVsAcademic #Influence #Violence #Government


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about dueling definitions, because that's what this boils
down to.
Got a message that basically said, hey, I watched your video about what to do when experts
disagree, but it presupposes the idea that there is a general consensus.
What do you do if there's not a general consensus?
Because I just watched two of my friends disagree, and I was wondering if you could shed a little
bit of light on it.
One of the friends is a quote, bearded word that starts with a B that means your parents
aren't married.
It's a nickname for a specific group in the military.
The other is a lawyer.
And they took issue with the use of a term.
During the hearings, one of the cops used the term terrorist.
Now the person from the army says that's not the right term.
The lawyer says it is.
Who's right?
Okay, so this group, if he was a member of this group, he probably went to a specific
class on this.
And he's using the definition, it's the use of violence or threat of violence to influence
those beyond the immediate area of attack to achieve a political, religious, ideological,
or monetary goal.
Beyond the immediate area of attack, the people they were trying to influence were on scene.
So it's not that word.
That's not the right word to use.
He probably threw out the term coup or self-coup.
Self-coup would be the right term, assuming that it was organized and intentional.
Does that mean the lawyer is wrong?
No.
No, the lawyer's right too.
The lawyer is right as well.
Because the lawyer's not using that definition.
The lawyer is using the statute, which I'm not mistaken, the cop read during the hearing.
The statute does not include that section about having to be, having to influence those
away from the immediate area.
That's not part of it.
So they're both right.
When you run into this issue where you have two experts who disagree, maybe start from
the position that they're both right, but there's a misunderstanding of what they're
talking about.
That's why they disagree.
And I will tell you that this isn't a new argument.
This is hotly contested.
The federal statute is super broad, like ridiculously broad.
I think the elements to it are, it has to be dangerous to human life and has to appear
to intend to intimidate or coerce.
Like that's it.
It's a ridiculously broad statute.
I'm not a lawyer, but I think it's probably so vague, it's void.
It's really broad.
Now is that the right term to use legally?
I don't know.
I'm not a lawyer, but I'm pretty sure that that chicken from Futurama could make the
case that the term applies.
But when you're talking about the academic theory, it doesn't because it's missing that
part of the definition.
This is a semantic argument.
People who are experts, who are very well informed about a topic, generally they also
have huge egos.
So they may start arguing over nothing.
This whole discussion that you describe could have been solved, could have been avoided
by simply explaining this is the difference.
This is the missing piece.
This is the definition.
So there you go.
Do I think it's appropriate to use that term?
I would go with self-coup to me.
But there is no self-coup statute in the federal books.
Discussions like this kind of get away from the main points here.
And the main point is that it certainly appears that the use of violence or the threat of
violence was used or attempted to be used to influence the federal government and overturn
the election.
I don't think anybody should lose sight of that.
That's what was going on.
That's the important part is that the institutions that the United States relies on were under
direct attack.
Now you can argue about that all you want, but when you get to the end of the day, that's
what happened.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}