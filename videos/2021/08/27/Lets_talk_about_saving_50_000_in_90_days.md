---
title: Let's talk about saving 50,000 in 90 days....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=G5OHvAEvSxA) |
| Published | 2021/08/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an overview of projections for the next 90 days regarding public health.
- Mentioning the potential loss of 100,000 lives over the next 90 days without proper measures.
- Pointing out that wearing masks in public could reduce this number by half.
- Noting the quantified cost of not wearing masks in public: about 50,000 lives over three months.
- Sharing news about a judge in Florida overturning the ban on schools mandating masks.
- Emphasizing the individual choice to save lives by wearing a mask or ignoring the consequences.
- Stressing the reality of the situation and the tangible impact on lives if mask-wearing is not practiced.
- Encouraging engagement in all available mitigation efforts to reduce the loss of lives.
- Framing mask-wearing as a patriotic duty and an act of love for country and neighbors.
- Describing the current situation as an intelligence test with life-or-death implications.
- Urging people to change their behavior to prevent unnecessary loss of life.
- Summarizing that wearing a mask daily can significantly reduce the number of lives lost.
- Concluding with a call to action to wear masks for the greater good and as part of a critical battle.

### Quotes

- "It's worth noting that here in Florida, a judge did just throw out DeSantis' little edict saying that schools could not mandate masks."
- "If you love your country, if you love your neighbors, if you want to be a patriot and do your part in the greatest battle this country has seen in a very long time, all it takes is wearing a mask."
- "This isn't just a public health issue anymore. It's an intelligence test with life-or-death consequences."

### Oneliner

Beau stresses the importance of wearing masks to reduce the projected loss of 100,000 lives over 90 days, making it a patriotic duty and an intelligence test with life-or-death consequences.

### Audience

General public, mask-wearing advocates

### On-the-ground actions from transcript

- Wear a mask daily to reduce the projected loss of lives. (exemplified)

### Whats missing in summary

The emotional appeal and urgency conveyed by Beau in the full transcript may not be fully captured in this summary. Viewing the full content will provide a more profound understanding of his message.

### Tags

#PublicHealth #MaskWearing #Patriotism #Community #Mitigation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about numbers, projections,
what's going to happen over the next 90 days,
at least what is expected to happen over the next 90 days,
how we can change that, what it would take to change that,
and a little bit of news out of Florida.
The model that is used to gauge our response
and kind of inform our decisions when it comes to our current public health issue.
It is suggesting that over the next about 90 days, we are going to lose 100,000 people.
100,000 people.
But that number could be cut in half if people would just wear a mask in public.
The cost of not wearing a mask in public can now be quantified.
Apparently, it's about 50,000 people over three months.
That seems a pretty high price to pay.
It's worth noting that here in Florida, a judge did just throw out DeSantis'
little edict saying that schools could not mandate masks. But it goes beyond
that, right? Because we're talking about something that is in many ways and in
many locations totally up to the individual. It is their freedom to save
50,000 lives if they choose to exercise it or they can choose to ignore it.
That's the cost. 50,000 lives over 90 days.
I'm not sure that even those who say that it's like super hard to wear a mask
would honestly say that that's worth the cost.
We're past the point of being able to deny this is real.
It's a real thing and it's costing real lives.
50,000 more will be lost if you don't wear a mask in public, it's that simple.
Those are the numbers.
And those numbers could go way down if people would engage in all of the mitigation efforts
that are available.
This is just one.
One that would cut it in half.
50,000 lives, 1,000 lives per state.
Seems like it would be worth it to me to wear a mask.
If you love your country, if you love your neighbors,
if you want to be a patriot and do
your part in the greatest battle this country has
seen in a very long time, all it takes is wearing a mask.
This isn't just a public health issue anymore. It's an intelligence test with
life-or-death consequences. There are a lot of people who are failing it. Some of
them are getting caught up and getting a failing grade because of the person
next to them. It's probably something we want to change. Fifty thousand over three
months. And all it takes from you is to wear a mask for just a little bit each
day. Anyway, it's just a thought. God have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}