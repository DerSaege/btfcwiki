---
title: Let's talk about Kennedy, Malcom Nance, and criticism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rurDFIgHiKI) |
| Published | 2021/08/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau receives a message pointing out that he and Malcolm Nance were early in predicting the fall of Afghanistan and questioning official estimates.
- Beau mentions Noam Chomsky's accurate estimate, indicating that he is not the only one who foresaw the situation.
- Despite similar views on most topics, Beau notes a difference with Nance where Nance reportedly referred to the Afghan people as ignorant hill people or goat herders.
- Beau questions why he and Nance are not criticizing the administration for what he sees as an abysmal failure in the withdrawal from Afghanistan.
- Beau brings up Kennedy's philosophy of questioning whether news is in the interest of national security, but he alters the premise to focus on saving lives.
- Beau believes that the current situation in Afghanistan, while chaotic and tragic, is not as bad as it could have been or potentially could become.
- He refrains from discussing a critical oversight that could escalate the situation, choosing to wait until people are out of harm's way before making such criticisms.
- Beau criticizes the major outlets for focusing on the Biden administration providing a list for safe passage, which he sees as a common practice throughout history.
- He points out the risks involved in the hasty withdrawal and regime change in Afghanistan but challenges critics to provide workable alternatives.
- Beau expresses his decision to withhold criticisms to avoid endangering lives and suggests that others should do the same during this dynamic situation.

### Quotes

- "Why are you carrying water for the administration?"
- "I don't believe the people making that criticism today are doing it in good faith."
- "I will hold my criticisms until the end because I don't want to get people killed."
- "It's not an insult. It's actually a compliment."
- "I strongly suggest you all watch him."

### Oneliner

Beau questions why he and Malcolm Nance aren't criticizing the administration's actions in Afghanistan, opting to withhold criticisms until people are safe, prioritizing saving lives.

### Audience

Community members, analysts.

### On-the-ground actions from transcript

- Hold valid criticisms until after people are out of harm's way (implied).
- Watch and stay informed about various commentators' perspectives for a well-rounded understanding of situations (implied).

### Whats missing in summary

Beau's emphasis on prioritizing saving lives and avoiding politicization in analyzing the situation in Afghanistan. 

### Tags

#Afghanistan #CriticalAnalysis #PrioritizingLives #MediaCriticism #PolicyEvaluation


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about Kennedy,
Malcolm Nance, at least I think this is referencing Malcolm Nance,
good faith criticisms,
and an apparent anomaly,
because I got a message.
You and Nance are the only two who called early on the fall of Afghanistan.
You are the only two who also called into question the official estimates
saying how long that fall would take.
At first, I don't actually know that that's true.
In fact, I know that Noam Chomsky provided an estimate that was
incredibly accurate, scary accurate to be honest.
You two seem to be in lockstep
on pretty much everything.
The only place where you differ is that he does believe they are ignorant hill people.
He pretty much called them goat herders.
My question is this.
If both of you saw it, why are you carrying water for the administration?
Why aren't you criticizing this abysmal failure of a withdrawal?
I'm assuming you're talking about Malcolm Nance.
If you don't know who he is, he's ex-CIA
and provides a lot of commentary now.
I don't know what he said.
I don't know what he said because I don't watch him, and that's not an insult.
We'll get to why I don't watch him in a little bit.
But I will say that I have never heard anybody say that Malcolm Nance was diplomatic.
I'm going to guess that he wasn't actually calling them ignorant hill people.
My guess is that he was more talking about the simplicity of their life and what they want.
I don't know because I don't watch him, but I just want to throw that out there.
Kennedy once said, he once asked the press,
he said that before you publish a story, you should ask yourself two questions.
Is it news?
And is it in the interest of national security?
I don't think those are good questions, but I like the premise.
Is it news?
And is it in the interest of saving lives?
Those are the questions I ask.
If you saw this coming, there are a limited number of ways in which it can occur.
What you are witnessing right now is the best case scenario.
You're calling it abysmal.
Okay, yeah, it's pretty chaotic.
It's been tragic.
There's a lot of bad stuff that's happening.
But understand, it's not as bad as it could have been
or as bad as it can still get by any means.
I honestly had a whole lot more people leaving on planes horizontally
than what is occurring right now.
I think most people who pictured a quick collapse probably saw the same thing.
Most did not expect the new de facto government to behave the way it is behaving.
This can still get really, really, really bad.
There are plenty of criticisms to be made.
There's one that to me is a glaring oversight that at any moment
could send the whole thing sideways and it could get really bad really quick.
And right now, it's like the Pentagon took it, hung it out there,
and they could drop it and the whole thing catch fire at any moment.
But I'm not going to talk about it because the opposition,
well, they get the internet too.
And that's not in the interest of saving lives.
It seems like the responsible thing to do would be to hold real criticisms,
good faith criticisms, valid criticisms, until after people are out of harm's way.
There are plenty to be made, but they're not the ones you see on the major outlets.
Today, right now, everybody's talking about the Biden administration provided a list.
That's unheard of.
No, it's not.
It's not unheard of.
It's not even uncommon.
The concept of safe passage has existed for thousands of years.
Is it risky?
Well, let's see.
We're in the middle of a hasty withdrawal in a country undergoing a regime change
that is also simultaneously undergoing a system of governance change
that was taken over by a non-state actor.
Yeah, it's risky.
Do you have an alternative?
I'm willing to bet you don't.
Not a workable one.
I would point out that, well, yeah, I would not want to be the first person
going through a checkpoint having been cleared this way.
I wouldn't.
But I would point out this has been going on a while,
and we don't have videos of Americans holding newspapers right now.
So far, they are keeping up their end.
That could change.
That could change because this is a very dynamic situation.
But I don't believe the people making that criticism today are doing it in good faith.
I know most of them aren't because yesterday, the criticism was that we should have kept Bagram,
an air base that's an hour and a half away from the capital.
In order to get to it, you would have to rely on safe passage.
The criticisms, half the time, they don't make sense.
They display a basic failing of understanding what's going on,
and they're contradictory.
It's politicizing this moment.
And in the process, well, providing the opposition groups there,
and there are more than one, with ideas.
Because I guarantee you, since this news came out,
the group responsible for the move on the airport,
oh, they are looking for those lists.
I will hold my criticisms until the end because I don't want to get people killed.
It's really that simple.
I can't speak for Nance, but given his history,
I would imagine that his motivations are probably kind of similar.
The reason I don't watch him, okay, because I said that earlier and I want to clarify,
it's not an insult.
It's not an insult.
It's actually a compliment.
There are some people who, when I'm working on something, I put them on mute.
I don't listen to anything they say.
I completely ignore them, not because they are bad, but because they are good.
And you don't want their estimate to influence yours.
That's why you had a whole bunch of them that were wrong,
because people talk to each other and they let each other influence their estimates.
So you wound up with this pile that said the national government was going to last months.
If your objective looking at it, that's not really likely.
That wasn't the likely outcome.
So I have no idea what his takes are.
But I strongly suggest you all watch him.
I won't until this is over.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}