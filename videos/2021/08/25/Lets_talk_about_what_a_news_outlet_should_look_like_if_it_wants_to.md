---
title: Let's talk about what a news outlet should look like if it wants to....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QdMRv1oF5dI) |
| Published | 2021/08/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring a reverse Q&A format today, starting with the answer before the question.
- Expressing insightful views on media manipulation, focusing on division and fear tactics.
- Advocating for sowing discontent within dominant groups to incite violence and weaken systems of government.
- Suggesting strategies to undermine national power symbols and create paramilitary groups.
- Linking patriotism with factional obedience to drown out dissenting voices and foster fear-based thinking.
- Sharing a hypothetical scenario about a new country facing threats from a larger, powerful nation.
- Describing a plot involving a think tank trying unconventional methods to destabilize the powerful nation.
- Posing a question about infiltrating a news outlet to weaken a powerful nation through messaging.
- Drawing parallels between the hypothetical messaging and the practices of certain news outlets.
- Concluding with thoughts on the impact of specific messaging on a country's stability.

### Quotes

- "They will be more susceptible to fear-based reasoning."
- "Conflate patriotism with obedience to a faction within the nation, and drown out all other voices."
- "If somebody was to sit down and try to determine what messaging could be carried by a news outlet to bring the United States to the brink, that news outlet, it would look exactly like Fox News."

### Oneliner

Beau explains media manipulation tactics, urging division and fear within dominant groups to weaken systems of government, paralleling Fox News' strategies.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Analyze news sources critically to identify potential divisive tactics and fear-mongering (suggested).
- Support media outlets that prioritize balanced reporting and diverse voices (implied).

### Whats missing in summary

Insights on how media narratives can shape societal perceptions and influence political landscapes.

### Tags

#MediaManipulation #FearTactics #CriticalAnalysis #CommunityAction #NewsReporting


## Transcript
Well, howdy there, Internet of People. It's Beau again.
So today we're going to do things a little bit differently.
Many times, I'll start off these videos by reading you a question I received,
and then I provide the answer.
I mean, that makes sense. That's a sound format.
Today we're going to do it backwards.
I'm going to read you the answer,
and then I'm going to tell you what the question was.
Because I received a question,
and I broke out my whiteboard and started making notes for this person,
and then I read what I wrote.
And it's interesting.
The news outlet should divide the populace
along racial, religious, and ethnic lines.
Side with the dominant group,
because the dominant group has more to lose.
They will be more susceptible to fear-based reasoning.
They're afraid of losing what they have.
Other, marginalize and tokenize all other voices.
Stoke discontent among the dominant power group,
because the dominant power group is more likely to use violence,
because they're more likely to get away with it,
because they're the dominant power group.
Undermine systems of government to include representative institutions.
So you remove that voice from the people that are marginalized,
those that got othered.
Safety institutions.
The example I gave was public health,
because it would make the government seem ineffective and weak.
Perhaps it needs to be replaced.
Symbols of national power, particularly the military.
If you couple that with rhetoric saying that the country needs to remain strong,
it is likely that paramilitary groups would spring up.
Conflate patriotism with obedience to a faction within the nation,
and drown out all other voices.
Feed conspiratorial and fear-based thinking,
because fear is a paralytic to a country and freezes progress,
makes it weak.
If you're like me, reading this, hearing this,
you probably have a certain US news outlet in mind.
The question that prompted this had nothing to do with Fox News.
I have a friend who's working on a book.
The general plot of the book is that there is a small new country,
newly established country, and it shares a border
with a much larger, established, more powerful country
that keeps threatening the new country.
The new country doesn't have any resources, doesn't have money.
It's new. It's weak.
So it can't meet the more powerful country on the open battlefield.
The plot is the stories told through the eyes of a think tank
that is trying to come up with unconventional means
to destabilize and degrade the more powerful nation.
The question that prompted this is,
what would the messaging look like if the think tank infiltrated
a news organization in the more powerful country
if the messaging was designed to weaken it until it breaks?
That was the answer.
I don't believe that Fox News is some opposition operation.
I really don't. They're just trying to make money.
But if somebody was to sit down and try to determine
what messaging could be carried by a news outlet
to bring the United States to the brink,
that news outlet, it would look exactly like Fox News.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}