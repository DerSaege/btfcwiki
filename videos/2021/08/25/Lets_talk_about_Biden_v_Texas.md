---
title: Let's talk about Biden v. Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FlURzCuaDpU) |
| Published | 2021/08/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Supreme Court rejected Biden's request to set aside a lower court ruling on the Remain in Mexico policy, putting Biden in a tough spot.
- This ruling interferes with foreign policy, a domain traditionally left to the executive branch.
- Biden now lacks leverage in negotiating with Mexico over the policy, potentially allowing Mexico to ask for significant concessions.
- The Supreme Court's decision limits Biden's ability to negotiate effectively, similar to Trump's challenges in Afghanistan due to being too transparent about intentions.
- Uncertainty looms over the future implications if Mexico refuses to cooperate, signaling uncharted territory and potential complications arising from judicial intervention in foreign affairs.

### Quotes

- "The Supreme Court just walked up behind Biden holding a mirror and showed everybody his cards."
- "Foreign policy is the purview of the executive branch, and it should probably stay that way."
- "This is a case that went from pretty boring to incredibly interesting very, very quickly."

### Oneliner

The Supreme Court's ruling on the Remain in Mexico policy limits Biden's leverage in foreign policy and sets a concerning precedent for executive authority.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Stay informed on the developments regarding the Remain in Mexico policy and its implications (implied)
- Advocate for transparency and accountability in foreign policy decisions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of the Supreme Court's ruling on the Remain in Mexico policy and the challenges it poses to Biden's negotiation leverage and foreign policy execution.

### Tags

#Biden #SupremeCourt #ForeignPolicy #RemainInMexico #JudicialInterference


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Biden versus Texas tradition
and what happens next.
So this court case was one that not a whole lot of people were paying attention to.
It really didn't seem that important.
It wasn't getting a whole lot of coverage
because nobody expected the Supreme Court to straight up lose their mind.
But that's what happened.
So now we have to pay attention to it because it's got some interesting effects.
Let me catch you up on the case.
Under Trump, the Remain in Mexico policy was put into place.
This kept asylum seekers on the other side of the border
while their stuff got processed, basically.
Now, the policy effectively ended during the Trump administration
because of the whole public health thing, kind of put it by the wayside.
But Biden officially ended it.
A lower court ruled that Biden had to reinstate it.
The Biden administration went to the Supreme Court
and asked them to kind of set that aside for a little bit
until the appeal process went through.
The Supreme Court said no.
The Supreme Court said no.
And they gave a justification of basically saying
that they didn't expect the appeal to succeed
because it's going to be hard for the Biden administration
to prove that their reasoning for getting rid of the policy
wasn't arbitrary and capricious.
Now, I have no idea if that's a good ruling or not.
I don't know anything about this kind of law.
When it comes to this stuff, I don't know anything about it.
However, it's a horrible ruling.
And it should be sending up red flags.
State Department should be having a full-blown meltdown right now.
Traditionally, the courts have stayed out of foreign policy.
The remain in Mexico policy is foreign policy.
I'm not sure how Biden is supposed to enact this.
He has to get permission from Mexico.
What if they don't want to?
Does Biden get held in contempt of court?
What occurs?
Or worse yet, what if Mexico understands the leverage
they suddenly have?
That international poker game where everybody's cheating?
The Supreme Court just walked up behind Biden holding a mirror
and showed everybody his cards.
Mexico is going to understand that the Biden administration
has to do this.
They can ask for whatever they want, a billion dollars,
a trillion dollars.
Texas.
Does Biden have to do it?
This ruling is wild.
The courts don't have the power.
American courts do not have the power
to compel the Mexican government to do something.
Biden has to negotiate for it.
And they know what he wants.
Therefore, he has no leverage.
Think about it when we were talking about Trump
working the deal in Afghanistan.
The reason his negotiators had so much trouble
is because he kept walking around screaming
that he wanted to get out of Afghanistan.
It's not that that was a bad idea.
It's not that he was wrong about needing to leave.
That was all right.
It was the fact that he kept openly saying it
that put his negotiators at a disadvantage.
The president isn't supposed to be clear on stuff like this.
Right now, you have a whole bunch of people
talking about Biden and how Biden won't commit to a date.
August 31st, September 11th, the 7th, whatever.
They're making it seem as though he's just confused.
That's exactly what he's supposed to be doing
because his negotiators can use that.
Well, the president may decide to stay.
We don't know.
That's actually the art of the deal.
The Supreme Court took that ability away from Biden
when it comes to the Remain-New Mexico policy.
He can't do that.
They know he wants it.
I have no idea where this goes from here.
I don't know what happens if Mexico doesn't agree.
I don't know the course of action.
We are definitely in uncharted territory.
This is why the courts have stayed out of foreign policy.
Foreign policy is the purview of the executive branch,
and it should probably stay that way.
Otherwise, you're going to have a lot of problems.
This is a case that went from pretty boring
to incredibly interesting very, very quickly.
So I would imagine seeing more coverage on this
as it progresses.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}