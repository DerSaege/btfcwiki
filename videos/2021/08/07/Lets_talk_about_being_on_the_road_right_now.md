---
title: Let's talk about being on the road right now....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7pl0TUOWMpw) |
| Published | 2021/08/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau finishes a large-scale production for his second channel, Beau on the Road, after traveling seven days through eight states covering roughly 4,500 miles from the Gulf Coast to the West Coast, ending in LA.
- Despite testing, masking, and being vaccinated, Beau is hesitant about doing meet and greets and setting up community networks due to the lack of precautions along the highways and interstates.
- Native reservations are the only places with consistent protocols while most other areas lack clear safety measures.
- Places like LA show more compliance compared to areas like Arizona and Texas where people seem to disregard the ongoing threat of the pandemic.
- Beau expresses surprise at the reluctance of people to take the available vaccine or treatment despite its existence.
- The production filming went well overall, with minor hiccups expected, but Beau is uncertain about duplicating it soon due to concerns about COVID-19 transmission.
- Beau stresses the importance of taking precautions seriously, even if those around you are not, and mentions that the upcoming episode will take time to put together.
- He encourages everyone to stay safe and concludes with a reminder to take the situation seriously, even if others are not.

### Quotes

- "Please take it seriously. Take the precautions even if those around you aren't."
- "Y'all just be safe out there."
- "I'm not sure how soon we're going to duplicate this."
- "People still are just kind of blowing this off as if it's over."
- "I just didn't know that people weren't going to take it."

### Oneliner

Beau finishes a successful production trip but expresses hesitance due to lack of COVID precautions, urging everyone to take safety seriously.

### Audience

Travelers, community members

### On-the-ground actions from transcript

- Take COVID precautions seriously, even if those around you aren't (suggested)
- Stay informed about the COVID situation in your area and act accordingly (implied)
- Encourage others to follow safety guidelines (implied)

### Whats missing in summary

The emotional impact of seeing the lack of COVID precautions firsthand during a long production trip.


## Transcript
Well, howdy there internet people, it's Bo again.
So hopefully, by the time y'all watch this,
I'll be at home, asleep.
We are finishing up the first large scale production
for the second channel, for the Bo on the Road channel.
We're on the road seven days, eight states,
roughly 4,500 miles,
which from the Gulf Coast to the West Coast,
the way to LA. It's been a lot of fun and everything went pretty well. All that
being said, even with testing for us, masking, everybody's vaccinated, not sure
how comfortable I feel actually going out and doing the meet and greets and
setting up the community networks and doing all of that stuff yet. There is not
a lot of precautions going on out here on the interstate, on the highways,
anything like that. The only area that we traveled through that had consistent
protocols in place were the reservations, were the native reservations. That's it.
That's all you got. Everywhere else is pretty haphazard. Now when we were out in
LA, there was obviously a little bit more compliance. However, when you're in
Arizona or Texas, for example, there's none. There is none. People still are just
kind of blowing this off as if it's over. It's not. It's going to keep going. You
know, once all of this got started up, I said it was gonna continue until there
was a safe, available, reliable vaccine or treatment. I mean, that exists. I just didn't
know that people weren't going to take it. So there's that. While filming production,
all of that stuff has been very successful. Everything went according to plan. I mean,
minus the obvious normal stuff that's going to go wrong. I'm not sure how soon we're going
duplicate this. I would feel horrible if somebody working with me wound up being
somebody that was transmitting it and since there is so little compliance
it's probably going to be put off a little bit longer but we did get out
here to test it to see what it would be like with a very minimal crew. So all
All that being said, this episode will probably take a while to put together because it's
going to be pretty long and y'all just be safe out there.
Please take it seriously.
Take the precautions even if those around you aren't.
Anyway, it's just a thought.
Y'all have a good day. Bye.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}