---
title: Let's talk about the national anthem....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=58LaxG9Jdsc) |
| Published | 2021/08/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the interpretation and meanings of the third verse of the US national anthem, the Star Spangled Banner, in response to a viewer's query.
- Providing context that the anthem is based on a poem written during the War of 1812, not the Revolutionary War, and questioning why it's the national anthem.
- Describing Francis Scott Key, the anthem's writer, as a racist who believed black people were inferior and evil.
- Analyzing the controversial third verse that mentions "hireling nor slave" and discussing four possible interpretations, including references to colonial marines and impressment.
- Asserting that the true meaning behind the verse remains unknown as Key never clarified it, debunking any certainty in interpreting it.
- Suggesting that the offensive nature of the anthem, considering its lyrics and Key's beliefs, gives grounds for people to be upset.
- Pointing out the lack of positive interpretations in the anthem and proposing that it should be changed due to its problematic content.
- Challenging the idea of tradition by revealing that the anthem was officially adopted by Congress as the national anthem less than 100 years ago in the 1930s.

### Quotes

- "There's no good way to read this."
- "The only interpretation that isn't just absolutely horrible is that it's an insult to all British people."
- "There's no reason it can't be changed."

### Oneliner

Exploring the controversial third verse of the US national anthem, Beau questions its racist origins, provides interpretations, and suggests that it should be changed due to its offensive content.

### Audience

Americans

### On-the-ground actions from transcript

- Advocate for changing the US national anthem (suggested)
- Educate others about the problematic history and lyrics of the anthem (implied)

### Whats missing in summary

Full understanding of the US national anthem's controversial third verse and its implications.

### Tags

#USNationalAnthem #Racism #FrancisScottKey #Interpretations #Change


## Transcript
Well, howdy there, internet people. It's Beau again. So today we're going to talk about a song
and its third verse. We're going to talk about different interpretations and meanings,
what it might mean. We're going to explore the discussion. We're going to do this because one of
y'all asked. I got a message detailing the back and forth between this person and their friend.
And they couldn't get to the bottom of it, but they want to know if it's as bad as it sounds.
And they said that if I couldn't do it, nobody could. And that's true. So today we're going to
talk about the Star Spangled Banner, the US national anthem. And we're going to talk about
whether or not it's racist. Okay, so a little bit of context to start with.
It's a poem that was written about a battle during the war of 1812, not the Revolutionary War.
And if you ever sit down and read it, you're going to wonder why it's the US national anthem.
It's basically a poem that is bragging about killing people during a war. It's this hyper
militaristic. And as I say this, all of a sudden, yeah, it makes complete sense that it's the US
national anthem. It was written by Francis Scott Key. And he would certainly play into it.
You know, the question is, is it racist? Well, was Francis Scott Key racist? Oh, yeah, definitely.
Even by the standards of the day. He said that black people were a distinct and inferior race.
And that they were the greatest evil that could be afflicted on a community.
So the idea that it's just impossible that a dude who said that
would write something racist, I mean, that doesn't make any sense. Of course he would.
So the trouble starts in the third verse. And it says, no refuge could save the hireling nor slave
from the terror of flight or the gloom of the grave. There are four interpretations to that.
You hear the word slave. The first is that it's slaves as we picture them today.
People still in slavery. That's almost certainly wrong. That's probably not what he meant.
The next interpretation is that it's a reference to the colonial war.
Now these were liberated slaves who were fighting for the British in exchange for their freedom.
Given some of the other stuff that Key said, he might hold a special animosity
towards black people who were fighting for the other side. So that makes sense.
Then there's the idea that it's a reference to impressment, which was a term that was used
in the early 1800s. Then there's the idea that it's a reference to impressment, which was a British
habit of rolling up to merchant ships, pulling some people off, and being like, hey, you're in the navy now.
Okay, given the fact that that was one of the stated reasons for the war, all right.
And then the last is that it's an insult to all British people, because since they're subject
to slavery, they are all slaves. I'm not buying that one. Those two in the middle make sense though.
Which is it? We don't know. That bit about if I can't do it, nobody can? Nobody can.
We don't know. Francis Scott Key never said. So anybody who is certain of their opinion on this is wrong.
We don't know. Short of finding a letter from him explaining it, it's a guess.
Me, I think it's a reference to the colonial marines. And this is entirely based on the fact that
hireling is literal. Hireling is a reference to the contractors of the day, mercenaries.
That's literal. So I'm going to say that slave is literal, and it's a reference to them.
It should be noted that Americans being Americans, at the end of the war, they demanded that the
British return their property. Now the British said no, that they didn't return them. If you're
Canadian and you're familiar with a community called Americans, those are their descendants.
Those are their descendants. They settled in Canada. So this discussion comes up because
there are some people who find this song offensive. Given the language in the song and Key's other statements,
I mean, yeah, people have... that's grounds to be offended. Absolutely. It definitely could mean
that it is celebrating killing slaves. Yeah. The reality is if you sit down and read this song,
there's no good way to read it. Either it is celebrating Americans who were impressed by the British
and forced to fight, and it's celebrating their deaths, or it's celebrating the death of people
who were literally fighting for their freedom. They were quite literally fighting in exchange for their freedom.
There's no good way to read this. The song itself is kind of bad.
I don't know that it is what the United States wants to represent it.
You know, I know people are going to talk about tradition. The reality is this wasn't officially adopted by Congress
as the national anthem until the 1930s, less than 100 years ago. It's not a long-running tradition.
Prior to this, it was my country tis of thee tended to serve as the national anthem during that period.
There's no reason it can't be changed. I would suggest that maybe it should be.
There's no hard answer to this. I can't settle the debate, but the question that provokes this debate
is whether or not the people who are upset by it have grounds to be upset. That I can answer.
Yeah, absolutely. The person who wrote this held some pretty messed up views, even for the time.
And there is certainly language in the song that is not good. It doesn't matter whether it's about slaves
as we see them, or about the colonial Marines, or about impressment. It doesn't matter. It's all bad.
There's no good way to read this. The only interpretation that isn't just absolutely horrible
is that it's an insult to all British people. That's the best interpretation.
There's no reason it can't be changed. And if you ever sit down and read the whole thing,
you would probably want it to be. Is that really what we want to represent this country?
I mean, it's not a great piece, to be honest. Not only that, nobody can sing it.
Most people don't know anything beyond the first verse. There are probably better alternatives.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}