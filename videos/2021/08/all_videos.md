# All videos from August, 2021
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2021-08-31: Let's talk about trains (trolleys), planes, and automobiles.... (<a href="https://youtube.com/watch?v=cm4zwo5EvJw">watch</a> || <a href="/videos/2021/08/31/Lets_talk_about_trains_trolleys_planes_and_automobiles">transcript &amp; editable summary</a>)

Beau tackles ethical dilemmas, urging a deeper understanding to break the cycle of recurring crises and advocating for long-term solutions over short-sighted interventions.

</summary>

"Everybody wants to be a cat until it's time to do cat stuff."
"When you're out there, it gets real gray real quick."
"The lesson that is being taught by Afghanistan, it's not being learned."
"These types of conflicts will continue until we learn the lesson."
"We're still going to allow that trolley car to run away the next time some charismatic politician waves the flag."

### AI summary (High error rate! Edit errors on video page)

Exploring the ethical dilemma of making tough decisions in crisis situations.
Drawing parallels between a real-life scenario and the classic trolley problem ethical dilemma.
Emphasizing the gray areas and consequences of decision-making in high-pressure situations.
Addressing the specific incident as an outlier and not reflective of typical United States engagements.
Arguing for a deeper understanding of the root problem to prevent recurring crises.
Critiquing the lack of learning from past conflicts and the perpetual cycle of similar outcomes.
Calling for a shift in approach to prevent future crises and avoid repeating past mistakes.
Challenging the idea that different political parties in power will significantly alter outcomes in conflicts.
Criticizing the tendency to ignore lessons from conflicts like Afghanistan and continue with unchanged strategies.
Urging for a change in mindset to prioritize long-term solutions over short-sighted interventions.

Actions:

for policy makers, activists, citizens,
Question current intervention strategies (suggested)
Advocate for long-term solutions (implied)
</details>
<details>
<summary>
2021-08-31: Let's talk about the US exit and criticism.... (<a href="https://youtube.com/watch?v=P0Fo5zGI00Y">watch</a> || <a href="/videos/2021/08/31/Lets_talk_about_the_US_exit_and_criticism">transcript &amp; editable summary</a>)

Beau examines invalid and valid criticisms of the US exit from Afghanistan, stressing the challenges of securing the capital and the importance of effective messaging in humanitarian operations.

</summary>

"The war at that point was over. There's no sense in wasting more."
"Bad messaging throughout. And that may not seem important, but it really is."
"Despite the chaos, this went really well."
"The operation itself was a success."
"They just want to criticize because they care more about their poll numbers than they do American lives."

### AI summary (High error rate! Edit errors on video page)

The United States has officially left Afghanistan, marking the end of a twenty-year mission.
Critiques and criticisms of the exit strategy are being examined, with a focus on invalid and valid criticisms.
Invalid criticism includes the idea that if the US had secured the capital, the chaos at the airport could have been avoided.
Securing the capital with a population of over four million people was not feasible given the limited troops available.
Critics suggesting that seizing the capital was the right move either lack understanding or are politicizing the situation.
The withdrawal from Afghanistan meant the US was not in a position to seize the capital, as it was no longer a war zone.
Valid criticisms involve tactical, operational, and strategic issues with the exit strategy.
Tactical errors included American troops immediately administering aid at the airport rather than securing the area first.
Operationally, pushing back the perimeter around the airport further could have improved security.
Strategically, the Biden administration faced challenges with messaging, failing to effectively communicate the humanitarian nature of the operation.
Missteps in messaging led to public confusion and may have undermined public support for the mission.
Proper messaging could have mitigated opposition propaganda and garnered more support for the operation.
Despite challenges and tragic incidents, Beau believes the operation overall was a success, but acknowledges areas for improvement in future missions.
Beau suggests that critics demanding immediate evacuations from Afghanistan should draft legislation granting the President such powers if they truly believe it is necessary.

Actions:

for policy analysts,
Contact senators and members of Congress to pass legislation addressing evacuation powers (suggested)
Advocate for clear and informative messaging in humanitarian operations (implied)
</details>
<details>
<summary>
2021-08-30: Let's talk about safe passage and a continuation of politics by other means.... (<a href="https://youtube.com/watch?v=U-ZmHozdDTU">watch</a> || <a href="/videos/2021/08/30/Lets_talk_about_safe_passage_and_a_continuation_of_politics_by_other_means">transcript &amp; editable summary</a>)

Developments show the de facto government negotiating safe passage, benefiting from removing dissidents and gaining legitimacy through international relations.

</summary>

"Politics makes strange bedfellows."
"Trust but verify."
"War is a continuation of politics by other means."

### AI summary (High error rate! Edit errors on video page)

Talks about developments that are not getting coverage, marking a shift in dynamics.
Mentions the de facto government negotiating with foreign powers and offering safe passage beyond the deadline.
Questions why trust should be placed in the de facto government and why they are extending safe passage.
Emphasizes that politics is not always transactional and that interests can sometimes align.
Suggests that removing dissidents from the country benefits the de facto government.
Explains the importance for the de facto government to maintain peace and legitimacy on the international scene.
Points out that securing safe passage may encourage mining operations, bringing economic benefits.
States that it is in the de facto government's interest to provide safe passage, legitimize themselves, and gain financially.
Mentions historical examples of negotiations with "bad guys" after conflicts.
Stresses the commonality of negotiating with opposition post-conflict and the continuation of politics after war ends.
Talks about propaganda during wartime and how it influences public perception.
Concludes that negotiations post-conflict are a common practice to solidify the de facto government's position.

Actions:

for foreign policy analysts,
Contact organizations involved in international relations for updates and insights (suggested).
Stay informed about developments in foreign policy and their implications (suggested).
</details>
<details>
<summary>
2021-08-29: Let's talk about learning from history.... (<a href="https://youtube.com/watch?v=G4r7ORDMOQc">watch</a> || <a href="/videos/2021/08/29/Lets_talk_about_learning_from_history">transcript &amp; editable summary</a>)

Beau stresses the importance of learning from history to avoid repeating past mistakes, pointing out how the mishandling of a public health issue in 2006 mirrors current events.

</summary>

"The one thing we learn from history is that we don't learn from history."
"We just refused to learn from it."
"We don't want real American history taught."
"It's worth reading. It's definitely worth taking a look at."
"They'll never catch on. Commoners."

### AI summary (High error rate! Edit errors on video page)

Emphasizes the importance of learning from past events to prevent repeat occurrences.
Points out a common theme in society where history repeats itself due to a lack of learning.
Suggests that society often fails to publicize the lessons of history effectively.
Reads an excerpt detailing how officials mishandled a public health issue in the past.
Expresses frustration at officials downplaying the severity of the disease for personal gain.
Notes that the workshop revealing these mistakes occurred in 2006, yet similar patterns are repeating now.
Criticizes the tendency in the U.S. to avoid acknowledging and discussing the country's failings.
Questions whether knowledge of past mistakes could have better prepared people for the current situation.
Shares a link to the workshop's information for further reading.
Draws parallels between past mishandlings and the current situation with COVID-19.

Actions:

for critical thinkers, history enthusiasts.,
Read the workshop information linked by Beau (suggested).
Analyze past mistakes to better understand current events (implied).
</details>
<details>
<summary>
2021-08-28: Let's talk about the US response, the good, the bad, and the status quo.... (<a href="https://youtube.com/watch?v=NFXAFeuovsw">watch</a> || <a href="/videos/2021/08/28/Lets_talk_about_the_US_response_the_good_the_bad_and_the_status_quo">transcript &amp; editable summary</a>)

Beau explains recent developments and underscores the critical question of how they impact operations on the ground.

</summary>

"That's actually the only question. That's the only thing that matters."
"Your stupid question may be the smartest question that gets asked."
"When you're talking about the lives that are in harm's way right now, it doesn't mean anything."

### AI summary (High error rate! Edit errors on video page)

Explains recent developments at the airport and the US drone response to an ISK planner.
Emphasizes the importance of questioning whether the actions will impact operations on the ground positively, negatively, or not at all.
Points out that the chain of events initiated in 2019 will continue regardless of recent news.
Mentions that politically, the news may matter, but operationally on the ground, it won't have a significant effect.
Notes that the response is unlikely to deter further incidents or change the ongoing evacuation efforts.
Stresses that focusing on how the news impacts current events is the most critical question.

Actions:

for policy analysts,
Stay informed about the developments in the region and their potential impact (suggested).
Support organizations aiding in evacuation efforts (implied).
</details>
<details>
<summary>
2021-08-28: Let's talk about a story for people in Louisiana.... (<a href="https://youtube.com/watch?v=Ce3uKXcd_t8">watch</a> || <a href="/videos/2021/08/28/Lets_talk_about_a_story_for_people_in_Louisiana">transcript &amp; editable summary</a>)

Beau urges Louisiana residents to take Hurricane Ida seriously, evacuate if necessary, and prepare for potential storm surges and damage.

</summary>

"Get your meds, get all of your emergency supplies, get your little battery for your phone, get everything that you need, your pets, your pet food, and get inland."
"Make sure that those Cajun guys who are going to come help are going to be doing rescue operations and not recovery operations."
"If you're in Louisiana, take this one seriously, because it is doing all of that same creepy stuff with the changes."

### AI summary (High error rate! Edit errors on video page)

Tells a funny story about a friend who enjoys hurricane parties in Louisiana.
Friend's wife experiences her first hurricane, Hurricane Michael, causing unease.
Urges people in Louisiana to pay attention to Hurricane Ida, scheduled to hit as a Category 4 with potential storm surges.
Mentions the devastation caused by Hurricane Michael, with businesses still unrepaired to this day.
Advises those in low-lying areas to evacuate and prepare emergency supplies, including medications, pets, and phone batteries.
Emphasizes the importance of heeding evacuation warnings and not waiting until it's too late.
Encourages residents to ensure they are prepared for rescue operations rather than recovery operations.
Stresses the seriousness of Hurricane Ida and the potential damage it could cause based on where it hits.

Actions:

for louisiana residents,
Evacuate if in low-lying areas and gather emergency supplies (suggested)
Ensure you have medications, emergency supplies, pet essentials, and evacuation plans ready (suggested)
Prepare for rescue operations by being inland and out of harm's way (suggested)
</details>
<details>
<summary>
2021-08-27: Let's talk about saving 50,000 in 90 days.... (<a href="https://youtube.com/watch?v=G5OHvAEvSxA">watch</a> || <a href="/videos/2021/08/27/Lets_talk_about_saving_50_000_in_90_days">transcript &amp; editable summary</a>)

Beau stresses the importance of wearing masks to reduce the projected loss of 100,000 lives over 90 days, making it a patriotic duty and an intelligence test with life-or-death consequences.

</summary>

"It's worth noting that here in Florida, a judge did just throw out DeSantis' little edict saying that schools could not mandate masks."
"If you love your country, if you love your neighbors, if you want to be a patriot and do your part in the greatest battle this country has seen in a very long time, all it takes is wearing a mask."
"This isn't just a public health issue anymore. It's an intelligence test with life-or-death consequences."

### AI summary (High error rate! Edit errors on video page)

Providing an overview of projections for the next 90 days regarding public health.
Mentioning the potential loss of 100,000 lives over the next 90 days without proper measures.
Pointing out that wearing masks in public could reduce this number by half.
Noting the quantified cost of not wearing masks in public: about 50,000 lives over three months.
Sharing news about a judge in Florida overturning the ban on schools mandating masks.
Emphasizing the individual choice to save lives by wearing a mask or ignoring the consequences.
Stressing the reality of the situation and the tangible impact on lives if mask-wearing is not practiced.
Encouraging engagement in all available mitigation efforts to reduce the loss of lives.
Framing mask-wearing as a patriotic duty and an act of love for country and neighbors.
Describing the current situation as an intelligence test with life-or-death implications.
Urging people to change their behavior to prevent unnecessary loss of life.
Summarizing that wearing a mask daily can significantly reduce the number of lives lost.
Concluding with a call to action to wear masks for the greater good and as part of a critical battle.

Actions:

for general public, mask-wearing advocates,
Wear a mask daily to reduce the projected loss of lives. (exemplified)
</details>
<details>
<summary>
2021-08-27: Let's talk about Kennedy, Malcom Nance, and criticism.... (<a href="https://youtube.com/watch?v=rurDFIgHiKI">watch</a> || <a href="/videos/2021/08/27/Lets_talk_about_Kennedy_Malcom_Nance_and_criticism">transcript &amp; editable summary</a>)

Beau questions why he and Malcolm Nance aren't criticizing the administration's actions in Afghanistan, opting to withhold criticisms until people are safe, prioritizing saving lives.

</summary>

"Why are you carrying water for the administration?"
"I don't believe the people making that criticism today are doing it in good faith."
"I will hold my criticisms until the end because I don't want to get people killed."
"It's not an insult. It's actually a compliment."
"I strongly suggest you all watch him."

### AI summary (High error rate! Edit errors on video page)

Beau receives a message pointing out that he and Malcolm Nance were early in predicting the fall of Afghanistan and questioning official estimates.
Beau mentions Noam Chomsky's accurate estimate, indicating that he is not the only one who foresaw the situation.
Despite similar views on most topics, Beau notes a difference with Nance where Nance reportedly referred to the Afghan people as ignorant hill people or goat herders.
Beau questions why he and Nance are not criticizing the administration for what he sees as an abysmal failure in the withdrawal from Afghanistan.
Beau brings up Kennedy's philosophy of questioning whether news is in the interest of national security, but he alters the premise to focus on saving lives.
Beau believes that the current situation in Afghanistan, while chaotic and tragic, is not as bad as it could have been or potentially could become.
He refrains from discussing a critical oversight that could escalate the situation, choosing to wait until people are out of harm's way before making such criticisms.
Beau criticizes the major outlets for focusing on the Biden administration providing a list for safe passage, which he sees as a common practice throughout history.
He points out the risks involved in the hasty withdrawal and regime change in Afghanistan but challenges critics to provide workable alternatives.
Beau expresses his decision to withhold criticisms to avoid endangering lives and suggests that others should do the same during this dynamic situation.

Actions:

for community members, analysts.,
Hold valid criticisms until after people are out of harm's way (implied).
Watch and stay informed about various commentators' perspectives for a well-rounded understanding of situations (implied).
</details>
<details>
<summary>
2021-08-26: Let's talk about the future of Afghanistan and this morning's events.... (<a href="https://youtube.com/watch?v=DrAkRnWuhng">watch</a> || <a href="/videos/2021/08/26/Lets_talk_about_the_future_of_Afghanistan_and_this_morning_s_events">transcript &amp; editable summary</a>)

Beau corrects misconceptions about Afghanistan, warns against maintaining a light footprint, and advocates for a swift US withdrawal due to factionalization and potential conflict.

</summary>

"This is proof the US needs to maintain a light footprint."
"We don't need to stay. We don't need a light footprint."
"It's more of a reason to leave."

### AI summary (High error rate! Edit errors on video page)

Correcting misconceptions about recent developments in Afghanistan.
An opposition group, ISK, not the Taliban, made a move on the airport.
ISK and the Taliban are not allies; they are more likely to shoot at each other than work together.
ISK's move was a direct challenge to the de facto government, not just the West.
ISK's goal is to show they are still relevant before the West completely leaves.
ISK is not incredibly capable; estimates suggest they have around 1000 members globally.
The future options for Afghanistan include power-sharing or civil war among factions.
Maintaining a "light footprint" in Afghanistan is not a viable solution according to Beau.
Beau warns against the US dropping a foreign operating base in Afghanistan as it could incite conflict with factions.
Surprisingly, the withdrawal did not severely degrade US and Western intelligence gathering capabilities in Afghanistan.
The factionalization in Afghanistan may lead to conflict unless the de facto government acts quickly to eliminate opposition.
Beau believes conflict is likely, and the US needs to expedite its withdrawal from Afghanistan.

Actions:

for policymakers, activists, concerned citizens,
Advocate for swift US withdrawal from Afghanistan (implied)
Stay informed about the situation in Afghanistan and support efforts towards peacebuilding (implied)
</details>
<details>
<summary>
2021-08-26: Let's talk about hindsight and how it could've been different.... (<a href="https://youtube.com/watch?v=LXqhfSn8aTE">watch</a> || <a href="/videos/2021/08/26/Lets_talk_about_hindsight_and_how_it_could_ve_been_different">transcript &amp; editable summary</a>)

Hindsight is 20-20, but saving lives amid crisis is what truly matters now in the Afghan situation.

</summary>

"Hindsight is 20-20. That's not an encouragement to provide it."
"The preservation of human life. Period. Full stop."
"There is no way that this was going to turn into a US victory."
"It's distracting from the actual important thing, which is saving lives."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Hindsight is 20-20, and it's easy to make decisions after knowing the outcome.
Most people claiming they could have done things differently are merely speculating.
It's vital to base decisions on estimates and information available at the time, not just what happened.
Holding onto Bagram, an air base, is debated as a potential game-changer.
Moving operations to Bagram is seen as more defensible, but the logistics pose significant challenges.
The focus should be on saving lives and facilitating the evacuation, not on hindsight or hypothetical decisions.
The preservation of human life is the top priority in the current situation in Afghanistan.
The withdrawal from Afghanistan was negotiated long ago and was never going to be a clear victory.
It's critical to prioritize saving lives rather than getting caught up in hindsight debates.
The focus should be on the immediate task of evacuating people safely.

Actions:

for policy analysts, decision-makers, humanitarian organizations.,
Coordinate with humanitarian organizations to facilitate the safe evacuation of individuals from conflict zones (implied).
Prioritize the preservation of human life by supporting efforts to save lives in crisis situations (implied).
</details>
<details>
<summary>
2021-08-25: Let's talk about what a news outlet should look like if it wants to.... (<a href="https://youtube.com/watch?v=QdMRv1oF5dI">watch</a> || <a href="/videos/2021/08/25/Lets_talk_about_what_a_news_outlet_should_look_like_if_it_wants_to">transcript &amp; editable summary</a>)

Beau explains media manipulation tactics, urging division and fear within dominant groups to weaken systems of government, paralleling Fox News' strategies.

</summary>

"They will be more susceptible to fear-based reasoning."
"Conflate patriotism with obedience to a faction within the nation, and drown out all other voices."
"If somebody was to sit down and try to determine what messaging could be carried by a news outlet to bring the United States to the brink, that news outlet, it would look exactly like Fox News."

### AI summary (High error rate! Edit errors on video page)

Exploring a reverse Q&A format today, starting with the answer before the question.
Expressing insightful views on media manipulation, focusing on division and fear tactics.
Advocating for sowing discontent within dominant groups to incite violence and weaken systems of government.
Suggesting strategies to undermine national power symbols and create paramilitary groups.
Linking patriotism with factional obedience to drown out dissenting voices and foster fear-based thinking.
Sharing a hypothetical scenario about a new country facing threats from a larger, powerful nation.
Describing a plot involving a think tank trying unconventional methods to destabilize the powerful nation.
Posing a question about infiltrating a news outlet to weaken a powerful nation through messaging.
Drawing parallels between the hypothetical messaging and the practices of certain news outlets.
Concluding with thoughts on the impact of specific messaging on a country's stability.

Actions:

for media consumers,
Analyze news sources critically to identify potential divisive tactics and fear-mongering (suggested).
Support media outlets that prioritize balanced reporting and diverse voices (implied).
</details>
<details>
<summary>
2021-08-25: Let's talk about Biden v. Texas.... (<a href="https://youtube.com/watch?v=FlURzCuaDpU">watch</a> || <a href="/videos/2021/08/25/Lets_talk_about_Biden_v_Texas">transcript &amp; editable summary</a>)

The Supreme Court's ruling on the Remain in Mexico policy limits Biden's leverage in foreign policy and sets a concerning precedent for executive authority.

</summary>

"The Supreme Court just walked up behind Biden holding a mirror and showed everybody his cards."
"Foreign policy is the purview of the executive branch, and it should probably stay that way."
"This is a case that went from pretty boring to incredibly interesting very, very quickly."

### AI summary (High error rate! Edit errors on video page)

The Supreme Court rejected Biden's request to set aside a lower court ruling on the Remain in Mexico policy, putting Biden in a tough spot.
This ruling interferes with foreign policy, a domain traditionally left to the executive branch.
Biden now lacks leverage in negotiating with Mexico over the policy, potentially allowing Mexico to ask for significant concessions.
The Supreme Court's decision limits Biden's ability to negotiate effectively, similar to Trump's challenges in Afghanistan due to being too transparent about intentions.
Uncertainty looms over the future implications if Mexico refuses to cooperate, signaling uncharted territory and potential complications arising from judicial intervention in foreign affairs.

Actions:

for policy analysts, activists,
Stay informed on the developments regarding the Remain in Mexico policy and its implications (implied)
Advocate for transparency and accountability in foreign policy decisions (implied)
</details>
<details>
<summary>
2021-08-24: Let's talk about the feed store and public health.... (<a href="https://youtube.com/watch?v=dByVMsE08FA">watch</a> || <a href="/videos/2021/08/24/Lets_talk_about_the_feed_store_and_public_health">transcript &amp; editable summary</a>)

Beau warns against the dangers of seeking out ivermectin as a COVID-19 treatment, stressing the importance of accurate information and available alternatives to protect lives.

</summary>

"Perhaps we should stop talking about what it does, it's an anti-parasitic, and start talking about what it is, a neurotoxin."
"Is your pride worth your life?"
"Don't do this."

### AI summary (High error rate! Edit errors on video page)

Small towns are experiencing people coming from larger cities to buy a certain anti-parasitic, ivermectin, commonly used in livestock, horses, and sheep.
The feed store owner in small towns quizzes buyers about the animals the product is for and won't sell if they don't know basic answers, as they prioritize local customers over outsiders.
Misinformation on the benefits of ivermectin for COVID-19 has led people to seek it out at feed stores based on Facebook memes and search results.
Instead of focusing on what ivermectin does as an anti-parasitic, Beau suggests reframing the narrative to its true nature as a neurotoxin to dissuade potential consumers.
Beau warns against the dangerous dosing differences between animals like dogs and humans when it comes to ivermectin, with severe consequences for improper usage.
Merck, the pharmaceutical company that manufactures ivermectin, has publicly stated that there is no scientific basis or evidence to support its use as a COVID-19 treatment.
Despite claims that ivermectin works and is being suppressed by Big Pharma, Merck's position contradicts this narrative as they express doubts about its efficacy and safety.
Beau questions the logic of risking one's life by seeking out ivermectin when vaccines, readily available at numerous locations, provide proven protection against COVID-19.
He stresses the importance of not falling for misinformation and desperation when safer alternatives like vaccines exist, urging people not to put their lives at risk by pursuing unproven treatments.
Beau concludes by reminding viewers of the consequences of early dismissals of COVID-19 severity and mitigation efforts, warning of potential lack of hospital beds for those who may fall ill.

Actions:

for health-conscious individuals,
Refrain from seeking out and using ivermectin as a COVID-19 treatment (implied)
Prioritize getting vaccinated against COVID-19 for protection (implied)
Educate others on the risks and lack of scientific evidence supporting ivermectin as a COVID-19 treatment (implied)
</details>
<details>
<summary>
2021-08-24: Let's talk about civilians, equipment, troops in that order.... (<a href="https://youtube.com/watch?v=bVQKK9c4tUk">watch</a> || <a href="/videos/2021/08/24/Lets_talk_about_civilians_equipment_troops_in_that_order">transcript &amp; editable summary</a>)

Beau challenges the narrative of evacuating civilians first in Afghanistan, explaining the complex demographics and reasons behind the sequence of evacuations, while underscoring the inevitability of the outcome and the need to learn from such interventions.

</summary>

"Put the penguin down. It's not really helping anything."
"There isn't a lot that could have been done to avoid what you're seeing."
"Understand these interventions end poorly."
"From the moment the deal was made, it was going to be this way."
"Even Trump's timeline didn't change the dynamics."

### AI summary (High error rate! Edit errors on video page)

Addressing the belief that civilians should have been evacuated first before equipment and troops, Beau argues that it doesn't make sense beyond a surface level.
Explains the demographics of civilians in Afghanistan, including government employees, civilian contractors, and NGO workers, who had reasons to stay even after being advised to leave.
Points out that the U.S. government lacks the ability to force American nationals abroad to return home, which is why many civilians remained in Afghanistan.
Clarifies the misconception around U.S. equipment left in Afghanistan, distinguishing between U.S.-made and U.S.-owned equipment.
Emphasizes that most of the Department of Defense's equipment was removed, and what was left behind belonged to the Afghan national government for their fight against opposition.
Challenges the notion that President Trump's approach of getting civilians and equipment out first is false, as troops were withdrawn before civilians and equipment.
States that the outcome in Afghanistan was inevitable due to the deal made and the withdrawal of troops, regardless of the timeline or actions taken.
Acknowledges the collective guilt felt by Americans watching the situation unfold and suggests learning from such interventions for the future.

Actions:

for policy analysts, advocates,
Learn from interventions to prevent similar outcomes (suggested)
Understand the complex dynamics of conflicts and interventions (suggested)
</details>
<details>
<summary>
2021-08-23: Let's talk about the FDA approval and why you should get vaccinated now.... (<a href="https://youtube.com/watch?v=sFkwOqCl5Ok">watch</a> || <a href="/videos/2021/08/23/Lets_talk_about_the_FDA_approval_and_why_you_should_get_vaccinated_now">transcript &amp; editable summary</a>)

Full FDA approval for a vaccine may not sway the anti-vaccine crowd, making it vital to focus on hesitant individuals to increase vaccination rates and avoid government mandates.

</summary>

"We need to make sure that we can get enough people vaccinated so we don't need those mandates."
"Violence is bad. But it's also going to help reinforce the conspiratorial mindset of those in the anti-crowd."
"If we don't get into that 70% to 90% range on our own, there will be government mandates of the general population."
"The government will impose it. If they think it's necessary, they're going to do it."
"Please go get vaccinated."

### AI summary (High error rate! Edit errors on video page)

Full FDA approval for a vaccine has been granted, but it may not sway the anti-vaccine crowd much.
About 20% of people identify as anti-vax, posing a challenge to reaching the 70% to 90% vaccination goal.
Distinguishes between the anti-vax crowd and the hesitant individuals, suggesting a focus on the latter for better success.
Private mandates for vaccination are expected to increase, driven by liability concerns rather than social responsibility.
Government mandates may become necessary if vaccination rates don't reach the desired levels, potentially leading to resistance and reinforcing conspiracy mindsets.
Beau advocates for increasing vaccination rates through persuasion and private means to avoid government mandates and further entrenching anti-vaccine sentiments.

Actions:

for americans,
Reach out and explain any questions to hesitant individuals (implied)
Get vaccinated and encourage others to do the same (implied)
</details>
<details>
<summary>
2021-08-23: Let's talk about Trump's most presidential act.... (<a href="https://youtube.com/watch?v=rl6YqTUs7TQ">watch</a> || <a href="/videos/2021/08/23/Lets_talk_about_Trump_s_most_presidential_act">transcript &amp; editable summary</a>)

Former President Trump recommended getting vaccinated, faced boos from supporters, and sparked a shift in Republican Party tone towards anti-science beliefs.

</summary>

"I recommend you get the vaccine."
"He told a group of his supporters something that they didn't want to hear."
"Wisdom is knowing that the doctor was the monster."
"You have a political establishment that pandered and it breathed life into this conspiratorial anti-science nonsense."
"Think about what makes them cheer."

### AI summary (High error rate! Edit errors on video page)

Trump advocated for people to get vaccinated during a rally in Alabama, but his supporters booed him.
Despite the booing, Trump recommended getting vaccinated and mentioned that he himself is vaccinated.
Beau praises Trump for not pandering to his supporters and telling them the truth.
The shift in tone within the Republican Party is due to realizing the impact of spreading anti-science beliefs.
The Republican Party's base is losing segments due to their commitment to anti-science rhetoric, potentially affecting elections.
Beau compares knowledge to wisdom using the Frankenstein analogy to illustrate the political establishment's situation.
The establishment's promotion of conspiracy theories has backfired, leading to a lack of control and backlash from their own base.

Actions:

for political observers,
Challenge anti-science rhetoric within your community (implied)
Support leaders who prioritize truth over pandering (implied)
</details>
<details>
<summary>
2021-08-22: Let's talk about an image and a lesson on propaganda.... (<a href="https://youtube.com/watch?v=C6WTa1QmaF8">watch</a> || <a href="/videos/2021/08/22/Lets_talk_about_an_image_and_a_lesson_on_propaganda">transcript &amp; editable summary</a>)

Beau addresses the dangerous implications of circulating manipulated images for political gain, urging for a critical understanding of propaganda's impact on human lives.

</summary>

"If your political platform aligns so well with military opposition, maybe your domestic political platform needs a little bit of work."
"Time is running out at that airport. They're sending a message. Time is running out."
"There are lives that are hanging in the balance, tens of thousands of lives."

### AI summary (High error rate! Edit errors on video page)

Addressing the circulation of an image comparing Afghan forces to the Marines at Iwo Jima.
Criticizing the use of military propaganda for political gain by media and commentators.
Pointing out the subtlety and layers in propaganda images that are often missed.
Emphasizing the importance of understanding the true message behind propaganda.
Calling out those who prioritize political interests over human lives in conflict zones.
Urging people to fact-check and not fall for manipulated narratives.
Advocating for dismantling propaganda messaging by exposing its flaws.
Stating the opposition in Afghanistan is objectively bad from various perspectives.
Warning against unwittingly supporting opposition propaganda that could harm innocent lives.
Noting the urgency of the situation at the airport in Afghanistan and the need for swift action.

Actions:

for media consumers,
Fact-check images and narratives before sharing (suggested).
Expose and dismantle propaganda messaging by analyzing and educating others on its flaws (implied).
Raise awareness about the importance of critically evaluating information shared online (implied).
</details>
<details>
<summary>
2021-08-22: Let's talk about about politicians wanting to go back.... (<a href="https://youtube.com/watch?v=y32qLGswI5I">watch</a> || <a href="/videos/2021/08/22/Lets_talk_about_about_politicians_wanting_to_go_back">transcript &amp; editable summary</a>)

The national government's push to return militarily is ill-advised; focus must shift to evacuating people to preserve lives.

</summary>

"That's no longer an option. That window is closed."
"Getting people out. Getting the Americans out. Getting the allies out. Getting any refugee that wants to go out. That should be the goal."
"There is one priority right now and that is the preservation of human life."

### AI summary (High error rate! Edit errors on video page)

The national government's shift towards discussing going back has caught Beau off guard.
Some senators have shown support for maintaining a minimal military presence in the country.
The plan involves establishing a forward operating base with special operations personnel for mitigation operations.
Beau warns that dropping a base without effective control of the countryside will lead to opposition infiltration and eventual overrun.
The opposition's strategy of slowly seeding areas with their personnel before making a move is emphasized.
Beau predicts a scenario where special forces leave the base for an operation, and the opposition strikes when the base is vulnerable.
Beau draws a grim parallel to Dien Bien Phu, signaling the disastrous consequences of attempting to establish a token security force.
The focus should be on preserving human life by evacuating people, rather than military strategies or geopolitical concerns.
Beau urges readiness to oppose any attempts to re-enter the country militarily, as the priority should be getting people out safely.
Politicians may resort to various tactics to justify going back, but Beau stresses the importance of standing against it.

Actions:

for policymakers, activists, citizens,
Evacuate people, Americans, allies, and refugees who wish to leave (implied)
Oppose any attempts to re-enter the country militarily (exemplified)
</details>
<details>
<summary>
2021-08-21: Let's talk about a technique for reaching out to your parents.... (<a href="https://youtube.com/watch?v=FCs_GuEOocc">watch</a> || <a href="/videos/2021/08/21/Lets_talk_about_a_technique_for_reaching_out_to_your_parents">transcript &amp; editable summary</a>)

Beau introduces a technique to reach out to parents by asking strategic questions, aiming to guide them towards new perspectives non-confrontationally.

</summary>

"Rather than telling them they're wrong, ask them questions that they may not necessarily know the answer to."
"It's about managing perceptions rather than trying to change reality."
"You can't just walk in there and say you're wrong. I mean, you can, but it's not going to be productive."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a technique to reach out to parents based on an old army training film.
The technique involves asking questions rather than telling parents they are wrong.
Beau shares a story from the training film about a second lieutenant convincing a first sergeant about the effectiveness of a device.
The key is to ask questions that lead parents to new information or perspectives.
The approach is non-confrontational and aims to avoid conflicts during family gatherings.
By using this tactic, Beau suggests that parents may see their children as having valid opinions.
It's about managing perceptions rather than trying to change reality.
Beau recommends being strategic in how you communicate with parents, especially when they hold different beliefs.
The goal is to guide parents towards considering alternative viewpoints without direct confrontation.
This technique has been effective for Beau in various situations over his life.

Actions:

for parents, children,
Ask questions to guide parents towards new information or perspectives (suggested)
Approach challenging topics with strategic questioning rather than direct confrontation (exemplified)
</details>
<details>
<summary>
2021-08-20: The roads to the public library.... (<a href="https://youtube.com/watch?v=2cvi7KHXqqI">watch</a> || <a href="/videos/2021/08/20/The_roads_to_the_public_library">transcript &amp; editable summary</a>)

Beau and Allison dive into the importance of libraries, addressing concerns about discarded books, advocating for privacy protection, and calling for better funding to support vital community resources.

</summary>

"Libraries are immensely important. In fact, they are more popular than they ever have been."
"Privacy is one of the core values of the library professions."
"We want our collections to be current. We want them to reflect our community's needs."
"Libraries need to be significantly better funded."
"Libraries are one of the only things in society that are 100% free and not means tested."

### AI summary (High error rate! Edit errors on video page)

Beau addresses concerns about books being found in dumpsters and invites Allison Macrina, a librarian, to explain the process of removing books from libraries.
Allison explains that libraries have to make decisions about older books that are no longer in use through a process called "weeding."
Libraries try to find new homes for discarded books through book sales, donations, or free distribution, but some books end up in dumpsters.
Allison clarifies that the removal of books is not censorship but a matter of space and keeping collections current to meet community needs.
She mentions the importance of libraries in providing free access to information, technology education, job skills training, and community programs.
Allison introduces the Library Freedom Project, a community that advocates for privacy protection in libraries and fights against surveillance.
She explains the historical connection between libraries and privacy advocacy, dating back to resisting anti-communist inquiries and post-9/11 privacy violations.
Allison describes the online crash courses offered by the Library Freedom Project to teach library workers about privacy protection strategies.
She addresses concerns about censorship in the publishing industry and the impact of media consolidation on the diversity of opinions available in libraries.
Allison advocates for better funding for libraries to continue providing valuable services to diverse communities.

Actions:

for library supporters, privacy advocates,
Support your local library by visiting, borrowing books, attending programs, and advocating for increased funding (exemplified)
Educate yourself on privacy rights and surveillance issues and support initiatives like the Library Freedom Project (exemplified)
</details>
<details>
<summary>
2021-08-20: Let's talk about your questions on over there.... (<a href="https://youtube.com/watch?v=KUMomGe4fjY">watch</a> || <a href="/videos/2021/08/20/Lets_talk_about_your_questions_on_over_there">transcript &amp; editable summary</a>)

Beau clarifies misconceptions about Afghanistan, equipment left behind, and the challenges faced during the withdrawal, urging for a more nuanced understanding of the situation.

</summary>

"A lot of times they don't. And in this case, you didn't really have any objective good guys. It just so happens that the objective bad guy won."
"It's everybody. Everybody who had a hand in this has some of the blame."
"This is what it looks like. You know for a long time a lot of the news that came out of there was very sanitized. What you're seeing now is reality. And it's always the reality."

### AI summary (High error rate! Edit errors on video page)

Explains the confusion caused by major media outlets and pundits inserting their own narratives into the situation in Afghanistan.
Clarifies the equipment left behind in Afghanistan, mentioning that the opposition, now the de facto government, operates differently from how the U.S. military does.
Addresses concerns about the use of equipment against civilians, explaining how it doesn't significantly change the dynamics of the situation.
Touches on the issue of biometrics and the potential challenges faced by those trying to leave the country.
Comments on the Vice President's role in potentially resisting the opposition.
Talks about the continued presence of the CIA and potential treatment of women by the opposition.
Mentions the evacuation process and the impact of base closures on it.
Considers the possibility of equipment left behind being compromised.
Examines the challenges faced during the withdrawal from Afghanistan and the preparedness of the Biden administration.
Concludes with thoughts on the Afghan population, refugees, and lessons to be learned from the situation.

Actions:

for global citizens concerned about afghanistan.,
Study and understand the events in politically bankrupt nations to develop sustainable solutions (implied).
Advocate for political processes over military intervention in such situations (implied).
</details>
<details>
<summary>
2021-08-19: Let's talk about getting out from over there and a teachable moment.... (<a href="https://youtube.com/watch?v=6Llm29VW-0s">watch</a> || <a href="/videos/2021/08/19/Lets_talk_about_getting_out_from_over_there_and_a_teachable_moment">transcript &amp; editable summary</a>)

Beau explains the importance of heeding travel advisories, the necessity of negotiating with opposition post-war, and the need to remove identifiers from photos of Afghan nationals for veterans.

</summary>

"Once the US effort is done, there will still be stragglers. There'll still be people there, and nothing will let you know how bad off you are as the appearance of those people who are coming to get you."
"Acting as if you're surprised [about negotiating with the opposition after war], that doesn't lend a whole lot to your credibility."
"The surest way to avoid something like this happening again is to not engage in these types of military interventions."

### AI summary (High error rate! Edit errors on video page)

Explains the levels of U.S. State Department travel advisories: level one, everything's cool; level two, use caution; level three, reconsider your trip; level four, get out; and an unofficial fifth level, get out now no matter what.
Details the embassy's escalating warnings to leave Afghanistan, culminating in a blunt "get out" notice on August 7th.
Mentions the significance of embassies discussing repatriation loans as a signal to leave immediately.
Notes that around 10,000 Americans were still in Afghanistan, with the official number likely being low due to some not checking in.
Emphasizes the importance of paying attention to travel advisories for those traveling overseas or knowing someone who does.
Addresses the cooperation with the opposition in Afghanistan to evacuate people and criticizes those who are against negotiating with the opposition.
Argues that negotiating with the opposition is a normal part of post-war processes and that portraying surprise at this is uninformed.
Asserts that the opposition in Afghanistan has nothing to gain from harming Americans and that such ideas stem from wartime propaganda.
Stresses that negotiating, cooperating, and exchanging prisoners after wars are standard procedures.
Encourages removing identifying information from photos of Afghan nationals for veterans who worked with them.

Actions:

for travelers and advocates.,
Blur identifying features in photos of Afghan nationals (suggested).
Pay attention to State Department travel advisories when traveling overseas (suggested).
</details>
<details>
<summary>
2021-08-19: Let's talk about equipment left behind.... (<a href="https://youtube.com/watch?v=Fl3NpETCkIE">watch</a> || <a href="/videos/2021/08/19/Lets_talk_about_equipment_left_behind">transcript &amp; editable summary</a>)

Beau explains the insignificance of U.S. equipment falling into opposition hands post-war, debunking fear-mongering around the issue.

</summary>

"The concern over the equipment being left behind doesn't hold much weight unless there are plans to go back."
"The fear-mongering around the equipment left in Afghanistan is an attempt to provoke outrage over something that is not a major issue."

### AI summary (High error rate! Edit errors on video page)

U.S. equipment falling into opposition hands is being fearfully sensationalized despite being a common occurrence at the end of wars.
The concern over the equipment being left behind doesn't hold much weight unless there are plans to go back.
The captured equipment, like night vision gear and Humvees, is not significant in the grand scheme of things.
Some of the equipment may contain communications technology that foreign powers could reverse engineer, but this has likely already happened.
The aircraft left behind, like Blackhawks and A-29s, are not high-tech versions and pose no real threat.
The fear-mongering around the equipment left in Afghanistan is an attempt to provoke outrage over something that is not a major issue.

Actions:

for policy analysts, activists,
Inform others about the common occurrence of equipment falling into opposition hands post-war (implied)
Combat fear-mongering by sharing Beau's perspective on the issue (implied)
</details>
<details>
<summary>
2021-08-18: Let's talk about the Saigon analogy.... (<a href="https://youtube.com/watch?v=AI7FTx2_lB0">watch</a> || <a href="/videos/2021/08/18/Lets_talk_about_the_Saigon_analogy">transcript &amp; editable summary</a>)

Beau explains the flawed Saigon analogy, criticizes America's approach to foreign policy, and calls for a shift towards helping countries stand independently.

</summary>

"We lost the peace."
"The attempt to tie this to a military defeat, to make it look like a failed war rather than a failed peace, is an attempt to avoid these questions."
"Eventually, the United States is going to have to realize that that form of empire building, those days are done."
"Empire is bad."
"If the United States wants a place as a world leader in the future, it's not going to be able to rely on the military."

### AI summary (High error rate! Edit errors on video page)

Explains the Saigon analogy and why it's flawed.
Points out the analogy's emotional manipulation rather than factual accuracy.
Contrasts the military end of Vietnam with Afghanistan's political end.
Argues that Afghanistan's loss was a political defeat, not a military one.
Criticizes the failure to win the peace in Afghanistan.
Analyzes America's approach to foreign policy and empire-building.
Suggests that American foreign policy needs to change to prioritize aiding countries to stand on their own.
Addresses the limitations of American military dominance in shaping global influence.

Actions:

for policy makers, activists, citizens,
Challenge and advocate for a shift in American foreign policy towards helping countries stand on their own (implied).
Support organizations working towards building sustainable independence in countries affected by U.S. interventions (implied).
</details>
<details>
<summary>
2021-08-18: Let's talk about balance and advice for the unvaccinated.... (<a href="https://youtube.com/watch?v=CcnCYdZw72k">watch</a> || <a href="/videos/2021/08/18/Lets_talk_about_balance_and_advice_for_the_unvaccinated">transcript &amp; editable summary</a>)

Beau addresses moral responsibilities, advises on emergency preparedness documents, and stresses the importance of comprehensive planning and communication for unforeseen circumstances.

</summary>

"You're going to fill out this form and you're going to discuss this plan with your children. Don't make us do it."
"Do not put your dogs on this. This is for human children."
"You are asking somebody to take over your financial responsibilities in the event of an emergency. You want to make it as easy as possible for the person you are dumping this on."

### AI summary (High error rate! Edit errors on video page)

Addressing moral and ethical responsibilities by providing a wide range of viewpoints.
Acknowledging being informed of neglecting duties and aiming to remedy that.
Being advised by someone to advise anti-vax people despite being pro-vax.
Explaining the importance of filling out certain documents for emergency preparedness.
Providing instructions on filling out an Emergency Child Care Plan for children.
Emphasizing not to include dogs in the Child Care Plan, as it is for human children.
Mentioning the importance of filling out a will and power of attorney document.
Cautioning against designating an unvaccinated individual as a power of attorney.
Recommending discussing Do Not Resuscitate/Do Not Intubate (DNR-DNI) decisions with family.
Instructing to designate a location for storing vital documents and records.
Listing documents to store, including insurance policies, medical records, tax records, and more.
Advising to include copies of last month's bills for someone taking over financial responsibilities.
Suggesting deleting internet history as an additional precaution.

Actions:

for parents, caretakers, and individuals needing guidance on emergency preparedness.,
Fill out an Emergency Child Care Plan with your children (suggested).
Fill out a will, power of attorney, and DNR-DNI documents (suggested).
Designate a secure location for storing vital documents and records (suggested).
Include copies of last month's bills in the designated location for financial continuity (suggested).
Delete internet history as a precaution (suggested).
</details>
<details>
<summary>
2021-08-17: Let's talk about what my life would be like if I was single.... (<a href="https://youtube.com/watch?v=oRx1TGa5UC8">watch</a> || <a href="/videos/2021/08/17/Lets_talk_about_what_my_life_would_be_like_if_I_was_single">transcript &amp; editable summary</a>)

Beau introduces the unique life of Panta Petrovic, a man living in a cave for twenty years who sets an example by getting vaccinated and urging others to do the same.

</summary>

"Every citizen should get vaccinated."
"A better example, better leadership than many governors in the United States."
"Twenty years he's lived up there like that."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of discussing what his life might be like if he were single.
Mentions being asked a question recently about his life if he were single.
Talks about Panta Petrovic, a man living in southern Serbia in a cave for twenty years.
Describes Panta's life in the cave, living off the land with basic furnishings and some animals.
Notes Panta's occasional trips to town for dumpster diving.
Compares Panta's presence near the town to that of Bigfoot.
Speculates on the positive introspection that twenty years of solitude might bring.
Shares Panta's recent emergence from the cave and his response to the public health situation.
Commends Panta for getting vaccinated and urging others to do the same, setting an example for leadership.
Echoes Panta's message of encouraging every citizen to get vaccinated.

Actions:

for community members,
Get vaccinated (exemplified)
Encourage others to get vaccinated (exemplified)
</details>
<details>
<summary>
2021-08-17: Let's talk about numbers, mistakes, and getting ready.... (<a href="https://youtube.com/watch?v=GRqtKm4A4mk">watch</a> || <a href="/videos/2021/08/17/Lets_talk_about_numbers_mistakes_and_getting_ready">transcript &amp; editable summary</a>)

Be prepared for potential lockdowns and supply chain disruptions by getting vaccinated and stocking up on essentials, while seeking advice from a trusted local doctor amidst misinformation.

</summary>

"It's a mistake not to be vaccinated, but one that can be corrected."
"Call your doctor, your doctor, not a YouTube doctor, not a doctor on Twitter or Facebook."
"Your local doc, you probably trust them."

### AI summary (High error rate! Edit errors on video page)

The Delta variant shows no signs of peaking, with a possibility of 200,000 cases a day.
Nearly 2,000 children were in hospitals due to the virus, setting a record.
It's a mistake not to be vaccinated, but one that can be corrected.
Unvaccinated individuals are described as "sitting ducks" by the director of NIH.
Numbers remain high for those who haven't received their shots.
Preparing for a potential scenario where lockdowns may occur again is advised.
Stocking up on essentials in case of restrictions and supply chain disruptions is suggested.
Politicians denying the situation may only act when it becomes unbearable.
A rush on stores and disruptions in the supply chain are possible with sudden lockdowns.
Contacting a trusted local doctor for vaccination advice is recommended amidst misinformation.

Actions:

for community members,
Contact your local doctor for vaccination advice (suggested)
Stock up on essentials in case of potential lockdowns (implied)
</details>
<details>
<summary>
2021-08-16: Let's talk about who's to blame for all this.... (<a href="https://youtube.com/watch?v=XVotMlYOGHA">watch</a> || <a href="/videos/2021/08/16/Lets_talk_about_who_s_to_blame_for_all_this">transcript &amp; editable summary</a>)

Exploring the blame game among Bush, Obama, Trump, and Biden, Beau reminds us that true accountability lies within ourselves to prevent future crises.

</summary>

"Who's to blame? We are."
"Looking for somebody to blame so we can wash our hands of it."
"The best thing that you can do for our veterans is to make sure that we stop creating combat veterans."
"That's not very conducive to stopping it."
"How to stop it from happening again, that's what we should be talking about."

### AI summary (High error rate! Edit errors on video page)

Exploring who is to blame for the current situation dominating headlines.
Analyzing the roles of Bush, Obama, Trump, and Biden in the series of events.
Not a Republican vs. Democrat issue, but rather a series of mistakes by all presidents involved.
Personally holds President Obama to a higher standard due to intelligence.
Criticizes President Trump's actions and predicts current events in foreign policy.
Acknowledges that blaming one president alone will not prevent similar situations in the future.
Emphasizes the role of public accountability in decision-making.
Warns against focusing on assigning blame instead of preventing similar failures.
Calls for preparing to mitigate the effects and help those affected by the current situation.
Urges for a focus on preventing future similar events rather than finding a scapegoat.

Actions:

for policy makers, activists, citizens,
Prepare to help those affected by the current crisis (suggested)
Focus on preventing similar future events (implied)
Advocate for accountability and transparency in decision-making (implied)
</details>
<details>
<summary>
2021-08-15: Let's talk about some questions about leaving over there.... (<a href="https://youtube.com/watch?v=6mYE6xQPoFw">watch</a> || <a href="/videos/2021/08/15/Lets_talk_about_some_questions_about_leaving_over_there">transcript &amp; editable summary</a>)

Beau explains the differences in withdrawal, mission completion, and military limitations in Afghanistan, stressing clear goals and potential solutions beyond US involvement.

</summary>

"Make sure that the goal is very well defined and that the military sticks to that goal."
"The US military is not actually designed for nation building."
"They're outmatched."
"The only thing that could make things better for the people in Afghanistan is for a foreign nation, not the United States."
"You want to get further down the line? Maybe some of the US military budget could be shifted to infrastructure development."

### AI summary (High error rate! Edit errors on video page)

Explains the differences between the withdrawal in Afghanistan and Vietnam, mentioning that the chaos on the ground may be the only similarity.
Talks about completing the mission in Afghanistan and how mission creep led to the extended stay of the US military.
Emphasizes the importance of defining clear goals and ensuring the military sticks to them to avoid mission creep.
Points out that the US military is not designed for nation-building and suggests the need for separate departments for fighting and building.
Mentions the failure of using the military for functions other than war, leading to the situation in Afghanistan.
Addresses why the Afghan national government couldn't hold its own, attributing it to lack of experience and being outmatched by the opposition.
Expresses doubt about fixing the situation in Afghanistan and suggests that foreign nations, other than the US, could provide token security forces.
Advises on ensuring clear and publicly known goals for any proposed intervention in the future.

Actions:

for policy analysts, activists.,
Advocate for clear and publicly known goals in any proposed intervention (implied).
Push for a shift of some of the US military budget towards infrastructure development in conflict zones (implied).
</details>
<details>
<summary>
2021-08-14: Let's talk about the census results causing concern for Tucker Carlson.... (<a href="https://youtube.com/watch?v=g7gbHarHBDw">watch</a> || <a href="/videos/2021/08/14/Lets_talk_about_the_census_results_causing_concern_for_Tucker_Carlson">transcript &amp; editable summary</a>)

Beau addresses concerns about census data, systemic racial issues, and the importance of working towards equality over fear of becoming a minority.

</summary>

"Skin tone shouldn't be that important, right?"
"They're fighting for equality. They're not fighting for vengeance."
"Now you're worried about that system being turned on you."

### AI summary (High error rate! Edit errors on video page)

Addressing concerns about census results and information causing worry among some people.
Sharing a message from a conservative viewer questioning lack of concern about decreasing white population.
Mentioning watching conservative media that downplay the challenges faced by minorities in the country.
Pointing out systemic issues in the U.S. that disadvantage certain racial groups.
Encouraging working on correcting these problems rather than being afraid of becoming a minority.
Expressing lack of personal concern about demographic shifts and focusing on working towards equality.
Emphasizing the importance of fighting for equality over seeking vengeance.
Stating that skin tone shouldn't hold such significance in society.
Arguing against adjusting life or policies based on demographic changes.
Noting concerns arise from realizing how the current system disadvantages minorities.

Actions:

for activists, advocates, community members,
Coordinate with organizations working towards equality (implied)
Work on correcting systemic issues related to race (implied)
</details>
<details>
<summary>
2021-08-14: Let's talk about 3000 headed back over there.... (<a href="https://youtube.com/watch?v=DPMteEIm_g4">watch</a> || <a href="/videos/2021/08/14/Lets_talk_about_3000_headed_back_over_there">transcript &amp; editable summary</a>)

Beau provides insights on the return of U.S. troops to Afghanistan, evacuating people with a slim chance of failure, and the likelihood of the opposition regaining control swiftly.

</summary>

"It's possible, but I don't see it as likely."
"The opposition there really isn't a bunch of ignorant, backward hill people. They're pretty smart."
"The odds are the opposition will control the country in very short order."
"The U.S. may decide to bomb until the rubble bounces."
"They're winning right now. They have no reason, there's no strategic value, in drawing the West back in."

### AI summary (High error rate! Edit errors on video page)

The Biden administration is sending 3,000 U.S. troops back into Afghanistan to evacuate people.
The troops' main objective is to secure the airport, embassy housing, and routes to evacuate Americans and allies.
The operation could take a day or up to a week.
There is a chance of things going wrong, but it's not a significant risk.
The opposition in Afghanistan is not to be underestimated; they are strategic and intelligent.
The U.S. security force in Afghanistan acted as a deterrent rather than an impenetrable defense.
The likelihood is that the opposition will regain control of the country once the U.S. forces leave.
The U.S. may use air support to aid the national government from afar after evacuation.
Beau doubts that Biden will resort to extensive military action in Afghanistan.
The U.S. air support might slow down the opposition but is unlikely to alter the outcome significantly.

Actions:

for policymakers, activists, citizens,
Support organizations aiding Afghan refugees (suggested)
Advocate for peaceful resolutions in foreign policy (implied)
</details>
<details>
<summary>
2021-08-13: Let's talk about me giving dating advice.... (<a href="https://youtube.com/watch?v=m0XX6OxfTNs">watch</a> || <a href="/videos/2021/08/13/Lets_talk_about_me_giving_dating_advice">transcript &amp; editable summary</a>)

Beau advises against seeking approval from others in matters of love, stressing that it's solely about the individuals involved, not anyone else.

</summary>

"Never seek approval from anybody when it comes to who you love. Nobody's opinion matters. Nobody's."
"It's just you and the other person."
"Fall in love. Don't fall in line."

### AI summary (High error rate! Edit errors on video page)

Providing dating advice on whether to date a trans woman.
Advising against seeking approval from others in matters of love.
Encouraging the listener to make their own decisions in relationships.
Hinting at the influence of the listener's friends on their question.
Mentioning the importance of individual choice in love.
Drawing parallels with the movie "A Bronx Tale" to illustrate dynamics at play.
Urging the listener to prioritize their feelings and connection with their partner.
Sharing empathy for young men facing societal pressures.
Referencing a "door lock test" as a measure of true love.
Concluding with a message to prioritize love over conformity.

Actions:

for young adults seeking dating advice.,
Fall in love based on genuine connection and not societal expectations (implied).
</details>
<details>
<summary>
2021-08-13: Let's talk about Trump, Crystal Lake, and coming back.... (<a href="https://youtube.com/watch?v=XuMQHX7zzlA">watch</a> || <a href="/videos/2021/08/13/Lets_talk_about_Trump_Crystal_Lake_and_coming_back">transcript &amp; editable summary</a>)

Former President Trump could have debunked false claims, but his silence harmed followers, while Republican leaders prioritize political gain over followers' well-being.

</summary>

"Leaders who allowed false beliefs without correction show their lack of care for their followers."
"Those who promoted Trump's return are the same advising against masks and vaccines for political gain."

### AI summary (High error rate! Edit errors on video page)

Reporting live from Camp Crystal Lake on Friday the 13th, waiting for the return of a mythical figure to set things right.
Former President Trump could have debunked false claims but didn't, benefiting politically.
Those who believed in the false claims saw their family and social life suffer without concern from Trump.
People who promoted Trump's return are the same advising against masks and vaccines for political gain.
Republican leaders who remain silent are willing to sacrifice followers for political gain.
If leaders don't encourage measures to protect health, they don't care about their followers.
Leadership of the Republican party and the mythical figure walking out of the lake both end with people harmed.
Advice to wash hands, wear masks, get vaccinated, and protect oneself despite misleading leadership.
Leaders who allowed false beliefs without correction show their lack of care for their followers.
Encouragement to take note of the character of leaders who mislead or fail to correct misinformation.

Actions:

for followers of political leaders,
Stay at home as much as you can to protect yourself and your family (implied)
Wash your hands regularly to prevent the spread of illness (implied)
Wear a mask when in public spaces to protect yourself and others (implied)
Get vaccinated to safeguard your health and the health of your community (implied)
</details>
<details>
<summary>
2021-08-12: Let's talk about personal responsibility in today's world.... (<a href="https://youtube.com/watch?v=izFoszGfWRo">watch</a> || <a href="/videos/2021/08/12/Lets_talk_about_personal_responsibility_in_today_s_world">transcript &amp; editable summary</a>)

In a critical analysis of personal responsibility, Beau challenges the failure of small government conservatives and libertarians to advocate for responsible behavior during the pandemic, undermining the core principles they claim to uphold.

</summary>

"You don't need to make me wear a mask because I'm already doing it, because I'm exercising that personal responsibility."
"They're not looking for a new philosophy. They're just tired of seeing funeral processions."
"But as simple as that philosophy is, you have to act on it."
"No, you don't need the government to tell you to do that because the government is a child of the people, not the other way around."
"It makes it seem that you don't believe your ideology, in the best case."

### AI summary (High error rate! Edit errors on video page)

Examines the concept of personal responsibility deeply entrenched in American politics.
Personal responsibility is seen as a way to reduce government intervention and increase individual freedom.
Criticizes individuals who refuse to wear masks or get vaccinated under the guise of personal responsibility.
Points out the contradiction in claiming personal responsibility while not taking necessary public health measures.
Expresses disappointment in small government conservatives and libertarians for not advocating for responsible behavior during the pandemic.
Notes that individuals leaving the Republican Party may not find solace in libertarianism due to lack of distinguishable differences.
Critiques libertarians for failing to provide a viable alternative and for echoing Republican talking points.
Argues that the refusal to wear masks or get vaccinated undermines the philosophy of self-governance.
Suggests that libertarians could have gained more support during the pandemic by promoting responsible behavior.
Emphasizes the need to act on personal responsibility by wearing masks and getting vaccinated without waiting for government mandates.

Actions:

for politically engaged individuals,
Wear a mask and get vaccinated (implied)
Advocate for responsible behavior in the community (implied)
</details>
<details>
<summary>
2021-08-12: Let's talk about Biden's message to OPEC, foreign policy, and oil.... (<a href="https://youtube.com/watch?v=-AvORj6UiNc">watch</a> || <a href="/videos/2021/08/12/Lets_talk_about_Biden_s_message_to_OPEC_foreign_policy_and_oil">transcript &amp; editable summary</a>)

President Biden's call for increased oil production unveils the dilemma of US oil reserves and foreign dependency, posing risks to national security and superpower status, urging consideration for alternative energy solutions.

</summary>

"America first and all that."
"I'm not saying it's right. I'm saying it's the way it is."
"If you absolutely want to destroy the United States, go right ahead."
"It might be a little bit more of a motivating factor."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

President Biden asked OPEC to increase oil production, prompting backlash from people advocating for America to use its own oil reserves.
The United States has the 11th largest oil reserves globally, about 35 billion barrels, which is approximately 2% of the world's oil supply.
If the US were to solely rely on its own oil reserves and maintain current consumption levels, it could run out of oil before the end of Biden's second term.
Comparatively, Saudi Arabia, with the second-largest known oil reserves, has enough oil to last for over two centuries.
If the US depleted its own oil reserves in about five years, it would become dependent on foreign oil, contrary to the America first policy.
The US military, although not involved in pillaging, could politically influence countries to ensure a steady oil supply.
A reliance on only domestic oil could jeopardize the US's superpower status as OPEC could cut off the oil supply.
Maintaining a policy of using others' oil reserves first was enforced from 1975 to 2015, suggesting a strategic approach.
Beau implies that transitioning to alternative energy sources like electric cars could be a wise long-term decision considering the limited domestic oil reserves.
The concept of using foreign oil before exhausting domestic reserves is a fundamental but often overlooked aspect of US foreign policy.

Actions:

for policy analysts, energy activists,
Advocate for sustainable energy solutions in your community (implied).
Support policies that encourage the transition to alternative energy sources (implied).
</details>
<details>
<summary>
2021-08-11: Let's talk about hurricanes and vaccines.... (<a href="https://youtube.com/watch?v=pZn_7pc9Z-g">watch</a> || <a href="/videos/2021/08/11/Lets_talk_about_hurricanes_and_vaccines">transcript &amp; editable summary</a>)

President Biden's vaccination advice in hurricane-prone regions is a factual warning, not fear-mongering, stressing the critical role of vaccination in hurricane preparedness amidst strained medical infrastructure.

</summary>

"It's just reality."
"It isn't fear-mongering."
"Yeah, you need to get your shots."
"This is going to continue until we have enough people vaccinated."
"Add getting vaccinated to your hurricane preparedness plan."

### AI summary (High error rate! Edit errors on video page)

President Biden's statement about getting vaccinated in hurricane-prone areas has sparked controversy.
Biden's message is not fear-mongering but a statement of fact about the impact of hurricanes on medical infrastructure.
In a pre-pandemic video, Beau discussed the consequences of being unvaccinated during a natural disaster like Hurricane Michael.
Hurricane damage to hospitals reduces medical capacity, making vaccination even more critical in hurricane-prone states.
The strain on medical infrastructure in hurricane-prone areas is already significant.
Beau stresses the importance of vaccination as part of hurricane preparedness in states like Florida, Georgia, Alabama, and others.
The reality is that without vaccination, individuals may face dire consequences during a hurricane due to reduced medical capacity.
Beau underscores that the pandemic will persist until enough people are vaccinated, regardless of personal preferences.
He warns that being unvaccinated during a hurricane can lead to severe consequences and loss.
Vaccination is a vital component of preparedness for natural disasters in areas prone to hurricanes.

Actions:

for residents in hurricane-prone areas,
Get vaccinated as part of your hurricane preparedness plan (suggested)
Recognize the importance of vaccination in areas vulnerable to hurricanes (implied)
</details>
<details>
<summary>
2021-08-11: Let's talk about Rand Paul needing a remedial civics lesson.... (<a href="https://youtube.com/watch?v=ag_NJEc5cI4">watch</a> || <a href="/videos/2021/08/11/Lets_talk_about_Rand_Paul_needing_a_remedial_civics_lesson">transcript &amp; editable summary</a>)

Beau explains the First Amendment to Senator Rand Paul and criticizes politicians who spread misinformation while claiming to champion the Constitution.

</summary>

"The intent of the First Amendment is to protect the people from Congress. That's you, Senator."
"You are the establishment. You're not anti-establishment. You're not anti-government."
"Many of those who pretend to champion the Constitution don't really want it applied."
"There is no debate over whether or not masks work. They do."
"An attempt to force them to carry a message from the government, that's you, Senator Paul, that's a violation of the First Amendment."

### AI summary (High error rate! Edit errors on video page)

Senator Rand Paul faces a suspension from YouTube for spreading medical misinformation, not for undermining public health efforts.
Paul falsely claims that YouTube is violating his freedom of speech, failing to understand that the First Amendment protects people from Congress, not Congress from people.
Beau points out that if masks were truly ineffective, the only remaining solution for public health in the US would be mandatory vaccines and vaccine passports.
He criticizes politicians who prioritize sound bites over their own beliefs, like Senator Paul who claims to be anti-establishment but is actually part of the government.
Beau addresses the harmful impact of spreading misinformation about masks on public health efforts and the hypocrisy of those who claim to champion the Constitution but only want it applied when convenient.

Actions:

for politically engaged citizens,
Fact-check information shared by politicians and public figures (implied)
Support platforms that combat misinformation (implied)
</details>
<details>
<summary>
2021-08-10: Let's talk about what it means to be woke and Garth Brooks.... (<a href="https://youtube.com/watch?v=_HSd3MflmTc">watch</a> || <a href="/videos/2021/08/10/Lets_talk_about_what_it_means_to_be_woke_and_Garth_Brooks">transcript &amp; editable summary</a>)

Garth Brooks's mask mandate sparked debates on woke-ness, with Beau challenging the oversimplified definition and advocating for actively addressing systemic issues and injustices.

</summary>

"True woke-ness involves actively working to end systemic issues and injustices."
"They were woke the whole time, trying to get that message out there."
"Once again, it boils down to somebody stepping out of line and the right-wing cancel culture coming for them."

### AI summary (High error rate! Edit errors on video page)

Garth Brooks mandated mask-wearing at his concerts, sparking controversy over being labeled as "woke" for a mildly socially responsible act.
Beau criticizes the oversimplification of the term "woke" to mean only being alert to injustice, arguing that true woke-ness involves actively working to end systemic issues and injustices.
He suggests that being truly woke means recognizing and addressing food insecurity, fear of violence, racial injustices, housing as a human right, environmental injustice, and the privilege associated with money.
Beau points out that Garth Brooks has a history of advocating for social issues, such as gay rights in his music in the 90s, challenging the notion that he has only recently become woke.
He criticizes the tendency to label individuals as woke when they deviate from expected norms or demonstrate socially responsible behavior.

Actions:

for social justice advocates,
Advocate for addressing systemic issues and injustices (exemplified)
Support social causes actively (exemplified)
Challenge oversimplified definitions of terms (exemplified)
</details>
<details>
<summary>
2021-08-09: Let's talk about two Florida tough guys.... (<a href="https://youtube.com/watch?v=PYJ3xN1Q8u0">watch</a> || <a href="/videos/2021/08/09/Lets_talk_about_two_Florida_tough_guys">transcript &amp; editable summary</a>)

Beau defines tough guys as those willing to do the hard right thing over the easy wrong thing, applauding medical professionals and leaders like Nikki Freed for providing genuine leadership during challenging times.

</summary>

"What makes you tough is the willingness to step up and do the right thing when others won't."
"We need more tough guys willing to step up, to lead, to provide the guidance that the country needs."

### AI summary (High error rate! Edit errors on video page)

Defines tough guys as those willing to do the hard right thing over the easy wrong thing, not necessarily the traditional image.
Describes the failure of mitigation efforts in Florida and the resulting stress on medical professionals.
Shares his wife's experience as a nurse who returned to work due to the shortage of medical staff in Florida.
Hails individuals like his wife and Nikki Freed, Secretary of Agriculture, as tough guys for stepping up and providing leadership during challenging times.
Contrasts genuine tough guys, like medical professionals and leaders advocating for public health measures, with those who pose as tough but fail to take basic precautions themselves.
Stresses the importance of true leadership in the face of widespread misinformation and the need for more people to step up and provide guidance.
Emphasizes that toughness is not about appearances or symbols like guns, but about doing what is right even when it's difficult.

Actions:

for community members, activists, voters,
Support medical professionals by volunteering at hospitals or clinics to alleviate staffing shortages (implied)
Advocate for public health measures like wearing masks and getting vaccinated to protect the community (implied)
</details>
<details>
<summary>
2021-08-07: Let's talk about being on the road right now.... (<a href="https://youtube.com/watch?v=7pl0TUOWMpw">watch</a> || <a href="/videos/2021/08/07/Lets_talk_about_being_on_the_road_right_now">transcript &amp; editable summary</a>)

Beau finishes a successful production trip but expresses hesitance due to lack of COVID precautions, urging everyone to take safety seriously.

</summary>

"Please take it seriously. Take the precautions even if those around you aren't."
"Y'all just be safe out there."
"I'm not sure how soon we're going to duplicate this."
"People still are just kind of blowing this off as if it's over."
"I just didn't know that people weren't going to take it."

### AI summary (High error rate! Edit errors on video page)

Beau finishes a large-scale production for his second channel, Beau on the Road, after traveling seven days through eight states covering roughly 4,500 miles from the Gulf Coast to the West Coast, ending in LA.
Despite testing, masking, and being vaccinated, Beau is hesitant about doing meet and greets and setting up community networks due to the lack of precautions along the highways and interstates.
Native reservations are the only places with consistent protocols while most other areas lack clear safety measures.
Places like LA show more compliance compared to areas like Arizona and Texas where people seem to disregard the ongoing threat of the pandemic.
Beau expresses surprise at the reluctance of people to take the available vaccine or treatment despite its existence.
The production filming went well overall, with minor hiccups expected, but Beau is uncertain about duplicating it soon due to concerns about COVID-19 transmission.
Beau stresses the importance of taking precautions seriously, even if those around you are not, and mentions that the upcoming episode will take time to put together.
He encourages everyone to stay safe and concludes with a reminder to take the situation seriously, even if others are not.

Actions:

for travelers, community members,
Take COVID precautions seriously, even if those around you aren't (suggested)
Stay informed about the COVID situation in your area and act accordingly (implied)
Encourage others to follow safety guidelines (implied)
</details>
<details>
<summary>
2021-08-06: Let's talk about distribution and the Balmis Expedition.... (<a href="https://youtube.com/watch?v=TRg3lH0mCw4">watch</a> || <a href="/videos/2021/08/06/Lets_talk_about_distribution_and_the_Balmis_Expedition">transcript &amp; editable summary</a>)

Beau addresses vaccine distribution disparities, recounts a historical example, and advocates for global access through collective will and resources.

</summary>

"It's a matter of will and money."
"Beyond wealthy countries' borders do not live lesser people."
"We just have to create a situation in which politicians and those with the money and those with the resources know that we want it done."

### AI summary (High error rate! Edit errors on video page)

Addresses the disparity in vaccine distribution, with wealthy countries getting doses first.
Mentions past instances of similar distribution issues being faced.
Talks about organizations working to streamline vaccine distribution to developing countries.
Tells the story of Dr. Francisco Javier de Bamas in 1802, who wanted to inoculate Spain's colonies against smallpox.
Describes how Dr. Bamas overcame logistical challenges by using cowpox for inoculation.
Mentions the use of orphan boys to transport the serum during the voyage.
Notes that younger people fared better during the inoculations across different countries.
Emphasizes that mass vaccination programs have existed for a long time, indicating that distribution is possible.
Stresses that distributing vaccines is a matter of will and money, not just logistics.
Draws attention to the contrast between reluctance in wealthy countries to take vaccines and lack of access in developing nations.
Advocates for prioritizing vaccine distribution to save lives globally.
Calls for political will and resources to ensure equitable vaccine access.
Urges for a collective effort to make vaccine distribution a reality.
Acknowledges the challenges but remains optimistic about achieving widespread vaccination.
Concludes with a call to action for political and financial support in vaccine distribution efforts.

Actions:

for global citizens, policymakers,
Support organizations working on streamlining vaccine distribution to developing countries (suggested)
Advocate for equitable vaccine access through political channels (implied)
</details>
<details>
<summary>
2021-08-05: Let's talk about the national anthem.... (<a href="https://youtube.com/watch?v=58LaxG9Jdsc">watch</a> || <a href="/videos/2021/08/05/Lets_talk_about_the_national_anthem">transcript &amp; editable summary</a>)

Exploring the controversial third verse of the US national anthem, Beau questions its racist origins, provides interpretations, and suggests that it should be changed due to its offensive content.

</summary>

"There's no good way to read this."
"The only interpretation that isn't just absolutely horrible is that it's an insult to all British people."
"There's no reason it can't be changed."

### AI summary (High error rate! Edit errors on video page)

Exploring the interpretation and meanings of the third verse of the US national anthem, the Star Spangled Banner, in response to a viewer's query.
Providing context that the anthem is based on a poem written during the War of 1812, not the Revolutionary War, and questioning why it's the national anthem.
Describing Francis Scott Key, the anthem's writer, as a racist who believed black people were inferior and evil.
Analyzing the controversial third verse that mentions "hireling nor slave" and discussing four possible interpretations, including references to colonial marines and impressment.
Asserting that the true meaning behind the verse remains unknown as Key never clarified it, debunking any certainty in interpreting it.
Suggesting that the offensive nature of the anthem, considering its lyrics and Key's beliefs, gives grounds for people to be upset.
Pointing out the lack of positive interpretations in the anthem and proposing that it should be changed due to its problematic content.
Challenging the idea of tradition by revealing that the anthem was officially adopted by Congress as the national anthem less than 100 years ago in the 1930s.

Actions:

for americans,
Advocate for changing the US national anthem (suggested)
Educate others about the problematic history and lyrics of the anthem (implied)
</details>
<details>
<summary>
2021-08-04: Let's talk about believing things without evidence.... (<a href="https://youtube.com/watch?v=qQGXkDxoUaw">watch</a> || <a href="/videos/2021/08/04/Lets_talk_about_believing_things_without_evidence">transcript &amp; editable summary</a>)

Beau addresses the harm in accepting theories without evidence, particularly those reinforcing Eurocentric biases and downplaying the achievements of non-European civilizations.

</summary>

"Those people over there, there's no way they could have done that. Brown people couldn't have done that. It had to be green people."
"Not just is that wrong for obvious reasons, it's historically inaccurate."
"It's probably not a good road to go down."

### AI summary (High error rate! Edit errors on video page)

Addressing the harmful impact of accepting theories without evidence.
Exploring the theory of super advanced species teaching ancient civilizations.
Questioning the Eurocentric bias in theories about ancient civilizations.
Criticizing the reinforcement of the idea that only European cultures were capable.
Warning against accepting theories with scant evidence and their potential negative impact.

Actions:

for critical thinkers,
Question theories without evidence (implied)
Challenge Eurocentric biases in historical interpretations (implied)
</details>
<details>
<summary>
2021-08-03: Let's talk about the hearing and dueling definitions.... (<a href="https://youtube.com/watch?v=4Xi58mf5TJE">watch</a> || <a href="/videos/2021/08/03/Lets_talk_about_the_hearing_and_dueling_definitions">transcript &amp; editable summary</a>)

Beau dives into dueling definitions between experts, showcasing a disagreement between a military friend and a lawyer over a term during hearings, illustrating the broader issue of influencing the government through violence.

</summary>

"When you run into this issue where you have two experts who disagree, maybe start from the position that they're both right, but there's a misunderstanding of what they're talking about."
"This is a semantic argument. People who are experts, who are very well informed about a topic, generally they also have huge egos."
"That's the main point is that the institutions that the United States relies on were under direct attack."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of dueling definitions when experts disagree.
Describes a scenario where two friends, one from the military and the other a lawyer, disagreed over the use of a term during hearings.
The military friend argued against using the term "terrorist," proposing "coup" or "self-coup" instead.
The lawyer, however, was correct in using the legal statute's definition, which differs from the academic perspective.
Beau suggests that both friends were right from their respective standpoints due to a misunderstanding of definitions.
Points out the broad and vague nature of the federal statute on what constitutes an act of terrorism.
Emphasizes that regardless of the semantic debate, the core issue was an attempt to influence the government through violence or threat during the election overturn.
Advocates for focusing on the critical aspect of the situation, which was the attack on vital US institutions by violence or the threat of violence.

Actions:

for analytical thinkers,
Organize educational sessions to clarify differing definitions and interpretations within specific fields (suggested)
Engage in respectful dialogues to bridge understanding gaps between conflicting perspectives (implied)
</details>
<details>
<summary>
2021-08-02: Let's talk about Republicans rejecting democracy.... (<a href="https://youtube.com/watch?v=Sx2JD5TR6VU">watch</a> || <a href="/videos/2021/08/02/Lets_talk_about_Republicans_rejecting_democracy">transcript &amp; editable summary</a>)

Alarming poll numbers reveal Republican attitudes towards democracy and voting as privilege, urging immediate action to defend democracy before authoritarianism grows further.

</summary>

"Americans will ignore these numbers at their peril."
"Government is an illusion. It's a lot like a dollar."
"Their base rejects the founding principles of this country."
"Now is your time, and you had better do something before the midterms."
"It's a privilege. Probably trying to ensure the purity of the ballot box."

### AI summary (High error rate! Edit errors on video page)

Talks about two polls showing alarming numbers among Republicans.
63% of Republicans do not believe democracy is working.
67% of Republicans view voting as a privilege, not a right.
Beau warns that the Republican Party base is leaning towards dictatorship.
Urges Americans to pay attention to these alarming numbers.
States that government is an illusion, much like a dollar's value.
Calls for accountability by subpoenaing everyone who has supported baseless claims.
Suggests charging individuals with perjury if they lie under subpoena.
Expresses concern that without action, authoritarianism will continue to grow.
Warns that without accountability, current events may become a trial run for worse situations.
Emphasizes the importance of defending democracy and institutions now.
Points out the contradiction in viewing voting as a privilege but not gun ownership.
Calls on politicians to act now in defense of democracy before the midterms.

Actions:

for american citizens,
Contact Congress to demand accountability for those supporting baseless claims (implied)
Take action to defend democracy and institutions before the midterms (implied)
</details>
<details>
<summary>
2021-08-01: Let's talk about Trump, kingmaker of the Republican party.... (<a href="https://youtube.com/watch?v=JO-3qSgkNNM">watch</a> || <a href="/videos/2021/08/01/Lets_talk_about_Trump_kingmaker_of_the_Republican_party">transcript &amp; editable summary</a>)

Former President Trump's influence as a kingmaker in the Republican Party is questioned as financial backing seems more critical than his endorsements, perpetuating the damaging influence of Trumpism within the GOP.

</summary>

"Trump isn't the kingmaker, he's just shaping up to be yet another mega-donor."
"The brand of authoritarianism, Trumpism, that he founded, it is still widespread within the Republican Party."
"He isn't the political juggernaut that he likes to pretend that he is."
"The glitz, the gold, it's not real. It's just a brand. It's an image."
"Trump isn't the influencer. He's a mega donor. That's it."

### AI summary (High error rate! Edit errors on video page)

Former President Trump's influence as a kingmaker in the Republican Party is in question after his endorsed candidate in Texas lost.
Trump's PAC invested $350,000 in advertising for his endorsed candidate in Ohio, indicating that his name alone may not be enough to secure victories.
The endorsement from Trump may not hold as much weight as previously thought, with the need for financial backing to boost candidates.
The brand of authoritarianism known as Trumpism still holds strong within the Republican Party, despite Trump's decreasing personal influence.
Republicans must reclaim control of their party from the Trump image and brand, which may be doing long-term damage to both the party and the country.
Trump, although projecting an image of political strength, may not be as influential as he portrays himself to be, often losing more than winning.
The glitzy image associated with Trump is perceived as more of a branding tactic rather than a true representation of political power.
The election results suggest that Trump's influence may be more about financial contributions rather than genuine endorsement power.
The Republican Party needs to break free from the dominance of the Trump brand and regain control to prevent further damage.
Trump's influence is shifting towards that of a mega donor rather than a political influencer.

Actions:

for republican party members,
Reclaim control of the Republican Party from the dominating influence of the Trump brand (implied).
Focus on rebuilding the party's image and platform independent of Trump's influence (implied).
Work towards reducing the prevalence of Trumpism within the Republican Party (implied).
</details>
