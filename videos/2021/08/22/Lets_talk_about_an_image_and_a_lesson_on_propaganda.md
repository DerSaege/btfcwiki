---
title: Let's talk about an image and a lesson on propaganda....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=C6WTa1QmaF8) |
| Published | 2021/08/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the circulation of an image comparing Afghan forces to the Marines at Iwo Jima.
- Criticizing the use of military propaganda for political gain by media and commentators.
- Pointing out the subtlety and layers in propaganda images that are often missed.
- Emphasizing the importance of understanding the true message behind propaganda.
- Calling out those who prioritize political interests over human lives in conflict zones.
- Urging people to fact-check and not fall for manipulated narratives.
- Advocating for dismantling propaganda messaging by exposing its flaws.
- Stating the opposition in Afghanistan is objectively bad from various perspectives.
- Warning against unwittingly supporting opposition propaganda that could harm innocent lives.
- Noting the urgency of the situation at the airport in Afghanistan and the need for swift action.

### Quotes

- "If your political platform aligns so well with military opposition, maybe your domestic political platform needs a little bit of work."
- "Time is running out at that airport. They're sending a message. Time is running out."
- "There are lives that are hanging in the balance, tens of thousands of lives."

### Oneliner

Beau addresses the dangerous implications of circulating manipulated images for political gain, urging for a critical understanding of propaganda's impact on human lives.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Fact-check images and narratives before sharing (suggested).
- Expose and dismantle propaganda messaging by analyzing and educating others on its flaws (implied).
- Raise awareness about the importance of critically evaluating information shared online (implied).

### Whats missing in summary

The full transcript provides a deeper insight into the manipulation of propaganda images and the ethical implications of using them for political purposes.

### Tags

#Propaganda #MediaLiteracy #PoliticalManipulation #Afghanistan #HumanRights


## Transcript
Well howdy there internet people it's Beau again. So today we're gonna talk
about an image, an image that is being circulated widely right now. We're gonna
talk about that image, we're gonna talk about the people that are sharing that
image in the media. We're gonna talk about some subtlety in that image that
is almost certainly being missed, and we're gonna talk about a secret when it
comes to that image, something you're probably not going to hear on a lot of
outlets. If you don't know what I'm talking about, the opposition in
Afghanistan chose right now to surface an image, remember that term surface, to
surface an image of their forces posed like the Marines at Iwo Jima. That iconic
image of the American flag being hoisted, the poll being pushed up. That image is
being shared widely in the United States by people in the media, political
commentators, people who should understand how information works. They
should know everything that I'm about to say. If you don't deal in information as
is your trade, you don't really have a reason to know this.
But those who put this out there, those
who carried this messaging forward, they absolutely should.
They should know.
Um, that image is military propaganda.
Military propaganda can serve a number of purposes,
but it falls into two real categories.
The first is to inspire your side.
The other is to demoralize the opposition.
So your favorite pro-America political commentator
that's carrying this should probably
explain which one of those they're trying to accomplish.
Given the fact that the attached messaging with this image
is, look how weak we are.
We're a laughing stock.
It's a failure.
Look at the American equipment and that's Joe Biden's fault.
That's what it's about.
It's about using opposition propaganda domestically for their own political ends.
People who I am certain still hold a grudge over Hanoi Jane are carrying
opposition propaganda to the American people.
Because they can use it politically.
I'm going to suggest that if your political platform
aligns so well with military opposition,
maybe your domestic political platform
needs a little bit of work.
It's not really a good look, to use that phrase.
But it does go well with that $0.99 American flag lapel
pin that is supposed to show you are a patriot.
The subtlety in this image that is being missed,
when Americans see this image, when
they see that image of the Marines at Iwo Jima,
they associate it with the end of World War II.
Right?
The thing is, when people create images like this,
When they create propaganda images, they do it in layers.
There's a lot of subtlety to it.
And the person who created this image,
they certainly know what I'm about to say.
That's not what it was.
If you look at American messaging at the time,
it was an outpost before the advance on the Inner Empire.
In the analogy, the inner empire is the airport.
Tens of thousands of lives hang in the balance,
and American political commentators
are carrying opposition messaging because it suits
their ends politically at home.
Because that's what they care about.
They don't actually care about the lives over there.
That's pretty apparent.
But it's newsworthy, right?
we have to talk about it.
We have to talk about it because it shows
that American military equipment,
because of Joe Biden, fell into opposition hands.
That's why we're talking about it.
We're not really trying to carry their messaging.
Because it's newsworthy,
because you're a journalist who would fact check,
which means you would download that image
and upload it into 10i.com.
That is T-I-N-E-Y-E.com.
That's a reverse image search.
It'll show you everywhere that image is online,
and when it first appeared, which in this case
was August 2nd, 20 days ago, four days before the first
provincial capital fell, which means
it was created before then.
You're not going to hear that because it
doesn't fit their narrative.
It doesn't fit the image that they want to cast.
If you encounter opposition propaganda,
assuming you want to defeat it and you're not somehow rooting for them, the goal is
to take that propaganda, acknowledge it exists, tell people it's garbage, and then defeat
one of the layers that exists, one of the message points, in this case drawing a parallel
between opposition forces in Afghanistan and US Marines during World War II.
I would suggest that that's probably a better talking point, that's probably a better story
if you actually are pro-America, if you are America first.
The thing is, the opposition there, they're objectively bad.
People get mad when I say that, but it doesn't matter where you stand on the political spectrum.
They're not the good guys.
They're objectively bad.
It's from a basic human rights standpoint, from fundamental human rights inequality,
they're bad.
From American national interests, they're bad.
From geopolitical standpoint, they're bad.
From an economic standpoint, they're bad.
It doesn't matter what lens you look at it through.
They're the bad guys.
There aren't many times in American history when the US military is in opposition to a
group that is objectively bad. It doesn't happen often. It's happening now, and those
with the American flag lapel pin who scream America first, who talk about how much they
love this country and what great patriots they are, have decided to carry the opposition's
messaging because it's good for them politically. Because on some level, their interests align.
There are lives that are hanging in the balance, tens of thousands of lives.
Perhaps emboldening the opposition by carrying their messaging isn't a good idea.
The only reason people are seeing this right now is because the outrage machine is carrying
it, posting it on their personal social media, talking about it on their show, showing the
image constantly.
That's why people are seeing it.
It's been online for 20 days.
They surfaced it right now to send a message, and it's a message that many are willingly
carrying, becoming unwitting accomplices in the dissemination of opposition propaganda.
The important part about this, since it is everywhere, time is running out at that airport.
They're sending a message.
Time is running out.
It's kind of time to pull out the stops on everything and get as many people out as you
can.
My guess on this is that September, they're going to go for it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}