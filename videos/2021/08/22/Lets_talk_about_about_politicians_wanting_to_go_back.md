---
title: Let's talk about about politicians wanting to go back....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=y32qLGswI5I) |
| Published | 2021/08/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The national government's shift towards discussing going back has caught Beau off guard.
- Some senators have shown support for maintaining a minimal military presence in the country.
- The plan involves establishing a forward operating base with special operations personnel for mitigation operations.
- Beau warns that dropping a base without effective control of the countryside will lead to opposition infiltration and eventual overrun.
- The opposition's strategy of slowly seeding areas with their personnel before making a move is emphasized.
- Beau predicts a scenario where special forces leave the base for an operation, and the opposition strikes when the base is vulnerable.
- Beau draws a grim parallel to Dien Bien Phu, signaling the disastrous consequences of attempting to establish a token security force.
- The focus should be on preserving human life by evacuating people, rather than military strategies or geopolitical concerns.
- Beau urges readiness to oppose any attempts to re-enter the country militarily, as the priority should be getting people out safely.
- Politicians may resort to various tactics to justify going back, but Beau stresses the importance of standing against it.

### Quotes

- "That's no longer an option. That window is closed."
- "Getting people out. Getting the Americans out. Getting the allies out. Getting any refugee that wants to go out. That should be the goal."
- "There is one priority right now and that is the preservation of human life."

### Oneliner

The national government's push to return militarily is ill-advised; focus must shift to evacuating people to preserve lives.

### Audience

Policymakers, Activists, Citizens

### On-the-ground actions from transcript

- Evacuate people, Americans, allies, and refugees who wish to leave (implied)
- Oppose any attempts to re-enter the country militarily (exemplified)

### Whats missing in summary

The full transcript provides detailed insights into the risks of returning militarily and the importance of prioritizing human life over geopolitical concerns.

### Tags

#NationalGovernment #MilitaryPresence #HumanitarianCrisis #Evacuation #Opposition


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
going back because we've officially entered that phase of this. I am off my
game. I expected the national government to hold on a wee bit longer than it did
and I expected us to at least get out of the country before a bunch of
politicians started talking about going back. I guess I was wrong. You've had a
couple senators now make statements in support of it. Most are kind of vague,
stuff like American soldiers should stay and accept the risk. To stuff that's a
little bit more specific, such as we need to maintain a minimal presence, a light
footprint. The idea is to get that token security force that would have worked a
month ago. But see that international poker game where everybody's cheating?
Yeah, other cards have been dealt. It has moved on to the next hand. That's no
longer an option. That window is closed. Y'all need to read your briefings a
little bit sooner. That won't work. The plan, plan, as it was laid out, is to
basically establish a forward operating base full of a bunch of special
operations people and they're going to run mitigation operations from there all
over the country. Like many things, you can tell what will happen by what has
happened. How did the opposition take the country so quickly? Because they started
in April. They started in April seeding areas with their people, beard-shaved,
blending in with the local population. Then when the time was right, they
merged. That's why there wasn't a lot of marching. That's how they were able to do it so
quickly. If a forward operating base is dropped in the middle of that country
with no apparent backup because we want to establish a light footprint, the
opposition will seed the area around that with its personnel. Imperceptibly,
they will move their people in slowly. They won't do anything. They're just going to hang out and be around it.
And then one day, those special guys, they're going to get a target they can't pass up.
They're going to get a golden opportunity on the other side of the
country and they're going to load up in those helicopters and they're going to take off.
And that's when the opposition will form up. And again, they're not going to do
anything. Not right away. They're going to wait until those helicopters get a good
distance away. And then they're going to overrun that base. That is what will
happen if this is attempted like this. This is the worst idea in a long string
of bad ideas over there. You've been dying for a Vietnam analogy. You're going to get one.
Dien Bien Phu. This is a really, really bad idea. That token security force, that window is closed.
The United States and its allies do not have effective control of the countryside.
The opposition does. They will seed it and they will overrun that base.
If you want to go back, you got to go back. You're basically going to refight the
entire war because the opposition has control of the country. You're going to have to refight the
entire war and then you can fall back and reduce troop levels and get to that
token security force. I'm sure the troops will be home by Christmas. It's also worth
noting that ideally that token security force wasn't American because of the
history of our presence there. The United States has a bruised ego right now and
is just desperately thrashing about trying to come up with something, cling to
something to make this less of a failure. It's the wrong move. If the US goes back
in, it will end the same way because we have learned nothing, as is very apparent.
There is one priority right now and that is the preservation of human life.
Getting people out. Getting the Americans out. Getting the allies out. Getting any
refugee that wants to go out. That should be the goal. That's what matters. Nothing
else. The geopolitical concerns, the time for trying to stabilize, all of that
stuff is over. You can't put the genie back in the bottle on this one. It's done.
I would guess that the politicians will continue this kind of rhetoric. You need
to be ready for it and you need to be ready to oppose it because they will
pull the human rights cards. They will pull the funding cards. They're going to pull it
all out. I don't think I need to tell any of y'all this. It's just more of a heads up
that it's coming. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}