---
title: Let's talk about 3000 headed back over there....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DPMteEIm_g4) |
| Published | 2021/08/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration is sending 3,000 U.S. troops back into Afghanistan to evacuate people.
- The troops' main objective is to secure the airport, embassy housing, and routes to evacuate Americans and allies.
- The operation could take a day or up to a week.
- There is a chance of things going wrong, but it's not a significant risk.
- The opposition in Afghanistan is not to be underestimated; they are strategic and intelligent.
- The U.S. security force in Afghanistan acted as a deterrent rather than an impenetrable defense.
- The likelihood is that the opposition will regain control of the country once the U.S. forces leave.
- The U.S. may use air support to aid the national government from afar after evacuation.
- Beau doubts that Biden will resort to extensive military action in Afghanistan.
- The U.S. air support might slow down the opposition but is unlikely to alter the outcome significantly.

### Quotes

- "It's possible, but I don't see it as likely."
- "The opposition there really isn't a bunch of ignorant, backward hill people. They're pretty smart."
- "The odds are the opposition will control the country in very short order."
- "The U.S. may decide to bomb until the rubble bounces."
- "They're winning right now. They have no reason, there's no strategic value, in drawing the West back in."

### Oneliner

Beau provides insights on the return of U.S. troops to Afghanistan, evacuating people with a slim chance of failure, and the likelihood of the opposition regaining control swiftly.

### Audience

Policymakers, activists, citizens

### On-the-ground actions from transcript

- Support organizations aiding Afghan refugees (suggested)
- Advocate for peaceful resolutions in foreign policy (implied)

### Whats missing in summary

Insights on the broader implications of the U.S. troop return to Afghanistan.

### Tags

#Afghanistan #UStroops #Evacuation #ForeignPolicy #Security


## Transcript
Well, howdy there, Internet of People. It's Beau again.
So today we're going to talk about what's going on over there,
why we're going back over there, what they'll be doing while they're there,
whether it can go sideways, how long they'll be there, what they'll be doing,
so on and so forth.
If you don't know what I'm talking about, the Biden administration
has announced that the U.S. will be sending 3,000 U.S. troops
back into Afghanistan.
So what are they going to be doing?
They're going to be there to evacuate people, get them out.
The administration is saying that it is very narrow in scope.
Yeah, yeah, from what I understand it is.
What are they going to be doing?
An operational template for something like this, they would show up and secure
the main airport there in the city, probably some secondary airfields
around the area, the embassy housing where there are a lot of Americans,
so on and so forth, and then they will secure roads, routes,
connecting these places.
Then they will evacuate and get everybody out, put them on a plane, and
leave. It will be very disruptive for the people
who live in these areas. How long will something like this take?
I didn't look to see which units are doing it.
This could be done in a day or two. It doesn't have to take a long time.
It could take up to a week.
Is there a chance of it going sideways and going bad?
Yeah, there is. Is it a big chance? No. The main factor is
what the opposition decides to do. They could, theoretically, decide to give
the U.S. a black eye on the way out the door.
And yeah, maybe, but contrary to media depictions in the
United States, the opposition there really isn't a bunch
of ignorant, backward hill people. They're pretty smart.
The reason the token security force that the U.S. had,
that was there, the reason it was effective, wasn't because
the opposition couldn't defeat them militarily. They absolutely could.
They were a speed bump. They were an alarm. If
they advanced on them, it would provoke a U.S. response.
The idea of hitting the U.S. as it's leaving,
it would be a bad move, and they haven't made a lot of bad moves.
It's possible, but I don't see it as likely.
Then the administration is suggesting that it is not
inevitable that the Capitol fall to the opposition.
I mean, sure, in the fog of war way where nothing is certain,
yeah, I mean, it might not happen, but it's probably going to.
That is the most likely scenario. Now, is there anything that can really tip
the scales there? Sure. Once all Americans are out,
all Westerners are out, really, the U.S. and allied nations
might utilize a lot of over-the-horizon capability, like we talked about before.
They've done a little bit, but they really might try to support the
national government from the air, and with no U.S.
assets on the ground for the opposition to strike back
against, it is not inconceivable that the U.S.
decide to bomb until the rubble bounces.
It's something they might do. Do I think that Biden would do it?
No, but not for the reasons that people would want.
My guess is that Biden wants this to be over with as quickly as possible.
Get our forces out, get our people out, back to the U.S., focus on infrastructure,
domestic stuff, change the story, and while everything
that's going to happen over there happens,
he keeps everybody looking the other way. It has nothing to do with his
reluctance to use military force. That's not really a thing.
I think that if he believed it would work,
he probably would. Realistically, though, it wouldn't. I don't believe it would.
I don't think that the United States' air support is going to be a deciding
factor in the overall outcome here. It might
slow the opposition advance, but I do not believe it would stop it.
I do not see many paths
that don't end with the opposition taking the entire country.
We talked about it before in likelihoods.
As that chain of events progressed, the further down
that list of predictions we got, the less and less likely
it was that the national government would be able to hold on, and at this
point, it would take a miracle. The odds are
the opposition will control the country in very short order.
I do think they'll wait for U.S. troops to get out of the way, U.S. forces, U.S.
personnel, because they don't. They're winning right now.
They have no reason, there's no strategic value,
in drawing the West back in. If they do it, it will be a bad decision based on
ego,
because at this moment, there's nothing stopping them.
So that's where we're at. That's a general
situation report on what's going on over there.
I wouldn't worry too much about them sending these troops back in.
They'll be gone pretty quickly. There is a chance of something going
sideways, but it's small, and the opposition is
likely to retake the entire country very quickly.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}