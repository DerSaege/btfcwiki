---
title: Let's talk about the census results causing concern for Tucker Carlson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=g7gbHarHBDw) |
| Published | 2021/08/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing concerns about census results and information causing worry among some people.
- Sharing a message from a conservative viewer questioning lack of concern about decreasing white population.
- Mentioning watching conservative media that downplay the challenges faced by minorities in the country.
- Pointing out systemic issues in the U.S. that disadvantage certain racial groups.
- Encouraging working on correcting these problems rather than being afraid of becoming a minority.
- Expressing lack of personal concern about demographic shifts and focusing on working towards equality.
- Emphasizing the importance of fighting for equality over seeking vengeance.
- Stating that skin tone shouldn't hold such significance in society.
- Arguing against adjusting life or policies based on demographic changes.
- Noting concerns arise from realizing how the current system disadvantages minorities.

### Quotes

- "Skin tone shouldn't be that important, right?"
- "They're fighting for equality. They're not fighting for vengeance."
- "Now you're worried about that system being turned on you."

### Oneliner

Beau addresses concerns about census data, systemic racial issues, and the importance of working towards equality over fear of becoming a minority.

### Audience

Activists, Advocates, Community Members

### On-the-ground actions from transcript

- Coordinate with organizations working towards equality (implied)
- Work on correcting systemic issues related to race (implied)

### Whats missing in summary

The emotional impact and nuances of Beau's message can be better understood by watching the full transcript.


## Transcript
Well, howdy there, Internet people. It's Bo again.
So today we're going to talk about the census,
because it has, the results of that and the information that came out,
that has certainly caused some concern among some people.
So we're going to talk about it.
I think the best example of the concern is from a message I got.
And it's from somebody who described themselves as a conservative
who watches this channel to see what the other side thinks.
And then it goes on to ask, you know, you're not really concerned about this.
You're not concerned about how there's less white people.
You know, I saw Tucker Carlson freak out about that.
I thought it was pretty funny.
No, I'm not concerned about it, because I watch Tucker Carlson's show.
And I watch a lot of conservative talk shows,
and I listen to conservative talk radio.
And they have told me my entire life that it's not hard
to be a minority in this country.
I don't know what they're concerned about, unless, of course,
they've been lying this whole time.
They knew it was unjust, and they spread the lie in an attempt to uphold it.
I mean, that's not what it is, right?
Or maybe it is.
Here's the thing about this.
All of a sudden, there's a lot of people that are afraid,
because they may be the minority race in the United States.
They're truly concerned about this.
If that is a concern, it is an acknowledgment
that there are systemic issues in the country dealing with race
that work to disadvantage certain groups.
If you're afraid of that, maybe you should start working
on correcting those problems now.
Maybe that would be a good idea.
Now, to be honest, no, I'm not concerned about it.
I'm really not.
It has nothing to do with watching those people.
It has to do with actually working and coordinating
with the people who are out there trying to get equality.
Because one of the things that has always amazed me
about the people in that fight is that they're fighting for equality.
They're not fighting for vengeance.
I'm not concerned about it.
Skin tone shouldn't be that important, right?
That's the world we want.
This isn't something that I'm going to go and adjust my life over.
I don't think that we should really dictate policy based on this.
The issue only arises because you know the current system
disadvantages those who are in the minority.
Now you're worried about that system being turned on you.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}