---
title: Let's talk about distribution and the Balmis Expedition....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TRg3lH0mCw4) |
| Published | 2021/08/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the disparity in vaccine distribution, with wealthy countries getting doses first.
- Mentions past instances of similar distribution issues being faced.
- Talks about organizations working to streamline vaccine distribution to developing countries.
- Tells the story of Dr. Francisco Javier de Bamas in 1802, who wanted to inoculate Spain's colonies against smallpox.
- Describes how Dr. Bamas overcame logistical challenges by using cowpox for inoculation.
- Mentions the use of orphan boys to transport the serum during the voyage.
- Notes that younger people fared better during the inoculations across different countries.
- Emphasizes that mass vaccination programs have existed for a long time, indicating that distribution is possible.
- Stresses that distributing vaccines is a matter of will and money, not just logistics.
- Draws attention to the contrast between reluctance in wealthy countries to take vaccines and lack of access in developing nations.
- Advocates for prioritizing vaccine distribution to save lives globally.
- Calls for political will and resources to ensure equitable vaccine access.
- Urges for a collective effort to make vaccine distribution a reality.
- Acknowledges the challenges but remains optimistic about achieving widespread vaccination.
- Concludes with a call to action for political and financial support in vaccine distribution efforts.

### Quotes

- "It's a matter of will and money."
- "Beyond wealthy countries' borders do not live lesser people."
- "We just have to create a situation in which politicians and those with the money and those with the resources know that we want it done."

### Oneliner

Beau addresses vaccine distribution disparities, recounts a historical example, and advocates for global access through collective will and resources.

### Audience

Global citizens, policymakers

### On-the-ground actions from transcript

- Support organizations working on streamlining vaccine distribution to developing countries (suggested)
- Advocate for equitable vaccine access through political channels (implied)

### Whats missing in summary

The emotional impact of prioritizing vaccine distribution for saving lives globally.

### Tags

#VaccineDistribution #GlobalAccess #HistoricalExample #Equity #CollectiveAction


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a little logistical issue
and how other people faced similar issues in the past
and they found a way to overcome it.
So we're going to go through a little history lesson as well.
We've talked about it on this channel before,
how there is a disparity in how things are being distributed.
As is typically the case, the wealthy are getting things first.
In this case, we're talking about the wealthiest countries
and we're talking about doses.
We're talking about the vaccine.
This isn't the first time that somebody has faced this issue.
And there are organizations out there that are trying to streamline it,
trying to get doses to the developing world faster.
But there's the constant complaint that it's hard,
that there are a lot of logistical issues to work out.
And sure, there are.
But I want to draw attention to a story.
It's the story of a doctor, Francisco Javier de Bamas.
Now this was in 1802, 1803.
He decided he wanted to inoculate the New World, Spain's colonies.
So he goes to the King of Spain because the doctor's smart.
And he's like, hey, I want to inoculate the colonies for smallpox.
And he knows the king's going to bankroll it because the king's daughter died of smallpox.
The king agrees.
Now what he's going to do is take cowpox, which protects you,
and basically use that to inoculate people with.
But there's a logistical issue.
It's hard to get it from Spain to the New World because they're not refrigerators.
So he came up with a novel approach.
And if you are somebody who specializes in medical ethics, just cover your ears.
He takes 22 orphan boys.
And during the voyage, he makes sure that two of them have it at all times,
uses the drainage from one set to give it to the next.
So when he arrives in the New World, he has basically what he needs, the serum.
He needs to start the inoculations.
Younger people fared better.
This little project, this little expedition,
it provided inoculations from Ecuador to Mexico to Peru to the Philippines to China.
And along the way, he had translated versions of basically a medical text on vaccination.
Now, I'm obviously not suggesting that this is how we distribute it.
What I am saying is don't tell me it can't be done.
It can be.
It's a matter of will.
It's not a matter of logistics in this case.
It's a matter of will and money.
We have had mass vaccination programs for forever, for a very, very long time.
This isn't something new.
It's a matter of money and it's a matter of will to get it done.
That's it.
The logistics can be worked out the same way they've always been worked out.
But we need the political will to do it.
The sad part is that in the wealthier countries,
you have people crying because they're being asked to take a shot.
And in the developing world, you have people dying because they can't get one.
That's probably something we should remedy.
Beyond wealthy countries' borders do not live lesser people.
And it's not something that's impossible to accomplish.
It can be done.
We just have to create a situation in which politicians
and those with the money and those with the resources know that we want it done.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}