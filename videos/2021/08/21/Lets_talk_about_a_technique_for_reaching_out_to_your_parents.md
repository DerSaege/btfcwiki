---
title: Let's talk about a technique for reaching out to your parents....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FCs_GuEOocc) |
| Published | 2021/08/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces a technique to reach out to parents based on an old army training film.
- The technique involves asking questions rather than telling parents they are wrong.
- Beau shares a story from the training film about a second lieutenant convincing a first sergeant about the effectiveness of a device.
- The key is to ask questions that lead parents to new information or perspectives.
- The approach is non-confrontational and aims to avoid conflicts during family gatherings.
- By using this tactic, Beau suggests that parents may see their children as having valid opinions.
- It's about managing perceptions rather than trying to change reality.
- Beau recommends being strategic in how you communicate with parents, especially when they hold different beliefs.
- The goal is to guide parents towards considering alternative viewpoints without direct confrontation.
- This technique has been effective for Beau in various situations over his life.

### Quotes

- "Rather than telling them they're wrong, ask them questions that they may not necessarily know the answer to."
- "It's about managing perceptions rather than trying to change reality."
- "You can't just walk in there and say you're wrong. I mean, you can, but it's not going to be productive."

### Oneliner

Beau introduces a technique to reach out to parents by asking strategic questions, aiming to guide them towards new perspectives non-confrontationally.

### Audience

Parents, Children

### On-the-ground actions from transcript

- Ask questions to guide parents towards new information or perspectives (suggested)
- Approach challenging topics with strategic questioning rather than direct confrontation (exemplified)

### Whats missing in summary

The full transcript provides detailed examples and insights on effectively communicating with parents by asking questions rather than engaging in confrontations. Viewing the entire transcript offers a comprehensive understanding of Beau's approach in navigating differing viewpoints within family dynamics.

### Tags

#ParentalCommunication #FamilyDynamics #NonConfrontationalApproach #PerspectiveShift #EffectiveCommunication


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about how to
reach out to your parents and a technique that I've used in the past
with various people that works. And we're going to do this because I had this
conversation and it was brief but the takeaway is that somebody wants to reach
out to their father but their father kind of discounts everything they say
because I mean they're young and how could they possibly know anything. It's
worth noting that if you're reaching out to your parents you don't know
everything but if you're in this situation where your father's
information diet consists of Fox News and I'm a great patriot dot ru, maybe
you do know some things that they don't know. There is an old army training film
that I've mentioned on the channel but it was years ago that addresses this
issue and this technique that it goes over works. I've used it ever since I saw
the film and it works so maybe try this. The film itself, its purpose is to train
new lieutenants on how not to be immediately despised when they show up
to their their unit for the first time. One of the scenes in it is this
lieutenant, if you don't know you can go to college, get your degree, go in as a
second lieutenant and you will immediately outrank people who have
been in the military for 20 years. And this does cause friction at
times. One of these scenes is this new second lieutenant trying to convince a
first sergeant that the M203 is good. Now for those of you who know how long the
M203 has been in service, yeah that's how old this video is. It's ancient but
the advice is great.
Ok so it starts off with the captain or major in the hallway and the second
lieutenant overhears him say that the first sergeant doesn't like this device.
And the second lieutenant's like, you know I trained on this thing. I'll go explain
to him you know why it's better. And the captain having a sense of humor was like,
yeah you go do that dude. So the second lieutenant walks into the room, the first
version of this scene. And he's like, first sergeant I understand that you
don't like the M203. You know I trained on it and you know I'd like to
explain to you why it would be more effective for us to use during war. And it
pans over to the first sergeant. And if you don't know anything about the
military, let's just cut this short. First sergeant is God.
Ok the idea of a second lieutenant explaining anything to a first sergeant
is funny in and of itself. But it pans to the first sergeant and you see all of his
ribbons and his insignia that indicates this guy has been to war
repeatedly. And the first sergeant's like, makes more effective a war huh son?
Looks down at his coffee. You know a lot about war. Puts his coffee down, leaves his own
office. End of conversation. Now keep in mind the second lieutenant is
absolutely correct in this, but it's the way he approached it. So the scene
resets and the second lieutenant walks in and he's like, hey Topp, you've already
been to Vietnam right? First sergeant's like, yes. Sets his coffee down because he knows a
conversation's coming. You know I trained on the 203, so how would we go about
using that to greatest effect when we get over there? First sergeant's like, well
you know it is useful to have them holding the rifle and the launcher at the
same time. And he starts talking and in the process he convinces himself that
maybe it's not so bad. It's the idea of using that very dynamic that gets them
to discount anything you say to your advantage. You ask them questions that
lead them to where they need to be. I know not, I mean it's kind of a
manipulative tactic to use on your parents, but at this point there are a
lot of people who have fallen down information silos and they're just
getting nothing but bad information. And this, in the process of hopefully helping
them get to some new information, it may also get them to see you as somebody who
does have valid opinions. Again, you're not talking about reality anymore, you're
talking about people's perceptions. And the perception is, you're a kid, you're always
going to be their kid. It doesn't matter, when you're 30 years old, they're still
going to look at you as a kid. But this tactic is something that I've used a lot
over my life to great effect. So rather than telling them they're wrong, ask them
questions that they may not necessarily know the answer to, or ask them questions
that will lead them to new information. And you can do it in a non-confrontational
way so it doesn't spoil Thanksgiving dinner. That's my best advice on this,
when you're talking about your parents, because you can't just walk in there and
say you're wrong. I mean, you can, but it's not going to be productive. So that's how I'd go
about it. I hope it helps. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}