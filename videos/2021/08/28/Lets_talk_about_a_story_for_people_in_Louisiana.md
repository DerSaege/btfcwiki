---
title: Let's talk about a story for people in Louisiana....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Ce3uKXcd_t8) |
| Published | 2021/08/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Tells a funny story about a friend who enjoys hurricane parties in Louisiana.
- Friend's wife experiences her first hurricane, Hurricane Michael, causing unease.
- Urges people in Louisiana to pay attention to Hurricane Ida, scheduled to hit as a Category 4 with potential storm surges.
- Mentions the devastation caused by Hurricane Michael, with businesses still unrepaired to this day.
- Advises those in low-lying areas to evacuate and prepare emergency supplies, including medications, pets, and phone batteries.
- Emphasizes the importance of heeding evacuation warnings and not waiting until it's too late.
- Encourages residents to ensure they are prepared for rescue operations rather than recovery operations.
- Stresses the seriousness of Hurricane Ida and the potential damage it could cause based on where it hits.

### Quotes

- "Get your meds, get all of your emergency supplies, get your little battery for your phone, get everything that you need, your pets, your pet food, and get inland."
- "Make sure that those Cajun guys who are going to come help are going to be doing rescue operations and not recovery operations."
- "If you're in Louisiana, take this one seriously, because it is doing all of that same creepy stuff with the changes."

### Oneliner

Beau urges Louisiana residents to take Hurricane Ida seriously, evacuate if necessary, and prepare for potential storm surges and damage.

### Audience

Louisiana residents

### On-the-ground actions from transcript

- Evacuate if in low-lying areas and gather emergency supplies (suggested)
- Ensure you have medications, emergency supplies, pet essentials, and evacuation plans ready (suggested)
- Prepare for rescue operations by being inland and out of harm's way (suggested)

### Whats missing in summary

The emotional impact of experiencing a hurricane and the urgency of preparing and evacuating to ensure safety. 

### Tags

#Louisiana #HurricaneIda #Evacuation #EmergencyPreparedness #NaturalDisaster #CommunitySafety


## Transcript
Well, howdy there, internet people. It's Bo again.
So today, I'm just going to tell you a story.
A funny story.
And if you live in Louisiana,
please listen to it.
I have a friend,
and he's been through a lot of hurricanes.
And on some weird level, he enjoys them.
He's one of those. He likes them. Has hurricane parties.
Never leaves, you know.
His wife had never been through one before.
And there was one tracking their way. Now, they had to stay for
a few reasons, but
she was a little concerned.
She asked him, she's like, is this anything to worry about?
He's like, no, it's a three.
We're going to have a party.
And then that three became a strong three.
And then that strong three became a four.
And then she wakes up to me,
I mean him,
him, not me, this isn't about me, because I would never be this goofy, right?
She wakes up to me lashing stuff down.
Taping up windows.
Doing all the preparation
that I should have done before.
Because her first hurricane
is going to be Michael.
I will say she handled it very well.
There was a little bit of unease when the
giant Florida pines started falling.
But she did fine.
I did get an earful, and I'm fairly certain she will never ride one out again.
But, at the end of the day, everything worked out.
I'm saying this, and if you're in Louisiana, I want you to pay attention,
because Ida is doing the exact same thing that Michael did.
It is now scheduled to hit, at time of filming,
it's scheduled to hit
at a four.
You're looking at fifteen foot storm surges.
If you're in a low-lying area,
you really need to pay attention to this. This looks like one it's probably time
to get out for,
not to schedule a party.
After Michael,
there were months
of relief operations.
Cutting people out of houses,
all kinds of stuff.
To this day,
there are towns you can drive through,
and you'll see businesses
in the downtown area,
where the front has just been ripped off, never repaired.
Ida is looking to be bad.
It all depends on where it hits,
as far as how bad.
If it hits
in one of those areas,
that it can cause a lot of damage because of the geography,
you don't want to be there.
If you have the ability,
get your meds,
get all of your emergency supplies, get your little battery for your phone,
get everything that you need, your pets, your pet food,
and get inland.
Make sure that those Cajun guys who are going to come help
are going to be doing rescue operations and not recovery operations.
Nobody wants to deal with that.
So please,
if you're in Louisiana, take this one seriously, because it is doing all of
that same creepy stuff with the changes.
I would,
I'd really pay attention to this one.
It's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}