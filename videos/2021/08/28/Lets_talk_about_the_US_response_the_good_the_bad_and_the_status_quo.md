---
title: Let's talk about the US response, the good, the bad, and the status quo....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NFXAFeuovsw) |
| Published | 2021/08/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains recent developments at the airport and the US drone response to an ISK planner.
- Emphasizes the importance of questioning whether the actions will impact operations on the ground positively, negatively, or not at all.
- Points out that the chain of events initiated in 2019 will continue regardless of recent news.
- Mentions that politically, the news may matter, but operationally on the ground, it won't have a significant effect.
- Notes that the response is unlikely to deter further incidents or change the ongoing evacuation efforts.
- Stresses that focusing on how the news impacts current events is the most critical question.

### Quotes

- "That's actually the only question. That's the only thing that matters."
- "Your stupid question may be the smartest question that gets asked."
- "When you're talking about the lives that are in harm's way right now, it doesn't mean anything."

### Oneliner

Beau explains recent developments and underscores the critical question of how they impact operations on the ground.

### Audience

Policy Analysts

### On-the-ground actions from transcript

- Stay informed about the developments in the region and their potential impact (suggested).
- Support organizations aiding in evacuation efforts (implied).

### Whats missing in summary

Detailed analysis of the broader geopolitical implications and potential future scenarios.

### Tags

#Developments #Geopolitics #Evacuation #Impact #Questioning


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about developments.
The good, the bad, the status quo, what it actually means.
We all know what happened at the airport over there.
And when it happened, President Biden came out,
we're going to hunt you down, right?
And the cat and mouse game began.
It started and they were looking for somebody to, well,
let slip the dog's voron.
And they found him.
Last night, by the time you watch this,
the US announced that it had conducted an over-the-horizon
response via drone and had likely taken out
an ISK planner.
I tweeted out the news when it broke.
And almost immediately, I got a question
in the comments and the replies.
And it says, hi, stupid question.
Is this good, bad, or status quo?
Man, for a stupid question, that's really smart.
Because if you're mission focused, if the mission's first,
that's actually the only question.
That's the only thing that matters.
Is this going to impact operations
on the ground in a positive way or in a negative way?
Or is it not going to change them?
That's what matters.
If we're not politicizing it and we're
talking about getting people out,
preservation of human life because that's
all that matters, what happens?
And the answer is nothing.
The answer is nothing.
Has no impact.
The chain of events that is underway,
it's a chain of events that was set in motion in 2019.
Nothing's going to change it.
And that doesn't change based on whether it's good news
or bad news for the party.
This in and of itself, militarily, what does it mean?
Nothing.
Let's say, hypothetically, that this is ISK's top planner
or was.
Let's say that that's who it was.
We don't know that.
But let's say it was.
What happens?
In that line of work, the chain of succession is pretty clear.
His replacement is already up and running.
They know who's next in line.
And the next person in line took that job.
Doesn't change anything.
Doesn't alter anything.
Not as far as the evacuation goes.
As far as getting people out.
And that's what matters.
Now, politically, does this matter?
Oh, yeah.
I'm sure it's going to make a lot of people feel better.
But as far as it impacting events on the ground,
it's not going to have a positive effect.
It's not going to have a negative effect.
It's not going to have one.
Time is too short.
The chain of events that was started in 2019,
the clock's almost run out on it.
Odds are anything that is going to happen
has already been set in motion.
And it's not going to be deterred
because of this response.
And it's unlikely that there will be a response
to the US response.
Not in time.
So it probably isn't going to have any effect at all,
militarily speaking, when it comes to the operation itself,
its status quo.
That is not a stupid question.
In reality, it's the only question that matters.
That's the right question to be asking.
Does this impact what's occurring?
And outside of the political sphere, it doesn't.
When you're talking about the lives
that are in harm's way right now,
it doesn't mean anything.
It's not going to deter another incident.
So sure, from a political standpoint,
from a US military prowess standpoint,
from all of this stuff, yeah, it matters.
But as far as the people trying to get out right now,
it's not going to make their life any easier or any harder.
So just remember that your stupid question
may be the smartest question that gets asked.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}