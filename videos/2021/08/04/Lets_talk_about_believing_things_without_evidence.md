---
title: Let's talk about believing things without evidence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qQGXkDxoUaw) |
| Published | 2021/08/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the harmful impact of accepting theories without evidence.
- Exploring the theory of super advanced species teaching ancient civilizations.
- Questioning the Eurocentric bias in theories about ancient civilizations.
- Criticizing the reinforcement of the idea that only European cultures were capable.
- Warning against accepting theories with scant evidence and their potential negative impact.

### Quotes

- "Those people over there, there's no way they could have done that. Brown people couldn't have done that. It had to be green people."
- "Not just is that wrong for obvious reasons, it's historically inaccurate."
- "It's probably not a good road to go down."

### Oneliner

Beau addresses the harm in accepting theories without evidence, particularly those reinforcing Eurocentric biases and downplaying the achievements of non-European civilizations.

### Audience

Critical Thinkers

### On-the-ground actions from transcript

- Question theories without evidence (implied)
- Challenge Eurocentric biases in historical interpretations (implied)

### Whats missing in summary

Full understanding of Beau's nuanced perspective on theories lacking evidence and their potential societal implications.

### Tags

#CriticalThinking #Eurocentrism #HistoricalBias #ChallengingTheories #Evidence-basedThinking


## Transcript
Well howdy there, Internet people. It's Beau again.
So today we are going to talk about something a little outside of the box.
I got a message
and it says, Beau, I really liked
the idea of wild theories holding people back in their own lives. I think it's
true,
but I think there's a foundation
for the foundation you mentioned.
There are a bunch of claims that get echoed without resistance because people
see them as harmless.
I don't think they are.
Before my dad was googling pizza places,
he was looking into aliens meeting ancient people.
It just seemed quirky so nobody said anything.
It might be really useful if you'd give your take on stuff like that every once
in a while.
I mean it makes sense.
It makes sense.
If you
start to accept
theories that don't really have a lot of evidence,
you may become more susceptible
to accepting theories that
are actually
a little more harmful.
Now, when it comes to stuff like this,
I kind of like them on one hand
because they're cool thought exercises
and it helps you think outside the box.
At the same time, I'm not so sure this one is actually harmless.
So, let's just start with the
basic premise.
The idea
is that
a super advanced species
traveled space, time, dimension, whatever,
and ran into ancient man,
stone age man.
And this super advanced species
thought it would be a good use of their time
to teach them to work with stone.
Hmm...
Doesn't make a whole lot of sense.
There's not a lot of evidence to back this stuff up. It's all interpretations
of
various things, most times outside, well outside,
of the general consensus of the experts in the field.
Doesn't necessarily mean they're wrong,
but it certainly doesn't mean you should put a lot of stock in the theory.
This one in particular, though,
the premise is shaky at best,
but there's something else that's always bothered me about it.
Where did
these visitors meet people?
The Aztecs,
Incans,
Mayans,
Egypt, right?
You know, we could go on with this list for a long time, but let's do it the short way.
Where didn't they meet people?
Europe. You know, ancient Greece didn't take place too long after
some of this stuff that gets talked about in this theory.
They had pretty amazing architecture.
Nobody suggests
that that had to have been done
by people from another planet.
It seems,
in many ways,
that it is
just another way
of reinforcing
a Eurocentric history.
This view that history revolves around Europe.
That's kind of what it seems like. There's no way those other people, those
people over there,
there's no way they would have been able to
develop this technology. There's no way they could have done this without help.
I mean, that's...
hmm...
It's like saying these civilizations, these cultures, couldn't have flourished
without assistance from Europe.
I don't like it
for that reason.
This one to me, I don't think it is
harmless. I think it
reinforces that idea.
When you run across theories
that are...
let's just say light on evidence, and they're based on interpretation,
they're based on feeling, they're based on
looking at things a certain way,
it's probably a good idea
to
take a hard look
at what preconceived notions
that theory reinforces.
Because it may be something like this.
Those people over there, there's no way they could have done that. Brown people couldn't have
done that. It had to be green people.
I don't...
I don't know that it's harmless. It does. It seems like a foundation
to that foundation.
Something that reinforces the idea
that the only culture capable of doing anything
is one that originated in Europe.
Not just
is that
wrong for obvious reasons, it's historically inaccurate.
So, I think that's probably your strongest take.
That may be one of the best ways to look at it.
Now, if you want to entertain something like this as a thought exercise,
sure, why not?
But to put stock in it
and allow the
acceptance
of this theory with very, very, very scant evidence,
um...
to go unchecked
and to allow that to lead you
to buying into other theories later with little evidence or no evidence,
it's probably not a good road to go down.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}