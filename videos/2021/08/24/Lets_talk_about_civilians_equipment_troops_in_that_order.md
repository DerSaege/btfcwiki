---
title: Let's talk about civilians, equipment, troops in that order....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=bVQKK9c4tUk) |
| Published | 2021/08/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the belief that civilians should have been evacuated first before equipment and troops, Beau argues that it doesn't make sense beyond a surface level.
- Explains the demographics of civilians in Afghanistan, including government employees, civilian contractors, and NGO workers, who had reasons to stay even after being advised to leave.
- Points out that the U.S. government lacks the ability to force American nationals abroad to return home, which is why many civilians remained in Afghanistan.
- Clarifies the misconception around U.S. equipment left in Afghanistan, distinguishing between U.S.-made and U.S.-owned equipment.
- Emphasizes that most of the Department of Defense's equipment was removed, and what was left behind belonged to the Afghan national government for their fight against opposition.
- Challenges the notion that President Trump's approach of getting civilians and equipment out first is false, as troops were withdrawn before civilians and equipment.
- States that the outcome in Afghanistan was inevitable due to the deal made and the withdrawal of troops, regardless of the timeline or actions taken.
- Acknowledges the collective guilt felt by Americans watching the situation unfold and suggests learning from such interventions for the future.

### Quotes

- "Put the penguin down. It's not really helping anything."
- "There isn't a lot that could have been done to avoid what you're seeing."
- "Understand these interventions end poorly."
- "From the moment the deal was made, it was going to be this way."
- "Even Trump's timeline didn't change the dynamics."

### Oneliner

Beau challenges the narrative of evacuating civilians first in Afghanistan, explaining the complex demographics and reasons behind the sequence of evacuations, while underscoring the inevitability of the outcome and the need to learn from such interventions.

### Audience

Policy Analysts, Advocates

### On-the-ground actions from transcript

- Learn from interventions to prevent similar outcomes (suggested)
- Understand the complex dynamics of conflicts and interventions (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of the misconceptions surrounding the evacuation sequence in Afghanistan and the underlying reasons for the situation's outcome.

### Tags

#Afghanistan #Evacuation #USGovernment #Interventions #PolicyAnalysis


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about an exit that people believe we should have used,
and it has been repeated so much it's just become a talking point.
Civilians out first, then equipment, then troops.
Anything else is ridiculous, and they should have done what the president, meaning Trump, told them to do.
And that's the right way to do it. Anything else is just wrong.
I got a message asking if I had the courage to talk about this, because nobody else will.
I will admit that I haven't heard anybody else address this, but it's not because they lack courage.
It's because it doesn't make any sense.
If you understand it beyond a surface level, this doesn't add up.
This is like walking into a mechanic shop when the AC on your car goes out with a penguin under your arm,
telling them that you brought the part.
The mechanic is going to be confused and not know what to say.
That's why people aren't responding to it.
But we'll go through it.
Okay, so we'll start with the civilians, right?
That's where people want to start. Who are they?
The civilians in Afghanistan right now, who are they?
Exchange students? Americans studying abroad?
Tourists soaking up the rays in sunny Afghanistan?
Who are they?
There's a bunch of different demographics, but the overwhelming majority fall into three groups,
three different categories.
The first being U.S. government employees, civilians,
but they're there helping the diplomatic or military efforts, and they had a reason to stay.
The second would be civilian contractors, and I'm not talking about the kind with Oakleys and rifles.
I'm talking about the type that would help keep the helicopters running,
do systems administration, that kind of stuff.
They were there to help the national government fight after the United States left.
They can't leave before the United States does.
That's the job.
Some of them probably stayed for money.
Some of them probably stayed out of some sense of honor, felt duty-bound to stay and help,
even after State Department gave them the advice to leave.
And then the third group are people working for NGOs, for non-government organizations, do-gooders,
the sort of person that would travel to Afghanistan to help.
The worse it gets, the less likely they are to leave.
That's what makes them so wonderful.
Without a doubt in my mind, if that airport falls,
some from that category will give up their seats to people they don't know.
That's why the civilians are still there.
The United States government actually doesn't have a lot of machinery
for forcing American nationals abroad to go back home.
It's not really a thing.
They can't round them up, hog time, and throw them on airplanes.
I mean, not at scale anyway.
So that's why they're still there.
State Department did send out notices talking about how bad it was.
People chose to stay.
That's why the civilians weren't taken out first.
Many of them had to stay because they were there to fill the gap
that was being created by the U.S. leaving.
They were always going to be there after the U.S. left.
Okay, next is equipment.
And I share some of the blame in this because I went back and watched the videos
where I talked about the equipment.
And I reference it offhand, but I never make this clear.
I use the word U.S. custody at times, but if you don't know what it means,
it doesn't make any sense.
I'm holding the penguin at that point.
The overwhelming majority of equipment that the United States took to Afghanistan
that it kept, that still belonged to DOD, was removed.
It did leave.
Some of it stayed behind.
Some of it always does at the end of wars.
But the overwhelming majority was removed.
When people say U.S. equipment, that means made in the USA.
That's what is meant by that saying.
It was owned by the national government.
Removing that equipment doesn't make a whole lot of sense.
That's how they were going to carry on the fight against the opposition.
Even as pessimistic as my assessments were,
I wouldn't suggest removing their only chance at victory.
We didn't know.
Even as pessimistic as my estimates were, I gave them longer than they lasted.
Removing that equipment that they needed to fight doesn't make any sense.
That's why it's still there.
U.S. made, not U.S. owned.
Most of DOD's stuff was removed.
Some of it was left, sure.
But that's not what most people are talking about right now.
That's not the footage that you're seeing.
And then troops out last.
Troops will be out last.
But the idea that President Trump would have got the civilians and the equipment out first,
that's just a lie.
That's not true because he pulled troops out before the civilians and equipment was out.
It's just made up.
What's happening is that Americans, they feel bad.
There's a collective guilt that is permeating everything as we watch what's unfolding.
So people are trying to find some way to suggest that it didn't have to be this way.
From the moment the deal was made, it was going to be this way.
The only thing that really could have changed it was a secondary force,
another country stepping in to fill the role of the United States
and help prop up the national government.
That's it.
Even Trump's timeline, pulling the troops out as early as he did,
it really didn't affect the outcome.
It didn't change the dynamics.
It was the deal.
It was the withdrawal that caused this.
This is how it was always going to look.
Now, if you are a Trump supporter, take some solace in this.
I don't know of any current politician that could have got us out of there without a mess.
Yeah, Trump made it a little bit worse than it had to be,
but at the end of the day, he just set the chain of events in motion
that at some point was going to happen.
The United States did not put enough in to getting that national government to stand on its own.
So this was going to be the outcome.
You can politicize it.
You can try to spin it.
You can come up with scenarios in which it wouldn't have happened this way,
but most of them aren't going to make any sense.
Put the penguin down.
It's not really helping anything.
There isn't a lot that could have been done to avoid what you're seeing.
It's the nature of this.
If you want to find some way to make it better, to make it less embarrassing for the United States, learn from it.
Understand these interventions end poorly.
This is what it looks like.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}