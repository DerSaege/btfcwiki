---
title: Let's talk about the feed store and public health....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dByVMsE08FA) |
| Published | 2021/08/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Small towns are experiencing people coming from larger cities to buy a certain anti-parasitic, ivermectin, commonly used in livestock, horses, and sheep.
- The feed store owner in small towns quizzes buyers about the animals the product is for and won't sell if they don't know basic answers, as they prioritize local customers over outsiders.
- Misinformation on the benefits of ivermectin for COVID-19 has led people to seek it out at feed stores based on Facebook memes and search results.
- Instead of focusing on what ivermectin does as an anti-parasitic, Beau suggests reframing the narrative to its true nature as a neurotoxin to dissuade potential consumers.
- Beau warns against the dangerous dosing differences between animals like dogs and humans when it comes to ivermectin, with severe consequences for improper usage.
- Merck, the pharmaceutical company that manufactures ivermectin, has publicly stated that there is no scientific basis or evidence to support its use as a COVID-19 treatment.
- Despite claims that ivermectin works and is being suppressed by Big Pharma, Merck's position contradicts this narrative as they express doubts about its efficacy and safety.
- Beau questions the logic of risking one's life by seeking out ivermectin when vaccines, readily available at numerous locations, provide proven protection against COVID-19.
- He stresses the importance of not falling for misinformation and desperation when safer alternatives like vaccines exist, urging people not to put their lives at risk by pursuing unproven treatments.
- Beau concludes by reminding viewers of the consequences of early dismissals of COVID-19 severity and mitigation efforts, warning of potential lack of hospital beds for those who may fall ill.

### Quotes

- "Perhaps we should stop talking about what it does, it's an anti-parasitic, and start talking about what it is, a neurotoxin."
- "Is your pride worth your life?"
- "Don't do this."

### Oneliner

Beau warns against the dangers of seeking out ivermectin as a COVID-19 treatment, stressing the importance of accurate information and available alternatives to protect lives.

### Audience

Health-conscious individuals

### On-the-ground actions from transcript

- Refrain from seeking out and using ivermectin as a COVID-19 treatment (implied)
- Prioritize getting vaccinated against COVID-19 for protection (implied)
- Educate others on the risks and lack of scientific evidence supporting ivermectin as a COVID-19 treatment (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the misinformation surrounding ivermectin as a COVID-19 treatment and the potentially fatal consequences of its misuse.

### Tags

#COVID-19 #Ivermectin #Misinformation #Vaccination #PublicHealth


## Transcript
Well howdy there internet people, it's Beau again.
I want to start off by saying that even at the height of 2020, as strange as that year
was, I never expected to be making a video quite like this one.
If you don't know, we've got a couple horses.
As such, I have been to the feed store once or twice in my life.
The local feed store, where I live, a couple thousand people, the owner has started quizzing
people that he doesn't know, that come in to buy a certain product, asking them about
the animal they say that it's for.
And if they don't know basic answers as far as the animal's anatomy or whatever, sorry,
he just ran out.
He just ran out, he's doing this because people are coming from larger cities to small towns
to try to get a certain anti-parasitic.
Understand you cannot come to a town of a couple thousand people and just try to blend
in.
The feed store owner in these small towns, they know the name of every horse in the county
and they don't know you.
They're probably not going to sell it to you, save your gas, save your time.
They're not going to sell it because the animals may need it.
They are more interested in keeping their actual customers happy and stocked than they
are in selling you snake oil.
If you are completely out of the loop on this, there is a group of people who believe that
ivermectin, an anti-parasitic that is commonly used in livestock, horses, and sheep, there's
probably a joke there, is a cure, is a therapeutic for our current public health issue.
And they're going to feed stores to get this and buy it.
This is a bad idea, obviously.
The idea originated from information that came out early on from some studies that associated
certain benefits and stuff like that.
These studies have been called into question for a bunch of reasons, which we will get
to, but that doesn't stop the Facebook memes.
People see the meme on Facebook and it says, hey, ivermectin does this.
So they hop over to Google and they type in ivermectin cures COVID.
And because that's what they typed in, the search results reflect that.
And that's where we're messing up.
Those of us who have been joking about this or have been trying to counter message or
whatever, we keep calling it an anti-parasitic that we use on our horses, on our livestock,
whatever.
Perhaps we should stop talking about what it does, it's an anti-parasitic, and start
talking about what it is, a neurotoxin.
Those people who are considering this, that idea of, oh, well, I'm not going to take the
vaccine because I don't know what's in it.
That's gone.
You can't say that anymore.
Because I can't imagine what could possibly be in the vaccine that would stop you because
you're drinking a neurotoxin.
It's worth, it is worth stating that there are times when ivermectin is used in people
in incredibly small and specific doses.
We are not going to get into the dosing for humans.
I'm not going to do that.
I don't think that's a good idea.
But to demonstrate a point, I am going to talk about the dosing for dogs.
Because for most people, that's going to be how you interact with it.
More than likely, the heartworm medication you give your dog has ivermectin in it.
I want to say the top end of the dosing for dogs is 0.024 milligrams per kilogram.
Pretty small amount.
Pretty small amount.
And that's what's okay for dogs.
Except for herding breeds, you have to be really careful.
Like Lassie, all right?
Sometimes at 0.1 milligram per kilogram, toxicity becomes apparent.
That is a pretty small window.
You're talking about 0.076 of a milligram.
That's the difference between this is okay and what's that, Lassie?
Timmy fell down a well, but you can't lead us to him because you're blind and having
seizures.
It is important to note that for animals, dogs, the dosing is in milligrams.
For people, it's in micrograms.
There are a thousand micrograms per milligram.
Perhaps this isn't something you should be eyeballing using the little cup that came
on top of your cold medicine.
Across the United States, there have been hundreds of calls to poison control.
There have been people in the hospital over this.
Don't do this.
Don't do this.
The other argument here that is coming from the people who are desperate to believe that
this is some kind of cure-all is that it does work, but see, the media is just doing their
part to make sure that people only get what's available from Big Pharma.
They don't want them using this other thing that's available at the vet store.
Big Pharma, right?
I've got some bad news about who makes ivermectin.
It's Merck.
It doesn't get more Big Pharma than Merck.
It is worth noting that Merck, the company that stands to make a dump truck full of cash
if this did work, put out a statement saying that there is no scientific basis, there is
no meaningful evidence to suggest that it works, and that they have questions about the safety.
The company that would make money on this is saying don't do it because they don't believe it'll work.
I get it on some level.
I do.
I understand it.
There are people who, very early on, before they had enough information to make an informed
judgment on this, they went ahead and bought into it's not a big deal.
It's just the flu.
I don't need to worry about it.
And now things are getting real, right?
Hospitals are filling up.
So they are desperately looking for something to provide some level of protection without
them having to back down from those statements.
Is your pride worth your life?
If you drove from any major city out to where I am to try to do this, to try to buy this
stuff, you passed 15 vaccination locations on the way.
There are alternatives.
There are things that actually offer protection.
Don't do this.
Don't do this.
The company that stands to make money from this is saying that they don't think it'll
work.
They're recommending against it and they would make the money.
It's the United States.
They're not going to pass up money like that.
It's worth remembering that because there were a whole bunch of people who took that
stance early on, who refused mitigation, who refused to do their part in this.
If you get sick from this, there may not be a bed at the hospital for you.
Don't do this.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}