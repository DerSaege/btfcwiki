---
title: Let's talk about learning from history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=G4r7ORDMOQc) |
| Published | 2021/08/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Emphasizes the importance of learning from past events to prevent repeat occurrences.
- Points out a common theme in society where history repeats itself due to a lack of learning.
- Suggests that society often fails to publicize the lessons of history effectively.
- Reads an excerpt detailing how officials mishandled a public health issue in the past.
- Expresses frustration at officials downplaying the severity of the disease for personal gain.
- Notes that the workshop revealing these mistakes occurred in 2006, yet similar patterns are repeating now.
- Criticizes the tendency in the U.S. to avoid acknowledging and discussing the country's failings.
- Questions whether knowledge of past mistakes could have better prepared people for the current situation.
- Shares a link to the workshop's information for further reading.
- Draws parallels between past mishandlings and the current situation with COVID-19.

### Quotes

- "The one thing we learn from history is that we don't learn from history."
- "We just refused to learn from it."
- "We don't want real American history taught."
- "It's worth reading. It's definitely worth taking a look at."
- "They'll never catch on. Commoners."

### Oneliner

Beau stresses the importance of learning from history to avoid repeating past mistakes, pointing out how the mishandling of a public health issue in 2006 mirrors current events.

### Audience

Critical thinkers, history enthusiasts.

### On-the-ground actions from transcript

- Read the workshop information linked by Beau (suggested).
- Analyze past mistakes to better understand current events (implied).

### Whats missing in summary

Insights on the dangers of repeating history's mistakes and the significance of acknowledging and learning from past failures.

### Tags

#LearningFromHistory #PublicHealth #GovernmentAccountability #PandemicResponse #USHistory


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about a common theme on this channel,
something I talk about a lot and I try to do.
It's the idea that, you know, something occurs
and we don't want it to happen again, so we come together,
we talk about it, we learn about it, and we find a way to make sure it doesn't happen again.
We look at events and try to learn from them.
While that's a common theme, I'm also very fond of the phrase,
the one thing we learn from history is that we don't learn from history.
So how do I reconcile those two?
My theory is that these events,
the reason we don't learn from them as a society is because when they're hot-washed,
when they're looked at in hindsight, when changes can be made,
we don't look at them as a society because it's no longer news.
It's just people in that specific field.
And then when the people in that specific field say,
hey, this is what we need to do to avoid what happened last time, nobody listens to them.
It's not that we can't learn from history.
It's that oftentimes the lessons of history, they're not really publicized.
So with that in mind, I'm going to read you something.
It's from a hot wash of this little public health issue that you might have heard about.
National, local, and state officials all operated in the same way.
At best, they communicated half-truths or even outright lies.
As terrifying as the disease was, the officials made it more terrifying by making little of it.
And they often underplayed it.
Local officials said things like, if normal precautions are taken, there is nothing to fear.
But then they would close all businesses.
Worry kills more people than the disease itself, a Chicago public health official was quoted as saying.
And the other quotes were, don't get scared.
And this is nothing more or less than an old-fashioned flu.
Communication was rarely honest because honesty would hurt morale.
In Philadelphia, after a public health official finally closed all public gatherings and public funerals,
the newspaper said, this is not a public health measure.
There was a lot of cognitive dissonance.
Man, when you read about how it was handled, it's kind of infuriating, right?
That officials would do that.
That they would downplay it knowingly.
That they would distribute half-truths or outright lies.
All in the name of preserving their position.
And to write off the people of this country for their own gain.
And as the numbers continue to climb, we need to remember this.
Because it becomes even more infuriating when you realize this is from
Pandemic Influenza Past, Present, Future workshop from October 17, 2006.
We didn't learn the lessons.
We didn't learn the lessons from last time.
We showed very clearly that we do not learn from history.
This workshop was by the CDC Health and Human Services.
The problem is we don't talk about this.
Because in the United States, we don't like to talk about our failings as a country.
We pretend they don't happen.
So those in power, those who made the mistakes,
their names, they never get tied to those failings.
Because we just want to wave the red, white, and blue and keep moving.
If this was common knowledge, if people knew how this is,
how public officials behaved during the last major pandemic,
do you think they would have been a little bit more on guard?
Maybe a little? Would have helped some, right?
I'm going to have the link to this down below.
The parallels are pretty pronounced.
As the numbers continue to climb, and as many governors continue to downplay
or spread half-truths or outright lies,
it's important to remember this lesson was already taught.
It already happened once.
We just refused to learn from it.
Because we don't want real American history taught.
We don't want to talk about our failings.
We don't want to dissect things after they happen
and go through and say this is what went wrong and this is where.
If you read this, you're going to come to find out that it was a propaganda machine
that really helped it spread last time.
It's pretty similar.
I mean, you would be forgiven for thinking that a lot of politicians saw this
and were like, they'll never catch on.
Commoners.
I think it's worth reading. It's definitely worth taking a look at.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}