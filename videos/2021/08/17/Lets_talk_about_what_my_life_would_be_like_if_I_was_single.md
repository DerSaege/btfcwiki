---
title: Let's talk about what my life would be like if I was single....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oRx1TGa5UC8) |
| Published | 2021/08/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of discussing what his life might be like if he were single.
- Mentions being asked a question recently about his life if he were single.
- Talks about Panta Petrovic, a man living in southern Serbia in a cave for twenty years.
- Describes Panta's life in the cave, living off the land with basic furnishings and some animals.
- Notes Panta's occasional trips to town for dumpster diving.
- Compares Panta's presence near the town to that of Bigfoot.
- Speculates on the positive introspection that twenty years of solitude might bring.
- Shares Panta's recent emergence from the cave and his response to the public health situation.
- Commends Panta for getting vaccinated and urging others to do the same, setting an example for leadership.
- Echoes Panta's message of encouraging every citizen to get vaccinated.

### Quotes

- "Every citizen should get vaccinated."
- "A better example, better leadership than many governors in the United States."
- "Twenty years he's lived up there like that."

### Oneliner

Beau introduces the unique life of Panta Petrovic, a man living in a cave for twenty years who sets an example by getting vaccinated and urging others to do the same.

### Audience

Community members

### On-the-ground actions from transcript

- Get vaccinated (exemplified)
- Encourage others to get vaccinated (exemplified)

### Whats missing in summary

The full transcript provides more details on Panta Petrovic's solitary life in a cave and his actions during the public health situation.

### Tags

#Vaccination #CommunityLeadership #PublicHealth #Isolation #Serbia


## Transcript
Well howdy there internet people, it's Beau again.
So today, we're going to talk about what my life would be like if I was single.
You know,
every once in a while you get asked a question,
and you have no idea how to respond.
And somebody asked me that recently.
I'm like, I don't know.
Live in a log cabin by myself and never see anybody.
I now know exactly what it would be like,
because I have found a guide.
His name is Panta Petrovic.
He lives in southern Serbia.
He literally lives in a cave.
He's got some basic furnishings in it,
but he lives in a cave.
He's been up there for like twenty years by himself.
Not entirely by himself, because he's got some dogs and chicken.
He's got a wild boar
that he's raised from a piglet.
And he basically lives off the land.
You know, he
fishes in a creek nearby, stuff like that. Every once in a while,
he will
kind of sneak into town and go like dumpster diving.
But generally lives a quiet, peaceful life by himself. Kind of a shadow to the
town that's nearby. Everybody knows he's up there, but they never really see him.
He's kind of like Bigfoot.
Twenty years
he's lived up there like that.
It's amazing.
I would imagine that much time
would definitely lead to a lot of positive introspection.
But he comes out of his cave
recently,
and he finds out like there's this whole public health thing going on.
And do you know what he said and what he did?
He got vaccinated
and made a public statement urging every citizen to get vaccinated,
saying that he wants to get
both doses and the booster
because eventually
it would come to his cave too.
Pantopetrovic.
A better example,
better leadership
than many governors in the United States,
than many politicians, people who
theoretically
should
be encouraging you to take
measures to protect yourself.
He led by example.
So
I just kind of wanted to repeat that message.
Every citizen
should get vaccinated.
He encourages every citizen to get vaccinated.
Anyway,
thanks Mr. Petrovic.
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}