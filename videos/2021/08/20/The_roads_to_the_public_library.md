---
title: The roads to the public library....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2cvi7KHXqqI) |
| Published | 2021/08/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses concerns about books being found in dumpsters and invites Allison Macrina, a librarian, to explain the process of removing books from libraries.
- Allison explains that libraries have to make decisions about older books that are no longer in use through a process called "weeding."
- Libraries try to find new homes for discarded books through book sales, donations, or free distribution, but some books end up in dumpsters.
- Allison clarifies that the removal of books is not censorship but a matter of space and keeping collections current to meet community needs.
- She mentions the importance of libraries in providing free access to information, technology education, job skills training, and community programs.
- Allison introduces the Library Freedom Project, a community that advocates for privacy protection in libraries and fights against surveillance.
- She explains the historical connection between libraries and privacy advocacy, dating back to resisting anti-communist inquiries and post-9/11 privacy violations.
- Allison describes the online crash courses offered by the Library Freedom Project to teach library workers about privacy protection strategies.
- She addresses concerns about censorship in the publishing industry and the impact of media consolidation on the diversity of opinions available in libraries.
- Allison advocates for better funding for libraries to continue providing valuable services to diverse communities.

### Quotes

- "Libraries are immensely important. In fact, they are more popular than they ever have been."
- "Privacy is one of the core values of the library professions."
- "We want our collections to be current. We want them to reflect our community's needs."
- "Libraries need to be significantly better funded."
- "Libraries are one of the only things in society that are 100% free and not means tested."

### Oneliner

Beau and Allison dive into the importance of libraries, addressing concerns about discarded books, advocating for privacy protection, and calling for better funding to support vital community resources.

### Audience

Library supporters, privacy advocates

### On-the-ground actions from transcript

- Support your local library by visiting, borrowing books, attending programs, and advocating for increased funding (exemplified)
- Educate yourself on privacy rights and surveillance issues and support initiatives like the Library Freedom Project (exemplified)

### Whats missing in summary

Explanation of how the Library Freedom Project's crash courses empower librarians to protect privacy and advocate for community rights.

### Tags

#Libraries #PrivacyProtection #CommunitySupport #Funding #Censorship #Surveillance #Education #Activism


## Transcript
Well, howdy there, internet people.
It's Bo again.
And today we're gonna be talking about the roads
to your public library,
because a whole bunch of you sent me an image
of a bunch of books in a dumpster.
And there were a lot of concerns.
So I have reached out to somebody
who will be able to answer them for us.
Okay, you want to introduce yourself
and take it from there?
Sure.
Hey, everybody.
My name is Allison Macrina.
I use the pronouns she and her.
I live in Philadelphia, Pennsylvania,
and I am a librarian and a privacy activist.
And I run a small organization
called Library Freedom Project.
All right.
Okay, so now that we have established
you are somebody who will be able to answer this question,
why are there dumpsters full of books?
Why is that?
Well, when you have a library, books get purchased.
They come in, you process them, you catalog them,
you put them in the library catalog,
and then you got to put them on the shelf.
But the problem is there's already books on the shelf.
So you have to make some decisions every so often
about which books get to stay and which have to go.
And so the older books, the ones that have been
through a lot of wear and tear,
the ones that maybe don't get checked out ever,
those, there's a whole process of removing them
from the library shelf.
It's called weeding.
Now, the first step is usually,
most libraries have a friends of the library book sale.
Right when you come in the library,
there's some books for sale for like a dollar,
or they have maybe a bigger sale,
like once a month or once a quarter or whatever.
That's like a whole first sort of filtering system
for these discarded books.
The next filter is like, donation,
totally free, maybe filling up
little free libraries or whatever.
And then the next filter is,
there's still always gonna be a bunch of books
that people don't need or want that are older editions,
that are like old textbooks,
that are out of date series and things like that.
And they have served their purpose,
and now they need to go to book heaven.
And unfortunately, the boat to book heaven
is a large dumpster.
Fair enough.
So these images that are floating around
that people are seeing,
this isn't some massive censorship effort
to get rid of Huxley's books or anything.
No, I mean, the thing is librarians like books,
and we don't like, we tend to strongly oppose it
in almost all of its forms.
And so it's not us saying that we think books are bad,
or that we want to hurt them or burn them
or anything like that.
It's simply a matter of space.
I think something that people don't really understand
about libraries is that libraries are not archives.
We don't collect things for historical value.
We wouldn't even have the space to do it if we wanted to.
Archives do that, but archives for the most part
have some fairly, if not rigorous,
at least standard way of deciding
what things have a historical merit
and what kind of stuff that's old
should still be kept for whatever reason.
And it's not really the case for libraries.
We want our collections to be current.
We want them to reflect our community's needs.
And I don't know of any community
that needs a bunch of musty, dusty, old, damaged books.
Right.
And the reason that these don't get donated
is because you can't really find a home for them?
Yeah, exactly.
I mean, when you consider the volume of books
that we're talking about,
I mean, even libraries with very small book budgets
are getting new materials all the time.
And if you go into your library,
any robust, well-functioning library
is gonna have already pretty full shelves.
Usually we try to leave, like, it's not a standard thing,
but we try to leave some space
at the end of each shelf, right?
We want a little bit of room to add more things,
not too much room,
because we want lots and lots of books there.
But if you get new books, the old ones have to go.
And also, if you consider the way that libraries function,
we're circulating these books all the time,
and so people are using them.
We want people to use them.
We want people to enjoy them.
But think about the way that people use books.
Have you ever dropped a cup of coffee on a library book,
or you were reading in the bathtub
and you dropped it in the water,
or you just got it kind of nasty in some other way?
Books come back to us all the time in these conditions,
and sometimes we just have to throw them out.
A lot of them get replaced.
Some of them don't.
They've served their purpose.
So it's really just a matter of, like,
there are a lot of books out there in the world.
There are a lot of books getting published all the time,
a lot of new materials coming in.
We can only donate so many.
And actually, book donation is pretty laborious.
Like, it takes a whole team of volunteers
to do all the sorting and get the book sale going.
And even if you just put books out for free,
some of them wind up in the trash anyway.
So it's a lot of effort to give them all a good home.
It's not possible with all of them.
All right.
Makes sense.
So today, with as much access as people have to the internet,
how important are libraries still?
Libraries are immensely important.
In fact, they are more popular than they ever have been.
And the popularity index for libraries has been,
it rose pretty significantly around the time
of the 2008 financial collapse, you know, the economic crisis,
because lots of people who lost their jobs
or lost different kinds of income had a greater need
to use the library for all different kinds of reasons.
So some of these people, because they can't afford internet
at home or can't afford a computer anymore
and need to use the library's computers for job searching
or whatever, or they came to the library
to try to learn computer skills because they had lost,
you know, they had been in whatever job
for however many years, and they lacked essential computer
skills that they didn't learn on that job,
or they just lost whatever discretionary funds they would
use to buy books.
There are all kinds of reasons that people started using
libraries in increasing numbers around then,
and it's held pretty steady since.
One thing that has declined significantly is the budgets
of libraries, but the budgets have gone
down while the interest and usership has gone up.
And so, you know, even with the internet,
you mentioned people doing all kinds of research
and finding information that they need online.
While that is certainly true, it's also true that the majority
of books that are published are not easily found
on the internet, at least not for free.
And also, people still like to read things in print.
You know, that's something we kind of learned
after e-books became popular a number of years ago.
Their popularity kind of plateaued.
Like, there's something about books
that people still really like and treasure.
And libraries offer an opportunity to try out books
that you maybe don't, you know, wouldn't want to purchase.
You don't want to learn a new skill or something, or,
you know, children's books because they go through them
so quickly or for whatever reason.
The other thing is that libraries offer a lot
of other kinds of programs
that are really essential in our communities.
And so, you know, we offer different kinds
of technology education.
We offer job skills training.
We do a lot of programs with immigrants and people
who are English language learners.
And so, you know, when you think about the library
as a community space where you can seek out information
if you are looking for information,
but you can also do, you know, social educational activities.
You can also use a computer.
You can browse a magazine.
Or you can just be.
There really isn't any other space in our society
that exists in this same way.
And I think given the way
that public spaces have been increasingly privatized
over the last 30 years or so and how, you know,
you can't easily even go to like, you know,
certain people can't go to the like mall or whatever
because if you stay there too long,
you're considered to be loitering
because if you're not a consumer,
you're not like spending money on things,
your presence is less welcome.
The library is a place where you can just go and be
and it's free for everyone.
And it doesn't matter if you're a citizen.
It doesn't matter if you live in that community.
It doesn't matter if you are homeless.
You are welcome to be there.
Nice. Nice.
Okay. So tell us about your other project.
Yeah. So I run this organization
called Library Freedom Project.
And what we are is a community of practice
for library workers.
So librarians and other folks who work in libraries
in different capacities.
We are working together as a community fighting
against surveillance in our communities
and fighting to protect and advocate for privacy.
Now you might be wondering like,
what is the relationship between libraries and privacy?
Why is this a thing that librarians care about?
Well, the reason is a few things.
Number one, privacy is one of the core values
of the library professions.
We actually have a code of ethics
that was created in our profession many decades ago.
And there are a number of different things codified
in our ethical mandate of like who we are and why we exist.
And intellectual freedom is a piece of that.
But privacy is a really big piece of it
because if you don't have privacy,
you can't really read, write, research freely.
Our free speech rights are really dependent
on our privacy rights.
If we feel like we're being watched,
then we're gonna self-censor,
we're gonna behave differently.
And so libraries already have this fundamental value.
The other part of it is that we also have a history
of fighting for privacy and protecting it.
And so, this goes back to the 1950s
of librarians resisting the Red Scare
and anti-communist inquiry
into our patrons reading records,
all the way through right after September 11th
when the USA Patriot Act was passed,
librarians were some of the only people
who really vocally resisted that legislation,
which was really remarkable because if you were an adult
during that time, if you remember what it was like
after 9-11, it was not a period
where any kind of dissent was tolerated.
It was very much like you're with us or you're against us.
And so librarians as a group really rejected
that way of thinking and said,
this is a bill that will really encroach
on people's privacy rights.
And so we have this long history of it.
And so what is different now?
And the reason that I started my project
is because privacy and violations of privacy
and surveillance have become totally different
at the end of the 21st century,
both because of computing power and how cheap and easy it is
to collect and store enormous amounts of data,
how lucrative that is to both industry,
for consumer technologies and like big tech,
but also because of how much police have been increasing
their technical ability and their surveillance capabilities.
So what this means for librarians is that our communities
are really affected by the loss of privacy.
The internet has impacted their loss of privacy
and the people who use the library the most,
and I'm talking about immigrants,
I'm talking about poor and working class people,
I'm talking about people who have been formerly incarcerated,
or people who've experienced homelessness.
These are all people who are much more at risk
from the loss of privacy,
who are much more targeted by different surveillance systems.
And they're also people who are much less likely
to know what to do to help themselves.
And so Library Freedom Project,
we're a community of library workers
who are learning about these different systems together
and learning about different mitigation strategies
that can help our communities, both in the micro level,
like helping people with better passwords,
the macro level, like advocating for bans
to facial recognition
and other invasive surveillance technologies.
Right. And your organization has some
pretty high level endorsements here from some names
that I've heard of.
There's this Snowden guy.
Yeah, there's Snowden guy.
Yeah, you know, we've been really fortunate that ever since,
I founded LFP in about,
it was like officially official in 2015,
but I started working on it really because of Snowden.
I started, you know, when the Snowden revelations came out
in the summer of 2013, I was really obsessed with the story.
And I started thinking about the way that it related
to my work as a librarian
and the way that my community members,
the way my patrons were interacting with that story.
They really were paying attention to it
and they were thinking about their own personal lives.
You know, the NSA is sort of this big abstract adversary,
but what we also learned from Snowden
is not just about NSA level stuff.
We learned about local police. We learned about Google.
We learned about all these ways that the systems
of surveillance are really arithmetic.
You know, they're all connected to each other.
And so, yeah, so what, you know, when I got started,
I think a lot of people in the hacker world
and privacy advocacy world,
and just in the kind of like civil liberty space
took notice of what I was doing
because it's an idea that really makes sense.
You have these folks who are public servants,
you know, library workers who really engage
with members of our community in a totally different way
than most other areas of society, right?
Like librarians are really welcoming
for the most part, right?
We want people to be in our buildings.
We want to help them and people are there
mostly to learn how to do things.
And technology is a really big part of it.
And we have this ethic of privacy.
And so people saw that it was a good idea.
And yeah, some of them gave us endorsements.
Shout out to Ed.
Hope you're doing good in Russia with your baby.
And your project, it has these crash courses, right?
Where you guys are, are y'all teaching online
or is this something where you'll go
and show up in person?
So this is just online because, you know,
obviously we live in hell and we don't get to,
where it's gonna be some time before we can do conferences
and things like that again.
But essentially what the crash courses are,
they're training programs for library workers
who want to learn about these skills together in a group.
Something that we really believe in
at Library Freedom Project is collective power.
And so our community,
the way that we've developed this community of practice,
which is about, it's a little more than 100 librarians
around the US and a couple in Canada and one in Mexico.
These are all people who've done different LFP
training programs.
And so the crash courses are one of those.
It's a shorter version.
It's a totally online version.
But the idea is,
we've got a group and it's about 30 participants or so,
and they're from libraries all over the country
and different experiences.
Some of them are a lot of public libraries,
but also academic libraries, folks in big cities,
in rural areas, in the South, in the Pacific Northwest.
And they all bring their own context
and what their community members are concerned about
what's happening with them locally.
And local politics really inform a great deal of this.
I mean, thinking about like last year, for example,
folks who were in cities where there was a lot of violence
there was a lot of BLM uprising activity happening.
There was also a lot of police surveillance
happening in those places.
And so that was a particular concern and need
in communities like Minneapolis.
Obviously now I can't think of other ones
where this was significant, but you get the idea, right?
So everybody brings their own context
and then we learn together.
And then after these training programs,
the folks who have participated in them
can do a few different things.
The first most obvious thing is they can bring
what they've learned back to their library communities.
And so they can teach classes to their patrons about privacy.
They can teach, they can do trainings
for their fellow staff.
A lot of them do conference talks,
obviously it's online now.
They become kind of, for lack of a better term,
they become like privacy thought leaders
in their library and in their region.
And if they're really super into the project
and they wanna keep going with it,
then they're invited to become part
of this ongoing community of practice
with these dozens of other library workers
that are part of LFP.
And then they can continue collaborating together.
They work on all different kinds of projects.
They ask each other questions,
they give each other support and advice.
And we are building that community all the time
and inviting more people into it
and strengthening our relationship with each other
and doing really big things
to protect privacy in our community.
That is wonderful.
Yeah.
I'm a huge proponent of community networks
and people have asked,
well, what about doing it on a professional level?
For those that are asking, this is how it's done,
just so you know.
So back to the original topics
and what brought all of this up.
And just so everybody knows,
when the photos came in,
I just reached out over Twitter.
This really wasn't planned to mesh as well as it is.
So when it comes to censorship,
well, we've talked about how weeding isn't that,
but what do people need to look out for?
Well, I think that when I think about censorship,
I'm thinking about much bigger issues
than just individual librarians
making decisions about what goes on the shelf or not.
Because we live in an environment right now
of such enormous media consolidation.
There's six now, I think five,
really big, powerful publishers, what Penguin Random House
bought Simon & Schuster,
and now who knows what they're called.
And there's only a handful of others
that are as powerful in that space.
The same is true in the academic publishing market.
And then not to mention that the biggest deciders
of free speech and censorship are the media.
Of free speech and censorship are the big tech companies,
Facebook, Google, Apple,
and there are different subsidiaries,
YouTube, Instagram, et cetera.
And so I'm concerned about censorship,
but I'm concerned more about the way that the acceptable,
there's sort of the narrowing of acceptable opinion,
and it all comes from these very consolidated companies
that are beholden to their shareholders only.
And so what does that mean for what gets published or not?
Well, one big thing that I see a lot
is how much really right-wing, sensational,
often hateful media is getting published.
I think in particular about,
now I can't think of the name of the book,
but that Abigail Schreier book
that's like the transgender craze
that's killing your daughters or something.
And this was a book that's like full of plagiarism
and total like conjecture and like bad research
and all this, but because it is so sensational
and it's making money for these big publishers,
that's the kind of thing that gets published.
So I realized I'm like off on a little bit of a tangent
to your original question,
but those are the issues that I'm really worried about
because it means that like what actually makes its way
to us in the library is not a diversity of opinion at all.
It's like the same,
the New York Times bestseller list
that's like five different books by James Patterson.
And then like a bunch of nonfiction
from these right-wing pundits and like not a whole lot else.
Right, and the thing is with the market, the way it is,
it even those who want to push more,
let's just say divergent viewpoints,
they end up self-censoring so they can stay marketable,
so they can at least get some of that idea out.
So your main concern seems to be more of a diluting
of a wide set of viewpoints
that eventually find their way to your library.
So it's not really this conspiratorial thing,
it's just capitalism doing what capitalism does
and trying to find the most marketable thing.
Yeah, actually it's exactly that.
And Noam Chomsky wrote about this like 30 years ago,
I forget which book it's in,
I think it's in Manufacturing Consent,
but it's an essay called Propaganda Model.
And he talks about all these different filters
that exist like to kind of define the acceptable range
of like public opinion.
And so one of them is big business,
which is what we're talking about,
but then there's also advertising.
Advertisers make a lot of the decisions
about what is acceptable to publish or acceptable speech
because they're the ones who are paying the bill.
So yeah, it's a bunch of different forces under capitalism
and I really blame a lot of that
for the proliferation of misinfo
because people rightly start to distrust like big tech
and big media and all these companies.
And then they go looking for alternative sources
and those alternative sources are not credible,
but they seem like an alternative,
they seem like something different,
they seem like non-mainstream.
And I understand the desire to find things like that.
Yeah, no, it definitely makes sense.
I think most people at some point go through that phase
where they're like, there has to be something else out there
and then the next thing you know, they're reading something
and it probably isn't the best thing for them.
And it's probably not accurate information.
So I'm just gonna throw this out there.
What do you wanna say?
Because this conversation was far more interesting
than I actually expected it to be.
I love to explode the boring librarian stereotype.
So I'm happy to have done that.
Well, what I wanna say is a few things.
So I think, since we came to be talking about this
because we were talking about the whole books in a dumpster debacle,
I think the reason that happened is a couple of things.
One, people love to be outraged on the internet
and like, let's just give them that, right?
That person who posted that thing,
it probably made their whole day to be angry about that.
And I wish them peace and quiet.
But the other thing is that people really don't understand
how libraries work.
And so I wanna just say a few things about our value
and why people should visit their library
and why they should protect us.
We are one of the only things in society that are...
In American society that are 100% free and not means tested.
And for folks who don't know what I mean by that,
when I say means tested, it means it's not just for poor people,
it's for everybody.
When social services are means tested and they're just for the poor,
that means they're gonna end up being really shitty
because our society doesn't care about poor people.
And so if things are just for poor people,
they end up like crappy or like underfunded or whatever.
Libraries being for everyone,
everyone who lives in a community pays into it
in some form with their taxes and everyone gets to benefit.
Now, unfortunately, we still manage to live in a society
where that kind of thing is really, really undervalued.
And you couple that with the fact that all social programs,
all social services in the US have been systematically defunded
for the last 30 or 40 years.
So where have those responsibilities gone
as like social workers don't exist as much anymore
as like the USPS is less and less funded, like all these things?
It means that libraries have assumed the burden of a lot of that lack.
And so lots of people end up using the library
who need some other form of care or like something else, right?
Which is the reason why homeless people end up camping out in the library
all day because there isn't anywhere else for them to go.
So I say all this to say that libraries need to be significantly better funded.
And if we have better funding, we can do a lot more of the amazing work
that we do now, providing technology help to people,
doing English language learning, doing children's story times
and other children's programming, doing all kinds of fun stuff
for people of all ages, and offering really great,
high-quality collections of books that you want to read
and you want to check out for free and then return
and not have to pay anything because libraries are amazing.
So I just invite people to check out their local library
and actually support it and recognize what a valuable thing it is in society.
And yeah, I think that is what I wanted to say.
Wonderful.
OK, plug your stuff.
Tell them where they can find you and your project.
We are Library Freedom Project.
We are on the web at libraryfreedom.org.
You can also follow us on Twitter at Library Freedom.
You can also follow me on Twitter at Flex Libris,
and I tweet about the project a lot.
And actually, very soon, I'm going to be redesigning our website a little bit
for a better directory of our members.
So if you are looking for privacy training
or you want to learn about our privacy advocacy work or anything like that,
we have people in every part of the US.
And so you can get in touch with us and talk to one of those people
and have them help you directly.
But in the meantime, yeah, check out our website.
You can get in touch with us there.
All right, sounds good.
OK, everybody, so that's going to be the show for today.
Y'all have a good day, and go check out your local library.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}