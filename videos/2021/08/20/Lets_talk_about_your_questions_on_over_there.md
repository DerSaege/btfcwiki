---
title: Let's talk about your questions on over there....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KUMomGe4fjY) |
| Published | 2021/08/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the confusion caused by major media outlets and pundits inserting their own narratives into the situation in Afghanistan.
- Clarifies the equipment left behind in Afghanistan, mentioning that the opposition, now the de facto government, operates differently from how the U.S. military does.
- Addresses concerns about the use of equipment against civilians, explaining how it doesn't significantly change the dynamics of the situation.
- Touches on the issue of biometrics and the potential challenges faced by those trying to leave the country.
- Comments on the Vice President's role in potentially resisting the opposition.
- Talks about the continued presence of the CIA and potential treatment of women by the opposition.
- Mentions the evacuation process and the impact of base closures on it.
- Considers the possibility of equipment left behind being compromised.
- Examines the challenges faced during the withdrawal from Afghanistan and the preparedness of the Biden administration.
- Concludes with thoughts on the Afghan population, refugees, and lessons to be learned from the situation.

### Quotes

- "A lot of times they don't. And in this case, you didn't really have any objective good guys. It just so happens that the objective bad guy won."
- "It's everybody. Everybody who had a hand in this has some of the blame."
- "This is what it looks like. You know for a long time a lot of the news that came out of there was very sanitized. What you're seeing now is reality. And it's always the reality."

### Oneliner

Beau clarifies misconceptions about Afghanistan, equipment left behind, and the challenges faced during the withdrawal, urging for a more nuanced understanding of the situation.

### Audience

Global citizens concerned about Afghanistan.

### On-the-ground actions from transcript

- Study and understand the events in politically bankrupt nations to develop sustainable solutions (implied).
- Advocate for political processes over military intervention in such situations (implied).

### Whats missing in summary

Insights on the potential long-term repercussions of the Afghanistan situation and implications for global politics.

### Tags

#Afghanistan #USMilitary #Withdrawal #Reality #PoliticalSolutions


## Transcript
Well, howdy there internet people, it's Beau again.
So today we're going to talk about your questions
about over there.
I have a giant list of questions we are going to go through
that came from y'all.
In this conversation, we're gonna talk about what is.
We're gonna talk about reality without spin.
We're not gonna interject a lot of ideology into this.
The reason many of these questions exist
is because major media outlets and a lot of pundits
are inserting what they wish into the narratives.
So the narratives are confusing, generating questions.
For the sake of continuity during this,
for those who watch these videos later,
I will refer to the side that the United States
has been helping for 20 years as the national government,
the side the US was opposing for 20 years as the opposition.
It is worth noting at this point,
the opposition is the de facto government.
Okay, so let's get started.
These questions are in no particular order.
What about selling the equipment to fund their evil ambitions?
Yesterday, I put out a video talking about the equipment that was left behind.
Some of it was US equipment, some of it was equipment the US had provided to the national
government, and now it's in the hands of the opposition.
There have been reports of the equipment being sold on, quote, the black market.
First, that's not the black market.
That's just the market.
It's Afghanistan, not the United States.
Second, that's normal.
Their military is not structured like ours.
Local commanders kind of have a spoils system.
It's going to be sold and they'll pocket the cash.
Is this going to be used to fund them?
Not really.
Again, the opposition is now the de facto government.
If they sold everything that the U.S. put into that country, it doesn't come close
to what they're going to get from lithium alone.
They're the government now.
So it's not going to change the dynamics.
What about using the equipment against civilians?
Okay, from an ideological standpoint, briefly, yes, every bullet matters.
a practical one, it doesn't change the dynamics. Picture a mop bucket full of
water. The water are the opposition supplies prior to their offensive, okay?
They are now the government. So take that mop bucket and throw it into a swimming
pool. Those are their supplies now. Now picture a Dasani bottle and you open the
bottle and fill the cap with water and pour that into the pool. That's what they
got that was in U.S. custody that was left.
Well, yes, it's not ideal.
It doesn't shift the dynamics of what's going to happen.
It's worth noting that without any of this, they were capable of retaking the country
in less than a month.
Tracking down those who helped.
Yeah, okay, so the biometrics.
That is an issue, but it's also worth noting that the opposition has had that equipment
for five years.
It is more of an issue now because they're the de facto government.
They have a lot more free reign to employ it.
This is going to be an issue for those trying to leave.
Aside from that, not so much.
I think people in the United States have the image of some kind of trial or tribunal or
something like that.
It's not how it's going to work.
The opposition is going to flip somebody.
They're going to give them a name.
That person is going to give them a name.
opposition is going to go to their house, ask them a couple of questions, and that's
it. There's not going to be a high burden of proof required before action is taken.
Biometrics is only going to really be a game changer when it comes to checkpoints, and
it's going to matter a lot there. But again, they already have that equipment. It's whether
or not they're going to be able to employ it in any real fashion.
What about using seized equipment to go after neighboring countries?
They won't.
The opposition does not have expansionist designs and neighboring countries are more
than capable of dealing with that equipment.
It wasn't incredibly sophisticated.
What about the holdouts under the VP?
Okay, so the Vice President is actually now the acting president because the
president of the national government fled. He's in an area that used to be
controlled by the Northern Alliance, rallying support to dislodge the
opposition. If there is anything currently in that country that stands a
snowball's chance of dislodging them, it's him and his crew. He knows what he's
doing. He knows what he's doing. It depends on the amount of support he can
muster, and that depends on how the opposition behaves. If they are less
repressive, he may not get any support. They have a snowball's chance. I wish
them the best. If there is a chance, it's them. I don't, it's not a good big chance.
It's pretty unlikely that they're able to pull it off, but it's possible.
Will there be a continued CIA presence?
Absolutely.
There's no qualification to that.
Yes, there will be.
Will the opposition be nicer to women as promised?
Yes, probably, but nowhere near what I think people might be expecting.
It's been 20 years.
History has a way, years passing, has a way of making things more liberal.
So they probably will have evolved a little bit on this topic.
But it's not going to be anywhere near what Western standards would imagine.
So the idea that it's going to be the same as it was under the national government, that's
really unlikely.
there probably will be some better treatment but that's also going to be
based on locality and we'll get to that as these questions progress I'm sure.
Would evacuations go more smoothly without base closures? This is an
interesting question and something that should really be looked into. The idea
being that if we didn't close the bases we would have more points to get people
out from. That makes sense. However the hold-up right now is not getting them
onto planes and getting them out it's having a place for them to land. It would
stand to reason at a cursory glance that with more locations it might have gone
smoother but probably not. When you really dig into it what you would
probably have is the same footage that you're seeing at the airport occurring
at multiple locations. Then you have to factor in the risk that comes with the
opposition lashing out against one of those locations and there being no
reinforcements to come back them up because everybody's occupied doing
something then you also have the risk of a bunch of scared panicked people
overrunning that installation and US soldiers who are also scared and panicked
responding in a really bad way. I think what people are looking for is some way
for this not to look the way it does. This is what it looks like. This is what
the end of these interventions looks like. There's not much that could be done
to shift that. Any chance equipment left was a Trojan horse? I'm sure that some of
it was doctored. We're never going to get a full accounting of that because a lot
of times that's done by individuals without orders. We'll never know how
much, but I am certain that some of the equipment there is going to be more
dangerous to the user than anybody else. That's a certainty.
Why are they saying we don't have the capability to go get them? Them being
stragglers all over the country. This was bad messaging. So the United
States military, of course, has the tactical capability to drop in and extract people.
There are units within the US military that can do that with a greater than 99% success
rate.
However, there are more than 1,000 packages all over the country, more than 1,000 people
that need to be rounded up, which pretty much guarantees a catastrophic failure at some
point.
So from a strategic standpoint, from a political one, it is unlikely that the Department of
defense is going to slap a Mogadishu on top of everything else that's going on.
As the numbers of stragglers drop because they find their own way out or
get their way get themselves to the airport, those types of initiatives
become more likely. But for right now there's a whole lot of people on their
own. Could the withdrawal have gone better? This is what it looks like. Sure,
In raw numbers, which is important, had we started evacuations the day this chain
of events was set in motion more than a year ago, raw numbers more would be out.
But at the end, it's always going to look like this because people won't take it
seriously. Oh, that's not really going to happen like the travel advisories. The
information was out there. People didn't want to accept it. Look at the public
health stuff in the United States. People have a hard time accepting bad news and
then when it becomes apparent that that's how it's going to play out, many
times it's too late or it becomes chaotic as you see now. How will the
opposition run the government? That's a good question. The leadership is smart.
Again, not ignorant Hill people. However, they're field commanders. They don't know
a whole lot about city planning. So what is likely to happen is that a lot of
bureaucrats who are currently in a job, they're going to stay in that job. The
opposition will have some kind of council and on that council only a few
people's opinion is really going to matter and that will establish minimum
rule sets for the whole country, meaning do this or don't do this. Outside of that,
local commanders who become community leaders, they're gonna have a pretty wide
range of what they can do. There's going to be a lot of, there will be a lot of
difference in how different areas are governed. It's very futile.
Possibility of resistance underground movements developing incredibly high.
Without a doubt in my mind, there were stay behind networks established.
Whether or not they become active, we don't know. Depends on how we're
oppressive the opposition is. If they're not that bad, they may stay dormant forever.
We don't know that yet. But do they exist? Yes. Opposition says they want peace and inclusivity
but their actions on the ground are different. Yeah, it's not like the US military. The commanders,
people at the very top, they establish minimum rule sets. Outside of that, there's a lot
free reign. So they may take things right up to the point of not violating what
the top leadership said. How that's going to shape out over time, we're going to
have to wait because those at the very top understand they have a chip in the
big game now. We're talking head of state. So they may be wanting to be a little
bit more liberal than they've portrayed themselves in the past because in the
past they had to maintain this radical image to keep people motivated.
The most radical revolutionary becomes a conservative the day after the rebellion.
So they may be a little bit more inclusive because they want to maintain control because
now they're in power.
Okay, it would be cool to get a breakdown of how they're actually taking over territory.
Is it mostly through negotiation with local leaders or has it been violent?
Both.
cases, the opposition shows up. They're like, hey, you're a colonel with the national government.
You know you can't beat us. Why don't you just come to our side? Then you and your family
will be safe and taken care of. And the person they're talking to is like, it's God's will.
And flip sides. That happened. How critical is cash? Will freezing of banks result in
pay issues for fighters and then weakness? No. If they run low on cash, they'll just
take it from the locals. The US government has to make this move because
again now they're dealing with a government. They're dealing with the
de facto government. I don't know that other nations are going to follow
through so the banks just may move. How sophisticated is the equipment we left
behind? It's not. I did a video on this yesterday. It's really not. Is getting
out like the Kobayashi Maru.
What a nerdy reference and one that I get.
Yeah, it is at this point.
Um, if you're trying to withdraw from a nation that has active opposition and
you never got that nation to the point where it can stand on its own, yeah,
it's, it's always going to, you're always going to lose, it's always going to look
like this.
Why was Biden admin so unprepared for the sweep of the country?
Okay, so I was probably one of the more pessimistic voices
when it comes to how long I expected the national government to last.
Even I gave them two more weeks. I would imagine
those that are advising DOD, they probably didn't want to point out that
DOD never got them ready for this type of total withdrawal.
So their estimates were a little more rosy, because as they walked in there and said,
hey they're not going to last a month, DOD would have to admit that they didn't
prepare them for the withdrawal. I know two advisors who do this
privately who said you know they're not going to last a day but those are the
only people I know that were aware of how ill-prepared the national government
was. Where were the Afghanistan Armed Forces? They collapsed. They collapsed. The
U.S. trained them to fight the way the U.S. fights with air support and fire
support and intelligence and surveillance platforms and stuff like
that and when we left we took it all. They were never prepared to fight that
level of opposition. The opposition knows what they're doing and can do it low-tech.
In some ways, it may have saved a lot of people that the national government, the
army collapsed as quickly as it did because it limited the fighting, or were
the weapons turnovers negotiated prior to our drawback withdrawal? I'm getting
this question a lot. The idea that people in the US kind of set this in motion. It's
kind of conspiratorial. I don't think that that's what happened and it's not
because I don't think that the previous administration would sell out like that.
It's because I don't think they have that level of coordination. This appears to
be pretty organic and mainly fear-based or those people who are able to
accurately assess their capabilities versus the opposition. What happens to
Blackwater and friends. If there is a silver lining, it's that their cash cow's
done. In order for them to continue to make money, the national government had
to stay in place. What should the world do next? Study this over and over and
over again until they realize that the world has to come up with a process to
to help politically bankrupt nations go through a chain of events that leads
leads them to coming out the other side of that chain of events, capable of standing on their own.
Stop relying on military intervention.
Why did the Army give up without a fight? Yeah, we've covered that a few times now.
What's the likely scenario for the majority of the refugees that we created over there?
Not good. Not good. We're never going to get them all out.
We're never going to get them all out. Even if we had started as soon as the deal was made,
this chain of events was put into motion, we would never get them all out. It's
going to be bad. Now there is the slim chance that the opposition is
being honest and they're just going to try to limit their moves to those
who were central to the U.S. being there, but if they decide to go beyond that
it's gonna it's gonna get really bad. Are the opposition representative of the
Afghan population? Is this the rural oppressed people taking over city
dwellers like so much taking on city dwellers like so much of American
politics? No, they're not representative. The average person in Afghanistan wants
It's the same thing that the average person all over the world wants, to live their life
and be left alone.
They're having a change of boss.
Their betters are shifting from one group to another.
Most of them, they just want to go about their lives.
And that's what most people all over the world want, is much as we like to pretend that there's
a gulf between us, there's not, there isn't.
there's a lesson for Americans to learn about this, it's that all of the rhetoric
that gets thrown out about Civil War and all of this stuff, it's based on the idea
that the good guys always win. That's not reality. A lot of times they don't. And in
this case, you didn't really have any objective good guys. It just so happens
that the objective bad guy won. We have to tone down our rhetoric in the United
States when it comes to stuff like this or what we're seeing over there we will
see here. This is what it looks like. You know for a long time a lot of the news
that came out of there was very sanitized. What you're seeing now is
reality. And it's always the reality. People want to find blame, you know. It's
everybody. It's everybody. Everybody who had a hand in this has some of the blame.
Every leader, every president, every senator, every congressperson, everybody
on the oversight committees, they all had to have known on some level that this
was going to be the outcome. It's the only way it could have ended. Anyway, it's
It's just a thought.
So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}