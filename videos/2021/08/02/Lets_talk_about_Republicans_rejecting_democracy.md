---
title: Let's talk about Republicans rejecting democracy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Sx2JD5TR6VU) |
| Published | 2021/08/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about two polls showing alarming numbers among Republicans.
- 63% of Republicans do not believe democracy is working.
- 67% of Republicans view voting as a privilege, not a right.
- Beau warns that the Republican Party base is leaning towards dictatorship.
- Urges Americans to pay attention to these alarming numbers.
- States that government is an illusion, much like a dollar's value.
- Calls for accountability by subpoenaing everyone who has supported baseless claims.
- Suggests charging individuals with perjury if they lie under subpoena.
- Expresses concern that without action, authoritarianism will continue to grow.
- Warns that without accountability, current events may become a trial run for worse situations.
- Emphasizes the importance of defending democracy and institutions now.
- Points out the contradiction in viewing voting as a privilege but not gun ownership.
- Calls on politicians to act now in defense of democracy before the midterms.

### Quotes

- "Americans will ignore these numbers at their peril."
- "Government is an illusion. It's a lot like a dollar."
- "Their base rejects the founding principles of this country."
- "Now is your time, and you had better do something before the midterms."
- "It's a privilege. Probably trying to ensure the purity of the ballot box."

### Oneliner

Alarming poll numbers reveal Republican attitudes towards democracy and voting as privilege, urging immediate action to defend democracy before authoritarianism grows further.

### Audience

American citizens

### On-the-ground actions from transcript

- Contact Congress to demand accountability for those supporting baseless claims (implied)
- Take action to defend democracy and institutions before the midterms (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of alarming poll numbers indicating Republican sentiments towards democracy and voting rights, urging immediate action to prevent the growth of authoritarianism.

### Tags

#Democracy #VotingRights #Authoritarianism #Accountability #DefendDemocracy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about two polls.
Two polls, two sets of numbers and a quote.
Somebody once said that if conservatives realize they can't win democratically,
they won't abandon conservatism, they will reject democracy.
And that's a cute quote.
You know, it paints them in a light that is very, well, childish.
I didn't realize it was true.
Two polls, one says 63% of Republicans do not feel that democracy is working.
The second says 67% of Republicans feel that voting is not a right,
it's a privilege, something that can be revoked by their betters.
The Republican Party, the base of the Republican Party is primed for
dictatorship.
They have abandoned democracy, they have abandoned the representative government
that the United States has, that the Constitution calls for.
They've given up on it.
Americans will ignore these numbers at their peril.
These are alarmingly high numbers because government is an illusion.
It is an illusion.
It's a lot like a dollar.
It controls your life, but
really only has power because you believe that it does.
Those numbers are high, high enough to cause significant issues.
And those numbers are that high because a group of self-serving individuals
called the election into question without evidence.
It might be time for Congress to subpoena everyone,
everyone who has backed this administration
who has backed these claims and demand their evidence.
And if they lie, they get charged with perjury right then on the spot.
No time to amend your answer, nothing.
The numbers, they can't go unchecked.
This has to be addressed because it will continue to grow.
In our current climate, with our current talking heads, it will continue to grow.
They will push authoritarianism.
They will push dictatorship.
That is where we are headed.
Those numbers are too high for this climate.
If there is a lack of accountability for
the actions that have occurred since the election,
it will only grow.
It won't be a blip in American history.
It will be a trial run.
It will be practice for the main event because they have their base primed.
Their base rejects the founding principles of this country.
They believe democracy isn't working and
they don't believe that you have a right to vote.
It's a privilege.
It's a privilege.
Something that can be revoked and it will be.
This can't be ignored.
I've said that voting is the least effective form of civic engagement.
But for many, it's their only form.
The ability to say yay or nay about little issues and who represents them.
That's all they have.
And that voice once gone, it is impossible to get back without dramatic events.
Those in the seat of power who claim that they want to defend democracy,
who want to defend the institutions of this country, they have to do it.
Now is the time, you can't wait, these numbers are too high.
This is alarming.
67% of Republicans feel that voting is a privilege, it is not a right.
Ask them that about gun ownership.
This isn't a semantic thing where they don't understand what right means
versus privilege.
They know, they don't care.
They have given away their self determination.
They have said this group of people over here, they know what's best for me, and
I will do what I'm told like an obedient lackey.
They don't even want a voice.
It's a privilege.
Probably trying to ensure the purity of the ballot box.
For all of those politicians, hashtag resistance.
Now is your time.
Now is your time, and you had better do something before the midterms.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}