---
title: Let's talk about Trump's most presidential act....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rl6YqTUs7TQ) |
| Published | 2021/08/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump advocated for people to get vaccinated during a rally in Alabama, but his supporters booed him.
- Despite the booing, Trump recommended getting vaccinated and mentioned that he himself is vaccinated.
- Beau praises Trump for not pandering to his supporters and telling them the truth.
- The shift in tone within the Republican Party is due to realizing the impact of spreading anti-science beliefs.
- The Republican Party's base is losing segments due to their commitment to anti-science rhetoric, potentially affecting elections.
- Beau compares knowledge to wisdom using the Frankenstein analogy to illustrate the political establishment's situation.
- The establishment's promotion of conspiracy theories has backfired, leading to a lack of control and backlash from their own base.

### Quotes

- "I recommend you get the vaccine."
- "He told a group of his supporters something that they didn't want to hear."
- "Wisdom is knowing that the doctor was the monster."
- "You have a political establishment that pandered and it breathed life into this conspiratorial anti-science nonsense."
- "Think about what makes them cheer."

### Oneliner

Former President Trump recommended getting vaccinated, faced boos from supporters, and sparked a shift in Republican Party tone towards anti-science beliefs.

### Audience

Political observers

### On-the-ground actions from transcript

- Challenge anti-science rhetoric within your community (implied)
- Support leaders who prioritize truth over pandering (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on the impact of political rhetoric on public health and the need for honesty in leadership.

### Tags

#Trump #Vaccination #RepublicanParty #AntiScience #Leadership


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a shift in tone.
The difference between knowledge and wisdom.
And the most presidential thing that Trump has ever done.
No joke.
I don't know when y'all are going to watch this because there's so much news happening so quickly.
But recently the former president held a rally in Cullman, Alabama.
And during that rally he did something that just amazed me.
He advocated for people to get vaccinated.
He's like, I recommend you get the vaccine.
And they booed him.
They booed him.
And he said, you have your freedoms.
But I recommend you get the vaccine.
I'll let you know if it doesn't work, I'm vaccinated.
It was great.
It was great.
The most presidential thing that man has ever done.
He told a group of his supporters something that they didn't want to hear.
He didn't pander to them.
He told them the truth.
It's sad that the most presidential thing he ever did occurred after he left office.
But I want to give him credit for it.
Now why is that tone shaping up and it's shaping up in different places within the Republican Party?
Well, the answer to that is pretty simple.
They realized that that monster they created, it has a mind of its own.
And they peddled and pandered that anti-science nonsense.
And well, their base bought into it.
But now their base is so committed to it that they're losing segments of their base.
They've run the numbers.
They understand that most of those being lost right now are Republicans.
And it's enough to swing an election.
So you're going to see a shift in tone.
It's going to happen more and more often.
You know, they say the difference between knowledge and wisdom.
Knowledge is knowing that Frankenstein wasn't the monster, that was the doctor.
Wisdom is knowing that the doctor was the monster.
And that's what you have here.
You have a political establishment that pandered and it breathed life into this conspiratorial anti-science nonsense,
this movement that they energized.
And now they can't control it and they're met with boos from their own base.
Well, if I did have any advice for the former president, it would be not to worry about those boos too much.
I mean, think about what makes them cheer.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}