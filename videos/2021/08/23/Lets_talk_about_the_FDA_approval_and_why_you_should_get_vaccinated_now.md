---
title: Let's talk about the FDA approval and why you should get vaccinated now....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sFkwOqCl5Ok) |
| Published | 2021/08/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Full FDA approval for a vaccine has been granted, but it may not sway the anti-vaccine crowd much.
- About 20% of people identify as anti-vax, posing a challenge to reaching the 70% to 90% vaccination goal.
- Distinguishes between the anti-vax crowd and the hesitant individuals, suggesting a focus on the latter for better success.
- Private mandates for vaccination are expected to increase, driven by liability concerns rather than social responsibility.
- Government mandates may become necessary if vaccination rates don't reach the desired levels, potentially leading to resistance and reinforcing conspiracy mindsets.
- Beau advocates for increasing vaccination rates through persuasion and private means to avoid government mandates and further entrenching anti-vaccine sentiments.

### Quotes

- "We need to make sure that we can get enough people vaccinated so we don't need those mandates."
- "Violence is bad. But it's also going to help reinforce the conspiratorial mindset of those in the anti-crowd."
- "If we don't get into that 70% to 90% range on our own, there will be government mandates of the general population."
- "The government will impose it. If they think it's necessary, they're going to do it."
- "Please go get vaccinated."

### Oneliner

Full FDA approval for a vaccine may not sway the anti-vaccine crowd, making it vital to focus on hesitant individuals to increase vaccination rates and avoid government mandates.

### Audience

Americans

### On-the-ground actions from transcript

- Reach out and explain any questions to hesitant individuals (implied)
- Get vaccinated and encourage others to do the same (implied)

### Whats missing in summary

The full transcript provides detailed insights on addressing vaccine hesitancy and the importance of increasing vaccination rates through persuasion and private means to avoid government mandates and prevent reinforcing anti-vaccine sentiments.

### Tags

#Vaccination #FDAapproval #VaccineHesitancy #PublicHealth #GovernmentMandates


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the big news.
One of them got full approval.
We're there.
Finally happened.
Full approval.
So we're going to talk about that,
what it means, why it may not be the game changer
that a lot of people are picturing,
and what we as Americans need to do
because of the situation we find ourselves in.
Because while it may not be a game changer in and of itself,
it does provide another tool.
Okay.
So one of the vaccines got full FDA approval.
A lot of people are suggesting that this is going to help
with the people who are anti.
It's not.
Not really.
Most of them are not anti-vaccine based on reason.
Their position is based on fear.
So it's probably not going to impact them a lot.
They're just going to move the goalposts.
They'll no longer say it's not approved and go to say,
well, it wasn't approved the right way or whatever.
Now, in and of itself, that's not a huge deal,
except for the fact that about one in five
self-identifies as anti-vax.
It's 20%.
20%.
That's really bad news.
That is bad news because the goal is to get 70% to 90%
vaccinated.
If 20% are starting from the position of I am anti,
I'm not going to do this under any circumstance,
you got to get everybody else.
Now, let's distinguish between the anti-crowd
and those who are hesitant.
The approval may help with those who are hesitant.
For some of them, they may have just been waiting for that.
For others, they are hesitant because of the rhetoric
that has come from the anti-crowd.
And even though it may not sway the anti-crowd,
losing the talking point of it's not FDA approved
may kind of loosen the hold that rhetoric
has on the hesitant crowd.
And at this point, the goal has to be
to get everybody who isn't just absolutely committed
to avoiding it vaccinated.
That's what we have to do.
So we have to start focusing on the hesitant demographic,
more so than trying to convert the anti-crowd.
Because with the hesitant side, you're
going to have more success.
You can point out that I want to say more than a billion doses
have been given now of the various vaccines.
And while only one has full FDA approval in the United States,
the other ones are expected to get it soon.
So this approval helps us target a little bit more
and know who to reach out to.
The reaction from the anti-crowd when this news came out
was exactly what you would expect.
Move the goalpost, pretend like it didn't happen,
pretend like they were never saying
that that's what they were waiting for.
But the hesitant crowd, they seem more receptive.
So since it isn't going to impact them, what will it do?
You're going to see a lot more private mandates.
Meaning if you want to exercise your power
meaning if you want to exercise at this location,
if you want to go to this private school,
if you want to work at this location,
you're going to have to be vaccinated.
Companies are going to take it upon themselves
to make that call, a bunch of them.
You're going to see that happen more and more and more.
And it's not necessarily because they
want to be socially responsible or anything like that.
It's because they want to avoid the liability.
The US military, you will have some government mandates.
The military is, of course, going
to mandate this vaccine.
That's going to happen.
I'd be surprised if it hasn't already happened.
They said they were going to as soon
as the approval came through.
So that's a large chunk of the population.
And there are some people who are in that anti-demographic
that are going to end up reconsidering their position
when they are mandated to do it by their job
or by their hobby or whatever.
So there will be some chipping away at that.
But the main goal and why it's important to recognize
that 20% number, one out of five, is if we don't get,
if the American people do not get into that 70% to 90%
range on their own using private means and conversation
and meeting in a marketplace of ideas and all of that
and just general persuasion, there
will be government mandates of the general population.
It will happen.
And I know some people may think that's a good idea.
It's probably not, not just from any ideological perspective.
But if it comes to that, first, you
can probably expect some minimal amount of violence
that's going to go along with it.
That's always bad.
Violence is bad.
But it's also going to help reinforce the conspiratorial
mindset of those in the anti-crowd.
So as somebody who has 100 hours of content dedicated
to the idea of, hey, go get vaccinated,
we need to make sure that we can get enough people vaccinated
so we don't need those mandates.
So the government doesn't have the reason to do that.
Because people keep saying, oh, the government can't do that.
Oh, yeah, they can.
They can and they will.
So we need to do everything we can to meet it,
to meet the goals, to meet what is necessary,
without that becoming a viable option.
Because if it does, those people who are anti,
that's going to send them even further down that rabbit hole.
We'll never get them back.
And that large of a segment of the population
having their view reinforced that it's
some kind of conspiracy is probably really bad right now.
We need to be working to pull people out.
So the idea of a general government mandate,
it's something that we've got to work to avoid.
Because the government will impose it.
There's no doubt about that.
If they feel it's necessary, they're going to do it.
We have to make sure that it's not.
I know it seems weird to be trying to protect the people
who wouldn't get a shot to protect you.
But if we want to get back to any semblance
of a functioning country here
when it comes to discussion and information exchange,
we have to get them out of that conspiratorial mindset.
And a government mandate,
it's just going to send them further down it.
So please go get vaccinated.
If you are hesitant, reach out, ask somebody.
There are a whole bunch of people who would be willing
to explain any questions that you have.
And if you are one of those who has been too busy
or just doesn't see it as a high priority
because you live out in the middle of nowhere,
I know a lot of those people, now's the time to do it.
We've got to get these numbers up.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}