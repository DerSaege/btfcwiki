---
title: Let's talk about what it means to be woke and Garth Brooks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_HSd3MflmTc) |
| Published | 2021/08/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Garth Brooks mandated mask-wearing at his concerts, sparking controversy over being labeled as "woke" for a mildly socially responsible act.
- Beau criticizes the oversimplification of the term "woke" to mean only being alert to injustice, arguing that true woke-ness involves actively working to end systemic issues and injustices.
- He suggests that being truly woke means recognizing and addressing food insecurity, fear of violence, racial injustices, housing as a human right, environmental injustice, and the privilege associated with money.
- Beau points out that Garth Brooks has a history of advocating for social issues, such as gay rights in his music in the 90s, challenging the notion that he has only recently become woke.
- He criticizes the tendency to label individuals as woke when they deviate from expected norms or demonstrate socially responsible behavior.

### Quotes

- "True woke-ness involves actively working to end systemic issues and injustices."
- "They were woke the whole time, trying to get that message out there."
- "Once again, it boils down to somebody stepping out of line and the right-wing cancel culture coming for them."

### Oneliner

Garth Brooks's mask mandate sparked debates on woke-ness, with Beau challenging the oversimplified definition and advocating for actively addressing systemic issues and injustices.

### Audience

Social justice advocates

### On-the-ground actions from transcript

- Advocate for addressing systemic issues and injustices (exemplified)
- Support social causes actively (exemplified)
- Challenge oversimplified definitions of terms (exemplified)

### Whats missing in summary

Deeper exploration of Garth Brooks's history of advocating for social issues and the implications of labeling individuals as woke prematurely.

### Tags

#GarthBrooks #Woke #SocialJustice #SystemicIssues #Advocacy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about Garth Brooks.
Yeah. Okay.
So if you missed it,
he did something mildly socially responsible, you know.
He said that if you're going to come to his concerts,
you're going to wear a mask.
Mandated it.
Because he did something
that was mildly socially responsible,
he of course was immediately branded
as having become woke.
He's woke now, Garth Brooks.
I find that funny.
You know, I've listened to some Garth Brooks in my day.
I know, big surprise, right?
See that term, it's getting thrown out so often now.
We really need to come up with a good understanding
of what it means.
Because the general consensus is it means alert to injustice.
And I think that's a horrible definition.
I don't think that's a good way to look at it at all,
because you can be alert, aware of an injustice
and want it to continue.
So I don't think that's good enough.
I think you have to be aware of it and want it to end.
You know, and if you were really woke,
you would understand that a lot of the systemic issues
that we face in this country and in this world,
they're actually at the level
that we can't even really be free until they're solved.
Until those issues are resolved, it's ended.
Till that injustice is gone.
Somebody who's truly woke, they would probably
understand that food is necessary.
And food insecurity is a big issue.
And they might even say that we're not really
going to be free until the last child cries
for a crust of bread.
They might understand that you can't really
be free if you're in fear of violence,
if you live under that constant fear.
So we won't be free until the last man dies,
for just words that he said.
They would probably understand that housing's a human right.
It's something that you have to have to survive.
So we wouldn't really be free until there's shelter
over the poorest head either.
They would probably, especially in this day and age,
have some understanding of the racial injustices in the world.
And most, if they were forward thinking enough,
they would want to get to that point where we acknowledge
the things that come along with race, the cultural differences,
and all of this stuff.
But they don't really matter anymore.
We move past that point of accepting.
And then we're back to the point where
it's just something that exists, that it's not
a defining characteristic.
It's not something that divides people.
So they would probably want the last thing for us
to notice to be the color of skin.
And the first thing that we look for is the beauty within.
And if people were really woke, they
would certainly be aware of environmental injustice.
And they would know we wouldn't be free until the skies
and the oceans are clean again.
Right?
That all kind of makes sense.
And that's standard.
Then you'd probably want to go to the point of understanding
that people should be free to love whoever they choose
and that the world is big enough for all different views.
And we should all be able to worship
from our own kind of pews.
Right?
That tolerance, that woke tolerance,
that newfangled thing.
Somebody who was pretty woke would also
understand that money allows a lot of privilege.
And that if we wanted a free society,
we would want money to talk for the very last time.
So nobody has to walk a step behind.
Yeah, I didn't become a poet.
That's a Garth Brooks song I'm talking about,
When We Shall Be Free.
It's from 1993.
Garth Brooks is woke, no doubt.
OK?
But it doesn't have anything to do with him mandating masks.
Y'all ever watched his videos?
Standing Outside the Fire?
Name positive representation for disabled people in music videos
prior to that.
He has always been woke.
This is becoming a recurring theme.
All of a sudden, people notice something.
They don't bow to dear leader in whatever propaganda
is coming out.
And then they get labeled woke, when the reality is,
they were woke the whole time.
They were trying to get that message out there.
Garth Brooks, in the 90s, was advocating for gay rights
in a song, in a country song.
I don't know that you're going to find anything
a whole lot more woke than that way back then.
Once again, it boils down to somebody
stepping out of line and the right-wing cancel
culture coming for them.
They're not doing what the party says.
They haven't joined in lockstep with the Republican Party
and everything that they currently represent.
They did something mildly socially responsible.
That's what it takes to be woke today, I guess.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}