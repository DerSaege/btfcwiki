---
title: Let's talk about getting out from over there and a teachable moment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6Llm29VW-0s) |
| Published | 2021/08/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the levels of U.S. State Department travel advisories: level one, everything's cool; level two, use caution; level three, reconsider your trip; level four, get out; and an unofficial fifth level, get out now no matter what.
- Details the embassy's escalating warnings to leave Afghanistan, culminating in a blunt "get out" notice on August 7th.
- Mentions the significance of embassies discussing repatriation loans as a signal to leave immediately.
- Notes that around 10,000 Americans were still in Afghanistan, with the official number likely being low due to some not checking in.
- Emphasizes the importance of paying attention to travel advisories for those traveling overseas or knowing someone who does.
- Addresses the cooperation with the opposition in Afghanistan to evacuate people and criticizes those who are against negotiating with the opposition.
- Argues that negotiating with the opposition is a normal part of post-war processes and that portraying surprise at this is uninformed.
- Asserts that the opposition in Afghanistan has nothing to gain from harming Americans and that such ideas stem from wartime propaganda.
- Stresses that negotiating, cooperating, and exchanging prisoners after wars are standard procedures.
- Encourages removing identifying information from photos of Afghan nationals for veterans who worked with them.

### Quotes

- "Once the US effort is done, there will still be stragglers. There'll still be people there, and nothing will let you know how bad off you are as the appearance of those people who are coming to get you."
- "Acting as if you're surprised [about negotiating with the opposition after war], that doesn't lend a whole lot to your credibility."
- "The surest way to avoid something like this happening again is to not engage in these types of military interventions."

### Oneliner

Beau explains the importance of heeding travel advisories, the necessity of negotiating with opposition post-war, and the need to remove identifiers from photos of Afghan nationals for veterans.

### Audience

Travelers and advocates.

### On-the-ground actions from transcript

- Blur identifying features in photos of Afghan nationals (suggested).
- Pay attention to State Department travel advisories when traveling overseas (suggested).

### Whats missing in summary

The full transcript provides detailed insights on the significance of travel advisories, negotiations with opposition post-war, and the importance of protecting identities in photos of Afghan nationals for veterans.

### Tags

#TravelAdvisories #Negotiation #Afghanistan #Veterans #CommunitySafety


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk
about a teachable moment when it comes to travel advisories and how we're
getting everybody out from over there and some points that are being made
that may not be the best points.
Okay, let's start with the teachable moment. The United States State Department
has these things called travel advisories. Most people ignore them. They
don't really pay attention to them. There are four levels and then a fifth
unofficial level. Level one is, hey, everything's cool. Level two is use
caution. Level three is reconsider your trip. Level four is get out.
Okay, now the fifth unofficial level, that's get out now no matter what.
Okay, so on April 27th of this year, the embassy there, they said, basically
told their employees, if you don't have to be here, don't be. Work remotely.
If you're ever in a country and you hear that, it's time to leave. June 28th,
they kind of put it out there a little bit more broadly. They're like, get
out. August 7th, they issued a very blunt get out notice. And there was a key
thing in this. If you're ever in a country and the embassy there starts
openly talking about repatriation loans, you need to leave right then, that minute,
that second. Do not pass go, do not collect $200, unless you're getting it
from the embassy. A repatriation loan is where the embassy will buy your ticket
for you to get you out. That occurred on August 7th. There are, last number I saw
was about 10,000 Americans still in Afghanistan. I'm going to guess that the
majority of them didn't need to be there. Some certainly did. Some certainly did.
They were essential. They had to be there. But I'm going to say the majority of them
didn't. And I would also point out that that number, 10,000, the official number,
whatever it is right now, that the US government is using is low because not
everybody checks in. The embassy doesn't know everybody who's there. Now today, I
guess they use travel records a lot more than they used to, so the numbers a
little bit more accurate, but there are certainly people that are not in
included in that. Okay, so that's something to learn from this. It's not
going to help those people who are there right now, but this is a good time to
talk about it. These travel advisories are pretty useful. If you travel overseas
or you know people who travel overseas, pay attention to these things. And you
can go to the State Department website and go to their travel advisories and
click on it, and they'll even tell you why. Right now, there's a bunch of them
that are level 4 because of the public health stuff, but you can click on some
countries and they'll say, you know, civil unrest. So just remember to pay attention
to those in the future, because once the the US effort is done, there will still
be stragglers. There'll still be people there, and nothing will let you know how
bad off you are as the appearance of those people who are coming to get you
at that point, because if those are your saviors, you're in a world of trouble.
Okay, so right now, it appears that the United States is cooperating with the
opposition there in an effort to get people out. And there's a whole bunch of
people who are taking issue with this. We're negotiating with the opposition. We
don't negotiate with the T word. Yeah, okay, tell me you don't know anything
about how this stuff happens without telling me you don't know anything about
how this stuff happens. Newsflash, at the end of every war, we negotiate with the
opposition. Every single one. That happens. That's a thing. Acting as if you're
surprised, that doesn't lend a whole lot to your credibility. Now, as far as them
being the T word, that's the purview of the non-state actor. They are now the
de facto government. The presumptive president is y'all known as the guy that
Trump let out. He's known as being pretty thoughtful. He's not new to
this. He has been around a long time. There is nothing to gain. There is
nothing to gain on their side from interfering with this. There's not. People
are leaning into the idea that they're going to do this horrible thing to all
of these people. That's really unlikely and it's a byproduct of wartime
propaganda. They are not ignorant hill people. They're pretty smart. They
probably remember what happened the last time a whole bunch of Americans got hurt.
I don't think there's anybody anywhere in the world that knows that as well as
the people of Afghanistan. If they did something like a lot of people are
suggesting and hurt those that were still there, that whole idea of bombing
till the rubble bounces, that becomes a whole lot more likely and they know that.
They have nothing to gain by doing that. Nothing. He's the presumptive
president. They won. At the end of every war we negotiate, we cooperate, we
exchange prisoners, all of that stuff. This is all really normal. It's all very
typical stuff.
Ok, so is there a risk in doing this? That's the real question. There is. There
is. It's not from the upper leadership. You may have some people that are a
little further down the chain. That, yeah, I mean that is a risk. I don't think it's
a big one. They don't have anything to gain by doing that and a whole lot to
lose. So I think that that fear is very overstated. I don't see it as a
huge possibility. It's a risk. It's something that needs to be monitored, but
it's probably not going to happen. So that's where you're at. Now as far as
them being the de facto government, understand, I don't know, some countries
probably already have, but they're going to be the recognized government pretty soon.
There's a lot of people in the United States who are on TV, who are
politicians, who are still using wartime propaganda, who are still pushing this
idea. Why? Because we could do it. We go back. They going to let us win this time, sir?
It's still there. It is still there. And they want to manufacture that feeling. So
the next time that this happens, the next time the US establishment wants to
intervene, there's that already underlying support. Well, we have to make
up for that time, that time over there. Don't let that happen. Don't let that
happen. The surest way to avoid something like this happening again is to
not engage in these types of military interventions. So that's where you're at.
Please pay attention to travel advisories if you travel overseas.
Understand, yeah, we're going to be talking with the opposition there a lot. Yes, the
thing about the guy who's about to become president there, who's most likely
to become president, being released by Trump, that's true. I mean, Trump didn't
physically do it. He encouraged another government to let him out so he could
participate in the negotiations. I would also point out that even though he would
be the president, he's not really going to be the Supreme Leader. That's somebody else.
So that's it. Now, as far as something else that somebody brought up today that
is worth noting, if you're a veteran and you have photos from your time over
there, and there are images of Afghan nationals who you worked with, please
remove them, blur their faces, tattoos, scars, name tapes, anything like that. That
could be really important to them. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}