---
title: Let's talk about equipment left behind....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Fl3NpETCkIE) |
| Published | 2021/08/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- U.S. equipment falling into opposition hands is being fearfully sensationalized despite being a common occurrence at the end of wars.
- The concern over the equipment being left behind doesn't hold much weight unless there are plans to go back.
- The captured equipment, like night vision gear and Humvees, is not significant in the grand scheme of things.
- Some of the equipment may contain communications technology that foreign powers could reverse engineer, but this has likely already happened.
- The aircraft left behind, like Blackhawks and A-29s, are not high-tech versions and pose no real threat.
- The fear-mongering around the equipment left in Afghanistan is an attempt to provoke outrage over something that is not a major issue.

### Quotes

- "The concern over the equipment being left behind doesn't hold much weight unless there are plans to go back."
- "The fear-mongering around the equipment left in Afghanistan is an attempt to provoke outrage over something that is not a major issue."

### Oneliner

Beau explains the insignificance of U.S. equipment falling into opposition hands post-war, debunking fear-mongering around the issue.

### Audience

Policy Analysts, Activists

### On-the-ground actions from transcript

- Inform others about the common occurrence of equipment falling into opposition hands post-war (implied)
- Combat fear-mongering by sharing Beau's perspective on the issue (implied)

### Whats missing in summary

The full transcript delves into the lack of significance in fear-mongering about U.S. equipment falling into opposition hands and how it's a common occurrence at the end of wars.

### Tags

#US #Equipment #OppositionHands #FearMongering #Debunking #Afghanistan


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about U.S. equipment falling into opposition hands.
I was going to talk about this later on, but I guess we're going to do it now
because it's become a talking point all of a sudden.
I had planned on using this as an example to show how we could reallocate funding
because I didn't expect this to be something that people were discussing
because this isn't surprising or concerning in the least.
Shouldn't be.
But there's people out there stoking outrage and fear with it.
So let's talk about it, right?
Okay, so what you have, the advancing forces over there,
they captured a lot of U.S. equipment.
You have people on major networks going,
oh look, that's a U.S.-made rifle.
Okay, it's Afghanistan.
I was completely unaware that it was hard to get an automatic rifle in Afghanistan.
That's news to me.
Didn't know that.
It's especially surprising because five years ago,
they did an audit and found that about 1.5 million small arms
had fallen into opposition hands in Afghanistan and Iraq.
This isn't new.
This isn't concerning.
Why should we be concerned about this equipment?
We're not going back, are we?
There it is.
Pushing it out there.
We need to be concerned about this equipment that's being left behind
because we're going to go back.
We can still do it.
Otherwise, there's no reason to worry about it, right?
What else?
They talked about night vision equipment falling into opposition hands.
And yeah, I mean, that's important.
We definitely don't want that.
If it's the 1990s, it's 2020.
You can get night vision equipment from Walmart.
Not a big deal.
Doesn't matter.
Humvees.
Yeah, it's a truck.
I mean, ideally, none of this stuff would be falling into opposition hands,
but this happens at the end of wars.
It's a truck.
I don't know what the concern is there.
There are some versions that are technologically advanced.
They have stuff in the back.
Haven't seen any of those because they were destroyed or pulled out
because that's a high priority, whereas a truck isn't.
Just a plain model.
Not really a big concern.
Although I haven't seen any footage, I'm sure that some artillery and armor
personnel carriers, stuff like that, fallen.
But again, doesn't matter.
Not a big deal.
Now, some of this stuff may have some communications equipment in it.
And I'm sure at some point, somebody is going to realize that, oh,
the Russians or Chinese would love to get their hands on that
and reverse engineer it.
Yeah, I mean, they absolutely would.
And that's why they did, like, in 2004, 2005.
Not really a concern there either because it's already happened.
None of this matters unless we're going back.
Then the part that they're using to scare people.
The aircraft, right?
And we talked about this in the video,
Let's Talk About the Future of Afghanistan, last year.
This exact scenario was discussed.
They're sitting there saying, they got their hands on Blackhawks,
making it seem like it's the same version the US flies today.
It's not.
It's a generation one version.
If you go back to that video, you'll hear me say,
we could give them helicopters.
They don't even have to be good helicopters.
That's exactly what was done.
The Russians and the Chinese are not flocking to Afghanistan
to get their hands on technology from the 1980s.
Not a thing.
Then they're really attempting to scare people using the term,
attack airplane.
The opposition there, they got their hands on attack airplanes.
And that is a true statement.
They got their hands on a bunch of A-29s.
Go ahead and pull up an image,
because I want people to understand
what they're talking about when this is said.
Yes, that's a prop on the front of that thing.
It's designed to go low and slow, doesn't have any range.
It isn't going to make it to the United States.
And this is not something an F-16 pilot
spends his night worrying about.
The entire fleet of aircraft that was left in Afghanistan
could be destroyed from the air by the US in minutes.
There is no threat there.
There is nothing to worry about.
The equipment that was left behind,
yeah, ideally none of it would fall into opposition hands,
but it happens at the end of every war.
That's, it's very common.
Again, if you didn't know this was coming,
you probably shouldn't be on TV giving your expert opinion.
Something else that's worth noting
is that a whole lot of this stuff
isn't even going to be used,
because either the Russians or the Chinese
are going to supply them.
They are now the government.
They can buy arms.
It's not the same.
The war's over.
The war is over.
Unless we're going back,
and unless the idea is to keep that desire
and that image alive, none of this matters.
We're certainly not going back, right?
We learned our lesson over the last 20 years.
At this point, I have seen nothing that matters,
that has fallen into opposition hands.
Now, there may be some advanced pieces of equipment there
that do, and if that happens,
they will be sent to the Chinese or the Russians
to be reverse engineered.
I would also point out that both China and Russia
have really good intelligence networks.
They probably already have the specs from this stuff,
or they already captured and reverse engineered it
from damaged stuff earlier in the war.
This is an attempt to provoke outrage and to fear monger
over something that doesn't really matter.
So, anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}