---
title: Let's talk about safe passage and a continuation of politics by other means....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U-ZmHozdDTU) |
| Published | 2021/08/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about developments that are not getting coverage, marking a shift in dynamics.
- Mentions the de facto government negotiating with foreign powers and offering safe passage beyond the deadline.
- Questions why trust should be placed in the de facto government and why they are extending safe passage.
- Emphasizes that politics is not always transactional and that interests can sometimes align.
- Suggests that removing dissidents from the country benefits the de facto government.
- Explains the importance for the de facto government to maintain peace and legitimacy on the international scene.
- Points out that securing safe passage may encourage mining operations, bringing economic benefits.
- States that it is in the de facto government's interest to provide safe passage, legitimize themselves, and gain financially.
- Mentions historical examples of negotiations with "bad guys" after conflicts.
- Stresses the commonality of negotiating with opposition post-conflict and the continuation of politics after war ends.
- Talks about propaganda during wartime and how it influences public perception.
- Concludes that negotiations post-conflict are a common practice to solidify the de facto government's position.

### Quotes

- "Politics makes strange bedfellows."
- "Trust but verify."
- "War is a continuation of politics by other means."

### Oneliner

Developments show the de facto government negotiating safe passage, benefiting from removing dissidents and gaining legitimacy through international relations.

### Audience

Foreign Policy Analysts

### On-the-ground actions from transcript

- Contact organizations involved in international relations for updates and insights (suggested).
- Stay informed about developments in foreign policy and their implications (suggested).

### Whats missing in summary

The full transcript provides detailed insights into the dynamics of negotiations with the de facto government and the importance of understanding political motivations in international relations.

### Tags

#ForeignPolicy #DeFactoGovernment #Negotiations #SafePassage #InternationalRelations


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're gonna talk about some developments
over there that are not getting the coverage.
I believe they should.
This is one of those little events
that changes the dynamics of things.
It's a good marking point to show how things have shifted.
And it's one of those things that isn't getting coverage,
but in two months, people are gonna be like,
How did we go down this chain?
And then this is where it really started.
OK.
So for continuity's sake, if you're watching these videos
in hindsight, from this point forward,
what we have been calling the opposition
is now the de facto government.
They have shored up their support pretty well.
And they're negotiating with foreign powers as a state.
One of those negotiations has led to a pledge of safe passage that extends
beyond August 31st, beyond the deadline. The de facto government there is saying
they will offer safe passage to those who are Western citizens, citizens of
other countries, there's almost a hundred nations involved in this, or those who
have documentation from those countries.
Okay, so in the little coverage that has occurred of this at time of filming, I'm hoping it
will pick up later.
The big question is, why should we trust them?
Why would they do this?
Why would they do us this favor?
is a holdover from people listening to Trump talk about foreign policy. Trump
was really bad at foreign policy. It's not transactional. It isn't that they're
doing us a huge favor. Sometimes interest align. You've heard that saying politics
makes strange bedfellows. Here's a perfect example. Who actually benefits
the most from this? Is it the United States? Does the US benefit the most from
getting Afghan nationals who want to leave the country from getting them out?
Or does the de facto government benefit the most? Rather than framing this as the
US negotiation team showing up and saying, oh please, pretty please, with a
cherry on top, will you let us take our friends with us?
perhaps the other side, they're not ignorant hill people. Maybe what they're
hearing is, hey you're a newly minted government that has active opposition
from several groups and there's a whole bunch of people who are terrified of
you which may kind of motivate them to fight against you and make them ready
recruits for those opposition groups. How about we just take them with us? They
probably want to do that. The United States and any country that's involved in
this operation is helping remove the dissidents from the country. That's only
good for the de facto government. There's no downside to that for them.
Okay, and then we have to understand that after all of this time of viewing them
in a certain way. They are now the de facto government. They have their hands
on those levers of power. They have to show that they can maintain the peace if
they want any legitimacy on the international scene. And even if they
don't, you know what they want? Power. You know what power comes from? Money. Do you
know what money comes from? They're minerals. If they can show that they can
guarantee safe passage decently well, it will encourage mining operations to come
in. If they can't provide that and they can't provide security, then those mining
operations are going to have to bring in private contractors, maybe even foreign
state militaries, which makes them look bad because their whole thing is to get
foreign occupations out. But now we have to let them in to help mine because we
can't do our job, it makes them look bad. But more importantly to them, if they're
having to pay contractors and all this stuff, well it lowers their cut. And just
like any politician anywhere in the world, once they're sitting in that
presidential palace, once they are sitting at those levers of power, they
want that money. We don't necessarily have to trust them. You trust but verify.
It is in their interests to do this. It is in their interest to do this. It
removes dissidents, it legitimizes them on the international scene, and it gets
some money. What more do you want? What more of a motivation do you want for them
to have done this? Yeah, they're probably going to do everything they can to
maintain that safe passage for as long as possible. Now, this leads into another
question that I got. It says, Bo, I'm a student of military history and you've
got it wrong. Can you name a single other time the United States sat down with the
bad guys, quote, immediately after the conflict ended and just started
negotiating and having tea together? Student of military history, I would get
your tuition back because the answer to that is pretty much all of them, always.
from the beginning. The biggest hint to the answer to your question
can be found on the shores of Tripoli. That was the first major military
engagement the United States engaged in as far as real force projection, military
operation overseas. We're discounting rebellions, wars against natives, and any
little quasi-war that popped up along the way. That's really funny and that was unintentional.
Okay. Google quasi-war. Anyway. So that conflict, which incidentally was over a group of pirates
not providing safe passage, went over there, fought the war at the end of the war, sat
down, negotiated, and signed a deal to get American troops back and pay a ransom to do
So from the very beginning, that's how it worked, and pretty much every conflict since
then.
It's common.
War is a continuation of politics by other means.
When the war ends, politics returns.
This isn't unusual.
It's pretty much what always happens.
They are now the de facto government.
And at this point, basically entering into a joint statement with a hundred other countries,
they're the government.
It's not really even de facto anymore.
They have the levers of power in that country.
So yeah, we're going to talk to them.
Why?
Because, I mean, we'd kind of like to have some of those minerals too.
It's a poker game and everybody's cheating.
The propaganda gets disseminated in a country during war.
The people that put that propaganda out, they don't believe it.
That's just for us common folk.
Our betters see the big picture.
Our betters view it as another tool in the foreign policy toolbox.
They don't become mortal enemies.
all about influence, power, and money. So, yeah, it really is. I'm hard-pressed to think
of any time when we didn't sit down and negotiate with the opposition right after the war. That
pretty much always happens. So, this little chain of events right here, it is very defining
in the sense of it is solidifying the de facto government's position.
If they do a good job of this, it is likely that a whole bunch of foreign investment comes
back, that they're going to get a cut-up.
If they do a good job of it, it is unlikely that Western intelligence provides support
to those opposition groups.
It's in their best interest to provide safe passage.
It is in their best interest to show that they can behave like a legitimate government.
It's not about trust.
It's about politics, it's about foreign policy.
And trust really doesn't have anything to do with that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}