---
title: Let's talk about who's to blame for all this....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XVotMlYOGHA) |
| Published | 2021/08/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Exploring who is to blame for the current situation dominating headlines.
- Analyzing the roles of Bush, Obama, Trump, and Biden in the series of events.
- Not a Republican vs. Democrat issue, but rather a series of mistakes by all presidents involved.
- Personally holds President Obama to a higher standard due to intelligence.
- Criticizes President Trump's actions and predicts current events in foreign policy.
- Acknowledges that blaming one president alone will not prevent similar situations in the future.
- Emphasizes the role of public accountability in decision-making.
- Warns against focusing on assigning blame instead of preventing similar failures.
- Calls for preparing to mitigate the effects and help those affected by the current situation.
- Urges for a focus on preventing future similar events rather than finding a scapegoat.

### Quotes

- "Who's to blame? We are."
- "Looking for somebody to blame so we can wash our hands of it."
- "The best thing that you can do for our veterans is to make sure that we stop creating combat veterans."
- "That's not very conducive to stopping it."
- "How to stop it from happening again, that's what we should be talking about."

### Oneliner

Exploring the blame game among Bush, Obama, Trump, and Biden, Beau reminds us that true accountability lies within ourselves to prevent future crises.

### Audience

Policy makers, activists, citizens

### On-the-ground actions from transcript

- Prepare to help those affected by the current crisis (suggested)
- Focus on preventing similar future events (implied)
- Advocate for accountability and transparency in decision-making (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the roles and responsibilities of Bush, Obama, Trump, and Biden in current events, urging a focus on accountability and prevention rather than assigning blame.

### Tags

#BlameGame #PoliticalAccountability #Prevention #DecisionMaking #PublicResponsibility


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about who's to blame for all of this.
Because that's the topic that is dominating the headlines, right?
It's what matters.
Who do we point the finger at?
Republican? Democrat? Red? Blue? Elephant? Donkey?
Who's to blame?
Which president
carries the most blame?
So we're going to talk about that.
You can start with Bush, right?
A logical place to start.
He started this
and he allowed mission creep
and he privatized it
and he enacted a lot of policies that drove recruitment for the opposition.
And then he started that other thing
in 2003 over in Iraq.
Diverted resources.
A lot of blame there.
Then you can look at Obama, right?
Look at him.
Who carried on many of the same policies
and escalated the use of unmanned aircraft,
which drove recruitment for the opposition.
Then you can go on to Trump, right?
Who set the chain of events in motion that we're seeing the conclusion of now.
He went in there
acting like it was a real estate deal.
Legitimized the opposition
and
made some deals that
weren't
the brightest.
Definitely has some of the blame.
Then you have President Biden
who
honestly tried,
tried, tried to make the right moves,
but they didn't work
and he is commander-in-chief.
He has some of the blame too.
This is not a Republican Democrat thing.
Not at all.
Not in any way, shape, or form.
Every president
that has presided over this war
has made major
mistakes.
Objectively, who's the most to blame?
Bush. Bush is without a doubt the most to blame for this
if you are looking at it through the lens of
decisions that led to this.
Who do I blame the most, personally?
And it's really backhanded, but
President Obama.
President Obama.
Not because he made any
decisions that were worse than
the other three,
but because I honestly believe he is much smarter
and should have known better.
Because I think he is the smartest
of these people.
I hold him to a higher standard.
Trump
set all of this in motion.
He did. There's no denying that.
And
you can go to the foreign policy playlist on my channel and go back and watch.
Everything that we've seen now,
you know that I said it was going to happen more than a year ago, exactly the way it did.
It wasn't hard to see this coming.
That being said, if you go back and watch those videos, and if you're new to the channel,
understand I do not pull punches when it comes to President Trump. I think he is one of the
worst presidents in American history.
I make a point to say
that by that point in the game,
I don't know that there's anybody that could have fixed it.
There were things that could have
been tried,
like the token security force.
But I mean, at the end of the day, that's last ditch.
It's literally handing the conflict off to somebody else
in an attempt to save lives. It's still not a win.
So, yeah,
there's definitely blame there.
Because he did make these deals.
He did make the commitments.
And then you have Biden,
who's presiding over the exit.
Doesn't matter
that he tried
to do the right things.
He's commander-in-chief.
He has the blame too.
But see,
that's only part of the story.
These four men,
why did they make the decisions they did?
What guides them?
Politics.
And when we say that,
we make it seem
as if it's
something that they do on their own.
When you're talking about decision-making
at this level,
you know what guides it? When they're playing politics,
polls.
We're standing here looking for somebody to blame, right?
Trying to figure out which president
is to blame.
We're standing here screaming, who killed the Kennedys?
After all, it was you and me.
We're responsible.
Right now, oh yeah, all the polls,
people are definitely in favor of leaving, but that wasn't the case in 2001, 2004,
2006, 2012.
When politicians play politics,
they're reading poll numbers and trying to figure out what is most politically advantageous.
Who's to blame?
We are.
Because we didn't make enough noise
when Mission Creep set in.
We didn't make enough noise when that other war started.
We didn't make enough noise
when
policies were enacted
that should have been an affront to American integrity.
We're to blame.
But the saddest part about all of this
is that it will happen again.
Because we're standing here right now
figuring out who to blame.
That's
not very conducive to stopping it.
If you can personify
this whole thing as somebody's folly,
well it's all their fault and we don't have to change anything that we do.
And that's what it seems like people are looking for,
a fall guy.
I don't think that's the right way to do this.
If we do this,
if we lay
this failure
at one person's feet, at the president's feet, whichever one you want to choose,
it will happen again guaranteed
because we will learn nothing.
Right now,
who's to blame? That doesn't matter.
How to stop it from happening again,
that's what we should be talking about.
And how to mitigate
the
effects
of this failure.
Because people right now, they're focused on,
oh look, here's this footage.
This is going to go on.
There are going to be people fleeing this country.
They're going to need help.
There are going to be
economic considerations
when it comes to helping them.
And we need to get ready for it.
This is our mess.
Looking for somebody to blame
so we can wash our hands of it.
All that is going to
ensure
is that
another generation from now,
when people forget what it's like,
that it happens again.
At the same time,
you have those engaged in sunk cost fallacy, right?
We have to fix it.
We have to
make sure that things go our way to honor our veterans.
I've said it before, I will say it again.
The best thing
that you can do
for our veterans
is to make sure
that we stop creating combat veterans.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}