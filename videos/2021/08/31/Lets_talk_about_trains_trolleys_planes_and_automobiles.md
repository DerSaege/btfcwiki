---
title: Let's talk about trains (trolleys), planes, and automobiles....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cm4zwo5EvJw) |
| Published | 2021/08/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the ethical dilemma of making tough decisions in crisis situations.
- Drawing parallels between a real-life scenario and the classic trolley problem ethical dilemma.
- Emphasizing the gray areas and consequences of decision-making in high-pressure situations.
- Addressing the specific incident as an outlier and not reflective of typical United States engagements.
- Arguing for a deeper understanding of the root problem to prevent recurring crises.
- Critiquing the lack of learning from past conflicts and the perpetual cycle of similar outcomes.
- Calling for a shift in approach to prevent future crises and avoid repeating past mistakes.
- Challenging the idea that different political parties in power will significantly alter outcomes in conflicts.
- Criticizing the tendency to ignore lessons from conflicts like Afghanistan and continue with unchanged strategies.
- Urging for a change in mindset to prioritize long-term solutions over short-sighted interventions.

### Quotes

- "Everybody wants to be a cat until it's time to do cat stuff."
- "When you're out there, it gets real gray real quick."
- "The lesson that is being taught by Afghanistan, it's not being learned."
- "These types of conflicts will continue until we learn the lesson."
- "We're still going to allow that trolley car to run away the next time some charismatic politician waves the flag."

### Oneliner

Beau tackles ethical dilemmas, urging a deeper understanding to break the cycle of recurring crises and advocating for long-term solutions over short-sighted interventions.

### Audience

Policy makers, Activists, Citizens

### On-the-ground actions from transcript

- Question current intervention strategies (suggested)
- Advocate for long-term solutions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of ethical decision-making in crisis situations and the need for a shift in approach to prevent recurring conflicts.

### Tags

#EthicalDilemma #UnitedStates #Afghanistan #ConflictResolution #PolicyChange


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about planes, trains, and automobiles.
Trolleys, I guess.
We're going to talk about the last operation
before the United States withdrew
and how there are many, many lessons in it.
You know, for the last couple of weeks,
we've heard a whole bunch of people talk about what they would have done
with perfect hindsight.
If they were in charge, well, it would be different.
So today I'm going to provide you with a scenario
and you get to make the call.
Everybody wants to be a cat until it's time to do cat stuff.
So, here's the scenario.
There's a car
packed with a whole bunch of boom
and it's headed to the airport.
If you do nothing,
a whole bunch of people are going to be lost, just like before.
But see, you have the power. You can make that call.
You can strike it.
But if you do,
odds are
there will be loss
of innocence.
That's the scenario
and that is what happened.
A family was taken out.
Somebody had to make that call.
You know, when we talk about this kind of stuff back here,
it's black and white, especially if you're on some news set somewhere.
But when you're out there, it gets real gray real quick.
That scenario,
it's almost identical to an ethical dilemma
that has been debated for a long time.
You can put your answer down below
because you can look at it from the utility standpoint, right?
Just numbers, pure math.
You do the strike,
it's going to be less gone.
You can look at it that way.
Or you can look at it from a moral standpoint and say, you know, right now
we're not really responsible
because we didn't actively engage in the harm that was occurring.
But if we intervene,
well, then we do share some of the responsibility.
It will be debated just like the trolley car problem.
If you're not familiar with it, you have certainly seen the meme somewhere on
social media.
Little guy standing at a switch,
runaway trolley car.
On the track that it's on, there are four people tied to it.
There's a side track
that has one person.
You can flip that switch
and send it over to that one person. What do you do?
It has been debated a very, very, very long time.
And somebody
had to make that call.
And they will second-guess themselves for the rest of their life.
But
that stuff becomes real clear back here
with the benefit of hindsight.
Now one thing I want to make really clear
about this
is that this is a very specific incident.
Most times
the United States engages in these kinds of strikes,
it is not that imminent.
It is not a situation like this.
What happened here
is not a good rebuttal to any criticism
of how the United States has used this tool over the last twenty years.
This is an outlier.
It's normally not like this. Normally
there is the opportunity to wait.
It doesn't have to be decided at that exact moment.
I just want to be real clear on that.
But if you take this
and you widen it,
and you're no longer talking about
the trolley car being just the car headed to the airport,
but the trolley car is the problem,
what is the problem?
What is the runaway train?
It's the intervention.
It's the war.
And every time one of these little runaway trolley cars gets moving,
there are tons of people willing to tie civilians to the tracks.
And somebody has to try to avoid them,
decide which switches to flip.
The only way to avoid this scenario
is to make sure that the trolley car doesn't start
to be a runaway trolley car.
That's the only way you stop it.
But that's not the lesson that we learned.
You can tell that from the commentary.
Because the commentary is,
oh, if my guy was in power,
if my political party was in power, oh, it would be different. No, it wouldn't.
Both major parties in the United States
held a pretty similar amount of time overseeing this campaign.
There weren't major differences.
The lesson that is being taught by Afghanistan,
it's not being learned.
Nobody's listening to what it's saying.
These types of conflicts will continue until we learn the lesson.
And they will have the same outcome.
Because it's pretty much always the same.
The people tied to the tracks,
they don't own the tracks and they don't own the trolley car.
They're just caught in the middle.
Somebody's global turf war.
But when we analyze it,
we pretend that we care about it,
but not enough to change how we do things.
We're still going to allow that trolley car to run away
the next time some charismatic politician waves the flag
and talks about imminent danger.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}