---
title: Let's talk about the US exit and criticism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=P0Fo5zGI00Y) |
| Published | 2021/08/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The United States has officially left Afghanistan, marking the end of a twenty-year mission.
- Critiques and criticisms of the exit strategy are being examined, with a focus on invalid and valid criticisms.
- Invalid criticism includes the idea that if the US had secured the capital, the chaos at the airport could have been avoided.
- Securing the capital with a population of over four million people was not feasible given the limited troops available.
- Critics suggesting that seizing the capital was the right move either lack understanding or are politicizing the situation.
- The withdrawal from Afghanistan meant the US was not in a position to seize the capital, as it was no longer a war zone.
- Valid criticisms involve tactical, operational, and strategic issues with the exit strategy.
- Tactical errors included American troops immediately administering aid at the airport rather than securing the area first.
- Operationally, pushing back the perimeter around the airport further could have improved security.
- Strategically, the Biden administration faced challenges with messaging, failing to effectively communicate the humanitarian nature of the operation.
- Missteps in messaging led to public confusion and may have undermined public support for the mission.
- Proper messaging could have mitigated opposition propaganda and garnered more support for the operation.
- Despite challenges and tragic incidents, Beau believes the operation overall was a success, but acknowledges areas for improvement in future missions.
- Beau suggests that critics demanding immediate evacuations from Afghanistan should draft legislation granting the President such powers if they truly believe it is necessary.

### Quotes

- "The war at that point was over. There's no sense in wasting more."
- "Bad messaging throughout. And that may not seem important, but it really is."
- "Despite the chaos, this went really well."
- "The operation itself was a success."
- "They just want to criticize because they care more about their poll numbers than they do American lives."

### Oneliner

Beau examines invalid and valid criticisms of the US exit from Afghanistan, stressing the challenges of securing the capital and the importance of effective messaging in humanitarian operations.

### Audience
Policy analysts

### On-the-ground actions from transcript

- Contact senators and members of Congress to pass legislation addressing evacuation powers (suggested)
- Advocate for clear and informative messaging in humanitarian operations (implied)

### Whats missing in summary

The full transcript delves into the nuances of the US exit from Afghanistan, providing valuable insights on strategic errors, operational challenges, and the importance of effective communication in such operations.

### Tags

#USexit #Afghanistan #Critique #Messaging #PolicyAnalysis


## Transcript
Well, howdy there, Internet people. It's Beau again.
So we're out.
The United States
has left.
The twenty-year mission
is over.
Now
is the appropriate time
to level critiques and criticism.
So we're going to do that.
Today we are going to focus mainly on the exit. We're going to talk about
invalid and valid criticism.
We're going to start with the invalid because it is gaining steam
and it is...
it doesn't make any sense.
So we're going to walk through it.
If you missed it, a report came out
that says the opposition there, now the de facto government, offered the capital
city to the United States, said you guys secure it.
And the Biden administration said no.
This is being
spun into the outraged juror
by kind of implying that
if the Biden administration had done this
and secured the capital,
well then that thing at the airport wouldn't have happened.
That kind of overlooks the fact that the reason the thing at the airport happened
is because we really didn't have enough troops to secure the airport.
It seems unlikely that we would have been able to secure the entire city.
I understand
that to most pundits in the United States
Afghanistan is a bunch of dirt and villages.
The capital city has more than four million people in it.
Securing it
is
difficult
to say the least.
We all saw
how many troops it took to secure the capital building
in the United States.
Now imagine
a city of four million.
The reason the opposition
offered it is because they didn't have the troops to secure it either.
If anybody is suggesting
that this was a good idea,
this is a 100 percent indication
that they have no business
providing quote expert advice.
Either
they don't know what they're talking about or they are politicizing it.
And this in and of itself
I cannot think of a single thing
that has come out of the
cable news network experts
that is more certain
to exponentially grow
the number of American lost.
The manpower wasn't there.
Aside from that
these people who are indicating this was a good idea
still don't understand what was happening. It was a withdrawal.
The United States wasn't waging war anymore.
Seizing the capital
doesn't really make a whole lot of sense.
It just gives multiple
locations for things to go wrong. It creates even more moving parts
in an operation that already had a whole lot.
This was not a good idea.
The Biden administration made the right call.
The war at that point
was over.
There's no sense
in wasting
more
in pursuit of
I'm not even sure.
Totally invalid criticism
in case I haven't made that clear.
So what are the valid ones?
Because there definitely are some.
Let's go through
what they look like by using
tactical, operational, and strategic examples.
On the tactical level,
when the thing at the airport happened,
my understanding is that a whole lot of American troops
ran in to be heroes.
They ran in
and immediately
started administering aid.
And to a lot of people at home right now,
probably like, good, that's what they're supposed to do.
It's actually not.
A very common tactic is to
create something like that,
and then when the responders show up,
do it again.
That training
has apparently been lost. That is probably something
that should be
reviewed
because we will
continue
to engage in these sorts of conflicts.
So that's something that should probably be
reinstilled.
The primary
objective at that point is to secure everything
and then
administer aid.
Okay, so
that's tactical, low-level.
Now we're going to go to operational.
The perimeter should have been pushed back.
The perimeter should have been pushed back. Now you can't push back to encompass the
whole city
because
you don't have the manpower.
But the more distance
between the perimeter
and the actual location where things are happening,
the better.
Now, is there a reason they didn't push it back further?
Yeah, the further out you push the perimeter, the more spaced out your troops are.
I can say this in hindsight because I know
that there's not going to be an on-the-ground push
to take the airport.
The commander there,
did they know that?
No. They could have suspected that
because
the offer
from the opposition, now the de facto government,
to
allow the United States
to keep the capital, to secure the capital,
indicates that they're not looking for a fight.
Which, that, by the way, is the appropriate take on that.
It actually backs up the whole reason why
the Biden administration has a little bit of faith in how they're going to behave.
So
you could have taken that as a sign, but if you're a commander, your job is to mitigate risk.
So
you don't want to space your people out too far.
You don't want them too thin.
I think it could have been further out.
But, also, it wasn't there.
At an operational level, that seems like the big one.
At a strategic level,
and this, to me, is the single most important thing and the biggest mistake the Biden administration made,
they had bad messaging throughout.
Bad messaging throughout.
And that may not seem important, but it really is.
Because this switched
from a hasty withdrawal
into a humanitarian operation.
But they couldn't get people behind it.
At those press conferences,
and I'm not going to lie, I actually like watching her,
you know,
some of the people in the room.
It is entertaining.
But
the goal of those press conferences should be to inform the American public.
There was a lot of information that went out
from
experts
who
may or may not know what they were talking about
that wasn't accurate.
And that could have been
mitigated
at those press conferences
by really informing
the American people rather than just creating soundbites.
Have a little faith
in the press.
Inform the press
and allow the press to inform the people.
That seems like a huge error.
And then there were a lot of people talking.
Because I know
this person won't get their feelings hurt by it.
The Secretary of Defense
said something that just,
I couldn't believe he said it.
He said that the United States didn't have the capability
to go use helicopters
to get people.
That was horrible messaging.
It was horrible.
Because pretty much every conservative American
knows that the United States does in fact have that capability.
The desirability of not using that capability wasn't explained.
The fact that it wasn't a good idea to go do it
wasn't explained. Instead it was just, well, we can't do that right now.
That type of
bad messaging
was what led to people saying, well, Biden just doesn't care.
It was mistakes like that.
When you are mounting an operation of this size, you have to have public support.
And during this,
you had people in Congress sharing opposition propaganda.
And yeah, a lot of that's on them.
It is on them for either not understanding what they were doing,
or they were quite literally rooting for U.S. troops to get hurt
so they could get political talking points.
And I'm sure that there are some on both sides of it.
They're both options.
I think that proper messaging could have helped a lot here.
So those are three criticisms that are very valid,
that can be learned from,
that could be changed, that might save people in the future.
I know that people want to talk about one specific thing,
the last U.S. combat operation in the country before the withdrawal.
That's going to get an entirely separate video, because it was very
tragically poetic.
The last combat operation in the country caused civilian loss.
It ended as it was wrong.
And we will talk about that separately.
One of the things that people really need to remember is that
despite the chaos, despite the tragedy,
despite people talking about things they don't understand,
this went really well.
An operation of this size, this went exceedingly well.
That's not to say that things didn't go wrong.
That's not to say that there weren't tragic incidents.
But the operation itself was a success.
Anybody who is saying differently doesn't know what they're talking about.
One other thing that I think kind of is an afterthought that the administration
might have done that would have helped early on is when they started coming out
and saying, well, why didn't the Biden administration evacuate them?
That could have been a moment for the Biden administration to come out and say,
hey, sure, I call on the senators who made these claims,
the members of Congress who made these claims,
I call on them to pass legislation that allows me,
the President of the United States, to force Americans to leave a foreign country at my will.
Because that's what they're saying he should have done,
something he doesn't have the power to do.
Put them on the spot.
If they really believe the President of the United States should have that power,
well, then they should draft a law.
But they don't actually believe he should have that power.
They just want to criticize because they care more about their poll numbers
than they do American lives.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}