---
title: Let's talk about two Florida tough guys....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PYJ3xN1Q8u0) |
| Published | 2021/08/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defines tough guys as those willing to do the hard right thing over the easy wrong thing, not necessarily the traditional image.
- Describes the failure of mitigation efforts in Florida and the resulting stress on medical professionals.
- Shares his wife's experience as a nurse who returned to work due to the shortage of medical staff in Florida.
- Hails individuals like his wife and Nikki Freed, Secretary of Agriculture, as tough guys for stepping up and providing leadership during challenging times.
- Contrasts genuine tough guys, like medical professionals and leaders advocating for public health measures, with those who pose as tough but fail to take basic precautions themselves.
- Stresses the importance of true leadership in the face of widespread misinformation and the need for more people to step up and provide guidance.
- Emphasizes that toughness is not about appearances or symbols like guns, but about doing what is right even when it's difficult.

### Quotes

- "What makes you tough is the willingness to step up and do the right thing when others won't."
- "We need more tough guys willing to step up, to lead, to provide the guidance that the country needs."

### Oneliner

Beau defines tough guys as those willing to do the hard right thing over the easy wrong thing, applauding medical professionals and leaders like Nikki Freed for providing genuine leadership during challenging times.

### Audience

Community members, activists, voters

### On-the-ground actions from transcript

- Support medical professionals by volunteering at hospitals or clinics to alleviate staffing shortages (implied)
- Advocate for public health measures like wearing masks and getting vaccinated to protect the community (implied)

### Whats missing in summary

The full transcript delves deeper into the challenges faced by medical professionals due to the failure of mitigation efforts in Florida and the importance of true leadership in combating misinformation and saving lives. 

### Tags

#Leadership #PublicHealth #CommunitySupport #Misinformation #Florida


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about tough guys.
You know, when you say that, tough guy,
you get an image.
You know, that phrase means different things
to different people.
To me, it's always meant
somebody who's willing to step up,
do the hard right thing
over the easy wrong thing.
Somebody who's not afraid of competition,
willing to get in there and fight,
willing to lead,
put others above themselves.
To me, that's a tough guy.
So today,
we're going to talk about two tough guys in Florida.
The mitigation efforts
here in Florida, as has been widely covered all over the news,
well, they're
they're an abject failure.
There's no other way to put it.
They are a failure.
Because of that,
medical professionals in Florida
have been stressed
for a while.
My wife,
she's been a nurse
seventeen, eighteen years.
A couple months ago, stepped away from the bedside.
Tired of watching people go needlessly
because of misinformation,
because they won't take the
the basic precautions
that are necessary.
She's not the only one, you know.
The burnout in the medical professional community is real.
About a week ago,
chief nurse from a
a local hospital
gave her a call,
said that
needed help.
They just don't have the nurses
to run the floors anymore.
My wife's on the schedule the next day.
Tough guy. Somebody willing to step up,
do the hard right thing
over the easy wrong thing.
Somebody who's not afraid of competition, in this case, competing with the grim
reaper himself.
Tough guy.
Probably not what you were thinking.
You know,
in the state government here,
you've got one person
who is consistently
out there saying, wear a mask,
get vaccinated,
putting forth the effort
to help her community,
to provide that leadership,
to do the
hard right thing
over the easy wrong thing.
Because she's a Democrat.
She's a Democrat. Nikki Freed,
secretary of agriculture.
She is likely
to be running against DeSantis
come
election time.
Not afraid of the competition.
You know, I'm in a deep,
deep red area here.
I know the liberals in the area,
and I know the
three leftists.
They're all vaccinated.
They're all masked.
She's out there encouraging
people who will likely vote against her
to get vaccinated,
to take those basic precautions
so she can help the community,
so she can lead, right?
Knowing that it's going to make that competition
tougher
come election time.
Because she's not afraid of the competition,
willing to fight.
Tough guy.
Now, all over the country
you have a bunch of
people posturing as tough guys.
Posing
with rifles, wearing plate carriers. Politicians trying to cast that image
that they're a leader,
that they're a tough guy,
that they'll fight.
But when it comes down to it,
the reality is
they're too weak to wear one of these.
They can't handle wearing a mask.
I find that concerning.
I don't think these are people
that
really have it in them
to lead.
Not through what this country's going to be facing.
We need more tough guys.
We need more people willing to step up,
to lead,
to provide the guidance
that
the country needs.
Because there is so much misinformation out there.
It is literally
costing people their lives.
When you picture tough guy,
you need to forget the idea of tattoos
and guns.
None of that makes you tough.
What makes you tough is the willingness to step up
and do the right thing
when others won't.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}