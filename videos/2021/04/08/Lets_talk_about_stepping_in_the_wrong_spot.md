---
title: Let's talk about stepping in the wrong spot....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NwjBG4PsqRQ) |
| Published | 2021/04/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of Princess Diana but shifts to discussing the Biden administration's consideration of continuing the use of landmines.
- Describes a hypothetical scenario of stepping on a landmine while exploring a peaceful area due to remnants left behind by the U.S.
- Questions the necessity of the U.S. continuing to use landmines, arguing that they serve no purpose and are antiquated.
- Points out that conventional conflicts, like the one depicted in M*A*S*H, are no longer common, making the use of landmines even more unnecessary.
- Argues that drones are a more effective and less harmful alternative to landmines in denying access to an area for the opposition.
- Advocates for the discontinuation of landmines, citing the potential dangers they pose even after conflicts have ended.
- Emphasizes that leaving behind tools like landmines for the opposition is illogical and unnecessary.
- Urges for the immediate removal of landmines without the need for further debate or consideration.
- Raises concerns about the risk posed to troops by leaving large quantities of landmines behind.
- Calls for action to remove and discontinue the use of landmines without delay.

### Quotes

- "We shouldn't. Period. Full stop. End of story."
- "There is no reason to keep these things. It needs to go away."
- "Seems like handing your opposition tools doesn't make a whole lot of sense."

### Oneliner

Beau argues for the immediate discontinuation of landmines, stressing their obsoleteness and potential harm even after conflicts have ended.

### Audience

Advocates and activists

### On-the-ground actions from transcript

- Contact your representatives to advocate for the immediate discontinuation of landmines (implied).
- Support organizations working towards a global ban on landmines (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the unnecessary nature of landmines in modern conflicts and the urgent need for their discontinuation.

### Tags

#Landmines #BidenAdministration #Conflict #Drones #Advocacy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a whole bunch of things.
We're going to talk about a whole bunch of things.
But first I want to point out there will be a video down in the comments.
It's about Princess Diana.
Definitely worth watching.
We'll add some additional context
to this conversation.
Now I want you to imagine you are seventeen, eighteen years old.
And you are on that coming of age adventure.
You're in a far away land.
You're born there, you grew up there.
But now you're out exploring.
You're old enough.
You are Jack Kerouac.
You're on the road.
And you're thinking about how it used to be when you were a kid.
During conflict,
whatever that conflict was,
and how you're so glad that there's peace now.
The U.S. is gone,
and there is peace.
And as you're walking along this road you see this perfect hill, one tree, looks
like something out of a movie.
And you're thinking, yeah,
I'm going to go up there and eat.
Tote your backpack up there,
find the perfect spot.
But along the way you step on another spot.
And you lose your leg,
if you make it,
because the U.S. left landmines behind.
Right now the Biden administration is reviewing
whether or not the United States should continue
to use landmines.
There's no review necessary for this.
We shouldn't. Period. Full stop. End of story.
There aren't a whole lot of countries that still use them.
DOD's rationale
is that, you know, in a conventional conflict
we may need
these devices.
Conventional conflict, for those who don't know, that's
mash.
Most people are familiar with that. The lines.
That's what you're talking about.
Uniformed force versus uniformed force.
Kind of static.
That's a conventional conflict.
Interestingly enough,
mash was set during Korea, and
that was pretty much the last conventional conflict of any duration
the United States really engaged in.
Doesn't happen like that anymore.
Things are different.
And even moving to near piers,
it would be conventional for a very short burst,
and then it would become unconventional, asymmetrical, whatever the new buzzword is.
We don't need them.
The whole purpose of these things is to deny the opposition access to an area.
We don't need to do that from the ground anymore.
We have drones,
and we don't leave them behind.
They don't continue
to indiscriminately harm after we've left.
We don't need these things.
They're antiquated.
They serve no purpose.
And I know
somebody's going to say, well, we need to make sure the troops have every tool they need.
Ask your buddies who were over there.
Ask your buddies who were over there involved in the recent unpleasantness.
Ask them
who the most high-value captures were,
and what did they make.
They improvised stuff.
What did they make it out of?
Maybe leaving a whole field of the stuff behind
isn't such a good idea.
Maybe putting it out there to begin with isn't.
Seems like handing your opposition tools doesn't make a whole lot of sense.
There is no reason to keep these things.
It needs to go away. I understand it's under review,
but during the review,
we're keeping them, basically.
There's no need for a review,
and there's no need to keep them.
They need to go away.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}