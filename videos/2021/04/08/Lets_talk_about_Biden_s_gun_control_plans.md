---
title: Let's talk about Biden's gun control plans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9n4jYXausyM) |
| Published | 2021/04/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- President Biden's announcement on gun control is a mix of executive orders, legislative proposals, and an appointment.
- Criticism arises that the executive orders do not go far enough, but Beau argues against further expansion due to limitations of presidential authority.
- The executive orders primarily focus on unregistered firearms and stabilizing braces on pistols, which Beau deems as ineffective measures.
- Beau suggests that the issue of homemade firearms is complicated by the nature of gun components and advancements in technology like 3D printing.
- He explains the function of stabilizing braces and dismisses claims that they significantly enhance a pistol's lethality.
- Beau expresses skepticism about the potential effectiveness of the executive directive to develop red flag law legislation at the state level.
- In terms of legislative proposals, Beau sees hope in addressing domestic violence-related gun violence but doubts their passage in the Senate.
- He criticizes the limited scope of proposed legislation and its connection to broader gun control themes that may face significant opposition.
- Beau predicts challenges in confirming the Biden administration's ATF director appointment due to past associations and political dynamics.
- Overall, he concludes that the announced measures are largely ineffective and unlikely to satisfy any side of the gun control debate.

### Quotes

- "Nobody on any side of this issue is going to be happy with this."
- "Thoughts and prayers doesn't mean a thing."
- "It's all pretty much ineffective."
- "This is a miss from the Biden administration."
- "At the end of the day, nobody should be happy, nobody should be mad."

### Oneliner

Beau explains why President Biden's gun control announcement falls short and is deemed ineffective by all sides involved.

### Audience

Advocates for effective gun control.

### On-the-ground actions from transcript

- Contact your representatives to advocate for comprehensive gun control measures (suggested).
- Stay informed about legislative developments related to gun control and advocate for meaningful change in your community (implied).

### Whats missing in summary

In-depth analysis and further context on the nuances and implications of the proposed gun control measures.

### Tags

#GunControl #BidenAdministration #ExecutiveOrders #LegislativeProposals #ATFDirector #DomesticViolence #Advocacy


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about President Biden's big announcement,
his big push, that big show that went on up there.
And whether or not it matters,
we're going to talk about the various components of it.
There are three sections.
There's the executive stuff, the legislative stuff,
and then the appointment.
I'll go ahead and tell you now,
it doesn't matter where you stand on this issue,
this isn't what you wanted.
This is not what you wanted.
Nobody on any side of this issue is going to be happy with this.
A lot of this is just feel-good stuff.
Okay, so first we'll start with the executive stuff.
There's already criticism that it doesn't go far enough.
President Biden should have gone further with his executive orders.
No, he should not have.
No, he should not.
He doesn't create law.
That's not how this country is supposed to work.
The President of the United States is not a dictator.
The criticism that the executive orders don't go far enough,
that is, that's not valid.
But there is some valid criticism.
The first two pieces we're going to talk about,
to sum them up, it is thoughts and prayers.
Completely ineffective, does not matter at all.
The first one is going after the unregistered stuff,
the ghost stuff, the homemade stuff.
All right.
To get a grasp on why anything they do in this field is just not going to matter,
you have to understand how it works.
We talked about it in the first series on gun control.
The receiver is what matters.
Under the law, that's the gun.
Okay?
The receiver is what matters.
All other pieces, all other parts go into that.
When people build one at home, they make that.
It doesn't have a serial number.
Putting serial numbers on parts is pretty pointless at that point.
There is also the idea that it may result in parts kits no longer being sold.
That doesn't change a thing.
They'll end up buying the barrel assembly in one place,
the trigger assembly somewhere else.
When it comes to this topic, if you are for gun control, hang it up.
Hang this up.
Get this out of your list of priorities.
The milk is spilt.
The genie is out of the bottle.
Pandora's box is opened.
With the automation and metalworking and 3D printing,
this is not something that can be addressed by addressing the item.
You have to address the motive.
This is going to be completely ineffective.
The next one is stabilizing braces on pistols.
I saw a major outlet say that it takes a pistol and turns it into a more lethal rifle.
No, it does not.
No, it does not.
It is quite literally a piece of plastic that attaches to the rear of the pistol
to stabilize it up against your arm.
Now theoretically, you can shoulder it and then fire it like a rifle.
Maybe if you've trained, that makes you more accurate,
but when we're talking about the incidents that we...
This is all designed to address.
Accuracy isn't really a thing.
Aside from that, I want to point out yet again, it's just a piece of plastic.
In some cases, metal.
Most of them have zero moving parts.
You don't even have to know how to work with plastic or work with metal
if somebody really wanted to.
This could be done with a skill saw and would.
Completely ineffective.
Thoughts and prayers doesn't mean a thing.
The third executive piece.
This may actually be okay.
We don't know yet.
It's directing DOJ to come up with model legislation for the states to use for red flag laws.
I have a whole video on these laws.
I don't want to rehash the whole thing, but short version.
The idea of getting firearms out of these people's hands.
Yeah, pretty much everybody's okay with that.
The way it's done is what matters, and we're not going to know the details until the model
legislation comes out.
So on that one, maybe, maybe if you're pro gun control, maybe you get something there.
Okay.
So the legislative stuff.
There there's hope.
The Biden administration clearly recognizes the tie to domestic violence.
Awesome.
It is the one area that they don't go far enough in.
If you want legislation, that's where you need to focus.
That's data driven.
That is what's going to matter.
And it's the one area where basically they want to bring back some old laws or I guess
renew some old laws that while good, don't go far enough.
They're not effective because of their limited scope.
And then there's some other stuff that he's going to call for, but it's realistically
it's not going to get through the Senate.
And now that the DV stuff is tied into a wider gun control theme, it may not get through
either.
Republicans are going to vote against it.
And there are a bunch of pro gun Democrats.
So I don't have a lot of hope for any of the legislative stuff getting through.
And the one thing that I think is actually going to matter didn't go far enough.
Okay, then you have the appointment.
The appointment for director of ATF.
The facts of this don't matter in this case.
They really don't.
The person that he has selected is tied to the events at Waco.
As far as the right wing is concerned, that's all they need to hear.
It doesn't matter if this guy just carried a file, he's tied to that event, they're
going to be against him.
So he's going to have a very hard time getting confirmed.
He's going to have a hard time getting through.
So that's where we're at.
That is where we're at.
At the end of the day, nobody should be happy, nobody should be mad.
I know the right is going to flip out over all of this stuff.
It's all pretty much ineffective.
I don't know why they would get mad.
It's not actually going to do anything.
But there's just the idea that it's gun control, so they'll oppose it.
This is a miss from the Biden administration.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}