---
title: Let's talk about what that caucus platform tells us.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OOvBGdAbfOk) |
| Published | 2021/04/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Talks about a leaked document and the importance of not dismissing it as a joke.
- Expresses concern over an outside group influencing Congress with concerning documents.
- Questions the silence from the Republican Party regarding the platform filled with racist content.
- Points out the lack of response from colleagues in the party and the potential political motivations behind it.
- Expresses fear that Biden's win may not have been decisive enough to reject Trumpism.
- Warns about the dangers of underestimating the influence of groups promoting authoritarianism.
- Stresses the need to stay politically active and vigilant against such ideologies.

### Quotes

- "You can be outraged at the content. You should be."
- "You have to stay politically active because those who want that brand of authoritarianism, they are still out there."
- "I want to know who this group is."
- "It could be lifted from any of the most horrible groups throughout history because it's the same strategy."
- "That's when really horrible things happen."

### Oneliner

Beau stresses the danger of dismissing concerning documents, warns against underestimating authoritarian influences, and urges staying politically active against such ideologies.

### Audience

Politically active citizens

### On-the-ground actions from transcript

- Stay politically active and vigilant against authoritarian influences (implied).

### Whats missing in summary

Full understanding of the implications of dismissing concerning documents and the necessity of political vigilance against authoritarian ideologies.

### Tags

#Warning #Authoritarianism #PoliticalActivism #RepublicanParty #Trumpism


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about that would-be caucus
that may or may not happen in the future now
it's kind of up in the air, and what it means
because there's a lot of people who
want to dismiss it, take it as a joke
or just say that it's not that important because
oh, it's just that group of people and that's just the way they are and all of that stuff.
I'm going to suggest that's a really bad idea.
I'm going to suggest that's very unwise.
If you don't know what I'm talking about, Green,
to hear her tell it, had a
document that was leaked by her staffers. It wasn't ready yet.
It wasn't ready to go out to the public and maybe she hadn't even read it and maybe
it was developed by, you know, some outside group and she
just didn't even know. Sure, let's pretend we believe that for a moment.
That's worse. That's worse. If it had just been her,
sure, she's the space laser lady.
Kind of grown to expect it from her, but the idea that it's some
outside group that's willing to put out a document like that that has the ear of
people in Congress,
oh, that's a concern. I want to know who that group is.
I want to know who else in Congress they talked to.
That didn't answer any questions. It created more
and they're concerning ones. The other concern
is the deafening silence that came from the Republican Party once that platform
surfaced.
Just a bunch of overt racist garbage and
the overwhelming majority said nothing. Her colleagues,
they said nothing. Sure, you had some
in the leadership step forward and say, oh, this isn't us. We're the party of
Lincoln or
whatever, but most were dead quiet
because what that tells me
is that had it been received well,
had it been politically advantageous for them to be okay with it,
they would have been. If they thought it helped their career,
that's a concern. You know, before the election,
I said my biggest fear was that Biden wins,
but not in a landslide. He doesn't win big enough to send the message
that Trumpism isn't politically tenable.
It's not a winning strategy. That didn't happen. He didn't win big,
not by big enough numbers to send that message.
And then the other part of the fear was that because he won,
people would think it was over. This document shows it's not.
My big concern was that all that happened
and then the next person who picked up the mantle of Trumpism
would be smart. They wouldn't
broadcast every move on Twitter. They'd be effective.
I want to know who this group is.
It's the same stuff. It could be lifted from any of the most horrible groups
throughout history
because it's the same strategy. You take the majority,
you get them kicking down at some group that you can easily marginalize
and that motivates that base. The thing is,
once that group gets in power, the people using that strategy,
that majority that they energized, that base,
they're going to demand that those in power do something
about that scapegoated group. And that's when really horrible things happen.
I would suggest that dismissing this
as the space laser lady,
that's a bad idea. We do that at our own peril.
You can be outraged at the content. You should be.
But you should also take it as a warning that you have to stay politically active
because those who want that brand of authoritarianism,
they are still out there. And apparently,
they have the ear of people in Congress. Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}