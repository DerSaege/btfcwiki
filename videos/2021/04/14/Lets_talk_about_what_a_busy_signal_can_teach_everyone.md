---
title: Let's talk about what a busy signal can teach everyone....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wHPn-3mjxZM) |
| Published | 2021/04/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau receives a video request from someone stating that he is good at creating compelling arguments that prompt people to action.
- The urgent need for people to get vaccinated is emphasized as appointments are being missed and time slots left unfilled.
- Beau considers various approaches to encourage people to get vaccinated, including live-streaming his second shot and using persuasive arguments.
- Instead of the traditional approaches, Beau shares a powerful analogy about calling a number for federal reimbursement for a loved one's final expenses due to the public health crisis, contrasting it with the ease of scheduling a vaccine appointment.
- He underlines the importance of getting vaccinated to avoid being a burden on loved ones who might have to seek reimbursement for final expenses.
- Beau provides details about the eligibility criteria for federal reimbursement for final expenses, including the necessity for the incident to have occurred in the US after January 20, 2020.
- He points out the stark contrast between the half-million people calling for reimbursement and the availability of vaccine appointments with operators ready.
- Beau encourages viewers to get vaccinated promptly to protect themselves and others from potential health issues.
- The analogy of a busy signal for federal reimbursement versus operators standing by for vaccine appointments serves as a strong call to action for getting vaccinated.
- He ends the video with a reminder to take the necessary steps to ensure no one has to call the reimbursement number for their final expenses due to the public health issue.

### Quotes

- "The urgency is lost."
- "But you can go get your shot and make sure that nobody has to call that number about you."

### Oneliner

Beau underlines the urgency of getting vaccinated by contrasting the ease of scheduling a vaccine appointment with the arduous process of seeking federal reimbursement for final expenses due to the public health crisis.

### Audience

Public Health Advocates

### On-the-ground actions from transcript

- Schedule a vaccine appointment to protect yourself and others (exemplified)
- Share Beau's analogy about the importance of vaccination with others in your community (suggested)

### Whats missing in summary

The emotional impact of Beau's analogy and the urgency of getting vaccinated are best experienced by watching the full video.

### Tags

#Vaccination #PublicHealth #CallToAction #Urgency #CommunitySafety


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to do something a little bit different.
We have a requested video.
The person started off by saying,
you know you're really good
at creating compelling arguments that prompt people to action.
I know when I'm being buttered up, what do you want?
And yeah, they wanted something.
They feel the urgency has been lost.
They feel the urgency has been lost.
This person, they administer the shot.
They administer the vaccine.
And right now there are appointments being missed
or time slots not even being taken.
The urgency is lost.
So I sat there, tried to come up with good arguments
to promote the idea of going to get your shot.
Thought about maybe they'd let me live stream me getting my second one.
Thought about going over the same arguments I used to promote
wearing a mask, you know.
Responsibility to your neighbor,
provide for the common defense, promote the general welfare, all that stuff.
Thought about pulling a Trey Crowder and just saying, if you don't get it you're scared.
Or going over the medical information, but the pros are going to do that.
I decided on something else.
When's the last time you heard a busy signal?
Like on a phone, a busy signal?
Probably been a while.
For me it's been years.
Michael, Hurricane Michael was the last time I heard one
when all the phones went down.
But you can hear one right now.
If you call 844-684-6333, your busy signal will be put on hold for up to nine hours.
That's the number you call if you want the federal government to reimburse you
for the final expenses of a loved one lost to the current public health issue.
There's going to be half a million people calling that line.
Busy signal to be reimbursed for the final expenses.
But to schedule an appointment for a vaccine, operators are standing by.
There's probably an issue with that.
Now if you are going to call that number, if you would like to be reimbursed to qualify,
it has to have occurred in the United States or its territories.
The expenses must have been incurred after January 20, 2020.
And the certificate must indicate the cause.
It has to be attributed to that or symptoms like that.
And the applicant to be reimbursed has to be a US citizen or legal resident.
That number, to call and do that, you're going to get a busy signal
or be on hold for up to nine hours.
But you can go get your shot and make sure that nobody
has to call that number about you.
And they're waiting for you.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}