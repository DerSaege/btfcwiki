---
title: Let's talk about California needing to get ready....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rFcUfedhrbA) |
| Published | 2021/04/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing a heads up for California, discussing the similarities and differences between hurricanes in Florida and wildfires in California.
- Mentioning the challenges of getting advance warning for wildfires compared to hurricanes.
- Explaining the concept of fuel moisture content as a measurement of combustibility.
- Describing how scientists measure fuel moisture content by taking clippings, weighing them, letting them dry, and weighing them again.
- Noting that the fuel moisture content in California is currently at its lowest observed level ever.
- Quoting San Jose State University Fire Weather Lab describing the situation as terrifyingly low.
- Pointing out the impact of two rainy seasons without much rain in California, leading to extremely dry conditions.
- Stating that fire loves dry material, hinting at the high risk due to the abundance of dry vegetation.
- Emphasizing the importance of being prepared for wildfires by having a packed bag, knowing where documents are, and having a plan for evacuation.
- Urging people to be prepared, especially considering the potential rapid onset of wildfires due to lightning strikes.

### Quotes

- "Terrifyingly low."
- "Fire loves dry stuff."
- "Be ready and get ready now."
- "When it comes to stuff like this, minutes may matter."
- "Be aware of this situation."

### Oneliner

Beau provides a warning about the extreme wildfire risk in California due to record-low fuel moisture content and urges immediate preparedness for potential rapid onset events like lightning strikes.

### Audience

California residents

### On-the-ground actions from transcript

- Prepare a bag with essentials, locate documents, and make an evacuation plan (implied).
- Ensure vehicles have gas and be aware of the potential impact of lightning events (implied).
- Stay informed about the wildfire situation and follow official bulletins and updates (implied).

### Whats missing in summary

Detailed instructions on creating a family emergency communication plan.

### Tags

#California #Wildfires #Preparedness #EmergencyPreparedness #ClimateChange


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about California,
provide a little heads up for California.
We're going to talk about measurements, wildfires,
and being ready.
Here in Florida, we have hurricanes.
And they're a lot like wildfires in some ways.
If they're coming and they're big enough,
pretty much the only thing you can do is get out of the way.
The difference is we get a lot of warning, days of warning,
saying, hey, this thing's on its way.
You don't always get that with wildfires.
Scientists have figured out how to measure and therefore
observe just about everything.
A measurement that I recently found out about
is called fuel moisture content.
So ratio determines how combustible things are,
how easy it is for something to burn.
The way they get this measurement is they go out
and they take clippings.
They bring them back.
They weigh them.
They wait 24 hours for them to dry.
And then they weigh them again.
And that gives them this ratio.
It is currently the lowest observed ever.
Terrifyingly low is a quote.
San Jose State University Fire Weather Lab
described it as grim, not to put too fine a point on it.
Basically, California went through two rainy seasons
without a lot of rain.
Things are very, very dry.
Somebody recently told me that fire loves chaos.
No, fire loves dry stuff.
And apparently, there is a whole lot of it out there.
These measurements came from the Santa Cruz Mountains
in particular.
So it's shaping up to be pretty bad.
What can you do?
Be ready.
Be ready and get ready now.
Make sure you have a bag packed.
Make sure that you know where your documents are.
Have a plan.
Have a plan for those people who may not be at that location.
And you're going to have to meet up somewhere else.
Make sure vehicles have gas in them.
Be very aware of lightning events,
because that seems to be a major factor in it.
When it comes to stuff like this, minutes may matter.
So get ready now.
Be aware of this situation.
There's a, if the lightning strike is near you,
there may not be a lot of warning.
You may not have time to get ready then.
So do it now.
Be aware of this situation.
And it's shaping up to be pretty bad.
They're putting out stories and bulletins about it now.
So if you are in this area or a similar one, get ready.
Make sure you have your stuff together.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}