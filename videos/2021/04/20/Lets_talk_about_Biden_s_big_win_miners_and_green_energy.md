---
title: Let's talk about Biden's big win, miners, and green energy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KZTafuVNFIY) |
| Published | 2021/04/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden secured a big win for green energy policies with the support of the United Mine Workers of America.
- The union leadership negotiated for funds for transitioning and training miners into green energy jobs.
- Miners are not ignorant; they understand their union's power through collective bargaining.
- The miners' union power relies on their numbers, which have been declining due to job losses in the coal mining sector since 2012.
- Waiting to negotiate for funds in the future may result in the loss of leverage for the miners.
- The media should avoid portraying miners as ignorant and instead recognize their understanding of collective bargaining.
- Insulting miners can have negative consequences for both them and green energy initiatives.
- Union members will access funds for retraining and transitioning, not the union itself.
- Academic perspectives often overlook the real-world understanding of working-class individuals.
- The importance of explaining the benefits to miners based on their current collective bargaining power.

### Quotes

- "Instead of casting them as ignorant hillbillies, perhaps talk to them as people who understand the power of collective bargaining because that's who they are."
- "Keep insulting them and it's going to backfire on everybody."

### Oneliner

President Biden's win for green energy policies through the United Mine Workers of America shows the power of collective bargaining and challenges media stereotypes of miners.

### Audience

Working-class communities

### On-the-ground actions from transcript

- Advocate for fair treatment and representation of working-class individuals in media (implied)
- Support union movements for transitioning workers into sustainable job sectors (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the importance of recognizing the power and understanding of working-class communities in collective bargaining and transitioning to new job sectors.


## Transcript
Well, howdy there, internet people. It's Beau again. So today we're going to talk about
President Biden getting a pretty big win, green energy, and elitist attitudes. So if
you don't know, President Biden got a pretty big win in his pursuit of green energy policies.
The United Mine Workers of America, the leadership of that union, has said, alright, we'll throw
our support behind this. Now of course there was a trade-off. It's politics. They get money
for transitioning and training to get the miners new jobs in the green energy sector.
They want their current union members to be able to get good paying jobs. Makes sense.
And nobody in America is going to have a problem with that. Now the union leadership, they
have to convince the union members that that's in their best interests. The media is casting
that as if it is going to be some really hard thing to do. It would just be great if the
media didn't cast miners as ignorant people. They're not ignorant people. This union, at
this moment, they have power. They have power because they have numbers. These miners know
that since 2012, about half the coal mining jobs have disappeared. They know that trend
is likely to continue. They know that their power through their union relies on numbers.
They know that once those numbers drop too low to swing an election, to have influence
over an election, no politician is going to look out for them because politicians don't
really care about the worker. They care about getting re-elected. So right now, they have
the power to get money for retraining and transitioning. If they wait another five or
ten years, they probably won't because they don't have that leverage. They won't have
the numbers to exert any influence over an election. They know that. That's why they're
in a union. Instead of casting them as ignorant hillbillies, perhaps talk to them as people
who understand the power of collective bargaining because that's who they are. I'm going to
suggest that if the media, in their desire to push green energy policies, continues to
cast miners as ignorant, it's going to backfire because they're not dumb, but they are pretty
proud. Keep insulting them and it's going to backfire on everybody. It's going to hurt
the miners and it's going to hurt green energy, all because of some classist, elitist attitude.
That's not the talking point you're looking for. It's going to be really hard to get miners
to look out for their own interests. I'm going to suggest that's a bad move. At the end of
the day, the deal, the idea that a union is going to get money or there's going to be
money available. I guess the union's not going to get the money. Union members will be able
to access money to get retraining and transitioning. That's a pretty big win for them too. That's
how it needs to be explained. Right now, because of their numbers, they have the power to get
this through collective bargaining. If they wait too long and the coal mining jobs disappear
and therefore union membership declines, they won't have that power. They know this. There
are a lot of times when academics refuse to see working class people as people who understand
the real world. Oftentimes, it works to the detriment of everybody. Anyway, it's just
a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}