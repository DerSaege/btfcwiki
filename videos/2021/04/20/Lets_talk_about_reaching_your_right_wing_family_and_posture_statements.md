---
title: Let's talk about reaching your right wing family and posture statements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iOH8iARwFZo) |
| Published | 2021/04/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- USStratCom, the government account in charge of US nukes, tweeted a vague statement that was misconstrued by right-wing personalities.
- Instead of providing context, the right-wing capitalized on fear, spreading messages of imminent nuclear war.
- The tweet in question was actually a preview of the posture statement, a routine occurrence justifying the organization's existence.
- The annual testimony occurs to address the possibility of other countries using nukes in extreme scenarios.
- Some ex-military personnel also contributed to fearmongering, despite likely understanding the context behind the tweet.
- Misinformation on social media led thousands of Americans to believe in the false narrative of impending nuclear war.
- Beau stresses the importance of addressing and alleviating fears with facts to have productive dialogues with those influenced by fear-based narratives.
- He warns against the spread of baseless fear, particularly on social media platforms.
- Beau urges people to understand and counteract the fear-driven motivations of certain groups, particularly on the right wing.
- The irresponsible actions of some individuals on Twitter caused unnecessary panic and anxiety among the public.

### Quotes

- "The thing is, some of the people who retweeted it with these statements, they're ex-Secret Service, ex-Air Force."
- "You have to alleviate that fear or you're not going to get anywhere."
- "There are thousands of Americans right now who feel that we might be on the brink of nuclear war."

### Oneliner

Beau clarifies a routine government tweet about US nukes misconstrued as a harbinger of nuclear war, stressing the need to counter fear with facts.

### Audience

Social media users

### On-the-ground actions from transcript

- Alleviate fears with facts (implied)
- Address misinformation with accurate information (implied)

### Whats missing in summary

The full transcript provides a detailed account of how misinformation and fear-based narratives can spread rapidly online, impacting public perception and creating unnecessary panic. Watching the full transcript can offer a comprehensive understanding of the importance of countering fear with facts in online discourse.

### Tags

#Misinformation #Fearmongering #SocialMedia #USNukes #CounterNarratives


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
how a pretty mundane, boring government account on Twitter, an account that normally gets
twenty retweets per tweet, wound up getting thousands in just a couple hours. The account
belongs to USStratCom, that is, United States Strategic Command. To keep it simple, they're
the outfit in charge of US nukes. It's not an organization a lot of people follow on
Twitter. However, they tweeted something, and it wasn't entirely clear what it was.
And the right-wing personalities that like to capitalize on fear, well, they saw it,
and they started to do just that. So what was the tweet? It says, hashtag USStratCom,
posture statement preview. The spectrum of conflict today is neither linear nor predictable.
We must account for the possibility of conflict leading to conditions which could very rapidly
drive an adversary to consider nuclear use as their least bad option. I mean, yeah, I
guess that's a little unclear in and of itself, but somebody who wanted to inform their audiences
could provide context. But what did the right-wing do? Biden getting testy again. Hope y'all
don't live near any major cities. I don't remember our military tweeting such grim things
during four years of Trump. And to think some of you were offended by Trump's mean tweets.
This should scare you. We're all going to effing die. Strange tweet for 1109pm on a
Monday night feels like the military is warning us that we have a dementia patient who is
about to destroy the world sitting in the White House. What happens to world stability
when a senile geriatric patient is running the White House? And it goes on and on and
on. So what's the reality? What was that tweet? The initial tweet that started all of this,
that led to tens of thousands of people believing we might be on the precipice of nuclear war?
Basically what it said it was, a preview of the posture statement. Hopefully by the time
y'all watch this, if I don't have to release it in the middle of the night because people
got too scared, the person over STRATCOM will be testifying before Congress giving the posture
statement which basically just justifies their existence. What is the justification for them
to exist? There are countries out there that have nukes that may want to use them if things
get bad. That's the justification. This testimony occurs every year. In fact, this year I think
it's late. I think it normally occurs in March. It's not an emergency. It's not something
to be scared of. It occurs every year. You can Google STRATCOM posture statement and
you can read all of the ones from the previous years. If anybody wanted to inform their audience,
they could provide that context. But they didn't. They chose to capitalize on it, to
fearmonger with it, to scare their audience. The thing is, some of the people who retweeted
it with these statements, they're ex-Secret Service, ex-Air Force. One of the people I
saw retweet it was in SAC, which is Strategic Air Command, the forerunner. These people
should know what this is, and I'm willing to bet that they do. But they chose to scare
their audience because they know their audience is motivated by fear. It's what the right
wing is really motivated by today. If you want to reach people who follow these personalities,
you have to be aware of this. Because before you can get anywhere with them on any subject,
you have to find out what they're afraid of and alleviate that fear with facts. Explain
what it is. Explain what's actually going on. Because of the irresponsible actions of
some people on Twitter, there are thousands of Americans right now who feel that we might
be on the brink of nuclear war. It's not happening. This is a mundane occurrence. The statements
are written weeks in advance. It's not a big deal. But it was seized upon on social media,
and as quickly as things travel, that's the cost. People being scared right now for no
reason. And just for future reference, if anything like that is going to happen, the
one outfit that will not be tweeting is Stratcom. They're not going to have time. Watching this
occur in real time, it's something that you should be aware of. Your uncle, father, cousin,
whoever, who follows these personalities, who has bought into all of the fear and lets
that fear run their life, you have to alleviate that fear or you're not going to get anywhere.
You won't be able to discuss any policy or any issue until you find out what they're
afraid of and then alleviate it with facts. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}