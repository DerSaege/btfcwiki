---
title: Let's talk about surprising Senate shenanigans and infrastructure....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=i7_lZlc0hhs) |
| Published | 2021/04/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The parliamentarian's surprising determination allows Democrats to revise a previous resolution.
- Biden's infrastructure bill is likely to pass despite Democratic infighting.
- Democrats may amend the 2021 resolution to include uncontroversial parts of the bill.
- Contentious aspects could be pushed into a resolution for fiscal year 2022.
- Funding in the bill includes removing lead pipes, public housing, electrical grid updates, VA hospitals, schools, rural internet, Amtrak, electric vehicles, child care facilities, and future public health prevention.
- Passing the bill through regular Senate channels without budget reconciliation seems unlikely due to Republican opposition.
- The parliamentarian's decision makes it probable that the majority of the infrastructure package will pass.

### Quotes

- "Surprise. Determined that the Democrats could revise or amend a previous resolution."
- "Biden's big infrastructure bill, yeah, that's probably going through."
- "Money to get rid of all the lead pipes, money for public housing, money for schools, money for high speed internet in rural areas."
- "It's a big infrastructure package."
- "It's just a thought y'all have a good day."

### Oneliner

The parliamentarian's surprising decision allows Democrats to revise a previous resolution, making Biden's infrastructure bill likely to pass with significant funding for various vital sectors.

### Audience

Politically engaged citizens.

### On-the-ground actions from transcript

- Advocate for the passing of the infrastructure bill through contacting elected representatives (implied).
- Stay informed about the progress of the infrastructure bill and its implications for different sectors (implied).

### Whats missing in summary

Details on the potential impacts of the infrastructure bill on communities and the economy.

### Tags

#InfrastructureBill #Politics #Senate #BudgetReconciliation #PublicHealth


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about some surprising Senate
shenanigans.
The parliamentarian, who is not very well liked right now,
this is the person who determined the minimum wage
increase could not be included in a budget reconciliation.
That was kind of expected.
Most determinations from the parliamentarian are expected.
There's not a lot of surprises.
Surprise.
Determined that the Democrats could revise or amend a previous resolution.
The key takeaway, the part of this that matters,
Biden's big infrastructure bill, yeah, that's probably going through, even with all of the
infighting in the Democratic Party right now, I don't think that they'll sabotage themselves
with this.
That looks like it's going through.
There are a whole bunch of different ways it can go through.
I think the most likely would be for them to amend the 2021 resolution to include like
half of it, the half that isn't objectionable to anybody, so it just
sails on through. And then put the stuff that they are going to have to argue
over in a resolution for fiscal year 2022 and push it through as much as it
can. I think that most of it's going through now. We've talked about it a
little bit and people are focusing on the bigger pieces the more interesting
pieces aside from all the stuff grabbing headlines there's money to get rid of
all the lead pipes that seems like it's important lead pipes and water you know
Flint and all of that there's money for that there's money for public housing
There's money to update the electrical grid, Texas.
Money for VA hospitals and clinics, money for schools,
money for high speed internet in rural areas.
That's really exciting for me.
It's Joe Biden, so of course there's money for Amtrak.
There is money for infrastructure related to electric vehicles
update child care facilities in in need areas and putting money into preventing
a future public health issue which that seems like a good idea. Overall it's a
big it's a big infrastructure package and if they had to take it through the
normal chain in the Senate rather than using budget reconciliation I don't
think it would go through. I don't think I don't think they'd make it. They'd have
to get a whole lot of Republicans to sign on to it, and it doesn't seem likely.
But with this determination, it would be surprising if the overwhelming majority
of it didn't go through anyway it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}