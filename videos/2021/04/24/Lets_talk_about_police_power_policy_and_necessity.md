---
title: Let's talk about police, power, policy, and necessity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dlVRvMiMUb0) |
| Published | 2021/04/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the overwhelming number of fatal shootings by law enforcement during the Chauvin trial.
- Addresses why he focuses on training and policy when discussing these situations.
- Challenges the notion that focusing on training and policy gives cops an excuse.
- Emphasizes that incidents often involve officers stepping outside of training and policy.
- Points out the importance of policy in driving accountability and reform.
- Stresses the significance of policy in changing officer behavior and ensuring accountability.
- Talks about the specific policies like positional asphyxiation, time, distance, and cover that are critical for accountability.
- Suggests that focusing on outdated training methods can lead to accountability in cases of officer misconduct.
- Differentiates between addressing systemic issues and pursuing accountability through policy.
- Encourages understanding and engaging with policy for impactful reform and accountability.

### Quotes

- "Knowledge is power."
- "If you can change the policy, well, the next time they do it and it's outside of policy, then they can be charged."
- "Policy is the most powerful tool you have."

### Oneliner

Knowledge is power in holding law enforcement accountable through understanding and engaging with policy for impactful reform and accountability.

### Audience

Reform Advocates

### On-the-ground actions from transcript

- Study and understand existing policies on law enforcement conduct (implied).
- Advocate for changes in policies to drive accountability and reform (implied).

### Whats missing in summary

Insights on specific examples and case studies could provide a deeper understanding of the impact of policy on accountability and reform in law enforcement.

### Tags

#PoliceReform #Accountability #Training #Policy #SystemicIssues #CommunityPolicing


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about how knowledge is power and I'm going to answer two questions
that I've been asked a lot over the last couple of weeks.
The first is, why haven't you talked about this specific situation, this thing that happened
in this city, why didn't you cover this one?
And the other question is, when you're covering these situations, why do you always focus
on training and policy?
It seems like you're giving the cops an out.
Okay, so first question.
During the Chauvin trial, it was 22 days long, by the source that I use, there were 103 fatal
shootings by law enforcement.
103 in 22 days.
I typically put out two videos per day.
If I was to dedicate this entire channel to discussing nothing but these incidents and
double my video output, I still wouldn't be able to cover them all.
That alone should indicate to most people that there's a situation that needs to be
addressed, I would suggest.
I also want to point out that when I tweeted that little fact out, somebody on Twitter
corrected me.
By the source I use, there were 111.
And that's completely believable.
I didn't look into it to see which was right, because a margin of error of eight people
over three weeks, that's completely believable.
Because the scope of the problem is so large, it's hard to keep track.
Okay, so why do I always focus on training and policy?
Seems like I'm giving the cops an out.
That is literally the exact opposite of how it works.
I tend to focus, not always, but most times, I focus on incidents where I can show the
cop may have stepped outside of training and policy.
That's actually not an excuse.
That's not how it works.
I challenge you to provide me with a single incident, one case, since the turn of the
century in which a cop was charged with excessive force or anything like that, who wasn't first
found to be outside of policy and training.
When the media frames these incidents, they ask the question, was it necessary?
That's not the question that gets asked when the shooting is being reviewed.
The question is, was it within policy?
At the moment it happened, was it within policy?
If you are looking for accountability, policy is what matters.
That's what drives it.
We want to talk about necessity when we're talking about reform.
When you're talking about changing it, yes, all of this other stuff is important.
You want to talk about not just the necessity, but why it happened.
Was there a systemic issue that may have played into it?
Probably.
But again, that's not going to come up in the accountability phase.
When you are looking for accountability, you can only look at policy, because that's what
drives it.
If you want reform rather than a systemic change, you really need to get familiar with
policy, because that's what matters.
That is what is going to change the way officers behave.
Because if you can change the policy, well, the next time they do it and it's outside
of policy, then they can be charged.
Policy is way more important.
Showing that an officer stepped outside of policy or did something they weren't trained
to do is step one in getting accountability.
It is not an excuse.
That is actually the exact opposite of how it works.
The main things that we talk about on this channel, and there's a reason I focus on specific
pieces of policy when we're talking about positional asphyxiation, time, distance, and
cover, auditory exclusion, this kind of training.
The reason I focus on this stuff rather than the newer stuff is the stuff I talk about,
all of it started being trained 25 years ago or more.
Any cop on the street today should have been trained this way and more than likely was,
and that's more than likely the policy.
If you focus on those, you can actually get somewhere when it comes to accountability.
Now, if you're looking to address the major systemic issues, policies, they're important
as far as getting them written for the future, but the current ones, they don't really matter
that much.
But if you're looking for accountability, the policy is the most powerful tool you have.
Show that they stepped outside of that, well, then you can pursue charges.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}