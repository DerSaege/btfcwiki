---
title: Let's talk about companies getting involved in politics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=badvAxD1nh4) |
| Published | 2021/04/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Singer, known for sewing machines, made 1911s for the US government in 1939, contributing to the war effort.
- The precision equipment they made, like the Sperry Sight for airplanes, helped defeat authoritarianism.
- Companies getting involved in politics isn't a new concept; Singer's involvement predates WWII.
- Companies influencing politics through campaign contributions challenges the idea of them staying out of politics.
- Confusing basic morality with politics is a larger issue in American society.
- Interfering with fundamental rights is a moral, not political issue.
- Organizations with exclusionary histories might be sensitive to attempts to deny basic rights.
- Many Americans fail to recognize the non-negotiable nature of American ideals when it comes to political interference.

### Quotes

- "Maybe they saw what was coming and they wanted to be on the right side of history."
- "Companies getting involved in politics isn't the biggest issue; confusing morality with politics is."
- "Interfering with fundamental rights is a moral issue."
- "Large segments of the American population are confusing basic morality with politics."
- "The promises made in the Declaration of Independence and the Constitution are not up for negotiation."

### Oneliner

Singer's historical involvement in WWII shows the importance of companies being on the right side of history, while confusion between basic morality and politics poses a more significant issue in American society.

### Audience

American citizens

### On-the-ground actions from transcript

- Advocate for upholding fundamental rights and values in society (implied).
- Educate others on the distinction between moral issues and political decisions (implied).

### What's missing in summary

The full transcript provides a deeper exploration of the historical involvement of companies in politics, urging individuals to critically analyze the intersection of morality and politics in societal issues.

### Tags

#Companies #Politics #Morality #FundamentalRights #AmericanHistory


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about companies getting involved in politics,
whether or not they should do that,
and uh...
some other stuff.
But first,
we're going to go through some cool little historical trivia
about your grandma's sewing machine company,
Singer.
I think most people are familiar with this company.
They make sewing machines. It's what they do, it's what they've always done.
Except in 1939 they made something else.
They contracted with the US government
to make 1911s.
If you don't know what that is,
it's the 45
that's in every World War II movie ever made.
They made them for the War Department, I want to say 500 of them.
And the War Department's like, these are amazing, they're great.
Problem is we need a hundred a day,
and y'all just can't keep up.
But these are incredibly precise.
So we have something else we'd like you to make for us.
Singer went on to make the Sperry Sight,
the thing that went into airplanes,
allowed them to look down
over Europe,
make sure that what they were dropping, well it was going where it was supposed to.
They made all sorts of stuff
for aircraft,
precision equipment.
And
that action,
that little adventure,
it helped defeat a pretty horrible brand of authoritarian government.
Now people can look at that and they can say, well you know a lot of companies got
involved in the war effort in the forties.
Yeah they did.
This was in 1939.
You could say it was just a good business move.
Maybe it was.
Maybe it was.
Or maybe
they saw what was coming
and they wanted to be on the right side of history.
Maybe it's that simple.
But at the end of the day it really doesn't matter, does it?
Because
that little adventure,
that's something that company can be proud of.
Because they were on the right side of history. They did step up
when it was needed.
People are saying companies shouldn't get involved in politics. There's a
couple of things we need to keep in mind as we talk about this.
First,
that's kind of a moot point.
Who owns your representative or your senator?
Who do they really listen to?
It's not the people who elected them.
In most cases it's the companies that
contribute to their campaigns.
That idea, that doesn't even make sense in this day and age.
Companies stay out of politics.
I don't think that the biggest issue
is companies getting involved in politics. I think the biggest issue is that there
are large segments of the American population
that
are confusing
basic issues of morality
with politics.
Politics is deciding whether to fund
that bridge or that new school or
that new train line.
That's politics.
Interfering with a group of people's fundamental rights?
No, that's a moral issue.
And if you were, perhaps,
part of an organization,
a sport maybe,
that had a history of being, well, less than inclusive
in its early days, you might be a little bit
more sensitive
to people trying to deny or interfere
with that same group of people
exercising fundamental rights.
I think that's a bigger issue.
I think it is a bigger issue
that there is a large segment of the American population
that thinks the promises
made in the Declaration of Independence,
the Constitution,
those basic American ideals,
well, those are up for negotiation
as long as it's, you know, my political party that wants to interfere with it.
I think that's the bigger issue.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}