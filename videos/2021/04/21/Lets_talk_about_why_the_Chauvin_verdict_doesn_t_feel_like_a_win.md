---
title: Let's talk about why the Chauvin verdict doesn't feel like a win....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EULbrMCWMCo) |
| Published | 2021/04/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Three separate instances of having the same sobering chat with newly politically engaged allies.
- Outlining the three options when grading performance: fails to meet standards, meets standards, exceeds standards.
- Acknowledging that a verdict or outcome that meets standards may not always evoke a feeling of victory.
- Emphasizing that meeting standards is just the starting point for a better society.
- Calling attention to the fact that meeting standards should not require significant political pressure.
- Describing exceeding standards as the ultimate goal for society - where actions are automatic and just.
- Reminding allies and activists that progress towards exceeding standards is a continuous journey.
- Encouraging newly engaged allies to stay committed despite potential disappointments and the ongoing work ahead.
- Noting the societal tendency to celebrate when things almost function as they should, despite still falling short.
- Urging allies to not be discouraged by the work still needed but to recommit to the cause and the people relying on them.

### Quotes
- "If you're grading performance, you really have three options."
- "Standards are the starting point."
- "You cannot stop."
- "That's a reason to recommit."
- "We aren't even getting the bare minimum."

### Oneliner
Beau talks about the importance of meeting and exceeding standards in society, urging allies to stay committed despite the ongoing work ahead.

### Audience
Newly engaged allies

### On-the-ground actions from transcript
- Keep educating yourself and staying politically engaged to work towards exceeding standards (implied).
- Continue putting in the work to push for societal change and progress (implied).

### Whats missing in summary
The emotional impact and depth of the speaker's words can be better understood by watching the full transcript.

### Tags
#Standards #Allyship #SocietalChange #Commitment #Activism


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about standards,
meeting standards and exceeding standards.
And a conversation I've had to have three times.
Three times I had the exact same conversation.
All three times was with a newly minted person, somebody
who has just woken up, become politically engaged.
And they're the allies, but the type
that have put in the work, the type that have shown up.
And the day comes, the verdict comes down.
It doesn't feel like a win.
Yeah, I bet not.
I bet it doesn't.
Because it's really not a win.
If you're grading performance, you really have three options.
You have fails to meet standards,
meet standards, exceed standards.
In this case, once charges were brought, meet standards.
But that's the starting point.
Standards are the starting point.
If you want a better society, standards are the bare minimum.
I bet that doesn't feel like a win,
not unless you are personally connected to this case.
And if we're really being honest with ourselves,
it didn't meet standards either.
So I'm going to talk about standards.
And if it does, it didn't meet standards either.
Because we have to think about all the political pressure
that had to be brought to bear to get to this point.
Standards would be when some unjustified event occurs
and everything just swings into motion on its own,
without political pressure.
You shouldn't have to fight to get the bare minimum,
but you should meet the standard.
In this case, American society failed to meet standards
to get the bare minimum.
Exceeding standards, where we want to get to,
that's when it doesn't happen anymore.
That's the society we want.
That's where we want to get to.
And we're a long way off from that.
Doesn't mean you can't dream about it,
but you have to acknowledge we have to continue to work
to get there.
So if you are a newly minted ally,
you cannot get discouraged,
because it wasn't as gratifying as you thought it might be.
Because there's a lot of work to be done
and there are a lot of people counting on you.
Right now, we are so used to failing to meet standards
that when things almost function the way they should,
we expect to feel overjoyed.
But then we start to think about it
and realize that it's still not working.
We aren't there.
We aren't even getting the bare minimum.
And yeah, that does mean that there's a lot of work
still to be done.
But that's not a reason to get discouraged.
That's a reason to recommit,
to realize that society needs you.
There are people counting on you.
So if this was the first time
that you got politically engaged,
that you became a real ally,
that you put in the work,
you cannot stop.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}