---
title: Let's talk about the phrase "we write the report"....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KFfH2UCfJ7E) |
| Published | 2021/04/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the problematic phrase "we write the report," suggesting that it implies a sense of authority to put anything in the report and have it believed due to public trust in officers.
- Reads a report from May 25, 2020, about an incident in Minneapolis involving a man who died after a police interaction.
- Points out discrepancies between the written report and what was captured on body cameras, implying that the report was crafted to shape public opinion.
- Emphasizes the importance of contrasting official reports with what one sees with their own eyes.
- Notes the widespread use of the phrase "we write the report" within law enforcement culture, even in the age of ubiquitous camera phones.
- Urges people to be critical of official reports and press releases from law enforcement, as they may not always provide the full or accurate picture.
- Encourages viewers to maintain a skeptical eye towards official narratives and to prioritize personal observation and critical thinking.
- Raises awareness about the manipulation of information by authorities to avoid accountability and shape public perception.
- Calls for vigilance in analyzing and questioning the information provided by law enforcement in incidents.
- Advocates for holding law enforcement accountable for the accuracy and transparency of their reports and actions.

### Quotes

- "Contrast what they put here and what you saw with your own eyes."
- "That phrase, we write the report, it has been around a long time."
- "Make sure there is a report that can't be edited or fashioned to be less than accurate."
- "They still attempt to shape public opinion by putting out less than accurate information."
- "This press release was an attempt by the department to make sure that what just happened, the accountability that just occurred, to make sure it didn't and that it never would."

### Oneliner

Be critical of official reports; contrast with personal observation to combat narrative shaping by authorities.

### Audience

Critical thinkers, community members

### On-the-ground actions from transcript

- Compare official reports with personal observations (implied)
- Question law enforcement narratives (implied)
- Advocate for transparency and accountability in police reports (implied)

### Whats missing in summary

Importance of questioning authority and seeking transparency in law enforcement narratives.

### Tags

#PoliceReports #NarrativeShaping #Transparency #Accountability #CommunityPolicing


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the phrase, we write the report.
I've talked about it before, a couple years ago I released a video explaining the phrase
and what it means and why I really don't like it.
That phrase to a certain group of people means that well, we can put whatever we want to
in the report and it will be believed because we have the public's trust, because we're
officers.
And that's kind of true.
Those reports are often taken at face value, even though they've been proven to be, well
let's just call it less than accurate at times.
So today I want to read the report.
May 25, 2020, Minneapolis.
On Monday evening, shortly after 8pm, officers from the Minneapolis Police Department responded
to the 3700 block of Chicago Avenue South on a report of a forgery in progress.
Officers were advised that the suspect was sitting on top of a blue car and appeared
to be under the influence.
Two officers arrived and located the suspect, a male believed to be in his 40s, in his car.
He was ordered to step from his car.
After he got out, he physically resisted officers.
Officers were able to get the suspect into handcuffs and noted he appeared to be suffering
medical distress.
Officers called for an ambulance.
He was transported to Henpin County Medical Center by ambulance, where he died a short
time later.
At no time were weapons of any type used by anyone involved in this incident.
The Minnesota Bureau of Criminal Apprehension has been called in to investigate this incident
at the request of the Minneapolis Police Department.
No officers were injured in the incident.
Body worn cameras were on and activated during this incident.
The headline here, man dies after medical incident during police interaction.
Man, that doesn't sound like what we saw in that video, does it?
Sounds like they wrote the report.
This release, this press release, this is what they tried to sell the public.
This is what they told everybody happened.
Even though they had the body cams, they had the information from the dispatcher.
They knew all of that.
And this is what went out.
Almost everything we heard in the trial, they had access to.
And this is what they told the public.
Sure, the obvious takeaway is to record.
Make sure that there is a report, if you will, that can't be edited or fashioned to be less
than accurate.
Maybe not tell the whole story.
Yeah, that's true.
And that is a good takeaway.
But beyond that, when you read one of these reports, when you get that first press release
that comes from the department explaining their actions, make sure you always keep this
one in mind.
Contrast what they put here and what you saw with your own eyes.
Remember that that phrase, we write the report, it has been around a long time.
It is ingrained in the culture of law enforcement.
And even with the prevalence of cameras on everybody's phone, they still attempt to use
it.
They still attempt to shape public opinion by putting out less than accurate information,
less than complete information, omitting the parts that, well, that might make us look
bad.
Remember this release.
This press release was an attempt by the department to make sure that what just happened, the
accountability that just occurred, to make sure it didn't and that it never would.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}