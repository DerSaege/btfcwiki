---
title: Let's talk about CEOs vs the GOP vs Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ikVjPelsIy0) |
| Published | 2021/04/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- 100 CEOs had a conference call to strategize flexing their corporate muscles against Republican efforts to restrict voting access.
- They view the Republican push as based on a lie and discussed freezing campaign contributions or investments in response.
- Former President Trump's recent speech to Republican donors focused on personal grievances and attacking fellow Republicans.
- Trump's actions create division within the Republican Party, unsettling major donors who dislike internal battles.
- The Republican Party, post-Trump, faces challenges in managing his unpredictable actions, fundraising obstacles, and corporate backlash.
- To survive, the Republican Party may need to rebrand as the party of the working class, requiring a shift in policies and rhetoric.
- Democrats could benefit from increased corporate funding while leveraging opposition to Trump to motivate their base.
- Biden's promises and opposition to Trump may drive negative voter turnout against Republicans.
- A significant political realignment in the US seems possible, with the Republican Party needing to evolve to stay relevant.
- Democrats have an opening to make political gains amidst the Republican Party's struggles.

### Quotes

- "Former President Trump's recent speech to Republican donors focused on personal grievances and attacking fellow Republicans."
- "The Republican Party, post-Trump, faces challenges in managing his unpredictable actions, fundraising obstacles, and corporate backlash."
- "A significant political realignment in the US seems possible, with the Republican Party needing to evolve to stay relevant."

### Oneliner

100 CEOs plan corporate pushback against Republican voting restrictions, while Trump's divisive actions create challenges and opportunities for both parties.

### Audience

Political analysts, activists

### On-the-ground actions from transcript

- Contact local organizations to support voting rights initiatives (exemplified)
- Join or support advocacy groups working towards fair voting practices (exemplified)
- Organize community events to raise awareness about the importance of voting (exemplified)

### Whats missing in summary

Insight into the potential long-term impacts of corporate activism on political dynamics.

### Tags

#CEOs #RepublicanParty #PoliticalStrategy #VotingRights #Trump #DemocraticParty


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about
an interesting political dynamic
that's shaped up over the weekend
and what it means for the future of the Republican Party
and how Democrats may use it to their advantage
in the future.
Over the weekend, 100 CEOs had a conference call on Zoom,
some of them from pretty big companies.
The purpose of this call was to discuss how they might best flex their corporate muscles
in opposition to the latest Republican push to limit access to voting.
They feel that this whole push is based on a lie.
Now the main reason they feel that way is because it is.
But they discussed options on how to counter this push.
One of them was freezing campaign contributions to any politician who supports it.
Another would be freezing or postponing, limiting, ending investments in states that pass it.
Those are pretty big moves.
Those are the kind of moves that will certainly get the Republican Party's attention and
probably change minds.
I would imagine just the suggestion of them doing this has already changed minds.
Aside from that, former President Trump had a little get-together with a lot of big Republican
donors, and he gave a speech and they walked away, let's just say a little disheartened because
this speech did not talk about how they were going to retake the Senate or take the House or win in
2022 or 2024. He used this speech to talk about his personal grievances and lash out at his political
opposition. And by political opposition I mean other Republicans, Pence, McConnell, people like
that. It reinforced the idea that supporting Trump would lead to infighting in the Republican
Party. Large donors don't like that because that means their money gets used to battle other
Republicans. Now, for the Republican Party excluding Trump, this puts them in a very
unenviable position because they can't control Frankenstein's monster. They have
no idea what Trump is going to do. He's a hindrance when it comes to gathering
campaign funds and he's still an asset when it comes to motivating the base, but
it's also led to corporate America turning against them. Realistically the
only move that the Republican Party has is to try to recast themselves as the
party of the working class. The problem with that is they would have to start
supporting policies that actually help the working class, that help those on the bottom.
That's not something the Republican Party is really known for.
It's not something that a lot of current Republicans will want to do because they'll have to use
the opposite talking points and they'll have to debate their past selves.
It's not a good position.
Now, for Democrats, they may end up being able to get more funding from corporate donors
while continuing to motivate the base by being in opposition to Trump, who is really good
at generating negative voter turnout, people who are showing up not necessarily to vote
for a candidate, but to vote against him and his candidate.
And all they really have to do is make good on Biden's promises.
There may be a significant political realignment coming, something we haven't seen in the
United States since the Southern Strategy, because the Republican Party can't really
survive on its current platform, what's left of it.
They're going to have to adapt.
They're going to have to move in a different direction.
How they accomplish that is anybody's guess, but until they figure it out, the Democratic
Party has a pretty big opening to make some substantial gains.
Anyway, it's just a thought.
Y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}