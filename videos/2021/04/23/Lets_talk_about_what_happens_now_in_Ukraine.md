---
title: Let's talk about what happens now in Ukraine....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=L2e67gZhr9E) |
| Published | 2021/04/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- US foreign policy is shifting towards countering more powerful nations rather than pursuing lesser powers.
- The recent tensions on the Ukrainian border involving Russian troops backing off have raised questions about why countries engaged in such posturing activities.
- Major powers like Russia posture near borders primarily for three reasons: posturing, intelligence gathering, and training.
- Posturing involves projecting a willingness to fight without actually engaging in conflict.
- Russia aims to test the resolve of the new US administration under President Biden.
- NATO's expansion towards Russia's borders prompts Russia to demonstrate readiness to defend its interests.
- Intelligence gathering occurs as countries observe and plan countermeasures in response to military buildups on their borders.
- Training exercises involve mobilizing troops quickly to test readiness and familiarize with potential conflict zones.
- There's also a psychological aspect where repeated posturing can desensitize opposing nations, potentially providing an edge in surprise attacks.
- The scaling back of Russian troops may involve prepositioning equipment near the border to maintain readiness without drawing attention.
- This prepositioning tactic was more effective in the past when information awareness was limited compared to today's battlefield.
- Any potential conflict between major powers is likely to start with air-based engagements rather than ground battles.
- The media coverage of these posturing activities will eventually desensitize the public, turning them into routine non-news events.
- The surveillance conducted by major powers helps reduce the chances of miscommunications or misinterpretations that could lead to conflicts.
- Overall, the posturing, intelligence gathering, and training activities near borders serve multiple purposes but are not indicative of an immediate desire for conflict.

### Quotes

- "Posturing involves projecting a willingness to fight without actually engaging in conflict."
- "The scaling back of Russian troops may involve prepositioning equipment near the border to maintain readiness without drawing attention."
- "The media coverage of these posturing activities will eventually desensitize the public, turning them into routine non-news events."

### Oneliner

Beau explains major powers' border posturing for posturing, intelligence gathering, and training reasons, aiming to project readiness without actual conflict while adapting to changing global dynamics.

### Audience

Global citizens

### On-the-ground actions from transcript

- Monitor international relations and be informed about geopolitical developments (implied).
- Advocate for diplomatic solutions to conflicts between major powers (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of major powers' border posturing, including reasons behind such actions and the evolving nature of global politics.

### Tags

#Geopolitics #MajorPowers #MilitaryStrategy #GlobalRelations #Diplomacy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk a little bit about near peers again
and how we should get used to certain things because the
world political landscape is changing.
US foreign policy is no longer going after lesser powers.
It is moving to counter more powerful nations.
So now that the tensions on the Ukrainian border are starting to kind of drop a
little bit and it does look like Russian troops are backing off,
I had some questions come in.
One is why do countries do this?
I apparently didn't do a good job of explaining why they do it,
just that you shouldn't worry about it. And two, what happens next?
Okay.
So major powers do this for three real reasons.
A fourth one kind of. The main reason is posturing.
This is the two toughest guys in the bar giving each other dirty looks.
Neither one of them really wants to fight,
but they have to give the appearance that they're willing to.
And so they're posturing. From the Russian standpoint,
they want to test the new administration.
They want to test President Biden's resolve.
That's what they were doing.
It's also more important for Russia to have a more militant posture than the
United States.
The US doesn't have to convince the world that we're ready to go to war.
We will go to war over anything.
So we don't have to worry about that.
From Russia's standpoint,
NATO keeps expanding closer and closer and closer to its borders.
They have to show that they're going to be willing to put up a fight.
And that's the main reason.
Another one is intelligence gathering.
If the US puts a bunch of tanks on your border,
you as the other country, you're going to get ready.
You're going to start to plan a counterattack,
and that's going to show moves on the ground.
And US intelligence would monitor it and see what your plan is.
And this is how battle plans get drawn up.
The Russians were undoubtedly doing that.
The third reason is training.
Like for real, not just an excuse.
You really are training.
Getting your forces mobilized,
getting a whole lot of troops mobilized and somewhere quickly,
that's the goal of any military.
Every once in a while, it's a good idea to see if you can actually do it.
And it also provides an added benefit if you're fairly likely
that conflict is going to occur in a certain area.
If you can get your troops there beforehand
so they get a rough feel of the land,
that's pretty beneficial in the event
that it ever does actually turn into a conflict.
And then a fourth reason, more of an added benefit,
not really the reason they do it,
but if countries do this, and they do this often,
opposing nations, they start to succumb to the boy who cried wolf, that idea.
It's going to be nothing.
It's going to be nothing, just like that video.
World powers do this.
So if there ever was a reason to launch a surprise attack,
maybe you get a little bit of an edge.
But that's more of an unintended consequence rather than part of the plan.
So what happens next?
Russian troops start to scale back.
They appear to be setting up to preposition equipment,
to leave equipment behind near the border.
This was something that was done a lot in the olden days.
And it does appear that they're doing it,
that they're going to leave tanks and some artillery, stuff like that, behind.
The theory behind it is that if the large pieces of equipment are there,
you're less likely to tip your hand.
You know, it doesn't make the news that there's an armored column rolling down the road.
You just have to bring in the troops to run the tanks.
Back then, yeah, that made sense.
Today, all major powers try to maintain total information awareness
when it comes to a battle space.
They try to know everything that's going on.
I don't know that it's actually going to provide any real tactical
or strategic advantage on today's battlefield,
because NATO knows exactly where any equipment they leave behind,
they know where it is.
So if a bunch of personnel carriers start pulling up to it,
it's going to tip the hand.
The other side to this is that any engagement between any major powers
will initially be primarily air.
It will be primarily air-based.
And the stuff on the ground, you know, 60 years ago, yeah, a platoon of tanks,
that's a pretty big obstacle.
Today, that's a target of opportunity to be destroyed en route to the main objective.
I don't know that it's actually going to be beneficial,
with the exception of helping to provide that posturing.
We're willing to if we have to, but they don't really want to.
So what else will happen next is, as this starts occurring more and more,
the American populace will get used to it again.
Eventually, this type of stuff won't even make the news.
These exercises, they will not even make the news.
It will turn into more of a, you know,
wake me up when they actually start shooting type of thing.
But until then, and as long as it continues to get ratings,
the media will undoubtedly lean into it and provide exposes on the invasion stripes
on tanks, but neither China nor Russia nor the United States
actually wants a conflict with another major power.
And given the amount of surveillance that all the major powers conduct
on each other, that level of information awareness is good in a way,
because it makes it a whole lot less likely that there are miscommunications
or one side misreads the other's intention.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}