---
title: Let's talk about Mario and it happening again....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ndc6EMuhH_M) |
| Published | 2021/04/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a recent incident involving Mario Gonzalez in California where he was handcuffed, face down, and held down by officers.
- Comparing the incident to George Floyd's case, noting that this incident happened faster.
- Stressing the importance of considering positional asphyxiation, especially in cases involving larger individuals like Mario.
- Emphasizing that weight plays a significant role in positional asphyxiation, not just the weight of the officers but also the weight of the subject.
- Mentioning the significance of rolling a person into the recovery position to prevent positional asphyxiation.
- Urging law enforcement officers to prioritize following protocols, such as moving individuals into the recovery position when suggested by a colleague.
- Pointing out that officers' grips and commands to stop resisting do not matter in cases of positional asphyxiation, as it is involuntary.
- Warning that waiting for a person to stop moving before releasing pressure can be fatal in cases of positional asphyxiation.
- Stressing the need for these key points to be part of the ongoing conversation surrounding incidents like Mario Gonzalez's.
- Refraining from speculating on charges until all information about the incident is available.

### Quotes

- "Your grip doesn't matter. How many times you press down on them harder, doesn't matter. They will not stop. It's involuntary. You're literally killing them."
- "If you wait for them to stop moving to let up on them, 100% of the time you will kill them."
- "People who are succumbing to this, they will not stop squirming, simply because the officer says stop resisting. It's involuntary."
- "Those three things need to be talked about when this conversation occurs."
- "This is the time to get that information out."

### Oneliner

Beau stresses the importance of understanding positional asphyxiation and following protocols to prevent fatal outcomes in incidents like Mario Gonzalez's. 

### Audience

Law Enforcement Officers

### On-the-ground actions from transcript

- Move individuals into the recovery position when suggested by a colleague (implied)
- Prioritize understanding and preventing positional asphyxiation in law enforcement training and protocols (implied)
- Educate fellow officers about the risks of positional asphyxiation and the importance of following proper procedures (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of the importance of recognizing and addressing positional asphyxiation in law enforcement encounters. Viewing the entire video can offer additional insights and context on this critical issue. 

### Tags

#LawEnforcement #PositionalAsphyxiation #PreventativeProtocols #Training #MarioGonzalez #California


## Transcript
Well, howdy there, Internet people. It's Beau again.
So, today we're going to talk about Mario
and it happening again. Before we get
into this, I want to point some things out.
We're going to talk about this as if it was what it appears to be.
However, it's important to note we don't actually know that yet.
That isn't confirmed. It certainly appears that way,
but we don't know that yet. I normally wouldn't make a video like this,
this quickly until we have all the information,
but because the video of the incident was released,
people are going to be talking about it today.
And whether or not this was what it appears to be,
there are a couple of things that need to be in that conversation.
Because if it wasn't, this is the perfect teachable moment to stop it from happening.
Okay, so what am I talking about, right?
Mario Gonzalez, a man in his 20s in California.
He was handcuffed, face down, and held down by officers.
You know what happened. There are some differences between this and Floyd.
This happened faster. This happened faster.
Just so you know, I'm not making this up on the fly to fit a current incident.
The first time I talked about positional asphyxiation on this channel was in 2018.
In it, I say if it's somebody Eric Garner size, they might die.
Weight has a lot to do with it. Mario was a big guy.
Mario was a big guy. When people talk about weight,
they always think about the weight of the officer.
The weight of the subject matters as well.
In this incident, you actually had an officer say,
he had no weight on his chest, trying to avoid what it appears happened.
When you're talking about bigger people, it doesn't take a lot of weight.
In fact, it can happen with no weight.
This needs to be a part of the conversation.
I believe the same officer said, suggested rolling him on his side,
putting him into the recovery position. That relieves the pressure.
It takes him out of the position that is likely to cause positional asphyxiation.
Another officer said something to the effect of,
I don't want to lose what I got, talking about his grip. So they didn't.
A couple of points here. If you're a cop or you know somebody who's a cop,
listen up and listen up good. First, your grip doesn't matter.
Your grip doesn't matter. How many times you say, stop resisting,
or stop moving, or stop squirming, doesn't matter.
How many times you press down on them harder, doesn't matter.
They will not stop. It's involuntary. You're literally killing them.
They're in a fight for their life.
The other point is that if an officer suggests rolling somebody into the recovery position,
you do it. No questions asked. It doesn't matter what you see.
They may see something you don't. Put that into any other situation.
If your partner yells gun, you pull yours, right?
Even though you don't see one, because they may see something you don't.
Same thing applies. Those are the three things that need to be in this conversation.
People who are succumbing to this, they will not stop squirming,
simply because the officer says stop resisting. It's involuntary.
If the officer is holding somebody down and waits for them to stop squirming,
it's because they died. If you wait for them to stop moving to let up on them,
100% of the time you will kill them.
Wait has a lot to do with it, and you always put them in the recovery position.
These three things need to be talked about when this conversation occurs.
I know people are going to want to know what I think about charges.
We don't have all the information yet. It appears to be this.
There are other things it could have been,
and I'm not going to speculate until we actually have all of the information.
This conversation about positional asphyxiation is going to occur.
This is the time to get that information out.
Those three points need to be brought up in the conversations.
People need to know this. Most importantly, officers need to know this.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}