---
title: Let's talk about Wayne LaPierre's little trip....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IFEbbEqBgrA) |
| Published | 2021/04/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses Wayne LaPierre's hunting incident, revealing footage of the unmitigated failure and embarrassment.
- Urges viewers to watch the footage for two reasons.
- Describes the hunting expeditions where typically men pay tens of thousands of dollars to go on a safari.
- Points out that in these hunts, the guides do all the work and even line up the shots for the hunters.
- Emphasizes how these hunting trips are not the challenging, rugged experiences they are often portrayed as.
- Criticizes the hunting culture where animals are killed for trophies and the act is passed off as a rite of manhood.
- Mentions that the footage was likely obtained by The Trace.

### Quotes

- "It's worth seeing what that actually entails."
- "It's point and shoot. They don't do anything."
- "They really just drop their money so they can pretend that they are something they're not."

### Oneliner

Beau addresses Wayne LaPierre's hunting incident, exposing the truth behind these trophy hunting expeditions and challenging the notion of rugged individuality.

### Audience

Animal rights activists

### On-the-ground actions from transcript

- Watch the footage to understand the reality of trophy hunting (suggested)
- Share the footage to raise awareness about the true nature of these hunting expeditions (suggested)

### Whats missing in summary

The emotional impact of witnessing the reality behind trophy hunting and its implications on animal welfare.

### Tags

#TrophyHunting #AnimalRights #WayneLaPierre #RuggedIndividuality #HuntingCulture


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Wayne LaPierre and rugged individualists and passages into
manhood and being a tough guy and all of that.
If you don't know who he is, he is head of a pretty powerful lobbying group here in the
United States, he's the chief over the NRA.
A few years ago, apparently he went on a hunt, and I use the term loosely, to go get an elephant.
It was such an unmitigated failure and was so embarrassing that they never released the
footage, but now it's out.
Usually I don't suggest people watch stuff like this.
I actually watch all kinds of horrible things, so y'all don't have to.
This time I definitely think you should, for two reasons.
One, yes, seeing Wayne LaPierre, son of the pioneers, rugged individualist, super tough
guy on this hunt, I think that might be enlightening for a lot of people.
Seeing the guides do all the work, seeing him miss a shot multiple times from like 10
yards, even after the guide got down and pointed exactly where to hit on the animal.
I think that's worth seeing.
But there's a second reason.
That's what these hunts are.
You see in this footage, that's not really surprising.
That's what they are.
All of these typically men, not always, who pay tens of thousands of dollars to go on
a safari, and they come back and they tell you the stories about their great hunt.
That's what they did.
The guides do all the work, literally line up the shot, move your sticks for you, and
you're going after an animal that is so old and so trusting of humans that as the guide
lines up your shot and tells you where to shoot because great hunter that you are, you
don't even know where you're supposed to aim at, that animal turns and looks at you, trustingly.
Why would you hurt it?
And it's all done so they can bring back trophies, tails or feet or whatever.
So they can pass that farce off as some kind of right of manhood.
I think it's worth seeing what that actually entails.
It's point and shoot.
They don't do anything.
It's not a challenge as they like to frame it.
The guides do it all.
And they really just drop their money so they can pretend that they are something they're
not.
I think it's worth seeing.
I think the footage was obtained by The Trace.
I'm sure it will be everywhere today.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}