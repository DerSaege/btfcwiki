---
title: Let's talk about policy, practice, and the trial....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=G5UK_nw4GEw) |
| Published | 2021/04/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing questions surrounding the Chauvin trial and the testimony about use of force.
- Explaining the concept of positional asphyxiation and its relevance in the trial.
- Pointing out the discrepancy between policy and practice regarding prone restraints in law enforcement.
- Describing how policies aim to prevent specific outcomes but are not always followed on the ground.
- Noting the ingrained culture within law enforcement that resists change in operational practices.
- Expressing skepticism towards defense experts who may deny the existence of positional asphyxiation.
- Emphasizing the importance of specific questioning by the prosecutor in challenging defense experts' claims.
- Acknowledging the ongoing nature of the trial and the hope for accountability.

### Quotes

- "The policies are set up that way because when something does happen, well, it's a big deal."
- "There is a running conversation between people like cops and cops themselves."
- "It's ingrained, and I don't see that changing."
- "But at the same time, most officers have used a prone restraint over and over and over again without issue."
- "Hopefully, the bad apple gets removed."

### Oneliner

Beau addresses discrepancies in law enforcement practices, policy, and culture amid ongoing Chauvin trial testimony, urging for accountability and challenging defense expert claims.

### Audience

Law enforcement reform advocates

### On-the-ground actions from transcript

- Challenge ingrained law enforcement practices through community advocacy (implied)
- Support accountability measures within police departments (implied)
- Advocate for specific questioning of defense experts in trials (implied)

### Whats missing in summary

Insights on the impact of continued advocacy for police accountability and reform efforts.

### Tags

#ChauvinTrial #PolicePractices #UseOfForce #Accountability #LawEnforcement #PolicyVsPractice


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about policy, practice,
reading a chapter ahead in the book,
and whether or not it's over.
Because we've got a bunch of questions
in relation to the trial.
The people who were testifying about use of force,
they've pretty much wrapped it up.
Now, we're talking about the Chauvin trial.
So the first thing is, hey, the term positional asphyxiation
came up, and I feel like I read a chapter ahead in the book.
Yeah.
Yeah.
That certainly appears to me to play a big factor in this.
If you don't know, a couple of years ago,
I put out a video that detailed this and how it happens.
And the situation that I outlined
was pretty close to what actually occurred.
The next one was about prone restraints.
And basically, it's against policy, but I see it all the time.
Yeah, you do.
The policies in most departments say don't do it.
They put limits on it.
Basically, in most places, it's like as soon
as the suspect is compliant, get them
into a different position.
In practice, that doesn't really happen.
Why?
Part of it is simply the fact that most times when people
are cuffed, they're already in that position.
So unless somebody goes into distress,
the officer doesn't move.
Another part of it is that the policy is designed to stop,
well, what happened.
That's what it's there to stop.
However, in most cases, it's not.
That's what it's there to stop.
However, in most cases, it doesn't go down like that.
In most cases, just being on your belly and being cuffed,
that alone doesn't do it.
That video I released a couple of years ago
lays out a chain of events, a storm that will
lead to a positional asphyxiation.
It's not just being cuffed and being facedown, in my opinion.
The policies are set up that way because when
something does happen, well, it's a big deal.
So the policy is just always against it.
Cops on the street don't always do that.
There is a running conversation between people
like cops and cops themselves.
Cops will ask, when are you going
to teach what we do out there on the street?
And the trainers will ask, when are you
going to do what we teach?
That's a problem in the culture of law enforcement, one
that on this topic, I don't see changing any time soon.
It's ingrained, and I don't see that changing.
But at the same time, most officers
have used a prone restraint over and over and over again
without issue.
It only becomes an issue when there's other factors at play.
I am not aware of a single case where just being cuffed
and being facedown caused it, not one.
There may be one, but I'm not aware of it.
The last one, is it over?
No, no.
The use of force experts who have testified,
they have done a good job.
They really have, and the prosecutor seems on the ball.
But I would expect a expert witness from the defense
to walk in and say that positional asphyxiation doesn't
exist.
It's not really a thing.
There are a handful of use of force experts who will say that.
I've looked at most of the research
that they're basing this theory on.
I don't believe it.
Most of it is experiments that are very clinical.
The person isn't under stress.
It isn't that storm of events.
It's just somebody laying down.
I have seen nothing to debunk positional asphyxiation.
This has been something that has been accepted for decades.
But there are a handful of people who will testify
and say, no, no, that's not it.
Now, the prosecutor seems on the ball.
The prosecutor seems on the ball.
So hopefully, we will hear a very specific line
of questioning.
Because there's only a handful of use of force experts
who will testify and say that it doesn't exist,
they get used over and over and over again.
I would hope the prosecutor asks, how many times have you
testified in a case with similar circumstances where somebody
on their belly piled upon all of these circumstances
existing, and then they're not able to prove it?
And if these circumstances are existing,
how many times have you testified
that positional asphyxiation doesn't exist?
Because the answer, in most cases,
is going to be dozens of times, if not 100 times.
And then, hopefully, the prosecutor
would follow up with, well, if you have to constantly say
that these circumstances don't lead to death,
maybe your research is wrong.
Maybe they do.
The prosecutor seems on the ball.
I'm hoping that that line of questioning
exists when, undoubtedly, this defense expert comes in.
It's not over, but I think they're doing a good job.
Hopefully, the bad apple gets removed.
Anyway, it's just a thought.
You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}