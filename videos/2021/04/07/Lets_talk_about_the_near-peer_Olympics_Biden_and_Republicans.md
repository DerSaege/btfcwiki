---
title: Let's talk about the near-peer Olympics, Biden, and Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JZ_P8Qztppc) |
| Published | 2021/04/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration is considering boycotting the 2022 Winter Games in Beijing due to human rights concerns.
- This potential boycott is not an escalation but a diplomatic move that occurs during near peer contests.
- Historical examples of boycotts during the Olympic Games include the 1980 Moscow Games and the 1976 Montreal Games.
- The Biden administration's stance is causing different reactions, with some Republicans calling for the games to be moved and others for an outright boycott.
- Olympic teams are private entities, and while the administration can organize them, it is up to the teams to decide on participating in a boycott.
- Republicans calling for a boycott against a jurisdiction mirrors similar tactics used in the past.
- Expect more diplomatic shows and boycott calls in near-peer foreign policy situations.
- A boycott could have a significant impact by drawing attention to the administration's concerns, especially with larger nations like China.
- Despite sounding trivial, not participating in the Winter Games can have real consequences and influence.
- The outcome of this situation will depend on how it plays out and whether it effectively raises awareness.

### Quotes

- "So don't panic over any of this. It's all pretty normal."
- "Expect to see a lot of it."
- "Y'all have a good day."

### Oneliner

The Biden administration considers boycotting the 2022 Winter Games in Beijing over human rights, sparking mixed reactions, but it's all part of normal diplomatic maneuvers in near-peer contests. 

### Audience

Diplomacy watchers

### On-the-ground actions from transcript

- Organize peaceful demonstrations or campaigns to raise awareness about human rights issues in China (implied)

### Whats missing in summary

The full transcript provides historical context and a nuanced view of the potential Winter Games boycott, offering insights into the implications and possible outcomes of such a diplomatic move. 

### Tags

#WinterGames #Boycott #Diplomacy #HumanRights #China #Olympics


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the Winter Games in 2022.
Some news broke last night and almost immediately started getting questions from younger people.
Wondering if this was something that we should really pay attention to, if this was something
we truly needed to watch, if this was some form of escalation.
Now if you don't know what I'm talking about, the Biden administration has indicated that
it may be seeking to organize a boycott of the 2022 Winter Games in Beijing.
Okay short version, is this an escalation?
No, not at all.
We've talked for the last few months about how American foreign policy is switching to
counter near peers, meaning China and Russia.
I said it was going to look like the Cold War.
In 1980 the games were held in Moscow.
The United States led a boycott of 60 or so nations.
The reason was that the Soviets were in Afghanistan.
The more things change, the more they stay the same.
In 1984 the games were in LA.
The Soviet Union organized a boycott.
I want to say it was, I think they said that their athletes wouldn't be safe, something
along those lines.
These diplomatic shows like this, they occur during near peer contests.
It's not an escalation, it is completely normal.
Now the Biden administration is saying that they want to do it over human rights reasons.
There's precedent for that as well.
In 1976 the games were held in Montreal.
29 nations boycotted because the committee wouldn't ban New Zealand from participating.
Why do they want New Zealand to be banned?
Their rugby team, I think, went to South Africa during apartheid after the UN had kind of
said no more sporting events, trying to bring about political pressure to get stuff changed.
It's all very normal.
It's not an escalation.
It's not something you should worry about.
Now the Biden administration is talking about organizing the boycott.
Republicans are calling for the games to be moved to a different location.
Some are calling for outright boycotts.
Moderate Republicans like Romney, he's saying that the team should go but we should boycott
everything else.
Critical piece of information here, the Olympic teams are private entities.
They're not part of the government.
The administration can organize them, talk to them, and ask them to do this, but generally
speaking Olympic teams don't want to freeze themselves out of the Olympics.
That's kind of their whole reason for being.
It takes a lot to make that happen.
It would take a concerted effort from a whole lot of people saying, hey, we need to do this
to send a message.
But to be super clear on something else, Republicans are calling for a private entity to engage
in a boycott of a jurisdiction to get them to change their policies.
Why don't you have a cold Coke and think about that for a minute?
I'm willing to bet Georgia will be on your mind.
It's not that they disagree with this tactic.
They don't like it being used against them.
When it comes to near-peer foreign policy, this is completely normal.
Expect to see a lot of it.
I will try to put together a video that details some of the other things that we might expect
to see, but generally speaking it's going to look like the Cold War, from the saber
rattling to stuff like this.
So don't panic over any of this.
It's all pretty normal.
How much effect it would have?
More than you might imagine.
Generally large nations care about their image a little bit more.
Something like this, a boycott like this, would draw a lot of attention to what the
administration wants attention drawn to.
China wouldn't like that.
They wouldn't like that image being cast across the globe.
This doesn't matter so much when you're talking about smaller regional powers, but with big
nations it does.
So as silly as it sounds, we're not going to go play in the Winter Games, it does actually
matter and it can accomplish some good.
The current stance from Beijing on the United States' human rights record, it may not
matter this time, because we don't have a well-established record now.
So we will have to see how it plays out, or it doesn't play.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}