---
title: Let's talk about wildfire tips....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gqCDuaDz_wQ) |
| Published | 2021/04/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides basic tips on preparing for wildfires after receiving feedback from viewers following a previous video on California wildfires.
- Recommends having at least two evacuation destinations prepared in advance, reachable through different routes.
- Suggests designating someone out of the area as a point of contact in case communication is lost during evacuation.
- Advises keeping extra clothes for everyone in the vehicle and having an evacuation bag ready.
- Emphasizes the importance of having copies of documents stored digitally or on a thumb drive.
- Urges having a plan for pets and keeping a radio handy.
- Stresses the need to bring cell phones, battery backups, and chargers.
- Recommends preparing the house before leaving by closing windows and doors but leaving them unlocked, lights on for visibility.
- Provides tips for preparing the house, such as disconnecting gas, AC, propane tanks, and moving flammable items away from walls.
- Warns against leaving sprinklers on, as it can impact water pressure needed by firefighters.

### Quotes

- "Have somebody you know and that everybody in your circle knows this is the person to call."
- "Make sure you have a plan for all your animals and you have a radio."
- "But definitely if you're in that area or a like area, take the precautions you need to."
- "It is shaping up to be pretty bad this year."
- "Y'all have a good day."

### Oneliner

Beau provides key tips for preparing for wildfires, including evacuation planning, document copies, and house preparation, stressing the need for proactive measures in high-risk areas.

### Audience

Residents in wildfire-prone areas

### On-the-ground actions from transcript

- Designate at least two evacuation destinations (suggested)
- Prepare copies of documents digitally or on a thumb drive (suggested)
- Have a plan for pets and keep a radio handy (suggested)
- Bring cell phones, battery backups, and chargers (suggested)
- Disconnect gas, AC, propane tanks, and move flammable items away from walls (suggested)

### Whats missing in summary

The full transcript provides detailed and practical tips on preparing for wildfires, covering evacuation planning, document storage, and house preparation strategies.

### Tags

#WildfirePreparation #EvacuationPlanning #EmergencyPreparedness #SafetyTips #CommunityResources


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about wildfires
and how to stay safe.
After that last video about California,
I had people reach out and say,
hey, you tell us to prepare,
but didn't tell us how to prepare.
So I'm gonna run through some basic tips here
that I found in a bunch of different places.
And then in the description,
there will be a bunch of links.
There'll be a link to a playlist from this channel
that deals with emergency preparedness.
And a lot of it is applicable
to a whole bunch of different situations.
There'll be a link to the long video
on having an emergency bag on the second channel,
and then links to fire evacuation sites.
Like that's what they do.
But I'm gonna go through the key tips that I found first.
Okay, so first thing is
at least two evacuation destinations.
So have two different places that you can go to
prepared beforehand.
And make sure that you can get to either of them
through different routes in case one is closed
due to fire or smoke or whatever.
Another thing that I would add to this
is to have somebody who is out of the area,
out of state to act as a point of contact.
That way if you and your circle are not together
when it's time to evacuate
and you can't communicate with each other
for whatever reason, a lost cell phone, whatever,
you have somebody that you can contact out of the area
who you know their communications will be working
to pass messages back and forth via landlines
or a borrowed phone or whatever.
That's good to have anyway at all times.
Have somebody you know and that everybody in your circle
knows this is the person to call.
Keep an extra set of clothes for everybody in your vehicle.
Okay, that's one less thing to have to worry about
or if you have to get out of bed in the middle of the night,
you already have the change of clothes there.
Have an evac bag.
And like we said, we've got a bunch of videos
on that on this channel.
And then if you know absolutely nothing about it,
there's the long video on the second channel.
In that, something that I don't always remember to say
because I have them on a thumb drive,
have copies of all of your documents
or scan them and put them on a thumb drive.
Have a digital copy of them at least.
Make sure you have a plan for all your animals
and you have a radio.
And then I feel like I shouldn't have to say this anymore.
Make sure you take your cell phone
and battery backup for it, chargers, stuff like that.
Now, this is all stuff that I didn't know
because we don't really have to deal
with fires down here very much.
But preparing your house before you leave.
Close the windows and doors,
but leave them unlocked apparently
and make sure you leave your lights on
so the firefighters can find your house in the smoke.
And I guess leaving it open is to help them.
There are other tips as far as like making sure
your hoses are connected,
you have a ladder nearby out near a corner of a house
so they can get on your roof.
Buckets of water, have that prepared, stuff like that.
It also says to move your furniture
and flammable stuff away from the walls
and put it in the center of the room
so there's less stuff to catch fire near the edges,
which just, I mean, that seems like common sense.
Shut off gas, AC, and propane tanks
and make sure if you have a grill,
move that away from your house.
Normally they're stored right up against it.
You probably don't want it there in the event of a fire.
And then another big one was don't leave your sprinklers on.
Don't leave anything that can impact water pressure
because that's going to hurt their abilities
to save your property.
So those were the key tips that were,
both the person I reached out to and asked,
they said and were also listed on the resources
that'll be in the description.
But definitely if you're in that area or a like area,
take the precautions you need to
because it is shaping up to be pretty bad this year.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}