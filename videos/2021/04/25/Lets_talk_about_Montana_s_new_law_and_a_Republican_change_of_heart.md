---
title: Let's talk about Montana's new law and a Republican change of heart....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KlUW41uSzqo) |
| Published | 2021/04/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responding to a specific request, Beau talks about Montana's gun nullification law that recently got signed.
- Clarifies that it is not a nullification law; it prevents state and local agencies from assisting in the enforcement of federal firearm laws.
- Mentioning that the ATF can still enforce federal laws, despite the state law.
- Emphasizes that this concept is not new, as it involves local jurisdictions choosing not to assist in enforcing federal laws they find unjust.
- Points out that Montana could be considered a sanctuary state due to this stance.
- Notes the Republican Party's historical opposition to this idea, making their current support interesting.
- Suggests that the embracing of this idea by Republicans might lead to a change in their view on sanctuary cities.
- Predicts that the law won't have a significant impact as most people already assume Montanans are armed.
- Anticipates the mental gymnastics opponents of sanctuary cities will face due to this law being supported by the Republican Party.

### Quotes

- "It's not quite the owning of the liberals that you may believe."
- "I cannot wait to see the mental gymnastics necessary to oppose sanctuary cities now."

### Oneliner

Beau explains Montana's gun law isn't nullification but a refusal to assist in federal enforcement, challenging Republican views on sanctuary cities.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact local representatives to understand and support or oppose similar legislation (implied).

### Whats missing in summary

The full transcript provides additional context and humor around the topic of Montana's gun nullification law and its implications for political dynamics.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to do a request to video.
Somebody really really really wanted me to talk about state legislation in Montana.
And they're right, this isn't something that I would typically talk about because I don't
think there's a wide audience for state legislation in Montana.
However this is different.
I definitely would have talked about this anyway.
But I got a very specific request here.
Make sure you tell your liberal viewers about Montana's gun nullification law that just
got signed.
I want to drink liberal tears.
Okay so first, it's not a nullification.
That is not what that is.
This law stops state and local agencies from assisting in the enforcement of federal law
related to firearms.
Just to perhaps save everybody some trouble, the ATF can still come get you.
It didn't nullify the law.
Y'all need to be real careful with that idea because that's not what happened.
It's just that state and local agencies aren't going to help.
And that state funds can't be used to assist in the enforcement of these laws.
That's what it is.
This isn't a new thing.
This isn't a new idea.
This isn't revolutionary.
It is something that the Republican Party has been dead set against for years.
Take as much time with this as you need to.
The local jurisdiction has decided to not help, not assist in the enforcement of federal
laws it believes to be unjust.
You could say that Montana has become a sanctuary state.
I do appreciate the Republican Party coming around to this view.
But nobody cares about the gun laws in Montana.
That's not going to be a significant insult to anybody.
The idea that the Republican Party is now starting to embrace the idea that local jurisdictions
do not have to assist in the enforcement of federal law, that's new though.
That's interesting.
So I'm going to assume that y'all will stop complaining about sanctuary cities because
it is the exact same process.
I think most people have seen Yellowstone or some other show based in Montana.
Most of the country just kind of assumes that all of y'all are armed at all times.
I don't think anybody's really going to care about this.
It's not quite the owning of the liberals that you may believe.
But I cannot wait to see the mental gymnastics necessary to oppose sanctuary cities now.
Because this is being championed and cheered on by the entire Republican Party.
And it is the exact same process.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}