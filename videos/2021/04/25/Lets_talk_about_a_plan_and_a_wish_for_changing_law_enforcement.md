---
title: Let's talk about a plan and a wish for changing law enforcement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=atsxHpuqZnk) |
| Published | 2021/04/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of discussing plans and wishes for significant change in how things are done.
- He points out that simply wanting to be at a different point but not having a plan is merely a wish, not a plan.
- Beau proposes a rough sketch of a plan to address excessive force in law enforcement by preventing officers from immediately resorting to their firearms.
- He suggests using holsters with sensors that automatically call for backup and EMS when a firearm is drawn.
- Beau advocates for national accreditation for law enforcement agencies to ensure adherence to use-of-force policies and proper auditing of incidents.
- He argues against federalizing law enforcement due to jurisdictional differences but supports accreditation for agencies to receive federal funding.
- The plan includes holsters funded by the federal government to discourage tampering and ensure accountability.
- Beau explains that the ultimate goal is to reduce law enforcement's reliance on lethal force by keeping firearms holstered.
- He acknowledges the debate around abolition of the current law enforcement system and suggests harm reduction measures as an interim solution.
- Beau underscores the importance of gradually building a new system while undermining the current reliance on violence to facilitate its eventual replacement.

### Quotes

- "Just wanting to be at point B, that's not a plan, that's a wish."
- "Abolition, putting a new system in place, that's getting to the hospital with that injury. That's where you want to end up."
- "Nobody wants to fill out the report and nobody wants to be responsible for the bill of the EMS."

### Oneliner

Beau presents a plan to reduce law enforcement's reliance on lethal force through holsters with sensors, national accreditation, and gradual system transformation, advocating for harm reduction on the path to potential abolition.

### Audience

Law reform advocates

### On-the-ground actions from transcript

- Advocate for national accreditation of law enforcement agencies to ensure adherence to use-of-force policies (suggested)
- Support funding for holsters with sensors by the federal government to enhance officer accountability (suggested)
- Encourage agencies to prioritize de-escalation techniques through grant money incentives (implied)

### Whats missing in summary

The full transcript provides a detailed plan for reducing excessive force in law enforcement, addressing systemic issues, and advocating for harm reduction measures as steps towards potential abolition.

### Tags

#LawEnforcement #PoliceReform #Accountability #HarmReduction #SystemTransformation


## Transcript
Well, howdy there internet people. It's Beau again.
So today we're going to talk about plans
and wishes.
I think most people want
pretty significant change
when it comes to how certain things are done.
The problem is I don't know that there are a lot of people who have come up with
plans on how to get from point A to point B.
Just wanting to be at point B,
that's not a plan, that's a wish.
You have to figure out how to get there.
So we're going to talk about a plan, a rough sketch of one,
that I think would work.
One of the big issues today when you're talking about law enforcement,
one of the reasons
that I believe a lot of
excessive force
happens
is because officers pull their firearm
as a precaution.
They think they may need to use force
and they go to their firearm first. You see them pull that out first.
Now,
if they do encounter force,
maybe it doesn't
necessitate a firearm,
but that's what they already have in their hand,
so that's what they use.
So,
it seems like
the
most direct route would be to get them to keep their firearm in their holster.
Get them to abide
by a pretty standard rule of gun safety,
which is don't point it at anything you aren't ready to kill.
So how do you do that?
The simple answer
is
you have a holster
that has a sensor in it.
And when the firearm is removed,
for officer safety, because you know the situation is about to turn violent,
it automatically calls
for backup
and EMS.
I would imagine that once departments start getting the bill
for those EMS calls,
well, they're going to start telling people to not pull their weapons out.
So how do you get them to use it?
How do you get
different law enforcement agencies
to adopt it?
I would suggest that we have a national accreditation of sorts,
and that if you want any federal money,
you want any of the grants,
you want any of those billions of dollars
that funnel down to local law enforcement agencies,
you have to be accredited.
I know there's a lot of people that want to federalize law enforcement,
and it sounds good in theory until you realize that every jurisdiction has different laws.
So that's...
it sounds good,
but
it's not really practical.
But if you had accreditation
that, say, had
decent use of force policies
and auditing
of
body cam footage,
comparing it to the report,
then anybody who's found to be lying has to be terminated.
Or if they use excessive force, go outside of policy, they have to be terminated.
And if they are terminated for one of those reasons
from an accredited agency, well, they can't be hired by any other
accredited agency.
Then,
that holster,
that's part of it.
And it's so important
that the feds buy it.
The federal government pays for the holsters.
Why?
Mainly because...
well, my reason, anyway,
is that if it is federal property,
I'm going to suggest you
don't disable it.
You don't modify it.
You don't destroy federal government property. That's a really bad idea.
If it belongs to the feds,
and they have to fill out a report
every time they pull it out,
well, that form would be a federal form.
I'm going to suggest they don't rely on that.
And if there's auditing done
of the report
versus the body cam footage,
it should
encourage officers to go to
their taser
or pepper spray
just as the precaution
in their non-dominant hand
so they can still pull their firearm if they need to.
But, in most situations,
they won't.
A lot of the incidents that are occurring
are occurring because they already have their gun in their hand.
So we need to keep it in the holster.
This will reduce
law enforcement's reliance
on lethal force.
That's a good thing.
Now,
I know there are people
who
believe that abolition
is the only solution.
And for those who don't know, most people,
when they say abolition, that doesn't mean that there's no longer public safety. It means
that a different system has been put in place of the one that currently exists.
There are a few
that actually mean there is no public safety, but
it's not most of the people who say that.
They want an entirely different system,
one that isn't so reliant on violence.
A lot of people
are going to say that that's the only solution
and, well,
anything that isn't that,
that's not any good.
It won't work.
The country,
this society,
it has an injury.
Systemic issues in law enforcement,
law enforcement misconduct, a whole bunch of things
that are inherent to the current law enforcement system. So we're just going to call the injury
law enforcement.
Abolition,
putting a new system in place,
that's getting to the hospital with that injury.
That's where you want to end up.
Right now we're in the ambulance,
and I think that we can provide some treatment along the way
because we've got a whole lot of people being lost
in the ambulance
on the way to the hospital, on the way to that final destination of a new system.
The idea
of discounting anything
that isn't abolition now,
I don't know that that's helpful
because we literally
just had a situation
in which somebody was murdered
in broad daylight
on the street,
in front of witnesses,
on camera,
and half the country is mad
that the officer was held accountable.
I'm going to suggest
that abolition is a long way off
and that we should
engage in as much harm reduction as we can
on the way
to that final destination.
I would also point out
that if you want to
bring in a new system,
the easiest way to do that is to start to build it
while the old system crumbles.
I would suggest
undermining
the reliance on violence,
which is what the current law enforcement system is based on,
would help it crumble faster
so
the new system could be put into place faster.
I will tell you this would be expensive.
These holsters would be expensive, this whole setup would be expensive, but
I truly believe it would save lives.
Nobody wants to fill out the report
and nobody wants to be responsible
for
the bill
of the EMS.
And nobody's going to want to deal with the penalties
of lying on a federal form.
If
agencies are responsible
for paying
for the EMS calls
or they stand to lose money,
the leadership of those agencies,
they're going to encourage people to use those
de-escalation
techniques
that are in that policy.
That grant money
is really important
to a whole lot of agencies.
If you can't get them to do it
simply for the value of human life
in this country,
they'll probably do it for the value of a dollar.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}