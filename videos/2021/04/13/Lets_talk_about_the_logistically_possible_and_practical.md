---
title: Let's talk about the logistically possible and practical....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gfaWQd4njB0) |
| Published | 2021/04/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Challenges the notion of something being logistically impossible and instead focuses on practicality.
- Addresses the question of whether confiscating guns in the US is logistically impossible.
- Points out the sheer volume of firearms in the United States compared to other countries.
- Calculates the number of gun owners who might resist confiscation, based on demographics.
- Estimates that dynamic entries to confiscate guns could take a hundred years at the current rate.
- Emphasizes the need for societal change and a shift in mindset towards glorifying violence.
- Suggests that a legislative fix alone may not be enough to address the issue of gun violence.
- Advocates for focusing on nonviolent means and changing societal attitudes towards violence.

### Quotes

- "Everything's impossible until it's not."
- "To change society, you don't always have to change the law. You have to change thought."
- "We have to stop glorifying violence at every turn."

### Oneliner

Beau challenges the practicality of confiscating guns in the US, advocating for a societal shift away from glorifying violence.

### Audience

Policy makers, activists, citizens

### On-the-ground actions from transcript

- Advocate for nonviolent means of addressing societal issues (implied)
- Work towards changing societal attitudes towards violence through education and awareness campaigns (implied)

### Whats missing in summary

In-depth analysis of the challenges and implications of attempting to confiscate guns in the US.

### Tags

#GunControl #ViolencePrevention #SocietalChange #Practicality #PolicyChange


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about a question I got.
Basically, somebody was talking to someone else,
and the other person said that something
was logistically impossible.
Everything's impossible until it's not.
When you're talking about logistics,
nothing's impossible, not really.
If you have enough resources and time,
anything can be done.
The question isn't really whether or not it's
logistically impossible.
The question is whether or not it's practical.
So what we're going to do is we're going to take this
impossible feat, and we're going to see if it's practical,
because that's really what the question is.
OK.
So it was about confiscation, whether or not
it was logistically impossible to confiscate
the guns in the United States.
We've all seen the memes.
If there was ever a gun confiscation,
there would be 300 million.
No, there wouldn't.
Yeah, there's that many guns.
In the US, there are more guns than people.
And that's really the reason I want to do this, because a lot of people overseas, they
don't understand the sheer volume of firearms in the United States, so they don't understand
why we're having the issue we are.
Okay, so yeah, there's more guns than people in the U.S. 300 million, that's about right.
But they're owned by a third of the population, roughly a third.
So 100 million, we're down to 100 million, 100 million people.
Okay so if law was passed tomorrow, they're banned, you have 30 days to turn them in.
Rhetoric aside, most people would, most would.
You will have some that would not comply passively, meaning they just don't turn them in.
Then you would have some that would actively not comply, meaning set up secondary markets.
Those you're not too concerned about.
You're concerned about those who would violently resist it.
Now the odds are the percentage is going to be around four percent.
Why?
Because it's this really weird thing.
In most demographics, that is the percentage that is comfortable with violence, about four
percent. Now with this particular demographic, when you're talking about gun owners, it might be a
little bit higher simply because of the demographic, but we're just going to roll with four percent
because we're not trying to come up with a complete timeline now. We're just going to see if it's
feasible. Four percent. That knocks us down to four million. Now if you are going to attempt this,
you would end up using dynamic entry. No knocks to to get these four million. Do
we have the resources for that? Absolutely. In the United States, yeah, we
do. We just have to retool from the current prohibition to the new one. Now
Now in 2015, there were 20,000 dynamic entries.
That's the most recent year that I could find good numbers on.
Now it almost certainly has gone up since then.
So we're just going to go ahead and double it to account not
just for the natural increase, but we're making it a priority.
So we're going to say 40,000 a year.
It's going to take a hundred years.
It will take a hundred years.
It's not impossible, but it's not practical.
The idea of going door to door in the U.S. to disarm people, it's just, it's not
practical because of the sheer volume.
And you may say, hey, but what about all those other ones, those people that voluntarily
turned them out. Yeah, but those aren't the people you're worried about. If they
were willing to voluntarily give them up, those aren't the people you're concerned
with. When it comes to this issue, that's not it. Going door-to-door, that's
not it. To change society, you don't always have to change the law. You have to change
thought. You have to change the way people think. For this, yeah, I mean you do have one issue
that is a common thread in 60% of the big incidents. That's worth exploring.
That is worth exploring. As far as the rest of it, it's going to have to be a realignment of
thought. We have to stop glorifying violence at every turn. We have to stop
viewing that as the solution because when we as a society do that we can't be
surprised when other people do. When individuals within that society take it
upon themselves to act in that manner.
In the United States, Pandora's boxes opened.
More guns than people.
I would be willing to bet most of the countries that were successful in reducing or eliminating
weapons on the street through buybacks and prohibitions like that.
They probably had less than four million total.
We are way beyond a legislative fix here.
And I would point out that this was just to see whether or not it was practical.
Realistically, it's unlikely that law enforcement would have the intelligence to conduct 40,000
in a year because it's not a market. It wouldn't be that easy. It would be harder to come up with
the information to conduct them. Aside from that, when you're talking about a security clamp down,
tends to make things worse. If the goal is to not have gun violence, this probably isn't the
around to go. We have to change the way people think. We have to focus on
nonviolent means and it's gonna take time and it's horrible but that's
the reality. You know, we can come up with quick fixes but they probably won't work.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}