---
title: Let's talk about a DOD memo, intelligence, and near-peers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=C0z-tOmQO74) |
| Published | 2021/04/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The intelligence community is urged to reconsider overclassifying information by top military officials to counter near peers like Russia and China.
- Overclassification prevents sharing information with allies, non-aligned countries, and non-state actors, hindering efforts to counter propaganda.
- Secrecy in the intelligence community often revolves around protecting the sources of information rather than the information itself.
- Private intelligence companies sometimes provide better information due to the intelligence community's lack of information sharing.
- The credibility of U.S. intelligence agencies has been damaged, leading to skepticism from allies and partners.
- Regardless of potential declassification, the U.S. will increase propaganda efforts, while Russia and China will continue their own.
- Misinformation circulation is already a problem and is expected to worsen as countries compete in the information war.
- Consumers of information need to be cautious of sources and how information is framed to discern accuracy.
- With nations aiming to control the narrative to win without fighting, individuals must carefully verify the information they consume.
- Speculation suggests that declassifying satellite imagery could be a step forward in countering near peers.

### Quotes

- "The concern is that Russia and China are ramping up their propaganda efforts because we are now countering near peers."
- "The reality is the intelligence community does over classify stuff."
- "You have to be very careful about the information you're consuming and about making sure that it's true."
- "It's just something to be aware of."
- "Y'all have a good day."

### Oneliner

Top military officials urge the intelligence community to reconsider overclassifying information to counter near peers like Russia and China, enhancing information sharing and countering propaganda effectively.

### Audience

Policy makers, intelligence community members

### On-the-ground actions from transcript

- Verify information from multiple credible sources (implied)
- Stay cautious of information sources and how information is framed (implied)
- Advocate for increased transparency and information sharing within intelligence agencies (implied)

### Whats missing in summary

The full transcript provides detailed insights into the challenges posed by overclassification in the intelligence community and the implications for countering propaganda efforts from near peer countries like Russia and China.

### Tags

#IntelligenceCommunity #Propaganda #Overclassification #InformationSharing #RussiaChinaCompetiton


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today we're going to talk a little bit more about the cat and mouse world that
is the intelligence community as we move into countering near peers.
News of a memo has kind of, uh, slowly emerged the memo originated with the
department of defense, nine, four stars apparently signed it.
So we are talking about admirals and generals.
And it was to the intelligence community.
And it was basically begging them to reconsider
the way they classify stuff.
The intelligence community has a longstanding tradition
of grossly overclassifying information,
meaning giving it a higher label than it necessarily
needs to have.
If the label is too high, it can't be shared.
It can't be shared with allies.
It can't be shared with non-aligned countries.
It can't be shared with non-state actors, people who these commanders need on their side.
The concern is that Russia and China are ramping up their propaganda efforts
because we are now countering near peers.
These propaganda efforts have to be countered with either propaganda efforts from the US
or by providing actual information to debunk the propaganda put out by the other states,
by the other countries.
If the intelligence community has the information overclassified,
well, that can't happen because they can't share the information to debunk it.
This is a major concern.
We've talked about it before. The reason information is secret is normally not
the information itself. It's how that information was obtained. The fact that
the US knows about Russia's new jet fighter, that in and of itself isn't a
secret. The secret is that we have a spy somewhere who could tell us about
Russia's new jet fighter, and that's the information they're trying to protect.
The reality is the intelligence community does over classify stuff.
There is a culture of secrecy that has gone way too far.
There are private intelligence companies that, in many cases,
can provide as good or better information than our national agencies
because the intelligence community doesn't share the information.
Compounding this is the current credibility of the U.S. intelligence agencies. There was
a time when a military commander could tell an ally or a non-state actor that we were working
with, hey, this is what they say. And they'd be like, all right, that credibility doesn't exist
anymore because of, well, Iraq, mainly.
So now when commanders are like, this is what our intelligence says,
the allies or the people we're working with, they're like, yeah, show us,
because they don't believe it, with good cause, obviously.
The takeaways from this, regardless of whether or not the intelligence community
does start to declassify stuff faster and allow for a little bit more sharing of information
is that the US is definitely going to ramp up its propaganda efforts.
China and Russia are definitely going to continue and there is going to be bleed over.
meaning if they begin to propagandize nation X about information related to China, some of that
information is going to come back to the U.S. Some of that information is going to end up in U.S.
discussions and it may not necessarily be true. You also have to understand that Russia and China
are doing the exact same thing.
We have a lot of issues already with less than accurate information being circulated
in the media.
This is about to get a lot worse because countries are going to actively try to win the information
war and control the narrative, therefore they won't have to fight.
you can win the war without fighting, that's ideal. So they're going to try to
spin narratives. Be ready for it. All of this stuff from the Cold War, it's coming
back and this is another big piece of it. So as you're consuming information, you
have to be even more cautious than you were before when it comes to the source
and how the information is framed because it could be completely accurate
information but it could be put into a storyline that is well less than
accurate or they leave a key piece out that's how controlling the narrative
works and that's the goal that's what they're all three of these nations are
going to be trying to do and those of us out here trying to live our lives well
we're caught in the middle of it so you have to be very careful about the
information you're consuming and about making sure that it's true.
What they're going to do, whether or not the intelligence community is going to do this,
I don't know.
My guess is that they will start declassifying satellite imagery because that is something
that China and Russia both know, that US satellite surveillance is just amazing.
That's not a secret.
knows that US satellites are the best. So the capabilities of them, it's not really
a secret. I would imagine that foreign intelligence services already know what they are. So sharing
that information can help the commanders in the field. That would be my guess as to where
they're going to go with this. Is that really what they're going to do? No clue. They won't
tell anybody because they're shrouded in this culture of secrecy that has gone way
too far, obviously. It's at the point now where even DOD is complaining about it. So
It's just something to be aware of.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}