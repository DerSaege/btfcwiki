---
title: Let's talk about Tucker Carlson's latest and a story....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U8FgfaRI8YA) |
| Published | 2021/04/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tucker Carlson suggested questioning parents if a child is wearing a mask outside, even considering calling the authorities.
- Beau shares a story countering this, about a child he saw in his apartment complex wearing masks for weeks.
- The child had a medical condition affecting her immune system, making wearing masks necessary for her safety.
- Beau discovered the reason behind the child wearing masks when he found out the parents' concerns about germs.
- He adjusted his behavior by taking his coffee on the back porch to respect the family's need for safety.
- Beau stresses that it's not our place to judge or interfere with how parents protect their children.
- He points out that Tucker Carlson's approach lacks empathy and understanding of individual circumstances.
- Beau advocates for minding one's own business and being a good neighbor rather than policing others.
- He warns against escalating situations unnecessarily and suggests practicing empathy and respect.
- The core message is about being understanding, respectful, and not imposing judgments or actions on others.

### Quotes

- "I'm going to suggest he's wrong."
- "This isn't something that is physically harming."
- "What you are talking about are parents who are trying to keep their kids safe."
- "The right move here is to be a good person."
- "Rather than enforce your will on parents via the force of the state, that just doesn't seem like a good idea to me."

### Oneliner

Be a good neighbor, respect parents' choices, and practice empathy rather than judgment or interference in child safety.

### Audience

Community members

### On-the-ground actions from transcript

- Respect others' choices (exemplified)
- Practice empathy towards families (exemplified)

### What's missing in summary

Beau's story showcases the importance of empathy, understanding, and respect in community interactions.

### Tags

#ChildSafety #Empathy #Respect #Community #Parenting


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about Tucker
Carlson's latest little declaration, something he got on national television and said,
and I'm going to provide a counter-argument to it in the form of a story.
Now, if you don't know, Tucker Carlson went on his show and said that if you see a child
outdoors, you know, in public, whatever, outside, wearing a mask, that you should ask the parents
why, because it's repulsive and that you might even want to consider calling the authorities.
Now, if you don't know, Tucker Carlson went on his show and said that if you see a child
outdoors, you know, in public, whatever, outside, wearing a mask, that you might even want to
consider calling the authorities. I'm going to suggest he's wrong.
See, I live in this apartment complex, right? And every morning, I get up, get my coffee,
and I sit out front and drink my coffee. And the first time I'm doing this, this kid walks by,
and she cuts out into the road, walks down a little ways, gets back on the sidewalk after
she passes me. And I didn't think anything of it because, I mean, it's not the first time somebody
has crossed the street to avoid walking by me. But she was wearing a mask, two, actually,
a medical grade one and then a fabric one over it that matched her clothes.
The next day, same thing, same thing, different fabric mask, always matched. This went on for
weeks. And I do have to admit, like, after like the second or third week with her still cutting
out into the road, I'm like, I mean, good on you for being situationally aware, but
I'm kind of always here, you know. But again, don't take offense to it.
Then one day, I'm in the apartment complex office filling out like a maintenance request form. I
think my washer had broke. And an envelope comes through the little slot in the door.
And I thought it was odd because it was open. You know, the office was open. Why didn't they
just walk inside and hand it over? And the lady who works there, she's like, yeah, you know,
they're really worried about germs. And it all kind of came together. I'm like, oh,
I bet this is the parents of the girl who wears the mask. And again, don't think much of it.
But once I find out that they have this concern about germs, you know, I hadn't seen Tucker
Carlson's statement yet. So rather than, you know, harassing her or calling the authorities,
I just started taking my coffee on the back porch. That seemed like the neighborly,
polite thing to do. I guess I was wrong. Oh, see, I forgot a really important piece to this
story. This was 20 years ago. Didn't have anything to do with the current public health thing.
The kid had something wrong with her immune system. What exactly? I don't know,
because it's not my business. It was not something that I was prepared to ask. I don't know exactly.
She had some issue with her immune system. And this was her little walk. This was her way of
able to experience the world. Her parents said, okay, you can walk around the apartment complex
because she couldn't go to school. I would point out that perhaps the intellectual juggernaut
that is Tucker Carlson might be wrong on this one. Perhaps he didn't think this all the way through
for a couple of reasons. One, you don't actually know what's going on. You don't know why that
child is wearing a mask. Two, even if it is for the current public health thing, it is not your
business to chastise a parent on how they protect their kid. For all you know, this child has real
problems putting their mask on and taking it off when they walk inside and out of a building. So,
the parent just wants them to keep it on all the time when they're out. I mean,
that would make sense, right? If you have kids, I'm sure you understand.
I would also point something else out. Today, I'm a pretty calm guy. Twenty years ago,
I was somebody who was always looking to go talk to somebody. And had somebody harassed that kid
or her parents, believe me, we would have met. Not everybody has had the benefit of
twenty years of calming down. Perhaps it would be better to just mind your business.
This isn't something that is physically harming. The fact that it hurts Tucker Carlson's feelings
and he finds it repulsive, that doesn't really weigh on society or it shouldn't.
At the end of the day, what you are talking about are parents who are trying to keep their kids safe.
That's what you're talking about. And Tucker Carlson's solution to this is to harass them
or call the authorities because he's a real small government kind of guy.
I'm going to suggest he's wrong. And I think the right move here is to be a good person.
Try to do the polite thing. Rather than enforce your will on parents via the force of the state,
that just doesn't seem like a good idea to me. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}