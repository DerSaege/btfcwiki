---
title: Let's talk about who steps in when the government can't....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_AN3OzQ-TZ4) |
| Published | 2021/04/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An intelligence estimate predicts a widening gap between people's expectations of government and what governments can provide, leading to non-state actors filling the role.
- Non-state actors are individuals or groups performing government functions without being part of the government, not necessarily armed or scary.
- Large charities and individuals who influence change on the ground are examples of non-state actors.
- People who helped during public health crises by providing medical supplies are considered non-state actors.
- The focus is on ensuring non-state actors are helping rather than hurting by organizing and being prepared for turbulent times.
- Building community networks separate from government reliance can prevent bad non-state actors from emerging.
- Adaptability is key for societies to prosper in the next 20 years, and decentralized power structures can fill gaps left by governments.
- The United States government is not very adaptable, so the responsibility falls on individuals to create decentralized structures.

### Quotes

- "Non-state actor is simply a person, organization, or group that is performing a function that is typically seen as the purview of government."
- "You go ahead and fill that vacuum yourself now."
- "We just have to start organizing now."

### Oneliner

An intelligence estimate predicts a gap between people's expectations and what governments can provide, urging individuals to become non-state actors and create decentralized structures for a prosperous future.

### Audience

Community members

### On-the-ground actions from transcript

- Build community networks to prepare for turbulent times (exemplified)
- Provide medical supplies and support to local areas (exemplified)
- Organize decentralized power structures separate from government reliance (exemplified)

### Whats missing in summary

The full transcript delves deeper into the concept of non-state actors and the importance of adaptability in societies for the next two decades.

### Tags

#NonStateActors #CommunityBuilding #DecentralizedStructures #Adaptability #SocietalProsperity


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So today, we're going to talk about a term that
came up yesterday, and it prompted
a whole lot of questions.
If you missed yesterday, an intelligence estimate came out,
and it was forecasting the next 20 years or so.
One of the key findings was that there's
going to be a widening gap between what people expect
of their governments, what they want their governments to do
and provide, and what governments can
or are willing to provide.
And then it says that non-state actors
will step in to fill the role.
And that prompted the questions, because if you have only
heard that term on this channel, that's probably pretty scary.
The only times I can think of me using it
is in relation to foreign policy and conflict.
So it's always been about armed groups.
Doesn't have to be, though.
It doesn't have to be.
A non-state actor is simply a person, organization,
or group that is performing a function that is typically
seen as the purview of government, that is actually
affecting change on the ground.
But they're not part of a government.
That's what a non-state actor is.
They don't have to be armed.
Doesn't have to be scary.
Most large charities, if they're good at what they do,
and they actually succeed in influencing things
on the ground, they're non-state actors.
To take it to another level and show you
how it doesn't have to be scary at all, a whole lot of you
are non-state actors.
If you go back and think about the onset of the public health
issue here, if you participated in those live streams,
you helped get medical supplies together
that the government couldn't provide
and help hospitals get what they needed,
you're a non-state actor.
You were filling in a function that is typically
seen as the purview of government.
It doesn't have to be bad.
So that prompts the next question, though.
How do we make sure that we don't get the non-state actors
with guns and we get the non-state actors
with medical supplies?
And while that's entirely up to us, how well we organize,
how prepared we are, we are moving
into a very turbulent couple of decades,
and we have to be ready for it.
It depends on us whether or not we're
willing to step up and help our communities.
I think it would be much better if that's how it went,
and I think it's very possible.
The safest way to do this, the most direct route
to make sure that the non-state actors in this country
are the ones helping rather than hurting
are to make sure we do it ourselves.
We can lead ourselves on this.
You know your local area's needs better than most,
and there are other people who would want to help.
We've talked about community networks
over and over and over again on this channel,
and we're going to do a whole lot more on it.
But building that separate power structure
that doesn't rely on government to get stuff done,
that's how you make sure that you don't
get the bad non-state actors.
You go ahead and fill that vacuum yourself now.
That way they don't have a foothold.
They don't have a reason to show up.
Now, again, this is all based off of an estimate.
I think there's a little bit of time
to get this stuff in place, but another key element
in that estimate is that adaptability is key.
That if societies want to move forward and want to really
prosper through the next 20 years,
they have to be adaptable.
Our government in the United States
is a pretty unwieldy machine.
It's not very adaptable.
Means it falls to us.
And we can create those decentralized power structures
to fill the gaps.
We just have to start organizing now.
We just have to be ready for it now.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}