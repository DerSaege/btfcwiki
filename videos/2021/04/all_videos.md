# All videos from April, 2021
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2021-04-30: Let's talk about an update on Biden's border.... (<a href="https://youtube.com/watch?v=oND9CXTkWwQ">watch</a> || <a href="/videos/2021/04/30/Lets_talk_about_an_update_on_Biden_s_border">transcript &amp; editable summary</a>)

Biden's border situation improves, showing logistics success, urging viewers to challenge the narrative of problems being unsolvable.

</summary>

"The problem is just too big to solve. With enough time and resources and will, anything can be solved."
"It can be done; we just have to have the will to do it."
"The same people that tell you that about every other problem this country faces."
"There were cases where these kids were waiting 10 11 days, not acceptable."
"Keep that in mind the next time the people who made it seem like this was just something that couldn't be done tell you something can't be done."

### AI summary (High error rate! Edit errors on video page)

Biden's border situation was a significant topic, initially deemed unmanageable and a crisis.
Border patrol had thousands of unaccompanied minors in custody, facing long wait times for processing.
As of now, there has been a significant reduction in the number of unaccompanied minors in border patrol custody.
The average wait time for processing has decreased, showing improvement in logistics.
Despite improvements, the situation is not considered "mission accomplished" yet.
There were concerns about potential backlogs on the health and human services side, but it has been avoided so far.
The same voices that painted the border issue as unsolvable often present other societal problems as insurmountable.
Beau encourages viewers to have the will to address and solve challenges, contrary to the narrative of problems being too big to tackle.

Actions:

for advocates for proactive problem-solving,
Stay informed on border issues and advocate for humane treatment of migrants (implied)
Support organizations working towards migrant rights and well-being (implied)
Challenge narratives that portray problems as insurmountable and advocate for proactive solutions (implied)
</details>
<details>
<summary>
2021-04-30: Let's talk about SEALs retraining and what it means.... (<a href="https://youtube.com/watch?v=uADU1HHdWrY">watch</a> || <a href="/videos/2021/04/30/Lets_talk_about_SEALs_retraining_and_what_it_means">transcript &amp; editable summary</a>)

Beau explains the Navy SEALs' redefining roles amid geopolitical shifts, reassuring no imminent conflict worries.

</summary>

"They're redefining their roles, their missions."
"Yes, they are retooling to counter near peers, but that seems like more of an excuse to do this all at one time, not the real reason behind it."
"None of this should worry you."
"As far as any of this meaning that conflict is going to happen soon, there's nothing. There's nothing."
"Definitely don't let it raise your anxiety level."

### AI summary (High error rate! Edit errors on video page)

Explains that the Navy SEALs are redefining their roles and missions, not specifically training for imminent war with China and Russia.
Mentions that the image of the SEALs portrayed in the 80s is changing as they adapt to new foreign policy objectives.
Suggests that the recent retooling of the SEALs may be more of an excuse to address behavior issues within the ranks.
Talks about the tarnished image of the SEALs due to recent events like substance abuse and hazing.
Describes the current process of retraining and retooling in the SEALs, including implementing new standards, conducting psyche vows, and matching teams with suitable commanders.
Emphasizes that while the SEALs are preparing to counter near-peer threats, the larger military shifts are part of geopolitics and not indicative of imminent conflict.
Assures the audience that the ongoing changes in the military should not raise concerns about imminent conflict with other countries.
Encourages viewers not to let these developments increase their anxiety levels.

Actions:

for military enthusiasts,
Stay informed about military changes and foreign policy shifts (implied)
Share accurate information about military restructuring with others (implied)
</details>
<details>
<summary>
2021-04-29: Let's talk about military spending and shift in language.... (<a href="https://youtube.com/watch?v=FSSIg-HJcbE">watch</a> || <a href="/videos/2021/04/29/Lets_talk_about_military_spending_and_shift_in_language">transcript &amp; editable summary</a>)

Beau explains the subtle shift in military terminology towards China and Russia as peer nations, criticizing it as propaganda to justify increased defense spending, while maintaining they are actually near peers.

</summary>

"I view that as propaganda."
"I don't think that it's accurate to say that they are peer nations militarily."
"They're not the same."
"It's getting ready for Cold War 2.0."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Military commanders are now referring to China and Russia as peer nations, but Beau continues to refer to them as near peers.
Military spending and a subtle shift in the use of the term "peer nations" are discussed.
The United States spends approximately $780 billion a year on defense, more than several other countries combined.
The idea behind US military expenditures was to be able to fight a war on two fronts independently.
The US doctrine leads to spending more on defense than its two largest competitors combined.
Despite excessive military spending, Beau believes the US can only fight China and Russia to a stalemate in a two-front war scenario.
Referring to China and Russia as peers is seen as priming the populace for increased defense spending.
Beau views the shift in terminology as propaganda to justify higher military expenditures.
China is noted for its manpower, but Beau argues that without significant assets, their military capabilities are limited.
Beau maintains that the term "peer nations" is inaccurate and will continue to use "near peer."

Actions:

for policy analysts, military personnel,
Advocate for transparent military spending to ensure resources are allocated effectively (implied).
Stay informed about military policies and decisions to contribute to informed public discourse (implied).
</details>
<details>
<summary>
2021-04-29: Let's talk about Biden's speech, autocracy, and democracy.... (<a href="https://youtube.com/watch?v=yKJaTwVJ_7M">watch</a> || <a href="/videos/2021/04/29/Lets_talk_about_Biden_s_speech_autocracy_and_democracy">transcript &amp; editable summary</a>)

Biden's speech hints at framing Cold War 2.0 as democracy versus autocracy, a politically advantageous move domestically, with potential implications for lesser powers and global tensions.

</summary>

"Democracy versus autocracy, autocratic governments, Russia and China."
"If the overwhelming majority of the United States begins to view autocracy as it did communism, Trump's circle is done."
"During the Cold War, they had to match their economic system to the pole of power that they wanted to be aligned with."
"Old boss, same as the new boss."
"It won't be a red scare. It will be an autocratic one."

### AI summary (High error rate! Edit errors on video page)

Biden's recent speech hints at framing Cold War 2.0 as democracy versus autocracy, focusing on countering China and Russia.
The domestic political side must be considered when discussing foreign policy, as it can be politically advantageous for Biden.
China and Russia are unlikely to be bothered by being branded as autocracies, as many of their citizens actually favor strongman leadership.
Lesser powers in the world may benefit from this framing as they won't have to match their economic system to a specific pole of power, allowing them to pursue regional interests.
Most countries view China, Russia, and the United States as oligarchies, with a small group of people truly in charge.
The democracy versus autocracy framing serves as a motivational tool for the U.S. government to rally its populace behind its foreign policy.
This framing is seen as a good choice as it is less likely to raise global tensions compared to other options.
The speech was testing the waters for this framing, and if well-received, it will likely shape future foreign policy approaches.

Actions:

for global citizens,
Rally behind foreign policy framing (suggested)
Understand the implications for global politics (suggested)
</details>
<details>
<summary>
2021-04-28: Let's talk about Wayne LaPierre's little trip.... (<a href="https://youtube.com/watch?v=IFEbbEqBgrA">watch</a> || <a href="/videos/2021/04/28/Lets_talk_about_Wayne_LaPierre_s_little_trip">transcript &amp; editable summary</a>)

Beau addresses Wayne LaPierre's hunting incident, exposing the truth behind these trophy hunting expeditions and challenging the notion of rugged individuality.

</summary>

"It's worth seeing what that actually entails."
"It's point and shoot. They don't do anything."
"They really just drop their money so they can pretend that they are something they're not."

### AI summary (High error rate! Edit errors on video page)

Addresses Wayne LaPierre's hunting incident, revealing footage of the unmitigated failure and embarrassment.
Urges viewers to watch the footage for two reasons.
Describes the hunting expeditions where typically men pay tens of thousands of dollars to go on a safari.
Points out that in these hunts, the guides do all the work and even line up the shots for the hunters.
Emphasizes how these hunting trips are not the challenging, rugged experiences they are often portrayed as.
Criticizes the hunting culture where animals are killed for trophies and the act is passed off as a rite of manhood.
Mentions that the footage was likely obtained by The Trace.

Actions:

for animal rights activists,
Watch the footage to understand the reality of trophy hunting (suggested)
Share the footage to raise awareness about the true nature of these hunting expeditions (suggested)
</details>
<details>
<summary>
2021-04-28: Let's talk about Mario and it happening again.... (<a href="https://youtube.com/watch?v=ndc6EMuhH_M">watch</a> || <a href="/videos/2021/04/28/Lets_talk_about_Mario_and_it_happening_again">transcript &amp; editable summary</a>)

Beau stresses the importance of understanding positional asphyxiation and following protocols to prevent fatal outcomes in incidents like Mario Gonzalez's.

</summary>

"Your grip doesn't matter. How many times you press down on them harder, doesn't matter. They will not stop. It's involuntary. You're literally killing them."
"If you wait for them to stop moving to let up on them, 100% of the time you will kill them."
"People who are succumbing to this, they will not stop squirming, simply because the officer says stop resisting. It's involuntary."
"Those three things need to be talked about when this conversation occurs."
"This is the time to get that information out."

### AI summary (High error rate! Edit errors on video page)

Addressing a recent incident involving Mario Gonzalez in California where he was handcuffed, face down, and held down by officers.
Comparing the incident to George Floyd's case, noting that this incident happened faster.
Stressing the importance of considering positional asphyxiation, especially in cases involving larger individuals like Mario.
Emphasizing that weight plays a significant role in positional asphyxiation, not just the weight of the officers but also the weight of the subject.
Mentioning the significance of rolling a person into the recovery position to prevent positional asphyxiation.
Urging law enforcement officers to prioritize following protocols, such as moving individuals into the recovery position when suggested by a colleague.
Pointing out that officers' grips and commands to stop resisting do not matter in cases of positional asphyxiation, as it is involuntary.
Warning that waiting for a person to stop moving before releasing pressure can be fatal in cases of positional asphyxiation.
Stressing the need for these key points to be part of the ongoing conversation surrounding incidents like Mario Gonzalez's.
Refraining from speculating on charges until all information about the incident is available.

Actions:

for law enforcement officers,
Move individuals into the recovery position when suggested by a colleague (implied)
Prioritize understanding and preventing positional asphyxiation in law enforcement training and protocols (implied)
Educate fellow officers about the risks of positional asphyxiation and the importance of following proper procedures (implied)
</details>
<details>
<summary>
2021-04-27: Let's talk about a DOD memo, intelligence, and near-peers.... (<a href="https://youtube.com/watch?v=C0z-tOmQO74">watch</a> || <a href="/videos/2021/04/27/Lets_talk_about_a_DOD_memo_intelligence_and_near-peers">transcript &amp; editable summary</a>)

Top military officials urge the intelligence community to reconsider overclassifying information to counter near peers like Russia and China, enhancing information sharing and countering propaganda effectively.

</summary>

"The concern is that Russia and China are ramping up their propaganda efforts because we are now countering near peers."
"The reality is the intelligence community does over classify stuff."
"You have to be very careful about the information you're consuming and about making sure that it's true."
"It's just something to be aware of."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The intelligence community is urged to reconsider overclassifying information by top military officials to counter near peers like Russia and China.
Overclassification prevents sharing information with allies, non-aligned countries, and non-state actors, hindering efforts to counter propaganda.
Secrecy in the intelligence community often revolves around protecting the sources of information rather than the information itself.
Private intelligence companies sometimes provide better information due to the intelligence community's lack of information sharing.
The credibility of U.S. intelligence agencies has been damaged, leading to skepticism from allies and partners.
Regardless of potential declassification, the U.S. will increase propaganda efforts, while Russia and China will continue their own.
Misinformation circulation is already a problem and is expected to worsen as countries compete in the information war.
Consumers of information need to be cautious of sources and how information is framed to discern accuracy.
With nations aiming to control the narrative to win without fighting, individuals must carefully verify the information they consume.
Speculation suggests that declassifying satellite imagery could be a step forward in countering near peers.

Actions:

for policy makers, intelligence community members,
Verify information from multiple credible sources (implied)
Stay cautious of information sources and how information is framed (implied)
Advocate for increased transparency and information sharing within intelligence agencies (implied)
</details>
<details>
<summary>
2021-04-27: Let's talk about Tucker Carlson's latest and a story.... (<a href="https://youtube.com/watch?v=U8FgfaRI8YA">watch</a> || <a href="/videos/2021/04/27/Lets_talk_about_Tucker_Carlson_s_latest_and_a_story">transcript &amp; editable summary</a>)

Be a good neighbor, respect parents' choices, and practice empathy rather than judgment or interference in child safety.

</summary>

"I'm going to suggest he's wrong."
"This isn't something that is physically harming."
"What you are talking about are parents who are trying to keep their kids safe."
"The right move here is to be a good person."
"Rather than enforce your will on parents via the force of the state, that just doesn't seem like a good idea to me."

### AI summary (High error rate! Edit errors on video page)

Tucker Carlson suggested questioning parents if a child is wearing a mask outside, even considering calling the authorities.
Beau shares a story countering this, about a child he saw in his apartment complex wearing masks for weeks.
The child had a medical condition affecting her immune system, making wearing masks necessary for her safety.
Beau discovered the reason behind the child wearing masks when he found out the parents' concerns about germs.
He adjusted his behavior by taking his coffee on the back porch to respect the family's need for safety.
Beau stresses that it's not our place to judge or interfere with how parents protect their children.
He points out that Tucker Carlson's approach lacks empathy and understanding of individual circumstances.
Beau advocates for minding one's own business and being a good neighbor rather than policing others.
He warns against escalating situations unnecessarily and suggests practicing empathy and respect.
The core message is about being understanding, respectful, and not imposing judgments or actions on others.

Actions:

for community members,
Respect others' choices (exemplified)
Practice empathy towards families (exemplified)
</details>
<details>
<summary>
2021-04-26: Let's talk about qualified immunity and misconceptions.... (<a href="https://youtube.com/watch?v=GnlziVpT4Xg">watch</a> || <a href="/videos/2021/04/26/Lets_talk_about_qualified_immunity_and_misconceptions">transcript &amp; editable summary</a>)

Beau explains frustrations around qualified immunity, clarifying its role in civil liability, not criminal prosecution, urging focus on policy and training for accountability.

</summary>

"Ending qualified immunity will not increase prosecutions."
"Qualified immunity does not relate to criminal prosecution."
"Policy, training, and governing statutes are what matters."
"Qualified immunity as it exists today in the United States, in five years, it's not going to look like that."
"That is a fight that hasn't even started yet."

### AI summary (High error rate! Edit errors on video page)

Addressing frustrations, misconceptions, and the concept of qualified immunity in law enforcement.
Viewer frustration towards Beau for not discussing qualified immunity as a tool to hold criminal cops accountable.
Qualified immunity does not directly relate to criminal prosecution but rather to civil liability.
Explaining that ending qualified immunity won't lead to more criminal prosecutions but could make officers financially accountable.
Emphasizing the importance of focusing on policy, training, and statutes in relation to criminal prosecutions, not just qualified immunity.
Mentioning the broader scope of qualified immunity beyond law enforcement, like school administrators having it.
Predicting changes in qualified immunity within the next few years due to ongoing political momentum.
Urging attention towards policy, training, and governing statutes as the key areas needing reform for accountability.

Actions:

for advocates for police accountability,
Advocate for reforms in policy, training, and governing statutes to improve police accountability (implied).
Stay informed and engaged in local and national efforts to address qualified immunity and other issues related to police accountability (implied).
</details>
<details>
<summary>
2021-04-26: Let's talk about an injustice in the UK.... (<a href="https://youtube.com/watch?v=wLVvpM776-Q">watch</a> || <a href="/videos/2021/04/26/Lets_talk_about_an_injustice_in_the_UK">transcript &amp; editable summary</a>)

UK post office employees faced wrongful convictions due to a software bug, prompting the need for accountability and inquiries into systemic injustice in both the UK and the US.

</summary>

"That's a whole lot of people who had their lives turned upside down by a glitch in software."
"We have a lot of situations in our country that are plainly unjust, that continue to persist because people don't want to admit they're wrong about something."

### AI summary (High error rate! Edit errors on video page)

The UK post office faced significant problems from 2000 to 2014, with employees stealing thousands of dollars, leading to 736 prosecutions.
Employees were wrongfully convicted due to a bug in the Horizon software that made it appear money was missing.
Despite proclaiming innocence, many were convicted as they couldn't prove the bug's existence.
Convictions are slowly being overturned, but some affected individuals have passed away or faced severe consequences like mortgaging their homes.
Only 39 people have had their convictions overturned so far out of 2,400 claims related to this issue.
Beau suggests the need for a formal inquiry with teeth to address the injustice caused by the software glitch.
He draws parallels to the US justice system, known for favoring those with better lawyers and sometimes failing to address injustices.
Beau urges both the UK and the US to acknowledge and rectify injustices, even if it means admitting mistakes.

Actions:

for government officials, policymakers,
Advocate for a formal inquiry with teeth to address the injustices caused by the software bug (suggested).
Push for accountability and justice for those wrongfully convicted (implied).
</details>
<details>
<summary>
2021-04-25: Let's talk about wildfire tips.... (<a href="https://youtube.com/watch?v=gqCDuaDz_wQ">watch</a> || <a href="/videos/2021/04/25/Lets_talk_about_wildfire_tips">transcript &amp; editable summary</a>)

Beau provides key tips for preparing for wildfires, including evacuation planning, document copies, and house preparation, stressing the need for proactive measures in high-risk areas.

</summary>

"Have somebody you know and that everybody in your circle knows this is the person to call."
"Make sure you have a plan for all your animals and you have a radio."
"But definitely if you're in that area or a like area, take the precautions you need to."
"It is shaping up to be pretty bad this year."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Provides basic tips on preparing for wildfires after receiving feedback from viewers following a previous video on California wildfires.
Recommends having at least two evacuation destinations prepared in advance, reachable through different routes.
Suggests designating someone out of the area as a point of contact in case communication is lost during evacuation.
Advises keeping extra clothes for everyone in the vehicle and having an evacuation bag ready.
Emphasizes the importance of having copies of documents stored digitally or on a thumb drive.
Urges having a plan for pets and keeping a radio handy.
Stresses the need to bring cell phones, battery backups, and chargers.
Recommends preparing the house before leaving by closing windows and doors but leaving them unlocked, lights on for visibility.
Provides tips for preparing the house, such as disconnecting gas, AC, propane tanks, and moving flammable items away from walls.
Warns against leaving sprinklers on, as it can impact water pressure needed by firefighters.

Actions:

for residents in wildfire-prone areas,
Designate at least two evacuation destinations (suggested)
Prepare copies of documents digitally or on a thumb drive (suggested)
Have a plan for pets and keep a radio handy (suggested)
Bring cell phones, battery backups, and chargers (suggested)
Disconnect gas, AC, propane tanks, and move flammable items away from walls (suggested)
</details>
<details>
<summary>
2021-04-25: Let's talk about a plan and a wish for changing law enforcement.... (<a href="https://youtube.com/watch?v=atsxHpuqZnk">watch</a> || <a href="/videos/2021/04/25/Lets_talk_about_a_plan_and_a_wish_for_changing_law_enforcement">transcript &amp; editable summary</a>)

Beau presents a plan to reduce law enforcement's reliance on lethal force through holsters with sensors, national accreditation, and gradual system transformation, advocating for harm reduction on the path to potential abolition.

</summary>

"Just wanting to be at point B, that's not a plan, that's a wish."
"Abolition, putting a new system in place, that's getting to the hospital with that injury. That's where you want to end up."
"Nobody wants to fill out the report and nobody wants to be responsible for the bill of the EMS."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of discussing plans and wishes for significant change in how things are done.
He points out that simply wanting to be at a different point but not having a plan is merely a wish, not a plan.
Beau proposes a rough sketch of a plan to address excessive force in law enforcement by preventing officers from immediately resorting to their firearms.
He suggests using holsters with sensors that automatically call for backup and EMS when a firearm is drawn.
Beau advocates for national accreditation for law enforcement agencies to ensure adherence to use-of-force policies and proper auditing of incidents.
He argues against federalizing law enforcement due to jurisdictional differences but supports accreditation for agencies to receive federal funding.
The plan includes holsters funded by the federal government to discourage tampering and ensure accountability.
Beau explains that the ultimate goal is to reduce law enforcement's reliance on lethal force by keeping firearms holstered.
He acknowledges the debate around abolition of the current law enforcement system and suggests harm reduction measures as an interim solution.
Beau underscores the importance of gradually building a new system while undermining the current reliance on violence to facilitate its eventual replacement.

Actions:

for law reform advocates,
Advocate for national accreditation of law enforcement agencies to ensure adherence to use-of-force policies (suggested)
Support funding for holsters with sensors by the federal government to enhance officer accountability (suggested)
Encourage agencies to prioritize de-escalation techniques through grant money incentives (implied)
</details>
<details>
<summary>
2021-04-25: Let's talk about Montana's new law and a Republican change of heart.... (<a href="https://youtube.com/watch?v=KlUW41uSzqo">watch</a> || <a href="/videos/2021/04/25/Lets_talk_about_Montana_s_new_law_and_a_Republican_change_of_heart">transcript &amp; editable summary</a>)

Beau explains Montana's gun law isn't nullification but a refusal to assist in federal enforcement, challenging Republican views on sanctuary cities.

</summary>

"It's not quite the owning of the liberals that you may believe."
"I cannot wait to see the mental gymnastics necessary to oppose sanctuary cities now."

### AI summary (High error rate! Edit errors on video page)

Responding to a specific request, Beau talks about Montana's gun nullification law that recently got signed.
Clarifies that it is not a nullification law; it prevents state and local agencies from assisting in the enforcement of federal firearm laws.
Mentioning that the ATF can still enforce federal laws, despite the state law.
Emphasizes that this concept is not new, as it involves local jurisdictions choosing not to assist in enforcing federal laws they find unjust.
Points out that Montana could be considered a sanctuary state due to this stance.
Notes the Republican Party's historical opposition to this idea, making their current support interesting.
Suggests that the embracing of this idea by Republicans might lead to a change in their view on sanctuary cities.
Predicts that the law won't have a significant impact as most people already assume Montanans are armed.
Anticipates the mental gymnastics opponents of sanctuary cities will face due to this law being supported by the Republican Party.

Actions:

for political observers,
Contact local representatives to understand and support or oppose similar legislation (implied).
</details>
<details>
<summary>
2021-04-24: Let's talk about police, power, policy, and necessity.... (<a href="https://youtube.com/watch?v=dlVRvMiMUb0">watch</a> || <a href="/videos/2021/04/24/Lets_talk_about_police_power_policy_and_necessity">transcript &amp; editable summary</a>)

Knowledge is power in holding law enforcement accountable through understanding and engaging with policy for impactful reform and accountability.

</summary>

"Knowledge is power."
"If you can change the policy, well, the next time they do it and it's outside of policy, then they can be charged."
"Policy is the most powerful tool you have."

### AI summary (High error rate! Edit errors on video page)

Explains the overwhelming number of fatal shootings by law enforcement during the Chauvin trial.
Addresses why he focuses on training and policy when discussing these situations.
Challenges the notion that focusing on training and policy gives cops an excuse.
Emphasizes that incidents often involve officers stepping outside of training and policy.
Points out the importance of policy in driving accountability and reform.
Stresses the significance of policy in changing officer behavior and ensuring accountability.
Talks about the specific policies like positional asphyxiation, time, distance, and cover that are critical for accountability.
Suggests that focusing on outdated training methods can lead to accountability in cases of officer misconduct.
Differentiates between addressing systemic issues and pursuing accountability through policy.
Encourages understanding and engaging with policy for impactful reform and accountability.

Actions:

for reform advocates,
Study and understand existing policies on law enforcement conduct (implied).
Advocate for changes in policies to drive accountability and reform (implied).
</details>
<details>
<summary>
2021-04-23: Let's talk about what happens now in Ukraine.... (<a href="https://youtube.com/watch?v=L2e67gZhr9E">watch</a> || <a href="/videos/2021/04/23/Lets_talk_about_what_happens_now_in_Ukraine">transcript &amp; editable summary</a>)

Beau explains major powers' border posturing for posturing, intelligence gathering, and training reasons, aiming to project readiness without actual conflict while adapting to changing global dynamics.

</summary>

"Posturing involves projecting a willingness to fight without actually engaging in conflict."
"The scaling back of Russian troops may involve prepositioning equipment near the border to maintain readiness without drawing attention."
"The media coverage of these posturing activities will eventually desensitize the public, turning them into routine non-news events."

### AI summary (High error rate! Edit errors on video page)

US foreign policy is shifting towards countering more powerful nations rather than pursuing lesser powers.
The recent tensions on the Ukrainian border involving Russian troops backing off have raised questions about why countries engaged in such posturing activities.
Major powers like Russia posture near borders primarily for three reasons: posturing, intelligence gathering, and training.
Posturing involves projecting a willingness to fight without actually engaging in conflict.
Russia aims to test the resolve of the new US administration under President Biden.
NATO's expansion towards Russia's borders prompts Russia to demonstrate readiness to defend its interests.
Intelligence gathering occurs as countries observe and plan countermeasures in response to military buildups on their borders.
Training exercises involve mobilizing troops quickly to test readiness and familiarize with potential conflict zones.
There's also a psychological aspect where repeated posturing can desensitize opposing nations, potentially providing an edge in surprise attacks.
The scaling back of Russian troops may involve prepositioning equipment near the border to maintain readiness without drawing attention.
This prepositioning tactic was more effective in the past when information awareness was limited compared to today's battlefield.
Any potential conflict between major powers is likely to start with air-based engagements rather than ground battles.
The media coverage of these posturing activities will eventually desensitize the public, turning them into routine non-news events.
The surveillance conducted by major powers helps reduce the chances of miscommunications or misinterpretations that could lead to conflicts.
Overall, the posturing, intelligence gathering, and training activities near borders serve multiple purposes but are not indicative of an immediate desire for conflict.

Actions:

for global citizens,
Monitor international relations and be informed about geopolitical developments (implied).
Advocate for diplomatic solutions to conflicts between major powers (implied).
</details>
<details>
<summary>
2021-04-22: Let's talk about LA having to find houses.... (<a href="https://youtube.com/watch?v=NEAV_90aZzw">watch</a> || <a href="/videos/2021/04/22/Lets_talk_about_LA_having_to_find_houses">transcript &amp; editable summary</a>)

A federal judge orders LA to find housing for all homeless individuals on Skid Row, prompting Beau to suggest directly solving the issue with a billion dollars rather than just achieving measurable results through various programs.

</summary>

"Maybe it's time to set our sights a little bit higher rather than mitigating. We should actually just solve the problem."
"All of the rhetoric, promises, plans, and budgeting cannot obscure the shameful reality of the crisis."
"When you have numbers and you have a billion dollars with which to work, you could probably actually solve the problem rather than just achieve measurable results."

### AI summary (High error rate! Edit errors on video page)

A federal judge ordered LA to find housing for all homeless individuals on Skid Row after the mayor promised a billion dollars to address the issue.
In the city, there are 41,000 homeless individuals, and if you include the county, the number rises to 66,000.
With a billion dollars, it could cost roughly $15,000 per person, leaving $330 million for program administration.
Beau suggests using the money to actually solve the problem by providing housing rather than just achieving measurable results through various programs.
Building a large facility could be more cost-effective than individual units when dealing with such large numbers.
Beau agrees with the judge that the reality of increasing homelessness in LA is shameful, with more homeless individuals dying on the streets each year.
He advocates for setting higher goals and directly solving the issue rather than simply mitigating it.
Providing housing seems like the most direct route to achieving measurable results with the available billion dollars.

Actions:

for community members, policymakers,
Provide housing for homeless individuals in your community (implied)
Advocate for cost-effective solutions like building large facilities (implied)
</details>
<details>
<summary>
2021-04-22: Let's talk about Chauvin, conviction, contradiction, and compromise.... (<a href="https://youtube.com/watch?v=g6gzAj1qm_s">watch</a> || <a href="/videos/2021/04/22/Lets_talk_about_Chauvin_conviction_contradiction_and_compromise">transcript &amp; editable summary</a>)

Navigating the gray areas of activism and reform, Beau delves into contradictions in justice reform and the complex web of issues surrounding elephant poaching.

</summary>

"Because a lot of our problems, a lot of the systemic issues, they're systemic. They're interlinked."
"It becomes very hard to stay consistent because you're trying to get to that ideal, but that's not the world you exist in."
"So where do you go? We're back to going after the poachers because that's the easiest link in the chain to break."
"Most people who want prison abolition, they do so because they don't like the violence of the state."
"A little lighthouse over there saying, no, this is too far."

### AI summary (High error rate! Edit errors on video page)

Addressing the contradiction between wanting criminal justice reform and being satisfied with Derek Chauvin's imprisonment.
Acknowledging the gray areas and compromises inherent in effecting real change in a complex system.
Exploring the interconnected systemic issues related to elephant poaching and the challenges in finding solutions.
Emphasizing the difficulty of maintaining ideological consistency when actively working towards change.
Recognizing the pragmatic approach of targeting certain aspects of a problem, such as going after poachers to disrupt the chain of violence funding.
Pointing out the unrealistic nature of expecting immediate solutions to deeply rooted problems like demand for ivory.
Arguing that individuals advocating for prison abolition may not prioritize defending someone like Chauvin.
Suggesting the importance of guideposts for those navigating the gray areas of effecting change within existing systems.

Actions:

for activists and reformers,
Support elephant preserves by donating or volunteering (implied)
Advocate for sustainable jobs programs in regions affected by poaching (implied)
Raise awareness about the systemic issues contributing to poaching and violence (implied)
</details>
<details>
<summary>
2021-04-21: Let's talk about why the Chauvin verdict doesn't feel like a win.... (<a href="https://youtube.com/watch?v=EULbrMCWMCo">watch</a> || <a href="/videos/2021/04/21/Lets_talk_about_why_the_Chauvin_verdict_doesn_t_feel_like_a_win">transcript &amp; editable summary</a>)

Beau talks about the importance of meeting and exceeding standards in society, urging allies to stay committed despite the ongoing work ahead.

</summary>

"If you're grading performance, you really have three options."
"Standards are the starting point."
"You cannot stop."
"That's a reason to recommit."
"We aren't even getting the bare minimum."

### AI summary (High error rate! Edit errors on video page)

Three separate instances of having the same sobering chat with newly politically engaged allies.
Outlining the three options when grading performance: fails to meet standards, meets standards, exceeds standards.
Acknowledging that a verdict or outcome that meets standards may not always evoke a feeling of victory.
Emphasizing that meeting standards is just the starting point for a better society.
Calling attention to the fact that meeting standards should not require significant political pressure.
Describing exceeding standards as the ultimate goal for society - where actions are automatic and just.
Reminding allies and activists that progress towards exceeding standards is a continuous journey.
Encouraging newly engaged allies to stay committed despite potential disappointments and the ongoing work ahead.
Noting the societal tendency to celebrate when things almost function as they should, despite still falling short.
Urging allies to not be discouraged by the work still needed but to recommit to the cause and the people relying on them.

Actions:

for newly engaged allies,
Keep educating yourself and staying politically engaged to work towards exceeding standards (implied).
Continue putting in the work to push for societal change and progress (implied).
</details>
<details>
<summary>
2021-04-21: Let's talk about the phrase "we write the report".... (<a href="https://youtube.com/watch?v=KFfH2UCfJ7E">watch</a> || <a href="/videos/2021/04/21/Lets_talk_about_the_phrase_we_write_the_report">transcript &amp; editable summary</a>)

Be critical of official reports; contrast with personal observation to combat narrative shaping by authorities.

</summary>

"Contrast what they put here and what you saw with your own eyes."
"That phrase, we write the report, it has been around a long time."
"Make sure there is a report that can't be edited or fashioned to be less than accurate."
"They still attempt to shape public opinion by putting out less than accurate information."
"This press release was an attempt by the department to make sure that what just happened, the accountability that just occurred, to make sure it didn't and that it never would."

### AI summary (High error rate! Edit errors on video page)

Explains the problematic phrase "we write the report," suggesting that it implies a sense of authority to put anything in the report and have it believed due to public trust in officers.
Reads a report from May 25, 2020, about an incident in Minneapolis involving a man who died after a police interaction.
Points out discrepancies between the written report and what was captured on body cameras, implying that the report was crafted to shape public opinion.
Emphasizes the importance of contrasting official reports with what one sees with their own eyes.
Notes the widespread use of the phrase "we write the report" within law enforcement culture, even in the age of ubiquitous camera phones.
Urges people to be critical of official reports and press releases from law enforcement, as they may not always provide the full or accurate picture.
Encourages viewers to maintain a skeptical eye towards official narratives and to prioritize personal observation and critical thinking.
Raises awareness about the manipulation of information by authorities to avoid accountability and shape public perception.
Calls for vigilance in analyzing and questioning the information provided by law enforcement in incidents.
Advocates for holding law enforcement accountable for the accuracy and transparency of their reports and actions.

Actions:

for critical thinkers, community members,
Compare official reports with personal observations (implied)
Question law enforcement narratives (implied)
Advocate for transparency and accountability in police reports (implied)
</details>
<details>
<summary>
2021-04-20: Let's talk about reaching your right wing family and posture statements.... (<a href="https://youtube.com/watch?v=iOH8iARwFZo">watch</a> || <a href="/videos/2021/04/20/Lets_talk_about_reaching_your_right_wing_family_and_posture_statements">transcript &amp; editable summary</a>)

Beau clarifies a routine government tweet about US nukes misconstrued as a harbinger of nuclear war, stressing the need to counter fear with facts.

</summary>

"The thing is, some of the people who retweeted it with these statements, they're ex-Secret Service, ex-Air Force."
"You have to alleviate that fear or you're not going to get anywhere."
"There are thousands of Americans right now who feel that we might be on the brink of nuclear war."

### AI summary (High error rate! Edit errors on video page)

USStratCom, the government account in charge of US nukes, tweeted a vague statement that was misconstrued by right-wing personalities.
Instead of providing context, the right-wing capitalized on fear, spreading messages of imminent nuclear war.
The tweet in question was actually a preview of the posture statement, a routine occurrence justifying the organization's existence.
The annual testimony occurs to address the possibility of other countries using nukes in extreme scenarios.
Some ex-military personnel also contributed to fearmongering, despite likely understanding the context behind the tweet.
Misinformation on social media led thousands of Americans to believe in the false narrative of impending nuclear war.
Beau stresses the importance of addressing and alleviating fears with facts to have productive dialogues with those influenced by fear-based narratives.
He warns against the spread of baseless fear, particularly on social media platforms.
Beau urges people to understand and counteract the fear-driven motivations of certain groups, particularly on the right wing.
The irresponsible actions of some individuals on Twitter caused unnecessary panic and anxiety among the public.

Actions:

for social media users,
Alleviate fears with facts (implied)
Address misinformation with accurate information (implied)
</details>
<details>
<summary>
2021-04-20: Let's talk about Biden's big win, miners, and green energy.... (<a href="https://youtube.com/watch?v=KZTafuVNFIY">watch</a> || <a href="/videos/2021/04/20/Lets_talk_about_Biden_s_big_win_miners_and_green_energy">transcript &amp; editable summary</a>)

President Biden's win for green energy policies through the United Mine Workers of America shows the power of collective bargaining and challenges media stereotypes of miners.

</summary>

"Instead of casting them as ignorant hillbillies, perhaps talk to them as people who understand the power of collective bargaining because that's who they are."
"Keep insulting them and it's going to backfire on everybody."

### AI summary (High error rate! Edit errors on video page)

President Biden secured a big win for green energy policies with the support of the United Mine Workers of America.
The union leadership negotiated for funds for transitioning and training miners into green energy jobs.
Miners are not ignorant; they understand their union's power through collective bargaining.
The miners' union power relies on their numbers, which have been declining due to job losses in the coal mining sector since 2012.
Waiting to negotiate for funds in the future may result in the loss of leverage for the miners.
The media should avoid portraying miners as ignorant and instead recognize their understanding of collective bargaining.
Insulting miners can have negative consequences for both them and green energy initiatives.
Union members will access funds for retraining and transitioning, not the union itself.
Academic perspectives often overlook the real-world understanding of working-class individuals.
The importance of explaining the benefits to miners based on their current collective bargaining power.

Actions:

for working-class communities,
Advocate for fair treatment and representation of working-class individuals in media (implied)
Support union movements for transitioning workers into sustainable job sectors (implied)
</details>
<details>
<summary>
2021-04-19: Let's talk about what that caucus platform tells us..... (<a href="https://youtube.com/watch?v=OOvBGdAbfOk">watch</a> || <a href="/videos/2021/04/19/Lets_talk_about_what_that_caucus_platform_tells_us">transcript &amp; editable summary</a>)

Beau stresses the danger of dismissing concerning documents, warns against underestimating authoritarian influences, and urges staying politically active against such ideologies.

</summary>

"You can be outraged at the content. You should be."
"You have to stay politically active because those who want that brand of authoritarianism, they are still out there."
"I want to know who this group is."
"It could be lifted from any of the most horrible groups throughout history because it's the same strategy."
"That's when really horrible things happen."

### AI summary (High error rate! Edit errors on video page)

Talks about a leaked document and the importance of not dismissing it as a joke.
Expresses concern over an outside group influencing Congress with concerning documents.
Questions the silence from the Republican Party regarding the platform filled with racist content.
Points out the lack of response from colleagues in the party and the potential political motivations behind it.
Expresses fear that Biden's win may not have been decisive enough to reject Trumpism.
Warns about the dangers of underestimating the influence of groups promoting authoritarianism.
Stresses the need to stay politically active and vigilant against such ideologies.

Actions:

for politically active citizens,
Stay politically active and vigilant against authoritarian influences (implied).
</details>
<details>
<summary>
2021-04-17: Let's talk about where his parents were.... (<a href="https://youtube.com/watch?v=tvOTlhDp-qQ">watch</a> || <a href="/videos/2021/04/17/Lets_talk_about_where_his_parents_were">transcript &amp; editable summary</a>)

Beau questions societal assumptions and systemic failures in addressing the tragic death of a 13-year-old boy in Chicago.

</summary>

"Where were his parents? Why was he hanging out with someone in their 20s?"
"In this child's very short life, he wound up being the victim. Over and over and over again."
"The entire society failed this boy."
"It's a tragedy. But that's true of pretty much any time a 13-year-old dies."
"This is a systemic failure on every front."

### AI summary (High error rate! Edit errors on video page)

Addresses questions circulating on social media about a situation in Chicago.
Questions the assumptions behind these inquiries and decides to answer them differently.
Raises questions about a young boy's background, behavior, and decisions leading to his victimization.
Points out the systemic neglect and stereotypes faced by marginalized neighborhoods.
Contrasts the treatment of different communities based on race and socioeconomic status.
Criticizes the system for perpetuating poverty and dangerous environments.
Questions why a 13-year-old was in a situation that led to his death.
Condemns the society's failure to protect and support the boy.
Expresses frustration at the lack of meaningful change or response to such tragedies.
Calls attention to the systemic failures and societal shortcomings that contributed to the boy's death.

Actions:

for advocates for social justice,
Challenge stereotypes and prejudices within your community (implied)
Advocate for equitable resources and support for marginalized neighborhoods (implied)
Support initiatives that address poverty and systemic neglect in underserved areas (implied)
</details>
<details>
<summary>
2021-04-16: Let's talk about police training and change.... (<a href="https://youtube.com/watch?v=jIqF5kHFv70">watch</a> || <a href="/videos/2021/04/16/Lets_talk_about_police_training_and_change">transcript &amp; editable summary</a>)

Beau stresses the urgent need for a cultural shift in law enforcement to prioritize risk mitigation and effective training practices to prevent unjust deaths.

</summary>

"There hasn't been any new policies. There haven't been any new protocols to keep people safe."
"They have been doing it wrong."
"We need change in the culture of law enforcement, a lot of it."

### AI summary (High error rate! Edit errors on video page)

Recalls incidents dating back to 1992 and stresses the long-standing nature of issues in law enforcement training and practices.
Considers the root causes of problems in law enforcement tactics and sees them as a combination of training issues and deeper cultural problems within the institution.
Compares the quick adoption of new policies in response to public health crises with the lack of progress in implementing new protocols in law enforcement despite knowing best practices.
Expresses frustration at the lack of change in law enforcement tactics, with videos detailing necessary techniques dating back 10-25 years.
Criticizes law enforcement leadership for being unwilling to change and prioritizing mimicking military high-speed teams over adopting effective practices to mitigate risk.
Points out the military's willingness to adapt and change tactics compared to the resistance to change within law enforcement.
Gives an example of swift action taken by military leadership in response to an incident involving a white NCO and a black civilian, contrasting it with the general unwillingness of law enforcement to acknowledge and correct mistakes.
Emphasizes the need for a cultural shift in law enforcement to prioritize risk mitigation and the adoption of effective, proven tactics to prevent unjust deaths.

Actions:

for law enforcement reform advocates,
Advocate for and support the implementation of new policies and protocols in law enforcement to prioritize safety and mitigate risks (exemplified).
Push for cultural changes within law enforcement institutions to prioritize training and willingness to change (implied).
</details>
<details>
<summary>
2021-04-15: Let's talk about Chicago and strobes.... (<a href="https://youtube.com/watch?v=E3J1a3iAPek">watch</a> || <a href="/videos/2021/04/15/Lets_talk_about_Chicago_and_strobes">transcript &amp; editable summary</a>)

The Chicago incident raises concerns about the under-discussed misuse of strobe lights in law enforcement and the importance of proper training and usage protocols.

</summary>

"Using them outside of those purposes because it's cool doesn't lend to getting the best results."
"Understand you don't use it like a normal light."
"I want to know how many officers have those strobes on their weapons and how many were trained to use them."

### AI summary (High error rate! Edit errors on video page)

Chicago incident involving the use of a strobe light is under-discussed.
Strobe lights are disorienting and affect vision in low-light situations.
Proper use of strobe lights differs from constant lights; they should be shined in the eyes.
Using a strobe light alone can create misperceptions of movements due to the brain filling gaps.
Lack of training on strobe light usage might have influenced the Chicago incident.
Second officer shining a constant light on the suspect is a recommended practice with strobe lights.
Misusing tools like strobe lights in law enforcement can lead to undesired outcomes.
Questions arise about officer training in using strobe lights effectively.
Importance of understanding the specific purposes and correct usage of law enforcement gadgets.
Civilian oversight should inquire about officers with strobes on their weapons and their training.

Actions:

for law enforcement oversight,
Inquire about officers with strobe lights on their weapons and their training (implied)
</details>
<details>
<summary>
2021-04-15: Let's talk about Biden's plan for leaving, options, and speculation.... (<a href="https://youtube.com/watch?v=NFxF9R-mnpQ">watch</a> || <a href="/videos/2021/04/15/Lets_talk_about_Biden_s_plan_for_leaving_options_and_speculation">transcript &amp; editable summary</a>)

Biden's unclear Afghanistan withdrawal plan lacks sense, raising speculation and urging for a swift exit.

</summary>

"We just need to get out now."
"With a lot of these options, it's still not a good idea."
"I truly feel like there is something that isn't publicly known."
"This is just a huge error, and we should get out as soon as possible."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Biden's plan for withdrawing from Afghanistan lacks clarity and sense.
Moving a couple thousand troops quickly shouldn't be difficult for the U.S.
The publicly stated reasons for staying beyond the deadline seem illogical.
Establishing another arbitrary deadline contradicts past knowledge.
Speculation arises due to missing information about the withdrawal plan.
Six potential reasons for the withdrawal plan's current course are discussed.
Options range from optics to potential power vacuums and privatization.
Privatizing the withdrawal using contractors is deemed a bad idea.
An extreme and cynical reason involves capitalizing on possible opposition responses.
Beau lacks a firm opinion due to the lack of complete information.
The current approach to withdrawal could be a significant error.
Beau suggests the need to get out of Afghanistan as soon as possible.

Actions:

for policymakers, activists,
Contact policymakers to advocate for a clear and efficient withdrawal plan (implied).
Stay informed and engaged with updates on the situation in Afghanistan (implied).
</details>
<details>
<summary>
2021-04-14: Let's talk about what a busy signal can teach everyone.... (<a href="https://youtube.com/watch?v=wHPn-3mjxZM">watch</a> || <a href="/videos/2021/04/14/Lets_talk_about_what_a_busy_signal_can_teach_everyone">transcript &amp; editable summary</a>)

Beau underlines the urgency of getting vaccinated by contrasting the ease of scheduling a vaccine appointment with the arduous process of seeking federal reimbursement for final expenses due to the public health crisis.

</summary>

"The urgency is lost."
"But you can go get your shot and make sure that nobody has to call that number about you."

### AI summary (High error rate! Edit errors on video page)

Beau receives a video request from someone stating that he is good at creating compelling arguments that prompt people to action.
The urgent need for people to get vaccinated is emphasized as appointments are being missed and time slots left unfilled.
Beau considers various approaches to encourage people to get vaccinated, including live-streaming his second shot and using persuasive arguments.
Instead of the traditional approaches, Beau shares a powerful analogy about calling a number for federal reimbursement for a loved one's final expenses due to the public health crisis, contrasting it with the ease of scheduling a vaccine appointment.
He underlines the importance of getting vaccinated to avoid being a burden on loved ones who might have to seek reimbursement for final expenses.
Beau provides details about the eligibility criteria for federal reimbursement for final expenses, including the necessity for the incident to have occurred in the US after January 20, 2020.
He points out the stark contrast between the half-million people calling for reimbursement and the availability of vaccine appointments with operators ready.
Beau encourages viewers to get vaccinated promptly to protect themselves and others from potential health issues.
The analogy of a busy signal for federal reimbursement versus operators standing by for vaccine appointments serves as a strong call to action for getting vaccinated.
He ends the video with a reminder to take the necessary steps to ensure no one has to call the reimbursement number for their final expenses due to the public health issue.

Actions:

for public health advocates,
Schedule a vaccine appointment to protect yourself and others (exemplified)
Share Beau's analogy about the importance of vaccination with others in your community (suggested)
</details>
<details>
<summary>
2021-04-14: Let's talk about California needing to get ready.... (<a href="https://youtube.com/watch?v=rFcUfedhrbA">watch</a> || <a href="/videos/2021/04/14/Lets_talk_about_California_needing_to_get_ready">transcript &amp; editable summary</a>)

Beau provides a warning about the extreme wildfire risk in California due to record-low fuel moisture content and urges immediate preparedness for potential rapid onset events like lightning strikes.

</summary>

"Terrifyingly low."
"Fire loves dry stuff."
"Be ready and get ready now."
"When it comes to stuff like this, minutes may matter."
"Be aware of this situation."

### AI summary (High error rate! Edit errors on video page)

Providing a heads up for California, discussing the similarities and differences between hurricanes in Florida and wildfires in California.
Mentioning the challenges of getting advance warning for wildfires compared to hurricanes.
Explaining the concept of fuel moisture content as a measurement of combustibility.
Describing how scientists measure fuel moisture content by taking clippings, weighing them, letting them dry, and weighing them again.
Noting that the fuel moisture content in California is currently at its lowest observed level ever.
Quoting San Jose State University Fire Weather Lab describing the situation as terrifyingly low.
Pointing out the impact of two rainy seasons without much rain in California, leading to extremely dry conditions.
Stating that fire loves dry material, hinting at the high risk due to the abundance of dry vegetation.
Emphasizing the importance of being prepared for wildfires by having a packed bag, knowing where documents are, and having a plan for evacuation.
Urging people to be prepared, especially considering the potential rapid onset of wildfires due to lightning strikes.

Actions:

for california residents,
Prepare a bag with essentials, locate documents, and make an evacuation plan (implied).
Ensure vehicles have gas and be aware of the potential impact of lightning events (implied).
Stay informed about the wildfire situation and follow official bulletins and updates (implied).
</details>
<details>
<summary>
2021-04-13: Let's talk about the logistically possible and practical.... (<a href="https://youtube.com/watch?v=gfaWQd4njB0">watch</a> || <a href="/videos/2021/04/13/Lets_talk_about_the_logistically_possible_and_practical">transcript &amp; editable summary</a>)

Beau challenges the practicality of confiscating guns in the US, advocating for a societal shift away from glorifying violence.

</summary>

"Everything's impossible until it's not."
"To change society, you don't always have to change the law. You have to change thought."
"We have to stop glorifying violence at every turn."

### AI summary (High error rate! Edit errors on video page)

Challenges the notion of something being logistically impossible and instead focuses on practicality.
Addresses the question of whether confiscating guns in the US is logistically impossible.
Points out the sheer volume of firearms in the United States compared to other countries.
Calculates the number of gun owners who might resist confiscation, based on demographics.
Estimates that dynamic entries to confiscate guns could take a hundred years at the current rate.
Emphasizes the need for societal change and a shift in mindset towards glorifying violence.
Suggests that a legislative fix alone may not be enough to address the issue of gun violence.
Advocates for focusing on nonviolent means and changing societal attitudes towards violence.

Actions:

for policy makers, activists, citizens,
Advocate for nonviolent means of addressing societal issues (implied)
Work towards changing societal attitudes towards violence through education and awareness campaigns (implied)
</details>
<details>
<summary>
2021-04-13: Let's talk about Biden picking Christine Wormuth for Secretary of the Army.... (<a href="https://youtube.com/watch?v=5GaLQt-bJyg">watch</a> || <a href="/videos/2021/04/13/Lets_talk_about_Biden_picking_Christine_Wormuth_for_Secretary_of_the_Army">transcript &amp; editable summary</a>)

President Biden's nominee for Secretary of the Army, Christine Wormuth, brings policy expertise, values local partnerships and international alliances, and prioritizes effective defense spending.

</summary>

"The big headline here is that if confirmed, Christine Wormuth will be the first woman Secretary of the Army."
"She is also real big on international alliances."
"In the past, she has spoken a lot about how money is spent."
"She won't be attempting to prepare for the next engagement by preparing for the last."
"I don't know of anything controversial in her history."

### AI summary (High error rate! Edit errors on video page)

President Biden's pick for Secretary of the Army, Christine Wormuth, will be the first woman to hold this position.
The Secretary of the Army is a civilian role that focuses on policy and administrative duties, not military operations.
Wormuth's resume includes positions in the DOD, Center for Strategic and International Studies, and as Under Secretary of Defense for Policy under Obama.
She values local partnerships and international alliances, which is a contrast to the previous administration.
Wormuth's lack of experience with defense contractors like Raytheon or Lockheed Martin is seen as a positive, as the Secretary of the Army influences spending decisions.
She prioritizes ensuring that money spent on defense produces effective results, rather than just meeting spending quotas.
Wormuth's background at Rand Corporation gives her insights on handling near peers and avoiding preparing for the last engagement.
Beau views her as a good pick for Secretary of the Army, without any known controversial history, and expects a smooth confirmation process.

Actions:

for policy enthusiasts, military analysts,
Research local partnership opportunities for community defense initiatives (implied)
</details>
<details>
<summary>
2021-04-12: Let's talk about an update on Biden's make or break moment.... (<a href="https://youtube.com/watch?v=xHvKSA9M13Y">watch</a> || <a href="/videos/2021/04/12/Lets_talk_about_an_update_on_Biden_s_make_or_break_moment">transcript &amp; editable summary</a>)

President Biden's delicate negotiation process with Iran requires tough decisions to bring them back into the international community, despite domestic and international pressures.

</summary>

"If I'm Biden, if I'm president, I remove them all. I go ahead and do it."
"At the end of the day, getting back in this deal, getting Iran back into the international community, getting them out of isolation, that needs to be the goal."
"There's still a risk of reaching an impasse."

### AI summary (High error rate! Edit errors on video page)

President Biden's efforts to bring the United States and Iran back into a deal are progressing slowly in Vienna.
Both countries want back in the deal, but there are sticking points to work out.
Iran wants all sanctions lifted at once, while the US prefers a step-by-step approach.
There's disagreement on the sequencing of actions and who should go first.
Biden faces pressure at home to appear strong, but removing sanctions could provide more leverage.
In Iran, there's an upcoming election, and they cannot appear to have caved to the US.
A key decision-maker in Iran is not in favor of a gradual lifting of sanctions.
Biden has the political capital to remove all sanctions and kickstart his Middle East foreign policy plans.
Getting Iran out of isolation and back into the international community is a critical step.
The ongoing negotiation process hasn't fallen apart but hasn't been completed yet.
There's still a risk of reaching an impasse, which could cause significant delays.
Beau suggests Biden should take the short-term hit in polls to move the negotiations forward.
Ultimately, bringing Iran back into the international community is key for Biden's broader foreign policy goals.

Actions:

for diplomacy enthusiasts,
Contact local representatives to advocate for a diplomatic resolution (suggested)
Stay informed about the ongoing negotiations (suggested)
</details>
<details>
<summary>
2021-04-12: Let's talk about CEOs vs the GOP vs Trump.... (<a href="https://youtube.com/watch?v=ikVjPelsIy0">watch</a> || <a href="/videos/2021/04/12/Lets_talk_about_CEOs_vs_the_GOP_vs_Trump">transcript &amp; editable summary</a>)

100 CEOs plan corporate pushback against Republican voting restrictions, while Trump's divisive actions create challenges and opportunities for both parties.

</summary>

"Former President Trump's recent speech to Republican donors focused on personal grievances and attacking fellow Republicans."
"The Republican Party, post-Trump, faces challenges in managing his unpredictable actions, fundraising obstacles, and corporate backlash."
"A significant political realignment in the US seems possible, with the Republican Party needing to evolve to stay relevant."

### AI summary (High error rate! Edit errors on video page)

100 CEOs had a conference call to strategize flexing their corporate muscles against Republican efforts to restrict voting access.
They view the Republican push as based on a lie and discussed freezing campaign contributions or investments in response.
Former President Trump's recent speech to Republican donors focused on personal grievances and attacking fellow Republicans.
Trump's actions create division within the Republican Party, unsettling major donors who dislike internal battles.
The Republican Party, post-Trump, faces challenges in managing his unpredictable actions, fundraising obstacles, and corporate backlash.
To survive, the Republican Party may need to rebrand as the party of the working class, requiring a shift in policies and rhetoric.
Democrats could benefit from increased corporate funding while leveraging opposition to Trump to motivate their base.
Biden's promises and opposition to Trump may drive negative voter turnout against Republicans.
A significant political realignment in the US seems possible, with the Republican Party needing to evolve to stay relevant.
Democrats have an opening to make political gains amidst the Republican Party's struggles.

Actions:

for political analysts, activists,
Contact local organizations to support voting rights initiatives (exemplified)
Join or support advocacy groups working towards fair voting practices (exemplified)
Organize community events to raise awareness about the importance of voting (exemplified)
</details>
<details>
<summary>
2021-04-11: Let's talk about who steps in when the government can't.... (<a href="https://youtube.com/watch?v=_AN3OzQ-TZ4">watch</a> || <a href="/videos/2021/04/11/Lets_talk_about_who_steps_in_when_the_government_can_t">transcript &amp; editable summary</a>)

An intelligence estimate predicts a gap between people's expectations and what governments can provide, urging individuals to become non-state actors and create decentralized structures for a prosperous future.

</summary>

"Non-state actor is simply a person, organization, or group that is performing a function that is typically seen as the purview of government."
"You go ahead and fill that vacuum yourself now."
"We just have to start organizing now."

### AI summary (High error rate! Edit errors on video page)

An intelligence estimate predicts a widening gap between people's expectations of government and what governments can provide, leading to non-state actors filling the role.
Non-state actors are individuals or groups performing government functions without being part of the government, not necessarily armed or scary.
Large charities and individuals who influence change on the ground are examples of non-state actors.
People who helped during public health crises by providing medical supplies are considered non-state actors.
The focus is on ensuring non-state actors are helping rather than hurting by organizing and being prepared for turbulent times.
Building community networks separate from government reliance can prevent bad non-state actors from emerging.
Adaptability is key for societies to prosper in the next 20 years, and decentralized power structures can fill gaps left by governments.
The United States government is not very adaptable, so the responsibility falls on individuals to create decentralized structures.

Actions:

for community members,
Build community networks to prepare for turbulent times (exemplified)
Provide medical supplies and support to local areas (exemplified)
Organize decentralized power structures separate from government reliance (exemplified)
</details>
<details>
<summary>
2021-04-10: Let's talk about predictions for 2040.... (<a href="https://youtube.com/watch?v=FIFfdbq5ntg">watch</a> || <a href="/videos/2021/04/10/Lets_talk_about_predictions_for_2040">transcript &amp; editable summary</a>)

Be prepared for global change towards social democracy or authoritarianism; adapt or face extinction.

</summary>

"Any nation-state that does not adapt in the next twenty years isn't going to make it."
"Over the next two decades, we have to be prepared for change, and if we're smart, we're going to have to direct it."
"When you're talking about shifting modes of governance at this level, it seems to me, reading between the lines in this assessment, is that we're going to move to social democracy, or we're going to move to something that is incredibly authoritarian."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a report by the National Intelligence Council, Global Trends 2040, as a mid to long term assessment.
The report outlines five themes: global challenges like climate change and technological disruption, fragmentation at different levels, disequilibrium between governments and citizens, contestation of affiliations, and the necessity for adaptation.
Structural forces at play include slowing population growth, increased migration, climate change, digital currencies, and advancing technology.
Social dynamics predict pessimism, distrust, information silos, and undermining of civic nationalism.
The assessment suggests that non-state actors will start taking over government functions globally, leading to new models of governance.
China and the United States remain dominant powers in most scenarios, with other poles of power emerging.
Five scenarios outlined in the report are Renaissance of Democracies, World Adrift, Competitive Coexistence, Separate Silos, and Tragedy and Mobilization.
Beau stresses the importance of preparing for change and directing it towards social democracy rather than authoritarianism in the future.

Actions:

for global citizens,
Read the report "Global Trends 2040" to understand future challenges and opportunities (suggested).
Stay informed about global trends and issues to be prepared for upcoming changes (implied).
</details>
<details>
<summary>
2021-04-08: Let's talk about stepping in the wrong spot.... (<a href="https://youtube.com/watch?v=NwjBG4PsqRQ">watch</a> || <a href="/videos/2021/04/08/Lets_talk_about_stepping_in_the_wrong_spot">transcript &amp; editable summary</a>)

Beau argues for the immediate discontinuation of landmines, stressing their obsoleteness and potential harm even after conflicts have ended.

</summary>

"We shouldn't. Period. Full stop. End of story."
"There is no reason to keep these things. It needs to go away."
"Seems like handing your opposition tools doesn't make a whole lot of sense."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of Princess Diana but shifts to discussing the Biden administration's consideration of continuing the use of landmines.
Describes a hypothetical scenario of stepping on a landmine while exploring a peaceful area due to remnants left behind by the U.S.
Questions the necessity of the U.S. continuing to use landmines, arguing that they serve no purpose and are antiquated.
Points out that conventional conflicts, like the one depicted in M*A*S*H, are no longer common, making the use of landmines even more unnecessary.
Argues that drones are a more effective and less harmful alternative to landmines in denying access to an area for the opposition.
Advocates for the discontinuation of landmines, citing the potential dangers they pose even after conflicts have ended.
Emphasizes that leaving behind tools like landmines for the opposition is illogical and unnecessary.
Urges for the immediate removal of landmines without the need for further debate or consideration.
Raises concerns about the risk posed to troops by leaving large quantities of landmines behind.
Calls for action to remove and discontinue the use of landmines without delay.

Actions:

for advocates and activists,
Contact your representatives to advocate for the immediate discontinuation of landmines (implied).
Support organizations working towards a global ban on landmines (implied).
</details>
<details>
<summary>
2021-04-08: Let's talk about Biden's gun control plans.... (<a href="https://youtube.com/watch?v=9n4jYXausyM">watch</a> || <a href="/videos/2021/04/08/Lets_talk_about_Biden_s_gun_control_plans">transcript &amp; editable summary</a>)

Beau explains why President Biden's gun control announcement falls short and is deemed ineffective by all sides involved.

</summary>

"Nobody on any side of this issue is going to be happy with this."
"Thoughts and prayers doesn't mean a thing."
"It's all pretty much ineffective."
"This is a miss from the Biden administration."
"At the end of the day, nobody should be happy, nobody should be mad."

### AI summary (High error rate! Edit errors on video page)

President Biden's announcement on gun control is a mix of executive orders, legislative proposals, and an appointment.
Criticism arises that the executive orders do not go far enough, but Beau argues against further expansion due to limitations of presidential authority.
The executive orders primarily focus on unregistered firearms and stabilizing braces on pistols, which Beau deems as ineffective measures.
Beau suggests that the issue of homemade firearms is complicated by the nature of gun components and advancements in technology like 3D printing.
He explains the function of stabilizing braces and dismisses claims that they significantly enhance a pistol's lethality.
Beau expresses skepticism about the potential effectiveness of the executive directive to develop red flag law legislation at the state level.
In terms of legislative proposals, Beau sees hope in addressing domestic violence-related gun violence but doubts their passage in the Senate.
He criticizes the limited scope of proposed legislation and its connection to broader gun control themes that may face significant opposition.
Beau predicts challenges in confirming the Biden administration's ATF director appointment due to past associations and political dynamics.
Overall, he concludes that the announced measures are largely ineffective and unlikely to satisfy any side of the gun control debate.

Actions:

for advocates for effective gun control.,
Contact your representatives to advocate for comprehensive gun control measures (suggested).
Stay informed about legislative developments related to gun control and advocate for meaningful change in your community (implied).
</details>
<details>
<summary>
2021-04-07: Let's talk about the near-peer Olympics, Biden, and Republicans.... (<a href="https://youtube.com/watch?v=JZ_P8Qztppc">watch</a> || <a href="/videos/2021/04/07/Lets_talk_about_the_near-peer_Olympics_Biden_and_Republicans">transcript &amp; editable summary</a>)

The Biden administration considers boycotting the 2022 Winter Games in Beijing over human rights, sparking mixed reactions, but it's all part of normal diplomatic maneuvers in near-peer contests.

</summary>

"So don't panic over any of this. It's all pretty normal."
"Expect to see a lot of it."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The Biden administration is considering boycotting the 2022 Winter Games in Beijing due to human rights concerns.
This potential boycott is not an escalation but a diplomatic move that occurs during near peer contests.
Historical examples of boycotts during the Olympic Games include the 1980 Moscow Games and the 1976 Montreal Games.
The Biden administration's stance is causing different reactions, with some Republicans calling for the games to be moved and others for an outright boycott.
Olympic teams are private entities, and while the administration can organize them, it is up to the teams to decide on participating in a boycott.
Republicans calling for a boycott against a jurisdiction mirrors similar tactics used in the past.
Expect more diplomatic shows and boycott calls in near-peer foreign policy situations.
A boycott could have a significant impact by drawing attention to the administration's concerns, especially with larger nations like China.
Despite sounding trivial, not participating in the Winter Games can have real consequences and influence.
The outcome of this situation will depend on how it plays out and whether it effectively raises awareness.

Actions:

for diplomacy watchers,
Organize peaceful demonstrations or campaigns to raise awareness about human rights issues in China (implied)
</details>
<details>
<summary>
2021-04-07: Let's talk about policy, practice, and the trial.... (<a href="https://youtube.com/watch?v=G5UK_nw4GEw">watch</a> || <a href="/videos/2021/04/07/Lets_talk_about_policy_practice_and_the_trial">transcript &amp; editable summary</a>)

Beau addresses discrepancies in law enforcement practices, policy, and culture amid ongoing Chauvin trial testimony, urging for accountability and challenging defense expert claims.

</summary>

"The policies are set up that way because when something does happen, well, it's a big deal."
"There is a running conversation between people like cops and cops themselves."
"It's ingrained, and I don't see that changing."
"But at the same time, most officers have used a prone restraint over and over and over again without issue."
"Hopefully, the bad apple gets removed."

### AI summary (High error rate! Edit errors on video page)

Addressing questions surrounding the Chauvin trial and the testimony about use of force.
Explaining the concept of positional asphyxiation and its relevance in the trial.
Pointing out the discrepancy between policy and practice regarding prone restraints in law enforcement.
Describing how policies aim to prevent specific outcomes but are not always followed on the ground.
Noting the ingrained culture within law enforcement that resists change in operational practices.
Expressing skepticism towards defense experts who may deny the existence of positional asphyxiation.
Emphasizing the importance of specific questioning by the prosecutor in challenging defense experts' claims.
Acknowledging the ongoing nature of the trial and the hope for accountability.

Actions:

for law enforcement reform advocates,
Challenge ingrained law enforcement practices through community advocacy (implied)
Support accountability measures within police departments (implied)
Advocate for specific questioning of defense experts in trials (implied)
</details>
<details>
<summary>
2021-04-06: Let's talk about surprising Senate shenanigans and infrastructure.... (<a href="https://youtube.com/watch?v=i7_lZlc0hhs">watch</a> || <a href="/videos/2021/04/06/Lets_talk_about_surprising_Senate_shenanigans_and_infrastructure">transcript &amp; editable summary</a>)

The parliamentarian's surprising decision allows Democrats to revise a previous resolution, making Biden's infrastructure bill likely to pass with significant funding for various vital sectors.

</summary>

"Surprise. Determined that the Democrats could revise or amend a previous resolution."
"Biden's big infrastructure bill, yeah, that's probably going through."
"Money to get rid of all the lead pipes, money for public housing, money for schools, money for high speed internet in rural areas."
"It's a big infrastructure package."
"It's just a thought y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The parliamentarian's surprising determination allows Democrats to revise a previous resolution.
Biden's infrastructure bill is likely to pass despite Democratic infighting.
Democrats may amend the 2021 resolution to include uncontroversial parts of the bill.
Contentious aspects could be pushed into a resolution for fiscal year 2022.
Funding in the bill includes removing lead pipes, public housing, electrical grid updates, VA hospitals, schools, rural internet, Amtrak, electric vehicles, child care facilities, and future public health prevention.
Passing the bill through regular Senate channels without budget reconciliation seems unlikely due to Republican opposition.
The parliamentarian's decision makes it probable that the majority of the infrastructure package will pass.

Actions:

for politically engaged citizens.,
Advocate for the passing of the infrastructure bill through contacting elected representatives (implied).
Stay informed about the progress of the infrastructure bill and its implications for different sectors (implied).
</details>
<details>
<summary>
2021-04-05: Let's talk about the difference in the next surge.... (<a href="https://youtube.com/watch?v=7BMEOIPWn_c">watch</a> || <a href="/videos/2021/04/05/Lets_talk_about_the_difference_in_the_next_surge">transcript &amp; editable summary</a>)

Beau clarifies support for bodily autonomy and advocates for vaccines, warning about the fourth COVID-19 surge and stressing the importance of ongoing precautions until vaccination rates are high.

</summary>

"You own your body."
"Vaccines are good. I do not object to them. I object to force, not vaccines."
"We have to continue to do this until we're there, until the vaccination rates are high enough."
"Every variant is a variant that might be resistant to the inoculation, to the vaccine."
"Wash your hands. Don't touch your face. Stay at home. If you have to go out, wear a mask."

### AI summary (High error rate! Edit errors on video page)

Wishes happy birthday to Cleo before diving into discussing the potential fourth surge of COVID-19.
Clarifies his stance on bodily autonomy, asserting support for personal decisions regarding tattoos, piercing, family planning, and vaccinations.
Expresses being vaccinated against various diseases, including receiving the COVID-19 vaccine.
Supports vaccines but opposes the idea of force, advocating for convincing others rather than mandating vaccinations.
Notes the beginning of the fourth surge, likely due to a new variant, B.1.1.7, affecting younger age groups.
Mentions laxity in following precautions, attributing it to fatigue and burnout after a year of vigilance.
Emphasizes the importance of maintaining precautionary measures until vaccination rates are significantly higher.
Alerts about the potential resistance of new variants to vaccines and stresses the need for continued vigilance.
Acknowledges the coverage of the current vaccines against the B.1.1.7 variant.
Urges people to uphold preventive measures like hand washing, mask-wearing, and staying home, while also advocating for vaccination.

Actions:

for general public,
Wash your hands, avoid touching your face, stay home if possible, wear a mask when going out, and get vaccinated (exemplified).
</details>
<details>
<summary>
2021-04-05: Let's talk about me.... (<a href="https://youtube.com/watch?v=Rcv5fuOdcZs">watch</a> || <a href="/videos/2021/04/05/Lets_talk_about_me">transcript &amp; editable summary</a>)

Non-government organization used Beau's content to de-radicalize individuals searching for violence, sparking pushback against him, but the program's impact remains paramount.

</summary>

"I don't care about dragging my name through the mud. It's about that program."
"On a long enough timeline our side wins guaranteed."
"Apparently ads are a good way to do it."
"If you have any questions about my views on anything, I have 1,300 videos recorded over the last couple of years."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Non-government organization used Beau's content in ads to de-radicalize individuals searching for violent information.
Beau received messages from viewers saying his videos helped them get off a bad path, which he considered wins.
Beau was flattered and proud when he found out about the organization using his content for de-radicalization.
Beau mentions he wasn't asked for permission but indicates he would have agreed to the use of his content.
Beau refrained from making a video about this program to maintain its organic appearance.
Some individuals started pushing out negative information about Beau, possibly to undermine the de-radicalization program.
Beau addresses past accusations and incidents like alien smuggling that were used to discredit his involvement in the program.
Beau believes relatability is key in reaching individuals searching for violent information, not just clean-cut individuals.
Despite efforts to undermine the program using Beau, he states that it will continue to do good.
Beau remains committed to creating videos and helping people even if his content is no longer used in the de-radicalization program.

Actions:

for online content creators,
Support de-radicalization programs (implied)
Continue creating content that helps individuals in need (implied)
Refrain from spreading negative information to undermine programs aimed at stopping violence (implied)
</details>
<details>
<summary>
2021-04-05: Let's talk about Biden's must-win moment tomorrow.... (<a href="https://youtube.com/watch?v=-aaw8O78faI">watch</a> || <a href="/videos/2021/04/05/Lets_talk_about_Biden_s_must-win_moment_tomorrow">transcript &amp; editable summary</a>)

The Biden administration's must-win moment in reshaping the Middle East relies on reinstating a deal with Iran, addressing valid criticisms, and navigating complex power dynamics.

</summary>

"The Biden administration needs a win here, a big one."
"They have to pull this off. Otherwise, their entire foreign policy plan for the Middle East, it's gone."
"You can't be seen as a legitimate power while having the non-state actors."
"Realistically, we haven't been in the deal for almost three years. We're starting at zero."
"It's going to take time."

### AI summary (High error rate! Edit errors on video page)

The Biden administration's must-win moment in foreign policy is centered on the goal of reshaping the Middle East and making it less of a global playground.
A key factor in this plan is the reinstatement of a deal between the United States and Iran.
Indirect talks are set to take place in Vienna with a third party facilitating communication between the two sides.
The impasse lies in the US demanding Iran to stop enriching while Iran insists on lifting sanctions.
Iran's upcoming election adds pressure as they cannot appear weak or cave to the US.
Both sides need to claim a victory for a mutual return to the deal to occur.
Failure to reach an agreement before the Iranian election could result in hardliners taking control, halting progress.
The Biden administration's foreign policy plan for the Middle East hinges on Iran's cooperation.
Direct talks are open to the US but Iran is hesitant, given the past US withdrawal from the deal.
Valid criticisms of the initial Iran deal include its failure to address Iran's non-state actors, a factor that needs to be resolved for Iran to be seen as a legitimate power.
Resolving the issue of non-state actors could be a significant win for the Biden administration, even if just curtailing their activities is achieved.
Repairing the damage caused by the US pulling out of the deal will take time, and progress may be gradual.

Actions:

for foreign policy analysts,
Support diplomatic efforts towards reinstating the deal between the US and Iran (implied)
Stay informed about the situation in Vienna and its implications for future Middle Eastern policy decisions (implied)
</details>
<details>
<summary>
2021-04-04: Let's talk about companies getting involved in politics.... (<a href="https://youtube.com/watch?v=badvAxD1nh4">watch</a> || <a href="/videos/2021/04/04/Lets_talk_about_companies_getting_involved_in_politics">transcript &amp; editable summary</a>)

Singer's historical involvement in WWII shows the importance of companies being on the right side of history, while confusion between basic morality and politics poses a more significant issue in American society.

</summary>

"Maybe they saw what was coming and they wanted to be on the right side of history."
"Companies getting involved in politics isn't the biggest issue; confusing morality with politics is."
"Interfering with fundamental rights is a moral issue."
"Large segments of the American population are confusing basic morality with politics."
"The promises made in the Declaration of Independence and the Constitution are not up for negotiation."

### AI summary (High error rate! Edit errors on video page)

Singer, known for sewing machines, made 1911s for the US government in 1939, contributing to the war effort.
The precision equipment they made, like the Sperry Sight for airplanes, helped defeat authoritarianism.
Companies getting involved in politics isn't a new concept; Singer's involvement predates WWII.
Companies influencing politics through campaign contributions challenges the idea of them staying out of politics.
Confusing basic morality with politics is a larger issue in American society.
Interfering with fundamental rights is a moral, not political issue.
Organizations with exclusionary histories might be sensitive to attempts to deny basic rights.
Many Americans fail to recognize the non-negotiable nature of American ideals when it comes to political interference.

Actions:

for american citizens,
Advocate for upholding fundamental rights and values in society (implied).
Educate others on the distinction between moral issues and political decisions (implied).
</details>
<details>
<summary>
2021-04-03: Let's talk about the GOP and MLB in Georgia.... (<a href="https://youtube.com/watch?v=geeNQQGwIso">watch</a> || <a href="/videos/2021/04/03/Lets_talk_about_the_GOP_and_MLB_in_Georgia">transcript &amp; editable summary</a>)

The Republican Party motivates its base through bigotry, uniting them against various causes, turning supporters even against their own kids.

</summary>

"How many times has the Republican Party motivated its base by appealing to their bigotry?"
"It's pretty much what it's always about."
"When you get down to the root of it, that's what it is."

### AI summary (High error rate! Edit errors on video page)

The Republican Party motivates its base by finding something to focus on and uniting them against it.
Examples include boycotting Netflix, Gillette, Coke, Pepsi, baseball, football, Starbucks, and more.
The party has turned supporters against masks, public health, refugees, trans people, and Spanish-speaking networks.
They've influenced their base through appeals to bigotry numerous times over the years.
The strategy involves turning supporters against groups like their own children.
Overall, the Republican Party's motivation often relies on appealing to bigotry.

Actions:

for voters, activists,
Boycott companies that support bigotry (implied)
Support organizations promoting inclusion and diversity (implied)
Challenge discriminatory practices in your community (implied)
</details>
<details>
<summary>
2021-04-03: Let's talk about AOC and the GOP paving the way left.... (<a href="https://youtube.com/watch?v=_bWS2GJdNa4">watch</a> || <a href="/videos/2021/04/03/Lets_talk_about_AOC_and_the_GOP_paving_the_way_left">transcript &amp; editable summary</a>)

AOC's campaign contributions spark GOP labeling fear, inadvertently paving the way for socialism's acceptance among younger generations.

</summary>

"The Republican Party is doing more to rehabilitate the word socialism than any socialist ever could."
"It is the Republican Party that is actually paving the way for truly socialist policies in the United States."

### AI summary (High error rate! Edit errors on video page)

AOC sent campaign contributions to Democrats facing challenges in 2022, but some returned the money due to fear of being labeled socialists by Republicans.
Republicans label anything remotely left-leaning as socialist, tapping into Cold War propaganda emotions.
Social democratic policies advocated by AOC are popular and effective for working-class Americans.
GOP's short-term strategy demonizing socialism may backfire in the long term by associating basic policies with socialism.
Republicans are unintentionally paving the way for acceptance of socialism among younger generations through their rhetoric.
GOP's focus on short-term gains could lead to the success of truly socialist policies in the future.

Actions:

for politically aware individuals,
Educate others on the difference between social democratic policies and socialism (suggested)
Support and advocate for policies that benefit working-class Americans (implied)
</details>
<details>
<summary>
2021-04-02: Let's talk about Democrats throwing SALT in Biden's plans.... (<a href="https://youtube.com/watch?v=FBoMxfGRT7Q">watch</a> || <a href="/videos/2021/04/02/Lets_talk_about_Democrats_throwing_SALT_in_Biden_s_plans">transcript &amp; editable summary</a>)

Congressional Democrats challenge Biden's infrastructure program over state and local tax deduction, risking internal party conflict and project success.

</summary>

"Your voters want it gone because you have to remember in a lot of these places, $100,000 a year is like making 30 where I live because of the cost of living."
"So while this isn't news right now because all of this is really preliminary, it's probably something we should watch."
"It's already starting to raise that internal struggle that normally if you're in the White House, you don't want your own party to have a major issue with a key piece of legislation you're trying to get through."

### AI summary (High error rate! Edit errors on video page)

Congressional Democrats are challenging President Biden's infrastructure program by focusing on the deduction for state and local taxes.
The deduction allows people to offset the cost of paying state and local taxes.
Trump had capped this deduction at $10,000, causing concern for representatives in blue states with high taxes.
Many constituents in these areas, with the high cost of living, demand the removal of this cap.
Biden is in a difficult position because removing the cap could be seen as giving tax breaks to the wealthy, including literal millionaires.
Removing the cap could also result in a reduction of revenue by approximately $80 billion, a factor that Biden needs to weigh.
Biden needs every Democratic vote as it's unlikely many Republicans will support investing in infrastructure.
Biden is urging Democrats to find an alternative way to cover the $80 billion if they want the cap removed.
The internal struggle within the Democratic Party over this issue could jeopardize Biden's infrastructure project.
Despite being preliminary, this issue needs to be monitored as it could impact the infrastructure plan significantly.

Actions:

for politically engaged individuals,
Monitor developments within the Democratic Party regarding the state and local tax deduction issue (suggested)
Stay informed on how this internal struggle may impact Biden's infrastructure project (suggested)
</details>
<details>
<summary>
2021-04-02: Let's talk about Biden canceling student loan debts.... (<a href="https://youtube.com/watch?v=0Klu9ZYQuIs">watch</a> || <a href="/videos/2021/04/02/Lets_talk_about_Biden_canceling_student_loan_debts">transcript &amp; editable summary</a>)

Beau clarifies misinformation on Biden's student loan debt cancellation, examines legal hurdles, and questions the economic impact.

</summary>

"This isn't going to fix the system of higher education. This is putting a Band-Aid on something that needs stitches."
"I think the way Biden is looking at it is, right now, you have $700 in student loan debt payments each month. If you no longer have those to make, you have $700 in disposable income."
"Even if this is done, there's still more work to do when it comes to the system of higher education in the US."

### AI summary (High error rate! Edit errors on video page)

Debunks the meme that Biden has already canceled student loan debt, clarifying that he has requested a legal finding from White House lawyers.
Initially, Biden was willing to cancel up to $10,000 in student loan debt, but progressives successfully pushed for $50,000.
Beau questions the trustworthiness of White House lawyers post the last four years and sought opinions from constitutional law experts on Biden's ability to cancel student loan debt through executive action.
The constitutional law experts provided contrasting opinions: one stated Biden cannot do it without Congress, another said he could via executive orders, and the third suggested certain agencies could do it at their discretion.
Beau views canceling student loan debt as more akin to getting rid of old boats for the Coast Guard rather than new spending requiring congressional approval.
He acknowledges that canceling student loan debt won't fix the larger issues within the higher education system but could potentially boost the economy by freeing up disposable income for individuals.
Speculates that Biden's focus on canceling student loan debt may be tied to economic factors to improve his standing for the 2022 and 2024 elections.
Beau concludes by suggesting that any alternative route to canceling student loan debt other than Congress is likely to face legal challenges and the outcome will depend on court proceedings.

Actions:

for voters, policy advocates,
Reach out to constitutional law experts for more clarity on legal implications of student loan debt cancellation (implied).
Monitor updates from the White House on the decision regarding student loan debt cancellation (implied).
Stay informed about potential legal challenges and court proceedings related to canceling student loan debt (implied).
</details>
<details>
<summary>
2021-04-01: Let's talk about Ukraine, near-peers, and intent.... (<a href="https://youtube.com/watch?v=ALJ5YMxghqs">watch</a> || <a href="/videos/2021/04/01/Lets_talk_about_Ukraine_near-peers_and_intent">transcript &amp; editable summary</a>)

Beau warns of escalating tensions between near peers like Russia and China, urging a focus on understanding, curbing sensationalized media consumption, and preventing unintended conflicts.

</summary>

"It's not ideological anymore. It's not capitalism versus communism. It's just about raw power."
"If this is going to be the focus of Biden's foreign policy, we really need to curtail that, because that can lead to really bad things happening."
"You don't want to live your life in fear."

### AI summary (High error rate! Edit errors on video page)

The Biden administration is shifting its focus in American foreign policy towards countering near peers like Russia and China.
This shift will impact aid efforts, diplomatic efforts, and military posture in the United States.
There is a buildup of Russian troops on the Ukrainian border, signaling a new normal of geopolitical tension.
The situation is akin to the Cold War, characterized by staring contests and brinksmanship without actual shooting.
Both China and Russia have strong intelligence services focused on understanding the intentions of the US and NATO.
The balance of power between these nations prevents real trouble, focusing on raw power rather than ideology.
While there may be less conflict, the risks are higher and escalation could occur rapidly with near peers.
Beau advises understanding the situation, avoiding sensationalized media, and curbing fear mongering to prevent misunderstandings that could lead to conflict.
Excessive consumption of sensationalized media could inadvertently signal intentions to other nations.
Beau suggests limiting media consumption to prevent accidental escalation and ensure a focus on understanding and de-escalation.

Actions:

for global citizens,
Limit consumption of sensationalized media to prevent misunderstandings and unintentional escalations (suggested)
Stay informed through reliable sources and focus on understanding the geopolitical landscape (implied)
</details>
<details>
<summary>
2021-04-01: Let's talk about Greta and why political statues exist.... (<a href="https://youtube.com/watch?v=aHhLXoMJ4vk">watch</a> || <a href="/videos/2021/04/01/Lets_talk_about_Greta_and_why_political_statues_exist">transcript &amp; editable summary</a>)

Beau explains the true purpose of political statues: creating heroes for inspiration based on societal norms, suggesting reevaluation of statues that no longer embody heroism.

</summary>

"Political statues are about creating heroes to inspire people."
"If history has deemed a person not a hero but in fact a villain, maybe it's time for those statues to go away."
"Most people are not inspired by old statues, most people are not inspired by old slave owners."

### AI summary (High error rate! Edit errors on video page)

Twitter joke about a Greta statue led to a deeper reflection on preserving historical figures.
Many people support keeping old statues but may not fully understand their purpose.
Political statues are not solely historical artifacts but meant to inspire and embody ideals.
Statues of famous southerners were mainly erected decades after the Civil War, not as historical markers.
The purpose of political statues is to create heroes for people to emulate and be inspired by.
Most southern statues were erected during times of romanticizing the war or civil rights movements.
Political statues represent societal norms at the time of creation, not the person's era.
When societal norms change, statues that no longer inspire or represent heroism should be reconsidered.
Statues of those deemed villains by history should be removed if they fail to inspire heroism.
Suggestions for statues to inspire include Greta Thunberg and Kristen Davis over Jefferson Davis.

Actions:

for history enthusiasts,
Re-evaluate statues in your community that may no longer inspire heroism (implied)
</details>
