---
title: Let's talk about Ukraine, near-peers, and intent....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ALJ5YMxghqs) |
| Published | 2021/04/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration is shifting its focus in American foreign policy towards countering near peers like Russia and China.
- This shift will impact aid efforts, diplomatic efforts, and military posture in the United States.
- There is a buildup of Russian troops on the Ukrainian border, signaling a new normal of geopolitical tension.
- The situation is akin to the Cold War, characterized by staring contests and brinksmanship without actual shooting.
- Both China and Russia have strong intelligence services focused on understanding the intentions of the US and NATO.
- The balance of power between these nations prevents real trouble, focusing on raw power rather than ideology.
- While there may be less conflict, the risks are higher and escalation could occur rapidly with near peers.
- Beau advises understanding the situation, avoiding sensationalized media, and curbing fear mongering to prevent misunderstandings that could lead to conflict.
- Excessive consumption of sensationalized media could inadvertently signal intentions to other nations.
- Beau suggests limiting media consumption to prevent accidental escalation and ensure a focus on understanding and de-escalation.

### Quotes

- "It's not ideological anymore. It's not capitalism versus communism. It's just about raw power."
- "If this is going to be the focus of Biden's foreign policy, we really need to curtail that, because that can lead to really bad things happening."
- "You don't want to live your life in fear."

### Oneliner

Beau warns of escalating tensions between near peers like Russia and China, urging a focus on understanding, curbing sensationalized media consumption, and preventing unintended conflicts.

### Audience

Global citizens

### On-the-ground actions from transcript

- Limit consumption of sensationalized media to prevent misunderstandings and unintentional escalations (suggested)
- Stay informed through reliable sources and focus on understanding the geopolitical landscape (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the current geopolitical tensions between near peers like Russia, China, and the US, offering insights on how to navigate potential risks through informed actions and responsible media consumption.

### Tags

#Geopolitics #InternationalRelations #USForeignPolicy #Russia #China


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to answer some questions
that have come in, because a whole lot of them
are coming in on the exact same topic.
We have talked for months about how the Biden administration
appears to be shifting its focus when
it comes to American foreign policy
to countering near peers.
Near peers are countries that are near our power level.
They are almost our peer.
Specifically, when we say this, when this term is used,
we're talking about Russia and China.
Those are the two nations that are our near peers.
We're not talking about lesser powers anymore.
And we've talked about how this would impact our aid efforts
and diplomatic efforts, stuff like that.
We haven't talked about how it would impact our military
posture in the United States.
There's a buildup on the Ukrainian border
of Russian troops.
And there are tons of questions about it.
This is kind of going to be the new normal.
If you want to know what the next few years are going
to look like as this plays out, look to the Cold War.
A lot of staring at each other, a lot of brinksmanship,
not a lot of actual shooting between these powers,
because none of these countries actually
wants a war with the other.
Everybody's pretty comfortable in their position.
They want to gain a little more power or whatever.
But they certainly don't want to risk
a war with a country that is nearly as powerful as they are.
So the likelihood of these little staring contests
actually turning into wars is pretty small,
for the most part.
Now, in the beginning, they're testing each other.
And there is some risk.
The good news is that both China and Russia
have very good intelligence services.
When you're talking about intelligence work on this level,
the main goal is to figure out the opposition country's
intent, what they intend to do, what they are willing to do,
what they want to do.
It's not so much getting the schematics to the new jet
fighter.
It's knowing what the other country
plans to do with that jet.
That's the important part.
So you're going to see more human intelligence and less
signal, or more spies, less satellites.
OK?
Since Russia and China are good, they're
going to have a pretty good read on the US and NATO's intent,
meaning this situation in Ukraine.
Russian intelligence knows whether or not
NATO is just going to roll over and let something happen.
If Russian intelligence suggests that NATO would let it happen,
well, Russia might go ahead and move in.
If Russian intelligence suggests that, well, NATO
wouldn't let it happen and they would fight back
on behalf of Ukraine, well, then they
wouldn't, because none of these countries
want to risk a war with each other.
Now, the good news is that that's the current situation.
You have a balance of power, and nobody
wants to disrupt it, because none of these countries
are in real trouble.
It's not ideological anymore.
It's not capitalism versus communism.
It's just about raw power, which, as horrible as that is,
it kind of makes things safer.
The downside to it is if the US and China
do it is if any of these situations ever do get loud,
they're going to get loud very quickly,
and they're going to be big.
So while there will be less conflict,
the risks are much greater.
The stakes are higher if one does start,
which the main thing you can do is, one, when this happens,
understand it's kind of normal.
Understand there's nothing you can do about it.
If something goes wrong, it's going
to be one of these countries misreading the other's intent,
and that's how it starts.
Hopefully, that won't happen.
The other thing that you can do is
curtail your media consumption.
As we switch from engaging lesser powers
to engaging near peers, it becomes really important
for the American people to not consume sensationalized media,
to not allow fear mongering to occur for two reasons.
One, you don't want to live your life in fear.
Two, there are risks associated with that kind of content
being consumed widely.
Because if a whole bunch of people
are watching a news broadcast that says,
we need to do this to Russia, Russian intelligence
may believe that's our intent.
These tabloid news, in quotation, networks,
they could quite literally start a war accidentally.
So you might, if you do watch those shows,
you might want to start limiting them,
because they need to become unprofitable.
So they stop making them.
If this is going to be the focus of Biden's foreign policy,
if this is how we're headed, this
is the direction we're headed, we really
need to curtail that, because that can lead
to really bad things happening.
If you're not familiar with the Cold War
and how everything worked, the Cuban Missile Crisis
is a good example.
But that is a really extreme example.
Stuff like that happened all the time.
Most times, the quite literal nuclear option
wasn't really on the table.
So that's where we're at.
As far as what's going on in Ukraine right now,
do you need to worry?
Probably not.
This is kind of the opening gambit of this.
It's probably not going to be a big deal.
Now, the other side of that is that they
are testing each other.
So in the beginning, there actually
kind of is a risk of misreading intent and something spiraling
out of control.
If it happens, you're probably not
going to have any advance warning of it.
It's just going to be, hey, we're at war now.
It's not going, there won't be a lead up
and a lot of manufacturing of consent.
It's just going to occur.
So that's where we're at.
I don't foresee the situation currently in Ukraine
I don't see that getting too bad.
It's probably going to be nothing.
There's a risk, but more than likely, the intention right now
is to just get a read on each other
and see how each country responds to these situations.
And we'll probably see something very similar play out
with China in the near future, because they're
going to want to test the United States as well,
to test the resolve.
And the United States will probably
make some kind of similar move against both of these nations
in the near future.
Not something that leads to actual shooting,
but leads to mobilization.
So we can see how they mobilize and get a read on them,
because all of this right now is about determining
intent from the other side.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}