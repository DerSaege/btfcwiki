---
title: Let's talk about Greta and why political statues exist....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aHhLXoMJ4vk) |
| Published | 2021/04/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Twitter joke about a Greta statue led to a deeper reflection on preserving historical figures.
- Many people support keeping old statues but may not fully understand their purpose.
- Political statues are not solely historical artifacts but meant to inspire and embody ideals.
- Statues of famous southerners were mainly erected decades after the Civil War, not as historical markers.
- The purpose of political statues is to create heroes for people to emulate and be inspired by.
- Most southern statues were erected during times of romanticizing the war or civil rights movements.
- Political statues represent societal norms at the time of creation, not the person's era.
- When societal norms change, statues that no longer inspire or represent heroism should be reconsidered.
- Statues of those deemed villains by history should be removed if they fail to inspire heroism.
- Suggestions for statues to inspire include Greta Thunberg and Kristen Davis over Jefferson Davis.

### Quotes

- "Political statues are about creating heroes to inspire people."
- "If history has deemed a person not a hero but in fact a villain, maybe it's time for those statues to go away."
- "Most people are not inspired by old statues, most people are not inspired by old slave owners."

### Oneliner

Beau explains the true purpose of political statues: creating heroes for inspiration based on societal norms, suggesting reevaluation of statues that no longer embody heroism.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Re-evaluate statues in your community that may no longer inspire heroism (implied)

### Whats missing in summary

The full transcript provides a thought-provoking analysis of the true purpose of political statues and the importance of inspiring heroism through public art.

### Tags

#HistoricalFigures #PoliticalStatues #Inspiration #SocietalNorms #Heroism


## Transcript
well howdy there internet people it's Beau again so today we are going to talk about historical
figures statues and most importantly what the purpose of political statues really is
we're gonna do this because I made a joke on twitter about the Greta statue
you know there are people mad about it saying they want to take it down stop destroying our history
in the replies to that it made me realize that I did not fully understand
the position of many people who want to preserve these old statues
they have all of the information they need to come to this conclusion and to understand the
purpose of political statues but they haven't put it together
because a whole lot of people were like she's not a historical figure what has she accomplished
she's not part of history she's not going to be mentioned in history books
right that that's debatable about whether or not she's going to be a historical figure
uh I think she probably will be but I mean it's up for debate the point is the people who are
very in favor of preserving these statues of famous southerners from the 1860s
fully understand that typically political statues are not made
at the time the person lived they're not actually historical artifacts not really
making a statue of somebody who's still alive that's normally a form of art it's not a political
thing those statues of the southerners that people want to hang on to the overwhelming majority of
them were not made until decades after the war 1890s to 1950s is when most of them were made
that kind of shows that the history existed we're talking about almost 100 years later
it's not there for historical purposes if if the history wasn't preserved
they wouldn't know to make the statue a hundred years later
political statues aren't about history they're about giving people a hero giving somebody
a person to look at and say you know what I can be like them I can embody them their ideals
they're worth emulating all of a sudden it makes sense why a whole bunch of people don't
like those southern statues right and that was the purpose most of them came around during the
period when the war was being romanticized or during the civil rights movement where the
message was very clear y'all better stay in your place
political statues reflect societal norms at the time they were made not when the person left
it stands to reason then when societal norms shift like hypothetically speaking we no longer view
owning other people as a good thing maybe those statues should go away
when it's no longer about trying to keep people down
political statues are to create heroes to inspire people
most people are not inspired by old statues
most people are not inspired by old slave owners
yeah I would much rather have a statue of Greta
than Jefferson Davis I'd rather have a statue of Kristen Davis than Jefferson Davis
somebody worth modeling yourself after trying to embody some of those attributes
political statues are about creating heroes if history has deemed a person not a hero
but in fact a villain maybe it's time for those statues to go away
anyway it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}