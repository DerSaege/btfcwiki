---
title: Let's talk about Democrats throwing SALT in Biden's plans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FBoMxfGRT7Q) |
| Published | 2021/04/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Congressional Democrats are challenging President Biden's infrastructure program by focusing on the deduction for state and local taxes.
- The deduction allows people to offset the cost of paying state and local taxes.
- Trump had capped this deduction at $10,000, causing concern for representatives in blue states with high taxes.
- Many constituents in these areas, with the high cost of living, demand the removal of this cap.
- Biden is in a difficult position because removing the cap could be seen as giving tax breaks to the wealthy, including literal millionaires.
- Removing the cap could also result in a reduction of revenue by approximately $80 billion, a factor that Biden needs to weigh.
- Biden needs every Democratic vote as it's unlikely many Republicans will support investing in infrastructure.
- Biden is urging Democrats to find an alternative way to cover the $80 billion if they want the cap removed.
- The internal struggle within the Democratic Party over this issue could jeopardize Biden's infrastructure project.
- Despite being preliminary, this issue needs to be monitored as it could impact the infrastructure plan significantly.

### Quotes

- "Your voters want it gone because you have to remember in a lot of these places, $100,000 a year is like making 30 where I live because of the cost of living."
- "So while this isn't news right now because all of this is really preliminary, it's probably something we should watch."
- "It's already starting to raise that internal struggle that normally if you're in the White House, you don't want your own party to have a major issue with a key piece of legislation you're trying to get through."

### Oneliner

Congressional Democrats challenge Biden's infrastructure program over state and local tax deduction, risking internal party conflict and project success.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Monitor developments within the Democratic Party regarding the state and local tax deduction issue (suggested)
- Stay informed on how this internal struggle may impact Biden's infrastructure project (suggested)

### Whats missing in summary

Further insights on potential solutions and compromises to address the deduction issue.

### Tags

#Biden #Infrastructure #DemocraticParty #TaxDeduction #InternalStruggle


## Transcript
Well, howdy there, internet people. It's Beau again. So today we're going to talk about President Biden's
first real internal struggle within the Democratic Party. Congressional Democrats are throwing salt
into Biden's infrastructure program. By salt, I mean state and local taxes. It's a deduction you
can take on your federal taxes to offset the cost of paying state and local taxes. It's a
deduction that has been around a really long time. Trump capped this deduction at $10,000.
So if you are a representative for a major metropolitan area in a blue state with high
taxes, you want this cap gone. Your constituents are demanding it's gone. Your voters want it gone
because you have to remember in a lot of these places, $100,000 a year is like making 30 where
I live because of the cost of living. It's very different. So their voters need it gone.
From Biden's point of view, it would be framed as giving tax breaks to the rich because not just
would it benefit those, that group of people, it would also benefit literal millionaires.
That's not a really good position for Biden to be in, not when he's talking about raising taxes
on those who are wealthy. The other side for Biden to consider is that if the cap is gone,
it reduces revenue, I think for about $80 billion. So Biden doesn't actually want the
cap gone. However, Biden needs every single Democratic vote because it's not likely that
many, if any, Republicans are going to vote to invest in infrastructure. So he needs them all
and that puts him in a pretty bad position. What he's basically said is find a way to pay for it,
come up with the $80 billion somewhere else, and we'll look at it. Or they can always sneak it in
during negotiations in Congress as the bill goes through. I mean, if that happens and it passes,
it shows up on Biden's desk and that's in it, it's not like he's going to veto his infrastructure
package. So those are the two options for him. However, it's already starting to raise that
internal struggle that normally if you're in the White House, you don't want your own party
to have a major issue with a key piece of legislation you're trying to get through.
And that's started. Now, it's not a huge number of Democrats, but it's big enough to matter.
So while this isn't news right now because all of this is really preliminary, it's probably
something we should watch because this is one of those things that seems completely unconnected
that could actually seriously jeopardize that massive infrastructure project.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}