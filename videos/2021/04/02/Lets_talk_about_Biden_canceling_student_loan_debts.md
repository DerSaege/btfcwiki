---
title: Let's talk about Biden canceling student loan debts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0Klu9ZYQuIs) |
| Published | 2021/04/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Debunks the meme that Biden has already canceled student loan debt, clarifying that he has requested a legal finding from White House lawyers.
- Initially, Biden was willing to cancel up to $10,000 in student loan debt, but progressives successfully pushed for $50,000.
- Beau questions the trustworthiness of White House lawyers post the last four years and sought opinions from constitutional law experts on Biden's ability to cancel student loan debt through executive action.
- The constitutional law experts provided contrasting opinions: one stated Biden cannot do it without Congress, another said he could via executive orders, and the third suggested certain agencies could do it at their discretion.
- Beau views canceling student loan debt as more akin to getting rid of old boats for the Coast Guard rather than new spending requiring congressional approval.
- He acknowledges that canceling student loan debt won't fix the larger issues within the higher education system but could potentially boost the economy by freeing up disposable income for individuals.
- Speculates that Biden's focus on canceling student loan debt may be tied to economic factors to improve his standing for the 2022 and 2024 elections.
- Beau concludes by suggesting that any alternative route to canceling student loan debt other than Congress is likely to face legal challenges and the outcome will depend on court proceedings.

### Quotes

- "This isn't going to fix the system of higher education. This is putting a Band-Aid on something that needs stitches."
- "I think the way Biden is looking at it is, right now, you have $700 in student loan debt payments each month. If you no longer have those to make, you have $700 in disposable income."
- "Even if this is done, there's still more work to do when it comes to the system of higher education in the US."

### Oneliner

Beau clarifies misinformation on Biden's student loan debt cancellation, examines legal hurdles, and questions the economic impact.

### Audience

Voters, policy advocates

### On-the-ground actions from transcript

- Reach out to constitutional law experts for more clarity on legal implications of student loan debt cancellation (implied).
- Monitor updates from the White House on the decision regarding student loan debt cancellation (implied).
- Stay informed about potential legal challenges and court proceedings related to canceling student loan debt (implied).

### Whats missing in summary

Insights on the potential socio-economic impacts of canceling student loan debt. 

### Tags

#StudentLoanDebt #Biden #EconomicImpact #ConstitutionalLaw #HigherEducation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about canceling student loan
debt.
There's a meme going around saying that Biden's already
done that.
That's not true.
Just letting you know.
However, he has asked for a finding.
He's basically asked White House lawyers, hey, can I do this?
Now, on the campaign trail, he said
he was willing to do up to $10,000.
Now, the number is $50,000 because progressives
managed to push him.
That's what happened.
They kept pushing for a higher number, and they got it.
So now he's trying to figure out if he can do it.
We have to wait to see what the White House lawyers say.
After the last four years, I don't exactly
trust White House lawyers.
So I reached out to some friends of mine.
There is a difference between understanding the Constitution
and understanding constitutional law.
Those two things don't always go together.
I asked three people who really understand constitutional law.
I got three different answers.
So these were the answers I got.
No, he can't do it.
Has to go through Congress because Congress
controls the purse strings.
The second, yes, he can do it.
These agencies are in the executive,
and he can direct them.
The third, not only could Biden do it via executive order,
some of these agencies could do it via their own discretion.
That seems unlikely to me.
The first two make sense constitutionally,
even though they're in direct opposition to each other.
To me, this isn't new spending.
To me, this is more like the Coast Guard getting rid
of old boats than new spending that would need Congress.
But we'll have to see what the legal finding says.
Now, the next question is, is this
going to fix the system of higher education
in the United States?
No, not at all.
This is putting a Band-Aid on something that needs stitches.
Our system of higher education is, it needs help.
It needs help.
This isn't going to do that.
But I don't think that's Biden's goal.
I don't think that's what he's planning here
or why he was willing to go up on the number.
I think the way Biden is looking at it is, right now,
you have $700 in student loan debt payments each month.
If you no longer have those to make,
you have $700 in disposable income, which
will boost the economy, will further stimulate the economy.
And if Biden can keep the economy up, do what he can,
it looks good in 2022 and 2024.
Biden has been around long enough
to know that, in many cases, when it comes to the election,
it's really about the economy.
That may be the case.
That may be the case in 2022 or 2024.
And I think that's what he's focusing on.
Even if this is done, there's still more work
to do when it comes to the system of higher education
in the US.
So there's a complete non-answer to the question,
because I couldn't get one.
We're going to have to wait and see what the White House says.
I would expect that, if this happens any way other
than going through Congress, it will be challenged in court.
And then we'll have to see what happens in the court proceedings.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}