---
title: Let's talk about Biden's plan for leaving, options, and speculation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NFxF9R-mnpQ) |
| Published | 2021/04/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's plan for withdrawing from Afghanistan lacks clarity and sense.
- Moving a couple thousand troops quickly shouldn't be difficult for the U.S.
- The publicly stated reasons for staying beyond the deadline seem illogical.
- Establishing another arbitrary deadline contradicts past knowledge.
- Speculation arises due to missing information about the withdrawal plan.
- Six potential reasons for the withdrawal plan's current course are discussed.
- Options range from optics to potential power vacuums and privatization.
- Privatizing the withdrawal using contractors is deemed a bad idea.
- An extreme and cynical reason involves capitalizing on possible opposition responses.
- Beau lacks a firm opinion due to the lack of complete information.
- The current approach to withdrawal could be a significant error.
- Beau suggests the need to get out of Afghanistan as soon as possible.

### Quotes

- "We just need to get out now."
- "With a lot of these options, it's still not a good idea."
- "I truly feel like there is something that isn't publicly known."
- "This is just a huge error, and we should get out as soon as possible."
- "It's just a thought."

### Oneliner

Biden's unclear Afghanistan withdrawal plan lacks sense, raising speculation and urging for a swift exit.

### Audience

Policymakers, Activists

### On-the-ground actions from transcript

- Contact policymakers to advocate for a clear and efficient withdrawal plan (implied).
- Stay informed and engaged with updates on the situation in Afghanistan (implied).

### Whats missing in summary

Context on the potential consequences and implications of Biden's unclear Afghanistan withdrawal plan.

### Tags

#Biden #Afghanistan #ForeignPolicy #WithdrawalPlan #Speculation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about Biden's withdrawal plans,
what he's doing, timelines, and options.
And a lot of people ask what I think of his plan.
I don't know.
I don't know because I think there is a big piece of it
that is not publicly known,
mainly because it doesn't make sense.
Up until now, everything the Biden administration
has done foreign policy-wise,
even if I didn't agree with it, I understood it.
I understood what they were hoping to accomplish.
That's not the case here.
His plan for getting out of Afghanistan,
it doesn't make sense.
Even the stated reason for us staying beyond
the current deadline doesn't make sense.
We can't move a couple thousand troops quickly.
That's like the United States' whole thing.
Yeah, withdrawing 2,500 to 3,500 troops, that's not hard.
So the idea that it couldn't be done quickly,
I just don't buy that.
Even if you were talking about all of NATO,
I want to say it's 10,000.
It's not hard to move.
It's not a large force.
It is a token security force.
We're not talking about big numbers.
So the publicly stated reason for staying
beyond the deadline doesn't make sense to me.
And then we have the other part that doesn't make sense.
Biden's foreign policy team, they're sharp.
They know what they're doing.
When Trump announced the arbitrary deadline,
everybody knew it was a bad idea.
You don't provide arbitrary deadlines
because then the opposition just kicks back
and waits for you to leave.
So with them knowing that, why would they turn around
and establish another one?
It doesn't make sense.
I don't have a hard analysis on it
because I think there's a big piece that's missing.
There's something that is not publicly known.
So all we can do is speculate.
I have six options on why this might be happening this way.
The first is just pure optics.
Biden does not want to preside over a hasty withdrawal
because he doesn't want to be seen
as the president who lost the war.
It could be that simple.
The second one is political.
If he does it this way, Trump didn't end the war.
Biden did.
And if this is the case, we'll be out way before September.
U.S. forces will be gone way before September.
And Biden is currently under-promising
in hopes of over-delivering,
just to rob Trump of that talking point.
It's a possibility.
The third is that he is holding out hope
of a power-sharing agreement being reached between the sides.
I wouldn't put money on that happening.
I hope that's not the reason.
The fourth is that he is stalling
in hopes of getting a regional force to step in
and be that token security force.
That one, I mean, I could kind of get behind
because with the U.S. leaving, there will be a power vacuum.
If there's a regional force that just needs a little bit of time
to get ready to come in and then we leave, okay, fine.
But we don't know that, and we haven't seen any signs of that yet, really.
I mean, we've seen signs that they were trying to put one together,
but we haven't seen signs of success.
Okay.
Another one is that they are privatizing it
and will be using contractors.
I'm going to say that's a really bad idea,
but if that's what's going on,
it's on the national government there, not the U.S. government.
And then there's a sixth one, which is very cynical.
One of the reasons that option one,
the optics side of things, is such a risk
is that if the U.S. stays beyond the current deadline,
it's very possible that the opposition responds,
and that would provide even worse optics.
Therefore, the sixth option would be capitalizing on that,
knowing that if we stay behind and we extend beyond that current deadline,
the opposition might respond.
So when they prepare for an offensive,
that gives the United States justification to respond in their own right
and go after command and control of the opposition on their way out the door,
thereby giving the national government an edge in any coming conflict.
And that's a big gamble.
None of these, other than stalling for regional force, are good reasons,
but there may be another one.
I truly feel like there is something that isn't publicly known,
because it doesn't add up from a foreign policy team
that has made a whole lot of good moves, because this isn't.
If there isn't something big that isn't publicly known,
this is just a huge error, and we should get out as soon as possible.
So as far as my take on it, I don't really have one.
I don't feel I have enough information to make a good one.
We can speculate.
We can guess.
That's what this is.
But I don't have a this is a good idea, bad idea,
until I have all the information.
If there is no more information, this is a bad idea.
We just need to get out now.
In fact, with a lot of these options, it's still not a good idea.
But I guess we're going to wait and see, see what happens.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}