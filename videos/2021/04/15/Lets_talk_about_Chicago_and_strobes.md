---
title: Let's talk about Chicago and strobes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=E3J1a3iAPek) |
| Published | 2021/04/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Chicago incident involving the use of a strobe light is under-discussed.
- Strobe lights are disorienting and affect vision in low-light situations.
- Proper use of strobe lights differs from constant lights; they should be shined in the eyes.
- Using a strobe light alone can create misperceptions of movements due to the brain filling gaps.
- Lack of training on strobe light usage might have influenced the Chicago incident.
- Second officer shining a constant light on the suspect is a recommended practice with strobe lights.
- Misusing tools like strobe lights in law enforcement can lead to undesired outcomes.
- Questions arise about officer training in using strobe lights effectively.
- Importance of understanding the specific purposes and correct usage of law enforcement gadgets.
- Civilian oversight should inquire about officers with strobes on their weapons and their training.

### Quotes

- "Using them outside of those purposes because it's cool doesn't lend to getting the best results."
- "Understand you don't use it like a normal light."
- "I want to know how many officers have those strobes on their weapons and how many were trained to use them."

### Oneliner

The Chicago incident raises concerns about the under-discussed misuse of strobe lights in law enforcement and the importance of proper training and usage protocols.

### Audience

Law enforcement oversight

### On-the-ground actions from transcript

- Inquire about officers with strobe lights on their weapons and their training (implied)

### Whats missing in summary

Importance of addressing proper training and usage protocols to prevent potential misuse and harm in law enforcement situations. 

### Tags

#Chicago #StrobeLights #LawEnforcement #Training #Oversight


## Transcript
Well howdy there internet people it's Beau again. So we're gonna talk about what happened in Chicago.
I'm not going to go over the whole thing. Everybody is talking about this and I have a feeling it's
going to be a topic for a while but there's something that I don't believe is going to be
discussed a lot and it's the use of that strobe. That strobe, that light, that flashing light.
It is all the rage right now and it's a good tool. It is a great tool. If one of those is being
shined at you it's very hard to tell how many people you're up against. It's very disorienting
in low light situations. It is hard for your vision to adjust. It's a great tool.
However you don't use it the same way you would use a constant light.
You have to shine it in a different place. Normally under normal circumstances with a
constant beam of light you shine it center mass. With this you shine it in the eyes.
The other thing and the more important thing in this situation is that while it is incredibly
disorienting to the person it is being shined at it is also it also limits the ability of the
person using it. That is why most trainers will say if you're going to use one you need a second
officer shining constant light on the suspect. That way you don't believe that movements that
are actually occurring relatively slowly are occurring quickly because you're not, your brain
isn't filling in the gaps where the light is off. I'm very curious about whether or not this officer
had ever been trained in the use of this thing or if he just bought a new gadget and put it on his
weapon and went out there on the street. I have a feeling that that strobe has more to do with what
happened than people are going to acknowledge. If you're using one of these things, if you have one,
understand you don't use it like a normal light. If you do, what you saw in that video is what is
going to happen when it flips off and comes back on. Every time it flips off your brain fills in
the gaps. This information I'm saying about having a second person shining the light,
it's one of those things yeah it's in a lot of the manuals as far as the the
the tactical manuals on how to use this. I looked, it's also in police magazines that are online.
I do not know if this officer was trained in how to use that but it doesn't appear that they were.
Understand that all of these little gadgets that are now finding their way in the local
police departments, they were developed for specific purposes. Using them outside of those
purposes because it's cool doesn't lend to getting the best results.
13 years old and certainly from what we can see on that video appeared to have his hands up.
If I am the civilian oversight there in Chicago, I want to know how many officers
have those strobes on their weapons and how many were trained to use them.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}