---
title: Let's talk about where his parents were....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tvOTlhDp-qQ) |
| Published | 2021/04/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses questions circulating on social media about a situation in Chicago.
- Questions the assumptions behind these inquiries and decides to answer them differently.
- Raises questions about a young boy's background, behavior, and decisions leading to his victimization.
- Points out the systemic neglect and stereotypes faced by marginalized neighborhoods.
- Contrasts the treatment of different communities based on race and socioeconomic status.
- Criticizes the system for perpetuating poverty and dangerous environments.
- Questions why a 13-year-old was in a situation that led to his death.
- Condemns the society's failure to protect and support the boy.
- Expresses frustration at the lack of meaningful change or response to such tragedies.
- Calls attention to the systemic failures and societal shortcomings that contributed to the boy's death.

### Quotes

- "Where were his parents? Why was he hanging out with someone in their 20s?"
- "In this child's very short life, he wound up being the victim. Over and over and over again."
- "The entire society failed this boy."
- "It's a tragedy. But that's true of pretty much any time a 13-year-old dies."
- "This is a systemic failure on every front."

### Oneliner

Beau questions societal assumptions and systemic failures in addressing the tragic death of a 13-year-old boy in Chicago.

### Audience

Advocates for social justice

### On-the-ground actions from transcript

- Challenge stereotypes and prejudices within your community (implied)
- Advocate for equitable resources and support for marginalized neighborhoods (implied)
- Support initiatives that address poverty and systemic neglect in underserved areas (implied)

### Whats missing in summary

The emotional depth and impact of Beau's reflections on the systemic failures and societal neglect leading to tragic outcomes for marginalized youth in Chicago.

### Tags

#SocialJustice #SystemicFailure #CommunitySupport #RacialEquity #YouthAdvocacy


## Transcript
Oh, howdy there internet people, it's Beau again.
So today, we're going to talk about some questions that
are popping up on social media about Chicago.
Now, to be completely clear, I don't even
know if any of the basis of some of these questions is true.
But it doesn't matter, because it's out there
and that's how it works.
So we're going to talk about them,
and then I'm going to answer the questions in my own way.
Where were his parents?
Why was he hanging out with someone in their 20s?
Did you hear his nickname, his reputation?
See what was on his social media, those substances?
What was he doing in that part of town?
Did you see how he was dressed?
Like a thug.
What did he expect to happen?
Pretty lengthy list of questions thrown up
to kind of change the storyline.
So let's change the storyline again.
Where were her parents at?
Why was she hanging out with someone in their 20s?
Do you know what her nickname was?
What her reputation was?
See her social media?
Why had she been drinking?
What was she doing in that part of town?
Did you see the way she was dressed?
What did she expect to happen?
What did she expect to happen?
Change that pronoun.
Changes the whole dynamic of those statements, doesn't it?
Really shouldn't, though, because those questions
can be asked about any 13-year-old.
In this child's very short life, he wound up being the victim.
Over and over and over again.
The victim of a system that wrote off
his entire neighborhood.
Shouldn't even go there.
And it's funny.
Every time I hear that, that part of town
is going to be in the news.
And I'm going to be the victim of that.
And I'm going to be the victim of that.
And I'm going to go there, and it's funny, every time I hear
that, that part of town always catches my ear.
Because I know what it really means.
You know, people say when they get pressed on it,
oh, well, it's high crime.
That's not what it means.
You and I both know that.
It's not what you're saying.
It's not that it's high crime.
It's not that it's high poverty.
It's that it's both of those things
and the people have a different skin tone.
They've been in trailer parks far more dangerous
than any hood.
They're never called that part of town.
But the system, they just wrote it off.
Shouldn't even go there, forgetting the fact
that people live there.
And then victimized a second time
by those who would use that systemic neglect to encourage
less than ideal behavior.
And a system that just, instead of, I don't know,
doing something to mitigate poverty, just writes it off.
Why was he with somebody in his 20s?
Why do we have a system that almost encourages that behavior?
Is this the only way out?
It's not really a way out.
But it can certainly seem like it to a 13-year-old.
And then the system killed him.
And then those wanting to protect that system,
well, they denigrated a 13-year-old after his death
to do anything they can do to make sure
that the calls for change continue to go unheeded.
The entire society failed this boy.
And I keep hearing, well, it's a tragedy.
It's a tragedy.
Yeah, I mean, it is.
But that's true of pretty much any time a 13-year-old dies.
I don't know that that term really does this justice.
This is a systemic failure on every front.
I don't know that these questions really matter.
Not really, because you wouldn't use them in other situations.
You're only going to use these in an effort to defend a system
we all know failed.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}