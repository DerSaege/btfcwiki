---
title: Let's talk about SEALs retraining and what it means....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uADU1HHdWrY) |
| Published | 2021/04/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains that the Navy SEALs are redefining their roles and missions, not specifically training for imminent war with China and Russia.
- Mentions that the image of the SEALs portrayed in the 80s is changing as they adapt to new foreign policy objectives.
- Suggests that the recent retooling of the SEALs may be more of an excuse to address behavior issues within the ranks.
- Talks about the tarnished image of the SEALs due to recent events like substance abuse and hazing.
- Describes the current process of retraining and retooling in the SEALs, including implementing new standards, conducting psyche vows, and matching teams with suitable commanders.
- Emphasizes that while the SEALs are preparing to counter near-peer threats, the larger military shifts are part of geopolitics and not indicative of imminent conflict.
- Assures the audience that the ongoing changes in the military should not raise concerns about imminent conflict with other countries.
- Encourages viewers not to let these developments increase their anxiety levels.

### Quotes

- "They're redefining their roles, their missions."
- "Yes, they are retooling to counter near peers, but that seems like more of an excuse to do this all at one time, not the real reason behind it."
- "None of this should worry you."
- "As far as any of this meaning that conflict is going to happen soon, there's nothing. There's nothing."
- "Definitely don't let it raise your anxiety level."

### Oneliner

Beau explains the Navy SEALs' redefining roles amid geopolitical shifts, reassuring no imminent conflict worries.

### Audience

Military enthusiasts

### On-the-ground actions from transcript

- Stay informed about military changes and foreign policy shifts (implied)
- Share accurate information about military restructuring with others (implied)

### Whats missing in summary

The full transcript provides detailed insights into the evolving roles and image of the Navy SEALs, addressing concerns about imminent conflicts and reassuring viewers about the normalcy of ongoing military changes.


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about perceptions,
realities, excuses, behavior, and the seals. Because I got a question that I think should be
answered. Okay, it says, I was not alive during the Cold War. A lot of this stuff is really scary
to me. I'm one that had an anxiety attack over the nuclear tweet. My brother said the seals are
now training for war with China and Russia. Doesn't that mean it's close? Short answer,
no. That's not what it means. They are, well I mean yeah they are actually training for war.
They're redefining their roles, their missions. Yes, they're training for this, but it's not like
they're training for a specific mission that's going to happen next month. They're redefining
their overall purpose. It's probably worth noting that the image that most people have of the seals
came about in the 80s, in the 1980s, with the release of the Charlie Sheen movie and Marsinko's
books. Both of those, they focused on primarily Seal Team Six until relatively recently that was
the only one that had a counter-terror role. The other ones had different missions. They're going
back to that. Our foreign policy is changing. They're coming out of the desert and going back
into the water. That's what's going on. Aside from that, there's something happening behind the
scenes and the way they are retooling the entire command at one time like this, that seems more
like an excuse to accomplish this other thing behind the scenes. The command, the admirals
over the seals, they have not been happy lately with the behavior of the seals and they feel that
the image has been tarnished. The seals, their capability has always been, they're the elite.
Character was too. Because of recent events involving substance abuse and hazing and then
the one incident everybody knows about because Trump got involved, they feel the image has been
tarnished. So during this retraining and retooling process, the seals are going to have psyche vows.
New standards are going to be implemented. Some of them will be leaving. They will be matching
teams with commanders who match those teams' personalities better. They're trying to create
a more effective force. Yes, they are retooling to counter near peers, but that seems like
more of an excuse to do this all at one time, not the real reason behind it.
Units change roles, shift roles all the time. It's a common occurrence.
This being semi-publicized the way it is, I think that's just to give them the ability
to conduct the personnel changes they want to. I think that's what's going on. There is nothing,
nothing out there right now that indicates an imminent conflict between the United States or
Russia or China. All of these shifts and there are going to be more. You'll probably hear about,
I don't know, maybe getting new strategic bombers or the number of special forces troops growing
because as we counter near peers, we will be training local forces more. None of this should
worry you. We know that this is happening geopolitically. We know that we're shifting.
The military is going to make changes to enact that foreign policy and to be ready for that.
None of it should be a worry. You shouldn't be concerned about it beyond the overall implications
of American empire and all of that. As far as any of this meaning that conflict is going to happen
soon, there's nothing. There's nothing. So definitely don't let it raise your anxiety level.
This is all pretty normal. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}