---
title: Let's talk about an update on Biden's border....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oND9CXTkWwQ) |
| Published | 2021/04/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Biden's border situation was a significant topic, initially deemed unmanageable and a crisis.
- Border patrol had thousands of unaccompanied minors in custody, facing long wait times for processing.
- As of now, there has been a significant reduction in the number of unaccompanied minors in border patrol custody.
- The average wait time for processing has decreased, showing improvement in logistics.
- Despite improvements, the situation is not considered "mission accomplished" yet.
- There were concerns about potential backlogs on the health and human services side, but it has been avoided so far.
- The same voices that painted the border issue as unsolvable often present other societal problems as insurmountable.
- Beau encourages viewers to have the will to address and solve challenges, contrary to the narrative of problems being too big to tackle.

### Quotes

- "The problem is just too big to solve. With enough time and resources and will, anything can be solved."
- "It can be done; we just have to have the will to do it."
- "The same people that tell you that about every other problem this country faces."
- "There were cases where these kids were waiting 10 11 days, not acceptable."
- "Keep that in mind the next time the people who made it seem like this was just something that couldn't be done tell you something can't be done."

### Oneliner

Biden's border situation improves, showing logistics success, urging viewers to challenge the narrative of problems being unsolvable.

### Audience
Advocates for proactive problem-solving

### On-the-ground actions from transcript
- Stay informed on border issues and advocate for humane treatment of migrants (implied)
- Support organizations working towards migrant rights and well-being (implied)
- Challenge narratives that portray problems as insurmountable and advocate for proactive solutions (implied)

### Whats missing in summary
The full transcript provides a detailed analysis of the improvements in Biden's border situation, urging viewers to challenge defeatist narratives and embrace proactive problem-solving approaches.

### Tags
#Biden #BorderCrisis #Logistics #ProactiveApproach #ProblemSolving


## Transcript
well howdy there internet people it's Beau again so today we are going to talk about Biden's border
a pretty big topic over the last month and it's gotten less and less press i said i would keep
y'all updated on how it was going once they started to lay out the logistics
when i made that video the talking points were that it was completely unmanageable
it was a crisis there was nothing we could do except business as usual and shut down the border
and leave them on the other side of the border because beyond america's borders
i added that last one but that was the general tone shut the border down don't let them across
just let them suffer over there don't make any attempt to alleviate it
the end of march there were 5,764 unaccompanied minors in border patrol custody they were waiting
an average of 133 hours to be processed over to health and human services the legal limit is 72
there were cases where these kids were waiting 10 11 days not acceptable
what's it look like now there are 954 unaccompanied minors in border patrol custody
average wait time to be processed out is 28 hours seems like logistics worked that's a reduction in
both of about 80 percent now to get a clear picture of the scope of how big this was in march
border patrol apprehended 18,663 unaccompanied minors
and the total number of unaccompanied minors so they were all processed through it wasn't 5,700
being dropped down to 900 the total number processed was closer to 19,000
now when i made that video i said that i expected there to be another
backlog that occurred on the health and human services side so far that's been avoided
that hasn't happened we've got reports of employees being incredibly stressed
because they're overworked and stuff like that they are hiring by the way
but the backlog hasn't occurred yet it still might so be ready for that if it occurs but overall
i mean it's not mission accomplished by any means but the imagery that looked like the
conditions from the previous guy that's not happening right now
if there is another large influx that overwhelms the system it could happen again but the numbers
are going to have to be more than 19,000 a month which is that seems pretty unlikely it looks like
they have a pretty good handle on it now yeah part of this is saying hey logistics one job well done
and all of that but there's another part to it the same people the same talking heads that told you
this was a crisis it was unmanageable the only thing that we could do was go back to business
as usual they're the same people that tell you that about every other problem this country faces
whether it be climate change or poverty or education or the minimum wage mental health
homeless anything that's always the thing the problem is just too big to solve
with enough time and resources and will anything can be solved
so keep that in mind the next time the people who made it seem like this was just something
that couldn't be done tell you something can't be done it can be done we just have to have the will
to do it anyway it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}