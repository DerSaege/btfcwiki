---
title: Let's talk about qualified immunity and misconceptions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GnlziVpT4Xg) |
| Published | 2021/04/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing frustrations, misconceptions, and the concept of qualified immunity in law enforcement.
- Viewer frustration towards Beau for not discussing qualified immunity as a tool to hold criminal cops accountable.
- Qualified immunity does not directly relate to criminal prosecution but rather to civil liability.
- Explaining that ending qualified immunity won't lead to more criminal prosecutions but could make officers financially accountable.
- Emphasizing the importance of focusing on policy, training, and statutes in relation to criminal prosecutions, not just qualified immunity.
- Mentioning the broader scope of qualified immunity beyond law enforcement, like school administrators having it.
- Predicting changes in qualified immunity within the next few years due to ongoing political momentum.
- Urging attention towards policy, training, and governing statutes as the key areas needing reform for accountability.

### Quotes

- "Ending qualified immunity will not increase prosecutions."
- "Qualified immunity does not relate to criminal prosecution."
- "Policy, training, and governing statutes are what matters."
- "Qualified immunity as it exists today in the United States, in five years, it's not going to look like that."
- "That is a fight that hasn't even started yet."

### Oneliner

Beau explains frustrations around qualified immunity, clarifying its role in civil liability, not criminal prosecution, urging focus on policy and training for accountability.

### Audience

Advocates for police accountability

### On-the-ground actions from transcript

- Advocate for reforms in policy, training, and governing statutes to improve police accountability (implied).
- Stay informed and engaged in local and national efforts to address qualified immunity and other issues related to police accountability (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of the limitations and implications of qualified immunity, stressing the need for broader reforms beyond its scope.

### Tags

#PoliceAccountability #QualifiedImmunity #PolicyReform #CivilLiability #Training #CommunityAction


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
a message I got. Frustrations, misconceptions, and qualified immunity.
Beau, I love your show. I watch you every day, sometimes twice a day. However, I have
to say that I am incredibly frustrated with you. And I think a lot of your audience is
frustrated with you. Because while you talk about policy and training all the time, you
never talk about the one thing that really matters. You never talk about the silver bullet
when it comes to putting criminal cops behind bars. Why don't you ever talk about qualified
immunity? Ok, so if you watch this channel a lot, you know I really really dislike it
when journalists both sides a quote. When one person says it's raining and the other
person says it's sunny, the journalist's job is not to print both. It's to look out
the window and tell you which is true. A lot of defenders of law enforcement will say that
if qualified immunity is ended, it will lead to cops going to jail for anything. That is
a lie. Flat out, that is a lie. Because qualified immunity has absolutely nothing to do with
criminal prosecution. It's not a silver bullet for putting bad cops behind bars. It's
not even a bullet. It has nothing to do with criminal prosecution. It has to do with civil
liability. Makes it, if qualified immunity was to be ended, individual officers could
be held financially responsible in court for violating constitutional rights. That's what
it means. The way qualified immunity works right now, basically it's a defense that
can be invoked by law enforcement. And if they invoke it, the family of the people suing,
they basically have to find an identical case. It's incredibly hard to overcome. Does qualified
immunity need to be changed, ended, revamped, limited, whatever? Yeah, it's way out of
control. Absolutely. I would be very careful about how it's done because qualified immunity
is something that is much broader in scope than I think a lot of people believe. I would
point out that school administrators have qualified immunity. There are a lot of different
facets to this. If you are just focusing on law enforcement, yeah, it is way out of control.
Absolutely. I would be careful with the law of unintended consequences when it comes to
this. Because in the short term, it is probably going to be pretty uncomfortable. But long
term, I think it would do a lot of good. Short term, let's say qualified immunity is ended
tomorrow and a family sues an individual officer, Officer Bad Apple, right? He gets sued. Judge
says you owe the family $10 million. Officer Apple makes $60,000 a year. They're never
seeing that money. Now, realistically, in most cases, it would be suing the officer
and the department and other deep pockets. So they would see the money. But then that
doesn't really change anything. I think that in the beginning would be uncomfortable.
As time goes on, you would probably end up with officers carrying insurance that amounts
to malpractice insurance. I don't think any of this is bad. But it is not a silver
bullet when it comes to criminal prosecution. It's not even a bullet. It's not related.
When it comes to criminal prosecution, what matters is policy and training and the statutes,
the laws on the books. The reason a lot of officers do not get charged is because they
can frame their actions as being within policy. If something obviously unjust occurs and the
officer can frame it as being within policy, then the policy needs to be tightened. The
policy needs to be changed. That's where criminal prosecutions come from. It doesn't
have anything to do with qualified immunity. Ending qualified immunity will not result
in more prosecutions. That's not going to happen. The two things are unrelated. It's
not to say that ending qualified immunity shouldn't be something that people are pursuing.
That they absolutely should. But it's not the silver bullet that apparently people believe
it is. And I think people believe it is because defenders of law enforcement, the unions,
have said this is what's going to happen. It's not true. It is flat out not true. So
this is something we should probably all take note of. When you get information, and it
boils down to slogans, you have to be very careful because you can end up believing something
is going to be far more effective than it is or believing it's going to be effective
at one thing when it is completely unrelated just based on slogans, especially the slogans
of the people opposed to it, because they will reframe it as something that it isn't.
And this isn't just something related to cops. This is across the board when it comes
to any political issue. You can't just assume the opposition's framing of something is
true. Ending qualified immunity will not increase prosecutions. When people say it will make
holding officers accountable easier, it's true. But they're talking about financially
accountable, not criminally. If you want criminal prosecutions, if you want them to be held
accountable to the same standards as everybody else, you have to look at policy and training
and the actual statutes on how they're governed, what they're permitted to do. That's what
matters. That's why I focus on it so much. I don't pay much attention to qualified immunity
because honestly, that fight, it's over. It's won. That battle is won. There are a
lot of states that are doing it on their own, that are ending, limiting, or revamping how
it's done. And I know a lot of people are saying, well, you know, they're not really
ending it here. They're just changing it. They're not ending it because it applies
to professions other than law enforcement as well. So that's why I don't focus on
it. That is something that's going to occur. Qualified immunity as it exists today in the
United States, in five years, it's not going to look like that. The political momentum
has already occurred. We're over the hill on that one. It's going to change. I would
imagine that it would change federally within the next two or three years, maybe this year.
But the policy and the training and the statutes governing it, that is a fight that hasn't
even started yet. So we have to start looking there. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}