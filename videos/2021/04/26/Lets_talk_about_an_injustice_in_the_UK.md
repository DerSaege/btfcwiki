---
title: Let's talk about an injustice in the UK....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wLVvpM776-Q) |
| Published | 2021/04/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The UK post office faced significant problems from 2000 to 2014, with employees stealing thousands of dollars, leading to 736 prosecutions.
- Employees were wrongfully convicted due to a bug in the Horizon software that made it appear money was missing.
- Despite proclaiming innocence, many were convicted as they couldn't prove the bug's existence.
- Convictions are slowly being overturned, but some affected individuals have passed away or faced severe consequences like mortgaging their homes.
- Only 39 people have had their convictions overturned so far out of 2,400 claims related to this issue.
- Beau suggests the need for a formal inquiry with teeth to address the injustice caused by the software glitch.
- He draws parallels to the US justice system, known for favoring those with better lawyers and sometimes failing to address injustices.
- Beau urges both the UK and the US to acknowledge and rectify injustices, even if it means admitting mistakes.

### Quotes

- "That's a whole lot of people who had their lives turned upside down by a glitch in software."
- "We have a lot of situations in our country that are plainly unjust, that continue to persist because people don't want to admit they're wrong about something."

### Oneliner

UK post office employees faced wrongful convictions due to a software bug, prompting the need for accountability and inquiries into systemic injustice in both the UK and the US.

### Audience

Government officials, policymakers

### On-the-ground actions from transcript

- Advocate for a formal inquiry with teeth to address the injustices caused by the software bug (suggested).
- Push for accountability and justice for those wrongfully convicted (implied).

### Whats missing in summary

The full transcript provides a detailed account of how a software bug led to wrongful convictions, urging action to rectify the injustice and prevent similar incidents in the future.

### Tags

#UK #US #JusticeSystem #Injustice #Accountability #SoftwareGlitch


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some news out of the UK, out of the United Kingdom.
Their post office has had some real problems.
From 2000 to 2014, we're talking about significant, significant problems.
People taking money, by people I mean employees, thousands of dollars.
There were 736 prosecutions over this theft of money.
A lot of people wound up getting locked up over it.
The problem is, it didn't happen.
Software, software called Horizon, it had a bug in it.
And that bug, well it made it look like people had taken money, like there was money missing.
And even though they proclaimed their innocence, they were just steamrolled.
They had no way to show that the bug existed, so a whole lot of them were convicted.
Now their convictions are slowly being overturned.
Problem is, some of them are no longer with us.
They've passed.
One of them took themselves out.
People mortgaged their homes to try to make up the difference, this missing money, to
pay it back.
One person, on the advice of their lawyer, pleaded guilty.
In 2019, 555 people settled.
So far there have been 2,400 claims related to this.
Thirty-nine people just had their convictions overturned.
I do not often comment on the internal workings of other countries.
It's purely internal, not something I normally do.
But I guess there's some discussion over whether or not there should be a formal inquiry, a
real one, a statutory one, one that has teeth.
Coming from a country that has a criminal justice system that is described as the most
elegant and beautiful way to determine who hired the better lawyer, a country that is
definitely no stranger to injustice, yeah, I'm definitely going to suggest you all need
an inquiry into this.
That's a whole lot of people who had their lives turned upside down by a glitch in software
that realistically people, the authorities, should have known something was up.
Because if the money really didn't go missing, that means that it was still there.
It was still in the accounts, it just wasn't showing up in the software, right?
To this point, nobody has been held accountable for these prosecutions.
Yeah I would suggest that there needs to be an inquiry.
And those in the United States, we might want to remember this incident.
Because it went on for more than a decade, simply because the government didn't want
to admit it was wrong.
That's really what it boils down to.
And we have a lot of situations in our country that are plainly unjust, that continue to
persist because people don't want to admit they're wrong about something.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}