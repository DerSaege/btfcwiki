---
title: Let's talk about police training and change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jIqF5kHFv70) |
| Published | 2021/04/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls incidents dating back to 1992 and stresses the long-standing nature of issues in law enforcement training and practices.
- Considers the root causes of problems in law enforcement tactics and sees them as a combination of training issues and deeper cultural problems within the institution.
- Compares the quick adoption of new policies in response to public health crises with the lack of progress in implementing new protocols in law enforcement despite knowing best practices.
- Expresses frustration at the lack of change in law enforcement tactics, with videos detailing necessary techniques dating back 10-25 years.
- Criticizes law enforcement leadership for being unwilling to change and prioritizing mimicking military high-speed teams over adopting effective practices to mitigate risk.
- Points out the military's willingness to adapt and change tactics compared to the resistance to change within law enforcement.
- Gives an example of swift action taken by military leadership in response to an incident involving a white NCO and a black civilian, contrasting it with the general unwillingness of law enforcement to acknowledge and correct mistakes.
- Emphasizes the need for a cultural shift in law enforcement to prioritize risk mitigation and the adoption of effective, proven tactics to prevent unjust deaths.

### Quotes

- "There hasn't been any new policies. There haven't been any new protocols to keep people safe."
- "They have been doing it wrong."
- "We need change in the culture of law enforcement, a lot of it."

### Oneliner

Beau stresses the urgent need for a cultural shift in law enforcement to prioritize risk mitigation and effective training practices to prevent unjust deaths.

### Audience

Law Enforcement Reform Advocates

### On-the-ground actions from transcript

- Advocate for and support the implementation of new policies and protocols in law enforcement to prioritize safety and mitigate risks (exemplified).
- Push for cultural changes within law enforcement institutions to prioritize training and willingness to change (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the longstanding issues in law enforcement training and tactics, calling for urgent changes to prevent further unjust deaths.

### Tags

#LawEnforcement #Training #CulturalShift #RiskMitigation #Justice


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about training and willingness to change and how long this
has been going on.
These incidents, the first one I can remember was in 1992.
They have been going on a long, long time.
When they happen, I tend to look at the tactics.
This is what happened, this is why, this is what best practices are, this is why it broke
down.
And that's leaving some people with the idea that this is a training issue.
And in some ways it is.
In some ways it is.
But it isn't just a training issue.
There are multiple issues within the culture of law enforcement and when they come together
it creates a perfect storm.
And every time it happens somebody gets planted.
Think to our recent public health issue here in the United States.
Once news broke that it was in the US, how long did it take for every hospital across
the country to institute new policies and protocols to protect people?
Days?
Weeks?
It wasn't long.
I may be one of the few people on YouTube that has videos that when they get popular
again I'm not happy about it because I know something horrible happened.
I have videos detailing time, distance, and cover, positional asphyxiation, putting eyes
on before you kick in a door, not making unrest worse.
I have videos about all this stuff and any time they pop back up I know something bad
happened.
The videos are a couple years old, some of them.
That's bad enough.
But the information contained in those videos is at least 10 years old and in most cases
25 years old.
And there still hasn't been any change.
There hasn't been any new policies.
There haven't been any new protocols to keep people safe.
Yes it's a training issue.
We know what best practices are.
We know what they're supposed to do, but they're not doing it.
And the leadership in law enforcement doesn't seem willing to change.
That's a big part of it, is an unwillingness to change with a desire to mimic those high
speed teams in the military.
They want their toys, their tools, their gadgets.
And I get it, mimicking them makes sense.
They are impressive men.
The thing is though, most of them have advanced degrees, speak multiple languages.
These are not men afraid of a classroom.
They're certainly not afraid of new tactics.
It's like their whole thing, figuring out a better way to do it, to mitigate risk.
You want to be like them, be like them.
You want to emulate something, emulate that.
You know, military found out that blunt force trauma across the chest was creating a lot
of unnecessary loss.
Suddenly everybody is learning how to do a needle decompression to mitigate risk.
And while the military is notoriously reluctant to change, law enforcement may be the one
institution that is even more reluctant.
You know, the other thing they do is if they are notified of a problem, they address it
most times.
There are some notable exceptions.
But you just had an incident with a white NCO and a black civilian on a sidewalk.
The video of that was posted to Twitter.
By that night, the commanding general of Jackson, of the installation near where it happened,
he's on Twitter.
By close of business the next day, the four-star over Trey Dock is in the loop publicly.
Changes will be made.
Changes will be made.
When it comes to law enforcement, there is an unwillingness to accept the fact that they've
been doing it wrong.
They have been doing it wrong.
And there is an unwillingness to accept the fact that the role in society has changed.
That a lot of biases that exist, well, they're not excuses anymore and nobody's going to
accept it.
All of those tactics that should be taught, many of them are.
They're just ignored.
Because when they're inappropriately applied and nothing happens, doesn't cause a huge
issue, nothing's done.
If one of those high-speed guys sweeps somebody else on the team with the muzzle of their
weapon, they're gone.
You don't wait for it to cause loss before you act.
This has been going on a long time and it is the same issues over and over again.
And we're not seeing any changes.
They're not trying to mitigate risk.
They want to mimic those high-speed teams, but they don't understand the part that they
want to do, the part they want to mimic.
That is the last six minutes of what those guys spent the last month doing, planning,
getting ready for, mitigating the risk.
We need change in the culture of law enforcement, a lot of it.
And it needs to happen pretty quickly.
Because every time one of these little perfect storms happen, somebody else unjustly dies.
That should be pretty strong motivation, I would think.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}