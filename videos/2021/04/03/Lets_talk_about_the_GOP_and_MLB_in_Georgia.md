---
title: Let's talk about the GOP and MLB in Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=geeNQQGwIso) |
| Published | 2021/04/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party motivates its base by finding something to focus on and uniting them against it.
- Examples include boycotting Netflix, Gillette, Coke, Pepsi, baseball, football, Starbucks, and more.
- The party has turned supporters against masks, public health, refugees, trans people, and Spanish-speaking networks.
- They've influenced their base through appeals to bigotry numerous times over the years.
- The strategy involves turning supporters against groups like their own children.
- Overall, the Republican Party's motivation often relies on appealing to bigotry.

### Quotes

- "How many times has the Republican Party motivated its base by appealing to their bigotry?"
- "It's pretty much what it's always about."
- "When you get down to the root of it, that's what it is."

### Oneliner

The Republican Party motivates its base through bigotry, uniting them against various causes, turning supporters even against their own kids.

### Audience

Voters, Activists

### On-the-ground actions from transcript

- Boycott companies that support bigotry (implied)
- Support organizations promoting inclusion and diversity (implied)
- Challenge discriminatory practices in your community (implied)

### Whats missing in summary

Full context and depth of examples, and Beau's commentary on the Republican Party's tactics.

### Tags

#RepublicanParty #Bigotry #Inclusion #Boycott #Activism


## Transcript
Well howdy there internet people. It's Beau again. So today we're going to talk about the Republican
Party and how the Republican Party motivates its base. What they typically do is find something to
focus the emotions of that base on. They turn their base, they turn their supporters against
something and therefore they can all be united in opposition to whatever this thing is.
And I went through and kind of made a little abbreviated list of the times they've done this
over the last four years where they've called for boycotts or whatever because they're doing it
again now, this time over baseball because the all-star game got yanked from Georgia.
So when else have they done this? Well they turn their supporters against masks
and public health in general. Netflix because they aired something about Obama, they cut some
deal with Obama, probably some other stuff too. Gillette because they made a commercial asking
people to be nice. Coke because they opposed racism. Pepsi because they were worried about
how their employees were going to be treated under Trump. Baseball because they opposed
voter suppression aimed at black people. Football because they opposed guns aimed at black people.
Starbucks because they supported refugees. The military because trans people are too weak. High
school sports because trans people are too strong. Target because they have bathrooms. Nike because
they made a commercial. Univision because the Spanish-speaking network supported Spanish-speaking
people. Macy's because they dropped Trump. Nordstrom's because they dropped Trump. Keurig
because everybody got confused. HBO because of a joke about Trump's birth certificate.
Doritos because of a rainbow chip. Home Depot, IBM, Apple, Pop-Tarts, Pringles, Disney, UPS,
QVC, Taco Bell, Tyson Foods, Yeti Coolers, GM, Volvo, Land Rover, PBS, and NASCAR because they
opposed hatred. And that's really what it boils down to. How many times has the Republican Party
motivated its base by appealing to their bigotry? Go back through that list. It's pretty much what
it's always about. When you get down to the root of it, that's what it is. And the messed up part
about this is along the way they turn their supporters against another group, their kids,
because until this recent incarnation of the Republican Party got their hooks into them,
they managed to raise their kids better than this. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}