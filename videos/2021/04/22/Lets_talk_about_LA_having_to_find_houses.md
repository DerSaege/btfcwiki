---
title: Let's talk about LA having to find houses....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NEAV_90aZzw) |
| Published | 2021/04/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A federal judge ordered LA to find housing for all homeless individuals on Skid Row after the mayor promised a billion dollars to address the issue.
- In the city, there are 41,000 homeless individuals, and if you include the county, the number rises to 66,000.
- With a billion dollars, it could cost roughly $15,000 per person, leaving $330 million for program administration.
- Beau suggests using the money to actually solve the problem by providing housing rather than just achieving measurable results through various programs.
- Building a large facility could be more cost-effective than individual units when dealing with such large numbers.
- Beau agrees with the judge that the reality of increasing homelessness in LA is shameful, with more homeless individuals dying on the streets each year.
- He advocates for setting higher goals and directly solving the issue rather than simply mitigating it.
- Providing housing seems like the most direct route to achieving measurable results with the available billion dollars.

### Quotes

- "Maybe it's time to set our sights a little bit higher rather than mitigating. We should actually just solve the problem."
- "All of the rhetoric, promises, plans, and budgeting cannot obscure the shameful reality of the crisis."
- "When you have numbers and you have a billion dollars with which to work, you could probably actually solve the problem rather than just achieve measurable results."

### Oneliner

A federal judge orders LA to find housing for all homeless individuals on Skid Row, prompting Beau to suggest directly solving the issue with a billion dollars rather than just achieving measurable results through various programs.

### Audience

Community members, policymakers

### On-the-ground actions from transcript

- Provide housing for homeless individuals in your community (implied)
- Advocate for cost-effective solutions like building large facilities (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the cost implications of providing housing for homeless individuals and challenges policymakers to aim higher in addressing the homelessness crisis in LA.

### Tags

#Homelessness #LosAngeles #HousingCrisis #CommunityPolicymaking #CostEffectiveSolutions


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about
setting a tougher goal than what's being imagined. We're going to talk about something
going on out in LA, in Los Angeles, the city and the county.
If you don't know, a federal judge just ordered the city and the county to find housing
for all of the people without houses who currently live on Skid Row.
This comes shortly after the mayor promised a billion dollars to help address this issue.
A billion. They said that they wanted to pursue strategies that could achieve measurable results,
they want to reduce it.
So in the city limits you have 41,000 people without houses, 66,000 if you include
the county as a whole. So we'll use the larger number. At a billion dollars,
that's roughly $15,000 per. Tiny houses cost about $10,000 each. If you're including some
decent profit for the manufacturer, you could probably get a deal if you're buying a whole
bunch of them. That would leave, $5,000 per person, leave roughly $330 million left over
to administrate whatever program was put into place to manage that. I mean, providing housing,
eliminating the issue is certainly a measurable result. It seems like the city and the county
want to funnel the money through a bunch of programs that might help alleviate the issue.
But when you have something that is measurable, when you have numbers and you have a billion
dollars with which to work, you could probably actually solve the problem rather than just
achieve measurable results. And to be super clear, this is not the most cost effective way to do it.
When you're talking about numbers that large, you would want to build a large facility.
You would want to build something that could combine the costs rather than a bunch of individual
units. I'm going to have to agree with the judge here. All of the rhetoric, promises, plans,
and budgeting cannot obscure the shameful reality of the crisis that year after year,
there are more homeless Angelenos and year after year, more homeless Angelenos die on the streets.
That's the reality. And if there is a billion dollars hanging out that could be budgeted for
this, I mean, it seems like the most direct route to achieving measurable results would be to provide
housing for people without homes. Maybe it's time to set our sights a little bit higher rather than
mitigating. We should actually just solve the problem. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}