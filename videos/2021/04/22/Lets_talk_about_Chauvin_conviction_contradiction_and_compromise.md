---
title: Let's talk about Chauvin, conviction, contradiction, and compromise....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=g6gzAj1qm_s) |
| Published | 2021/04/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the contradiction between wanting criminal justice reform and being satisfied with Derek Chauvin's imprisonment.
- Acknowledging the gray areas and compromises inherent in effecting real change in a complex system.
- Exploring the interconnected systemic issues related to elephant poaching and the challenges in finding solutions.
- Emphasizing the difficulty of maintaining ideological consistency when actively working towards change.
- Recognizing the pragmatic approach of targeting certain aspects of a problem, such as going after poachers to disrupt the chain of violence funding.
- Pointing out the unrealistic nature of expecting immediate solutions to deeply rooted problems like demand for ivory.
- Arguing that individuals advocating for prison abolition may not prioritize defending someone like Chauvin.
- Suggesting the importance of guideposts for those navigating the gray areas of effecting change within existing systems.

### Quotes

- "Because a lot of our problems, a lot of the systemic issues, they're systemic. They're interlinked."
- "It becomes very hard to stay consistent because you're trying to get to that ideal, but that's not the world you exist in."
- "So where do you go? We're back to going after the poachers because that's the easiest link in the chain to break."
- "Most people who want prison abolition, they do so because they don't like the violence of the state."
- "A little lighthouse over there saying, no, this is too far."

### Oneliner

Navigating the gray areas of activism and reform, Beau delves into contradictions in justice reform and the complex web of issues surrounding elephant poaching.

### Audience

Activists and reformers

### On-the-ground actions from transcript

- Support elephant preserves by donating or volunteering (implied)
- Advocate for sustainable jobs programs in regions affected by poaching (implied)
- Raise awareness about the systemic issues contributing to poaching and violence (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on the challenges of effecting change amidst systemic issues and contradictions in activism. Viewing the entire discourse offers a deeper understanding of navigating complex societal problems.

### Tags

#JusticeReform #Activism #SystemicIssues #PragmaticApproach #Compromises


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk about a
contradiction, an apparent contradiction. It's a question that's come up a few times
and it's probably something we should talk about. There are people in the United States
who want real change, real reform in the criminal justice system. We're talking about people who
want to move to restorative justice. We're talking about people who want to drastically reform or
maybe even abolish prisons. Yet a lot of them are happy that Chauvin is currently in prison.
Is that hypocrisy? Is it a contradiction?
I'm going to say it's not. I'm going to say it's not.
You know, when you are talking about politics, philosophy, ideology here on the internet,
everything is black and white. It's real simple. And people like me, people who do this,
we are the worst offenders. I try not to paint things in black and white,
but I'm sure I do it at times. You know, if you believe we have a picture perfect plan,
we've got you fooled. We don't. We don't. Because the second you put down your phone
or your laptop and you step out there and try to go and effect real change, create real change in
the system that exists, trying to get to that system you want, well all of a sudden stuff isn't
so black and white. It becomes very, very gray very quickly. And that's when compromises get made.
Because a lot of our problems, a lot of the systemic issues, they're systemic. They're
interlinked. So you have to triage. Decide what you're going to try to fix first.
Where you're going to expend your political capital. And that's going to create contradictions.
Those people who are out there really deep in the fight, I don't know any of them that are
ideologically consistent and pure at all times. Not a single one.
Because when you're actually out there doing it, it becomes very hard to stay consistent.
Because you're trying to get to that ideal, but that's not the world you exist in.
The world you exist in isn't black and white. It's very gray.
Talking about it in this case though, using this case as an example,
I don't know that it's going to do any good. Because I would suggest those people who want
to find hypocrisy, well they're going to no matter what. Because it's such a divisive topic right now.
So rather than try to illustrate it using this, we're going to go to something else
and show exactly how complicated some of these issues are.
If you don't know, I am a big supporter of elephant preserves.
Recently I tweeted out a summary of something that happened in South Africa. Three poachers
go into a park and they were there to either take out elephants or rhinos and get their tusks.
Park rangers see them. They take off running and they run smack into a herd of elephants
and one of them gets stepped on. A little bit of poetic justice there, right?
Tweeted out the story and somebody basically was like, you know, I can't really find this as funny
as I think maybe it might be to some people because most of the poachers are doing it
because they're in abject poverty. They don't really have a choice. They're unemployed.
And that's absolutely true. It really is. Most of the people who are actually out there going
after the animals, they're doing it to survive. It's not entirely dissimilar from being an amateur
pharmacist here in the United States selling products that you shouldn't. For a lot of people,
it may seem like the only way out or at least a way to survive. But it's not.
It may seem like the only way out or at least a way to survive.
So then the solution to this issue, to the issue of poaching, is jobs programs, right?
And yeah, for that part, but if you take another step back,
then you realize the proceeds from the sale of that ivory,
well, that funds pretty much every horrible thing in the region. A lot of mass violence is funded
by the sale of the ivory. So if your goal is to stop that violence, what's the easiest chain to
break? What's the easiest link in the chain? The poachers. So you go after them.
You go after them. Don't worry about jobs programs because somebody's still going to fill that role.
Somebody's still going to try to go get that ivory. Then if you take another step back,
you realize, well, the real issue is the demand. If we could get rid of the demand,
well, then the people causing the conflicts, they wouldn't have anybody to sell it to,
therefore there wouldn't be any funds for the mass violence, therefore wouldn't be worth doing.
That's true. Absolutely true. However, the people who are creating them to demand
are typically in countries that don't care. And they are also typically very wealthy and powerful,
and in many cases are the people who decide who gets prosecuted and who doesn't.
So that's not a realistic option. Pragmatically, you can't do that. So where do you go?
We're back to going after the poachers because that's the easiest link in the chain to break.
It becomes really gray because normally people who care about animals also care about people,
in most cases. It's not always true, but in most cases. So they probably understand the pressures
that are, I don't want to say forcing, but definitely encouraging the actual poachers
to go after the animals. But if they care about people, they also understand the proceeds fund
those conflicts, which hurt a lot of innocent people. It's real gray.
And it all depends on the compromises that you're willing to make if you're out there in the field
doing it. People who do what I do now, we don't have to worry about that, most of them, because
most of them aren't out there. And that isn't to say that the people who remain ideologically
consistent and talk about it all the time. It's not to say that they're wrong either. I think they
serve a pretty important purpose. Because if you are out there and you are making those compromises
and you're trying to get to that ideal system while existing in the one that's here,
you could probably use some guideposts. A little lighthouse over there saying,
no, this is too far. I would suggest that most people who want prison abolition,
they do so because they don't like the violence of the state. They don't think it's just.
Expecting them to expend their political capital and their effort
to argue on behalf of somebody who has become the symbol of violence of the state,
that doesn't seem realistic. That doesn't seem pragmatic. That doesn't seem like something
that would happen out there in the gray world, just here where everything's black and white.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}