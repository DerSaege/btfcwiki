---
title: Let's talk about me....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Rcv5fuOdcZs) |
| Published | 2021/04/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Non-government organization used Beau's content in ads to de-radicalize individuals searching for violent information.
- Beau received messages from viewers saying his videos helped them get off a bad path, which he considered wins.
- Beau was flattered and proud when he found out about the organization using his content for de-radicalization.
- Beau mentions he wasn't asked for permission but indicates he would have agreed to the use of his content.
- Beau refrained from making a video about this program to maintain its organic appearance.
- Some individuals started pushing out negative information about Beau, possibly to undermine the de-radicalization program.
- Beau addresses past accusations and incidents like alien smuggling that were used to discredit his involvement in the program.
- Beau believes relatability is key in reaching individuals searching for violent information, not just clean-cut individuals.
- Despite efforts to undermine the program using Beau, he states that it will continue to do good.
- Beau remains committed to creating videos and helping people even if his content is no longer used in the de-radicalization program.

### Quotes

- "I don't care about dragging my name through the mud. It's about that program."
- "On a long enough timeline our side wins guaranteed."
- "Apparently ads are a good way to do it."
- "If you have any questions about my views on anything, I have 1,300 videos recorded over the last couple of years."
- "Y'all have a good day."

### Oneliner

Non-government organization used Beau's content to de-radicalize individuals searching for violence, sparking pushback against him, but the program's impact remains paramount.

### Audience

Online content creators

### On-the-ground actions from transcript

- Support de-radicalization programs (implied)
- Continue creating content that helps individuals in need (implied)
- Refrain from spreading negative information to undermine programs aimed at stopping violence (implied)

### Whats missing in summary

Insight into the importance of maintaining programs aimed at de-radicalization despite external challenges.

### Tags

#DeRadicalization #ContentCreation #OnlineImpact #CommunitySupport #ViolencePrevention


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today is going to be a little different.
Today we're going to talk about me, something I normally
don't do on this channel.
I try to make this channel about ideas, not the person
presenting them.
The story starts months ago, from what I understand, I
think before the election.
But I don't know the story's taking place yet.
A non-government organization saw my content.
They're like, we can use this.
And they started paying to have it run as like ads when
people would search for something bad, when they would
search for how to join a bad group or a militant group, or
when they would search for how to do something horrible.
They would see my smiling face.
They'd see that thumbnail.
The idea, they click on it, they watch it,
and they get de-radicalized.
Pretty cool.
Now, over the years, I've gotten messages from some of you
saying that you were on a bad road,
and my videos helped get you off of it.
And to me, those were the wins.
That's what really mattered,
actually creating some change there.
So a couple months ago, when I find out this is happening
in a group that is dedicated to de-radicalizing people,
find out they were using my content, man,
I was flattered, very proud, a little taken aback at first
because nobody asked me if they could use my content.
But to be completely fair on this one, had they come to me
and said, hey, this is what we're going to do, I would
have said yes.
I would have said yes.
I think it's amazing.
It's a great program.
I wanted to make a video about it then when I found out it
was going on.
But, I couldn't.
Made a tweet about it, but I couldn't make a video.
Because in order for this to work,
has to at least somewhat appear organic.
You know, if you were searching for something horrible,
and my video helped to get you off that road,
and you continued watching videos,
and then you stumbled across one that says,
hey, that thing that happened to you,
that was totally intended.
you might feel tricked. It might undo the good that was done.
So I couldn't talk about it as much as I wanted to.
I couldn't make a video. I couldn't imagine
anybody not wanting a program like that to succeed.
I mean, it's amazing.
The idea of using just
ads and content that already exists
to stop violence, to save lives.
I don't see how that could be a bad thing.
But apparently there are some. There are some.
Because a whole bunch of information started getting pushed out
about me. Now, I don't know
I don't want to assign a motive. I don't know if it was
directed specifically at me or it was an attempt to
delegitimize and undermine the program. I don't know which, and I don't really want to speculate,
but information started getting pushed out about me. Some of it's silly. He plays up his accent,
and we've talked about it. Sometimes I do. I did a whole lot in the beginning.
To stuff that isn't quite so silly.
Everything from my past use of inflammatory rhetoric, to everything I have ever done or
been accused of, and some stuff that I was never accused of.
All got pushed out there, if you don't know, even though we've talked about it on the channel,
years ago, 12, 13. But a long time ago, I got busted for alien smuggling. I know, shock, right?
Guess this was the idea was to paint it that this program shouldn't use people like me with that
background. Which leads to the idea that the people who can reach those who are actively
searching for how to do something violent are clean-cut people with no
background. I don't know that that's true. That doesn't make a whole lot of sense
to me. It seems like there should be some kind of relatability. You know, if it was
just clean-cut people talking about doing good and all of that stuff with a
slightly paramilitary vibe that worked, well, we wouldn't have extremists. We'd
have a whole bunch of people in the Boy Scouts. It's not the real world. I think there does
need to be some kind of relatability. Now, if this push was aimed at me and getting me
out of this program, congratulations you won. My content is no longer being used for this.
OK, but to me, it's not about me.
I don't care about dragging my name through the mud.
It's about that program.
That program is still going.
And it doesn't matter what your goal was.
You're going to fail.
I will still be right here making videos.
I got messages from people who got off a bad road
from my videos before this program ever started.
will get them after. That program, it will continue and it will continue to do good.
And even if you were able to undermine it to the point that it stopped, 10 more would spring up
in its place because there are enough people who are tired of the division, who are tired
of the violence and they will use whatever tools they have available to
make it stop. Apparently ads are a good way to do it. Didn't change anything. On a
long enough timeline our side wins guaranteed. Now if you were one of the
ones who pushed this information out there for whatever reason, congratulations,
you won. You succeeded in removing a tool to de-radicalize people who were
searching for how to do something violent, remember that next time
something horrible happens. I hope the clicks were worth it. I'll be right here. If you
have any questions about my views on anything, I have 1,300 videos recorded over the last
couple of years and I will let my record speak for itself. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}