---
title: Let's talk about the difference in the next surge....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7BMEOIPWn_c) |
| Published | 2021/04/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Wishes happy birthday to Cleo before diving into discussing the potential fourth surge of COVID-19.
- Clarifies his stance on bodily autonomy, asserting support for personal decisions regarding tattoos, piercing, family planning, and vaccinations.
- Expresses being vaccinated against various diseases, including receiving the COVID-19 vaccine.
- Supports vaccines but opposes the idea of force, advocating for convincing others rather than mandating vaccinations.
- Notes the beginning of the fourth surge, likely due to a new variant, B.1.1.7, affecting younger age groups.
- Mentions laxity in following precautions, attributing it to fatigue and burnout after a year of vigilance.
- Emphasizes the importance of maintaining precautionary measures until vaccination rates are significantly higher.
- Alerts about the potential resistance of new variants to vaccines and stresses the need for continued vigilance.
- Acknowledges the coverage of the current vaccines against the B.1.1.7 variant.
- Urges people to uphold preventive measures like hand washing, mask-wearing, and staying home, while also advocating for vaccination.

### Quotes

- "You own your body."
- "Vaccines are good. I do not object to them. I object to force, not vaccines."
- "We have to continue to do this until we're there, until the vaccination rates are high enough."
- "Every variant is a variant that might be resistant to the inoculation, to the vaccine."
- "Wash your hands. Don't touch your face. Stay at home. If you have to go out, wear a mask."

### Oneliner

Beau clarifies support for bodily autonomy and advocates for vaccines, warning about the fourth COVID-19 surge and stressing the importance of ongoing precautions until vaccination rates are high.

### Audience

General public

### On-the-ground actions from transcript

- Wash your hands, avoid touching your face, stay home if possible, wear a mask when going out, and get vaccinated (exemplified).
  
### Whats missing in summary

The detailed analysis of COVID-19 trends and vaccination rates can be better understood by watching the full video.

### Tags

#COVID-19 #Vaccination #Precautions #Health #BodilyAutonomy #CommunitySafety


## Transcript
Well, howdy there, internet people.
It's Beau again.
So first, let me start by saying, happy birthday, Cleo.
Today, we are going to talk about the fourth surge,
hopefully the last one.
But before we do that, I want to talk about something else.
People have read into my position on something,
and I want to clarify it.
I'm a big supporter of bodily autonomy.
You own your body.
That means that you get to determine tattoos, piercing,
family planning, what goes into your body, all that stuff.
People have read into that and come up
with the wrong conclusion.
So to be super clear on something, I am vaccinated
against just about everything under the sun,
and I already got my first little Fauci Ouchie.
Vaccines are good.
I do not object to them.
I object to force, not vaccines.
I will argue with you all day to try to convince you to get one.
OK, so the fourth surge.
The docs are saying that we're at the beginning of it
right now, and it's likely to be a new variant, B.1.1.7.
The thing about this is that it is hitting younger age brackets.
In Orange County here in Florida,
a third of the hospitalizations were people under the age of 45.
We're getting lax is what it's boiling down to.
Fatigue is setting in, burnout.
It's hard to stay at an elevated posture
for more than a year, and that's what we've had to do.
And we had to keep doing it a little bit longer.
People are relaxing.
They're not washing their hands as much.
They're not wearing their masks, all of that stuff.
We have to continue to do this until we're there,
until the vaccination rates are high enough.
Right now, they're at about one in five.
It's not enough yet.
We have to stay alert.
Every variant is a variant that might be resistant
to the inoculation, to the vaccine.
Now, according to what I've seen, B.1.1.7,
the current vaccines cover it.
So bear that in mind.
The longer this goes on, because we aren't taking
the precautions we should, the worse it's going to be.
That should go without saying.
People have heard it for a year.
We have to keep up our posture.
We have to keep up that level of precaution.
So hopefully I won't have to say this to you,
but I'm going to say this.
Hopefully I won't have to say this too many more times.
Wash your hands.
Don't touch your face.
Stay at home.
If you have to go out, wear a mask.
And I'm going to add, go get vaccinated to the end of that.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}