---
title: Let's talk about Biden's speech, autocracy, and democracy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yKJaTwVJ_7M) |
| Published | 2021/04/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's recent speech hints at framing Cold War 2.0 as democracy versus autocracy, focusing on countering China and Russia.
- The domestic political side must be considered when discussing foreign policy, as it can be politically advantageous for Biden.
- China and Russia are unlikely to be bothered by being branded as autocracies, as many of their citizens actually favor strongman leadership.
- Lesser powers in the world may benefit from this framing as they won't have to match their economic system to a specific pole of power, allowing them to pursue regional interests.
- Most countries view China, Russia, and the United States as oligarchies, with a small group of people truly in charge.
- The democracy versus autocracy framing serves as a motivational tool for the U.S. government to rally its populace behind its foreign policy.
- This framing is seen as a good choice as it is less likely to raise global tensions compared to other options.
- The speech was testing the waters for this framing, and if well-received, it will likely shape future foreign policy approaches.

### Quotes

- "Democracy versus autocracy, autocratic governments, Russia and China."
- "If the overwhelming majority of the United States begins to view autocracy as it did communism, Trump's circle is done."
- "During the Cold War, they had to match their economic system to the pole of power that they wanted to be aligned with."
- "Old boss, same as the new boss."
- "It won't be a red scare. It will be an autocratic one."

### Oneliner

Biden's speech hints at framing Cold War 2.0 as democracy versus autocracy, a politically advantageous move domestically, with potential implications for lesser powers and global tensions.

### Audience

Global citizens

### On-the-ground actions from transcript

- Rally behind foreign policy framing (suggested)
- Understand the implications for global politics (suggested)

### Whats missing in summary

Analysis of the potential long-term effects of framing foreign policy as democracy versus autocracy.

### Tags

#ForeignPolicy #ColdWar #Democracy #Autocracy #GlobalPolitics


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about foreign policy again, and this is going
to be one of those conversations we are, we are talking about the way things
are, not the way they should be.
This is more to talk about what is likely to happen, not necessarily
what we want to happen.
Biden's speech, I think it gave us a sneak peek of the framing of Cold War 2.0.
We've talked about it numerous times now.
We are moving to countering near-peer nations, meaning China and Russia.
And one of the questions I've had is how is this going to be framed?
It's not capitalism versus communism anymore, but there has to be some way to motivate the
American populace to get behind this.
I think we saw the framing.
It's going to be democracy versus autocracy, autocratic governments, Russia and China.
That appears to be how the Biden administration is going to try to frame it.
So what does this mean?
When you are talking about foreign policy, you always have to consider the domestic side
of the politics as well.
Domestically, this is political gold for Biden.
It is political gold because if this framing, if this narrative takes hold, democracy versus
autocracy, Trump's circle, they're done.
If the overwhelming majority of the United States begins to view autocracy as it did communism,
Trump's circle is done because their rhetoric is very autocratic, because Trump is an autocrat.
Domestically, it definitely plays to Biden's favor.
Let's talk about the other two countries, China and Russia.
How are they going to respond to being branded this way?
They're not going to care.
They're going to roll their eyes.
This isn't a hugely insulting thing.
It isn't something that is really going to matter to them,
because realistically, in these countries,
there is a large amount of people
who want one person in control, who
want that strongman leader, one person who
has final say. They want that. So saying that they have a government that provides that
isn't really going to bother their people. It's not going to bother their citizens. So
the governments aren't going to care. Now when major powers begin playing world chess,
the other consideration are the countries that typically get used as pawns. The lesser
powers of the world. How are they going to view it? For them, it's kind of gut.
During the Cold War, they had to match their economic system to the pole of
power that they wanted to be aligned with. This isn't true with this framing
because most autocracies maintain at least a facade of democracy so they can
align themselves however they wish. It will allow them to pursue regional
interests more than just do the bidding of the poll of power they decide to
align with. That's good for them. It's also important to note that most
countries that get used as pawns, they don't view China and Russia as autocracies
in the United States as the defender of democracy or anything like that. They
tend to view all three countries as oligarchies. A country where a small
group of people is truly in charge. That's how they look at it. Old boss, same
as the new boss. This framing is good domestically. It provides the
motivational tool that the United States government needs to rally the
populace behind it, and as far as the available options, it's the least likely
to raise global tensions. Overall, this is a good choice. Again, not talking about
how it should be where countries are working together for the betterment of
man, but the way things are. Where countries square off geopolitically for
economic interests. That appears to be the framing. I think this speech was
testing the waters for it. If it is received well, that's how it's going to
be done. So be ready for that. It won't be a red scare. It will be an autocratic
one. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}