---
title: Let's talk about military spending and shift in language....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FSSIg-HJcbE) |
| Published | 2021/04/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Military commanders are now referring to China and Russia as peer nations, but Beau continues to refer to them as near peers.
- Military spending and a subtle shift in the use of the term "peer nations" are discussed.
- The United States spends approximately $780 billion a year on defense, more than several other countries combined.
- The idea behind US military expenditures was to be able to fight a war on two fronts independently.
- The US doctrine leads to spending more on defense than its two largest competitors combined.
- Despite excessive military spending, Beau believes the US can only fight China and Russia to a stalemate in a two-front war scenario.
- Referring to China and Russia as peers is seen as priming the populace for increased defense spending.
- Beau views the shift in terminology as propaganda to justify higher military expenditures.
- China is noted for its manpower, but Beau argues that without significant assets, their military capabilities are limited.
- Beau maintains that the term "peer nations" is inaccurate and will continue to use "near peer."

### Quotes

- "I view that as propaganda."
- "I don't think that it's accurate to say that they are peer nations militarily."
- "They're not the same."
- "It's getting ready for Cold War 2.0."
- "Y'all have a good day."

### Oneliner

Beau explains the subtle shift in military terminology towards China and Russia as peer nations, criticizing it as propaganda to justify increased defense spending, while maintaining they are actually near peers.

### Audience

Policy analysts, military personnel

### On-the-ground actions from transcript

- Advocate for transparent military spending to ensure resources are allocated effectively (implied).
- Stay informed about military policies and decisions to contribute to informed public discourse (implied).

### Whats missing in summary

Beau's detailed analysis of the implications of the shift in military terminology and the necessity of accurate representation in defense strategies. 

### Tags

#MilitarySpending #USDefense #ForeignPolicy #Propaganda #China #Russia


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
near peers, peers, military spending, and a subtle shift in language because I got a
question and it's a good one. Okay, so the question basically boiled down to I've noticed
a lot of military commanders are now referring to China and Russia as peer nations yet you
are still calling them near peers. Is there a reason why? There definitely is. And it
illustrates how subtle shifts in language help prime a populace for foreign policy decisions
that are coming down the road. I will refer to China and Russia as peer nations militarily
when they are. They're not. They are not. When you're talking about conventional conflict,
military expenditures, how much a country spends on its military is a pretty good metric
of how capable they are. This isn't true with unconventional conflict like what we saw in
Afghanistan or Iraq, but when you're talking about conventional tanks and planes and stuff
like that, yeah. The United States spends roughly 780 billion dollars a year on defense.
That is more than China, India, Russia, the United Kingdom, Saudi Arabia, Germany, and
France combined. People talk about US military expenditures and they always want to know
why we spend so much. The idea started with the logic that the US needed to be able to
fight a war on two fronts by itself. The US, because of this policy, will always spend
more than its two largest competitors combined. That's always going to happen. It's kind of
US doctrine. Obviously, given the fact that we spend more than pretty much everybody,
that has gotten a little carried away. We spend way more than we need to to even achieve
that wild doctrine of being able to fight the next two most powerful countries. We spend
way beyond that. Realistically, if the United States had to go to war with China and Russia
at the same time, the US would be able to fight them to at least a stalemate. That's
assuming there's no wild card thrown in there. So, why are military commanders and people
in the defense industry now referring to them as peers when they're really not? Because
we can't allow there to be a mineshaft gap. You can't ask for more money and encourage
defense spending to counter people who are almost as powerful as you. It's getting
ready for Cold War 2.0. It's getting ready for that mass of military expenditure. They're
not peer nations. Now, each country has things that it's good at. Some things they're
better at than the United States. We talked about surface-to-air missiles recently. And
Russia's systems, they probably are better than the US. There are things that they are
better at. But using that to suggest that they have the same war fighting capabilities
as the US, that's just not true. They're not the same. But with that shift in language,
you can see the intention of pushing for more military expenditures rather than less, which
is what we actually need. And you can see the elevation in tensions because now they're
peers. Now they're a threat. Now you need to worry about them. Realistically, they are
not. They're not the same. China has a lot of manpower. They have a lot of personnel
that they can call on. Without the physical resources, the major assets to go along with
that, doesn't mean much. Nobody plans on invading China. So that's why that shift in language
is occurring. I view that as propaganda. So I'm not shifting my language. I don't think
that it's accurate to say that they are peer nations militarily. So I will keep using near
peer. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}