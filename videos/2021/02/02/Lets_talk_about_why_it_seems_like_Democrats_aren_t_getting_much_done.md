---
title: Let's talk about why it seems like Democrats aren't getting much done....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wKO-UdTS-9k) |
| Published | 2021/02/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the structure of the US government divided into three branches: judicial, executive, and legislative, with the legislative branch further divided into the House of Representatives and the Senate.
- Details the current control of Democrats in the House, Senate, and White House, but the Senate is split 50-50, with a tie-breaking vote by the vice president.
- Describes the filibuster in the Senate, a process that allows a senator to delay or stop a bill from passing by continuously speaking, requiring 60 votes to stop it.
- Mentions Democrats' reluctance to abolish the filibuster due to potential Republican control in the future, limiting their options for passing legislation.
- Differentiates between Democrats and leftists, stating that most Democrats are center-right on the international spectrum and behave as centrists rather than leftists.
- Suggests that if Democrats were a true leftist party, they could use tactics like filibustering to bring attention to their bills and make Republicans pay politically.
- Points out that because Democrats are typically centrist and status quo, they lack the power and approach of truly leftist parties to push through radical changes.
- Indicates that without behaving like a truly left party, Democrats will struggle to pass bills in the Senate due to the 60-vote requirement.
- Explains the slow progress of Biden's agenda is due to Senate rules, particularly the filibuster, which requires bipartisan support for legislation.
- Comments on the obstructionist nature of the Republican Party and how their talking points may not serve the best interests of their working-class supporters.

### Quotes

- "If Democrats were a left party and reached out to the working class the way left-leaning parties do, they'd be able to do this."
- "They don't wield power like leftists, because they're not leftists."
- "I am radically anti-authoritarian. And I am left-leaning."
- "Most Democrats are center-right on the international spectrum."
- "Republican Party talking points are designed for those who are politically illiterate."

### Oneliner

Beau explains the hurdles Democrats face in passing legislation due to Senate rules and their centrist nature, hindering radical changes.

### Audience

US citizens

### On-the-ground actions from transcript

- Reach out to representatives to advocate for changes in Senate rules to facilitate the passing of legislation (implied).
- Educate and inform politically illiterate individuals about the true implications of Republican Party policies and actions (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the challenges faced by Democrats in passing legislation and sheds light on their centrist nature hindering radical changes.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about two questions
from overseas about how our government works here
in the US.
If you're an American, it's worth watching,
because if you were lucky enough to get a civics course,
it's probably been a while.
The first question is, why can't the Democrats
get anything done?
They have control.
Our government is divided into three branches.
The judicial, which is the Supreme Court
and the other courts.
The executive, which is the White House
and the agencies under it.
And the legislative.
The legislative is divided into two houses,
the House of Representatives and the Senate.
Together, they're referred to as Congress,
but that can get confusing for people overseas,
because a lot of Americans, particularly those in the South,
will say Congress when they are just
talking about the House of Representatives.
Currently, the Democrats control the House, the Senate,
and the White House.
So it seems like they should be able to move legislation
forward.
The problem is that in the Senate,
it's currently split 50-50, 50 senators
on each side from each party.
The Democrats have the tie-breaking vote,
which is the vice president.
So theoretically, they have control.
However, in the Senate, there exists
a thing called the filibuster.
I think it comes from a Dutch word meaning pirate.
But anyway, it's a process that allows
a senator who is in opposition to a bill
to talk and talk and talk and delay and eventually stop
a bill from getting passed.
To show the extremes to which this can go, back in,
I want to say, 1957, Strom Thurmond
talked for literally 24 hours straight
to delay passage of a civil rights bill.
Of course, it had to do with race.
It's the US.
So to stop a filibuster, you don't need 51 votes.
You need 60.
So they don't have that many.
Now, the filibuster is not in the Constitution.
It's a rule from the Senate that could be changed in theory.
Democrats are unlikely to do that, because at some point,
the Republican Party may regain control of the Senate.
And a few bills later, the country
is going to look like a scene from a handmaiden's tale.
So it's unlikely they're going to get rid of that.
Now, they do have other options, like budget reconciliation.
I did a video on that that only requires 51 votes.
But those mechanisms are limited in scope.
For real legislation, they need 60 votes, not 51.
That's the holdup.
That is the holdup.
The next question is just hilarious to me.
Why don't your leftists behave like leftists?
Because Democrats aren't leftists.
You have a few people in the Democratic Party
that are left-ish, like AOC, Bernie, people like that.
Most Democrats are center-right on the international spectrum.
Here in the US, they are left because they're
left of Republicans.
But on the real scale, they're center-right.
They're the centrist party.
They don't behave like leftists.
They don't wield power like leftists,
because they're not leftists.
I know the media, in many ways, keeps
saying stuff like radical left.
I am radically anti-authoritarian.
And I am left-leaning.
A lot of my friends are radical left.
They dislike Biden more than most Republicans dislike Biden.
Biden and most Democrats are not radical left.
They are not even really left on the international spectrum.
So they don't act like it.
Yeah, it would make sense with these rules in place.
If it was a real leftist party, they would say, OK,
filibuster it.
I dare you, because every one of our bills
has something in it for the people.
So when Republicans don't want to pass a relief bill,
they let them filibuster it.
Let them go in there and talk.
And while they're in the Senate talking,
Democrats from the House will be in front of cameras saying,
you know, we sent over a relief bill that has $1,400 in it
for each and every one of you.
Republicans are over there talking about it in the Senate
for the third day straight.
You know that executive order that Biden
signed about clean energy?
We know a lot of you all in the energy sector
are worried about your jobs.
So we were going to turn it into legislation,
include extended unemployment, retraining opportunities,
all of that stuff.
Republicans are talking about it.
And make them pay for it politically.
You know, if the Democrats were a left party
and reached out to the working class the way
left-leaning parties do, they'd be able to do this.
Because they are typically centrist,
because they are typically status quo
with a little bit of leftward push,
they don't wield power like that.
They don't make the attempt to do that.
Because for the most part, they're status quo.
They're not going to attempt any radical changes.
Now, there are some bills that they have
that include radical changes.
But unless they start behaving like a truly left party,
they will never get through the Senate.
Because they don't need 51 votes.
They need 60.
So and if you're in the US and you're
wondering why Biden's agenda is moving so slowly, this is why.
That's why there's been so much discussion over Senate rules.
Because with the filibuster, they have control,
but they have control of the legislative agenda.
They don't have enough to force the legislation through.
They need support from the Republican Party.
And the Republican Party is going
to take the stance of just being obstructionist.
That's what they're good at.
And it's what they are going to attempt to do.
From overseas, this may look weird.
Because it is generally people who
are perceived as working class that
support the Republican Party.
Republican Party talking points, and I'm not
saying this to be mean, are designed
for those people who are politically illiterate.
They don't know what's being said,
and they don't know the way their representatives vote.
Oftentimes, most times, people who vote Republican
are voting against their own interests.
So I wouldn't hold your breath for a whole lot of super
progressive changes under Biden.
It's going to be slow going.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}