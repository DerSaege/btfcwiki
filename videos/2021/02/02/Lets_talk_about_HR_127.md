---
title: Let's talk about HR 127...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7TdQzLNr-2I) |
| Published | 2021/02/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing HR 127 and the concerns surrounding it.
- People from both sides view the bill as bad.
- HR 127 requires licensing, training, and insurance for gun owners.
- The bill poses logistical challenges, such as requiring individuals with future issues to report themselves.
- The financial burden of insurance disarms poor individuals.
- The bill does not address closing the domestic violence loophole.
- Assuring pro-2nd Amendment individuals that the bill won't pass the Senate.
- Mentioning that the bill won't achieve its intended goal.
- Warning Democrats that supporting this bill may cost them in the 2022 House elections.
- Emphasizing that the bill is unlikely to pass the Senate and could become a campaign tool for Republicans.
- Suggesting that the bill should be withdrawn before gaining more attention.

### Quotes

- "It is as bad as it seems."
- "It disarms poor people. That's what this bill does."
- "This will never make it through the Senate under any circumstance."
- "It becomes a crime for poor people, but if you have money, it's okay."
- "The bill should probably be withdrawn before it gets too far and too much attention gets drawn to it."

### Oneliner

Beau addresses concerns about HR 127, describing it as disarming poor individuals and unlikely to pass the Senate, cautioning Democrats about potential repercussions in the 2022 House elections.

### Audience

Advocates, voters, lawmakers

### On-the-ground actions from transcript

- Engage in advocacy efforts to raise awareness about the implications of HR 127 (suggested).
- Contact your representatives to express concerns about the bill (exemplified).
- Join local gun rights or gun regulation advocacy groups to stay informed and engaged (implied).

### Whats missing in summary

Deeper analysis on the potential impacts of HR 127 on marginalized communities and the importance of grassroots activism in influencing gun legislation.

### Tags

#HR127 #GunControl #PoliticalAnalysis #PolicyImpact #DemocraticParty


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about HR 127 because both sides, fine people on both sides
have been asking about it.
And everybody seems to think it's a bad bill.
Doesn't matter where they're at on this particular issue, they're like this seems really bad.
It is.
It is as bad as it seems.
That's also why you probably shouldn't worry about it too much from the standpoint of it
being implemented.
There are a lot of political ramifications to it that we'll get to in a second.
But first, what's the bill?
It is a bill designed to require licensing, training requirements, insurance for gun owners.
Okay?
Logistically, yes, the bill doesn't make sense because it requires those people who it's
aimed at, which are those who would have issues in the future, to report themselves.
That seems like super unlikely.
The insurance is an issue as well.
While it may seem like a good idea on paper, you have somebody who is monetarily on the
hook for the actions of that owner, the downside to it is the cost to the owner.
Right now you have people who are choosing between food and rent.
They're not going to be able to pay for the insurance.
It disarms poor people.
That's what this bill does.
The people who had the money to fly up to DC for the 6th, they get to keep their guns.
It's a bad bill.
It doesn't...
I didn't see anything in it about closing the DV loophole.
Seriously?
The one thing that everybody can agree on, nobody's going to introduce a bill on.
Right?
Okay.
So, if you are pro-2nd Amendment, calm down.
This will never make it through the Senate under any circumstance.
You would have to have a whole bunch of Republicans not only suddenly embrace gun regulation,
but embrace objectively bad gun regulation.
Calm down.
It's not going to happen.
To those who support sensible regulation, this isn't it.
This isn't it.
At the end of the day, this is one of those bills that makes it... it becomes a crime
for poor people, but if you have money, it's okay.
That's the end result of this.
It doesn't actually accomplish the goal.
If you are a Democrat, you are the only person who really needs to worry.
If you are a true Democratic Party member, this bill is handing the Republican Party
the House in 2022.
This will create a lot of single-issue voters.
A lot of people who don't have money are going to be told they're trying to disarm you with
everything going on right now, and it'll work.
Aside from that, I would point out there's no way this is going to get through the Senate.
It won't pass.
So you're handing them a campaign tool for something that has zero chance of ever being
enacted.
The bill should probably be withdrawn before it gets too far and too much attention gets
drawn to it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}