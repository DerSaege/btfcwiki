---
title: Let's talk about Shell and peak oil production....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PO0LuSWi2lM) |
| Published | 2021/02/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of peak oil as the moment when oil production stops increasing and starts declining.
- Shell, a major company, predicts that peak oil already happened in 2019, accelerated by public health concerns.
- Shell plans to achieve net-zero carbon emissions by 2050, but the plan seems more like a rough sketch.
- Despite investing in cleaner energies, Shell will increase production of liquefied natural gas.
- Shell's announcement may influence other companies to follow suit, like GM's plan to phase out gas and diesel by 2035.
- Raises the question of whether the current decline in oil production is sufficient to mitigate projected environmental issues.
- Speculates that Shell's announcement could prompt others to take transitioning to cleaner energies more seriously due to financial incentives.

### Quotes

- "Peak oil will occur in 2019. It already happened."
- "The bad news is that their plan to achieve this status by 2050 is not really a plan."
- "Did it happen soon enough?"
- "This announcement by Shell may trigger the dominoes to start falling."
- "Not because they care about the environment, but because it's going to be good for their pocketbooks."

### Oneliner

According to Beau, Shell predicts peak oil already happened in 2019, aiming for net-zero emissions by 2050 with a plan that seems more like a rough sketch, potentially influencing other companies towards cleaner energy for financial gains.

### Audience

Environment advocates, energy companies

### On-the-ground actions from transcript

- Invest in cleaner energies and reduce reliance on fossil fuels (exemplified)

### Whats missing in summary

Importance of transitioning to cleaner energy sources for environmental sustainability and financial benefits.

### Tags

#PeakOil #Shell #CleanEnergy #EnvironmentalSustainability #Transition #NetZero


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about peak oil,
when it's going to happen.
If you're not familiar with the concept,
you may have heard it.
But if you're not familiar with exactly what it means,
it's the moment in time when oil production ceases to increase
and begins to decline.
There's been a lot of discussion over when
that's going to actually occur, because it's
good for the environment when it does occur.
We finally have an answer from a major company of that sort,
Shell.
According to Shell, peak oil will occur in 2019.
It already happened.
It looks like they believed it was
going to occur sometime around now or next year,
but the public health thing kind of hastened that.
So that's good news.
And they have committed to becoming a net zero polluter
when it comes to carbon.
That's good news.
Like anything with the climate, good news
is always accompanied by bad news.
The bad news is that their plan to achieve this status by 2050
is not really a plan.
It's kind of like the plan you had when you were 20
to become a millionaire by 22, more of a rough sketch
with a whole lot of wishing.
They will be investing heavily into cleaner energies,
but they will also still be, I think
it was liquefied natural gas.
They'll still be actually increasing production of that.
So it's a mixed bag, but there is good news.
This is a major company of this sort saying we're on the decline,
saying that it's over.
And while in and of itself that may not mean much,
because Shell is notoriously not great at follow
through with plans like this, it may encourage other companies
to take a stance like GM took.
GM, if you don't know, plans to basically phase out
gas and diesel by 2035.
So good news, bad news, good news.
And then we have the question that matters.
Did it happen soon enough?
Is this 1% or 2% decline that is beginning now,
is that going to be enough to mitigate
what is projected to happen?
That alone, probably not.
But this announcement by Shell may trigger the dominoes
to start falling and people to start taking the transition
to cleaner energies a little more seriously,
not because they care about the environment,
but because it's going to be good for their pocketbooks.
And historically, that has been a stronger motivator.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}