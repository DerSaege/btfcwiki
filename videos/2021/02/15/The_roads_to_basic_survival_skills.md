---
title: The roads to basic survival skills....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0DF56frxMAw) |
| Published | 2021/02/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into the topic of emergencies and survival skills.
- He showcases his EDC kit, including essentials like a knife, striker, handsaw, and more.
- Beau also demonstrates the contents of his bag, focusing on food, water, and tools for survival.
- He examines pre-packed survival bags from stores, evaluating their completeness for a 72-hour survival period.
- Beau explains the importance of redundancy and backups in survival gear.
- The transcript covers setting up shelter, starting a fire, purifying water, and cooking food in emergency situations.
- Keith shares his expertise on processing firewood and setting up a fire.
- Beau walks through different methods of purifying water, including using LifeStraw, Sawyer Mini, and Aqua Tabs.
- The importance of knives, first aid kits, shelter-building, and food choices in emergency preparedness is discussed.
- Beau touches on the necessity of practical skills like snaring, fishing, and navigation in survival scenarios.

### Quotes

- "One is none and two is one."
- "Fire loves chaos."
- "Knowledge weighs nothing."

### Oneliner

Be prepared for emergencies with survival essentials like shelter, fire-starting tools, water purification methods, and practical skills.

### Audience

Survivalists, outdoor enthusiasts

### On-the-ground actions from transcript

- Organize your own EDC kit with essentials for survival (suggested).
- Practice setting up shelter and starting a fire in your local outdoor area (exemplified).
- Learn different methods of water purification like using LifeStraw or Sawyer Mini (implied).
- Familiarize yourself with practical skills like snaring and fishing for emergencies (suggested).

### Whats missing in summary

Detailed step-by-step guides on setting up shelter, starting a fire, purifying water, and cooking food in emergency situations may be best understood by watching the full video.

### Tags

#EmergencyPreparedness #SurvivalSkills #ShelterBuilding #WaterPurification #FireStarting


## Transcript
Well, howdy there internet people, it's Beau again.
And today we're not gonna be talking about any roads
because there aren't really any around us.
Today we're gonna be talking about emergencies.
And by the end of this, you should know multiple ways
to build a shelter, purify water, cook food, start a fire,
all of the stuff you need to survive in an emergency.
We are going to open up our bags, I have Keith with me,
So you're going to see what people who are outdoorsy
and who do do this stuff a lot have in their bags.
We're also going to open up some of those pre-packed bags
that you can find in a store.
And take a look inside of them and see
if they really are everything you
need to survive for 72 hours.
OK, so what you're looking at now
is the contents of my EDC kit.
EDC, everyday carry.
This is the bare minimum that you need to have.
They've rope for snares.
This actually folds out into a knife.
You have a striker, a handsaw, multi-tool whistle,
water purification tablets.
The candle is a trick candle.
So if it blows out, it'll relight by itself.
You have a little compass.
This is a fishing kit, hook, line, sinker.
You have a survival card.
The applicatorless tampon is good for kindling.
Aside from that, if you have one, you have none.
That's a duplicate of it.
Okay, so what you're seeing now
is the contents of my bag laid out.
All right, so you see food and water.
You have an MRE, utensils, life straw, and sawyer.
You have a cooking implement.
You have a knife with paracord around the handle
to attach to a stick to make a spear or a jig.
That is a laundry bag.
The white thing there on the left,
that's for using as a cast net or something like that.
See the two little containers that say SOS. Those are my EDC kits. You see a
radio for transmitting and then another one for receiving. See the life straw
cup. So that is food and water with some additional stuff. Moving over you see a
whole bunch of stuff to create fire. You also see that there's duplicates of a
lot of stuff because if you have one you have none. It will break you will lose it.
Alright, we have duct tape, rope, Mylar tent slash blanket, and a poncho, a knife, bag,
first aid kit, big first aid kit that's more for trauma, a couple of compasses, and then
we of course have the knives and other implements, tools and stuff like that.
Okay, so somebody might be curious as to why I have two EDC kits.
Let's say you end up out in the woods and there's somebody with you and they're going
to venture off on their own.
They have that with them in case they get separated.
All right, so this is what I keep in my bag.
My bag, it may look very expensive.
It is not.
I got it at a military surplus store for about $9.99,
I think the tag said.
So you can shop around and find some of this stuff
more affordable.
But to get you started, again, this is for the region
that I live in, but I have my water purification,
my water filter, my aqua tabs.
I have a way to start fire here, a fire striker,
a lighter, a candling device which is my flashlight to give me instant light so I don't have to
light a fire for it, and it also clips to the bill of a hat so that you can see in front
of you and have your hands free.
I have my cutting tool which is a German army issue Swiss army knife, and I have my other
cutting tool.
This is a Mora Garbird.
Most of y'all may have seen this in like bushcraft videos or something.
This here is my compass again for land navigation.
I've got a roll of Gorilla Tape that's for patching, making tools, anything I may need
to do with it.
Here are backups for all of this.
You know the old saying, one is none and two is one, so always have backups.
I keep three of these black contractor bags in my bag, mainly for ground cost, but I also
use one inside of my bag as waterproofing to keep all my gear dry.
I carry a couple of liters of water with me.
I carry a canteen, a military issue canteen with a metal cup so that I can boil water
if I need to.
And here's my grail water filter.
This here is like a complete just comfort item that I have in my bag.
It's comparable to a woobie.
If you don't know what a woobie is, a woobie is like the greatest thing the military ever
invested its money in and gave a GI.
It's a, they call it a poncho liner, but it's a multipurpose blanket, sleep system, anything
that you really want it for, it's great for.
This here is improved upon as a civilian model because it has a hood built into it so you
can actually wear it underneath your poncho to keep you warm in colder conditions if it
was raining.
It also zips up into a sleeping bag on its own, unlike a regular woobie.
But it's a really great item that I have with me.
I have my food.
I've got, again, in the summertime here in Florida, man, you start sweating your butt
off, you need to replace those electrolytes.
So I have electrolyte replacements, gummies, I have meals, and I have snack bars, which
are clip bars to give you calories to keep you going.
Then I have, right here is a bag of clothes.
I keep these because maybe if you live in the city and you have a uniform you have to
wear or you're a woman and you have to wear dress shoes or something like that.
You may want to keep an extra pair of shoes to change into that are more
comfortable for walking long distances or keep you a change of clothes in case
you have a uniform you have to wear. You may not want to walk 30 miles in a suit
back home or in chef pants and non-slip shoes. So it's something to think about
that I actually keep in mind. My sleep system also includes my paracord and my
bank line here and my ripstop nylon poncho it's a military style but and I
don't want to forget about my individual first-aid kit and mainly the major part
of that is this because I am allergic to yellow flies and yellow flies are
everywhere in this area so it's great to have that to take the sting out. Okay so
now we're gonna open up a couple of those bags that we bought the when we
Selected these we didn't look at the contents because the idea behind them is that you there pick them up and go
They are pre-packed with everything that you need
So we're gonna open it up and see if they match up to the bags that we have if they contain food water fire shelter
Knife first-aid kit. That's what you have to have anything else is a luxury
Should be noted none of this is sponsored we bought this stuff or it's stuff that we had that we hadn't opened yet
whatever
But none of this none of the brands that we mentioned in this have anything to do with this video
Okay, so this is one of the brands that we'd heard of
Again, we didn't look at the contents. So let's see if it has everything that we need. This one is made by ready wise
All right, so decent backpack a
a lot of people want the ones that look like they're military it's not always the best idea
if you're looking to blend in it may be better to have a civilian looking one okay so we have
Emergency blanket, so shelter, shelter, that looks like a bio bag. Camp stove, fuel for
the camp stove. Water purification, so we have food, water, shelter. Little first aid
kit. Fire, matches, very limited. Looks like a whistle. Compass, signal mirror, flint,
fire striker pack of cards masks dust masks food tissue flashlight that looks
like it's powered by squeezing it some water pouches and some basic survival
food so we have food water food food water fire shelter first aid kit no
knife no knife in this this isn't bad given the price I want to say this one
was like 70 bucks for what you have here it's worth the money is it everything
you need though no no it's a base it's something to start with okay so this
This is one made by Camillus, which is a knife company.
It is a three day survival kit.
All right, cool little molly bag, looks a lot like my first aid kit.
Inside we have a flashlight, orange poncho, so shelter, Mylar blanket, shelter, heat factory,
like hand warmers.
Air pouches, Coast Guard approved rations.
These taste horrible.
More water, water, a chemlite, a glow stick, and a first aid kit, minimal first aid kit.
So, we've got food, water, shelter, no fire, and no knife from the knife company.
This one was only like 30 bucks.
The contents of it, yeah, I mean, that's $30 worth of contents, but it is nowhere near
what you need.
Okay so this is a two-person three-day tornado emergency prepared kit put out by First Aid
Only the name of the company I want to say this one was like 60 bucks as well.
Okay so emergency blanket, shelter, poncho, shelter, tissues, tape, shelter, tissues, water
Water bag.
Collapsible water bag.
You can fill this up.
Okay, gloves, emergency wind up flashlight, AMFM radio flashlight winds up and charges
your mobile phone.
Little first aid kit.
More of those Coast Guard approved rations.
a whole bunch of water, cold pack, first aid, a couple of glow sticks, some dust masks,
Bio bags, a Leatherman, well, not a Leatherman, a multi-tool with a knife, water, okay, there's
a bunch of single dose medicines in here.
We've got antiseptic wipes, non-aspirin, towelettes, ibuprofen, a couple of tubes of toothpaste,
another blanket, emergency blankets, shelter, shelter, little baby playing cards, eye wash,
More rations, toothbrush, looks like a whistle, hand warmers, goggles.
So this looks like it has everything except for fire.
This isn't great.
It's not perfect.
But this is out of the ones we've seen. This is the best
And again, this one was like 60 bucks the name of this is first aid only tornado emergency preparedness if this had fire
It would be the only one we've looked at so far that actually covered everything
Everything you need. Okay, so this was advertised as a three-day survival kit
It doesn't even have a brand and honestly
I was just dying to see what was in a $20 kit.
Okay Mylar Blanket.
Looks like a survival bracelet that has a compass, a whistle.
And these normally have actually have a fire starter right here, a striker, but this one
is defective.
slot for it is there but the piece of steel piece of metal is not. Flashlight, a
carabiner, little saw, knife, thistle, survival card, which one it is. So this has a
bunch of little tools on it. Kind of useful. I've got one in my bag
somewhere compass ballistic pin and a light um no not even close to everything
you need but totally worth 20 bucks as far as the contents so there you go okay
so you probably noticed that Keith's bag and my bag are pretty similar we use
different lists but they end up with the same items those bags bags that are
built like that are generally for people who have a little bit of a skill set so
they they don't have to lug everything and at the end of the day knowledge weighs
nothing the more you can learn the less you're gonna have to carry now as far as
the pre-packed bugged out bags that we bought we will basically be giving those
to people without addresses people who can actually use them okay so if you
were to ask somebody who's outdoorsy what you need to put in your bag one of
the first things they're going to tell you is that you need a knife. And then
they're going to make a recommendation. Because you're asking somebody who is
outdoorsy, they're going to use their knife a lot. They're going to make a
recommendation based on that fact. This is one of the least expensive pocket
knives I own. It's like 50 bucks. It's a Spyderco. Keith, what kind of knife do you
have 70 bucks okay because they're meant to last if you're doing this for
emergency scenarios and basically this is going to be a bag that gets put
together and thrown in a closet and never used you probably don't need to do
that there are kits that you can pick up at big box stores or order online this
This one, made by Camillus, has a machete, an e-tool, and a little saw in it along with
a match.
This one, which is Ozark Trail, that's a Walmart brand, little hatchet, field knife,
machete, flashlight, striker, paracord, kind of stuff.
You get both of those for the cost of my pocket knife.
Realistically, these are going to work.
Are they going to hold an edge as long?
No.
Are they as cool because of the name?
No.
But they're going to work.
They're going to serve their function during an emergency.
And realistically, you're probably not
planning on spending months out in the woods at a time.
So just weigh that in mind when you get that knife
recommendation, and then you see the price tag on it.
OK, so Keith has been gathering firewood this whole time.
So we're gonna see what he's got and how we're doing.
All right guys, when you're processing your firewood,
you wanna start by breaking it down into small pieces
and separating it into piles.
Your first pile you want to be pencil lead
and pencil sized pieces.
Your next pile you'd want to be finger
and thumb sized pieces, and then after that,
you'd want like wrist size or larger pieces in a pile.
And if they're any bigger than that,
really you wanna split them so that they'll burn well.
All right, so this is kind of the configuration
that you want to set your kindling up into.
Again, your very fine kindling to start
before you add your bird's nest,
before you start trying to set fire to your tinder.
You want to set it up this way,
and I'll show you how to do it in a minute,
but the reason is is you want to point this like a V
into the wind so that this acts as a shield.
If the wind's blowing towards you,
then you would have a bundle on top to roll over
once you get your tinder started
so that it will have fuel to start.
Keys to a fire are three things.
You need fuel, oxygen, and heat.
So to build it, you're going to take your extra fine kindling here, and you're going
to get it into three bundles, kind of formula V here, so you have a place to get your tinder
started out of the elements.
And your third, again, will go up here towards the front so that whenever it starts, you
were able again you would be over on this side and you would pull this over
on top once it got going to keep the fire going then you would add more fine
tinder then you would add your finger and thumb size and keep adding to
between those two until it gets hot enough that you could add the thicker
wood to keep your fire going. Alright here in northwest Florida we're kind of
lucky about year-round there's this stuff called deer moss that just grows
everywhere on the ground and it's a pretty much free tender. So I'll show you how easy
it is to get you a fire going. I say that and my match blows up.
And when you burn this stuff, I mean, I would be careful.
People have been known to accidentally set areas of wood on fire because this stuff goes
up pretty fast as you can see.
And again, I just picked that up off the ground a moment ago.
Alright guys, so we're going to show you how to use a ferrocium rod and striker.
You get these things, they have this black coating on it.
You got to use the striker, the steel striker, to get that off first before you can really
get that spark going. Like I said in the back end it comes with two of these
little tufts of tinder. I'm going to stick this down in here with my deer moss.
I'll try to get this going real quick. And usually you want to hold the striker
in place and pull your Pharisee and run away so that it showers the sparks
instead of trying to shove them off.
see how quick that took? get your moss going, go ahead and roll your twigs and sticks up
over. man you got that on one try and i forgot to hit record. did you? nah i messed up. thanks.
Again, you can sit here and just keep feeding it.
Wait till we get bigger and hotter.
Get you a little more of a cold bed down in here.
And then you would start adding these.
But again, that's going to be another few minutes away
before we could get hot enough to do that.
Throw those on.
And you ain't got to do it all fancy.
People talk about doing a lay for a fire.
Other than that first set up, they
see fire loves chaos so just throw that stuff on there. Who says that? Nobody. I've never heard that before in my life.
Fire loves chaos. Sounds good though.  Now that's a fire.
Okay, so now we're going to go get some water.
Looks delicious.
Okay, so this is the LifeStraw cup.
It has the normal LifeStraw right here inside of it.
And this is the easiest way, this way you don't have to put your face down in the water.
take the cup, easy, put it in the water so you don't disturb all of the sediment. Theoretically
you don't really have to, but it would be cool if your water wasn't grained to begin
with. Slide that in, screw it back on, give it just a second. That's all there is to
it. As far as ease of use this is my favorite. This Sawyer Mini it comes with
easy to use instructions and tells you right here filters down to 0.1 microns
and that doesn't take out chemicals and it does pretty much get all sediment
and most bacteria and stuff that would be large coming in. It's not going to kill
anything of course but it comes when you buy it the Sawyer Mini here comes with
this little straw that you can put on the end or my bad again pay attention to the
arrow the straw goes on to this end because this is the way that the water
is supposed to flow this way you can use this get down and suck the water out if
you needed to and it comes with this little squeeze pouch
that you can fill with water, I'm going to kind of
clear it up a little bit first
so that we can get in there easier.
You have to get that water in there.
Take that straw off and as you can see right here on
the bottom side of this, there are threads
fixing it right there, you just roll
the bag up and that's what filters in a second. See it started running clear now. You got
some purified water there. Purified filtered anyways. Another quick thing to note about
the Sawyer Mini is that if this bag wasn't enough or you wanted to keep it but you needed
more water it will fit on most standard threads here as a smart water bottle
it would do the same thing you would fill it with water you would turn it
upside down and you would give it a squeeze if you wanted to be a little
safer make sure that you were killing anything that was in there you could use
aqua tabs now aqua tabs here you would do the same thing you just did with that
little thing except for this time we'd fill it all the way with water you know
then you would take pull them out so you can see what they actually look like
again you would want this whole container filled with water this is a
one liter bottle this is meant to actually purify 0.75 so or 0.75 liters
so you would put that little tablet in there this would be much fuller again
you'd want to shake it up the instruction says do this for about ten
minutes. Keep mixing it up and it would be fuller but once you do that you mix it
up for about 10 minutes then you would let it sit for about half an hour to
purify. Once that's done then it would be no simple thing to go ahead and hook
this up have purified filtered water while you're out in the field. This here
is kind of a luxury item in my bag it's a secondary water filter that I carry
and this is pretty pricey it's about 90 dollars it's called the grail geo press
and how it works is this it has this little spout here for you to drink out
of the course but you take this loose you slide this out and your filters at
the bottom and you would fill your cup up and it's about port to a liter and you
don't have to fill it all the way up but you could fill it way fuller than I
just did you press the filter in like that then you put your body weight
against it. It will slowly filter your water and it works a lot quicker than some other
things, especially if it's gravity fed. So you do the geopress, press it down.
Okay so when it gets to the subject of water filtration, purifying water, there are two
names that always come up LifeStraw and Sawyer. There are differences in what they filter but the
main difference as far as the reason that most people choose one over the other is length of
service and ease of use. LifeStraw a whole lot easier to use Sawyer more complicated but while
this will filter a hundred thousand gallons of water this one will filter a thousand. That's
your big trade-off with it. Your main thing when deciding between them should
really be about how long you plan on using it as long as the filtration
concerns aren't applicable to you. So take a look at the differences in the
effectiveness of the filters and then determine length of service. So when
you're setting up your shelter what you're really looking for is an area
that's on the higher ground as you can see this is going downhill with about
the highest point here besides where the camera is you want to find a structure
or two trees far enough apart that you could build your shelter in between it
and you want to look up and make sure there are no what they call widow makers
any branches or anything that could fall down or dead trees leaning over they
could fall in the middle of night or in a high wind and kill you while you're
out here again you want to make sure that the area where you are building
your shelter is free of limbs and debris like right here.
We would go ahead and clear this area out first, of course.
Make sure there was nothing down here on the ground, make sure it was super cleaned out.
And then we would set up our ridge line.
And I have this set up with a quick release.
So again, you start at waist height or knee height, depending on how low you want your
shelter, put your little stake in there like that, build you a quick release, then tie
off to the other side of your tree.
Most of you all know how to do a trucker's hitch.
So that's what we want to do here, we're going to tie off.
I'm not going to make this look all pretty pretty, it is going to look nice.
excess. Now, once you got your ridgeline getting secured, then you would affix your
pressing knots. I showed you a minute ago how to do one and affix it. If you already have
these fixed to your ridgeline, again this makes it so much easier whenever you are
in an emergency situation and you do need to build shelter. So let's go ahead
ahead and put our other one on. Again, it's loop through the inside once, come back through
a second time. Again, they just keep falling right inside of each loop into the center
until you got all three. Again, three is all you need. The way we're setting up our shelter.
get up our I'd be using as my ground cloth here all these vines here are not
optimal for doing this but you'd want an area where you could set this down so
you can stay dry or again you could fill it with pine straw dead leaves to fluff
this up and make it a little more comfortable and so next part of the
This shelter is the poncho, the main part, the roof.
We're going to take it long ways and we're going to drape it over our ridge line.
Find us two more sticks real quick to make us some little toggles out of, just so we
can fix this.
Again, I'll show you real quick.
Take that, and stick it through right there, put your toggle in there, keep that cinch
right there for a moment, and really tighten it up to this side, or the opposite side from
where you started.
These are quick little things that you can have set up that will make it so much easier
if you do have an emergency situation and you do know what you're doing, you do have
your gear set up, so again, there's a toggle here on this ridge line, we're just going
to pull that tight, you see how tight that stays, pull it down, pull it down, and then
Then we would move to the backside and that is where these stakes come in handy.
Again, these are like little 70 cent stakes that you can get at Walmart or anywhere.
Make sure that you don't have any of this debris in the way you don't want to puncture
a hole in your stuff.
Bring that back real tight here and use your stake to pin it down to the ground, all the
way down.
Same thing to the other side.
And then for me, like I said, I always carry three, one for right in the middle.
Now with that being said, again
Get my ground cover, slide it up in here
Little punch of stuff away, move these vines over
Again
Now you got a shelter. I am
Six foot three
and
I fit up underneath this comfortably
You stay out of the rain, stay dry, again these vines aren't optimal, but again if you're
in an emergency situation this will keep you dry.
This will keep you out of the elements, this will keep you sheltered, so here you go.
The first step in setting up your shelter is setting up your ridge line and the easiest
way to do that is to set up a quick release and how you do that is you take your cordage,
whatever you're using, whether it's 550 paracord or bank line or anything else, and you want
make what's called a bow line knot. What you're going to do is take the end of your cordage,
you're gonna make a loop and pinch it, then you're going to insert it into your loop and
let it dangle back out. You're gonna let it wrap around the piece that was in your other
hand, and you're gonna pinch it to that loop in the middle. When you do that, you're gonna
pull tight and that makes a loop with this little tail in the middle. That is a bow line
not. So when you're setting up your ridgeline you want to set it at waist
height or knee height depending on what kind of concealment you're looking for or
how much coverage from the elements you're needing. So I'm going to set this
one at about waist height. So to set to begin with the quick release on this
would be to bring your knot around, slide your slack through a little bit
like that a loop, pine strata here, and then that acts as a quick release so that when
you pull that stick out you can get out of the way quickly, get moving.
Next step is to make your pressing knots.
What you want to do is take a piece of your cordage about as long as from the tip of your
middle finger to your elbow.
When you do that you're going to do two overhand knots and what you're going to do is called
a fisherman's knot, where you do an overhand knot.
You may remember how to do these when you were a kid, when you did friendship bracelets.
Do those little knots right there on the end,
and then you just pull them together,
and it's secure.
That is your pressing knot
for fixing your shelter to your ridge line.
Now how we're going to do that, is we take our
knot here, and we go
One time through, two times through, and then a third time, just three times.
And you bunch it up here on your ridge line.
will do is I will whenever I have fixed my shelter here the grommets my corners
this will slip through and I will put me a little a little toggle in there when
When you do that, it allows this to cinch up tight.
Hi guys, in addition to the way I showed you how to just set up your shelter to keep you
out of the elements, another way you can set it up is in a bivvy style, which is more of
a traditional triangle tent shape where it splits the tarp right down the middle.
If you do that, it makes it easier to conceal where you're camping if you need that kind
of concealment.
So if you do do that, make sure that you use foliage that blends in.
If you're using dead pine needles to cover up your shelter and everything around it is
live pine needles, it's going to stand out.
So try to use the native foliage around you to blend everything in.
Okay so it's time to eat.
Yeah, mountain house.
Move it off.
Chef Keith here is about to get to work for us.
What do you got?
Mountain house?
Yeah, mountain house.
two serving meal. I think it says two servings per container and it's beef
stroganoff with noodles and a savory cream sauce with mushrooms and onions
freeze-dried. So the instructions say that we need to have hot water to
activate this and get it going. Let's just add hot water ready in 10 minutes. No
extra cleanup. 12 grams of protein. The instructions call for one and two thirds
cup. This right here is about two cups. I'm gonna eyeball it. I'm using the water
that I purified in my grail geopress from down at the lake earlier. Usually I'd
wait for there to be a little better coal bed here, but I'm losing light and I'm
getting hungry. So I'm going to sneak down here and I'm going to manacle that in right there by that fire.
Only coals not right up underneath the flames or anything like that. I got it
right up on the coal bed and pretty soon it'll start boiling. When it starts
boiling, do not reach in there and grab it. You need to have something to take
it off the fire with. As always when dealing with fire be careful, you know, be a responsible
person, adult, don't try this at home and all of that stuff. Okay so our water started boiling,
got some moving bubbles there. We'll go ahead and tear this off. It's got a little tear right here
area and a zip, a little zip-like zipper on the inside. We'll use this glove here to take this.
Hot water off, so we don't want to burn ourselves.
I'm making sure I'm not sitting in antsy.
Again, go ahead and add that hot water in there, and you can hear that hitting that
side of that hot stainless steel cup right there.
Finish sealing it up and let her sit for 10 minutes, then in 10 minutes we will have supper.
There's my handy dandy little spoon.
a little bit. Oh my fire pushing stick there. Why? Long day sir. Hey you know what's fun?
What's that? Nobody's gonna try to sneak up on us tonight. That is fun. Too bad it is in a
cloudless night. Sleep out here underneath the stars. Oh man. And that's why it's always
how you need to have your shelter built before it gets dark. Okay, so can you think of anything
we missed while filming? Anything we missed while filming? Food, water, fire, shelter.
Don't know that I mentioned in any of it. If you notice, both of us, our first aid kits,
separate from the bag from the main bag because if there is an emergency you
don't have to lug all of your stuff to get there. I've got the little baby one
in my bag and I have this which is a trauma kit which is basically the same
thing you have. It's an IFAC you can get them from eBay or wherever. It has it is
for more serious injuries. This is the one that you buy and you spend a decent
of money on and hope you never actually have to open it. It's like something that uh applying
pressure isn't going to fix. Oh man. Nice into it. Long day though. Stay here by the campfire.
Get some party beef stroganoff here in a few minutes.
That titanium cup's already cooled off enough now
If I can touch it, if I can hold the handles, it was just, again you heard it sizzling against
the side of that cup.
They might have been able to see it, but it was right on the edge, I'm not sure.
Kerry will fix that somehow.
Thank you Kerry.
She's magic.
Oh, man.
So realistically, we got everything set up
in two, maybe three hours of actually doing it.
And then if we wanted to, theoretically, we
could stay here for weeks.
We don't want to.
Right.
Well, theoretically you could, but if you
were having to track your own game or anything like that,
probably have to keep moving. They'd get used to your scent and scare off or you
may draw attention if you hang out in the place too much. I'm not sure I
mentioned it. Okay so we didn't do anything with snaring. I think I
mentioned this earlier but I'm not sure so I'll go over it again. We didn't do
anything with snaring and setting the snares up although we have the stuff to
do it mainly because neither one of us is really wanting to eat squirrel
tonight but aside from that just remember that mousetraps there are things that are a little bit
more technologically advanced than what was available in the stone age that are a whole lot
easier to use. Snaring and stuff like that it is impressive in the sense of it's a cool skill to
have, you can do it with absolutely nothing. But, I mean, mousetraps aren't cheap, or
aren't expensive. And it's easier to take out your MRE and put a little dab of peanut
butter on a mousetrap and set it out there and catch you something for the next day or
have something to use as bait for the next day when you need to catch something else.
right. We also didn't do any fishing stuff, which we could have. There's a whole bunch
of ways to do it. We didn't do it because the way you would do it in a survival situation
is something that more than likely Florida Fishing Game would not be happy about. So
there are probably videos out there about that, but it's just not something I'm going
to demonstrate. You don't want one of those Coast Guard SOS rations that are
in the, if they were in some of those pre-packed bags, you don't want one?
One of those hardtack biscuits? Yeah. No, I'm good. You sure? Chip a tooth, don't
right I mean you have to they're high calorie they will keep you alive but man
they're horrible yeah flavorless and dense
I also think whenever you're putting stuff in you're not going for exercise
so if you do pack food don't pack stuff like protein bars that are low
carbohydrates and that are meant to keep you lean or keto foods I would
I would steer clear that I'd look for a high calorie foods
that's gonna propel you through the day
instead of worrying about your figure.
Right.
But again, if you're in a pinch,
then that's all you have, stick it in the bag and go.
All right, I think we're getting close to time.
What do you think?
Sure.
About 10 minutes?
I mean, realistically, I'm pretty sure that stuff's
probably safe to eat without cooking, but.
We'll stir it up again real quick.
And then, I'm gonna form my half into my cup,
and get the other half over here.
Go, sir.
So quick recap, we covered food, water, fire, shelter,
talked about knives, talked briefly about first aid kits.
I have another video about first aid kits
and the different types that exist,
and maybe we can splice some of that in.
Anything that you think we missed at all?
Not really.
I'm saying for the kind of stuff you need to have in your bag,
again, not the exact brands, but what you need to survive,
what you need to get by with.
Again, if you're in one of these situations where you do have
to bug out or it is a SHTF situation,
you may be able to find extra stuff you
along the way but you need to have a good base to start with and the five C's
you know what are the five C's I don't I don't use that acronym so you need like
a canning device which would be like a canteen or a bottle or something to put
your water in you would need a candling device which would be like either a
candle or a flashlight something that can put out light for a long amount of
time if you're not going to be able to start a fire or move with a fire then
then you are going to need something to start a fire with.
You're going to need cordage for your shelter,
you're going to need cover, which is your shelter,
and then you need a cutting device.
And usually you would add in a compass on that too.
So, again, that's probably 60s, but.
Right.
We didn't do any navigation stuff, we do.
If people like this, you know, this is 101,
if people like this, we may go out and do land nav
and maybe even do like a little seer thing.
Okay, so that concludes today's adventure.
Hopefully, you've picked up some new skills, or at least learned some new ways to do some things you already knew how to
do in other ways.  These are skills that everybody should know.
You never know when there's going to be some kind of a disaster, some kind of incident that you're going to need to
improvise with.  And that's, hopefully, one of the, uh,
The things you learn the most when you're out camping or out in the woods is how to
improvise and how to create something from nothing.
You don't have to prepare for the zombie apocalypse to have a basic skill set and how to survive.
Anyway, it's just a thought.
y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}