---
title: Let's talk Lindsay Graham wanting to impeach Kamala Harris....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nPCJ7fHyC5o) |
| Published | 2021/02/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Lindsey Graham suggested impeaching Vice President Harris if Republicans retake the House in 2022, citing her support for bail funds as equivalent to the former president's actions.
- He explains what bail funds are, accounts set up to help individuals post bail beyond their financial means so they can participate in their own defense.
- Beau argues that denying bail should be reserved for genuine flight risks or threats to society, not for those with bail set beyond their financial reach.
- He points out that excessive bail is against the Constitution, specifically the 8th Amendment, and supports Vice President Harris for backing a constitutional principle.
- Beau criticizes the Republican Party for allegedly abandoning founding principles, coordinating with defense in trials, and ignoring impartiality oaths.
- He challenges the potential impeachment of Vice President Harris as a partisan move that most Americans see through, suggesting it will only appeal to a small faction.
- Beau questions the selective targeting of Vice President Harris for impeachment, a woman of color, compared to the lack of action against an older white male.
- He concludes by expressing his thoughts and wishing everyone a good day.

### Quotes

- "Please by all means have your lackeys in the House impeach the Vice President because she tweeted support for a constitutional premise."
- "The majority of the United States sees right through it."
- "Just the woman who isn't white."
- "Play your silly little game."
- "Y'all have a good day."

### Oneliner

Senator Lindsey Graham's suggestion to impeach Vice President Harris for supporting bail funds is criticized as a partisan move that goes against constitutional principles, with Beau questioning the selective targeting based on race.

### Audience

Voters, political activists

### On-the-ground actions from transcript

- Challenge partisan moves in politics (implied)
- Support constitutional principles (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of potentially impeaching Vice President Harris based on her support for bail funds, questioning the partisan nature and racial implications of such actions.

### Tags

#Impeachment #Partisanship #ConstitutionalPrinciples #BailFunds #SelectiveTargeting


## Transcript
Well howdy there internet people it's Beau again.
So today we're going to talk about something that Senator Lindsey Graham said.
You know he's a senator mind you.
He suggested in the aftermath of the acquittal that if the house is retaken by Republicans
in 2022 well they may just decide to attempt to impeach Vice President Harris because you
know she tweeted support for bail funds over the summer and that is somehow in some way
equivalent to what the twice impeached former president did.
There's an equivalency there.
You know a lot of people don't know what bail funds are so let me explain it real quick.
A bail fund is basically an account that gets set up so those people who have bails that
are set that are beyond their financial means can access so they can then post their bail,
get out and participate in their own defense.
That's the idea of a bail fund.
In the US you really shouldn't be denied bail unless you're a genuine flight risk or a real
continued threat to society at large.
That's when you should be denied bail.
Other than that you should get it.
I would suggest that if a judge sets a bail beyond the financial means of the accused
well that's in essence denying them bail.
I would suggest that that's excessive.
It's something we don't want in the United States.
So much so that it's in the Constitution Senator.
It's the 8th Amendment.
Please by all means have your lackeys in the House impeach the Vice President because she
tweeted support for a constitutional premise.
Because she supported the Constitution of the United States.
The founding principles of this country.
I can't see any way that that would backfire sir.
You know it's not like most of America has already realized the Republican Party has
abandoned all of our founding principles.
That they have never read the Constitution or they don't care what's in it.
It's not like the entire country saw major political figures within the Republican Party
coordinating with the defense in a trial in which they were supposed to be impartial.
Under an oath that they took demanded by the Constitution.
It's not like we all saw that or anything.
Yeah please go ahead.
Play your silly little game.
Make it completely partisan.
I'm sure it will fire up those sunshine patriots that you have in your base.
But the majority of the United States sees right through it.
And I would point out that I find it really interesting that you didn't try to come up
with some reason to impeach the old white guy.
Just the woman who isn't white.
I'm sure that doesn't mean anything.
I'm sure that's just a coincidence, Senator.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}