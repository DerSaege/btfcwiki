---
title: Let's talk about Gaetz, the pledge, patriotism, performance, and politicians....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DCJknkoh9bY) |
| Published | 2021/02/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tells a story of a civilian and a colonel having a significant exchange about patriotism in the early 2000s.
- Describes how the civilian believed patriotism was about action, not just symbols.
- Recalls the civilian's 30 years of service to the country, which gave weight to his perspective on patriotism through action.
- Mentions Republicans led by Matt Gaetz expressing outrage over a committee's decision not to start meetings with the Pledge of Allegiance.
- Points out that members of Congress can demonstrate patriotism through daily actions rather than performative rituals.
- Criticizes politicians for prioritizing publicity stunts over putting the country and its people first.
- Emphasizes that true patriotism is displayed through actions, not mere words or symbolic gestures.

### Quotes

- "Patriotism is shown through action, not through worship of symbols."
- "You show your patriotism through action, not through publicity stunts."
- "Put the country and the people of this country above your own political interests."

### Oneliner

Beau shares a story from the early 2000s illustrating that patriotism is about action, not symbols, criticizing politicians for prioritizing performative gestures over genuine care for the country and its people.

### Audience

Politically conscious individuals

### On-the-ground actions from transcript

- Prioritize actions that benefit the country and its people over performative gestures (implied)

### Whats missing in summary

The full transcript provides deeper insights into the importance of genuine patriotism through actions rather than symbols alone.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about performances, and the pledge, and politicians, and patriotism.
I've told this story on this channel before, but it has been a while and it bears repeating.
It's very fitting today.
In the early 2000s, I witnessed a conversation and it has stuck with me, the underlying themes.
Really important.
Conversation was between a civilian and a colonel.
Now I don't know if you are aware, but during this period in time, if you were in a suit,
you were wearing that American flag lapel pin.
It was super important.
By early 2000s, I mean late 2001, early 2002.
Some things had just happened and everybody was showing how patriotic they were.
The civilian wasn't wearing one.
The colonel asked him why, not in an accusatory tone because he knew him, but he asked him
why.
And the guy said that he believed patriotism was shown through action, not through worship
of symbols.
That's a good answer, right?
During that time period when everybody is being super patriotic, a guy in the military
asks you why you're not wearing your American flag lapel pin.
That is a good answer.
Made even more poignant by why the conversation was taking place.
The guy was a civilian, but he had served this country for 30 years doing some pretty
horrible stuff.
The reason the conversation was taking place was because the guy was coming back, because
he was in Afghanistan the first time against the Soviets.
Might have information that could be useful, patriotism through action.
Right now a bunch of Republicans led by Matt Gaetz are, well they are engaging in their
normal outrage because a certain committee in the House of Representatives doesn't want
to open every meeting with the Pledge of Allegiance.
Now I have a video on the evolution of the pledge.
I'll put it down below, it's worth watching.
But I'd point out that a lot of these people in an uproar, they are members of Congress.
They're up on Capitol Hill.
They can show us every single day through action that they are patriots, that they care
about this country, they care about the people of this country, they care about this country
standing in the international community.
They can do that through action every single day.
But they want to engage in some performative ritual.
I mean I guess if I was part of a political party that had spent the better part of a
year fomenting open rebellion against the ideas of the republic and the underlying principles
of it, yeah I mean I guess I would want footage of me standing there with my hand on my heart
staring at the flag as well.
But you don't need it.
All you have to do is put the country and the people of this country above your own
political interests.
That's it.
You show your patriotism through action, not through publicity stunts.
I would also point out that if you are up on Capitol Hill, you are by definition a politician.
Nobody believes what you say anyway.
You watch what you do.
Anyway it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}