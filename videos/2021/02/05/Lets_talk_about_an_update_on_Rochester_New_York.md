---
title: Let's talk about an update on Rochester New York....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DGk9_rcUEgg) |
| Published | 2021/02/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update on the situation in Rochester, New York and mentions state legislators taking action.
- Talks about a bill introduced to ban the use of sprays against minors entirely.
- Acknowledges the good intentions behind the bill but questions its effectiveness in addressing excessive force.
- Shares a story about a former deputy's reaction to the footage and the potential escalation of force if one tool is taken away.
- Argues that excessive force is any force beyond the minimum necessary for an arrest, suggesting addressing attitudes rather than specific tools.
- Proposes legislation requiring random body camera footage checks and consequences for officers using excessive force.
- Appreciates the legislators' intentions but stresses the importance of effective legislation to address the root issue.

### Quotes

- "Excessive force is any force in excess of the minimum necessary to affect the arrest."
- "The issue isn't that a specific type of force was used. The issue is that force was used."
- "Go after the attitude."
- "Their heart is definitely in the right place. Now we just have to get the legislation there."
- "Y'all have a good day."

### Oneliner

Beau provides insights on addressing excessive force by focusing on attitudes over tools, urging for legislation to require random body camera footage checks.

### Audience

Legislators, activists, community members

### On-the-ground actions from transcript

- Advocate for legislation requiring random body camera footage checks and consequences for officers using excessive force (suggested)
- Support initiatives that focus on addressing attitudes towards the use of force within law enforcement (suggested)

### Whats missing in summary

Further details on the specific steps required to push for legislative changes and effectively address issues of excessive force and police conduct.

### Tags

#Rochester #Legislation #ExcessiveForce #PoliceReform #CommunityPolicing


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to do a little update
on the situation in Rochester, New York.
We talked about what happened
and there have been some developments.
Almost as soon as the news broke, state legislators
jumped into action.
I've already read the bill.
By the time y'all watch this, it'll probably have been introduced.
And I want to be real clear on something before we get too far into this.
Sometimes when I'm talking about legislation that
isn't going to work the way
they think,
I'm low-key criticizing the people who wrote it.
I'm not doing that in this case.
Their heart is in the right place.
They saw something they never wanted to see again
and they took the most direct route possible
to make sure they never see it again.
The bill
bans the use of sprays against minors.
Period. Full stop. That's the end of it.
Under any circumstance.
And I get it.
I get it.
That would definitely stop that particular issue from happening again.
You know, it definitely seemed excessive.
So much so that I was talking to a former deputy
from around here.
He was a deputy like twenty-five years ago.
And he had seen the footage.
He said, you know,
if I did something like that,
Quinn would have met me in the parking lot with his mag light.
Quinn was the sheriff.
And I met him.
Now I don't know that Quinn really would have hit him with a mag light.
But I don't know that he wouldn't have either
because
that video.
That was
pretty dramatic.
But that little joke
tells us something else too.
There's a lot of stuff on those belts.
If you just take away one tool,
they will go to something else.
A baton, maybe something worse.
Most of America looked at that and they saw excessive force.
Because that's what we have been trained to view
as excessive.
Something that is just so egregious,
it provokes outrage.
But that's not what excessive force is.
Not really.
Excessive force
is any force
in excess
of the minimum necessary to affect the arrest.
That's excessive force.
So
if you want meaningful
legislation,
don't go after a tool.
Because the issue
isn't that a specific type of force was used.
The issue
is that force was used.
So rather than
go after
a specific tool on their belt,
go after the attitude.
Go after the attitude.
You want meaningful legislation,
introduce a bill
that requires
body camera footage
be randomly reviewed,
and any officer
found to be using excessive force,
force in excess of the minimum required to affect the arrest,
loses their state certification
or whatever it is you all have up there.
Again,
I am not criticizing
the legislators in this case.
They,
their heart
is definitely in the right place.
They saw something that needs to change
and they were willing to introduce
legislation to change it.
Their heart's there.
Now we just have to get the legislation there.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}