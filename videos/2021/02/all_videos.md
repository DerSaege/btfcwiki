# All videos from February, 2021
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2021-02-28: Let's talk about running for office.... (<a href="https://youtube.com/watch?v=VvoV2V_5YcU">watch</a> || <a href="/videos/2021/02/28/Lets_talk_about_running_for_office">transcript &amp; editable summary</a>)

Potential candidates face ideological struggles and compromises when considering running for office, navigating a system that can break down radical motivations while demanding conformity for survival.

</summary>

"If you have an ideology that's on the fringe, if you really want to move things in a radical direction, you can't tell anybody. Because then you become unelectable immediately."
"Holding public office often involves a thankless journey with minimal tangible accomplishments."
"You had better be committed to your beliefs. But you never get to say what they are."
"Candidates must be ready to navigate a world where being true to their fringe ideology might require staying undercover."
"Running for office is described as a challenging and potentially rewarding endeavor that may break many individuals."

### AI summary (High error rate! Edit errors on video page)

People considering running for office are disheartened with the current state of things.
Many potential candidates hold ideologies that may be considered inconsistent with traditional political structures.
Individuals on the left or very anti-authoritarian face challenges in conforming to hierarchical systems and using force.
Those seeking purity in their ideology may find it difficult to pass the standards required for office.
Despite ideological inconsistencies, those genuinely concerned and seeking advice are encouraged to pursue office.
Candidates entering politics must be prepared to face a system that can break down ideologically motivated individuals.
Being in office may require compromising on one's beliefs to fit into the mainstream and ensure re-election.
Declarations of radical ideologies openly could lead to immediate unelectability.
Success in office may be limited, but small victories can be significant for those driven by strong ideological motivations.
Holding public office often involves a thankless journey with minimal tangible accomplishments.
Candidates must be ready to navigate a world where being true to their fringe ideology might require staying undercover.
Knowing what compromises are acceptable and sticking to them is vital for individuals venturing into politics.
Running for office is described as a challenging and potentially rewarding endeavor that may break many individuals.
The decision to pursue political office and endure its challenges ultimately rests on the individual's willingness and strength.

Actions:

for aspiring candidates,
Run for office while being prepared to face challenges and compromises (implied)
Be ready to navigate a system that may require compromising beliefs to fit in (implied)
Understand the potential thankless nature of holding public office and the need for strong ideological commitment (implied)
</details>
<details>
<summary>
2021-02-28: Let's talk about how we talk about other countries.... (<a href="https://youtube.com/watch?v=VKpQbqRnNks">watch</a> || <a href="/videos/2021/02/28/Lets_talk_about_how_we_talk_about_other_countries">transcript &amp; editable summary</a>)

Beau stresses the importance of recognizing the people behind other countries, cautioning against blaming individuals for their government's actions.

</summary>

"The people of these countries, they're just people just like you and me. They don't have any control over this stuff."
"Holding the average citizen responsible for the actions of its government, that's something that I can't believe is widely accepted in the United States."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of viewing other countries and their people beyond just their leadership.
Points out how politicians and media often portray other countries as united behind their leadership to simplify narratives.
Notes that most countries are more divided than the United States and people have less control over their governments.
States that individuals in the US may have more in common with average citizens in other countries than with their own government representatives.
Warns against accepting collateral damage based on the actions of other countries' leadership.
Emphasizes that the people in these countries are just like us, lacking control over government decisions.
Expresses disbelief at holding average citizens accountable for their government's actions and hopes this standard is never applied to the US.

Actions:

for global citizens,
Acknowledge the individuals in other countries beyond just their leadership (implied)
</details>
<details>
<summary>
2021-02-27: Let's talk about the House passing the stimulus bill.... (<a href="https://youtube.com/watch?v=yurGxoqjswM">watch</a> || <a href="/videos/2021/02/27/Lets_talk_about_the_House_passing_the_stimulus_bill">transcript &amp; editable summary</a>)

The House passes the stimulus bill with $1400 checks, facing Republican opposition, and potential filibuster modifications looming.

</summary>

"Nay, nay, nay, nay, nay. All of them."
"But this is a big win for the Democratic Party."
"Every Republican voted against helping out working families."
"It's definitely a win."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The stimulus bill passed the House in the early morning hours.
Key provisions of the bill include $1400 stimulus checks, increased jobless benefits, and more.
The bill also includes a minimum wage increase that may face challenges in the Senate.
Republicans unanimously voted against the bill.
Speaker Pelosi's statement on the minimum wage being inevitable raises questions.
The bill is seen as a win for the Democratic Party.
There's speculation about potential moves regarding the filibuster.
Republicans opposed helping working families, citing they are not the party of country clubs.
The bill represents a significant victory for the Democrats.
Beau plans to address filibuster modifications later.

Actions:

for us citizens,
Contact your representatives to express support or concerns about the stimulus bill (suggested).
Stay informed about potential filibuster modifications and their impact (suggested).
</details>
<details>
<summary>
2021-02-27: Let's talk about ending the filibuster.... (<a href="https://youtube.com/watch?v=Id-QM86wyqk">watch</a> || <a href="/videos/2021/02/27/Lets_talk_about_ending_the_filibuster">transcript &amp; editable summary</a>)

Beau addresses changing the filibuster, proposing drastic options, and advocating for transparency through a speaking filibuster to progress on social issues.

</summary>

"Doing something simply because it's always been done that way, that's a really bad reason to continue doing something."
"There's no doubt about that. But there is the law of unintended consequences that needs to be thought about."
"A little bit of gridlock in government is good."
"I think it needs to be a change."
"Altering the filibuster, even getting rid of it, that would help move us along."

### AI summary (High error rate! Edit errors on video page)

Addressing the topic of changing the filibuster, citing significant public interest after recent events.
Disputing the argument that the filibuster should be upheld simply due to tradition or Senate norms.
Explaining the accidental creation of the filibuster, debunking surrounding myths.
Outlining two main options for changing the filibuster: abolishing it entirely or implementing a speaking filibuster.
Expressing concern that abolishing the filibuster could enable extreme policy changes when the opposition gains power.
Countering that argument by proposing that showcasing successful policies without the filibuster could politically damage opponents.
Advocating for a speaking filibuster, predicting it may reveal politicians' true thoughts and provide transparency on issues like the Equality Act and minimum wage.
Acknowledging the need for government restraint but urging progress on social issues by altering or eliminating the filibuster.

Actions:

for policy advocates,
Advocate for transparency in government by supporting the implementation of a speaking filibuster (implied).
Engage in debates and dialogues on the necessity of changing or abolishing the filibuster within political circles (implied).
</details>
<details>
<summary>
2021-02-26: Let's talk about why Democrats couldn't get the minimum wage increase.... (<a href="https://youtube.com/watch?v=AxDtc1Q6hWo">watch</a> || <a href="/videos/2021/02/26/Lets_talk_about_why_Democrats_couldn_t_get_the_minimum_wage_increase">transcript &amp; editable summary</a>)

Democrats push for a minimum wage increase, but obstacles make it unlikely, especially with Republican opposition.

</summary>

"The odds of them pushing this through, especially at $15 an hour, are slim to none."
"There's probably not going to be any movement on that for quite a while."

### AI summary (High error rate! Edit errors on video page)

Democrats want to increase the minimum wage, but it's unlikely to happen.
The Senate parliamentarian ruled that using budget reconciliation for a minimum wage increase is outside the scope.
Republicans are unlikely to support a minimum wage increase, especially at $15 an hour.
Democrats may need to compromise and lower the proposed increase to $11 or $12 to have a chance of passing.
A minimum wage increase is no longer a realistic topic until at least the midterms, and even then, it's uncertain.
The rest of the bill may progress without the minimum wage increase.
Movement on the minimum wage increase is unlikely for quite a while.

Actions:

for policy advocates,
Advocate for a minimum wage increase in your community (implied)
Support organizations working towards fair wages (implied)
</details>
<details>
<summary>
2021-02-26: Let's talk about the Equality Act, changing society, and Mr. Rogers.... (<a href="https://youtube.com/watch?v=GzGPkjjVfFk">watch</a> || <a href="/videos/2021/02/26/Lets_talk_about_the_Equality_Act_changing_society_and_Mr_Rogers">transcript &amp; editable summary</a>)

Beau underscores the power of individual actions in creating societal change, advocating for inclusivity beyond legislation.

</summary>

"To change society, you don't have to change the law."
"We don't have to wait on Capitol Hill."
"We have to send the message as individuals."
"You have to change the way people think."
"Because it will become the norm without the legislation."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of the Equality Act and its purpose to extend basic protections to marginalized groups in the United States.
Notes that the Equality Act, having passed in the House, faces a challenge in the Senate where it is likely to be filibustered.
Explains that Democrats need 60 votes to pass the Equality Act in the Senate, which may not happen.
Comments on how Republicans have weaponized the Equality Act as a talking point to rally their base against marginalized groups.
Recalls a scene from 1969 related to desegregation of pools as an example of societal change not just through laws but also by changing people's mindset.
Urges individuals to take action in their daily lives to create a more inclusive and accommodating environment for marginalized communities.
Encourages activities like advocating for inclusive hiring practices to make society more accepting without solely relying on legislation.
Stresses the importance of individuals sending messages, normalizing inclusivity, and breaking stigmas to bring about societal change.
Asserts that changing societal norms doesn't always require changing laws but focuses on changing people's perspectives.
Concludes by affirming that individuals can drive change and create a more inclusive society without solely depending on legislative actions.

Actions:

for advocates, activists, citizens,
Advocate for inclusive hiring practices (implied)
Engage in activities that foster a more accepting environment (implied)
Send messages of inclusivity in daily life (implied)
Normalize inclusivity and break stigmas (implied)
</details>
<details>
<summary>
2021-02-26: Let's talk about Biden's response in the Middle East.... (<a href="https://youtube.com/watch?v=QbxItByHILo">watch</a> || <a href="/videos/2021/02/26/Lets_talk_about_Biden_s_response_in_the_Middle_East">transcript &amp; editable summary</a>)

Beau breaks down the unconfirmed report of a strike in Syria by the Biden administration, analyzing its implications on foreign policy and the necessity of proportional response to Iranian-backed non-state actors.

</summary>

"Before you support the troops, you need to support the troops."
"It was possible to send the message through the talks to say, hey, if y'all are doing this, we have to freeze the talks."
"Look, they're hitting us, and Biden is so weak, he didn't even respond."

### AI summary (High error rate! Edit errors on video page)

Beau provides an overview of breaking news regarding an unconfirmed report of the Biden administration authorizing a strike against Iranian-backed non-state actors in Syria.
He explains the potential impact on Biden's foreign policy, especially with regards to Iran's involvement through non-state actors.
Non-state actors are discussed as tools used by Iran to apply pressure indirectly, allowing plausible deniability by the government in Tehran.
Beau analyzes the political necessity for Biden to respond to such actions, balancing the need to avoid appearing weak both domestically and internationally.
Comparisons are drawn to past American responses to similar situations involving non-state actors and sponsored governments.
The decision to target the non-state actors instead of the sponsored government is seen as a message from Biden that proportional action will be taken without escalating the situation.
Beau questions the necessity of the response and whether it could have been handled through diplomatic talks without military action.
Politically, responding was deemed necessary to avoid criticism and potential sabotage of Biden's foreign policy plans regarding Iran.
The response is viewed as a calculated move to avoid escalation and maintain control over the situation without major repercussions.
Beau ends by cautioning against blindly supporting military actions before fully understanding the situation and advocating for informed decision-making.

Actions:

for foreign policy analysts,
Monitor diplomatic talks for potential escalations (implied)
Advocate for informed decision-making in response to military actions (implied)
</details>
<details>
<summary>
2021-02-25: Let's talk about a lesson from Los Angeles for the whole country.... (<a href="https://youtube.com/watch?v=m2YJB6blPA8">watch</a> || <a href="/videos/2021/02/25/Lets_talk_about_a_lesson_from_Los_Angeles_for_the_whole_country">transcript &amp; editable summary</a>)

Los Angeles' historical lesson warns against decisions driven by fear, urging thoughtful discourse over emotion-led reactions in the country.

</summary>

"This country is being run by fear."
"The average citizen is overcome with fear because the leadership knows that people who are fearful will always need a leader."
"We have a whole lot of discussions going on in this country that are base emotion reactions."
"The country is being run by fear, not by policy, not by thought, but by fear."
"They literally created an enemy out of thin air."

### AI summary (High error rate! Edit errors on video page)

Los Angeles has a lesson for the entire country stemming from historical events in 1942, shortly after Pearl Harbor.
The west coast was on edge, taking preparations and making sure they were ready for a supposed Japanese attack.
A well-known animation studio in Burbank had troops stationed to prevent it from falling into enemy hands.
Despite being told an attack was imminent, it didn't happen on the predicted date of February 24th.
In the early hours of February 25th, the skies over LA lit up with anti-aircraft fire, but no Japanese attack occurred.
The hysteria and fear created from prolonged propaganda led to shooting at a weather balloon and false alarms.
The US is currently experiencing similar fear-induced reactions, leading to wild theories, demonization of marginalized groups, and exaggerated threats at the southern border.
Fear is driving the country, not rational policy or thought, as fear makes people reliant on leadership for protection.
Many national debates are driven by emotions like fear rather than rational ideas, leading to harmful outcomes.
The country is urged to move away from making decisions based solely on fear and embrace thoughtful discourse instead.

Actions:

for citizens,
Challenge fear-based narratives and seek rational thought in decision-making (implied).
Encourage and participate in thoughtful, fact-based debates and dialogues within communities (implied).
</details>
<details>
<summary>
2021-02-25: Let's talk about a better plan for the teens at the border.... (<a href="https://youtube.com/watch?v=HY9GkB8VUIM">watch</a> || <a href="/videos/2021/02/25/Lets_talk_about_a_better_plan_for_the_teens_at_the_border">transcript &amp; editable summary</a>)

Beau advocates for humane solutions to housing children at the border, criticizing the current fear-driven system and proposing alternatives like Hilton hotels, boarding schools, and sponsorship/fostering.

</summary>

"We can come up with a better plan. We can do better."
"It's cheaper to be a good person."
"There's no reason to detain them. There's no reason to put them in camps."
"We just have to get over that fear."
"We can do better."

### AI summary (High error rate! Edit errors on video page)

Proposes better solutions for housing children instead of locking them up in camps.
Advocates for understanding resources available and coming up with a more humane plan.
Suggests processing unaccompanied teens quickly and efficiently at the border.
Emphasizes the need to differentiate between asylum seekers and high-initiative individuals seeking a better life.
Presents three housing options: Hilton hotels, boarding schools, and sponsorship/fostering.
Points out the moral and practical flaws in the current system.
Addresses potential objections and offers solutions to ensure the well-being and safety of the children.
Critiques the current system as driven by fear and habit rather than logic or compassion.

Actions:

for policy makers, activists, community leaders,
Contact local embassies and offer support in processing unaccompanied teens at the border (suggested)
Reach out to boarding schools to inquire about accommodating high-initiative individuals seeking asylum (suggested)
Advocate for sponsoring or fostering unaccompanied teens in your community (suggested)
</details>
<details>
<summary>
2021-02-24: Let's talk about Trump and Miller coming back this week.... (<a href="https://youtube.com/watch?v=vbpWShYAZVY">watch</a> || <a href="/videos/2021/02/24/Lets_talk_about_Trump_and_Miller_coming_back_this_week">transcript &amp; editable summary</a>)

Former President Trump's upcoming speech targets Biden's immigration plan to scare Republicans into obedience with Stephen Miller's talking points, portraying immigrants as threats to rally the base.

</summary>

"Immigrants, particularly refugees and asylum seekers, are not scary. They're not coming here to do anything wrong to you. They are coming here because they want to get somewhere safe."
"Those in the House and the Senate don't have any backbone."
"The whole goal of this is to scare people."

### AI summary (High error rate! Edit errors on video page)

Former President Trump's upcoming speech will target Biden's immigration plan to keep his base energized by terrifying them.
Stephen Miller is providing talking points to Republicans to ensure they hold the line and obey.
Echoing these talking points means individuals are further to the right and not true Republicans or conservatives.
The talking points likely include blaming Biden for the influx of people at the southern border, which is actually a result of Trump's failed policies.
Contrasting Trump's treatment of immigrants with Biden's approach to unaccompanied minors reveals stark differences in policy and intention.
Biden's plan involves a transition for minors to the Office of Refugee Resettlement, aiming to unite them with family members in the US.
The talking point about Biden planning a path to citizenship being amnesty is countered by the fact that it creates a legal mechanism for immigration.
The main goal of these talking points is to scare people, portraying immigrants, refugees, and asylum seekers as threats when they are seeking safety.
Beau questions the influence of a twice-impeached former president over the Republican Party and criticizes the lack of backbone in current party leadership.

Actions:

for politically engaged individuals,
Reach out to local immigrant rights organizations to see how you can support refugees and asylum seekers (implied)
Advocate for humane immigration policies in your community and beyond (implied)
</details>
<details>
<summary>
2021-02-24: Let's talk about Neera Tanden and the status quo.... (<a href="https://youtube.com/watch?v=ObYW39QAHEw">watch</a> || <a href="/videos/2021/02/24/Lets_talk_about_Neera_Tanden_and_the_status_quo">transcript &amp; editable summary</a>)

Beau breaks down why Neera Tanden's nomination and policies are not surprising under Biden's administration.

</summary>

"Dog bites man. This is exactly what was promised. It's exactly what was delivered."
"There's not really a story here. This is who she is."
"You're not going to get deep systemic change on this front from Joe Biden."

### AI summary (High error rate! Edit errors on video page)

Neera Tanden is Biden's nominee to run the budget and executive branch.
Tanden faces criticism from all sides, including the far right and Republicans for her Twitter behavior.
The left dislikes Tanden for being status quo and against Medicare for all.
Biden promised no fundamental change in the economy, which Tanden represents.
Beau doesn't see the need to talk about Tanden as her positions are well known.
Biden's economic policies are not expected to be super progressive.
Beau believes whoever comes after Tanden will be similar in policy stance.
The lack of deep systemic change under Biden is not surprising.
Beau suggests there's no real story with Tanden as her positions are in line with Biden's promises.
Deep systemic change is unlikely in the economy under Biden.

Actions:

for policymakers, political analysts,
Advocate for deep systemic change in economic policies (implied)
Stay informed on political appointments and policies (implied)
</details>
<details>
<summary>
2021-02-24: Let's talk about Biden still sending people home.... (<a href="https://youtube.com/watch?v=PdNfnsMdDZ4">watch</a> || <a href="/videos/2021/02/24/Lets_talk_about_Biden_still_sending_people_home">transcript &amp; editable summary</a>)

Biden's immigration policies prioritize national security risks, serious criminal offenders, and individuals arriving after November 2020, aiming to provide a pathway to citizenship while maintaining humane treatment and just designation of asylum seekers.

</summary>

"We need to focus on their treatment and whether or not people are being designated as asylum seekers properly, that's more important."
"The immigration bill needs to get through. If this window of time did not exist, it would torpedo it."
"We need to make sure that people are treated humanely, that we are recognizing asylum seekers, that immigration has guidelines."
"This immigration bill is a good start. It's not what I want."

### AI summary (High error rate! Edit errors on video page)

Biden's immigration policies are causing confusion as some individuals are still being sent back despite promises not to do so.
There are three categories of individuals prioritized for deportation under Biden's policies.
The first category includes national security risks such as foreign spies and members of transnational groups intending harm.
The second category consists of individuals who have committed serious criminal violations, not just any felony but a serious one.
Biden did not cancel Operation Talon but actually made the subject group the number two priority for ICE.
The third category comprises individuals who arrived after November 1, 2020, without permission.
The specific dates, November 1, 2020, and January 2021, play a significant role in eligibility for citizenship pathways.
Biden's immigration bill aims to provide a pathway to citizenship for those living in constant fear for years.
The window between November 2020 and January 2021 prevents accusations of encouraging illegal immigration by setting eligibility criteria.
Beau stresses the importance of focusing on the humane treatment of individuals and proper designation of asylum seekers.
Recognizing asylum seekers, maintaining guidelines for immigration processes, and ensuring humane treatment should be top priorities.
The immigration bill is a step towards a future where all people are treated equally, regardless of borders.
Beau advocates for recognizing the big picture and the necessity of treating individuals humanely and justly in the immigration process.

Actions:

for advocates, immigration activists,
Contact local organizations supporting asylum seekers and refugees to offer assistance and support (suggested)
Join community efforts to ensure humane treatment and proper designation of asylum seekers in the immigration process (implied)
Advocate for fair immigration policies and guidelines within your community and beyond (exemplified)
</details>
<details>
<summary>
2021-02-23: Let's talk about Biden's first real foreign policy test in Iran.... (<a href="https://youtube.com/watch?v=cUeQCyhIukE">watch</a> || <a href="/videos/2021/02/23/Lets_talk_about_Biden_s_first_real_foreign_policy_test_in_Iran">transcript &amp; editable summary</a>)

Beau analyzes Biden's first foreign policy test with Iran, focusing on the challenge of achieving a politically acceptable deal while navigating regional power dynamics and avoiding the appearance of weakness.

</summary>

"The deal has to be politically tenable for everybody involved so they can take it back home and be okay with it."
"Both countries are in the position where they want this deal. Iran wants the deal. The United States wants the deal."
"There are probably going to be phases in this deal-making process."
"He has chosen, President Biden has chosen, a mountain of a goal for his first foreign policy test."

### AI summary (High error rate! Edit errors on video page)

Beau analyzes Biden's first real foreign policy test concerning Iran and the nuclear deal.
The goal is to bring Iran out of isolation and into the international community to stabilize the Middle East.
Various moving parts include following the Shia-Sunni divide, curbing non-state actors, prisoner exchanges, and their relationship with other countries.
The deal must be politically acceptable to all factions in Iran to be successful.
Iran's regional power stems from standing up to the U.S. for 40 years, making them a significant player.
Both Iran and the U.S. want the deal but cannot appear weak in negotiations.
Biden's foreign policy team is strong, but a comprehensive deal upfront is unlikely; smaller deals may pave the way.
The process of reaching a deal with Iran could span years, with phases and side deals likely.

Actions:

for policy analysts,
Reach out to local policymakers to advocate for diplomatic solutions with Iran (implied).
Stay informed about international relations and foreign policy developments to understand the nuances of negotiations (implied).
</details>
<details>
<summary>
2021-02-22: Let's talk about grading Biden's response to Texas.... (<a href="https://youtube.com/watch?v=03hmEJyIXSY">watch</a> || <a href="/videos/2021/02/22/Lets_talk_about_grading_Biden_s_response_to_Texas">transcript &amp; editable summary</a>)

President Biden's proactive disaster response in Texas surpasses expectations, despite facing criticism for not visiting, diverting resources needed on the ground.

</summary>

"From an emergency management standpoint, he should not go down there."
"A major disaster declaration literally means that the situation is beyond the capabilities of the state and local officials."
"He is well beyond any expectations of any recent president."

### AI summary (High error rate! Edit errors on video page)

President Biden is facing criticism for his response to the situation in Texas.
There are calls for Biden to visit Texas, but from an emergency management standpoint, it's not recommended.
Biden's decision to not visit Texas is the right move to prevent diverting emergency resources.
Criticism towards Biden includes issuing a major disaster declaration after the situation escalated.
The emergency declaration was issued before the crisis unfolded, indicating Biden's proactive approach.
The disaster in Texas was preventable and foreseeable, leading to a situation beyond the state's capabilities.
Biden's response to the disaster is generally seen as effective compared to previous presidents.
While improvements could be made, Biden's actions have surpassed expectations in emergency management.

Actions:

for emergency management officials,
Prepare emergency resources and aid on the ground (implied)
</details>
<details>
<summary>
2021-02-22: Let's talk about education and national security.... (<a href="https://youtube.com/watch?v=KDwUkHILm50">watch</a> || <a href="/videos/2021/02/22/Lets_talk_about_education_and_national_security">transcript &amp; editable summary</a>)

Beau stresses the critical importance of education in national security, advocating for equitable funding and recognition of teachers as elite educators who weaponize knowledge for societal benefit.

</summary>

"Education is critical to national security and it needs to be treated like that."
"The most elite unit in the US military are teachers."
"They found a way to weaponize education."
"It needs to be funded like that."
"Education is fundamental to developing technology, skills, and infrastructure necessary for national defense."

### AI summary (High error rate! Edit errors on video page)

Scott Adams' tweet sparked a discourse on education's importance, drawing parallels between teachers and military personnel.
Questions raised include why teachers don't have similar funding and benefits as the military.
Beau advocates for investing in education akin to the military, citing its critical role in national security.
Education is fundamental to developing technology, skills, and infrastructure necessary for national defense.
Beau contrasts military recruiters with teachers, noting the latter's pivotal role in educating and training personnel.
The US Army Special Forces are portrayed as elite teachers who weaponize education to quickly field capable military forces.
Green Berets' reputation stems from their ability to raise armies through education, not just combat prowess.
Beau stresses the need to prioritize and fund education for national security and its broader societal impact.

Actions:

for teachers, policymakers, activists,
Advocate for increased funding and recognition of teachers as critical to national security (implied)
Support initiatives that prioritize investing in education similar to the military (implied)
</details>
<details>
<summary>
2021-02-22: Let's talk about Republican math and Trump math.... (<a href="https://youtube.com/watch?v=2DQu1Cjbgc8">watch</a> || <a href="/videos/2021/02/22/Lets_talk_about_Republican_math_and_Trump_math">transcript &amp; editable summary</a>)

Republican Party's survival hinges on distancing itself from Trump and his policies, as continuing to cater to him guarantees losses in future elections.

</summary>

"Less than half of people who voted for him in November still support him."
"The Republican Party's only chance for any kind of victory is to begin the process of detropification."

### AI summary (High error rate! Edit errors on video page)

Republican math, Trump math, and the misconceptions surrounding a headline about almost half of Republicans voting for Trump third party in 2024.
The survey actually shows that less than half of the people who voted for Trump in November still support him, indicating a significant drop in support.
The 100% of voters who supported Trump in November wasn't enough for him to win the election, so having less support now is a clear indication of losing.
Many of Trump's ardent supporters are aging, raising doubts about their participation in the 2024 election.
The Republican Party's reluctance to see the reality of Trump's diminishing support and the potential consequences of continuing to cater to him.
The Republican Party's only chance for victory is to distance themselves from Trump, his rhetoric, and his enablers through a process of detropification.
Criticizing the Republican Party for not taking a proactive stance in denouncing Trump's behavior, rhetoric, and policies, which could lead to their downfall in upcoming elections.

Actions:

for republicans, political analysts,
Begin the process of detropification within the Republican Party (implied).
Actively condemn Trump's behavior, rhetoric, and policies within the party (implied).
</details>
<details>
<summary>
2021-02-21: Let's talk about when you can get your vaccine.... (<a href="https://youtube.com/watch?v=UAGFRLX6pAw">watch</a> || <a href="/videos/2021/02/21/Lets_talk_about_when_you_can_get_your_vaccine">transcript &amp; editable summary</a>)

Beau provides a COVID-19 vaccine timeline update, expressing skepticism about its cautious nature but stressing the importance of continued safety measures until widespread vaccination is achieved.

</summary>

"I think they've gone the other way with it. I think they're being overly cautious."
"If we relax, every transmission is another chance for mutation."
"Hang in there. And eventually we will get through this."

### AI summary (High error rate! Edit errors on video page)

Provides an update on the COVID-19 vaccine timeline, reflecting on the initial ambitious projections.
Mentions the logistical challenges of distributing the vaccine to millions of people.
Indicates a shift in the timeline, with priority groups like older individuals and healthcare workers being first in line.
Estimates that open season for vaccines, available to all, may occur by July.
Expresses personal skepticism about the cautious timeline, believing it could potentially be expedited.
Emphasizes the importance of continued safety measures like handwashing, mask-wearing, and social distancing to prevent virus transmission and mutations.
Encourages resilience in dealing with pandemic fatigue and staying vigilant until widespread vaccination is achieved.
Concludes with a message of hope and perseverance through the challenging times.

Actions:

for public health advocates,
Follow safety protocols: Wash hands, wear masks, and practice social distancing (implied)
Stay informed about vaccine rollout in your area and prioritize getting vaccinated (implied)
</details>
<details>
<summary>
2021-02-21: Let's talk about a forecast for the GOP.... (<a href="https://youtube.com/watch?v=wO_dAFmnj2I">watch</a> || <a href="/videos/2021/02/21/Lets_talk_about_a_forecast_for_the_GOP">transcript &amp; editable summary</a>)

A state senator's departure from the GOP signals the need for de-Trumpification to avoid continuous defeats and internal strife, possibly leading to Democratic victories in upcoming midterms.

</summary>

"It has become a party of personality rather than a party of principle."
"You can't win a primary without Trump's support but you can't win a general election with it."
"The Republican Party has no choice but to engage in de-Trumpification."
"Even though the election just happened, the campaigning for the midterms, that's going to start real soon."
"Concerns over what to tell future generations about being part of a party under Trump's influence."

### AI summary (High error rate! Edit errors on video page)

A state senator in Arkansas left the GOP and became independent, criticizing the Republican Party's shift towards personality over principle.
Senator Jim Hendren pointed out that Trump's support is necessary to win primaries, but detrimental in general elections.
Trump's polarizing presence in the Republican Party has driven away a significant portion of the conservative movement.
The forecast suggests that the Republican Party must undergo de-Trumpification to avoid a series of defeats.
Failure to disassociate from Trump could lead to decreased campaign contributions and a losing party image.
Concerns over what to tell future generations about being part of a party under Trump's influence influenced Hendren's decision to leave.
Hendren's insights, as an insider, indicate ongoing internal struggles within the GOP and potential Democratic victories in the midterms.
The party's ability to de-radicalize Trump supporters will be a critical factor in its future success.
Hendren's departure from the party signifies a broader issue beyond Arkansas, reflecting a national trend within the Republican Party.
The upcoming midterms may see significant Democratic gains if internal GOP conflicts persist.

Actions:

for gop members, political analysts,
Engage in de-Trumpification within the Republican Party to avoid future defeats (implied).
Stay informed about internal struggles within the GOP and their implications for future elections (implied).
Prepare for potential Democratic victories in the upcoming midterms by monitoring GOP developments (implied).
</details>
<details>
<summary>
2021-02-20: Let's talk about cloning condors and black-footed ferrets.... (<a href="https://youtube.com/watch?v=mgMuZh9G_kM">watch</a> || <a href="/videos/2021/02/20/Lets_talk_about_cloning_condors_and_black-footed_ferrets">transcript &amp; editable summary</a>)

Beau questions why ethical dilemmas arise only in species preservation, urging a shift to prioritize preserving life over bottom lines.

</summary>

"I think it's a test run because there are obviously going to be ethical objections to this."
"The preservation of life, not the preservation of bottom lines."
"Earth is going to be fine. It may go on without us, though."
"The primary ethical responsibility should be the preservation of life."

### AI summary (High error rate! Edit errors on video page)

Condors are facing extinction, with a black-footed ferret named Elizabeth Ann born in December to aid in their preservation.
Elizabeth Ann was created from frozen cells of another ferret named Willa from the late 1900s.
The black-footed ferret species was declared extinct in 1979 but was rediscovered on a ranch in Wyoming.
The breeding program for black-footed ferrets has been successful, but all existing ferrets trace back to only seven individuals.
Elizabeth Ann and others like her aim to increase genetic diversity, which is vital for species survival.
Some ferrets have already been vaccinated, indicating a serious commitment to saving the species.
Saving species like black-footed ferrets raises ethical dilemmas, sparking a debate on the intervention's morality.
Beau questions why ethical concerns arise only when saving species or mitigating damage, not during extinction events.
The primary ethical responsibility should be preserving life on Earth for all beings, not just focusing on financial interests.
Beau urges a shift from prioritizing bottom lines to prioritizing life preservation, as it is vital for the planet's future.

Actions:

for conservationists, environmentalists,
Support conservation efforts for endangered species (implied)
Advocate for ethical considerations in species preservation (implied)
</details>
<details>
<summary>
2021-02-20: Let's talk about Texas tough guys, Beto, and AOC.... (<a href="https://youtube.com/watch?v=7CGyBYDo48Q">watch</a> || <a href="/videos/2021/02/20/Lets_talk_about_Texas_tough_guys_Beto_and_AOC">transcript &amp; editable summary</a>)

The most critical lesson from Texas: Tough leadership matters more than tough talk in times of crisis.

</summary>

"When those who cast that tough guy image couldn't just bark orders into a phone, they had no clue what to do."
"Those are the ones who came through. Those are the ones who stepped up."
"Sometimes the tough guy doesn't wear cowboy boots and a white hat. Sometimes she wears high heels."

### AI summary (High error rate! Edit errors on video page)

The most critical lesson from the Texas crisis is not about winterizing equipment but about tough leadership.
Winning elections in Texas requires projecting a tough image that voters seek for protection.
People in Texas value the image of a reliable protector during tough times.
Leaders who flee when faced with challenges, like the recent power outage, betray that tough image.
Beto O'Rourke, often mocked for being weak, stayed to organize supplies and make calls during the crisis.
A liberal woman from New York raised two million dollars for Texas relief efforts.
Criticisms of liberals throwing money at problems are misguided when lack of resources is the issue.
Leaders who can only operate with government support falter in crisis situations.
Those who stepped up to help during the Texas crisis were often not the tough-talking leaders.
Supporting selfish leaders leads to selfish governance, not the tough leadership Texans desire.

Actions:

for texans, voters, activists,
Support community leaders who step up in times of crisis (exemplified)
Donate to relief efforts for areas facing resource shortages (exemplified)
</details>
<details>
<summary>
2021-02-19: Let's talk about rewriting the Constitution and Rep Boebert.... (<a href="https://youtube.com/watch?v=IZeAidVNseg">watch</a> || <a href="/videos/2021/02/19/Lets_talk_about_rewriting_the_Constitution_and_Rep_Boebert">transcript &amp; editable summary</a>)

Beau addresses the misconception surrounding the infallibility of the U.S. Constitution, advocating for a realistic understanding of its amendable nature and the importance of embracing change.

</summary>

"The Constitution of the United States is not infallible."
"The United States Constitution was designed to be a living document."
"It's a document. It's a contract with a process that allows it to be amended."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the topic of rewriting the Constitution, specifically the parts that individuals may not like.
A tweet from Representative Lauren Boebert sparked Beau's commentary, questioning the idea of protecting and defending the Constitution without considering its amendment process.
Beau explains the four ways outlined in Article 5 of the Constitution to rewrite parts that are disliked.
Beau notes that historically, successful attempts at amending the Constitution have originated in Congress rather than through a convention method.
The Constitution is portrayed as a living document designed to be changed as societal views evolve, not as a static entity holding the country back.
Beau expresses concern over the dangerous mythologizing of American history and the Constitution, advocating for a realistic understanding of its amendable nature.

Actions:

for citizens, policymakers,
Understand the process outlined in Article 5 of the Constitution for amending parts that are disliked (exemplified)
Advocate for a realistic understanding of the Constitution as a living document that can be changed (suggested)
</details>
<details>
<summary>
2021-02-19: Let's talk about DOD going back over there.... (<a href="https://youtube.com/watch?v=Vl27mVPsHtQ">watch</a> || <a href="/videos/2021/02/19/Lets_talk_about_DOD_going_back_over_there">transcript &amp; editable summary</a>)

Beau questions the strategy of sending US troops to Iraq, advocating for cost-effective training methods and cautioning against imperialism in resisting organic changes in the Middle East.

</summary>

"War is a continuation of politics by other means."
"Perhaps sending U.S. troops there to Iraq to basically act as a lightning rod for every non-state actor in the region isn't a good idea."
"And us trying to stop that is imperialism."

### AI summary (High error rate! Edit errors on video page)

Department of Defense open to sending troops to Iraq to support NATO's training mission for Iraqi national forces.
Questions the strategy of treating Iraq like irregular indigenous forces rather than bringing their instructors to a different location for training.
Suggests bringing Iraqi forces to the US for training is more cost-effective and strategically sound.
Raises concerns about sending US troops to Iraq acting as a lightning rod for non-state actors in the region.
Argues that having European or US forces in Iraq can give the appearance of propping up the Iraqi government as a puppet.
Stresses the importance of treating Iraq as an equal to stabilize its government and avoid entanglement in Middle Eastern conflicts.
Points out that changes in the Middle East, including border redrawings, are necessary due to colonial legacy and internal strife.
Supports organic changes in the Middle East and cautions against US imperialism in resisting border redrawings by regional nations.

Actions:

for policy analysts, military strategists,
Support organic changes in the Middle East by advocating for respect of regional nations' decisions on border redrawings (implied).
Advocate for cost-effective and strategic training methods for Iraqi national forces, such as bringing them to the US for training (implied).
</details>
<details>
<summary>
2021-02-18: Let's talk about minimum wages and more stimulus.... (<a href="https://youtube.com/watch?v=aEwtGgWFxHA">watch</a> || <a href="/videos/2021/02/18/Lets_talk_about_minimum_wages_and_more_stimulus">transcript &amp; editable summary</a>)

Walmart aims to raise average wage to $15 but faces backlash for not increasing the minimum wage, prompting debates on corporate responsibility and worker benefits.

</summary>

"If you can't pay a living wage, the company has no right to exist in the United States."
"It's to benefit the worker, not the corporations."
"Walmart wants the government to pick up the slack."
"They have to make money."
"I wish my earnings increased by only 69%."

### AI summary (High error rate! Edit errors on video page)

Walmart is keeping its starting wage at $11 an hour, but aiming to increase the average wage to $15 an hour.
The company's revenue grew by 7.3% last year, with e-commerce growing by 69%.
Despite the revenue growth, Walmart's stock prices fell, which was seen as bad news on Wall Street.
Walmart stated that raising the minimum wage to $15 an hour should benefit the economy, but Beau disagrees.
Beau argues that the minimum wage is meant to benefit the worker, not the corporations.
Walmart's CEO, McMillan, acknowledged that customers spend more when they receive stimulus money, benefitting the economy.
Walmart wants the government to provide stimulus while not committing to raise their minimum wage.
Beau believes that if a company cannot pay a living wage, it has no right to exist in the United States.

Actions:

for workers, advocates,
Advocate for fair wages in your workplace (implied)
Support policies that prioritize worker well-being over corporate profits (implied)
Stay informed and engaged in the debate on minimum wage increases (implied)
</details>
<details>
<summary>
2021-02-18: Let's talk about Biden's judges and blue slips.... (<a href="https://youtube.com/watch?v=S_NEiCeHtCY">watch</a> || <a href="/videos/2021/02/18/Lets_talk_about_Biden_s_judges_and_blue_slips">transcript &amp; editable summary</a>)

Beau explains the implications of the blue slip process elimination for Biden's judges, revealing potential obstacles and political dynamics in judicial nominations.

</summary>

"It's not required, but they'll probably say it is."
"What should be and what is are very, very, very different."
"So as far as getting rid of it altogether, yeah, I think it's a good idea."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the three kinds of judges in the federal judicial system: district court, circuit court (appeals), and the Supreme Court.
Describes the blue slip rule that allowed senators to have a say in judicial nominations, and how it was eliminated for circuit court judges in 2017.
States that Democrats, contrary to expectations, are not planning to restore the blue slip process for district court judges.
Predicts that Republicans may criticize this decision, claiming victimhood and unconstitutionality.
Clarifies that the blue slip process is not a constitutional requirement but a Senate rule.
Suggests that Biden's judges may have an easier confirmation process without the blue slip process in place.
Examines the idealistic purpose of the blue slip process in promoting regional representation but points out its potential for misuse.
Supports the idea of getting rid of the blue slip process altogether for a more uniform judicial selection process.
Expresses uncertainty about the Democratic Party's willingness to push for the abolition of the blue slip process.
Concludes by noting that circuit court judges are likely to face fewer obstacles compared to district court judges.

Actions:

for political enthusiasts, activists,
Contact your representatives to express your views on judicial nominations (implied)
Stay informed about changes in the judicial nomination process and advocate for fair and transparent procedures (implied)
</details>
<details>
<summary>
2021-02-17: Let's talk about windmills in Texas and cars on Mars.... (<a href="https://youtube.com/watch?v=qLTKHdnoptM">watch</a> || <a href="/videos/2021/02/17/Lets_talk_about_windmills_in_Texas_and_cars_on_Mars">transcript &amp; editable summary</a>)

Beau contrasts stone age tech in Texas with Mars rover, debunks windmill failure myths, and questions prioritizing profit over people in energy transition.

</summary>

"I can't believe that these pundits and these politicians would say that we can't make it work like they're doubting the ingenuity of the American worker."
"Those are the options. A disaster film or Star Trek."
"Blame it on that. Maybe that'll even upset the plans to transition our energy."
"It's a way to continue to put profits over people."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Describes a stone age technology as a possible survival method in Texas due to power grid failure.
Contrasts the use of stone age technology in Texas with NASA's advanced mission to Mars.
Points out that Iowa successfully uses wind power for 40% of their electricity and had less issues during the cold weather due to winterization of equipment.
Mentions that natural gas failures were more significant than windmill failures in Iowa.
ERCOT confirmed windmill failure was the least significant factor in the Texas power issues.
Questions the motives of politicians spreading misinformation about windmill failures.
Expresses disbelief in doubting the American worker's ingenuity and patriotism regarding transitioning energy sources.
Compares the future options of prioritizing profit over people in Texas or embracing progress like the Mars mission.
Shares temperature comparison between Houston and Des Moines at the time of filming.

Actions:

for texans, environmentalists, policymakers,
Winterize energy equipment (suggested)
Support transitioning to sustainable energy sources (exemplified)
</details>
<details>
<summary>
2021-02-17: Let's talk about what to do in Texas.... (<a href="https://youtube.com/watch?v=XtFwAR9182M">watch</a> || <a href="/videos/2021/02/17/Lets_talk_about_what_to_do_in_Texas">transcript &amp; editable summary</a>)

Beau addresses the lack of government response in Texas, provides practical tips for staying warm without electricity or water, criticizes a heartless social media post by a former Texas mayor, and urges Texans to take care of each other.

</summary>

"Sink or swim, it's your choice."
"Only the strong will survive and the weak will perish."
"Quit crying and looking for a handout. Get off your rear and take care of your own family."
"Y'all have this. Y'all will be all right."
"Even in Florida, we get little flyers, notices from the state government, telling us about what to do to prepare for a hurricane."

### AI summary (High error rate! Edit errors on video page)

Addresses the lack of government response in Texas during a crisis.
Provides practical tips for staying warm without electricity or water.
Encourages using tents, blankets, stones, and candles to trap heat.
Advises dressing in layers, staying close together, and sealing off areas to retain heat.
Criticizes a former Texas mayor for a heartless social media post about self-reliance during the crisis.
Emphasizes the importance of community support and leadership during tough times.
Criticizes the government in Texas for failing to adequately prepare for the crisis.
Urges Texans to take care of each other and make necessary changes for the future.

Actions:

for texans,
Use tents, blankets, stones, and candles to trap heat (exemplified).
Dress in layers, stay close together, and seal off areas to retain heat (exemplified).
Take care of your own family and community during tough times (implied).
</details>
<details>
<summary>
2021-02-17: Let's talk about Trump's love letter to McConnell... (<a href="https://youtube.com/watch?v=itxlRvrNorA">watch</a> || <a href="/videos/2021/02/17/Lets_talk_about_Trump_s_love_letter_to_McConnell">transcript &amp; editable summary</a>)

Beau delves into Trump's letter to McConnell, criticizing the lack of progressive solutions and warning about the electoral consequences of Trump's endorsements.

</summary>

"Trump's policies are regressive. They're going backwards."
"Trump's endorsement on the national level for anybody that's going to have to compete for the Electoral College is a guarantee that they will lose."
"Not the guy writing angry letters to the editor from Mar-a-Lago."
"I wouldn’t want Trump's endorsement if I was a Republican because long term it is going to do more damage."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Exploring the love letter Senator McConnell received from former President Trump.
Trump's letter aims to convey that everything is fine and he is not upset.
Trump criticizes McConnell, stating that the Republican Party cannot be respected with McConnell at its helm.
Trump believes McConnell's leadership is driving the party towards weakness.
Trump's letter portrays his belief that McConnell's strategies are ineffective and regressive.
Despite some truths in Trump's criticisms of McConnell, Trump fails to offer progressive solutions.
Trump describes McConnell as a status quo person who lacks the vision for progress.
Trump predicts that Republicans will not win if they continue to support McConnell.
Trump's endorsement on a national level could lead to electoral losses due to his own electoral defeat.
Beau points out the irony of Trump calling McConnell a loser despite Trump's own failure to secure a second term.

Actions:

for political observers,
Analyze political candidates beyond endorsements for a thorough understanding (implied)
</details>
<details>
<summary>
2021-02-16: Let's talk about Texas freezing and what it tells us.... (<a href="https://youtube.com/watch?v=hNyRL_xFWoU">watch</a> || <a href="/videos/2021/02/16/Lets_talk_about_Texas_freezing_and_what_it_tells_us">transcript &amp; editable summary</a>)

Texas faces power outages due to lack of preparation and profit-driven decision-making, raising concerns about climate readiness and political influences.

</summary>

"Texas wasn't prepared. Texas didn't take the steps to mitigate this problem along the way."
"The dollar was more important. They decided, well, I mean, how many people can really freeze?"
"You have to build the arc before it rains type of thing."
"There's only one president that would say something like that."
"Your party, the party that likes to support large energy companies, they're the reason you're not going to watch this for a few days because you don't have power."

### AI summary (High error rate! Edit errors on video page)

Texas faced a crisis due to lack of preparation for severe weather, leading to power outages.
The state failed to take steps to mitigate the problem over the years, causing the current situation.
Private equity groups influenced decision-making by prioritizing profits over necessary changes.
Texas energy companies neglected winterizing equipment, despite being warned about the potential issue.
Beau questions the reliability of energy companies' long-term forecasts given their failure to prepare for current crises.
The importance of proactive measures to address climate change is emphasized.
Beau criticizes the lack of federal assistance for Texas due to its political leaning.
He points out the dangers of blindly following party lines without questioning harmful policies.
The narrative of prioritizing profits over people's well-being is a recurring theme in decision-making.
Beau urges Texans to be mindful of the need for resilient energy policies in the face of climate challenges.

Actions:

for texans, climate activists,
Advocate for resilient energy policies and climate preparedness (implied)
Question political decisions that prioritize profits over people's well-being (implied)
</details>
<details>
<summary>
2021-02-16: Let's talk about Biden, Haaland, and extra scrutiny.... (<a href="https://youtube.com/watch?v=nENnZa8tkBA">watch</a> || <a href="/videos/2021/02/16/Lets_talk_about_Biden_Haaland_and_extra_scrutiny">transcript &amp; editable summary</a>)

Beau scrutinizes Deb Haaland's appointment, finding no dirt and speculating on opposition motives.

</summary>

"I found nothing. Nothing. And understand, that's literally never happened before."
"If she does face the level of opposition I think she's going to, the same level of scrutiny that I put her through, I'm going to put them through."
"Those, however, who are just in it for themselves, to feather their own nest, to secure their own position, they're pretty easily influenced."
"We're going to look at their campaign contributions and see if it's really the Senators who oppose her or if it's the people who pay them."
"Anyway, it's just a thought. Have a good day."

### AI summary (High error rate! Edit errors on video page)

Examines Biden's potential appointee for Secretary of the Interior, Deb Haaland.
Republicans are gearing up to oppose Haaland, making her one of the most contested appointments.
Beau finds no corruption or scandals in Haaland's background, a departure from the norm.
Haaland's open-book transparency surprises Beau, who even reached out to journalists for dirt but found none.
Despite Haaland's qualifications and alignment with the Secretary of the Interior's duties, Republicans are already moving to oppose her.
Beau questions if the opposition stems from a fear that Haaland will take her role seriously in protecting the Interior.
Considers the difference between ideologically driven individuals like Haaland and those motivated by personal gain.
Beau hints at subjecting Haaland's opposition to the same level of scrutiny regarding campaign contributions.
Raises the point that opposition to Haaland might not be solely from senators but possibly influenced by financial backers.
Beau concludes with a thought on the potential motivations behind the opposition to Haaland.

Actions:

for political analysts, activists,
Investigate campaign contributions of senators opposing nominees (implied)
</details>
<details>
<summary>
2021-02-15: The roads to basic survival skills.... (<a href="https://youtube.com/watch?v=0DF56frxMAw">watch</a> || <a href="/videos/2021/02/15/The_roads_to_basic_survival_skills">transcript &amp; editable summary</a>)

Be prepared for emergencies with survival essentials like shelter, fire-starting tools, water purification methods, and practical skills.

</summary>

"One is none and two is one."
"Fire loves chaos."
"Knowledge weighs nothing."

### AI summary (High error rate! Edit errors on video page)

Beau delves into the topic of emergencies and survival skills.
He showcases his EDC kit, including essentials like a knife, striker, handsaw, and more.
Beau also demonstrates the contents of his bag, focusing on food, water, and tools for survival.
He examines pre-packed survival bags from stores, evaluating their completeness for a 72-hour survival period.
Beau explains the importance of redundancy and backups in survival gear.
The transcript covers setting up shelter, starting a fire, purifying water, and cooking food in emergency situations.
Keith shares his expertise on processing firewood and setting up a fire.
Beau walks through different methods of purifying water, including using LifeStraw, Sawyer Mini, and Aqua Tabs.
The importance of knives, first aid kits, shelter-building, and food choices in emergency preparedness is discussed.
Beau touches on the necessity of practical skills like snaring, fishing, and navigation in survival scenarios.

Actions:

for survivalists, outdoor enthusiasts,
Organize your own EDC kit with essentials for survival (suggested).
Practice setting up shelter and starting a fire in your local outdoor area (exemplified).
Learn different methods of water purification like using LifeStraw or Sawyer Mini (implied).
Familiarize yourself with practical skills like snaring and fishing for emergencies (suggested).
</details>
<details>
<summary>
2021-02-15: Let's talk about Shell and peak oil production.... (<a href="https://youtube.com/watch?v=PO0LuSWi2lM">watch</a> || <a href="/videos/2021/02/15/Lets_talk_about_Shell_and_peak_oil_production">transcript &amp; editable summary</a>)

According to Beau, Shell predicts peak oil already happened in 2019, aiming for net-zero emissions by 2050 with a plan that seems more like a rough sketch, potentially influencing other companies towards cleaner energy for financial gains.

</summary>

"Peak oil will occur in 2019. It already happened."
"The bad news is that their plan to achieve this status by 2050 is not really a plan."
"Did it happen soon enough?"
"This announcement by Shell may trigger the dominoes to start falling."
"Not because they care about the environment, but because it's going to be good for their pocketbooks."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of peak oil as the moment when oil production stops increasing and starts declining.
Shell, a major company, predicts that peak oil already happened in 2019, accelerated by public health concerns.
Shell plans to achieve net-zero carbon emissions by 2050, but the plan seems more like a rough sketch.
Despite investing in cleaner energies, Shell will increase production of liquefied natural gas.
Shell's announcement may influence other companies to follow suit, like GM's plan to phase out gas and diesel by 2035.
Raises the question of whether the current decline in oil production is sufficient to mitigate projected environmental issues.
Speculates that Shell's announcement could prompt others to take transitioning to cleaner energies more seriously due to financial incentives.

Actions:

for environment advocates, energy companies,
Invest in cleaner energies and reduce reliance on fossil fuels (exemplified)
</details>
<details>
<summary>
2021-02-15: Let's talk Lindsay Graham wanting to impeach Kamala Harris.... (<a href="https://youtube.com/watch?v=nPCJ7fHyC5o">watch</a> || <a href="/videos/2021/02/15/Lets_talk_Lindsay_Graham_wanting_to_impeach_Kamala_Harris">transcript &amp; editable summary</a>)

Senator Lindsey Graham's suggestion to impeach Vice President Harris for supporting bail funds is criticized as a partisan move that goes against constitutional principles, with Beau questioning the selective targeting based on race.

</summary>

"Please by all means have your lackeys in the House impeach the Vice President because she tweeted support for a constitutional premise."
"The majority of the United States sees right through it."
"Just the woman who isn't white."
"Play your silly little game."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Senator Lindsey Graham suggested impeaching Vice President Harris if Republicans retake the House in 2022, citing her support for bail funds as equivalent to the former president's actions.
He explains what bail funds are, accounts set up to help individuals post bail beyond their financial means so they can participate in their own defense.
Beau argues that denying bail should be reserved for genuine flight risks or threats to society, not for those with bail set beyond their financial reach.
He points out that excessive bail is against the Constitution, specifically the 8th Amendment, and supports Vice President Harris for backing a constitutional principle.
Beau criticizes the Republican Party for allegedly abandoning founding principles, coordinating with defense in trials, and ignoring impartiality oaths.
He challenges the potential impeachment of Vice President Harris as a partisan move that most Americans see through, suggesting it will only appeal to a small faction.
Beau questions the selective targeting of Vice President Harris for impeachment, a woman of color, compared to the lack of action against an older white male.
He concludes by expressing his thoughts and wishing everyone a good day.

Actions:

for voters, political activists,
Challenge partisan moves in politics (implied)
Support constitutional principles (implied)
</details>
<details>
<summary>
2021-02-13: Let's talk about what Republicans must know about the impeachment.... (<a href="https://youtube.com/watch?v=1s7lY8cgraE">watch</a> || <a href="/videos/2021/02/13/Lets_talk_about_what_Republicans_must_know_about_the_impeachment">transcript &amp; editable summary</a>)

Addressing the impeachment trial and Senate vote, Beau underscores the disparity between what should be impartial proceedings and what actually transpires, urging accountability based on constitutional principles.

</summary>

"Those who require your consent, your support, those who represent you are doing everything within their power to show you that they don't believe it matters at all."
"If you believe the Constitution matters, those who behaved in this manner cannot be reelected by you because your convictions, your beliefs are utterly worthless unless they motivate your actions."
"If you still get chills when you hear the star-spangled banner, if you think it's a travesty that people don't recite the Pledge of Allegiance, you cannot simply look the other way as those senators openly and flagrantly violate their oaths."

### AI summary (High error rate! Edit errors on video page)

Addressing the impeachment trial and the recent vote in the Senate.
Contrasting what the proceedings should be versus what they actually are.
Expressing concern over senators meeting with the defense and withholding information.
Pointing out that senators are not impartial as required by the Constitution.
Mentioning how senators may vote based on political interests rather than facts.
Urging average Republicans to take issue with their representatives' actions.
Emphasizing the importance of upholding the Constitution and the oath within it.
Calling for accountability by suggesting that senators violating their oath should be primaried.
Encouraging individuals to act on their beliefs and convictions.
Stressing that beliefs should translate into tangible actions for them to hold true value.

Actions:

for average republicans,
Hold senators accountable through primary elections (suggested)
Vote out senators who violated their oath (suggested)
</details>
<details>
<summary>
2021-02-13: Let's talk about how "canceling" isn't new.... (<a href="https://youtube.com/watch?v=xe16MdERJxw">watch</a> || <a href="/videos/2021/02/13/Lets_talk_about_how_canceling_isn_t_new">transcript &amp; editable summary</a>)

Beau outlines the historical context and economic motivations behind cancel culture in the US, debunking misconceptions about oppression and leftist plots.

</summary>

"It's just math."
"Who decides who gets canceled? Companies. Large corporations."
"They're obsolete viewpoints."
"It's not new. It just used to work in their favor."
"What they're complaining about is not some leftist plot. It is good old-fashioned American capitalism at work."

### AI summary (High error rate! Edit errors on video page)

Explains the phenomenon of cancel culture and how it's not a new concept in the United States.
Talks about how some people are now negatively impacted by cancel culture after benefiting from it for a long time.
Describes how those who have lost privilege due to cancel culture view it as oppression.
Attributes the impact of cancel culture to basic math and the shift in societal beliefs.
Points out that companies, driven by profit, are the ones making decisions on canceling individuals based on public statements.
Notes that older individuals with outdated viewpoints are becoming less valuable consumers for companies compared to younger, progressive individuals.
Mentions historical instances of pre-cancellation, such as discriminating against LGBTQ individuals in the workplace.
Addresses the misconception that cancel culture is a new leftist plot, explaining it as a result of American capitalism.

Actions:

for activists, progressives, consumers,
Educate peers on the historical context of cancel culture (implied)
Support companies that prioritize inclusive values (implied)
</details>
<details>
<summary>
2021-02-13: Let's talk about debating a civics teacher.... (<a href="https://youtube.com/watch?v=dyOhTOG70i4">watch</a> || <a href="/videos/2021/02/13/Lets_talk_about_debating_a_civics_teacher">transcript &amp; editable summary</a>)

Beau challenges the belief that governmental laws are the sole path to societal order, advocating for a society based on the golden rule instead of violence.

</summary>

"It's not laws. It's a monopoly on violence."
"Societal order can exist without governmental law and without violence."
"It's just a thought. Have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of philosophy, sparked by a student wanting to debate a closely held belief with their teacher.
The student questions the teacher's statement that governmental laws are the only way to achieve societal order.
Beau argues that governmental laws do not achieve societal order; instead, it is the monopoly on violence that enforces order.
He challenges the idea that government is separate from society and asserts that societal order comes from the monopoly on violence granted to the government.
Beau proposes an alternative society where societal order is based on people abiding by the golden rule without the need for laws or violence.
He envisions a society where individuals prioritize the interests of others over selfishness, believing it is possible but requires significant effort and education.
Beau questions whether this alternative societal structure can be scaled up and acknowledges the potential challenges and arguments against it.
He presents a scenario where borders dissolve, and nation-states disappear, exploring potential outcomes ranging from cooperation to the rise of violence-driven monopolies.
Beau contemplates the dynamics between geographic areas and how power struggles may unfold in the absence of a centralized system.
He concludes by suggesting that attempts at self-organization could potentially lead back to the current system of societal governance.

Actions:

for philosophy enthusiasts, educators, policymakers,
Advocate for community-based approaches to societal order (implied)
Educate and instill values like the golden rule in society (implied)
</details>
<details>
<summary>
2021-02-12: Let's talk about how to teach or learn history.... (<a href="https://youtube.com/watch?v=Q97aRXTp8BE">watch</a> || <a href="/videos/2021/02/12/Lets_talk_about_how_to_teach_or_learn_history">transcript &amp; editable summary</a>)

A stressed mom struggling to teach history gets guidance on making it interesting by focusing on the "why" and using engaging methods like the "Scooby-Doo method."

</summary>

"If you can understand the whys of yesterday, you're going to understand the motives of tomorrow."
"That is something that history teachers have an issue with."
"The why is what makes it interesting. Those are the juicy parts."
"Start with something that you can get their attention with."
"You're doing fine. You're good."

### AI summary (High error rate! Edit errors on video page)

A mom is stressed about teaching history to her child.
The mom feels like she's failing in the role of being a teacher.
History teachers often face challenges in getting students interested in history.
Beau suggests focusing on the "why" in history, as it makes it interesting and tells the story.
He advises encouraging kids to ask why things happened in history.
Beau mentions that the school may provide a curriculum, but it's not compulsory to follow it.
He introduces the "Scooby-Doo method" of teaching history, focusing on mysteries and engaging storytelling.
Beau recommends starting with dramatic events like D-Day to grab the child's attention.
Teaching history in reverse chronological order can keep the learner interested with the curiosity of why.
Understanding the reasons behind historical events helps in comprehending future motives.

Actions:

for parents, educators,
Try encouraging kids to ask "why" about historical events (suggested).
Use engaging storytelling and dramatic events to teach history (suggested).
Focus on understanding the motives behind historical events (suggested).
</details>
<details>
<summary>
2021-02-11: Let's talk about me agreeing with Tucker Carlson.... (<a href="https://youtube.com/watch?v=n-w0zyfgDYc">watch</a> || <a href="/videos/2021/02/11/Lets_talk_about_me_agreeing_with_Tucker_Carlson">transcript &amp; editable summary</a>)

Beau analyzes Tucker Carlson's contradictory assessments, focusing on Carlson acknowledging the effectiveness of BLM and the necessity of overdue change in society.

</summary>

"Their assessments are going to be more accurate because they're going to point to your weaknesses and your strengths."
"Here we have Tucker Carlson admitting to BLM's effectiveness."
"I agree with Tucker Carlson."
"If I look back 50 years into history and my country hasn't changed, something is wrong."
"This change is overdue, needed, and good."

### AI summary (High error rate! Edit errors on video page)

Analyzing Tucker Carlson's assessments and effectiveness.
Mentioning the importance of looking at assessments from those who oppose you for a clearer picture.
Critiquing Tucker Carlson's wild, theory-laden rant from the previous night.
Pointing out Tucker Carlson's contradictory statement of not speculating while leading his audience to speculate.
Refusing to focus on Carlson's truth-seeking quest and speculations that may arise from it.
Addressing a particular passage where Carlson acknowledges the effectiveness of BLM.
Noting the significant impact BLM and their corporate sponsors had on the country from Memorial Day onwards.
Correcting Carlson's statement on the time frame of change brought about by BLM.
Agreeing with Carlson on the necessity and positive nature of the change brought by BLM.
Emphasizing the importance of change over stagnation in a society.

Actions:

for media consumers,
Dissect contradictory statements in media (analyzed)
Acknowledge and support overdue societal change (implied)
</details>
<details>
<summary>
2021-02-11: Let's talk about Republicans leaving the party.... (<a href="https://youtube.com/watch?v=J9mNGWyL8xw">watch</a> || <a href="/videos/2021/02/11/Lets_talk_about_Republicans_leaving_the_party">transcript &amp; editable summary</a>)

A hundred Republicans aim to form a center-right party to capitalize on anti-Trump sentiment, potentially shifting the conservative movement back towards the center.

</summary>

"The goal is to move the conservative movement back to center."
"They want to capitalize on the anti-Trump sentiment that is sweeping the nation."
"Democrats in the U.S. are sure, they're left-ish, but that's not saying much."
"It does appear that they're going to acquit Trump."
"If the conservative movement does come back to center, I think it's a good thing."

### AI summary (High error rate! Edit errors on video page)

A conference call with over a hundred Republicans aimed to form a third party.
Participants include representatives from recent Republican administrations.
The goal is to capitalize on anti-Trump sentiment and move the conservative movement back to the center.
They want to name their party the center-right party.
Democrats in the U.S. are already considered center-right on the international spectrum.
Most leftists do not view Democrats as their representatives.
Beau believes this new party could gain seats in the House and Senate due to the current Republican Party's disconnect.
The current Republicans might acquit Trump, leading to more damaging revelations.
Beau suggests that a shift back to center for the conservative movement could be beneficial.
If conservatives move to the center, Democrats will have to differentiate themselves further.

Actions:

for politically interested individuals,
Form a community organization to support political shifts (implied)
Stay informed about political developments and sentiments (implied)
</details>
<details>
<summary>
2021-02-10: Let's talk about why Biden might have brought in a very disliked person.... (<a href="https://youtube.com/watch?v=eYI0JDBbwP8">watch</a> || <a href="/videos/2021/02/10/Lets_talk_about_why_Biden_might_have_brought_in_a_very_disliked_person">transcript &amp; editable summary</a>)

A theory explains the controversial potential Biden appointment of Cass Sunstein based on his skillset and past actions, shedding light on the confusion surrounding his role.

</summary>

"Nobody saw that one coming, to be honest."
"That should, I hope that sheds some light on some of the questions."
"If it is not for this purpose, nobody has a clue as to why he's being brought on."
"It's just a thought."
"His skillset might make sense."

### AI summary (High error rate! Edit errors on video page)

A theory involving a potential Biden appointment named Cass Sunstein confused many.
Sunstein's past work and reputation as not well-liked raised eyebrows.
Sunstein's previous controversial actions, like hindering environmental regulations, have made him unpopular.
Despite initial confusion, Sunstein's assignment to work on immigration rules started making sense.
Sunstein's skillset might be more relevant than his ideology for his new role.
His history of undermining agency regulations could be beneficial in certain scenarios.
Sunstein's stance on federal law being interpreted by the president could impact Biden's immigration reform.
Beau presents a theory to explain Sunstein's unexpected appointment.
The lack of alternative theories suggests that Sunstein's appointment may serve a specific purpose.
Beau acknowledges the confusion surrounding Sunstein's appointment and offers his analysis.

Actions:

for political analysts,
Analyze the skills and past actions of political appointees to better understand their potential impact (suggested).
Stay informed and engaged with political appointments and their implications (implied).
</details>
<details>
<summary>
2021-02-10: Let's talk about Trump's impeachment and the courage to lead.... (<a href="https://youtube.com/watch?v=cIN_AZD7jIo">watch</a> || <a href="/videos/2021/02/10/Lets_talk_about_Trump_s_impeachment_and_the_courage_to_lead">transcript &amp; editable summary</a>)

Impeachment, leadership courage, and the Senate's allegiance to a fading base are scrutinized.

</summary>

"If you wanted to be a leader, every once in a while you had to be first through the door."
"He wasn't going to be first through the door. He wasn't even going to show up."
"Republicans in the Senate are looking for any reason to acquit."
"Republicans in the Senate are chasing the ghost of a base rather than leading, rather than protecting the country."
"At the time when the country needs them the most, Republicans in the Senate are chasing the ghost of a base rather than leading, rather than protecting the country."

### AI summary (High error rate! Edit errors on video page)

Impeachment part two, day one, and the courage to lead are discussed.
Leadership qualities are emphasized, including being willing to lead by example.
A video is mentioned that lays out the case against the former president.
Analysis of the former president's speech encouraging fighting and inciting the crowd is provided.
Trump is criticized for lacking the courage to lead or take responsibility.
The mob's actions and their loyalty to Trump are detailed.
Republicans in the Senate are portrayed as seeking reasons to acquit despite evidence.
The short-term focus of Republicans on appeasing an energized base is criticized.
A majority of Americans want Trump barred from running for office again.
The potential disillusionment of Trump's base and those who enabled him is discussed.

Actions:

for citizens, leaders,
Hold representatives accountable for their decisions (exemplified)
Stay informed and engaged in political processes (exemplified)
Advocate for putting the country's interests over party affiliations (exemplified)
</details>
<details>
<summary>
2021-02-09: Let's talk about DOD's stand down memo.... (<a href="https://youtube.com/watch?v=67DEEjVRKrU">watch</a> || <a href="/videos/2021/02/09/Lets_talk_about_DOD_s_stand_down_memo">transcript &amp; editable summary</a>)

The DOD's military stand-down memo aims to remind soldiers of their oath, addressing recruitment challenges through education access while facing societal issues.

</summary>

"Your oath is to the Constitution, not any individual person."
"Increasing access to education could provide the military with better recruits."
"They're probably just going to more intensively investigate social media and stuff like that of people in the military."

### AI summary (High error rate! Edit errors on video page)

The Department of Defense (DOD) released a memo for a military-wide stand-down to remind soldiers of their oath to the Constitution.
The stand-down is symbolic and not expected to be super effective, but it serves as a serious gesture.
One out of five Capitol participants had ties to the military, but this statistic may not be a deciding factor in their involvement.
Movements like the Capitol events tend to attract prior service members who can become leaders due to their training.
There is a debate on what qualities soldiers should possess, with Colin Powell advocating for critical thinkers and McNamara having a different view.
Increasing access to education could provide the military with better recruits who are critical thinkers and well-rounded individuals.
Lack of access to education in society leads the military to recruit individuals who may not have broad exposure to the world.
DOD may opt for short-term fixes like intensively monitoring social media rather than addressing societal issues for recruitment.

Actions:

for military personnel, policymakers.,
Increase access to education for broader societal benefits (implied).
Address societal issues to improve recruitment pool diversity (implied).
</details>
<details>
<summary>
2021-02-09: Let's talk about Colombia stepping up.... (<a href="https://youtube.com/watch?v=7VP-4rSuv80">watch</a> || <a href="/videos/2021/02/09/Lets_talk_about_Colombia_stepping_up">transcript &amp; editable summary</a>)

The US must recognize its global responsibilities in light of Colombia's extensive refugee support, revealing American exceptionalism's flaws.

</summary>

"We're not in it alone. It's not always us. They don't always want to come here."
"The United States is failing in its moral responsibility to the world."
"Colombia just isn't extending the legal status like residency. They're getting employment status as well and health care."

### AI summary (High error rate! Edit errors on video page)

Addresses the misconception that the US is solely responsible for global issues, like accepting refugees.
Contrasts the number of refugees Colombia has accepted with the US's intake.
Colombia extended legal status to around 600,000 to 1.7 million refugees from Venezuela during an economic downturn.
Points out that Colombia has a GDP of about 320 billion, making their efforts notable.
Mentions the US's GDP of 21.4 trillion, showing a stark comparison.
Walmart's revenue of 520 billion is given as a reference point.
Notes that economic refugees seek safety and better economic opportunities, not necessarily to come to the US.
Colombia not only offers legal status but also employment status and healthcare to refugees.
Criticizes the US for not meeting its moral responsibility to the world.
Biden's cap of 125,000 refugees was considered ambitious, compared to Colombia's actions.

Actions:

for global citizens, policymakers,
Advocate for increased refugee support in your community (implied).
Support organizations aiding refugees locally (implied).
</details>
<details>
<summary>
2021-02-08: Let's talk about the case against Trump.... (<a href="https://youtube.com/watch?v=2cDBZmxv-44">watch</a> || <a href="/videos/2021/02/08/Lets_talk_about_the_case_against_Trump">transcript &amp; editable summary</a>)

Beau believes Trump's second impeachment case should focus on incitement and extra-legal activity, despite expectations of Republican acquittal.

</summary>

"They don't want to spend a lot of time on it because they want to get to Biden's agenda."
"He'd be a huge pacifier to a lot of people."
"But I have a feeling they will put what they believe to be their own political interests over that of the country."

### AI summary (High error rate! Edit errors on video page)

Trump's second impeachment is historic, and he believes the case should focus on Trump's actions during the trial.
Republicans are likely to frame it as a Democratic plot, playing into existing rhetoric.
Democrats want to swiftly move through the trial to focus on Biden's agenda for potential re-election.
The case should argue that Trump's actions incited the Capitol riot and set a tone for extra-legal activity.
Trump's rhetoric, alleging election fraud, led people to believe they needed to act, culminating in the Capitol violence.
Despite the clear evidence, many Republicans are expected to acquit Trump due to political considerations.
Beau doubts Trump will return to power in 2024, regardless of the trial outcome.
If acquitted, attention may shift to holding accountable those who enabled Trump's actions.
Paradoxically, convicting Trump might be the safest route for some Republicans' political futures.
Beau suggests that some Republicans may prioritize their political interests over the country's well-being.

Actions:

for politically-engaged individuals,
Contact your representatives to express your views on the impeachment trial (suggested)
</details>
<details>
<summary>
2021-02-08: Let's talk about Biden's border challenge.... (<a href="https://youtube.com/watch?v=jw6nbE4125Y">watch</a> || <a href="/videos/2021/02/08/Lets_talk_about_Biden_s_border_challenge">transcript &amp; editable summary</a>)

Media headlines exaggerate southern border issues; Beau calls for humane responses and assistance, not panic.

</summary>

"Large numbers of people have been showing up at one time at that southern border for almost 20 years."
"This is not a crisis. This is not a challenge."
"The only aliens we're afraid of are the ones that chase Sigourney Weaver."

### AI summary (High error rate! Edit errors on video page)

Media headlines on the southern border crisis seem to exaggerate the situation, using words like "surge," "challenge," "emergency," and "crisis."
Large numbers of people showing up at the southern border have been a continuous issue for almost 20 years, becoming a problem only under specific administrations.
The previous administration used the situation as a pretext to build a border wall, while the new administration claims that America is back, which should mean handling the situation without panic.
Beau urges the Biden administration to honor international agreements, embrace refugees, and provide non-military assistance to countries of origin affected by failed US foreign policy.
Transitioning from a world policeman to a world EMT could be a more effective and humane approach than building walls or increasing enforcement.
Beau advocates for allowing people in while working on necessary actions, mentioning that the United States should not fear immigrants but instead offer assistance and support.
He concludes by stating that the situation at the southern border is not a crisis or a challenge if the Biden administration's promises and rhetoric are to be believed.

Actions:

for policy makers, activists, advocates,
Provide non-military assistance to countries of origin (exemplified)
Transition from being the world's policeman to being the world's EMT (implied)
</details>
<details>
<summary>
2021-02-07: Let's talk about two schools of thought and change.... (<a href="https://youtube.com/watch?v=3zpODzHfrQM">watch</a> || <a href="/videos/2021/02/07/Lets_talk_about_two_schools_of_thought_and_change">transcript &amp; editable summary</a>)

Beau presents two schools of thought on systemic change: reformists work within the system, while revolutionaries focus on building new infrastructure, both aiming for the same goal.

</summary>

"The reformist says, well, you know, that's nice. That location you want to get to, that's nice. But it can't happen right now."
"The reality is both schools of thought need the other."
"The duty of the revolutionary is to get rid of those objections, to build the infrastructure, to overcome those pragmatic reasons."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of two schools of thought on deep systemic change in the country.
Reformists advocate using the system's machinery to address issues through voting, petitions, and campaigning for politicians.
They focus on policies that reduce harm and enhance social safety nets.
Revolutionaries, on the other hand, prioritize building new infrastructure and systems over electoral processes.
They are more hands-on in addressing issues directly and creating solutions like co-ops and community networks.
Beau argues that these two groups are not necessarily in opposition but are heading towards the same goal.
He presents a metaphor of buses moving at different speeds but in the same direction to illustrate the relationship between reformists and revolutionaries.
Beau stresses the importance of both short-term (reformist) and long-term (revolutionary) goals in achieving societal change.
He points out the need for reformists to recognize areas that need change, which is often brought to light by revolutionaries.
Similarly, revolutionaries require popular support and infrastructure usage from reformists to make a substantial impact.
Beau warns against echo chambers on social media, urging a realistic assessment of support for ideas.
He underscores the effectiveness of building new systems rather than solely opposing the existing one to drive societal change.
The duty of revolutionaries is to eliminate objections and build infrastructure to garner popular support for their cause.
Beau concludes by leaving the audience with his thoughts and well wishes.

Actions:

for social activists,
Start a community network to address local issues (exemplified)
Get involved in building infrastructure like co-ops and intentional communities (exemplified)
Support policies that reduce harm and improve social safety nets (exemplified)
</details>
<details>
<summary>
2021-02-07: Let's talk about Black History Month and pulling your kids out.... (<a href="https://youtube.com/watch?v=BPRudrdS3ak">watch</a> || <a href="/videos/2021/02/07/Lets_talk_about_Black_History_Month_and_pulling_your_kids_out">transcript &amp; editable summary</a>)

Beau challenges objections to Black History Month, advocating for comprehensive historical education and condemning the exclusion of vital knowledge from children.

</summary>

"If Black History Month bothers you, you never get to say, get over slavery again, ever."
"The funny part about this is, do you know when Black History Month can end? When white folks stop getting mad about it."
"History is important."
"It is incredibly important to yank your kid out of something like this."
"You are choosing to make certain your child is ignorant of things they need to know."

### AI summary (High error rate! Edit errors on video page)

Beau expresses surprise at the revelation of parents being able to pull their kids out of Black History Month curriculum in schools.
The history of Black History Month dates back to 1926 when historian Carter Woodson proposed a week to study black history.
In 1970, Black History Month was extended to a full month, starting from January 2nd to February 28th.
Beau points out that objecting to Black History Month means never being able to say "get over slavery" again.
He challenges the idea of opting out of certain history classes for civil rights reasons, using algebra as an example.
Beau criticizes the notion of parents deciding to exclude their children from learning specific aspects of history, labeling it as wrong.
He suggests that Black History Month can only end when white individuals stop being upset about it.
Beau stresses the importance of a comprehensive world history curriculum in schools to combat Eurocentrism and misinformation.
History education is vital, and Beau urges against limiting children's exposure to different historical perspectives.
He concludes by encouraging viewers to think about the implications of restricting historical education for children.

Actions:

for parents, educators, activists,
Teach comprehensive world history in schools (suggested)
Advocate for inclusive and diverse historical curriculums (suggested)
Emphasize the importance of history education to parents and educators (suggested)
</details>
<details>
<summary>
2021-02-06: Let's talk about the First Amendment and Canada's designations.... (<a href="https://youtube.com/watch?v=L8ch6psq-SQ">watch</a> || <a href="/videos/2021/02/06/Lets_talk_about_the_First_Amendment_and_Canada_s_designations">transcript &amp; editable summary</a>)

Beau explains why the U.S. cannot designate certain groups as Canada did, relying on existing strategies to address threats effectively.

</summary>

"Membership in an organization in and of itself cannot be a crime in the United States."
"We do not need the federal government to have the power to designate membership in any group as a crime."
"They have the tools. They work. They just have to put in the work."

### AI summary (High error rate! Edit errors on video page)

Explains the five freedoms and six rights under the First Amendment.
Canada recently designated certain groups as bad, causing trouble for members.
In the United States, membership in an organization cannot be a crime due to the First Amendment.
The U.S. government is not considered responsible enough to designate domestic groups.
Federal strategies and legislation exist to disrupt groups without making them illegal.
Certain agencies and communities can now keep an eye on designated groups due to Canada's actions.
Federal law enforcement uses tactics like flipping members or placing undercover agents within groups.
Operatives going undercover in these particular groups don't need to hide their military backgrounds.
The federal government already has the necessary tools to address threats, similar to surveillance.
The U.S. government can take down organizations like the ones designated by Canada using existing strategies.

Actions:

for activists, policymakers,
Mobilize community to advocate for responsible government actions (implied)
</details>
<details>
<summary>
2021-02-06: Let's talk about Biden's foreign policy signals.... (<a href="https://youtube.com/watch?v=w-iONmH0oEc">watch</a> || <a href="/videos/2021/02/06/Lets_talk_about_Biden_s_foreign_policy_signals">transcript &amp; editable summary</a>)

Beau provides insight into Biden's foreign policy decisions, signaling a shift in approach towards the Middle East while maintaining American dominance.

</summary>

"Today, we're going to talk specifically about the Mideast because he's made some moves there that are significant in and of themselves and also show his long-term plan."
"Regardless of how much good news comes out, any time you're talking about foreign policy, the goal of it is to maintain American dominance."

### AI summary (High error rate! Edit errors on video page)

Providing an overview of Biden's foreign policy and what it signals for the future, particularly in the Middle East.
The US will no longer support offensive Saudi Arabian operations in Yemen, signaling a shift in policy.
Despite the change, the US will still be involved in Yemen, opposing non-state actors.
The decision to stop supporting offensive operations in Yemen is more about Iran than Saudi Arabia.
Biden's move indicates a desire to reach a lasting agreement with Iran and avoid confrontational postures.
The goal is to eventually reduce the US presence in the Middle East by shifting away from fossil fuels.
The decision is driven more by business interests and climate policies rather than a moral awakening.
Biden's foreign policy aims to maintain American dominance, even with changes in approach and focus on different regions.
Despite potential positive outcomes, the ultimate aim remains American influence and presence.
Foreign policy, including Biden's, is ultimately about maintaining American dominance, not altruism.

Actions:

for foreign policy analysts,
Analyze and stay informed about US foreign policy decisions (suggested)
Advocate for diplomatic solutions and peace-building efforts in conflict regions (implied)
</details>
<details>
<summary>
2021-02-05: Let's talk about an update on Rochester New York.... (<a href="https://youtube.com/watch?v=DGk9_rcUEgg">watch</a> || <a href="/videos/2021/02/05/Lets_talk_about_an_update_on_Rochester_New_York">transcript &amp; editable summary</a>)

Beau provides insights on addressing excessive force by focusing on attitudes over tools, urging for legislation to require random body camera footage checks.

</summary>

"Excessive force is any force in excess of the minimum necessary to affect the arrest."
"The issue isn't that a specific type of force was used. The issue is that force was used."
"Go after the attitude."
"Their heart is definitely in the right place. Now we just have to get the legislation there."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Provides an update on the situation in Rochester, New York and mentions state legislators taking action.
Talks about a bill introduced to ban the use of sprays against minors entirely.
Acknowledges the good intentions behind the bill but questions its effectiveness in addressing excessive force.
Shares a story about a former deputy's reaction to the footage and the potential escalation of force if one tool is taken away.
Argues that excessive force is any force beyond the minimum necessary for an arrest, suggesting addressing attitudes rather than specific tools.
Proposes legislation requiring random body camera footage checks and consequences for officers using excessive force.
Appreciates the legislators' intentions but stresses the importance of effective legislation to address the root issue.

Actions:

for legislators, activists, community members,
Advocate for legislation requiring random body camera footage checks and consequences for officers using excessive force (suggested)
Support initiatives that focus on addressing attitudes towards the use of force within law enforcement (suggested)
</details>
<details>
<summary>
2021-02-05: Let's talk about Gaetz, the pledge, patriotism, performance, and politicians.... (<a href="https://youtube.com/watch?v=DCJknkoh9bY">watch</a> || <a href="/videos/2021/02/05/Lets_talk_about_Gaetz_the_pledge_patriotism_performance_and_politicians">transcript &amp; editable summary</a>)

Beau shares a story from the early 2000s illustrating that patriotism is about action, not symbols, criticizing politicians for prioritizing performative gestures over genuine care for the country and its people.

</summary>

"Patriotism is shown through action, not through worship of symbols."
"You show your patriotism through action, not through publicity stunts."
"Put the country and the people of this country above your own political interests."

### AI summary (High error rate! Edit errors on video page)

Tells a story of a civilian and a colonel having a significant exchange about patriotism in the early 2000s.
Describes how the civilian believed patriotism was about action, not just symbols.
Recalls the civilian's 30 years of service to the country, which gave weight to his perspective on patriotism through action.
Mentions Republicans led by Matt Gaetz expressing outrage over a committee's decision not to start meetings with the Pledge of Allegiance.
Points out that members of Congress can demonstrate patriotism through daily actions rather than performative rituals.
Criticizes politicians for prioritizing publicity stunts over putting the country and its people first.
Emphasizes that true patriotism is displayed through actions, not mere words or symbolic gestures.

Actions:

for politically conscious individuals,
Prioritize actions that benefit the country and its people over performative gestures (implied)
</details>
<details>
<summary>
2021-02-04: The roads to funding your community network projects.... (<a href="https://youtube.com/watch?v=Gw0gt6Ao_yk">watch</a> || <a href="/videos/2021/02/04/The_roads_to_funding_your_community_network_projects">transcript &amp; editable summary</a>)

Beau outlines a six-step process for successful community fundraising ventures, stressing achievable goals, effective communication, and post-event impact sharing.

</summary>

"Pick battles that are big enough to matter and small enough to win."
"Make sure it happens. Make good on whatever it is you have set out to do."
"You will make a difference."

### AI summary (High error rate! Edit errors on video page)

Beau shares insights on running a successful fundraiser for a community network.
He breaks down a six-step process applicable to any fundraising venture.
Planning is key, with a focus on setting defined and achievable goals.
Details matter, including what to obtain, how to deliver it, and getting people involved.
Pre-advertising phase involves announcing the goal, how to participate, and when it will happen.
Beau stresses the importance of communication and avoiding assumptions when determining needed supplies.
The event itself should be fun and engaging to maintain participant interest.
Delivery is critical—ensure goals are met, communicate effectively, and document the process.
Post-advertising involves sharing documentation with contributors to show the impact of their support.
Evaluation is necessary to learn from successes and failures, adjust goals, and aim higher for future efforts.

Actions:

for community organizers and fundraisers.,
Contact local shelters or organizations to understand their immediate needs (suggested).
Plan and execute fundraising events with defined, achievable goals (exemplified).
Document the process and share the impact with contributors (implied).
</details>
<details>
<summary>
2021-02-04: Let's talk about the farce tonight in the Senate.... (<a href="https://youtube.com/watch?v=MObC8F6Ojk4">watch</a> || <a href="/videos/2021/02/04/Lets_talk_about_the_farce_tonight_in_the_Senate">transcript &amp; editable summary</a>)

Beau explains the strategic Senate process of introducing multiple amendments during budget reconciliation to potentially manipulate political opponents' votes, revealing the farcical nature of these proceedings.

</summary>

"It is absurd. Normally the party that is in power doesn't really want their supporters watching because it's typically them that makes the mistakes."
"That's what they're talking about. If you hear tomorrow that a Senator voted in some ridiculous manner, that's what happened."
"It's enlightening."
"It's a sham. It's a farce."
"Both parties do it. It is the Senate."

### AI summary (High error rate! Edit errors on video page)

Explains the Senate's use of budget reconciliation to push through Biden's relief plan without risking a filibuster.
Mentions the strategy of introducing many amendments during the process to potentially trick political opponents into voting against popular measures.
Describes the absurdity and farcical nature of the Senate proceedings during these moments.
Notes that both parties, not just Republicans, partake in this behavior of introducing amendments to create tricky voting situations.
Emphasizes the importance of paying attention to such Senate events, even if they are often seen as jokes or farces.
Points out that the goal behind introducing numerous amendments is not necessarily to pass them but to create political pitfalls for opponents.

Actions:

for citizens, voters,
Watch Senate proceedings closely to understand the tactics and strategies employed by both parties during critical processes (implied).
Stay informed about how political maneuvers like introducing amendments can impact decision-making in government (implied).
</details>
<details>
<summary>
2021-02-04: Let's talk about the Cheney vote and the future of the Republican party.... (<a href="https://youtube.com/watch?v=3kV0xgDz6l8">watch</a> || <a href="/videos/2021/02/04/Lets_talk_about_the_Cheney_vote_and_the_future_of_the_Republican_party">transcript &amp; editable summary</a>)

Representative Cheney's impeachment vote exposes Republican Party's internal struggle and the need for reinvention beyond Trump's influence, focusing on policy over whataboutism and lies.

</summary>

"Currently it's the party of whataboutism, deflection and lies."
"Younger people are more liberal."
"The Republican Party is at a crossroads."
"Disaster zone."
"The clearest path to redefining itself."

### AI summary (High error rate! Edit errors on video page)

Representative Cheney voted to impeach Trump, revealing the internal struggle in the Republican Party.
Despite efforts from Trump loyalists, a vote to oust Cheney from a leadership position failed.
The failed vote showed Trump's weakness rather than his power.
The secret ballot vote allowed representatives to go against Trump without facing backlash.
Beau questions whether Senate Republicans voting freely via secret ballot could alter the outcome of the impeachment trial.
He points out that the Republican Party is currently without a clear identity or national leader.
Beau criticizes the party for being focused on whataboutism, deflection, and lies instead of policy.
He mentions that the party needs to move away from ultra-nationalism and soundbites.
Beau suggests that the Republican Party needs to become more liberal in its policies to attract younger voters.
Convicting Trump in the impeachment trial is seen as a way for the party to redefine itself.

Actions:

for political activists, republicans,
Convict Trump at the impeachment trial and set a new tone (exemplified)
</details>
<details>
<summary>
2021-02-03: Let's talk about the hilarious news out of Georgia.... (<a href="https://youtube.com/watch?v=xvdp-gHW32E">watch</a> || <a href="/videos/2021/02/03/Lets_talk_about_the_hilarious_news_out_of_Georgia">transcript &amp; editable summary</a>)

Addressing investigations into potential voter irregularities in Georgia, including notable figures, reminding of the presumption of innocence, and pointing out the potential humor in the situation.

</summary>

"Georgia does believe that there may have been a very small number of people who voted not in accordance with the law."
"Everybody remember innocent unless proven guilty and all of that."
"So there's that. That's occurring."
"If you are a comedian, I sincerely apologize for the state you find yourself in because there is no way you are going to be able to compete with reality anymore."
"Y'all have a good morning."

### AI summary (High error rate! Edit errors on video page)

Addressing news from Georgia about former President Trump's claims and the investigations initiated.
Mentioning investigations opened into people suspected of voting improperly, including Lin Wood.
Georgia officials questioning Wood's residency change to South Carolina based on an email.
Clarifying that the investigations are not accusations but part of gathering facts.
Reminding viewers about the presumption of innocence until proven guilty.
Noting the potential humor in the situation for some.
Summarizing that Georgia suspects a small number of voters of not following the law.
Concluding with well wishes for the audience.

Actions:

for georgia residents, political observers,
Contact Georgia officials for updates on the investigations (suggested).
Stay informed about the developments in the Georgia voter investigations (implied).
</details>
<details>
<summary>
2021-02-03: Let's talk about Detrumpification and McCarthyism.... (<a href="https://youtube.com/watch?v=s2GqeV9mc94">watch</a> || <a href="/videos/2021/02/03/Lets_talk_about_Detrumpification_and_McCarthyism">transcript &amp; editable summary</a>)

Beau explains detrumpification, dismisses parallels to McCarthyism, and urges understanding history to avoid false comparisons.

</summary>

"Those who don't understand history are doomed to make false parallels."
"It's not an accusation, it's a statement of fact."
"The United States is going to have to reckon with what has occurred over the last four years."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of detrumpification, ensuring former Trump administration officials don't influence public policy.
Compares detrumpification to McCarthyism, dismissing them as not the same.
Notes that McCarthyism was based on accusations with little evidence, unlike detrumpification.
Points out that detrumpification deals with factual statements about senior Trump administration officials, not accusations.
Draws a modern parallel to McCarthyism in the context of former Trump administration officials and election claims.
Suggests that the U.S. needs to confront and reckon with the events of the past four years.
Emphasizes the importance of understanding history to avoid making false parallels.

Actions:

for history enthusiasts, political analysts.,
Research and understand historical contexts to avoid making false parallels (suggested).
Advocate for accountability and transparency in political processes (implied).
</details>
<details>
<summary>
2021-02-02: Let's talk about why it seems like Democrats aren't getting much done.... (<a href="https://youtube.com/watch?v=wKO-UdTS-9k">watch</a> || <a href="/videos/2021/02/02/Lets_talk_about_why_it_seems_like_Democrats_aren_t_getting_much_done">transcript &amp; editable summary</a>)

Beau explains the hurdles Democrats face in passing legislation due to Senate rules and their centrist nature, hindering radical changes.

</summary>

"If Democrats were a left party and reached out to the working class the way left-leaning parties do, they'd be able to do this."
"They don't wield power like leftists, because they're not leftists."
"I am radically anti-authoritarian. And I am left-leaning."
"Most Democrats are center-right on the international spectrum."
"Republican Party talking points are designed for those who are politically illiterate."

### AI summary (High error rate! Edit errors on video page)

Explains the structure of the US government divided into three branches: judicial, executive, and legislative, with the legislative branch further divided into the House of Representatives and the Senate.
Details the current control of Democrats in the House, Senate, and White House, but the Senate is split 50-50, with a tie-breaking vote by the vice president.
Describes the filibuster in the Senate, a process that allows a senator to delay or stop a bill from passing by continuously speaking, requiring 60 votes to stop it.
Mentions Democrats' reluctance to abolish the filibuster due to potential Republican control in the future, limiting their options for passing legislation.
Differentiates between Democrats and leftists, stating that most Democrats are center-right on the international spectrum and behave as centrists rather than leftists.
Suggests that if Democrats were a true leftist party, they could use tactics like filibustering to bring attention to their bills and make Republicans pay politically.
Points out that because Democrats are typically centrist and status quo, they lack the power and approach of truly leftist parties to push through radical changes.
Indicates that without behaving like a truly left party, Democrats will struggle to pass bills in the Senate due to the 60-vote requirement.
Explains the slow progress of Biden's agenda is due to Senate rules, particularly the filibuster, which requires bipartisan support for legislation.
Comments on the obstructionist nature of the Republican Party and how their talking points may not serve the best interests of their working-class supporters.

Actions:

for us citizens,
Reach out to representatives to advocate for changes in Senate rules to facilitate the passing of legislation (implied).
Educate and inform politically illiterate individuals about the true implications of Republican Party policies and actions (implied).
</details>
<details>
<summary>
2021-02-02: Let's talk about HR 127... (<a href="https://youtube.com/watch?v=7TdQzLNr-2I">watch</a> || <a href="/videos/2021/02/02/Lets_talk_about_HR_127">transcript &amp; editable summary</a>)

Beau addresses concerns about HR 127, describing it as disarming poor individuals and unlikely to pass the Senate, cautioning Democrats about potential repercussions in the 2022 House elections.

</summary>

"It is as bad as it seems."
"It disarms poor people. That's what this bill does."
"This will never make it through the Senate under any circumstance."
"It becomes a crime for poor people, but if you have money, it's okay."
"The bill should probably be withdrawn before it gets too far and too much attention gets drawn to it."

### AI summary (High error rate! Edit errors on video page)

Addressing HR 127 and the concerns surrounding it.
People from both sides view the bill as bad.
HR 127 requires licensing, training, and insurance for gun owners.
The bill poses logistical challenges, such as requiring individuals with future issues to report themselves.
The financial burden of insurance disarms poor individuals.
The bill does not address closing the domestic violence loophole.
Assuring pro-2nd Amendment individuals that the bill won't pass the Senate.
Mentioning that the bill won't achieve its intended goal.
Warning Democrats that supporting this bill may cost them in the 2022 House elections.
Emphasizing that the bill is unlikely to pass the Senate and could become a campaign tool for Republicans.
Suggesting that the bill should be withdrawn before gaining more attention.

Actions:

for advocates, voters, lawmakers,
Engage in advocacy efforts to raise awareness about the implications of HR 127 (suggested).
Contact your representatives to express concerns about the bill (exemplified).
Join local gun rights or gun regulation advocacy groups to stay informed and engaged (implied).
</details>
<details>
<summary>
2021-02-01: Let's talk about what happened in Rochester, New York.... (<a href="https://youtube.com/watch?v=4Du7Q2Y0FVg">watch</a> || <a href="/videos/2021/02/01/Lets_talk_about_what_happened_in_Rochester_New_York">transcript &amp; editable summary</a>)

Rochester PD pepper-spraying a 9-year-old child raises questions about policing practices and the need for consent-based approaches.

</summary>

"You pepper sprayed a handcuffed kid in a traumatic situation. Congratulations."
"We as a society really have to look at how we deal with law enforcement in this country."
"Sure, yes, it is early. There is the possibility that information may come forward that changes my mind, but that seems super unlikely."
"You talk and get them out of the vehicle."
"We should probably move to consent based policing."

### AI summary (High error rate! Edit errors on video page)

Rochester, Rochester PD incident involving pepper spraying a 9-year-old child.
Child was handcuffed and pepper sprayed during a family problem.
Union defended the use of pepper spray, citing difficulties in getting people into cars.
Beau criticizes the union for justifying the use of force on a child.
Police officers failed to handle the situation appropriately without using pepper spray.
Lack of empathy and understanding shown towards the child's traumatic experience.
Beau questions the need for such excessive force on a child in this situation.
Suggests alternative de-escalation techniques like using stuffed animals or talking.
Points out the cultural issues within the department and the problematic defense by the union.
Calls for a reevaluation of law enforcement practices towards consent-based policing.

Actions:

for community members, advocates,
Advocate for consent-based policing (implied)
Support mental health professionals (implied)
</details>
<details>
<summary>
2021-02-01: Let's talk about link analysis and the FBI.... (<a href="https://youtube.com/watch?v=LKSsR7fBPZI">watch</a> || <a href="/videos/2021/02/01/Lets_talk_about_link_analysis_and_the_FBI">transcript &amp; editable summary</a>)

Beau explains link analysis, clarifies misconceptions, and warns against military involvement in law enforcement, stressing the importance of upholding rights to combat authoritarian movements like Trumpism.

</summary>

"You can't give up those rights, surrender those rights, ignore those rights to combat those people or they won."
"Just because your name's on the board doesn't mean you did anything wrong."
"If you use the military to do this, they won."
"During the investigation, during everything that comes out, we have to uphold the rights and the principles that they attacked."
"The underlying theme of why the Sixth was so wrong is because it is widely seen as an attack on basic principles and rights within the United States."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of link analysis in investigations prompted by a former assistant director of the FBI mentioning it on MSNBC.
Describes how link analysis is portrayed in movies with pictures on a wall and different colored strings connecting them.
Addresses the assumption that there is a criminal connection between members of Congress and those responsible for activities on the 6th.
Emphasizes that link analysis doesn't necessarily mean criminal conduct but looks for shared motivations or sympathies.
Mentions the historical context in the United Kingdom where separate groups shared goals and kept each other informed.
Clarifies that link analysis aims to identify conduits between peaceful and potentially criminal actors.
Raises the possibility that individuals may have unknowingly broken the law due to misconceptions.
Warns against advocating for military involvement in law enforcement functions within the United States.
Stresses the importance of upholding rights and principles in combating movements like Trumpism.
Urges to be anti-authoritarian and maintain the presumption of innocence during investigations.

Actions:

for investigators, activists, concerned citizens,
Uphold rights and principles during investigations (implied)
Maintain presumption of innocence for individuals involved in investigations (implied)
Advocate against military involvement in law enforcement functions within the United States (implied)
</details>
