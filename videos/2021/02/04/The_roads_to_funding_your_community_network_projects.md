---
title: The roads to funding your community network projects....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Gw0gt6Ao_yk) |
| Published | 2021/02/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau shares insights on running a successful fundraiser for a community network.
- He breaks down a six-step process applicable to any fundraising venture.
- Planning is key, with a focus on setting defined and achievable goals.
- Details matter, including what to obtain, how to deliver it, and getting people involved.
- Pre-advertising phase involves announcing the goal, how to participate, and when it will happen.
- Beau stresses the importance of communication and avoiding assumptions when determining needed supplies.
- The event itself should be fun and engaging to maintain participant interest.
- Delivery is critical—ensure goals are met, communicate effectively, and document the process.
- Post-advertising involves sharing documentation with contributors to show the impact of their support.
- Evaluation is necessary to learn from successes and failures, adjust goals, and aim higher for future efforts.

### Quotes

- "Pick battles that are big enough to matter and small enough to win."
- "Make sure it happens. Make good on whatever it is you have set out to do."
- "You will make a difference."

### Oneliner

Beau outlines a six-step process for successful community fundraising ventures, stressing achievable goals, effective communication, and post-event impact sharing.

### Audience

Community organizers and fundraisers.

### On-the-ground actions from transcript

- Contact local shelters or organizations to understand their immediate needs (suggested).
- Plan and execute fundraising events with defined, achievable goals (exemplified).
- Document the process and share the impact with contributors (implied).

### Whats missing in summary

The full transcript captures Beau's detailed guidance on organizing and executing successful fundraising events for community networks, focusing on achievable goals, effective communication, and post-event impact sharing.

### Tags

#CommunityFundraising #EffectiveCommunication #AchievableGoals #ImpactSharing #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Beau again,
and today we are on the road.
["The Road to the World"]
Today, we are going to talk about
how to obtain material, how to get things.
If you are going to be running a community network of any kind,
no matter what the focus is, at some point,
you're going to need to raise funds or obtain material.
It is going to happen.
Now, we're going to go behind the scenes
of the most recent fundraiser from the other channel.
However, the six-step process here, it doesn't matter
If you are raising money, you are doing a food drive,
you are trying to get medical supplies, doesn't matter.
The steps are the same.
The steps are the same.
Okay, so what are the steps?
First is planning, of course.
You need to come up with a defined goal
that you want to achieve.
And this is hard for people who like
to get directly involved in stuff
Because we tend to think big.
And when we define something, we're
going to feed people who need to be fed.
That's not a goal.
That's a wish.
A goal is, we're going to feed 100 people.
You need to quantify whatever the project is.
Whatever you're attempting to gain material for,
you need to make sure that it is something
that can be achieved, not something that's open-ended.
It makes people more likely to become involved.
This way, they don't feel like they're just throwing money
at a giant problem and hoping it gets solved.
There's something specific you want to do.
You have to define that goal.
And it has to be achievable.
And what I mean by that is, if you
are going to be doing these kind of events a lot,
you can't ever fail, especially in the beginning.
Whatever that goal is, you have to achieve it.
So you need to pick battles that are big enough to matter
and small enough to win.
It's OK to overshoot your goal.
It is OK to overshoot your goal.
In fact, you want to every single time.
And this is also the same stage where you work out the details.
Exactly what are you trying to obtain?
How is it going to be delivered?
All of that stuff, and how you're actually
going to get people involved.
Hey, hey.
Hey, how's it going?
Got a mask?
Yeah, I'm putting one on.
Come on in.
What's going on?
What you got going on today?
Uh, same thing we do every day, Pinky.
Uh, we're doing a fundraiser.
Oh, for who?
DV Shelter.
Oh, same as last year?
Yes, but no.
Uh, we got 12 instead of five.
Like, last year we had to do 12.
Well, okay, heck yeah, heck yeah.
Um, so what are you thinking?
Um, same thing as last year,
tablets and headphones and that kind of stuff?
Is it for the older kids?
Yeah, yeah, no, it's it's exactly like last year. We'll do tablets
But we gotta get 12 so but the channel's grown so yeah, I mean we should be able to do it
Theoretically, we'll find out.
Yeah, so basically looking at this, we need to raise like $2,500, which we should be able
to do.
That's the achievable thing, and then we'll probably, hopefully, go over that, and if
we do, we'll figure out what to do with the excess.
So that's what we're, that's what we're going to be rolling with.
But.
So what's the video for though?
Why are you shooting?
We're on the road.
I mean we're not on the road, but this is Beau on the road.
Oh, okay.
So this is the secondary part.
The fundraiser is going to be on the main channel, but this, the, we're going to set
up, this video is explaining and showing how to actually conduct a fundraiser, step by
stuff and you're gonna be in this one you're drafted oh okay that sounds like
fun join the crew so the only one of the questions that we have as you you know
everybody is Chromebooks Chromebooks instead of tablets what do you think
that's actually what I'm trying to figure out right everybody's saying
Chromebooks for a Christmas gift yeah I mean well they're stuck at home at
at school. Yeah, but it's Christmas. It's supposed to be fun. It's supposed to be a
present. It's not supposed to be school. Chromebooks are fun. For nerds. I have a
Chromebook. Anyway. I'm not retracting my previous statement. Alright, fine. Um, alright, so you're
saying tablets well I just for the battery the amount of battery it's going
to use if someone wants to reuse it are you doing the Netflix thing yeah yeah I
just think it would be easier and the kids would probably like it more takes
up less space it's lighter weight it's good not use as much battery to perform
all the same functions that a tablet would do but they can't use it for you
Okay so iPad or no? iPad or no. Or do we go with like we did
laughter? Well I mean iPad does have a lot of cool things about it but again
whenever it's a gift I would look at the cost beyond it being the gift. You're
looking at the AirPods for the headphones which is an extra cost which
may not be that much but if they were to lose one or break one then that's a
cost out of their pocket later on they may have to have iTunes cards for
downloading stuff you usually don't have that the same kind of cost with a
Samsung tablet or something comparable a droid yeah droid all right all right
okay so then see you say tablets I'm gonna put this on Twitter and they get
So tablets or Chromebooks?
Right, that's the big thing.
Twitter gets it decided.
Yeah, and we'll get on the Netflix subscription
and then like a $25 gift card to get whatever it is they want.
Headphones, and if we get a tablet,
we'll get them a case as well.
Well, they'd probably need a case either way.
Chromebooks are indestructible.
Have you seen mine?
have you met my kids? So when it came to planning out the fundraiser, your
live streams generally bring in the amount that you were shooting for. Why
not try for a little higher amount? I have no doubt that we're going to raise
the money needed to get the gifts. There's no doubt in my mind. The internet
people will come through. I have complete faith in that. But when we're
setting up fundraisers, we want to make sure that we can always deliver on it.
And this is true. It doesn't matter what you're doing. It doesn't matter what you are setting up, what you're trying to
achieve.
You want to make sure from the outset that it's going to be a win.
Because if you lose, if you don't get it, people have less faith in it.
So every goal you set is something you know that you will be able to achieve.
You know you'll be able to deliver on.
You want to under promise and over deliver every single time.
That way, nobody is ever gonna feel like they put their money towards something or
their time towards something and they didn't achieve the goal.
We wanna pick battles that are big enough to matter and small enough to win.
We're gonna exceed this, no doubt in my mind.
We'll use the excess for other stuff that the shelter needs.
That way we're always winning.
We're always creating change.
We're always making sure that not just are our goals being satisfied and we are
influencing what's going on around us, but those who are supporting us, don't
put their time, money, effort into something that doesn't pan out, it's always
a win, and you set it up that way from the very beginning.
So, in that scene, he clearly, he's like, we're going to get this, you know that, yeah,
we did, but we have that definable goal, that achievable goal, that thing that we can say,
this is what we're going to do, this is what we're going to achieve.
As much as altruistic people, we just want all the resources we can get because we know
we can put them to good use.
You need to have something that you can point to and say, this specifically is what we're
going to do, and anything else is going to go to a like goal, something with this organization,
for this cause, whatever.
So the next step is the pre-advertising phase.
That is where you announce what you're going to do, how people can become involved, what
defined specific achievable goal is and when it's going to happen all of that
stuff. A lot of times organizations will be working with another group. As much as
social media people will tell you to leverage each other's audiences,
I disagree. Because for us, the people who are likely watching this, you're
You're probably pretty political.
The organization that you may be teaming up with, they may not be, and you don't want
to give them bad press.
Aside from that, there may be blowback that comes the other way at some point.
So it's better to, in my way of thinking, operate independently towards that collective
goal.
Twitter has spoken.
Really?
Tablets.
Nice.
There actually a teacher did a poll of their class, of
teens, the people that actually matter in this case.
And they said tablets by a pretty good majority.
So we will do tablets.
So I noticed when you advertised the fundraiser the other day in your video, you didn't name
the specific organization.
Is there a reason?
Everybody you know like me?
No, right?
I am politically divisive at times.
So what you don't want, and this is true of any organization, if you're trying to help
an organization, you don't want to become a liability to it.
In the area where I live, with my views, if I come out and say, this specific organization,
I'm helping them, there may be the appearance that they endorse me, which is something,
that's a liability they don't need.
is there may be some big donor that they have
who doesn't like me,
and it creates that association in that person's mind
if they see a bunch of advertising
with the two names together,
and it may give them a negative view of that organization.
So generally, the way I do it is normally,
I don't even tell the organization I'm doing anything
until it's already done.
That way, they don't suffer any blowback for anything that I'm doing.
I don't tell them that I'm doing it, and I don't mention them specifically until it's done.
And that way, it's just them catching something from me.
They're getting support from us, not them resharing or retweeting something that comes from me.
That way, there's never that connection.
The third phase is the event itself, and this is something where
a lot of us make mistakes. Make it fun. Make it fun. Normally, if people are prompted to get involved
in something directly like this, there's a problem. You need medical supplies. You need food. You need
something for people and they are in a bad way. However, if the event is so solely focused on
that fact, it is going to be depressing and people aren't going to want to be involved in the next
one. If at the end of it, they go home and they feel bad, they are unlikely to come to the next
one. So yes, mention what it's about, obviously, at the beginning, maybe in the middle once,
and at the end. But don't make the whole event about whatever that issue is because you may end
up driving people away. The funny part about this is I actually learned that from the shelter we
helped with this fundraiser. They have a number of events that they hold and they aren't related
to the issues that they deal with, not directly. One of them is like everybody comes out and
and like decorates a purse and then they auction them off or there's one where everybody wears
high heels and they walk a mile and it's fun.
Yes, there's the symbolic, you're walking a mile in her shoes and all of that and everybody's
aware of what the cause is but you're not hammering it into the point where they feel
bad at the end of that event.
We want to leave people on a positive note.
All right, so the fundraiser was a success.
Awesome.
We brought in a little more than seven grand.
So more than double the goal, we got about 4,500 extra to work with.
That's before or after YouTube's cut.
We just eat that.
The ad revenue covers that.
So people give that, they give seven grand, we're going to spend seven grand.
Okay. Um, so well that covers everything you wanted to do.
Uh, right. Uh, what do you want to do with the access? Uh,
they've already told us that we need pajamas. Um, and then we,
we've got to figure out what we're gonna do with the rest.
Okay. So list time. Right.
I'll make a mental list.
Pretend that we've worked together. There's a whiteboard.
We need to write this down.
Okay, so what else?
I mean, I've seen the video that you did in the past where you bought a bunch of supplies.
Same thing. Well, here, let's put pajamas on the list so we don't forget it.
Right.
Pajamas, and then let's see, what else do you think they'll need?
Maybe food? Cleaning supplies?
Cleanings wise, detergents, soap, hygiene kits, I don't know.
Diapers and formula?
Yeah, probably, maybe.
formula let's see shampoo hygiene kits anything like that yeah okay they
normally have clothes and blankets stuff like that okay so this is the normal
stuff a shelter needs anything else you can think of food cleaning supplies
detergent paper towels paper towels would be helpful right well that's
cleaning supplies but well yeah right like paper towels bleach TPE because of
right now bleach and hand sanitizer and stuff like that right well yeah
everything's still kind of hard for some folks to get bleach hand sanitizer right
Any masks, anything like that? Maybe. I'll tell you what, I'm going to go call them, that might be easier.
That would probably be a lot better than us guessing. Go ask them what they need.
Alright.
Dude, your list is garbage.
My list.
Okay, so what do they actually need? They said we could get more presents, go all in on presents.
They need a wrapping paper because even the stuff from the Marines and the Air Force guys
and the Air Force guys that's coming unwrapped because it's got to be new, pajamas which
we have, gift cards, like prepaid stuff, like a Visa pizza, fast food, whatever, paper towels,
hey we got one, trash bags, I broke down pajamas again, and then bras and underwear.
That ought to be fun.
Yes, it's a women's shelter.
So, I mean, primarily, that's who's there.
I didn't really think about that.
But yeah, so we gotta get that.
And then, realistically, this is their wish list.
Odds are, we're gonna get this
and then probably have to cut them a check as well.
Because I think we're good.
Which that's fantastic. They give us a list of everything that they could want and we're going to be able to satisfy it.
You can't ask much more than that.
Fantastic.
At least we got paper towels.
I don't think anything else you listed is on there.
Alright, so you had us guess the list of supplies that they needed.
We've never done that in the past.
Why did you do that?
One of the big pitfalls that I've seen
when people do this is trying to guess
what the organization they're trying to help needs.
You have to have communication.
You're never gonna get it right.
And while, sure, all the stuff that we listed,
they're gonna need in the future.
When you're doing something like this
and you're trying to address immediate needs,
immediate. We have to triage. And with what we have, had we got what we put on our list,
we would have got one item that they really needed. Whereas the bras and underwear and
wrapping paper, they need that like now. The other stuff, yeah, they can use it, but they
would be using it way into the future. And a lot of organizations don't have the room
to store stuff for years before it gets used.
OK, fourth stage, delivery.
Make sure it happens.
Make sure it happens.
Make good on whatever it is you have set out to do.
And this is especially true, like let's say you set out,
we're going to feed 1,000 people.
And at the end of your fundraising event,
or your material event, or whatever,
you have enough meals for 950 people,
You better come out of pocket.
You better find a way to get that next 50,
because you can't fail at that achievable goal.
Whatever that goal is, you have to make it happen.
Once you have committed to it, there's no turning back.
Once you are through the door, you cannot stop.
And during this phase, make sure that there's
a lot of communication between you and the people
that you are helping, because their needs may not
be exactly what you're predicting.
and you'll see that play out.
Document it.
And this isn't just a checks and balances thing.
Yeah, it's good to show, yes, we did it,
here's the proof that we did it, all of that stuff.
But most times, if people are giving you stuff,
they already feel pretty comfortable with you.
It's not really about that, there's another step to it.
So you want to make sure that your delivery goes smoothly.
OK, so we are going shopping today.
We're going to go get wrapping paper, pajamas, gift cards,
paper towels, trash bags, pajamas again,
because I wrote it down twice, and bras and underwear.
Oh, that's not that bad of a list,
but can't the boss pick up the last two items on there?
No, you are a grown man.
You can get bras and underwear.
It's not a big deal.
Oh.
Okay, you ready?
Yep.
Let's go.
Let's do this.
The next is post-advertising.
Everything's done.
The documentation that you made, make sure that the people who are involved,
the people who really made it happen, get to see it.
And they know that it went down and that it made a difference.
That's what it's about, right?
And realistically, most people who provide material or provide funding,
Most times they can't get out there and become directly involved.
For us, for people who do it all the time, we see the benefits.
We get that feeling.
We know what it's like.
A lot of them don't.
They've never been able to do it themselves for whatever reason, and it doesn't matter
what the reason is.
Make sure that you can help show them the good that they have done.
So after we got everything, we did the post-advertising phase of it, where we made the video with
the stuff behind us thanking the people who helped make it happen.
We put photos on Instagram showing it being delivered.
We made sure that those who put their time, effort, and money into making it happen got
to see that it did happen.
And then the last one is the evaluation.
Once you're done with everything, go back through it.
What went wrong?
What went right?
Did you grossly exceed your goal?
You can set your sights higher next time because you chose a definable goal, an achievable
goal, something you knew that you were going to get.
But if you start blowing those goals out of the water, you may need to raise your sights
a little bit.
your sights a little higher hope to achieve more.
So once it's all said and done, we evaluated it, and this is actually part of that.
What we were able to determine, one, was that because of y'all being the way y'all are,
we can raise our sights.
We can achieve more.
can shoot for bigger goals that we know that we're going to achieve. And, again, if
you were one of those who were in that live stream, if you were one who donated,
if you just chatted and kept the engagement up, you made all that happen.
You brought a little bit of happiness to those people who desperately needed it.
And then we were able to take care of some other basic needs for that
shelter.
Okay so I hope that this gave you some insight because if you're going to be running a community
network you're going to do this, I promise it's going to have to happen a couple of times
at least.
I hope that helps anytime you have to do this work through those six stages and continually
evaluate how you're doing, and you'll achieve your goals.
You will make a difference.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}