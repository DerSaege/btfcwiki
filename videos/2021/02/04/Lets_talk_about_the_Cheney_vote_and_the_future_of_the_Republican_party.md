---
title: Let's talk about the Cheney vote and the future of the Republican party....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3kV0xgDz6l8) |
| Published | 2021/02/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Representative Cheney voted to impeach Trump, revealing the internal struggle in the Republican Party.
- Despite efforts from Trump loyalists, a vote to oust Cheney from a leadership position failed.
- The failed vote showed Trump's weakness rather than his power.
- The secret ballot vote allowed representatives to go against Trump without facing backlash.
- Beau questions whether Senate Republicans voting freely via secret ballot could alter the outcome of the impeachment trial.
- He points out that the Republican Party is currently without a clear identity or national leader.
- Beau criticizes the party for being focused on whataboutism, deflection, and lies instead of policy.
- He mentions that the party needs to move away from ultra-nationalism and soundbites.
- Beau suggests that the Republican Party needs to become more liberal in its policies to attract younger voters.
- Convicting Trump in the impeachment trial is seen as a way for the party to redefine itself.

### Quotes
- "Currently it's the party of whataboutism, deflection and lies."
- "Younger people are more liberal."
- "The Republican Party is at a crossroads."
- "Disaster zone."
- "The clearest path to redefining itself."

### Oneliner
Representative Cheney's impeachment vote exposes Republican Party's internal struggle and the need for reinvention beyond Trump's influence, focusing on policy over whataboutism and lies.

### Audience
Political activists, Republicans

### On-the-ground actions from transcript
- Convict Trump at the impeachment trial and set a new tone (exemplified)

### Whats missing in summary
The full transcript provides a detailed analysis of the current state of the Republican Party, discussing the need for a shift in focus towards policy and away from whataboutism and lies.


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about the future of
the Republican Party because that vote that was taken last night in the House,
it shed some light on it. If you don't know what happened,
Representative Cheney voted in favor of impeaching Trump. In order to show
Trump's continued grip and sway and power in the Republican Party, his loyalists engineered a vote
that would oust her from a leadership position. It failed miserably. Trump loyalists were outvoted
two to one. Rather than showing his power, it showed how weak he is. Granted, it was done
via secret ballot, so the representatives who voted against what he wanted, they don't have
to face the inevitable backlash that would come from his most vocal supporters. But rather than
cementing his control, it weakened it. And it left us with a couple of questions. First is
I wonder what would happen if Republicans in the Senate were free to vote their conscience,
were free to vote for the truth, were free to do what duty demands in the Senate trial.
If that vote was held via secret ballot, if it would alter the outcome, if you would have more
Republicans show backbone and do what is necessary to save their party. And then the second question
is what is the Republican Party now? You know, it's not the party of Trump anymore.
It's not a party of personality. And it's not a party of policy. They allowed that personality
to undermine everything they pretended that they stood for. So they are currently without an
identity and without a national leader. It is the perfect opportunity for the Republican Party to
reinvent itself. Because currently it's the party of whataboutism, deflection and lies.
Rather than address any issue or any criticism, they just point a finger. Currently large portions
of their base are debating whether or not AOC had a right to be scared because they weren't close
enough to her. I'm curious as to how many yards away they have to be before she's allowed to be
scared. That kind of deflection, that kind of spin only goes so far because no matter what kind of
spin is placed on that, it draws attention to the fact that Republican supporters stormed the
Capitol with intent to do her harm. Nothing changes that. That whataboutism, that deflection,
that reliance on ultra-nationalism and soundbites, it isn't going to be successful.
Last election showed that. They're going to have to become a party of policy again.
And they're going to have to adopt more liberal policy. Owning the libs on social media is not
policy. If they want voters, they're going to have to become more liberal because their current base
is aging and eventually it will age out of voting. Younger people are more liberal.
The Republican Party is at a crossroads. They're currently just in a disaster zone.
Disaster zone. And the only way out, the clearest path to redefining itself,
is convicting Trump at the impeachment and setting a new tone, reinventing themselves completely.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}