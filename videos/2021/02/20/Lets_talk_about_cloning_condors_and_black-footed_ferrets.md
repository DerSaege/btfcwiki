---
title: Let's talk about cloning condors and black-footed ferrets....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mgMuZh9G_kM) |
| Published | 2021/02/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Condors are facing extinction, with a black-footed ferret named Elizabeth Ann born in December to aid in their preservation.
- Elizabeth Ann was created from frozen cells of another ferret named Willa from the late 1900s.
- The black-footed ferret species was declared extinct in 1979 but was rediscovered on a ranch in Wyoming.
- The breeding program for black-footed ferrets has been successful, but all existing ferrets trace back to only seven individuals.
- Elizabeth Ann and others like her aim to increase genetic diversity, which is vital for species survival.
- Some ferrets have already been vaccinated, indicating a serious commitment to saving the species.
- Saving species like black-footed ferrets raises ethical dilemmas, sparking a debate on the intervention's morality.
- Beau questions why ethical concerns arise only when saving species or mitigating damage, not during extinction events.
- The primary ethical responsibility should be preserving life on Earth for all beings, not just focusing on financial interests.
- Beau urges a shift from prioritizing bottom lines to prioritizing life preservation, as it is vital for the planet's future.

### Quotes

- "I think it's a test run because there are obviously going to be ethical objections to this."
- "The preservation of life, not the preservation of bottom lines."
- "Earth is going to be fine. It may go on without us, though."
- "The primary ethical responsibility should be the preservation of life."

### Oneliner

Beau questions why ethical dilemmas arise only in species preservation, urging a shift to prioritize preserving life over bottom lines.

### Audience

Conservationists, environmentalists

### On-the-ground actions from transcript

- Support conservation efforts for endangered species (implied)
- Advocate for ethical considerations in species preservation (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the ethical implications of species preservation and the need to prioritize life over financial interests.

### Tags

#SpeciesPreservation #EnvironmentalEthics #Conservation #GeneticDiversity #EndangeredSpecies


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about condors.
Condors are on the verge of extinction.
If I was to create a flock of condors on this island,
well you'd have nothing to say.
That of course is the late John Hammond.
US Fish and Wildlife
apparently took that quote as a challenge,
spared no expense mind you.
And because of that challenge,
in December a black footed ferret named Elizabeth Ann was born to a surrogate mother.
She was created
using frozen cells
from another ferret named Willa
who
lived in the late nineteen hundreds
as I heard it recently described and
felt really old.
Um...
this species
is on the verge of extinction.
In fact in nineteen seventy nine it was declared extinct.
And then some rancher in Wyoming's like, nah y'all are wrong,
I've got some here on the property.
They became the basis for the breeding program
which has been pretty successful.
However,
every black footed ferret in existence today
can trace their lineage back to seven ferrets.
So
Elizabeth Ann
and those who may come later who are like her
are uh...
hopefully going to increase
diversity in that pool
because diversity is pretty much always a good thing and it aids with species survival.
It's worth noting
that Fish and Wildlife is
taking saving this species so seriously
that uh...
some of them have already been vaccinated.
I don't have my vaccine yet, but
ferrets have.
The uh... the reason is twofold.
The first is obvious.
They help keep the prairie dog population down.
And
we do have some kind of responsibility
to keep this species alive.
However, I think something else is happening too.
I think it's a test run
because
there are obviously going to be ethical objections to this.
I, unlike John Hammond,
do believe there's an ethical discussion to be had here.
To be clear, I'm
in support of the people doing it,
but I definitely think it's worth a discussion.
My question
is why do these
ethical dilemmas only arise
when it's time to save a species
or
mitigate some damage to the planet?
Where are these ethical dilemmas
as we wipe a species out
or we damage the planet?
Is it really as simple
as
people with money and power
using their influence
to convince the population that, well, it'll be alright?
No.
I would suggest
that the most important ethical consideration
is
whether or not we can keep the planet in a state that is suitable
for all of us,
not just the ferrets, but us as well.
You know, we hear all the time, we're going to destroy the planet, we're going to kill the planet.
No, we're not. Earth is going to be fine.
It may go on without us, though.
The question isn't whether or not the Earth is going to be destroyed. The question is
whether or not it is going to remain suitable
for the
entities that are here right now.
I think that should be the primary
ethical responsibility,
the preservation of life,
not the preservation of bottom lines,
which seems to be
taking precedent
in a lot of ways recently.
And it's something that is definitely going to have to change
one way or another.
I mean, it's going to happen.
I would point out that if you do have problems with this,
just remember this also puts us one step closer to that theme park,
and
despite the fear-mongering that was put out about it, I would
personally love to go.
Anyway,
it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}