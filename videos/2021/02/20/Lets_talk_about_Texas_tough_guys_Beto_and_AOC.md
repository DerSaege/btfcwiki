---
title: Let's talk about Texas tough guys, Beto, and AOC....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7CGyBYDo48Q) |
| Published | 2021/02/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The most critical lesson from the Texas crisis is not about winterizing equipment but about tough leadership.
- Winning elections in Texas requires projecting a tough image that voters seek for protection.
- People in Texas value the image of a reliable protector during tough times.
- Leaders who flee when faced with challenges, like the recent power outage, betray that tough image.
- Beto O'Rourke, often mocked for being weak, stayed to organize supplies and make calls during the crisis.
- A liberal woman from New York raised two million dollars for Texas relief efforts.
- Criticisms of liberals throwing money at problems are misguided when lack of resources is the issue.
- Leaders who can only operate with government support falter in crisis situations.
- Those who stepped up to help during the Texas crisis were often not the tough-talking leaders.
- Supporting selfish leaders leads to selfish governance, not the tough leadership Texans desire.

### Quotes

- "When those who cast that tough guy image couldn't just bark orders into a phone, they had no clue what to do."
- "Those are the ones who came through. Those are the ones who stepped up."
- "Sometimes the tough guy doesn't wear cowboy boots and a white hat. Sometimes she wears high heels."

### Oneliner

The most critical lesson from Texas: Tough leadership matters more than tough talk in times of crisis.

### Audience

Texans, voters, activists

### On-the-ground actions from transcript

- Support community leaders who step up in times of crisis (exemplified)
- Donate to relief efforts for areas facing resource shortages (exemplified)

### Whats missing in summary

The full transcript provides additional context on the importance of leadership authenticity and community support during crises.

### Tags

#Texas #Leadership #CommunitySupport #CrisisResponse #Voting


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about what
may be the most important lesson to learn from everything that just went down in Texas,
it's not to winterize your equipment, although that's really important and I hope that people
do that. Something else. See, if you want to win an election in Texas, you've got to be a tough guy.
You've got to cast that image because that's what the voters in Texas want.
They want that good guy in the white hat, somebody that they can count on to protect and defend
their community when the bad guys come to town. Somebody they can count on when the chips are down
and when the power is down, what do they do? Flee the state? Flee the country?
Flee the country? That's embarrassing, isn't it? It's almost like people who try to cast that image
may not really be that. That image is a mirage.
Those who like that image, like that tough guy attitude, they also like to mock people,
like to make fun of people. One of the people that they just truly enjoy making fun of
is a guy named O'Rourke. Gets cast as a weakling a lot. He didn't run away. He was organizing,
supplies, wellness checks, phone banking, 700,000 calls, I think.
Somebody else, that big city liberal woman from New York raised two million dollars.
Two million dollars, not her district, it's not even her state. Two million dollars. And I know
somebody's going to say something funny down below like typical tax and spend liberal throwing money
at the problem. Yeah, when the problem is solved, you're going to get a little bit of a kick.
Typical tax and spend liberal throwing money at the problem. Yeah, when the problem is a lack of
resources and support, throwing money at it is actually kind of super effective.
But that's what happened because they didn't freeze.
They could operate without the machinery of government. That's what happened. When those
who cast that tough guy image couldn't just bark orders into a phone, they had no clue what to do.
So they gave up. They threw their hands up. They froze figuratively while their constituents
froze literally. And those who are the target of so much of their anger and wrath,
those are the ones who came through. Those are the ones who stepped up.
That's something that the people of the great state of Texas might want to keep in mind.
If you support those who spout selfish, self-absorbed talking points,
you very well may end up with selfish, self-absorbed leaders. You don't end up with the tough guy.
If you continue to look for that kind of leadership, those who can only operate when they
have the force of government behind them, you're going to be in this situation again because it
will happen. And once again, you'll be praying for the bartender and the weakling to come to your aid.
I do find it entertaining that Texas is finding out that sometimes the tough guy doesn't wear
cowboy boots and a white hat. Sometimes she wears high heels. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}