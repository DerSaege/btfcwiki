---
title: Let's talk about Detrumpification and McCarthyism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=s2GqeV9mc94) |
| Published | 2021/02/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of detrumpification, ensuring former Trump administration officials don't influence public policy.
- Compares detrumpification to McCarthyism, dismissing them as not the same.
- Notes that McCarthyism was based on accusations with little evidence, unlike detrumpification.
- Points out that detrumpification deals with factual statements about senior Trump administration officials, not accusations.
- Draws a modern parallel to McCarthyism in the context of former Trump administration officials and election claims.
- Suggests that the U.S. needs to confront and reckon with the events of the past four years.
- Emphasizes the importance of understanding history to avoid making false parallels.

### Quotes

- "Those who don't understand history are doomed to make false parallels."
- "It's not an accusation, it's a statement of fact."
- "The United States is going to have to reckon with what has occurred over the last four years."

### Oneliner

Beau explains detrumpification, dismisses parallels to McCarthyism, and urges understanding history to avoid false comparisons.

### Audience

History enthusiasts, political analysts.

### On-the-ground actions from transcript

- Research and understand historical contexts to avoid making false parallels (suggested).
- Advocate for accountability and transparency in political processes (implied).

### Whats missing in summary

Further insights on the potential consequences of drawing inaccurate historical parallels. 

### Tags

#Detrumpification #McCarthyism #HistoricalParallels #Accountability #PoliticalAnalysis


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about a question I received.
But before we do that, I'm going to give a little bit of background information
on where I'm pretty sure this question originated.
Apparently, a watchdog group has been set up
to kind of track former senior Trump administration officials
and to find out where they're going to end up working
and make sure they're not in a position to influence public policy.
The question I received was, isn't detrumpification like McCarthyism?
Detrumpification being the process of making sure that former Trump administration officials
aren't in a position to influence public policy.
The answer to that question, is it like McCarthyism?
No.
No.
Other than the idea that there's a list of people
that you don't want working in a specific position, no.
Not at all.
McCarthyism was based on accusations, many of which had little to no evidence.
There were some pretty big names implicated and accused of being communists
or communist sympathizers during the McCarthy era.
With what this watchdog is apparently going to do with the idea of detrumpification,
you were talking about senior Trump administration officials
and you're talking about senior Trump administration officials.
It's not an accusation, it's a statement of fact.
These are people who were on TV with their title below them.
They worked in the White House.
There's not going to be a lot of false positives.
People aren't just going to randomly claim that somebody is a
former Trump administration official.
That's not going to happen.
So no, it isn't the same.
However, if you wanted a modern parallel to the popular image of McCarthyism
and the way it is widely seen, it exists.
It's there.
What's the image of the McCarthy era?
There was a politician, a senator in that case,
who was trying to rally their base, trying to get ahead politically.
And they made a bunch of accusations based on little to no evidence.
And it divided the country.
It sought to discredit people.
The claim was that a bunch of radically left people had infiltrated deep
into the State Department.
And that this was something we needed to be worried about.
The parallel exists.
If you wanted to use McCarthyism to describe something that occurred recently,
it wouldn't be to describe those trying to hold former Trump administration
officials accountable.
It would be to describe the activities of former Trump administration
officials and those who parroted their claims in regards to the election.
At least McCarthy held his hearings like actually at Congress,
not in a hotel or in a landscaping firm parking lot.
The United States is going to have to reckon with what has occurred
over the last four years.
If you want to draw historical parallels, you're going to have to go beyond
the popular image.
You're going to have to go beyond the slogan of, oh, it's a list of people
who we don't want working in a certain place.
You're going to have to get deeper.
And I think it's an important thing that we should probably do.
Those who don't understand history are doomed to make false parallels.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}