---
title: Let's talk about the hilarious news out of Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xvdp-gHW32E) |
| Published | 2021/02/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing news from Georgia about former President Trump's claims and the investigations initiated.
- Mentioning investigations opened into people suspected of voting improperly, including Lin Wood.
- Georgia officials questioning Wood's residency change to South Carolina based on an email.
- Clarifying that the investigations are not accusations but part of gathering facts.
- Reminding viewers about the presumption of innocence until proven guilty.
- Noting the potential humor in the situation for some.
- Summarizing that Georgia suspects a small number of voters of not following the law.
- Concluding with well wishes for the audience.

### Quotes

- "Georgia does believe that there may have been a very small number of people who voted not in accordance with the law."
- "Everybody remember innocent unless proven guilty and all of that."
- "So there's that. That's occurring."
- "If you are a comedian, I sincerely apologize for the state you find yourself in because there is no way you are going to be able to compete with reality anymore."
- "Y'all have a good morning."

### Oneliner

Addressing investigations into potential voter irregularities in Georgia, including notable figures, reminding of the presumption of innocence, and pointing out the potential humor in the situation.

### Audience

Georgia Residents, Political Observers

### On-the-ground actions from transcript

- Contact Georgia officials for updates on the investigations (suggested).
- Stay informed about the developments in the Georgia voter investigations (implied).

### Whats missing in summary

The tone and nuances of Beau's delivery and humor can be best understood by watching the full video.

### Tags

#Georgia #ElectionInvestigations #PresumptionOfInnocence #VoterIntegrity #PoliticalNews


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some news out of the great state of Georgia.
Before we get started I would like to draw attention to the patch on my hat that is on
correctly.
This isn't a joke, no matter how much I laugh while telling all this.
Okay so what's the news?
Even if you haven't been keeping up with the news you might have heard something about
the former president of the United States, President Reject Trump, and his claims regarding
Georgia.
Unsubstantiated claims, baseless claims, claims that as of yet we have seen no evidence of,
yet those claims sparked a whole lot of problems.
One of his biggest champions, and champions of these theories, is a man named Wood.
I am certain that he and the former president are delighted to know that the state of Georgia
is taking the allegations very seriously and has opened a few, a very small number of investigations
into people who they believe may have possibly voted in a manner not in accordance with the
law.
One of those who has reportedly had an investigation opened into them is none other than Lin Wood.
At the center of the investigation is when he moved to South Carolina.
Wood says that this just happened, that he just changed his residency.
Georgia election officials believe otherwise based on some email that was sent at some
point.
So there's that.
That's occurring.
I do not like to parrot baseless claims, so I would like to point out this is just an
investigation, just trying to get all the facts, look at all the evidence.
It's not even a full-blown accusation yet, so everybody remember innocent unless proven
guilty and all of that.
It's just something that I thought y'all might want to know about.
Y'all might find it humorous in some ways.
If you are a comedian, I sincerely apologize for the state you find yourself in because
there is no way you are going to be able to compete with reality anymore.
But yeah, so it turns out that the state of Georgia does believe that there may have been
a very small number of people who voted not in accordance with the law.
They are looking into them.
Anyway, it's just a thought.
Y'all have a good morning.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}