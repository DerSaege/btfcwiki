---
title: Let's talk about DOD going back over there....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Vl27mVPsHtQ) |
| Published | 2021/02/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Defense open to sending troops to Iraq to support NATO's training mission for Iraqi national forces.
- Questions the strategy of treating Iraq like irregular indigenous forces rather than bringing their instructors to a different location for training.
- Suggests bringing Iraqi forces to the US for training is more cost-effective and strategically sound.
- Raises concerns about sending US troops to Iraq acting as a lightning rod for non-state actors in the region.
- Argues that having European or US forces in Iraq can give the appearance of propping up the Iraqi government as a puppet.
- Stresses the importance of treating Iraq as an equal to stabilize its government and avoid entanglement in Middle Eastern conflicts.
- Points out that changes in the Middle East, including border redrawings, are necessary due to colonial legacy and internal strife.
- Supports organic changes in the Middle East and cautions against US imperialism in resisting border redrawings by regional nations.

### Quotes

- "War is a continuation of politics by other means."
- "Perhaps sending U.S. troops there to Iraq to basically act as a lightning rod for every non-state actor in the region isn't a good idea."
- "And us trying to stop that is imperialism."

### Oneliner

Beau questions the strategy of sending US troops to Iraq, advocating for cost-effective training methods and cautioning against imperialism in resisting organic changes in the Middle East.

### Audience

Policy analysts, military strategists

### On-the-ground actions from transcript

- Support organic changes in the Middle East by advocating for respect of regional nations' decisions on border redrawings (implied).
- Advocate for cost-effective and strategic training methods for Iraqi national forces, such as bringing them to the US for training (implied).

### Whats missing in summary

The full transcript provides a nuanced perspective on US military involvement in Iraq and the need for strategic, cost-effective training methods rather than deploying troops.

### Tags

#USMilitary #Iraq #ForeignPolicy #MiddleEast #Imperialism


## Transcript
Well howdy there internet people, it's Beau again. So we got some interesting news today
and it came out in pieces and it gave a lot of people pause. However, the full news story
should still give people pause. The first piece of news that came out was, hey, the Department
of Defense, the U.S. military, they are really open to sending troops to Iraq.
Whoa, hang on now. Now as the story comes out, it's to support NATO's mission there. Now NATO's
mission there, if you don't know what's going on, NATO's mission there is training. Their goal is
to get the Iraqi national forces up to par. This is an important mission. It's a good mission.
It makes sense. However, I have a question about the strategy.
For all intents and purposes, Iraq is an allied nation.
Why are we still treating them like irregular indigenous forces out in the field?
If our goal is to train the trainer so we can eventually get out, it would make a whole lot
more sense to bring their forces, bring their instructors to Benin or Brag so they can learn
and then send them back. That makes more sense for a whole bunch of different reasons. It's
obviously more cost effective to do it that way, but aside from that, I'm not sure if it's
more cost effective to do it that way, but aside from that, war is a continuation of politics by
other means. If the politics are trying to get out of the Middle East, if our foreign policy
is trying to withdraw from the Middle East, perhaps it's not a good idea to send our troops there.
If our foreign policy as it stands right now is trying to not play into the Shia-Sunni divide,
that's a good move. It makes sense. We're trying to move away from fossil fuels,
which makes the Middle East less strategically important. It's become painfully clear that we
have absolutely no intention of supporting an independent Kurdistan, and we are actively working
to achieve a good deal with Iran concerning their weapons development.
Perhaps sending U.S. troops there to Iraq to basically act as a lightning rod for every
non-state actor in the region isn't a good idea. This is giving them a target to provoke the United
States. It doesn't make sense on any level. If the goal is training, if we are going there to train
the trainer, we can bring them here. We do it with other nations. Yes, historically, we've done it
there. We didn't have a choice because we weren't training the trainer. We were training the average
person who was walking out there to fight. That's not what's happening anymore. It is more cost
effective to bring them here, and it's safer for everybody involved. And if the idea is to get
Iraq to the point where it can stand on its own, it's probably counterproductive to have a bunch
of European forces there or U.S. forces there propping them up because that's the appearance.
Yes, if you actually pay attention to what's going on, it's training. It's training. Yeah,
that's true. But is that the perception? Or does the U.S. presence lead to furthering the idea and
furthering the opposition propaganda that Iraq's national government is a puppet? However, if they
were being treated as any other power would be treated, they'd be coming here. They'd be coming
here. And how would that play? Being treated as an equal. If you want to stabilize their government,
that's the way to do it. When our goal is to extract ourselves from the Middle East,
we probably shouldn't be knowingly and willingly putting ourselves in a position
to be drawn back into it. And while we're on this topic, for Americans in particular here,
as the European powers transition away from the energy that is predominantly produced there,
there are going to be changes that occur in the Middle East, organic changes. Nations are going
to redraw their borders. Things are going to change. This is a good thing. A lot of the
borders in the Middle East exist the way they are today because of colonialism. They shouldn't look
like that. That's why you have so much internal strife in these countries is because you have
ethnic groups that aren't really on the same page. Because when Europeans divided it up,
they used geographic, physical geographic markers rather than people as the lines.
So you have groups of people that got left out. You have groups of people that are in the same
country that probably shouldn't be because they don't think the same way. They have different
values. So there's going to be a lot of borders that end up being redrawn by them, which is good.
There is a tendency in the United States to resist such things because it's how we've always
known. Please keep in mind these aren't our countries. If they want to redraw their borders,
they can. And us trying to stop that is imperialism.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}