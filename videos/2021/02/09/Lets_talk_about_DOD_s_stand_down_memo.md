---
title: Let's talk about DOD's stand down memo....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=67DEEjVRKrU) |
| Published | 2021/02/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Department of Defense (DOD) released a memo for a military-wide stand-down to remind soldiers of their oath to the Constitution.
- The stand-down is symbolic and not expected to be super effective, but it serves as a serious gesture.
- One out of five Capitol participants had ties to the military, but this statistic may not be a deciding factor in their involvement.
- Movements like the Capitol events tend to attract prior service members who can become leaders due to their training.
- There is a debate on what qualities soldiers should possess, with Colin Powell advocating for critical thinkers and McNamara having a different view.
- Increasing access to education could provide the military with better recruits who are critical thinkers and well-rounded individuals.
- Lack of access to education in society leads the military to recruit individuals who may not have broad exposure to the world.
- DOD may opt for short-term fixes like intensively monitoring social media rather than addressing societal issues for recruitment.

### Quotes

- "Your oath is to the Constitution, not any individual person."
- "Increasing access to education could provide the military with better recruits."
- "They're probably just going to more intensively investigate social media and stuff like that of people in the military."

### Oneliner

The DOD's military stand-down memo aims to remind soldiers of their oath, addressing recruitment challenges through education access while facing societal issues. 

### Audience

Military personnel, policymakers.

### On-the-ground actions from transcript
- Increase access to education for broader societal benefits (implied).
- Address societal issues to improve recruitment pool diversity (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the effectiveness and implications of the DOD's stand-down memo, along with insights into recruitment challenges and the importance of critical thinking in the military. 

### Tags

#DOD #Military #Recruitment #Education #SocietalIssues


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about
DOD's stand-down memo. What it means. Whether it's going to be effective. Whether it's really
as important as the media is making it out to be. What it would take to actually be effective.
And so on and so forth. If you have no idea what I'm talking about, the Department of
Defense released a memo. And that memo basically says that the entire military at some point
soon is going to stand down. And the superiors are going to go to people and say, hey, just
a reminder, your oath is to the Constitution, not any individual person like, let's say,
a president. And here's a method of reporting any activity that may be suspicious and may
lead to events similar to what occurred on the 6th. Is this going to be super effective?
No. No. But it's not designed to be. This is a symbolic move to let everybody in the
military know they're going to take this seriously. There's going to be a lot of follow-on to
this. The media is leaning into one statistic as the reason why the military really needs
to take it seriously and really needs to do something about it right now. And it's super
important. And that statistic is that one out of five of the people who participated
in the Capitol, well, they have some kind of tie to the military. They have some form
of veteran status. When you say it like that, one out of five, that sounds like a lot. I
would point out that the overwhelming majority of people who participated in the events at
the Capitol were men. What percentage of men in the United States have veteran status?
About one in five. It doesn't appear that that was a deciding factor. Now, I would suggest
that veteran status should lower your chances of being involved in something like this.
But in and of itself, that statistic doesn't mean much. And I don't think that's what's
motivating DOD's decision. I think they know that historically, when movements like this
exist, when they form, when they attract members who are prior service, those people tend to
end up in positions of leadership. And if they have real training, they can act as real
force multipliers. I think that's why DOD's doing it. And that's why they should do it.
That's why they should take it seriously. Now, what would it take to really be effective
at this? What would it take to get rid of the people who are susceptible to these kinds
of movements? It gets to the heart of a debate that has existed in military circles since
the beginning of militaries. What do we want our soldiers to be? And there are two schools
of thought, and they can be represented by two different people. One could be represented
by Colin Powell. Colin Powell wanted people who were bilingual critical thinkers. He didn't
want to give a waiver for anything. He wanted the best and brightest. The other side of
the spectrum could be summed up as McNamara, who was basically of the opinion that, well,
anybody can work at M-16. They don't have to be that smart. I would point out that McNamara's
pilot program became known as McNamara's morons. Colin Powell's vision is what we tend to associate
with our most elite forces. So how do they do this? How do they get the PhDs who can
win a bar fight? They have to recruit them the same way they do today for the elite forces.
What's the easiest way to go about this? What's the easiest way to get that recruitment pool
of people who can critically think and who have seen a little bit more than just a public
school? Increase access to education. You can learn critical thinking in a public school.
You can learn critical thinking having never gone to school. However, the more you are
exposed to the world, the more you are exposed to different ideas, the more likely you are
to critically think. Increasing access to education would provide the military with
better recruits. The problem is that it is also one of their biggest recruitment tools.
You've seen it in the commercials. Because we don't have a lot of access to education
in this country, they use that. We'll pay for your college education. So they end up
with recruits who in many cases have never left their hometown. If you want those bilingual
critical thinkers, those people who have been around, you have to provide society as a whole
the ability to see more, to do more, to visit more places, to learn more things. And then
you'll have it in your recruitment pool. That's probably not going to be the way DOD goes
about it. They're probably just going to more intensively investigate social media
and stuff like that of people in the military. They'll probably look for a short-term fix
rather than a societal one. But at the end of the day, the military is taking it seriously.
They're doing it, I think, for the right reasons, not because of the statistic that keeps getting
cited. And it's going to be a long, drawn-out process. But that's what's going on. And
I would point out that the stand-down does not mean the entire military will be standing
down at one time, which was a question I got. Anyway, it's just a thought. Y'all have a
good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}