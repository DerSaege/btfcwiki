---
title: Let's talk about Colombia stepping up....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7VP-4rSuv80) |
| Published | 2021/02/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the misconception that the US is solely responsible for global issues, like accepting refugees.
- Contrasts the number of refugees Colombia has accepted with the US's intake.
- Colombia extended legal status to around 600,000 to 1.7 million refugees from Venezuela during an economic downturn.
- Points out that Colombia has a GDP of about 320 billion, making their efforts notable.
- Mentions the US's GDP of 21.4 trillion, showing a stark comparison.
- Walmart's revenue of 520 billion is given as a reference point.
- Notes that economic refugees seek safety and better economic opportunities, not necessarily to come to the US.
- Colombia not only offers legal status but also employment status and healthcare to refugees.
- Criticizes the US for not meeting its moral responsibility to the world.
- Biden's cap of 125,000 refugees was considered ambitious, compared to Colombia's actions.

### Quotes

- "We're not in it alone. It's not always us. They don't always want to come here."
- "The United States is failing in its moral responsibility to the world."
- "Colombia just isn't extending the legal status like residency. They're getting employment status as well and health care."

### Oneliner

The US must recognize its global responsibilities in light of Colombia's extensive refugee support, revealing American exceptionalism's flaws.

### Audience

Global citizens, policymakers

### On-the-ground actions from transcript

- Advocate for increased refugee support in your community (implied).
- Support organizations aiding refugees locally (implied).

### Whats missing in summary

The emotional impact of recognizing the disparity in refugee support between countries and the call to action for a more equitable distribution of responsibility globally.

### Tags

#Refugees #GlobalResponsibility #USPolicy #AmericanExceptionalism #Colombia #GDP


## Transcript
Well howdy there internet people it's Beau again. So today we're going to talk about how we're not
in it alone and how they don't always want to come here. After that video on Biden's border
challenges, I got a whole bunch of messages asking why it always has to be us. Why we have to take
care of the world? Why isn't anybody else helping? I was in the process of pulling the different
numbers from different countries and showing how many refugees everybody accepts in comparison to
Trump's cap of 15,000 last year. I was in the process of doing that when some news from Columbia broke.
So rather than total everything up, we're just going to use one country, Columbia.
Columbia got a massive influx of people six, seven years ago when the economy went down in Venezuela.
A lot of them came with no papers whatsoever. Columbia has decided to extend legal status to
all of them. All of them. Low end estimate, 600,000. Low end, ridiculously low end.
The high end estimate is 1.7 million. A realistic one is around 1.1.
But even if you take that low end, 600,000, for Columbia and the US was taking 15,000.
We're not in it alone. It's not always us. They don't always want to come here.
We're not even in the number one spot.
I want to point out that the realistic number of about a million people
that are going to be extended legal status is occurring in a country that has a gross domestic
product of about 320 billion dollars with a B. For comparison, the United States has a gross
domestic product of 21.4 trillion with a T. You really can't even compare those numbers.
To get something a little bit closer, Walmart's revenue is 520 billion.
They're leading the way. America, land of the free, home of the brave and all that, right?
I guess unless you speak a different language or you have a tan. This idea that everybody in the
world wants to come here is based in American exceptionalism. It's not true. They just want
to get somewhere safe or in this case somewhere with a better economy. Those economic refugees,
everybody was so worried about. I would point out that Colombia just isn't extending the legal
status like residency. They're getting employment status as well and health care.
They can handle it. 320 billion GDP.
The United States is failing in its moral responsibility to the world.
I would point out that Biden's cap of 125,000 was seen as ambitious.
600,000 on the low end.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}