---
title: Let's talk about Biden's foreign policy signals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=w-iONmH0oEc) |
| Published | 2021/02/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing an overview of Biden's foreign policy and what it signals for the future, particularly in the Middle East.
- The US will no longer support offensive Saudi Arabian operations in Yemen, signaling a shift in policy.
- Despite the change, the US will still be involved in Yemen, opposing non-state actors.
- The decision to stop supporting offensive operations in Yemen is more about Iran than Saudi Arabia.
- Biden's move indicates a desire to reach a lasting agreement with Iran and avoid confrontational postures.
- The goal is to eventually reduce the US presence in the Middle East by shifting away from fossil fuels.
- The decision is driven more by business interests and climate policies rather than a moral awakening.
- Biden's foreign policy aims to maintain American dominance, even with changes in approach and focus on different regions.
- Despite potential positive outcomes, the ultimate aim remains American influence and presence.
- Foreign policy, including Biden's, is ultimately about maintaining American dominance, not altruism.

### Quotes

- "Today, we're going to talk specifically about the Mideast because he's made some moves there that are significant in and of themselves and also show his long-term plan."
- "Regardless of how much good news comes out, any time you're talking about foreign policy, the goal of it is to maintain American dominance."

### Oneliner

Beau provides insight into Biden's foreign policy decisions, signaling a shift in approach towards the Middle East while maintaining American dominance.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Analyze and stay informed about US foreign policy decisions (suggested)
- Advocate for diplomatic solutions and peace-building efforts in conflict regions (implied)

### Whats missing in summary

The nuances of Beau's delivery and tone.

### Tags

#Biden #ForeignPolicy #MiddleEast #AmericanDominance #Peacebuilding


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about Biden's foreign policy
and what is being telegraphed,
what we can infer from what has already transpired.
A lot of it we've already talked about.
When he put together his little second state department,
we made a bunch of predictions
on what his foreign policy was going to be.
And yeah, that's what it's been.
That is what it's been.
Today, we're going to talk specifically about the Mideast
because he's made some moves there
that are significant in and of themselves
and also show his long-term plan.
Okay, so the big news,
we are no longer going to be supporting
offensive Saudi Arabian operations in Yemen.
What does that mean?
Okay, it means we're no longer going to be supplying
equipment, logistics, intelligence for offensive operations.
Doesn't mean we're done.
We'll probably still be providing air defense
and stuff like that, but no more offensive operations.
Does that mean that that's gonna end?
No, the Saudis are gonna find another supplier.
They probably have already found one,
but we're not gonna be involved with it.
With that side of it, does that mean the US
is out of Yemen completely and we're done with it?
No, because there are non-state actors in Yemen
who are using the conflict there as cover.
Okay, so we will still be engaging them
in an oppositional manner.
Why is this happening?
This really has less to do with Saudi Arabia
and the situation there on the peninsula
than it does with Iran.
Iran is backing the other side.
This is the United States unilaterally saying,
we are not going to adopt a confrontational posture
with you.
That's what we're telling Tehran by doing this.
This kind of is a very clear signal
that Biden is going to try to reach
a lasting agreement with Iran.
That is the right move.
That is the right move.
You need to look at Saudi Arabia and Iran
as the US and the Soviet Union in that region.
They are the superpowers and they are oppositional.
If we are helping one, we are just by default
oppositional to the other.
So we're trying to pull back from that posture.
It's the right move because it allows us
to eventually wind down our presence in the Middle East.
Is this because the Biden administration and the US
government in general has suddenly
realized that this isn't exactly the right thing for us
to be doing over there, kind of engaging
in a little bit of imperialism type stuff?
No, no, no, no.
It has to do with business interests.
The more successful Biden's climate policies are
and shifting away from fossil fuels,
the more we're going to wind down
our presence in the Middle East.
Two things are related.
It's not that the US government finally
found a conscience about all of this stuff.
It's just not as important.
It's not as much in US interests.
And regardless of how much good news comes out
of the Biden administration's foreign policy,
and I believe there's going to be a lot,
we still need to keep in mind that the goal is
maintaining American dominance.
It's still about maintaining that presence,
maintaining influence.
They're just shifting the way they're doing it
and shifting the areas that are more important.
I would imagine that towards the end of the Biden
administration, we are going to see more influence exerted
in Africa because there are more important resources there.
So regardless of how much good news comes out,
any time you're talking about foreign policy,
and this is anybody's foreign policy,
the goal of it is to maintain American dominance.
So even though this is good news,
this is good news for peace,
this is good news for a whole lot of people
who are caught in the crossfire,
it's still about maintaining American dominance.
When you are talking about foreign policy,
never forget that part because you might,
without acknowledging that, you may suddenly believe
that the US government has become very altruistic.
That isn't happening.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}