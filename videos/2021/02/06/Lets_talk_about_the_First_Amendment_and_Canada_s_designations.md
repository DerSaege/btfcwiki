---
title: Let's talk about the First Amendment and Canada's designations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=L8ch6psq-SQ) |
| Published | 2021/02/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the five freedoms and six rights under the First Amendment.
- Canada recently designated certain groups as bad, causing trouble for members.
- In the United States, membership in an organization cannot be a crime due to the First Amendment.
- The U.S. government is not considered responsible enough to designate domestic groups.
- Federal strategies and legislation exist to disrupt groups without making them illegal.
- Certain agencies and communities can now keep an eye on designated groups due to Canada's actions.
- Federal law enforcement uses tactics like flipping members or placing undercover agents within groups.
- Operatives going undercover in these particular groups don't need to hide their military backgrounds.
- The federal government already has the necessary tools to address threats, similar to surveillance.
- The U.S. government can take down organizations like the ones designated by Canada using existing strategies.

### Quotes

- "Membership in an organization in and of itself cannot be a crime in the United States."
- "We do not need the federal government to have the power to designate membership in any group as a crime."
- "They have the tools. They work. They just have to put in the work."

### Oneliner

Beau explains why the U.S. cannot designate certain groups as Canada did, relying on existing strategies to address threats effectively.

### Audience

Activists, policymakers

### On-the-ground actions from transcript

- Mobilize community to advocate for responsible government actions (implied)

### Whats missing in summary

In-depth analysis of the potential consequences of designating domestic groups as illegal in the U.S.

### Tags

#FirstAmendment #GovernmentStrategies #CommunityAction #USPolitics


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about the
First Amendment and something that Canada did that now a lot of people are wondering why we don't do
it in the United States. If you've watched this channel for any length of time, you have probably
heard me say, that's not a First Amendment issue, like over and over and over and over again.
This time it is. This time it is, for once. Okay, so what does the First Amendment say?
Okay, so five freedoms, six rights. You have the freedom of religion, which is divided into
the right to establish and the right to exercise. And then the ones you're familiar with, you know,
press, speech, assembly, petition. I would like to point out you have the right to petition for a
redress of grievances. A senator recently said you have the right to a redress of grievances. You
don't. You have the right to ask to have your problem solved. You do not have a right to have
your problem solved. But that's not the important part. The important part is assembly and speech.
Canada recently designated certain groups as bad. And if you are a member of these groups,
well, you're in trouble. We can't do that in the United States because of the First Amendment.
This is why there are a lot of organizations, very, very bad organizations,
that have continued to exist. So that's the constitutional reason. Membership in an organization
in and of itself cannot be a crime in the United States. The practical reason is the U.S. government
is not Canada. Our government has not shown itself responsible enough to handle the power to
handle the power to designate domestic groups. If they had this power, environmental groups,
groups seeking racial justice, anybody challenging the status quo would end up getting designated at
some point. It's not something we actually want in this country. So does that mean these groups are
just free to do whatever? No, no, no. The feds have a whole bunch of tactics, strategies, tools,
and legislation to help them disrupt groups without making the group itself illegal.
You're probably familiar with a lot of them, like RICO. These strategies and tactics have been used
to disrupt foreign intelligence services, international organizations,
groups that have a lot more operational security than the current subjects.
Aside from that, I would point out that now that an allied nation, Canada, has designated these
groups the way they have, there are certain agencies and a certain community that typically
cannot work within the United States that now has its hands untied a little bit. They still can't
get actively involved, but they can keep an eye on it, and they can share information
with our allies, as a good ally would do. I'm not sure how the protocols work today. Some
information may leave Virginia and go to the Canadians, and then turn around and go right back
to the FBI. Or now, because of the fusion centers, it may be directly shared. I'm not sure how that
works today. But the new players on the field are of a much higher caliber than anybody these groups
have faced before. Aside from that, you have normal federal law enforcement that will use the normal
tactics. They will go in and they will flip members that are already part of the organization.
Billy, who is a member of Group X, gets caught in Ohio with a gun and picks up a weapons charge
or something. The feds show up and they're like, hey, you know what's weird? A part in this gun
was made in Kentucky. It entered interstate commerce. So this is our case now, and you are
looking at this sentence, unless you play ball. If you do play ball, you'll walk out of here right
now and you're going to tell us everything from now on. That tends to work with a lot of people.
The other option is actually putting people inside, their own people. With these groups in
particular, it will be incredibly easy because they actively recruit law enforcement and former
law enforcement and military and former military. When you're sending somebody into a group,
it's normally a bad thing to have that kind of background or to even have that kind of bearing
before military people are ever sent into a situation like this. They have to learn how to
not appear military. It's not going to matter with these groups. They don't have the operational
security to make it through any kind of scrutiny that is certainly going to come.
We do not need the federal government to have the power to designate membership in any group
as a crime. They have the tools. This is just like the surveillance stuff.
They have the tools. They work. They just have to put in the work.
These strategies have taken down organizations far more advanced, far more sophisticated
than those the Canadians just listed and designated. So that's the reason why we can't do the same
thing, but also some insight into how the U.S. government is going to be able to do the same
thing. So that's the reason why we can't do the same thing. Anyway, it's just a thought.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}