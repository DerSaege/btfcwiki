---
title: Let's talk about what happened in Rochester, New York....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4Du7Q2Y0FVg) |
| Published | 2021/02/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Rochester, Rochester PD incident involving pepper spraying a 9-year-old child.
- Child was handcuffed and pepper sprayed during a family problem.
- Union defended the use of pepper spray, citing difficulties in getting people into cars.
- Beau criticizes the union for justifying the use of force on a child.
- Police officers failed to handle the situation appropriately without using pepper spray.
- Lack of empathy and understanding shown towards the child's traumatic experience.
- Beau questions the need for such excessive force on a child in this situation.
- Suggests alternative de-escalation techniques like using stuffed animals or talking.
- Points out the cultural issues within the department and the problematic defense by the union.
- Calls for a reevaluation of law enforcement practices towards consent-based policing.

### Quotes

- "You pepper sprayed a handcuffed kid in a traumatic situation. Congratulations."
- "We as a society really have to look at how we deal with law enforcement in this country."
- "Sure, yes, it is early. There is the possibility that information may come forward that changes my mind, but that seems super unlikely."
- "You talk and get them out of the vehicle."
- "We should probably move to consent based policing."

### Oneliner

Rochester PD pepper-spraying a 9-year-old child raises questions about policing practices and the need for consent-based approaches.

### Audience

Community members, advocates

### On-the-ground actions from transcript

- Advocate for consent-based policing (implied)
- Support mental health professionals (implied)

### Whats missing in summary

Full details and emotional impact can be better understood by watching the full video.

### Tags

#RochesterPD #PoliceBrutality #ConsentBasedPolicing #CommunityPolicing #Advocacy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about what happened in Rochester, Rochester PD.
Had somebody send me a request to talk about it and I thought it was funny because it said
you know I know you like to wait to see everything before you make a video about something like
this.
So I looked it up and I watched the body cameras and watched the union talk about it.
Yeah let me just start off by saying that there are not going to be a whole lot of situations
in which I find pepper spraying a handcuffed 9 year old child appropriate.
That's pretty much always going to be wrong in my book.
But that's what happened.
If you're not familiar with it yet, to use their terminology, there was a family problem.
The child was being removed from the home, the child didn't want to get in the car, so
they pepper sprayed her while she was handcuffed.
The union has already gone into defense mode, talking about how hard it is to get an adult
into a car.
Just imagine how tough that is.
Yeah I have a video on it, I'll put it down below for you guys since y'all obviously need
some help.
I would point out that this wasn't an adult, it was a child.
A 9 year old handcuffed child.
The union rep talked about how stressful it is to be a cop coming off of one call going
to the next, this stuff doesn't occur in a vacuum and all of that stuff.
Nobody cares.
Nobody cares.
If you cannot handle the different types of calls and you cannot compartmentalize what
occurs on the different calls, you shouldn't be a cop.
If you can't get a handcuffed 9 year old child, and not a suspect, a child into the back of
a car without using pepper spray, you shouldn't be a cop.
They go on because somebody asked about the psychological impact on the child who is being
removed from her home and now somebody is going to have to attempt to foster trust with
her and I imagine that's going to be like super hard now, thank you for that.
And he says, the union rep says that, well you want to talk about traumatic, did you
hear what her mother said to her?
Yeah, yeah, okay, but that makes it worse.
It's a traumatic situation by the union's own admission.
Let's take this and put it into any other traumatic situation in which you are dealing
with a 9 year old child.
Let's say an officer rolls up on a Land Rover that is flipped over in the ditch.
When the officer arrives at the vehicle, the only person who is still with us is the 9
year old.
She wants to stay with her parents, but the officer knows that it's probably bad to leave
her in there with them.
But she's sitting there crying for her dad just like the girl in this video.
Does the officer pepper spray her to get her out of the vehicle?
Of course not.
They would be a monster if they did that.
What do they do?
Right?
Probably have one in your squad car.
It works.
Doesn't even have to be a child.
Everybody feels better with a stuffed animal.
Or you talk.
You talk and get them out of the vehicle.
Could have used some of those tricks that cops are so well known for.
What we got on the video was one officer saying, do what I say, or pepper spray is going in
your eyeballs, and the other one saying, just go ahead and do it.
So good cop, worse cop.
That doesn't work.
That's not how that's supposed to work at all, is it?
It's in every book, every movie.
It's in the manual.
This was wrong on every level.
I actually can't see anything redeeming about it at all.
Sure, yes, it is early.
There is the possibility that information may come forward that changes my mind, but
that seems super unlikely.
I would like to point out that with everything being wrong, and it really lending a lot of
credence to the idea that this is going to be a giant cultural issue within the department,
not just an isolated one, you have the union up there standing between two thin blue line
flags attempting to defend the indefensible.
And then they will wonder why people want to defund them and provide more funding to
mental health professionals.
This is why.
This is why that slogan exists.
You pepper sprayed a handcuffed kid in a traumatic situation.
Congratulations.
We as a society really have to look at how we deal with law enforcement in this country.
We should probably move to consent based policing.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}