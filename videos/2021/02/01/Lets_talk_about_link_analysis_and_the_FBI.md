---
title: Let's talk about link analysis and the FBI....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LKSsR7fBPZI) |
| Published | 2021/02/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of link analysis in investigations prompted by a former assistant director of the FBI mentioning it on MSNBC.
- Describes how link analysis is portrayed in movies with pictures on a wall and different colored strings connecting them.
- Addresses the assumption that there is a criminal connection between members of Congress and those responsible for activities on the 6th.
- Emphasizes that link analysis doesn't necessarily mean criminal conduct but looks for shared motivations or sympathies.
- Mentions the historical context in the United Kingdom where separate groups shared goals and kept each other informed.
- Clarifies that link analysis aims to identify conduits between peaceful and potentially criminal actors.
- Raises the possibility that individuals may have unknowingly broken the law due to misconceptions.
- Warns against advocating for military involvement in law enforcement functions within the United States.
- Stresses the importance of upholding rights and principles in combating movements like Trumpism.
- Urges to be anti-authoritarian and maintain the presumption of innocence during investigations.

### Quotes

- "You can't give up those rights, surrender those rights, ignore those rights to combat those people or they won."
- "Just because your name's on the board doesn't mean you did anything wrong."
- "If you use the military to do this, they won."
- "During the investigation, during everything that comes out, we have to uphold the rights and the principles that they attacked."
- "The underlying theme of why the Sixth was so wrong is because it is widely seen as an attack on basic principles and rights within the United States."

### Oneliner

Beau explains link analysis, clarifies misconceptions, and warns against military involvement in law enforcement, stressing the importance of upholding rights to combat authoritarian movements like Trumpism.

### Audience

Investigators, activists, concerned citizens

### On-the-ground actions from transcript

- Uphold rights and principles during investigations (implied)
- Maintain presumption of innocence for individuals involved in investigations (implied)
- Advocate against military involvement in law enforcement functions within the United States (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of link analysis in investigations, addresses misconceptions, and underscores the significance of preserving rights and principles in combating authoritarian movements like Trumpism.

### Tags

#LinkAnalysis #Misconceptions #PresumptionOfInnocence #Rights #AntiAuthoritarian #Trumpism


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about link analysis in investigations.
Why?
Because a whole bunch of people have questions about it because a former assistant director
of the FBI in a very brief interview on MSNBC mentioned it and said some things and it prompted
a whole bunch of questions.
I went back and looked at the exchange.
He didn't say anything inaccurate.
However, he made the assumption that everybody watching understood what link analysis was
and how it worked.
While most people are familiar with it generally, and we'll get to that, they've drawn a lot
of conclusions that may not be accurate.
So first, what did he say?
Let's start there.
He said that in the FBI building there is probably a board that has members of Congress
next to those responsible for the activities of the 6th and they're doing link analysis.
Okay, so the conclusion that a lot of people drew was that he believes there was a criminal
connection between the two.
It's not necessarily the case and we're going to get to that.
You have seen link analysis done before in movies.
When you have a bunch of pictures on a wall and there's different colored string connecting
them in cop shows, that's link analysis.
In places that have a bigger budget, there's a giant touch screen TV and the links are
either different colors to signify different kinds of relationships or they're solid lines
and dotted lines and dot and dashes and each one means a different thing, a different type
of connection.
You can go up and touch the photo and it will bring up information about that person.
So all the questions.
Do I think that there are members of Congress on this board?
I don't know.
I mean, they don't let me in the room to see the big board, but if there aren't members
of Congress on the board investigating the activities of the 6th, they're doing it wrong.
I can think of quite a few off the top of my head that should be on there, but it doesn't
necessarily mean they engaged in criminal conduct.
It could just be that there is a link, a shared motivation, a sympathy, something like that.
People in the United Kingdom are going to be way more familiar with this than people
in the US because there was a time when there was a certain Irish political party that was
completely legit and then there was a more militant group that shared the same goals
and while they were separate, they talked.
They kept everybody up to speed on what the other was doing.
That's what the FBI would be looking for.
The conduits between the peaceful side and those likely to engage in criminal conduct.
That's what the link analysis is for.
It doesn't necessarily mean that everybody on the board broke the law.
However, because of a misunderstanding about how these laws work, there may be people who
broke the law who don't know they broke the law.
If Bill goes to politician Bob and Bill says, hey, on the 6th, we're going to do this wild
thing and we're going to go to this building and go inside and do this stuff and the politician
is like, oh no, no, I'm in public office.
I can't be associated with that, but here's some money to get some buses.
That politician is in some hot water.
If the politician was aware that a criminal activity was going to occur and they helped
facilitate it through cash or material or anything else, that's a problem.
They don't actually have to participate in the event.
So there's that aspect of it.
The next question was, wouldn't it be more efficient for the military to do this?
I mean, yeah, but please don't advocate for that.
Federal law enforcement is going to be slower because they have to abide by rules that the
military doesn't.
A general rule to follow here is if anything that you're advocating requires the U.S. military
to engage in a law enforcement function within the United States, stop advocating for it.
That's bad.
Like there are very, very few situations in which that's appropriate.
And if it is appropriate, odds are you're not going to know about it anyway.
They're just going to do it and then once it's done, they're going to say the FBI did
it or the National Guard or somebody else because setting the precedent for that is
really bad.
The U.S. military should never look at the United States as a battle space.
They're more efficient because they can violate people's rights.
The underlying theme of why the Sixth was so wrong is because it is widely seen as an
attack on basic principles and rights within the United States.
You can't give up those rights, surrender those rights, ignore those rights to combat
those people or they won.
If you use the military to do this, they won.
You have to allow federal law enforcement to do it their way.
It is going to take longer.
But there's an upside to this.
Assuming this is happening and the former assistant director certainly appears to believe
that it is, they are taking it more seriously than I believed anyway.
I've been a big proponent of going after the big fish rather than just those people who
showed up.
If they're engaging in the type of link analysis that he suggested, it appears that they're
planning on doing that.
So that's a good thing.
But it is going to take time.
One other thing that I want to caution everybody on.
Since this information is out there and since he said this, there are undoubtedly going
to be journalists trying to get the names of everybody on these boards.
Understand just because your name's on the board doesn't mean you did anything wrong.
Depending on what kind of link analysis they're doing, you can have completely innocent people
on these boards.
In fact, if they are using opposition to certain people as a link, AOC might be on the board.
Not as a member of the movement or wider organization, but as a unifying factor in certain people's
links.
So if journalists are successful, and I'm certain now that they're going to be trying
to get these names, if they leak at any point, the presumption of innocence here is really
important because there could be totally innocent people on these boards.
People who are actually opposed to this movement could be on these boards.
So just bear that in mind.
And please, while we're going through this, while we are trying to sift through the debris
of this, remember the key thing that we have to be right now to stifle Trumpism and this
movement is to be anti-authoritarian.
During the investigation, during everything that comes out, we have to uphold the rights
and the principles that they attacked.
Otherwise, they win.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}