---
title: Let's talk about why Biden might have brought in a very disliked person....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eYI0JDBbwP8) |
| Published | 2021/02/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A theory involving a potential Biden appointment named Cass Sunstein confused many.
- Sunstein's past work and reputation as not well-liked raised eyebrows.
- Sunstein's previous controversial actions, like hindering environmental regulations, have made him unpopular.
- Despite initial confusion, Sunstein's assignment to work on immigration rules started making sense.
- Sunstein's skillset might be more relevant than his ideology for his new role.
- His history of undermining agency regulations could be beneficial in certain scenarios.
- Sunstein's stance on federal law being interpreted by the president could impact Biden's immigration reform.
- Beau presents a theory to explain Sunstein's unexpected appointment.
- The lack of alternative theories suggests that Sunstein's appointment may serve a specific purpose.
- Beau acknowledges the confusion surrounding Sunstein's appointment and offers his analysis.

### Quotes

- "Nobody saw that one coming, to be honest."
- "That should, I hope that sheds some light on some of the questions."
- "If it is not for this purpose, nobody has a clue as to why he's being brought on."
- "It's just a thought."
- "His skillset might make sense."

### Oneliner

A theory explains the controversial potential Biden appointment of Cass Sunstein based on his skillset and past actions, shedding light on the confusion surrounding his role.

### Audience

Political analysts

### On-the-ground actions from transcript

- Analyze the skills and past actions of political appointees to better understand their potential impact (suggested).
- Stay informed and engaged with political appointments and their implications (implied).

### Whats missing in summary

Context on the potential implications of Cass Sunstein's appointment and further analysis on how his skillset may shape immigration rules under the Biden administration.

### Tags

#Biden #PoliticalAppointments #CassSunstein #ImmigrationReform #Analysis


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about a theory
involving a Biden appointment.
About two weeks ago, a name started surfacing
as a potential Biden appointment.
And to be honest, it confused everybody
who pays attention to the appointments
that nobody pays attention to,
the ones that don't make headlines.
Because, well, one of these things is not like the others.
He didn't match, he didn't belong, didn't make sense,
didn't line up with anybody else.
He is a person who is not very well liked.
Not by anybody watching this channel anyway.
His name is Cass Sunstein.
And if you're an environmentalist,
you just got chills thinking about him
working in the Biden administration.
If you opposed Bush's use of military commissions,
you don't like him either.
There aren't a lot of people watching this channel
who like this person.
So it kind of felt like the other shoe was about to drop,
like the Biden administration was going to take off the mask
and show us that they're not gonna do anything.
Then it came out he was gonna work with DHS
and everybody got even more puzzled.
Now, I remember him writing a paper
that advanced a theory that basically said,
because social media is separated off
via Facebook groups or chat rooms or whatever,
that there's the potential for a wild theory
to be introduced and for it to have a cascade effect
where it ends up causing like real world problems,
like we just saw.
This isn't a particularly groundbreaking theory today.
However, the paper's from like 2007, 2008.
So, okay, he understands it, he gets it,
but that doesn't really make sense
for a DHS appointment either,
because it's past the point of academics now.
Now, while some of the countermeasures
that he proposed, sure, maybe they can be enacted,
at this point, it's up to the people in the field.
This isn't enough for a DHS appointment.
Then it comes out he's going to be working
on the immigration rules
and everything started making sense.
This is a case where you can't look at his ideology,
you have to look at his skillset.
Why do environmentalists despise this man?
Because he ran something called
the Office of Information and Regulatory Affairs,
not a high profile position.
But environmentalists despise him because,
well, the EPA would come up with some regulation,
try to advance their agenda in their own way,
make things better, and Sunstein would pop out of a closet
with a dagger and gut that regulation
and make it completely ineffective,
make it to where the EPA could not do
what they wanted to do.
So, if you happen to have an agency within DHS
that is ignoring the president
to the point of putting literal babies on an airplane
and sending them back to a country
on the verge of a civil war
after the president told them not to,
having him on your side might make sense.
Having somebody with the skillset
to prohibit a federal agency
from being able to enact their own agenda might make sense.
It's also worth noting that he is a strong proponent
of the idea that federal law should be interpreted
by the president, not necessarily by judges.
That may also play into Biden's immigration reform.
So, that's my theory.
That's the only one that I have.
Nobody else has one either.
If it is not for this purpose,
nobody has a clue as to why he's being brought on,
and it's probably bad news.
So, that should, I hope that sheds some light
on some of the questions that I've gotten about it.
Yeah, it confused us too.
Nobody saw that one coming, to be honest.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}