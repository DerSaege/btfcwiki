---
title: Let's talk about Trump's impeachment and the courage to lead....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cIN_AZD7jIo) |
| Published | 2021/02/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Impeachment part two, day one, and the courage to lead are discussed.
- Leadership qualities are emphasized, including being willing to lead by example.
- A video is mentioned that lays out the case against the former president.
- Analysis of the former president's speech encouraging fighting and inciting the crowd is provided.
- Trump is criticized for lacking the courage to lead or take responsibility.
- The mob's actions and their loyalty to Trump are detailed.
- Republicans in the Senate are portrayed as seeking reasons to acquit despite evidence.
- The short-term focus of Republicans on appeasing an energized base is criticized.
- A majority of Americans want Trump barred from running for office again.
- The potential disillusionment of Trump's base and those who enabled him is discussed.

### Quotes

- "If you wanted to be a leader, every once in a while you had to be first through the door."
- "He wasn't going to be first through the door. He wasn't even going to show up."
- "Republicans in the Senate are looking for any reason to acquit."
- "Republicans in the Senate are chasing the ghost of a base rather than leading, rather than protecting the country."
- "At the time when the country needs them the most, Republicans in the Senate are chasing the ghost of a base rather than leading, rather than protecting the country."

### Oneliner

Impeachment, leadership courage, and the Senate's allegiance to a fading base are scrutinized.

### Audience

Citizens, Leaders

### On-the-ground actions from transcript

- Hold representatives accountable for their decisions (exemplified)
- Stay informed and engaged in political processes (exemplified)
- Advocate for putting the country's interests over party affiliations (exemplified)

### Whats missing in summary

The full transcript expands on the repercussions of blind loyalty and the potential consequences for those involved.

### Tags

#Impeachment #Leadership #Courage #Senate #Accountability #PoliticalProcesses


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about impeachment, part two, day one.
And the courage to lead.
Because that's what's missing.
You know somebody I know who was a real leader once told me that if you wanted to be a leader,
every once in a while you had to be first through the door.
That you couldn't ask others to do things you wouldn't do yourself.
I think he was right.
I think that's what real leadership is.
That video that they introduced, that really lays out the case doesn't it?
If you haven't seen it you should watch it.
It's just a timeline, shows the former president's speech where he says we, we are going to go
to the capitol.
That he's going to go with them.
And that they have to fight because democracy is being stolen, their country is being taken
away.
I don't see how anybody can look at that speech and see it as anything other than rousing.
Some might say inciting.
But see the thing is, just like any other time, Trump couldn't find the courage to
lead.
He wasn't going to be first through the door.
He wasn't even going to show up.
I have known men like Trump my entire life.
Men with grand schemes of power and glory as long as somebody else does the dirty work
for them.
The funny part is, if he had the courage to lead, his little attempted coup might have
gone better.
But he didn't.
Then goes on to show the mob, chant whatever he tweets, because he was in control of that
mob.
Goes on to show them saying, they're doing it for the president, your boss, they tell
a cop.
They were under no illusion as to who was in command, who was in control, whose orders
they were following.
That's why he wasn't leading.
He was just being a boss, ready to take the credit for the dirty work that others perform.
Even with this mountain of evidence, Republicans in the Senate are looking for any reason to
acquit.
It doesn't make sense, if you really think about it, but in the short term, what they're
doing is sniveling and groveling at the feet of that base, that energized base that was
willing to do that.
Because just like when Trump got elected and they couldn't see what's coming, they don't
see what's coming now.
As it stands, at this moment, a majority of Americans want Trump barred from running for
office again.
Because of that, they feel they need that base.
See, here's the thing, time is going to pass, and as time passes, that base, they're going
to watch their friends and family do time.
They're going to visit them through glass.
They're going to realize that their strong man leader left them twisting in the wind,
when all they did was what he wanted.
That's something that's probably pretty disillusioning.
I wonder if that disillusionment is going to be limited to just the man who duped them,
duped being their word, or if that disillusionment will extend to those who enabled him, those
who assisted, those in the Senate who they might see as accessories after the fact.
I think that's pretty likely.
I don't think that that energized base exists.
I think when he left them to do his dirty work, it was over.
They don't want to admit they're wrong yet, but at that moment, it was over.
And then when they cried out for pardons for following his orders, oh, that just cemented
it because their cries went unheard.
Dear leader left them behind.
Not just would he not be first through the door, he wasn't even going to show up.
And now the country is left wondering whether just half of Republicans are willing to put
the country over party, over their perceived base that probably doesn't exist anymore.
McConnell signaled that it's a vote of conscience, meaning Republicans are free to vote however
they want.
And even with that, most of America has decided that the Republicans in the Senate will continue
to cover for the failed leader.
Most of them do not expect just half of Republicans to have the courage to lead, to be first through
the door, to be the one that's going to say, yeah, I'm going to convict him.
He did this.
We all saw it.
At the time when the country needs them the most, Republicans in the Senate are chasing
the ghost of a base rather than leading, rather than protecting the country.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}