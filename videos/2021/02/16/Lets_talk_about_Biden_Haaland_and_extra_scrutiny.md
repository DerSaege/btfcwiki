---
title: Let's talk about Biden, Haaland, and extra scrutiny....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nENnZa8tkBA) |
| Published | 2021/02/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Examines Biden's potential appointee for Secretary of the Interior, Deb Haaland.
- Republicans are gearing up to oppose Haaland, making her one of the most contested appointments.
- Beau finds no corruption or scandals in Haaland's background, a departure from the norm.
- Haaland's open-book transparency surprises Beau, who even reached out to journalists for dirt but found none.
- Despite Haaland's qualifications and alignment with the Secretary of the Interior's duties, Republicans are already moving to oppose her.
- Beau questions if the opposition stems from a fear that Haaland will take her role seriously in protecting the Interior.
- Considers the difference between ideologically driven individuals like Haaland and those motivated by personal gain.
- Beau hints at subjecting Haaland's opposition to the same level of scrutiny regarding campaign contributions.
- Raises the point that opposition to Haaland might not be solely from senators but possibly influenced by financial backers.
- Beau concludes with a thought on the potential motivations behind the opposition to Haaland.

### Quotes

- "I found nothing. Nothing. And understand, that's literally never happened before."
- "If she does face the level of opposition I think she's going to, the same level of scrutiny that I put her through, I'm going to put them through."
- "Those, however, who are just in it for themselves, to feather their own nest, to secure their own position, they're pretty easily influenced."
- "We're going to look at their campaign contributions and see if it's really the Senators who oppose her or if it's the people who pay them."
- "Anyway, it's just a thought. Have a good day."

### Oneliner

Beau scrutinizes Deb Haaland's appointment, finding no dirt and speculating on opposition motives.

### Audience

Political analysts, activists

### On-the-ground actions from transcript

- Investigate campaign contributions of senators opposing nominees (implied)

### Whats missing in summary

Beau's in-depth analysis and insights into the potential motives behind opposition to Deb Haaland can be best understood by watching the full transcript. 

### Tags

#DebHaaland #SecretaryOfInterior #PoliticalAppointee #RepublicanOpposition #CampaignContributions


## Transcript
Well howdy there Internet people, it's Beau again.
So today we're going to talk about another one of Biden's potential appointees and extra scrutiny.
This appointee is for the position of Secretary of the Interior.
I think she very well may be the single most contested appointment that he has made.
Republicans are already laying the groundwork to oppose her.
Her name is Deb Haaland.
I wasn't super familiar with her beyond the headlines.
So I dug into her last night by the time you'll watch this.
I went through her public statements.
I went through her voting record.
I went through her campaign contributions.
Went through everything I could find.
Looking for something, anything, that was not in line.
I was looking for that normal mundane level of corruption
that we just tolerate and accept from DC politicians.
I found nothing.
Nothing.
And understand, that's literally never happened before.
It was so odd that I sent text messages to three journalists
that, no joke, read, give me some dirt on Deborah Haaland.
Two of them got back to me.
They found nothing.
She was very much an open book.
Her stated positions, those are her positions.
I know, it's weird, right?
As far as personal scandals, yeah, she's got something like 30 years ago.
And to be honest, in comparison to today's scandals,
it's really just humanizing in a way.
Turns her entire life story into a story of perseverance.
But none of what you typically expect to find.
I got to thinking about the fact that she faced extra scrutiny
because she wasn't openly corrupt.
That's probably a sign that we have a real problem.
But with all this in mind, with her positions being an open book
and them being very in line with the duties of the Secretary of the Interior,
why do you think Republicans are moving to oppose her already?
Do you think it's because they think she isn't going to do a good job?
Or do you think it's because they're afraid she will do a good job?
That she'll take that position just a wee bit too seriously
and actually move in ways that attempt to protect the Interior?
That's what it seems like to me.
And then I started thinking about that extra scrutiny thing again
and how it applied to her
and how I looked through her voting record and her campaign contributions.
You know, I have always said that ideologically motivated people
and that's what she appears to be, the true believers,
they are the most dangerous people on the planet
because you can't change their mind and they won't change the subject.
They're fanatics.
Those, however, who are just in it for themselves,
to feather their own nest, to secure their own position,
they're pretty easily influenced.
If she does face the level of opposition I think she's going to,
the same level of scrutiny that I put her through,
I'm going to put them through.
We're going to look at their campaign contributions
and see if it's really the Senators who oppose her
or if it's the people who pay them.
Because I got a feeling, mainly because I already looked,
that that's what it's going to turn out to be.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}