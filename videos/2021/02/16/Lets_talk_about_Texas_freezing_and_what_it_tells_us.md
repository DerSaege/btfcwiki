---
title: Let's talk about Texas freezing and what it tells us....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hNyRL_xFWoU) |
| Published | 2021/02/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas faced a crisis due to lack of preparation for severe weather, leading to power outages.
- The state failed to take steps to mitigate the problem over the years, causing the current situation.
- Private equity groups influenced decision-making by prioritizing profits over necessary changes.
- Texas energy companies neglected winterizing equipment, despite being warned about the potential issue.
- Beau questions the reliability of energy companies' long-term forecasts given their failure to prepare for current crises.
- The importance of proactive measures to address climate change is emphasized.
- Beau criticizes the lack of federal assistance for Texas due to its political leaning.
- He points out the dangers of blindly following party lines without questioning harmful policies.
- The narrative of prioritizing profits over people's well-being is a recurring theme in decision-making.
- Beau urges Texans to be mindful of the need for resilient energy policies in the face of climate challenges.

### Quotes

- "Texas wasn't prepared. Texas didn't take the steps to mitigate this problem along the way."
- "The dollar was more important. They decided, well, I mean, how many people can really freeze?"
- "You have to build the arc before it rains type of thing."
- "There's only one president that would say something like that."
- "Your party, the party that likes to support large energy companies, they're the reason you're not going to watch this for a few days because you don't have power."

### Oneliner

Texas faces power outages due to lack of preparation and profit-driven decision-making, raising concerns about climate readiness and political influences.

### Audience

Texans, Climate Activists

### On-the-ground actions from transcript

- Advocate for resilient energy policies and climate preparedness (implied)
- Question political decisions that prioritize profits over people's well-being (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the consequences of neglecting preparation for crises and the influence of profit motives on decision-making processes.

### Tags

#Texas #ClimateChange #EnergyPolicy #PoliticalInfluence #Preparedness


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Texas.
What's going on there?
So basically, the queen was being coronated.
And her sister met this guy and decided they were
going to get married right away.
They told the queen.
Queen freaked out.
Everything froze.
No, of course not.
That's not what happened.
No.
What happened was Texas wasn't prepared.
Texas didn't take the steps to mitigate this problem
along the way.
This could have been solved by a bunch of little things
occurring over the last few years
and just fixing the problem, mitigating it
before it happens.
Now that it's happened, you can't go out and winterize
the equipment when it's frozen.
It doesn't work that way.
It's too late.
But it's not like they didn't know this was going to happen.
Happens what, every 10 years, getting a wee bit more severe?
They've been told.
Somewhere people decided that the dollar was more important.
So they didn't winterize the equipment like the states
to the north.
That's what happened.
They knew that this was an issue.
They knew this was coming.
And they decided, well, I mean, how many people
can really freeze?
It's really what it boiled down to.
I'm certain that the private equity groups that
keep handing money to Senator Corbyn's campaign,
I'm sure they probably would have told him
that sound advice.
Keep that money in the bank.
Don't mitigate.
Don't make changes.
Don't disrupt the status quo.
Just keep everything rolling along.
Deal with it when it comes.
Well, we see how that worked out.
So what's going to happen when it's not
the weather that changes, but it's the climate?
Because it's the same thing.
In many cases, it's some of the same people.
Now, we don't need to take steps to mitigate, right?
We'll deal with it when and if it happens,
even though they're being told that it's going to happen.
It's the same thing.
And it's the same poor decision making
being made by the same people.
Just like winterizing the equipment,
you can't wait till it gets here to do it.
You have to build the arc before it rains type of thing.
These energy companies, they couldn't forecast 10 years.
Do you really think you should trust their forecast
for 2050 or beyond?
Or do you think it's possible that they're just
saying whatever they need to to keep that money in the bank,
rather than engage in the preparations
and the small changes that need to happen?
It's something that the people in the very red state of Texas
should keep in mind as this current administration attempts
to change our energy policy to something
a little more resilient, something that might be able
to withstand the climate issue.
And I would point out that since Texas is a red state,
Biden has said there's not going to be any federal assistance
from this.
No, of course not.
He didn't say that.
There's only one president that would say something like that.
Hearing that under anybody other than Trump is ridiculous.
And I think that's a good thing.
Hearing that under anybody other than Trump is ridiculous.
But it was accepted as normal because there has become
a pattern has developed of just repeating and being OK
with whatever your party says.
Your party, the party that likes to support
large energy companies, they're the reason
you're not going to watch this for a few days
because you don't have power.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}