---
title: Let's talk about Republican math and Trump math....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2DQu1Cjbgc8) |
| Published | 2021/02/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican math, Trump math, and the misconceptions surrounding a headline about almost half of Republicans voting for Trump third party in 2024.
- The survey actually shows that less than half of the people who voted for Trump in November still support him, indicating a significant drop in support.
- The 100% of voters who supported Trump in November wasn't enough for him to win the election, so having less support now is a clear indication of losing.
- Many of Trump's ardent supporters are aging, raising doubts about their participation in the 2024 election.
- The Republican Party's reluctance to see the reality of Trump's diminishing support and the potential consequences of continuing to cater to him.
- The Republican Party's only chance for victory is to distance themselves from Trump, his rhetoric, and his enablers through a process of detropification.
- Criticizing the Republican Party for not taking a proactive stance in denouncing Trump's behavior, rhetoric, and policies, which could lead to their downfall in upcoming elections.

### Quotes

- "Less than half of people who voted for him in November still support him."
- "The Republican Party's only chance for any kind of victory is to begin the process of detropification."

### Oneliner

Republican Party's survival hinges on distancing itself from Trump and his policies, as continuing to cater to him guarantees losses in future elections.

### Audience

Republicans, Political Analysts

### On-the-ground actions from transcript

- Begin the process of detropification within the Republican Party (implied).
- Actively condemn Trump's behavior, rhetoric, and policies within the party (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of Trump's diminishing support within the Republican Party and the necessary steps for the party's survival by distancing itself from him.

### Tags

#RepublicanParty #Trump #Detropification #Elections #Support


## Transcript
Well, howdy there, internet people, it's Bo again.
Today we're going to talk about Republican math, Trump math, because it is apparently
different than everybody else's math. Okay, so there's a headline. It started going around last
night and it says that almost half of Republicans would vote Trump third party in 2024.
When you say it like that, wow, that sounds like he still has a whole lot of power doesn't
it?
Problem is that's not actually what the survey said.
A better way, a more accurate way to say that would be that less than half of the people
who voted for him in November, which was actually what was surveyed, still support him.
If they supported him, if they supported his policies, if he still had the hold that he
did on people. They wouldn't care what party it was. Less than half of people
who voted for him in November still support him. That's what that survey
actually shows and I would like to point out that 100% of the people who voted
him in November was just enough to lose the election. Less than that will
certainly lose the election. However, for whatever reason, the Republican Party
isn't seeing it this way. They also aren't seeing that that 100% number
comes from people who voted in November, which is before January, particularly January 6th.
And a whole lot of people saw Trump's mask slip just a little too far that day,
and they wouldn't vote for him third party or Republican.
Aside from that, many of Trump's most ardent supporters are aging.
They may not be voting for anybody in 2024.
However, the Republican Party is, for whatever weird reason,
reading this as a sign of Trump's strength, of his longevity.
And that's fine.
You can continue to cower, and you'll end up with one of two things happening.
Either he will be your nominee in 2024, and the Republican Party will again lose.
They will again lose.
Because nobody is going to go from being anti-Trump to being pro-Trump.
That's not going to happen.
However, as more things come out, it's kind of likely that more people go from being
pro-Trump to being anti-Trump.
So that's pretty much a guaranteed loss.
Or they continue to treat him as though he has power and continue to cater to his whims,
continue to give him speaking gigs, and come 2024 if he doesn't get the nomination he
runs third party because they convinced him that he has power.
In which case he splits their vote and they lose again.
The Republican Party's only chance to save itself, its only chance for any kind of victory
anytime soon, is to begin the process of detropification.
Getting rid of his rhetoric, getting rid of those who enabled him, and turning their backs
on everything that he stood for.
their only chance and that's probably not going to get him anything in 2024 because it's going to
take a while because they don't have any initiative and they will just wait it out rather than actively
condemning his behavior, his rhetoric, and his policies. I understand that the Republican Party
really likes to defund education. I understand that it likes to demonize
education, doesn't like intellectuals, but I would strongly suggest that those who
are making policy decisions and determining who has power and who has
strength get themselves a math tutor. Anyway, it's just a thought. Y'all have a
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}