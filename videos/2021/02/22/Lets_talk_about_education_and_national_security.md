---
title: Let's talk about education and national security....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KDwUkHILm50) |
| Published | 2021/02/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Scott Adams' tweet sparked a discourse on education's importance, drawing parallels between teachers and military personnel.
- Questions raised include why teachers don't have similar funding and benefits as the military.
- Beau advocates for investing in education akin to the military, citing its critical role in national security.
- Education is fundamental to developing technology, skills, and infrastructure necessary for national defense.
- Beau contrasts military recruiters with teachers, noting the latter's pivotal role in educating and training personnel.
- The US Army Special Forces are portrayed as elite teachers who weaponize education to quickly field capable military forces.
- Green Berets' reputation stems from their ability to raise armies through education, not just combat prowess.
- Beau stresses the need to prioritize and fund education for national security and its broader societal impact.

### Quotes

- "Education is critical to national security and it needs to be treated like that."
- "The most elite unit in the US military are teachers."
- "They found a way to weaponize education."
- "It needs to be funded like that."
- "Education is fundamental to developing technology, skills, and infrastructure necessary for national defense."

### Oneliner

Beau stresses the critical importance of education in national security, advocating for equitable funding and recognition of teachers as elite educators who weaponize knowledge for societal benefit.

### Audience

Teachers, policymakers, activists

### On-the-ground actions from transcript

- Advocate for increased funding and recognition of teachers as critical to national security (implied)
- Support initiatives that prioritize investing in education similar to the military (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of how education parallels military significance, illustrating the vital role teachers play in national security beyond combat operations.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the importance of education because of a tweet from a guy
named Scott Adams.
That's the person who created Dilbert, a character that highlights the inefficiency and ineffectiveness
of large bureaucracies.
And he has an interesting take here.
Why can't members of the military unionize?
You know why.
It's obvious.
Teachers can form unions and they have similar importance to homeland security.
First, yeah they do.
Good for you for recognizing that.
My question is, if they have similar importance, why don't they have virtually unlimited funding?
Why isn't their funding included in a must-pass bill?
Why don't they have Try Care for Life?
Banks that cater to them specifically.
Thank you USAA.
Preferential home loans.
All of this stuff.
You know how they could get some of that?
Unions.
I would point out that the person who made their fortune highlighting how large bureaucracies
exploit the worker has a problem with workers leveling the playing field.
It would be great if we recruited people straight out of high school, paid for them to become
educated to become teachers, much like we do the military, because it is that important
to national security.
We had somebody who seemed to be asking a genuine question.
How is being a teacher anything like being in the military when it comes to national
security?
And they brought up the image of you don't see teachers storming the beaches at Normandy.
You're right.
You're right.
You don't.
The landing craft that brought them to the beach was developed by people who were well
educated.
The aircraft, the physics required to put the artillery where it needed to be, the skill
set all came from education.
The technology that the US military depends on to be on the battlefield comes from those
educated, most times from public school.
It is that important.
Aside from that, I would point it out a different way.
If you enlist after you're a recruiter, when you first show up, who is the first person
you meet?
A person in a funny hat who is yelling at you, right?
And what are they doing?
They're a teacher.
Their job is to educate because it is that critically important.
If you're talking about the US Army, go to the other end, the most elite of the elite,
another group of people in funny hats, long tabs, US Army Special Forces.
What is their job?
They're teachers.
The most elite unit in the US military are teachers.
They found a way to weaponize education.
They show up and they train local forces so they can train more.
It fields a military very quickly.
That's why a lot of governments are so fearful of the Green Berets.
It's not because they are stone cold killers.
I mean, they are, but it's mainly because they can raise an army by educating them.
Education is critical to national security and it needs to be treated like that.
It needs to be funded like that.
And aside from just the national security aspects of it, it permeates every other facet
of this country and of the world.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}