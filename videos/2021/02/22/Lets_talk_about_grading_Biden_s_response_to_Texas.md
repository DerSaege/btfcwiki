---
title: Let's talk about grading Biden's response to Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=03hmEJyIXSY) |
| Published | 2021/02/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- President Biden is facing criticism for his response to the situation in Texas.
- There are calls for Biden to visit Texas, but from an emergency management standpoint, it's not recommended.
- Biden's decision to not visit Texas is the right move to prevent diverting emergency resources.
- Criticism towards Biden includes issuing a major disaster declaration after the situation escalated.
- The emergency declaration was issued before the crisis unfolded, indicating Biden's proactive approach.
- The disaster in Texas was preventable and foreseeable, leading to a situation beyond the state's capabilities.
- Biden's response to the disaster is generally seen as effective compared to previous presidents.
- While improvements could be made, Biden's actions have surpassed expectations in emergency management.

### Quotes
- "From an emergency management standpoint, he should not go down there."
- "A major disaster declaration literally means that the situation is beyond the capabilities of the state and local officials."
- "He is well beyond any expectations of any recent president."

### Oneliner
President Biden's proactive disaster response in Texas surpasses expectations, despite facing criticism for not visiting, diverting resources needed on the ground.

### Audience
Emergency management officials

### On-the-ground actions from transcript
- Prepare emergency resources and aid on the ground (implied)

### Whats missing in summary
The full transcript provides a detailed analysis of President Biden's response to the crisis in Texas and sheds light on the importance of effective emergency management practices.

### Tags
#EmergencyManagement #CrisisResponse #TexasCrisis #ProactiveLeadership #DisasterDeclaration


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about President Biden's response to what's going on in Texas
and whether or not he's doing a good job, whether or not he is meeting expectations
as far as things go because he is catching some criticism.
Some of it is coming from people who like him.
You have people who like him, who trust him, who find him reassuring, who want him to come
down and just show his face.
And I get that.
From a political standpoint, from a PR standpoint, yeah, go down there, throw some paper towels
at people, hook up a generator, get some footage and leave.
It makes sense politically.
From an emergency management standpoint, it doesn't make sense at all.
It's a bad idea.
When the president comes to town, emergency resources get diverted, police, fire, EMS,
hospitals have to make preparations, roads get interrupted.
It's not good.
It's not something Texas needs right now.
So he is doing the right thing by not going.
Presidents in the past have made a spectacle of going to survey the damage.
It's all political when they do that, and it's always wrong.
Once the situation is well in hand, sure, go down there.
Until then, stay away.
Now the criticism that he should come because that's how he needs to control things, that's
not how that works.
That's just wrong.
From an emergency management standpoint, he should not go down there.
He is also catching criticism because he has just now issued a major disaster declaration.
And this is, from what I understand, coming from Republican politicians in Texas.
They are probably painting themselves into a corner without realizing it.
They may not understand some of the nuances behind the declarations.
So let's go through a timeline.
Things started going bad on the 15th.
They got real bad on the 16th as far as power being lost and the real problems starting
to occur.
When did Joe Biden issue the emergency declaration, which is what allows FEMA to start distributing
money?
The 14th.
The day before it happened.
Because he reads his presidential daily briefing and he knew it was coming.
I'm wondering why the politicians in Texas didn't.
A major disaster declaration literally means that the situation is beyond the capabilities
of the state and local officials.
Beyond the fact that this was entirely preventable, if I was the majority party in Texas, I'm
not sure that I would be pointing to that and trying to draw attention to it.
Because this situation rests entirely on the management that led up to it.
This was something that was completely foreseeable.
This was something that could have been avoided, could have been mitigated.
And now that declaration, the major disaster declaration, has been issued because something
that was preventable and something that could be mitigated is now beyond the control and
capabilities of the state and local officials.
Overall, as far as what we have come to expect from a government response to a disaster,
to an emergency like this, A, he's doing really well in comparison to everybody for
a while, actually.
Could he do better?
Yeah.
There are things that he could have done.
It would have been nice if, since he was obviously aware of the issues and he was probably aware
of Texas's failings, he could have had FEMA step in and put out alerts and provide the
information that people needed prior to their power going out.
But when you want to criticize this, you're hitting levels of emergency management that
we don't see in the United States.
You're talking about really proactive stuff.
He is well beyond any expectations of any recent president.
But no, even if you like him, even if for whatever reason you think he would feel better
if he was just there shaking hands and handing out blankets, it's a bad idea.
It's going to divert resources that are needed right now.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}