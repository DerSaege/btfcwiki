---
title: Let's talk about Neera Tanden and the status quo....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ObYW39QAHEw) |
| Published | 2021/02/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Neera Tanden is Biden's nominee to run the budget and executive branch.
- Tanden faces criticism from all sides, including the far right and Republicans for her Twitter behavior.
- The left dislikes Tanden for being status quo and against Medicare for all.
- Biden promised no fundamental change in the economy, which Tanden represents.
- Beau doesn't see the need to talk about Tanden as her positions are well known.
- Biden's economic policies are not expected to be super progressive.
- Beau believes whoever comes after Tanden will be similar in policy stance.
- The lack of deep systemic change under Biden is not surprising.
- Beau suggests there's no real story with Tanden as her positions are in line with Biden's promises.
- Deep systemic change is unlikely in the economy under Biden.

### Quotes

- "Dog bites man. This is exactly what was promised. It's exactly what was delivered."
- "There's not really a story here. This is who she is."
- "You're not going to get deep systemic change on this front from Joe Biden."

### Oneliner

Beau breaks down why Neera Tanden's nomination and policies are not surprising under Biden's administration.

### Audience

Policymakers, Political Analysts

### On-the-ground actions from transcript

- Advocate for deep systemic change in economic policies (implied)
- Stay informed on political appointments and policies (implied)

### Whats missing in summary

Insights on Beau's perspective and analysis on the lack of surprises in Neera Tanden's nomination and policies.

### Tags

#NeeraTanden #JoeBiden #EconomicPolicies #SystemicChange #PoliticalAnalysis


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Neera Tanden.
We're going to do this because y'all really, really want to talk about Neera Tanden.
I have like a hundred messages asking for it.
If you don't know who she is, she is Biden's nominee to run the budget, basically.
Day-to-day running of the executive branch in the budget.
She is not well liked.
In fact, she is so disliked, I have a recent video that is titled something to the effect
of let's talk about a Biden nominee that isn't well liked or something like that.
You go to the comment section, there are rants about Neera Tanden, even though that's not
who the video is about.
People see the title and just assume it's about her because she is that disliked.
Why?
She catches criticism from all angles, from all sides.
So let's go through it.
From the far right, she doesn't look like them and she's linked to the Clintons.
That's really enough for them.
For those who are what passes as a normal Republican today, well, they don't like her
because she was like super mean to them on Twitter.
That's really what it's about.
And then the left.
Why doesn't the left like her?
When I say left right now, I'm not talking about Democrats, not talking about liberals.
I'm not talking about center left.
I'm talking about the left.
Why don't they like her?
Because she is status quo.
She is absolutely status quo.
Dog bites man.
I don't really know what people were expecting with this, to be honest.
That's why I didn't do a video about it.
It didn't spark a lot of interest to me because this is exactly what Biden promised.
And she doesn't hide the fact that she is ridiculously status quo.
Everybody knows she's against Medicare for all.
That's not going to be a surprise position when that comes up.
This is what Biden ran on.
Nothing will fundamentally change when it comes to the economy, budgets, stuff like
that.
This is what he promised.
It's what he's delivering.
You may not like it.
I don't like it.
And that's why people want me to talk about it, I think, or want me to talk about her.
But I don't need to talk about her.
It's not like she hides these positions.
It's not like they're a secret.
So with that, I can just talk about the positions.
I can talk about the fact that I think the economic system is broke, that we need to
have deep systemic change, that things do need to fundamentally change on this level.
It's like a third of my videos.
But going after her and acting like it's a big shock, I don't see it.
This isn't man bites dog.
This is dog bites man.
This is exactly what was promised.
It's exactly what was delivered.
There is a chance that she won't get confirmed.
The next nominee is going to be exactly like her.
If you are hoping for super progressive policies from Joe Biden when it comes to the economy
and the day-to-day running of the federal government, you are in for a rude awakening.
That is not going to happen.
And he never said it would.
He was very open about the fact that nothing is going to fundamentally change.
So the reason I haven't talked about her before, even though I'm sure she's good for views,
is because there's not???to me, there's not really a story here.
This is who she is.
Everybody knows that's who she is.
It's what Biden promised.
It's what he delivered.
And if she doesn't get confirmed, whoever is next is going to be just like her.
You're not going to get deep systemic change on this front from Joe Biden.
You might get it for foreign policy.
You might get it on immigration.
You might get it in a lot of places.
It's not going to be dealing with the economy.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}