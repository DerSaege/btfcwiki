---
title: Let's talk about Trump and Miller coming back this week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vbpWShYAZVY) |
| Published | 2021/02/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump's upcoming speech will target Biden's immigration plan to keep his base energized by terrifying them.
- Stephen Miller is providing talking points to Republicans to ensure they hold the line and obey.
- Echoing these talking points means individuals are further to the right and not true Republicans or conservatives.
- The talking points likely include blaming Biden for the influx of people at the southern border, which is actually a result of Trump's failed policies.
- Contrasting Trump's treatment of immigrants with Biden's approach to unaccompanied minors reveals stark differences in policy and intention.
- Biden's plan involves a transition for minors to the Office of Refugee Resettlement, aiming to unite them with family members in the US.
- The talking point about Biden planning a path to citizenship being amnesty is countered by the fact that it creates a legal mechanism for immigration.
- The main goal of these talking points is to scare people, portraying immigrants, refugees, and asylum seekers as threats when they are seeking safety.
- Beau questions the influence of a twice-impeached former president over the Republican Party and criticizes the lack of backbone in current party leadership.

### Quotes

- "Immigrants, particularly refugees and asylum seekers, are not scary. They're not coming here to do anything wrong to you. They are coming here because they want to get somewhere safe."
- "Those in the House and the Senate don't have any backbone."
- "The whole goal of this is to scare people."

### Oneliner

Former President Trump's upcoming speech targets Biden's immigration plan to scare Republicans into obedience with Stephen Miller's talking points, portraying immigrants as threats to rally the base.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Reach out to local immigrant rights organizations to see how you can support refugees and asylum seekers (implied)
- Advocate for humane immigration policies in your community and beyond (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of how immigration issues are manipulated for political gain and underscores the importance of critical analysis and empathy in understanding complex policies and their impact on vulnerable populations.

### Tags

#Immigration #PoliticalRhetoric #Refugees #AsylumSeekers #RepublicanParty


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about talking points.
Because former President Trump is going to give a speech this weekend,
and it's already come out that a big portion of that speech
is going to be dedicated to going after Biden's immigration plan.
And that makes complete sense.
The only way Trump can keep his base energized
is to terrify them, to scare them.
That's really all he can do.
So we can kind of rally the white people around the idea
that the brown people are out to get them.
That's what this is going to be about.
To make sure that Republicans hold the line
and do what they're told like obedient lackeys,
they're going to be given talking points from none other than Stephen Miller.
That's the architect of a lot of Trump's failed policies.
I want to start by saying, anybody who echoes these talking points,
they're not Republicans.
They're not conservatives.
They're something else.
They're further to the right.
These are the type of people who, in my opinion,
would probably cheer on events like the 6th.
They are not the same.
They have a very different agenda.
And they are starting to form up into a more defined group.
The good news for us is that, well, Stephen Miller is not an incredibly original man.
We can guess at what these talking points are going to be.
We're already starting to see a few of them.
The first is that, well, this big group of people
that's coming in at the southern border, see, that's Biden's fault
because he relaxed Trump's successful immigration policies.
That's going to be the talking point.
What's the reality?
This big group of people, this is the backlog from Trump's failed policies.
This is Biden cleaning up Trump's mess, which is really what it is.
These people were waiting on the other side of the border
because Trump just couldn't figure out what to do, couldn't lead,
couldn't make a decision, couldn't figure anything out.
He knew he couldn't let them in because then he couldn't scare white folk with them.
The next is that they're going to draw a parallel.
They're going to point out an apparent hypocrisy, the kids in cages thing.
If you don't know, immigration is something that I am incredibly progressive on.
There is no way that Biden could ever realistically propose legislation
that would be even remotely close to being up to my standards.
However, I would like to point out some key differences between these two.
Let's start with the way it worked under Trump.
A family shows up.
The toddler is ripped, screaming and crying from her mother.
She is then thrown into a cage that is overcrowded, doesn't have proper hygiene,
where she is, according to reports, taken care of by other children in the cell.
They feed her, they change her.
This is done in hopes of being able to leverage the children
so the parents agree to go home, so they self-deport.
The whole thing was set up as a deterrent, which if you read the protocols
on how refugees are supposed to be treated, which we are signatory of
and are obligated to follow by the Constitution, you're not supposed to do that.
But that's the goal.
None of this is really in dispute.
That's the goal.
That's what was happening.
So the idea was to take kids away from their parents, put them in bad conditions,
use that as leverage to get them to leave.
The intention is to get them gone.
That's the goal.
Under Biden's, an unaccompanied minor shows up.
Starting off, it's very different.
An unaccompanied minor shows up.
This is a person who made the trip themselves as a minor.
There's not going to be a lot of toddlers or infants in this.
These are going to be older teens.
They are taken and they are put in this facility.
And according to Biden, we have assurances from the Biden administration
that the conditions are much better.
I don't believe that, to be honest.
The US government has zero trust for me on this issue.
None.
So we have to go off of evidence.
Do we have any evidence to suggest that the conditions will be better?
Kind of.
We do know that Biden has ordered the facility to be compliant
with current public health regulations.
In and of itself, that means it can't be overcrowded.
We know that that order's occurred.
Do we know that they're going to live up to it?
No.
And I won't believe that they are living up to it until I see it with my own eyes
or a watchdog I trust sees it with theirs.
We don't know about the conditions yet.
But what we do know is that the plan is to get them over to ORR,
Office of Refugee Resettlement.
They're staying here.
They're going to try to send them to a family member in the States.
Is it the same thing?
No, it's not.
Is it good?
Is what Biden's doing good?
No, no.
But it's not the same thing.
It is an incredibly inappropriate transition spot.
That's what it is.
Theoretically, this is supposed to be short term.
We'll see.
We'll see the conditions.
We'll see how long it lasts.
If they're doing this a year from now, there's a problem.
This shouldn't be going on a year from now.
And I would point out that it's not like the minors would be in this place a year.
According to what we've been told, they're going to be cycled through.
They're going to be uniting them with their families,
with relatives here in the States.
So the intention is different.
The conditions are supposed to be different.
The initial starting point is definitely different.
They're not the same.
Biden's move is not good.
It's not.
In my eyes, there isn't much of a difference.
But I am very hard line on this subject.
So for the rest of America, there might be a difference.
It might seem more pronounced.
One might seem OK because it's what is rather than what should be.
And this is one of those topics where I have a real hard time separating the two.
For decades, this is what happens with unaccompanied minors.
I think there's probably a better way.
OK.
The next talking point is going to be, well, Biden is planning a path to citizenship.
That's amnesty.
Fine.
Call it that.
I don't care.
The reality is that for years, we have heard that, well, we don't care if they come.
We just want them to do it legally.
This is creating a legal mechanism for them to do it.
So if you've heard that from somebody and all of a sudden they're against this bill,
well, they don't care about them doing it legally.
They're a bigot.
It's really that simple because this is a legal mechanism.
They're creating a legal mechanism for it to happen.
That's where we're at.
Those are going to be the three main things that they go after.
Now, they'll probably throw in some other scare tactics revolving around, I don't know,
some transnational threat, something along those lines.
Some boogeyman might sneak in.
But the three main ones that I can see are kids in cages and amnesty and look at the mess
that Biden made already.
The whole goal of this is to scare people.
Immigrants, particularly refugees and asylum seekers, are not scary.
They're not coming here to do anything wrong to you.
They are coming here because they want to get somewhere safe.
Most of their countries are messed up because of our foreign policy.
It's amazing to me that a twice impeached, failed president who couldn't get reelected
still has this much sway over the Republican Party.
Somebody who led them into total defeat is still somehow in control because those in
the House and the Senate don't have any backbone.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}