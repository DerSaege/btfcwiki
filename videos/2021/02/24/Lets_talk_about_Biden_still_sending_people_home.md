---
title: Let's talk about Biden still sending people home....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PdNfnsMdDZ4) |
| Published | 2021/02/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's immigration policies are causing confusion as some individuals are still being sent back despite promises not to do so.
- There are three categories of individuals prioritized for deportation under Biden's policies.
- The first category includes national security risks such as foreign spies and members of transnational groups intending harm.
- The second category consists of individuals who have committed serious criminal violations, not just any felony but a serious one.
- Biden did not cancel Operation Talon but actually made the subject group the number two priority for ICE.
- The third category comprises individuals who arrived after November 1, 2020, without permission.
- The specific dates, November 1, 2020, and January 2021, play a significant role in eligibility for citizenship pathways.
- Biden's immigration bill aims to provide a pathway to citizenship for those living in constant fear for years.
- The window between November 2020 and January 2021 prevents accusations of encouraging illegal immigration by setting eligibility criteria.
- Beau stresses the importance of focusing on the humane treatment of individuals and proper designation of asylum seekers.
- Recognizing asylum seekers, maintaining guidelines for immigration processes, and ensuring humane treatment should be top priorities.
- The immigration bill is a step towards a future where all people are treated equally, regardless of borders.
- Beau advocates for recognizing the big picture and the necessity of treating individuals humanely and justly in the immigration process.

### Quotes

- "We need to focus on their treatment and whether or not people are being designated as asylum seekers properly, that's more important."
- "The immigration bill needs to get through. If this window of time did not exist, it would torpedo it."
- "We need to make sure that people are treated humanely, that we are recognizing asylum seekers, that immigration has guidelines."
- "This immigration bill is a good start. It's not what I want."

### Oneliner

Biden's immigration policies prioritize national security risks, serious criminal offenders, and individuals arriving after November 2020, aiming to provide a pathway to citizenship while maintaining humane treatment and just designation of asylum seekers.

### Audience

Advocates, Immigration Activists

### On-the-ground actions from transcript

- Contact local organizations supporting asylum seekers and refugees to offer assistance and support (suggested)
- Join community efforts to ensure humane treatment and proper designation of asylum seekers in the immigration process (implied)
- Advocate for fair immigration policies and guidelines within your community and beyond (exemplified)

### Whats missing in summary

The full transcript provides a detailed breakdown of Biden's immigration policies, the categories prioritized for deportation, eligibility criteria for citizenship pathways, and the importance of focusing on humane treatment and proper designation of asylum seekers.

### Tags

#Immigration #Biden #Citizenship #AsylumSeekers #HumanRights


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about Biden's immigration policies,
because there are questions, because some people are still being sent home,
and I thought we weren't going to do that anymore.
That's what he said, right?
So part of the problem is people not understanding the categories of people
who are prioritized for this.
That's part of it, the people who are going to be sent home.
That's with two of the categories.
With one of them, there's another consideration that people are missing,
and this is important.
Okay, so what are the categories?
The first one is literally national security risks.
These would be people involved in espionage.
Yeah, it makes sense that a nation state would want to send home foreign spies.
I mean, yeah, that one makes sense.
Another group of people in this category are those who are members of a
transnational group who might be intent on doing a bunch of harm to innocents.
Yeah, that also kind of makes sense.
The second category are people who committed a serious criminal violation.
Serious being the key word.
Not just felony, not just felony, but a serious one.
Right now, a Republican talking point is that Biden canceled Operation Talon,
which wasn't even really an operation yet.
It wasn't really a thing, but let's just pretend that it was.
He didn't cancel it.
He made the subject of that operation,
which is a specific group of serious offenders,
he made them the number two priority for all of ICE.
He's not ignoring it.
He made it an even larger priority.
Then we have the third category, and
I would assume that the first two categories nobody has a problem with.
The third category is one that raises eyebrows because it's odd.
People who arrived after November 1, 2020, without permission.
That seems like a very arbitrary date.
It's not.
It's not.
It's important.
Okay, so first thing that I want to point out is that if you are claiming asylum,
according to what I've read, this doesn't even apply to them.
That's part of the problem is that the Trump administration treated asylum seekers
and refugees, which are actually two different types of immigration,
but for this conversation, we're going to treat them as one,
treated them the same way they treated somebody who just crossed
the border without permission because they wanted to.
That, the ability for the government to differentiate between the two is back.
And they're going to receive different treatment from what we've been told.
Do we know any of this is fact yet?
No, because we haven't seen it.
It's all kind of new.
But that's an important piece of this.
However, that date is before Biden was elected.
His statements, well, they didn't mean anything.
He had no power whatsoever by this date.
So the next date that's important is January 1 of 2021.
That's the one that's in the immigration bill.
Biden is trying to provide a pathway to citizenship for
people who have lived in the shadows of this country,
lived in constant fear for years, for years.
This is important.
This is a really important piece of legislation that in my opinion must pass.
January 1, you had to be here before January 1 to be eligible for this.
Also before he took office.
November 1 is the other date, if you arrived after that date,
you can be sent home.
It creates a window and it robs Republicans of a talking point.
Because if he was just creating a pathway of citizenship and didn't have a window
of time in which people couldn't just show up and then be eligible,
well, they would say he's encouraging illegal immigration.
That's why this is there.
It's politics.
Is it good?
No, no, it's not what I would do.
It's not what I would want.
It's actually exactly what I would do if I was in his position,
because the immigration bill is that important.
It's politics.
People have to acknowledge that.
That's why that's there.
More important than this, rather than focusing on these categories,
we need to focus on their treatment.
And whether or not people are being designated as asylum seekers properly,
that's more important.
If somebody shows up from a wealthy country and they just came without permission
because they wanted to and didn't really care, that's very different than somebody
who showed up from Haiti or El Salvador.
And if they sent home, they will cease to be.
We need to make sure that that, the ability to differentiate there,
exists and it's being done, and that they are treated humanely
throughout the entire process.
That's the priority.
Big picture is that the immigration bill needs to get through.
If this window of time did not exist, it would torpedo it.
That's why it's there.
That is why it's there.
This immigration bill is a good start.
It's not what I want.
I am not afraid of better food, a different language, and people with tans.
I think I should be able to drive from where I'm at to Panama City, Panama,
the same way somebody drives from New York to Panama City, Florida.
But we're a long way off from that.
This is a step in that direction.
We need to try to reach that goal where all people are created equal,
where beyond America's borders do not live a lesser people.
This is a step in that direction.
We have to keep the big picture in mind.
We have to make sure that people are treated humanely,
that we are recognizing asylum seekers, that immigration has guidelines,
so they can't just say, oh, this person's a national security risk.
They have to have some justifiable reason for declaring them that.
That should be the concern, not these categories.
These categories make complete sense.
National security, public safety, and then the window of opportunity
to make sure that that immigration bill passes.
That's why it's there.
It's not ideal, but it's workable, and it's moving in the direction
we need to go.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}