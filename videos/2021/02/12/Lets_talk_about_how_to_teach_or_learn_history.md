---
title: Let's talk about how to teach or learn history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q97aRXTp8BE) |
| Published | 2021/02/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A mom is stressed about teaching history to her child.
- The mom feels like she's failing in the role of being a teacher.
- History teachers often face challenges in getting students interested in history.
- Beau suggests focusing on the "why" in history, as it makes it interesting and tells the story.
- He advises encouraging kids to ask why things happened in history.
- Beau mentions that the school may provide a curriculum, but it's not compulsory to follow it.
- He introduces the "Scooby-Doo method" of teaching history, focusing on mysteries and engaging storytelling.
- Beau recommends starting with dramatic events like D-Day to grab the child's attention.
- Teaching history in reverse chronological order can keep the learner interested with the curiosity of why.
- Understanding the reasons behind historical events helps in comprehending future motives.

### Quotes

- "If you can understand the whys of yesterday, you're going to understand the motives of tomorrow."
- "That is something that history teachers have an issue with."
- "The why is what makes it interesting. Those are the juicy parts."
- "Start with something that you can get their attention with."
- "You're doing fine. You're good."

### Oneliner

A stressed mom struggling to teach history gets guidance on making it interesting by focusing on the "why" and using engaging methods like the "Scooby-Doo method."

### Audience

Parents, educators

### On-the-ground actions from transcript

- Try encouraging kids to ask "why" about historical events (suggested).
- Use engaging storytelling and dramatic events to teach history (suggested).
- Focus on understanding the motives behind historical events (suggested).

### Whats missing in summary

The full transcript provides detailed guidance on teaching history effectively and engagingly, focusing on the importance of understanding the "why" in historical events.

### Tags

#Education #Teaching #History #Parenting #Engagement


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about a message from a mom
who seems
a wee bit stressed
at the fact that she's been pushed into the role of being a teacher right now
because of everything going on.
The message basically says, hey you're the only person that's ever gotten me
interested at all in history.
My child
is not interested at all in history and I'm supposed to teach them history and I
have no idea what I'm doing and I feel like I'm failing now.
Let's start with that last part.
If your biggest concern
after being drafted into the role of teacher
is that you can't
get your child super interested in history,
you're doing fine.
You're good.
That is something that
history teachers
have an issue with.
And it has to do with how we teach history.
You forget
why you click
on the video.
It's because I relate it to something now.
Relate it to something known.
Relate it to something that's interesting and work backwards.
Most history texts
in school,
they teach history by answering four questions.
Who, what,
when, and where.
The interesting part
is why.
Why.
And that is often left out
when people teach history.
Which is sad because the why is what makes it a story.
The why is what
makes it interesting. Those are the juicy parts.
And it tends to get overshadowed.
So start with that.
Start with
trying to encourage
them to ask why something happened
when you're looking back at history.
Aside from that,
something that I'm sure is going to cause every history teacher in the country to
send me hate mail,
the school's going to send you a curriculum, no doubt.
You don't have to follow it.
That's not a law.
All that matters is that your child gets the information.
You can use the Scooby-Doo method.
It's something that
teachers in a classroom can't do because they have a whole bunch of kids.
But if it's just you with your kid or just you yourself
wanting to learn a little more about history,
you can use the Scooby-Doo method.
At the end of every episode of Scooby-Doo,
what happens?
They rip the mask off of that thing, whatever that thing is,
and it's Congress.
Some old white dude doing something behind the scenes for money.
Pretty much every episode of Scooby-Doo.
But you still watch, right?
Because there's a mystery because you're trying to figure out why.
That's the important part.
You know that at the end of the day,
those meddling kids are going to figure out what's going on.
You can do the same thing.
If you are teaching World War II history,
don't start
with some German dude in a bar.
Start with D-Day. Start with something dramatic.
Start with something that you can get their attention with. Maybe let them watch a movie.
Saving Private Ryan, Band of Brothers.
If they're older, if they're not up to that, the longest day, something more tame.
But give them something to ask questions about.
Why?
Why did this happen?
Why were people doing this?
Well, this German dude in a bar.
Well, why was he mad?
Well, at the end of World War I,
you end up teaching history in reverse chronological order.
But if you're just doing it one-on-one,
the person stays interested
because
there's a lot of whys out there.
And that's what history is really for.
If you can understand the whys of yesterday,
you're going to understand the motives of tomorrow.
And that's why it's useful.
So,
that would be my advice.
History is history.
If they know the material,
they know the material.
The method in which
the school may be suggesting to teach it,
which is typically
very linear,
isn't the most interesting way to teach it.
And I really would just stress that if this is your biggest worry after being
drafted,
you're fine. You're doing great.
I'm sure that everything will turn out okay. This is almost over.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}