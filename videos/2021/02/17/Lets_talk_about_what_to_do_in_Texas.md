---
title: Let's talk about what to do in Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XtFwAR9182M) |
| Published | 2021/02/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the lack of government response in Texas during a crisis.
- Provides practical tips for staying warm without electricity or water.
- Encourages using tents, blankets, stones, and candles to trap heat.
- Advises dressing in layers, staying close together, and sealing off areas to retain heat.
- Criticizes a former Texas mayor for a heartless social media post about self-reliance during the crisis.
- Emphasizes the importance of community support and leadership during tough times.
- Criticizes the government in Texas for failing to adequately prepare for the crisis.
- Urges Texans to take care of each other and make necessary changes for the future.

### Quotes

- "Sink or swim, it's your choice."
- "Only the strong will survive and the weak will perish."
- "Quit crying and looking for a handout. Get off your rear and take care of your own family."
- "Y'all have this. Y'all will be all right."
- "Even in Florida, we get little flyers, notices from the state government, telling us about what to do to prepare for a hurricane."

### Oneliner

Beau addresses the lack of government response in Texas, provides practical tips for staying warm without electricity or water, criticizes a heartless social media post by a former Texas mayor, and urges Texans to take care of each other.

### Audience

Texans

### On-the-ground actions from transcript

- Use tents, blankets, stones, and candles to trap heat (exemplified).
- Dress in layers, stay close together, and seal off areas to retain heat (exemplified).
- Take care of your own family and community during tough times (implied).

### Whats missing in summary

The full transcript provides in-depth insights into the challenges faced by Texans due to the lack of government response during a crisis and the importance of community support and leadership in such situations.

### Tags

#Texas #GovernmentResponse #CommunitySupport #CrisisManagement #Leadership


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, it's going to be a little bit different.
We've got a little PSA that we're going to do first.
And then once that's done, we will
go on with our like a normal morning video addressing
something.
But both things have to do with Texas.
Since it has become very apparent
that the government response in Texas is lacking,
provide some tips.
First, do not.
Do not burn things inside your home.
Don't do that.
Not just is there the obvious risk,
but the fumes also can cause a problem.
So what can you do?
If you have a tent, set it up.
Cover it with blankets, stuff like that.
Keep in mind that you are trying to trap heat.
So don't just use the items you would normally think of.
I would imagine a lot of you being in Texas,
you may still have some of those big blue tarps left over.
Those will help trap heat.
You're trying to trap heat in a small area
because it is easier to keep a small area warm than a large
one.
So that's step one.
If you don't have a tent, build a pillow for it.
Use your dining room table.
Move it to a smaller room away from the front door
or back door.
We'll get there in a second.
But get that table somewhere into a smaller room
and use that as a base.
Everybody in the house is going to be there
because the body heat will help keep
things warm in a smaller area.
In lieu of burning things inside your home,
you can build a fire outside.
Heat stuff and then bring it in.
You can use stones, rocks, bricks even.
You probably, in Texas, you probably
have some stonework that's loose outside, those brick pavers
and stuff like that.
Last time I was there, it was everywhere.
It was like all the rage.
You can heat that stuff up.
Understand some of it, they pop as it heats.
So stand back from the fire.
And when you put it into a pot to bring it inside,
cover it up with a wet rag or something.
You don't want a burn on top of what you're going through.
Bring it in.
That will radiate heat.
It will help warm that smaller area.
Make sure no little ones or animals get to it.
You can keep a couple of batches of these going.
And that will help heat a small space.
If you don't have stones or anything like that
that you can use, heat smaller pots.
You probably have those.
We're going to have to improvise and adapt here.
So heat smaller pots.
Anything that will radiate heat.
For some reason, you don't have any of that.
Maybe you have, it's Texas, the wrought iron sign outside
that says, we don't call 911.
Heat it up.
It'll radiate heat.
That's what you're trying to do.
Anything that you can heat up outside and then bring inside
safely, use it.
Use it, preferably.
It's something that wouldn't get destroyed in the process.
Something else that you can do is inside you
can use candles, especially tea lights.
Those are the little, for the guys,
not to be stereotypical or anything,
but the little fraction of a candle that's
in a little metal cup.
Take a few of those and take a terracotta pot, a plant pot.
Odds are the plant's frozen.
So just take it out, flip it upside down,
and use something, maybe bricks, to hold it up
so there's oxygen flow to the fire.
The heat from those little tea lights will raise up.
Heat rises.
It goes into the pot, and then the pot will radiate heat.
Dress in layers.
Stay close together.
Stay warm.
Stay away from the doors.
Put towels, rolled up towels, clothes, dirty clothes,
doesn't matter, under doors.
Close off areas of the house that you don't need heated.
Anything you can to keep the area
that you are trying to keep warm, small, and sealed.
And so it will retain heat.
If you have a room that is interior, completely interior,
no exterior walls, that might be best,
depending on how your house is made.
This is going on longer than I think people expected.
I think people expected a government response a little
bit faster.
Doesn't appear like it's happening.
But please keep in mind, y'all are Texans.
Y'all have this.
It's not fun, but y'all got it.
Just stay calm and do what you can.
Y'all will be all right.
OK.
So now on to the normal morning episode.
Today we're going to talk about a government official in Texas.
A gentleman by the name of Tim Boyd,
reportedly the former mayor now of a city in Texas,
made a social media post.
And I'm going to read that because I have a litany
of problems with what he said.
Let me hurt some feelings while I have a minute.
No one owes you, our, your family, anything.
Nor is it the local government's responsibility
to support you during trying times like this.
Sink or swim, it's your choice.
The city and county, along with power providers
or any other service, owes you nothing, all capital letters,
exclamation point.
I'm sick and tired of people looking for a danged handout.
If you don't have electricity, you step up and come up
with a game plan to keep your family warm and safe.
If you have no water, you deal without and think
outside the box to survive and supply water to your family.
If you are sitting at home in the cold
because you have no power and are sitting there waiting
for someone to come rescue you because you're, wrong,
you're lazy, is direct result of your raising,
going after people's parents in this too,
only the strong will survive and the weak will perish,
like a place in Louisiana, not die, by the way.
Spelling and grammar is a result of your raising as well, sir.
Folks, God has given us the tools to support ourselves
in times like this.
This is sadly a product of a socialist government
where they feed people to believe that the few will work
and others will become dependent on handouts.
Oddly enough, that's actually what capitalist governments
feed people about socialist governments.
That's not actually true.
Am I sorry that you've been dealing
without electricity and water?
Yes, but I'll be danged if I'm going
to provide for anyone that is capable of doing it themselves.
I'm wondering how many people actually have the capability
to go and winterize the equipment that was supposed
to provide them with electricity and water.
I don't think that's a long list.
He may not have a problem with it
because maybe he has a generator
because he has a nice taxpayer-funded salary.
We have lost sight of those in need
and those that take advantage of this system
and meshed them into one group.
Bottom line, quit crying and looking for a handout.
Get off your rear and take care of your own family.
Bottom line, don't be a...
Don't...
Bottom line, don't a part of the prob...
No.
Bottom line, don't a part of problem
be a part of the solution.
Sure.
Sure.
I mean, that makes sense.
Which is this?
This is the mayor.
This is a leader within that community
making a social media post.
You know what he could have done instead of this?
Provided the information I just did.
Would that have been part of the problem
or part of the solution?
It's funny to me because while this individual
has apparently resigned,
this attitude is very pervasive in government.
The idea that it's, well,
the poor people will take care of themselves.
My only job is to sit here and wait for a handout.
My check from those people who are counting on me to lead.
And then when the going gets rough,
I'm going to make a social media post and then quit.
Yeah.
Totally, totally the archetype of the tough guy,
rugged individualist leader right here.
If you are going to collect a check
to be the steward of a geographic area
and the second things get tough, you quit.
You don't get to lecture anybody
about what they need to do when times get tough.
I would point out that this image
that people love so much of the rugged individualist tough guy,
that really aggressive male, that stereotypical image.
See, the thing is in the stories,
the narratives that created that particular brand of hero,
the funny thing is they always use that aggressiveness
to help other people.
You don't have stories with heroes
who are just capable and selfish.
That's not a thing.
That's not a genre of literature.
I'm sorry.
Not that I expect this person to write a lot.
There are people who have been in this situation for days.
There are people who probably aren't going to make it.
And the government in Texas is widely to blame for this,
not this guy in particular, symptom of a problem.
But this situation, they knew it was going to happen.
These cold snaps, they occur.
They have occurred for a long time.
The infrastructure could have been updated,
but the government didn't do its job.
They were lazy, just wanting that handout, those tax dollars,
so they could do whatever they wanted.
People in Texas, y'all got this.
Y'all will be all right.
You don't need them.
You can lead yourself.
Granted, they currently, the best advice people can give you
is to use a pitchfork to get rocks out of a fire
and live like a 14th century peasant
in one of the wealthiest states in one of the wealthiest
countries on the planet because of ineffective leadership.
But you've got this.
You will make it through.
And when it's done, please make the changes
that are necessary because I will tell you, even in Florida,
we get little flyers, notices from the state government,
from the county government, telling us
about what to do to prepare for a hurricane, what
we need to have on hand, making sure that everybody's ready.
It doesn't seem like the state of Texas did that.
And we're talking about Florida here.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}