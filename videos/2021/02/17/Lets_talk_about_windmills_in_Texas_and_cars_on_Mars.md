---
title: Let's talk about windmills in Texas and cars on Mars....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qLTKHdnoptM) |
| Published | 2021/02/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes a stone age technology as a possible survival method in Texas due to power grid failure.
- Contrasts the use of stone age technology in Texas with NASA's advanced mission to Mars.
- Points out that Iowa successfully uses wind power for 40% of their electricity and had less issues during the cold weather due to winterization of equipment.
- Mentions that natural gas failures were more significant than windmill failures in Iowa.
- ERCOT confirmed windmill failure was the least significant factor in the Texas power issues.
- Questions the motives of politicians spreading misinformation about windmill failures.
- Expresses disbelief in doubting the American worker's ingenuity and patriotism regarding transitioning energy sources.
- Compares the future options of prioritizing profit over people in Texas or embracing progress like the Mars mission.
- Shares temperature comparison between Houston and Des Moines at the time of filming.

### Quotes

- "I can't believe that these pundits and these politicians would say that we can't make it work like they're doubting the ingenuity of the American worker."
- "Those are the options. A disaster film or Star Trek."
- "Blame it on that. Maybe that'll even upset the plans to transition our energy."
- "It's a way to continue to put profits over people."
- "Y'all have a good day."

### Oneliner

Beau contrasts stone age tech in Texas with Mars rover, debunks windmill failure myths, and questions prioritizing profit over people in energy transition.

### Audience

Texans, environmentalists, policymakers

### On-the-ground actions from transcript

- Winterize energy equipment (suggested)
- Support transitioning to sustainable energy sources (exemplified)

### Whats missing in summary

The full transcript provides deeper insights into the contrast between prioritizing profit and progress in energy transition debates.

### Tags

#Texas #Energy #WindPower #Mars #Misinformation #Patriotism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about windmills in Texas and cars on Mars.
This morning, in this morning's video, I described a literal stone age technology that people
in Texas might be able to use if they have to, to survive because the power grid's down,
because the electric's out.
A stone age technology.
Meanwhile, in another part of the solar system tomorrow, NASA will be putting a rover about
the size of an SUV with a little baby space helicopter strapped to its belly on the surface
of Mars after it completed almost a 300 million mile journey.
Take samples of the Martian surface.
Back here on Earth, there are politicians attempting to convince you on social media
that well, we just can't make windmills work in the cold.
That seems super unlikely.
That doesn't seem true at all because it's not.
Iowa, the state of Iowa, gets 40% of their electricity via wind power.
They're not having the same problems because they were responsible.
They were responsible.
They chose to winterize their equipment.
And I know somebody's going to say, well, there are some power outages in Iowa.
There are in the western part, the part that isn't under MESO, which is the network up
there.
The western part is part of the Southwest Power Pool, the SPP, that network, which relies
heavily on natural gas.
Incidentally, 50% of the natural gas failed.
Fifteen percent of their windmills failed.
It's not windmills.
It's just not true.
They're making it up.
They are making it up.
I would also point out that ERCOT, which is the network over Texas, they flat out said
that windmill failure was the least significant factor in what's happening.
It's just not true.
So those who are saying this, well, there has to be some reason, right?
I mean, they can't all be wrong.
It's almost like they got handed talking points because in Texas, well, fossil fuels are king,
so we need to cast doubt on anything else.
Blame it on that.
Maybe that'll even upset the plans to transition our energy.
They're making it up.
And aside from that, I would suggest that I can't believe that these pundits and these
politicians would say that we can't make it work like they're doubting the ingenuity of
the American worker.
That doesn't sound very patriotic at all.
I'm fairly certain that the people in Texas can figure out how to do this.
The people in Iowa did.
It's just not true.
It's made up.
It's a talking point.
It's a way to continue to put profits over people.
And in that way, it is a fantastic little comparison because those are the options for
the future.
We can follow people who will mislead the public and who put profit over people, and
we can get Texas.
Or we can look at Mars and see what happens when we try to drive humanity forward, try
to reach new heights, set goals that matter.
Those are the options.
A disaster film or Star Trek.
Anyway it's just a thought.
Y'all have a good day.
And I will point out one more thing.
At time of filming, the temperature in Houston is 35 degrees.
In Des Moines, it's negative 2.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}