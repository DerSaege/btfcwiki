---
title: Let's talk about Trump's love letter to McConnell...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=itxlRvrNorA) |
| Published | 2021/02/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the love letter Senator McConnell received from former President Trump.
- Trump's letter aims to convey that everything is fine and he is not upset.
- Trump criticizes McConnell, stating that the Republican Party cannot be respected with McConnell at its helm.
- Trump believes McConnell's leadership is driving the party towards weakness.
- Trump's letter portrays his belief that McConnell's strategies are ineffective and regressive.
- Despite some truths in Trump's criticisms of McConnell, Trump fails to offer progressive solutions.
- Trump describes McConnell as a status quo person who lacks the vision for progress.
- Trump predicts that Republicans will not win if they continue to support McConnell.
- Trump's endorsement on a national level could lead to electoral losses due to his own electoral defeat.
- Beau points out the irony of Trump calling McConnell a loser despite Trump's own failure to secure a second term.

### Quotes

- "Trump's policies are regressive. They're going backwards."
- "Trump's endorsement on the national level for anybody that's going to have to compete for the Electoral College is a guarantee that they will lose."
- "Not the guy writing angry letters to the editor from Mar-a-Lago."
- "I wouldn’t want Trump's endorsement if I was a Republican because long term it is going to do more damage."
- "Y'all have a good day."

### Oneliner

Beau delves into Trump's letter to McConnell, criticizing the lack of progressive solutions and warning about the electoral consequences of Trump's endorsements.

### Audience

Political observers

### On-the-ground actions from transcript

- Analyze political candidates beyond endorsements for a thorough understanding (implied)

### Whats missing in summary

Insight into the specific content of Trump's letter and its potential impact on political dynamics.

### Tags

#Politics #Trump #McConnell #RepublicanParty #Endorsements


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the love letter that Senator McConnell received from
former President Trump.
If you don't know what I'm talking about, please go find a full copy and read it because
I'm only going to cover the highlights and it is in fact glorious.
Okay, so this is basically President Trump letting the country know that everything is
fine.
It's fine, okay?
He's fine.
He's not upset at all.
The Republican Party can never again be respected or strong with political leaders, in scare
quotes mind you, like Senator Mitch McConnell at its helm.
McConnell's dedication to business as usual, status quo policies, together with his lack
of political insight, wisdom, skill, and personality has rapidly driven him from Senate Majority
Leader to Minority Leader.
And it will only get worse.
Okay, so what's happening here is Trump is falling to something that a lot of people
fall prey to.
The belief that just because somebody else's plan is bad, well that means that yours is
good.
That's not how this works.
That's not how any of this works.
Some of what he's saying here, yeah, it's true.
McConnell is a status quo person.
He is establishment.
He is business as usual.
That is who he is in his heart of hearts.
In his little two sizes, two small heart, he is status quo.
That is a fact.
However, Trump doesn't want progress, which is what would actually win.
Trump's policies are regressive.
They're going backwards.
Many of them have already been tried a hundred years ago.
Incidentally, they failed then too.
They're not, just because McConnell is status quo doesn't mean that Trump's platforms or
policies are good.
It goes on.
This is actually my favorite part.
Mitch is a dour, sullen, and unsmiling political hack.
If Republicans, Senators are going to stay with him, they will not win again.
I just want to point out that Mr. Trump is writing this about Senator McConnell.
Mr. Trump, a president who failed to secure a second term.
Do you know how few presidents fail to secure a second term?
It's a pretty short list.
The overwhelming majority of presidents get a second term.
It takes a lot to not get a second term.
Trump is the winner though.
He's the one that can win.
Senator McConnell, who is on his 756th year in Congress, yeah, he's the loser.
For real, McConnell's on like his sixth or seventh term.
He's the one that doesn't know how to win.
The guy on Capitol Hill, he's the loser.
Not the guy writing angry letters to the editor from Mar-a-Lago.
The reality is Trump's endorsement on the national level for anybody that's going to
have to compete for the Electoral College is a guarantee that they will lose.
Trump lost by a pretty wide margin.
Anybody that Trump endorses is going to be viewed as a copy of Trump, a Xerox copy.
Not even of the same quality.
Just somebody following on, somebody who lost.
A president who was unable to secure a second term.
I would not want Trump's endorsement if I was a Republican because long term it is going
to do more damage because you will forever be linked with him, with his failed policies,
with a twice impeached one term president.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}