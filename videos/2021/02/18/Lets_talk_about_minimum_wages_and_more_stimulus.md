---
title: Let's talk about minimum wages and more stimulus....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aEwtGgWFxHA) |
| Published | 2021/02/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Walmart is keeping its starting wage at $11 an hour, but aiming to increase the average wage to $15 an hour.
- The company's revenue grew by 7.3% last year, with e-commerce growing by 69%.
- Despite the revenue growth, Walmart's stock prices fell, which was seen as bad news on Wall Street.
- Walmart stated that raising the minimum wage to $15 an hour should benefit the economy, but Beau disagrees.
- Beau argues that the minimum wage is meant to benefit the worker, not the corporations.
- Walmart's CEO, McMillan, acknowledged that customers spend more when they receive stimulus money, benefitting the economy.
- Walmart wants the government to provide stimulus while not committing to raise their minimum wage.
- Beau believes that if a company cannot pay a living wage, it has no right to exist in the United States.

### Quotes

- "If you can't pay a living wage, the company has no right to exist in the United States."
- "It's to benefit the worker, not the corporations."
- "Walmart wants the government to pick up the slack."
- "They have to make money."
- "I wish my earnings increased by only 69%."

### Oneliner

Walmart aims to raise average wage to $15 but faces backlash for not increasing the minimum wage, prompting debates on corporate responsibility and worker benefits.

### Audience

Workers, advocates

### On-the-ground actions from transcript

- Advocate for fair wages in your workplace (implied)
- Support policies that prioritize worker well-being over corporate profits (implied)
- Stay informed and engaged in the debate on minimum wage increases (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the debate around corporate responsibilities and worker benefits in relation to minimum wage policies.

### Tags

#Walmart #MinimumWage #CorporateResponsibility #WorkerRights #Economy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some news from Walmart.
A couple of different news stories that are being treated as different news stories, but
it should really just be one story because it's all related.
Okay, first up, Walmart is not going to raise its minimum wage.
Its starting wage is going to stay at $11 an hour.
However, they're going to try to raise the average wage to $15 an hour.
So a whole bunch of people will be making way more and a whole bunch of people will
be making way less.
The starting wage will still be $11 an hour.
The company says it wants to do this to maintain a ladder of opportunity, which means they
want to keep some people working below a living wage and just sit there with like a fishing
pole with a dollar hanging off of it and keep them climbing up that ladder.
Incidentally, Walmart's revenue grew 7.3% last year.
Their e-commerce section only grew by 69%.
Only 69%.
I wish my revenue, my earnings would increase by only 69%.
See this was actually seen as bad news on Wall Street.
Stock prices fell.
That much of an increase.
These increases were bad news.
More news from Walmart.
It says that $15 an hour, that minimum wage, well that should be paced to benefit the economy.
No!
No!
It's to benefit the worker, not the corporations.
That's not what the minimum wage is for.
It's to benefit the worker, those on the bottom, those making the minimum.
That's what it's there for.
But see the funny thing is, is that if you assist those on the bottom, those that need
money, they spend it and it helps the economy.
Don't believe me?
I'm going to read you something that was said as somebody was encouraging the federal government
to provide stimulus.
We can see in our customer behavior that some customers, as they received this most recent
stimulus, are spending it more on basics, more on private brands, smaller pack sizes,
things like that.
This is from a guy named McMillan.
He's the CEO of Walmart.
Walmart is simultaneously saying they're not really going to raise their minimums, but
they want the government to pick up the slack.
If you want people to spend money at Walmart, they have to make money.
They have to make money.
And I would point out that if you can't pay a living wage, the company has no right to
exist in the United States.
That's from the guy who actually came up with all of it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}