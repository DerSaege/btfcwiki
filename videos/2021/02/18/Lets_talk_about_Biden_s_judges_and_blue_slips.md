---
title: Let's talk about Biden's judges and blue slips....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=S_NEiCeHtCY) |
| Published | 2021/02/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the three kinds of judges in the federal judicial system: district court, circuit court (appeals), and the Supreme Court.
- Describes the blue slip rule that allowed senators to have a say in judicial nominations, and how it was eliminated for circuit court judges in 2017.
- States that Democrats, contrary to expectations, are not planning to restore the blue slip process for district court judges.
- Predicts that Republicans may criticize this decision, claiming victimhood and unconstitutionality.
- Clarifies that the blue slip process is not a constitutional requirement but a Senate rule.
- Suggests that Biden's judges may have an easier confirmation process without the blue slip process in place.
- Examines the idealistic purpose of the blue slip process in promoting regional representation but points out its potential for misuse.
- Supports the idea of getting rid of the blue slip process altogether for a more uniform judicial selection process.
- Expresses uncertainty about the Democratic Party's willingness to push for the abolition of the blue slip process.
- Concludes by noting that circuit court judges are likely to face fewer obstacles compared to district court judges.

### Quotes

- "It's not required, but they'll probably say it is."
- "What should be and what is are very, very, very different."
- "So as far as getting rid of it altogether, yeah, I think it's a good idea."
- "It's just a thought. Y'all have a good day."

### Oneliner

Beau explains the implications of the blue slip process elimination for Biden's judges, revealing potential obstacles and political dynamics in judicial nominations.

### Audience

Political enthusiasts, activists

### On-the-ground actions from transcript

- Contact your representatives to express your views on judicial nominations (implied)
- Stay informed about changes in the judicial nomination process and advocate for fair and transparent procedures (implied)

### Whats missing in summary

Analysis on the potential consequences of maintaining or abolishing the blue slip process for district court judges.

### Tags

#JudicialNominations #BlueSlipProcess #BidenAdministration #PoliticalPower #DemocraticParty


## Transcript
Well, howdy there, internet people. It's Beau again. So today we're going to talk about Biden's
judges and how they might get confirmed a little bit more easily than we imagined.
Before we get into this, we have to do a quick crash course on the federal judicial system.
There are three kinds of judges. You have district court, circuit court, which is appeals,
and the Supreme Court. Prior to 2017, there was something called the blue slip, and the blue slip
was a mechanism for the senators from the state where the judge would work to throw up a road
block to kind of have an unofficial veto power. It wasn't real veto power, but their opinion
mattered a whole lot. In 2017, though, in an attempt to pack the court with far right-wing
judges, the Senate judiciary got rid of this rule. They wanted to let Trump pack the courts.
They got rid of it for circuit court judges, appeals. It remained for district.
Everybody, myself included, kind of expected Democrats to restore this rule when they took
over, mainly because, generally speaking, Democrats are not very good at wielding power.
They try to play fair. This time, they're not going to. They're not going to restore the rule.
In fact, they have said that they haven't ruled out getting rid of the blue slip process for
district court judges as well. If this happens, I would imagine that Republicans are going to cry
foul. They will say that they only did it because there were a lot of vacancies, and I don't know,
whatever. They will come up with some talking points, and they will say, well, I don't know,
they'll come up with some talking point that allows them to frame themselves as the victim,
is what it's going to boil down to. They'll say it's unconstitutional. I'll go ahead and tell you
now, it's not. This is not in the Constitution. This is a Senate rule. The Constitution says that
the president needs the advice and consent of the Senate. Nothing about the state where the judge is
going to work, blue slips, none of this is in the Constitution. It's not required,
but they'll probably say it is. So what does this mean? It means that Biden's judges
will probably get through a lot easier because if they're being appointed to a state that
is very conservative, they don't have that unofficial veto power anymore. They can't
throw up those roadblocks. So people are obviously going to ask, why does this rule exist?
Theoretically, it's a good thing. Theoretically, it makes sense. The United States is very diverse.
If you live in California, you would probably not want a federal judge from Texas ruling on your
gun laws. It makes sense. If you have senators, politicians in general, who are actually looking
out for the best interests of their citizens, of their constituents, and they are actually
attempting to better their lives and provide the best service they can, it would make sense.
That's not how this gets used, though. That is not how this gets used. Something that would be
more likely to happen is, I don't know, let's say it's the 1960s and we're in the South and we don't
want a judge that supports civil rights. That's how it ends up getting used. So while in theory,
in a perfect world, the blue slip process makes sense. It is something that should exist.
We're not in a perfect world. What should be and what is are very, very, very different.
So as far as getting rid of it altogether, yeah, I think it's a good idea. I think it's a good
idea. It would make sense and it would make it to where you don't have such disparity between
the different circuits and how people are subject to the laws and how the laws get interpreted.
So it would make things a little bit more uniform. But I don't know that the Democratic Party is
going to have the political will to move forward with that part of it. But it certainly appears
that they will keep the rule as is. So the appeals court judges, the circuit court judges,
they'll get through easier. The district court judges, well, they may still face roadblocks.
I mean, to be honest, it was a surprise that the Democrats didn't just cave and put the rule back.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}