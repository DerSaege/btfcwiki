---
title: Let's talk about me agreeing with Tucker Carlson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=n-w0zyfgDYc) |
| Published | 2021/02/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Analyzing Tucker Carlson's assessments and effectiveness.
- Mentioning the importance of looking at assessments from those who oppose you for a clearer picture.
- Critiquing Tucker Carlson's wild, theory-laden rant from the previous night.
- Pointing out Tucker Carlson's contradictory statement of not speculating while leading his audience to speculate.
- Refusing to focus on Carlson's truth-seeking quest and speculations that may arise from it.
- Addressing a particular passage where Carlson acknowledges the effectiveness of BLM.
- Noting the significant impact BLM and their corporate sponsors had on the country from Memorial Day onwards.
- Correcting Carlson's statement on the time frame of change brought about by BLM.
- Agreeing with Carlson on the necessity and positive nature of the change brought by BLM.
- Emphasizing the importance of change over stagnation in a society.

### Quotes

- "Their assessments are going to be more accurate because they're going to point to your weaknesses and your strengths."
- "Here we have Tucker Carlson admitting to BLM's effectiveness."
- "I agree with Tucker Carlson."
- "If I look back 50 years into history and my country hasn't changed, something is wrong."
- "This change is overdue, needed, and good."

### Oneliner

Beau analyzes Tucker Carlson's contradictory assessments, focusing on Carlson acknowledging the effectiveness of BLM and the necessity of overdue change in society.

### Audience

Media consumers

### On-the-ground actions from transcript

- Dissect contradictory statements in media (analyzed)
- Acknowledge and support overdue societal change (implied)

### What's missing in summary

Beau's engaging delivery and nuanced perspective on media assessments and societal change. 

### Tags

#TuckerCarlson #BLM #SocietalChange #MediaAnalysis #OppositionAssessments


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Tucker Carlson, a phrase I hope I don't have to say
very often.
But we're going to talk about Tucker Carlson and assessments.
Assessments of effectiveness.
If you want an accurate picture of how effective you are, the best place to look is somebody
who opposes you.
Their assessments are going to be more accurate because they're going to point to your weaknesses
and your strengths.
They're going to be more objective.
You're going to get a clearer picture of your own capabilities by looking at your opposition's
assessment.
Now with that in mind, I want to talk about some stuff that Tucker Carlson said last night.
He went on this wild, theory laden rant.
In it he says, no joke, we don't know and we're not going to speculate.
And then he stares blankly into the teleprompter, reading off questions designed to lead his
audience to speculate.
I don't really want to talk about that though.
I don't want to talk about Mr. Carlson's quest for the truth because the truth is out there
and certainly him or his audience will eventually fill in the gaps, speculate, put it all together,
figure out who the boogeyman is and Tucker Carlson, having not speculated himself, can
pretend like he has no idea where the theories came from.
But that isn't what I want to talk about.
I want to talk about one passage in particular.
It was surrounded by a bunch of just utter garbage.
But he said, beginning on Memorial Day, BLM and their sponsors in corporate America, yeah
I know a lot of y'all are still waiting for your checks from all of that grassroots activity.
It's not that this is just a gross misrepresentation of reality or anything.
But anyway, okay, beginning on Memorial Day, BLM and their sponsors in corporate America
completely changed this country.
They changed this country more in five months than it had changed in the previous 50 years.
Yes they did.
Absolutely.
Here we have Tucker Carlson admitting to BLM's effectiveness.
50 years is an interesting number too.
It's 2021.
50 years would put us in 1971.
And maybe it's just alliteration, but I would point out that Dr. King was taken from us
in 1968.
So if you want this assessment to be completely accurate, it would be 53 years.
Yeah, this is true.
It is absolutely true.
He goes on to say that BLM bum-rushed us and threw out the old order.
Good.
Good.
I agree with Tucker Carlson.
The difference is I know that if I look back 50 years into history and my country hasn't
changed, something is wrong.
This is 100% accurate.
The difference is I'm not terrified of change and I know that this change is overdue, needed,
and good.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}