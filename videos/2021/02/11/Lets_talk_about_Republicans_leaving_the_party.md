---
title: Let's talk about Republicans leaving the party....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=J9mNGWyL8xw) |
| Published | 2021/02/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A conference call with over a hundred Republicans aimed to form a third party.
- Participants include representatives from recent Republican administrations.
- The goal is to capitalize on anti-Trump sentiment and move the conservative movement back to the center.
- They want to name their party the center-right party.
- Democrats in the U.S. are already considered center-right on the international spectrum.
- Most leftists do not view Democrats as their representatives.
- Beau believes this new party could gain seats in the House and Senate due to the current Republican Party's disconnect.
- The current Republicans might acquit Trump, leading to more damaging revelations.
- Beau suggests that a shift back to center for the conservative movement could be beneficial.
- If conservatives move to the center, Democrats will have to differentiate themselves further.

### Quotes

- "The goal is to move the conservative movement back to center."
- "They want to capitalize on the anti-Trump sentiment that is sweeping the nation."
- "Democrats in the U.S. are sure, they're left-ish, but that's not saying much."
- "It does appear that they're going to acquit Trump."
- "If the conservative movement does come back to center, I think it's a good thing."

### Oneliner

A hundred Republicans aim to form a center-right party to capitalize on anti-Trump sentiment, potentially shifting the conservative movement back towards the center.

### Audience

Politically interested individuals

### On-the-ground actions from transcript

- Form a community organization to support political shifts (implied)
- Stay informed about political developments and sentiments (implied)

### Whats missing in summary

The full transcript provides additional insights into potential shifts within the conservative movement and the implications for Democrats in the current political climate.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the news coming out of the Republican Party.
There was a conference call.
Had more than a hundred Republicans on it.
And the purpose of this call was to discuss forming a third party.
What's the difference between this potential third party and all the other ones that we
have?
Names.
There were people on this call whose name you might recognize.
They had representatives from all four of the recent Republican administrations.
These are insiders.
These are people who know how the game is played.
These are people who have their own base, have their own donors, have their own connections.
They want to capitalize on the anti-Trump sentiment that is sweeping the nation.
The goal is to move the conservative movement back to center.
So much so they want to call their party the center-right party.
It makes sense.
They're reading the room.
They understand the way the wind is blowing.
There is one small problem.
The United States already has a center-right party.
You might have heard of them.
They're called Democrats.
They control the House, the Senate, the Oval Office right now, those people.
Republicans have bought into their own propaganda.
On the international spectrum, Democrats are center-right.
They're not leftists.
They're certainly not radical.
For the U.S., sure, they're left-ish.
But that's not saying much.
We just had a fascist in the White House.
If you talk to most Marx-reading leftists, they will tell you Democrats are not their
people.
There are a few social Democrats within the Democratic Party who I consider to be left.
But most of the leftists from the Republican means, yeah, they don't really claim them
either.
They're not leftists.
They're center-right.
Aside from that, though, it makes sense.
It's a good plan.
I actually think that they would have the ability to seat people in the House and the
Senate their first time out because the current Republican Party is not reading the room.
They do not know which way the wind is blowing.
It does appear that they're going to acquit Trump.
And then from that point forward, every new revelation looks bad on them.
Every time one of the accused says, well, I did it because Trump wanted me to, it demonstrates
that those in the Senate were incapable of saying the truth.
Or they didn't care.
Or they're corrupt.
Either way, it bodes well for the center-right party.
They're not reading the room.
Odds are they would underestimate a new third party the same way they underestimated Trump.
So I think it has a chance.
Now as far as I'm concerned, if the conservative movement does come back to center, I think
it's a good thing because realistically, at that point, if Democrats want to differentiate
themselves, well, they really only have one way to go now, don't they?
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}