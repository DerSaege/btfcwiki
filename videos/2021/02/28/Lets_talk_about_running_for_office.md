---
title: Let's talk about running for office....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VvoV2V_5YcU) |
| Published | 2021/02/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People considering running for office are disheartened with the current state of things.
- Many potential candidates hold ideologies that may be considered inconsistent with traditional political structures.
- Individuals on the left or very anti-authoritarian face challenges in conforming to hierarchical systems and using force.
- Those seeking purity in their ideology may find it difficult to pass the standards required for office.
- Despite ideological inconsistencies, those genuinely concerned and seeking advice are encouraged to pursue office.
- Candidates entering politics must be prepared to face a system that can break down ideologically motivated individuals.
- Being in office may require compromising on one's beliefs to fit into the mainstream and ensure re-election.
- Declarations of radical ideologies openly could lead to immediate unelectability.
- Success in office may be limited, but small victories can be significant for those driven by strong ideological motivations.
- Holding public office often involves a thankless journey with minimal tangible accomplishments.
- Candidates must be ready to navigate a world where being true to their fringe ideology might require staying undercover.
- Knowing what compromises are acceptable and sticking to them is vital for individuals venturing into politics.
- Running for office is described as a challenging and potentially rewarding endeavor that may break many individuals.
- The decision to pursue political office and endure its challenges ultimately rests on the individual's willingness and strength.

### Quotes

- "If you have an ideology that's on the fringe, if you really want to move things in a radical direction, you can't tell anybody. Because then you become unelectable immediately."
- "Holding public office often involves a thankless journey with minimal tangible accomplishments."
- "You had better be committed to your beliefs. But you never get to say what they are."
- "Candidates must be ready to navigate a world where being true to their fringe ideology might require staying undercover."
- "Running for office is described as a challenging and potentially rewarding endeavor that may break many individuals."

### Oneliner

Potential candidates face ideological struggles and compromises when considering running for office, navigating a system that can break down radical motivations while demanding conformity for survival.

### Audience

Aspiring candidates

### On-the-ground actions from transcript

- Run for office while being prepared to face challenges and compromises (implied)
- Be ready to navigate a system that may require compromising beliefs to fit in (implied)
- Understand the potential thankless nature of holding public office and the need for strong ideological commitment (implied)

### Whats missing in summary

The full transcript delves deep into the internal conflicts and external pressures faced by individuals considering running for political office, shedding light on the complex dynamics of ideological purity versus practical compromises in the political arena.

### Tags

#RunningForOffice #IdeologicalStruggles #PoliticalCompromises #Challenges #PublicService


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some of you running for office.
There are people who are pretty disheartened
with the current state of things
and
they want to run, but
their ideology
well it seems ideologically inconsistent.
I got messages
from a couple of people
and they all share that.
They're either left, and when I say left I mean
left on the international spectrum, not American left,
or they're very anti-authoritarian.
And ideologically
well you can't be part of a hierarchy like that.
You can't use force,
you can't become part of the ruling class.
And yeah, that's true.
If you do that, you are
you are compromising.
You are becoming ideologically inconsistent. You will never pass a purity test.
That is for sure.
There's no doubt about that.
But I'm going to suggest
that from the outset
you are so concerned that you're sending messages to people about it,
asking for advice,
you're probably the exact person who should do it.
But you need to be ready for what you're walking into, what you're volunteering for.
Because you are volunteering to go to a city
that is known
for breaking
ideologically motivated people and just turning them into a cog in a wheel.
You're volunteering to throw yourself into the grinder.
You had better be committed to your beliefs.
But you never get to say what they are.
Because if you do, well that's a little too radical for the mainstream and you'll
never get re-elected. I do think it's funny that
none of the messages
actually said the ideology at all.
And that's good.
You've got to get used to it. You're never going to be able to walk out and say, oh, well I'm a
socialist or whatever.
Because then it's over.
You should be prepared to be
completely isolated
because
those who are around you, well you're going to be too radical for them. And your
current allies,
those people who support you right now,
once you're in office,
you sell out.
Part of the ruling class. You're part of the system now.
It's going to be thankless
because there's not going to be a lot of wins.
You're not going to get much done
realistically.
Because you're going to be fighting against
well, everybody.
But the few wins you do get,
they'll be real wins.
They'll be moves in the right direction
because you are ideologically motivated.
It's been said that nobody worthy
of holding public office will run anymore.
And I believe that to be true
for the most part.
If you're going to do this, you have to know
right away, right up front,
what your goals are, what you're hoping to accomplish,
how willing
you are
to live a life that is basically you being undercover
and pretending to be something that you're really not.
Because if you have an ideology that's on the fringe,
if you really want
to move things in a radical direction,
you can't tell anybody.
Because then you become unelectable
immediately.
You need to know what you're willing to compromise on.
And you have to stick to it.
You are volunteering for a lot.
Whether or not you can do it, whether or not it's a good idea,
it's entirely up to you.
For most people,
it's
probably something
that would break a lot of people.
But I would imagine
it would be pretty rewarding if you could do it,
if you're strong enough.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}