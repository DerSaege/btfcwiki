---
title: Let's talk about how we talk about other countries....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VKpQbqRnNks) |
| Published | 2021/02/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of viewing other countries and their people beyond just their leadership.
- Points out how politicians and media often portray other countries as united behind their leadership to simplify narratives.
- Notes that most countries are more divided than the United States and people have less control over their governments.
- States that individuals in the US may have more in common with average citizens in other countries than with their own government representatives.
- Warns against accepting collateral damage based on the actions of other countries' leadership.
- Emphasizes that the people in these countries are just like us, lacking control over government decisions.
- Expresses disbelief at holding average citizens accountable for their government's actions and hopes this standard is never applied to the US.

### Quotes

- "The people of these countries, they're just people just like you and me. They don't have any control over this stuff."
- "Holding the average citizen responsible for the actions of its government, that's something that I can't believe is widely accepted in the United States."

### Oneliner

Beau stresses the importance of recognizing the people behind other countries, cautioning against blaming individuals for their government's actions.

### Audience

Global citizens

### On-the-ground actions from transcript

- Acknowledge the individuals in other countries beyond just their leadership (implied)

### Whats missing in summary

The full transcript adds depth to understanding the impact of foreign policy decisions on everyday people globally. 

### Tags

#ForeignPolicy #GlobalCitizenship #Unity #GovernmentAccountability #NarrativeShift


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about how we view other
countries and the people of other countries.
We're going to do this because we have an administration
that appears to want to engage in actual foreign policy
rather than soundbite foreign policy.
Because of that, other countries are going to become
more prominent in our headlines. We're gonna talk about other nations more. When
we do that we need to remember that when the media or most pundits or a
politician says a country's name, well Japan is going to do this. China is going
to do this. They are talking about the leadership of those countries. Not the
people, not the people. One of the things that happens and one of the reasons that
politicians discuss things in this way is to cast an image that that other
country is somehow united, that they are somehow all behind the idea that their
leadership is enacting, that they're all on the same page. The reality is most countries
are more divided than the United States is, and most people in those countries have less control
over their governments than we do, if you can imagine that. It doesn't matter where you're at
on the political spectrum on this one. In the last six months, you have definitely realized
that you really don't have much control over your government, right? They're in the same boat.
You or I have more in common with the average person in Tehran or Riyadh than we do with our
own representative in DC. We really need to keep that in mind, because if we allow politicians
and the media to paint other countries as being completely united behind the actions of their
leadership, it becomes really easy to accept collateral. It becomes really easy to say,
up well they were against us anyway we see it a lot and we saw it a whole lot
over the last couple of days that's that's a dangerous premise the people
of these countries they're just people just like you and me they're just people
They don't have any control over this stuff.
And holding the average citizen responsible for the actions of its government, that's
something that I can't believe is widely accepted in the United States.
I, for one, really hope that standard is never applied to us.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}