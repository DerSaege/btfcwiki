---
title: Let's talk about ending the filibuster....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Id-QM86wyqk) |
| Published | 2021/02/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the topic of changing the filibuster, citing significant public interest after recent events.
- Disputing the argument that the filibuster should be upheld simply due to tradition or Senate norms.
- Explaining the accidental creation of the filibuster, debunking surrounding myths.
- Outlining two main options for changing the filibuster: abolishing it entirely or implementing a speaking filibuster.
- Expressing concern that abolishing the filibuster could enable extreme policy changes when the opposition gains power.
- Countering that argument by proposing that showcasing successful policies without the filibuster could politically damage opponents.
- Advocating for a speaking filibuster, predicting it may reveal politicians' true thoughts and provide transparency on issues like the Equality Act and minimum wage.
- Acknowledging the need for government restraint but urging progress on social issues by altering or eliminating the filibuster.

### Quotes

- "Doing something simply because it's always been done that way, that's a really bad reason to continue doing something."
- "There's no doubt about that. But there is the law of unintended consequences that needs to be thought about."
- "A little bit of gridlock in government is good."
- "I think it needs to be a change."
- "Altering the filibuster, even getting rid of it, that would help move us along."

### Oneliner

Beau addresses changing the filibuster, proposing drastic options, and advocating for transparency through a speaking filibuster to progress on social issues.

### Audience

Policy advocates

### On-the-ground actions from transcript

- Advocate for transparency in government by supporting the implementation of a speaking filibuster (implied).
- Engage in debates and dialogues on the necessity of changing or abolishing the filibuster within political circles (implied).

### Whats missing in summary

The full transcript provides deeper insights into the potential consequences of altering or eliminating the filibuster and encourages critical thinking regarding government transparency and progress on social issues. 

### Tags

#Filibuster #Government #Transparency #PolicyChange #SocialIssues


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the filibuster.
More importantly we're going to talk about changing it.
Because after last week a whole lot of people want to.
And it makes sense.
So we're going to go over the two most popular suggestions for changing it.
Now there are some people who believe it's tradition in the Senate and we just have to
have it.
No, we don't.
It's not in the Constitution, even if it was in the Constitution, we have a method of changing
that as well.
Doing something simply because it's always been done that way, that's a really bad reason
to continue doing something.
And realistically the filibuster was kind of created by accident.
There's a lot of mythology and legend surrounding it, most of it isn't true.
It was kind of created by accident.
Okay so what are the options for changing it?
The first one is real simple, get rid of it completely.
Make it a relic of the past, a thing that just ceases to be, no longer exists.
And that is certainly an option.
It's a drastic option, but it's an option.
The concern with doing that is that eventually one day the opposition party will have control
of the House, the Senate, and the White House.
And if you are talking about the Republicans of today, in just a few short votes they could
make this country look like a scene from a handmaidens tale.
And there isn't much anybody could do to stop them.
That's the concern.
Now the counter argument to that is that no, we scrap the filibuster now, push the agenda,
get it through, show that our policies work.
And since they're working, it would be just politically devastating for the Republicans
to get rid of it.
And that's a strong argument.
That is a strong argument.
That may be factually accurate, that may be how it works out, that may be true.
But you do have to consider the alternative and what happens when they have that power.
The other option is to amend a whole bunch of Senate rules and require it to be a speaking
filibuster.
Meaning that if somebody wants to block the Equality Act, well they have to get up and
talk about it.
They have to get up and talk for hours for as long as they want to delay voting on that
bill.
This does two things.
One, generally speaking, politicians aren't really public servants.
Most of them are lazy.
They're not going to want to spend their time doing it.
So they're not going to filibuster every bill.
The other reason that I like this is that I would imagine that around hour 12, people's
masks, well they'll begin to slip.
They'll start saying the quiet part out loud, especially if the rules require them to stay
on topic.
And I think that there are a whole lot of people in the United States that need to actually
hear how their representatives feel about them.
Not just with the Equality Act, but with the minimum wage as well.
Because I guarantee you, you have a bunch of Republicans talking about poor people for
any period of time where it's not censored, it's not scripted, and they have to stay on
topic.
Oh, there are going to be sound bites for days on that.
So that's another option.
Now whether or not people want to do this, there's a lot of debate and discussion over
it.
I think it needs to be a change.
I don't know that I would oppose getting rid of it completely, but I do want to just remind
everybody that a little bit of gridlock in government is good.
Not as much as we have right now, because we can't get basic stuff through.
But you do want government to be cautious as it moves forward.
You want a little restraint.
Now the problem is, in the United States when it comes to social issues, we have shown so
much restraint that we're still in the 1960s, and we shouldn't be.
Altering the filibuster, even getting rid of it, that would help move us along.
There's no doubt about that.
But there is the law of unintended consequences that needs to be thought about.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}