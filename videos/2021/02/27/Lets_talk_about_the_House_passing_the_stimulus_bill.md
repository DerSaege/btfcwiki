---
title: Let's talk about the House passing the stimulus bill....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yurGxoqjswM) |
| Published | 2021/02/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The stimulus bill passed the House in the early morning hours.
- Key provisions of the bill include $1400 stimulus checks, increased jobless benefits, and more.
- The bill also includes a minimum wage increase that may face challenges in the Senate.
- Republicans unanimously voted against the bill.
- Speaker Pelosi's statement on the minimum wage being inevitable raises questions.
- The bill is seen as a win for the Democratic Party.
- There's speculation about potential moves regarding the filibuster.
- Republicans opposed helping working families, citing they are not the party of country clubs.
- The bill represents a significant victory for the Democrats.
- Beau plans to address filibuster modifications later.

### Quotes

- "Nay, nay, nay, nay, nay. All of them."
- "But this is a big win for the Democratic Party."
- "Every Republican voted against helping out working families."
- "It's definitely a win."
- "Y'all have a good day."

### Oneliner

The House passes the stimulus bill with $1400 checks, facing Republican opposition, and potential filibuster modifications looming.

### Audience

US citizens

### On-the-ground actions from transcript

- Contact your representatives to express support or concerns about the stimulus bill (suggested).
- Stay informed about potential filibuster modifications and their impact (suggested).

### Whats missing in summary

Insight into the potential consequences of the stimulus bill and the political landscape surrounding it.

### Tags

#StimulusBill #HouseVote #MinimumWage #RepublicanOpposition #DemocraticParty


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the stimulus bill.
It made it through the house last night.
Well, this morning at like 2 AM.
So what's in it?
Okay, $1,400 stimulus checks for Americans making less than $75,000 a year.
It increases jobless benefits to $400 a week through the end of August,
expands the child tax credit up to $3,600,
includes $350 billion for state, local, and tribal governments, and
another $170 billion to open up schools and
institutions of higher learning and all of that.
It also includes the minimum wage increase,
which is expected to run into trouble in the Senate.
Bernie has a plan B.
It's interesting.
It's not a total solution.
It's not really a minimum wage increase.
It is twisting the arms of large companies to get them to increase theirs.
It would have a lot of impact.
It's a good idea.
It's a good idea.
So giving credit where credit is due, I would like to read off the names
of every Republican in the House that voted for this, and I'm done.
They all voted against it, every single one of them.
Nay, nay, nay, nay, nay.
All of them.
Now, Nancy Pelosi said something really odd.
She said that a $15 hour minimum wage was inevitable,
even though some may see it as inconceivable.
It was inevitable.
It's an odd statement.
I'll be honest, I have no idea what she's talking about.
The votes are not there.
The votes are not there.
So either she has a very different definition of the word inevitable, or
this was an unforced error where she said something she didn't need to,
because politically it doesn't make sense.
This is a win.
This is a win for the Democratic Party,
as evidenced by the fact that not a single Republican voted for it.
As much as they want to cast this as a bipartisan bill, it's obviously not.
Biden's theme of unity isn't really coming to fruition.
The other option is that they have something up their sleeve.
There are a lot of people pushing to end or modify the filibuster.
Maybe she knows something we don't.
But that was an odd statement.
That was something that didn't need to be said,
that kind of came out of nowhere.
And I'm very curious as to what it means.
But at the end of the day, every Republican voted against helping
out working families in the middle of all of this because, as they said,
they're not the party of country clubs, even though the guy literally lives at one.
They all voted against it.
I think that was a really bad move politically for them.
But this is a big win for the Democratic Party.
It's not the win everybody wanted as far as the minimum wage, but
it's definitely a win.
So that's where we're at this morning.
And we will talk about modifying the filibuster later this afternoon.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}