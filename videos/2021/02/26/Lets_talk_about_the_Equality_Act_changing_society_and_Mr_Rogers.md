---
title: Let's talk about the Equality Act, changing society, and Mr. Rogers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GzGPkjjVfFk) |
| Published | 2021/02/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of the Equality Act and its purpose to extend basic protections to marginalized groups in the United States.
- Notes that the Equality Act, having passed in the House, faces a challenge in the Senate where it is likely to be filibustered.
- Explains that Democrats need 60 votes to pass the Equality Act in the Senate, which may not happen.
- Comments on how Republicans have weaponized the Equality Act as a talking point to rally their base against marginalized groups.
- Recalls a scene from 1969 related to desegregation of pools as an example of societal change not just through laws but also by changing people's mindset.
- Urges individuals to take action in their daily lives to create a more inclusive and accommodating environment for marginalized communities.
- Encourages activities like advocating for inclusive hiring practices to make society more accepting without solely relying on legislation.
- Stresses the importance of individuals sending messages, normalizing inclusivity, and breaking stigmas to bring about societal change.
- Asserts that changing societal norms doesn't always require changing laws but focuses on changing people's perspectives.
- Concludes by affirming that individuals can drive change and create a more inclusive society without solely depending on legislative actions.

### Quotes

- "To change society, you don't have to change the law."
- "We don't have to wait on Capitol Hill."
- "We have to send the message as individuals."
- "You have to change the way people think."
- "Because it will become the norm without the legislation."

### Oneliner

Beau underscores the power of individual actions in creating societal change, advocating for inclusivity beyond legislation.

### Audience

Advocates, Activists, Citizens

### On-the-ground actions from transcript

- Advocate for inclusive hiring practices (implied)
- Engage in activities that foster a more accepting environment (implied)
- Send messages of inclusivity in daily life (implied)
- Normalize inclusivity and break stigmas (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the historical context of civil rights legislation and the significance of societal mindset shifts in creating lasting change. 

### Tags

#EqualityAct #SocietalChange #Inclusivity #MarginalizedCommunities #IndividualActions


## Transcript
Well howdy there internet people, it's Beau again.
So I don't want to rain on everybody's parade or anything, but today we're going to talk
about the Equality Act.
What it is, what it's supposed to do, what's likely to happen to it when it gets to the
Senate, how to change society, and this shirt.
One of y'all sent me this shirt and I have been sitting on it, holding it, waiting for
a topic that truly connects to it, something that's actually worthy of it.
One of my favorite scenes.
Okay, so the Equality Act, what it is designed to do, it has passed in the House, what it's
designed to do is amend the Civil Rights Act of 1964 and extend those incredibly basic
protections to another group of marginalized people in the United States, people who have
a different orientation or identity.
The problem is now it has to go to the Senate, where it is almost certainly going to be filibustered.
So Democrats need 60 votes to get it passed.
They're probably not going to get it.
This is probably not going to become law this time.
Republicans have turned this into one of their talking points.
It's how they motivate their base.
Those people over there, they're different, kick down.
So this probably isn't going to go anywhere.
And that can be very disheartening.
At the same time, it's meant to amend the Civil Rights Act of 1964.
And that law taught us something, taught the country something.
The scene on this shirt is from 1969, years later.
One of the things the Civil Rights Act was supposed to do was desegregate pools.
It did, in theory, according to law, it did that.
But it didn't actually happen.
Because to change society, you don't have to change the law, you have to change the
way people think.
So a beloved character on TV had a scene on his show where a black cop came and sat down
next to him.
They put their feet in a kiddie pool together.
And then they shared a towel.
Something that today would be called performative nonsense, no doubt.
That sent a message.
It sparked a conversation.
It helped move the needle.
It helped change thought.
Here's the thing.
Everybody can send that message.
Everybody can play a part in that today for this group of marginalized people.
We can extend that hand.
We can engage in activities to make this country just a little bit more accommodating, a little
bit more livable for people who lack very basic protections.
It could be encouraging your employer to hire.
It could be anything, literally anything, that creates a space in this country that
is more accepting of people who lack very basic protections.
We don't have to wait on Capitol Hill.
We don't need a law to tell us to be good people.
We can do that without them.
And they can catch up.
If the Civil Rights Act of 1964 taught us anything, it's that we have to shift thought
even if there is legislation.
We have to send the message as individuals.
We have to normalize it.
We have to get rid of the stigma.
And we can do that.
We don't need to wait on them.
They can catch up.
And if they don't, if we do it right and we are passionate enough about it, we won't
need the legislation.
Because it will become the norm without the legislation.
To change society, you don't have to change the law.
You have to change the way people think.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}