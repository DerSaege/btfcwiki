---
title: Let's talk about why Democrats couldn't get the minimum wage increase....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=AxDtc1Q6hWo) |
| Published | 2021/02/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Democrats want to increase the minimum wage, but it's unlikely to happen.
- The Senate parliamentarian ruled that using budget reconciliation for a minimum wage increase is outside the scope.
- Republicans are unlikely to support a minimum wage increase, especially at $15 an hour.
- Democrats may need to compromise and lower the proposed increase to $11 or $12 to have a chance of passing.
- A minimum wage increase is no longer a realistic topic until at least the midterms, and even then, it's uncertain.
- The rest of the bill may progress without the minimum wage increase.
- Movement on the minimum wage increase is unlikely for quite a while.

### Quotes

- "The odds of them pushing this through, especially at $15 an hour, are slim to none."
- "There's probably not going to be any movement on that for quite a while."

### Oneliner

Democrats push for a minimum wage increase, but obstacles make it unlikely, especially with Republican opposition.

### Audience

Policy advocates

### On-the-ground actions from transcript

- Advocate for a minimum wage increase in your community (implied)
- Support organizations working towards fair wages (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of the challenges facing the proposed minimum wage increase and the political dynamics that hinder its passage.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the minimum wage increase, the proposed minimum wage increase
and why we're probably not going to see it.
You can stick a fork in that.
There's confusion as to why it's not getting through because Democrats want it and Democrats
have control of the Senate.
Democrats have a simple majority.
They have 51 votes if you include the tie-breaking vote from Vice President Harris.
Most things require 60 votes.
They were trying to move it forward using a process called budget reconciliation.
Using this process you can get stuff through with only 51 votes.
However it's limited in scope.
The Senate parliamentarian ruled that minimum wage increases was outside of the scope.
I have a video from like a month ago about budget reconciliation.
It's called, let's talk about Bernie taking his gloves off or something along those lines.
I'll put it down below.
In it I give examples of what budget reconciliation can be used for and what it can't be used
for.
One of the examples I gave of what it probably can't be used for is a minimum wage increase.
Said it was worth making the argument but that it probably wouldn't work.
That's what happened.
They put forth a good faith effort to get it through using this method.
They can't but realistically it is outside the scope.
But they tried.
So now they can't get it through with a simple majority.
They need Republicans to vote in favor of giving low income people, working people something.
If there is anything that the last year has taught us it's that the Republican party has
absolutely no interest in helping working people.
The odds of them pushing this through, especially at $15 an hour, are slim to none.
They may be able to water it down to $11 or $12 and maybe pick up the votes necessary
to get it through.
But even that seems unlikely to be honest.
So for all intents and purposes a minimum wage increase is no longer a topic.
It's not something that is realistically going to happen at least until the midterms.
And that's assuming Democrats pick up seats.
It does look like the rest of the bill is going to be alright.
But that element of it isn't going to make it.
So there we are.
That's where we're at on that particular subject.
There's probably not going to be any movement on that for quite a while.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}