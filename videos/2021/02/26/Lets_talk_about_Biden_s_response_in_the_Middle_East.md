---
title: Let's talk about Biden's response in the Middle East....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QbxItByHILo) |
| Published | 2021/02/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides an overview of breaking news regarding an unconfirmed report of the Biden administration authorizing a strike against Iranian-backed non-state actors in Syria.
- He explains the potential impact on Biden's foreign policy, especially with regards to Iran's involvement through non-state actors.
- Non-state actors are discussed as tools used by Iran to apply pressure indirectly, allowing plausible deniability by the government in Tehran.
- Beau analyzes the political necessity for Biden to respond to such actions, balancing the need to avoid appearing weak both domestically and internationally.
- Comparisons are drawn to past American responses to similar situations involving non-state actors and sponsored governments.
- The decision to target the non-state actors instead of the sponsored government is seen as a message from Biden that proportional action will be taken without escalating the situation.
- Beau questions the necessity of the response and whether it could have been handled through diplomatic talks without military action.
- Politically, responding was deemed necessary to avoid criticism and potential sabotage of Biden's foreign policy plans regarding Iran.
- The response is viewed as a calculated move to avoid escalation and maintain control over the situation without major repercussions.
- Beau ends by cautioning against blindly supporting military actions before fully understanding the situation and advocating for informed decision-making.

### Quotes

- "Before you support the troops, you need to support the troops."
- "It was possible to send the message through the talks to say, hey, if y'all are doing this, we have to freeze the talks."
- "Look, they're hitting us, and Biden is so weak, he didn't even respond."

### Oneliner

Beau breaks down the unconfirmed report of a strike in Syria by the Biden administration, analyzing its implications on foreign policy and the necessity of proportional response to Iranian-backed non-state actors.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Monitor diplomatic talks for potential escalations (implied)
- Advocate for informed decision-making in response to military actions (implied)

### Whats missing in summary

Deeper insights into the potential consequences of military responses on diplomatic efforts and political perceptions.

### Tags

#ForeignPolicy #BidenAdministration #Iran #NonStateActors #MilitaryResponse


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to go over some breaking news.
At the time of filming, a lot of what I'm about to say isn't really confirmed.
It's very early in this, so keep that in mind.
This is all very preliminary information.
It is subject to change, but it's worth talking about before all of the pundits get in on
it and start making their claims about what it is and what it means.
Okay so, what's the news, right?
It is reported that the Biden administration authorized a strike against Iranian-backed
non-state actors in Syria.
Now this isn't confirmed, but it is incredibly likely that these are the same Iranian-backed
non-state actors who recently staged attacks in Iraq.
So what does all of this mean?
What does it mean for Biden's foreign policy, in which Iran is kind of center?
Iran uses their non-state actors the same way that Western powers use contractors or
covert forces who can be disavowed.
They're a method of applying pressure without it coming directly from the government.
Now the reality is, these groups, they don't move without permission from Tehran, just
like our forces don't.
But the appearance is there.
The government in Tehran can say, oh no, no, no, that wasn't us.
We're really sorry that that happened.
I can't believe that.
Plausible deniability.
That's what it's about.
Biden politically had to respond.
He had to respond.
If you go to the video, let's talk about Biden's first real foreign policy test.
Iran can't appear weak and Biden can't appear weak to the American people.
Iran can't appear weak to the Middle East in general because that's what gives them
their status.
So Biden chose to keep it at the level it was at.
He chose to go after the non-state actors.
Presidents in the past have opted for a more typically American response.
When a non-state actor does something, we hit the sponsored government.
That's typically what occurs.
And in fact, faced with this exact scenario, the last president took out a kind of beloved
Iranian general, which is why...
So, that's how we typically respond.
The last time a non-state actor hit something in the U.S., we invaded half the Middle East.
We are not known for being reserved when it's a non-state actor and a government or group
of people is trying to maintain plausible deniability.
We normally don't go for that.
We normally go after the source.
That's not happening here.
Biden is trying to send a message that we're not just going to ignore it.
We are going to cause you damage at a proportional level, but we still want to move forward with
what's going on as far as the deal.
That's the message.
Is this the right way to send that?
I don't know.
That's a tough call.
There are going to be those who say that we should just ignore it because we haven't had
massive damage inflicted.
There's been some, but it's not overwhelming.
And we could just kind of look away.
Most people in the United States don't even know these attacks happened.
So it's not like the populace at this point is crying out for a response.
However, it could be that there's another one, there was another one on the way, and
the Biden administration was worried about it actually inflicting damage and causing
that outcry.
Once that outcry begins, it is good for ratings for the media to feed into it, to get those
yellow ribbons sold.
Once that chain of events begins, his foreign policy plans, his plan for Iran, his plans
to thaw the Shia-Sunni divide, all of that, it's gone.
It's gone.
So he doesn't want to allow that to happen.
He doesn't want that escalation to occur, as evidenced by him using the literal lowest
amount of force he could use to respond.
Some will say there was no need to respond.
And I tend to fall more on that side.
The talks are starting.
It was possible to send the message through the talks to say, hey, if y'all are doing
this, we have to freeze the talks.
Iran wants the deal, too.
They just can't appear weak.
So I don't know that it was necessary to actually achieve the goal.
However, politically, if he didn't respond, if Biden didn't respond, it would become a
Republican talking point.
And it would be used as a reason to torpedo the Iran foreign policy scheme.
Look, they're hitting us, and Biden is so weak, he didn't even respond.
So that's where we're at.
It isn't from everything that we've seen so far.
Again, it's early, but it doesn't look like this is a prelude to anything major.
This looks like a proportional response based against non-state actors, which they are from
Tehran's point of view, they are expendable.
Tehran isn't going to be really mad about this.
They're going to pretend like, oh, well, we didn't even know those people the same way
the U.S. would.
You will probably hear a lot of banging of drums over this.
I don't think so.
I don't think at this point with the information we have, it looks like the Biden administration
is trying everything it can to not escalate, which that part I can get behind.
That is the right move.
I don't know that this amount of force was even necessary.
But again, we don't have all the information yet.
So that's where we're at.
And just keep in mind, before you support the troops, you need to support the troops.
We have to find out what's going on before we start hanging out yellow ribbons and waving
the flag.
Anyway, it's just a thought.
And I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}