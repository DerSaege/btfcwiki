---
title: Let's talk about the case against Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2cDBZmxv-44) |
| Published | 2021/02/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's second impeachment is historic, and he believes the case should focus on Trump's actions during the trial.
- Republicans are likely to frame it as a Democratic plot, playing into existing rhetoric.
- Democrats want to swiftly move through the trial to focus on Biden's agenda for potential re-election.
- The case should argue that Trump's actions incited the Capitol riot and set a tone for extra-legal activity.
- Trump's rhetoric, alleging election fraud, led people to believe they needed to act, culminating in the Capitol violence.
- Despite the clear evidence, many Republicans are expected to acquit Trump due to political considerations.
- Beau doubts Trump will return to power in 2024, regardless of the trial outcome.
- If acquitted, attention may shift to holding accountable those who enabled Trump's actions.
- Paradoxically, convicting Trump might be the safest route for some Republicans' political futures.
- Beau suggests that some Republicans may prioritize their political interests over the country's well-being.

### Quotes

- "They don't want to spend a lot of time on it because they want to get to Biden's agenda."
- "He'd be a huge pacifier to a lot of people."
- "But I have a feeling they will put what they believe to be their own political interests over that of the country."

### Oneliner

Beau believes Trump's second impeachment case should focus on incitement and extra-legal activity, despite expectations of Republican acquittal.

### Audience

Politically-engaged individuals

### On-the-ground actions from transcript

- Contact your representatives to express your views on the impeachment trial (suggested)

### Whats missing in summary

Insights on the potential long-term consequences of the impeachment trial

### Tags

#Trump #Impeachment #PoliticalRhetoric #CapitolRiot #Accountability


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Trump's historic second impeachment.
Nobody gets impeached more than me.
And what the case should be during the trial.
Republicans are already going to try to frame it as another Democratic plot of some sort,
feeding into that rhetoric that already exists.
Democrats have made it clear that they want to get this over with.
They don't want to spend a lot of time on it because they want to get to Biden's agenda
because they feel that that's what's going to secure their re-election.
So what's the case?
It's two parts.
First they should argue that this wouldn't have happened without Trump.
That he laid the groundwork for this by saying things like, get him out of here, I'll pay
your legal bills, bump their head when you put them in the car, don't be too gentle,
stand by, stand down.
That he set the tone for extra legal activity.
That would be part of it.
And that his rhetoric, while they're trying to steal it, it's rigged, fraud, all of that,
that that brought people to a point where they felt they had to act.
And he's already set the tone for them to act in a manner outside of the wall.
So when he says fight, they're ready to.
And then they go to the Capitol and they parrot his slogans.
They use his rhetoric.
They target his political opposition and they do what he wanted them to do.
Then when it's over, they admit that they feel duped by him and that they call on him
for a pardon because, I mean, after all, they were just doing what he wanted them to.
That's the case.
And this whole time pointing out that these behaviors violate his oath of office, that
they are not in support of the Constitution, that they are, in fact, attempting to undermine
it.
That's the case that should be made.
It's simple.
It doesn't require a lot.
And most of it's on camera.
That would be the case.
And then we would all sit back and pretend to be surprised when a whole bunch of Republicans
vote to acquit despite that case being made, despite most of it being on film.
Because well, they're concerned about their political futures, which is funny to me because
at that point we have to question whether or not they take their oath of office seriously.
And I am fairly convinced that the United States will, in fact, not vote Trump back
into power in 2024.
I don't think that's going to happen.
So even if they acquit him, I'm not that concerned about that.
But the reason they would acquit him is their own political future, which to me is funny.
Because if he was convicted for a whole lot of people, that would be the end of it.
That would be the end of it.
They would feel that accountability had occurred and they would let it go.
He'd be a huge pacifier to a lot of people.
However, if he's acquitted, I'd be willing to bet that those same people who would let
it go would then turn their attention to those who enabled them, many of those in the House
and Senate, and that they would try to hold them accountable because they did parrot his
rhetoric, the same rhetoric that was parroted by those at the Capitol on the 6th, that they
played a part in it.
I mean, in a bizarre way, in order to secure their political futures, they have to act
in a counterintuitive way.
They have to convict Trump.
Got a feeling they won't.
But that is their safest route for their own future.
But I have a feeling they will put what they believe to be their own political interests
over that of the country.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}