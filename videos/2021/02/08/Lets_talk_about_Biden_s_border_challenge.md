---
title: Let's talk about Biden's border challenge....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jw6nbE4125Y) |
| Published | 2021/02/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Media headlines on the southern border crisis seem to exaggerate the situation, using words like "surge," "challenge," "emergency," and "crisis."
- Large numbers of people showing up at the southern border have been a continuous issue for almost 20 years, becoming a problem only under specific administrations.
- The previous administration used the situation as a pretext to build a border wall, while the new administration claims that America is back, which should mean handling the situation without panic.
- Beau urges the Biden administration to honor international agreements, embrace refugees, and provide non-military assistance to countries of origin affected by failed US foreign policy.
- Transitioning from a world policeman to a world EMT could be a more effective and humane approach than building walls or increasing enforcement.
- Beau advocates for allowing people in while working on necessary actions, mentioning that the United States should not fear immigrants but instead offer assistance and support.
- He concludes by stating that the situation at the southern border is not a crisis or a challenge if the Biden administration's promises and rhetoric are to be believed.

### Quotes

- "Large numbers of people have been showing up at one time at that southern border for almost 20 years."
- "This is not a crisis. This is not a challenge."
- "The only aliens we're afraid of are the ones that chase Sigourney Weaver."

### Oneliner

Media headlines exaggerate southern border issues; Beau calls for humane responses and assistance, not panic.

### Audience

Policy Makers, Activists, Advocates

### On-the-ground actions from transcript
- Provide non-military assistance to countries of origin (exemplified)
- Transition from being the world's policeman to being the world's EMT (implied)

### Whats missing in summary

Beau's engaging delivery and nuanced perspective on immigration issues can be best understood by watching the full transcript.

### Tags

#Immigration #SouthernBorder #Refugees #USForeignPolicy #BidenAdministration


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the headlines I've been reading about what's going on on
the southern border.
Because it seems, it seems kind of like the media has forgot what year it is.
Because some of those words and those headlines, surge, challenge, emergency, crisis, dun dun
dun.
Yeah.
See the thing is, large numbers of people have been showing up at one time at that southern
border for almost 20 years.
Only a problem under one administration.
Only a challenge for an administration known for its incompetence and ineffectiveness.
Only a challenge for an administration that needed a pretext to build a giant vanity project
on the southern border.
That's when it was a challenge.
That administration's not in power anymore.
The new administration has loudly proclaimed that America is back.
And if that's the case, if half of the Biden administration's rhetoric is to be believed
and accepted, isn't that a challenge?
It's just Monday morning.
We're the United States, we got this.
This is the moment to show we honor our international agreements again.
It's the moment to show that we deserve to have that statue in our harbor.
That this is the place where the huddled masses yearning to breathe free can come.
That we don't look outside our borders and see a lesser people.
That the fear and abject cowardice fostered by the Trump administration is gone.
No, it's not a challenge.
Not at all.
The Biden administration has made a lot of moves when it comes to immigration.
So much so it honestly needs its own video and we will get to that.
But something piqued my curiosity.
They expressed interest in providing direct assistance to the countries of origin of the
refugees.
Yeah do that, please, by all means, start.
Yeah that'd be great.
Non-military assistance.
That would be fantastic.
That's a good idea.
If you don't know, if you didn't follow the channel back when this was a hot topic under
Trump, most of the refugees are coming from countries that are in the situation they are
in because of failed US foreign policy.
You know the big question is why can't they fix their own house?
Because some guy in a star-spangled hat kept coming down there and kicking the door in
and messing stuff up.
By all means, the Biden administration should begin the transition from being the world's
policeman to being the world's EMT.
Not just would it be less expensive than that wall or increased enforcement, it would help
not only those people who would show up at our southern border, but would help others
as well.
Yeah do that.
But until then, let them in.
Do what we need to do.
This is the United States.
The only aliens we're afraid of are the ones that chase Sigourney Weaver.
No, this is not a crisis.
This is not a challenge.
If the Biden administration's rhetoric is to be believed, this isn't a moment to cower.
This is the moment they've been waiting for.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}