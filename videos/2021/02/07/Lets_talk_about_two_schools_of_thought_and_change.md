---
title: Let's talk about two schools of thought and change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3zpODzHfrQM) |
| Published | 2021/02/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of two schools of thought on deep systemic change in the country.
- Reformists advocate using the system's machinery to address issues through voting, petitions, and campaigning for politicians.
- They focus on policies that reduce harm and enhance social safety nets.
- Revolutionaries, on the other hand, prioritize building new infrastructure and systems over electoral processes.
- They are more hands-on in addressing issues directly and creating solutions like co-ops and community networks.
- Beau argues that these two groups are not necessarily in opposition but are heading towards the same goal.
- He presents a metaphor of buses moving at different speeds but in the same direction to illustrate the relationship between reformists and revolutionaries.
- Beau stresses the importance of both short-term (reformist) and long-term (revolutionary) goals in achieving societal change.
- He points out the need for reformists to recognize areas that need change, which is often brought to light by revolutionaries.
- Similarly, revolutionaries require popular support and infrastructure usage from reformists to make a substantial impact.
- Beau warns against echo chambers on social media, urging a realistic assessment of support for ideas.
- He underscores the effectiveness of building new systems rather than solely opposing the existing one to drive societal change.
- The duty of revolutionaries is to eliminate objections and build infrastructure to garner popular support for their cause.
- Beau concludes by leaving the audience with his thoughts and well wishes.

### Quotes

- "The reformist says, well, you know, that's nice. That location you want to get to, that's nice. But it can't happen right now."
- "The reality is both schools of thought need the other."
- "The duty of the revolutionary is to get rid of those objections, to build the infrastructure, to overcome those pragmatic reasons."

### Oneliner

Beau presents two schools of thought on systemic change: reformists work within the system, while revolutionaries focus on building new infrastructure, both aiming for the same goal.

### Audience

Social Activists

### On-the-ground actions from transcript

- Start a community network to address local issues (exemplified)
- Get involved in building infrastructure like co-ops and intentional communities (exemplified)
- Support policies that reduce harm and improve social safety nets (exemplified)

### Whats missing in summary

The full transcript provides a detailed exploration of the dynamics between reformists and revolutionaries in driving societal change.

### Tags

#SystemicChange #Reformists #Revolutionaries #InfrastructureBuilding #CommunityNetworks


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about
two different schools of thought.
One of the things I love about y'all
is that y'all are very diverse,
and I don't just mean that demographically.
I mean in the way you think.
Pretty much everybody who watches this channel
is behind the idea that we need deep systemic change
in this country.
That's uniting.
How to go about it?
There's some divergence of thought there.
The first school of thought is the reformist school of thought.
Those are people who want to use the machinery of the system
to make the system a little bit better,
to address problems that they can readily see.
These are people who really lean into voting,
petitions, campaigning for politicians,
running for office yourself, electoralism, generally speaking.
These are people who want policies that reduce harm,
that make things just a little bit better.
They want better social safety nets,
foreign policy that doesn't take out a bunch of innocents.
Objectively good things.
The other side of that is the radical thought,
the revolutionary school of thought.
When I say that, I don't mean with weapons.
Generally speaking, the most effective people
who had that mindset, well, they built.
They built the infrastructure.
They built the new system.
These are people who really don't care much about voting.
They're more about getting involved directly with issues
and correcting them, building that infrastructure.
These are the people that found co-ops,
start community networks, maybe even
start intentional communities.
If you're a reformist and have no idea what that means,
it's like a commune, only not.
These are your labor organizers, people like that.
The common theme right now is that these two groups of people
are working in opposition.
I don't know that that's true.
I don't know that that's even a good way to look at it,
because at the end of the day, the groups
are headed to the same location, at least
close to each other anyway.
It really boils down to two sentences.
The reformist says, well, you know, that's nice.
That location you want to get to, that's nice.
But it can't happen right now.
And a whole bunch of pragmatic reasons why it can't.
The person of revolutionary thought
says it has to happen now.
We don't have a choice.
We're running out of time.
The thing is, both of those sentences
can be true about something at the same time.
Something may have to happen right now, but it can't.
The political will isn't there.
The support isn't there.
Don't believe me?
Look at climate change.
These two schools of thought should not be in opposition.
They're headed to the same location.
It's like that bus analogy.
People get behind an idea.
The idea is the bus.
If they support it, they get on.
Some people want to carry it further,
so they go a few more stops than other people.
Some people want to go a few more stops than others.
Some people want to ride it all the way to the end.
This is more like there's two different buses
and they're moving at different speeds,
but they're still headed in the same direction.
And lately, the two schools of thought
have grown to see each other as opposition, when realistically,
there are two other buses that are
pulling in the other direction.
It's not going to happen.
It's not opposition.
I think of it as short-term and long-term goals
more than anything.
And the reality is both schools of thought need the other.
The reformists, most of them probably
wouldn't know all of the areas that need help,
that need harm reduction, without the radicals,
without the revolutionaries.
And the revolutionaries, they need the reformists.
They need that popular support.
They need somebody to use the infrastructure
they are building.
Otherwise, they won't do any good.
We tend to think of echo chambers on social media
as something that only occurs in the right,
because it happens a whole lot there.
It also occurs on the left.
And I mean the real left and the American left.
It occurs in both.
And there is a habit, particularly
among those of revolutionary thought,
to overestimate the amount of support
that certain ideas have.
It's probably wise to remember that the most successful people
when it comes to creating societal change,
they didn't fight against the old system.
They built a new one and encouraged people to use it.
And once that infrastructure was in place
and enough of those who seek reform now
came over and started using it, that's
when the societal shift happened.
You take away that sentence.
It's nice, but it can't happen now,
because of these pragmatic reasons.
It is the duty of the revolutionary
to get rid of those objections, to build the infrastructure,
to overcome those pragmatic reasons.
That's how you build popular support.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}