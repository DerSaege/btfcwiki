---
title: Let's talk about Black History Month and pulling your kids out....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BPRudrdS3ak) |
| Published | 2021/02/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau expresses surprise at the revelation of parents being able to pull their kids out of Black History Month curriculum in schools.
- The history of Black History Month dates back to 1926 when historian Carter Woodson proposed a week to study black history.
- In 1970, Black History Month was extended to a full month, starting from January 2nd to February 28th.
- Beau points out that objecting to Black History Month means never being able to say "get over slavery" again.
- He challenges the idea of opting out of certain history classes for civil rights reasons, using algebra as an example.
- Beau criticizes the notion of parents deciding to exclude their children from learning specific aspects of history, labeling it as wrong.
- He suggests that Black History Month can only end when white individuals stop being upset about it.
- Beau stresses the importance of a comprehensive world history curriculum in schools to combat Eurocentrism and misinformation.
- History education is vital, and Beau urges against limiting children's exposure to different historical perspectives.
- He concludes by encouraging viewers to think about the implications of restricting historical education for children.

### Quotes

- "If Black History Month bothers you, you never get to say, get over slavery again, ever."
- "The funny part about this is, do you know when Black History Month can end? When white folks stop getting mad about it."
- "History is important."
- "It is incredibly important to yank your kid out of something like this."
- "You are choosing to make certain your child is ignorant of things they need to know."

### Oneliner

Beau challenges objections to Black History Month, advocating for comprehensive historical education and condemning the exclusion of vital knowledge from children.

### Audience

Parents, educators, activists

### On-the-ground actions from transcript

- Teach comprehensive world history in schools (suggested)
- Advocate for inclusive and diverse historical curriculums (suggested)
- Emphasize the importance of history education to parents and educators (suggested)

### Whats missing in summary

Deeper exploration of the impact of limited historical education on children's understanding of societal issues and the importance of diverse perspectives in shaping a well-rounded worldview.

### Tags

#BlackHistoryMonth #Education #History #Inclusivity #Activism


## Transcript
Well howdy there internet people, it's Bo again.
So today we're gonna talk about something
that just blew my mind.
And we're probably gonna listen to some pings on the roof
because it just stopped raining.
You know, prior to 2020, I was very fond of the saying
that nothing surprises me.
I've been surprised a lot over the last year or so.
This is no exception.
I just found out that there are schools in the United States
allowing parents to yank their kids out
of the Black History Month curriculum.
The apparent impression is that it is some kind
of liberal, PC, woke crowd thing.
So today we're gonna talk about the history
of Black History Month.
It began when a historian, Carter Woodson,
proposed the second week in February
as a designated period to study black history.
In 1926, it is 2021.
This idea was accepted in 1926.
In North Carolina, people in North Carolina
in 1926 were less racist than people today.
Were more open-minded, were more progressive, 1926.
1926, second week in February,
February 12th to February 20th,
Abraham Lincoln's birthday to Frederick Douglass' birthday.
And if you are one of those people
who pulled your kids out of this,
you probably have no idea who Frederick Douglass is.
And that's sad.
Now, back then it was just a week.
It became a month in 1970,
was proposed in 1969 at Kent State.
And the first one ran from January 2nd to February 28th.
So even if you just want to go back
to when it became a full month,
it was half a century ago.
If Black History Month bothers you,
you never get to say, get over slavery again, ever.
This started in 1926, 60 years after the end of slavery.
60 years after the period in time
when black people were seen widely as subhuman,
were literally property.
And the people in North Carolina were ready for it in 1926.
It's been almost a hundred years
and you're mad about a class in school.
Want to opt out for your civil rights.
Yeah, I wish I could have opted out
of algebra for my civil rights.
I don't often make judgments,
but I gotta say, if you tell the school
not to teach your children a specific kind of history,
you're wrong, you are wrong.
The funny part about this is,
do you know when Black History Month can end?
When white folks stop getting mad about it.
Because at that point,
it can just be taught with the normal curriculum.
It won't be glossed over.
It won't be seen as unimportant,
something you can yank your kid out of
and it not impact them.
The history curriculums in the United States
are not that great to begin with.
Most of them are not.
Limiting them further in any way is a really bad idea.
Most history curriculums in the US
are very centered on the United States.
Some may be Eurocentric, which is bad.
That's what leads to the ideas that took root,
that have caused all of these problems,
that are objectively wrong.
If we actually had good world history taught in schools,
there wouldn't be petitions
and people getting mad about Onion articles
because schools are teaching Arabic numerals.
History is important.
If you watch this channel often,
you know I'm a little bit of a history nerd.
It is incredibly important
to yank your kid out of something like this.
It is limiting them.
You are choosing to make certain
your child is ignorant of things they need to know.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}