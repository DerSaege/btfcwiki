---
title: Let's talk about a lesson from Los Angeles for the whole country....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=m2YJB6blPA8) |
| Published | 2021/02/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Los Angeles has a lesson for the entire country stemming from historical events in 1942, shortly after Pearl Harbor.
- The west coast was on edge, taking preparations and making sure they were ready for a supposed Japanese attack.
- A well-known animation studio in Burbank had troops stationed to prevent it from falling into enemy hands.
- Despite being told an attack was imminent, it didn't happen on the predicted date of February 24th.
- In the early hours of February 25th, the skies over LA lit up with anti-aircraft fire, but no Japanese attack occurred.
- The hysteria and fear created from prolonged propaganda led to shooting at a weather balloon and false alarms.
- The US is currently experiencing similar fear-induced reactions, leading to wild theories, demonization of marginalized groups, and exaggerated threats at the southern border.
- Fear is driving the country, not rational policy or thought, as fear makes people reliant on leadership for protection.
- Many national debates are driven by emotions like fear rather than rational ideas, leading to harmful outcomes.
- The country is urged to move away from making decisions based solely on fear and embrace thoughtful discourse instead.

### Quotes

- "This country is being run by fear."
- "The average citizen is overcome with fear because the leadership knows that people who are fearful will always need a leader."
- "We have a whole lot of discussions going on in this country that are base emotion reactions."
- "The country is being run by fear, not by policy, not by thought, but by fear."
- "They literally created an enemy out of thin air."

### Oneliner

Los Angeles' historical lesson warns against decisions driven by fear, urging thoughtful discourse over emotion-led reactions in the country.

### Audience

Citizens

### On-the-ground actions from transcript
- Challenge fear-based narratives and seek rational thought in decision-making (implied).
- Encourage and participate in thoughtful, fact-based debates and dialogues within communities (implied).

### Whats missing in summary

The full transcript delves into the dangers of fear-driven decision-making and the importance of shifting towards rational discourse for positive outcomes.

### Tags

#Fear #DecisionMaking #Community #History #LessonsLearned


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about what Los Angeles can teach the entire country because
LA has a lesson for us.
It's very relevant to a whole lot of things going on in the country right now.
Something happened this morning, early this morning in 1942, but it's still relevant.
To lay the scene, this is shortly after Pearl Harbor.
So to say that people on the west coast were on edge, that's an understatement.
Wasn't just LA, wasn't just California, it was the entire west coast.
They were taking preparations.
They were making sure that they were ready.
They knew there was a Japanese carrier out there and they knew that they were next.
They'd been told the enemy was out there, that their opposition was just waiting for
the chance to invade.
And they took that seriously, so seriously in fact that a very well-known animation studio
in Burbank, California had troops stationed at it because there was no way we were going
to let that piece of American real estate fall into opposition hands.
Now this dragged on and they were told they had to stay ready and they did.
And it was a lot of work.
They had to stay on edge.
They had to be afraid all the time.
And then February 24th, it kind of paid off because the Office of Naval Intelligence was
like yeah, today it's going to happen.
It is going to happen.
The alert went out.
Somehow true to a lot of predictions from Naval Intelligence, it actually didn't happen
on the 24th.
But early in the morning on the 25th, 316 AM, the skies over LA were bright with fire.
Lasted about an hour and in that time, 1,400 12.8 pound anti-aircraft shells were fired.
Six rounds of 50 cal were loosed.
And when it was all said and done, when the smoke cleared, it really wasn't that bad.
Could have been a lot worse.
Lost five.
Three in car accidents.
Two from heart attack.
There was no Japanese attack.
Not a single plane.
In fact, during the entire war, the Japanese never flew over LA.
Hysteria.
I think they say that it started with somebody shooting at a weather balloon and then the
lights from the anti-aircraft batteries got other people shooting.
They had been propagandized so long, had been told to be on edge so long, to be fearful
for so long, that they literally created an enemy out of thin air.
That's happening a lot in the United States right now.
From the wild theories that are now being spewed in Congress to people who have been
marginalized and demonized because of their identity or their orientation, to our southern
border where we're being invaded by teenagers.
The thing is, when that happens, eventually it does reach a boiling point.
Almost always.
And typically, when that happens, we only hurt ourselves.
This country is being run by fear.
Not by policy, not by thought, but by fear.
The average citizen is overcome with fear because the leadership knows that people who
are fearful will always need a leader.
They'll need somebody to turn to, to protect them from the other.
We have a whole lot of discussions going on in this country that are base emotion reactions.
They're not based in thought.
The discussions aren't led by thought, by rational ideas.
They're based in fear.
That always goes wrong.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}