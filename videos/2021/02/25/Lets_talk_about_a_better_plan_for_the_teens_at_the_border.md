---
title: Let's talk about a better plan for the teens at the border....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HY9GkB8VUIM) |
| Published | 2021/02/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Proposes better solutions for housing children instead of locking them up in camps.
- Advocates for understanding resources available and coming up with a more humane plan.
- Suggests processing unaccompanied teens quickly and efficiently at the border.
- Emphasizes the need to differentiate between asylum seekers and high-initiative individuals seeking a better life.
- Presents three housing options: Hilton hotels, boarding schools, and sponsorship/fostering.
- Points out the moral and practical flaws in the current system.
- Addresses potential objections and offers solutions to ensure the well-being and safety of the children.
- Critiques the current system as driven by fear and habit rather than logic or compassion.

### Quotes

- "We can come up with a better plan. We can do better."
- "It's cheaper to be a good person."
- "There's no reason to detain them. There's no reason to put them in camps."
- "We just have to get over that fear."
- "We can do better."

### Oneliner

Beau advocates for humane solutions to housing children at the border, criticizing the current fear-driven system and proposing alternatives like Hilton hotels, boarding schools, and sponsorship/fostering.

### Audience

Policy makers, activists, community leaders

### On-the-ground actions from transcript

- Contact local embassies and offer support in processing unaccompanied teens at the border (suggested)
- Reach out to boarding schools to inquire about accommodating high-initiative individuals seeking asylum (suggested)
- Advocate for sponsoring or fostering unaccompanied teens in your community (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of the inefficiencies and moral shortcomings of the current system, urging for a more compassionate and logical approach towards housing children at the border. Watching the full transcript will provide a comprehensive understanding of Beau's proposed solutions and the reasoning behind them.

### Tags

#Immigration #ChildDetention #BorderCrisis #HumaneSolutions #CommunityEngagement


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about solutions, plans, a better plan.
Not using a hammer because we think everything's a nail,
not doing things the way we've always done them,
simply because we've always done them that way.
We're going to do this because I had a whole bunch of people
ask me if I had a better plan when I said that I thought
the camps were inappropriate transition spots.
Of course I have a better plan.
You do too.
Guaranteed.
Guaranteed.
With just a little bit of thought and a little bit of information,
you can come up with a better plan as well.
Okay, I have three that we're going to go over.
The first one is actually silly.
It's a joke to prove a point.
But even though it is a joke and it is silly,
it's still actually a better plan than locking kids up.
If we take this situation and put it in any other context,
locking them up wouldn't even be something that we think of.
If I was to tell you,
hey you know little Jimmy's parents got taken out in a car accident.
I'm going to adopt him,
but it's going to take like a month for the paperwork to come through.
So do me a favor, run him up to the county jail and stick him in there for a month.
You would look at me like I was a monster.
And rightfully so.
But that's what we're doing as a country.
Because there are a large percentage of Americans
who are apparently terrified of teenagers with brown skin.
It's unnecessary.
We are waiting for their cases to be decided.
We're waiting for paperwork.
None of this necessitates them being locked up.
Okay, so if you want to come up with a plan, a better plan,
you need to know the resources that you have.
You need to know what you're working with.
Currently the last estimates we have per day to house one of these kids,
they range anywhere from $750 to $775 per day.
We're going to use $775,
but if you use $750 it wouldn't alter the outcome of any of this.
At $775 per day, that is $282,000 per year.
The facility that is going to be reactivated
is going to house 713 to 17 year olds.
That is $198 million per year.
That's your budget.
That's what you have to work with.
I am willing to bet you can come up with better ways
to house kids for, you know, a little less than $300,000 a year.
Okay, so if we're going to do this,
we need to start at the beginning of the process.
Because part of the problem is that we're not processing them quickly.
I mean, I would suggest maybe it has to do with the fact
that there's a whole bunch of money to be made by warehousing them.
But we're going to get it done quickly.
So we're going to start from the beginning.
A teen makes an unaccompanied trip,
a thousand miles, risking their life,
gets to the border, Border Patrol picks them up.
Border Patrol will then get their fingerprints, biometrics, and a DNA swab,
get their name and country of origin.
We're really only doing this for those who are terrified of teenagers, okay?
At this point, Border Patrol sends that information to our embassies down south,
all of them, just in case the kid lied about where they're from.
The embassies contact the locals.
They run those names.
They get their histories, see if they have warrants.
Most importantly, and the thing that's going to happen the most,
see if they're missing persons in case they're run away.
This should take a week, okay?
Sure, during that period, fine, because people are scared, hold them.
But that should be the end of it.
That should be the end of it.
If they aren't in one of those three groups,
they're either people who are in fear for their life, that's why they made the trip.
So they're asylum seekers and they should be granted asylum.
Or they're a teenager who woke up, looked around, was like, you know what?
I'm not gonna settle for this.
I'm gonna go to the US and I'm gonna try to build a better life for myself.
Newsflash, those are not teens we should lock up.
Those are teens we should give scholarships and business loans to.
Those are the kind of people we want, high initiative people,
people that are self starters, people that are willing to risk their life for
a better life.
Isn't that what we always wanted?
So now we have those two groups of people left.
It's up to me, let them all in.
Now, they didn't walk 1,000 miles without a plan.
They had somewhere they were gonna go.
So we need people to go and check out those locations.
Make sure that they weren't scammed.
Make sure that the people who were there actually want them to show up.
Make sure it's relatively safe, okay?
During that period, the teens can be housed one of three ways.
We'll start with the silly one.
I looked tonight.
If I wanted to stay at the Hilton, one of the Hiltons in Southern Texas,
it would run me anywhere from 83 to $179 a night.
So we're just gonna say 200 to keep the math simple.
We're also gonna give the Hilton 50 bucks per day to give them breakfast,
lunch, and dinner, three-star hotel, room service.
I know they charge a little more than that normally, but
they're gonna be at 100% occupancy.
They'll be fine with it.
They'll be cool with it.
That takes care of food, shelter, hygiene, all of that stuff.
But they're gonna have incidentals, so
we'll give each one of them $600 a month on a prepaid visa.
So 20 bucks a day.
That brings our total cost up to $270 a day.
They can use that visa to get clothes, put minutes on their phone, whatever.
That leaves us $505 left over per day per kid.
That's $129 million per year if you're talking about 700 kids.
So if you were gonna pay staff $60,000 a year,
you could have 2,000 of them.
Probably won't need to do that.
Probably end up saving a lot of money doing it that way,
doing it the silly way, the joke.
Because even housing them at the Hilton is cheaper, and
they're not locked up.
Turns out it's cheaper to be a good person.
A little bit more realistic of an option would be to put them in a boarding school.
Because as we said, these are high initiative people.
They're gonna be here.
They should have an education.
Tuition, average tuition at a boarding school is $58,000 a year.
Now there's gonna be fees on top of that.
And they will have incidentals, even more so there,
because we don't want them to be left out.
And most people who go to boarding schools are of means.
So we're just gonna triple that number.
Still cheaper, by the way.
Now, the most logical thing to do
would be to treat it like the Little Jimmy situation.
Sponsors, fosters, 700 kids,
even if it's ten times that, 7,000.
There's gonna be no shortage of families willing to do that,
especially if you're like, hey, we'll give you 100 grand to offset expenses.
I don't think the way we're doing it is moral.
I don't think it's the best way.
I don't even think it's a good way.
We can do better.
Now, objections to this would be, if you used the hotel,
maybe they got into a fight.
Sure, I mean, that can happen.
I mean, I would suggest that we bring public health service people over,
social workers, stuff like that.
But people are scared of teenagers.
So sure, call the cops.
At least at that point, there's a crime with a victim,
not just kids trying to get somewhere safe.
Then they can go to a detention center, juvenile center.
Fine.
What if they leave?
Well, I would suggest that it's less likely for
people to want to leave if their conditions are good.
That stands to reason.
But if they do leave, we know where they're going.
So, I mean, does it really matter if they get there before the paperwork comes back?
Is that really that much of a concern?
I don't think so, personally.
I find it funny that in the land of the free and
the home of the brave, our first instinct is to lock up a bunch of kids while we
wait for paperwork to come back because we're terrified of teenagers with brown skin.
I don't think that fits.
I don't think that's who we want to be.
We can come up with a better plan.
We can do better.
We're just used to doing it this way.
We just assume it's the way to do it.
But if you put that in any other context, it doesn't make sense.
We wouldn't do it in any other situation.
So yeah, we can definitely come up with a better plan.
I can, you can, anybody who even tried to would come up with a better plan than this.
There's no reason to detain them.
There's no reason to put them in camps.
We have other options.
We have better options that are better for them, better for the country, better for
everybody involved.
We just have to get over that fear.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}