---
title: Let's talk about how "canceling" isn't new....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xe16MdERJxw) |
| Published | 2021/02/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the phenomenon of cancel culture and how it's not a new concept in the United States.
- Talks about how some people are now negatively impacted by cancel culture after benefiting from it for a long time.
- Describes how those who have lost privilege due to cancel culture view it as oppression.
- Attributes the impact of cancel culture to basic math and the shift in societal beliefs.
- Points out that companies, driven by profit, are the ones making decisions on canceling individuals based on public statements.
- Notes that older individuals with outdated viewpoints are becoming less valuable consumers for companies compared to younger, progressive individuals.
- Mentions historical instances of pre-cancellation, such as discriminating against LGBTQ individuals in the workplace.
- Addresses the misconception that cancel culture is a new leftist plot, explaining it as a result of American capitalism.

### Quotes

- "It's just math."
- "Who decides who gets canceled? Companies. Large corporations."
- "They're obsolete viewpoints."
- "It's not new. It just used to work in their favor."
- "What they're complaining about is not some leftist plot. It is good old-fashioned American capitalism at work."

### Oneliner

Beau outlines the historical context and economic motivations behind cancel culture in the US, debunking misconceptions about oppression and leftist plots.

### Audience

Activists, Progressives, Consumers

### On-the-ground actions from transcript

- Educate peers on the historical context of cancel culture (implied)
- Support companies that prioritize inclusive values (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of cancel culture and its historical roots, offering a fresh perspective on the current discourse.

### Tags

#CancelCulture #Capitalism #Beliefs #Inclusivity #SocialChange


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about what happens when the numbers change.
Because something is happening in the United States, all over the United States.
And people are acting like it's new.
Like it's something that hasn't occurred before.
But it's been around a really, really long time.
The difference is that prior to very recently,
they were the beneficiary of this little process.
And now all of a sudden, well, they're not.
They are being negatively impacted by it.
And they don't like it.
Today we are going to talk about cancel culture.
See, for a long time, those who are complaining about it today,
they benefited from it.
And they didn't know that they were benefiting from it.
And now they've lost that privilege.
And when you lose a privilege that you have had for so long,
that you don't even know exists, well, it feels like oppression.
And that's what's occurring.
And when they talk about it, they want to credit it
and attribute it to anything other than the reality.
And the reality is it's just math.
Cold, hard, unforgiving math.
If you say something in a public space today that is bigoted,
marginalizing, or othering,
odds are you are going to have a negative outcome.
Something is going to happen.
You may not get hired.
You may lose a job.
You may get bounced off of social media.
It didn't used to be that way.
The reason it didn't used to be that way is because, well,
that's a time when those viewpoints were the majority.
They're not now.
They're not.
They're obsolete viewpoints.
They are held by a minority of people.
Therefore, the people who actually decide whether or not
somebody gets canceled,
they have no reason to cater to those people.
Who decides who gets canceled?
Companies. Large corporations.
Companies aren't in the business of making political statements.
They're in the business of making money.
Their job is to appeal to the largest demographic possible.
So, if there is a group of people who wants to hold a viewpoint,
but those who are adamantly opposed to that viewpoint are larger,
most companies will side with the group that opposes it.
And that's what's happening.
So, you have the general shift in belief.
And then there's another mathematical equation that needs to be factored into this.
Generally speaking, those people who hold bigoted,
marginalizing, or othering viewpoints, well, they tend to be older.
How many good years do they have left in them?
10, 15, 20 if they start running?
What about the young progressive that they're constantly making fun of?
The 20-year-old.
Yeah, they've got another 60 years in them.
Who is a more valuable consumer to the company?
Who is going to make them more money?
One group is going to age out of being their consumer pretty quickly.
The other one is going to be around a while.
These viewpoints are quite literally dying.
It's that simple.
It's just math.
And this has been around a long, long time.
It just benefited them so they didn't complain about it.
They can sit there and act like it's new.
And they can tell that to the 20-year-old progressive.
The problem is I'm old enough to remember when it was really common for people to say stuff like,
you know, I don't care if somebody's gay, but that doesn't belong at work.
And if somebody was a little too open about it, well, they wouldn't get hired or they'd lose their job.
That sounds like cancel culture, doesn't it?
I can remember a time, if you want to talk about social media,
I can remember when somebody who had just transitioned,
well, they had a hard time being on social media at all because of the real name policy.
Because when Jim became Jane, well, the social media company said,
oh, sorry, that's not your real name.
We can't have you on here.
So she got pre-canceled.
It's not new.
It just used to work in their favor.
There was a funny slogan when companies first started making this shift.
If you were on the right, you probably heard people say stuff like,
get woke and go broke, because those people who held those views, held those bigoted views,
well, they were going to boycott any company that espoused a progressive one.
The buying power wasn't there.
Those millennials, who aren't actually the 20-year-olds, by the way,
but those people that constantly get made fun of, they're a driving force in the economy.
Not the older people.
Not as much.
They're more valuable.
It's not really that complex.
Society moved on, and a whole bunch of people refused to keep up.
And when you want to compare the people who are being canceled today
to those examples like I mentioned,
I want to point out that the significant difference between the two
is that, well, one is an immutable characteristic,
and the other is just a bigoted belief held by somebody
who didn't want to keep up with the rest of the world.
What they're complaining about is not some leftist plot.
It is good old-fashioned American capitalism at work.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}