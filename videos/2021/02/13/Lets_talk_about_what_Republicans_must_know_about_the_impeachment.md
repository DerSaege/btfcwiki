---
title: Let's talk about what Republicans must know about the impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1s7lY8cgraE) |
| Published | 2021/02/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the impeachment trial and the recent vote in the Senate.
- Contrasting what the proceedings should be versus what they actually are.
- Expressing concern over senators meeting with the defense and withholding information.
- Pointing out that senators are not impartial as required by the Constitution.
- Mentioning how senators may vote based on political interests rather than facts.
- Urging average Republicans to take issue with their representatives' actions.
- Emphasizing the importance of upholding the Constitution and the oath within it.
- Calling for accountability by suggesting that senators violating their oath should be primaried.
- Encouraging individuals to act on their beliefs and convictions.
- Stressing that beliefs should translate into tangible actions for them to hold true value.

### Quotes

- "Those who require your consent, your support, those who represent you are doing everything within their power to show you that they don't believe it matters at all."
- "If you believe the Constitution matters, those who behaved in this manner cannot be reelected by you because your convictions, your beliefs are utterly worthless unless they motivate your actions."
- "If you still get chills when you hear the star-spangled banner, if you think it's a travesty that people don't recite the Pledge of Allegiance, you cannot simply look the other way as those senators openly and flagrantly violate their oaths."

### Oneliner

Addressing the impeachment trial and Senate vote, Beau underscores the disparity between what should be impartial proceedings and what actually transpires, urging accountability based on constitutional principles.

### Audience

Average Republicans

### On-the-ground actions from transcript

- Hold senators accountable through primary elections (suggested)
- Vote out senators who violated their oath (suggested)

### Whats missing in summary

Full context and detailed analysis of the impeachment trial proceedings and the implications of senators' actions.

### Tags

#Impeachment #Senate #Accountability #Constitution #Actions


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the impeachment trial.
We're going to talk about that vote.
We're going to talk about what's going on up in the Senate.
We're going to talk about what those proceedings should be
and what they are, because those two things aren't the same.
What should it be?
It should be a group of senators sitting under oath
or affirmation to impartially decide the facts.
That's what it should be.
What is it?
We have reports of senators meeting with the defense
to talk about strategy.
That doesn't sound impartial to me.
We have reports of members of Congress
apparently withholding information
that certainly seems to be pretty relevant
to what's going on.
So if it's not a group of senators
sitting under oath or affirmation
as required by the Constitution, what's the vote going to be?
A group of senators voting based solely on what they believe
is going to be most beneficial to their political futures.
That's what it's going to be.
And for Republicans, average Republicans out here,
not those up in D.C.,
but average Republicans out here, that presents a problem.
Because I'm willing to bet that if you're a Republican,
you probably view the Constitution
as almost divine, divinely inspired,
a document that is incredibly important.
And an oath required by that document
is probably important as well.
Yet those who require your consent, your support,
those who represent you are doing everything
within their power to show you
that they don't believe it matters at all.
Even if you support Trump, even if you believe Trump,
even if you think this was justified,
even if you believe his debunked, baseless claims,
you have to admit that the behavior of the senators
is not in line with the Constitution.
There's no arguing that.
You have many senators up there
flagrantly violating that oath.
They need to be primaried.
They need to be voted out of office.
I'm not telling you to become a Democrat.
I'm not telling you to become more liberal.
I'm telling you to believe and act on your own convictions.
If you believe the Constitution matters,
those who behaved in this manner cannot be reelected by you
because your convictions,
your beliefs are utterly worthless
unless they motivate your actions.
If they don't translate into your actions,
into your real tangible, real-world actions,
they aren't your beliefs.
They aren't your principles.
They aren't your convictions.
It's just a slogan, you say.
If you still get chills
when you hear the star-spangled banner,
if you think it's a travesty
that people don't recite the Pledge of Allegiance,
you cannot simply look the other way
as those senators openly and flagrantly violate their oaths.
The facts are pretty clear.
What occurred, it's on video.
We know what happened.
And even if you believe Trump
and believe his claims that have been debunked
and are completely baseless, you have to admit that.
And I would actually point out that if you believe those claims,
that's further proof that he set the stage for this.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}