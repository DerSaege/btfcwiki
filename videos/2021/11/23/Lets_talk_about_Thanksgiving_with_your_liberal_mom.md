---
title: Let's talk about Thanksgiving with your liberal mom....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uBVtaW4nHwI) |
| Published | 2021/11/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing a different perspective on "Help Me Ruin Thanksgiving Dinner" focusing on dealing with a liberal mom who is a labor organizer.
- The challenge is explaining that liberal policies don't go far enough and uphold harmful systems, but facing resistance and glazed-over eyes during these talks.
- Seeking a framing device to address this issue in both personal and labor organizing situations.
- Suggests two methods: one involving negotiation framing for organized labor understanding, and the other presenting an idealistic dream for long-term change.
- In negotiation framing, starting with a strong position and making the opposition come further to meet halfway seems to work, even if not fully believed.
- Presenting an idealistic dream of maximum freedom, cooperation, and quality of life for all is another approach to shift beliefs, even if seen as unattainable in the present.
- Encouraging people to strive towards the dream rather than just conceding ground might lead to more tangible shifts in beliefs.
- Both negotiation framing and idealistic dreaming have their merits in reaching and persuading a liberal mom or those allied with similar goals but pragmatic in approach.

### Quotes

- "Remind them that it's not. We should be striving for that world where everybody gets a fair shake."
- "This is where we want to get to. The thing is, most people like to make dreams a reality."
- "We want to get to that world of maximum amount of freedom, for the maximum amount of people."

### Oneliner

Beau suggests negotiation framing and idealistic dreaming as methods to shift beliefs and reach a liberal mom or pragmatic allies.

### Audience

Progressive activists

### On-the-ground actions from transcript

- Frame arguments as negotiations (suggested)
- Present idealistic dreams for change (suggested)

### Whats missing in summary

Detailed examples of how negotiation framing and idealistic dreaming have influenced beliefs and actions in real-life scenarios.

### Tags

#Activism #Progressive #PoliticalChange #FamilyDynamics #LaborOrganizing


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we have another installment in Help Me Ruin
Thanksgiving Dinner.
But this one's a little bit different.
It's a lot different, actually.
This person isn't trying to ruin Thanksgiving dinner.
And up until now in this series, all of the questions
have been about how to deal with your conservative Facebook
abusing, Fox News, watching, probably racists, uncle, you know.
This one's about how to deal with your liberal mom.
The person is a labor organizer and they are, to use their term, A, with a circle, a little
more left than their mom.
mom's a liberal and they have these conversations where the person's trying
to explain that a lot of liberal policies don't go far enough and in
many ways they uphold systems that cause a lot of harm for people and when they
start having this conversation, eyes glaze over and they don't get
anywhere. It says that when they feel like the only time they get concessions
is when mom's just like okay and not really actually getting the point. So
they're looking for a framing device for both of these situations because they
run into the same issue when they're labor organizing. Okay so there are two
methods that I've seen success with on this. One is going to be more useful
with the labor organizing, and then the other is more useful for people who
don't have a pragmatic basis for it. So we'll start with the the labor stuff.
People in unions or people who are organized labor or are in that process,
they understand negotiation. Frame it that way. Yeah, this position over here,
this left and very very anti-authoritarian position that I'm
advocating, this is the starting point, you know, because they've seen the
compromises year after year with politicians. They know how negotiations
work because of dealing with management. So you say that you're going to start here.
So they have to come further to meet us. Company wants to give us 12. We need 24. We're going
to ask for 36. Same thing. That I've seen work, at least in the sense of getting them
to acknowledge and at times even advocate for it. Most times they don't actually believe
it though. Most times they're kind of, it's kind of like saying they want 36 an
hour when they know they need, they're gonna get 24. But that's fine because as
they repeat those ideas and as they advocate for these other policies it
helps shift that over 10 window. Now the other way and a way that I have found a
little bit more success with, it's an ideal. It's a dream. People of that
ideological bent, if they're realistic they know it's not happening in our
lifetimes, at least not mine. It's a long road. So you present it as the ideal, as
the dream. This is where we want to get to. We want to get to that world of
maximum amount of freedom, for the maximum amount of people, with the
maximum amount of cooperation, therefore everybody has the maximum quality of
life, right? And you present it as the dream. This is where we want to get to.
The thing is, most people like to make dreams a reality and it... I have found
that that is more effective at getting people to actually believe it. They may
not advocate for it as much because they see it as a dream. They see it as
something to strive towards. But it seems, at least in my experience, that it
produces more of a tangible shift in what they actually believe rather than
them just starting with it as a place to concede from and to give a little
ground from. In the grand scheme of things, both of these are good. That would
be my method of attempting to try it, of trying to reach your liberal mom. It's a
little different when you're talking about somebody who is in many ways allied
with your goal but they're looking at that pragmatic side of things and what
they've seen their entire life all of the compromise and all of that stuff so
use it present it as a negotiation tool you know people say utopian today as an
insult. Remind them that it's not. We should be striving for that world where
everybody gets a fair shake. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}