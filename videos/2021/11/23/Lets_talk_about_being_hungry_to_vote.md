---
title: Let's talk about being hungry to vote....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Fm1ohE_50EU) |
| Published | 2021/11/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of hunger to participate in voting and electoral politics in the United States.
- Joe Madison, a civil rights activist turned radio host, is on a hunger strike at 72 years old until voting rights legislation is passed.
- Democrats in the House have passed voting rights acts, but they stalled in the Senate, where 19 states have passed laws making it harder to vote.
- The Freedom to Vote Act has all 50 Democrats' support in the Senate, with Vice President Harris as a tiebreaker, but faces the filibuster.
- The hunger strike aims to pressure President Biden to fulfill campaign promises and end the filibuster, not just pass voting rights legislation.
- The filibuster is a Senate rule not outlined in the Constitution, historically used to obstruct civil rights, but its purpose was for debate and consideration.
- Joe Madison sees this as a new civil rights movement and urges eliminating the filibuster to ensure voting rights legislation passes.

### Quotes

- "As food is essential for the existence of life, voting is essential for the existence of democracy."
- "This hunger strike is definitely aimed at President Biden, asking him to make good on his campaign promises."
- "You're appealing to the humanity of the opposition."
- "Allowing large segments of the American population to be disenfranchised at this level, that's pretty risky too."

### Oneliner

Joe Madison's hunger strike calls on President Biden to fulfill promises and end the filibuster for voting rights legislation, appealing to the humanity of the opposition.

### Audience

Voters, Activists, Politicians

### On-the-ground actions from transcript

- Support voting rights organizations (implied)
- Contact senators to support the Freedom to Vote Act and eliminate the filibuster (implied)

### Whats missing in summary

The emotional impact of Joe Madison's hunger strike and the urgency to protect voting rights in the face of legislative challenges.

### Tags

#VotingRights #HungerStrike #CivilRights #Filibuster #Democracy


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk
about being hungry to vote,
being hungry to participate in the system, that desire to cast your vote and
participate in the electoral politics of the United States. The idea to be heard,
those promises that were made.
And we're going to talk about a person named Joe Madison.
Joe Madison, civil rights activist from back in the day, turned radio host.
Still definitely a civil rights activist. He said, as food is essential for the
existence of life, voting is essential for the existence of democracy. He is
calling out to President Biden, asking for backup, requesting, demanding that
voting rights legislation get passed. Joe Madison is 72 years old and he is on his
second week of being on a hunger strike. Something he says he will remain on
until voting rights legislation is passed. It should go without saying that
at 72 years old, this type of demonstration is dangerous. Doesn't
matter how good of health you're in, this is dangerous. When he was asked if he was
willing to die for this cause, lay down his life for this cause, he simply said
yes. So what's the holdup? You know, President Biden said, right after he took
office, he said that the black community always had his back, so he will always
have theirs. Well, for their part, Democrats in the House have passed
Before the People Act and the John Lewis Voting Rights Advancement Act. However,
both those stalled in the Senate. Meanwhile, at the state level, 19 states
have passed 33 laws, making it harder to vote, making it more difficult to cast a
vote in some way. There is a concerted effort on the part of Republicans to
suppress the vote, to make it more difficult to participate in the
electoral system in the United States. Now, there is the Freedom to Vote Act
that could theoretically pass in the Senate. It has support of all 50 Democrats.
With Vice President Harris as a tiebreaker, it passes, except for that
filibuster thing going on. You know, the filibuster, the tool that was used many
times throughout American history to deprive people of their civil rights.
That's how it's being used today as well. This hunger strike, it is definitely
aimed at President Biden, asking him to make good on his campaign promises. At
the same time, it's not really a request to push through voting rights
legislation. They've tried. This is a request to end the filibuster. This is a
request to get rid of that rule. Remember, the filibuster is not something outlined
in the Constitution. It's a rule that the Senate was just like, okay, we're going to do it this way.
The purpose of it, in theory, was never to actually allow the minority party to
stop legislation from going forward. The idea was to make sure that every bill
was given ample debate and real consideration. That's the theory. That's
not necessarily how it's been used. So Joe Madison says we are in a new civil
rights movement and he is pushing to get that legislation through, to get rid of the
filibuster. I'm curious as to whether Republicans are going to take notice of
this, whether they're going to talk about it, whether this is going to weigh on their
conscience, because when you're talking about this type of demonstration, that's
really what it's about. You're appealing to the humanity of the opposition.
Republicans, if just a few of them wanted to demonstrate that they had humanity,
they could allow this to go through, not filibuster it. With that as an unlikely
option, the responsibility falls to the Democratic Party to end the filibuster.
We've talked about it before. It's a risky proposition, but allowing large
segments of the American population to be disenfranchised at this level, that's
pretty risky too. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}