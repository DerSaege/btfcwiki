---
title: Let's talk about hot takes, the Christmas parade, and intent....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LyvRzwjSCvE) |
| Published | 2021/11/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Speculation and assigning political intent to incidents like the Christmas parade tragedy lead to sensationalism and ratings-driven coverage.
- Despite law enforcement denying a political motive and linking the incident to domestic violence, the story won't receive prolonged media attention.
- The complex solutions to issues like domestic violence, mental health support, and toxic masculinity are not attractive for media coverage.
- Rather than focusing on blame, the tragedy could serve as a catalyst for meaningful societal change.
- Media's prioritization of sensationalism over in-depth analysis prevents constructive dialogues on critical issues.
- Guard against misinformation by questioning narratives that lack evidence or intent speculation.
- The coverage of the incident initially fed on fear and speculation without concrete information about the suspect's motives.
- Society's aversion to in-depth, nuanced dialogues means the story will likely fade quickly from media attention.
- The incident bears similarities to other mass incidents, with multiple stressors and a domestic violence connection, but this might not be the focus of media coverage.
- Meaningful change requires moving beyond sound bites and pointing fingers to address systemic issues.

### Quotes

- "Ignore hot takes."
- "Our media isn't useful for that."
- "This incident could be the catalyst for good if we talked about it."

### Oneliner

Speculation and sensationalism overshadowing a tragedy, preventing meaningful societal change dialogues.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Challenge misinformation by questioning narratives lacking evidence (implied)
- Engage in meaningful dialogues about complex societal issues like domestic violence and mental health support (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how media sensationalism and speculation prevent meaningful dialogues on critical societal issues.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about hot takes and Christmas parades and assigning intent
to things and why this story is going to disappear.
Why it's not going to be a massive round the clock story for the next two weeks as is often
the case after situations like this.
As soon as it happened last night, as soon as news broke, speculation began.
Was the suspect right wing, left wing?
Were they white?
Were they black?
And it started right then.
People attempting to assign intent, assign a political motive to it because that's what
sells.
That's what gets viewers.
That's what gets clicks.
I do have to say I am super proud of every political violence expert I follow on Twitter
because everybody was screaming into the void.
We don't know any of this.
We can't answer this question.
We can't speculate as to intent.
But that was ignored for the sake of ratings.
News outlets ran coverage guessing about what it was, asking questions when they're supposed
to be providing answers, inflaming a situation that was already inflamed.
Now if you don't know, law enforcement has come out, they said they don't think there's
a political motive and that the suspect had just left the scene of a domestic violence
incident.
Wow that sounds like a whole lot of other mass incidents doesn't it?
A bunch of stressors at once and a DV connection.
And that's why this story is going to disappear because we're not going to do anything about
it.
The solutions, they're not sound bites.
They're not easy, feel good answers.
So it's going to be ignored.
Not going to generate clicks so it won't get coverage.
The reality is this could be used as a catalyst to talk about the DV connection to other mass
incidents.
It could be used as a catalyst to close some of those loopholes.
It could be used to talk about how maybe we should have counselors and mental health professionals
available.
It could be used to talk about toxic masculinity.
Could be used to talk about how we should probably be teaching some kind of conflict
resolution skills.
Could be used to talk about all of that.
But see those are complex things.
Nobody wants to hear that.
They want to know who to blame.
They want to point the finger at the other team rather than look for the solution.
This incident could be the catalyst for good if we talked about it.
But we won't.
The media at large is not going to continue to carry this story.
It will not get round-the-clock coverage for weeks because there's no money in it.
At the end of it, well, thoughts and prayers.
That's what's going to happen.
Ignore hot takes.
All of the coverage was trying to assign intent.
If you go to the other channel and watch that video about consuming information, you'll
find out that one of the best ways to guard against misinformation is to look for people
who are assigning intent that they can't back up.
They can't prove.
They're just guessing.
Because when they do that, they're shaping a narrative.
And that's what was happening.
Before we knew anything about the suspect, people were already throwing out that T word.
All we knew at the time when that speculation began was that a car had gone into a Christmas
parade and that it appeared deliberate.
That's it.
We didn't even know for sure that it was deliberate, just that it looked it.
But you had hours of coverage, fear-mongering, speculating, with nothing to back it up.
Now that the intent is known, that the situation is a little clearer, this story will more
than likely disappear because this country is apparently completely unwilling, not ready,
or unable to have conversations that amount to more than a sound bite.
Our media isn't useful for that.
This incident, it shares similarities with every other mass incident that's occurred.
Multiple stressors hitting the person at once, a DV incident, and then the lashing out.
It's there, but I'm willing to bet that won't be the coverage.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}