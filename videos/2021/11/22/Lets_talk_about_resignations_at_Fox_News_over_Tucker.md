---
title: Let's talk about resignations at Fox News over Tucker....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nbyG42KGhag) |
| Published | 2021/11/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the recent resignations of Stephen Hayes and Jonah Goldberg from Fox News due to Tucker Carlson's documentary about the events of January 6th.
- Hayes and Goldberg believed that Fox News' top opinion hosts, including Tucker Carlson, amplified false narratives to support Trump over the past five years.
- The resignations were fueled by concerns of potential violence incited by spreading misinformation to viewers.
- Beau mentions that Fox News' news division contradicted the information presented by Tucker Carlson.
- There's a fear that the misinformation spread by major hosts like Tucker Carlson could lead viewers to take harmful actions based on false information.
- Hayes and Goldberg's resignations and comments may play a significant role legally if any incidents occur as a result of misinformation spread by Fox News hosts.
- Despite Hayes and Goldberg not being as prominent as Tucker Carlson, their resignations shed light on the potential consequences of spreading inflammatory misinformation.
- Beau expresses concern that inflammatory information presented by Fox News might provoke certain viewers to react, potentially leading to violence.
- Beau hopes for a peaceful resolution but acknowledges the possibility of misinformation inciting harmful responses.
- The resignations of Hayes and Goldberg are emphasized as significant events to be remembered, especially if their concerns materialize in violence.

### Quotes

- "If a person with such a platform shares such misinformation loud enough and long enough, there are Americans who will believe and act upon it."
- "I hope that Hayes and Goldberg are wrong and that cooler heads prevail."
- "Either way, remember these resignations because if it does occur, you'll hear about them again."

### Oneliner

Beau addresses resignations at Fox News over misinformation spreading, fearing potential violence due to inflammatory content.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Monitor and fact-check information from news sources (implied)
- Advocate for responsible journalism practices (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the concerns surrounding misinformation in media and its potential real-world consequences.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk a little bit about Tucker Carlson, Fox News, and two people
who aren't with Fox News anymore.
Stephen Hayes and Jonah Goldberg resigned.
They left Fox News because of Tucker Carlson's documentary about what happened on the 6th.
They had some words that Fox corporate should probably pay attention to.
They talk about how over the past five years Fox's top opinion host basically amplified
Trump's false narratives and created some of their own just to help him out.
They go on to talk about how Tucker's little thing wasn't an isolated incident, it was
just the most egregious.
They say that what's contained in his production is contradicted not just by common sense,
not just by the testimony and on-the-record statements of many participants, but by the
reporting of the news division of Fox News itself.
Then it goes on, and this is the part that I think Fox should definitely pay attention
to.
If a person with such a platform shares such misinformation loud enough and long enough,
there are Americans who will believe and act upon it.
They are resigning because they're worried about violence.
That's their concern.
They've made that pretty clear.
If it occurs, and I don't believe that Hayes and Goldberg are off the mark on this, if
it occurs, these resignations, these comments are going to matter to Fox when it comes to
court.
The concern is that with some of their main hosts spreading misinformation, that it may
inflame some of their viewers, that it may cause them to take actions based on the information
that Tucker and Fox gave them, information that their news division contradicts and says
isn't true.
That's going to be really hard legally to overcome if somebody was to take them to court
over it after an incident.
I'm not sure, these names, they're big, but they're not huge.
They're not Tucker Carlson huge.
I'm not sure that this is going to be enough to sway the content there, but it's definitely
something that's going to become important later because I don't believe that they're
wrong in what they think is likely to happen.
The information that gets presented and the way it gets presented is intentionally inflammatory
and it is likely, my uneducated opinion, that it might produce a response from some of their
viewers.
I hope that Hayes and Goldberg are wrong and that cooler heads prevail.
I'm not certain that that's the reality we live in, but either way, remember these resignations
because if it does occur, you'll hear about them again.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}