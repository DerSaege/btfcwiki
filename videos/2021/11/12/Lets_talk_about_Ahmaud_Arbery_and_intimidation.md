---
title: Let's talk about Ahmaud Arbery and intimidation.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=u6ddiXdtNYY) |
| Published | 2021/11/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Commentary on the Ahmaud Arbery case and defense attorney's controversial statements.
- Defense attorney took issue with Reverend Al Sharpton sitting with the family.
- Attorney expressed not wanting any more black pastors involved.
- Focus on the attorney's statement about not wanting Reverend Sharpton there.
- Beau questions what exactly about Reverend Sharpton is intimidating.
- Raises the point that the jury is aware of the national attention on the case.
- Beau challenges the notion of Reverend Sharpton being intimidating due to his age and profile.
- Questions if a different religious figure's presence, like Pat Robertson, would be deemed intimidating.
- Disputes the defense attorney's assertion that Sharpton's presence is intimidating.
- Beau connects this to the broader issue of how skin tone alone can be perceived as a threat.
- Emphasizes the significance of discussing how skin tone can influence interactions with law enforcement.
- Concludes by encouraging reflection on the implications of deeming a 70-year-old pastor intimidating based on race.

### Quotes

- "There is nobody sitting on that jury right now who is unaware of the fact that national eyes are on that case."
- "What exactly about Reverend Sharpton is intimidating?"
- "For whatever reason, the skin tone alone is enough."
- "Nobody is going to look at Reverend Sharpton and be intimidated. It's a fiction."
- "A pastor who's almost 70 years old. Still intimidating."

### Oneliner

Beau challenges the defense attorney's claim of Reverend Al Sharpton being intimidating in the Ahmaud Arbery case, shedding light on the broader issue of racial perceptions and systemic biases affecting interactions with law enforcement.

### Audience

Activists, Justice Seekers, Advocates

### On-the-ground actions from transcript

- Challenge and confront instances of racial bias and intimidation in your community (implied).
- Support individuals who face discrimination and intimidation based on their race (implied).

### Whats missing in summary

Detailed analysis of the intersection of race, perception, and intimidation in legal proceedings.

### Tags

#AhmaudArbery #RacialJustice #SystemicBias #LawEnforcement #CommunityEngagement


## Transcript
Well, howdy there, Internet people. It's Bo again.
So today, we're going to talk a little bit about the Ahmaud Arbery case
and some of the commentary that's going on
because of what that defense attorney said.
Because the commentary is focusing on one of two things,
and I get it, but when I was listening to it,
I heard something else that seems far more important to touch on.
If you don't know what happened, an attorney for the defense
said some wild stuff.
Reverend Al Sharpton went down and sat with the family.
And the defense attorney took issue with this.
He had a problem with it.
He said that it wasn't disruptive, didn't cause a distraction,
but that he didn't want him in there anymore.
And in the process of this, he said,
we don't want any more black pastors.
And most of the commentary is focused on that comment.
And sure, there's definitely room to discuss that.
And then another group of people is pointing to the fact that after that,
he said, if Al Sharpton is their pastor, then fine, but that's it.
Yeah, let me tell you, who doesn't get to tell that family
how many pastors they can have?
And again, I get it.
There's room for commentary on both of these things.
And maybe I'm way off base on this.
But see, I heard something else.
There is nobody sitting on that jury right now
who is unaware of the fact that national eyes are on that case.
Everybody on that jury is aware of that.
He said that when it came to Reverend Sharpton's presence,
he said that, I believe it's intimidating.
Intimidating.
What exactly about Reverend Sharpton is intimidating?
Specifically, what is it about him that's intimidating?
I mean, no disrespect to the man when I say this,
but pull up a recent photo of him.
I'm sure in his day, he was probably a really tough dude.
That man is almost 70 years old.
I don't know that intimidating is the first word that would spring to mind.
And I'm not buying the idea that it's because he's a high-profile African-American.
I don't buy that at all.
Intimidating.
I wonder if the religious figure that had come to sit with the family,
if it had been Pat Robertson, if he would have been intimidating.
To say that in a case like this, a case that hinges on
and was brought about somebody's perception of somebody,
that while it hasn't been proven by the court,
it certainly appears as though it was in large part based on race.
That just seems like such a wild statement.
A statement that honestly I wish the jury had heard.
Recently I had a friend say something to the effect of,
well, my skin tone gets viewed as a weapon anyway.
And a white friend of ours didn't understand what he meant.
This. This is what he meant.
It's intimidating.
It is intimidating.
For whatever reason, the skin tone alone is enough.
If you hear somebody say that or make reference to it,
this is what they're talking about.
Nobody is going to look at Reverend Sharpton and be intimidated.
It's a fiction.
It's made up.
And again, there's nobody on that jury who is unaware
that the entire black community is looking at that case.
So I don't buy that as an excuse.
That was the word that came to mind.
Intimidating.
It seems like this might be a moment to talk about that.
When people talk about how their skin tone puts them at risk,
how it elevates the posture of law enforcement.
This is a good example of it.
A pastor who's almost 70 years old.
Still intimidating.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}