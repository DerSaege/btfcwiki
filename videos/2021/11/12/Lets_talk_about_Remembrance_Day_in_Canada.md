---
title: Let's talk about Remembrance Day in Canada....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6iI7VPlfwjM) |
| Published | 2021/11/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing Remembrance Day in Canada and a group using it to express anti-vaccine beliefs.
- Comparing Remembrance Day to Memorial Day in the United States.
- Explaining the origins of Remembrance Day from Armistice Day after World War I.
- Connecting the significance of wearing the poppy flower to those lost during World War I.
- Sharing historical insights on the Spanish flu during World War I and its impact on battle plans.
- Mentioning the different theories about the origin of the Spanish flu, including its link to World War I trenches.
- Describing how the flu changed battle plans and was downplayed by the media due to patriotism.
- Criticizing the misinformation spread about the flu during the war and its consequences.
- Questioning the accuracy of reported flu-related deaths, particularly in Canada.
- Calling out the irony of using Remembrance Day to express anti-vaccine beliefs while wearing a poppy symbolizing lost lives.

### Quotes

- "If they have asked you for your papers they have already forgotten, this Remembrance Day has more significance than all the ones we've had since World War II."
- "I just want to point out that if you took this moment to stage this little stunt, you're wearing a poppy that symbolizes a whole lot of people who were lost because of great patriots just like you."

### Oneliner

Beau addresses the misuse of Remembrance Day for anti-vaccine beliefs and criticizes the irony of such actions while honoring lives lost during World War I.

### Audience

Canadians, History Enthusiasts

### On-the-ground actions from transcript

- Respect the significance of Remembrance Day and honor the sacrifices made by wearing a poppy (exemplified).

### Whats missing in summary

Deeper historical context and analysis on the connection between pandemics, war, and misinformation.

### Tags

#RemembranceDay #Canada #SpanishFlu #Misinformation #AntiVaccine #History


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Remembrance Day in Canada.
I don't normally comment on internal issues in other countries, mainly because I don't
feel like I have anything to add, but I feel like I have something to add to this.
It appears that on Remembrance Day a group of people decided to use that as their platform,
use that as the moment to espouse their anti-vaccine beliefs.
For those in the United States, Remembrance Day, even though it lines up with our Veterans
Day, it's more akin to Memorial Day, it's a somber day.
Somebody said at one of these ceremonies into a microphone, if they have asked you for your
papers they have already forgotten, this Remembrance Day has more significance than all the ones
we've had since World War II.
Fun fact, Remembrance Day grew out of Armistice Day, which marks the end of World War I.
You know, that's why you wear the poppy, that flower, it's a reference to a poem about Flanders
Fields and symbolizes those lost during World War I.
World War I ended in 1919. You know what else was going on at that time? A flu. A bad one.
You know, we call it the Spanish flu. We call it that because Spain wasn't involved in the
war. Because they weren't involved in the war, their media did their job. They covered
it. They reported on it. Whereas a lot of the countries, all of the countries that were
involved in the war didn't. Nobody knows for sure where it started, but the prevailing
theories are that the first recorded cases were in Kansas. Another suggests that it actually
grew out of the trenches of World War I. That's the one I believe. Those with mild cases,
well, they stayed and fought. Those who were really sick, well, they were sent to the rear
or went home, which helped it spread. Just seems to make sense to me.
The flu had such an impact that it changed battle plans. Offensives were delayed because
of it. And because it was impacting what was going on on the battlefield, well, the media
in the countries that were involved in the war, they didn't really want to report on
it. It was unpatriotic. Because no general wants their opposition to know that their
troops are sick. Because that would be the perfect time to try to cross no man's land,
right, and go after them. Instead, the media in the countries that were involved in the
war, they downplayed it. They produced misinformation about it. They lied repeatedly. Because of
that, people didn't take the precautions they should. And it spread and it took out tens
of millions. Great patriots of the various countries made this decision. It was unpatriotic
to worry about the flu. Not when our boys were in harm's way, right? And that caused
more and more of them to be lost. Now, because countries were less than honest about what
was going on, it's hard to get good reads on how many were lost. I've seen numbers for
Canada, but I'm honestly pretty skeptical of them. Because they're very, very low in
comparison to other nations. The US, as an example, lost about 45,000 to the flu, not
the fighting. I just want to point out that if you took this moment to stage this little
stunt, you're wearing a poppy that symbolizes a whole lot of people who were lost because
of great patriots just like you. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}