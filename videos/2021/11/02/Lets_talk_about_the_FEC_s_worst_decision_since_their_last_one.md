---
title: Let's talk about the FEC's worst decision since their last one....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IVPYOn-NaTY) |
| Published | 2021/11/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Federal Election Commission made a troubling decision allowing foreign entities to fund referendums in the United States, raising concerns about foreign influence.
- There are worries about political figures acting as agents for foreign intelligence services and the implications of this decision.
- This decision opens up avenues for obtaining favor through referendums, potentially influencing election outcomes.
- Beau points out the potential strategy of using referendums to mobilize specific voting blocs, like churchgoers, to support certain political candidates.
- From a counterintelligence perspective, this decision is a nightmare due to the risks of corruption and external influence.
- Beau expresses concern about the increased potential for corruption at both federal and local levels because of this decision.
- States have the authority to enact their laws regarding foreign funding of referendums, which could help prevent further corruption.
- Beau suggests considering proposing ballot initiatives or referendums at the state level to counteract the FEC's decision.

### Quotes

- "This is just another way to obtain favor."
- "From a counterintelligence standpoint, this is a nightmare."
- "Yet another avenue to funnel money, to encourage corruption."

### Oneliner

The FEC's decision allowing foreign funding of referendums raises concerns about corruption and foreign influence in US politics.

### Audience

Voters, Activists, Legislators

### On-the-ground actions from transcript

- Propose a ballot initiative or referendum at the state level to counteract the FEC's decision (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the risks associated with the FEC's decision and suggests potential actions to address the concerns raised. Viewing the entire video will provide a more in-depth understanding of the topic. 

### Tags

#FEC #ForeignInfluence #Corruption #Referendums #USPolitics


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about
the worst decision the FEC has made in quite some time. The Federal Election Commission
has decided that it is like totally cool for foreign entities to bankroll referendums inside
the United States. I don't know, maybe it's the professional paranoid in me who believes
that Humpty Dumpty didn't fall, he was pushed, but I just have lots of questions. I don't
see how anybody could decide that at a time when there is still widespread speculation
about certain high-profile political figures within the United States acting knowingly
or unknowingly as agents of influence for foreign intelligence services that this is
a good idea. Somebody thought this was a good idea. This is just another way to obtain favor.
If you are a Republican senator running in the deep south, sure they can't help you with
your campaign because that's an actual election, but that referendum, you know what? You know
who would help you, who would probably vote for you? Church folk. Why don't we run a referendum
about teaching Bible study as an elective in public schools? Doesn't matter if it gets
shot down in the courts, all we're doing is helping you get the vote out. What was in
that briefing you just went to? From a counterintelligence standpoint, this is a nightmare. This is a
nightmare. Aside from that, I was just thinking myself that the one thing US politics didn't
have enough of was groups of people bidding on our politicians. Yet another avenue to
funnel money, to encourage corruption. I'm just looking at the federal level. When you
get to the state and local level, this is going to be wild. Now, the FEC's decision
only applies to federal rules. Your state can enact their own laws about this. They
probably should. This is an open door for even more corruption. So I would, I don't
know, maybe do a referendum, propose a ballot initiative or something. Anyway, it's just
a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}