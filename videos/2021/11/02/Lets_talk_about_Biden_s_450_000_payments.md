---
title: Let's talk about Biden's $450,000 payments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eJjFtQz2Zik) |
| Published | 2021/11/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the controversy surrounding Biden's $450,000 payments to those separated at the border.
- Acknowledges the outrage and grants permission for viewers to express disapproval to representatives or the White House.
- Clarifies that the payments are actually settlements from lawsuits the Biden administration anticipates losing.
- Points out that the Trump administration's zero tolerance policy likely violated U.S. law, international law, and the Constitution.
- Predicts that if the cases go to court, requested payouts could be much higher than the settlement offer.
- Suggests that going to court could expose more information, potentially embarrassing the country further.
- Notes that settling quickly is an attempt by the Biden administration to avoid further embarrassment.

### Quotes

- "If you can't defend them, it's clearly wrong."
- "If it goes to court, you're going to be mad later."
- "Settling quickly is an attempt to avoid further embarrassment."
- "These settlement payments are a gift to the Republican Party."
- "Y'all have a good day."

### Oneliner

Beau clarifies the controversy over Biden's $450,000 payments as settlements from lawsuits and predicts potential embarrassment if cases go to court.

### Audience

Viewers concerned about Biden's settlement payments.

### On-the-ground actions from transcript

- Contact your representative or the White House to express disapproval with the policy (implied).

### Whats missing in summary

Insights on the potential implications of the settlement payments and the importance of understanding the context behind the controversy.

### Tags

#Biden #SettlementPayments #BorderSeparation #Lawsuits #TrumpAdministration


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about those
$450,000 payments that the Biden administration wants to make. Everybody's asking about them.
The people who sent messages were just asking for a breakdown on what's going on,
most of the people who sent messages sent messages in the vein of what I'm about to read.
Beau, you have been incredibly silent when it comes to Biden's $450,000 payments to those people
who were separated at the border. It's not what it says, but that's what we're going to say on
the channel. I guess even you can't defend his actions. If you can't defend them, it's clearly
wrong. Why don't you tell your audience that it's okay to be mad about this?
Okay, I haven't talked about it because I didn't think y'all were serious. I didn't know that this
was real outrage. I thought it was fake outrage. It's hard to tell sometimes. Apparently you want
my permission to contact your representative or contact the White House to express your
disapproval with this policy. You have it. You have my blessing. Go right ahead and do it.
I won't stop you. But before you pick up the phone, let me give you some context. Everybody
who sent a message called it a payment. And sure, that is an accurate enough term. It is in fact a
payment. A more accurate term would be a settlement from a lawsuit. A bunch of them. The Biden
administration is trying to settle a bunch of lawsuits at once. Why not? Because they're
going to lose. They're going to lose. So they want to settle. It's kind of that simple.
As we discussed when this was going on, back when Trump had his zero tolerance policy in place,
it is an almost certainty that the Biden administration is going to settle
this. Back when Trump had his zero tolerance policy in place, it is an almost certainty
that the Trump administration's policy violated U.S. law, international law,
treaties we're a party to, and violated the U.S. Constitution all at once.
They can't win in court. That's why they're willing to settle. If you just want a glimpse
of the protocols for the treatment of refugees and then read the supremacy clause of the U.S.
Constitution, those protocols, law of the land, just start there. And that's just the tip of the
iceberg, really. Okay, so if you don't want these payments to be made, what happens? If you are
successful in your campaign to convince the Biden administration not to settle, what occurs? Each
case goes to court. It costs a whole lot more money for the trials. And it's also worth noting
that in these lawsuits, the average requested payout, I want to say it's around three and a
half million. It's more than three million dollars, I know that. So they'll be paid more.
This is a lowball settlement offer. My guess is that it's probably going to go up. So if you're
mad now, boy, you're going to be mad later. If it goes to court, this other thing's going to happen
too. They'll be able to spin a record. The Biden administration has made it pretty clear that they
have absolutely no interest in exerting executive privilege over Trump's decision-making process,
which means we'll get to find out a whole lot of stuff.
These settlement payments, they're a gift to the Republican Party. They are a gift. If all of these
go to court, we're going to find out all kinds of stuff. This is the Biden administration doing what
administrations do. They're trying to sweep this under the rug in a way. They're trying to
make amends as quickly as possible and as quietly as possible so it doesn't further
embarrass the country. That's what's occurring. You don't want them to do that. That's fine with
me. I don't care. If I were you, though, and I was angry about this, I would try to figure out why.
My preferred source of information, my right-wing talking head, the person that I watch,
why they didn't give me this context, why they didn't explain what was going on.
I'm willing to bet it's simply because it makes the Trump administration look bad.
You want to oppose this, you have my permission. Apparently that's important.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}