---
title: Let's talk about Big Bird, Ted Cruz, and an opening....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lJO8jacO8XM) |
| Published | 2021/11/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans trying to reframe themselves as more favorable to suburban voters by concealing their far right-wing beliefs.
- Recasting opposition to public health measures as parental rights.
- Need to maintain support from the MAGA base while appealing to suburban voters.
- Right wing outrage over Big Bird getting vaccinated.
- Accusations of Big Bird being a communist and government propaganda.
- Republicans speaking out of both sides of their mouth to cater to different factions.
- Use of Big Bird getting vaccinated as a political issue to mask true intentions.
- Republicans concealing their true beliefs and goals, presenting a false image.
- Inconsistencies emerging in their messaging due to hiding their far right-wing stance.
- Democrats have an opening to go on the offensive by questioning these inconsistencies.

### Quotes

- "They have to hide that. If they say who they are, if they're honest about who they are and where they stand on things, well, it's not electable, so they have to twist it."
- "When you are talking about a new variation of Republican that is outside of the confines of what we traditionally expect to find, the moral battlefield is the front line."
- "I'm willing to bet that most suburban moms aren't going to be cool with them calling Big Bird a communist."

### Oneliner

Republicans hide far right-wing beliefs by rebranding, using Big Bird's vaccination as a political issue while Democrats have an opening to question inconsistencies.

### Audience

Voters, Democrats

### On-the-ground actions from transcript

- Question Republican candidates on their stance regarding the politicization of Big Bird's vaccination (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the political strategy behind the Republican Party's attempts to reframe themselves and the potential for Democrats to capitalize on inconsistencies in messaging. Viewing the entire transcript offers a comprehensive understanding of these dynamics. 

### Tags

#Republicans #PoliticalStrategy #BigBird #Vaccination #Democrats


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to, yeah,
we're gonna talk about Big Bird.
We're gonna talk about Big Bird.
And a unique development in American politics.
Last week, we talked about how Republicans
were going to try to recast themselves,
reframe themselves in ways that are more
favorable to suburban voters. Ways that would allow them to be seen in a better
light by people who live in the suburbs. They looked at Virginia, they saw a
winning strategy, and they're going to try to duplicate it. They're going to try
to cast themselves as traditional conservatives and conceal who they
really are and hide the fact that they have morphed into a far right-wing
variation that is well outside the confines of what you would traditionally expect to
find in American politics, what you would traditionally expect to find in a representative
democracy.
They have to hide that.
If they say who they are, if they're honest about who they are and where they stand on
things, well, it's not electable, so they have to twist it.
They have to massage their own image.
One of the things we said they were going to do was recast their opposition to public
health as parental rights.
But for this to work, for this strategy to be successful, they also have to hang on to
that MAGA base.
So every once in a while, they have to feed into the outrage machine, the anger, the fear,
hatred that keeps the MAGA base mobilized. And this is where there will be an opening.
If you don't know, the right wing in the United States has decided it is time to cancel Big
Bird because Big Bird engaged in the most egregious of behavior. He got vaccinated.
Now it is worth noting that I remember Big Bird getting vaccinated as a kid.
Big Bird has been a pro-vaccine for, I don't know, almost half a century.
It's not a shock.
It's not outrageous.
Everybody knew this was going to happen because it always does.
But once it did, the outrage machine went into effect.
Those people who are trying to recast themselves as being in favor of public health measures,
well they went on the offensive.
My favorite comment was from Wendy Rogers, who I would check this to make sure I'm 100%
accurate, but she blocked me on Twitter.
said, Big Bird is a communist. Ted Cruz had some commentary about comrade Big Bird himself.
He said that it was government propaganda. This is a common refrain coming from the right.
Big Bird did something wrong by getting vaccinated. This is where the opening arises because they
They have to speak out both sides of their mouth.
They have to talk to that far right anti-science, that crowd over there.
They have to appeal to them somehow without scaring the suburban voters.
So from now on, when these candidates, when they come out and say, no, I'm vaccinated,
I'm in favor of the vaccine, Big Bird has to be brought up.
Then why was Big Bird getting vaccinated an issue?
What was the opposition to it?
If you're in favor of vaccination and you just think it's a parental rights issue, why
is Big Bird getting vaccinated?
Why is that a problem?
These inconsistencies are going to occur because they have to hide who they really are.
They have to conceal the fact that they aren't your granddad's Republicans, that they're
a different crop, that they have different goals, that they aren't conservative, that
they are in fact far right outside of the scope of what we expect to find.
That's why they have to reframe themselves, because they are outside of the Overton window
in the United States.
So they have to pretend that they're still somewhat in favor of the system of governance
we have in the United States.
They have to pretend like they still support the country on one hand while still talking
to those who would radically transform it and take away most people's voices on the
other.
When these inconsistencies arise, this is the point where the Democratic Party, if they
were so inclined, could go on the offensive.
People need to be asking these Republican candidates what the problem is.
Why is Big Bird's vaccination record a political issue?
Why is that a national topic of discussion?
Why are you feeding your base?
If you are for the vaccine, if you are for public health, why are you calling it propaganda?
Why are you calling Big Bird a communist over this?
Why are you leaning into this idea that the vaccination is actually bad?
These inconsistencies are going to emerge with every topic they try to reframe themselves
on and it's the only opening Democrats are going to have to go on the offensive.
have abandoned the moral battlefield in the United States. When you are talking
about a new variation of Republican that is outside of the confines of what we
traditionally expect to find, the moral battlefield is the front line. You are
fighting for the soul of this country. As they attack American institutions like
Like Big Bird, that might be the moment to actually go on the offensive.
To actually point this stuff out.
I'm willing to bet that most suburban moms aren't going to be cool with them calling
Big Bird a communist.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}