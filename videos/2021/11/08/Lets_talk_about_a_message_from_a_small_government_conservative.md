---
title: Let's talk about a message from a small government conservative....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Re2-E1cyEwg) |
| Published | 2021/11/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A small government conservative Republican is feeling disconnected from the Republican Party.
- The Republican Party is no longer for small government conservatives, but rather nationalists.
- Small government conservatives are now more in alignment with the Democratic Party.
- Being a small government conservative means accepting individual responsibility for the community.
- Beau suggests reading works by Lawrence Britt and Umberto Eco to understand political strategies.
- Beau predicts political moves based on what a fascist might do, not what a Republican might do.
- The essence of being a small government conservative is allowing others their freedoms as long as they don't harm anyone.
- Republican big government now focuses on limiting individual liberty, contrasting with Democrats who aim to uplift working-class people.
- The disconnect felt by small government conservatives stems from the shift in the Republican Party's values.
- Beau encourages the small government conservative to focus on community and individual responsibility.

### Quotes

- "I have never asked myself, what would a Republican do next? I've asked myself, what would a fascist do next?"
- "The reason you feel disconnected is because they're not small government conservatives anymore."
- "Small government conservatives are more in alignment with the Democratic Party."
- "Republican big government now is about curtailing individual liberty."
- "You have to accept that responsibility. I got it."

### Oneliner

A small government conservative Republican feels disconnected from the current Republican Party and finds alignment with Democratic values, focusing on community responsibility.

### Audience

Small government conservatives

### On-the-ground actions from transcript

- Read works by Lawrence Britt and Umberto Eco to understand political strategies (suggested).
- Focus on community and individual responsibility (implied).

### Whats missing in summary

The full transcript provides a deeper insight into the shift in values within political parties and the impact on small government conservatives.

### Tags

#SmallGovernment #RepublicanParty #PoliticalShifts #CommunityResponsibility #Alignment


## Transcript
Well howdy there internet people, it's Beau again. So today we have a message from a
mythical creature. Somebody that a whole lot of people watching this channel
don't believe actually exists, but I assure you they are out there in the
wild somewhere. We have a message from a small government conservative, a real one.
Okay, I've been a Republican since I was old enough to vote. I'm older than you
are now. I follow politics closely and I've been watching your videos for
about two years. I'm curious how you're able to consistently predict what
strategy Republicans will use next. I'm a Republican and can't guess what
they're going to do next. They've become so disconnected from us. I'm one of those
people you call small government conservatives. I don't hate anybody. I
don't mind trans people. I've only met two, but they were both really nice. I
don't give a darn which bathroom they use. I got vaccinated because I'm not an
idiot. I'm just wondering when you think they'll get back to the values we once
had. My vaccinated brother was trashing Big Bird today in front of his kids. I'm
supposed to be mad at a word you definitely would not hear on Sesame
Street, Big Bird. I understand that you might not want to answer this on the
channel if you think they'll get back to normal soon. I know you say you aren't a
Democrat, but you're definitely pulling for them most times. You probably don't
want to reduce your side's enthusiasm. So if you don't want to answer or make a
video, could you please send me whatever method you use to make your predictions.
The only thing that makes me feel better is that Democrats seem just as
disconnected from their voters as Republican politicians are from us. I
don't mind if you use my message, but please don't use my name. Small
government conservative. Okay, I'm going to answer your question as far as how I
make my little guesses, but I'm going to do it at the end. First, I want to kind of
dive into something. The reason you feel disconnected is because they're not
small government conservatives anymore. The Republican Party is not for small
government conservatives. You may hear people say that that's what they are, but
if you're really a small government conservative, you can't support the
Republican Party anymore because they're nationalists. They're just as big
government, if not more so, than Democrats. The difference being that the
Democratic Party tends to at least end rhetoric, even if they don't pull it off,
even if they may not necessarily even try, some of them. Their big government is
more designed to give working-class people a leg up. Republican big
government now is about curtailing individual liberty. Now the only real
exception you're going to find to this is, I guess, the vaccine mandate, but given the
fact that you're a small government conservative, that doesn't actually
bother you, does it? It doesn't because part of the ethos of being a small
government conservative is accepting individual responsibility for your
community, right? So you're going to get vaccinated to protect your community,
because if you want a small government, you have to accept that responsibility. I
got it. All of this makes sense. I totally get the bathroom thing, too. I totally
understand it, because part of the core essence of being a small government
conservative is that other people can do whatever they want, as long as they don't
hurt anybody. If they hurt somebody, then you come down on them like a pile of
bricks, right? But up until they hurt someone, it doesn't matter what they do,
even if you don't necessarily approve of it. You're okay with it, as long as
they're not hurting anyone, right? That's not the Republican Party today. Small
government conservatives are more aligned with the Democratic Party than
they are the Republican Party because of this switch, because the whole idea of
being a small government conservative is not being told by the government what
bathroom to use. You have your fence up. Whatever happens on the other side of
that, as long as it isn't hurting anybody, that's not your problem. You don't get
involved. I get it. Believe me. Okay, so how do I guess what they're going to do
next? If you want to use the method I do, you have to do a little bit of reading.
It's not hard reading. It's not heavy reading. But you need to read some work
by a person named Lawrence Britt and a person named Umberto Eco. They both
wrote about the same subject. Britt was more about the pragmatic side of it, the
actual effects. Umberto Eco was more about the philosophical side. If you're
into politics as much as you seem, start with Umberto Eco, and you probably
don't even need to read Britt, to be honest. Once you're done with that,
understand that in the last five years or so, I have never asked myself, what
would a Republican do next? I've asked myself, what would a fascist do next? The
record of accuracy speaks for itself. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}