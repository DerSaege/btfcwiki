---
title: Let's talk about Gohmert and the Texas attorney general's race....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=e4cM6mkEfT8) |
| Published | 2021/11/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Congressman Louie Gohmert has entered the Texas Attorney General race, forming an exploratory committee.
- Gohmert's main talking point is the potential indictment of current attorney general, Ken Paxton.
- There's a website, gomert.net, where you can find more information.
- Texas law states that if someone wins a primary and is indicted after, their name stays on the ballot.
- Gohmert is seeking donations, aiming for $1 million by November 19th from 100,000 citizens.
- Concerns are raised about the potential implications of having a Democrat as Attorney General in Texas.
- Gohmert's entry into the race is seen as a move to interject into the election and push back against fraud claims.
- The transcript underscores the need to take this election seriously despite the humor involved.
- Potential consequences of putting Gohmert in such a position are discussed.
- The importance of maintaining free and fair elections in a representative democracy is emphasized.

### Quotes

- "They learned nothing."
- "These fraud claims will continue to be echoed in any election they lose."
- "Putting somebody like Gohmert in that position would allow them to greatly alter the outcome of an election."

### Oneliner

Congressman Gohmert's entry into the Texas Attorney General race raises concerns about potential implications, underscoring the need for vigilance in maintaining election integrity.

### Audience

Texans, Voters

### On-the-ground actions from transcript

- Visit gomert.net to gather more information and stay informed (suggested).
- Donate to support Gohmert's exploratory committee if you choose to do so (implied).

### Whats missing in summary

Full context and depth of analysis on the potential impact of Gohmert's entry into the Texas Attorney General race.

### Tags

#Texas #AttorneyGeneral #Election #Gohmert #KenPaxton


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about the Texas Attorney
General's race again, because that is shaping up
to be a fun one in this election cycle.
It appears that Congressman Louie Gohmert has decided
to throw his hat into the ring there
and formed an exploratory committee to look into that.
A website has surfaced, it's gomert.net, if you want to go see this for yourself.
You know, there was that rule of never speaking ill of another Republican.
Well, apparently that is definitely gone, because the main talking point that Gomert
is presenting here is that current attorney general, Ken Paxton, fellow Republican, is
going to be indicted.
We need a Texas attorney general whose top attorneys working for him have not found it
necessary to send a letter to the FBI urging an investigation into corruption of their
boss.
Keep in mind, under Texas law, if someone wins a Texas primary and is indicted after
the primary, his name cannot be removed from the ballot. That seems to be the main talking point.
I cannot wait to hear Ken Paxton's response. It goes on to say, you know, here's how you can
help. This is exploratory. Though the proper documents have been filed to seek donations,
Louis needs 100,000 citizens to send $100 each or any other amount to get to $1 million by November
19th. Go ahead and do the math on that. 100,000 citizens sending $100 each gets you 10 million.
I'm not certain that I would trust this person with my money or any budget whatsoever.
But that's here. All jokes aside, though, there is one concerning little bit about this.
Also, keep in mind, if there is corruption or fraud in the four largest Texas cities in the
the November 2024 election, the district attorneys in those counties will likely still be Democrats.
The Biden DOJ will still be Democrat-controlled that year, and we cannot afford to have the
Texas Attorney General be a Democrat and turn a blind eye to problems Democrats have created.
It is a very open statement that Gohmert will interject into the election.
They learned nothing.
They learned nothing from what happened.
These fraud claims will continue to be echoed in any election they lose.
They're already planning for it now, and while the rest of this is really funny, putting
somebody like Gohmert in that position would allow them to greatly alter the outcome of
of an election in ways that aren't conducive to free elections and aren't really the way
a representative democracy is supposed to work.
So while there is a lot of humor here, it's also an election that needs to be taken seriously.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}