---
title: Let's talk about a Malcolm X quote....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3mEYPKSFZiA) |
| Published | 2021/11/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring a famous quote by Malcolm X about white liberals, often misrepresented and misunderstood.
- Malcolm X's warning about white liberals and their potential danger, compared to white conservatives.
- Malcolm X's emphasis on not falling prey to any political party and advocating for black Americans to find their own source of power.
- The historical context of Malcolm X's statements in relation to the Southern strategy and political power dynamics.
- Clarifying the misconception that Malcolm X's quote endorsed the Republican Party or right-wing ideologies.
- Beau's use of the term "white liberal" in contemporary contexts, pointing out the manipulation and empty promises made by politicians.

### Quotes

- "White liberals, well, they were foxes. White conservatives were wolves."
- "He was trying to advocate for black Americans to find their own source of power."
- "Whether it came from a wolf or a fox. But the fox is more likely to make promises they don't intend to keep."
- "Malcolm X today would be a right wing pro-gun conservative. Well, I mean, one out of three, I guess, isn't too bad."
- "That's what he was talking about."

### Oneliner

Beau dives into Malcolm X's quote on white liberals, clarifying its true meaning and advocating for black empowerment while debunking misconceptions.

### Audience

Activists, Political Historians

### On-the-ground actions from transcript

- Reach out to black creators on YouTube for impassioned explanations of Malcolm X's quotes (suggested)
- Advocate for black empowerment and community building (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of Malcolm X's perspective on white liberals and conservatives, urging for self-empowerment within the black community.

### Tags

#MalcolmX #WhiteLiberals #PoliticalPower #BlackEmpowerment #Misconceptions


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a quote, a very well-known quote,
a kind of surprising quote, if you only view part of it,
and a quote that is often misrepresented.
We're going to talk about it because in a recent video, I used a term and it prompted a question.
We're going to kind of dive into it, explain the quote itself, the context around the quote,
and its applications today. So today we're going to talk about Malcolm X and white liberals.
Now, before we get into this, I want to point something out.
Under normal circumstances, if you are serious and you are trying to get a good read,
you want an explanation or an interpretation of a Malcolm X quote,
perhaps reaching out to me isn't the best route.
There are thousands of brilliant black creators on YouTube who could probably provide a better explanation
that is more impassioned and has way more nuance than what I'm going to be able to.
I just want to put that out there before we get into it.
I don't mind answering this one because honestly,
I don't believe this person would watch a black creator on YouTube.
And I don't believe it's a serious question.
I think it's intended to be a gotcha question and it starts off with a lie.
OK, so what's the message?
As a white man who's extensively read Malcolm X, you know it's going to be good when it starts off like that.
I'm surprised you didn't bring him up when talking about white liberals.
Didn't he once say that white liberals were the most dangerous and thereby endorse Republicans?
I guess we know why you didn't bring it up.
Democrats try to own the civil rights leaders image, but that one quote makes it clear where they really stood.
Malcolm X today would be a right wing pro-gun conservative.
This pause is to let anybody who's actually listened to Malcolm X finish laughing at that last line.
No, he wouldn't be a right wing pro-gun conservative.
We'll come back to that. But we're going to focus on the quote, the white liberal quote.
This message doesn't actually pin down a specific quote.
The good thing is that Malcolm X can be accused of many things.
Being unclear isn't one of them.
Being inconsistent isn't one of them.
It is really hard to listen to Malcolm X and walk away with the wrong meaning.
This particular theme of the white liberal, he talked about it repeatedly.
And normally when he did it, he used the storytelling device, the imagery of wolves and foxes.
White liberals, well, they were foxes.
White conservatives were wolves.
The fox is more dangerous because that wolf, well, it's going to snarl at you openly.
It's going to let you know where you stand.
You don't have to wonder what the white conservative wolf is thinking.
But see that white liberal fox, maybe it pretends to be your friend.
Maybe it doesn't appear threatening, but it is.
That's what he's getting at.
Now, from there, he goes on to refute the idea that it has anything to do with endorsing a political party.
Every time I'm aware of him using this imagery and talking about this,
his main overall point is to advise black people not to fall prey to any political party.
Because he says that white folk, they don't divide themselves that way.
They don't view themselves as Democrats or Republicans.
They view themselves as liberals or conservatives.
And it's all about getting or maintaining power.
He says that a white liberal Democrat would vote for a white liberal Republican
before they would vote for a white conservative Democrat.
Therefore, the parties are just a facade.
Now today, that seems kind of weird, right?
Because liberals lean one way, conservatives lean the other, generally speaking.
But at the time he said this, the Southern strategy, well, it's starting to play out.
So that observation makes complete sense.
100% accurate.
That was definitely happening at the time.
So from here, you have to kind of wonder what the point is.
Because this sounds like something that is geared to just be like,
well, none of these politicians are going to help you.
Two wings of the same bird type of thing, right?
Stuff we still hear today.
That is not what Malcolm X was saying either.
He was trying to advocate for them to get their own source of power.
In fact, in one speech he goes on about how, I don't remember the exact numbers,
I want to say it was there was 8 million eligible black voters,
but only 3 million of them voted.
And he talked about how politicians bent the knee and tried to get those 3 million votes.
And just imagine what they would do for 8 million.
He was trying to advocate for black Americans to find their own source of power.
That's what it was about.
It definitely was not an endorsement of the Republican Party.
Now, this is why I used the terminology white liberal in that video.
Because it still goes on today.
The idea was that the political parties were just after getting and maintaining power
and using black Americans as pawns.
I think he used the term a political football.
I think is what he said.
But that's the general idea.
Was that they made promises they didn't intend to keep.
So you couldn't count on them.
Whether it came from a wolf or a fox.
But the fox is more likely to make promises they don't intend to keep.
Which is something that is occurring today with white liberal politicians.
So why use the term?
Now, to this last part.
Malcolm X today would be a right wing pro-gun conservative.
Well, I mean, one out of three, I guess, isn't too bad.
He would have been pro-gun.
Malcolm X would not be right wing anything.
This is a man who said, show me a capitalist, I'll show you a blood sucker.
I don't think this guy had any right wing allegiances whatsoever.
And again, that quote gets put on those memes.
The context of it isn't that white liberals are bad.
That white conservatives are good.
It's that they're both bad.
Neither one of them are actively pursuing helping the black community.
That's what he was talking about.
Again, when it comes to Malcolm X, this was an incredibly clear man.
He didn't mince his words.
That's why he got seen as such a militant.
It's because he didn't couch his language in anything.
He was just like, this is the way it is.
And he didn't try to soften it.
So would Malcolm X be a right wing pro-gun conservative today?
No.
You want an explanation of that quote?
There it is.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}