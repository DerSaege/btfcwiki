---
title: Let's talk about what Democrats can learn from the election results....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0mfLNp8ceWI) |
| Published | 2021/11/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the recent election and looking ahead to 2022 and 2024.
- Republicans' strategy involves fear-mongering to scare people, a tactic that has worked.
- Centrist Democrats trying to distance themselves from Trump won't be effective.
- Democrats need to figure out how to energize and mobilize urban voters, their key base.
- Progressive candidates with energy and inspiration are key to winning in places like Ohio and Florida.
- Examples from Massachusetts and Florida show the importance of progressive platforms and energetic candidates.
- Democrats need to understand that they are a coalition party with varying ideological groups.
- Giving left-leaning groups something to show up for is vital.
- Countering Republican fear-mongering requires energetic and inspiring individuals.
- Simply being anti-Trump won't be a winning strategy for Democrats in upcoming elections.
- Democrats should pay attention to smaller victories in strategic locations as they can lead to larger wins.
- Building a coalition with inspiring leaders is key to moving forward successfully.

### Quotes

- "Republicans are scared, we got that, we understand, but we can't become paralyzed by fear."
- "If you want the more left groups of that coalition to show up, you have to give them something to show up for."
- "Fear-mongering, scary, be afraid. You know what would be a good counter to that? a bunch of young, inspiring, energetic people."
- "Those can translate into larger winds later."
- "Democrats need to figure out how to energize and mobilize urban voters, their key base."

### Oneliner

Beau gives insights into election strategies: Republicans rely on fear-mongering, while Democrats need inspiring progressives to energize urban voters.

### Audience

Political activists, progressive organizers

### On-the-ground actions from transcript

- Support and campaign for young, inspiring, and energetic progressive candidates in your community (implied).
- Engage with left-leaning groups and provide them with reasons to show up for political events and campaigns (implied).
- Pay attention to smaller victories in strategic locations and understand their potential impact on larger elections (implied).

### Whats missing in summary

The full transcript offers a detailed analysis of recent election strategies and the importance of progressive candidates in energizing voters.


## Transcript
Well, howdy there, Internet people, it's Beau again.
So today, we're gonna talk about the election
and we're gonna talk about what we can learn from it.
And we're gonna see if we can figure out
what the strategies are gonna be in 2022 and 2024,
what the Republicans are gonna do,
what Democrats might try,
what's gonna work and what won't.
And we're gonna see if we can figure that out
based on what just happened.
Okay, so we'll start off with
What's the Republican strategy going to be?
They're going to scare people.
That's going to be the strategy.
They're going to run on fear-mongering.
Why?
Because it worked.
It worked.
So that's what they're going to do.
They're going to try to create moral panic and scare people.
What will not defeat this?
A centrist Democrat screaming, I'm not Trump.
It's not going to work.
I'm not Trump only gets you so far.
Okay, so that didn't succeed.
That's basically what occurred in Virginia, if you're wondering.
So the question that Democrats need to answer is, how do you get the voters out in cities,
in numbers?
How do you get them to show up?
How do you get them energized, especially considering that's the base?
Those are the voters that Democrats count on.
How do you get them to show up?
And I know, progressives right now are just super excited knowing that I'm going to talk
about Boston.
Right.
Okay, so you have Michelle Wu.
The thing is, while that's a cool election, it's an important victory, I get it.
On a national scale, it's Massachusetts.
A progressive winning in Massachusetts
isn't really stop the presses, not in and of itself.
There are other characteristics about the new mayor,
the incoming mayor, that make that story interesting.
But it's not simply, well, she's progressive.
Massachusetts isn't gonna be a good barometer.
We need to know what it's gonna take to win
places like Ohio and Florida. We need to know what's going to get out voters in
Cleveland, Cincinnati, Dayton, St. Pete, places like that. That's what we need to
know. The thing is, it's the same answer. A young, inspiring, energetic, progressive.
That's what it takes because that's what won in all those places. In St. Pete
it's worth noting that literally every candidate endorsed by labor won. But one
I want to point to is Richie Floyd. Now the people in St. Pete probably love
him because from what I understand, pretty involved in getting the minimum
wage increased here, right? But beyond that there's something interesting about
about this particular candidate.
If you were to go to
the Twitter profile,
look in the bio, you will see organizer for DSA,
Democratic Socialists of America,
in the bio.
One in Florida.
I would imagine that every political advisor, worth their salt,
would have said you need to get that out of your bio
if you're going to run in Florida.
But they won.
In what, the 20th district, you had Sheila McCormick.
She won.
Basically ran on a platform of UBI, Medicare for All,
and living wage.
If you want people to show up, you're
going to need somebody energetic and inspiring and progressive.
what it's going to take. Democrats, especially those in the establishment, in the decision-making
process, need to remember it is a coalition party. You have center-right, centrist, center-left,
social democrats, democratic socialists, and then like you have some like hardcore lefties
who vote democratic party begrudgingly, like out of harm reduction. They don't actually even
like the Democratic Party. If you want the more left groups of that coalition to
show up, you have to give them something to show up for. If you look at the
current state of things, you have a centrist president with legislation
that's being held up by two Democratic centrists, the idea that a centrist is going to be the
solution isn't going to fly with a lot of people.
Now besides that, you have the aspect of the Republican strategy.
Fear-mongering, scary, be afraid.
You know what would be a good counter to that?
a bunch of young, inspiring, energetic people.
People who can get them, who can get the independents, the undecided voters to understand that, yeah,
the world is scary, but we can make it through it.
Republicans are scared, we got that, we understand, but we can't become paralyzed by fear.
We can't just stop.
We have to keep moving forward.
People who can inspire.
That's what they need.
If they want to win in 2022 and 2024, that's the formula.
They run a bunch of centrist Democrats whose whole platform is we're not as bad as Trump
will be or Trump endorsed candidates, they will lose.
People inside the Democratic Party need to take note of that.
You need to take note of these smaller winds that are in locations that matter.
Because those can translate into larger winds later.
Got nothing to lose.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}