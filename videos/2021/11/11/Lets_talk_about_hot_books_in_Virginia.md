---
title: Let's talk about hot books in Virginia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hkpUgHoDjog) |
| Published | 2021/11/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the removal of school books in Virginia, specifically in Spotsylvania, where officials proposed burning books.
- School board members in Spotsylvania suggested throwing books into a fire to eradicate "bad stuff" and wanted to display the books before burning them.
- Beau questions the logistics behind burning books in the community and how they plan to demonstrate this process.
- Noting that representatives from Cortland and Livingston are advocating for literal book burning.
- Expresses concern for individuals swayed by fear-mongering tactics, pointing out the danger of following a similar path to oppressive regimes.
- Observes a rapid progression in the adoption of characteristics seen in historical authoritarian regimes within the United States political landscape.
- Emphasizes the urgency of recognizing the alarming direction the country is moving towards, with government officials discussing burning controversial books.
- Urges for national attention on the situation in Spotsylvania, hoping it serves as a wake-up call for people to take action and strive for a better future.

### Quotes

- "We have moved on to the literal book burning portion of the show."
- "The 14 characteristics that exemplify the worst regimes in history are being parroted in the United States today."
- "This is, I'm hoping that this becomes a national story."
- "If nothing up until now has served as the wake-up call that you need, this should be it."
- "The phone's ringing."

### Oneliner

In Spotsylvania, Virginia, officials suggesting book burning prompts urgent call to action to prevent a dangerous slide into authoritarian practices nationwide.

### Audience

Community members, activists, educators

### On-the-ground actions from transcript

- Raise awareness about the situation in Spotsylvania, Virginia by sharing information on social media and engaging in community dialogues (suggested)
- Reach out to local news outlets and media platforms to encourage coverage of the book burning proposal in Spotsylvania (implied)

### Whats missing in summary

The emotional impact and urgency conveyed in Beau's message can be better understood by watching the full video.

### Tags

#BookBurning #Education #Authoritarianism #CommunityAction #UrgentAppeal


## Transcript
Well, howdy there internet people, it's Beau again.
So, today we're going to talk about school books again.
School books being removed.
Only this time not in Texas, with a list of 850.
This time we're going to talk about Virginia.
Spotsylvania, Virginia.
You know, when I made that video about Texas,
I threw in that rage reference, you know, the rage against the machine line.
They don't have to burn the books, they just remove them.
And then not too long after that, I got that question about how I was able to predict
what the Republican Party was going to do next.
And I said that I stopped asking myself years ago what a Republican would do,
and started asking myself what a different political group would do.
What members of a different type of ideology would do.
And that's why I've been so accurate.
In Spotsylvania, Virginia, school board members
have quite literally proposed burning books.
One said, I think we should throw those books in a fire.
Somebody responding said that they want to see the books before we burn them,
so we can identify within our community that we are eradicating this bad stuff.
How would you do that?
How would you show your community?
Put them in a big pile before you torch them?
I'm interested to know what it would look like, what the logistics behind this are.
The representatives there on the school board,
they're representing Cortland and Livingston.
We have moved on to the literal book burning portion of the show.
You can't ignore this.
If you were one of those people who sat there and you got swayed
because you had people talking about some critical theory,
and it scared you, and it swayed your vote, this is where you're headed.
This is what you're looking at now.
For years, years, there have been people pointing out the similarities,
pointing out the route that the Republican Party has been taking,
pointing out how they match up with those 14 characteristics so perfectly.
And I think for a lot of people, they took it as rhetoric.
I'm sure that there were people who watched that video
where I was asked how I predict them.
And they got to that last line and they're like, wow, that was good.
That was good rhetoric.
It's not.
That's literally what I do.
And I'll be honest, I didn't see this coming yet.
It's moving quickly.
The 14 characteristics that exemplify the worst regimes in history
are being parroted in the United States today.
It's not even a slow crawl.
They're just knocking them down one right after another.
And this is where we're at.
You can't think it can't happen here anymore.
We're not at the beginning.
It's not starting to look that way.
It looks that way.
And now we have government officials talking about burning books
because they're too controversial for their students.
I guess they're not pure enough.
This is, I'm hoping that this becomes a national story.
I'm recording this in the afternoon.
I'm going to wait till tomorrow morning to release this video.
I would hope that Spotsylvania, Virginia is all over the news between now and then.
If nothing up until now has served as the wake up call that you need,
this should be it.
This should be it.
Sure, you can write it off and say, oh, it was just rhetoric.
But I don't think so.
We need to see them so we can show our community that we are eradicating this evil,
this bad stuff.
We're getting rid of it.
That should be terrifying.
Somebody in charge of education.
Somebody who should understand the implications of what they are saying.
If you needed a call to get active, if you needed a call to start making the case for that better future,
to start looking forward instead of looking past, looking to the past to the 1930s or 40s,
this is it. The phone's ringing.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}