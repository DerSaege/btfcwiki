---
title: Let's talk about the other 14 characteristics...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yEDzkZ1xwmI) |
| Published | 2021/11/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains 14 characteristics of a certain ideology, comparing Lawrence Britt's and Umberto Eco's work.
- Cult of tradition is about appealing to the past for greatness, rejecting modernism.
- Action for action's sake involves impulsive decisions for appearance over effectiveness.
- Disagreement is considered treason, fear of difference is used to unite against scapegoats.
- Obsession with plots and viewing enemies as both too strong and too weak.
- Pacifism is seen as the enemy, life must be a constant struggle.
- Contempt for the weak and hero culture where everyone is deemed heroic.
- Machismo and weaponry, selective populism, and favoring the uneducated are key characteristics.

### Quotes
- "Contempt for the weak. People that constantly kick down."
- "Everybody's a hero. There's a culture surrounding heroism, even if it's not really heroic."
- "Pacifism is the enemy, and the reason this is the case is because life has to be a struggle."

### Oneliner
Beau explains 14 characteristics of a certain ideology, pointing out the dangerous patterns present in modern politics.

### Audience
Political analysts, activists

### On-the-ground actions from transcript
- Educate others on recognizing and understanding the 14 characteristics of a concerning ideology (implied).

### Whats missing in summary
The detailed breakdown and examples provided by Beau in the full transcript give a deeper understanding of the characteristics of a concerning ideology.

### Tags
#Ideology #UmbertoEco #Politics #WarningSigns #ContemporaryIssues


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about the other 14
characteristics. You know over the years on this channel I have talked about 14
characteristics of a certain ideology. I've used Lawrence Britt's work when I do this.
Those 14 characteristics are tangible. Stuff you can see around you, the effects of
this ideology. Now recently I mentioned a different set, a different way of
looking at it through the philosophical lens, which is Umberto Eco. There's also
14 main points there and we're going to go over those today. And you can decide
whether or not they match up with anything.
Ok so the first one is that it has a cult of tradition. The rhetoric, the
ideas, the philosophy, it's all about an appeal to the past. Back in the good old
days, back when things were better, you know, the way they used to be. And if you
thought about it enough and you return to it, one day you too could make America
great again. That one's really easy to see. Rejection of modernism basically
means that enlightenment is bad. New ideas, new innovations, new theories are
bad. You don't want a populace that is going to be ruled over to be too smart.
They just need to follow orders and do what they're told. So you don't really
want new ideas. You want to cling to that tradition. Three is action for action's
sake. Now this is where the leaders behave impulsively. They make decisions
that sometimes don't really make any sense, but they sound good.
Oh, we're having this problem here, build a wall. Action for action's sake.
Everybody knew it wasn't going to be effective. And even when they're talking
about their political opposition, they demand that action for action's sake.
You remember when they were calling for the president and vice president to head
down to the border? They wanted to see them down there? Yes, because President
Biden or Vice President Harris is incapable of reading a report. They have
to physically stand on the border and take that action. Action for action's sake.
It's very common for people who have mistaken photo ops for actual leadership
to want this. Action for action's sake. The fourth thing is that disagreement is
treason. You Rhino, you didn't agree with us on this infrastructure bill. You're
not a real Republican traitor. What was it they were screaming about Pence again
on the 6th? The fifth thing is a fear of difference. Now more broadly, there's
always the appeal against the intruder, right? When you're talking about these
ideologies, these regimes. Obviously, the disdain for immigrants falls into this.
But it also applies to anybody else who's different. So people of different
orientations or identities might be scapegoated because they have to have
that scapegoat to unite against. Appeal to social frustration is number six. Now a
lot of times this is just middle-class economic anxiety, right? That is typically
always there, but beyond that there's a fear of political humiliation and the
concern about political pressure, social pressure, from classes of people that
were typically viewed as below them. Those lower-class people, right? They're
going to take your job. If you looked at the birth rates, you know one day you're
going to be the minority. It's there. Number seven is obsession with a plot. I mean,
that should be like super obvious. Think about any theory involving a letter of
the alphabet, or the way they look at the election, or the public health issue, or
anything. Obsession with a plot. Everybody's out to get them, right? The
eighth thing is that the enemy is both too strong and too weak at the same time.
You snowflakes who are simultaneously involved in an international conspiracy
to undermine the very fabric of the country. Can't work a can opener, but
you're doing that, right? Too strong and too weak. Nine is that pacifism is the
enemy, and the reason this is the case is because life has to be a struggle. You
have to stay united against whatever manufactured opposition is presented. So
you always have to be on guard. You have to be outraged every day. You got to get
those two minutes of hate. Number ten is contempt for the weak. People that would
constantly kick down. Those who just can't handle it, you know? That's how it's
presented. Welfare queens, you know, stuff like that. That's been there in the United
States for a very long time because even outside of this ideology, the desire to
keep the classes separate in this country, that's always been there.
There's always been the push to kick down. Number eleven is that everybody's a
hero. There's a culture surrounding heroism, even if it's not really heroic,
and you see this a lot too. Oh, what's that? You got fired because you didn't get
vaccinated? Heroes. Heroes for the cause. Whatever the cause may be, it doesn't
matter, but you loudly proclaim them to be heroes. Then you have number twelve is
machismo and weaponry. It's the United States weaponry is a given. That's been
there forever, but then that macho attitude, that's kind of been there too,
and there's a lot of that that has existed for years. But if you want to
tie it into a new political movement, how many of these people are currently
talking about renewed masculinity and a fear that certain things will make the
military weak and not macho enough, not masculine enough.
Then thirteen is selective populism. Now this occurs when you have a bunch of
people in an information silo. Say they only get their information from one
news channel, or they fall into an information silo on the web where each
website just reinforces the others, right? Then it seems as though that vocal
minority is actually representative of the entire country, and then once that
happens, they can say that anybody who doesn't believe the way they do, well,
they're not real Americans or whatever country this happens to be taking place
in, right? Because disagreement is treason. And then the fourteenth one is that they
love the uneducated. I mean, there's actually a quote, something about somebody
loving the uneducated. The reason for that, again, is that you don't want an
intelligent populace if you're trying to be authoritarian. You want them to be
less educated. You don't want them to have the tools for complex thought. You
want to reduce things down to meme level. Let's go. Just chants, slogans, bumper
sticker mentality. These are the fourteen characteristics as presented by Umberto
Eco. Now I know because it matches up so well, somebody's going to say, well, he just
made that list up to match the great MAGA movement. This was made in 1995.
Now, the interesting part about it is, if you never watched the other videos on
fourteen characteristics using Lawrence Brit's work, it matches up just as well.
Two different sets with fourteen characteristics each, and they match up
this well with one group, with one political movement. And both lists are
examining the same type of ideology. That's probably not a coincidence. Both
lists were made prior to the Make America Great movement existing. Almost
like they used them as a roadmap. With everything going on, I think that this is
one of those things that Americans need to become very familiar with. Because you
still have the idea that it can't happen here, when in reality it is
happening here, right now in front of our eyes. It's a real thing. It can't be
pushed to the side. Certainly there are some politicians, some pundits, who use
that rhetoric, calling the Republicans that. Use it as a tool, just a way to
inflame. I'm sure it's happening, but the reality is, when you look at the works
that have tried to identify the characteristics that all of those regimes
shared, and you compare it with the current state of the Republican Party,
they all match up. And they were all made before this movement was even founded.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}