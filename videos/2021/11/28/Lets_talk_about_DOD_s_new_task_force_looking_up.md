---
title: Let's talk about DOD's new task force looking up....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wxCDxjH2jJ4) |
| Published | 2021/11/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing news from the Department of Defense regarding the creation of a new group, the Airborne Object Identification and Management Synchronization Group.
- The group's focus will be on investigating unexplained objects in the sky within US airspace, not necessarily aliens.
- Emphasizing that the group is more concerned with terrestrial objects possibly originating from countries like Beijing or Moscow.
- The main purpose is to monitor advanced aviation technology and ensure US preparedness against potential threats.
- Speculating that the group is not primarily about extraterrestrial objects but rather about maintaining security against opposition nations.
- Mentioning the prevalence of new aviation technology like unmanned aircraft and hypersonic capabilities.
- Stating that the group aims to prevent unauthorized aircraft from flying over US territory.
- Expressing skepticism about the group's involvement in extraterrestrial investigations as portrayed in popular media.
- Implying that while the group might come across unexpected findings, its central focus remains on earthly technology.
- Concluding with a casual farewell, wishing everyone a good day.

### Quotes

- "I don't think that this is going to have anything to do with looking to the stars."
- "Their main focus is definitely going to be things of a terrestrial origin."

### Oneliner

The Undersecretary of Defense's new group focuses on terrestrial objects, not aliens, monitoring advanced aviation technology for US security against opposition nations.

### Audience

Aviation enthusiasts, defense analysts.

### On-the-ground actions from transcript

- Monitor and stay informed about advancements in aviation technology and security measures (implied).

### Whats missing in summary

Deeper insights into the potential implications of advanced aviation technology on national security.

### Tags

#DepartmentOfDefense #AirborneObjects #AviationTechnology #NationalSecurity #USDefense


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about some news out of DOD,
and some news that will certainly
be being discussed all over the internet
as people try to assign meaning to it.
The Undersecretary of Defense for Intelligence and Security
was told to create a new group in their office.
It will be the Airborne Object Identification and Management
Synchronization Group.
That's a mouthful.
What is this group going to be doing?
They're going to be looking at unexplained and unidentified
things in the sky.
Aliens, right?
While I want to believe, I think we
need to bring everybody back to Earth for a second.
There's something in the press release
that people should take note of before this starts spiraling out
of control and creating a whole bunch of theories.
The search, where they're going to be looking,
it says that they're going to be looking
at objects that are of interest in US airspace, special use
airspace, US airspace.
If they were looking for something
from outside of the planet, it wouldn't
be limited to US airspace.
This group is going to be far more concerned
with objects that may have possibly originated
in Beijing or Moscow than something that may have
originated in the Delta Quadrant.
This is more about our shift to near piers.
That's really what this is.
There's a lot of new aviation technology out there,
from unmanned aircraft to the hypersonic stuff
we've been talking about.
And this is DoD's little task force
to make sure that nobody else's stuff is flying over Kansas.
It's really what this is about.
I don't think that this is going to have anything
to do with looking to the stars.
It's going to be much more grounded than that.
And trying to identify any technologies or capabilities
that opposition nations might have
that the US is unprepared for.
So while it would be fun and it's a great plot for a TV show
or something, probably not what it's actually about.
It is more than likely looking at advanced aircraft
from this planet, rather than looking at stuff
from somewhere else in the galaxy.
So I hate to be the wet blanket on that,
but that's really what the press release is kind of indicating.
So now maybe they stumble across something else along the way,
but their main focus is definitely
going to be things of a terrestrial origin.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}