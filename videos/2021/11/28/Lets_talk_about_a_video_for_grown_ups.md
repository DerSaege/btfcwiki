---
title: Let's talk about a video for grown ups...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Zo4YCG8GKdU) |
| Published | 2021/11/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces a topic for grown-ups only, prompting those with children nearby to stop the video.
- He recounts a premise from a counselor about kids questioning what they did wrong after Christmas.
- The counselor shared that kids compare gifts, leading to feelings of failure or inadequacy.
- Beau suggests a simple solution to this issue: switch the tags on gifts from Santa.
- By switching the tags, expensive gifts are attributed to parents, and Santa brings what the child needs instead.
- Beau acknowledges that societal traditions and systems contribute to unequal outcomes during the holiday season.
- Parents may need to explain Santa's role earlier than expected to address economic disparities.
- Children understand their family's social and economic status more than adults realize.
- Shifting Santa's role to providing necessary gifts can help children comprehend differences in gift-giving.
- Beau acknowledges that this shift in tradition won't happen immediately but suggests it's worth striving for to avoid uncomfortable post-holiday dialogues.

### Quotes

- "Switch the tags."
- "It can really help alleviate that."
- "But I think it might be something worth working towards."
- "It doesn't take a whole lot for us to stop it."
- "Y'all have a good day."

### Oneliner

Beau introduces the idea of switching gift tags from Santa to alleviate post-holiday insecurities among children, addressing economic disparities subtly and gradually.

### Audience

Parents, caregivers, educators

### On-the-ground actions from transcript

- Start a community initiative to shift the tradition of gift-giving post-holidays (suggested)
- Initiate dialogues with parents and educators about the potential impact of economic disparities on children's perceptions (implied)

### Whats missing in summary

The full video provides a detailed exploration of how societal traditions impact children's perceptions and suggests a subtle shift in gift-giving traditions to mitigate post-holiday insecurities.

### Tags

#GiftGiving #HolidayTraditions #Parenting #Children #EconomicDisparities


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about something that is for grown-ups only.
I know some people may have little ears within earshot right now.
If you do, stop this video.
To give people time to do that, I'm going to tell you how this started.
I don't know.
I don't actually know where this premise came from, because my wife and
I were both outside on our phones and I was responding to a text.
She was doing something on her phone and I was aware that she started talking,
but I finished up my text before I gave her my undivided attention.
The topic was so interesting, I never went back and found out where it originated.
So this could have been from one of her friends who's a counselor.
It could have been from a YouTube video.
It could have been from a screenshot or a meme.
I have no idea.
But the gist of it is that a counselor was venting,
saying that they know a week or so after Christmas,
they're going to be talking to kids who are wondering what they did wrong.
Or wondering how they failed.
Little Jimmy's going to walk in and ask how he can do better.
Because little Billy, well he got that really cool expensive gift from Santa.
And little Jimmy didn't.
Given the traditions in this country surrounding this holiday,
and the system we live under that produces incredibly unequal outcomes.
It doesn't really seem like there's anything that we can do about this.
But there kind of is, and it's really simple.
Switch the tags.
Switch the tags, that big expensive gift that Santa brings.
You brought that, you got that.
And we shift Santa to bringing what you need.
It can really help alleviate that.
It can help shift that part of the tradition
without freaking a whole bunch of people out.
The only defense that some parents have against this
is to tell them the origin story of Santa a little bit earlier than they might want to.
The thing is, the reason they might not want to
is because they know that as much as we like to pretend it isn't true,
kids do understand their parents' social and economic status.
Probably way more than we give them credit for.
So if they tell them early, well they get it.
Makes sense to them.
But at the same time, if we just shifted Santa's role here,
made it to where he brought what you needed,
that would accomplish the same thing.
Because they are aware.
And then that would make it a little bit more clear
as to why little Billy got what little Jimmy didn't.
I know this isn't something that's going to happen because of a video.
It's one of those things that this would take a long time and be a gradual shift.
But I think it might be something worth working towards.
I don't think anybody really wants that conversation to be taking place.
And it doesn't take a whole lot for us to stop it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}