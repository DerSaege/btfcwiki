---
title: Let's talk about whether to take the bait at Thanksgiving dinner....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=S26tCOWRHKs) |
| Published | 2021/11/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the dilemma of whether to have difficult Thanksgiving dinner political debates.
- Describing a situation where someone is openly trans at an event with closeted LGBTQ individuals.
- Sharing a story about the impact of political debates on relationships during the Kavanaugh hearings.
- Advising on handling such situations by swiftly shutting down potentially harmful comments.
- Emphasizing the importance of being mindful of the impact of words on relationships.
- Encouraging ending contentious debates quickly rather than trying to change minds.
- Suggesting that if political subjects arise, one should be cautious as assumptions may not always be correct.
- Recommending aiming to prevent lasting damage to relationships during heated debates.
- Urging a quick response to harmful comments as a favor to both the speaker and others present.
- Reminding everyone to be aware that assumptions about others' beliefs may not always be accurate during political talks.


### Quotes

- "If you know this conversation is going to come up, I'd set the goal of not changing minds but ending it as quickly as possible."
- "To everybody else who may be listening to this, when you start talking about politics around that table, just remember that you may not know everything you think you do about every person sitting there."


### Oneliner

Beau addresses navigating uncomfortable Thanksgiving political debates, advising quick shutdowns to prevent relationship damage.


### Audience

Thanksgiving participants


### On-the-ground actions from transcript

- End contentious debates swiftly by addressing harmful comments immediately (suggested)
- Be mindful of assumptions about others' beliefs during political talks (implied)


### Whats missing in summary

Beau's tone and personal anecdotes add depth and relatability to navigating difficult political debates at Thanksgiving.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about Thanksgiving
and whether or not you should have the conversation.
Because I got a message from somebody who's like,
as far as the ruining Thanksgiving thing,
yeah, I've got that down.
I can make arguments.
I got a strong background in rhetoric.
I've got that.
I need to know if I should.
How far do I go?
When do I allow myself to take the bait?
And the situation they're in is that they are openly trans.
So they know the topic's going to come up.
However, there are other people who
are going to be present at this event who
are part of that community, part of the LGBTQ community,
but they're in the closet.
and they don't know if they should respond or let it slide and it's not like they can pull them aside and ask them
because they're not really supposed to know.
They're looking for advice on that and it actually sounds like they're looking for permission to be honest.
I'll tell you a story.
During the Kavanaugh hearings,
during the Kavanaugh hearings,
I got tons
of messages from women who were saying they could never look at their father, their uncle,
their grandfather, their brother, somebody the same way again because they said something
in support of Kavanaugh.
They said something, just a political debate thing to win points, typically something discrediting
the victims, the accusers, and it altered those relationships forever.
There's some things that once you say them, you can't take them back. If you're
saying them about a person who is in the demographic you're talking about, and you
don't know that they're in that demographic, you may go a little bit
further than you would if you knew. Once it happens, those relationships are
forever changed. If you know this conversation is going to come up, I would
set the goal of not changing minds but ending it as quickly as possible.
Stopping them, stopping the uncle, the brother, whoever, from saying whatever
they're going to say that might permanently alter their relationship with
the other people at the table. I would set that as a goal. So in short, if you
know you view a good faith argument as a good time, as soon as the subject comes
up, go for the throat. Shut them up immediately. And realistically, you're
doing them a favor. To everybody else who may be listening to this, when you start
talking about politics around that table, just remember that you may not know
everything you think you do about every person sitting there. Anyway, it's just a
fault, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}