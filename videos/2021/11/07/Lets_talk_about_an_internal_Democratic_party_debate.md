---
title: Let's talk about an internal Democratic party debate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=g8WWNvq3yu8) |
| Published | 2021/11/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Democratic strategists debate whether the party should be "woke" to win elections or if it's costing them votes.
- Being "woke" signifies advocating for basic dignity and acceptance for all Americans.
- The real issue is whether it's politically expedient to leave marginalized groups behind to win elections.
- Beau questions the integrity of those in power who prioritize winning over fighting for marginalized communities.
- In the 2020 election, 66.1% of eligible voters cast their ballots, leaving 33.9% who did not vote due to various reasons.
- Beau urges focusing on engaging the 33.9% of alienated non-voters rather than solely targeting the 66.1% who voted.
- He stresses the importance of fighting for marginalized groups and making a compelling case for their rights.
- Beau criticizes the Democratic Party for potentially alienating socially progressive individuals by not advocating strongly for marginalized communities.
- He suggests that abandoning challenging issues because they are difficult reinforces negative perceptions of the Democratic Party.
- Beau calls for genuine advocacy, especially on issues like clean energy, to inspire and attract voters genuinely.

### Quotes

- "If you want to cast yourself as the left-wing party in the United States, at least pretend on some level to be the slightest bit socially progressive or I'm sorry woke."
- "The idea of just abandoning entire groups of people because it's too hard is why the Democratic Party has the reputation it has."
- "If you want to keep fighting for the 66.1%, go right ahead. But I'm willing to bet it would be easier to get that 33.9."
- "Nobody's making a case for them. Nobody's reaching out to them."
- "Your four human rights."

### Oneliner

Democratic strategists debate whether the party should prioritize winning elections over advocating for marginalized groups, while Beau stresses the importance of fighting for basic dignity and acceptance for all Americans.

### Audience

Political activists and Democratic Party members

### On-the-ground actions from transcript

- Reach out to the 33.9% of alienated non-voters and make a compelling case for their participation (implied).
- Advocate for marginalized groups, including making a case for reparations and trans rights (implied).

### Whats missing in summary

The full transcript provides deeper insights into the debate within the Democratic Party on prioritizing electoral wins over advocacy for marginalized communities. 

### Tags

#DemocraticParty #MarginalizedCommunities #Elections #SocialJustice #Advocacy


## Transcript
Well, howdy there internet people. It's Beau again. So today we are going to talk
about the future of the Democratic Party and a discussion that is happening among
Democratic strategists about whether or not a certain term is something that
Democrats should portray themselves to be in order to win elections or if in
fact it is costing them elections. We're going to go over their discussion a
little bit and then we're going to break out some numbers. This is prompted
because somebody asked how I felt about certain Democratic political strategists
saying that the Democratic Party couldn't afford to be woke because if
they were woke, well, it would just drive voters towards the Republican Party. To be
real clear about what that means when you say woke. What you're really talking
about is the desire of some people, large segments of the American population, to
live with like basic dignity to be accepted as part of the United States.
That's what we're really talking about. This discussion isn't about whether or
not the Democratic Party should be woke. It's a discussion about whether or not
it is more politically expedient to kick those people under the bus. That's the
discussion. You can church it up however you want to, but the discussion is
whether or not it is better for those in power to not worry about those outliers,
those groups on the outside, those people that have been othered. That's the
discussion. I would ask if you're gonna do that, what good are you? If you're
going to do that, if you're gonna make the decision, hey, we can't fight for
these people, if we do we won't win the election, what makes you any different
than the people who want to keep them othered, who are open about it at least?
I mean, they're more honest. You know, the whole thing is, well, the Republicans will
do this if they win an election, they'll do this to these poor people. Right, but
you don't want to make the case for them. You don't want to fight for them. You're just going to
give up on them. I don't really see the difference. See, when political strategists
talk, they pay attention to numbers. In 2020, you had 158 million people cast a vote in the
national election. 158 million. Those are the people that Democratic strategists
are looking at. It amounts to 66.1% of those eligible to vote. There are 240
million eligible voters in the United States. If you take that 66.1%
and divide it in half between the parties, you will soon realize that that
number is lower than the 33.9% who did not vote. Because either they feel their
vote doesn't matter, that it won't count for anything, or they can't tell the
difference between the two parties. If you don't believe those people exist,
just look in the comments of any of these videos. It's a pretty widespread
belief. And when stuff like this happens, I can't really blame them for holding it.
When discussions like this are occurring, I really I can sympathize with their
position. I'm a big fan of choosing fights that are big enough to matter and
small enough to win. Yeah, sometimes you can fight the fights you can win, and
sometimes you have to fight the fights that need fighting. And when you are
talking about the basic dignity of millions of Americans, I would suggest
that's a fight that needs fighting. It's a fight that where you just don't have a
choice. You have to make the case. And if you can't, perhaps somebody better can do
it. Rather than focusing on this 66.1% and fighting and scrapping to try to get
their votes, why not focus on that 33.9%? A third of the country that is so
alienated they don't even vote. Now, sure, some of them just didn't vote because
they live in an incredibly red state and they don't think that their vote
matters or whatever, but I'm willing to bet that's not all of them. I'm willing
to bet there's a whole bunch of them who are like, you know, I can't vote for
either of these people out of good conscience because I know they're gonna
sell me out. And here you are proving them right. You're proving them correct.
You either make the case for marginalized people, you make the case
for reparations, you make the case for trans people, you make the case for any
group that needs it or get out of the way. If you want to cast yourself as the
left-wing party in the United States, at least pretend on some level to be the
slightest bit socially progressive or I'm sorry woke. This isn't a discussion
that the Democratic Party needs to be having. It's not whether or not it should
pursue socially progressive policies. The conversation that needs to be happening
is how best to make the case. You're looking at Virginia and concerned about
what happened there. Well, I mean, you could instead of focusing on, well, they
used this to get to us, so we're just going to drop this whole topic, you could
fight back. I mean, I know that as of late that doesn't seem to be a Democratic
Party tradition, but it is something that could be done. You could go
about, I want to make sure I got this right. You're so concerned about books
and schools having influence over your kids, they get exposed to a book and it's
going to change their whole moral fiber. Man, you must be a bad parent. You need
big daddy government to teach your kids. I'm willing to bet that would work in
the South. I'm fairly certain of it. Because at the heart, that is their
argument. The argument is that you need to have the government raise your child
because you can't impart your values to them. I'm willing to bet that could be
overcome with the slightest bit of effort. The idea of just abandoning
entire groups of people because it's too hard is why the Democratic Party has the
reputation it has. It's why there's a whole bunch of people who are socially
progressive who go out of their way to say that they're not Democrats. If you
want to maintain power, which seems to be the ultimate goal there, you need to
focus on the numbers that you can get to. If you want to keep fighting for the
66.1%, go right ahead. But I'm willing to bet it would be easier to get that
33.9. I'm willing to bet that group that has been written off by the
Democratic Party has also been written off by the Republican Party. Nobody's
making a case for them. Nobody's reaching out to them. All it takes are people who
can inspire. People who can get out there and make the case, yes, we need clean
energy and actually mean it. Not just do a lukewarm bit for the sake of votes
when you don't really mean it because you're taking a lot of money from, I
don't know, fossil fuel industries. The term woke in and of itself is a problem.
That was the label the Republicans slapped on it. Your four human rights. Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}