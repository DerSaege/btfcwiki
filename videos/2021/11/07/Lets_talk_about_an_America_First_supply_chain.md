---
title: Let's talk about an America First supply chain....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iOO_IdKArNI) |
| Published | 2021/11/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of the supply chain, which is the route from production to consumption turning raw materials into finished products and delivered to their final destination.
- Addresses the stress on the supply chain due to a worldwide public health issue that resulted in disruptions and shortages in some places.
- Critiques the proposed solution of an America-first supply chain, arguing that it wouldn't solve the underlying issues caused by global disruptions.
- Points out that a centralized supply chain within the United States could make it more vulnerable to various disruptions like blizzards, floods, or hurricanes.
- Challenges the notion that localizing production solely in the United States is a feasible or effective solution, as it could weaken the system instead of strengthening it.
- Compares the vulnerability of a centralized supply chain to military strategies of cutting off supply routes and explains how centralization can make it easier to disrupt.
- Emphasizes the importance of a global supply chain that has been in place for a long time and how localizing it within the U.S. wouldn't address the current disruptions.
- Concludes by stating that decentralization typically leads to more security and questions the validity of an America-first supply chain as a solution to the current challenges.

### Quotes

- "Our supply chain is not broken. It has not snapped."
- "An America-first supply chain is not going to solve the problem. It's just a talking point by politicians."
- "Localizing it within the United States wouldn't stop that. It's a made-up talking point."
- "It's politicians preying on the ignorance of their base."
- "Things are normally more secure if they are decentralized."

### Oneliner

Beau explains the flaws in an America-first supply chain proposal amid global disruptions, stressing the importance of decentralization for security.

### Audience

Policy makers, Supply chain managers

### On-the-ground actions from transcript

- Challenge proposed policies promoting centralized supply chains (implied)

### Whats missing in summary

Beau's engaging delivery and detailed analysis on the implications of supply chain localization versus globalization.

### Tags

#SupplyChain #Globalization #AmericaFirst #Decentralization #PolicyMaking


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about the supply chain.
We're going to talk about the supply chain, what it is, why it's stressed, whether or not it has snapped.
We're going to go over the alternatives, what people are proposing as a solution to the issue we're currently facing,
and we're going to talk about whether or not it makes sense.
And we're just going to kind of run through it all.
First, before we get into that, think about this.
If you were to ask people five years ago what the supply chain was,
how many of them do you think would be able to answer?
How many of them do you think would be able to give you even a rough definition
or understand how far it spread all over the world?
Okay, so what is a supply chain?
It is the route that turns raw materials into finished products and then to their final destination.
Easiest way to say it is it is the route from production to consumption.
Why are we talking about it all of a sudden? Because it's disrupted. It got stressed.
It hasn't really snapped. People are saying it that way.
A little bit of hyperbole going on. Our supply chain is not broken.
It has not snapped. People are just saying that because they may not be able to get the,
you know, PlayStation 72 for Christmas.
And there are some instances where there are shortages in places.
But the supply chain is stressed, not snapped.
What stressed it? A worldwide public health issue that took out five million people
and according to the IMF is going to cost about 28 trillion dollars.
That's a pretty big issue. Okay, so that's what it took to stress it.
What's the solution being proposed? An America-first supply chain.
We're gonna make everything here. We're gonna do it all here.
Okay, would that actually alleviate the issue caused by a worldwide public health issue of that scope?
No, it's not going to solve the problem.
You're still going to have those issues. Doesn't change a thing.
But what else might shut it down if it becomes centralized?
If it becomes localized to the United States?
If there isn't redundancy like everywhere?
A blizzard, a flood, a hurricane, earthquake, pretty much anything.
It's not a solution. It actually takes a system that is working pretty well and makes it vulnerable.
It's just a talking point by politicians who are trying to prey on people's ignorance, for lack of a better word.
A supply chain in the United States that exists solely within the United States is concentrated.
That means anytime there is a disruption from any of those things that I mentioned
or a whole giant list of other things, it would have issues.
The only place in the United States where you have a climate that is pretty consistent,
where you wouldn't have these issues is in the southwestern United States where there's no water,
which would make it really hard to have production centered there
because you wouldn't be able to have any more cities. It's stressed as it is.
This talking point is designed for people who do not understand the way things move around
and how easily things are disrupted, which is funny because most of the America First crowd
likes to fashion themselves or portray themselves as some kind of great military mind.
If you are in a conflict, what do you want to cut?
Supply routes, right? And how is it easiest when it's centralized?
When there are certain points that it has to pass, right? Like a bridge.
We've all seen the World War II movies about taking out bridges, right?
It's the same thing. Centralizing it within the United States does not make it safer.
It does not make it more resilient. It makes it weaker.
This is just a way for politicians to kind of play into the idea that America is exceptional.
We can do all of this on our own. We don't need the entire world to provide supplies.
They're wrong. They are wrong.
The reality is that our global supply chain has existed for quite some time
and the reason people are just now learning the term is because
this is kind of the biggest interruption that's ever occurred in it
since it has entered its modern phase.
And localizing it within the United States wouldn't stop that.
It's a made-up talking point.
It's something that, one, they won't be able to do anyway.
And two, if they were successful in doing it, it would be more vulnerable, not less.
It's politicians preying on the ignorance of their base.
That's the drive behind an America-first supply chain.
Things are normally more secure if they are decentralized.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}