---
title: Let's talk about Fred Gray, Alabama, and a new south....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CXGPxa2G6Cw) |
| Published | 2021/11/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces Fred D. Gray, not to be confused with Freddie Gray, and his impact in Montgomery, Alabama.
- Fred D. Gray was a pivotal figure in the civil rights movement, representing well-known clients like Rosa Parks and Martin Luther King Jr.
- Despite Gray's significant accomplishments, the state of Alabama wants $25,000 to rename a road named after him due to a law protecting Confederate monuments.
- People from all over Alabama have offered to pay the fine, reflecting a new South embracing change.
- Beau questions why Alabama chooses to honor Confederate heritage over an American icon like Fred D. Gray.
- He underscores Gray's journey from oppression to success and suggests that honoring him is a true American success story.
- Beau challenges the notion of Confederate heritage, questioning its relevance and positive impact on the South.
- He urges Alabama to move forward and not dwell on a past that does not resonate with the majority of Southerners.
- While many are willing to pay the fine to prevent Montgomery from bearing the cost, the mayor is considering fighting it in court.
- Beau suggests that using a street renamed after Fred Gray to continue his legacy is a more fitting tribute than the street itself.

### Quotes

- "It's an American success story."
- "It is a new South."
- "Using a street renamed after him to accomplish the same thing today is probably more of a fitting memorial than the street itself."

### Oneliner

Beau sheds light on Fred D. Gray's civil rights contributions, questioning Alabama's choice to uphold Confederate heritage over honoring an American icon like Gray, as people rally to pay a fine to rename a street in his honor.

### Audience

History enthusiasts, civil rights advocates

### On-the-ground actions from transcript

- Rally support to pay the fine for renaming the street in honor of Fred D. Gray (suggested)
- Advocate for honoring civil rights icons like Fred D. Gray in public spaces (exemplified)

### Whats missing in summary

The full transcript provides a deeper understanding of Fred D. Gray's legacy in the civil rights movement and challenges the outdated honoring of Confederate heritage.

### Tags

#FredDGray #CivilRights #Alabama #ConfederateHeritage #AmericanIcon


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about Fred D. Gray.
Not Freddie Gray.
Fred D. Middle Initial Gray.
And Montgomery, Alabama.
Now, if you're not familiar with Fred Gray,
if you don't recognize that name,
I can assure you you're familiar with his work.
You know some of the things he accomplished.
He was born in 1930.
So he's 90, 91 years old, something like that now.
Spent time in Montgomery, Alabama
on Jefferson Davis Avenue.
It was there
that he swore
he would destroy everything that was segregated.
And he did everything he could to keep that promise.
Some of his clients who
you might have heard of at some point
include Rosa Parks,
Claudette Colvin,
the...
some people from the Tuskegee experiment,
Martin Luther King.
He got Martin Luther King off
on
tax evasion charges
in front of an all-white jury.
Martin Luther King referred to him as the general counsel
of the movement.
Court cases include
Browder v. Gale
and Alabama v. Dixon and a whole bunch of others that I'm sure I don't know.
He went on in 1985, he was president of the National Bar Association. In 2001,
he became the first black president
of the Alabama State Bar.
Incredibly accomplished man.
Now the city of Montgomery
decided to name a road after him.
And a fitting one would be
Jefferson Davis Avenue.
So now the state of Alabama
wants $25,000
because back when everybody was whining
about Confederate monuments,
they passed a law
saying you couldn't rename them, remove them, so on and so forth,
or you'd face a $25,000 fine.
Now,
upon hearing this,
my first thought was, I'm willing to bet we can find the people to put
together that money and pay that fine.
And then I started looking into the story a little bit more.
People from all over the South,
all over Alabama and all over Montgomery, have already offered to pay the fine.
Because it is a new South.
It is a new South.
And Montgomery,
a place known as the cradle of the Confederacy,
is trying to step into that new South.
But the state of Alabama
is
intent
on clinging to
a personality
from a failed nation
more than a hundred and fifty years ago.
Instead of honoring
an American icon,
somebody literally born into oppression
who made it in every possible way.
Along the way, stuff that I didn't even include in this.
I mean, he was a preacher too.
This is a person who,
true rags to riches story,
pauper to the halls of power.
It's an American success story.
Could honor that.
But instead,
they're going to say something about heritage,
which to this day I don't understand.
It's one of the most confusing things on the planet to me,
is how five horrible years
from more than a hundred and fifty years ago
is the summation of Southern heritage.
I mean,
everybody watching this channel knows it's a dog whistle,
but let's pretend it's not for a second.
Take that out of it for a second. Let's say it's not about hate.
Let's remove
the abject horror
that was slavery from this story.
I still don't understand how this
is Southern heritage.
Five horrible years
with no positive impacts on the South,
more than a hundred and fifty years ago.
Designing women,
the TV show,
had more of a positive impact on the South, and incidentally it lasted longer.
I don't see
how that is the summation of Southern
heritage,
even if you take away
all of the evil that is associated with it.
The state of Alabama
needs to decide whether they want to at least join the 1900s,
while
most people in the South
are ready
to move forward.
A lot of these state legislatures, they don't exactly have their finger on the
pulse
of the people.
Now, while there are plenty of people who are willing to pay this fine
to see
that
Montgomery
isn't left holding the bill,
the mayor
is
considering taking it to court
and fighting it.
I think in the case of Fred Gray,
that may be the most fitting thing of all,
because this is a man
who spent large portions of his life
using the machinery for change, using the law,
enforcing the law
to catch up with thought.
Using a street
renamed after him
to accomplish the same thing today
is probably
more of a fitting memorial
than the street itself.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}