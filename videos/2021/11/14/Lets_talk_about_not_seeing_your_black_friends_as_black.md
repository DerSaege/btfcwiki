---
title: Let's talk about not seeing your black friends as black....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0ZqETKLuYkQ) |
| Published | 2021/11/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of images, observations, and stereotypes in understanding systemic racism.
- Shares a message from someone questioning the existence of systemic racism based on personal experiences.
- Challenges the listener to question where their perceptions of race come from.
- Points out the influence of media, news, and societal narratives in shaping stereotypes.
- Addresses the impact of systemic racism on how black individuals are perceived.
- Defines systemic racism as a byproduct of historical oppression and institutionalization.
- Urges the listener to trust their observations about the good qualities of black individuals.
- Encourages acknowledging and dismantling manufactured stereotypes.
- Compares societal stereotypes to personal misconceptions.
- Emphasizes the discomfort of confronting societal views on race.

### Quotes

- "Your own observations. 100% of the time, this is what it's like."
- "Trust your observations when it comes to whether or not it's going to rain."
- "The stereotype is manufactured. It's a holdover."
- "You know those stereotypes aren't right."
- "Your evidence is in your message."

### Oneliner

Beau challenges a listener to confront manufactured stereotypes by trusting their own observations about systemic racism.

### Audience

Listeners reflecting on personal perceptions.

### On-the-ground actions from transcript

- Trust your observations about the good qualities of individuals (implied).
- Acknowledge and challenge manufactured stereotypes (implied).

### Whats missing in summary

The full transcript provides a deep dive into understanding systemic racism through personal observations and societal influences.


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
images, observations, and stereotypes. We're going to do this because I got a
message asking for evidence of something and the message itself contains the
evidence. It's one of those things where the person's right there. They're right
there ready to make the jump, ready to really begin understanding some of the
stuff that's going on around them. And I think it might be an observation that a
lot of people need to hear. I watched your survival skills video and then your
big channel showed up on my feed and I've been listening ever since when I'm
working or on the tractor. You've made me realize I was definitely farther left
than I thought, but I'm like your friend in that Sharpton video when it comes to
race. I just don't get it. I just don't follow the idea of systemic racism."
I think it's because like you, I live on 450 acres in the middle of nowhere and
all the black people I've ever met or are friends with are just like me. They're
good hard-working people. I've always said that I don't even consider them
black. I've picked up that I'm not really supposed to say it that way, but I've
never viewed them as different. I'm just wondering if you can give me an example
of some national systemic racism that I can actually see for myself where I am.
This message, this message has it in it.
You say, all the black people I've ever met or are friends with are just like me.
They're good hard-working people. Your own observations. 100% of the time, this
is what it's like. But those people, you don't view them as black. So
where are you getting your standard of what black means? Where is it coming from?
Because it's not coming from your own observations, from what you've seen, what
you've heard, what you've experienced. It's coming from somewhere else. Where?
All around you, right? That stereotype, that image. Movies, books, TV, the news,
internet, discussions, sayings, whatever. All around you. It permeates everything
to create that image that is 100% in direct contradiction to what you have
personally experienced. Is that image good? Is it a good image? It's not, is it?
It's not. See, it's one of those things. You're right. Black people, generally
speaking, do not like being told that they're not even seen as black. That
kind of comes off wrong. And they take it to mean, many times, that the person views
that they've given up on their blackness. But that's not really what
most white people mean when they say it. What most white people mean is actually
even worse than that. It's not good, right? It's not a good image. All of the black
people I've ever met or are friends with are just like me. They're good, hard-working
people. I don't even consider them black. Because the image, the stereotype, right?
That they're not good, hard-working people. That stereotype is nationwide. It is
systemic. It's the byproduct of living in a country that, through law, through
tradition, through force, kept them down. Did everything it could to keep them
down. And if you're going to subject a group of people to that, well, you have to
cast them as lesser. Otherwise, people wouldn't tolerate it. So that image
exists. It's a holdover. It's the after-effects of a very long history in
this country of institutionalizing it. That's systemic racism. The fact that
your image of black people is completely different than what you've actually
experienced is because all around you, you're told, society paints it as if they
aren't good, hard-working people just like you. You wanted evidence. It's in
your message. Now, I got a challenge for you. You say you live on 450 acres. First, I
would love to live on 450 acres. We aren't quite the same, I don't think. But if you live on 450
acres, you run a tractor, I'm willing to bet you are a person who trusts your own
observations when it comes to whether or not it's going to rain, when it comes to
what needs to go into the soil. Trust them here. Trust them here. Your
observations say that black people are good, hard-working people just like you.
Every one of them you've ever met. Trust that and acknowledge that the
stereotype is manufactured. It's a holdover. It exists because in order to
enact the institutionally racist policies of yesteryear, it had to exist.
They had to make white people feel comfortable with keeping black people
down. And this was the narrative that was manufactured. The reason you sit
there and you say that the good, hard-working people that you know, that
you don't even view them as black, is because you don't want to attribute a
negative attribute to them. Because you're still in that mindset of viewing
it through the stereotype. You know those stereotypes aren't right. I'm willing to
bet you really didn't marry your cousin and you can read, right? I mean, you wrote
me a message, but that's the stereotype about rural white folk. You know they're
wrong. The difference is this is something that is pervasive. It's all over society.
And if you think it's uncomfortable acknowledging this and acknowledging
that you've thought this, imagine how uncomfortable it is acknowledging that
this is how society viewed it and it's directed at you. Your evidence is in your
message. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}