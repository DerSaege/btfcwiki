---
title: Let's talk about making the case and ruining Thanksgiving....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DyIWXgJlJrs) |
| Published | 2021/11/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A woman in her 30s reached out for help ruining Thanksgiving dinner after her uncle condescendingly shut down her previous attempt to bring up reparations at the table.
- Beau advises focusing on making a moral argument when trying to win a moral debate, suggesting using indirect approaches to lead the person to acknowledge certain truths.
- He recommends using the example of Agent Orange exposure in Vietnam to help Uncle Bob understand the concept of reparations without directly discussing slavery or segregation.
- By getting Uncle Bob to recognize the government's responsibility in compensating those impacted by Agent Orange exposure, Beau guides the argument towards discussing how government policies like segregation limited generational wealth.
- Beau suggests reframing the argument from a moral standpoint to a property rights case to explain why reparations should be considered for past injustices.
- The goal is to guide the debate towards acknowledging the impact of historical injustices on generational wealth without directly confronting moral beliefs.

### Quotes

- "You're trying to win a moral debate."
- "Make a different case. Better yet, get them to make it."
- "Maybe it's time we start talking about it at Thanksgiving dinner."
- "Y'all have a good day."

### Oneliner

A guide to reframing the reparations debate during Thanksgiving dinner to focus on property rights and generational wealth impact.

### Audience

Americans engaging in Thanksgiving debates.

### On-the-ground actions from transcript

- Start open, rational dialogues about sensitive topics like money, politics, and religion at family gatherings (suggested).
- Encourage discussing challenging subjects to prevent harm and improve understanding (suggested).

### Whats missing in summary

The detailed step-by-step process Beau outlines for reframing the reparations debate without directly confronting moral beliefs. 

### Tags

#Thanksgiving #Reparations #FamilyDinner #GenerationalWealth #PropertyRights


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about how
to ruin Thanksgiving dinner.
I got a message from somebody, a woman in her 30s,
who the message started with, help
me ruin Thanksgiving dinner.
You keep saying to make the case, make the case.
I want to make the case.
Help me out.
Last year at Thanksgiving, she tried
to make the case for something.
And her uncle, who was very condescending,
called her princess and called her a stupid girl.
So she wants to make the case for reparations.
She wants a rematch.
OK, I will help you ruin Thanksgiving dinner.
OK, so you want to make a moral argument, right?
Because you're trying to win a moral debate.
You're trying to win a moral discussion.
So instinctively, what you want to say is, hey, you know,
heck, people kind of built the South twice,
once before the Civil War and once after it was burned.
This is a debt.
It's owed.
We need to pay it.
That's the argument you want to make.
The problem is getting people to admit
that their moral compass is broken is hard.
That's difficult. But you're not limited to that argument.
You can use all tools at your disposal.
I have found it incredibly useful to get
the person arguing in favor of the points
you want to make before you ever start actually talking
about what you want to.
So in this case, I found out a little bit about Uncle Bob.
Bob's not his name.
And I've kind of tailored this argument to Uncle Bob.
But this would work with most Americans
who are opposed to reparations.
So to start off with, you want them
to acknowledge certain things are true and right and good.
And it's how we should behave.
Now, Uncle Bob was in the 101st, screaming eagles, right?
He's in the Army.
OK.
So to start off with, I don't want you talking about slavery.
I don't want you talking about segregation.
I don't want you talking about black people.
I don't want you talking about the color black.
I want you talking about the color orange, Agent Orange.
Agent Orange was a defoliant used from 1961 to 1971
in Vietnam.
US troops who were exposed to it, they had a lot of issues.
They had a lot of issues.
Now, eventually, the diseases were linked to exposure
to this defoliant, right?
Initially, it was just one disease.
And if you had that disease and you were exposed,
DOD offered compensation.
Now, over time, DOD expanded the list.
You know, there were more diseases that got included.
But it's not like they reached out to the troops
and were like, hey, we owe you money.
So a whole bunch of troops never got to claim that money.
This weird thing happened.
In some cases, that compensation is
available to their survivors.
You know that this is the right thing to do.
Pretend that it's not.
Tell Uncle Bob that's wrong.
And watch Uncle Bob convince you that it's right.
That no, no, no.
The government did that.
And therefore, they have to pay.
And just because that soldier isn't there doesn't mean
that their survivors shouldn't get that money.
Let him convince you.
You're super smart, Uncle Bob.
You're absolutely right.
You know, I understand.
Because that soldier, because his earning potential
was limited because of the exposure,
well, then their kids had less.
They had less to build their life with.
It reduced their generational income, work that word in.
And when you're talking about this,
make sure you work in the time frame, 1961 to 1971.
That becomes important later.
Locks them down.
Doesn't make them make a silly argument.
So you say, yeah, absolutely.
You have convinced me.
You're right.
Since the government did that, limited their earning
potential, it definitely should go
to their survivors, their kids, their grandkids,
whoever. Makes sense.
Right?
OK.
And you've got that term generational wealth out there.
Now, still don't talk about slavery.
Go to segregation first.
Right?
And what you actually want to do is not really
just start talking about it.
Just be like, did you know that it was 100 years
before the US government really started
trying to remedy civil rights issues in the US?
100 years after slavery.
Didn't get anything until 1964, 1968.
Same time frame.
Right?
And at this point is when you can start
talking a little bit more openly about how
the policies of segregation wound up
limiting the generational wealth of the kids.
And you can start from there.
At this point, Uncle Bob's already
agreed that the 1960s, not too far back.
And that if the government does something
that limits earning potential, the kids
are entitled to the benefits.
He already made your argument for you.
Just repeat it back to him.
Is it going to work?
Probably not.
It probably won't work on Uncle Bob,
but it may work on some of the other people at the table.
Uncle Bob's going to be really mad that his stupid girl
princess played him.
But you will definitely get the result
you're looking for at Thanksgiving dinner.
That's the way to frame the argument.
When you're talking about something that is moral,
it may not always be the best route to make the moral case.
Make a different case.
Better yet, get them to make it.
And then just say, you already said this was true.
The government policy of segregation
limited their earning potential.
Therefore, their kids were impacted.
Therefore, they're entitled to compensation,
just like the other case.
And then once you've made that, you
can move it back to slavery, because it's
the same thing in the sense of this injury,
since you're not going to be able to make the moral argument
with them.
You're not going to be able to make the moral case, make
the property rights case.
This was an injury that was sustained because
of the behavior of the US government.
Therefore, they're entitled to compensation.
And just go with it that way.
If anybody else needs help ruining Thanksgiving dinner,
please don't hesitate to send me a message.
You know, for a long time in this country,
we've been trained.
Don't talk about money, politics, and religion, right?
What are people getting killed over?
What are people getting hurt over today?
Maybe it's time we start talking about it
at Thanksgiving dinner.
Maybe it's time we start talking about it rationally
as much as possible so less people get hurt.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}