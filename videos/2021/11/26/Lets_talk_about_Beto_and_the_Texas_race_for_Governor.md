---
title: Let's talk about Beto and the Texas race for Governor...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=M0V2Z7sgoQo) |
| Published | 2021/11/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the governor's race in Texas and providing advice for O'Rourke.
- Advising O'Rourke to focus on Texas-specific issues rather than national politics.
- Acknowledging O'Rourke's strengths as a speaker and in addressing Abbott's failures like the grid.
- Suggesting O'Rourke to reconsider his stance on AR-15s and conduct a visible demonstration with firearms.
- Pointing out the significance of the AR-15 in Texas politics and the impact of gun rights on voters.
- Warning that O'Rourke's stance on taking AR-15s could hinder his chances of winning in Texas.
- Emphasizing the uphill battle of advocating gun control in Texas as a whole state.
- Mentioning that voters might gravitate towards Abbott if O'Rourke doesn't retract his statement on AR-15s.
- Concluding that without a significant change in stance on gun control, O'Rourke may not be able to secure victory in Texas.

### Quotes

- "If he doesn't walk that statement back substantially and in a very visible way, I don't think he has an electric grid's chance in Texas of winning."
- "That is a losing battle in Texas."
- "They will freeze and hug their rifle and vote for Abbott."

### Oneliner

Beau provides advice for O'Rourke in the Texas governor's race, urging a reconsideration of his stance on AR-15s to enhance his chances of winning.

### Audience

Texas voters

### On-the-ground actions from transcript

- Conduct a visible demonstration with firearms to show a change in stance on gun control (suggested)
- Focus on Texas-specific issues rather than national politics (exemplified)

### Whats missing in summary

The full transcript provides detailed insights into the dynamics of Texas politics and the significant impact of gun rights on voter behavior.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the governor's race in Texas and O'Rourke.
Because I got a pretty funny message.
It said, hey I understand the way you actually guess what politicians are going to do is
by figuring out what benefits them the most and just being incredibly cynical.
However I have this fantasy that politicians actually watch your channel to get ideas.
How do you think O'Rourke can win in Texas?
O'Rourke is a great speaker.
He's very energizing and he's made a lot of good moves up front by saying, you know, this
is about Texas.
It's not about Biden.
It's not about Trump.
We're going to talk about things that matter to Texans.
That's a smart move.
And he's done a good job of hammering home Abbott's failures, like the grid.
However, if I had any advice to give to O'Rourke, it would be to go to the range with a camera
crew and a mini-14 ranch rifle and an AR-15.
Shoot them both and then, I get it now, I understand.
You know, they're both semi-automatic rifles, same caliber.
I get it.
And walk back that statement.
This is a man who said, we're going to take your AR-15s.
That is a losing battle in Texas.
To the pro-gun control crowd, the AR-15, that's the monster.
That's the one, right?
To the gun crowd, the AR-15 is the most popular rifle in the United States.
And gun rights creates a lot of single-issue voters.
I believe that Abbott could be overcame, but not by somebody who said that they're going
to take the AR-15s.
If he doesn't walk that statement back substantially and in a very visible way, I don't think he
has an electric grid's chance in Texas of winning.
While gun control may be popular in little pockets in Texas, that's an uphill fight.
That is an uphill fight when you're talking about the whole state.
There are certainly people who want Abbott gone, but if their other option is somebody
who is on the record as saying, we're going to take your AR-15s, they will freeze and
hug their rifle and vote for Abbott.
I think that without a substantial, invisible walking back and change of heart on that issue,
I don't actually think he can win.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}