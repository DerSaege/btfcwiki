---
title: Let's talk about the perception of race in the US from outside....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CObebKvaX1k) |
| Published | 2021/11/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- There is a discrepancy between the societal issues portrayed in the news about the USA and the reality of a more integrated America.
- Racism is receding in the United States despite the negative news coverage, with a shift in attitudes towards race among younger Americans.
- The news tends to focus on systemic nationwide issues, such as cases involving law enforcement and old citizens arrest laws like in Georgia.
- To change society, the focus should be on changing thought rather than just laws, as thought drives legal changes.
- It is vital for white Americans who have shifted their views on race to actively become anti-racist and work towards dismantling racist power structures.
- The current conflict arises from the need to dismantle institutional systems rooted in racism that are still in place despite changing societal views.
- The media often presents issues as having two sides, even when they don't, which can lead to a skewed perception of reality.
- The seeming resurgence of old, broken ideas is actually their death throes, indicating progress towards a more inclusive society.
- Traveling and working internationally can help break down prejudices and lead to a more integrated and modern worldview.
- Beliefs must be converted into action, advocating for change and engaging in civic duties to dismantle institutional structures rooted in racism.

### Quotes

- "Travel kills prejudice."
- "If you aren't racist, you need to advocate and to whatever civic duty you feel is necessary."
- "To change society, you don't change the law, you change thought."
- "Thought has changed enough to where a lot of the laws that aren't overtly racist but still manage to be disproportionate in their impacts are still on the books."
- "We're witnessing in this seeming resurgence is really the death throes."

### Oneliner

There's a discrepancy between media portrayal and reality in the US, with a shift in racial attitudes but a need for action to dismantle racist power structures.

### Audience

Americans, Global Citizens

### On-the-ground actions from transcript

- Advocate for change and get involved in civic duties to dismantle institutional structures rooted in racism (implied)
- Travel and work internationally to break down prejudices and gain a more modern worldview (exemplified)

### Whats missing in summary

The importance of converting beliefs into action to bring about meaningful change in dismantling racist power structures.

### Tags

#US #Racism #Media #Integration #AntiRacism #InstitutionalStructures #Travel #Advocacy


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
whether or not the US is the way it's perceived from the outside if you're
just looking at the news. Because I got this comment on Twitter.
Beau, while not living in a perfect nation myself, whose is, the societal issues in the news about
the USA are disturbing. But when I talk to my American work colleagues, I see a
very different America. One that is more integrated than the news we get here.
From somebody in Australia I think, going by their profile. There's no question in
this but it seems implied. Is that true? Yeah, yeah, absolutely. The United
States is far more integrated than the news lets on. You know, I've said it
before, your average working-class white rural person has more in common with
the average working-class black inner-city person than they are ever
going to have in common with their representative in DC. Overall, people have
become more integrated. I believe that racism is receding in the United States
despite what we see in the news. I think there are multiple factors to the
resurgence of a lot of this stuff. But I think one of the big ones is that a
whole bunch of people who got to where they're at, got to a position of power
because of the institutions that are racist. They're afraid of those
institutions going away because they're scared of competing on an even
playing field. So they're using that power they obtained to try to keep those
institutions in place. And the news, in an attempt to both sides something that
really doesn't have two sides, finds somebody to lend their voice to these
ideas. And when they do it, they have to find people who are pretty extreme
because most well-adjusted people have given up on these ideas. If you look at
younger Americans, the attitude towards race, it is shifting. It's shifting. And
this all makes sense. The other thing that plays into the perception from
outside of the United States is that coverage is often about issues that are
nationwide. Issues that are relatable to a whole lot of people. They are systemic,
right? The cases involving law enforcement, those are obvious. That case
in Georgia, see the thing is a big part of that had to do with an old citizens
arrest law. A systemic issue, something that's been around a long time that
probably needs to go by the wayside. These system errors, we might want to
call them, they still exist. Most Americans don't like them, right? Because
we've talked about it before. To change society, you don't change the law, you change
thought. And when thought changes, the law follows. We're at that point now where
thought has changed. Most Americans don't have the view of race that they used to.
We have a very loud minority of people who still maintain those old, antiquated,
broken ideas, sure. But the majority of Americans don't. So there is more of an
outcry when systemic issues lead to high-profile incidents that are often
tragic. The thing is, a lot of those people who have adjusted their views, who
are quote not racist, right? They think that's where it ends for them. All they
have to do is be not racist. That's not true. Because those institutional systems,
those institutions that were built on racism, they still exist. That's why it's
incredibly important for white Americans whose views on race have shifted, who
aren't racist, to become anti-racist. To want to dismantle those power structures
that those powerful people are trying so hard to keep in place. That's where the
conflict is coming from. Because that shift has to occur. Thought has changed in
this case, right? It's already shifted enough to where a lot of the laws that
aren't overtly racist but still manage to be disproportionate in their impacts,
they're still on the books. They're still around. We have to get rid of those. And
it takes people to acknowledge that they exist and then work to get rid of them.
What you see from the outside are those systemic issues coming to the surface
and the media both sides in something that doesn't have two sides, not really.
And I think what we're witnessing in this seeming resurgence is really the
death throes. It's the end. It's these old, broken, antiquated ideas drowning and
lashing out at everything around them. So the appearance from the outside when
you're looking at the news versus what you see when you're talking to people,
yeah, it's probably pretty different. Especially when you're talking about
Americans who do work that is international. Travel kills prejudice. So
people who are working in fields that cross borders, they're going to be far
more integrated than Americans who don't. And they're going to have a more modern
view of the world. So it's important to understand that we are winning the
shift towards changing the way people think. But it doesn't matter what you
believe. If those beliefs aren't converted into action, they don't mean
anything. If you aren't racist, you need to advocate and to whatever civic duty
you feel is necessary, whether it be vote or get involved or whatever, to remove
those institutional structures that still keep people down, that are rooted
in racism. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}