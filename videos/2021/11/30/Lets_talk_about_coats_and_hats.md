---
title: Let's talk about coats and hats....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fWvAI6XEkkE) |
| Published | 2021/11/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Observes people on Instagram and YouTube all wearing coats and hats, finds it odd.
- Did meme research and discovered only 1601 people on average die from hypothermia yearly.
- Refuses to wear a coat because of the low risk of hypothermia, sees it as an infringement on freedom.
- Claims coats don't work and are a myth created to sell products, suggests rearranging "coat" spells "taco" which keeps you warm.
- Believes people promoting coats and hats are shills for big jacket companies.
- Responds to questions about discussing the new variant, vaccines, masks, and hygiene.
- Advocates for basic hygiene practices like washing hands, not touching face, staying home, wearing masks, and getting vaccinated.
- Encourages getting a booster shot and bundling up.
- Concludes by wishing everyone a good day.

### Quotes

- "Coats don't even work. It's a myth."
- "All these people wearing coats and hats on Instagram and YouTube, shills for big jacket."
- "This isn't actually a complicated subject when it comes to masks working."
- "Wash your hands. Don't touch your face. Stay at home as much as you can."
- "Get vaccinated. Get a booster and don't forget to bundle up."

### Oneliner

Beau questions the necessity of coats, debunks myths, and advocates for basic hygiene practices and vaccination to stay safe.

### Audience

Social media users

### On-the-ground actions from transcript

- Wash your hands (implied)
- Don't touch your face (implied)
- Stay at home as much as you can (implied)
- Wear a mask if you have to go out (implied)
- Get vaccinated (implied)
- Get a booster shot (implied)
- Bundle up (implied)

### Whats missing in summary

The full transcript includes Beau's humorous take on the correlation between wearing coats and hats and conspiracy theories, along with debunking myths around hypothermia and advocating for basic health practices during a global pandemic.

### Tags

#Coats #Hats #Hygiene #Vaccination #ConspiracyTheories #SocialMedia


## Transcript
Well howdy there internet people. It's Bo again. So today we're going to talk about coats and hats
because I noticed something. I was on Instagram yesterday and I noticed something I think y'all
should be aware of. Everybody who was outside wearing coats and hats. And then since I thought
that was a little odd, I went to YouTube and looked at all the new videos. Anybody who was outside
wearing coats and hats. Like they're trying to tell us something. Doing it all at once like that.
It's odd, right? But they're not fooling me. They're not getting me. I did my own research.
I saw a meme on Facebook. I know that normally doesn't count as research, but this was different.
It had like citations down at the bottom. I didn't read them, but while I was doing my meme research,
what I found out was that only 1,601 people on average die every year from hypothermia.
1,601 people. That's not a lot of people. You got a 99.99923% success rate with that.
That's probably not going to happen. You're not going to infringe on my freedom and make me wear
a coat over that. No. Uh-uh. And here's the weird part. If you look into that 1,601 number,
if you look into that, what you're going to find out, most of those people, they were wearing coats.
Coats don't even work. It's a myth. Something they made up to sell you stuff. And if you tell
people that and you're like, hey, I'm wearing a coat, but I'm still cold, what do they tell you?
Dress in layers. Get a booster for your coat when the coat didn't work to begin with. Again,
trying to sell you something. See, they're not getting me. I'm no sheep. I don't need a wool
coat and feel all warm and snugly. Something else you may not know is if you rearrange
the letters in the word coat, it'll spell taco and tacos will keep you warm. See, it's all a game.
It is all a game. Out to sell you stuff. All these people wearing coats and hats on Instagram
and YouTube, shills for big jacket. All of them. So one of y'all, thank you to whoever it was,
posted my last video on the new variant on a forum full of some very interesting people who
inundated me with messages. Yeah, so there's that. To answer the questions that were posed in a few
of them, rather than just show you how silly you look by saying some of this stuff. No, there isn't
a giant conspiracy for everybody to start talking about the new variant at once. It's kind of big
news and we're in the middle of a global pandemic. That's why it became a topic of conversation.
As far as when I will stop promoting vaccines and masks and general hygiene, because apparently
that's an issue too. I don't know when the lethal virus is kind of under control. Maybe then,
but probably not until then. This isn't actually a complicated subject when it comes to masks
working. You've known since you were a kid and your parents kept telling you to cover your mouth
when you cough or sneeze, that putting something in front of your mouth and nose when you cough
or sneeze reduces spread. You don't need a PhD or a Facebook meme to tell you this. This is
common sense, much like wearing a coat. Anyway, wash your hands. Don't touch your face. Stay at
home as much as you can. Wear a mask if you have to go out. Get vaccinated. Get a booster and don't
forget to bundle up. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}