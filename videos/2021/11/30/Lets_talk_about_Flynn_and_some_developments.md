---
title: Let's talk about Flynn and some developments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MerRqK0zUaU) |
| Published | 2021/11/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent developments in a certain movement in the United States are being discussed, with advice for those with family members involved.
- A small civil war is happening within a certain alphabet-themed theory world.
- Len Wood released an audio claiming to be a chat between himself and former General Flynn, a key figure in this movement.
- In the audio, Flynn dismisses the movement as "total nonsense," which could significantly impact its followers.
- Despite initial reactions of disbelief, the audio might cause doubt among the movement's adherents.
- An open letter on Telegram from a former believer explains why he's stepping away, citing unfulfilled promises and personal hopes tied to the movement.
- Many followers of this theory were manipulated or sought hope due to personal stressors.
- The collapse of their belief system can be extremely stressful for these individuals who had invested heavily in it.
- Suggestions are made to give these individuals space and time to process recent events rather than using logic to convince them.
- It is advised not to push individuals too hard as they navigate their way out of this information silo.

### Quotes

- "And Len Wood released on Telegram audio of a recording that claims to be a conversation between himself and former General Flynn."
- "However, at the same time, something else circulating on Telegram is an open letter from somebody who believed in the movement."
- "Some of them really wanted it to be true because they are authoritarians at heart."
- "If I had a family member who had fallen down this little information silo, I'd give them space right now."
- "Maybe that's true. I don't know. But I would certainly give them time to process the things that are going on around them."

### Oneliner

Recent developments in a certain movement in the US lead to internal strife, prompting advice for those with involved family members to give space and time for processing.

### Audience

Family members of individuals involved in movements.

### On-the-ground actions from transcript

- Give space to family members involved in certain movements (suggested).
- Allow time for processing recent events (suggested).

### Whats missing in summary

The nuanced emotions and personal struggles of individuals deeply entrenched in a collapsing belief system.

### Tags

#US #Movement #BeliefSystem #FamilySupport #Advice


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about some recent developments
in a certain movement in the United States.
And we're going to kind of go through them
and perhaps give some advice to anybody who has a family
member in this movement.
You know, ever since it took hold,
I've avoided really talking about it a whole lot
because I don't want to get it in the air, you know,
even unintentionally.
I have referenced it a few times,
but never really broke it down because any publicity is bad.
However, inside a certain alphabet-themed theory,
inside that world, a small civil war has been going on.
And Len Wood released on Telegram
audio of a recording that claims to be
a conversation between himself and former General Flynn.
If you are not familiar with the mythology of this movement,
Flynn is one of the leading contenders
to be the super secret person behind it all.
In the audio that was released on a Telegram channel dedicated
to this movement, Flynn called it total nonsense.
Somebody who took an oath in support of it
called it total nonsense.
That is probably going to have a pretty substantial effect
on adherence to this movement.
Right now, they're still in the phase of saying,
oh, you know, he had to say that.
But I imagine that that conversation will be replayed
over and over again, and it will seriously undermine
people's beliefs in it.
And that's good, right?
And it is on some level.
However, at the same time, something else circulating
on Telegram is an open letter from somebody
who believed in the movement, who
thought that Trump was going to be reinstalled
as president or whatever.
And it's an open letter that talks
about why he's stepping away.
Talks about how he's tired of all the unfulfilled promises.
And that's easy enough to mock.
However, if you read beyond what is typically
quoted in the media reports, you find out
that he was really hoping it was true because his wife's sick.
And in the pantheon of Q mythology,
once Trump is re-elevated or whatever, well,
then us normal folk will have access
to the medical technologies that our betters have, right?
And he's asking about something, some kind of medical bed,
something out of science fiction that would cure his wife.
A lot of the people who fell down into this information
silo, who believed in this theory,
they had a lot of stressors.
They had a lot of things going on.
A lot of them were manipulated into it.
Now, don't get me wrong.
Some of them really wanted it to be true
because they are authoritarians at heart.
But a lot of them found something to cling on to,
some kind of hope, or they did it out of fear
because they were told to be afraid of something.
They were radicalized over years.
And now that the whole system, the belief system,
is falling apart, it is probably very stressful for them.
You're talking about the people who
for years, they lost their families, their jobs,
were ridiculed constantly.
And now they find out that it was all for nothing.
And one of the people that they put their faith in
said it was total nonsense while he attended their conventions.
If I had a family member who had fallen down
this little information silo, I'd give them space right now.
I would give them space right now.
When you look at the patterns involving people
who engage in mass incidents, you'll
find out they had multiple stressors at once,
and it's often linked to a domestic thing.
I would suggest that that movement is probably
in a very volatile state right now.
While it might be easy to take this audio recording
and be like, look, listen, the guy you think is behind it all
thinks you're a kook.
His words, it's probably not a good idea.
People often say that you can't get somebody out of a position
that they got into emotionally by using logic.
Maybe that's true.
I don't know.
But I would certainly give them time
to process the things that are going on around them.
If you don't know, there was yet another failure
of yet another prediction that didn't come true.
This little group of people, they're probably on edge.
And I don't believe it would take much to push them.
I would give them time to process it,
give them time to process the last few years.
And hopefully, they can find their way out
of this information silo on their own.
I would not try to push it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}