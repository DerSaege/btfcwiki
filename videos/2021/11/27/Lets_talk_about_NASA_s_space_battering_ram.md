---
title: Let's talk about NASA's space battering ram....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TSaXjGAZ1fI) |
| Published | 2021/11/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- NASA is conducting the Double Asteroid Redirection Test (DART) on the 24th in Vandenberg, California.
- DART is the first planetary defense test involving kinetic impactors, acting as a space battering ram.
- The test aims to determine how to deflect an unidentified asteroid that could potentially collide with Earth.
- It is a concept test to establish a starting point for planetary defense strategies.
- The goal is to alter the speed of the asteroid by a small fraction through impact.
- Success in altering the asteroid's speed will be observable from Earth.
- The spacecraft will utilize a Roll Out Solar Array (ROSA) to generate three times more power than regular arrays.
- Despite launching in the current month, DART will reach its destination in September 2022.
- Space exploration and addressing challenges like climate change lead to technological advancements applicable on Earth.
- Beau encourages focusing on challenges beyond war to foster technological innovation.

### Quotes

- "DART is the first planetary defense test using kinetic impactors, acting as a space battering ram."
- "Success in altering the asteroid's speed will be observable from Earth."
- "Space exploration and challenges like climate change lead to technologies useful on Earth."

### Oneliner

NASA's DART test aims to deflect potential asteroid threats, showcasing technological advancements benefiting Earth.

### Audience

Space enthusiasts

### On-the-ground actions from transcript

- Stay informed about NASA's DART mission and planetary defense tests (suggested).
- Support advancements in space exploration and technology development (suggested).

### Whats missing in summary

The transcript provides insights into NASA's DART test and the potential benefits of space exploration technologies, encouraging a focus on challenges beyond war for technological innovation. Watching the full transcript can offer a deeper understanding of planetary defense concepts and technological advancements related to space exploration. 

### Tags

#NASA #DART #SpaceExploration #PlanetaryDefense #TechnologicalInnovation


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
something NASA has going on. It's going up on the 24th out there in Vandenberg, California.
NASA has something leaving on a jet plane. Not really, but we do know that it won't be
back again. It's called DART, the Double Asteroid Redirection Test. It's the first real planetary
defense test. It's trying out a concept using kinetic impactors, which is a really nice
way to say space battering ram. It's to review a concept related to how to deal with an unidentified
asteroid, something we didn't know was coming, that might be on a collision course for Earth.
Now to be clear, this isn't happening right now. This is a test to check out a concept,
so if this ever was to be the situation, they would have a place to start from. So they
wouldn't have to send up, you know, oil rig workers on it. Anyway, the idea is to get
this thing up there and smash it into an asteroid and see if they can alter the speed of the
asteroid by a fraction of a percent. If it works, that will be observable from Earth.
If it does reduce the speed, they'll know the concept is workable. One of the coolest
parts to this, for me, is how it's getting there. It's going to be using something called
a ROSO, which is a Roll Out Solar Array. In and of itself, that doesn't really sound that
special, but this one is. It uses a lot of advanced technology that allows this solar
array to generate three times the amount of power that normal arrays do. I cannot wait
till this technology hits the open market here on Earth. Now, while it is leaving this
month, it won't be arriving at its destination until September of 2022. It's a long trip.
The solar array kind of illustrates a point, something that we've talked about on the channel
before, whether it be climate change or space exploration. When you have a giant goal, when
you have this challenge that you're trying to meet, a lot of stuff gets developed along
the way. And while many people may just take the attitude of, ah, that'll never happen,
that's something out of a movie with Bruce Willis, preparing for that kind of eventuality
generates technologies that we will be able to use here on Earth. And this is true of
a lot of exploration, a lot of research, a lot of scientific endeavors create technologies
that we end up using every day. Historically in the United States, most of these technological
advances have come through, well, World War II and the Cold War. It would be great if
we could focus on meeting other kinds of challenges that aren't related to war to get that same
kind of technological innovation moving along. So to be clear, yet again, there is no actual
threat. So if you catch little bits and pieces of this online, this is to test out a concept
there's not actually an asteroid headed to Earth. Because I'm sure that somewhere down
some information silo, that's what they're going to say. That's not happening. This is
just a test of concept, but a pretty cool one at that. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}