---
title: Let's talk about tweets, Russia, and that plan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WWmk6udymV0) |
| Published | 2021/11/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalls discussing a Russian plan from 2019 to influence and destabilize the United States through social media.
- Mentions the plan aimed to stoke racial tensions and radicalize black Americans.
- Notes that despite the story fading, the operation continued.
- Cites the analysis of 32,000 tweets during a recent trial, with many originating from overseas, particularly Russia.
- Points out Russia's history of exploiting racial tensions in the U.S. as outlined in the released documents.
- Suggests that if successful, the operation could lead to a generational movement among black Americans to break away from the U.S.
- Draws parallels between how the U.S. supported the Kurds and how black Americans are targeted due to legitimate grievances.
- Emphasizes that addressing systemic racism is not just a moral battle but a national security priority.
- Stresses the importance of recognizing and rectifying legitimate grievances to prevent external exploitation.
- Concludes by urging action to confront systemic racism as a means of safeguarding the nation.

### Quotes

- "If you want to say you're a patriot, if you want to keep America safe, we have to address systemic racism."
- "Other countries are actively attempting to support it and have been for years."

### Oneliner

Beau addresses Russian efforts to exploit racial tensions in the U.S., urging action against systemic racism as a critical national security measure.

### Audience

Americans, Activists, Policy Makers

### On-the-ground actions from transcript

- Address systemic racism in communities (suggested)
- Recognize and rectify legitimate grievances (implied)

### Whats missing in summary

The full transcript provides detailed insights into Russian manipulation via social media, illustrating the urgent need to confront systemic racism for national security.

### Tags

#RussianInterference #SystemicRacism #NationalSecurity #SocialMedia #BlackAmericans


## Transcript
Well howdy there internet people. It's Beau again.
So today we're going to talk about tweets and Russia
and a story that we talked about a couple years ago.
I did a video back in 2019 I think
about the release of a plan,
some documents that detail the plan by the Russian government
to let's say influence the United States.
And by that I mean completely destabilize it,
break it up and create another country.
The documents got released.
However, shortly thereafter people started screaming
fake news, Russia, Russia, Russia.
So the story faded. Nobody covered it.
The plan was to stoke racial tensions within the United States
over social media and to create a wedge
and radicalize black Americans.
Black Americans are chosen because of their treatment in the United States.
Now even though the story died, the operation didn't.
The former assistant director of the FBI said that
32,000 tweets were analyzed after a recent high-profile trial.
32,000 tweets that were in hashtags supportive of the defendant.
What they found was that 29,000 had their geolocation turned off.
That in and of itself means nothing.
Mine's turned off. That's just somebody who values their privacy.
However, after tracking them back,
they found out that almost 18,000 of those tweets originated from overseas
with a whole lot of them coming from Russia.
Russia has a long history of exploiting the racial tensions in the United States.
The operation is laid out in that plan from those documents.
It's kind of pie in the sky. It's ambitious now.
But if they were successful in setting up a generational element to it,
where more and more black Americans are like,
we have to break away from this country, it could be successful.
Don't laugh. The United States does it all the time.
It's why the U.S. was so supportive of the Kurds,
an ethnic group within a country that wasn't treated fairly.
And black Americans have the right percentage for this to work.
The U.S. has used this in Iraq. It's used in Syria.
Those are just recent examples.
This process is used by intelligence agencies all over the world
and has been for a very long time.
The addition of social media is kind of new,
but this idea has a lot of precedence.
So the fact that it still certainly appears to be ongoing, what does this tell us?
When we talk about racial tensions in the United States,
we typically frame it in moral terms,
because yes, addressing the United States' long history of systemic racism is a moral thing to do.
It's also something that should be a national security priority.
The reason Russian intelligence has identified black Americans as a target demographic
is because they are poorly treated, because they have legitimate grievances.
That's why. It's why the U.S. targeted the Kurds.
This process only works if the grievances are real.
It is no longer just a moral battle.
If you want to say you're a patriot, if you want to keep America safe,
we have to address systemic racism.
Other countries are actively attempting to support it and have been for years.
Eventually, they'll be successful.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}