---
title: Let's talk about a message to conservative parents....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Zjs95ewhkjY) |
| Published | 2021/11/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Conservative parents are receiving messages from their children who are feeling lost and conflicted about how to interact with them.
- The children express disillusionment with the hypocrisy they see in adults who preach moral values but don't practice them.
- Despite being raised Catholic, the children have distanced themselves from the church due to perceived hypocrisy.
- They strive to live by the values they were taught, including supporting progressive ideals and helping the disenfranchised.
- The children express frustration at being labeled as idealists and socialists by their parents for wanting to create positive change.
- The father mentioned in the message is portrayed as someone who has changed ideologically, becoming more dismissive of progressive ideals.
- The children struggle to communicate with their parents, especially when there are fundamental disagreements on social and political issues.
- Beau encourages conservative parents to understand that their children learned these values from them and urges them not to give up on fostering idealism.
- He acknowledges the importance of younger generations embracing idealism and striving for a better world, despite the challenges they face.
- Beau reminds the children that they have the power to shape a more equitable and compassionate society by upholding the values instilled in them.

### Quotes

- "Your kids, the ones that you're now calling a socialist, the ones you're alienating, where did they get the ideas? They got them from you."
- "The job of every parent is to make their child better than they are. They succeeded in that."
- "Don't let them down. Be better than they are. Fix the world."

### Oneliner

Conservative parents are urged to understand the ideological shifts in their children, who learned their values from them and now embody idealism to create a better world.

### Audience

Parents, Youth

### On-the-ground actions from transcript

- Have open and honest dialogues with your parents about your beliefs and values, encouraging mutual understanding (implied).
- Uphold the values of compassion, progressiveness, and empathy in your daily interactions and support efforts that aim to create positive change (implied).
- Embrace idealism and continue striving for a better world, even in the face of opposition or disagreement (implied).

### Whats missing in summary

The full transcript provides a deep insight into the generational divide in beliefs and values, urging both parents and children to communicate effectively and work towards a more compassionate and just society.

### Tags

#GenerationalDivide #FamilyValues #ProgressiveIdeals #Communication #YouthEmpowerment


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we have a message for conservative parents.
We're going to kind of talk directly to them,
because about five times a week,
I get a message from one of their children.
I say children, I don't mean kids. They're
eighteen to twenty-six.
And they all pretty much carry the same tone.
It's the same message over and over again.
Situations are a little different, but overall
it's pretty much the same.
I'm going to read
one of those messages.
And I'm reading this one because it hits all the points.
I'm at a loss anymore,
and I'm not sure what to do,
what to think, or how to interact with my father anymore.
This man is one of the hardest working guys I know,
and is a great father, and goes out of his way to support my sister and I.
He's always been extremely self-sacrificing
when it comes to us and the family.
Part of it has to do with him making up for a mother who's not exactly great at
being one.
I was raised Catholic.
I went to Catholic grade school,
and then to an all guys Catholic high school.
Growing up, I was taught the Ten Commandments.
I was taught the Golden Rule.
I was taught to feed the hungry, shelter the homeless, help the sick.
As a kid, this all sounded like a no-brainer.
Of course, that's what we should do.
As I grew up, I started to realize that for all the preaching I'd heard,
all the church masses, charity events, etc.,
all of it was lip service.
These same people that sent me to school to teach me how to be a good person and
learn moral values don't exactly employ them as often as I was taught to.
It's this hypocrisy
that turned me off to the church and religion as a whole, and I just quit going.
Despite no longer practicing, I try to live my life in a good way.
I'm far, far from perfect,
but I try to be a decent human being.
I donate when I can to various efforts. I support progressive ideals that sound a
whole lot
like what I was raised up to believe was right.
I want to help the disenfranchised.
I think it's wrong for corporations to treat their employees like slaves.
I want the rich to pay any amount of taxes.
I want to help refugees fleeing terrible lives that my country helped
create.
I want to feed the hungry, shelter the homeless,
tend to the sick.
But for all that, I get told I'm an idealist
and that my generation is going to ruin the country.
I just don't get it.
I'm trying so hard to live by what I was taught,
and yet I'm called a terrible socialist by my own father,
someone I've always respected and cared a great deal for.
This all happened with Trump,
and it's been awful since the day he took office.
Despite being a great father,
he's one of the many
who are seemingly okay turning a blind eye to evil as long as it doesn't affect
him.
It horrifies and depresses me on a level I can't even begin to describe.
I try so hard to avoid politics,
but I can constantly hear him shouting
because he spends all day on TikTok
or some sort of social media feeding him nothing but rage piece after rage piece,
and despite him telling me numerous times that all they want to do is divide us,
he lets them do it.
I just don't understand why.
If he's aware of it, how does this happen?
This morning he asks me if I heard something about this $450,000
the government is paying to the refugees,
and after watching your video,
I did my best to explain it to him.
I'm really not the best, and sometimes I can get a little heated,
but I try to maintain a level head.
After explaining,
he got mad at the refugees,
not the administration.
No, they shouldn't have tried to get in here illegally then.
That is what he says when I say you can't use kids like that.
I honestly don't know how I'm supposed to look at him the same
after he says stuff like that.
Then when I tell him the 1% need to be taxed at all
because they pay so very little,
he tells me we should be grateful for them.
They don't have to pay.
They should be exempt from that
because their families built this country.
Where would we be without them?
We owe them everything.
This country is.
You can't just take their money
as if the left is trying to take all of their money
and leave them in the poorhouse.
No, they should be exempt, and we should be grateful.
When I then explain how companies like Walmart
and others pay their employees so little,
the taxpayer ends up subsidizing them,
he still doesn't seem to get it or care.
When I say nobody working 40 hours a week
should struggle to get by,
I get some weird, well, who determines what that is?
Who determines the livable wage?
And then he goes on some rant about people thinking
that livable wage means they have to have $60,000 cars
and big TVs and new iPhones.
And just all this stuff I know he gets
from right-wing brainwashing.
He tells me all the time the rich get richer,
the poor get poorer,
but then complains when people try to change it.
In recent years, I've seen a side to him
that is ruining the father I knew him to be.
I know you're not a therapist,
nor do you have all the answers,
but I just don't know how to try
and get him to understand any of this
when it seems like he already understands.
He's just upset that others haven't given up.
They wanna shake up the system,
and it's starting to affect him.
He works two jobs, has a little bit of stock investments,
and really wasn't a bad guy,
but recent years have brought out
the worst in him ideologically.
And while I don't like either side,
the right-wing in this country
is incredibly dangerous right now
with the way it convinces people
what it's doing isn't akin to fascism.
I get that we shouldn't all agree on everything,
and it's okay to disagree,
but I'm not sure I know what things are absolutes
and what things are it's okay to disagree on anymore.
These are just some of the recent issues and disagreements,
but I assure you there are many more examples of this.
Whatever this is, he's gotten himself believing.
Five a week, no joke.
Now I'll carry this theme.
If you are a conservative parent
who is at odds politically with your child,
this probably can add some context
to what they're feeling.
And I think that's a good thing.
I think that's a good thing.
But this probably can add some context to what they're feeling
because they're all the same.
They come in every week, and they all sound like this.
This one hit all the points, you know,
but they all have this same theme.
The question that you should be asking yourself
is where did they get these crazy ideas, right?
Your kids, the ones that you're now calling a socialist,
the ones you're alienating, where did they get the ideas?
They got them from you.
Got them from watching you, Dad.
And how did you give it to them?
How did you train them to have these beliefs
as core to who they are?
You told them over and over again.
And eventually it became a part of who they were.
And hopefully you showed them because I'm of the opinion
you can't tell a kid anything.
You have to show them.
So at least on some level, you embodied these ideas.
You gave them these ideas.
And then you gave up on them.
If you are somebody whose parent is like this,
take solace in one thing.
The job of every parent is to make their child better
than they are.
They succeeded in that.
The idea that they gave up is probably true to some degree.
But that doesn't mean you should.
The reality is we need your idealism.
We need you talking about utopia.
We need you shaking things up because there's
a lot of people who have given up on it.
What you're doing, you're trying to get the promises.
Your parents told you this is the way the world works.
This is what it takes to be a good person.
And you got a little bit older and realize
that's not how it works.
But it is how it should work.
And the people who can make it work that way are you.
You can do that.
Your parents won't.
They taught you this by repeating it over and over
again, in this case, sending you to a school
so you can learn these values.
Values that in some cases are so hard to live up to.
They're quite literally divine.
But you want to do it because you want that world.
Don't give that up.
Because the sad fact is the way they
taught you to be like this, to care for those around you,
to try to create that better world, to be an idealist,
the method that they used by telling you over and over again
the same thing, that is exactly how they wound up
calling you a socialist, using this rhetoric that they got off
TV and social media on you.
So what happened?
What happened?
I understand that it's probably very difficult.
It's probably very hard to all at once realize
the world isn't what you were told it is
and that your parents aren't who they portrayed themselves
to be, that they don't live up to the values
that they instilled in you.
But those values, they are what will change the world.
They are what will shift us away from the danger
that all of y'all mentioned.
You have a lot to clean up because generation
after generation gave up.
And now it's all on you.
Over and over again, issue after issue,
we've known that these things were coming.
We just didn't care.
The American people, I know that there
are individuals who say, I've been
in the fight against climate change for 20 years,
and I understand that.
But as a generalization, the American people didn't care.
Now we have a younger crop who does,
who cares about the people around them,
who wants everybody to get a fair shake.
That's a better world.
The idea of every parent is to make sure their kids are
better than they are.
They succeeded in that, that they've
given you all the tools.
Don't let them down.
Be better than they are.
Fix the world.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}