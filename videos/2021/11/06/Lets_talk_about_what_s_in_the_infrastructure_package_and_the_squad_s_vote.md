---
title: Let's talk about what's in the infrastructure package and the squad's vote...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mL3Vv0I8M6Y) |
| Published | 2021/11/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Overview of the infrastructure bill and where the money is going.
- The White House estimates the bill will create 2 million jobs.
- Detailed breakdown of funding allocations, such as $39 billion for public transit and $65 billion for internet infrastructure.
- Noted investments in removing lead pipes, electric charging stations, and increasing electric grid resiliency.
- $110 billion earmarked for roads and bridges, the largest investment in bridges since the national highway system inception.
- The bill is considered a good start but not sufficient for the country's full infrastructure needs.
- The progressive members known as "the squad" voted against the bill to ensure social and climate infrastructure were addressed simultaneously.
- Republicans crossed the aisle to vote for the bill, some out of necessity and others to undermine the squad's leverage.
- Beau sees the squad's decision to vote against the bill as maintaining their promise and sending a message without causing harm.
- There are more components of the infrastructure package that the Biden administration aims to pass.

### Quotes

- "It's good, but not great."
- "Them voting against it is just honoring that promise."
- "Republicans crossed the aisle to make up their votes."
- "They sent a message to Pelosi saying hey if we say we're gonna vote against it we will."
- "It's just a thought."

### Oneliner

Beau breaks down the infrastructure bill's allocations and explains the squad's strategic voting against it to ensure broader infrastructure needs are met.

### Audience

Policy enthusiasts, activists, voters

### On-the-ground actions from transcript

- Contact your representatives to advocate for comprehensive infrastructure bills that address social and climate needs (implied).

### Whats missing in summary

Further details on the additional components of the infrastructure package proposed by the Biden administration.

### Tags

#InfrastructureBill #PublicTransit #ProgressivePolitics #BidenAdministration #CommunityAction


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to,
we're gonna talk about the infrastructure bill,
talk about what's in it.
It's passed, it's on its way to President Biden's desk.
So let's talk about where that money is going
and what it's supposed to do.
After that, we will talk a little bit
about the squads vote.
Okay, so the White House is saying
it's gonna create two million jobs,
2 million jobs on average.
People I have talked to who are not politicians suggest it's going to create 1.5 to 2 million
jobs.
So the White House saying 2 million, that's within the estimates you get from nonpartisan
sources.
It's just the most optimistic one.
But still, 1.5 million isn't bad.
Okay, there is $39 billion, all of these numbers are in billions, $39 billion for public transit
that's designed to expand systems, so expand the routes, and to increase accessibility
for disabled people, and there's money in there to buy low and no emission buses.
There is $7.5 billion to put in electric charging stations for electric vehicles.
There is $5 billion for schools to buy electric buses.
There is $55 billion for water and wastewater infrastructure.
Included in that is $15 billion to get rid of lead pipes because that's still a thing.
$25 billion for airways, air traffic control, tower updates, runways, terminals, gates,
stuff like that.
$65 billion to increase electric grid resiliency.
In that, there is some money for carbon capture technology and for hydrogen.
There's $65 billion for internet infrastructure, which will increase rural internet access.
I am super happy about that.
If you've ever seen one of my live streams, you know why.
But it's not just rural internet, which is how it's often being framed.
It also applies to tribal areas and to low income areas that aren't necessarily rural.
Then there's $110 billion for roads and bridges.
billion of that is specifically earmarked for bridges, a little less than 40 billion.
That is the largest investment in bridges since the national highway system became a
thing.
There is also 66 billion for our rail system, which is the largest investment since Amtrak
has been around.
So overall, it's a big bill.
It is a big bill.
It has lots of stuff.
is it just kind of a loose summary of it? Is it everything that we need? No. Is this
going to bring us up to, you know, the 2020s? No. Is it a good start? Yeah. But it isn't
the overall infrastructure package the United States needs to remain competitive. Okay.
So it's good, but not great.
Now on to the squads vote, because there's been a whole lot of questions about that.
If you don't know, the most progressive people in the House voted against it.
And people are asking why, especially because a bunch of Republicans voted for it.
And I think there's some concern that maybe there was some stuff snuck in at the last
moment.
That's not what it is.
The squad, as they have come to be known, they said they weren't going to vote for this
until the social infrastructure and the climate infrastructure went through.
They wanted it all at once.
They wanted to make sure that everything got done.
Them voting against it is just honoring that promise.
Now, I'm not a big fan of burning political capital for no reason.
I don't think they did that in this case.
Because at the end of this, what you had happen was Republicans crossed the aisle to make
up their votes.
Now they did it in a sense to literally to own the squad.
Republicans is doing something they don't want to do to own the Libs.
Sounds familiar, right?
That's what kind of happened here.
Now some of the Republicans that voted for it understand it's an infrastructure bill
that we absolutely have to have.
There's no real debate over this.
So they went ahead and let it go through.
Some did it solely to vote against AOC.
Some did it for a more cynical reason, understanding that if it passed, it removed the squad's
leverage.
leverage that they that they had trying to use this to make sure that they could
get what they wanted in the social and climate side. I don't think it was a bad
move on their part to vote against it. They sent a message to Pelosi saying
hey if we say we're gonna vote against it we will and they got to send a
message to their constituents saying this is what we promised and this is
we did. We didn't back down. And at the same time, it didn't cause any harm because the bill went
through. So that's, that's where we're at. Now there are still more components to the overall
infrastructure package that the Biden administration wants to get through. And we haven't seen those
yet. So we'll just have to wait and see. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}