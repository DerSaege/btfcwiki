---
title: Let's talk about Biden, gas prices, and inflation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-_l2ltcOQhU) |
| Published | 2021/11/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about inflation in the economy and how it is still a concern despite good news.
- Criticizes the media for choosing to inflame rather than inform about inflation.
- Mentions that global food prices are up, making inflation a global issue.
- Points out that gas prices are impacted by OPEC not increasing production as fast as demand.
- Suggests that transitioning to renewable energy in the U.S. could help reduce inflation spikes.
- Emphasizes the need to stop relying on oil and transition to solar, wind power, or other renewable forms of energy.
- States that switching to renewable energy is not just better for the environment but also for reducing costs.
- Points out that infrastructure programs can accelerate the transition to renewable energy.
- Criticizes those reluctant to switch to renewable energy as engaging in a self-defeating attitude.
- Concludes by stating that transitioning to renewable energy is not just beneficial for the environment but also for personal finances.

### Quotes

- "It should be noted that global food prices are up."
- "If we switch to solar, wind power, any of the other forms, you're plugging your car in to charge it."
- "Not just is it better for the environment, not just will it help reduce the effects of climate change, it's better for your pocket."

### Oneliner

Beau criticizes media for inflaming rather than informing about inflation, advocates for transitioning to renewable energy to reduce inflation spikes and benefit the environment and personal finances.

### Audience

American citizens

### On-the-ground actions from transcript

- Transition to renewable energy sources like solar or wind power (suggested)
- Support infrastructure programs that accelerate the transition to renewable energy (implied)

### Whats missing in summary

Importance of transitioning to renewable energy for economic stability and environmental sustainability.

### Tags

#Inflation #RenewableEnergy #ClimateChange #MediaCriticism #EconomicTransition


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're gonna talk about inflation.
I know there was some good news,
some good reporting that came out about the economy today,
but you still have inflation going on in the background.
And this is another example where the media
in the United States is choosing to inflame
rather than inform.
When it gets discussed, people are pointing three places.
They're pointing to food prices, gas prices,
and the White House, as in what is President Biden
gonna do about this?
It should be noted that global food prices are up.
Inflation is a global issue right now.
Domestic policy can only go so far in addressing some of it.
Now, with gas prices, you have OPEC
that is not upping production.
They haven't pushed up production
as fast as demand has climbed.
Therefore, the price of oil is higher.
And they don't seem super willing to change course.
So it's likely that gas prices will continue to climb.
So what can we do about it?
Well, we could stop using gas, and we could start there.
The switch to renewable energy in the United States
will eliminate this concern, which
will help reduce spikes in inflation later.
Prices won't go up for fuel because we won't be using it.
Even if it goes up elsewhere in the world,
we would be insulated.
This is a change that is coming.
It's not one of those things that can be put off.
Well, it can be put off.
to be delayed by large corporate interests, but it can't be stopped.
How about that?
If we switch to solar, wind power, any of the other forms, you're plugging your car
in to charge it.
We're not reliant on oil.
We're not reliant on something that is subject to supply and demand.
We're using something that's everywhere.
So that should reduce the cost of running your vehicle and at least make it more stable.
This would be true of pretty much all energy forms as the transition continues.
The infrastructure programs that are being pushed would speed this along.
If you're concerned about the price of the pump but you're reluctant to switch over to
renewable energy, you are engaging in a self-defeating attitude.
The solution is in front of you.
Not just is it better for the environment, not just will it help reduce the effects of
climate change, it's better for your pocket.
As is typically the case, it turns out that it's cheaper to be a good person.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}