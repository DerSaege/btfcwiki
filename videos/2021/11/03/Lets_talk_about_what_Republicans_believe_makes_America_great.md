---
title: Let's talk about what Republicans believe makes America great....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ymqWuIMcilM) |
| Published | 2021/11/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the evolution of the Republican Party from its historical principles to its current beliefs.
- Sharing a powerful message about what makes America great: the ability for anyone to come and become American.
- Emphasizing the importance of America's diversity and how it fuels the nation's progress.
- Quoting Ronald Reagan on the significance of continuously welcoming new Americans for America's leadership.
- Criticizing the shift in the Republican Party towards slogans, nationalism, and denial of facts.
- Urging the Republican Party to revisit its core values and ideals.
- Noting the repercussions of closing borders and ceasing to welcome new Americans on America's global leadership.

### Quotes

- "Anyone from any corner of the earth can come to live in America and become an American."
- "If we ever closed the door to new Americans, our leadership in the world
    would soon be lost."
- "You got suckered in by slogans, basic nationalism, a stupid red hat and a worst slogan."
- "Two plus two equals five. Y'all love to use the term Orwellian. That's pretty Orwellian."
- "It wasn't long after a certain recent president shut down our borders, stopped taking new Americans, villainized them, that we lost our position of leadership in the world."

### Oneliner

Exploring the Republican Party's shift from its historical principles, urging a return to core values, and stressing America's strength through diversity.

### Audience

American voters

### On-the-ground actions from transcript

- Conduct soul-searching within the Republican Party to realign with core values (suggested)
- Embrace diversity and welcome new Americans to enrich the nation (implied)

### Whats missing in summary

Deeper analysis on the impact of nationalism and denial of facts on political ideologies. 

### Tags

#RepublicanParty #America #Diversity #RonaldReagan #Nationalism


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about the Republican Party,
where it is today, where it was at one time, what it used
to believe, what it believes today, the principles that
go along with it.
And we're going to see if it's even really recognizable when
you compare it to what it once was.
But before we get into that, I want
talk a little bit about something I believe, something that I have always
found as a good summation of what makes America great. Something that I
think that a lot of people could benefit from hearing right now. So we're gonna go
into that. So I want you to kind of understand that we're gonna go through
that first, and then we'll get to the Republican side of things.
But remember this point, because at the end of it, it's really just an
observation about a country which I love.
It was stated best in a message I received not long ago.
A man wrote and said, hey, you know, you can live in France, but you cannot become
a Frenchman. You can go live in Germany or Turkey or Japan, but you cannot become
German, a Turk, or Japanese. But anyone from any corner of the earth can come to
live in America and become an American. Yes, the torch of Lady Liberty
symbolizes our freedom and represents our heritage, the compact with our
parents, our grandparents, and our ancestors. It is that Lady who gives us
our great and special place in the world, for it's the great life force of each
generation of new Americans that guarantees that America's triumph shall
continue unsurpassed into the next century and beyond. Other countries may
seek to compete with us, but in one vital area, as a beacon of freedom and
opportunity that draws the people of the world, no country on earth comes close.
This, I believe, is one of the most important sources of America's greatness.
We lead the world because unique among nations, we draw our people, our strength, from every
country and every corner of the world.
And by doing so, we continuously renew and enrich our nation.
While other countries cling to the still past, here in America, we breathe life into dreams.
create the future and the world follows us into tomorrow. Thanks to each wave of
new arrivals to this land of opportunity, we're a nation forever young, forever
bursting with energy and new ideas. And always on the cutting edge, always
leading the world to the next frontier. This quality is vital to our future as a
nation. If we ever closed the door to new Americans, our leadership in the world
would soon be lost. That's Ronald Reagan. That is Ronald Reagan. I might have
dropped a word here or there, but that is Ronald Reagan. There's the idea among
Republicans that the country has moved left. No, that's not what happened. Believe
me, I'm kind of left. I would know if that was going on. That's not what's
happening. The country didn't move left, you moved right. You got suckered in by
slogans, basic nationalism, a stupid red hat and a worst slogan. Because of that,
you were willing to look the other way on things you didn't believe in, things
that disagreed with your principles. Because there was a time when what I
I just said. That was uttered by the conservative icon himself. And now, I mean, that's lefty
nonsense. The country didn't move. You did. You moved right. You got suckered in. At this
point, you're denying math. You're still walking around saying, well, you know it's got a 99.9%
survival rate? No, it doesn't. No, it doesn't. There's roughly 330 million
people in the United States. At a 99.9% survival rate, that means once
everybody caught it, we would have lost 330,000. We have lost more than double
that, and not everybody's got it. But they still have you saying it. Two plus two
equals five. Y'all love to use the term Orwellian. That's pretty Orwellian, to be
honest. I think it might be a good idea for the Republican Party to do a little
bit of soul-searching. Look back at what they always wanted to be. The idea is
that they claimed they embodied. I would point out that Reagan was right. It
It wasn't long after a certain recent president shut down our borders, stopped taking new
Americans, villainized them, that we lost our position of leadership in the world.
Anyway, it's just a thought.
So have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}