---
title: The roads to understanding misinformation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ICpy7urM6Lo) |
| Published | 2021/11/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzes tactics used by commentators, pundits, and news outlets to manipulate audiences online.
- Advises viewers to be wary of emotionally manipulative headlines.
- Explains how headlines play into confirmation bias by appealing to different political affiliations.
- Points out techniques like inserting politically divisive names, asking leading questions, and using quotes as headlines to shape narratives.
- Talks about personifying organizations or individuals in headlines to influence perceptions.
- Advises on being critical of polls and understanding how statistics can be misleading.
- Warns about false links leading to misinformation and using images out of context to incite outrage.
- Discloses methods like factual yet misleading statements and intentionally unclear communication.
- Describes assigning intent as a tactic used to manipulate audiences by creating false narratives.
- Encourages viewers to be vigilant, especially about headlines that provoke outrage daily.

### Quotes

"Headlines, in theory, should pique your curiosity. They shouldn't encourage you to form an opinion right away."
"A headline should inform you, not inflame you."
"If every single day this news entity has a new thing for you to be angry about, you need to be on guard."

### Oneliner

Beau explains manipulative tactics used in media to influence audiences and warns against emotionally charged headlines creating false narratives.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Fact-check sources before sharing information (implied)
- Verify statistics and polling methodologies (implied)
- Reverse image search to verify the context of images (implied)
- Be vigilant against emotionally manipulative headlines (implied)

### Whats missing in summary

In-depth examples and analyses of various manipulative tactics used in media and how to combat misinformation. 

### Tags

#MediaLiteracy #InformationConsumption #Misinformation #CriticalThinking #FactChecking


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about the roads of the
information superhighway.
We're going to talk about how to be better consumers of
information.
What we're going to do today is I'm going to go over some
tactics that I have seen become more common online among
commentators and pundits, and even among news outlets.
and how these tactics and techniques
are intending to manipulate you.
Once you're familiar with them, it's
kind of like an inoculation.
You become less susceptible to them.
They don't work as well anymore.
So we're going to go over a bunch of them.
Understand they can be used in conjunction with each other.
And let's just dive right in.
What's the first thing you see, whether it
be a text article or a video, the headline, right?
The headline.
If the headline elicits an emotional reaction, be on guard.
Be ready.
Headlines, in theory, should pique your curiosity.
They shouldn't encourage you to form an opinion right away.
They are intended to inform, not inflame.
So if you start to feel some way just based on the headline, your guard needs to go up.
And odds are you're going to see some of this other stuff after that.
So how do they inflame rather than inform?
One thing is playing into confirmation bias.
Jim's proposed massive spending bill, and that's a tame headline, not incredibly evocative.
But even with something that tame, you're going to get different reactions just based
on the headline from different groups of people.
If you're a Republican and you read that, you're like, ah, tax and spend liberals.
If you're a Democrat, you're thinking, oh, please let it be healthcare, right?
So encouraging that team mentality is something that has become incredibly common because
it's good for ratings.
So they do it.
Sometimes if you're a political commentator, it's hard not to.
There's no real way to talk about politics without playing into some demographic with
the headline.
going to happen. What you need to be on guard for is when it when it begins to
infect areas that aren't actually partisan, like as an example public
health. Okay, so once the confirmation bias is there, what are other techniques
they use. One is inserting a name, a name that is politically divisive. Ex-Clinton staffer
alleges blank, right? If you're a Republican, you already think that person's lying. Now
the reality is that person could have been a staffer for one of the Clintons 20 years
ago, but that association, it helps create that first impression.
Another is to ask a question.
When you're talking about consuming information, you want answers, not questions, right?
A lot of times, pundits will use a question to shape a narrative.
Does Bo kick puppies?
That's the headline.
It doesn't matter if the body of the article says that I don't.
The allegation was made.
It's already put out there and it helps shape it from the very beginning.
One is to use a quote from somebody prominent as the headline itself.
What this does is it protects the news outlet from getting sued.
If the headline is Trump colon, so Trump has said all illegals are evil, most people will
They'll disregard that part where Trump, where it's identifying Trump, and they just get
that headline.
It helps play into a confirmation bias.
People who are already prejudiced against those who come here without prior authorization,
they're already going to lean in, they're going to love this article because it's stoking
that outrage machine. Remember, a headline should inform you, not inflame you. Anytime
you see a quote as a headline, be on guard. Okay, so something else you'll see a lot is
personification. Personification of an organization. Sometimes it's fair to do that. Biden's foreign
foreign policy. Is it really Biden's foreign policy? Did Joe Biden sit down and write all
this stuff out? No. It's State Department's foreign policy, but it's the Joe Biden administration.
So that's fair. Same thing with Trump. Personifying Amazon as Bezos. Most times, that's fair.
could be decisions that were made that he didn't have anything to do with, but
most times that's going to be a fair personification. But what if it's a
person who is politically divisive? What if it's Soros or Fauci, right? Somebody
that has been linked to wild theories. Putting their name in the headline allows
people to immediately associate it with something they either like or don't okay
so getting out of the headline and again you can see these tactics in in the body
or in the actual presentation Tucker Carlson is famous for using questions to
shape a narrative. Never answers them, just asks them in a leading manner. Okay.
Polls. Polls are only as good as the way they were collected. If I was to put out
a poll on my social media that said, do you think Trump is a great president? The
answer is going to be a resounding no. That's not a surprise, it's not
representative of anything. Now if it was a sample of Fox News viewers and they
said no, well then yeah maybe we should pay attention to that. But you have to
look at the context of the poll, how it was, how the information was collected,
the questions and the options they were given. As an example, if the choices are
is Trump a great president, good president, best president ever, or just
okay? The odds are that people who see that, who don't believe any of those,
they're just not going to respond. So 96% of people think Trump is at least good.
It's all about the options that they were presented. And then how it was
collected, something that conservative outlets used to do, I don't know if they still do this,
but they used to only call people on landlines. So people who only had landlines or who were
available via landlines during the day typically skewed older, therefore more conservative. So
they could shape the outcome of a poll by doing that. So anytime you see a poll, make sure you
know the context around how that was collected, how the information was collected.
Now if we're talking about polls, we should also talk about statistics.
Statistics in the media, there's really two uses, two real main uses anyway.
One is to provide context.
A big part of the problem today is that people don't like to provide context.
don't see that as much anymore. Most times people are trying to establish a
causal relationship. One thing causes the other. The problem is statistics that
appear linked don't necessarily show a causal relationship. I happen to know
that if Ben and Jerry's has a good month in sales that a whole bunch of people
are going to drown that month. It's not because Ben and Jerry go out and hold
people under water because they made a bunch of money, it's because it's summer.
The good part about this is that when people use statistics that don't actually
show a causal relationship and try to imply that it does, if you are quick-witted
acquitted, you can get them to admit to the exact opposite of what they were trying to
show with it most times.
A good example is those statistics that circulate every so often.
Black Americans make up 13% of the population, but they're responsible for 30% of the crime
or whatever it is.
When you ask somebody, are you saying that having a darker skin tone makes you more likely
to commit a crime?
Is there a causal relationship between these two?
Most people back away from that statistic real fast.
And when they do, it leaves you with an opening.
Well, if it's not that, then what is it?
Could it be that there's a systemic issue with enforcement, the criminal justice system,
of poverty because of... and it leaves the door open to actually provide context to that statistic.
But you have to be fast. And to be honest, I've done this for a long time.
The long-term results of this are hit-and-miss. Sometimes you actually change the person's
opinion, and other times you will see them using the same statistic later on
social media. But it does give you that opening to at least try. So when you see
statistics, even if they appear to show a causal relationship, make sure they
really do. Look into how those statistics were collected if you have
questions. Now most times there will be a link or at least the at least some kind
of reference to figure out where those statistics came from so you can go and
look and see how they were collected. Okay so the next one, false links. Okay
You will find this a lot, a whole lot on right-wing websites.
It'll say, this study proves that some false claim about our public health issue, and if
you actually click on the link that they provide, sometimes it's not even about that topic,
but there's a false sense of security that is created when there's a bunch of links,
their citations, and people just assume they're true, and they don't track them down most times.
So be aware of that. Something else that occurs a lot, definitely happens to your uncle on Facebook
all the time, is an image out of context being used to stoke outrage. So the image shows up,
it's normally in meme format and you know this horrible thing happened and
it's whoever's fault right now anytime you run across an image that raises
questions either the presentation of the image is suspect or it's making a claim
that doesn't make sense. Take the image, save it, go to TINEYE and upload the image. It will give
you results like a search engine of pretty much everywhere that image is online. This is how I
will debunk stuff very quickly at times because a lot of people who peddle in misinformation,
they like to use visual aids. The problem is, since they're making stuff up, they
don't really have genuine ones, so they will use stuff out of context from other
places. Many times when you have that feeling about an image, you run it
through TenEye, you will find out that that image existed online before the
event that they're claiming it represents. Now, there are more savvy
peddlers of misinformation and they know that people do this. So what they've
started doing is rotating the image 15 degrees or more. When they do this, it
messes up the search engine. So if you have an image that gets no results,
rotate it. See if you can find it that way. Most times you can. If that happens,
not only was the person being misleading, they were doing it intentionally. Okay,
then you also have factual yet misleading statements. The Biden
administration plans to give illegal aliens $450,000.
I mean, that's a factual statement, but it's misleading
because it lacks the context of as a settlement for a
lawsuit because they were treated poorly.
You see that a lot.
And this is often combined with the question routine.
They give you a piece of information, such as there was a protest against mandates at
this airline.
The union filed a complaint about these mandates.
Is there a strike?
Is there a sick out?
we know? What do you think is happening? And that's how they do it, and they can
manufacture a narrative in this way. Then you get misinformation simply because
people were unclear. Now, this can be intentional or not, but this does happen
a lot. Believe it or not, your favorite commentator is in fact a human being, so
So sometimes it can be a mistake.
If you know a whole lot about bees, and you watched that video over on the other channel
about the American bumblebee, you walked away from that going, what just happened?
Over on the other channel, if you don't know, I don't script.
I get an idea in my head about what I'm going to talk about, and then I talk about it.
Then I review the video, make sure I got all the points, and go from there.
happened. I don't know, I messed up. And it's funny because you can see clearly
that I intended to talk about something but it got dropped. Early in the video I
mentioned non-native species. And I have a... I actually have a giant tub of honey
sitting on the shelves behind me as an Easter egg. The plan of the video was to
use the American Bumblebee to talk about a bunch of different pollinators somewhere
something happened, and I dropped a whole section of what I had planned to say.
Now I think most people caught on when I started talking about commercial bees.
If they know anything about bees, they know that bumblebees aren't commercial bees.
That those are honeybees, hence the tub of honey.
But I don't think the word honeybee exists in that entire video.
I dropped the ball on it.
happens because it's kind of a harmless error in the sense of I would assume
that most people who are interested in this topic enough to watch the video on
it know the information I left out it's probably not a big deal but at the same
time there are probably people who walked away from that video thinking
that somewhere there are commercial bee operations running with American
bumblebees, which that's not a thing.
So sometimes it's an accident.
But sometimes people can be intentionally unclear.
And this also gets combined with the question routine.
We don't have this evidence yet.
We don't really know.
What do you think is going on?
And they lead you down a path.
They will play into a perceived lack of information when they're doing it intentionally.
They don't just drop an entire section of the video.
And then another one is assigning intent.
And this is a big one too.
somebody's intent. That's actually really hard. Figuring out what somebody did?
Pretty easy. Why? That's an entirely different matter. When you hear the word
seems, appears, apparently, these are clues that you're getting somebody's
opinion. You're getting their perception of the events. Now a lot of times this
This is when intent gets assigned.
Unless the person has a really good track record of being able to discern intent and
they're providing a bunch of context, be very leery at this point.
Because it could be intentionally misrepresented by the person assigning intent.
Joe Biden traveled to Moscow secretly.
He must be a spy for the Russians, right?
Or maybe he was going there for a secret summit.
You have to have some reason to assign the intent that is being assigned.
And today, with as inflamed as people are, that requirement for a little bit of evidence
to back up the opinion, it's slipping a lot. A good example, Bo chose to put this video on
the other channel because he didn't want people to know he had messed up in that American Bumblebee
video. Assigning intent without any evidence. The reason this is going to go on the other channels
is because it's long. And now that I said this, I feel obligated to share it back to, anyway.
These are the main methods that are currently being used, and it's a cat and mouse game, it constantly changes,
but these are the main methods that I see being used that are actually successful at manipulating people.
So hopefully now that you've heard about them, you can be a little bit more on guard for them, but the key thing is that
headline.
headline, if you're outraged just by the headline, it's on purpose.
They did that on purpose.
Sometimes there are situations in which you're going to be outraged by what happened.
But if every single day this news entity, or this outlet, or this commentator has a
new thing for you to be angry about, for you to divide yourself off from other segments
of society, you need to be on guard.
The world is chaotic, it is scary, but there isn't something to provoke outrage occurring
every day.
So if your favorite commentator makes you feel outraged and angry every day, they're
doing it on purpose, and they're probably creating false narratives in the process.
Anyway, it's just a thought.
have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}