---
title: Let's talk about the other case and verdict....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Fcdq_ypetAc) |
| Published | 2021/11/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Running late due to hesitancy to speak on a specific case.
- Refrains from adding to a certain ongoing discourse.
- Chooses to address the case of Cameron Lamb instead.
- Lamb's fatal encounter with police stemmed from an argument with his girlfriend.
- Police arrive at Lamb's home after being called by a witness, leading to his death within seconds.
- Prosecution argues the officers had no valid reason to be at the scene.
- Allegations surface regarding potential evidence tampering by the officers.
- Officer DeValcunier opts for a judge trial over a jury trial and is found guilty.
- Judge's ruling seems to focus on the violation of the Fourth Amendment.
- Beau concludes by stressing the importance of recognizing progress in efforts to make a positive impact.

### Quotes

- "Those people who are trying to make the world a better place, they have a habit of looking for problems."
- "You can't fix it if you don't know what's broken."
- "It's important to make sure that you take a moment to acknowledge what is probably a win."

### Oneliner

Beau hesitates on a specific case and sheds light on the fatal encounter of Cameron Lamb with the police, underlining the significance of recognizing progress in efforts to bring positive change.

### Audience

Advocates for justice

### On-the-ground actions from transcript

- Advocate for police accountability and transparency (implied)
- Support organizations working towards police reform (implied)

### Whats missing in summary

Insights on the broader implications of police encounters and the importance of seeking justice and reform efforts.

### Tags

#PoliceAccountability #Justice #CommunityActivism #Progress #Advocacy


## Transcript
Well, howdy there internet people, it's Beau again.
So, we're running late today, as you can tell.
We're running late today because I don't know what to say, to be honest.
My inbox is full of people asking me to talk about a specific case.
The thing is, I don't know that I have anything of value to add to that conversation.
I really don't.
The conversation is taking place, people are talking about it, and I don't know that I
have anything beneficial to add.
You know, I always try to be very careful with my words.
I didn't get into this to feed into providing people with their two minutes of hate.
So rather than talk about that verdict, rather than talk about that case, I want to talk
about a different case.
I want to talk about the case of Cameron Lamb.
You might remember that name.
Was a name that was chanted during the unrest, you know.
And if you don't remember what happened, he got into an argument with his girlfriend in
2019, and it was a bad one.
By all accounts, it was a bad one.
She's moving out.
She's moving out.
She leaves, moves some stuff to a relative's place, comes back to pick up some other stuff
he left by the curb, and they get into a screaming match out there by the road in the driveway.
He throws lug nuts at her car, and she leaves.
And he decides to get in the car and follow her.
Up to this point, nobody's called the cops.
Nobody has called the cops.
And he starts to kind of chase after her.
His roommate calls.
He's like, yeah, you need to get back here.
And he does.
He goes home.
He goes home.
But during that short period where he's chasing after her, a cop sees it and calls it in.
And a helicopter guides two plainclothes officers to his house where he's parking his vehicle.
One of them is named DeValcunier.
According to reporting, it took nine seconds for DeValcunier to arrive and to shoot Lamb
and kill him.
He's brought upon charges, and the prosecution, they make the case.
They're like, there's no call to the cops.
They have no reason to be there.
They didn't identify themselves.
They have no reason to be in the backyard.
They don't have a warrant.
They don't even have what they would need to get a warrant.
They didn't ask for permission to be in the backyard.
Blays it out.
Sets it up.
Fourth Amendment violation.
They go on to cast doubt on the officer's story with a voicemail that was recorded.
They also suggest that officers may have staged the scene and moved a firearm to make it appear
as though Lamb was armed when he wasn't.
They also suggested that cops may have planted evidence because things were found on Lamb
after he was moved from the scene that weren't there on scene.
Now DeValcunier decided to waive his right to a jury trial.
Asked for a judge to decide.
That is common for law enforcement.
It's a common move for cops to do when they're on trial because they believe judges will
be more favorable to them.
I mean they would say it as they believe the judge has a better understanding of the law.
Something like that.
The judge found him guilty.
Found him guilty.
Involuntary man and armed criminal action.
Now if I understand the law correctly there, the armed criminal action carries a minimum
mandatory of three years to be served consecutively.
So on top of whatever he's sentenced to for the involuntary man.
The judge really it seemed to hinge on the Fourth Amendment issue.
The fact that they shouldn't have even been there.
So it didn't matter what else occurred.
Never should have been in the backyard.
That verdict came down today.
That verdict came down today.
Those people who are trying to make the world a better place, they have a habit of looking
for problems.
Which they have to do.
Don't get me wrong.
You can't fix it if you don't know what's broken.
It's important to make sure that you take a moment to acknowledge what is probably a
win.
If you don't and you just focus on things that are broke, you'll burn out.
You can't do that because there's a whole lot of people counting on you to make the
world a better place.
Anyway, it's just a thought.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}