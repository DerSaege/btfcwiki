---
title: Let's talk about bad news for DeJoy and good news for the postal service....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OYQlQmhMUBQ) |
| Published | 2021/11/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden can't directly fire the Postmaster General, but he can appoint people to the board who can.
- The board has nine members, with six being Trump appointees and allies of the Postmaster General.
- There have been concerns about DeJoy's leadership and ethics prompting calls for his removal.
- The Biden administration intends to replace two board members, potentially giving them a majority.
- DeJoy's policies have caused delays in Postal Service operations, impacting the supply chain.
- The post office plays a significant role in the supply chain by delivering components and parts promptly.
- Every setback in the supply chain is critical as businesses strive to operate smoothly.
- DeJoy may be on the verge of being ousted as the Biden administration takes action.
- Uncertainty remains whether this move is strategic planning or a last-minute decision.
- The Postal Service's efficiency is vital in tackling current supply chain challenges.

### Quotes

- "DeJoy may be headed for the door."
- "The Postal Service has to get up to speed."
- "Every setback is a major setback."

### Oneliner

Biden administration makes strategic moves to potentially oust Postmaster General DeJoy, addressing concerns about leadership and ensuring Postal Service efficiency amid supply chain challenges.

### Audience

Government officials, Postal Service employees.

### On-the-ground actions from transcript

- Contact elected representatives to voice support for Postal Service reforms (implied)
- Stay informed about developments regarding DeJoy's potential ousting and its impact on Postal Service operations (implied)

### Whats missing in summary

Insights on the broader implications of Postal Service reforms and the significance of an efficient supply chain in today's economy.

### Tags

#PostalService #DeJoy #BidenAdministration #SupplyChain #Efficiency


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about the Postal Service
and how apparently the first rule
of ousting the Postmaster General
is to not talk about ousting the Postmaster General.
It appears that DeJoy may be headed for the door.
Now, for a while, people have been looking to Biden,
wondering when Biden was going to fire DeJoy.
That's not how it works.
There's a board.
There's a board of nine people.
Biden can't directly fire the Postmaster General.
However, he can appoint people who will fire him.
That board made up of nine people,
six of whom are Trump appointees and allies of DeJoy.
People want DeJoy gone
because there have been some questions
about leadership and just general ethics, to be honest.
Conventional wisdom said that when these two appointments
that are coming up to be these two positions
that are nearing the end of their term,
that Biden was gonna re-nominate one of them
because they're a Democrat.
And Biden's kind of an establishment Democrat person.
So it stood to reason
that that person would get to keep their job.
Apparently not.
The administration has announced intentions
to replace both of them.
Once that occurs,
there will be five Biden appointees sitting on that board.
And DeJoy may be in serious trouble,
may be headed for the door.
Now, there are questions as to whether or not
this was the Biden administration playing 4-D chess
and just keeping this close to their vest,
or they just now realized there was an issue
and decided to act on it at the last minute.
We'll probably never know which it is,
but it does appear that the Biden administration
is making the first moves to oust DeJoy.
DeJoy's policies have resulted
in the Postal Service slowing down.
Right now, with the backlogs that we're having
with the supply chain, that can't be the case.
The Postal Service has to get up to speed.
When we're talking about the supply chain,
the post office really doesn't seem to kind of fit into it,
but it does.
A lot of things get mailed.
And when they do, if they arrive late,
it slows other things down.
We could be talking about components and parts,
just little things that are required
for the smooth running of businesses.
Right now, every setback is a major setback
when we're trying to get that supply chain up
and running the way it was before.
So it may be that DeJoy is headed for the door.
We'll have to wait and see,
but the first moves are being made.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}