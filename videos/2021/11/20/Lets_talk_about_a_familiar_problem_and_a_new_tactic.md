---
title: Let's talk about a familiar problem and a new tactic....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=obvaoJORHYg) |
| Published | 2021/11/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the concern of a viewer whose parents refuse to get vaccinated, citing concerns of being "poisoned".
- He explains that the fear of being poisoned often stems from misinformation about vaccine ingredients.
- Beau suggests checking the actual vaccine components to debunk these fears.
- He mentions a tactic where some individuals believe the vaccine is riskier than contracting the virus due to misinformation on survivability rates.
- Using US data, Beau illustrates that the vaccine is significantly safer than contracting the virus, even with inflated adverse event reports.
- Beau delves into the Vaccine Adverse Event Reporting System (VAERS) and its limitations, pointing out that reports to VAERS are voluntary and subject to biases.
- He humorously mentions a bizarre VAERS report of an MMR vaccine causing alien abduction and turning someone into the Incredible Hulk.
- Beau provides a specific VAERS entry number (0-221-579-1) to showcase the absurdity and unreliability of some reports.
- He encourages relying on factual information and statistics to demonstrate the safety and effectiveness of vaccines in reducing transmission and complications.
- Beau concludes by urging people to embrace the vaccine as a reliable solution that offers numerous benefits and reduces the burden on healthcare systems.

### Quotes

- "The vaccine is way safer."
- "Take the win on this one."
- "Our failure to counteract misinformation in the United States leads it to be all over the place."

### Oneliner

Beau addresses vaccine hesitancy by debunking misinformation with factual evidence and statistics, urging individuals to embrace the safety and effectiveness of vaccines.

### Audience

Individuals with vaccine-hesitant family members.

### On-the-ground actions from transcript

- Verify vaccine ingredients to debunk misinformation (suggested).
- Use factual data to showcase vaccine safety (exemplified).
- Address doubts by comparing vaccine safety with contracting the virus (implied).

### Whats missing in summary

In-depth exploration of the importance of countering misinformation to foster vaccine acceptance.

### Tags

#VaccineHesitancy #DebunkingMisinformation #VaccineSafety #PublicHealth #CommunityResponse #FactualEvidence


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about something
that's going to sound real familiar at first.
But while it's familiar, it's ongoing,
and I've seen a new tactic, so we're going to go over that.
And this message, while it starts off
like something we've heard over and over again,
there's a little twist at the end
that shows how interconnected everything really is.
And it's worth noting.
OK, so I have a personal question for you.
My parents won't get vaccinated.
My mom says that she does not want to get poisoned.
How can I help them to see that the vaccine is the best
solution to get out of this situation?
Every time I call them, they tell me we can't do anything
and everyone is hostile against us.
My response is always, well, get vaccinated
and you will have no problems.
They despise me for such statements
and I can't take it anymore.
I live in Switzerland.
See, in the US, we typically look
at this as an American problem, and it is.
It is.
But because US culture is so dominant in media,
stuff spreads.
Stuff spreads.
And it's worth noting that the issues
that we failed to properly address here, well,
they spread just like the public health issue all over the world.
OK, so this word poisoned.
Let's start there.
I've run across this here in the US.
Typically, somebody who uses this term,
they have a list of chemicals or a chemical
that they believe is in the vaccine.
Out of the times I have checked, it's literally never been there.
What it normally is is something that has the same root.
And people who may not understand how slight change is
when it comes to chemistry can drastically alter something,
believe it's the same chemical.
So I would start there.
I would start by checking to see if whatever they're worried
about is actually in it, because it's probably not.
So start with that.
But there's another tactic.
People who go this route with it,
they tend to believe that they're being safe.
They may not necessarily be conspiratorial.
They may just have fallen victim to misinformation.
And they think that because in their mind
there is high survivability, that it's riskier
to get the vaccine.
So the answer, and I've seen this work twice,
is to show them that that's not the case.
You can use US numbers for this because they're widely accessible.
I don't know Switzerland's.
But given the fact that whatever they believe
is probably heavily based on US stuff, you can use this.
These numbers are from August 26th, mainly,
because that's when I pulled them last time
and I'm too lazy to pull new ones.
At that time, there were 350 million doses
that had been given out.
In VAERS, there were 6,968 lost.
Now, if you know how VAERS works,
don't worry, we're going to get to that.
If you don't, just know for right now,
this number, incredibly inflated, incredibly inflated.
And we'll get to that in a minute.
But right now it doesn't matter,
because this is what they believe.
They believe this number is accurate.
So just let it stand for a second.
You can tear it apart after you prove your point.
350 million doses, a little less than 7,000 lost.
Okay.
How many cases have there been in the US?
47.4 million.
How many have been lost?
766,000.
You do not have to be a mathematician to figure out
which one is safer.
More doses, less lost, even with the inflated number.
This shows them the vaccine is safer than getting it.
They're going to get one or the other.
Odds are they're going to get the vaccine
or they're going to contract it, which is safer.
I've seen this work, so add it to your toolbox.
Now, for those who...
Let's talk about VAERS.
Okay, VAERS is an early warning system.
It is totally not designed to be used the way people use it.
In fact, on the website, it actually has a disclaimer.
VAERS accepts reports of adverse events and reactions
that occur following vaccination.
Healthcare providers, vaccine manufacturers,
and the public can submit reports to the system.
While very important in monitoring vaccine safety,
VAERS reports alone cannot be used to determine
if a vaccine caused or contributed
to an adverse event or illness.
The reports may contain information that is incomplete,
inaccurate, coincidental, or unverifiable.
In large part, reports to VAERS are voluntary,
which means they are subject to bias.
That's on the website.
Okay, so from here, that should be enough for most people,
but a lot of people like anecdotal evidence
more than the people who run the website saying,
hey, you can't actually use it for this.
So you want to give them that anecdotal evidence,
give them my favorite VAERS entry ever.
It is 0-221-579-1.
That is my favorite entry in the entire system.
According to this entry, again, incomplete, unverifiable.
An MMR vaccine caused alien abduction,
turning green, super strength, and rage attacks.
Vaccine turned him into the Incredible Hulk.
That's on there.
This shows you how susceptible to bad information it could be.
You know, I'm not a scientist,
but I definitely have questions about this one.
I'm a little skeptical that this actually occurred.
There are a bunch of them like this.
This is just my favorite.
Now, I know if you are one of those who is in the information
silo that suggests that the vaccine is bad,
you're going to say, no, that was debunked.
That's not even on VAERS.
I'm looking at it right now.
Yes, it is.
Your favorite right-wing site that debunked
the existence of this entry forgot
to take something into account.
See, prior to February, the number was 221579-1.
In February, they redid the numbering system.
You have to add a zero at the beginning to see it.
And just so you know, that's actually
in the instructions of the search engine.
I am doubtful of the ability of any fact checker
that can't figure out how to work the search engine
that they're using.
It is there, I assure you.
So I would use those two things.
See if they have a list.
If they do, check.
See if it's even in there.
It's probably not.
And then compare the safety of getting vaccinated
versus the safety of contracting it.
The vaccine is way safer.
Even if you take the incredibly inflated numbers from VAERS,
it's still safer.
That's where I would go with it.
At the end of the day, we have a vaccine that is safe.
It is reliable.
It is effective.
It reduces transmission.
It reduces serious complications.
It reduces hospitalization.
It's a win.
Take the win.
Just take the win on this one.
That's where I would go with it.
And this information using US numbers
may be helpful because the odds are that the information they
have in Switzerland or in any other country
is heavily based on, quote, activists here in the US.
So this information may directly refute the stuff
that they've been told based on US sources.
So I would try this first.
If you have comparable data from Switzerland,
it would be useful to have on hand.
I feel you.
I feel you.
I know a lot of people who are trying
to get their parents vaccinated, and they
are running into this issue.
They're running into the problem of their parents
being overwhelmed by the amount of information
that is coming in.
And then in some ways, it seems like they froze.
It's not even that they're certain they're against it.
They just have enough information
that has caused doubt.
So you have to help alleviate that doubt.
Showing them that whatever they think is going to poison them
is not in the vaccine and showing them
that the vaccine is safer than actually contracting it
might go a long way.
The global numbers, I want to say,
it's like 4 billion doses given or something like that.
It's ridiculously high.
If there was an issue, we'd know by now.
They've waited long enough for those of us
who rushed out to get it immediately to play guinea pig.
I hope it helps.
And this is definitely a moment for us
to realize that while we are in the United States,
it's important for us to address issues like this quickly
because they spread.
They spread.
Our failure to counteract misinformation
in the United States leads it to be all over the place.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}