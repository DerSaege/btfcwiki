---
title: Let's talk about Texas and this winter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=p6q4zo0BNjI) |
| Published | 2021/11/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recalling the Texas winter crisis from last year when the electrical grid failed and caused significant issues.
- A report suggests that winterizing equipment could have reduced power outages by 67%.
- The failures occurred in temperatures presumed to be within the plant's tolerance, indicating overestimation of cold weather resistance.
- The Texas Railroad Commission, responsible for natural gas, is releasing new winterization rules after winter.
- Statements from ERCOT express confidence in addressing electric-related issues but lack specific updates on progress.
- Beau doubts the grid's readiness for the upcoming winter, not convinced by the assurances from authorities.
- Urges Texans to prepare for winter by stocking up on essentials like firewood and charcoal.
- Advises planning for extreme cold scenarios ahead of time.
- Beau remains skeptical of the grid's preparedness and encourages individuals to be proactive in readiness.
- Emphasizes the importance of being prepared for potential failures, suggesting skepticism towards official promises.

### Quotes

- "It appears as though they needed to winterize."
- "If we see something like we did last year, I think it'll fail again."
- "It's much easier to build the arc before the flood."

### Oneliner

Be prepared for winter in Texas: doubts persist on grid readiness despite official assurances, urging proactive measures.

### Audience

Texans

### On-the-ground actions from transcript

- Stock up on firewood and charcoal for winter preparation (suggested).
- Plan ahead for extreme cold scenarios (suggested).

### Whats missing in summary

Beau's detailed skepticism and call for individual readiness in the face of potential grid failures.

### Tags

#Texas #WinterCrisis #GridFailure #Preparedness #Skepticism


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about Texas and winter,
because winter is coming.
All right, so most people remember
what happened last year.
Electrical grid failed, caused a lot of problems.
There's been a report.
I know everybody's going to be surprised by this,
but it appears as though they needed to winterize,
as everybody said last year.
and for preceding years.
The report says that had the equipment been winterized,
it would have reduced the power outages by 67%.
That's significant.
That is significant.
One of the other things to note is
that they're suggesting that they need more connections.
Because if they have to restart a grid from nothing,
They need electricity to do it, you know?
One of the surprising things was that 81% of the failures occurred in temperatures that
were presumed to be within the plant's tolerance, meaning that they are overestimating how cold
it can get before they see failure.
That's worrisome because it means that their projections are off.
they will be corrected. Now, the Texas Railroad Commission, which is in charge
of natural gas, because that makes sense, they are releasing new winterization
rules. They'll be ready in a couple months, like after winter. Okay, now we
also have this statement from from ERCOT. For all the major electric related
issues and recommendations identified in this report, we are confident they are
already being addressed in actions taken or underway by the Texas Legislature
Public Utility Commission or ERCOT. We have made significant progress since
Winter Storm Urie and continue to take aggressive actions to ensure electric
grid reliability.
Then it goes on, the electric grid
will be able to perform significantly better
this coming winter than in the past.
I'm not going to mince words with this.
I don't believe this.
I don't.
Normally, when you're getting an update from an agency
like this, there's a lot of, hey, we've
updated 80% of our lines, we've winterized this amount of equipment, so on and so forth.
Yeah, I'm not seeing that. It appears that underway means that the Texas Legislature said,
hey, do something about this. Not that it was actually done. If I lived in Texas,
I would be ready this winter. I would make sure that I had firewood, that I had a bag of charcoal
setting aside. You know, people ask about that one. That's for after hurricanes, so
I can cook. I would make sure that you have everything that you wish you had
last year. I don't believe that the grid is up to the task. If we see something
like we did last year, I think it'll fail again. I don't think they're there yet.
They weren't very forthcoming last year and I don't really believe what they're
saying now. I would note that they can't be that far along in getting ready if
they don't even have the winterization rules done yet. I would definitely be
prepared. I would take this moment to figure out how you would deal with
with extreme cold if you have to deal with it again. It's much easier to build
the arc before the flood. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}