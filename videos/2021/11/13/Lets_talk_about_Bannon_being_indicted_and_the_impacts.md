---
title: Let's talk about Bannon being indicted and the impacts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=igfjSS0f_T0) |
| Published | 2021/11/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Bannon got indicted, may prompt others ignoring subpoenas to reconsider.
- Cases from January 6th are now starting to be decided.
- Lengthy legal process may impact the midterms.
- Republicans might have miscalculated by dragging out the process.
- Bannon's actions could affect the midterm elections.
- Potential bombshells may arise during the campaigns.
- Trump trying to block release of his presidential archives.
- Republican obstruction could backfire in the elections.
- Expect a prolonged Bannon saga with possible delays.
- The longer it drags out, the more likely revelations during midterms.

### Quotes

- "Do not expect a speedy end to the Steve Bannon saga."
- "The longer it drags out, the more likely it is that the revelations come out during the midterms."
- "Republican obstruction could backfire in the elections."

### Oneliner

Bannon's indictment and legal process could impact midterms, with Republican obstruction potentially backfiring.

### Audience

Political analysts, voters

### On-the-ground actions from transcript

- Stay updated on the legal proceedings involving Bannon and related political events (suggested).
- Remain informed about the potential impacts of these events on the upcoming elections (suggested).

### Whats missing in summary

Insights into the potential consequences for the Democratic Party and the importance of staying informed about ongoing political events.

### Tags

#SteveBannon #Indictment #MidtermElections #RepublicanParty #LegalProcess


## Transcript
Well, howdy there, internet people.
It's Beau again.
So Bannon got indicted, in case you didn't hear.
There's already a bunch of messages coming in
as far as what are the impacts of this.
And sure, yes, the obvious one is that it may make
other people who are ignoring subpoenas from the committee
reconsider their position.
So there is that, but there's something else
that I think's worth noting.
You know, everybody's pointing to that meme
and saying that we have reached the find out portion
of the chain of events.
The Republican Party may also want to take a look
at the meme of congratulations, you played yourself.
The term speedy trial,
in the federal system is a little bit of a misnomer.
If you think about it, the real cases from the 6th,
from January 6th, are kind of just now starting
to be decided.
Those that carried heavier penalties,
they're just now starting to be resolved.
It's November.
It seems incredibly likely that given the length of time
it's going to take for everything to process through,
all of this is going to be playing out
during the campaigns for the midterms.
That might've been a miscalculation
on the part of Republicans.
It seems like it would have been better politically
to just go ahead and let all this information come out,
take your bumps and bruises,
have certain people not elect to run for re-election
and move on, rather than fight it, drag it out
and have all of this news break in the middle
of some of these people's campaigns.
Which seems to be pretty likely at this point.
If Bannon puts up any kind of a fight whatsoever,
it's going to drag it out and it's going to start
to impact the midterm elections.
Now, I mean, he could just walk in and plead guilty.
I don't know what good it would do,
but that is an option.
But the most likely chain of events seems to be
him trying to capitalize and fundraise on it
and boost his profile with it,
in which case he gets drug out.
In which case, when the revelations finally start coming out,
it'll be during the campaign for the midterms.
And I'm guessing there are going to be some
pretty big bombshells that get dropped
coming from this and from the archives thing.
Which, there's a temporary stay
that will probably end up going to Biden,
but then it'll end up going to the Supreme Court, my guess.
If you don't know what I'm talking about there,
Trump is trying to stop the archives from his presidency
from being released.
And is trying to claim executive privilege,
even though the executive, which is President Biden,
has declined to exercise it.
That's also probably going to end up playing out
around the same time.
The timing of this couldn't be worse for the Republican Party
if they continue to obstruct.
The longer they obstruct, the more likely it is
that it impacts the midterm elections.
I do not believe they're going to be able to hold this off
until after the elections, which may have been their plan.
But I don't see that going their way, to be honest.
So, first thing to know is do not expect
a speedy end to the Steve Bannon saga here.
Because if he puts up any resistance whatsoever,
if he tries to fight the charges at all,
it's going to drag out.
So, we'll just kind of have to take a wait and see on this.
But don't expect a swift closure to it.
But the longer it drags out, the more likely it is
that the revelations come out during the midterms,
during the campaigns.
And that may be the Democratic Party's saving grace
when it comes to the midterms.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}