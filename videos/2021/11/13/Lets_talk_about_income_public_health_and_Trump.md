---
title: Let's talk about income, public health, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t_ZSEmO9DIo) |
| Published | 2021/11/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Income gap affects willingness to vaccinate kids: higher income, quicker vaccination.
- Education not likely the primary factor in vaccination discrepancies.
- Lower income families face barriers like transportation and work concerns for vaccination.
- Offering help with transportation could make a difference in vaccination rates.
- Committee reveals Trump administration's delayed response to CDC warnings in February.
- CDC's inability to brief due to upsetting Trump caused three months delay in response preparation.
- Delay in response preparation potentially cost lives due to ego-driven narrative control.
- Time is lives in organizing a response, especially in public health emergencies.
- Muzzling experienced individuals hindered effective mitigation efforts.
- The importance of remembering the consequences of delayed responses and narrative control in public health crises.

### Quotes

- "Time is lives in organizing a response, especially in public health emergencies."
- "His ego costs time, and when you're talking about organizing a response, time is lives."
- "The virus doesn't care about the political spin. It never did."
- "That needs to be remembered."
- "Offering help with transportation could make a difference in vaccination rates."

### Oneliner

Income influences vaccination rates; transportation barriers affect lower-income families. Trump's ego delayed public health response, costing lives.

### Audience

Parents, Community Members

### On-the-ground actions from transcript
- Offer to provide transportation assistance for families to get their kids vaccinated (exemplified)
- Advocate for transparent and timely public health communication (implied)

### Whats missing in summary

The full transcript provides additional details on the income gap in vaccination rates and the specific consequences of delayed response due to political interference.

### Tags

#PublicHealth #Vaccination #IncomeGap #TrumpAdministration #DelayedResponse


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about
some public health news that has come out. Two different pieces of information, both
worth hearing. We're going to start with the most pressing. It's being disclosed that there
is an income gap when it comes to the willingness of parents to get their kids vaccinated. The
more money you make, the more likely you are to get your kids vaccinated real soon. Right?
If you make more than a hundred grand a year, about half of them plan on getting their kids
vaccinated like immediately, real soon. If you make less than fifty, that number drops
to about a third. Now, conventional wisdom on this is suggesting it has to do with education.
I don't believe that. I don't think that it has to do with education because in these
statistics and these studies, when they're talking about education, they're talking about
credentialing. They're talking about formal education. I do not believe that that's really
what's playing into this. That people who have more money are therefore better educated
and therefore more likely to accept science. I get the connection, but I think it is strained.
Four in ten lower income families said that they have concerns about the ability to get
their kid to somewhere to get vaccinated, about the ability to physically get them there.
Four in ten. That more than makes up for that gap. I think that's probably the primary thing,
or maybe concerns about if the kid has symptoms afterward and they have to take off work.
I think that is where that hesitancy is coming from. So if you're in a position to maybe
offer a ride, help out in that manner, maybe do it. Now, another piece of reporting that
came out of the committee that is looking into the Trump administration's response to
the public health issue. What they kind of outlined is that in February, when the CDC
was like, hey, it's not a matter of if, it's a matter of when we're going to get hit. Well,
that made Trump mad. That upset him. And then the CDC couldn't give a briefing. March, April,
May, and it was June before they could talk again. Three months in the beginning. Three
months in the beginning, which would have been the ideal time to marshal support for
an organized response. It would have been the time to get Americans ready for what was
coming. It would have been the time to have CDC officials out there giving interviews
talking about historical pandemics, how long they lasted, what it took, and helped prep
for what was coming. We lost three months and an untold amount of people because of
that man's ego, because he didn't like the bad news. He wanted to control the narrative.
The virus doesn't care about the political spin. It never did. His ego costs time, and
when you're talking about organizing a response, time is lives. That doesn't need to be forgotten.
That needs to be remembered. There's a whole bunch of information that came out of that
committee yesterday. That is the most important piece from what I see. That's the part that
cannot be overlooked, was the muzzling of the people who had the experience and had
the data to actually mitigate and to make a difference. And rather than leading and
marshaling support, he told them to be quiet. Anyway, it's just a thought. Y'all have a
good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}