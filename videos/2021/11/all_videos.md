# All videos from November, 2021
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2021-11-30: Let's talk about coats and hats.... (<a href="https://youtube.com/watch?v=fWvAI6XEkkE">watch</a> || <a href="/videos/2021/11/30/Lets_talk_about_coats_and_hats">transcript &amp; editable summary</a>)

Beau questions the necessity of coats, debunks myths, and advocates for basic hygiene practices and vaccination to stay safe.

</summary>

"Coats don't even work. It's a myth."
"All these people wearing coats and hats on Instagram and YouTube, shills for big jacket."
"This isn't actually a complicated subject when it comes to masks working."
"Wash your hands. Don't touch your face. Stay at home as much as you can."
"Get vaccinated. Get a booster and don't forget to bundle up."

### AI summary (High error rate! Edit errors on video page)

Observes people on Instagram and YouTube all wearing coats and hats, finds it odd.
Did meme research and discovered only 1601 people on average die from hypothermia yearly.
Refuses to wear a coat because of the low risk of hypothermia, sees it as an infringement on freedom.
Claims coats don't work and are a myth created to sell products, suggests rearranging "coat" spells "taco" which keeps you warm.
Believes people promoting coats and hats are shills for big jacket companies.
Responds to questions about discussing the new variant, vaccines, masks, and hygiene.
Advocates for basic hygiene practices like washing hands, not touching face, staying home, wearing masks, and getting vaccinated.
Encourages getting a booster shot and bundling up.
Concludes by wishing everyone a good day.

Actions:

for social media users,
Wash your hands (implied)
Don't touch your face (implied)
Stay at home as much as you can (implied)
Wear a mask if you have to go out (implied)
Get vaccinated (implied)
Get a booster shot (implied)
Bundle up (implied)
</details>
<details>
<summary>
2021-11-30: Let's talk about Flynn and some developments.... (<a href="https://youtube.com/watch?v=MerRqK0zUaU">watch</a> || <a href="/videos/2021/11/30/Lets_talk_about_Flynn_and_some_developments">transcript &amp; editable summary</a>)

Recent developments in a certain movement in the US lead to internal strife, prompting advice for those with involved family members to give space and time for processing.

</summary>

"And Len Wood released on Telegram audio of a recording that claims to be a conversation between himself and former General Flynn."
"However, at the same time, something else circulating on Telegram is an open letter from somebody who believed in the movement."
"Some of them really wanted it to be true because they are authoritarians at heart."
"If I had a family member who had fallen down this little information silo, I'd give them space right now."
"Maybe that's true. I don't know. But I would certainly give them time to process the things that are going on around them."

### AI summary (High error rate! Edit errors on video page)

Recent developments in a certain movement in the United States are being discussed, with advice for those with family members involved.
A small civil war is happening within a certain alphabet-themed theory world.
Len Wood released an audio claiming to be a chat between himself and former General Flynn, a key figure in this movement.
In the audio, Flynn dismisses the movement as "total nonsense," which could significantly impact its followers.
Despite initial reactions of disbelief, the audio might cause doubt among the movement's adherents.
An open letter on Telegram from a former believer explains why he's stepping away, citing unfulfilled promises and personal hopes tied to the movement.
Many followers of this theory were manipulated or sought hope due to personal stressors.
The collapse of their belief system can be extremely stressful for these individuals who had invested heavily in it.
Suggestions are made to give these individuals space and time to process recent events rather than using logic to convince them.
It is advised not to push individuals too hard as they navigate their way out of this information silo.

Actions:

for family members of individuals involved in movements.,
Give space to family members involved in certain movements (suggested).
Allow time for processing recent events (suggested).
</details>
<details>
<summary>
2021-11-29: Let's talk about what we know and don't about the new variant... (<a href="https://youtube.com/watch?v=KcQZPhUPfFo">watch</a> || <a href="/videos/2021/11/29/Lets_talk_about_what_we_know_and_don_t_about_the_new_variant">transcript &amp; editable summary</a>)

Beau breaks down the uncertainty surrounding the Omicron variant, stressing the importance of maintaining mitigation efforts despite limited information on its severity.

</summary>

"Growing up, my favorite cartoon always told me that knowing was half the battle."
"It's probably, based on an educated guess, more easily transmitted."
"There's a lot that isn't known now."
"Wash your hands. Don't touch your face. Stay at home as much as you can."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Growing up, knowing was half the battle, but with the Omicron variant, we only have a tiny bit of information.
Omicron was discovered in South Africa, but there's no evidence it originated there.
Countries worldwide have confirmed cases, except for a few places like Australia, South America, and Antarctica.
The variant has already spread to various countries like Germany, Italy, Hong Kong, and more.
Flight restrictions may not be effective since the variant is already widespread.
The main concern is whether Omicron will out-compete the Delta variant.
Omicron is considered a variant of concern due to mutations that could potentially make it more transmissible.
The severity of Omicron is still unknown, and experts are acting with caution.
It will take around two weeks to get a clearer picture of the variant's impact.
Nations are implementing precautions like flight restrictions, even though they may not be entirely effective.
Personal mitigation efforts are vital, such as wearing masks, washing hands, and getting vaccinated.
Beau advises maintaining mitigation efforts due to uncertainties surrounding the severity of the Omicron variant.

Actions:

for global citizens,
Wash your hands, don't touch your face, stay at home as much as you can, wear a mask when going out, and get vaccinated (suggested).
Maintain mitigation efforts that have worked, and if you've relaxed on safety measures, bring them back up to reduce risks (suggested).
</details>
<details>
<summary>
2021-11-29: Let's talk about Fred Gray, Alabama, and a new south.... (<a href="https://youtube.com/watch?v=CXGPxa2G6Cw">watch</a> || <a href="/videos/2021/11/29/Lets_talk_about_Fred_Gray_Alabama_and_a_new_south">transcript &amp; editable summary</a>)

Beau sheds light on Fred D. Gray's civil rights contributions, questioning Alabama's choice to uphold Confederate heritage over honoring an American icon like Gray, as people rally to pay a fine to rename a street in his honor.

</summary>

"It's an American success story."
"It is a new South."
"Using a street renamed after him to accomplish the same thing today is probably more of a fitting memorial than the street itself."

### AI summary (High error rate! Edit errors on video page)

Introduces Fred D. Gray, not to be confused with Freddie Gray, and his impact in Montgomery, Alabama.
Fred D. Gray was a pivotal figure in the civil rights movement, representing well-known clients like Rosa Parks and Martin Luther King Jr.
Despite Gray's significant accomplishments, the state of Alabama wants $25,000 to rename a road named after him due to a law protecting Confederate monuments.
People from all over Alabama have offered to pay the fine, reflecting a new South embracing change.
Beau questions why Alabama chooses to honor Confederate heritage over an American icon like Fred D. Gray.
He underscores Gray's journey from oppression to success and suggests that honoring him is a true American success story.
Beau challenges the notion of Confederate heritage, questioning its relevance and positive impact on the South.
He urges Alabama to move forward and not dwell on a past that does not resonate with the majority of Southerners.
While many are willing to pay the fine to prevent Montgomery from bearing the cost, the mayor is considering fighting it in court.
Beau suggests that using a street renamed after Fred Gray to continue his legacy is a more fitting tribute than the street itself.

Actions:

for history enthusiasts, civil rights advocates,
Rally support to pay the fine for renaming the street in honor of Fred D. Gray (suggested)
Advocate for honoring civil rights icons like Fred D. Gray in public spaces (exemplified)
</details>
<details>
<summary>
2021-11-28: Let's talk about a video for grown ups... (<a href="https://youtube.com/watch?v=Zo4YCG8GKdU">watch</a> || <a href="/videos/2021/11/28/Lets_talk_about_a_video_for_grown_ups">transcript &amp; editable summary</a>)

Beau introduces the idea of switching gift tags from Santa to alleviate post-holiday insecurities among children, addressing economic disparities subtly and gradually.

</summary>

"Switch the tags."
"It can really help alleviate that."
"But I think it might be something worth working towards."
"It doesn't take a whole lot for us to stop it."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a topic for grown-ups only, prompting those with children nearby to stop the video.
He recounts a premise from a counselor about kids questioning what they did wrong after Christmas.
The counselor shared that kids compare gifts, leading to feelings of failure or inadequacy.
Beau suggests a simple solution to this issue: switch the tags on gifts from Santa.
By switching the tags, expensive gifts are attributed to parents, and Santa brings what the child needs instead.
Beau acknowledges that societal traditions and systems contribute to unequal outcomes during the holiday season.
Parents may need to explain Santa's role earlier than expected to address economic disparities.
Children understand their family's social and economic status more than adults realize.
Shifting Santa's role to providing necessary gifts can help children comprehend differences in gift-giving.
Beau acknowledges that this shift in tradition won't happen immediately but suggests it's worth striving for to avoid uncomfortable post-holiday dialogues.

Actions:

for parents, caregivers, educators,
Start a community initiative to shift the tradition of gift-giving post-holidays (suggested)
Initiate dialogues with parents and educators about the potential impact of economic disparities on children's perceptions (implied)
</details>
<details>
<summary>
2021-11-28: Let's talk about DOD's new task force looking up.... (<a href="https://youtube.com/watch?v=wxCDxjH2jJ4">watch</a> || <a href="/videos/2021/11/28/Lets_talk_about_DOD_s_new_task_force_looking_up">transcript &amp; editable summary</a>)

The Undersecretary of Defense's new group focuses on terrestrial objects, not aliens, monitoring advanced aviation technology for US security against opposition nations.

</summary>

"I don't think that this is going to have anything to do with looking to the stars."
"Their main focus is definitely going to be things of a terrestrial origin."

### AI summary (High error rate! Edit errors on video page)

Addressing news from the Department of Defense regarding the creation of a new group, the Airborne Object Identification and Management Synchronization Group.
The group's focus will be on investigating unexplained objects in the sky within US airspace, not necessarily aliens.
Emphasizing that the group is more concerned with terrestrial objects possibly originating from countries like Beijing or Moscow.
The main purpose is to monitor advanced aviation technology and ensure US preparedness against potential threats.
Speculating that the group is not primarily about extraterrestrial objects but rather about maintaining security against opposition nations.
Mentioning the prevalence of new aviation technology like unmanned aircraft and hypersonic capabilities.
Stating that the group aims to prevent unauthorized aircraft from flying over US territory.
Expressing skepticism about the group's involvement in extraterrestrial investigations as portrayed in popular media.
Implying that while the group might come across unexpected findings, its central focus remains on earthly technology.
Concluding with a casual farewell, wishing everyone a good day.

Actions:

for aviation enthusiasts, defense analysts.,
Monitor and stay informed about advancements in aviation technology and security measures (implied).
</details>
<details>
<summary>
2021-11-27: Let's talk about tweets, Russia, and that plan.... (<a href="https://youtube.com/watch?v=WWmk6udymV0">watch</a> || <a href="/videos/2021/11/27/Lets_talk_about_tweets_Russia_and_that_plan">transcript &amp; editable summary</a>)

Beau addresses Russian efforts to exploit racial tensions in the U.S., urging action against systemic racism as a critical national security measure.

</summary>

"If you want to say you're a patriot, if you want to keep America safe, we have to address systemic racism."
"Other countries are actively attempting to support it and have been for years."

### AI summary (High error rate! Edit errors on video page)

Recalls discussing a Russian plan from 2019 to influence and destabilize the United States through social media.
Mentions the plan aimed to stoke racial tensions and radicalize black Americans.
Notes that despite the story fading, the operation continued.
Cites the analysis of 32,000 tweets during a recent trial, with many originating from overseas, particularly Russia.
Points out Russia's history of exploiting racial tensions in the U.S. as outlined in the released documents.
Suggests that if successful, the operation could lead to a generational movement among black Americans to break away from the U.S.
Draws parallels between how the U.S. supported the Kurds and how black Americans are targeted due to legitimate grievances.
Emphasizes that addressing systemic racism is not just a moral battle but a national security priority.
Stresses the importance of recognizing and rectifying legitimate grievances to prevent external exploitation.
Concludes by urging action to confront systemic racism as a means of safeguarding the nation.

Actions:

for americans, activists, policy makers,
Address systemic racism in communities (suggested)
Recognize and rectify legitimate grievances (implied)
</details>
<details>
<summary>
2021-11-27: Let's talk about NASA's space battering ram.... (<a href="https://youtube.com/watch?v=TSaXjGAZ1fI">watch</a> || <a href="/videos/2021/11/27/Lets_talk_about_NASA_s_space_battering_ram">transcript &amp; editable summary</a>)

NASA's DART test aims to deflect potential asteroid threats, showcasing technological advancements benefiting Earth.

</summary>

"DART is the first planetary defense test using kinetic impactors, acting as a space battering ram."
"Success in altering the asteroid's speed will be observable from Earth."
"Space exploration and challenges like climate change lead to technologies useful on Earth."

### AI summary (High error rate! Edit errors on video page)

NASA is conducting the Double Asteroid Redirection Test (DART) on the 24th in Vandenberg, California.
DART is the first planetary defense test involving kinetic impactors, acting as a space battering ram.
The test aims to determine how to deflect an unidentified asteroid that could potentially collide with Earth.
It is a concept test to establish a starting point for planetary defense strategies.
The goal is to alter the speed of the asteroid by a small fraction through impact.
Success in altering the asteroid's speed will be observable from Earth.
The spacecraft will utilize a Roll Out Solar Array (ROSA) to generate three times more power than regular arrays.
Despite launching in the current month, DART will reach its destination in September 2022.
Space exploration and addressing challenges like climate change lead to technological advancements applicable on Earth.
Beau encourages focusing on challenges beyond war to foster technological innovation.

Actions:

for space enthusiasts,
Stay informed about NASA's DART mission and planetary defense tests (suggested).
Support advancements in space exploration and technology development (suggested).
</details>
<details>
<summary>
2021-11-26: Let's talk about the perception of race in the US from outside.... (<a href="https://youtube.com/watch?v=CObebKvaX1k">watch</a> || <a href="/videos/2021/11/26/Lets_talk_about_the_perception_of_race_in_the_US_from_outside">transcript &amp; editable summary</a>)

There's a discrepancy between media portrayal and reality in the US, with a shift in racial attitudes but a need for action to dismantle racist power structures.

</summary>

"Travel kills prejudice."
"If you aren't racist, you need to advocate and to whatever civic duty you feel is necessary."
"To change society, you don't change the law, you change thought."
"Thought has changed enough to where a lot of the laws that aren't overtly racist but still manage to be disproportionate in their impacts are still on the books."
"We're witnessing in this seeming resurgence is really the death throes."

### AI summary (High error rate! Edit errors on video page)

There is a discrepancy between the societal issues portrayed in the news about the USA and the reality of a more integrated America.
Racism is receding in the United States despite the negative news coverage, with a shift in attitudes towards race among younger Americans.
The news tends to focus on systemic nationwide issues, such as cases involving law enforcement and old citizens arrest laws like in Georgia.
To change society, the focus should be on changing thought rather than just laws, as thought drives legal changes.
It is vital for white Americans who have shifted their views on race to actively become anti-racist and work towards dismantling racist power structures.
The current conflict arises from the need to dismantle institutional systems rooted in racism that are still in place despite changing societal views.
The media often presents issues as having two sides, even when they don't, which can lead to a skewed perception of reality.
The seeming resurgence of old, broken ideas is actually their death throes, indicating progress towards a more inclusive society.
Traveling and working internationally can help break down prejudices and lead to a more integrated and modern worldview.
Beliefs must be converted into action, advocating for change and engaging in civic duties to dismantle institutional structures rooted in racism.

Actions:

for americans, global citizens,
Advocate for change and get involved in civic duties to dismantle institutional structures rooted in racism (implied)
Travel and work internationally to break down prejudices and gain a more modern worldview (exemplified)
</details>
<details>
<summary>
2021-11-26: Let's talk about Beto and the Texas race for Governor... (<a href="https://youtube.com/watch?v=M0V2Z7sgoQo">watch</a> || <a href="/videos/2021/11/26/Lets_talk_about_Beto_and_the_Texas_race_for_Governor">transcript &amp; editable summary</a>)

Beau provides advice for O'Rourke in the Texas governor's race, urging a reconsideration of his stance on AR-15s to enhance his chances of winning.

</summary>

"If he doesn't walk that statement back substantially and in a very visible way, I don't think he has an electric grid's chance in Texas of winning."
"That is a losing battle in Texas."
"They will freeze and hug their rifle and vote for Abbott."

### AI summary (High error rate! Edit errors on video page)

Analyzing the governor's race in Texas and providing advice for O'Rourke.
Advising O'Rourke to focus on Texas-specific issues rather than national politics.
Acknowledging O'Rourke's strengths as a speaker and in addressing Abbott's failures like the grid.
Suggesting O'Rourke to reconsider his stance on AR-15s and conduct a visible demonstration with firearms.
Pointing out the significance of the AR-15 in Texas politics and the impact of gun rights on voters.
Warning that O'Rourke's stance on taking AR-15s could hinder his chances of winning in Texas.
Emphasizing the uphill battle of advocating gun control in Texas as a whole state.
Mentioning that voters might gravitate towards Abbott if O'Rourke doesn't retract his statement on AR-15s.
Concluding that without a significant change in stance on gun control, O'Rourke may not be able to secure victory in Texas.

Actions:

for texas voters,
Conduct a visible demonstration with firearms to show a change in stance on gun control (suggested)
Focus on Texas-specific issues rather than national politics (exemplified)
</details>
<details>
<summary>
2021-11-25: Let's talk about whether to take the bait at Thanksgiving dinner.... (<a href="https://youtube.com/watch?v=S26tCOWRHKs">watch</a> || <a href="/videos/2021/11/25/Lets_talk_about_whether_to_take_the_bait_at_Thanksgiving_dinner">transcript &amp; editable summary</a>)

Beau addresses navigating uncomfortable Thanksgiving political debates, advising quick shutdowns to prevent relationship damage.

</summary>

"If you know this conversation is going to come up, I'd set the goal of not changing minds but ending it as quickly as possible."
"To everybody else who may be listening to this, when you start talking about politics around that table, just remember that you may not know everything you think you do about every person sitting there."

### AI summary (High error rate! Edit errors on video page)

Addressing the dilemma of whether to have difficult Thanksgiving dinner political debates.
Describing a situation where someone is openly trans at an event with closeted LGBTQ individuals.
Sharing a story about the impact of political debates on relationships during the Kavanaugh hearings.
Advising on handling such situations by swiftly shutting down potentially harmful comments.
Emphasizing the importance of being mindful of the impact of words on relationships.
Encouraging ending contentious debates quickly rather than trying to change minds.
Suggesting that if political subjects arise, one should be cautious as assumptions may not always be correct.
Recommending aiming to prevent lasting damage to relationships during heated debates.
Urging a quick response to harmful comments as a favor to both the speaker and others present.
Reminding everyone to be aware that assumptions about others' beliefs may not always be accurate during political talks.

Actions:

for thanksgiving participants,
End contentious debates swiftly by addressing harmful comments immediately (suggested)
Be mindful of assumptions about others' beliefs during political talks (implied)
</details>
<details>
<summary>
2021-11-24: Let's talk about new hypersonic developments.... (<a href="https://youtube.com/watch?v=osHD5nGLPnQ">watch</a> || <a href="/videos/2021/11/24/Lets_talk_about_new_hypersonic_developments">transcript &amp; editable summary</a>)

Chinese hypersonic missile test triggers US media panic, but US defense advancements likely surpass public knowledge in ongoing arms race.

</summary>

"Get used to this. If you weren't alive during the Cold War, if you didn't study it, understand this is going to play out over and over again."
"It's a back and forth. It's cat and mouse. And this is how arms races began."
"It does appear that the United States probably has a lot more parity with China and Russia than the government is letting on."

### AI summary (High error rate! Edit errors on video page)

Chinese hypersonic missile test sparked US media frenzy, portraying US falling behind.
US foreign policy now focuses on countering near peers like China and Russia.
Three contracts for glide phase interceptor technology were awarded by the Missile Defense Agency.
Lockheed Martin, Northrop Grumman, and Raytheon Missile and Defense received the contracts.
These contracts are for intercepting hypersonic missiles, not developing them.
The US has been working on hypersonic technology for years, evident through companies' experience.
Existing equipment like the Spy-6 radar shows US progress in tracking and intercepting hypersonic missiles.
Media previously portrayed hypersonic missiles as unstoppable, but US defenses are advancing.
US likely has more advanced hypersonic capabilities than publicly known.
US military's strategy involves keeping technological advancements secret from the public.
Arms races between nations like the US, China, and Russia are ongoing and characterized by secrecy and technological advancements.
The US is likely more on par with China and Russia in hypersonic technology than acknowledged.
Continuous technological advancements and secrecy define the dynamics of military competition.
The US has made significant progress in hypersonic capabilities, contrary to initial perceptions of falling behind.
Military advancements and arms races have historical precedence in the Cold War era.

Actions:

for defense analysts, policymakers,
Monitor advancements in hypersonic technology within your country's defense industry (exemplified)
Stay informed about international arms races and technological developments (exemplified)
</details>
<details>
<summary>
2021-11-24: Let's talk about Ahmaud Arbery and the system functioning.... (<a href="https://youtube.com/watch?v=ix9K4BfDmL8">watch</a> || <a href="/videos/2021/11/24/Lets_talk_about_Ahmaud_Arbery_and_the_system_functioning">transcript &amp; editable summary</a>)

The justice system failed in Ahmaud Arbery's case; justice prevailed due to public pressure and unwavering perseverance.

</summary>

"Justice was served in spite of the system failing."
"Justice was served because of public pressure, because of those black pastors certain people didn't want around."
"This isn't over. This isn't a situation where everything went smoothly."

### AI summary (High error rate! Edit errors on video page)

Commentary claims the justice system functioned properly in Ahmaud Arbery's case, but Beau disagrees vehemently.
The system did not function as it should, despite the right verdict being reached.
Local cops were instructed not to make arrests by the Brunswick DA following the incident.
Different district attorneys continued to interfere after being told not to make arrests.
Public outrage ensued after the video of the incident surfaced on May 5th.
A grand jury was convened quickly after the video release, with the Georgia Bureau of Investigation stepping in.
Beau questions the system's functionality when a DA is indicted in the process.
Despite the right outcome, justice was served due to public pressure and perseverance, not the system working effectively.
Acknowledging the victories is vital to avoid burnout, but it was hard-won in this case.
The situation surrounding Ahmaud Arbery's case was a win achieved through relentless efforts and dedication from many individuals.
Justice prevailed not because the system functioned, but rather because people refused to give up.
The victory was a testament to the power of collective action and perseverance against systemic failures.

Actions:

for activists, advocates, community members,
Support and amplify the voices of marginalized communities in seeking justice (implied)
Advocate for systemic changes to prevent similar injustices in the future (implied)
Continue to apply public pressure on authorities to ensure accountability and fairness (implied)
</details>
<details>
<summary>
2021-11-23: Let's talk about being hungry to vote.... (<a href="https://youtube.com/watch?v=Fm1ohE_50EU">watch</a> || <a href="/videos/2021/11/23/Lets_talk_about_being_hungry_to_vote">transcript &amp; editable summary</a>)

Joe Madison's hunger strike calls on President Biden to fulfill promises and end the filibuster for voting rights legislation, appealing to the humanity of the opposition.

</summary>

"As food is essential for the existence of life, voting is essential for the existence of democracy."
"This hunger strike is definitely aimed at President Biden, asking him to make good on his campaign promises."
"You're appealing to the humanity of the opposition."
"Allowing large segments of the American population to be disenfranchised at this level, that's pretty risky too."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of hunger to participate in voting and electoral politics in the United States.
Joe Madison, a civil rights activist turned radio host, is on a hunger strike at 72 years old until voting rights legislation is passed.
Democrats in the House have passed voting rights acts, but they stalled in the Senate, where 19 states have passed laws making it harder to vote.
The Freedom to Vote Act has all 50 Democrats' support in the Senate, with Vice President Harris as a tiebreaker, but faces the filibuster.
The hunger strike aims to pressure President Biden to fulfill campaign promises and end the filibuster, not just pass voting rights legislation.
The filibuster is a Senate rule not outlined in the Constitution, historically used to obstruct civil rights, but its purpose was for debate and consideration.
Joe Madison sees this as a new civil rights movement and urges eliminating the filibuster to ensure voting rights legislation passes.

Actions:

for voters, activists, politicians,
Support voting rights organizations (implied)
Contact senators to support the Freedom to Vote Act and eliminate the filibuster (implied)
</details>
<details>
<summary>
2021-11-23: Let's talk about Thanksgiving with your liberal mom.... (<a href="https://youtube.com/watch?v=uBVtaW4nHwI">watch</a> || <a href="/videos/2021/11/23/Lets_talk_about_Thanksgiving_with_your_liberal_mom">transcript &amp; editable summary</a>)

Beau suggests negotiation framing and idealistic dreaming as methods to shift beliefs and reach a liberal mom or pragmatic allies.

</summary>

"Remind them that it's not. We should be striving for that world where everybody gets a fair shake."
"This is where we want to get to. The thing is, most people like to make dreams a reality."
"We want to get to that world of maximum amount of freedom, for the maximum amount of people."

### AI summary (High error rate! Edit errors on video page)

Introducing a different perspective on "Help Me Ruin Thanksgiving Dinner" focusing on dealing with a liberal mom who is a labor organizer.
The challenge is explaining that liberal policies don't go far enough and uphold harmful systems, but facing resistance and glazed-over eyes during these talks.
Seeking a framing device to address this issue in both personal and labor organizing situations.
Suggests two methods: one involving negotiation framing for organized labor understanding, and the other presenting an idealistic dream for long-term change.
In negotiation framing, starting with a strong position and making the opposition come further to meet halfway seems to work, even if not fully believed.
Presenting an idealistic dream of maximum freedom, cooperation, and quality of life for all is another approach to shift beliefs, even if seen as unattainable in the present.
Encouraging people to strive towards the dream rather than just conceding ground might lead to more tangible shifts in beliefs.
Both negotiation framing and idealistic dreaming have their merits in reaching and persuading a liberal mom or those allied with similar goals but pragmatic in approach.

Actions:

for progressive activists,
Frame arguments as negotiations (suggested)
Present idealistic dreams for change (suggested)
</details>
<details>
<summary>
2021-11-22: Let's talk about resignations at Fox News over Tucker.... (<a href="https://youtube.com/watch?v=nbyG42KGhag">watch</a> || <a href="/videos/2021/11/22/Lets_talk_about_resignations_at_Fox_News_over_Tucker">transcript &amp; editable summary</a>)

Beau addresses resignations at Fox News over misinformation spreading, fearing potential violence due to inflammatory content.

</summary>

"If a person with such a platform shares such misinformation loud enough and long enough, there are Americans who will believe and act upon it."
"I hope that Hayes and Goldberg are wrong and that cooler heads prevail."
"Either way, remember these resignations because if it does occur, you'll hear about them again."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the recent resignations of Stephen Hayes and Jonah Goldberg from Fox News due to Tucker Carlson's documentary about the events of January 6th.
Hayes and Goldberg believed that Fox News' top opinion hosts, including Tucker Carlson, amplified false narratives to support Trump over the past five years.
The resignations were fueled by concerns of potential violence incited by spreading misinformation to viewers.
Beau mentions that Fox News' news division contradicted the information presented by Tucker Carlson.
There's a fear that the misinformation spread by major hosts like Tucker Carlson could lead viewers to take harmful actions based on false information.
Hayes and Goldberg's resignations and comments may play a significant role legally if any incidents occur as a result of misinformation spread by Fox News hosts.
Despite Hayes and Goldberg not being as prominent as Tucker Carlson, their resignations shed light on the potential consequences of spreading inflammatory misinformation.
Beau expresses concern that inflammatory information presented by Fox News might provoke certain viewers to react, potentially leading to violence.
Beau hopes for a peaceful resolution but acknowledges the possibility of misinformation inciting harmful responses.
The resignations of Hayes and Goldberg are emphasized as significant events to be remembered, especially if their concerns materialize in violence.

Actions:

for media consumers,
Monitor and fact-check information from news sources (implied)
Advocate for responsible journalism practices (implied)
</details>
<details>
<summary>
2021-11-22: Let's talk about hot takes, the Christmas parade, and intent.... (<a href="https://youtube.com/watch?v=LyvRzwjSCvE">watch</a> || <a href="/videos/2021/11/22/Lets_talk_about_hot_takes_the_Christmas_parade_and_intent">transcript &amp; editable summary</a>)

Speculation and sensationalism overshadowing a tragedy, preventing meaningful societal change dialogues.

</summary>

"Ignore hot takes."
"Our media isn't useful for that."
"This incident could be the catalyst for good if we talked about it."

### AI summary (High error rate! Edit errors on video page)

Speculation and assigning political intent to incidents like the Christmas parade tragedy lead to sensationalism and ratings-driven coverage.
Despite law enforcement denying a political motive and linking the incident to domestic violence, the story won't receive prolonged media attention.
The complex solutions to issues like domestic violence, mental health support, and toxic masculinity are not attractive for media coverage.
Rather than focusing on blame, the tragedy could serve as a catalyst for meaningful societal change.
Media's prioritization of sensationalism over in-depth analysis prevents constructive dialogues on critical issues.
Guard against misinformation by questioning narratives that lack evidence or intent speculation.
The coverage of the incident initially fed on fear and speculation without concrete information about the suspect's motives.
Society's aversion to in-depth, nuanced dialogues means the story will likely fade quickly from media attention.
The incident bears similarities to other mass incidents, with multiple stressors and a domestic violence connection, but this might not be the focus of media coverage.
Meaningful change requires moving beyond sound bites and pointing fingers to address systemic issues.

Actions:

for media consumers,
Challenge misinformation by questioning narratives lacking evidence (implied)
Engage in meaningful dialogues about complex societal issues like domestic violence and mental health support (implied)
</details>
<details>
<summary>
2021-11-21: Let's talk about boosters and speed.... (<a href="https://youtube.com/watch?v=zPxsbNzIqvo">watch</a> || <a href="/videos/2021/11/21/Lets_talk_about_boosters_and_speed">transcript &amp; editable summary</a>)

The US government is set to approve booster shots, urging individuals to act promptly to aid global vaccine distribution.

</summary>

"It's kind of like putting your own mask on before you help others on an airplane."
"If a whole bunch of people hold out because they're upset about this, it's going to keep the numbers low in the US."
"The quickest way for them to get it at this point is going to be for you to get yours."
"So anyway, it's just a shot."
"Have a good day."

### AI summary (High error rate! Edit errors on video page)

The US government is likely to approve booster shots for everyone over 18.
Booster shots can provide up to 90-95% protection and may be vital in winter.
Concerns arise about poorer countries having to wait longer for vaccines due to booster distribution in wealthy nations.
Despite this, individuals are encouraged to get the booster shot promptly to facilitate global vaccine distribution.
The government prioritizes boosting vaccination rates domestically before focusing on aiding other countries.
Getting the booster shot quickly may expedite international vaccine distribution efforts.
Beau compares the situation to putting on your own mask first before helping others on an airplane.
He acknowledges some issues with the approach but believes it's unlikely to change.
Beau advises getting the booster shot as soon as possible to increase domestic numbers and support vaccine distribution to countries with limited access.
Ultimately, Beau concludes by reminding viewers that getting the booster shot is a simple action.

Actions:

for global citizens,
Get the booster shot quickly to support international vaccine distribution efforts (implied)
</details>
<details>
<summary>
2021-11-21: Let's talk about Thanksgiving and Sesame Street.... (<a href="https://youtube.com/watch?v=QIsBFta_2lw">watch</a> || <a href="/videos/2021/11/21/Lets_talk_about_Thanksgiving_and_Sesame_Street">transcript &amp; editable summary</a>)

Beau navigates a Thanksgiving dilemma involving Ted Cruz, Sesame Street, and social progress, advocating for more progressive content and addressing resistance to change.

</summary>

"Sesame Street isn't woke enough."
"Humanity will crash through it and society will move forward and I am fairly certain that Big Bird and Oscar will be leading the charge."
"Those who want to uphold the past, the status quo, to keep people down so they can stay comfortable, they always lose."

### AI summary (High error rate! Edit errors on video page)

Beau sets the stage for a Thanksgiving family dilemma involving Ted Cruz, Sesame Street, and social progress.
A man is reluctant to visit his father for Thanksgiving due to his father's admiration for Ted Cruz and disapproval of Sesame Street's introduction of an Asian-American character.
The man's wife is Asian, with a family history of internment during World War II, and his father unfairly expects her to represent all Asian people.
Beau praises Sesame Street as not just a kids show, but a tool for education and addressing difficult topics like food insecurity, addiction, and gender equality.
He advocates for even more progressive content on Sesame Street, including characters discussing adoption, refugees, health, and gender equality.
Beau points out that Sesame Street has a long history of addressing challenging topics and being inclusive and diverse.
He criticizes those opposed to Sesame Street's progressive programming, labeling them as socially regressive individuals who resist change and uphold unjust power structures.
Beau concludes with optimism, stating that social progress always prevails in the long run, despite resistance from those wanting to maintain the status quo.

Actions:

for viewers, activists, parents,
Support inclusive and diverse programming like Sesame Street (implied).
Engage in difficult but necessary conversations with children about social issues (implied).
Advocate for progressive content that educates and empowers children (implied).
</details>
<details>
<summary>
2021-11-20: Let's talk about the other case and verdict.... (<a href="https://youtube.com/watch?v=Fcdq_ypetAc">watch</a> || <a href="/videos/2021/11/20/Lets_talk_about_the_other_case_and_verdict">transcript &amp; editable summary</a>)

Beau hesitates on a specific case and sheds light on the fatal encounter of Cameron Lamb with the police, underlining the significance of recognizing progress in efforts to bring positive change.

</summary>

"Those people who are trying to make the world a better place, they have a habit of looking for problems."
"You can't fix it if you don't know what's broken."
"It's important to make sure that you take a moment to acknowledge what is probably a win."

### AI summary (High error rate! Edit errors on video page)

Running late due to hesitancy to speak on a specific case.
Refrains from adding to a certain ongoing discourse.
Chooses to address the case of Cameron Lamb instead.
Lamb's fatal encounter with police stemmed from an argument with his girlfriend.
Police arrive at Lamb's home after being called by a witness, leading to his death within seconds.
Prosecution argues the officers had no valid reason to be at the scene.
Allegations surface regarding potential evidence tampering by the officers.
Officer DeValcunier opts for a judge trial over a jury trial and is found guilty.
Judge's ruling seems to focus on the violation of the Fourth Amendment.
Beau concludes by stressing the importance of recognizing progress in efforts to make a positive impact.

Actions:

for advocates for justice,
Advocate for police accountability and transparency (implied)
Support organizations working towards police reform (implied)
</details>
<details>
<summary>
2021-11-20: Let's talk about bad news for DeJoy and good news for the postal service.... (<a href="https://youtube.com/watch?v=OYQlQmhMUBQ">watch</a> || <a href="/videos/2021/11/20/Lets_talk_about_bad_news_for_DeJoy_and_good_news_for_the_postal_service">transcript &amp; editable summary</a>)

Biden administration makes strategic moves to potentially oust Postmaster General DeJoy, addressing concerns about leadership and ensuring Postal Service efficiency amid supply chain challenges.

</summary>

"DeJoy may be headed for the door."
"The Postal Service has to get up to speed."
"Every setback is a major setback."

### AI summary (High error rate! Edit errors on video page)

Biden can't directly fire the Postmaster General, but he can appoint people to the board who can.
The board has nine members, with six being Trump appointees and allies of the Postmaster General.
There have been concerns about DeJoy's leadership and ethics prompting calls for his removal.
The Biden administration intends to replace two board members, potentially giving them a majority.
DeJoy's policies have caused delays in Postal Service operations, impacting the supply chain.
The post office plays a significant role in the supply chain by delivering components and parts promptly.
Every setback in the supply chain is critical as businesses strive to operate smoothly.
DeJoy may be on the verge of being ousted as the Biden administration takes action.
Uncertainty remains whether this move is strategic planning or a last-minute decision.
The Postal Service's efficiency is vital in tackling current supply chain challenges.

Actions:

for government officials, postal service employees.,
Contact elected representatives to voice support for Postal Service reforms (implied)
Stay informed about developments regarding DeJoy's potential ousting and its impact on Postal Service operations (implied)
</details>
<details>
<summary>
2021-11-20: Let's talk about a familiar problem and a new tactic.... (<a href="https://youtube.com/watch?v=obvaoJORHYg">watch</a> || <a href="/videos/2021/11/20/Lets_talk_about_a_familiar_problem_and_a_new_tactic">transcript &amp; editable summary</a>)

Beau addresses vaccine hesitancy by debunking misinformation with factual evidence and statistics, urging individuals to embrace the safety and effectiveness of vaccines.

</summary>

"The vaccine is way safer."
"Take the win on this one."
"Our failure to counteract misinformation in the United States leads it to be all over the place."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the concern of a viewer whose parents refuse to get vaccinated, citing concerns of being "poisoned".
He explains that the fear of being poisoned often stems from misinformation about vaccine ingredients.
Beau suggests checking the actual vaccine components to debunk these fears.
He mentions a tactic where some individuals believe the vaccine is riskier than contracting the virus due to misinformation on survivability rates.
Using US data, Beau illustrates that the vaccine is significantly safer than contracting the virus, even with inflated adverse event reports.
Beau delves into the Vaccine Adverse Event Reporting System (VAERS) and its limitations, pointing out that reports to VAERS are voluntary and subject to biases.
He humorously mentions a bizarre VAERS report of an MMR vaccine causing alien abduction and turning someone into the Incredible Hulk.
Beau provides a specific VAERS entry number (0-221-579-1) to showcase the absurdity and unreliability of some reports.
He encourages relying on factual information and statistics to demonstrate the safety and effectiveness of vaccines in reducing transmission and complications.
Beau concludes by urging people to embrace the vaccine as a reliable solution that offers numerous benefits and reduces the burden on healthcare systems.

Actions:

for individuals with vaccine-hesitant family members.,
Verify vaccine ingredients to debunk misinformation (suggested).
Use factual data to showcase vaccine safety (exemplified).
Address doubts by comparing vaccine safety with contracting the virus (implied).
</details>
<details>
<summary>
2021-11-19: Let's talk about Texas and this winter.... (<a href="https://youtube.com/watch?v=p6q4zo0BNjI">watch</a> || <a href="/videos/2021/11/19/Lets_talk_about_Texas_and_this_winter">transcript &amp; editable summary</a>)

Be prepared for winter in Texas: doubts persist on grid readiness despite official assurances, urging proactive measures.

</summary>

"It appears as though they needed to winterize."
"If we see something like we did last year, I think it'll fail again."
"It's much easier to build the arc before the flood."

### AI summary (High error rate! Edit errors on video page)

Recalling the Texas winter crisis from last year when the electrical grid failed and caused significant issues.
A report suggests that winterizing equipment could have reduced power outages by 67%.
The failures occurred in temperatures presumed to be within the plant's tolerance, indicating overestimation of cold weather resistance.
The Texas Railroad Commission, responsible for natural gas, is releasing new winterization rules after winter.
Statements from ERCOT express confidence in addressing electric-related issues but lack specific updates on progress.
Beau doubts the grid's readiness for the upcoming winter, not convinced by the assurances from authorities.
Urges Texans to prepare for winter by stocking up on essentials like firewood and charcoal.
Advises planning for extreme cold scenarios ahead of time.
Beau remains skeptical of the grid's preparedness and encourages individuals to be proactive in readiness.
Emphasizes the importance of being prepared for potential failures, suggesting skepticism towards official promises.

Actions:

for texans,
Stock up on firewood and charcoal for winter preparation (suggested).
Plan ahead for extreme cold scenarios (suggested).
</details>
<details>
<summary>
2021-11-18: Let's talk about nurses and misinformation.... (<a href="https://youtube.com/watch?v=jziu9ujx88o">watch</a> || <a href="/videos/2021/11/18/Lets_talk_about_nurses_and_misinformation">transcript &amp; editable summary</a>)

Nurses celebrate increased accountability for spreading COVID-19 vaccine misinformation, aiming to combat the deadly impact of false information profiteers.

</summary>

"Misinformation is profitable."
"They're tired of fighting the public health issue and they're tired of fighting the misinformation."
"That misinformation is deadly."
"A lot of people have used those letters after their name as a shield."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Nurses received exciting news regarding accountability for spreading misinformation related to COVID-19 vaccines.
A joint statement from various nursing associations emphasized nurses' professional accountability for the information they provide to the public.
Nurses who spread misleading information may face disciplinary action by their board of nursing.
Misinformation can jeopardize public health and endanger nurses' licenses and careers.
The statement aims to combat the profitability of misinformation spread by individuals with credentials.
Nurses, particularly those on the front lines in ER and ICU settings, welcomed this news with relief and satisfaction.
Fighting both the public health crisis and misinformation has left nurses exhausted.
The spread of misinformation by credentialed individuals can have deadly consequences.
Nurses are hopeful that the decline in misinformation from credentialed sources will make it less believable.
Many individuals have used their professional credentials as a shield to profit from spreading misinformation.

Actions:

for nurses, healthcare professionals,
Contact nursing associations for guidance on combating misinformation (suggested)
Support efforts to hold accountable those spreading misleading information (implied)
Stay informed and vigilant against misinformation in healthcare settings (suggested)
</details>
<details>
<summary>
2021-11-18: Let's talk about Muncie Central High and V for Vendetta.... (<a href="https://youtube.com/watch?v=NrHxTU-k47M">watch</a> || <a href="/videos/2021/11/18/Lets_talk_about_Muncie_Central_High_and_V_for_Vendetta">transcript &amp; editable summary</a>)

Teacher assigns V for Vendetta project, students draw parallels with modern movements, establishment suppresses dissent, Beau criticizes lack of critical thinking in education.

</summary>

"You are reinforcing it every step of the way."
"Trying to stop students from thinking critically about what they see and what they hear and their own experiences is indoctrination."
"They see what exists. They see what's around them. And yes, there are parallels. They're not wrong."

### AI summary (High error rate! Edit errors on video page)

Teacher at Muncie Central High School assigned V for Vendetta project, drawing parallels between modern social movements and the plot of the movie.
Posters created by students were put up in the hallway, causing discomfort to the establishment and school resource officers.
Establishment asked teacher to remove the posters, sparking a student-led demonstration and remote learning for students.
Beau suggests watching V for Vendetta to understand the parallels between the movie and the events at the school.
Beau criticizes the establishment for reacting negatively and militarizing the situation, likening them to the antagonists in the movie.
Students are drawing their own conclusions, not being indoctrinated, based on what they see and hear.
The situation at Muncie Central High School mirrors the plot of V for Vendetta, with students being silenced and controlled by the establishment.
Beau encourages the school to acknowledge their mistake and embrace critical thinking rather than trying to suppress it.
Beau warns that attempts to keep students ignorant and prevent critical thinking are happening in school districts across the country.
Suppressing critical thought in education can lead to a society easily misled and manipulated.

Actions:

for students, teachers, activists,
Speak out against attempts to suppress critical thinking in schools (suggested)
Get involved in advocating for education that encourages critical thought (suggested)
</details>
<details>
<summary>
2021-11-17: Let's talk about Christopher Miller handling Trump.... (<a href="https://youtube.com/watch?v=46EZfBHQo1o">watch</a> || <a href="/videos/2021/11/17/Lets_talk_about_Christopher_Miller_handling_Trump">transcript &amp; editable summary</a>)

Beau sheds light on how Christopher Miller handled Trump's decision-making, revealing concerns about potential military use against Americans and the need for caution in supporting certain political figures.

</summary>

"He played the, well, you're about to find out what crazy is game."
"No military coup, no major war, and no troops in the streets."
"It's really weird that this same sentiment has occurred that many times."
"The greatest war machine that the world has ever known."
"That should be pretty concerning."

### AI summary (High error rate! Edit errors on video page)

Provides insight into Christopher Miller, the last acting Secretary of Defense under Trump.
Points out that individuals associated with the Trump administration may be trying to rehabilitate their image.
Describes how Miller handled situations where Trump was on the verge of making disastrous decisions.
Explains Miller's approach in countering what he perceived as lacking judgment from the former president.
Mentions Miller's three goals upon taking over as acting Secretary of Defense.
Indicates that two of Miller's top priorities were aimed at preventing Trump from using the military against Americans.
Notes the recurring sentiment among military officials about anticipating a potential coup during Trump's presidency.
Warns about the dangers of rhetoric that could lead to the military being turned against the people.
Emphasizes the need to be cautious and mindful of the implications of supporting certain political figures.
Urges listeners to be aware of the potential consequences of unchecked behavior from political leaders.

Actions:

for american citizens,
Remain vigilant and critical of political rhetoric that could endanger democratic norms and institutions (implied).
Stay informed about the actions and decisions of political leaders that could impact national security and civil liberties (implied).
</details>
<details>
<summary>
2021-11-17: Let's talk about Birmingham, Biden, and deception.... (<a href="https://youtube.com/watch?v=UGRspj67rmM">watch</a> || <a href="/videos/2021/11/17/Lets_talk_about_Birmingham_Biden_and_deception">transcript &amp; editable summary</a>)

Beau exposes politicians celebrating projects they voted against, revealing the disconnect between their actions and constituents' interests.

</summary>

"They don't represent you. They represent themselves."
"When it comes to the infrastructure package that's gone through, the components that are on the way, the reality is it is great for the economy."
"Could it be better? Sure. But it's unlikely they're going to get this through because of stuff like this."
"Expect to see a lot of this. These packages that the administration is pushing through over Republican opposition, they're good for the country."
"They know it's good for the people of Birmingham, but they will still vote against it at a partisan interest."

### AI summary (High error rate! Edit errors on video page)

Talks about a development in Birmingham, Alabama related to Biden's infrastructure package.
Mentions a proposed belt line project around Birmingham, which is expected to boost economic development and create jobs.
Notes the economic impact of completing the northern belt line could exceed $2 billion in 10 years.
Points out that Gary Palmer, a congressperson representing Alabama's 6th District, released a press statement celebrating the project.
Reveals that Gary Palmer actually voted against the infrastructure package he's praising.
Raises the issue of politicians trying to take credit for projects they opposed.
Suggests that politicians may prioritize partisan interests over the benefits to their constituents.
Emphasizes the disconnect between politicians' actions and the interests of the people they represent.
Encourages people to be aware of politicians who celebrate projects they voted against.
Concludes by calling out politicians for prioritizing their own interests over those of the community.

Actions:

for voters,
Watch out for politicians who celebrate projects they opposed (suggested)
</details>
<details>
<summary>
2021-11-16: Let's talk about civics and autism.... (<a href="https://youtube.com/watch?v=IRsXRFYhqiA">watch</a> || <a href="/videos/2021/11/16/Lets_talk_about_civics_and_autism">transcript &amp; editable summary</a>)

A parent's concern about the disconnect between civics education and reality leads Beau to seek insight on autism, revealing a need for practical application over research in academia.

</summary>

"Nobody knows anything about autism."
"That disconnect is real."
"What it should be and what it is are two very different things."
"A healthy skepticism of political leaders is probably something we need to encourage."
"We're doing such a bad job of keeping those promises."

### AI summary (High error rate! Edit errors on video page)

A parent reached out about their child learning civics in the eighth grade in Florida.
The child is experiencing a disconnect between what they are taught in civics class and what they see in the world.
Beau researched autism before suggesting a framing device to address this disconnect.
Despite hours of talking to experts on autism, Beau found a disconnect between academic research and practical application.
Speaking to autistic adults provided Beau with more insight than speaking to academics.
Beau views the Declaration of Independence as a promise or ideal to strive towards.
He notes that the system may not function as it should due to human imperfections.
There's a concern about the push for patriotic education and mythology in teaching civics.
Beau believes that promoting healthy skepticism towards political leaders is necessary.
He points out the significant gap between the promise of equality and the reality experienced by eighth-graders.

Actions:

for parents, educators, policymakers,
Talk to autistic adults for better insight on autism (suggested)
Encourage healthy skepticism towards political leaders (exemplified)
</details>
<details>
<summary>
2021-11-16: Let's talk about CRT and thanksgiving dinner.... (<a href="https://youtube.com/watch?v=ab-3GERLfao">watch</a> || <a href="/videos/2021/11/16/Lets_talk_about_CRT_and_thanksgiving_dinner">transcript &amp; editable summary</a>)

A teacher tackles Thanksgiving dinner controversy by introducing Critical History Theory, challenging American mythology, and addressing racism subtly.

</summary>

"Most people who are ranting about CRT couldn't define it if their life depended on it."
"The purpose of history is to look back so you don't make the same mistakes."
"It's about the possibility of slowly dismantling power structures that have kept people down."

### AI summary (High error rate! Edit errors on video page)

A teacher wants to prevent her Uncle Bob from ranting about a theory during Thanksgiving dinner.
The theory is critical race theory (CRT), which is often misunderstood by those who oppose it.
Beau suggests creating a new concept, Critical History Theory (CHT), to redirect the conversation.
CHT focuses on examining the intersection of history and law in the US to challenge mainstream approaches to justice.
By removing race from CRT and inserting history, Beau aims to make the concept more palatable to Uncle Bob.
Beau advises starting with historical laws like the National Firearms Act to demonstrate the impact of past legislation.
Connecting past laws to present consequences can help Uncle Bob understand the importance of learning from history.
Beau recommends discussing laws with unintended or negative consequences to illustrate the need for scrutiny and reform.
By introducing the racial aspect of CHT later in the conversation, Beau aims to reveal underlying motivations behind opposition to CRT.
Beau underscores the need to challenge ingrained American mythology that whitewashes history and perpetuates harmful narratives.

Actions:

for teachers, educators, activists,
Introduce Critical History Theory (CHT) in educational settings to challenge mainstream approaches to justice (suggested).
Start dialogues on historical laws and their impacts on society to foster understanding and reform (suggested).
Encourage critical thinking and reflection on past legislation and its consequences (suggested).
</details>
<details>
<summary>
2021-11-15: Let's talk about a new pregnancy complication.... (<a href="https://youtube.com/watch?v=Hz04HPivzLQ">watch</a> || <a href="/videos/2021/11/15/Lets_talk_about_a_new_pregnancy_complication">transcript &amp; editable summary</a>)

Pregnant individuals face higher homicide rates than pregnancy-related issues, leading to a push to include homicide in maternal mortality rates and anticipate unintended consequences from restricted family planning laws.

</summary>

"If you are pregnant or were pregnant in the previous 42 days, you are more likely to be murdered than you are to die of what would typically be seen as a pregnancy related issue."
"The two most dangerous times for women is when they are leaving or when they are pregnant."
"By limiting that type of family planning, they are leaving people exposed, leaving them more vulnerable."

### AI summary (High error rate! Edit errors on video page)

Mentions a study revealing high homicide rates for pregnant individuals compared to traditional pregnancy-related issues.
Previous smaller study results were considered too high due to a small sample size.
New comprehensive study covers all 50 states over two years (2018-2019).
Push for CDC to include homicide in maternal mortality rates due to the alarming statistics.
Risk factors for increased rates include being young and being a black woman.
Experts in intimate partner violence confirm pregnancy as a dangerous time for women.
Expectation that rates will increase due to limitations on family planning access.
Anticipates higher rates in the future due to laws restricting family planning.
Suggests shelters and DV organizations reach out more during pregnancy to provide support.
Warns of unintended consequences for those advocating for restrictive family planning laws.

Actions:

for policy advocates, healthcare workers,
Support shelters and organizations countering DV issues by reaching out during pregnancy (implied).
Advocate for inclusive family planning policies to prevent increased vulnerability (implied).
</details>
<details>
<summary>
2021-11-15: Let's talk about Flynn and comments.... (<a href="https://youtube.com/watch?v=U0GZ0lUL9FY">watch</a> || <a href="/videos/2021/11/15/Lets_talk_about_Flynn_and_comments">transcript &amp; editable summary</a>)

Beau warns against the dangerous rhetoric of pushing for one religion in the nation, urging patriots to stand up against such betrayal of American ideals.

</summary>

"If you consider yourself a patriot, you must vocalize your opposition to this."
"There is no basis in American patriotism when it comes to supporting this garbage."
"They are telling you who they are, and you better believe it."

### AI summary (High error rate! Edit errors on video page)

Flynn made a concerning statement about having one religion in the nation, which goes against American ideals of religious freedom and separation of church and state.
He calls on patriots to vocalize their opposition to such ideas, as they contradict the values enshrined in the US Constitution.
Beau references historical documents like the Treaty of Tripoli and quotes from founding figures to support the separation of religion and government.
He warns that such rhetoric may increase before the midterms as a desperate move by those on a losing streak.
Beau points out that supporting the intertwining of religion and government is a betrayal of the founding principles of the country.

Actions:

for patriots, defenders of american ideals,
Oppose any attempts to intertwine religion and government (implied)
Educate others on the importance of the separation of church and state (implied)
</details>
<details>
<summary>
2021-11-14: Let's talk about not seeing your black friends as black.... (<a href="https://youtube.com/watch?v=0ZqETKLuYkQ">watch</a> || <a href="/videos/2021/11/14/Lets_talk_about_not_seeing_your_black_friends_as_black">transcript &amp; editable summary</a>)

Beau challenges a listener to confront manufactured stereotypes by trusting their own observations about systemic racism.

</summary>

"Your own observations. 100% of the time, this is what it's like."
"Trust your observations when it comes to whether or not it's going to rain."
"The stereotype is manufactured. It's a holdover."
"You know those stereotypes aren't right."
"Your evidence is in your message."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of images, observations, and stereotypes in understanding systemic racism.
Shares a message from someone questioning the existence of systemic racism based on personal experiences.
Challenges the listener to question where their perceptions of race come from.
Points out the influence of media, news, and societal narratives in shaping stereotypes.
Addresses the impact of systemic racism on how black individuals are perceived.
Defines systemic racism as a byproduct of historical oppression and institutionalization.
Urges the listener to trust their observations about the good qualities of black individuals.
Encourages acknowledging and dismantling manufactured stereotypes.
Compares societal stereotypes to personal misconceptions.
Emphasizes the discomfort of confronting societal views on race.

Actions:

for listeners reflecting on personal perceptions.,
Trust your observations about the good qualities of individuals (implied).
Acknowledge and challenge manufactured stereotypes (implied).
</details>
<details>
<summary>
2021-11-14: Let's talk about making the case and ruining Thanksgiving.... (<a href="https://youtube.com/watch?v=DyIWXgJlJrs">watch</a> || <a href="/videos/2021/11/14/Lets_talk_about_making_the_case_and_ruining_Thanksgiving">transcript &amp; editable summary</a>)

A guide to reframing the reparations debate during Thanksgiving dinner to focus on property rights and generational wealth impact.

</summary>

"You're trying to win a moral debate."
"Make a different case. Better yet, get them to make it."
"Maybe it's time we start talking about it at Thanksgiving dinner."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

A woman in her 30s reached out for help ruining Thanksgiving dinner after her uncle condescendingly shut down her previous attempt to bring up reparations at the table.
Beau advises focusing on making a moral argument when trying to win a moral debate, suggesting using indirect approaches to lead the person to acknowledge certain truths.
He recommends using the example of Agent Orange exposure in Vietnam to help Uncle Bob understand the concept of reparations without directly discussing slavery or segregation.
By getting Uncle Bob to recognize the government's responsibility in compensating those impacted by Agent Orange exposure, Beau guides the argument towards discussing how government policies like segregation limited generational wealth.
Beau suggests reframing the argument from a moral standpoint to a property rights case to explain why reparations should be considered for past injustices.
The goal is to guide the debate towards acknowledging the impact of historical injustices on generational wealth without directly confronting moral beliefs.

Actions:

for americans engaging in thanksgiving debates.,
Start open, rational dialogues about sensitive topics like money, politics, and religion at family gatherings (suggested).
Encourage discussing challenging subjects to prevent harm and improve understanding (suggested).
</details>
<details>
<summary>
2021-11-13: Let's talk about income, public health, and Trump.... (<a href="https://youtube.com/watch?v=t_ZSEmO9DIo">watch</a> || <a href="/videos/2021/11/13/Lets_talk_about_income_public_health_and_Trump">transcript &amp; editable summary</a>)

Income influences vaccination rates; transportation barriers affect lower-income families. Trump's ego delayed public health response, costing lives.

</summary>

"Time is lives in organizing a response, especially in public health emergencies."
"His ego costs time, and when you're talking about organizing a response, time is lives."
"The virus doesn't care about the political spin. It never did."
"That needs to be remembered."
"Offering help with transportation could make a difference in vaccination rates."

### AI summary (High error rate! Edit errors on video page)

Income gap affects willingness to vaccinate kids: higher income, quicker vaccination.
Education not likely the primary factor in vaccination discrepancies.
Lower income families face barriers like transportation and work concerns for vaccination.
Offering help with transportation could make a difference in vaccination rates.
Committee reveals Trump administration's delayed response to CDC warnings in February.
CDC's inability to brief due to upsetting Trump caused three months delay in response preparation.
Delay in response preparation potentially cost lives due to ego-driven narrative control.
Time is lives in organizing a response, especially in public health emergencies.
Muzzling experienced individuals hindered effective mitigation efforts.
The importance of remembering the consequences of delayed responses and narrative control in public health crises.

Actions:

for parents, community members,
Offer to provide transportation assistance for families to get their kids vaccinated (exemplified)
Advocate for transparent and timely public health communication (implied)
</details>
<details>
<summary>
2021-11-13: Let's talk about Bannon being indicted and the impacts.... (<a href="https://youtube.com/watch?v=igfjSS0f_T0">watch</a> || <a href="/videos/2021/11/13/Lets_talk_about_Bannon_being_indicted_and_the_impacts">transcript &amp; editable summary</a>)

Bannon's indictment and legal process could impact midterms, with Republican obstruction potentially backfiring.

</summary>

"Do not expect a speedy end to the Steve Bannon saga."
"The longer it drags out, the more likely it is that the revelations come out during the midterms."
"Republican obstruction could backfire in the elections."

### AI summary (High error rate! Edit errors on video page)

Bannon got indicted, may prompt others ignoring subpoenas to reconsider.
Cases from January 6th are now starting to be decided.
Lengthy legal process may impact the midterms.
Republicans might have miscalculated by dragging out the process.
Bannon's actions could affect the midterm elections.
Potential bombshells may arise during the campaigns.
Trump trying to block release of his presidential archives.
Republican obstruction could backfire in the elections.
Expect a prolonged Bannon saga with possible delays.
The longer it drags out, the more likely revelations during midterms.

Actions:

for political analysts, voters,
Stay updated on the legal proceedings involving Bannon and related political events (suggested).
Remain informed about the potential impacts of these events on the upcoming elections (suggested).
</details>
<details>
<summary>
2021-11-12: Let's talk about Remembrance Day in Canada.... (<a href="https://youtube.com/watch?v=6iI7VPlfwjM">watch</a> || <a href="/videos/2021/11/12/Lets_talk_about_Remembrance_Day_in_Canada">transcript &amp; editable summary</a>)

Beau addresses the misuse of Remembrance Day for anti-vaccine beliefs and criticizes the irony of such actions while honoring lives lost during World War I.

</summary>

"If they have asked you for your papers they have already forgotten, this Remembrance Day has more significance than all the ones we've had since World War II."
"I just want to point out that if you took this moment to stage this little stunt, you're wearing a poppy that symbolizes a whole lot of people who were lost because of great patriots just like you."

### AI summary (High error rate! Edit errors on video page)

Addressing Remembrance Day in Canada and a group using it to express anti-vaccine beliefs.
Comparing Remembrance Day to Memorial Day in the United States.
Explaining the origins of Remembrance Day from Armistice Day after World War I.
Connecting the significance of wearing the poppy flower to those lost during World War I.
Sharing historical insights on the Spanish flu during World War I and its impact on battle plans.
Mentioning the different theories about the origin of the Spanish flu, including its link to World War I trenches.
Describing how the flu changed battle plans and was downplayed by the media due to patriotism.
Criticizing the misinformation spread about the flu during the war and its consequences.
Questioning the accuracy of reported flu-related deaths, particularly in Canada.
Calling out the irony of using Remembrance Day to express anti-vaccine beliefs while wearing a poppy symbolizing lost lives.

Actions:

for canadians, history enthusiasts,
Respect the significance of Remembrance Day and honor the sacrifices made by wearing a poppy (exemplified).
</details>
<details>
<summary>
2021-11-12: Let's talk about Ahmaud Arbery and intimidation..... (<a href="https://youtube.com/watch?v=u6ddiXdtNYY">watch</a> || <a href="/videos/2021/11/12/Lets_talk_about_Ahmaud_Arbery_and_intimidation">transcript &amp; editable summary</a>)

Beau challenges the defense attorney's claim of Reverend Al Sharpton being intimidating in the Ahmaud Arbery case, shedding light on the broader issue of racial perceptions and systemic biases affecting interactions with law enforcement.

</summary>

"There is nobody sitting on that jury right now who is unaware of the fact that national eyes are on that case."
"What exactly about Reverend Sharpton is intimidating?"
"For whatever reason, the skin tone alone is enough."
"Nobody is going to look at Reverend Sharpton and be intimidated. It's a fiction."
"A pastor who's almost 70 years old. Still intimidating."

### AI summary (High error rate! Edit errors on video page)

Commentary on the Ahmaud Arbery case and defense attorney's controversial statements.
Defense attorney took issue with Reverend Al Sharpton sitting with the family.
Attorney expressed not wanting any more black pastors involved.
Focus on the attorney's statement about not wanting Reverend Sharpton there.
Beau questions what exactly about Reverend Sharpton is intimidating.
Raises the point that the jury is aware of the national attention on the case.
Beau challenges the notion of Reverend Sharpton being intimidating due to his age and profile.
Questions if a different religious figure's presence, like Pat Robertson, would be deemed intimidating.
Disputes the defense attorney's assertion that Sharpton's presence is intimidating.
Beau connects this to the broader issue of how skin tone alone can be perceived as a threat.
Emphasizes the significance of discussing how skin tone can influence interactions with law enforcement.
Concludes by encouraging reflection on the implications of deeming a 70-year-old pastor intimidating based on race.

Actions:

for activists, justice seekers, advocates,
Challenge and confront instances of racial bias and intimidation in your community (implied).
Support individuals who face discrimination and intimidation based on their race (implied).
</details>
<details>
<summary>
2021-11-11: Let's talk about the other 14 characteristics... (<a href="https://youtube.com/watch?v=yEDzkZ1xwmI">watch</a> || <a href="/videos/2021/11/11/Lets_talk_about_the_other_14_characteristics">transcript &amp; editable summary</a>)

Beau explains 14 characteristics of a certain ideology, pointing out the dangerous patterns present in modern politics.

</summary>

"Contempt for the weak. People that constantly kick down."
"Everybody's a hero. There's a culture surrounding heroism, even if it's not really heroic."
"Pacifism is the enemy, and the reason this is the case is because life has to be a struggle."

### AI summary (High error rate! Edit errors on video page)

Explains 14 characteristics of a certain ideology, comparing Lawrence Britt's and Umberto Eco's work.
Cult of tradition is about appealing to the past for greatness, rejecting modernism.
Action for action's sake involves impulsive decisions for appearance over effectiveness.
Disagreement is considered treason, fear of difference is used to unite against scapegoats.
Obsession with plots and viewing enemies as both too strong and too weak.
Pacifism is seen as the enemy, life must be a constant struggle.
Contempt for the weak and hero culture where everyone is deemed heroic.
Machismo and weaponry, selective populism, and favoring the uneducated are key characteristics.

Actions:

for political analysts, activists,
Educate others on recognizing and understanding the 14 characteristics of a concerning ideology (implied).
</details>
<details>
<summary>
2021-11-11: Let's talk about hot books in Virginia.... (<a href="https://youtube.com/watch?v=hkpUgHoDjog">watch</a> || <a href="/videos/2021/11/11/Lets_talk_about_hot_books_in_Virginia">transcript &amp; editable summary</a>)

In Spotsylvania, Virginia, officials suggesting book burning prompts urgent call to action to prevent a dangerous slide into authoritarian practices nationwide.

</summary>

"We have moved on to the literal book burning portion of the show."
"The 14 characteristics that exemplify the worst regimes in history are being parroted in the United States today."
"This is, I'm hoping that this becomes a national story."
"If nothing up until now has served as the wake-up call that you need, this should be it."
"The phone's ringing."

### AI summary (High error rate! Edit errors on video page)

Addressing the removal of school books in Virginia, specifically in Spotsylvania, where officials proposed burning books.
School board members in Spotsylvania suggested throwing books into a fire to eradicate "bad stuff" and wanted to display the books before burning them.
Beau questions the logistics behind burning books in the community and how they plan to demonstrate this process.
Noting that representatives from Cortland and Livingston are advocating for literal book burning.
Expresses concern for individuals swayed by fear-mongering tactics, pointing out the danger of following a similar path to oppressive regimes.
Observes a rapid progression in the adoption of characteristics seen in historical authoritarian regimes within the United States political landscape.
Emphasizes the urgency of recognizing the alarming direction the country is moving towards, with government officials discussing burning controversial books.
Urges for national attention on the situation in Spotsylvania, hoping it serves as a wake-up call for people to take action and strive for a better future.

Actions:

for community members, activists, educators,
Raise awareness about the situation in Spotsylvania, Virginia by sharing information on social media and engaging in community dialogues (suggested)
Reach out to local news outlets and media platforms to encourage coverage of the book burning proposal in Spotsylvania (implied)
</details>
<details>
<summary>
2021-11-10: Let's talk about a Malcolm X quote.... (<a href="https://youtube.com/watch?v=3mEYPKSFZiA">watch</a> || <a href="/videos/2021/11/10/Lets_talk_about_a_Malcolm_X_quote">transcript &amp; editable summary</a>)

Beau dives into Malcolm X's quote on white liberals, clarifying its true meaning and advocating for black empowerment while debunking misconceptions.

</summary>

"White liberals, well, they were foxes. White conservatives were wolves."
"He was trying to advocate for black Americans to find their own source of power."
"Whether it came from a wolf or a fox. But the fox is more likely to make promises they don't intend to keep."
"Malcolm X today would be a right wing pro-gun conservative. Well, I mean, one out of three, I guess, isn't too bad."
"That's what he was talking about."

### AI summary (High error rate! Edit errors on video page)

Exploring a famous quote by Malcolm X about white liberals, often misrepresented and misunderstood.
Malcolm X's warning about white liberals and their potential danger, compared to white conservatives.
Malcolm X's emphasis on not falling prey to any political party and advocating for black Americans to find their own source of power.
The historical context of Malcolm X's statements in relation to the Southern strategy and political power dynamics.
Clarifying the misconception that Malcolm X's quote endorsed the Republican Party or right-wing ideologies.
Beau's use of the term "white liberal" in contemporary contexts, pointing out the manipulation and empty promises made by politicians.

Actions:

for activists, political historians,
Reach out to black creators on YouTube for impassioned explanations of Malcolm X's quotes (suggested)
Advocate for black empowerment and community building (implied)
</details>
<details>
<summary>
2021-11-10: Let's talk about Gohmert and the Texas attorney general's race.... (<a href="https://youtube.com/watch?v=e4cM6mkEfT8">watch</a> || <a href="/videos/2021/11/10/Lets_talk_about_Gohmert_and_the_Texas_attorney_general_s_race">transcript &amp; editable summary</a>)

Congressman Gohmert's entry into the Texas Attorney General race raises concerns about potential implications, underscoring the need for vigilance in maintaining election integrity.

</summary>

"They learned nothing."
"These fraud claims will continue to be echoed in any election they lose."
"Putting somebody like Gohmert in that position would allow them to greatly alter the outcome of an election."

### AI summary (High error rate! Edit errors on video page)

Congressman Louie Gohmert has entered the Texas Attorney General race, forming an exploratory committee.
Gohmert's main talking point is the potential indictment of current attorney general, Ken Paxton.
There's a website, gomert.net, where you can find more information.
Texas law states that if someone wins a primary and is indicted after, their name stays on the ballot.
Gohmert is seeking donations, aiming for $1 million by November 19th from 100,000 citizens.
Concerns are raised about the potential implications of having a Democrat as Attorney General in Texas.
Gohmert's entry into the race is seen as a move to interject into the election and push back against fraud claims.
The transcript underscores the need to take this election seriously despite the humor involved.
Potential consequences of putting Gohmert in such a position are discussed.
The importance of maintaining free and fair elections in a representative democracy is emphasized.

Actions:

for texans, voters,
Visit gomert.net to gather more information and stay informed (suggested).
Donate to support Gohmert's exploratory committee if you choose to do so (implied).
</details>
<details>
<summary>
2021-11-09: Let's talk about society, laws, and two phrases.... (<a href="https://youtube.com/watch?v=v4a9QAN-kTE">watch</a> || <a href="/videos/2021/11/09/Lets_talk_about_society_laws_and_two_phrases">transcript &amp; editable summary</a>)

Societal change originates from shifting mindsets, not legislative actions; laws enforce, not drive, transformation.

</summary>

"Societal change does not occur in a statute book, it occurs inside the four-inch space inside your skull."
"Law does not create societal change. Thought does."
"Before they're ever going to get a law, society has to change."

### AI summary (High error rate! Edit errors on video page)

Society changes through altering how people think, not just through laws.
In a representative democracy, representatives should vote according to the majority of their district's wishes.
Laws don't lead societal change; they enforce changes that have already occurred in society.
Money often influences representatives' decisions, leading to a higher threshold for societal change.
Society's shift in thought precedes legal changes, as seen in historical examples like integration.
The narrative that laws drive societal change is not accurate; societal change primarily stems from shifts in collective thinking.
Beau contrasts the perception of laws as change agents with examples like the ongoing war on drugs.
Politicians must advocate for societal shifts before laws can effectively bring about change.
The battleground for societal change lies in influencing individuals' beliefs.
Laws without societal support are ineffective, as evidenced by examples like prohibition and the war on drugs.

Actions:

for activists, policymakers, citizens,
Advocate for societal change by engaging in meaningful dialogues and challenging existing beliefs (implied).
Support politicians who prioritize representing the majority will of their constituents (implied).
Educate others on the importance of influencing societal thought for effective change (implied).
</details>
<details>
<summary>
2021-11-09: Let's talk about Little Rock, history, and making the case.... (<a href="https://youtube.com/watch?v=dNtVVI-7Z8I">watch</a> || <a href="/videos/2021/11/09/Lets_talk_about_Little_Rock_history_and_making_the_case">transcript &amp; editable summary</a>)

Beau stresses the importance of teaching historical context, community action, and moral advocacy to avoid repeating past mistakes and drive meaningful change.

</summary>

"We have to teach the why. Why things happened."
"Community networks work. It does show that community networks work."
"When you're talking about topics that are getting viewed as woke today, they're not political issues. They're moral ones."
"When history is written, it doesn't record that the governor was a moderate in 1955."
"You have to make the case. You have to be organized."

### AI summary (High error rate! Edit errors on video page)

History education should encourage asking "why" and exploring "what ifs" to understand context better.
Teaching history through timelines may lead to losing valuable context.
Imagining a different timeline where a small town in Arkansas integrated schools smoothly before Little Rock.
Integration happened smoothly in the hypothetical town because the case for it was made effectively.
The opposition to integration, like segregationists, follows a similar playbook across different timelines.
In the hypothetical scenario, the Attorney General steps in to ensure integration proceeds smoothly.
The potential political implications of successful integration at a local level on state politicians.
Emphasizing the importance of teaching historical context alongside significant events.
The power of community networks in influencing political views, whether positively or negatively.
Questioning why the Civil Rights Act took almost a decade to pass despite public support.
Criticizing white progressives and liberals for not actively supporting Civil Rights causes due to political expediency.
Drawing parallels between historical inaction and current political shifts influenced by vocal minorities.
Stressing the moral importance of standing up for issues deemed "woke" and making a case for them.
Warning against shifting stances for political expediency and the importance of converting beliefs into action.
Encouraging organization and action to support causes that require attention and not just political convenience.

Actions:

for history enthusiasts, educators, activists.,
Teach historical context effectively in education (implied).
Engage in community action to influence political views positively or counteract negative influences (implied).
Advocate for moral causes and make a compelling case for them (implied).
Organize and take action to support critical social issues, not just for political expediency (implied).
</details>
<details>
<summary>
2021-11-08: Let's talk about a message from a small government conservative.... (<a href="https://youtube.com/watch?v=Re2-E1cyEwg">watch</a> || <a href="/videos/2021/11/08/Lets_talk_about_a_message_from_a_small_government_conservative">transcript &amp; editable summary</a>)

A small government conservative Republican feels disconnected from the current Republican Party and finds alignment with Democratic values, focusing on community responsibility.

</summary>

"I have never asked myself, what would a Republican do next? I've asked myself, what would a fascist do next?"
"The reason you feel disconnected is because they're not small government conservatives anymore."
"Small government conservatives are more in alignment with the Democratic Party."
"Republican big government now is about curtailing individual liberty."
"You have to accept that responsibility. I got it."

### AI summary (High error rate! Edit errors on video page)

A small government conservative Republican is feeling disconnected from the Republican Party.
The Republican Party is no longer for small government conservatives, but rather nationalists.
Small government conservatives are now more in alignment with the Democratic Party.
Being a small government conservative means accepting individual responsibility for the community.
Beau suggests reading works by Lawrence Britt and Umberto Eco to understand political strategies.
Beau predicts political moves based on what a fascist might do, not what a Republican might do.
The essence of being a small government conservative is allowing others their freedoms as long as they don't harm anyone.
Republican big government now focuses on limiting individual liberty, contrasting with Democrats who aim to uplift working-class people.
The disconnect felt by small government conservatives stems from the shift in the Republican Party's values.
Beau encourages the small government conservative to focus on community and individual responsibility.

Actions:

for small government conservatives,
Read works by Lawrence Britt and Umberto Eco to understand political strategies (suggested).
Focus on community and individual responsibility (implied).
</details>
<details>
<summary>
2021-11-08: Let's talk about Big Bird, Ted Cruz, and an opening.... (<a href="https://youtube.com/watch?v=lJO8jacO8XM">watch</a> || <a href="/videos/2021/11/08/Lets_talk_about_Big_Bird_Ted_Cruz_and_an_opening">transcript &amp; editable summary</a>)

Republicans hide far right-wing beliefs by rebranding, using Big Bird's vaccination as a political issue while Democrats have an opening to question inconsistencies.

</summary>

"They have to hide that. If they say who they are, if they're honest about who they are and where they stand on things, well, it's not electable, so they have to twist it."
"When you are talking about a new variation of Republican that is outside of the confines of what we traditionally expect to find, the moral battlefield is the front line."
"I'm willing to bet that most suburban moms aren't going to be cool with them calling Big Bird a communist."

### AI summary (High error rate! Edit errors on video page)

Republicans trying to reframe themselves as more favorable to suburban voters by concealing their far right-wing beliefs.
Recasting opposition to public health measures as parental rights.
Need to maintain support from the MAGA base while appealing to suburban voters.
Right wing outrage over Big Bird getting vaccinated.
Accusations of Big Bird being a communist and government propaganda.
Republicans speaking out of both sides of their mouth to cater to different factions.
Use of Big Bird getting vaccinated as a political issue to mask true intentions.
Republicans concealing their true beliefs and goals, presenting a false image.
Inconsistencies emerging in their messaging due to hiding their far right-wing stance.
Democrats have an opening to go on the offensive by questioning these inconsistencies.

Actions:

for voters, democrats,
Question Republican candidates on their stance regarding the politicization of Big Bird's vaccination (implied).
</details>
<details>
<summary>
2021-11-07: Let's talk about an internal Democratic party debate.... (<a href="https://youtube.com/watch?v=g8WWNvq3yu8">watch</a> || <a href="/videos/2021/11/07/Lets_talk_about_an_internal_Democratic_party_debate">transcript &amp; editable summary</a>)

Democratic strategists debate whether the party should prioritize winning elections over advocating for marginalized groups, while Beau stresses the importance of fighting for basic dignity and acceptance for all Americans.

</summary>

"If you want to cast yourself as the left-wing party in the United States, at least pretend on some level to be the slightest bit socially progressive or I'm sorry woke."
"The idea of just abandoning entire groups of people because it's too hard is why the Democratic Party has the reputation it has."
"If you want to keep fighting for the 66.1%, go right ahead. But I'm willing to bet it would be easier to get that 33.9."
"Nobody's making a case for them. Nobody's reaching out to them."
"Your four human rights."

### AI summary (High error rate! Edit errors on video page)

Democratic strategists debate whether the party should be "woke" to win elections or if it's costing them votes.
Being "woke" signifies advocating for basic dignity and acceptance for all Americans.
The real issue is whether it's politically expedient to leave marginalized groups behind to win elections.
Beau questions the integrity of those in power who prioritize winning over fighting for marginalized communities.
In the 2020 election, 66.1% of eligible voters cast their ballots, leaving 33.9% who did not vote due to various reasons.
Beau urges focusing on engaging the 33.9% of alienated non-voters rather than solely targeting the 66.1% who voted.
He stresses the importance of fighting for marginalized groups and making a compelling case for their rights.
Beau criticizes the Democratic Party for potentially alienating socially progressive individuals by not advocating strongly for marginalized communities.
He suggests that abandoning challenging issues because they are difficult reinforces negative perceptions of the Democratic Party.
Beau calls for genuine advocacy, especially on issues like clean energy, to inspire and attract voters genuinely.

Actions:

for political activists and democratic party members,
Reach out to the 33.9% of alienated non-voters and make a compelling case for their participation (implied).
Advocate for marginalized groups, including making a case for reparations and trans rights (implied).
</details>
<details>
<summary>
2021-11-07: Let's talk about an America First supply chain.... (<a href="https://youtube.com/watch?v=iOO_IdKArNI">watch</a> || <a href="/videos/2021/11/07/Lets_talk_about_an_America_First_supply_chain">transcript &amp; editable summary</a>)

Beau explains the flaws in an America-first supply chain proposal amid global disruptions, stressing the importance of decentralization for security.

</summary>

"Our supply chain is not broken. It has not snapped."
"An America-first supply chain is not going to solve the problem. It's just a talking point by politicians."
"Localizing it within the United States wouldn't stop that. It's a made-up talking point."
"It's politicians preying on the ignorance of their base."
"Things are normally more secure if they are decentralized."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of the supply chain, which is the route from production to consumption turning raw materials into finished products and delivered to their final destination.
Addresses the stress on the supply chain due to a worldwide public health issue that resulted in disruptions and shortages in some places.
Critiques the proposed solution of an America-first supply chain, arguing that it wouldn't solve the underlying issues caused by global disruptions.
Points out that a centralized supply chain within the United States could make it more vulnerable to various disruptions like blizzards, floods, or hurricanes.
Challenges the notion that localizing production solely in the United States is a feasible or effective solution, as it could weaken the system instead of strengthening it.
Compares the vulnerability of a centralized supply chain to military strategies of cutting off supply routes and explains how centralization can make it easier to disrupt.
Emphasizes the importance of a global supply chain that has been in place for a long time and how localizing it within the U.S. wouldn't address the current disruptions.
Concludes by stating that decentralization typically leads to more security and questions the validity of an America-first supply chain as a solution to the current challenges.

Actions:

for policy makers, supply chain managers,
Challenge proposed policies promoting centralized supply chains (implied)
</details>
<details>
<summary>
2021-11-06: Let's talk about what's in the infrastructure package and the squad's vote... (<a href="https://youtube.com/watch?v=mL3Vv0I8M6Y">watch</a> || <a href="/videos/2021/11/06/Lets_talk_about_what_s_in_the_infrastructure_package_and_the_squad_s_vote">transcript &amp; editable summary</a>)

Beau breaks down the infrastructure bill's allocations and explains the squad's strategic voting against it to ensure broader infrastructure needs are met.

</summary>

"It's good, but not great."
"Them voting against it is just honoring that promise."
"Republicans crossed the aisle to make up their votes."
"They sent a message to Pelosi saying hey if we say we're gonna vote against it we will."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Overview of the infrastructure bill and where the money is going.
The White House estimates the bill will create 2 million jobs.
Detailed breakdown of funding allocations, such as $39 billion for public transit and $65 billion for internet infrastructure.
Noted investments in removing lead pipes, electric charging stations, and increasing electric grid resiliency.
$110 billion earmarked for roads and bridges, the largest investment in bridges since the national highway system inception.
The bill is considered a good start but not sufficient for the country's full infrastructure needs.
The progressive members known as "the squad" voted against the bill to ensure social and climate infrastructure were addressed simultaneously.
Republicans crossed the aisle to vote for the bill, some out of necessity and others to undermine the squad's leverage.
Beau sees the squad's decision to vote against the bill as maintaining their promise and sending a message without causing harm.
There are more components of the infrastructure package that the Biden administration aims to pass.

Actions:

for policy enthusiasts, activists, voters,
Contact your representatives to advocate for comprehensive infrastructure bills that address social and climate needs (implied).
</details>
<details>
<summary>
2021-11-06: Let's talk about a message to conservative parents.... (<a href="https://youtube.com/watch?v=Zjs95ewhkjY">watch</a> || <a href="/videos/2021/11/06/Lets_talk_about_a_message_to_conservative_parents">transcript &amp; editable summary</a>)

Conservative parents are urged to understand the ideological shifts in their children, who learned their values from them and now embody idealism to create a better world.

</summary>

"Your kids, the ones that you're now calling a socialist, the ones you're alienating, where did they get the ideas? They got them from you."
"The job of every parent is to make their child better than they are. They succeeded in that."
"Don't let them down. Be better than they are. Fix the world."

### AI summary (High error rate! Edit errors on video page)

Conservative parents are receiving messages from their children who are feeling lost and conflicted about how to interact with them.
The children express disillusionment with the hypocrisy they see in adults who preach moral values but don't practice them.
Despite being raised Catholic, the children have distanced themselves from the church due to perceived hypocrisy.
They strive to live by the values they were taught, including supporting progressive ideals and helping the disenfranchised.
The children express frustration at being labeled as idealists and socialists by their parents for wanting to create positive change.
The father mentioned in the message is portrayed as someone who has changed ideologically, becoming more dismissive of progressive ideals.
The children struggle to communicate with their parents, especially when there are fundamental disagreements on social and political issues.
Beau encourages conservative parents to understand that their children learned these values from them and urges them not to give up on fostering idealism.
He acknowledges the importance of younger generations embracing idealism and striving for a better world, despite the challenges they face.
Beau reminds the children that they have the power to shape a more equitable and compassionate society by upholding the values instilled in them.

Actions:

for parents, youth,
Have open and honest dialogues with your parents about your beliefs and values, encouraging mutual understanding (implied).
Uphold the values of compassion, progressiveness, and empathy in your daily interactions and support efforts that aim to create positive change (implied).
Embrace idealism and continue striving for a better world, even in the face of opposition or disagreement (implied).
</details>
<details>
<summary>
2021-11-05: Let's talk about Texas, schoolbooks, and the Attorney General's race.... (<a href="https://youtube.com/watch?v=J0_ojZ0QIiQ">watch</a> || <a href="/videos/2021/11/05/Lets_talk_about_Texas_schoolbooks_and_the_Attorney_General_s_race">transcript &amp; editable summary</a>)

A Texas state representative's inquiry into 850 books on race and gender raises questions of political motives, while Beau recommends meeting an impressive Democrat candidate for Attorney General.

</summary>

"Banned books are the best books and they will certainly be available at your public library."
"This honestly seems like a political move designed to appeal to people who can't even teach their children to bigot right."
"If you really think about it, who would be behind this move? The only people I can think of that would support this would be bigots who also happen to be bad parents."
"It's about getting name recognition."
"You know the funny thing is, while I can't remember his name, I happen to have run into somebody also running for Attorney General in Texas."

### AI summary (High error rate! Edit errors on video page)

A state representative in Texas sent letters to schools about 850 books concerning race and gender, expressing concerns about potential psychological distress for students. The list includes titles such as "What is White Privilege?" and "How to Be an Anti-Racist."
The move appears politically motivated to gain attention and possibly cater to a narrow demographic of bigots who may also be bad parents.
Beau questions the intention behind the representative's actions, suggesting it's more about name recognition and creating buzz rather than genuine policy change.
He mentions a Democrat candidate for Attorney General in Texas, Lee Merritt, who he found impressive and recommends meeting at a scheduled event in Austin on November 10th.
Beau criticizes the potential book removal, stating that banned books are often the best and can still be accessed through public libraries.
Overall, Beau's commentary exposes the political motives behind the book inquiry and advocates for embracing diverse perspectives rather than censorship.

Actions:

for texans, voters,
Attend Lee Merritt's meet-and-greet in Austin on November 10th to get to know a candidate who may not support the book removal (suggested).
Embrace diverse perspectives by accessing banned books through public libraries if they are removed from schools (implied).
</details>
<details>
<summary>
2021-11-05: Let's talk about Biden, gas prices, and inflation.... (<a href="https://youtube.com/watch?v=-_l2ltcOQhU">watch</a> || <a href="/videos/2021/11/05/Lets_talk_about_Biden_gas_prices_and_inflation">transcript &amp; editable summary</a>)

Beau criticizes media for inflaming rather than informing about inflation, advocates for transitioning to renewable energy to reduce inflation spikes and benefit the environment and personal finances.

</summary>

"It should be noted that global food prices are up."
"If we switch to solar, wind power, any of the other forms, you're plugging your car in to charge it."
"Not just is it better for the environment, not just will it help reduce the effects of climate change, it's better for your pocket."

### AI summary (High error rate! Edit errors on video page)

Talks about inflation in the economy and how it is still a concern despite good news.
Criticizes the media for choosing to inflame rather than inform about inflation.
Mentions that global food prices are up, making inflation a global issue.
Points out that gas prices are impacted by OPEC not increasing production as fast as demand.
Suggests that transitioning to renewable energy in the U.S. could help reduce inflation spikes.
Emphasizes the need to stop relying on oil and transition to solar, wind power, or other renewable forms of energy.
States that switching to renewable energy is not just better for the environment but also for reducing costs.
Points out that infrastructure programs can accelerate the transition to renewable energy.
Criticizes those reluctant to switch to renewable energy as engaging in a self-defeating attitude.
Concludes by stating that transitioning to renewable energy is not just beneficial for the environment but also for personal finances.

Actions:

for american citizens,
Transition to renewable energy sources like solar or wind power (suggested)
Support infrastructure programs that accelerate the transition to renewable energy (implied)
</details>
<details>
<summary>
2021-11-04: Let's talk about what Democrats can learn from the election results.... (<a href="https://youtube.com/watch?v=0mfLNp8ceWI">watch</a> || <a href="/videos/2021/11/04/Lets_talk_about_what_Democrats_can_learn_from_the_election_results">transcript &amp; editable summary</a>)

Beau gives insights into election strategies: Republicans rely on fear-mongering, while Democrats need inspiring progressives to energize urban voters.

</summary>

"Republicans are scared, we got that, we understand, but we can't become paralyzed by fear."
"If you want the more left groups of that coalition to show up, you have to give them something to show up for."
"Fear-mongering, scary, be afraid. You know what would be a good counter to that? a bunch of young, inspiring, energetic people."
"Those can translate into larger winds later."
"Democrats need to figure out how to energize and mobilize urban voters, their key base."

### AI summary (High error rate! Edit errors on video page)

Analyzing the recent election and looking ahead to 2022 and 2024.
Republicans' strategy involves fear-mongering to scare people, a tactic that has worked.
Centrist Democrats trying to distance themselves from Trump won't be effective.
Democrats need to figure out how to energize and mobilize urban voters, their key base.
Progressive candidates with energy and inspiration are key to winning in places like Ohio and Florida.
Examples from Massachusetts and Florida show the importance of progressive platforms and energetic candidates.
Democrats need to understand that they are a coalition party with varying ideological groups.
Giving left-leaning groups something to show up for is vital.
Countering Republican fear-mongering requires energetic and inspiring individuals.
Simply being anti-Trump won't be a winning strategy for Democrats in upcoming elections.
Democrats should pay attention to smaller victories in strategic locations as they can lead to larger wins.
Building a coalition with inspiring leaders is key to moving forward successfully.

Actions:

for political activists, progressive organizers,
Support and campaign for young, inspiring, and energetic progressive candidates in your community (implied).
Engage with left-leaning groups and provide them with reasons to show up for political events and campaigns (implied).
Pay attention to smaller victories in strategic locations and understand their potential impact on larger elections (implied).
</details>
<details>
<summary>
2021-11-04: Let's talk about how Republicans plan to win in 2022 and 2024.... (<a href="https://youtube.com/watch?v=zYCEPOoR7bs">watch</a> || <a href="/videos/2021/11/04/Lets_talk_about_how_Republicans_plan_to_win_in_2022_and_2024">transcript &amp; editable summary</a>)

Beau dives into the Republican Party's strategy for 2022 and 2024, focusing on reframing rhetoric to appeal to suburban voters while cautioning against the normalization of divisive tactics.

</summary>

"You can't allow the Republican Party to use this strategy successfully."
"We're in a point in time where we can't allow people who don't want a representative democracy to control the narrative."
"This is the moment where the average person has to get involved in the conversation."

### AI summary (High error rate! Edit errors on video page)

Exploring the Republican Party's likely strategy for 2022 and 2024, post-Virginia election.
Republicans aiming to maintain their base while appealing to suburban voters.
Reframing Trump's rhetoric to suit suburban crowds without alienating the MAGA base.
Strategy involves reframing issues like public health, education, and immigration.
Use of moral panic triggers, focusing on issues like kids' vaccinations and border security.
Plans to reframe xenophobia as a focus on increased border security and addressing the supply chain.
Identifies potential targets for blame and division, such as trans people.
Urges the need for Democrats to challenge these reframed narratives.
Warns that if the economy falters, Republicans will heavily lean into that for electoral advantage.
Emphasizes the importance of countering emerging talking points on social media to prevent the normalization of divisive rhetoric.
Calls for inspiring leadership from the Democratic Party to counter the fear-based strategy of the Republicans.
Urges active engagement from ordinary people to prevent the success of the Republican strategy.

Actions:

for social media users,
Counter emerging Republican talking points on social media (implied)
</details>
<details>
<summary>
2021-11-03: The roads to understanding misinformation.... (<a href="https://youtube.com/watch?v=ICpy7urM6Lo">watch</a> || <a href="/videos/2021/11/03/The_roads_to_understanding_misinformation">transcript &amp; editable summary</a>)

Beau explains manipulative tactics used in media to influence audiences and warns against emotionally charged headlines creating false narratives.

</summary>

"Headlines, in theory, should pique your curiosity. They shouldn't encourage you to form an opinion right away."
"A headline should inform you, not inflame you."
"If every single day this news entity has a new thing for you to be angry about, you need to be on guard."

### AI summary (High error rate! Edit errors on video page)

Analyzes tactics used by commentators, pundits, and news outlets to manipulate audiences online.
Advises viewers to be wary of emotionally manipulative headlines.
Explains how headlines play into confirmation bias by appealing to different political affiliations.
Points out techniques like inserting politically divisive names, asking leading questions, and using quotes as headlines to shape narratives.
Talks about personifying organizations or individuals in headlines to influence perceptions.
Advises on being critical of polls and understanding how statistics can be misleading.
Warns about false links leading to misinformation and using images out of context to incite outrage.
Discloses methods like factual yet misleading statements and intentionally unclear communication.
Describes assigning intent as a tactic used to manipulate audiences by creating false narratives.
Encourages viewers to be vigilant, especially about headlines that provoke outrage daily.

Actions:

for media consumers,
Fact-check sources before sharing information (implied)
Verify statistics and polling methodologies (implied)
Reverse image search to verify the context of images (implied)
Be vigilant against emotionally manipulative headlines (implied)
</details>
<details>
<summary>
2021-11-03: Let's talk about what Republicans believe makes America great.... (<a href="https://youtube.com/watch?v=ymqWuIMcilM">watch</a> || <a href="/videos/2021/11/03/Lets_talk_about_what_Republicans_believe_makes_America_great">transcript &amp; editable summary</a>)

Exploring the Republican Party's shift from its historical principles, urging a return to core values, and stressing America's strength through diversity.

</summary>

"Anyone from any corner of the earth can come to live in America and become an American."
"If we ever closed the door to new Americans, our leadership in the world
would soon be lost."
"You got suckered in by slogans, basic nationalism, a stupid red hat and a worst slogan."
"Two plus two equals five. Y'all love to use the term Orwellian. That's pretty Orwellian."
"It wasn't long after a certain recent president shut down our borders, stopped taking new Americans, villainized them, that we lost our position of leadership in the world."

### AI summary (High error rate! Edit errors on video page)

Exploring the evolution of the Republican Party from its historical principles to its current beliefs.
Sharing a powerful message about what makes America great: the ability for anyone to come and become American.
Emphasizing the importance of America's diversity and how it fuels the nation's progress.
Quoting Ronald Reagan on the significance of continuously welcoming new Americans for America's leadership.
Criticizing the shift in the Republican Party towards slogans, nationalism, and denial of facts.
Urging the Republican Party to revisit its core values and ideals.
Noting the repercussions of closing borders and ceasing to welcome new Americans on America's global leadership.

Actions:

for american voters,
Conduct soul-searching within the Republican Party to realign with core values (suggested)
Embrace diversity and welcome new Americans to enrich the nation (implied)
</details>
<details>
<summary>
2021-11-02: Let's talk about the FEC's worst decision since their last one.... (<a href="https://youtube.com/watch?v=IVPYOn-NaTY">watch</a> || <a href="/videos/2021/11/02/Lets_talk_about_the_FEC_s_worst_decision_since_their_last_one">transcript &amp; editable summary</a>)

The FEC's decision allowing foreign funding of referendums raises concerns about corruption and foreign influence in US politics.

</summary>

"This is just another way to obtain favor."
"From a counterintelligence standpoint, this is a nightmare."
"Yet another avenue to funnel money, to encourage corruption."

### AI summary (High error rate! Edit errors on video page)

The Federal Election Commission made a troubling decision allowing foreign entities to fund referendums in the United States, raising concerns about foreign influence.
There are worries about political figures acting as agents for foreign intelligence services and the implications of this decision.
This decision opens up avenues for obtaining favor through referendums, potentially influencing election outcomes.
Beau points out the potential strategy of using referendums to mobilize specific voting blocs, like churchgoers, to support certain political candidates.
From a counterintelligence perspective, this decision is a nightmare due to the risks of corruption and external influence.
Beau expresses concern about the increased potential for corruption at both federal and local levels because of this decision.
States have the authority to enact their laws regarding foreign funding of referendums, which could help prevent further corruption.
Beau suggests considering proposing ballot initiatives or referendums at the state level to counteract the FEC's decision.

Actions:

for voters, activists, legislators,
Propose a ballot initiative or referendum at the state level to counteract the FEC's decision (suggested).
</details>
<details>
<summary>
2021-11-02: Let's talk about Biden's $450,000 payments.... (<a href="https://youtube.com/watch?v=eJjFtQz2Zik">watch</a> || <a href="/videos/2021/11/02/Lets_talk_about_Biden_s_450_000_payments">transcript &amp; editable summary</a>)

Beau clarifies the controversy over Biden's $450,000 payments as settlements from lawsuits and predicts potential embarrassment if cases go to court.

</summary>

"If you can't defend them, it's clearly wrong."
"If it goes to court, you're going to be mad later."
"Settling quickly is an attempt to avoid further embarrassment."
"These settlement payments are a gift to the Republican Party."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the controversy surrounding Biden's $450,000 payments to those separated at the border.
Acknowledges the outrage and grants permission for viewers to express disapproval to representatives or the White House.
Clarifies that the payments are actually settlements from lawsuits the Biden administration anticipates losing.
Points out that the Trump administration's zero tolerance policy likely violated U.S. law, international law, and the Constitution.
Predicts that if the cases go to court, requested payouts could be much higher than the settlement offer.
Suggests that going to court could expose more information, potentially embarrassing the country further.
Notes that settling quickly is an attempt by the Biden administration to avoid further embarrassment.

Actions:

for viewers concerned about biden's settlement payments.,
Contact your representative or the White House to express disapproval with the policy (implied).
</details>
<details>
<summary>
2021-11-01: Let's talk about Manchin and political games.... (<a href="https://youtube.com/watch?v=p4fvfNKTM_g">watch</a> || <a href="/videos/2021/11/01/Lets_talk_about_Manchin_and_political_games">transcript &amp; editable summary</a>)

Beau questions Senator Manchin's motives and actions, accusing him of playing political games while standing in the way of progress and deflecting blame onto others.

</summary>

"Stop playing political games."
"He's feeling heat. He's feeling pressure."
"If you didn't want the climate policy and just came out and said, nope, I'm an oil and gas man, can't do that, I could at least respect it."
"I view it all as one giant political game."
"Senator Manchin is the person who is standing in the way of progress."

### AI summary (High error rate! Edit errors on video page)

Senator Manchin held an impromptu press conference criticizing progressives for playing political games.
Beau questions Senator Manchin's framing of himself as not part of the establishment despite being in politics since the late 1900s.
Senator Manchin is accused of diverting attention, feeling pressure, and playing political games to criticize progressives.
Beau points out that Senator Manchin has used leverage tactics himself to hold up progress.
Senator Manchin questions the cost and money behind climate policy and social spending, despite economist endorsements.
Beau suggests Senator Manchin's reasons for opposing climate policy might be influenced by large contributions from energy sector companies.
Beau believes that social safety nets for workers may threaten mine companies' ability to mistreat their workers, leading to Senator Manchin's opposition.
Senator Manchin's biggest donors are revealed to be large investment banks, law firms, and oil and gas companies, not reflective of a regular working man.
Beau criticizes Senator Manchin for deflecting blame onto progressives and not understanding the stakes of blocking climate legislation and social safety nets.
Senator Manchin is accused of playing political games and standing in the way of progress, potentially harming the Democratic Party in upcoming elections.

Actions:

for progressive activists,
Visit opensecrets.org to research and understand contributions made to representatives (suggested).
Hold accountable politicians who prioritize corporate interests over climate legislation and social safety nets (implied).
Advocate for transparency in political funding and hold representatives like Senator Manchin accountable for their actions (implied).
</details>
<details>
<summary>
2021-11-01: Let's talk about 30% of Republicans, Trump, and the midterms.... (<a href="https://youtube.com/watch?v=3uGV_-uZsM0">watch</a> || <a href="/videos/2021/11/01/Lets_talk_about_30_of_Republicans_Trump_and_the_midterms">transcript &amp; editable summary</a>)

Beau talks about the distancing between Republican politicians and individuals involved in the events of January 6th, stressing the need for Republicans to reduce the high percentage of those who believe violence may be necessary in the future.

</summary>

"They don't really have a choice if they want to make it."
"30% is incredibly high."
"They're gonna have to tame down their rhetoric."
"It's worth noting that I think the numbers for independents was 17, and I think for Democrats it was 11%."
"This is something we gotta keep an eye on."

### AI summary (High error rate! Edit errors on video page)

Beau talks about the distancing occurring between Republican politicians and the individuals who participated in the events of January 6th.
He mentions that politicians may have to distance themselves from the individuals involved to secure their political future.
Beau notes the historical consequences of turning on individuals you have previously inflamed.
Most members of Congress mentioned in a report about organizing rallies that fed into the Capitol events have denied involvement.
Trump responded to the Washington Post with a statement full of lies, according to the Post.
Trump's spokesperson referred to the individuals at the Capitol on January 6th as "agitators not associated with the president."
Lindsey Graham reportedly told the sergeant-at-arms to "take back the Senate" after it had been taken, which may not sit well with the Republican base.
New polling indicates that 30% of Republicans believe violence may be necessary in the future to save the country, with the number rising to 39% among those who believe the election was stolen.
Beau points out the decrease in the percentage of Republicans believing violence may be necessary, but the current 30% is still concerning.
He stresses the importance of reducing this percentage before the midterms, which will require Republicans to take action and change their rhetoric.
Beau suggests that Republicans linked to the events of January 6th may struggle to rebrand themselves as part of the moral majority.
He mentions that addressing this issue must come from within the Republican Party and expresses concerns about the potential consequences if it is not addressed.
Beau shares his interactions with individuals from the political spectrum who were unable to provide clear answers about their intentions regarding the use of guns and potential conflicts.
He raises concerns about the lack of a clear cause or agenda among those ready to use violence.

Actions:

for republicans,
Republicans need to take action to reduce the percentage of individuals who believe violence may be necessary in the future. (implied)
</details>
