---
title: Let's talk about Christopher Miller handling Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=46EZfBHQo1o) |
| Published | 2021/11/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Provides insight into Christopher Miller, the last acting Secretary of Defense under Trump.
- Points out that individuals associated with the Trump administration may be trying to rehabilitate their image.
- Describes how Miller handled situations where Trump was on the verge of making disastrous decisions.
- Explains Miller's approach in countering what he perceived as lacking judgment from the former president.
- Mentions Miller's three goals upon taking over as acting Secretary of Defense.
- Indicates that two of Miller's top priorities were aimed at preventing Trump from using the military against Americans.
- Notes the recurring sentiment among military officials about anticipating a potential coup during Trump's presidency.
- Warns about the dangers of rhetoric that could lead to the military being turned against the people.
- Emphasizes the need to be cautious and mindful of the implications of supporting certain political figures.
- Urges listeners to be aware of the potential consequences of unchecked behavior from political leaders.

### Quotes

- "He played the, well, you're about to find out what crazy is game."
- "No military coup, no major war, and no troops in the streets."
- "It's really weird that this same sentiment has occurred that many times."
- "The greatest war machine that the world has ever known."
- "That should be pretty concerning."

### Oneliner

Beau sheds light on how Christopher Miller handled Trump's decision-making, revealing concerns about potential military use against Americans and the need for caution in supporting certain political figures.

### Audience

American citizens

### On-the-ground actions from transcript

- Remain vigilant and critical of political rhetoric that could endanger democratic norms and institutions (implied).
- Stay informed about the actions and decisions of political leaders that could impact national security and civil liberties (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Christopher Miller's actions as acting Secretary of Defense and the potential risks associated with unchecked behavior from political figures.

### Tags

#ChristopherMiller #TrumpAdministration #MilitaryCoup #PoliticalRhetoric #NationalSecurity


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
Christopher Miller. He was the last acting Secretary of Defense under Trump. And some
information came out about how he was acting, and I think it's worth going into. Now, something
we have to keep in mind is that Miller, like everybody else associated with the Trump administration,
is probably trying to rehabilitate their image a little bit. So in some ways, any of these
interviews that occur, any of these statements, we kind of have to weigh them and see if it's
plausible. So what did Miller say? He said that he acted like a madman when it came to
providing the former president with advice. When it came to Iran, he was like, you know
what, yeah, we can do it, and walked him through exactly how destructive it would be. He said,
yeah, you know, we'll throw in a hundred man planes, you'll lose a few, but it's just how
it works, you know. And apparently other people in the room were like, he's the new guy, don't
listen to him, he's crazy. Now, according to Miller, he did this for that reason. He
found the president's judgment lacking, felt Trump's judgment wasn't there, that he was
a little too provocative, a little too aggressive. So to counteract that, he played the, well,
you're about to find out what crazy is game. Is it plausible? Yeah. Yeah. In fact, it wouldn't
even be the first time we've heard this from somebody, that this was how former President
Trump was handled when he was on the verge of making disastrous decisions, more disastrous
decisions. So it is believable, but we also have to keep in mind that there is that image
rehabilitation thing that's going on. A lot of people associated with the Trump administration
are attempting to do that. So do I believe it? Do I believe that Pompeo was the voice
of sanity in the room? I can't say for certain that it's not true. I'm open to this being
true, but there's something else that he said. Apparently when he took over, he had three
goals. No military coup, no major war, and no troops in the streets. The acting Secretary
of Defense of the United States had three top priorities. Two of them were to stop former
President Trump from using the US military against Americans. That I definitely believe.
Why do I believe it? Because if I had a nickel for every time somebody in the civilian leadership
of DOD or a top general said that, I'd have like 35 cents, which isn't a lot of money,
but it's really weird that this same sentiment has occurred that many times, right? I definitely
believe that those in the military who were closest to former President Trump were anticipating
a coup, and I believe they had a reason to. It's something that we need to keep in mind.
As we move forward, and a lot of politicians have painted themselves in this corner where
they have to support the former President, and they carry on that rhetoric, we need to
remember where that rhetoric leads. That rhetoric leads to the top people in the military trying
to figure out how to stop the greatest war machine that the world has ever known from
being turned against the people of the United States. That should be pretty concerning,
and it should definitely weigh on people's decisions. This isn't the first time we've
heard this. It's not even the second or the third time we've heard this. We've heard this
a lot. This was their concern because of the former President's behavior. So, it is definitely
something to keep in mind. It's just a thought. Anyway, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}