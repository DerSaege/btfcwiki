---
title: Let's talk about nurses and misinformation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jziu9ujx88o) |
| Published | 2021/11/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Nurses received exciting news regarding accountability for spreading misinformation related to COVID-19 vaccines.
- A joint statement from various nursing associations emphasized nurses' professional accountability for the information they provide to the public.
- Nurses who spread misleading information may face disciplinary action by their board of nursing.
- Misinformation can jeopardize public health and endanger nurses' licenses and careers.
- The statement aims to combat the profitability of misinformation spread by individuals with credentials.
- Nurses, particularly those on the front lines in ER and ICU settings, welcomed this news with relief and satisfaction.
- Fighting both the public health crisis and misinformation has left nurses exhausted.
- The spread of misinformation by credentialed individuals can have deadly consequences.
- Nurses are hopeful that the decline in misinformation from credentialed sources will make it less believable.
- Many individuals have used their professional credentials as a shield to profit from spreading misinformation.

### Quotes

- "Misinformation is profitable."
- "They're tired of fighting the public health issue and they're tired of fighting the misinformation."
- "That misinformation is deadly."
- "A lot of people have used those letters after their name as a shield."
- "Y'all have a good day."

### Oneliner

Nurses celebrate increased accountability for spreading COVID-19 vaccine misinformation, aiming to combat the deadly impact of false information profiteers.

### Audience

Nurses, healthcare professionals

### On-the-ground actions from transcript

- Contact nursing associations for guidance on combating misinformation (suggested)
- Support efforts to hold accountable those spreading misleading information (implied)
- Stay informed and vigilant against misinformation in healthcare settings (suggested)

### Whats missing in summary

The emotional weight conveyed by nurses' relief and hope for a decline in misinformation spread by credentialed individuals.

### Tags

#Nurses #Misinformation #COVID19 #Accountability #Healthcare


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about nurses, because I have some news that has made every
nurse that I know very happy, something I think they've been waiting for, and we're
going to kind of go over it.
So we're going to talk about nurses and misinformation.
If you don't know, nurses are governed by a lot of associations and regulatory bodies.
A statement has been released.
When identifying themselves by their profession, nurses are professionally accountable for the
information they provide to the public.
Any nurse who violates their state nurse practice act or threatens the health and safety of
the public through the dissemination of misleading or incorrect information pertaining to COVID-19
vaccines and associated treatment through verbal or written methods, including social
media, may be disciplined by their board of nursing.
Nurses are urged to recognize that dissemination of misinformation not only jeopardizes the
health and well-being of the public, but may place their license and career in jeopardy
as well.
I know right now nurses are going, who put out that statement?
Which association did it?
Who's responsible for this?
I'm pretty sure it's all of them.
This was a joint statement by the ACEN, the ANA, the AONL, the NLN, the CNEA, the NSNA,
the OADN, and last but not least, the National Council of State Boards of Nursing.
Welcome to the find out portion of the show.
So why did this statement have to be made?
Because misinformation is profitable.
People like to have their stuff confirmed by people with credentials.
There's a market for it.
So all of those people who have been using their credentials to promote their TikTok
channel or sell a book or whatever and have been spreading misinformation may be doing
that without those letters after their names.
The nurses that I know had, well I can't actually say what they said, most nurses I know were
ER nurses or ER nurses who then went to the ICU, and if you don't know, they say stuff
that would make a Navy chief blush.
But let's just leave it at they were really happy about this news because they're tired.
They are tired.
They're tired of fighting the public health issue and they're tired of fighting the misinformation.
So this was exciting news for them.
Those who are spreading misinformation and abusing their credentials to do so.
I mean, it costs people.
I know people are waiting for the rest of that sentence.
It costs people what?
Time? Money?
No, it costs people.
That misinformation is deadly.
And this was a move that I think a whole lot of nurses have been waiting for and hoping
for.
I know those that I know, and I know a lot, are very happy about this.
There wasn't anybody that was like, oh no, they were all super excited because once the
flow of misinformation from credentialed sources kind of declines, it's going to be less believable.
A lot of people have used those letters after their name as a shield, as a way to say, oh,
I know what I'm talking about.
Buy my book.
Watch my TikTok channel.
And they've profited from it.
Well I hope you made enough to sustain yourself for a while.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}