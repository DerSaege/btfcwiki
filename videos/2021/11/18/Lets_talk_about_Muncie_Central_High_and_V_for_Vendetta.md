---
title: Let's talk about Muncie Central High and V for Vendetta....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NrHxTU-k47M) |
| Published | 2021/11/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Teacher at Muncie Central High School assigned V for Vendetta project, drawing parallels between modern social movements and the plot of the movie.
- Posters created by students were put up in the hallway, causing discomfort to the establishment and school resource officers.
- Establishment asked teacher to remove the posters, sparking a student-led demonstration and remote learning for students.
- Beau suggests watching V for Vendetta to understand the parallels between the movie and the events at the school.
- Beau criticizes the establishment for reacting negatively and militarizing the situation, likening them to the antagonists in the movie.
- Students are drawing their own conclusions, not being indoctrinated, based on what they see and hear.
- The situation at Muncie Central High School mirrors the plot of V for Vendetta, with students being silenced and controlled by the establishment.
- Beau encourages the school to acknowledge their mistake and embrace critical thinking rather than trying to suppress it.
- Beau warns that attempts to keep students ignorant and prevent critical thinking are happening in school districts across the country.
- Suppressing critical thought in education can lead to a society easily misled and manipulated.

### Quotes

- "You are reinforcing it every step of the way."
- "Trying to stop students from thinking critically about what they see and what they hear and their own experiences is indoctrination."
- "They see what exists. They see what's around them. And yes, there are parallels. They're not wrong."

### Oneliner

Teacher assigns V for Vendetta project, students draw parallels with modern movements, establishment suppresses dissent, Beau criticizes lack of critical thinking in education.

### Audience

Students, Teachers, Activists

### On-the-ground actions from transcript

- Speak out against attempts to suppress critical thinking in schools (suggested)
- Get involved in advocating for education that encourages critical thought (suggested)

### Whats missing in summary

The full transcript provides a detailed analysis of the events at Muncie Central High School and the broader implications on education and critical thinking.

### Tags

#Education #CriticalThinking #StudentActivism #Indoctrination #Schools


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about whether life imitates art
or art imitates life in Muncie Central High School
and some classwork that got assigned there
and the completely predictable chain of events
that arose from that classwork.
A teacher there was teaching the story of V for Vendetta.
Assigned classwork, assigned a project.
This led to students producing posters
and these posters drew parallels
between modern social movements
and the plot from V for Vendetta,
which I mean, yeah, that makes sense.
Kind of the point.
These posters then went up in the hallway
where the establishment had an issue with it.
It hurt their feelings.
According to reporting,
school resource officers didn't like it.
That led to a discussion.
That discussion led to the teacher being asked
to take the posters out of the hallway.
That predictably prompted a student-led demonstration,
which then led to the establishment
putting the students on remote learning, e-learning.
If only there was some book that could have predicted
this exact chain of events.
I don't actually expect the people
who are in opposition to this to read.
So watch the movie.
And when you realize one of the most poignant scenes
in the movie is the part where somebody says something
about the establishment
and the establishment gets his feelings hurt,
and then a bunch of militarized goons in fatigue
show up and corner them,
maybe you'll start to see some parallels.
I guess we're thankful nobody got black bagged.
Once you're done processing that scene,
review the footage from the hallway again.
Nice pants, by the way.
You look like real warriors.
Act like it too, because when I think warriors,
I think picking on teenagers and teachers.
Of course the students see similarities.
You're cosplaying it in the hallways for them.
The opposition to this being taught suggests,
oh, well, see, they're being indoctrinated.
No, the students are drawing their own conclusions
based off of what they see and what they hear,
and you are reinforcing it every step of the way.
You're following it like it's a script.
Of course they see parallels.
They exist.
They see the parallels because they're not indoctrinated.
You don't see them because you are indoctrinated.
You are playing the role of the bad guy.
Stuff in the hallway isn't the only similarity,
because see, in this analogy, the students,
well, they would be the citizens.
And in the story, the students have this protest,
or the citizens have this protest,
and the establishment puts them on remote learning,
puts out a curfew.
It's the exact same thing.
It's the same thing.
Of course they see similarities.
Of course they do.
You're playing it out for them.
I'm sure the goal of this was to cast doubt
on what they have seen with their own eyes
to try to indoctrinate them.
It's not working, because to the contrary,
you're confirming everything they believe.
This might be the time for the school there
to try to diffuse this, to acknowledge, hey, you know what?
You're right.
We were wrong.
We were using V for Vendetta as a how-to guide
instead of a warning.
They see what exists.
They see what's around them.
And yes, there are parallels.
They're not wrong.
You are.
The school board is.
I would point out that this type of move
to keep students ignorant is occurring in school districts
across the country.
You might want to get involved.
You might want to speak out, because this isn't isolated.
V for Vendetta is on that list of 850 books
that circulated in Texas.
When our schools are concerned about educating,
concerned about critical thought,
we have an issue that will last for generations.
Trying to stop students from thinking critically
about what they see and what they hear
and their own experiences is indoctrination.
And it leads to a society that can be led astray very easily.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}