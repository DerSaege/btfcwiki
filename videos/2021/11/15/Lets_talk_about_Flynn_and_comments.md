---
title: Let's talk about Flynn and comments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U0GZ0lUL9FY) |
| Published | 2021/11/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Flynn made a concerning statement about having one religion in the nation, which goes against American ideals of religious freedom and separation of church and state.
- He calls on patriots to vocalize their opposition to such ideas, as they contradict the values enshrined in the US Constitution.
- Beau references historical documents like the Treaty of Tripoli and quotes from founding figures to support the separation of religion and government.
- He warns that such rhetoric may increase before the midterms as a desperate move by those on a losing streak.
- Beau points out that supporting the intertwining of religion and government is a betrayal of the founding principles of the country.

### Quotes

- "If you consider yourself a patriot, you must vocalize your opposition to this."
- "There is no basis in American patriotism when it comes to supporting this garbage."
- "They are telling you who they are, and you better believe it."

### Oneliner

Beau warns against the dangerous rhetoric of pushing for one religion in the nation, urging patriots to stand up against such betrayal of American ideals.

### Audience

Patriots, defenders of American ideals

### On-the-ground actions from transcript

- Oppose any attempts to intertwine religion and government (implied)
- Educate others on the importance of the separation of church and state (implied)

### Whats missing in summary

Beau's passionate delivery and historical references provide a compelling argument against the dangerous idea of one religion in the nation.

### Tags

#ReligiousFreedom #SeparationOfChurchAndState #AmericanIdeals #Patriotism #Midterms


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about Flynn's statements.
News came out over the weekend, in case you missed it.
Flynn was giving a speech, and he said something
that it's not surprising.
It's pretty predictable.
It's on the list of progressions.
But hearing it uttered in seriousness by somebody
who has access to a lot of political figures
is something we need to pay attention to.
He said, if we're going to have one nation under God,
which we must, we have to have one religion.
One religion.
I do not understand how people who fashion themselves
as patriots can listen to that, can cheer that, can applaud
that, can even look away and pretend
that they didn't hear it.
If you consider yourself a patriot,
you must vocalize your opposition to this.
There weren't any founders who believed this.
Even the origin myths, the stories
that are told to children.
Why did the pilgrims come here?
Religious freedom, right?
This runs counter to everything that is
supposed to be an American ideal.
It's enshrined in the US Constitution.
I guess some people's oaths do, in fact, have expiration dates.
Because the First Amendment to the Constitution says pretty
clearly that Congress shall make no law respecting
an establishment of religion or prohibiting
the free exercise thereof.
If that isn't enough, you could go to the Treaty of Tripoli.
Article 11 is pretty clear.
The government of the United States of America
is not in any sense founded on the Christian religion.
That was presented to the Fifth Congress by John Adams.
Go to the Supremacy Clause of the US Constitution.
Treaties are the law of the land.
Thomas Jefferson said, believing with you
that religion is a matter which lies solely
between man and his God, that he owes account
to none other for his faith or his worship,
that the legitimate powers of government
reach actions only and not opinions,
I contemplate with sovereign reverence
that act of the whole American people, which
declares that the law of the land
and the act of the whole American people, which
declared that their legislature should make no law respecting
an establishment of religion or prohibiting the free exercise
thereof, thus building a wall of separation between church
and state, adhering to this expression of the supreme
will of the nation in behalf of the rights of conscience,
I shall see with sincere satisfaction
the progress of those sentiments which
restore to man all his natural rights.
There is no basis in American patriotism
when it comes to supporting this garbage.
This isn't from the United States.
It's not a founding principle of this country.
It never was.
It's manufactured.
You want to know where it comes from?
It's not going to be from any of our founders.
Once again, I would like you to go to Lawrence Britt's list.
It's the eighth characteristic, the intertwining
of religion and government.
They are telling you who they are.
You'd better believe them.
We can expect this kind of rhetoric
to ratchet up between now and the midterms,
because they understand that they're on a losing streak.
If their candidates, if the people who support them
don't make a comeback in the midterms,
they know they're done, because nobody wants to support a loser.
They are going to ratchet it up.
They are going to try to emulate that fiery rhetoric that
brought about so many national changes in other countries.
You cannot cling to this garbage and call yourself a patriot,
because it is, in fact, a betrayal of every ideal
this country was founded on.
They are telling you who they are,
and you better believe it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}