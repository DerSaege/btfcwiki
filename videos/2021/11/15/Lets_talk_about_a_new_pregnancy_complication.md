---
title: Let's talk about a new pregnancy complication....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Hz04HPivzLQ) |
| Published | 2021/11/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mentions a study revealing high homicide rates for pregnant individuals compared to traditional pregnancy-related issues.
- Previous smaller study results were considered too high due to a small sample size.
- New comprehensive study covers all 50 states over two years (2018-2019).
- Push for CDC to include homicide in maternal mortality rates due to the alarming statistics.
- Risk factors for increased rates include being young and being a black woman.
- Experts in intimate partner violence confirm pregnancy as a dangerous time for women.
- Expectation that rates will increase due to limitations on family planning access.
- Anticipates higher rates in the future due to laws restricting family planning.
- Suggests shelters and DV organizations reach out more during pregnancy to provide support.
- Warns of unintended consequences for those advocating for restrictive family planning laws.

### Quotes

- "If you are pregnant or were pregnant in the previous 42 days, you are more likely to be murdered than you are to die of what would typically be seen as a pregnancy related issue."
- "The two most dangerous times for women is when they are leaving or when they are pregnant."
- "By limiting that type of family planning, they are leaving people exposed, leaving them more vulnerable."

### Oneliner

Pregnant individuals face higher homicide rates than pregnancy-related issues, leading to a push to include homicide in maternal mortality rates and anticipate unintended consequences from restricted family planning laws.

### Audience

Policy Advocates, Healthcare Workers

### On-the-ground actions from transcript

- Support shelters and organizations countering DV issues by reaching out during pregnancy (implied).
- Advocate for inclusive family planning policies to prevent increased vulnerability (implied).

### Whats missing in summary

Importance of addressing the intersection of pregnancy and intimate partner violence comprehensively.

### Tags

#MaternalMortality #HomicideRates #FamilyPlanning #DVAdvocacy #PolicyChange


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk
about a study that revealed something that isn't really surprising if you're
familiar with the topic, but I'm fairly certain it's going to raise some
eyebrows. I saw a similar study, I want to say last year, I want to say it was last
year, but it was of a single state over a one-year period. The results were high,
which was expected, but they seemed a little too high for me to believe and
because it was such a small sample size I kind of discounted it. I don't even
think I reported on it. A new more comprehensive study has come out covering
all 50 states over a two-year period, 2018 and 2019. What it suggests is that
if you are pregnant or were pregnant in the previous 42 days, you are more likely
to be murdered than you are to die of what would typically be seen as a
pregnancy related issue. Homicide rates are twice that of the number of women
lost to bleeding or placenta disorders, which are the leading causes under the
traditional framework. Because of this information there is now a push to have
the CDC include homicide in the maternal mortality rates. As with anything else,
there are things that will increase your risk. If you are young, if you're under
the age of 24, your rates are even higher. If you are a black woman, your rates are
even higher. The reason I said that this wasn't surprising, even though I'm sure
it's raising eyebrows, if you talk to any DV expert, if you talk to any expert in
intimate partner violence, they will tell you that the two most dangerous times
for women is when they are leaving or when they are pregnant. So this
tracks with conventional knowledge of that subject. It's just the rates are a
lot higher than I think anybody really anticipated. Now, sure, reframing maternal
mortality rates, stuff like that, but this is also information that can provide a
good lens for other topics. And the reason I'm fairly certain this is going
to become bigger news is when people decide to look at it through one
particular lens. Obviously it suggests that shelters, organizations that are
countering DV issues, should be reaching out more during pregnancy. That seems
like a given or right after. It is also worth noting that despite everybody's
best efforts, given this new information that is confirming other smaller
studies, I am fairly certain that these numbers will go up. These rates will go
up in the near future. The reason for that is that there are going to be a
bunch of women who cannot access certain types of family planning now due to
newer laws. Some are proposed, some have been enacted, some are already making
their way through the court system. But by limiting that type of family planning,
they are leaving people exposed, leaving them more vulnerable. If you are one of
those who pushed for those types of restrictions, when these numbers go up,
welcome to the law of unintended consequences. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}