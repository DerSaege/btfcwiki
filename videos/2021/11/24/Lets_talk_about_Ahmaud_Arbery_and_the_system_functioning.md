---
title: Let's talk about Ahmaud Arbery and the system functioning....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ix9K4BfDmL8) |
| Published | 2021/11/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Commentary claims the justice system functioned properly in Ahmaud Arbery's case, but Beau disagrees vehemently.
- The system did not function as it should, despite the right verdict being reached.
- Local cops were instructed not to make arrests by the Brunswick DA following the incident.
- Different district attorneys continued to interfere after being told not to make arrests.
- Public outrage ensued after the video of the incident surfaced on May 5th.
- A grand jury was convened quickly after the video release, with the Georgia Bureau of Investigation stepping in.
- Beau questions the system's functionality when a DA is indicted in the process.
- Despite the right outcome, justice was served due to public pressure and perseverance, not the system working effectively.
- Acknowledging the victories is vital to avoid burnout, but it was hard-won in this case.
- The situation surrounding Ahmaud Arbery's case was a win achieved through relentless efforts and dedication from many individuals.
- Justice prevailed not because the system functioned, but rather because people refused to give up.
- The victory was a testament to the power of collective action and perseverance against systemic failures.

### Quotes

- "Justice was served in spite of the system failing."
- "Justice was served because of public pressure, because of those black pastors certain people didn't want around."
- "This isn't over. This isn't a situation where everything went smoothly."

### Oneliner

The justice system failed in Ahmaud Arbery's case; justice prevailed due to public pressure and unwavering perseverance.

### Audience

Activists, advocates, community members

### On-the-ground actions from transcript

- Support and amplify the voices of marginalized communities in seeking justice (implied)
- Advocate for systemic changes to prevent similar injustices in the future (implied)
- Continue to apply public pressure on authorities to ensure accountability and fairness (implied)

### Whats missing in summary

The emotional weight and dedication of individuals behind the fight for justice in Ahmaud Arbery's case.

### Tags

#JusticeSystem #AhmaudArbery #PublicPressure #SystemicChange #CommunityPersistence


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about a verdict out of Georgia.
We're going to talk about the verdict in the case of what
happened to Ahmaud Arbery.
There's a running theme right now going on in commentary
that the justice system functioned as it should.
No, no, it absolutely did not.
It did not.
And I'm not talking about the verdict.
From everything I've seen, the verdict's the right one.
That's not what I'm talking about.
I'm talking about this incredibly inaccurate and, to
me, pretty dangerous idea that the system
functioned as it should.
Because it didn't.
It did not.
A quick little recap.
According to reporting, on February 23, local cops were told by the Brunswick DA not to
make any arrests.
And then a different DA is reported to have continued that same line and continued to
interfere on February 24th and April 2nd.
May 5th is when the video went out.
That's when the public got to see that video.
Within hours, the Atlantic DA had said that a grand jury would be convened and that GBI,
the Georgia Bureau of Investigation, was stepping in.
That doesn't sound like the system functioned as it should.
when you had a DA get indicted. Sounds like it's pretty broke. Sounds like a lot of the issues
that are being seen as no longer relevant were very much at play here. But that video came out
and that public pressure came into play. And then it didn't take long at all. But it was February,
March, April, May. Four months. Well, four calendar months. I guess it was 70
something days. That doesn't sound like things are functioning as they should.
It is important to acknowledge the winds. If you don't, you'll burn out. It's
important to acknowledge the winds. And in some ways, yeah, this is a
wind, but it was hard fought. It wasn't that the system worked. It wasn't that
justice was served because the system functioned. Justice was served in spite
of the system failing. It happened because of public pressure, because of
those black pastors certain people didn't want around, because people didn't
give up on it. I think that is a very important takeaway. It is something that
we need to remember here. This isn't over. This isn't a situation where
everything went smoothly. It was a win, but it was hard-fought by a whole lot of
people who put in a whole lot of time.
I heard somebody say that it wasn't a celebration, but it was proof that the
spirit of a mob could overcome that mob. I like looking at it that way. Anyway, it's
It's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}