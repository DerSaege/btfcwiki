---
title: Let's talk about new hypersonic developments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=osHD5nGLPnQ) |
| Published | 2021/11/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Chinese hypersonic missile test sparked US media frenzy, portraying US falling behind.
- US foreign policy now focuses on countering near peers like China and Russia.
- Three contracts for glide phase interceptor technology were awarded by the Missile Defense Agency.
- Lockheed Martin, Northrop Grumman, and Raytheon Missile and Defense received the contracts.
- These contracts are for intercepting hypersonic missiles, not developing them.
- The US has been working on hypersonic technology for years, evident through companies' experience.
- Existing equipment like the Spy-6 radar shows US progress in tracking and intercepting hypersonic missiles.
- Media previously portrayed hypersonic missiles as unstoppable, but US defenses are advancing.
- US likely has more advanced hypersonic capabilities than publicly known.
- US military's strategy involves keeping technological advancements secret from the public.
- Arms races between nations like the US, China, and Russia are ongoing and characterized by secrecy and technological advancements.
- The US is likely more on par with China and Russia in hypersonic technology than acknowledged.
- Continuous technological advancements and secrecy define the dynamics of military competition.
- The US has made significant progress in hypersonic capabilities, contrary to initial perceptions of falling behind.
- Military advancements and arms races have historical precedence in the Cold War era.

### Quotes

- "Get used to this. If you weren't alive during the Cold War, if you didn't study it, understand this is going to play out over and over again."
- "It's a back and forth. It's cat and mouse. And this is how arms races began."
- "It does appear that the United States probably has a lot more parity with China and Russia than the government is letting on."

### Oneliner

Chinese hypersonic missile test triggers US media panic, but US defense advancements likely surpass public knowledge in ongoing arms race.

### Audience

Defense analysts, policymakers

### On-the-ground actions from transcript

- Monitor advancements in hypersonic technology within your country's defense industry (exemplified)
- Stay informed about international arms races and technological developments (exemplified)

### Whats missing in summary

Insights into the historical context of arms races and military technological advancements.

### Tags

#HypersonicMissiles #USDefense #ArmsRace #TechnologicalAdvancements #MilitaryCompetition


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk a
little bit more about hypersonic missile technology and the mineshaft gap and a
script that anybody who wasn't alive during the Cold War needs to get used to.
Recently the Chinese did a test involving hypersonic missiles. US media
went wild saying that US intelligence was stunned and made it appear as though
the United States was really far behind on this technology. Made it seem as though
the Chinese had a substantial edge. Now that our foreign policy has shifted to
countering near peers, China and Russia, you're going to see more and more of
this. Welcome to the arms race portion of the show. So what have we discovered
since then? You know at the time I said I wouldn't worry too much about this. We
probably have technology that is classified and isn't publicly known. We
do know that we have these programs out there, so on and so forth. So three
contracts were handed out by the Missile Defense Agency. One went to Lockheed
Martin, another went to Northrop Grumman, and the third went of course to Raytheon
Missile and Defense. The thing is, this isn't for hypersonic missiles. It's for a
GPI. That's a glide phase interceptor. This is something to shoot down
hypersonic missiles. It'll go on an Aegis, so it'll be ship-borne. If you read the
reporting about these contracts going out, there's some interesting little
tidbits in it. First, all three of these companies have experience with
hypersonic missiles. Northrop was so involved in 2019 that they built a brand
new facility in Alabama to deal with hypersonic technology. That mineshaft gap
doesn't seem so pronounced now. It seems as though the US has been working on it for
years. In fact, there is some equipment, such as the Spy-6 radar, that the Navy
took delivery of more than a year ago that is capable of tracking it. So this
is on the defensive side. This is shooting this thing down. Now keep in
mind, just a month ago or so, the media made it seem as though these things were
unstoppable. They're not. And we're well on our way to having that product.
There's also something about a low-Earth orbit satellite to track it. Now, it is
relatively safe to assume that since we are this far along on the defensive side,
that the US's hypersonic capabilities are probably far more advanced than the
media or Defense Department is letting on. This is one of those situations where
what the American public doesn't know, well, that's what makes them the American
public. It's secret. This script, this chain of events, where a foreign power,
specifically China or Russia, has some technological advance that makes the
news. And the US military establishment is like, yeah, we don't know how to do any
of that. Shock. And then a little while later, you find out they've been working
on it for years and have already taken delivery of some equipment and have
already fielded some stuff. Get used to this. If you weren't alive during the
Cold War, if you didn't study it, understand this is going to play out over
and over again. It's a back and forth. It's cat and mouse. And this is how
arms races began. So that seems pretty likely. For those who were concerned
about it, it does appear that the United States probably has a lot more parity
with China and Russia than the government is letting on. Just the little
tidbits that are showing up in the reporting suggest that we are much
further along than anything they said at the time of the Chinese test.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}