---
title: Let's talk about CRT and thanksgiving dinner....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ab-3GERLfao) |
| Published | 2021/11/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A teacher wants to prevent her Uncle Bob from ranting about a theory during Thanksgiving dinner.
- The theory is critical race theory (CRT), which is often misunderstood by those who oppose it.
- Beau suggests creating a new concept, Critical History Theory (CHT), to redirect the conversation.
- CHT focuses on examining the intersection of history and law in the US to challenge mainstream approaches to justice.
- By removing race from CRT and inserting history, Beau aims to make the concept more palatable to Uncle Bob.
- Beau advises starting with historical laws like the National Firearms Act to demonstrate the impact of past legislation.
- Connecting past laws to present consequences can help Uncle Bob understand the importance of learning from history.
- Beau recommends discussing laws with unintended or negative consequences to illustrate the need for scrutiny and reform.
- By introducing the racial aspect of CHT later in the conversation, Beau aims to reveal underlying motivations behind opposition to CRT.
- Beau underscores the need to challenge ingrained American mythology that whitewashes history and perpetuates harmful narratives.

### Quotes

- "Most people who are ranting about CRT couldn't define it if their life depended on it."
- "The purpose of history is to look back so you don't make the same mistakes."
- "It's about the possibility of slowly dismantling power structures that have kept people down."

### Oneliner

A teacher tackles Thanksgiving dinner controversy by introducing Critical History Theory, challenging American mythology, and addressing racism subtly.

### Audience

Teachers, educators, activists

### On-the-ground actions from transcript

- Introduce Critical History Theory (CHT) in educational settings to challenge mainstream approaches to justice (suggested).
- Start dialogues on historical laws and their impacts on society to foster understanding and reform (suggested).
- Encourage critical thinking and reflection on past legislation and its consequences (suggested).

### Whats missing in summary

The full transcript provides detailed strategies for navigating difficult conversational topics and challenging ingrained beliefs by reframing concepts and engaging in meaningful historical dialogues.

### Tags

#ThanksgivingDinner #CriticalRaceTheory #CHT #AmericanMythology #Racism #Education


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk a little bit more
about how to ruin Thanksgiving dinner.
Not really, this person was pretty specific saying
they didn't want to ruin Thanksgiving dinner,
but they're a teacher.
And they have their own Uncle Bob,
who is constantly on Facebook,
just ranting and raving about a certain theory
and blames teachers for injecting it into classrooms.
And she is certain that the topic will come up.
And she doesn't want him saying a bunch of stuff
in front of her kids.
So she wants something to kind of derail the conversation
and perhaps expose what she described as latent racism.
OK, so you have the assumption that racism
has a lot to do with it, and you're probably right.
The thing is, most people who are ranting about CRT
couldn't define it if their life depended on it.
And you're going to count on that here in a second.
But the real issue is that these courses, because it's no longer the Cold War, they're
trying to teach history rather than American mythology, rather than propaganda that says
the country never did anything wrong.
They're trying to teach history.
This is upsetting to people who became very invested in that mythology.
That's the real issue.
you're dealing with somebody who you perceive to probably be racist. So how
are you going to get through this conversation? When it comes up, remove
race from it. How can you remove race from CRT? I'm not sure about CRT but I'm
a big proponent of CHT. What is CHT? Critical History Theory. You've never
heard of it because I just made it up. The definition for it is a Critical
The history theory is a framework of analysis and an academic movement to examine the intersection
of history and law in the United States and to challenge mainstream American liberal approaches
to justice.
If you do know the definition of critical race theory, you realize I just removed the
word race, inserted history, and dropped the reference to the civil rights movement.
That bit about American liberal approaches, make sure that's in there because he's totally
going to see that as being Democrats.
So you have this theory that you're going to support.
You have to make it relevant to him.
You have to show him how laws from the past impact him and how later on you can use this
to showcase other things.
So I would start with, based on what you've said, the National Firearms Act, the first
gun control act back in the 30s.
And ask the question, you know, ask the question, did this law have any unintentional or unforeseeable
consequences that impact people later?
Do you think that this was a good gun law?
What are some of the problems that it created and almost guaranteed he's going to go on
a rant about short-barreled rifles and stabilizing braces.
It doesn't matter if you don't know what that is, just let him talk.
At this point, now that he has kind of admitted that laws from the past carry on unforeseen
consequences today, this is when you can address the whole not looking back thing.
No, the purpose of history is to look back so you don't make the same mistakes, and so
those young patriotic Americans can fix the country tomorrow.
They have to know what's broke in order to fix it, so they have to look back.
They'll agree with that.
You could go to the war, talk about the Patriot Act with the Republican Party casting so much
doubt through conspiracy theories on certain government agencies, the Patriot
Act has become unpopular among the right wing. Ask him about that. Are there any
unforeseen consequences? Are there any consequences that maybe they were
intentional but they aren't good? Go there. Talk about government regulations
that aren't laws that he might have a problem with. Taking your shoes off at
airport. Long-term consequences from something a long time ago. Get him going
along with this and ask him to provide examples of laws that had
consequences that were far-reaching that either weren't what was intended by the
the law or the law was bad, so it needs to be reviewed and you have to learn about it
and learn about those impacts because if you want to fix it, if you want those kids to
look forward, they have to know it's broken.
Now up till now, I'm willing to bet just about anything, you're going to have complete 100%
agreement.
Now is when you interject with, and of course, some of CHT is, in fact, looking at laws that
have racist impacts or laws that were racist when they were written.
And watch it change.
I have a feeling what you actually want to do is kind of show that the racism is there
and that that's a motivating factor for it.
This is how you do that, because I'm willing to bet up until that point where you say it
will also examine racist laws just like CRT, he'll be okay with it.
And that will showcase very clearly that it isn't about looking back.
It isn't about any of the rhetoric that they use.
It's about the possibility of slowly dismantling power structures that have kept people down.
That's how we go about it.
At the very least, it's going to completely derail the conversation because what might
happen is you get him talking about the Gun Control Act from the 30s and that conversation
may just spin off into a different direction.
But when it comes to CRT, you have to acknowledge most of the people who are against it have
no clue what it is.
They have no idea what it is.
They associate it with disrupting American mythology, a mythology that they are heavily
invested in because it makes up a large portion of their identity.
That's how they define themselves.
I'm a good red-blooded American.
So if you disrupt that mythology, you're actually taking a piece of who they are because they're
that heavily invested in stories that basically amount to fan fiction.
exactly true. Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}