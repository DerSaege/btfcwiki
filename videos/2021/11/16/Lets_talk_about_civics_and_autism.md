---
title: Let's talk about civics and autism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IRsXRFYhqiA) |
| Published | 2021/11/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A parent reached out about their child learning civics in the eighth grade in Florida.
- The child is experiencing a disconnect between what they are taught in civics class and what they see in the world.
- Beau researched autism before suggesting a framing device to address this disconnect.
- Despite hours of talking to experts on autism, Beau found a disconnect between academic research and practical application.
- Speaking to autistic adults provided Beau with more insight than speaking to academics.
- Beau views the Declaration of Independence as a promise or ideal to strive towards.
- He notes that the system may not function as it should due to human imperfections.
- There's a concern about the push for patriotic education and mythology in teaching civics.
- Beau believes that promoting healthy skepticism towards political leaders is necessary.
- He points out the significant gap between the promise of equality and the reality experienced by eighth-graders.

### Quotes

- "Nobody knows anything about autism."
- "That disconnect is real."
- "What it should be and what it is are two very different things."
- "A healthy skepticism of political leaders is probably something we need to encourage."
- "We're doing such a bad job of keeping those promises."

### Oneliner

A parent's concern about the disconnect between civics education and reality leads Beau to seek insight on autism, revealing a need for practical application over research in academia.

### Audience

Parents, educators, policymakers

### On-the-ground actions from transcript

- Talk to autistic adults for better insight on autism (suggested)
- Encourage healthy skepticism towards political leaders (exemplified)

### Whats missing in summary

The full transcript delves into the disconnect between academic research and practical application, urging society to bridge the gap between ideals and reality in civic education.

### Tags

#Civics #Autism #Education #Academia #Equality


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
civics and autism. A couple of weeks ago I got a question from a mom. A question from
a mom whose child is in the eighth grade in Florida. That means they're taking
civics. They have to learn to be a good patriotic American and they are learning
that mythology. The child is aware of the world so their experiences say one
thing. The course says something else. You know they sit there and they listen to
the teacher talk about the rule of law and the equal application of justice and
then they look at the news and they're like wow, that teacher's lying to me. Now I
have a framing device that I've used for this disconnect. For the disconnect
between the way the system is supposed to work and the way the system does. I've
used it before on the channel. The thing is I don't know anything about autism.
Not really. So before I said hey, you know maybe think of framing it this way, I
wanted to make sure that that wouldn't be harmful. So I reached out to
experts on autism. Hours of conversation and I walked away knowing less than I
did when I started. It was incredibly frustrating for me and I'm not
personally involved in it. I can't imagine how frustrating it would be for a parent.
There is an idea that because this topic is so present in the media that we have
a good understanding of it. Doesn't seem to be the case. It seems that the
academic world is focused on research and not necessarily the practical
application of that research. There's a disconnect there. I was asking what to me
seemed to be a pretty simple question and could not get a straight answer.
That's probably something that field needs to look at. The research for
research sake is great, but there are people who could benefit from a
practical application of that research today. I was venting about this on
Twitter. I talked to a whole bunch of experts on autism and the only
thing I know now is that nobody knows anything about autism. Somebody in the
comments, have you considered talking to autistic adults? I mean yeah, that
sounds like a good idea. 22 minutes later, I had more insight, more
information, and a better understanding than I got from hours of conversations
with academics. That disconnect is real. That disconnect is real. So what's the
framing device? You may have heard me refer to the Declaration of Independence
as a promise, an ideal to strive towards. Not that that's how things actually work.
Because I didn't know a whole lot about autism. My experiences were pretty
limited. What I knew is that people I had worked with in the past in the security
field were brilliant analytical minds with less than stellar social skills. But
the reason they were great analytically is because they didn't like uncertainty.
And in the security field, eliminating uncertainty is a huge part of the job. It
made them indispensable. They could provide analysis in a day that would
take other people a week. That was my experience. I was concerned about
introducing that uncertainty. That this is the ideal, this is what we're striving
for. What you see, your experiences as far as what you've seen in the world, that's
the way it is. What it should be and what it is are two very different things.
One's a promise, an ideal of how the system should function. That system is
made up of people. People are imperfect and some people are bad. Therefore that
system rarely functions as it should or even something that even resembles the
way it should. That's how I would try to address and reconcile what's being
taught in that course and the world around them. There are a couple of other
things we should notice from this. This push to teach civics in a fashion that
is not critical of how things actually work. This patriotic education, this
embracing of mythology. Those people who pushed for it, they wanted it to create
great Americans and, let's be honest, indoctrinate people a little bit. I
certainly feel like that is going to backfire spectacularly. A student in the
eighth grade is more than capable of discerning that what they're being
taught in class is a lie. It's not the way the world functions and many of
those people in political office, such as those people who push for these kind of
courses, have no intention of making it function that way. I'm fairly certain
that a student identifying this in the eighth grade is not going to lead to
love of country. I don't necessarily think it's a bad thing. I think a healthy
skepticism of political leaders is probably something we need to encourage. The flip
side to that is that in the ideal, in the promise, we direct the country, the
citizens. We are so far away from that promise where all men are created equal,
all people are created equal today, would be the phrasing, that it is painfully
obvious to an eighth grader. We're doing such a bad job of keeping those
promises, of steering the country towards the ideal, that it doesn't match up at
all. The world as it exists, as experienced by an eighth grader who is aware of the
news, suggests that everything they're being taught, nobody's even trying to
make it happen. Probably something that we should think about as a society and
how we can get from where we are to a little bit closer to that ideal. Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}