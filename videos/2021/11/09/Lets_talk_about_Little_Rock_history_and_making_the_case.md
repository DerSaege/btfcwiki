---
title: Let's talk about Little Rock, history, and making the case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dNtVVI-7Z8I) |
| Published | 2021/11/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- History education should encourage asking "why" and exploring "what ifs" to understand context better.
- Teaching history through timelines may lead to losing valuable context.
- Imagining a different timeline where a small town in Arkansas integrated schools smoothly before Little Rock.
- Integration happened smoothly in the hypothetical town because the case for it was made effectively.
- The opposition to integration, like segregationists, follows a similar playbook across different timelines.
- In the hypothetical scenario, the Attorney General steps in to ensure integration proceeds smoothly.
- The potential political implications of successful integration at a local level on state politicians.
- Emphasizing the importance of teaching historical context alongside significant events.
- The power of community networks in influencing political views, whether positively or negatively.
- Questioning why the Civil Rights Act took almost a decade to pass despite public support.
- Criticizing white progressives and liberals for not actively supporting Civil Rights causes due to political expediency.
- Drawing parallels between historical inaction and current political shifts influenced by vocal minorities.
- Stressing the moral importance of standing up for issues deemed "woke" and making a case for them.
- Warning against shifting stances for political expediency and the importance of converting beliefs into action.
- Encouraging organization and action to support causes that require attention and not just political convenience.

### Quotes

- "We have to teach the why. Why things happened."
- "Community networks work. It does show that community networks work."
- "When you're talking about topics that are getting viewed as woke today, they're not political issues. They're moral ones."
- "When history is written, it doesn't record that the governor was a moderate in 1955."
- "You have to make the case. You have to be organized."

### Oneliner

Beau stresses the importance of teaching historical context, community action, and moral advocacy to avoid repeating past mistakes and drive meaningful change.

### Audience

History enthusiasts, educators, activists.

### On-the-ground actions from transcript

- Teach historical context effectively in education (implied).
- Engage in community action to influence political views positively or counteract negative influences (implied).
- Advocate for moral causes and make a compelling case for them (implied).
- Organize and take action to support critical social issues, not just for political expediency (implied).

### Whats missing in summary

The full transcript provides a deep dive into the importance of historical context, community action, and moral advocacy in driving positive change and avoiding repeating past mistakes.

### Tags

#History #Education #CommunityAction #MoralAdvocacy #PoliticalShifts


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk a little bit more about Little Rock
and how we teach history,
and why we should probably encourage people learning about history to ask the question why,
and what if. We should explore the what ifs as well, because it can provide more context
to understanding important events. The reason I want to do this is because when I gave those
poll numbers for Little Rock, a whole bunch of people were like, what? That doesn't sound
right at all. And it makes sense because of how we teach history. We teach history by
timelines, by looking back in retrospect and saying this was important, this was important,
we'll cover this and this and this. You lose a lot of context when you do it that way,
because we use timelines. We're very reliant on them. So today we're going to engage in
a little bit of interdimensional travel. We're going to go to a different timeline. We're
going to go to a timeline where the first schools to integrate in Arkansas, well, they
weren't Little Rock. We're going to say that the first school was in a little small town.
Education versus Board of Education happens. And some small town is like, you know what?
This is what's morally right. Let's do it. Let's do it. And then a school board, you
know what, let's make it weirder. An all-white school board unanimously votes to integrate.
And then they get out there and they make the case for it. They're like, you know what?
If we do this, it'll be cheaper. It's the law of the land. And it's morally right in
God's eyes. So what happens? They integrate. Things go smoothly because they made the case
for it. Now, of course, you have to wonder, okay, what if that happened? What's next,
right? So you do have that group that's in opposition to integration. You have the segregationists.
So they're going to show up. They're going to bring their guns and they're going to scream
about communist plots. And yeah, I mean, the playbook's kind of the same. Year to year,
time to time, dimension to dimension. They pretty much do the same thing. So they show
up with their guns. They scream about communism. But then what? See, because Brown versus Board
of Education, the Attorney General steps in. And there's kind of a restraining order put
on the segregationists. And integration goes fine in that little town. But then what? What
do the segregationists do? See, they're already active. They're already out there making their
case, right? They're going to keep doing it. They're going to keep making waves. What if
you're the governor of Arkansas and you see that? You might think, hey, that appears to
be a popular position. That might get me votes. Those people are politically active. Maybe
I should shift my stance on it a little bit. Maybe I should become a hardline segregationist.
Then two years later in Little Rock, when Little Rock decides to integrate, that governor
calls out the National Guard. In case you haven't figured it out, this is our timeline.
This is what actually happened. The name of the little town is Hoxie. That's what happened.
So what can we learn from this? What can we learn from this? There's three things. The
first is we need to focus more on teaching the context when we are teaching history,
rather than just the important events. We have to teach the why. Why things happened.
The other thing is that it does show that community networks work. Now in this case,
it was used for evil. It was used by a bunch of guys, most of whom probably have white
sheets hanging in their closet. But it showed how local action can, in fact, shift the views
of state or federal politicians. It shows that. And then it shows one more thing. It
leads into something that's really important right now. And I don't know that a whole lot
of people are going to like this. See that thing in Hoxie, it occurred in 1955. It was
1964 before we got the Civil Rights Act. Almost 10 years. Why? What took so long? What took
so long? If people knew it was right, you know, when the Civil Rights Act was passed,
Americans supported it two to one. Why did it take almost a decade? An additional decade
of suffering. Why? Because they didn't make the case. Because a whole bunch of white liberals,
a whole bunch of white progressives, they were like, yep, that's outside my fence. And
they didn't get involved. They didn't make the case. They went along to get along. They
didn't make the case. They were a lot like that governor. More politically expedient.
And maybe not voice your position too loudly. Because those other people, they're active.
And they're out there making their case. So you should just be quiet. And what ended up
happening was the people that had the least amount of institutional power were left fighting
for themselves. Now granted, yeah, there were definitely white progressives and white liberals
that helped, but nowhere near as many as polls suggest should have been out there helping.
Why is this important? Why does this matter? Because the Democratic Party is considering
doing the exact same thing right now. A whole bunch of positions that are popular in polling,
that are popular among Americans, they're shifting their stance on it. Because a very
vocal minority is out there with their guns screaming about communism. And there's the
idea that they need to move away from anything that could be perceived as woke. That's wrong.
It's morally wrong. When you're talking about topics that are getting viewed as woke today,
they're not political issues. They're moral ones. They're ones you have to make the case
for. You don't have a choice. And I get it. Most of them are concerned about their political
power and their legacy. But one other thing that you can learn from this is that when
history is written, it doesn't record that the governor was a moderate in 1955. It just
records that in 1957, called out the National Guard. Shifting your stance isn't going to
help you. Nobody's going to care what you said with your words last year. They're going
to care about how you acted when it was time to convert those beliefs into action. And
if you decided it was more politically expedient to step away from those causes, the causes
that actually need your support, it's going to be remembered. That's what will be recorded.
When it comes to stuff like this, we have to make the case. You have to be organized.
The reason it took an additional 10 years is because white liberals, white progressives,
who believed one way, didn't convert that into action. They didn't organize. They didn't
fight the fights that needed fighting. They went with what was politically expedient.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}