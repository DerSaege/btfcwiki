---
title: Let's talk about society, laws, and two phrases....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=v4a9QAN-kTE) |
| Published | 2021/11/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Society changes through altering how people think, not just through laws.
- In a representative democracy, representatives should vote according to the majority of their district's wishes.
- Laws don't lead societal change; they enforce changes that have already occurred in society.
- Money often influences representatives' decisions, leading to a higher threshold for societal change.
- Society's shift in thought precedes legal changes, as seen in historical examples like integration.
- The narrative that laws drive societal change is not accurate; societal change primarily stems from shifts in collective thinking.
- Beau contrasts the perception of laws as change agents with examples like the ongoing war on drugs.
- Politicians must advocate for societal shifts before laws can effectively bring about change.
- The battleground for societal change lies in influencing individuals' beliefs.
- Laws without societal support are ineffective, as evidenced by examples like prohibition and the war on drugs.

### Quotes

- "Societal change does not occur in a statute book, it occurs inside the four-inch space inside your skull."
- "Law does not create societal change. Thought does."
- "Before they're ever going to get a law, society has to change."

### Oneliner

Societal change originates from shifting mindsets, not legislative actions; laws enforce, not drive, transformation.

### Audience

Activists, policymakers, citizens

### On-the-ground actions from transcript

- Advocate for societal change by engaging in meaningful dialogues and challenging existing beliefs (implied).
- Support politicians who prioritize representing the majority will of their constituents (implied).
- Educate others on the importance of influencing societal thought for effective change (implied).

### Whats missing in summary

The full transcript provides historical context and modern-day examples to illustrate how societal change precedes legal enforcement and the necessity of influencing beliefs for lasting impact.

### Tags

#SocietalChange #Law #MindsetShift #Advocacy #CommunityLeadership


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about how to change society.
We're going to talk about societal change,
two phrases that I am incredibly fond of, what they mean,
and the law.
And in the process, we're going to discover
that a widely held belief comes from a narrative that
less than accurate. Okay, so I'm very fond of the phrases, you don't need a
law to tell you to be a good person, you don't need a law to tell you to do the
right thing, or to change society, you don't have to change the law, you have to
change the way people think. I've said this a lot, I don't know that I've ever
done a video explaining what I mean by this, and some questions have arisen
isn't about it. So we're just gonna kind of dive into it and take a look at that
idea. You know that, well, we live in a society, right? We live in a society. What
kind? A representative democracy. Now in theory, I understand this isn't the way
it works in practice and we're gonna get to that, but in theory, in a
representative democracy, your representative should never have to
a decision. They never decide anything, not on their own. Their job is to vote the way a majority
of the people in the geographic district that they represent wants. That's it. That's their goal.
That's their job. That's what they're supposed to do. If 51% of people say this, that's the
way they're supposed to vote. If 51% of people are opposed, they should vote in opposition to it.
it. That's how it's supposed to work. So what does that mean in regards to law?
And the idea that law is a vehicle for societal change in this way. By the time
a law gets passed, a majority of society has already changed the way they're
thinking. They've already changed the societal makeup. Society has already
changed by the time the law is proposed because it takes 51%. Law is not a
vehicle for societal change, it's the enforcement tool that gets used to
bring everybody else in line. It doesn't change society, it's an after-effect of
societal change. Now in the way things work in practice in real life today, it's
It's actually even more pronounced because today you've got that money, right?
That money flowing to those representatives and they do make decisions.
They don't just enact the will of the people, they prioritize however they want to.
That money typically flows to keep the status quo in place, so it takes an even higher percentage
of people in a given district to say, this is what I want for it to actually happen.
And I know that this runs counter to a narrative that a lot of people believe that laws are
vehicles for societal change.
In fact, it's the only thing that changes anything, right?
A good example of this is integration.
We have this narrative in our heads September of 1957 in Little Rock.
The Little Rock Nine were going to go to school, and the people, they didn't want that.
Nope.
They were completely opposed to it.
So much so that the law, in the form of the 101st Airborne, had to show up and be like,
yeah, you need to get out of the way.
These kids are going to school.
But is that really what happened, or is that just kind of making it a story rather than
history. The reality is a Gallup poll from September of 1957 said that 51% of
people thought those students should be admitted now. 25% said wait a year and if
you don't know there were some people of the opinion that the goal should be to
diffuse everything first. It was too tense so they needed to wait and they
needed to wait a year. So between wait a year and 51%, these are people who are in
favor of integration, 76% of the population. The people who said never
integrate, 16%. Society had already changed. The law showed up as the
enforcement class to get other people in line. Societal change occurs in thought,
not law, because without the thought the law doesn't exist. You want an example
where that's already the case? A modern one? Look at the war on drugs. 65% of
Americans want it ended. 66% believe that it's time to decriminalize everything.
Yet it's still going on. Society has changed. The law hasn't caught up yet. So
the idea that law is a vehicle for societal change is false. It's not really
how it works. It's just a better storyline to follow along with in history,
but that's not what occurs. This is why it's a bad idea to abandon that
moral battlefield, to not make the case. When politicians decide, oh we're not
We're not going to make arguments in favor of these folk over here.
They're letting them down in more ways than one.
Because before they're ever going to get a law, society has to change.
And if you're not willing to make the case, it never will.
Societal change does not occur in a statute book, it occurs inside the four inch space
inside your skull.
That's where society changes.
And if a law comes along and the case hasn't been made for it, society decides it doesn't
want it, what happens?
They ignore it.
That's what prohibition and the war on drugs taught us.
Law does not create societal change.
Thought does.
Anyway, it's just a thought.
You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}