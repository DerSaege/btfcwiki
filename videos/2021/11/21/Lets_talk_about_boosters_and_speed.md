---
title: Let's talk about boosters and speed....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zPxsbNzIqvo) |
| Published | 2021/11/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The US government is likely to approve booster shots for everyone over 18.
- Booster shots can provide up to 90-95% protection and may be vital in winter.
- Concerns arise about poorer countries having to wait longer for vaccines due to booster distribution in wealthy nations.
- Despite this, individuals are encouraged to get the booster shot promptly to facilitate global vaccine distribution.
- The government prioritizes boosting vaccination rates domestically before focusing on aiding other countries.
- Getting the booster shot quickly may expedite international vaccine distribution efforts.
- Beau compares the situation to putting on your own mask first before helping others on an airplane.
- He acknowledges some issues with the approach but believes it's unlikely to change.
- Beau advises getting the booster shot as soon as possible to increase domestic numbers and support vaccine distribution to countries with limited access.
- Ultimately, Beau concludes by reminding viewers that getting the booster shot is a simple action.

### Quotes

- "It's kind of like putting your own mask on before you help others on an airplane."
- "If a whole bunch of people hold out because they're upset about this, it's going to keep the numbers low in the US."
- "The quickest way for them to get it at this point is going to be for you to get yours."
- "So anyway, it's just a shot."
- "Have a good day."

### Oneliner

The US government is set to approve booster shots, urging individuals to act promptly to aid global vaccine distribution.

### Audience

Global citizens

### On-the-ground actions from transcript

- Get the booster shot quickly to support international vaccine distribution efforts (implied)

### Whats missing in summary

Beau provides insight into the importance of promptly getting a booster shot to support global vaccine distribution efforts.

### Tags

#BoosterShots #Vaccination #GlobalHealth #GovernmentAction #COVID19


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about boosters,
because it looks like that's about to become a reality.
It does appear that the US government
will be approving booster shots for everybody over the age of 18.
Now at time of filming, there's still one small hurdle
to be overcome before it's official,
but I'm pretty sure that's going to happen.
It does appear by all signs that in the next few days,
that approval will come down.
OK, so what does this mean?
It means that you can get a booster shot for your vaccine.
The data suggests it moves you back up
to 90%, 95% protection range, which is good.
The docs think that this will be incredibly important come
winter when everybody's inside.
They're concerned about another surge at that point in time,
and they're hoping that this will help mitigate that.
Everything they're saying makes sense.
This isn't a big deal.
We all had booster shots when we were kids.
Now, the real objection that is coming up
is from people who are concerned about countries that don't
really have good vaccine access yet.
And the concern is that if more powerful, wealthy nations start
distributing boosters now, that those poor countries are
going to have to wait longer.
And they're right.
That's what's going to happen.
That is what is going to occur.
Now, what does that mean?
Does that mean you shouldn't go get it?
Now, of course that's not what it means.
But the reality is that the US has committed
to this course of action.
You not getting it isn't going to help.
In fact, it'll probably slow them
receiving their vaccines down.
The government is committed to this course of action.
That is what it is going to do.
It's going to try to get people to get booster shots.
So if you're going to get one, get one quick.
Get it soon.
That way, the government isn't trying to persuade you
to go get it.
You've already got it.
It's out of the way.
And they're going to see those numbers go up.
They're going to be more likely to get them produced
for the global South, for countries
that aren't as wealthy.
So I know it's counterintuitive because most people,
especially those watching this channel,
want everybody to get vaccinated.
This is one of those things where the government's
committed to this course of action,
and you're not going to be able to change it.
The quickest way for them to get it at this point
is going to be for you to get yours.
Because if a whole bunch of people
hold out because they're upset about this,
it's going to keep the numbers low in the US, which
means they're going to keep more here, which
means they're going to expend energy on trying to get you
to get your booster, rather than starting
to look at other countries and help them.
This is kind of like putting your own mask on before you
help others on an airplane.
Now, it's a bad analogy because the reality is most of us
already have our mask on.
We're just making sure that all the connections fit perfectly.
I do have a small issue with this.
But at the same time, there's nothing
we're going to do to change this.
The way the United States government works,
this is what they're going to do.
They will hold vaccines for people in the US.
Now, if they get a surge of them and then they stop,
they're going to be like, oh, these are all the people
that are really going to get them.
So if you're going to get it, get it
as soon as it's available.
Get it out of the way so those numbers pump up
so they feel more comfortable in releasing more vaccines
to go overseas to countries that still don't have enough.
So anyway, it's just a shot.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}