---
title: Let's talk about Thanksgiving and Sesame Street....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QIsBFta_2lw) |
| Published | 2021/11/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau sets the stage for a Thanksgiving family dilemma involving Ted Cruz, Sesame Street, and social progress.
- A man is reluctant to visit his father for Thanksgiving due to his father's admiration for Ted Cruz and disapproval of Sesame Street's introduction of an Asian-American character.
- The man's wife is Asian, with a family history of internment during World War II, and his father unfairly expects her to represent all Asian people.
- Beau praises Sesame Street as not just a kids show, but a tool for education and addressing difficult topics like food insecurity, addiction, and gender equality.
- He advocates for even more progressive content on Sesame Street, including characters discussing adoption, refugees, health, and gender equality.
- Beau points out that Sesame Street has a long history of addressing challenging topics and being inclusive and diverse.
- He criticizes those opposed to Sesame Street's progressive programming, labeling them as socially regressive individuals who resist change and uphold unjust power structures.
- Beau concludes with optimism, stating that social progress always prevails in the long run, despite resistance from those wanting to maintain the status quo.

### Quotes

- "Sesame Street isn't woke enough."
- "Humanity will crash through it and society will move forward and I am fairly certain that Big Bird and Oscar will be leading the charge."
- "Those who want to uphold the past, the status quo, to keep people down so they can stay comfortable, they always lose."

### Oneliner

Beau navigates a Thanksgiving dilemma involving Ted Cruz, Sesame Street, and social progress, advocating for more progressive content and addressing resistance to change.

### Audience

Viewers, Activists, Parents

### On-the-ground actions from transcript

- Support inclusive and diverse programming like Sesame Street (implied).
- Engage in difficult but necessary conversations with children about social issues (implied).
- Advocate for progressive content that educates and empowers children (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the role of Sesame Street in education and addressing social issues, along with a call for more progressive content on the show.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we have another installment in the video series
getting ready for conversations
that will take place on Thanksgiving.
Today we'll end up talking about Ted Cruz,
Sesame Street, and social progress.
This is prompted by a question that came in,
it's really long, not gonna read the whole thing,
but the summary of it is a guy doesn't wanna go see his dad,
He doesn't want to go see his dad for Thanksgiving because his dad is a huge fan of Ted Cruz.
Ted Cruz ripping America apart one family at a time.
Because he's a huge fan of Ted Cruz, his father has been all over Facebook ranting
and raving about Sesame Street.
The latest complaint is that Sesame Street intends on introducing an Asian-American character.
The person who sent the message, well, their wife is Asian.
And by that, he goes on to clarify
that she grew up in San Francisco
and her family has been here long enough
to have had members of her family interned
during World War II.
Despite length of residence in the United States,
his father is intent on making her the spokesperson
for all Asian people everywhere
anytime the opportunity arises.
Since his wife is going to make him go, he wants ammunition for this conversation that he knows is going to happen.
What he said he wanted was one of those videos where somebody's like,
Hey, when did this become so woke? And I'm like, always has been. You just didn't know it.
And yeah, I mean, that's definitely true of Sesame Street.
But there's something else we have to get into first. What is Sesame Street?
It's a kids show, sure, I mean, no doubt, but it's not just for entertainment, right?
It's for education.
And it teaches not just letters and numbers, but it addresses a lot of difficult topics,
and it has.
It's a tool for parents.
Those who are super involved with their kids, it gives them new framing devices to talk
about difficult subjects.
Those who, because we live in the world we live in, and they have to work 60 hours a
week, can't be involved as much as they'd like, it gives them a tool to kind of reinforce
the conversations that they can have.
And for those kids that have parents who aren't involved at all, it gives them a ready stand
to teach them about the world and have those difficult conversations. That's
what Sesame Street is. It's what it's always been. We're talking about a show
that debuted in the late 60s, early 70s with a ridiculously integrated
neighborhood. I know you're shocked that it is socially progressive gasp, right?
I'm shocked. It was kind of always the point, kind of always the point, to address the issues
that kids today are facing and things that they overhear, whether it be through a phone
call which a kid hears about racism or any of the other things that have been addressed.
To me, Sesame Street isn't woke enough.
If it was me, if I was running Sesame Street, we would have characters that help kids understand
food insecurity, help them understand addiction, help them understand incarceration, homelessness,
adoption, talks about refugees, health in general, gender equality.
We'd have a character with two dads.
We'd have skits that promoted natural hair and getting rid of the idea of Eurocentric
beauty standards, helping people feel normal in their own skin and who they are.
We'd have an Afghan muppet named Zari championing girls' rights, in case you haven't figured
it out.
All of that stuff, that's already Sesame Street.
Sesame Street has a long history of addressing difficult topics, or topics that are seen
as outside the normal conversation, but they know that kids are hearing it.
If you want to know how far this goes, I suggest you look up a character named Cammie.
She had a conversation with former President Clinton.
If you want to know how far Sesame Street goes to introduce difficult topics to kids,
start there.
Sesame Street, in a lot of ways, fills in the gap for parents who are scared to have
these conversations, who don't know how to talk to their kids.
As far as the big complaint of them assigning races to the Muppets, you're a few decades
too late for that. You need to look to Rosita in 1991. That's the first one that I'm aware
of. There's probably some before then. I'm not actually a Sesame Street expert, but I
do know over the years they have always sought to be inclusive and diverse. From the beginning,
The human characters were intentionally diverse.
You're talking about a show that had blind muppets.
You had a dog that spoke sign language, Barkley.
I guess he couldn't bark.
You have an autistic muppet, you have a bunch of stuff.
It's Sesame Street.
It's what they do.
Now the opposition to this, what do you really make of it, you know?
It is what it is.
They were told to be mad about it so they are.
They're easily led astray and this anger and this manufactured outrage gives them an
excuse to be their worst when it comes to being socially regressive and that's what
it is.
That's what it's about.
It's about holding up progress.
about wanting to go back to a different time. Make no mistake about it, the people
who are opposed to Sesame Street's programming today and what they're
teaching today are the same people who would have been opposed to Mr. Rogers
and that pool scene. That's who you're talking about. That's what you're talking
about. You're talking about people that want to uphold the status quo so they
don't want socially progressive ideas. They want to uphold the status quo,
whatever that is, which means if there are power structures that are unjust, by
default these people want to uphold those unjust power structures, right? The
good news is you can look to that Mr. Rogers scene and see how this plays out.
On a long enough timeline, the social progressive always wins. Always. No
exceptions. Those who want to uphold the past, the status quo, to keep people down
so they can stay comfortable, they always lose. They always end up on the wrong
side of history because humanity will always move forward. Humanity will always
progress. It doesn't matter what barriers they erect, what they put in
the way. Humanity will crash through it and society will move forward and I am
fairly certain that Big Bird and Oscar will be leading the charge. Anyway, it's
It's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}