---
title: Let's talk about Manchin and political games....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=p4fvfNKTM_g) |
| Published | 2021/11/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Manchin held an impromptu press conference criticizing progressives for playing political games.
- Beau questions Senator Manchin's framing of himself as not part of the establishment despite being in politics since the late 1900s.
- Senator Manchin is accused of diverting attention, feeling pressure, and playing political games to criticize progressives.
- Beau points out that Senator Manchin has used leverage tactics himself to hold up progress.
- Senator Manchin questions the cost and money behind climate policy and social spending, despite economist endorsements.
- Beau suggests Senator Manchin's reasons for opposing climate policy might be influenced by large contributions from energy sector companies.
- Beau believes that social safety nets for workers may threaten mine companies' ability to mistreat their workers, leading to Senator Manchin's opposition.
- Senator Manchin's biggest donors are revealed to be large investment banks, law firms, and oil and gas companies, not reflective of a regular working man.
- Beau criticizes Senator Manchin for deflecting blame onto progressives and not understanding the stakes of blocking climate legislation and social safety nets.
- Senator Manchin is accused of playing political games and standing in the way of progress, potentially harming the Democratic Party in upcoming elections.

### Quotes

- "Stop playing political games."
- "He's feeling heat. He's feeling pressure."
- "If you didn't want the climate policy and just came out and said, nope, I'm an oil and gas man, can't do that, I could at least respect it."
- "I view it all as one giant political game."
- "Senator Manchin is the person who is standing in the way of progress."

### Oneliner

Beau questions Senator Manchin's motives and actions, accusing him of playing political games while standing in the way of progress and deflecting blame onto others.

### Audience

Progressive activists

### On-the-ground actions from transcript

- Visit opensecrets.org to research and understand contributions made to representatives (suggested).
- Hold accountable politicians who prioritize corporate interests over climate legislation and social safety nets (implied).
- Advocate for transparency in political funding and hold representatives like Senator Manchin accountable for their actions (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of Senator Manchin's actions and motives, shedding light on the influence of corporate funding in politics and the consequences of obstructing climate legislation and social safety nets.

### Tags

#SenatorManchin #PoliticalGames #CorporateFunding #ClimateLegislation #ProgressiveAdvocacy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're gonna talk about
Senator Manchin's little impromptu press conference.
And I get it, he needed to vent,
but I'll be honest, something he said
kinda rubbed me the wrong way.
Left me wondering what dimension he's living in,
because it's certainly not this one.
If you don't know, he held a press conference to come out
and say that progressives needed to stop playing political games.
That in and of itself kind of bothers me.
That sentence, stop playing political games.
You know, it makes it sound like
Senator Manchin isn't a politician.
Like he's not part of the establishment.
Like he didn't enter politics in the late 1900s, 1982.
And I think he was in office all but
five of those years.
be a little off there, makes it sound like he's just some kind of outsider, but we know
that's not the case.
Framing it this way, in and of itself, seems like a political game.
And for somebody who has been in politics this long, we know that they're not actually
adverse to playing political games.
If he is, perhaps he should have chosen a different profession.
It's not that.
He's feeling heat.
He's feeling pressure.
He wants to divert attention.
He's playing a political game.
He's criticizing the progressives for using the leverage they have with the infrastructure
package to get what they want in the climate policy and social spending stuff.
Saying that that's a political game, I would point out that that's the exact same tactic
Senator Manchin's been using this entire time to hold up progress.
So I don't think that's it.
See he's been framing it around the idea that he has questions about the cost, he has questions
about the money behind climate policy and social spending stuff.
Even though I think the White House said 17 Nobel Prize winning economists have signed
off on it, said it would lower inflation.
See I think that's a political game.
I think the reality is he doesn't want climate policy.
And I think he has his reasons.
I think 100,000 of those reasons came from EPP.
I think 82,000 of those reasons came from Energy Transfer LP.
I think if you add in Telluria and Canoco Phillips,
you get about a quarter million reasons.
So much money those companies gave him.
There's more from the energy sector.
I think maybe that's the reason that he
has concerns and questions.
And I think suggesting anything else,
I think that's a political game.
And when it comes to the social spending stuff,
I think we all know that, well, mine companies don't always
treat their workers well.
And them having a social safety net, well, mine companies
wouldn't be able to get away with as much.
I think suggesting anything else would be a political game.
If you ever want to know who has purchased time, influence,
contributed, whatever term you want to use with a
representative, go to opensecrets.org.
And they'll have all the contributors and everything
listed there.
If you go to mansions, the man who often likes to frame
himself as somebody who's paternalistically looking out for the working man, you will
see that his biggest donors are large investment banks, investments in securities, law firms,
and oil and gas companies.
He's a regular working man.
This whole thing is a political game.
designed to get people looking at the progressives who are trying to get much-needed climate
legislation and social safety nets for working class people, trying to turn them into the
villain so King Cole over there can walk away scot-free.
It's the way it seems to me.
That's the kind of political game I see.
The thing is, I don't know that Senator Manchin, after all his years in the establishment,
in politics, playing political games, actually understands the stakes of this.
If this stuff doesn't go through, the Democratic Party is going to have a very hard time in
the midterms and in the next presidential election.
Now in and of itself, that's not a big deal.
That's just politics, political games, right?
But when you are talking about this specific period in time, when you have a Republican
party teetering on the brink of accepting rhetoric and principles that are not within the confines
of a representative democracy, it might be worth paying attention.
Senator Manchin is the person who is standing in the way of progress.
He is the person who is playing political games.
And I don't think anybody should allow him to change that narrative, to change that story,
to deflect blame and point to somebody else.
If you, if he didn't want the climate policy and just came out and said, nope, I'm an oil
and gas man, can't do that, I'll lose my funding, I could at least respect it.
I could at least respect the honesty of it.
to come out and act like you truly care about the people, when you're not doing anything
for the miners, you're not doing anything for the climate, you're not doing anything
for social safety nets, I don't buy it.
And I view it all as one giant political game.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}