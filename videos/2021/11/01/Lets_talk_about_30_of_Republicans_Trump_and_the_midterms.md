---
title: Let's talk about 30% of Republicans, Trump, and the midterms....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3uGV_-uZsM0) |
| Published | 2021/11/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau talks about the distancing occurring between Republican politicians and the individuals who participated in the events of January 6th.
- He mentions that politicians may have to distance themselves from the individuals involved to secure their political future.
- Beau notes the historical consequences of turning on individuals you have previously inflamed.
- Most members of Congress mentioned in a report about organizing rallies that fed into the Capitol events have denied involvement.
- Trump responded to the Washington Post with a statement full of lies, according to the Post.
- Trump's spokesperson referred to the individuals at the Capitol on January 6th as "agitators not associated with the president."
- Lindsey Graham reportedly told the sergeant-at-arms to "take back the Senate" after it had been taken, which may not sit well with the Republican base.
- New polling indicates that 30% of Republicans believe violence may be necessary in the future to save the country, with the number rising to 39% among those who believe the election was stolen.
- Beau points out the decrease in the percentage of Republicans believing violence may be necessary, but the current 30% is still concerning.
- He stresses the importance of reducing this percentage before the midterms, which will require Republicans to take action and change their rhetoric.
- Beau suggests that Republicans linked to the events of January 6th may struggle to rebrand themselves as part of the moral majority.
- He mentions that addressing this issue must come from within the Republican Party and expresses concerns about the potential consequences if it is not addressed.
- Beau shares his interactions with individuals from the political spectrum who were unable to provide clear answers about their intentions regarding the use of guns and potential conflicts.
- He raises concerns about the lack of a clear cause or agenda among those ready to use violence.

### Quotes

- "They don't really have a choice if they want to make it."
- "30% is incredibly high."
- "They're gonna have to tame down their rhetoric."
- "It's worth noting that I think the numbers for independents was 17, and I think for Democrats it was 11%."
- "This is something we gotta keep an eye on."

### Oneliner

Beau talks about the distancing between Republican politicians and individuals involved in the events of January 6th, stressing the need for Republicans to reduce the high percentage of those who believe violence may be necessary in the future.

### Audience

Republicans

### On-the-ground actions from transcript

- Republicans need to take action to reduce the percentage of individuals who believe violence may be necessary in the future. (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the current political landscape, focusing on the divide within the Republican Party and the concerning percentage of individuals who believe violence may be necessary in the future. Viewing the full transcript gives a comprehensive understanding of Beau's insights and concerns. 

### Tags

#PoliticalDivide #RepublicanParty #Violence #Rhetoric #ElectionFraud


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about some new polling,
some new reporting,
some very unscientific, unofficial polling that I did.
And the continued distancing that is occurring
between those in the Republican Party in political office,
whose rhetoric helped inflame people
and led to the events of the 6th,
and the people who were there that day.
We talked about it recently and said that in order for them
to make it politically,
these politicians are going to have to throw these people
under the bus. They're going to have to sell them out.
They don't really have a choice if they want to make it.
And we talked about how that may look
and how that may lead to issues in and of itself,
because generally speaking,
once you kind of inflame people to the point
where they're ready to use violence
and then you turn on them,
it doesn't historically always have a good outcome.
So we talked about how that was going to happen.
That has happened at breakneck speed.
Most, if not all, of the members of Congress
who were mentioned in that reporting
that said that they helped organize
or had something to do with the rallies
that fed the events at the Capitol,
they've mostly come out and said,
oh, that wasn't me.
I didn't know anything about that.
That was my staff. They were acting on their own.
I don't support that.
Under the bus they go.
The Washington Post just put out,
or is putting out a pretty comprehensive report
on the 6th.
They reached out to Trump for comment.
He responded with a giant statement
that they refused to publish
because they say it was full of a bunch of lies.
And given his last old man yelling at clouds
letter to the editor over at the Wall Street Journal,
I'm inclined to believe that it was full of lies.
But his spokesperson said something pretty entertaining.
He said that the people at the 6th were, quote,
agitators not associated with the president.
Even Trump is trying to distance himself now,
at least through his spokesperson.
Now, keep in mind, it wasn't that long ago
that they're trying to turn him into martyrs.
So that's an interesting development.
In that reporting,
another thing that's kind of unrelated to this,
but it was disclosed that apparently Lindsey Graham,
when, after the Senate had been taken,
he looked at the sergeant of arms and said,
what are you doing?
Take back the Senate.
You've got guns, use them.
I'm willing to bet that's not gonna play real well
with the Republican base.
Now, the new polling that came out,
said that 30% of Republicans felt the violence
might be necessary in the future
to save the country or whatever.
That number jumps up to 39%
if you just count those who believe
the election was stolen.
30% is a significant amount.
And I am certain that there are gonna be headlines about it.
It's gonna be turned into a big deal.
And it is in one way or another.
And we're gonna talk about the concerns that exist there.
But something I want to provide for context
is that in February, that number was 39%.
And in September of last year, prior to the election,
that number was 44%.
It's going down.
It's trending the right direction.
So we do have that going for us.
But 30% is incredibly high.
It is incredibly high.
That number has to be reduced before the midterms.
If it doesn't, we're probably in for a bumpy ride.
There will probably be incidents.
That number's got to be reduced.
And the problem is,
it's going to require Republicans to reduce it.
It can't be reduced by Democrats.
It's going to have to be Republicans coming forward
and saying, no, the election wasn't stolen.
No, we're gonna have to do it.
No, we don't need to go this route.
No, blah, blah, blah.
They're gonna have to be the ones to do it.
They're gonna have to tame down their rhetoric.
And many of them are trying in their own way
to at least switch the story to something else.
We recently saw Senator Hawley
give his opinion on masculinity.
That wasn't very well received.
And I think that in many cases,
those closely linked to the sixth
are going to have real issues
trying to rebrand themselves as part of the moral majority
after some of their statements and actions
in regards to the election.
If the Republican Party was smart,
they would encourage a lot of these people
to no longer run, to retire, to leave.
That would be the smart move.
It's unlikely that they do that, though.
So we have to deal with this 30% number.
After the recent question that surfaced
about when do we get to use the guns,
I put a call out on Twitter
and I tried to talk to people
who are from that side of the political spectrum.
And I made it clear up front
that I was gonna ask three questions.
One was, who are they?
Who are you going to use them against?
Them, who is this they people you talk to?
Not a single person I spoke to had an answer.
They didn't know.
I asked how they foresaw the conflict progressing.
Not a single one of them had an answer.
And then the last question was,
what happens after the conflict,
after the guns get used, what happens next?
There was never an answer beyond a slogan,
restore the Republic, quote.
Now, the conventional logic behind this
is that this is good because they lack intent.
They don't have a plan, so on and so forth.
As somebody who has read a little bit
about this type of stuff, this is really bad.
This is bad because they're inflamed to the point
where they are ready to use violence,
but they don't really have a clear cause.
They don't really have a clear agenda,
a plan, anything like that,
which means it is likely to be very indiscriminate.
This is a concern.
And the problem for the rest of the country
is that it has to be addressed
from within the Republican Party.
30%, that number is really high.
It's worth noting that I think the numbers
for independents was 17,
and I think for Democrats it was 11%.
11% is actually still pretty high, to be honest,
but they're all trending in the same direction.
They're trending down.
A third is definitely something
that needs immediate attention,
and I'm concerned that as people like Holly
fail to make the transition to,
well, I'm a member of the moral majority,
that they're going to revert back to the rhetoric
that seemed to work for them before.
This is something we gotta keep an eye on,
especially in the run-up to the midterms.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}