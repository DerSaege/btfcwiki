# All videos from December, 2021
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2021-12-31: Let's talk about a New Year update.... (<a href="https://youtube.com/watch?v=CWJzMIWes0c">watch</a> || <a href="/videos/2021/12/31/Lets_talk_about_a_New_Year_update">transcript &amp; editable summary</a>)

Beau provides updates on channel plans, merchandise caution, and future content, urging viewers to subscribe and stay safe in the new year.

</summary>

"Make your resolution. Have fun. Be safe."
"The only place I have merchandise is on Teespring."
"So just bear that in mind."
"Now, over the next year, you'll probably end up going back to three videos a day."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Wishing a happy new year and giving updates on the channel's direction.
No major changes planned for the main channel as it's working well.
Second channel, "The Roads with Beau," will have more content, including long-format documentaries and how-to videos on various topics like gardening and emergency preparedness.
Viewers are encouraged to subscribe to "The Roads with Beau" for future content.
Patreon subscribers will soon receive an announcement of something special as a token of appreciation.
Cautioning viewers against purchasing merchandise from bootleg companies duplicating Teespring products, as they are of lower quality and more expensive.
Plans to increase video uploads to three a day during midterms, but overall content will remain consistent.
Encouraging viewers to make resolutions, have fun, and stay safe in the upcoming year.

Actions:

for content creators,
Subscribe to "The Roads with Beau" for upcoming content (suggested).
Support Beau's work on Patreon (suggested).
Purchase merchandise only from Teespring to ensure quality (suggested).
</details>
<details>
<summary>
2021-12-31: Let's talk about a British question about statues.... (<a href="https://youtube.com/watch?v=849iARfOWQc">watch</a> || <a href="/videos/2021/12/31/Lets_talk_about_a_British_question_about_statues">transcript &amp; editable summary</a>)

Beau shares humor and history, discussing Benedict Arnold and the irony of traitor statues with a lesson on being on the right side.

</summary>

"History has a sense of humor."
"There are people who see it who don't really understand what it represents because it isn't labeled."
"It's a lot more prominent down here though, and it's nowhere near as funny."
"History does have a sense of humor just like today."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau shares a humorous exchange he had with a British person over Twitter regarding statues of traitors in the South and across the country.
The chat sparks an amusing dive into the story of Benedict Arnold during the American Revolution.
Beau gives a brief overview of Benedict Arnold as a major general in the Continental Army who later became a traitor.
Arnold's betrayal was driven by feelings of being overlooked for promotions and honors.
The story goes that Arnold offered to surrender West Point to the British but ended up with a minor command instead.
An anecdote is shared where Arnold jokes with an American officer about what they'd do if they captured him, referencing his wounded leg.
A statue at Saratoga commemorates Arnold's left leg without explicitly naming him, showcasing history's sense of humor.
Beau humorously appreciates the level of pettiness in creating a statue of Benedict Arnold's leg.
History's irony is noted as Confederate statues are repurposed into memorials for civil rights leaders.
Beau concludes with the importance of being on the right side of history and wishing everyone a good day.

Actions:

for history enthusiasts,
Visit historical sites to learn about significant events and figures (suggested)
Support initiatives that repurpose controversial statues for positive purposes (implied)
</details>
<details>
<summary>
2021-12-30: Let's talk about next year's news cycles.... (<a href="https://youtube.com/watch?v=GBrNKttAfkQ">watch</a> || <a href="/videos/2021/12/30/Lets_talk_about_next_year_s_news_cycles">transcript &amp; editable summary</a>)

Recent stats show news viewership decline, leading to potential rise in sensational content; Americans reportedly more comfortable in 2021 under President Biden.

</summary>

"News outlets are businesses. They have to make money."
"Americans are more comfortable in 2021 than they were in 2020."
"You watch the news because something scary happened."

### AI summary (High error rate! Edit errors on video page)

Recent statistics comparing viewership in 2020 to 2021 show significant drops across major news outlets like CNN, Fox News, MSNBC, ABC, NBC, CBS, New York Times, and Washington Post.
The decline in viewership could lead news outlets to produce more sensationalist content to attract viewers and boost ratings.
News outlets, being businesses, need to increase viewership to stay profitable, possibly by focusing on sensational stories or expanding readership overseas.
Anecdotal evidence from YouTube channels with over 50,000 subscribers shows an overall increase in viewership, possibly due to providing context and moving away from sensationalism.
Americans in 2021 reportedly feel more comfortable and safer under President Biden compared to President Trump, potentially impacting how they respond to news and events.

Actions:

for media consumers,
Support independent media outlets and platforms (suggested)
Seek news sources that provide context and avoid sensationalism (suggested)
</details>
<details>
<summary>
2021-12-30: Let's talk about Biden's economy.... (<a href="https://youtube.com/watch?v=0ihsmA_HUms">watch</a> || <a href="/videos/2021/12/30/Lets_talk_about_Biden_s_economy">transcript &amp; editable summary</a>)

President Biden's economic performance in his first year has exceeded expectations, with significant growth in various metrics, challenging the narrative of a failing economy.

</summary>

"This is probably going to be recorded in the future as the Biden boom."
"The narrative that the economy is circling the drain, well, that's just not true."
"Had Trump even just been average as a president, it wouldn't have been as easy for Biden to make these large gains."

### AI summary (High error rate! Edit errors on video page)

President Biden's first year in office has shown significant economic growth, with gross domestic product doubling the record of the last 40 years.
The U.S. financial markets are outperforming world markets by the largest margin since 2000.
When comparing various economic metrics, Biden's administration ranks number one or two in nine out of ten categories, with per capita disposable income being the only area where he lags behind.
The narrative that the economy is failing under Biden is not accurate, as his administration's economic performance surpasses that of the previous seven presidencies.
While some credit goes to Biden's policies, a significant portion of the economic gains can also be attributed to former President Trump's shortcomings in his last year.
The future economic growth is uncertain without the passing of Build Back Better, as projections have been revised downwards due to political hurdles.
Despite positive economic trends, the dramatic increases seen in Biden's first year may not continue at the same pace in the following year.

Actions:

for economic analysts, policymakers,
Monitor the impact of policy decisions on the economy (suggested)
Advocate for policies that support economic growth (suggested)
Stay informed about economic trends and projections (suggested)
</details>
<details>
<summary>
2021-12-29: Let's talk about Trump losing his base by doing the right thing.... (<a href="https://youtube.com/watch?v=MRO-hHfvYgE">watch</a> || <a href="/videos/2021/12/29/Lets_talk_about_Trump_losing_his_base_by_doing_the_right_thing">transcript &amp; editable summary</a>)

Former President Trump faces backlash from his base for advocating vaccination, potentially paving the way for a new leader within the MAGA movement.

</summary>

"The response to this tweet was basically nothing but condemnation from his supporters, talking about how he sold them out."
"The base that was created through the rhetoric that Trump used to gain power is moving on without him."
"Those who might try to surpass Trump are going to be even more disingenuous than the former president."
"The vaccines work. They provide protection. They save lives. Go get vaccinated."
"Former President Trump has become the semi-reasonable person in the room in a way when it comes to the overall MAGA movement."

### AI summary (High error rate! Edit errors on video page)

Former President Trump has become the voice of reason within the MAGA movement, urging people to get vaccinated despite facing pushback from his base.
Trump's base perceives his support for vaccines as a betrayal and a sign of weakness, leading to condemnations and accusations of selling out.
The base, created through Trump's rhetoric, is now moving on without him, remaining steadfast in their denial of reality and resistance to vaccines.
Despite the backlash, Trump and Marjorie Taylor Greene advocate for vaccination, facing resistance from their base for the first time.
Trump's attempt to encourage vaccination is seen as a departure from his usual rhetoric and may actually benefit the country.
The irony lies in Trump's base turning on him for promoting vaccinations, potentially paving the way for a new leader within the MAGA movement.
Those who prioritize maintaining power through voter suppression are unlikely to advocate for vaccines, leaving their supporters unprotected.
There is a possibility for someone more self-serving than Trump to emerge within the MAGA universe and capitalize on his weakened state.
Individuals seeking to surpass Trump in the movement are warned to be copies of him, potentially with even more disingenuous motives.
Beau stresses the importance of understanding that vaccines are effective, save lives, and urges everyone to get vaccinated.

Actions:

for republicans, maga supporters,
Advocate for vaccination within your community (exemplified)
Encourage others to prioritize public health over political rhetoric (implied)
Support leaders who prioritize the well-being and protection of their followers (exemplified)
</details>
<details>
<summary>
2021-12-29: Let's talk about Rand Paul and voting.... (<a href="https://youtube.com/watch?v=5BvL2XcfU1w">watch</a> || <a href="/videos/2021/12/29/Lets_talk_about_Rand_Paul_and_voting">transcript &amp; editable summary</a>)

Beau explains how increasing voter participation is not stealing an election but a necessity for democracy, criticizing the Republican Party's reliance on voter suppression.

</summary>

"That's not stealing an election, that's winning one."
"They can't win without suppressing the vote."
"It has nothing to do with stealing an election."

### AI summary (High error rate! Edit errors on video page)

Beau addresses Rand Paul's tweet discussing how to "steal an election" by increasing voter participation through absentee ballots, targeting potential voters, and counting votes.
Beau argues that increasing voter participation is vital for a functioning democracy and not equivalent to voter fraud or stealing an election.
He suggests that conservatives like Rand Paul fear increased voter participation because it may shift outcomes against dated policies that benefit the working class.
Beau points out that claims of widespread voter fraud are baseless and used to suppress votes to maintain power.
He mentions gerrymandering as a tactic used by the Republican Party to suppress votes and influence election outcomes in their favor.
Beau criticizes the Republican Party for failing to adapt to societal changes, leading to alienation and loss of voter support.
He explains that Republicans rely on voter suppression tactics because they cannot win elections based on fair participation due to their declining support.
Beau concludes that the Republican Party's resistance to change and reliance on voter suppression tactics stem from their inability to win without such measures.

Actions:

for voters, activists, politically engaged,
Increase voter participation by encouraging absentee ballots and voter registration (implied)
</details>
<details>
<summary>
2021-12-28: Let's talk about the X factor at Burlington.... (<a href="https://youtube.com/watch?v=Fc7XvTKfgyg">watch</a> || <a href="/videos/2021/12/28/Lets_talk_about_the_X_factor_at_Burlington">transcript &amp; editable summary</a>)

Beau stresses the critical need for comprehensive police training to prevent unnecessary use of force and advocates for a shift towards de-escalation strategies over aggressive tactics to ensure public safety.

</summary>

"If you know the outcome of this and you would have done the same thing, you're a pretty horrible person."
"They need that practice. They need to be able to switch, switch modes."
"You're not supposed to be that anyway. You're supposed to be peace officers."
"You are willingly accepting that collateral when there are very, very simple steps to stop that."
"You want to mitigate stuff like this. You want to stop it."

### AI summary (High error rate! Edit errors on video page)

Revisiting a video to address comments questioning the effectiveness of police training and justifying their actions in critical situations.
People argue that cops shouldn't need training to avoid fatal outcomes and defend their actions by claiming they'd do the same.
Emphasizing the importance of training for police to improve decision-making and reduce mistakes in high-pressure situations.
Explaining that training is often lacking in scenarios where officers need to switch from lethal to non-lethal tactics.
Criticizing the lack of comprehensive training programs for police, especially in scenarios involving potential use of force.
Suggesting that training should focus on de-escalation tactics and alternative approaches to handling armed suspects.
Advocating for the implementation of best practices to prevent unnecessary use of force by law enforcement.
Encouraging officers to constantly reassess situations and make informed decisions based on new information.
Arguing that the mindset of "I would have done the same thing" is detrimental and ignores opportunities for improvement through training.
Stating that accepting fatal outcomes as inevitable without exploring preventive measures is irresponsible and callous.
Stressing the need for police to prioritize de-escalation and conflict resolution over a militarized approach.
Urging individuals to rethink their perspective if they believe replicating past fatal outcomes is justifiable or inevitable.
Proposing that officers should be trained to react appropriately in situations involving armed individuals to minimize harm.
Emphasizing the importance of training and practice in preventing unnecessary use of force by law enforcement.
Calling for a shift in police culture towards prioritizing peacekeeping and de-escalation strategies over aggressive tactics.

Actions:

for police departments, policymakers, advocates,
Implement comprehensive and ongoing training programs for law enforcement officers to enhance decision-making skills and prioritize de-escalation tactics (suggested).
Advocate for the adoption of best practices in policing to prevent unnecessary use of force and prioritize community safety (implied).
Support initiatives that focus on turning critical incidents into teachable moments for law enforcement officers to learn and improve their responses (suggested).
</details>
<details>
<summary>
2021-12-28: Let's talk about Burlington and LAPD.... (<a href="https://youtube.com/watch?v=elLXwaz5j5k">watch</a> || <a href="/videos/2021/12/28/Lets_talk_about_Burlington_and_LAPD">transcript &amp; editable summary</a>)

Beau provides a critical analysis of an incident involving police shooting, raising questions about policy, necessity, and the use of lethal force.

</summary>

"It expands it a little bit."
"That's what it demonstrates."
"It's all fun and games until it's real."
"The rounds are going into your kids' bedrooms."
"The question is, was it necessary to shoot?"

### AI summary (High error rate! Edit errors on video page)

Explains the incident at Burlington and the Los Angeles Police Department involving a person with a bike lock hitting people and shots being fired.
Describes cops arriving, suspect leaving and re-entering the building to beat a woman, and cops making a messy entry.
Points out that the cops involved were not a SWAT team but regular police officers.
Details the officer with a rifle firing three rounds, accidentally killing a 14-year-old girl in a dressing room.
Criticizes the media for not accurately portraying what the officers encountered and questioning the officers' behavior.
Raises concerns about the justification for shooting someone without a gun and the lack of de-escalation tactics used.
Suggests that while the officers may be ruled justified within policy, it doesn't mean their actions were necessary or right.
Calls for changes in policy, more training, and a reevaluation of the use of lethal force.
Urges people to watch the body camera footage to understand the reality of such situations.
Warns about the dangers of using firearms in home defense scenarios, citing risks of overpenetration in household layouts.
Questions the necessity of shooting when nobody was in imminent danger, surrounded by cops.

Actions:

for community members, activists, police reform advocates.,
Watch the body camera footage and listen to the officer's voice (suggested).
Advocate for policy changes and more training (implied).
</details>
<details>
<summary>
2021-12-27: Let's talk about questions and the Potter case.... (<a href="https://youtube.com/watch?v=dwYI6tdNrNc">watch</a> || <a href="/videos/2021/12/27/Lets_talk_about_questions_and_the_Potter_case">transcript &amp; editable summary</a>)

Beau questions the impact of the Potter verdict on law enforcement behavior, expressing doubt about significant change and discussing accountability comparisons with medical professionals.

</summary>

"More officers may opt to take plea deals."
"The optimism around significant change is not shared by Beau."
"Comparisons between medical professionals and law enforcement accountability are often misleading."
"Removing qualified immunity for officers is discussed."
"Speculating the future actions of prosecutors based on one incident is seen as unreliable."

### AI summary (High error rate! Edit errors on video page)

Exploring two concepts arising after the Potter verdict and its fallout regarding the future steps and developments.
The first concept is whether the verdict will lead to more aggressive prosecution of law enforcement.
Lawyers may be more likely to take on cases against law enforcement if a strong case can be made.
A possible outcome is that more officers may opt to take plea deals.
The second concept questions whether the verdict will prompt law enforcement to change their ways.
While lessons may be learned, the optimism around significant change is not shared by Beau.
Lessons should focus on addressing the use of force, over-reliance on force, and moving towards consent-based policing.
Beau believes that the main lesson some may take is to not admit wrongdoing on camera.
Beau doubts that changing this behavior will significantly impact future prosecutions.
The idea that doctors don't face charges for mistakes is debunked, as they can be charged with manslaughter.
Comparisons between medical professionals and law enforcement accountability are often misleading.
The concept of removing qualified immunity to hold officers accountable through civil processes is discussed.
The argument that officers should be held personally accountable through civil processes is deemed rare and unlikely.
Beau questions if the Potter verdict will lead to more aggressive prosecution or just plea deals for severe cases.
Speculating the future actions of prosecutors based on one incident is seen as unreliable.
Beau concludes by expressing doubt that this case alone will bring about significant change in law enforcement behavior.

Actions:

for reform advocates, legal professionals.,
Advocate for reforms to address over-reliance on force and move towards consent-based policing (implied).
Support efforts to remove qualified immunity for law enforcement officers (implied).
</details>
<details>
<summary>
2021-12-27: Let's talk about political theories of everything.... (<a href="https://youtube.com/watch?v=RPl7t8kRCTI">watch</a> || <a href="/videos/2021/12/27/Lets_talk_about_political_theories_of_everything">transcript &amp; editable summary</a>)

Beau explains the dangers of falling for grand conspiracy theories, urging people to prioritize real connections over internet theories. The US will grapple with the fallout from these theories for a long time.

</summary>

"There is no man behind the curtain."
"It's the ultimate reduction when it comes to trying to understand which way the world actually works."
"It's comforting because you feel you have a handle on it."
"I kind of started scrolling and going through different threads where people are believers in these theories."
"The US is going to be dealing with the fallout from this for a really long time."

### AI summary (High error rate! Edit errors on video page)

Describes a common trend of trying to tie together various aspects of life into a unified conspiracy theory due to fear of chaos and disorder.
Shares a personal anecdote of someone who spiraled down a conspiracy theory and lost everything in their life.
Points out the fallacy in believing that a single group of people controls everything behind the scenes.
Compares the idea of a grand conspiracy to bumper sticker politics and sloganism for oversimplifying complex issues.
Emphasizes that the world is chaotic and scary, but believing in a grand conspiracy is reductionist and misleading.
Warns about the steep costs individuals may pay to support such theories and urges people to prioritize real connections over internet theories.
Expresses concern about the long-lasting impact of these conspiracy theories on society.

Actions:

for internet users,
Reconnect with family and friends for comfort and support (implied)
Encourage critical thinking and fact-checking to avoid falling for harmful conspiracy theories (implied)
Support individuals who may be entangled in conspiracy theories with empathy and understanding (implied)
</details>
<details>
<summary>
2021-12-26: Let's talk about origin stories and you.... (<a href="https://youtube.com/watch?v=3ACwRQPrVzc">watch</a> || <a href="/videos/2021/12/26/Lets_talk_about_origin_stories_and_you">transcript &amp; editable summary</a>)

Beau clarifies he didn't create the term's current usage but acknowledges his audience's role in transforming it into a force for good.

</summary>

"How does it feel to create something that people use as their standard of morality? Wow."
"If you have the means, you have the responsibility."
"Y'all turned it into the force for good."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the origin of a term and its current usage, pointing out a common error he noticed.
While assisting with tornado relief efforts in Kentucky, Beau encountered someone using the term "rule 303" frequently.
This encounter led Beau to research the phrase, discover people doing positive things, and eventually find his channel.
Beau expresses his interest in parasocial relationships and how influencers and their audience interact.
He clarifies that he did not create the current usage of the term in question.
Beau advocates for servant leadership and taking action to make the world a better place if you have the means.
He explains that the term's origin, involving military contractors and law enforcement, was not intended to describe moral standards.
Beau acknowledges that his audience transformed the term into a force for good, not him.
He mentions that his early video, "Let's Talk About Rule 303," did not suggest the term should be used as it is today.
Beau credits his audience for turning something of questionable origin into a positive force and expresses curiosity about their future endeavors.

Actions:

for content creators,
Create content that inspires positive change (exemplified)
Amplify positive initiatives within your community (exemplified)
</details>
<details>
<summary>
2021-12-26: Let's talk about Brandon, Biden, and Jared.... (<a href="https://youtube.com/watch?v=cLM5Nj2IfYA">watch</a> || <a href="/videos/2021/12/26/Lets_talk_about_Brandon_Biden_and_Jared">transcript &amp; editable summary</a>)

Beau addresses the "Let's go Brandon" trend, advocating for meaningful communication instead of wasted opportunities like Jared's frivolous chant.

</summary>

"Don't be like Jared."
"It's irrelevant. It's silly. It's a real-life version of Orange Man bad."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the trend of saying "Let's go Brandon" as a way to express displeasure with President Biden within the conservative circle.
He acknowledges that criticizing leaders, including President Biden, is valid but suggests doing it with actual criticism rather than juvenile chants.
Beau finds humor in the irony of people who criticized liberals for saying "orange man bad" now using a similar chant against Biden.
The incident that prompted the recent surge in "Let's go Brandon" chants involved President Biden responding casually to a family member saying it on a call during a Santa tracker event.
Beau speculates on the various interpretations of Biden's response, ranging from being classy to being unaware of the context.
He shares his belief that Biden's PR team likely informed him of the chant previously, but it may have been forgotten due to its irrelevance.
The main takeaway from Beau is to not waste opportunities for meaningful communication like the individual, Jared, who said "Let's go Brandon" in a moment with a wide audience.
He encourages thinking about what one might say if given a platform to reach millions and avoiding wasting such a chance.
Beau expresses disappointment in the missed potential for Jared to advocate for something he deeply cares about instead of uttering a seemingly frivolous statement.
In summary, Beau views the whole "Let's go Brandon" situation as irrelevant and a missed chance for constructive communication.

Actions:

for social media users,
Advocate for meaningful messages when presented with a platform (implied)
Think about what you want to convey to millions before speaking in public (implied)
</details>
<details>
<summary>
2021-12-25: Let's talk about a lighthearted Q and A.... (<a href="https://youtube.com/watch?v=9hHN-m5Lj-w">watch</a> || <a href="/videos/2021/12/25/Lets_talk_about_a_lighthearted_Q_and_A">transcript &amp; editable summary</a>)

Beau engages in a light-hearted Q&A session covering topics from favorite board games to barbecue preferences, showcasing his casual and friendly demeanor.

</summary>

"I guess two because my wife gives everybody pajamas because she wants us all to look the same."
"I honestly never cared, to me they're fun, you know."
"Some people might say this is a very serious question. What is your favorite regional style of barbecue?"
"It's not looking good."

### AI summary (High error rate! Edit errors on video page)

Beau engages in a lighthearted Twitter Q&A session for Christmas, answering a variety of questions from his followers without prior knowledge of the queries.
Questions range from serious advice to playful banter, covering topics such as favorite board games, beard care routines, favorite Christmas traditions, and even hypothetical scenarios like meeting fictional characters.
Beau shares personal anecdotes, hints at future content for his channel, and provides glimpses into his family life and hobbies.
The transcript captures Beau's casual and friendly demeanor as he navigates through the amusing and thought-provoking questions from his audience.
Beau's responses showcase his sense of humor, easygoing nature, and willingness to share glimpses of his life with his viewers.

Actions:

for content creators and fans,
Find ways to incorporate humor and lightheartedness into your interactions with your audience (implied).
Share personal anecdotes and stories to connect with your viewers on a deeper level (implied).
Be open to answering a diverse range of questions from your audience to foster engagement and connection (implied).
</details>
<details>
<summary>
2021-12-24: The roads to the miners in Brookwood, Alabama.... (<a href="https://youtube.com/watch?v=jQ7FBjrnSqk">watch</a> || <a href="/videos/2021/12/24/The_roads_to_the_miners_in_Brookwood_Alabama">transcript &amp; editable summary</a>)

Beau catches up on events in Canada and Alabama, showcasing the power of community support and organization in times of need.

</summary>

"If you have the ability to foster that kind of camaraderie, that kind of community, that kind of network around you, it can only make your community better."
"It's worth doing because you can weather storms, whether they be hurricanes or they be, well, other kinds of issues that impact the entire community."
"Solidarity Santa came through. And you all played a part in that."
"That's kind of the way the history of the labor movement in the United States works."
"It makes it a whole lot easier to bargain for what you want and to create the kind of world you want."

### AI summary (High error rate! Edit errors on video page)

Updates on events in Canada, Alabama, and another location.
Money raised for a homeless camp in Canada and children of minors on strike in Alabama.
Money sent to Aaron for shopping in Canada; photos posted on Twitter.
Heartwarming experience in Canada, where Beau was invited to have a meal.
Beau drove to Brookwood, Alabama, for the minor strike.
Signs of community support with yard signs all over town.
Visited a meeting hall with organized supplies for strikers.
Resemblance of the supply depots after Hurricane Michael.
Importance of organization for long-term strikes.
Stories shared by people who worked in the mine since the 1980s.
Sense of community and support during strike rally.
Lack of support from US politicians for the labor movement.
History of labor movements in the United States being cyclical.
Every kid who needed help received it during the events.
Emphasis on community building and organization for a better society.

Actions:

for community organizers, activists, supporters,
Support local community initiatives (implied)
Foster camaraderie and networks in your community (implied)
Organize aid drives for those in need (implied)
</details>
<details>
<summary>
2021-12-24: Let's talk about fun supply chain chaos on Christmas.... (<a href="https://youtube.com/watch?v=pv8lP1AtUYA">watch</a> || <a href="/videos/2021/12/24/Lets_talk_about_fun_supply_chain_chaos_on_Christmas">transcript &amp; editable summary</a>)

Beau navigates supply chain issues to ensure teens in shelters receive gifts, showcasing community's generosity and problem-solving spirit.

</summary>

"That problem, we're going to fix it."
"Y'all have taken a channel that really did start as kind of a joke and turned it into a real force."
"This is the type of thing that can happen when people come together."

### AI summary (High error rate! Edit errors on video page)

Annual fundraiser for teens in domestic violence shelters during the holidays, ensuring they receive gifts specifically for older kids who are often overlooked.
Unexpectedly, the shelter had no teens this year, a first-time occurrence prompting redirection to another shelter.
The journey to fill bags with gifts turned complicated due to supply chain issues and scarcity of items like tablets, accessories, and consoles.
Despite challenges, the community's generosity and support made it possible to provide gifts and contribute $5,000 for teen programs.
Beau expresses gratitude for the community's support and teamwork, turning a channel initially seen as a joke into a force for good.

Actions:

for community members, supporters.,
Support fundraisers for teens in domestic violence shelters (suggested).
Donate gifts or funds to shelters supporting older kids during the holidays (implied).
Contribute to teen programs by donating or volunteering (implied).
</details>
<details>
<summary>
2021-12-23: Let's talk about when you aren't the target.... (<a href="https://youtube.com/watch?v=45YNPPZlMv8">watch</a> || <a href="/videos/2021/12/23/Lets_talk_about_when_you_aren_t_the_target">transcript &amp; editable summary</a>)

Beau recounts a story of police interaction, paralleling it with animal behavior to illustrate privilege and understanding others' fears.

</summary>

"If you see somebody who is concerned about a certain thing and it just doesn't register with you why they're worried, it might be because you're not the target."
"Seems like that might be a reality that some people need to accept."

### AI summary (High error rate! Edit errors on video page)

Recounts a story about a friend and another friend getting pulled over in a remote area in Alabama, where the driver, who is black, immediately displayed fear and compliance towards the police.
Observes various animals in a peaceful setting, including bunnies, squirrels, and birds, with a horse nearby.
Notes that all the animals, except for the horse and himself, get spooked and scared when a hawk flies overhead.
Draws a parallel between the fear response of the animals to the presence of a predator and the fear experienced by individuals who are typically targeted by authorities.
Suggests that understanding others' fears and concerns requires acknowledging one's own privilege and position in society.
Concludes with a reflection on the need for empathy and acceptance of differing experiences and perspectives.

Actions:

for empathy seekers,
Acknowledge and validate the fears and concerns of those who may be targets of discrimination or profiling (implied).
</details>
<details>
<summary>
2021-12-23: Let's talk about Biden, Hunters, and trophies.... (<a href="https://youtube.com/watch?v=RqlWcFH1Ti8">watch</a> || <a href="/videos/2021/12/23/Lets_talk_about_Biden_Hunters_and_trophies">transcript &amp; editable summary</a>)

Beau suggests Biden take executive action to ban imports of wildlife trophies, a move that could yield wins amidst congressional challenges.

</summary>

"This could give Biden some wins."
"The administration is going to have to start thinking outside the box if it wants to get anywhere."
"It's just a thought."
"You're not going to get a lot of pushback either."
"You all have a good day."

### AI summary (High error rate! Edit errors on video page)

Boris Johnson proposed banning imports of trophies from at-risk animals, a move supported by Beau.
The United States imports more wildlife trophies than any other country due to exemptions in the Endangered Species Act.
Wealthy individuals can pay for permits to hunt endangered species under the guise of conservation.
These "canned hunts" often involve hunting animals in enclosed spaces or with no natural fear of humans.
Biden could make changes to the Endangered Species Act through executive order to ban such imports.
Biden faces challenges in Congress due to Republican obstructionism and may need wins to show progress.
Taking action through executive orders could pave the way for future legislative changes.
Democrats need to push through wins to show progress before the midterms.
Beau suggests Biden should focus on achievable causes like banning imports of wildlife trophies.
Thinking creatively and acting promptly is necessary for the administration to make progress.

Actions:

for conservation advocates,
Ban imports of trophies from at-risk animals through executive order (suggested)
Push for wins through achievable causes (implied)
</details>
<details>
<summary>
2021-12-22: Let's talk about when nobody else is following Rule 303.... (<a href="https://youtube.com/watch?v=F5yK7s27iuw">watch</a> || <a href="/videos/2021/12/22/Lets_talk_about_when_nobody_else_is_following_Rule_303">transcript &amp; editable summary</a>)

Beau addresses moral dilemmas, stressing integrity, taking breaks, and continuing to do good even when faced with apathy.

</summary>

"Integrity is doing the right thing when nobody else is."
"Sometimes you're not going to be able to change the world, but you should do everything you can to make sure that the world doesn't change you."
"It's okay to take a break, you're allowed, everybody's allowed."

### AI summary (High error rate! Edit errors on video page)

Beau receives a message with philosophical and moral questions about following a rule when no one else around honors it.
The message raises questions about volunteering at the hospital to honor a rule (Rule 303) and potentially exposing a dying father to COVID.
Beau talks about the importance of maximizing good even when others don't seem to care or want it.
He addresses the immediate question of continuing to volunteer in a risky situation when caring for an immunocompromised father.
Beau stresses the concept of integrity, doing the right thing even when nobody else is, regardless of recognition or approval.
Taking a break is encouraged to prevent burnout and enjoy time with loved ones.
He acknowledges the importance of perseverance in doing good, even when faced with apathy from others.
Beau reassures that volunteers will still be needed and encourages staying true to one's values.
The message ends with a reminder to not let the world change you, even when faced with challenges.
Beau advocates for taking breaks but getting back in the fight to continue making a difference.

Actions:

for helpers and volunteers,
Take a break to prevent burnout and spend quality time with loved ones (implied)
Continue volunteering if possible, but know it's okay to take a break (implied)
Stay true to your values and continue doing good even when faced with apathy (implied)
</details>
<details>
<summary>
2021-12-22: Let's talk about giving Trump a boost.... (<a href="https://youtube.com/watch?v=os2TQ1IOQrc">watch</a> || <a href="/videos/2021/12/22/Lets_talk_about_giving_Trump_a_boost">transcript &amp; editable summary</a>)

Former President Trump's shift towards promoting vaccination reveals the consequences of political inaction and the necessity for prioritizing public health for the common good.

</summary>

"All he did was give them permission to be their worst."
"He's doing this because it's finally impacting him."
"Getting vaccinated is something that will help."
"It still mitigates a whole lot."
"It's still very worth getting."

### AI summary (High error rate! Edit errors on video page)

Former President Trump is on tour, and there are moments where he's been booed for mentioning his vaccination status.
Trump's attempts to encourage people to get vaccinated indicate a shift in his approach, as he never really asked his base to do much beyond giving them permission to act negatively.
The necessity for Trump to tour and push vaccination is a result of his administration's failure in managing public health, leading to a situation where the Republican Party struggles to compete without resorting to questionable tactics like gerrymandering.
There seems to be a disconnect between the actions of the Republican Party and the interests of their voters, especially regarding public health measures and vaccination.
Beau questions why Republicans would support a party that has lost so many voters due to their inaction and downplaying of critical issues.
Trump's motivations for promoting vaccination now seem more self-serving, as it directly impacts him and his need to retain influence.
The emphasis on vaccination as a life-saving measure, even if not perfect against all variants, is stressed as a critical step towards public health protection and overall benefit to the country.
Beau calls for those who have previously followed harmful rhetoric to now take positive action in getting vaccinated for the greater good.
There's a suggestion that had Trump been more proactive in promoting vaccination and public health measures from the beginning, his current efforts might have been more successful.
The overall theme revolves around the consequences of political inaction and the necessity for individuals to prioritize public health for the common good.

Actions:

for republicans,
Get vaccinated for the common good (implied)
Support public health measures (implied)
</details>
<details>
<summary>
2021-12-21: Let's talk about the op-ed from the generals.... (<a href="https://youtube.com/watch?v=VUTuBI3j2pg">watch</a> || <a href="/videos/2021/12/21/Lets_talk_about_the_op-ed_from_the_generals">transcript &amp; editable summary</a>)

Beau breaks down concerns from generals' op-ed on 2024 elections, downplaying military involvement fears but supporting suggested actions for transparency and accountability.

</summary>

"They got the math wrong, but they got the right answer."
"This isn't something I would be too concerned about at the moment."
"Their suggestions are really good, all of them."
"DOD is typically an organization that does not change until it fails."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Three generals penned an op-ed expressing concerns about the 2024 elections and the potential involvement of elements within the U.S. military in supporting a losing candidate who refuses to concede.
The generals' concerns are based on the fear that some military units may go rogue and not respect the election outcome.
They point to statistics suggesting that 10% of the individuals involved in the Capitol incident were service-connected, but Beau questions the significance of this figure.
Beau argues that the demographic of veterans among the general population is not significantly higher than expected, downplaying the relevance of the mentioned stat.
Beau is not alarmed by the support of some former military officials for Trump, as most of them lack significant influence over current military institutions.
The idea of lower-level officers leading their units rogue is seen as a legitimate concern by Beau, but he does not believe it should raise major alarms.
The generals suggest various actions to mitigate their concerns, including holding leaders of the Capitol incident accountable, providing constitutional training, and conducting more counterintelligence checks within the military.
Beau agrees with the suggested actions, viewing them as necessary regardless of the specific concerns raised.
He expresses more concern about non-military organizations supporting a potential insurrection rather than military involvement.
Beau believes the generals may be exaggerating their concerns to push for necessary changes within the Department of Defense (DOD) and suggests that monitoring the situation is prudent but not cause for immediate panic.

Actions:

for political analysts,
Hold leaders accountable for incidents like the Capitol breach and push for Department of Justice action (suggested).
Implement ongoing training on the Constitution, civic duty, and refusal of illegal orders within the military (suggested).
Conduct more counterintelligence checks on military personnel to ensure transparency and mitigate risks (suggested).
Combat disinformation within military ranks through proactive measures (suggested).
Advocate for war gaming insurrections and coups after thorough counterintelligence checks (suggested).
</details>
<details>
<summary>
2021-12-21: Let's talk about Sherman and Atlanta.... (<a href="https://youtube.com/watch?v=SB0xZWyQHkE">watch</a> || <a href="/videos/2021/12/21/Lets_talk_about_Sherman_and_Atlanta">transcript &amp; editable summary</a>)

Beau debunks common myths surrounding General Sherman's march, showcasing the complex reality behind historical narratives and the heavy toll of civil conflict.

</summary>

"Almost immediately spin comes into it, and it obscures the facts."
"The accuracy of historical figures' stories is normally not accurate."
"If you're going to get to the bottom of something, you need to do it quickly."
"At the end of it, it's normally the civilians that pay the heaviest price."
"The other reason I'm bringing this up today is because it happened today."

### AI summary (High error rate! Edit errors on video page)

Debunks the inaccurate beliefs surrounding General Sherman's infamous march.
General Sherman's actions were not as commonly portrayed; the reality is different.
Sherman's march wasn't just about burning everything in his path.
September 2nd marked the fall of Atlanta, leading to Sherman's strategic decisions.
Sherman divided his troops and executed a plan to sustain his campaign.
Atlanta was partially destroyed before the march to the sea commenced.
The march involved battles and strategic acquisition of supplies.
Sherman's aim was to disrupt morale in the South.
Many of the destructive actions were carried out by Confederate troops, not solely Sherman's army.
The liberation of slaves was a part of Sherman's campaign, with complex consequences.
Special Field Order Number 15 granted land to refugees following Sherman.
Historians debate whether Sherman's actions constitute total war.
The accuracy of historical narratives is often distorted by immediate spin and political agendas.
Civil conflicts have severe consequences, especially for civilians.
The transcript coincides with the historical end date of Sherman's campaign.

Actions:

for history buffs, educators, civil war enthusiasts,
Research and learn about historical events from multiple perspectives (suggested)
Support accurate historical education and narratives in schools and communities (exemplified)
Engage in civil discourse about complex historical figures and events (implied)
</details>
<details>
<summary>
2021-12-20: Let's talk about the Newsweek article and Trump.... (<a href="https://youtube.com/watch?v=-faO1Vc9-FI">watch</a> || <a href="/videos/2021/12/20/Lets_talk_about_the_Newsweek_article_and_Trump">transcript &amp; editable summary</a>)

Newsweek suggests millions could seize power if Trump loses, but fear may encourage destructive outcomes; building, not destroying, makes the country better.

</summary>

"You don't make your country great by destroying stuff. You get it by building."
"If you want to be a tough guy, destruction is easy, building, making the country better, that's hard."
"Scaring people into believing Trump must win is not beneficial."
"You won't fix the country by destroying stuff."
"Being alarmed over this possibility now is very self-defeating."

### AI summary (High error rate! Edit errors on video page)

Newsweek article suggests millions of Americans could seize power if Trump loses in 2024.
Social media posts and events with weapons influence this narrative of millions of Americans ready to act.
Only 0.6% of the population represents millions of Americans, creating a magnified fear.
Recent polling shows only a quarter of Americans want Trump to run in 2024.
Winning comfortably in 2024 may not be feasible for Trump, and his base is aging.
Putting Trump back in office could signal the end of democracy and the American experiment.
Scaring people into believing Trump must win by a large margin is not beneficial.
Building and fixing problems are more effective than destruction for making the country better.
Macho banter and tough talk won't solve the issues; constructive actions are needed.
Encouraging outcomes presented by the article through alarm may be counterproductive.

Actions:

for concerned citizens,
Build and work towards fixing problems in your community (exemplified)
Encourage constructive actions over destructive behavior (exemplified)
</details>
<details>
<summary>
2021-12-20: Let's talk about BBB, the GDP, and Manchin.... (<a href="https://youtube.com/watch?v=FQxbTSggnDM">watch</a> || <a href="/videos/2021/12/20/Lets_talk_about_BBB_the_GDP_and_Manchin">transcript &amp; editable summary</a>)

Senator Manchin's opposition to Build Back Better will negatively impact the US economy for at least nine months, prioritizing political gain over billions of dollars in economic growth.

</summary>

"Failing to pass this will negatively impact the United States' economy for at least nine months."
"The Republicans want the United States to fail right now because then you'll blame Biden."
"Y'all better go buy some coal."
"That might should be the talking point."
"Never let these people fool you into thinking they care about you again."

### AI summary (High error rate! Edit errors on video page)

Senator Manchin announced his opposition to Build Back Better, siding with Republicans, impacting the bill's passing outlook.
Wall Street's concern was negative growth implications due to failure to pass Build Back Better, leading to revisions in expected GDP growth.
Goldman Sachs had to reduce expected GDP growth for three quarters due to the detrimental impact of not passing the bill.
The decision will negatively impact the US economy for at least nine months, reducing growth projections significantly.
Failing to pass Build Back Better will reduce economic output growth by a third, affecting the US economy by billions of dollars.
This decision prioritizes causing the US to fail to blame Biden over helping those in need.
The timing of this decision, right before Christmas, affects critical issues like tax credits that impact the most vulnerable.
The Republican Party and Senator Manchin are portrayed as disregarding the needs of the poor in favor of political gain.
The decision not to pass the bill is estimated to cost the United States tens of billions of dollars in economic growth.
Goldman Sachs suggested that corporate tax rates probably won't increase, providing a silver lining amidst the negative impacts.

Actions:

for politically engaged individuals,
Contact your representatives to advocate for policies that prioritize economic growth and support vulnerable populations (suggested).
Join organizations focused on economic justice and advocacy for those in need (implied).
</details>
<details>
<summary>
2021-12-19: Let's talk about reforming or replacing the CIA.... (<a href="https://youtube.com/watch?v=C9CRDYKJzA4">watch</a> || <a href="/videos/2021/12/19/Lets_talk_about_reforming_or_replacing_the_CIA">transcript &amp; editable summary</a>)

Beau weighs the challenges of replacing or reforming the CIA, ultimately arguing that true change requires shifting from competitive to cooperative international relations.

</summary>

"The behavior of intelligence agencies all over the world is a symptom of the problem. It's not the problem itself."
"Until nation states cease to be or that international poker game where everybody's cheating, until that changes into everybody sitting at the table playing with Legos and trying to build something together, that's what intelligence agencies are going to be."
"Anything short of that, it's a temporary fix at best by the very definition of intelligence work."

### AI summary (High error rate! Edit errors on video page)

Proposes a hypothetical scenario where a group of people is discussing how to address issues with the Central Intelligence Agency (CIA).
Considers two options: replacing the CIA with a new agency or reforming the existing one.
Explores the challenges of replacing the CIA, including the transfer of institutional memory and potential continuity of questionable activities.
Examines the limitations of reforming the CIA, focusing on its core mission of intelligence gathering.
Contemplates the option of not having an intelligence agency at all, but acknowledges the political reality that necessitates such an agency.
Argues that the behavior of intelligence agencies worldwide is a symptom of deeper issues related to international affairs and foreign policy.
Emphasizes that intelligence agencies are designed to secure the power and position of their home countries in a competitive international environment.
Suggests that true change can only come from shifting towards a more cooperative international framework, reducing nationalism and competition.
Concludes that any temporary fixes to intelligence agency issues are insufficient without addressing the underlying competitive nature of international relations.

Actions:

for policy makers, activists, advocates,
Advocate for a shift towards cooperative international relations by engaging in diplomacy and promoting collaboration (implied).
Support policies that prioritize cooperation over competition in foreign affairs (implied).
</details>
<details>
<summary>
2021-12-19: Let's talk about China and Russia helping each other.... (<a href="https://youtube.com/watch?v=JVWzCT6t-E4">watch</a> || <a href="/videos/2021/12/19/Lets_talk_about_China_and_Russia_helping_each_other">transcript &amp; editable summary</a>)

Beau explains the complex dynamics of advanced foreign policy between China, Russia, and the United States, indicating a lack of desire for direct conflict despite geopolitical posturing.

</summary>

"It's geopolitical posturing. It's framing. There's not really the desire to go to war from any party."
"They know what they're doing."
"It's better to be up against somebody who has been there."
"I don't see a direct contest between these nations occurring."
"They don't really want it to occur."

### AI summary (High error rate! Edit errors on video page)

Explains the advanced foreign policy dynamics between China, Russia, and the United States.
Points out that the drum beating and nationalistic framing are not indicative of actual intentions.
Mentions that China and Russia do not want war with the United States due to high risks involved.
Suggests that in hotspot situations, a power might back down to avoid conflict.
Notes that both China and Russia have reasons to test U.S. resolve, particularly due to past administration actions.
States that politically, China and Russia might back each other up, but limited involvement in small conflicts is expected.
Anticipates diplomacy power moves, sanctions, espionage, covert operations, and possibly proxy wars, but a direct conflict is unlikely.
Emphasizes the importance of mature intelligence agencies in preventing major conflicts.
Concludes that the geopolitical posturing seen is not a desire for war but rather strategic framing.

Actions:

for foreign policy analysts,
Monitor diplomatic relations and actions between countries (implied)
Advocate for peaceful resolutions to international conflicts (implied)
Support efforts to prevent escalation through diplomacy and communication (implied)
</details>
<details>
<summary>
2021-12-18: Let's talk about new polling and bad news for republicans.... (<a href="https://youtube.com/watch?v=GXq5OK-EBY0">watch</a> || <a href="/videos/2021/12/18/Lets_talk_about_new_polling_and_bad_news_for_republicans">transcript &amp; editable summary</a>)

New polling shows a significant vaccination divide between Trump and Biden voters, posing trouble for the Republican Party as increasing opposition may lead to surprise victories for Democrats.

</summary>

"40% of Trump voters are unvaccinated, compared to only 7% of Biden voters."
"There are going to be surprise victories for Democrats because of these numbers."
"It's unnecessary, and these numbers are getting bigger."

### AI summary (High error rate! Edit errors on video page)

New polling reveals trouble for the Republican Party due to a significant disparity in vaccination rates between Trump and Biden voters.
The poll focuses on whether the government should encourage vaccination, not on mandates or policies.
40% of Trump voters are unvaccinated, compared to only 7% of Biden voters.
Republicans are becoming more opposed to encouraging vaccination, with the number rising from 35% to 48% in three months.
Democrats and independents show higher support for vaccination encouragement compared to Republicans.
The increasing opposition among Republicans may lead to surprise victories for Democrats in elections.
The trend of more people opposing vaccination encouragement is expected to continue.
The disparity in vaccination rates between party supporters may impact future elections significantly.

Actions:

for voters, party supporters,
Encourage vaccination within your community (suggested)
Support positive messaging around vaccination (implied)
Advocate for bipartisan efforts to increase vaccination rates (implied)
</details>
<details>
<summary>
2021-12-18: Let's talk about foreign policy concepts you'll need soon..... (<a href="https://youtube.com/watch?v=FvEu6MQUWK4">watch</a> || <a href="/videos/2021/12/18/Lets_talk_about_foreign_policy_concepts_you_ll_need_soon">transcript &amp; editable summary</a>)

Russia and China's alliance against NATO, setting the stage for a new Cold War framed around democracy versus authoritarianism, may resurrect historical terminology with different connotations.

</summary>

"The new Cold War will likely be framed as democracy versus authoritarianism."
"The future of foreign policy seems to be shaping into this new Cold War scenario."
"The essence of alignment with democracy or authoritarianism will remain central in the new global order."

### AI summary (High error rate! Edit errors on video page)

Russia and China are holding video conferences and expressing great friendship and cooperation, positioning themselves against NATO's expansion.
The world is dividing into blocs closely allied with the United States, Russia, and China, setting the stage for a new Cold War.
The new Cold War will likely be framed as democracy versus authoritarianism, with nations choosing sides.
The historical terms like First World, Second World, Third World, and Fourth World may resurface with different connotations in the context of this new global alignment.
Third World nations historically referred to non-aligned countries, not necessarily poor or developing nations.
Fourth World used to denote indigenous cultures, often marginalized and not active players in global politics.
The future of foreign policy seems to be shaping into this new Cold War scenario, with nations like the UAE caught in the middle of choosing sides.
The US government's attempt to ensure UAE's alignment with the democracy team has caused a hiccup in a fighter jet deal due to national security concerns.
The political dynamics around these alliances and alignments will play a significant role in shaping the global landscape.
While new terminology may replace the old classifications, the essence of alignment with democracy or authoritarianism will remain central in the new global order.

Actions:

for foreign policy analysts,
Monitor geopolitical developments closely and stay informed about global alliances and alignments (implied).
Advocate for diplomacy and peaceful resolutions in international conflicts (implied).
Support indigenous cultures and marginalized communities in the face of global power struggles (implied).
</details>
<details>
<summary>
2021-12-17: Let's talk about Republicans winning over Biden.... (<a href="https://youtube.com/watch?v=HJEB0SH62O0">watch</a> || <a href="/videos/2021/12/17/Lets_talk_about_Republicans_winning_over_Biden">transcript &amp; editable summary</a>)

Republicans celebrate as Biden pulls out of settlement negotiations, leading to potential trials uncovering more and families receiving increased compensation, all against the intended goal. Biden's move may prolong public scrutiny on Trump-era treatment at the border, prompting criticism and calls for DOJ to resume negotiations. Families seeking closure face uncertain court battles ahead.

</summary>

"Republicans won by ensuring that the families of those separated at the border will be paid more and there will be constant public discussion of their treatment."
"The purpose of a settlement is to avoid further embarrassment and to get the families to accept less money than they would get if they went to trial."
"I think Biden is wrong for this."
"If the families were willing to settle, it's because they want to get on with their lives."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Republicans are celebrating their victory in causing the Biden administration to withdraw from settlement negotiations with families separated at the border.
Biden's decision to pull out was influenced by the Republicans making the position untenable due to potential costs.
The purpose of settlements is to avoid further embarrassment and for families to accept less money than they might receive in court.
With cases now going to trial, criminal actions may be uncovered, prolonging public scrutiny on the treatment during the Trump administration.
Biden's move to pull out might lead to families receiving more money through trials than the $400,000 settlements Republicans were concerned about.
Republicans unintentionally ensured that families get paid more and that their treatment remains a public focus, contrary to their initial goal.
Beau criticizes Biden's decision and suggests the Department of Justice resume negotiations to avoid lengthy court battles for families wanting to move forward.
The government's legal strategy might involve admitting it can't win in court, prompting quicker compensation for affected families.
Beau believes that families willing to settle seek closure and the government should respect their wishes to move on.

Actions:

for advocates, activists, voters,
Resume settlement negotiations to provide closure for families seeking to move on (suggested)
Explain to the American people the reasons behind settlement negotiations and the challenges faced in court (suggested)
</details>
<details>
<summary>
2021-12-17: Let's talk about Fox News and grocery stores.... (<a href="https://youtube.com/watch?v=5455J1fpd4U">watch</a> || <a href="/videos/2021/12/17/Lets_talk_about_Fox_News_and_grocery_stores">transcript &amp; editable summary</a>)

Beau explains the marketing strategy of selling fear and anger through Fox News, paralleling it with grocery store recipes as a way to encourage consumption.

</summary>

"What happens if your product, if what you're selling, is fear and anger?"
"Fox spread a narrative that they should have had a reason to doubt."
"Fox has a product of its own. That product is advertising."
"A good way to get people to tune in is to keep them angry, to keep them scared."
"Fox's defense saying that it was loose hyperbole, it was rhetoric, supposed to be entertaining."

### AI summary (High error rate! Edit errors on video page)

Drawing a parallel between grocery store recipes and Fox News selling fear and anger.
Explaining how recipes on grocery items are a marketing strategy to sell more products.
Connecting the strategy of using recipes to encourage more consumption with Fox News selling fear and anger.
Mentioning a lawsuit against Fox News by Dominion alleging defamation.
Speculating that Fox News may offer a substantial settlement to Dominion to avoid further legal proceedings and disclosure of private communications.
Pointing out that Fox News plays into spreading fear and anger to attract viewers and sell advertising.
Noting that Fox News claimed their content was entertainment and hyperbole, not intended to be taken seriously.

Actions:

for media consumers,
Support independent and responsible journalism (implied)
Be critical of the media content you consume (implied)
Advocate for transparency and accountability in media organizations (implied)
</details>
<details>
<summary>
2021-12-16: Let's talk about the important question about the texts.... (<a href="https://youtube.com/watch?v=ixjLUFX1zhI">watch</a> || <a href="/videos/2021/12/16/Lets_talk_about_the_important_question_about_the_texts">transcript &amp; editable summary</a>)

Beau raises the importance of questioning the intent behind text messages related to former President Trump, stressing that understanding the reason for sending those messages is vital.

</summary>

"Why were those messages sent?"
"That's what matters."
"Understanding the reason for sending those messages is vital."
"The intent behind the messages is critical."
"I'm dying to know which one you hope it is."

### AI summary (High error rate! Edit errors on video page)

Beau raises the importance of questioning the intent behind certain text messages related to former President Trump.
The key question revolves around why the messages were sent in the first place.
He points out that the intent behind the messages is critical, especially for supporters of Trump.
There are two possible intents behind the messages: either the senders believed Trump supported the events or they thought he was incompetent.
Beau stresses that understanding the reason for sending those messages is vital in understanding the mindset at that time.
The focus should be on why multiple individuals close to Trump felt the need to send those messages.
Beau leaves the audience pondering which scenario they hope it is.

Actions:

for supporters and skeptics,
Question the intent behind messages (implied)
Seek understanding from multiple perspectives (implied)
</details>
<details>
<summary>
2021-12-16: Let's talk about Earth's Black Box.... (<a href="https://youtube.com/watch?v=QkUfOOij9C0">watch</a> || <a href="/videos/2021/12/16/Lets_talk_about_Earth_s_Black_Box">transcript &amp; editable summary</a>)

Beau introduces the Earth's black box, a city bus-sized structure powered by solar energy to record 30-50 years of data for future generations, capturing humanity's potential triumphs or ultimate failure.

</summary>

"The Earth is getting its own black box."
"It's meant to be indestructible."
"One of its greatest triumphs, or it'll record its final failure."
"A wild idea."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduces the concept of humanity's legacy and the Earth's black box.
Compares the Earth's black box to a black box on a plane, recording data to learn from past mistakes.
Describes the Earth's black box as the size of a city bus, powered by solar energy, and capable of storing 30 to 50 years of data.
Mentions that the Earth's black box will record information on climate change, world state tweets, pollution studies, health data, and species loss.
States that the Earth's black box will be operational by 2022 with data already being compiled for it.
Notes that the concept and creators of the Earth's black box are from Australia, but its location remains undisclosed.
Emphasizes that the Earth's black box is designed to be indestructible and will require advanced technology to access.
Mentions that information inside the Earth's black box will be encoded in math and symbolism for future understanding.
Comments on the significance of individuals today working to leave something beneficial for future generations.
Concludes by stating that the Earth's black box will either document humanity's greatest achievement or its final failure.

Actions:

for environmental enthusiasts, futurists,
Support initiatives focused on long-term data collection and preservation (exemplified)
Advocate for sustainable energy sources like solar power (exemplified)
Encourage education on climate change, pollution, and species loss (exemplified)
</details>
<details>
<summary>
2021-12-15: Let's talk about those text messages about the 6th.... (<a href="https://youtube.com/watch?v=Jr2a4O1T200">watch</a> || <a href="/videos/2021/12/15/Lets_talk_about_those_text_messages_about_the_6th">transcript &amp; editable summary</a>)

Beau speculates on the strategic reasons behind unnamed text messages and potential criminal charges, suggesting a power move beyond surface revelations.

</summary>

"They're playing to win dirty."
"Maybe she learned a lot from him."
"It's not like the committee doesn't know the numbers."
"I think this was much more of a power move than people are giving it credit for."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Mentioned the lesser-discussed text messages sent on January 6th, urging Trump to call off the Capitol riot.
Speculated on the reasons for some text messages not having names attached.
Suggested that withholding names could be a strategic move to be revealed before the election or as a warning.
Raised the possibility of Cheney being involved in the strategic decision-making due to her father's influence.
Discussed the potential implication of criminal charges against Trump based on the committee's statements.
Proposed that the actions taken by the committee were more about sending a message than just revealing information.
Concluded with the belief that the unfolding events were a power move to convey a message effectively.

Actions:

for political observers,
Contact your representatives to express your thoughts on accountability in political actions (suggested).
Stay informed about ongoing political developments and their implications (implied).
</details>
<details>
<summary>
2021-12-15: Let's talk about a new study and a milestone.... (<a href="https://youtube.com/watch?v=WisPUJ4I-xw">watch</a> || <a href="/videos/2021/12/15/Lets_talk_about_a_new_study_and_a_milestone">transcript &amp; editable summary</a>)

Beau from South Africa provides insights on a new COVID-19 variant, stressing cautious optimism and the importance of staying informed, vaccinated, and boosted.

</summary>

"Wash your hands. Don't touch your face. Wear a mask if you have to go out. Get vaccinated, get a booster."
"The odds are pretty good that this is accurate."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Shares a new study from South Africa about a new variant, stressing the need for all information before drawing conclusions.
Mentions the grim milestone of 800,000 COVID-19 deaths in the United States, likely to reach a million.
Emphasizes that the study discussed is not peer-reviewed yet.
Notes the rapid spread of the new variant and the importance of being informed.
Reports that the new variant is spreading quickly and the findings seem to match known information.
Presents a mix of good and bad news regarding the new variant.
Good news: The variant appears to cause milder symptoms and fewer hospitalizations in South Africa.
Caution from experts about lag time in hospitalizations and vaccine effectiveness.
Bad news: The Pfizer vaccine seems less effective at preventing infection with the new variant.
Good news: The Pfizer vaccine remains effective at reducing hospitalizations, although less so than before.
Speculates that the new variant may displace the Delta variant and seems to affect younger people more.
Mentions the lack of widespread booster access in South Africa and the potential impact of boosters in the United States.
Advises taking precautions such as washing hands, wearing masks, and getting vaccinated and boosted.

Actions:

for health-conscious individuals,
Wash hands, avoid touching face, wear masks, get vaccinated and boosted (implied)
</details>
<details>
<summary>
2021-12-14: Let's talk about the post-Christian church.... (<a href="https://youtube.com/watch?v=FVmny9-kI88">watch</a> || <a href="/videos/2021/12/14/Lets_talk_about_the_post-Christian_church">transcript &amp; editable summary</a>)

Beau raises thought-provoking questions about whether churches truly embody the teachings of Christ or function as post-Christian power structures, urging Christians to reconcile teachings with church practices.

</summary>

"Does your church encourage forgiveness? Or does it encourage you to hold a grudge?"
"If you are a Christian, this is something you're going to have to reconcile."
"Is it a Christian church? Or is it post-Christian?"
"Suddenly I have an image of somebody walking around flipping over tables and chasing people with a scourge."
"It is far easier and far more marketable if you give permission to be your worst rather than provide encouragement to be your best."

### AI summary (High error rate! Edit errors on video page)

Beau introduces a term that has been on his mind and expresses his intention to talk about religion, specifically the Christian religion, which he usually avoids discussing.
He questions whether churches truly embody the teachings of Christ or serve as power structures with different motives.
Beau prompts listeners to ponder if their churches encourage behaviors like feeding the hungry, welcoming strangers, caring for the needy, visiting the sick and those in prison, loving thy neighbor, and forgiveness.
He contrasts these teachings with potential negative attitudes encountered in churches, such as stigmatizing welfare recipients, advocating for walls, dismissing the needy, and lacking forgiveness.
Beau challenges the perception of the United States as a Christian nation and suggests that churches should advocate for Christ's teachings in government policies.
He concludes by questioning if churches are genuinely Christian or merely existing as post-Christian power structures focused on socialization rather than embracing Christ's teachings.

Actions:

for christians,
Question whether your church truly embodies Christ's teachings (implied)
Advocate for church practices that genuinely encourage Christ-like behaviors (implied)
Engage in introspection regarding the alignment of church teachings with personal beliefs and actions (suggested)
</details>
<details>
<summary>
2021-12-14: Let's talk about disaster relief being transformative.... (<a href="https://youtube.com/watch?v=mYzZ25B8Cg4">watch</a> || <a href="/videos/2021/12/14/Lets_talk_about_disaster_relief_being_transformative">transcript &amp; editable summary</a>)

Engaging in disaster relief work provides a transformative experience that hones critical skills and perspectives for effective activism under extreme conditions.

</summary>

"There's nothing that will make you a better activist than doing that kind of relief work in the immediate aftermath of some natural disaster."
"It's constant. It is constant."
"Once you have done that, nothing faces you."
"Everything becomes easier because you've done it in the worst possible conditions."
"Just helping people get what they need."

### AI summary (High error rate! Edit errors on video page)

Disaster relief is transformative because it encompasses all aspects of activism and charity work simultaneously, addressing basic needs like homelessness, food insecurity, lack of fresh water, and medical care.
In disaster relief, the absence of electricity leads to a cascade of challenges such as closed stores, limited gasoline, full shelters, and communication breakdowns.
Effective activism in disaster relief involves timely delivery of resources and assistance, working around the clock to ensure the right aid reaches the right people.
Engaging in disaster relief leads to collaboration with unexpected partners, breaking down political or ideological barriers for a common mission.
The intense and continuous nature of disaster relief work helps activists overlook internal conflicts within groups, fostering a singular focus on the mission.
Participation in disaster relief activities exposes individuals to extreme situations, making subsequent activism in more stable conditions seem easier.
Observing varied human behaviors during disaster response showcases both the unity and disarray that can unfold in communities during crises.
Disaster relief work exposes individuals to the breakdown of societal norms and structures, requiring adaptability and quick thinking in rapidly changing environments.
Engaging in disaster relief helps individuals develop critical skills like resourcefulness, networking, and collaboration, even with those they may not typically associate with.
Participating in disaster relief efforts can enhance one's ability to navigate challenges, find necessary resources, and build connections, all valuable skills for achieving activist goals.

Actions:

for activists and volunteers,
Volunteer for disaster relief efforts (exemplified)
Develop skills in networking and collaboration through disaster relief work (exemplified)
Learn to adapt quickly to changing situations in society by engaging in disaster relief (exemplified)
</details>
<details>
<summary>
2021-12-13: Let's talk about coup by PowerPoint..... (<a href="https://youtube.com/watch?v=U4p6QlrQU3c">watch</a> || <a href="/videos/2021/12/13/Lets_talk_about_coup_by_PowerPoint">transcript &amp; editable summary</a>)

Beau reveals ongoing coup attempts and stresses the importance of vigilance against political manipulation for the future of democracy.

</summary>

"A coup attempt in which people said, oh, it can't happen here, and looked the other way, and the media didn't want to call it a coup. That's not a failed coup. It's practice."
"The fate of the republic, the fate of the American experiment, lies in the hands of the people Donald Trump called rhinos. Republicans in name only."
"With the current media coverage, although I'm glad they're finally acknowledging it as a coup attempt, the way it's being presented as past tense and something that we need to look into what happened and not what's happening. It's concerning."

### AI summary (High error rate! Edit errors on video page)

Reveals that he has covered the points in the PowerPoint on his channel over the past ten months.
Mentions posting documents related to the PowerPoint on his Twitter months ago.
Points out the sudden media reaction to the PowerPoint and the use of the term "coup."
Describes the alleged PowerPoint circulated among the top of the Trump camp outlining options for a coup.
Explains the term "auto coup" or "self coup" where a person comes to power through legitimate means and circumvents checks on their power.
Emphasizes that the coup attempt is ongoing, not something of the past.
Analyzes Trump's political strategy in backing candidates who support his agenda, particularly in Georgia.
Expresses concern that people are viewing the coup attempt as a fluke rather than ongoing practice.
Asserts that future attempts are likely if those behind the coup have another chance.
Stresses the importance of individuals within the Republican party standing up against Trump's influence as a defense against future coup attempts.

Actions:

for political analysts, concerned citizens,
Watch political developments closely to identify any signs of ongoing attempts to manipulate power (implied)
Speak out against any undemocratic actions or attempts to circumvent established processes (implied)
</details>
<details>
<summary>
2021-12-13: Let's talk about Rand Paul, Kentucky, and a change of heart.... (<a href="https://youtube.com/watch?v=KVGkFqeGDPE">watch</a> || <a href="/videos/2021/12/13/Lets_talk_about_Rand_Paul_Kentucky_and_a_change_of_heart">transcript &amp; editable summary</a>)

Senator Rand Paul's history of opposing disaster relief funding raises doubts about his recent change of heart, prompting Beau to question his sincerity and hope for consistency in future votes.

</summary>

"Politicizing that suffering would be low for even the deepest partisan."
"Everybody's for law and order until they get caught."
"I hope I'm wrong."

### AI summary (High error rate! Edit errors on video page)

Senator Rand Paul sent President Biden a letter asking for emergency relief funds for tornado disaster in Kentucky.
Paul has a history of opposing such funding and has voted against it multiple times.
Beau generally doesn't criticize someone who changes their wrong decisions to right ones, but...
Senator Rand Paul's past actions contradict his recent statement about uniting to help and heal after disasters.
Beau believes in giving people the benefit of the doubt but questions Paul's sincerity based on his history.
Beau finds relief work after disasters transformative and hopes people understand Paul's prior stance against such relief.
He shares a lawyer's insight that people change their position on issues like law and order only when they are personally affected.
Beau hopes Senator Paul's recent actions indicate a genuine change in position for future disaster relief votes.

Actions:

for kentuckians, voters,
Contact Senator Rand Paul's office to express support or concerns about his stance on disaster relief funding (suggested).
Participate in local disaster relief efforts to help those affected by natural disasters in Kentucky (implied).
</details>
<details>
<summary>
2021-12-12: Let's talk about semantics, Shepherd's Pie, liberals, and leftists.... (<a href="https://youtube.com/watch?v=9NE9BI-OpgE">watch</a> || <a href="/videos/2021/12/12/Lets_talk_about_semantics_Shepherd_s_Pie_liberals_and_leftists">transcript &amp; editable summary</a>)

Beau explains semantics through cooking shepherd's pie, illustrating the importance of clarifying distinctions like liberals vs. leftists to combat political fear-mongering.

</summary>

"Arguing over material changes, sometimes it's important."
"They will tell their base, their constituents, that liberal and leftist mean the same thing."
"Allowing them to use the term leftist, radical left, when they're talking about liberals, that's just handing them a way to scare their base."
"That gap between being a liberal who is in support of capitalism and being a leftist who is generally against capitalism, that's a pretty big gap there."
"It's probably more important for liberals to make that distinction."

### AI summary (High error rate! Edit errors on video page)

Defines semantics as the search for meaning in words and linguistics.
Explains that when people dismiss something as "just semantics," they mean it is an irrelevant argument over definitions.
Illustrates the concept with an example of a train conductor and a passenger arguing over the destination.
Shares his love for cooking shepherd's pie, a dish loved by many except for his five-year-old.
Describes making shepherd's pie with various meats like chicken or bacon, acknowledging the material change in the dish.
Differentiates between shepherd's pie and cottage pie based on the meat used, not just for semantics but due to a substantial difference.
Emphasizes that correcting someone over material changes is not irrelevant but educational.
Draws a parallel between his son not eating shepherd's pie due to misunderstanding and the intentional conflation of liberals and leftists by Republicans.
Breaks down how the Republican Party's labeling of liberals as leftists creates fear and motivates their base through scare tactics.
Calls for Democrats to challenge and correct the misrepresentation of liberals as leftists for political benefit.

Actions:

for politically engaged individuals,
Challenge misrepresentations in political discourse (suggested)
Educate others on the differences between liberals and leftists (implied)
</details>
<details>
<summary>
2021-12-12: Let's talk about nosy neighbors.... (<a href="https://youtube.com/watch?v=qYgzRULatDQ">watch</a> || <a href="/videos/2021/12/12/Lets_talk_about_nosy_neighbors">transcript &amp; editable summary</a>)

Beau addresses nosy neighbors calling the police unnecessarily, suggesting monetary consequences over moral arguments to deter this behavior and protect property values.

</summary>

"You should really only call the law if you think that lethality is an appropriate possible response."
"Aside from that, that's happening to every house in the neighborhood."
"If you could suggest that they could be removing tens of thousands of dollars of equity from themselves and all of their neighbors, they might stop."
"It's statistics. It's the constant police presence. That's what drops property rates."
"But if they call long enough on enough different people, there's going to be crime."

### AI summary (High error rate! Edit errors on video page)

Explains a situation where neighbors on apps like Nextdoor brag about calling the police on people for minor reasons like driving up and down the street or sitting in a car for too long.
Acknowledges the potential dangers of introducing armed police into non-threatening situations.
Suggests curbing this behavior by appealing to self-interest or greed rather than solely relying on moral arguments.
Points out that crime rates, not just crime itself, impact property values and that constant police presence can lower property values.
Proposes that neighbors who excessively call the police may actually be harming property values in their own neighborhood.
Recommends using monetary consequences as a deterrent for such behavior, as it may be more effective than moral appeals.
Encourages looking up studies on the correlation between crime rates and property values to present factual evidence in such situations.

Actions:

for community members,
Share statistical studies on crime rates and property values to raise awareness about the impacts of excessive police calls on property values (suggested).
Encourage neighbors to reconsider their approach by discussing the potential financial consequences of their actions on property values (implied).
</details>
<details>
<summary>
2021-12-11: Let's talk about voting traditions in the US.... (<a href="https://youtube.com/watch?v=CduQMNto8yA">watch</a> || <a href="/videos/2021/12/11/Lets_talk_about_voting_traditions_in_the_US">transcript &amp; editable summary</a>)

Beau explains the historic tradition of safeguarding smaller demographics in voting, addressing criticism and condemning voter suppression tactics.

</summary>

"If they weren't concerned about that, they wouldn't have a whole house where every state, no matter its size, no matter its population, got two votes."
"Gerrymandering is wrong. I can't believe this is a conversation we're still having today."
"The only way they can preserve power is by suppressing the vote of people who would vote against them."
"These are actions of people who are supporting bad ideas and they know they're supporting bad ideas."
"They'll undermine the founding principles of the country."

### AI summary (High error rate! Edit errors on video page)

Explains the historical tradition in the US of looking out for smaller demographics in the voting process.
Addresses a message criticizing his video on voting rights in Texas.
Refutes the claim that the US Constitution does not protect smaller demographics in voting.
Points to Article 1, Section 3 of the Constitution, which created the United States Senate to safeguard the interests of smaller states.
Argues that safeguarding smaller demographics in voting is an integral part of the government's structure.
Emphasizes that ensuring equal representation is an evolving concept in the US.
Criticizes gerrymandering and its impact on diluting voting power.
Condemns efforts to suppress votes of those who may vote against certain parties.
Calls out Republicans for resorting to voter suppression tactics due to a lack of new ideas.

Actions:

for voters, activists, citizens,
Contact your representatives to advocate against gerrymandering and voter suppression (implied).
Join or support organizations working to ensure fair voting practices (implied).
</details>
<details>
<summary>
2021-12-11: Let's talk about the FDA, an FOIA, and 75 years.... (<a href="https://youtube.com/watch?v=51AXkd0peDk">watch</a> || <a href="/videos/2021/12/11/Lets_talk_about_the_FDA_an_FOIA_and_75_years">transcript &amp; editable summary</a>)

Beau clarifies a claim about a 75-year wait for information release, urging for transparency and efficiency in handling a large Freedom of Information Act request.

</summary>

"There's enough little screenshots that could be turned into memes that could lead people to believe that."
"Start with the important stuff."
"It's in Freedom of Information Act request. 500 pages a month is normal."
"There's no secret here, there's no grand conspiracy, it's just the way it works."
"If they actually do want to assure the vaccine hesitant, they should be trying to make it as easy as possible for the FDA to get that information out."

### AI summary (High error rate! Edit errors on video page)

Addressing a claim about a request taking 75 years, Beau explains the situation behind a Freedom of Information Act (FOIA) request made to the FDA for documents related to the emergency approval of the vaccine.
The request includes hundreds of thousands of documents that need to be processed, with a standard of providing 500 pages per month for large FOIA requests.
The FDA will start releasing the information immediately, but it will take 75 years to process all of it due to the massive request size, not because they are trying to hide something.
Beau mentions that judges have intervened in similar cases to expedite the process due to intense public interest, suggesting that bringing in additional resources could speed up the information release.
He advises the plaintiffs to narrow the scope of their request to focus on relevant information, as many pages may contain redacted or duplicate content that could slow down the process.
Beau stresses the importance of releasing pertinent information quickly to address vaccine hesitancy and increase vaccination rates, stating that public interest and transparency should be prioritized.

Actions:

for government officials, activists,
Bring in additional resources from other agencies to expedite the processing of FOIA request (suggested)
Plaintiffs should narrow the scope of their request to focus on relevant information and ease the processing burden (suggested)
</details>
<details>
<summary>
2021-12-10: Let's talk about new foreign policy messaging from Biden.... (<a href="https://youtube.com/watch?v=htWCoAk8Q4s">watch</a> || <a href="/videos/2021/12/10/Lets_talk_about_new_foreign_policy_messaging_from_Biden">transcript &amp; editable summary</a>)

American foreign policy messaging shifts towards framing Cold War 2.0 as democracy versus authoritarianism, motivating Americans against authoritarianism, but the true motivations remain rooted in power dynamics.

</summary>

"Cold War 2.0 is going to be democracy versus authoritarianism."
"To be an American patriot, you have to be against authoritarianism."
"Those people who are in favor of an authoritarian system, they're going to do everything they can to stop this messaging from taking hold."
"It's not really going to be democracy versus authoritarianism. That's the gift wrapping for this little contest."
"What's behind the contest is the same thing that is behind everything in foreign policy, power and nothing else."

### AI summary (High error rate! Edit errors on video page)

American foreign policy messaging is shifting towards framing Cold War 2.0 as democracy versus authoritarianism.
Biden's conference about democracy resurgence was actually about dividing countries into those loosely or closely allied with the US.
The new messaging aims to motivate Americans against authoritarianism, similar to the anti-leftism push during the Cold War.
Pushback against anti-authoritarian messaging will reveal the true authoritarians in the US.
Those opposing the messaging likely support authoritarianism and may start to closely ally themselves with authoritarian leaders like Putin.
Just as leftist ideas became unacceptable during the first Cold War, support for authoritarian ideas will become taboo in the US.
Framing the near-peer contest as democracy versus authoritarianism might not represent the true motivations behind foreign policy actions.
Despite the anti-authoritarian messaging, foreign policy decisions are primarily driven by power dynamics.

Actions:

for policy analysts,
Analyze foreign policy decisions and messaging to understand underlying power dynamics (implied).
</details>
<details>
<summary>
2021-12-10: Let's talk about Daunte Wright and Kim Potter.... (<a href="https://youtube.com/watch?v=Kvx__g-2R84">watch</a> || <a href="/videos/2021/12/10/Lets_talk_about_Daunte_Wright_and_Kim_Potter">transcript &amp; editable summary</a>)

Beau provides commentary on the Duane Wright case trial, expressing doubts about a potential conviction due to the lack of intent and discussing the significance of appropriate charges in cases involving law enforcement.

</summary>

"The charges are appropriate."
"Legal and moral and legal and justice, those aren't always the same thing."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Provides commentary on an ongoing trial involving the Duane Wright case.
Expresses disbelief in the potential for a conviction due to lack of intent to kill by the police officer.
Notes that the officer is charged with manslaughter, not murder, reflecting the lack of intent as a key factor.
Suggests that the strongest evidence for the officer's lack of intent is her shouting "taser, taser, taser" before the incident.
Comments on the rarity of appropriate charges in cases involving law enforcement.
Draws a parallel between a movie scene and the situation in the Duane Wright case to illustrate the seriousness of the charges.
Speculates that the trial may focus more on differentiating between degrees of guilt rather than innocence or guilt.
Advises understanding the statutes behind the charges to form informed opinions on legal cases.
Emphasizes the distinction between legal, moral, and justice aspects in such trials.

Actions:

for legal observers,
Understand the statutes behind charges ( suggested )
Form informed opinions on legal cases ( exemplified )
</details>
<details>
<summary>
2021-12-09: Let's talk about something good from something bad.... (<a href="https://youtube.com/watch?v=2iGOJj1Ehe0">watch</a> || <a href="/videos/2021/12/09/Lets_talk_about_something_good_from_something_bad">transcript &amp; editable summary</a>)

Beau talks about transforming offensive statues into public art as a significant step towards change, sending a direct message of progress and transformation in the southern United States.

</summary>

"Something that's good creating good offspring."
"I think it's beautiful. I think it's a wonderful idea."
"It sends a pretty direct message that change is coming."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the idea of discussing good news for a change, especially when it stems from something negative.
He expresses a preference for good coming from bad rather than good from good, as it signifies real progress and change.
Beau mentions the origin of the term "rule 303" and how it was transformed into something positive by people, signifying a change caused by collective action.
There have been recent developments in the culture war surrounding statues in the United States.
The offensive statue of Nathan Bedford Forrest in Nashville, covered in graffiti, has been removed.
The controversial statue of Robert E. Lee in Charlottesville, a significant source of tension, has also been taken down.
The government has decided to melt down the Robert E. Lee statue and turn it into a piece of public art at the Jefferson School African-American Heritage Center under the swords to plowshares proposal.
Beau finds this transformation of the statue into public art fitting and a positive step towards change.
He acknowledges that while some may have issues with this decision, he personally sees it as a beautiful and wonderful idea.
Beau views this transformation as a small step in changing the southern United States and sees it as more than just political pressure but a significant message of impending change.

Actions:

for community members, activists,
Support and advocate for the transformation of offensive statues into public art (exemplified)
Engage in dialogues and actions that contribute to positive change in your community (implied)
</details>
<details>
<summary>
2021-12-08: Let's talk about the thing we missed in Crenshaw's speech.... (<a href="https://youtube.com/watch?v=UqqXWsIeYqA">watch</a> || <a href="/videos/2021/12/08/Lets_talk_about_the_thing_we_missed_in_Crenshaw_s_speech">transcript &amp; editable summary</a>)

Beau breaks down the Republican Party's fear-based strategy, urging for education to combat manipulation and fear.

</summary>

"Fear. I've said it over and over again. That is how the Republican Party motivates their base. Fear."
"They tell them what to be afraid of and who to blame for it."
"A real leader, if they're telling you to be afraid of something, they're going to give you a solution."
"Education, it destroys fear."
"Stop being afraid. Stop letting them control you that way."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the Crenshaw clip and shifts focus from the funny part towards a more serious aspect.
Crenshaw criticizes the Freedom Caucus and refers to Marjorie Taylor Greene as "space laser lady."
Crenshaw's main point revolves around how fear is utilized to motivate the Republican Party's base.
The Republican Party's campaign strategy is based on instilling fear and directing blame.
Beau points out that creating fear and identifying who to blame is a recurring pattern in Republican strategies.
Examples are given, such as the misinformation about undocumented immigrants causing harm to Americans.
Beau contrasts the fear-mongering tactics with the lack of concern over public health crises.
He stresses the importance of education in combating fear and uncertainty.
The manipulation through fear and blame is emphasized as a method to control and win elections.
Beau encourages conservatives to seek knowledge and understanding to counter fear-based manipulation.

Actions:

for conservatives,
Educate yourself on political strategies of fear and manipulation (implied)
Seek knowledge about issues causing fear or uncertainty (implied)
Encourage others to prioritize education over fear (implied)
</details>
<details>
<summary>
2021-12-08: Let's talk about Kellogg and basic questions about unions.... (<a href="https://youtube.com/watch?v=g8ypJjlwld0">watch</a> || <a href="/videos/2021/12/08/Lets_talk_about_Kellogg_and_basic_questions_about_unions">transcript &amp; editable summary</a>)

Beau addresses the Kellogg union situation, focusing on the two-tiered system and the importance of public support for unions to maintain fair wages and benefits.

</summary>

"I don't cross a picket line. There's nothing more to that sentence. Period. Full stop. I don't do it."
"Public support is important, it is always helpful for the union."
"The key part here, when you're talking about it, is the company saying they're just going to hire new employees."
"Organized labor activity is good, actually."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau addresses questions about Kellogg and the ongoing situation with the union.
He points out that he doesn't personally know anyone in the union, so his insights are secondhand.
The issue at Kellogg revolves around a two-tiered system where newer employees do not receive the same benefits as longer-tenured employees.
Public support by not consuming Kellogg products is seen as significant to support the union's cause.
Beau stresses the importance of public support for union activity and its long-term impacts on companies.
Referencing the John Deere strike, Beau illustrates how public support can influence future consumer choices and impact companies for a prolonged period.
Supporting organized labor activities can help prevent wages from losing buying power over time.

Actions:

for workers, consumers, activists,
Support the union by not consuming Kellogg products (implied)
Show public support for organized labor activities (exemplified)
</details>
<details>
<summary>
2021-12-07: Let's talk about statistics for Republicans and a Democratic edge.... (<a href="https://youtube.com/watch?v=F-bpASAVEPU">watch</a> || <a href="/videos/2021/12/07/Lets_talk_about_statistics_for_Republicans_and_a_Democratic_edge">transcript &amp; editable summary</a>)

The influence of political affiliation on vaccination rates and COVID-19 outcomes reveals stark disparities and potential electoral impacts, underscoring the consequences of misinformation spread.

</summary>

"Your political party is the most important factor in determining whether or not you are vaccinated."
"If you are unvaccinated, you are 5.8 times as likely to get it. You are 14 times as likely to not recover from it."
"Most of those lost will be Republicans."
"Misinformation allowed to circulate by the Republican Party is costing their own constituents."
"Anyway, go get vaccinated."

### AI summary (High error rate! Edit errors on video page)

The vaccination status is most influenced by political affiliation.
Counties with higher vaccination rates tended to vote for Biden, while those with lower rates voted for Trump.
Republican-leaning counties have significantly lower vaccination rates compared to Democratic-leaning counties.
The Democratic Party has a much higher vaccination percentage compared to the Republican Party.
Unvaccinated individuals are significantly more likely to contract and not recover from COVID-19.
The gap in vaccination rates between Democrats and Republicans results in a higher number of Republican deaths.
These statistics could impact election outcomes in tightly contested areas.
Misinformation allowed by the Republican Party has led to these concerning numbers.
Beau hopes Republicans question other misinformation spread by their party.
Encourages everyone, regardless of political affiliation, to get vaccinated.

Actions:

for republicans, democrats, all,
Get vaccinated (exemplified)
</details>
<details>
<summary>
2021-12-07: Let's talk about altering the Supreme Court.... (<a href="https://youtube.com/watch?v=MDBaOIcUtt0">watch</a> || <a href="/videos/2021/12/07/Lets_talk_about_altering_the_Supreme_Court">transcript &amp; editable summary</a>)

Beau questions the impartiality of the Supreme Court, advocating for the replacement of all justices to restore integrity and non-partisanship.

</summary>

"The institutional purity is gone."
"You made it a political tool. It's going to be one now."
"Congratulations."
"I think that at this point, we probably just need all new justices."
"The Supreme Court ceased to be the institution you are describing."

### AI summary (High error rate! Edit errors on video page)

Talks about an expected decision by the Supreme Court and the response to it.
Receives a message urging the audience to drop the idea of expanding or reducing the Supreme Court.
Argues that the Court should be impartial and non-partisan, not a political tool.
Questions the timing of cases coming up now, linking it to new Republican-appointed justices.
Expresses concerns about the Supreme Court losing its institutional integrity.
Believes that the Supreme Court became a political tool when unqualified nominees were confirmed.
Criticizes Republicans for wanting to use the Court for political gain while expecting Democrats to respect it.
Challenges the notion of recent constitutional changes that could warrant overturning precedents like Roe v. Wade.
Suggests that the Trump administration undermined various institutions, including the Supreme Court.
Advocates for replacing all Supreme Court justices to restore impartiality.
Foresees potential backlash if Roe v. Wade is overturned or reduced, given public support for upholding it.
Points out that state-level cases may arise due to trigger bans already in place.
Comments on how Republican actions could inadvertently boost voter turnout for Democrats in upcoming elections.
Asserts that the Supreme Court's claim of being impartial is no longer valid, advocating for its alteration irrespective of the decision.

Actions:

for political activists, concerned citizens,
Advocate for judicial reform by supporting initiatives aimed at altering the structure of the Supreme Court (implied).
Get involved in local politics and elections to influence the appointment of judges and justices (implied).
</details>
<details>
<summary>
2021-12-06: Let's talk about the TV show Yellowstone.... (<a href="https://youtube.com/watch?v=7VKTtMswwS4">watch</a> || <a href="/videos/2021/12/06/Lets_talk_about_the_TV_show_Yellowstone">transcript &amp; editable summary</a>)

Beau explains how Yellowstone could have introduced woke topics through consistent social commentary, contrasting it with the show's actual direction and audience perception.

</summary>

"Yellowstone was never meant to be something for the Make America Great Again crowd."
"If you didn't catch any of this and all of a sudden got offended and got upset because somebody on Twitter told you to be mad about an animal rights activist character, you're not RIP, you're Jimmy."

### AI summary (High error rate! Edit errors on video page)

Beau is discussing the TV show Yellowstone, a popular show among certain demographics.
Beau explains his decision not to watch the very last episode of Yellowstone due to the show becoming "woke."
The introduction of an animal rights activist character in Yellowstone led Beau to avoid the final episode.
Beau suggests that if a show wants to introduce woke topics, it should prepare the audience with consistent social commentary throughout.
He outlines various societal issues that could have been addressed in Yellowstone to ease the introduction of woke themes.
Beau mentions the importance of addressing topics like social safety nets for orphans, the juvenile justice system, criminal justice system, and prison system.
He also suggests incorporating themes related to reservations, missing indigenous women, race, law enforcement misconduct, and gender equality.
Beau proposes showcasing strong female characters to challenge toxic masculinity and discussing family planning openly.
He recommends including political elements in the show, such as running for office as an independent in a red state.
Beau contrasts his suggested approach with Yellowstone's actual portrayal, stating that the show did not incorporate woke themes throughout.
He points out that Yellowstone features tough characters and criminal elements but didn't follow the woke narrative he described.
Beau expresses surprise at the portrayal of right-wing MAGA groups as the villains in Yellowstone, suggesting it wasn't meant for that audience.
He addresses the reaction of viewers who felt offended by the introduction of an animal rights activist character in the show.
Beau humorously distinguishes between viewers who may identify with different characters from the show, implying a lack of true understanding for some.

Actions:

for tv show viewers,
Analyze and incorporate relevant social commentary into media productions (suggested).
Address societal issues in storytelling to raise awareness (suggested).
Challenge toxic masculinity in narratives by featuring strong female characters (suggested).
Promote open dialogues on race, gender equality, and political issues in media (suggested).
</details>
<details>
<summary>
2021-12-06: Let's talk about Texas voting rights.... (<a href="https://youtube.com/watch?v=GavKH2RQxDA">watch</a> || <a href="/videos/2021/12/06/Lets_talk_about_Texas_voting_rights">transcript &amp; editable summary</a>)

Department of Justice sues Texas over alleged violation of Voting Rights Act, accusing Republicans of diluting Black and Latino votes to undermine democracy.

</summary>

"Republicans have realized it cannot win on outdated, bad ideas."
"They have decided to give up on the founding ideas of this country instead."
"They can't win through the democratic process, so they have given up on democracy itself."

### AI summary (High error rate! Edit errors on video page)

Department of Justice filed suit against Texas over new maps.
Alleges Texas in violation of Voting Rights Act section two.
Republicans accused of redrawing maps to negatively impact specific groups.
Suit claims Republicans diluted votes of Black and Latino Americans.
Republicans broke up predominantly black districts to dilute their voice.
Did the same with Latino districts, impacting their voting ability.
Suit likely to succeed, but Republicans may attempt similar tactics elsewhere.
Republicans shifting from outdated ideas to undermining democracy.
Predicted by voting rights experts and those familiar with gerrymandering.
Be prepared for other states to face similar challenges.

Actions:

for voters, activists, community members,
Stay informed and engaged with redistricting processes (implied)
Support organizations fighting for fair voting practices (implied)
Advocate for transparency and fairness in redistricting (implied)
</details>
<details>
<summary>
2021-12-05: Let's talk about which way the Overton window in the US is headed.... (<a href="https://youtube.com/watch?v=N1Bv4jW7_Ok">watch</a> || <a href="/videos/2021/12/05/Lets_talk_about_which_way_the_Overton_window_in_the_US_is_headed">transcript &amp; editable summary</a>)

Beau explains how society's liberal shift contrasts the government's rightward movement to maintain stability, illustrating the concept through the Overton window and the Supreme Court.

</summary>

"On a long enough timeline, the social progressive always wins."
"Society moves forward. Thought changes. The law catches up."
"The government is moving to the right. It is becoming more right-wing, more authoritarian."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of the Overton window as the range of acceptable debate on a topic.
Republicans view the country as moving left, while people on the left see a dangerous shift towards far-right ideas.
The discrepancy in perceptions arises from differing definitions of terms like "left" and "liberal."
Society's thought progression tends to shift left, becoming more liberal, while the government moves right to maintain stability.
Provides an example with the Supreme Court to illustrate the government's shift towards conservatism.
Describes a pattern of two steps forward and one step back in societal progress.
Anticipates that the government will eventually bounce left as societal progressivism prevails in the long run.

Actions:

for citizens, activists, advocates,
Challenge authoritarianism by advocating for progressive societal changes (implied).
Stay informed about societal shifts and government actions to ensure alignment with societal values (implied).
Engage in civil discourse to clarify definitions and bridge gaps in understanding (implied).
</details>
<details>
<summary>
2021-12-05: Let's talk about information silos and theories.... (<a href="https://youtube.com/watch?v=PucdTO7srLg">watch</a> || <a href="/videos/2021/12/05/Lets_talk_about_information_silos_and_theories">transcript &amp; editable summary</a>)

Beau explains the dangers of information silos using the analogy of a grain silo, warning against getting trapped in harmful narratives and the need for professional help to break free.

</summary>

"Don't go into a grain silo."
"It's a game of telephone, where the more likely your information is to cause something horrible to happen, the more you get paid."
"You may need to call in people with the right tools."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of information silos using the analogy of a grain silo, where information is self-contained and walled off from everything else.
Describes the dangers of going inside a grain silo, with the risk of getting stuck due to pressure from the grain.
Draws parallels between exploring theories and being trapped in a grain silo, where one might appear stable on the surface but have hidden dangers.
Compares being trapped in a grain silo to being trapped in misinformation or conspiracy theories, where individuals may need professional help to get out.
Emphasizes the importance of not going into a grain silo and avoiding getting sucked into dangerous information silos.
Mentions the profit motive behind spreading sensationalized information, leading to more people getting stuck in harmful narratives.
Raises questions about the abundance of information in silos and the pressure to keep the audience engaged with increasingly sensational content.
Suggests that like being trapped in a grain silo, breaking free from harmful information silos may require intervention from those with the right tools.
Concludes by reflecting on the challenges of navigating through information silos and the potential need for professional assistance to escape them.

Actions:

for internet users,
Avoid falling into information silos (implied)
Seek professional help if trapped in harmful narratives (implied)
</details>
<details>
<summary>
2021-12-04: Let's talk about an error cops will make after Michigan.... (<a href="https://youtube.com/watch?v=Eoq5XPWGuY4">watch</a> || <a href="/videos/2021/12/04/Lets_talk_about_an_error_cops_will_make_after_Michigan">transcript &amp; editable summary</a>)

Law enforcement agencies need secure protocols to verify officers' identities without compromising safety in potential threat situations.

</summary>

"You don't feel safe, get out."
"Can't use information that is publicly visible like that."
"Because anything that you're going to train the students to do, you're also probably training the suspect."
"That won't work. That will go badly."
"We should probably really start to re-evaluate stuff now that we're training kids how to deal with stuff like this."

### AI summary (High error rate! Edit errors on video page)

An incident involving students sheltering in a class and being told it's safe to come out by a person claiming to be a cop.
Students hesitated when the person used the term "bro" and ultimately escaped through a window.
Law enforcement agencies are looking for ways to prevent similar incidents but need to be cautious about the information they provide.
Simply asking for department name and badge number might not be the safest approach as suspects could exploit this information.
Beau suggests a safer alternative where officers provide a number that is not publicly visible, and students confirm the officer's identity with dispatch before opening the door.
Training students on safety measures can inadvertently train suspects as well, so it's vital to have secure protocols in place.
Beau warns against using information that suspects could easily exploit in such situations.
Parents should communicate with their kids about any new procedures being taught and ensure they understand the safest course of action.
Beau advocates for reevaluating the training methods for students in dealing with potentially dangerous situations.
He questions the necessity of teaching middle schoolers about sign and countersign tactics.

Actions:

for parents, educators, law enforcement,
Contact the Sheriff's Department if your child is being taught to ask for department name and badge number to verify an officer's identity (suggested).
Communicate with your kids about the safer alternative where officers provide a unique number, not publicly visible, for verification before opening the door (suggested).
Advocate for secure protocols in place to ensure students' safety in potential threat situations (implied).
</details>
<details>
<summary>
2021-12-04: Let's talk about Florida State Guard and DeSantis.... (<a href="https://youtube.com/watch?v=t_WBSZKCozg">watch</a> || <a href="/videos/2021/12/04/Lets_talk_about_Florida_State_Guard_and_DeSantis">transcript &amp; editable summary</a>)

DeSantis' Florida program is a political stunt with an insufficient budget, likely just a training initiative at present, not a major concern.

</summary>

"Its goal was to be a political stunt to appeal to the less informed members of his base who actually want that."
"That's not enough money to do anything."
"This is a joke. It's a political stunt."
"But he's going to market it as something else, as a political tool, because he knows his base wants General DeSantis."
"That's not enough money to really run one, even if they already had everything, even if they already had the equipment."

### AI summary (High error rate! Edit errors on video page)

DeSantis' program in Florida is a political stunt, not designed to be effective at creating a paramilitary organization.
The allocated budget of three and a half million dollars is insufficient to establish any significant operation.
The program appears to be a training initiative similar to FEMA's CERT program, focusing on basic emergency response skills.
Beau suggests that there is no need to be overly concerned about this program currently.
He points out that the allocated budget is far too low to support any meaningful paramilitary organization.
With the budget constraints, the program is likely to be just a small office in Tallahassee teaching first aid.
DeSantis may be using this program as a political tool to appeal to certain authoritarian desires within his base.
Beau implies that the program could potentially become more concerning with increased funding in the future.
Setting up task forces within existing law enforcement agencies like FDLE might be a more worrisome scenario.
The current program lacks the necessary resources to be a substantial operational force, even if fully equipped.

Actions:

for florida residents,
Keep informed about any developments related to the program (implied)
Monitor the allocation of funds and resources towards community emergency response training (implied)
</details>
<details>
<summary>
2021-12-03: Let's talk about not talking about race.... (<a href="https://youtube.com/watch?v=gkq8PjQUnYg">watch</a> || <a href="/videos/2021/12/03/Lets_talk_about_not_talking_about_race">transcript &amp; editable summary</a>)

A soldier's belief that ignoring racism will eliminate it prompts Beau to explain the importance of addressing systemic issues through familiar statistics favored by racists.

</summary>

"How do you demonstrate systemic racism to somebody? Use the statistics that racists use."
"I've actually never had that not work. That's always been pretty successful."
"We can't stop talking about it until the playing field's level."

### AI summary (High error rate! Edit errors on video page)

Received a message from a soldier claiming that if we stopped talking about racism, it will be eliminated in America.
Soldier is Hispanic and believes that not acknowledging racism will make it disappear.
Explains the individual versus systemic impact of not talking about racism.
Suggests using statistics often favored by racists to demonstrate systemic racism.
Points out the need to address institutional issues that contribute to disproportionate crime rates among certain groups.
Emphasizes the importance of acknowledging societal pressures that create unequal outcomes.
Recommends discussing and addressing institutional racism before considering stopping the discourse on race.
Mentions the popular Morgan Freeman meme that suggests stopping talking about race as a solution to racism.
Urges for leveling the playing field before ceasing to address race issues.
Encourages engagement in difficult but necessary dialogues to combat racism effectively.

Actions:

for activists, educators, allies,
Challenge misconceptions by using statistics to demonstrate systemic racism (implied)
Engage in dialogues that address institutional issues contributing to racial disparities (implied)
</details>
<details>
<summary>
2021-12-03: Let's talk about a letter from Texas..... (<a href="https://youtube.com/watch?v=sl2qYasoGg0">watch</a> || <a href="/videos/2021/12/03/Lets_talk_about_a_letter_from_Texas">transcript &amp; editable summary</a>)

A Texan facing backlash for liberal beliefs learns from Beau about the distinction between liberalism and communism, the Overton Window concept, and the importance of forming independent ideologies.

</summary>

"Is being liberal and encouraging people to wear a mask really communist, like those around me are saying?"
"There's a thing called the Overton Window."
"If you're feeling isolated in the way you believe, what that means is that you're not being led around."
"I know what I believe, but I also know that I've been wrong about stuff before."
"It may be frustrating, it may be depressing, but for what it's worth, I think you're doing a good job."

### AI summary (High error rate! Edit errors on video page)

A young person in Texas is facing difficulties due to their liberal ideology in a red state, where they get picked on and called a communist for wearing a mask.
Beau explains the difference between being liberal and communist, clarifying that communism falls on the left-right spectrum, not the liberal-conservative spectrum.
He mentions that being liberal does not equate to being communist, nor does encouraging mask-wearing.
Beau addresses the conflation of liberal and leftist beliefs, mentioning that leftist ideologies reject capitalism.
He refers to a video by Mexie to explain the distinction between liberal and leftist ideologies further.
Beau suggests that public health, like wearing masks, should transcend politics and be based on common sense and necessity.
He advises the young person to meet others where they are when discussing the importance of mask-wearing, even referring to biblical references to support the practice.
Beau touches on the Overton Window concept, explaining how the acceptable range of discourse in the US can shift over time, leading to certain ideas being labeled as communist.
He underscores that the founding fathers were liberals and advocates for embracing liberalism when learning about the Constitution and US history.
Beau encourages forming independent ideologies and opinions, applauding the young person for developing their own beliefs rather than conforming.

Actions:

for young adults in conservative environments.,
Share educational resources on the differences between ideologies with those who may misunderstand them (implied).
Engage in respectful and informative dialogues with others to explain the importance of issues like mask-wearing (implied).
Encourage critical thinking and independent ideology formation among peers (implied).
</details>
<details>
<summary>
2021-12-02: Let's talk about how to pick a charity.... (<a href="https://youtube.com/watch?v=ve4T1BoiNgU">watch</a> || <a href="/videos/2021/12/02/Lets_talk_about_how_to_pick_a_charity">transcript &amp; editable summary</a>)

Beau provides a comprehensive guide on selecting and vetting charities, stressing personal involvement for effectiveness and impact.

</summary>

"There is no wrong way to help, all right?"
"Getting involved on a personal level, it's probably more effective."
"It's the war on poverty, the war on hunger, the war on homelessness. Wars cost money."

### AI summary (High error rate! Edit errors on video page)

Beau provides a guide on how to pick and vet charities, sharing his personal approach.
He advises starting by deciding on a cause that is close to your heart.
Beau recommends making a list of charities that work towards that cause and evaluating them based on your criteria.
He prefers organizations that are "boots on the ground" and those that act as clearinghouses for fundraising.
Beau underscores the importance of getting personally involved rather than just cutting a check.
Effectiveness and impact evaluation are key factors in selecting a charity to support.
Beau suggests talking to both clients and employees of the charity to gauge their effectiveness.
When assessing finances, he advises looking at overhead costs as a percentage and considering executive salaries in context.
Beau advocates for supporting individuals within charities who are effective and committed to the cause.
Following effective individuals across different organizations can lead to further impactful involvement and support.

Actions:

for donors, volunteers, supporters,
Contact charities to inquire about their operations and impact (suggested)
Talk to clients and employees of charities to gauge effectiveness (exemplified)
Follow and support effective individuals within charities on social media (suggested)
</details>
<details>
<summary>
2021-12-02: Let's talk about a low to no snow future.... (<a href="https://youtube.com/watch?v=VXuyjuGPGqw">watch</a> || <a href="/videos/2021/12/02/Lets_talk_about_a_low_to_no_snow_future">transcript &amp; editable summary</a>)

Researchers warn of declining snowpack in the Western United States due to global warming, necessitating urgent collaboration and preparation across fields to address water scarcity and wildfire risks.

</summary>

"The impacts of climate change are hitting us now."
"We need an unprecedented amount of cooperation and collaboration across all fields of endeavor so we can get ready."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Researchers in the Western United States have observed a decline in snowpack on mountain tops due to global warming, leading to less water storage.
The snowpack acts as a natural reservoir, providing water for agricultural uses during dry periods.
With declining snowpack, there won't be enough water to meet demand, requiring innovative water storage solutions within the next 19 years.
The study predicts episodic low to no snow in the 2040s, necessitating urgent action.
The decrease in snowpack will have cascading effects on rivers, groundwater, and wildfires.
Collaboration among scientists, engineers, and planners is vital to address the impacts of climate change and prepare for the future.
Mitigating climate change effects now and transitioning away from fossil fuels are critical steps.
Immediate action is necessary to mitigate the severe consequences that will be more evident in the future.
Cooperation across various fields is needed to proactively address the challenges posed by the declining snowpack.
The time to act and prepare for these changes is now.

Actions:

for climate activists, scientists, policymakers,
Collaborate with scientists, engineers, and planners to develop innovative water storage solutions (suggested)
Advocate for transitioning away from fossil fuels to combat climate change (suggested)
</details>
<details>
<summary>
2021-12-01: Let's talk about what Republicans get wrong about guns.... (<a href="https://youtube.com/watch?v=eXKxSI8mnIQ">watch</a> || <a href="/videos/2021/12/01/Lets_talk_about_what_Republicans_get_wrong_about_guns">transcript &amp; editable summary</a>)

Beau exposes the inaccuracies in a tweet about the Second Amendment and Australia, clarifying that leftists don't aim to confiscate guns and debunking myths propagated by the Republican Party.

</summary>

"Leftists don't want your guns. That's not a thing. That is made up. That's not real."
"Australia today has more guns than it did before Port Arthur, before the ban."
"Conservatives took guns in Australia. Not liberals, not leftists."
"The whole theory, this whole talking point, it's a straw man that was created for ignorant people to use."
"May God have a good day."

### AI summary (High error rate! Edit errors on video page)

Critiques a tweet from Madison Cawthorne about the Second Amendment and Australia.
Points out the misinformation and misunderstandings in the tweet.
Explains that leftists are not out to take guns from people.
Mentions that it was conservatives, not leftists or liberals, who implemented the gun ban in Australia.
Debunks the myth that Australians had all their guns taken away after the ban.
Emphasizes the importance of understanding the truth rather than relying on manipulated talking points.
Indicates that the Republican establishment may prefer to keep their voters ignorant and easy to manipulate.
Stresses that the whole theory about needing the Second Amendment to protect freedom is manufactured and not based on facts.
Encourages Republicans to pay attention to the manipulation happening within their own party.
Summarizes by reiterating that leftists don't want your guns and that the misinformation surrounding the Second Amendment needs to be addressed.

Actions:

for republicans, gun owners,
Fact-check information shared by political figures (implied)
Educate yourself and others on the reality of gun control and leftist beliefs (implied)
Challenge misinformation within your political circles (implied)
</details>
<details>
<summary>
2021-12-01: Let's talk about an update on a ruined Thanksgiving dinner.... (<a href="https://youtube.com/watch?v=9lBBtW5883o">watch</a> || <a href="/videos/2021/12/01/Lets_talk_about_an_update_on_a_ruined_Thanksgiving_dinner">transcript &amp; editable summary</a>)

Beau provides an update on a Thanksgiving dinner where a woman stands up for reparations, leading to a confrontational yet insightful family exchange.

</summary>

"I had spent time arguing with somebody. I should have just slapped the second he called me a B word."
"Never apologize for standing up for what I believe in."
"Those hateful people have changed you. I like grandma."
"Tell the internet people I said hi and that it didn't ruin Thanksgiving."
"It might have been the best one I've ever had."

### AI summary (High error rate! Edit errors on video page)

Providing an update on a Thanksgiving dinner where a woman sought help for ruining the dinner by making the case for reparations.
The woman stuck to Beau's formula and engaged in a heated exchange with Uncle Bob.
Uncle Bob brought up Biden's settlement payments, triggering the woman to change the topic to Agent Orange compensation.
Uncle Bob was personally affected by Agent Orange and became defensive during the argument.
The woman mentioned reparations for slavery and segregation, causing Uncle Bob to react negatively.
Grandma intervened, shutting down Uncle Bob's offensive comments.
Despite tension, dinner proceeded smoothly with a large table and plenty of food.
The woman confronted Uncle Bob about his behavior, leading to a reflective moment for him.
After the dinner, the woman expressed regret for causing a scene but was affirmed by Grandma for standing up for her beliefs.
The woman reflected on the incident and received praise from Grandma for her actions.

Actions:

for family members,
Stand up for what you believe in, even if it causes tension (exemplified)
Shut down offensive or disrespectful comments in your presence (exemplified)
Engage in difficult but constructive dialogues with family members (exemplified)
</details>
