---
title: Let's talk about a new study and a milestone....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WisPUJ4I-xw) |
| Published | 2021/12/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares a new study from South Africa about a new variant, stressing the need for all information before drawing conclusions.
- Mentions the grim milestone of 800,000 COVID-19 deaths in the United States, likely to reach a million.
- Emphasizes that the study discussed is not peer-reviewed yet.
- Notes the rapid spread of the new variant and the importance of being informed.
- Reports that the new variant is spreading quickly and the findings seem to match known information.
- Presents a mix of good and bad news regarding the new variant.
- Good news: The variant appears to cause milder symptoms and fewer hospitalizations in South Africa.
- Caution from experts about lag time in hospitalizations and vaccine effectiveness.
- Bad news: The Pfizer vaccine seems less effective at preventing infection with the new variant.
- Good news: The Pfizer vaccine remains effective at reducing hospitalizations, although less so than before.
- Speculates that the new variant may displace the Delta variant and seems to affect younger people more.
- Mentions the lack of widespread booster access in South Africa and the potential impact of boosters in the United States.
- Advises taking precautions such as washing hands, wearing masks, and getting vaccinated and boosted.

### Quotes

- "Wash your hands. Don't touch your face. Wear a mask if you have to go out. Get vaccinated, get a booster."
- "The odds are pretty good that this is accurate."
- "Y'all have a good day."

### Oneliner

Beau from South Africa provides insights on a new COVID-19 variant, stressing cautious optimism and the importance of staying informed, vaccinated, and boosted.

### Audience

Health-conscious individuals

### On-the-ground actions from transcript

- Wash hands, avoid touching face, wear masks, get vaccinated and boosted (implied)

### Whats missing in summary

Importance of staying updated with new information on COVID-19 variants and vaccines.

### Tags

#COVID-19 #SouthAfrica #NewVariant #Vaccines #BoosterShot


## Transcript
Well, howdy there, internet people.
Lidsbo again.
So today, we're going to talk about a new study
out of South Africa and kind of go
through it in hopes of providing a little bit of information.
However, this study has a lot of caveats to it.
So don't just hear the first part and take off.
You need to hear all of it.
Before we get into that, I do want
to point out that the United States has
passed a pretty grim milestone, 800,000 gone.
And it seems incredibly likely that that number will
pass a million before this is over.
OK, so the new study, it's out of South Africa,
talking about the new variant.
The first thing that I want to point out
is that under normal circumstances,
I wouldn't even be talking about this study yet,
because it's not peer reviewed.
It has not been peer reviewed.
It has not been peer reviewed.
I feel like I've said that enough now.
However, the one thing that everybody can agree on
when it comes to the new variant
is that it is spreading very, very, very quickly.
So you need to have as much information as you can.
The other thing is that all of the findings
kind of track with the information that is known.
So the odds are pretty good that this is accurate.
All right.
So we're going to play a game of good news, bad news here.
The good news, it does appear that it is more mild.
It looks like in South Africa, there
was a 29% reduction in hospitalizations
from what's occurring now to back to when it started.
So that's good.
However, experts do want to caution
that sometimes hospitalizations can lag a couple of weeks.
So while it looks this way, we don't know for sure.
And this hasn't been peer reviewed.
Okay, bad news.
The Pfizer vaccine seems less effective
at preventing infection.
Good news is that it's still pretty good at reducing
hospitalization.
Initially, that number was 93% with the new variant.
It's around 70%.
That's a pretty big reduction.
However, 70% is still pretty good.
Definitely worth, still definitely
worth getting the vaccine.
It does seem likely that the new variant will displace Delta.
It also seems to be hitting younger people than before.
But that may have to do with them just being freer
and more prone to large crowds, so on and so forth.
Don't really know that yet.
Now, this is a mixed bag of news here.
In South Africa, they don't have widespread access to boosters.
So that's bad news.
Good news is, in the United States, they're here.
The boosters may change some of these numbers.
It may make the vaccines more effective, but we don't know.
I felt like this was important information to share early,
simply because a whole lot of people
are going to be gathering soon.
So you want to be armed with the best information you can be.
At the end, as far as I'm concerned,
what do you need to do?
Wash your hands.
Don't touch your face.
Wear a mask if you have to go out.
Get vaccinated, get a booster.
It's worth noting all of this has
to do with Pfizer, with the Pfizer vaccine.
As more information comes out, I'll
let you know if I come across anything.
This just seemed like it was worth putting out today.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}