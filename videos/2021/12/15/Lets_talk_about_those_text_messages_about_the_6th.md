---
title: Let's talk about those text messages about the 6th....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Jr2a4O1T200) |
| Published | 2021/12/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Mentioned the lesser-discussed text messages sent on January 6th, urging Trump to call off the Capitol riot.
- Speculated on the reasons for some text messages not having names attached.
- Suggested that withholding names could be a strategic move to be revealed before the election or as a warning.
- Raised the possibility of Cheney being involved in the strategic decision-making due to her father's influence.
- Discussed the potential implication of criminal charges against Trump based on the committee's statements.
- Proposed that the actions taken by the committee were more about sending a message than just revealing information.
- Concluded with the belief that the unfolding events were a power move to convey a message effectively.

### Quotes

- "They're playing to win dirty."
- "Maybe she learned a lot from him."
- "It's not like the committee doesn't know the numbers."
- "I think this was much more of a power move than people are giving it credit for."
- "Y'all have a good day."

### Oneliner

Beau speculates on the strategic reasons behind unnamed text messages and potential criminal charges, suggesting a power move beyond surface revelations.

### Audience

Political observers

### On-the-ground actions from transcript

- Contact your representatives to express your thoughts on accountability in political actions (suggested).
- Stay informed about ongoing political developments and their implications (implied).

### Whats missing in summary

Deeper analysis and context on the potential power dynamics and strategic messaging in political actions.

### Tags

#TextMessages #Cheney #CapitolRiot #CriminalCharges #PoliticalStrategy


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about those text messages,
like pretty much everybody else.
But we are going to focus on the ones that aren't really
getting much play, and then something else that was said.
Don't get me wrong, the messages originating
with Trump's own son and the employees there at Fox,
There's definitely a lot of discussion there, but that's quick.
In the midst of what was going on on the 6th, they texted him.
They were sending messages to him, reaching out to him, trying to get him to call it off.
And then they tried to absolve him of responsibility in front of their audiences.
the network itself spent a year trying to pin the blame on anybody but him.
And there is certainly room for discussion there, but I think everybody's going to be
talking about that for a while.
I want to talk about the text messages that don't have names attached to them because
that seems odd.
You have to wonder why they didn't assign the name, right?
I've been thinking about it, and I can only come up with a couple of reasons.
But they don't make any sense, because honestly, if that's what they're doing, they're playing
to win.
They're playing to win dirty.
They're playing hardball.
And generally speaking, Democrats don't really do that very often, so I have a hard time
believing that's what it is. But see there's that other factor. It's not just
Democrats. That's Cheney up there. What if she really is her father's
daughter? Whether you like Cheney or not, nobody's ever going to accuse him of
pulling a punch politically. That wasn't the thing. Unless of course, you know, he
He could save that information for later use.
That would fit more with her dad.
Maybe she learned a lot from him.
Because the only two reasons I can come up with for this, one, they're just going to
hold the names until right before the election, which yeah, I mean that would probably swing
a point or two.
Or it's a shot across the bow.
It's a shot across the bow saying, hey, we have your messages too.
You might want to reconsider your position or you're next.
That's a possibility as well.
And that would truly be, well, a lot like her dad.
It's a possibility.
We don't know that's what it is, but I can't think of any other reason to do it.
It's not like the committee doesn't know the numbers.
It's not like it would be hard to track that back.
So they have to have a reason for leaving those names out.
I doubt it's professional courtesy.
To be honest, I'm kind of willing to bet
that Cheney's kind of ready to metaphorically burn
the Republican House down on her way out the door.
So I don't think she's just trying to be nice.
And then there's that other thing
that isn't getting much attention,
although I have seen this at least brought up.
She, you know, they were talking about the other question before the committee about
whether or not former President Trump corruptly attempted to interfere with the proceedings,
except that's not how it was said.
The way it was said was straight out of a statute, straight out of a federal statute.
The implication being that the committee might refer the former president himself for criminal
charges. Now, I've made no secret of the fact that I think that's really unlikely
because in DC there is a long-running history of attempting to protect the
institution of the presidency. But perhaps if the institution of the
presidency attacks the institution of democracy, maybe democracy wins in that
little coin toss. So, maybe it's just being thrown out there for no reason. Or, rather
than it just being them tipping their hand and saying they're considering referring
the former president of the United States for criminal charges, maybe that's also a
shot across the bow. Maybe that's also saying, hey, you do understand that if a
criminal investigation into the president, into the former president, for
the events of the 6th occurs, well there's a whole lot of people that would
get swept up in that. I think this was much more of a power move than people
are giving it credit for. I don't think this was really about just outing out
some some figures on Fox. I don't think that's what was the main goal here.
I think it was sending a message, and I'm fairly certain that message was
received. I can't think of any other reason for the day to have unfolded the
way it did. Now that's speculation to be clear. I don't have any evidence to back
that up, but I can't think of any other reason. I would love to hear one if you
have it, because I don't want to get my hopes up thinking that, you know, Cheney
taught the Democrats how to play hardball, but it's kind of the only
I can think of. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}