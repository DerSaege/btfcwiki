---
title: Let's talk about new foreign policy messaging from Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=htWCoAk8Q4s) |
| Published | 2021/12/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- American foreign policy messaging is shifting towards framing Cold War 2.0 as democracy versus authoritarianism.
- Biden's conference about democracy resurgence was actually about dividing countries into those loosely or closely allied with the US.
- The new messaging aims to motivate Americans against authoritarianism, similar to the anti-leftism push during the Cold War.
- Pushback against anti-authoritarian messaging will reveal the true authoritarians in the US.
- Those opposing the messaging likely support authoritarianism and may start to closely ally themselves with authoritarian leaders like Putin.
- Just as leftist ideas became unacceptable during the first Cold War, support for authoritarian ideas will become taboo in the US.
- Framing the near-peer contest as democracy versus authoritarianism might not represent the true motivations behind foreign policy actions.
- Despite the anti-authoritarian messaging, foreign policy decisions are primarily driven by power dynamics.

### Quotes

- "Cold War 2.0 is going to be democracy versus authoritarianism."
- "To be an American patriot, you have to be against authoritarianism."
- "Those people who are in favor of an authoritarian system, they're going to do everything they can to stop this messaging from taking hold."
- "It's not really going to be democracy versus authoritarianism. That's the gift wrapping for this little contest."
- "What's behind the contest is the same thing that is behind everything in foreign policy, power and nothing else."

### Oneliner

American foreign policy messaging shifts towards framing Cold War 2.0 as democracy versus authoritarianism, motivating Americans against authoritarianism, but the true motivations remain rooted in power dynamics.

### Audience

Policy Analysts

### On-the-ground actions from transcript

- Analyze foreign policy decisions and messaging to understand underlying power dynamics (implied).

### Whats missing in summary

The transcript dives into the surface framing of American foreign policy messaging but encourages deeper analysis of the true motivations rooted in power dynamics.

### Tags

#AmericanForeignPolicy #ColdWar #Authoritarianism #AntiAuthoritarianism #PowerDynamics


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the future
of American foreign policy messaging
because there have been some interesting developments
and some of them are going to be very humorous.
It appears that the strategy behind the messaging
that is going to go out with our near peer contests
has been decided.
And we got a sneak peek of it
with Biden's little, his conference
about embracing a resurgence of democracy.
And we have to appeal to those authoritarian countries
and get them to embrace democracy again, right?
And that sounds good.
It sounds like that would be a good purpose
for a conference.
The thing is, none of the authoritarian nations were invited.
It's not what it was about.
It was about dividing the world up
between the countries that are loosely aligned
with the United States,
whether they're in favor of democracy
or they're authoritarian, doesn't matter.
They were invited.
Those countries that are more closely aligned
with China and Russia, well, they weren't.
This is the framing that's going to go out.
Cold War 2.0 is going to be democracy
versus authoritarianism,
the same way the first Cold War
was defined by capitalism versus communism,
or it was framed as democracy versus communism.
Good.
That's great framing,
not because I really think the United States
should be entering into another long string
of near peer contests,
but because this messaging
is going to be used to motivate the American people
the same way there was messaging,
and by messaging, I mean propaganda, just to be clear,
the same way messaging during the Cold War
became very, very against leftism,
the new near peer contest
is going to be very, very against authoritarianism.
And if there is going to be one tangible good thing
that comes out of it, it's going to be this,
because you will see the media and politicians
begin to embrace anti-authoritarian talking points
and use them to motivate the American people
in this new Cold War.
It will end up, assuming their plan works,
it will end up meaning that to be an American patriot,
you have to be against authoritarianism,
which is fantastic.
That should be true.
Now, there's a whole bunch of actual anti-authoritarians
in the United States right now
that are laughing in an incredibly cynical manner.
And yeah, a lot of this is really hypocritical.
The United States has a long history of authoritarianism,
but that's not going to be the messaging.
We're not talking about realities.
We're talking about what is going to be pushed out
as the framing device for this near-peer contest.
And that certainly appears to be what they have decided on.
And I mean, if you're going to have framing, this is good.
This is good. I like this.
Now, another humorous development
that is going to come from this is you'll be able to watch
and figure out who the real authoritarians
in the United States are.
The people who push back against this messaging,
if I had to guess, Tucker Carlson, Newsmax,
maybe Hannity, people like that,
they're going to push back really hard
because they want authoritarianism.
They will do anything to make sure
that this messaging doesn't take hold.
So you'll start to see them more closely align themselves
with the Putins of the world.
In fact, you've already started to see that.
They're trying to paint him as some kind
of benevolent dictator or something.
And that'll go on until there's a tipping point.
You know, we can look to the first Cold War
to see how this is going to play out.
You know, a lot of times history determines the future.
There's a link, so to speak.
And just like during the first Cold War,
it suddenly became unacceptable
to have leftist ideas in the United States,
it will become unacceptable
to have authoritarian ideas in the United States.
So those people who are in favor of an authoritarian system,
an even more authoritarian system in the United States,
they're going to do everything they can
to stop this messaging from taking hold.
It's not because they don't want the contest.
It's not because they don't want
this near-peer confrontation.
They've been ranting China, China, China for years, right?
They've been egging it on.
They've been talking about how, you know,
their military is doing this
and ours isn't masculine enough and whatever.
They've been pushing it.
But now that they're going to see the framing, it's changing.
They're not going to want it anymore
because it will undermine their own beliefs,
their own messaging.
I can only hope that this framing takes hold.
Now, one of the things you should really understand
is that it's not really what's happening out there.
It's not really going to be democracy
versus authoritarianism.
That's not...
That's the gift wrapping for this little contest.
It's not really what's inside of it.
If you look at the list of countries
that were invited to the, you know,
Love Democracy Fest that Biden held,
there were a lot of countries that aren't exactly free
that were invited,
but they aligned more closely with the US.
So while the messaging is good,
it's going to be against authoritarianism.
Just always bear in mind that it's just that.
It's messaging.
It's messaging.
It's not really what's behind the contest.
What's behind the contest is the same thing
that is behind everything in foreign policy,
power and nothing else.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}