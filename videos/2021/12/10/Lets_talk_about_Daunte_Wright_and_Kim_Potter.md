---
title: Let's talk about Daunte Wright and Kim Potter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Kvx__g-2R84) |
| Published | 2021/12/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides commentary on an ongoing trial involving the Duane Wright case.
- Expresses disbelief in the potential for a conviction due to lack of intent to kill by the police officer.
- Notes that the officer is charged with manslaughter, not murder, reflecting the lack of intent as a key factor.
- Suggests that the strongest evidence for the officer's lack of intent is her shouting "taser, taser, taser" before the incident.
- Comments on the rarity of appropriate charges in cases involving law enforcement.
- Draws a parallel between a movie scene and the situation in the Duane Wright case to illustrate the seriousness of the charges.
- Speculates that the trial may focus more on differentiating between degrees of guilt rather than innocence or guilt.
- Advises understanding the statutes behind the charges to form informed opinions on legal cases.
- Emphasizes the distinction between legal, moral, and justice aspects in such trials.

### Quotes

- "The charges are appropriate."
- "Legal and moral and legal and justice, those aren't always the same thing."
- "Y'all have a good day."

### Oneliner

Beau provides commentary on the Duane Wright case trial, expressing doubts about a potential conviction due to the lack of intent and discussing the significance of appropriate charges in cases involving law enforcement.

### Audience

Legal Observers

### On-the-ground actions from transcript

- Understand the statutes behind charges ( suggested )
- Form informed opinions on legal cases ( exemplified )

### What's missing in summary

Exploration of the nuances and challenges in ensuring justice within the legal system.

### Tags

#LegalSystem #PoliceBrutality #DuaneWright #Trial #Justice


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to do something we never do on this channel.
We're going to talk about an ongoing trial.
I don't do it because, well, for a whole bunch of reasons.
But one of the main ones is if you start commentary on a trial,
as soon as the next day rolls around and there's a new development,
people want commentary on that.
And then the channel turns into court TV,
and I just really don't have any interest in that.
So I normally don't comment on ongoing trials,
as far as the developments in the case.
This is a little different because it's not really about the trial,
the question anyway.
I understand that you don't normally talk about trials
that are happening right now.
But could you maybe make an exception or at least send me a message?
Because after watching the full video of the Duane Wright case,
I don't understand how they can get a conviction.
I do not believe that cop meant to kill him.
I don't believe that cop meant to kill him.
Prosecution doesn't believe that cop meant to kill him.
The charges reflect that.
The officer is not charged with murder, charged with manslaughter.
Intent meaning to do it, that's not part of it.
That is not part of it.
I'm not a lawyer.
I'm not an expert on the statutes up there.
But a plain reading says that it really revolves around culpable negligence
and the reckless handling of firearm.
Watch the video again and see if you see that.
Because intent isn't part of this.
As far as intent goes, the strongest evidence
would be her shouting taser, taser, taser,
showing that she didn't intend to do it.
That's also the strongest evidence, in my opinion,
for reckless handling of a firearm.
I think what's going on is that we are so accustomed to prosecutors
either overcharging law enforcement so they can get political brownie points,
or undercharging them so they can get any conviction at all,
or they want to let their buddy off, that we
don't know what to do when the charges actually reflect what's in the video.
This is probably the best charging I've ever seen when it comes
to a case involving law enforcement.
What seems apparent in that video is directly reflected in the charges.
Now, maybe the defense has more evidence.
Maybe the prosecution has more evidence.
But based on what we've seen, those charges are appropriate.
So to put it into another perspective,
I noticed that your profile picture is a scene from a movie.
And I want you to think about that movie
and understand the cops would not have cared
whether the car went over a bump or not.
They shot Marvin in the face.
And that's what you're talking about right now.
That's the type of charge you're looking at.
So the defense has a huge uphill battle,
because basically they have to demonstrate that what is seen in that video
didn't happen.
So I don't really know that this is a trial that is going
to determine guilt or innocence.
I think it's going to be more of a trial that's
determining between man one and man two and what the jury sees
as far as that's concerned.
Just when you're looking at cases like this,
make sure you understand the statutes behind the charges.
Because if you don't, you might have an opinion
that isn't based on the realities of the legal system.
And the other part to remember is when you're talking about it at this point,
you're not really talking about justice anymore.
You're not talking about morality.
You're not talking about all these ideals.
You're talking about law, what's written down.
Legal and moral and legal and justice, those aren't always the same thing.
They rarely are.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}