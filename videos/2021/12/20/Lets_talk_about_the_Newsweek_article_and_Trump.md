---
title: Let's talk about the Newsweek article and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-faO1Vc9-FI) |
| Published | 2021/12/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Newsweek article suggests millions of Americans could seize power if Trump loses in 2024.
- Social media posts and events with weapons influence this narrative of millions of Americans ready to act.
- Only 0.6% of the population represents millions of Americans, creating a magnified fear.
- Recent polling shows only a quarter of Americans want Trump to run in 2024.
- Winning comfortably in 2024 may not be feasible for Trump, and his base is aging.
- Putting Trump back in office could signal the end of democracy and the American experiment.
- Scaring people into believing Trump must win by a large margin is not beneficial.
- Building and fixing problems are more effective than destruction for making the country better.
- Macho banter and tough talk won't solve the issues; constructive actions are needed.
- Encouraging outcomes presented by the article through alarm may be counterproductive.

### Quotes

- "You don't make your country great by destroying stuff. You get it by building."
- "If you want to be a tough guy, destruction is easy, building, making the country better, that's hard."
- "Scaring people into believing Trump must win is not beneficial."
- "You won't fix the country by destroying stuff."
- "Being alarmed over this possibility now is very self-defeating."

### Oneliner

Newsweek suggests millions could seize power if Trump loses, but fear may encourage destructive outcomes; building, not destroying, makes the country better.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Build and work towards fixing problems in your community (exemplified)
- Encourage constructive actions over destructive behavior (exemplified)

### Whats missing in summary

The full transcript includes detailed analysis and commentary on the potential consequences of fearmongering tactics and the importance of constructive actions for societal improvement.

### Tags

#Fearmongering #ConstructiveActions #Democracy #Trump #CommunityBuilding


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're going to talk about that Newsweek article.
It's causing quite a stir.
If you were waiting for the analysis of the op-ed from the
generals that was supposed to come out today, it's going to
come out tomorrow, because I think it's more important to
address this.
Similar themes, though.
If you don't know what I'm talking about, Newsweek
released an article that kind of suggests that there are millions of Americans out there
ready and waiting in the wings and that if Trump loses in 2024, well, they're going
to seize power and that perhaps the only way to avoid civil conflict is for Trump to win
by comfortable margins in 2024, three years from now.
A whole lot can change in three years, a whole lot.
incredibly irresponsible to put this out.
These types of contests, they amount to PR contests, and Newsweek is providing free PR.
We need to kind of go through some of this.
Millions of Americans.
When you say it like that, that's scary.
That's a whole lot of people right there, millions of Americans.
I'd like to remind everybody we live in a country of more than 300 million people.
0.6% of the population is millions of Americans.
Aside from that, where are they getting this from, this idea of millions of Americans?
Social media posts, people who show up at events with their rifle and their plate carrier
that's never been dirty.
That's where they're getting it from.
It's that image that is leading to this idea, millions of Americans.
Those social media posts play a lot into this.
It might be a really good idea for the people who wrote this article to talk to the people
who covered how this sort of thing is countered overseas and ask them about, I don't know,
contact circles.
All of those posts that are made, they're all sitting on a server somewhere with their
contact circle on it.
Can you imagine being able to get contact circles without even having to do phone intercepts?
If this was to proceed to that level, many of the people they're talking about, that's
just the people who get picked up first.
And you have the idea that this millions of Americans, that it represents a lot of people.
Let's be real clear on this.
Recent polling suggests that only one out of four Americans even wants Trump to run
in 2024.
That doesn't mean that they're willing to get out there in the field for him.
One out of four.
His star, it's declining.
declining. This is a man who not too long ago couldn't even fill a 20,000
seat stadium, only got five or six thousand seats sold. Doesn't seem
like he's had of quite the movement that is being painted. Now don't get me wrong,
don't underestimate this man, but don't turn him into a figure of mythical
proportions either because that attracts followers.
Then the idea that the only way or possibly the only way to avoid conflict would be for
him to win by comfortable margins.
First I want to point out that if he wins, it's over.
The American experiment is over.
The republic is over.
Democracy.
ideals that people talk about, they're done.
It's gone.
He has already proven that he will do anything to maintain power.
Putting him back in office, it's over.
It's over.
Let's start with that.
So maybe it's not a good idea to scare people into that position.
Aside from that, that won't happen.
He isn't going to win by comfortable margins.
I don't even think that he'd win, not at this point, but I don't know, it's three years
from now.
I wouldn't be so bold as to make a hard prediction based on something like that.
But one in four wants him to run.
That's not enough to win.
That isn't enough to win.
It's also worth noting that that number is going to decline.
His base is aging.
They're older.
Many of them will age out of voting permanently over the next three years.
And then you have to factor in the fact that a whole lot of them won't do things like wear
a mask or get a shot.
It's going to increase that even further.
This is the kind of thing that can create a mythical figure that people might rally
behind.
This idea that, oh well, you're with me or bad things are going to happen.
It's not a good idea.
Now I understand that there are probably some people watching this who are part of those
millions of angry Americans.
You think you're going to fix the country through this way, you won't.
You won't.
Understand, I can mess stuff up with the best of them, but that's not helping.
It doesn't fix anything.
Doesn't fix anything at all.
You do not make your country great.
You don't help your community.
You don't help address the issues that your loved ones care about by destroying stuff.
You get it by building.
You fix it by building.
By actually working to fix the problems, not make them worse.
If you want to make this country great, you're not going to do it with a rifle, you're going
to do it with a drill gun.
Don't let all of this macho banter get to your head.
It's not going to help.
It's going to hurt.
It's going to destroy the thing that you claim you care about.
Three years from now is a long way off.
A whole lot can happen between now and then.
A whole lot.
Being alarmed over this possibility now is very self-defeating, and it may in fact encourage
the outcomes that are presented in that article.
If you want to be a tough guy, destruction is easy, building, making the country better,
that's hard.
what tough guys do. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}