---
title: Let's talk about BBB, the GDP, and Manchin....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FQxbTSggnDM) |
| Published | 2021/12/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Manchin announced his opposition to Build Back Better, siding with Republicans, impacting the bill's passing outlook.
- Wall Street's concern was negative growth implications due to failure to pass Build Back Better, leading to revisions in expected GDP growth.
- Goldman Sachs had to reduce expected GDP growth for three quarters due to the detrimental impact of not passing the bill.
- The decision will negatively impact the US economy for at least nine months, reducing growth projections significantly.
- Failing to pass Build Back Better will reduce economic output growth by a third, affecting the US economy by billions of dollars.
- This decision prioritizes causing the US to fail to blame Biden over helping those in need.
- The timing of this decision, right before Christmas, affects critical issues like tax credits that impact the most vulnerable.
- The Republican Party and Senator Manchin are portrayed as disregarding the needs of the poor in favor of political gain.
- The decision not to pass the bill is estimated to cost the United States tens of billions of dollars in economic growth.
- Goldman Sachs suggested that corporate tax rates probably won't increase, providing a silver lining amidst the negative impacts.

### Quotes

- "Failing to pass this will negatively impact the United States' economy for at least nine months."
- "The Republicans want the United States to fail right now because then you'll blame Biden."
- "Y'all better go buy some coal."
- "That might should be the talking point."
- "Never let these people fool you into thinking they care about you again."

### Oneliner

Senator Manchin's opposition to Build Back Better will negatively impact the US economy for at least nine months, prioritizing political gain over billions of dollars in economic growth.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact your representatives to advocate for policies that prioritize economic growth and support vulnerable populations (suggested).
- Join organizations focused on economic justice and advocacy for those in need (implied).

### Whats missing in summary

Analysis of the broader implications on society and vulnerable communities from the failure to pass Build Back Better.

### Tags

#BuildBackBetter #USPolitics #EconomicImpact #Senate #WallStreet


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about BBB and the GDP.
We're going to talk about Build Back Better
and the Gross Domestic Product of the United States.
If you missed it, Senator Manchin decided
he was going to announce his opposition to Build Back Better.
He was going to side with the Republicans.
And that really, let's just say it dimmed the outlook
of this passing.
As soon as the announcement happened,
as soon as he said that, there was all kind of commentary.
Tons, tons of commentary.
And most of it focused on the partisan side,
him crossing partisan lines, him not really being a Democrat,
so on and so forth.
Don't get me wrong, there is room for partisan discussion
here.
But I want to talk about something a little bit more
important.
I want to talk about how this decision is actually
going to impact the United States as a whole.
Because while that commentary was happening,
while the average person who is interested in politics,
while they were talking about the political aspects of it,
the people up on Wall Street, oh, they
had a very different take.
They were concerned about something else.
Goldman Sachs, an organization that
has a long running history of caring for poor people, right?
No, of course not.
They reached out to their investors,
basically said that failure to pass Build Back Better
had negative growth implications.
Negative growth implications.
And they had to make some revisions.
They had to change the expected growth of the GDP.
They had to reduce what they believed
the economy would grow by.
This decision was so bad, failing to pass this
is so detrimental that the analyst at Goldman Sachs,
well, they had to reduce the expected growth of the GDP.
It was so bad, it is going to reduce
the growth of the economic output of the entire country.
And it's not a short term change.
They reduced the first quarter estimate from 3% to 2%.
The second quarter from 3.5% to 3%.
The third quarter from 3% to 2.75%.
This decision was so bad, not only
did they have to alter the expected growth of the GDP
and reduce it, it goes out for three quarters.
Nine months.
Failing to pass this will negatively
impact the United States' economy
for at least nine months.
And when you hear these numbers, it doesn't sound like a lot.
3% to 2%, that's the first quarter.
It's just a 1% reduction.
Another way to say that would be that this decision was so bad
that failing to pass this is so damaging that it reduced
the expected growth of the US economy by a third.
That's another way to say it.
When you're talking about percentages like this,
it kind of lures you into a false sense of security
because they're small numbers.
So let's put it in perspective.
The GDP of the United States is roughly $21 trillion.
To keep the math simple for me, we're
just going to say $20 trillion.
The first quarter, let's say that that
is going to be the reduction across the board.
It's just going to be a 1% reduction for the whole year.
OK, so 3% of $20 trillion is $600 billion.
2% is $400 billion.
The Republicans and Senator Manchin
deleted $200 billion of economic growth.
That's how that would work out.
Now again, we're just using the first quarter
and assuming it goes on for the whole year.
That's pretty substantial.
Why would they do that?
You think these people aren't invested with folks up
on Wall Street?
You think they don't know?
People say things like the cruelty
is the point all the time.
In this case, that's it.
The Republicans want the United States
to fail right now because then you'll blame Biden.
It's that simple.
I mean, I want you to think about the timing of this.
Right before Christmas, that expanded tax credit,
the child tax credit, not going to happen, huh?
Incidentally, Goldman Sachs said that that
was the most important question to near-term outlook.
You know, an organization that cares about poor people, right?
No, of course not.
They know if the people don't have money,
they're not going to spend any.
It's good for them, too.
They're not advocating for those who really need it.
They just know that if people have it, they'll spend it.
But Manchin and the Republicans have
decided that's not important because what's more important
is for the United States to fail, so you blame Biden.
It's really what it boils down to.
Right before Christmas, the general attitude
of the Republican Party and Senator Manchin is, hey,
you know what?
Forget about those poor people.
Y'all better go buy some coal.
Oh, well, I guess we see Senator Manchin's motivation here.
I understand the desire to focus on the partisan aspects,
but the reality is this cost the United States
tens of billions of dollars in economic growth.
That's how bad this decision was.
That might should be the talking point.
Now, there is some good news from Goldman Sachs, though,
when they were talking to their people.
There is some silver lining to this.
It's that, hey, you know, corporate tax rates probably
won't go up.
Never let these people fool you into thinking
they care about you again.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}