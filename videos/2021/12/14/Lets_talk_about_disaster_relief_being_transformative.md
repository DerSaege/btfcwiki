---
title: Let's talk about disaster relief being transformative....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mYzZ25B8Cg4) |
| Published | 2021/12/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Disaster relief is transformative because it encompasses all aspects of activism and charity work simultaneously, addressing basic needs like homelessness, food insecurity, lack of fresh water, and medical care.
- In disaster relief, the absence of electricity leads to a cascade of challenges such as closed stores, limited gasoline, full shelters, and communication breakdowns.
- Effective activism in disaster relief involves timely delivery of resources and assistance, working around the clock to ensure the right aid reaches the right people.
- Engaging in disaster relief leads to collaboration with unexpected partners, breaking down political or ideological barriers for a common mission.
- The intense and continuous nature of disaster relief work helps activists overlook internal conflicts within groups, fostering a singular focus on the mission.
- Participation in disaster relief activities exposes individuals to extreme situations, making subsequent activism in more stable conditions seem easier.
- Observing varied human behaviors during disaster response showcases both the unity and disarray that can unfold in communities during crises.
- Disaster relief work exposes individuals to the breakdown of societal norms and structures, requiring adaptability and quick thinking in rapidly changing environments.
- Engaging in disaster relief helps individuals develop critical skills like resourcefulness, networking, and collaboration, even with those they may not typically associate with.
- Participating in disaster relief efforts can enhance one's ability to navigate challenges, find necessary resources, and build connections, all valuable skills for achieving activist goals.

### Quotes

- "There's nothing that will make you a better activist than doing that kind of relief work in the immediate aftermath of some natural disaster."
- "It's constant. It is constant."
- "Once you have done that, nothing faces you."
- "Everything becomes easier because you've done it in the worst possible conditions."
- "Just helping people get what they need."

### Oneliner

Engaging in disaster relief work provides a transformative experience that hones critical skills and perspectives for effective activism under extreme conditions.

### Audience

Activists and volunteers

### On-the-ground actions from transcript

- Volunteer for disaster relief efforts (exemplified)
- Develop skills in networking and collaboration through disaster relief work (exemplified)
- Learn to adapt quickly to changing situations in society by engaging in disaster relief (exemplified)

### Whats missing in summary

The full transcript provides a detailed account of the unique challenges and transformative experiences encountered during disaster relief efforts, offering valuable insights into human behavior and societal dynamics under extreme conditions.

### Tags

#DisasterRelief #Activism #CommunityAid #HumanitarianEfforts #Networking


## Transcript
Well, howdy there internet people.
It's Bo again.
So today, we're going to talk about disaster relief and why it's transformative.
I said that in a recent video and I had somebody ask me about it.
And they're basically like, hey, here's your entire resume of activities like this.
Why do you think that disaster relief is transformative?
You've never said that about any of this other type of activist work.
And it's not hard to explain really.
It's unlike anything else.
Doing disaster relief is unlike anything else because it's everything else all at once.
Think about it, when people talk about activism and charity work and stuff like that, most
times they're trying to help out with homelessness, food insecurity, lack of fresh water, medical
care, base needs stuff, right?
You're dealing with all of that after a natural disaster.
The only difference is there's no electricity.
Because there's no electricity, there's no stores.
Because there's no electricity and no stores open, there's probably no gasoline.
Because you have a bunch of people who are probably without homes, the shelters, they're
full.
Hotels start to fill up.
You're probably sleeping in your car.
Your communications are spotty at best.
You're not hopping on the internet anytime soon because the phone lines and all that
stuff is probably down.
A big part of being an effective activist is getting the right stuff, the right people
to the right place at the right time.
And when you're doing disaster relief, that's all you're doing 24 hours a day for the entire
time you're there.
It's nonstop.
You end up working with people that you would never think you would work with.
I think I've told this story on the channel.
After Michael, my little crew, people like me, were working hand in glove with this right-wing
veterans group.
By right-wing veterans group, not like the kind that went up there on the 6th.
They weren't like that.
But all these folks had Trump flags.
But it never came up.
Because all of that got put to the side, mission first type of thing, right?
You do that for a couple of weeks, that normal leftist infighting that occurs in activist
groups, you don't even notice it anymore.
You're not even aware that it occurs.
It's not even on your radar.
It's nonstop.
And if you're doing it for a few days, by the end of the third or fourth day, you are
completely unfazed.
And you go drop off food at a location.
And when you get there, you find out the guy next door just cut his leg with a chainsaw.
So you have to put a tourniquet on it, throw him in the back of the truck, and get him
to a medical facility, whatever one is still standing.
And then while you're there, you get a radio call over a CB that you barely understand.
And you know that they need tarps at a different location than the place where you picked up
the guy who got cut.
They had tarps.
You go back there, get those, go to the place that needed the tarps.
And you find out before the storm, well, that was a really nice subdivision.
It had like a grains keeping place and everything.
And I bet they got gas in there.
It's constant.
It is constant.
If you go do disaster relief for two weeks with somebody who knows what they're doing,
that's a PhD level course in activism.
Once you are done, once you have done that, nothing faces you.
Because you have done activism in the worst possible conditions.
It doesn't get any harder than doing it that way.
And you end up seeing the best and worst of people.
Because right after it happens, everybody comes together.
It is, we are the world, kumbaya, everything, right?
Then once people get their base needs met and they start to feel a little bit more secure
for the moment, this weird paranoia sets in.
Somebody is going to come steal their fire or whatever.
And then it becomes uneasy at times.
Doing disaster relief, I have had more guns pointed at me doing that than just about anything.
It's a very different environment.
Law enforcement, it varies by the individual cop and how they're feeling at the moment.
Because some of them are just convinced that anybody who's coming from out of town, well
they're secretly a looter or something.
And others are going to give you an escort and flashlights as you drive through town.
And you never know what you're going to run into.
It is the unexpected and the unforeseen over and over and over again for the entire time
you're doing it.
If you want to get better at whatever kind of activism you're doing, do disaster relief
for a little bit.
Understand, as previously stated, it can get dangerous.
But if you do that, believe me, when you're done, you're going to look at everything differently.
Everything becomes easier because you've done it in the worst possible conditions.
And you have seen people behave in a variety of ways.
And you see how the things that we take for granted break down very, very quickly.
And how many people today don't know how to adapt to changing situations in society.
There was something that happened here where people, they had a medical need, not huge,
but big enough to matter, and they had sheltered in a restaurant.
And because they were so used to never going behind the counter, they had no idea that
a first aid kit was literally right around the corner.
If you do disaster relief, you start to see those normal societal constraints as more
flexible when disasters arrive because you've experienced it.
I can't say it any more clearly.
There's nothing that will make you a better activist than doing that kind of relief work
in the immediate aftermath of some natural disaster.
Just helping people get what they need.
Once you do that, you end up accidentally honing this skill of being able to find what
you have to have.
And being able to network even with people you don't like.
And if you want to achieve your goals, those are two skills you have to have.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}