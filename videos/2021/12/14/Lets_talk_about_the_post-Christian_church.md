---
title: Let's talk about the post-Christian church....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FVmny9-kI88) |
| Published | 2021/12/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces a term that has been on his mind and expresses his intention to talk about religion, specifically the Christian religion, which he usually avoids discussing.
- He questions whether churches truly embody the teachings of Christ or serve as power structures with different motives.
- Beau prompts listeners to ponder if their churches encourage behaviors like feeding the hungry, welcoming strangers, caring for the needy, visiting the sick and those in prison, loving thy neighbor, and forgiveness.
- He contrasts these teachings with potential negative attitudes encountered in churches, such as stigmatizing welfare recipients, advocating for walls, dismissing the needy, and lacking forgiveness.
- Beau challenges the perception of the United States as a Christian nation and suggests that churches should advocate for Christ's teachings in government policies.
- He concludes by questioning if churches are genuinely Christian or merely existing as post-Christian power structures focused on socialization rather than embracing Christ's teachings.

### Quotes

- "Does your church encourage forgiveness? Or does it encourage you to hold a grudge?"
- "If you are a Christian, this is something you're going to have to reconcile."
- "Is it a Christian church? Or is it post-Christian?"
- "Suddenly I have an image of somebody walking around flipping over tables and chasing people with a scourge."
- "It is far easier and far more marketable if you give permission to be your worst rather than provide encouragement to be your best."

### Oneliner

Beau raises thought-provoking questions about whether churches truly embody the teachings of Christ or function as post-Christian power structures, urging Christians to reconcile teachings with church practices.

### Audience

Christians

### On-the-ground actions from transcript

- Question whether your church truly embodies Christ's teachings (implied)
- Advocate for church practices that genuinely encourage Christ-like behaviors (implied)
- Engage in introspection regarding the alignment of church teachings with personal beliefs and actions (suggested)

### Whats missing in summary

Beau's emotional delivery and emphasis on self-reflection can be best experienced by watching the full video.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about a term that I have had just rattling around inside my head.
So we're going to end up talking about something we normally don't talk about on this channel.
One of the topics that I try to avoid for the most part.
But to be honest, I think if I don't talk about this,
this term will just stay stuck in my head forever.
So here we are.
So today we're going to talk a little bit about religion.
Specifically, the Christian religion.
And before we get into this, I do want to acknowledge that I'm aware that this isn't
all churches.
I'm very well aware of that.
I know churches that are not subject to this commentary.
That being said, I know a whole lot more that are subject to it.
So what is a church in general?
It's not a building.
It's not just a building.
It's a group of people.
It's a power structure.
It has power because of the group of people.
So a church is a power structure made up of a group of people.
A Christian church, in theory, should be a group of people who encourage the teachings
of Christ.
Kind of goes without saying.
Seems like it should.
Now I want you to think about your church, the one you go to or, for a lot of people
on this channel, the one you used to go to at some point in time if you were Christian.
Does it actually encourage the teachings of Christ?
Is that a thing?
That it goes on?
The people in that church, your peers in that church, I'm not talking about the sermon.
That's just something that happens.
The other people in the church, do they encourage that behavior, those teachings?
If you're being honest, what are you more likely to hear from your peers there?
That you should give the hungry something to eat, the thirsty something to drink?
Or are you more likely to be encouraged to call them welfare queens?
What about the stranger outside?
Are you encouraged to invite them in?
Or are you more likely to hear a chant of, build the wall, build the wall?
What about the needy?
Encouraged to clothe them, care for them, assist them, or tell them to get a job, be
bum?
What is more likely?
What about looking at them as sick, healing the sick, taking care of them, visiting them?
Willing to bet that in many churches, well, you give everybody health care, that's socialism,
and therefore something to be feared.
What about visiting people in prison?
Is that encouraged, or is it more one of those, he made his choices, lock him up and throw
away the key, lock her up, lock her up.
What about loving thy neighbor as thyself?
Or are you encouraged to other people?
Those people, they don't count as your neighbors.
So those people, you can hold them to a standard you could never live up to yourself.
Therefore always feel better about yourself.
What is more likely?
Does your church encourage forgiveness?
Or does it encourage you to hold a grudge?
To continue to look down on those people you've othered.
What's more likely?
And if most of the answers are from the second option, you have to ask yourself whether or
not it's really a Christian church, or maybe it's a post-Christian church, a power structure
that exists for other purposes.
You know, Christians are very, very fond of saying the United States was founded as a
Christian nation.
Now I've at length shown that that's not actually the case.
While there are a bunch of Christians who participated in the founding, a whole lot
of them signed onto a document that explicitly said the United States of America was not
founded in any way on the Christian religion.
But if it was a Christian church, wouldn't it probably use that power structure to try
to get the government to embody some of the teachings of Christ?
But I'm willing to bet that if through these questions most of your answers came from the
second column, if anybody proposed using the government to do any of this stuff, well that
church would not call them Christ-like.
They'd probably have some other names for them.
So is it a Christian church?
Or is it post-Christian?
Is it just a power structure that exists because people are used to it existing?
Just dogmatic, going through the steps, going through the motions, a place where people
show up to socialize and engage in whatever.
Not really about any of the teachings.
Suddenly I have an image of somebody walking around flipping over tables and chasing people
with a scourge.
This is a question that eventually people are going to have to ask.
If you are a Christian, this is something you're going to have to reconcile.
The teachings versus what occurs among your peers, among your church, that power structure.
And at some point it might be useful to acknowledge that it is far easier and far more marketable
and we'll get far more people in those seats if you give permission to be your worst rather
than provide encouragement to be your best.
Anyway it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}