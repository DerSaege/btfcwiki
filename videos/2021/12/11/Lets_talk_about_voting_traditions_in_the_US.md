---
title: Let's talk about voting traditions in the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CduQMNto8yA) |
| Published | 2021/12/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the historical tradition in the US of looking out for smaller demographics in the voting process.
- Addresses a message criticizing his video on voting rights in Texas.
- Refutes the claim that the US Constitution does not protect smaller demographics in voting.
- Points to Article 1, Section 3 of the Constitution, which created the United States Senate to safeguard the interests of smaller states.
- Argues that safeguarding smaller demographics in voting is an integral part of the government's structure.
- Emphasizes that ensuring equal representation is an evolving concept in the US.
- Criticizes gerrymandering and its impact on diluting voting power.
- Condemns efforts to suppress votes of those who may vote against certain parties.
- Calls out Republicans for resorting to voter suppression tactics due to a lack of new ideas.

### Quotes

- "If they weren't concerned about that, they wouldn't have a whole house where every state, no matter its size, no matter its population, got two votes."
- "Gerrymandering is wrong. I can't believe this is a conversation we're still having today."
- "The only way they can preserve power is by suppressing the vote of people who would vote against them."
- "These are actions of people who are supporting bad ideas and they know they're supporting bad ideas."
- "They'll undermine the founding principles of the country."

### Oneliner

Beau explains the historic tradition of safeguarding smaller demographics in voting, addressing criticism and condemning voter suppression tactics.

### Audience

Voters, Activists, Citizens

### On-the-ground actions from transcript

- Contact your representatives to advocate against gerrymandering and voter suppression (implied).
- Join or support organizations working to ensure fair voting practices (implied).

### Whats missing in summary

The full transcript provides a detailed historical context on voting rights and the importance of safeguarding smaller demographics in the voting process. Viewing the entire video can offer a deeper understanding of these concepts.

### Tags

#VotingRights #Constitution #Gerrymandering #Representation #VoterSuppression


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're gonna talk a little bit more
about voting rights, and we're gonna talk about
the traditions in this country, and the founding principles,
and how this country has historically, maybe,
looked out for smaller demographics at times,
because I got this message.
You are so disingenuous in your video
about voting rights in Texas.
You said the US had a founding principle
of protecting smaller demographics
from larger demographics in the voting process.
Wrong.
We don't cater to smaller demographics.
Cite the article section and clause of the Constitution
that shows this as a founding principle.
The larger demographic wins always,
then they make the rules.
If they don't like it, they shouldn't have come here.
The they who shouldn't have come here
would be black Americans and Latinos in Texas, in Tejas.
I think it goes without saying that a whole lot of people
who perhaps have a darker skin tone in Texas,
they didn't cross the border, the border crossed them
when the US took the area they were already living on.
And I'm totally just not even going
to get into the whole idea of black Americans shouldn't have
come here.
You might want to really think about those statements.
But aside from that, I will play your silly little game.
Cite article, section, and clause of the US Constitution
that shows a principle of looking out
for smaller demographics when it comes to voting.
OK, Article 1, Section 3, clause all of them,
of them in that section. Now, Article 1, Section 3, it creates something and it's
super obscure, perhaps you've never heard of it. It's called the United States
Senate, a whole house in the legislative branch. Its purpose was to kind of
safeguard the interests of smaller states or states that maybe had a large
geographic area but were more rural and therefore didn't have a lot of population to get a lot
of seats in the house.
This was how they safeguarded the smaller demographics and made sure that those states
had their interests represented.
It's not just like some obscure founding principle.
It's literally how our government is set up.
The idea of making sure that everybody has equal representation is an evolving one in
the United States.
When it first started, yeah, it was pretty limited as to who was going to get to vote
for the people who were going to represent them.
That has changed over time, but the tradition of making sure that the areas that because
they're smaller in geographic size or they don't have the numbers to outweigh other larger
demographics, making sure that their interests are protected is enshrined in the Constitution.
It is the Senate.
It's the reason the Senate exists.
If they weren't concerned about that, they wouldn't have a whole house where every state,
no matter its size, no matter its population, got two votes.
And that all the bills had to go through there.
It gave smaller demographics the ability to have a check.
Yeah, that doesn't seem that complicated.
I would also point out that if you
have to divide up and dilute the voting power of a group in a certain area to stay on top,
they're the larger demographic in that area.
Gerrymandering is wrong. I can't believe this is a conversation we're still having today. That seems
like it should go without saying. Like a whole lot of things in in this video. So
the point being Republicans are going to try to do this. They know that their
platform of fear-mongering is kind of growing old. They don't have any new
ideas to present. So the only way they can preserve power is by suppressing
the vote of people who would vote against them or by diluting the power of
that vote and mixing those districts in with Republican strongholds. These are
actions of people who are supporting bad ideas and they know they're supporting
bad ideas but they're so desperate to cling to power that they'll undermine
the founding principles of the country.
Anyway, it's just a thought.
With all, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}