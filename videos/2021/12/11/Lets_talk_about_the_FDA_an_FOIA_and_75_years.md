---
title: Let's talk about the FDA, an FOIA, and 75 years....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=51AXkd0peDk) |
| Published | 2021/12/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a claim about a request taking 75 years, Beau explains the situation behind a Freedom of Information Act (FOIA) request made to the FDA for documents related to the emergency approval of the vaccine.
- The request includes hundreds of thousands of documents that need to be processed, with a standard of providing 500 pages per month for large FOIA requests.
- The FDA will start releasing the information immediately, but it will take 75 years to process all of it due to the massive request size, not because they are trying to hide something.
- Beau mentions that judges have intervened in similar cases to expedite the process due to intense public interest, suggesting that bringing in additional resources could speed up the information release.
- He advises the plaintiffs to narrow the scope of their request to focus on relevant information, as many pages may contain redacted or duplicate content that could slow down the process.
- Beau stresses the importance of releasing pertinent information quickly to address vaccine hesitancy and increase vaccination rates, stating that public interest and transparency should be prioritized.

### Quotes

- "There's enough little screenshots that could be turned into memes that could lead people to believe that."
- "Start with the important stuff."
- "It's in Freedom of Information Act request. 500 pages a month is normal."
- "There's no secret here, there's no grand conspiracy, it's just the way it works."
- "If they actually do want to assure the vaccine hesitant, they should be trying to make it as easy as possible for the FDA to get that information out."

### Oneliner

Beau clarifies a claim about a 75-year wait for information release, urging for transparency and efficiency in handling a large Freedom of Information Act request.

### Audience

Government officials, activists

### On-the-ground actions from transcript

- Bring in additional resources from other agencies to expedite the processing of FOIA request (suggested)
- Plaintiffs should narrow the scope of their request to focus on relevant information and ease the processing burden (suggested)

### Whats missing in summary

Beau's detailed explanation on the process and implications of large FOIA requests can provide clarity on bureaucratic procedures and transparency challenges. 

### Tags

#FOIA #VaccineApproval #Transparency #Government #PublicInterest


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to go through a claim
that is being made.
And the claim says that a request is going to take 75 years.
And while that number is actually part of the story,
the way this is being presented is, well, let's just call it
a little less than accurate.
And it's causing a lot of apprehension
that probably shouldn't be there.
OK, so we're going to start from the beginning,
because there are elements of truth to this theory,
and we have to kind of separate it all out.
A Freedom of Information Act request was put in to the FDA.
And it requested all documents, all documents,
related to the emergency approval of the vaccine.
seen.
This is hundreds of thousands of documents.
Each document has to be processed.
Every document has to be gone through.
They have to look for patient information, trade secret information, so on and so forth.
It takes time.
With large FOIA requests like this, the standard is to provide the people who requested it
500 pages per month.
So they have requested so many that it's going to take like 75 years on that schedule.
Now the way it's being framed and presented is that the FDA is trying to hide something
and they're not going to release anything for 75 years.
That's not true.
They'll start releasing immediately, but it's going to take 75 years on that schedule
to process all of it because the request is so large.
This 500 pages per month standard, this runs across different agencies.
It's not abnormal that this is the way it works.
The plaintiffs, the people who made the request, they say they want it in 108 days because
that's how long it took the FDA to get emergency approval.
That is fantasy land.
That's never going to occur.
You're talking about an office that, if I'm not mistaken, has like a dozen people in it
processing thousands of pages per day.
Never going to happen.
At the same time, I am aware of cases where a judge was like, no, you need to break with
this schedule.
You need to produce more than 500 pages per month.
Now, all of the ones I'm aware of involve DOD, which has a much larger office for processing this stuff.
However, the reason that the judges use is intense public interest.
I think that applies here.
I think that applies here.
The plaintiffs are saying, hey, we want this information because it will assure those who are hesitant.
I don't know that I actually believe that's the reason they want it, but I believe that's true.
I believe that if this information was released that it would assure a lot of people.
I think there is intense public interest.
I think it's in the government's interest to get this information out as soon as possible.
possible. So I think it would be a good idea to bring in people, transfer them in
from other agencies who know how to do this and get this information out as
quickly as possible. But the idea that the FDA is trying to keep this secret
for 75 years, that's manufactured. That's not true. It's that the information would
take that long to process under the normal schedule.
At the same time, the plaintiffs do have another option.
They could narrow the scope of their request.
Realistically, a lot of the pages that they're going to get
that are going to slow down this request,
they're going to be blank by the time they get them.
They're going to be completely redacted
because they're going to contain a whole bunch
of patient information.
The plaintiffs could request anything that was adverse, anything that suggested they
shouldn't release it.
They shouldn't grant emergency approval and narrow the scope.
It would be less pages.
It would move faster.
So they have an option too.
From where I stand, I actually do believe that there's intense public interest and
I think it is in the United States' best interest to get this information out as soon as possible.
I mean, society as a whole, the more people who are less hesitant, the more people get vaccinated.
That's a good thing. That's a win. There are elements of truth to this story. While this isn't,
As far as I know, this isn't a widely circulating one at the moment.
It probably will become one because it can easily be framed as the FDA is trying to keep
this secret for 75 years.
That's not really what's happening.
But there's enough little screenshots that could be turned into memes that could lead
people to believe that.
And if they looked, they would find the FDA arguing that that's how long it's going
to take.
But it's not that that's when they would start releasing it.
That's when they would finish.
So my advice to the people involved would be the FDA should hurry up and release as
much as possible as soon as possible in the stuff that matters.
Start with the important stuff.
And I think the plaintiffs should probably narrow their scope, you know.
Don't request everything.
Request the stuff that matters and try to ease that burden a little bit.
This is something where if the plaintiffs are arguing in good faith and they actually
do want to assure the vaccine hesitant, they should be trying to make it as easy as possible
for the FDA to get that information out. Not as hard as they possibly can, but so
start to finish, that's where it goes. It's in Freedom of Information Act
request. 500 pages a month is normal. They requested so many pages that it's going
to take 75 years at that rate to get all of the information processed, but there's
There's no secret here, there's no grand conspiracy, it's just the way it works.
But there are exceptions that have been made in the past to the 500 pages per month thing,
and I think this qualifies as one, I think this should be one.
So anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}