---
title: Let's talk about Earth's Black Box....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QkUfOOij9C0) |
| Published | 2021/12/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the concept of humanity's legacy and the Earth's black box.
- Compares the Earth's black box to a black box on a plane, recording data to learn from past mistakes.
- Describes the Earth's black box as the size of a city bus, powered by solar energy, and capable of storing 30 to 50 years of data.
- Mentions that the Earth's black box will record information on climate change, world state tweets, pollution studies, health data, and species loss.
- States that the Earth's black box will be operational by 2022 with data already being compiled for it.
- Notes that the concept and creators of the Earth's black box are from Australia, but its location remains undisclosed.
- Emphasizes that the Earth's black box is designed to be indestructible and will require advanced technology to access.
- Mentions that information inside the Earth's black box will be encoded in math and symbolism for future understanding.
- Comments on the significance of individuals today working to leave something beneficial for future generations.
- Concludes by stating that the Earth's black box will either document humanity's greatest achievement or its final failure.

### Quotes

- "The Earth is getting its own black box."
- "It's meant to be indestructible."
- "One of its greatest triumphs, or it'll record its final failure."
- "A wild idea."
- "Y'all have a good day."

### Oneliner

Beau introduces the Earth's black box, a city bus-sized structure powered by solar energy to record 30-50 years of data for future generations, capturing humanity's potential triumphs or ultimate failure.

### Audience

Environmental enthusiasts, futurists

### On-the-ground actions from transcript

- Support initiatives focused on long-term data collection and preservation (exemplified)
- Advocate for sustainable energy sources like solar power (exemplified)
- Encourage education on climate change, pollution, and species loss (exemplified)

### Whats missing in summary

Importance of individuals working collectively towards a sustainable future through innovative projects like the Earth's black box.

### Tags

#Humanity #Legacy #Earth #BlackBox #ClimateChange #FutureGeneration


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about humanity's legacy, what
we will be handing to the future.
The Earth's black box.
You know, a black box on a plane.
People are familiar with that.
Indestructible little box that records all the data.
So in the event of something bad happening,
people can go through it and try to figure out what went wrong,
stop it from happening again.
It's a tool to help those in the future
learn from the missteps of those in the past.
The Earth is getting its own black box.
It's going to be about the size of a city bus,
made of steel, powered by solar.
It'll have the equipment inside to store
30 to 50 years of data.
It will record information related to climate change,
tweets about the state of the world,
studies on pollution, health information, species loss,
stuff like that.
It'll be operational by 2022.
But they're already compiling the data for it.
Once it's up and running, you'll be
able to access the information via an online portal.
The Earth's black box.
Now, it's being put together.
The concept and the people behind it
looks like they're all from Australia.
Where the box will be, well, that's undisclosed.
They're going to put it somewhere where hopefully it
will survive.
It's meant to be indestructible.
So to open it, it's going to take a lot of advanced
technology.
Because of that, and because they
don't know who's going to find it or when,
the information will be encoded in math and symbolism.
So it can be easily understood by those in the future.
So they can learn from it.
It's a wild concept.
One of the things about it that is very striking to me
is the idea that there is a segment of humanity
today who is prepared to try to leave something better,
leave something helpful for those in the future.
Yet, there may not be enough of them,
of that demographic of people, those
who care about the future, to stave off, well,
the need for the Earth to have a black box.
It's wild.
It's a wild idea.
At the end of the day, this setup,
it will either record one of humanity's greatest
achievements, one of its greatest triumphs,
or it'll record its final failure.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}