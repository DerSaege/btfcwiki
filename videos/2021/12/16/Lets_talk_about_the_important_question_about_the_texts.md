---
title: Let's talk about the important question about the texts....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ixjLUFX1zhI) |
| Published | 2021/12/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau raises the importance of questioning the intent behind certain text messages related to former President Trump.
- The key question revolves around why the messages were sent in the first place.
- He points out that the intent behind the messages is critical, especially for supporters of Trump.
- There are two possible intents behind the messages: either the senders believed Trump supported the events or they thought he was incompetent.
- Beau stresses that understanding the reason for sending those messages is vital in understanding the mindset at that time.
- The focus should be on why multiple individuals close to Trump felt the need to send those messages.
- Beau leaves the audience pondering which scenario they hope it is.

### Quotes

- "Why were those messages sent?"
- "That's what matters."
- "Understanding the reason for sending those messages is vital."
- "The intent behind the messages is critical."
- "I'm dying to know which one you hope it is."

### Oneliner

Beau raises the importance of questioning the intent behind text messages related to former President Trump, stressing that understanding the reason for sending those messages is vital.

### Audience

Supporters and skeptics

### On-the-ground actions from transcript

- Question the intent behind messages (implied)
- Seek understanding from multiple perspectives (implied)

### Whats missing in summary

The full transcript provides a detailed exploration of the potential intents behind certain text messages and encourages critical thinking about the motivations of those involved.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk a little bit about those text
messages.
Because to be honest, I've been waiting for somebody
to ask the most important question.
And I haven't seen anybody do it yet.
There's a whole lot of commentary going on,
but nobody is asking the question that matters.
Intent.
What was the intent behind those messages?
I have watched these personalities try to spin this in every way possible.
They've tried to downplay it, told us to deny what we've seen with our own eyes, try to
play the victim, mask it with fake anger, all kinds of stuff.
But none of that addresses intent.
Why were the text messages sent to begin with?
That's the question people need to be asking, especially if you are a supporter of former
President Trump.
You need to be asking that question.
Why were those messages sent?
What was their intent?
you really only have two options and this isn't a false dichotomy. This is one
of those things where you're pregnant or you're not. Okay? Either the people who
sent those messages believed that the President of the United States at the
time was enjoying what he was seeing, that he was cheering on the coup,
insurrection, riot, tourist visit, whatever you want to call it. He was cheering it
and therefore they needed to send those messages, it would make sense to send
them in that case if they believed that he was enjoying what he was seeing, that
he was supportive of it. Or they thought he didn't enjoy it, in which case the
only reason those messages would have been sent is if they believed that, well,
Well, not to put too fine a point on it, that he was completely incompetent and could not
do his job.
He was ineffective.
He was too weak and too ill-informed, too bad at what he does to call off his own dogs.
That's the question that needs to be asked.
Why were those messages sent to begin with?
They can weasel out of just about anything else, but not that, because it's one thing
or the other.
Either they believed the president liked what he saw, so they needed to send those messages
because he didn't understand the long-term effects and the impacts it would have on his
legacy.
That actually sounds like some of the messages, doesn't it?
Or they believed that he didn't know any better.
That he didn't like it, but didn't know what to do.
And he needed them to hold his hand so he could say, stop doing that.
The rest of this stuff, when it comes to, well, you know, they were being hypocritical.
Of course they were, it's Fox.
That's not a surprise.
That's the question that matters, the intent.
Why were those messages sent?
By multiple people.
People who were pretty close to him.
Why did they feel they needed to say that?
That's what matters.
That's what's going to give you insight into his state of mind at that time.
And if you are somebody who still supports the former president, I'm dying to know which
one you hope it is.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}