---
title: Let's talk about how to pick a charity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ve4T1BoiNgU) |
| Published | 2021/12/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau provides a guide on how to pick and vet charities, sharing his personal approach.
- He advises starting by deciding on a cause that is close to your heart.
- Beau recommends making a list of charities that work towards that cause and evaluating them based on your criteria.
- He prefers organizations that are "boots on the ground" and those that act as clearinghouses for fundraising.
- Beau underscores the importance of getting personally involved rather than just cutting a check.
- Effectiveness and impact evaluation are key factors in selecting a charity to support.
- Beau suggests talking to both clients and employees of the charity to gauge their effectiveness.
- When assessing finances, he advises looking at overhead costs as a percentage and considering executive salaries in context.
- Beau advocates for supporting individuals within charities who are effective and committed to the cause.
- Following effective individuals across different organizations can lead to further impactful involvement and support.

### Quotes

- "There is no wrong way to help, all right?"
- "Getting involved on a personal level, it's probably more effective."
- "It's the war on poverty, the war on hunger, the war on homelessness. Wars cost money."

### Oneliner

Beau provides a comprehensive guide on selecting and vetting charities, stressing personal involvement for effectiveness and impact.

### Audience

Donors, Volunteers, Supporters

### On-the-ground actions from transcript

- Contact charities to inquire about their operations and impact (suggested)
- Talk to clients and employees of charities to gauge effectiveness (exemplified)
- Follow and support effective individuals within charities on social media (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the importance of personal involvement, impact assessment, and supporting effective individuals within charities, enhancing one's philanthropic efforts.

### Tags

#Charity #Donations #Impact #CommunitySupport #PersonalInvolvement


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about how to pick a charity,
how to vet a charity.
Had a lot of questions about it over the last few weeks,
and somebody recently sent one there in Nashville.
They're looking to get involved.
I'm gonna go over how I do it.
Now, understand there are tons of guides on the internet
explaining different ways of doing it.
If you don't like mine, there's another one.
And keep that in mind throughout this entire thing.
There is no wrong way to help, all right?
So this is how I do it.
First thing, decide on the cause.
You know, people say they wanna help,
and they have a whole bunch of causes
that they may be interested in.
Pick one.
Pick one cause that is very close to your heart.
And it's important that you truly believe in this cause.
All right?
So you pick that cause,
and you look at all of the charities,
make a list of all the charities
that work to better that situation, whatever it is.
Then go through and evaluate them by your criteria
in how they operate and how they seek to go about it.
As an example, I like two kinds of organizations.
I like boots on the ground,
hands on trying to mitigate the problem,
whatever it is, right there,
and make a material difference in people's lives
at the time it's happening.
I like those kind of organizations.
And I like the clearinghouse organizations,
the organizations that really just kind of fundraise
and then give that money to the community
and give that money to the organizations
that are boots on the ground.
That's what I like, okay?
You may not like it that way.
You may want to fund research.
You may want to fund lawyers who are going to lobby.
You may want to get involved in electoral politics.
There's a whole bunch of different ways.
None of these are really wrong.
It's just a different way of looking at it.
Find the one that supports the cause
that is close to your heart
that also does it the way you would want it done.
The reason this is important is because
there's a difference between cutting a check
and getting involved.
Now, don't get me wrong.
Every charity out there needs people
who are just going to cut the check, okay?
Again, there's no wrong way to do this.
But getting involved on a personal level,
it's probably more effective,
and it definitely leads to you staying with that cause
and being more involved for a longer period of time,
therefore doing more good, okay?
Now, once you pick your cause
and evaluate it by those criteria,
your list has shortened, okay?
You probably don't have a giant list anymore.
You only have a few.
So now look at how effective they are.
Look at how effective they are.
Look at the impacts of their programs,
whether or not they're successful.
If it is something that has a defined goal,
like an end game,
see how much progress they've made towards it.
Most of the causes I support are pretty open-ended.
There is no end game to domestic violence.
It's gonna be ongoing for a while.
So there's no way to measure it in that progress sense,
but you can measure the impacts and how effective they are.
The easiest way that I have found to do this
is to talk to people.
Talk to either clients,
people that have been personally involved
on the receiving end of these programs,
of this charity, whatever.
Talk to them, see how effective they are.
And then talk to employees,
people who actually work at that charity.
Now that sounds like it might be hard, but it's not.
Believe me, if you call pretty much any charity and say,
hey, I'm thinking about giving you money.
I'd like to know a little bit about what you do.
Believe me, they'll tell you.
They'll probably invite you out for a tour.
Okay, so once you have decided the cause,
the criteria you want,
and you have found a couple that create the positive impact
you'd like to see, run things the way you want them,
all of this is very subjective, and that's okay.
Once you finish that, then you look at the finances.
Then you look at the finances.
And you look at the finances last because,
well, we'll get there.
When you're looking at them,
sometimes you will see very, very large numbers,
depending on the organization.
When you're looking at the overhead,
always look at it as a percentage.
You know, if an organization has $3 million in overhead
and brings in $5 million a year,
yeah, that's probably an issue.
However, if it has $3 million in overhead
and brings in $50 million a year,
that's not so much of an issue.
Always look at it as a percentage.
Another hot topic, something that gets debated a lot,
is executive salaries.
The salaries of the top people often get disclosed,
and sometimes they're pretty high.
Again, try to put it into perspective.
If you're talking about a small local charity
that is boots on the ground,
yeah, you probably don't expect the CEO
to be making $600,000 a year or something.
However, if you are talking about a nationwide charity
that requires lots of logistics and tons of moving parts
and different programs and stuff like that,
that may be normal.
Compare it to what you would see
if that job was not a nonprofit.
Compare it to what you would see
if it was running a retail chain,
if that's what they were the CEO of.
The reason you want to do this,
because it is kind of counterintuitive,
you know, it's a charity, right?
You want the cause that you support
that's close to your heart.
You want them to have the best people possible.
Sometimes the best people possible for a job,
well, they cost a lot.
They cost a lot of money.
You don't want them limited based on that.
When you're talking about logistics,
somebody who is good at logistics
doesn't necessarily even have to care about the cause
to be great at it.
It certainly helps if they do care, though.
So keep that in focus.
When you get up to those higher levels,
those numbers can get high.
Don't get me wrong.
There's a lot of room for criticism there.
But it's important to remember
that when we talk about stuff like this,
it's the war on poverty, the war on hunger,
the war on homelessness.
Wars cost money.
They take resources.
And to my way of thinking,
I don't always get upset when I see a huge salary
because if it is a large organization
that has a lot of different programs,
a lot of moving parts,
and it's covering a massive area of operations,
I want them to have somebody who is capable of handling it
and who isn't going to be stolen away by the private sector.
Okay, so one of the other things that you can do
is when you're looking at these programs,
when you're looking at the different nonprofits,
the different charities,
and you start to get involved
and you notice somebody who is really good at what they do,
remember them.
Follow them on social media.
Them as an individual, not them as that position
within that charity
because a lot of them move around
to different organizations.
The DV shelter that we do the fundraisers
for here on this channel,
which we'll be doing soon, by the way,
that shelter, I met a woman working there
who I just now realized I didn't ask her
if I could use her information,
so we're going to be kind of vague.
We're going to call her Michelle.
I met Michelle.
She was working there.
She was amazing, super effective.
She made those fundraising events,
things that everybody wanted to attend,
so it brought in a lot of money, right,
which allowed for a lot of programs
that allowed for them to be hands on,
boots on the ground, getting stuff done,
making positive material impacts in people's lives.
Exactly what I wanted, right?
Okay, so eventually she leaves the shelter.
She goes to work for somewhere else, somewhere up north,
and then she comes back down to Central Florida,
and she goes to work for another shelter
called Harbor House, okay,
and I know absolutely nothing
about Harbor House's operations, nothing,
but because she was so effective,
she was so good at what she did,
she believed in that cause so much,
I would feel very comfortable
supporting one of their initiatives.
Definitely take notice of the people in these charities
that are good at what they do.
It will help you expand the ways you can help
and the ways you can get involved,
and if you choose an organization
that does things the way you want them done
and it's serving a cause that is close to your heart,
you're gonna stay involved longer.
You're gonna do more good.
So that's how I would go about it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}