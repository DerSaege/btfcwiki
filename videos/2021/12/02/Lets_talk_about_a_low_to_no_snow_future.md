---
title: Let's talk about a low to no snow future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VXuyjuGPGqw) |
| Published | 2021/12/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Researchers in the Western United States have observed a decline in snowpack on mountain tops due to global warming, leading to less water storage.
- The snowpack acts as a natural reservoir, providing water for agricultural uses during dry periods.
- With declining snowpack, there won't be enough water to meet demand, requiring innovative water storage solutions within the next 19 years.
- The study predicts episodic low to no snow in the 2040s, necessitating urgent action.
- The decrease in snowpack will have cascading effects on rivers, groundwater, and wildfires.
- Collaboration among scientists, engineers, and planners is vital to address the impacts of climate change and prepare for the future.
- Mitigating climate change effects now and transitioning away from fossil fuels are critical steps.
- Immediate action is necessary to mitigate the severe consequences that will be more evident in the future.
- Cooperation across various fields is needed to proactively address the challenges posed by the declining snowpack.
- The time to act and prepare for these changes is now.

### Quotes

- "The impacts of climate change are hitting us now."
- "We need an unprecedented amount of cooperation and collaboration across all fields of endeavor so we can get ready."
- "It's just a thought."

### Oneliner

Researchers warn of declining snowpack in the Western United States due to global warming, necessitating urgent collaboration and preparation across fields to address water scarcity and wildfire risks.

### Audience

Climate activists, scientists, policymakers

### On-the-ground actions from transcript

- Collaborate with scientists, engineers, and planners to develop innovative water storage solutions (suggested)
- Advocate for transitioning away from fossil fuels to combat climate change (suggested)

### Whats missing in summary

The full transcript provides a detailed insight into the implications of declining snowpack on water supply and the environment, stressing the urgency of collaborative action to mitigate climate change effects and prepare for the future.

### Tags

#ClimateChange #WaterScarcity #Collaboration #Mitigation #Wildfires


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about a low to no snow future
and what that means, and in particular, what
that means for California and the Western United States.
Because some researchers out west, well,
they released a study.
And what they did was they went back through history,
and they looked at the peak snowpack.
This is the snowpack up on the top of mountains.
The peak normally occurs around April 1.
And unsurprisingly, the trend is that, well, there's
less snowpack.
I mean, it's not surprising, because the world
is getting warmer.
So even if the same amount of precipitation
fell up on the top of those mountains, well, some of it
would fall as water rather than snow.
So it wouldn't join the snowpack.
OK, so why is this important?
If you don't know, that snowpack acts as a natural reservoir,
keeping all of that water, all that fresh water,
up on the top of those mountains.
And then when spring and summer comes, it starts to melt,
and it flows down just in time for agricultural uses
and in a period when typically there's not a lot of rainfall.
It's perfect, except it's declining.
And it's declining at a rate that is concerning,
to say the least.
The researchers came up with some definitions.
They defined low snow as being in the 30th percentile
or less of the historic peak snowpack.
OK?
They defined episodic low to no snow
as a consecutive five-year period
when half of a mountain basin is below the 30th percentile.
Persistent is when that happens for more than 10 years.
These definitions are important, and you
need to know them because they are expecting episodic low
to no snow in the 2040s, right around the corner,
19 years away.
OK?
So what this means is that there's
not going to be enough water up in those snowpacks
to satisfy demand.
Now, in essence, it means that, well,
they have to find a way to store every single drop that falls,
whether it falls as water or whether it falls as snow.
They have to figure something out,
and they have 19 years to do it.
Aside from this, it causes a cascade effect,
where one thing affects another, which affects another,
affects another.
And it is a certainty that a reduction in snowpack
will impact rivers, groundwater, all kinds of stuff that all
have to do with the water supply.
It's also going to alter the way wildfires occur.
So what does this mean?
It means that, particularly in the Western United States,
but probably all over the world, there
needs to be an unprecedented amount of collaboration
between scientists, engineers, and planners.
We have to get ready for this.
Otherwise, it's going to occur in a way
that, once it starts, we're not going to have time to catch up.
The impacts of climate change are hitting us now.
So we have to start to mitigate now.
It would probably be a really smart idea
to also start doing things to lessen
the effects of climate change in the future, which
would include transitioning away from fossil fuels.
That is an ongoing battle.
In the meantime, we also have to address
the effects that are coming.
We know they're coming.
They're already here.
They're just not so severe yet that people are really
noticing them the way they should
or the way they will in 20 years.
So we need that unprecedented amount
of cooperation and collaboration across all fields of endeavor
so we can get ready.
And it needs to start now.
That's the takeaway from this study.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}