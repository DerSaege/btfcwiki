---
title: Let's talk about a lighthearted Q and A....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9hHN-m5Lj-w) |
| Published | 2021/12/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau engages in a lighthearted Twitter Q&A session for Christmas, answering a variety of questions from his followers without prior knowledge of the queries.
- Questions range from serious advice to playful banter, covering topics such as favorite board games, beard care routines, favorite Christmas traditions, and even hypothetical scenarios like meeting fictional characters.
- Beau shares personal anecdotes, hints at future content for his channel, and provides glimpses into his family life and hobbies.
- The transcript captures Beau's casual and friendly demeanor as he navigates through the amusing and thought-provoking questions from his audience.
- Beau's responses showcase his sense of humor, easygoing nature, and willingness to share glimpses of his life with his viewers.

### Quotes

- "I guess two because my wife gives everybody pajamas because she wants us all to look the same."
- "I honestly never cared, to me they're fun, you know."
- "Some people might say this is a very serious question. What is your favorite regional style of barbecue?"
- "It's not looking good."

### Oneliner

Beau engages in a light-hearted Q&A session covering topics from favorite board games to barbecue preferences, showcasing his casual and friendly demeanor.

### Audience

Content creators and fans

### On-the-ground actions from transcript

- Find ways to incorporate humor and lightheartedness into your interactions with your audience (implied).
- Share personal anecdotes and stories to connect with your viewers on a deeper level (implied).
- Be open to answering a diverse range of questions from your audience to foster engagement and connection (implied).

### Whats missing in summary

Insights into Beau's personality and approach to engaging with his audience through playful Q&A sessions.

### Tags

#Q&A #CommunityEngagement #Humor #ContentCreation #FamilyLife


## Transcript
Well, howdy there, internet people.
It's Beau again.
Merry Christmas.
So, this is a question and answer session via Twitter.
A whole bunch of questions were asked.
I have not looked at them yet, so I don't have answers.
And we're just gonna kind of roll through them.
This is something for you to listen to
hang out to on Christmas if you are bored.
Okay, so I put out on Twitter, hey I want to record something light hearted and fun
to release on Christmas.
Doing one of those Q&A videos, if you have any non-serious questions, drop them below.
Also I won't be reading anything until I film.
There are 300 and something questions, I'm probably not going to do them all.
All right.
Do you know the contents of my survival git?
Hint, it's a winter theme.
Elsa Band-Aids.
If it's not that, I will just let it go.
What's the likelihood that you're going to try and grow
a pink pineapple?
Pretty high.
Do you currently know where your tremors are?
I do.
I'm assuming that's telling me to shave my beard.
What's the worst advice you have ever received
from your spouse?
And what's the best advice you have ever received
from your kids?
I don't know that I have an answer to that.
My wife's a pretty smart lady.
She normally doesn't give bad advice.
Um, yeah, I'm gonna pass on that one.
How are you doing, though?
I like to ask everyone during the holidays because it can be hard.
I hope you and your family are well and enjoying yourselves.
We are.
Everything's doing great, thank you.
Okay, favorite board game?
Risk.
Dessert.
First or after a meal.
Yes, both.
Whenever.
Christmas carol or song? I don't know that I have a favorite Christmas carol or song. I'm not sure.
Plans for channel in the future? This channel is going to remain more or less the same. There's
no planned changes to this one. The Bow on the Road channel, the Roads with Bow is the name of it.
But what with the whole constant public health thing,
that's kind of shifting at the moment.
And if things aren't fixed by the spring,
we're going to show people how to grow stuff.
And you're going to get a lot of homesteading tutorials
along with interviews and stuff like that.
Any plans to stream on Twitch?
Not yet.
I mean, it's something that people have talked about.
We're kind of looking into it, but it doesn't really seem to be my format
That's more for hot takes and I I like lukewarm takes
Movie-preference action-comedy Western horror or something else historical drama favorite non-serious YouTube channel
Probably Bailey Sarian I
I don't know if I would say that's not serious, but the way she presents it, it makes it seem like it's not really that
serious.
What do you think about the Snoopy vs. the Red Baron Christmas song?
I've always kind of wondered if that was a reference to the Christmas truce during World War I.
If you don't know what I'm talking about, there's some history.
Something to look up today.
Can you recommend a Christmas cocktail?
Try Holiday Jack or Winter Jack is what it's called.
Which of Santa's reindeer is favorite?
Is your favorite?
I don't know that I have one.
Also what colored pants are you wearing today?
Bold of you to assume that I'm wearing their blue jeans.
or Marianne? Marianne, do you have a favorite child? And will the answer get you removed
from this world? All of my children are pretty different, so they're... yeah, that's a standard
parent answer. On a slightly less controversial note, who is your favorite YouTuber? Either
to watch or to collaborate with.
You know, I just said Bailey, so I don't know if I have favorites.
See, it's those extremes.
I don't know that I have a favorite or something is best or worst.
I think that's what always throws me off.
So during the last year, everyone around here should be informed what the cue is, but I'm
I'm not familiar with the A conspiracy.
Non-serious question.
Got it, Q&A. Got it.
OK.
Christmas question.
What is your favorite type of jerky?
Beef?
And do you have any good recipes?
Not for jerky.
I really don't.
Real question, don't read.
Well, I guess I'll skip that.
Do your kids have any chores or normal tasks
that they dislike and what do you do or not to work through it.
All of my children have an issue with taking their plate from the table and moving it to
the sink or the dishwasher or whatever.
I don't know what that's about.
What do I do or not do?
It depends.
I kind of have two parenting modes.
like Andy Griffith or Saul. So it's either we have the conversation explaining
why this is important and so on and so forth or I wait for them to get
involved in whatever it is they're gonna do next and call them back and have them
do it then. What's your favorite dad joke? I don't know any jokes about dads. Be
disappointed if you don't have one. Yeah, I have a lot of dad jokes but they come
up in the moment. I don't know. Star Trek or Star Wars? Star Trek. Do you like
watching the MCU and if so what's your take on the Hawkeye series so far?
I understand why people like the MCU. I get it. It's not so much for me. I don't know.
I think I may be past the point where I find it super interesting. It's not like it's
It's unbearable or I dislike it.
It's just not my thing.
Trey Crowder or Cory or Forrester for your running mate, they can run, I'll be like Secretary
of State or something, Ford, Chevy, GMC, or Ram trucks and why?
Redneck has a very, very stiff opinion on this, something that they're just completely
inflexible on, Ford, Chevy, GMC, or Ram, yeah, any Jeep with a truck bed or a Nissan Frontier.
Where I live, I need a truck that has a relatively high clearance.
It has to be a four-wheel drive vehicle, meaning not that it's a farm truck that they put four-wheel
drive on.
It has to actually be designed to go off-road.
It also needs to be relatively narrow, and it needs to be able to tow stuff.
That limits my options a lot.
with truck beds or a Nissan Frontier.
Best Matthew Broderick movie, War Games.
Why do you grow so many pumpkins?
Because I like Halloween.
My wife and I love Halloween.
It's always a big thing for us.
It's a little annoying that we haven't been able to have giant Halloween parties and
stuff because of everything going on.
What is your favorite nail polish color to wear?
Well, I've only worn the one, and I'm just going to call it pretty woman red, is a way
to describe it.
It's a very bright red, let's see, matched or odd socks, matched.
What is your guilty pleasure? I don't get guilty about pleasure.
Whatever happened to the animals that came to Casa de Bo have any others shown up?
Let's see. So the pig, the random pig turned out to be a neighbor's.
The German Shepherd still comes by and says hi every once in a while.
Random German Shepherd. He's cool. I like him.
him. The horse lives across the street, has not come back for a visit, at least
not that I know of. New animals that have shown up. We have a bobcat, a small
Florida bobcat on the very back side of the property, and other than that I think
I think that's it, I think that's the only new addition, something that's seen.
Do you intentionally grow your beard out or are you just like me and absolutely despise
shaving?
I like having a beard and then there's also the fact that I'm one of those people that
I get five o'clock shadow at like noon.
It doesn't do me a whole lot of good to shave.
It's a constant process.
In fact, if you go back and look, you'll find some with me with just a goatee because I
shaved the one time on camera.
And if you were to just scroll by, I don't know how long it took, maybe three weeks and
I looked like this again.
a constant process and I find it unnecessary. I don't look clean cut no matter what I do.
So there's no reason to go out of my way for that. What is one thing that you miss during
the pandemic that you can't wait to get back to doing? Going on the road, going out and
traveling it's I'm really finally starting to be like you know what we
really need to get this over with quickly. It's starting to grate a lot favorite
taco all of them does everybody know that Carhartt has a great veterans
discount all the time they do now I did not know that spill it what's your
recipe for shepherd's pie and is there a vegetarian version my shepherd's pie is
just crumbles grilled on the bottom with a layer of mashed potatoes cooked on
top what is yours okay so take a a bread loaf pan and you're gonna cook all the
ingredients separately and you take whatever you would normally use right
and get that done you also want some stovetop stuffing line the bottom of
pan with stovetop and then put your mashed potatoes and other ingredients, whatever you
want to put into it.
You can put in vegetables if you want, I do at times.
And I don't layer it, it's all mixed together.
Then I seal the top of it with the stuffing.
I also put a layer of cheese over the top.
And then I put it in the oven.
The stovetop stuffing gets kind of crispy, but it traps all the moisture inside.
That's the secret, that's my secret anyway.
The eternal Christmas question, is Die Hard a Christmas movie?
Sure.
I'm giving my nieces and nephews Christmas gifts, should I give them guns or books?
Books?
I would assume that that would probably be the safest bet.
What's your estimation of the combat capability of Santa and or his reindeer?
Not high.
When you're talking about noise and light discipline, litter discipline, he leaves stuff
everywhere he goes.
That nose on Rudolph giving away his position, sleigh bells ringing, I don't think it's
high.
where did you and Miss Bo meet? Yeah, okay, this is a funny story. So, my wife came into
a portrait studio. I was the photographer. Her mom made the appointment for her, made
her come do it. And, you know, while she was there, take note, guys, because there's some
important things here. You know I flirted a little bit but because flirting is
supposed to be subtle, you know, she didn't really pick up on it. Her mom had
to tell her that that had occurred, which I found funny. Her mom also went out of
her way to tell me that she was single. So we had a brief conversation then
And then, you know, when the portraits were ready, I called to have her come pick up the photos.
And she picks up the phone, you know, the phone gets answered.
And I am talking to her in that, you know, I'm sorry, did I leave my boots under your bed voice?
And, yeah, she's just talking back and then she bursts out laughing because her mom made the appointment and I'd been
talking to her mom the whole time.  So her mom then gave me her phone number to call and when I did she answered,
I'm on quarters. She's sick. So, it's like a week before she comes back by and when she comes in,
you know, I didn't frame up any portraits or anything because I wanted to have a moment to
talk to her. Again, if you want to talk to somebody, create a situation where you can talk to them and
it's not high pressure and they don't feel uncomfortable. More notes. So as I start to
frame them up, she's like, you've had these for a week and you couldn't have this done already?
Wow, this is not going well.
And I was like, well, you know, I thought, you know, I wanted to have a moment to talk
to you.
And she's like, yeah, how's that working out for you?
Fair enough.
So yeah, that I kind of stopped talking at that point.
And as she was walking away, she kind of did that little look over her shoulder thing,
have my number and that's kind of what started it all off. How often you could
see snow where you live? Never. Never here. What's your family's favorite thing
to do in the snow? Play with the dogs. When we are somewhere and it is snowing
the dogs love the snow. Well Destro loves the snow. Baroness likes to look at the
snow you can abolish one phrase from the modern lexicon what do you pick the
term reelect reelect anybody none of them do you ever build anything with the
materials in that shed Merry Christmas so you know I've said this on the
channel before but I think people forget. I don't know maybe four months ago we
built a exact replica of the shop and that's what's behind me. My real shop
is is noisy and I can only film at certain times so we built a replica and
and move the stuff in here.
So this stuff moves a whole lot less.
People always ask about the charcoal back there.
That is reserve for after hurricanes.
So we have something to cook with when the power's down,
something like that.
But in the real shop, yes, we build all sorts of stuff,
like including this.
We're on that now.
So, we do.
What's one song that you can't help but sing along to
no matter the company you're in?
I don't know, horrible 80s music.
I don't know if it's one song.
I sing a lot.
I sing in the car, too.
I gotta ask, did you build your shelves?
We built these.
The ones in the actual shop were here when we got here.
So you're looking for plans.
We just copied that format.
I mean, I can take a photo of it if you want, put it on Twitter or Instagram.
Let's see.
Stuyart Christmas movie.
I think I already answered that, yes.
What is your favorite Christmas tradition with your family and where did it come from?
I have no idea why, but my wife's family, they put peanuts in the stockings.
That's kind of one of my new favorites just because the kids love it.
What's your favorite cartoon to watch with your kids?
Let's see, Phineas and Ferb, Storybots, Dino Trucks, Miraculous, those.
Let's see, what's your top three Christmas movies and why are they Elf Christmas Vacation
and Die Hard?
Yeah, you know, not so much a fan of Elf, to be honest.
That kind of humor doesn't really do it for me.
My wife loves it, I'm not a fan.
do you believe in rock and roll? Can music save your moral soul? And can you teach me
how to dance real slow? Okay, yeah, that's that Madonna song, right? Have you hiked,
camped in a cold environment? Yes. If so, what's the coldest outdoor style of place
you've been? Oddly enough, the lowest temperature that I have ever camped in was in
Ohio it wasn't anywhere we heard like it was in Ohio let's see what's the most
shocking to you question you ever received from one of your kids my oldest
son when he was I don't know seven somebody he had just learned the
general concept of where children came from and we are in the small town where
I grew up and we run into somebody I dated in high school and with her
present and her husband present he looks at me and asks how well we know each
other that was that was shocking that was shocking to me were you aware that
randolph the blue nose reindeer not Rudolph was the original shiny
nose reindeer I did not know that I'm gonna have to look that up who's your
favorite comedian? I don't know, Ali Siddique or Drew Morgan. That's polar opposites. But yeah,
I find both of them very relatable in very different ways. What's your favorite movie
genre? Yeah, historical dramas. What's your beard's name? Don't have a beard. Also, the birthday cake
video you did with your wife she's awesome is one of my favorites I'd love
to see you do a Q&A with her we've actually talked about that oddly enough
if we do it you'll probably see it before this video because we're
recording I'm recording this in advance but that that may be coming at some
point in the future.
Are your t-shirts sorted by color or topic or both?
I imagine them having department store dividers labeling long racks of t-shirts in your closet.
Yeah, so we have a lot of t-shirt racks.
They're not very well organized in any fashion at all, to be honest.
I have a good memory, so I remember roughly where they're at and just find them.
Nothing to add to what's here make me laugh, okay?
What TV shows are must watch or have been must watch for you over the last several years?
Let's see, Lucifer, Yellowstone, You, The Mayans.
Those are the ones, the last few that I have binged on.
Where have you always wanted to travel but for whatever reason have not been able to?
I've always wanted to go to Giza.
Just haven't gone.
What's your favorite Christmas horror movie?
You know, I just recently watched one that I thought was great.
I can't remember the name of it to be honest.
So Santa Claus is up at the North Pole, and there's like a zombie outbreak among the
elves.
Meanwhile, Krampus is... that whole thing's going on as well.
It's fantastic, but I can't remember the name of it.
I'll look it up and try to get it in a comment down below.
Are you shaping your beard into two separate forks, or is it just growing naturally?
That's natural.
That's normally the sign that I need to trim it.
Mayo or Miracle Whip?
Neither really, but mayo.
I have noticed your beard has two extra long tendrils at the bottom.
Beading or braiding?
I have not considered that.
I don't know.
Like a tour to meet the critters, real or imagined, that inhabit your beard?
What are the benefits and challenges of raising kids in the South?
In Florida specifically.
Well, in Florida, you're always going to have a problem of, well, they have a place to go
if they skip school and go to the beach.
I don't know.
It's more of a slower paced culture overall in the South.
move at a slower pace.
I think that in many ways it does help with education in the sense that there's more processing
time for everything.
Because of the wide variety of people you have in the South, people can if they choose
to have an easier time applying things practically that they learned in school.
Like anything else, when it comes to education, it depends on what you put into it.
Do you drink on Christmas sometimes?
If so, what's your go-to?
Winter Jack.
If you don't like DeSantis and how Florida is run, why in the world do you still live
there?
Florida is more or less a county run state.
Generally Florida man does not acknowledge that the governor exists.
The general idea is that the governor is far and the rivers are many.
What he doesn't know doesn't hurt him type of thing.
So up until very, very recently, nothing that he did really impacted me personally where
I live.
If you had to choose one hat to save in a burning house scenario, which one would you
choose and explain why?
I guess this one.
I don't know.
laundry detergent to use on your t-shirts. So if I buy it, it's Arm and Hammer
Scentless. My wife gets gain, I think. Pretty sure.
Nosey question. You always say you have a lot of kids. How many do you actually
I actually have, I was a teen in the 90s, I have not now.
There are four in the house.
There are two additional that are, there are kids, but in a more unofficial sense.
So 6 to 4 depending on how you count.
Let's see.
What was your best good surprise this last year?
This whole last year has been great for me to be honest.
I really can't complain about much.
I want a video montage of all your fantastic shirts.
versus G.I. Joe? G.I. Joe, of course. What's the meaning behind wearing the Curious George
hat? I actually have a video on this. It's called Let's Talk About Curious George. Short
version is definitely watch the video if you don't know the story of Curious George because
I didn't. If you just look at that storyline, you could draw some meaning that isn't really
The story of Curious George is far more interesting than I think a lot of people know.
Definitely worth watching.
What is your beard care routine?
Wax or oil?
Wood comb?
Make a difference?
I actually do use a wood comb.
That I do do.
But I don't use wax or oil.
use Dove soap. So if I needed you to check out a hot rod in Florida would you
be willing to do it? It depends on where it in Florida, maybe. A personal question
if you don't mind, why the beard? I've always had facial hair pretty much if I
was able to. How did you meet your lovely wife? Bacon or sausage? Sausage. Can you do
you like sewing? I can sew, I don't particularly like it. Take us around for a spin around
your property. That's probably going to happen over on the other channel come spring because
we're going to be putting in stuff to kind of show people that it can be done.
We're going to build a, basically build a balcony facing each direction because people
have talked about having trouble growing stuff on their balconies.
So we're going to build balconies on a fake building and grow stuff on it and demonstrate
how it can be done.
Cake yes or no no gingerbread crisp or soft soft presents eve or morning both
one in the evening well i guess two because my wife gives everybody pajamas because she wants
us all to look the same um and uh then the rest of the morning best concert you've ever been to
Steve Earle in Pensacola, Florida. Most regrettable fashion choice. I grew up in
the 80s and 90s. Everything. Everything. Favorite fictional character. Sherlock
Holmes. Let's see. What's your favorite game? Video and board, card, video game?
I mean, probably Civilization, Board Game Risk, there's a theme there.
Let's see.
What is your favorite MCU movie?
I don't know if that...
There's a Spider-Man cartoon that I think is actually part of the MCU.
I don't know.
Let's see.
Do people decorate their tree to look the same every year?
I don't think so.
I think most people use the same ornaments,
but I don't know anybody that really
tries to make it look the same way they did the year before.
Let's see.
What's your favorite guilty pleasure food? Oreos! What's on your shelves back there?
Um, let's see. So we have the mini museum, the elephant, a piece of art I was sent by
one of y'all, the YouTube plaque, I think it's a mortar case, an axe, it's the stuff
that was directly behind the camera in the shop and it just got moved in here.
So it's kind of a mix.
What's your favorite kind of pie?
Shepherd's pie, apple pie, no pumpkin pie.
I like Al-Pi.
Let's see, favorite Lego set theme.
I liked the pirate ones growing up, but I like building the Jurassic Park ones with
my kids.
Pancakes or waffles?
Pancakes.
Do Floridians like eating alligators?
I don't.
So it's a delicacy, I'm not really that impressed by it to be completely honest.
If it is prepared perfectly, it can be good, but most times it's not.
It's one of those things that people down here eat on rare occasions, but it's not like
Like a normal meal.
What's a song you love but kind of hate yourself for loving it?
I don't know.
See I don't generally like, I don't beat myself up for liking something that isn't
That's cool.
And I have a very eclectic taste when it comes to music.
Whatever gene it is that causes people to be ashamed of that, I don't have that one.
What's your favorite digit of Pi, too?
your favorite Pink Floyd Christmas song? Comfortably, no. Do you consider candy
corn to be edible? Of course. Anything's edible if you're brave enough. Now, I
actually like candy corn, kind of. Who's your favorite superhero or person from
myth, Prometheus. Not sure if this is light-hearted, but any advice for someone
with ADHD wanting to help in the community scared of not being able to
commit? Oh no, that's fine. Oh yeah, I have advice for that. That's cool. So there,
if you find groups that are active, that get involved in a lot of
of different stuff. Just tell them you want to show up and help the day of.
There's always stuff that needs to be done the day of an event and for you it
will always be different. It will always be something new and you don't have to
bog yourself down in in the the politics of all of it. You just show up help and
leaf. Do you use beard oil? Lots of questions about my beard hair. Let's see. Where did
the idea come from to have the shirts match the message? I've always been a fan of Easter
eggs like that and it again it started as like many things on this channel a
joke just it was something that was supposed to be a one-off and then it
wasn't so let's see what made you start YouTube
Um, Pseudo Joe, since y'all are on Twitter, or Pseudo Bear, um, him and my wife were both
like why do you do this on this, you just need to go ahead and put him on YouTube.
And it was, it was all a joke when it started.
And here we are.
It's the best present you've received that you didn't realize you wanted or needed.
My wife got me the most ridiculous thing that you can imagine.
It's nylon with a magnet inside of it and you wear it on your wrist.
If you do any kind of building at all, get one.
When you're on a ladder, it's cool because the screws, they're right there on your wrist.
You don't have to hold them in your mouth anymore.
I mean, not that you should do that, but it's one of those things that you would never buy
for yourself, but I recently lost mine and I need to go find another one.
It was one of those things that...
And when I first got it, I was like, wow, that's great.
And then I used it and I was like, man, this is awesome.
So I would highly suggest getting one of those.
Um, let's see.
Grits and how?
Yes, of course grits.
I like cheese grits.
What's your Waffle House order?
I mean, that's incredibly stereotypical.
You know, you're sitting here just turning me into a giant cartoon of the South, assuming
that I have a standard order at Waffle House.
Triple scrambled cheese plate covered in chunked.
Stick or manual for daily driver.
I have an automatic.
Wood chip or straw.
Which SEC team?
I really don't watch sports.
I really don't.
Favorite Western?
Unforgiven or Maverick?
Let's see.
Why haven't you read The Count of Monte Cristo?
I have read The Count of Monte Cristo.
If you could share your home, be buds with any animal and it was magically guaranteed
to neither pose a threat to you or your family, nor negatively impact the animal in any way,
which animal would it be?
Either an elephant or a wolf.
I don't know.
I'd have to put some serious thought to that.
Why is Santa giving to the needy called Christmas, but when we want the system to do it, it's
called socialism because they haven't figured out what Santa is.
You know, where he lives in that utopia place where all the workers work according to their
ability and everybody gets what they need and they have access and they share it with
everybody else and he's even literally just red.
Is Florida Man real or is he just another urban legend that Sam is using to keep people
away from Florida?
So Florida man is real in the sense that people in Florida are a different breed.
It's also worth noting that y'all get to hear about it because we have sunshine laws.
When it comes to our incidents, we don't try to keep them quiet.
In fact, it's against the law to try to not talk about it.
So y'all get to hear all of the weird stories like, you know, some guy throwing an alligator
through a drive-through window to hold the store up or something like that.
And yeah, that stuff happens here.
So it's, it's a real thing, but it's also, there's probably weird stuff like this that
happens everywhere.
It's just all of our business gets put out all over the media.
So how is Rory the hummingbird?
I would imagine that's the hummingbird that's off the back porch.
I did not know that the hummingbird had a name, or if anybody told me, I forgot.
I haven't seen him or her, I don't know, I respect its privacy, in a while.
Your demeanor is understandably very serious, but sometimes your sense of humor comes through.
What are your favorite comedy shows and or comedians when you want to chuggle?
I already answered that.
Ali Sadiq, Drew Morgan.
The secret to humor is surprise.
So I like anything that subverts my expectations.
You seem like a pretty outdoorsy kind of guy.
Do you have any geeky, guilty pleasure like Star Trek, Star Wars, Lord of the Rings, or
anything like that. I do watch Star Trek. Not sure about the others, but I feel
like I've heard him talk Star Trek before. Please confirm whether or not
you're a Trekkie. I'm not a Trekkie, I don't think, but I do watch Star Trek. I
like the the messages in it. Let's see. Will you dye your beard green and
decorate it like a Christmas tree.
You know, I might have done that had I not seen this now,
way too late to do it.
What are your go-to comfort shows or movies?
The Gilmore Girls, let's see.
When are you getting a new hat?
Probably never.
I mean, this hat actually has been switched out for identical hats along the way during
this channel.
When does one drop the True Origin of Santa truth asking for a friend because I never
had children?
Yeah, so there's a whole bunch of different ways parents do that.
I'm going to be super careful in talking about this right now.
Some parents, it's really common now for parents to explain the metaphorical aspects of Santa
very, very early, but still keep the curiosity around.
And then sometimes the true origin is just never known until they find out on their own.
So I don't know.
I don't know that I have any advice on that one.
Let's see.
Hobbies you'd like to do if time and money weren't a factor.
Start a school.
unsweetened iced tea the worst beverage of all time? Yes. If you could meet any
fictional character who would it be? Sherlock Holmes. If you could really
train and learn one extreme sport like skateboarding, surfing, freehand climbing
or other such activity what would it be? I don't know. I mean I'd really like to be
really good at mountain climbing but let's see how not to be serious okay what
was the best Christmas present you received but didn't expect your child or
adult. So when I was very young, my dad told me that this giant box was a coffee
table for my mom and that I needed to, you know, make sure I didn't tell her.
This was his way of keeping me from snooping and finding out that inside was
the USS flag which was a giant highly sought-after GI Joe toy. Didn't expect
it because my dad was enlisted Army. That was something that they put away to
get. What are your horse's names and what are their personalities like and which
Which one is your favorite?
The horse's names are Pirate, Jackie, and Marilyn.
Let's see.
So Pirate is, she's more like a giant dog.
She was a polo horse and she got injured.
So she's honestly kind of my favorite.
She follows me around and she's a dog.
I can't even ride her.
And then Jackie is a mare.
If you're asking about a horse's personality, she's a mare.
And she is the merriest mare of them all.
And then Marilyn is a baby and is very curious and kind of goofy, to be honest.
But she's still young.
What is your favorite rom-com, favorite romantic comedy?
French Kiss.
Have you ever tossed any deer droppings near the chimney or anything to show that reindeer
stop by?
No, I have not.
Just taking that to a whole new level.
What's your go-to genre of music to lift your mood?
Like bubblegum pop, Britney Spears, Miley Cyrus, type of stuff like that.
the music that suits the moods you want to be in. Not the moods you're in. What's
your drag name? I think it would be Bonita. I'm gonna go with that. I'm gonna
go with that. Let's see, if you had a time machine to go anywhere you wanted past or
future, where would you go and why? I don't know, five years into the future.
And try to change anything I don't like, I guess. Some people might say this is a
very serious question. What is your favorite regional style of barbecue?
Kansas City, Carolina, Memphis, Texas or other Memphis. Do you play disc golf? No.
Is a hot dog a sandwich or a taco? Well a taco is a sandwich so a hot dog is
definitely a sandwich. Dogs or cats? Which is better? Dogs. Who is your favorite
NSYNC member? I do not have one. What is your favorite drink? I am Super Simple
Jack. How far left is Santa? All the way. Santa's all the way left. Doesn't respect
borders. No money. No classes. No, yeah, stateless, classless, moneyless. Let's see.
Your t-shirts and Easter eggs on the shelves are a never-ending source of
mystery and entertainment. Please explain the things on your shelf. Hats and
and t-shirts.
The whole idea behind it when we started doing it is just something to mix it up a little
bit to people who watch the channel over and over again to get to see the little changes
and anytime something changes back there, there is a reason.
There's something that made me add or subtract something.
What's your go-to EDC?
A Spyderco Para is what I carry, and for those everyday carry, I'm assuming you're talking
about a knife.
I don't own a gun anymore.
And I did, I'm a white guy in the south, I carried a 1911.
Or a Heckler & Conch, USP.
Let's see, beetles or stones, beetles, what's the most hillbilly, redneck food you enjoy?
I don't know, probably my Waffle House order.
Moon pies, moon pies.
And what is the most metropolitan elitist food that you enjoy?
Bananas foster.
What does your hair look like?
My head's shaved.
There are videos where I'm not wearing a hat.
There's not many, but there's a couple.
many kids do y'all have? How did y'all not go insane? Nobody says we
didn't. Yeah. You know, anything, you know, you have one kid and then you have the
second one, right? And then it's still one-on-one, you know. When you get to the
point where you have more children than adults, that's a, you're outnumbered and
and they can wear you down.
Eventually they will win.
So when are you doing your G.I. Joe walk-on cameo is out back?
Yeah.
Yeah, that tracks, I guess.
I'm not even mad. That tracks.
What's your favorite takeout place where you live?
Well, outside of a town of four thousand people, we don't have a takeout place.
Um, what's the most festive COVID mask you own?
Um, I have one that looks like a claymore.
It's front toward enemy.
Um, and I mean, I, I think that's kind of festive.
And then I have one that like has all the tattoo Disney princesses and then an
Alice in Wonderland 1-2.
Which child did you steal the elephant from?
Oh, yeah, that's my second oldest.
Thin gravy or thick gravy on biscuits, thin, white with sausage.
Was there a book, article, or story you read or heard in the past that came to mind multiple
times as you felt yourself growing into a communicator and educator?
Asking is someone who wishes to enter a similar field."
So for me, I think what helped me get to the point where I've gotten better at this over
time was giving a lot of briefings and not wanting to answer questions.
Because at the time, my face did not conceal my opinion of the question, and most times
the people asking the questions were my bosses.
So I learned to provide briefings in such a way that I wouldn't be asked questions
afterward. So, that was my motivation right there. That's how I learned to get things
to a level where it didn't matter what the topic was, everybody sitting at that table,
no matter where they were at, what their field of expertise was, they were going to be able
to relate to what was being said and understand it.
How often do you go riding?
Not as much as I would like, but that's going to change soon.
What do you do for fun most often?
Play with my kids.
This has been a recent debate.
Which gummy bear is best?
Haribo.
I don't even have to read your options.
Can we please see pictures of the new horse?
I have posted some on Twitter.
Do you have any jokes you normally wouldn't tell to general population?
I've known a few military types that can tell.
Yeah, I know those jokes.
I have a really good memory, so if I hear a joke, I'll remember it.
I don't tell jokes like those.
Most stuff like that, it's a defense mechanism, a coping mechanism of some kind.
These are probably from infantry guys.
I'm not saying that as something, oh, well, that's just something horrible that the infantry does.
every little world has their own versions but yeah I I know some but I
haven't told any of them and forever it's literally more than a decade can
you cook if so yes what is your go-to recipe shepherds about somebody answer
that. Okay. Do you know your horses names yet? I always knew their names. I didn't
know their breeds. So for those that don't know somebody on Twitter asked me
one time you know what the breeds were. I don't have a clue. I don't know and you
know I'm one of those people I can look I can glance at a husky and tell you
whether it's Alaskan or Siberian. I can tell dogs apart. Around here, I can tell
you which breeder they came from. When somebody asks me about the horses, it's
a white one. It's a brown one. I don't know. I really don't. I'm learning a little bit more
about them, like as far as how to identify the breeds, but I honestly never
cared to me it was to me they're fun you know it's not quite the same my wife is
very into it to me I just enjoy being around them it's not so much work let's
see will the new telescope work I don't know if you're talking about a present
you have bought for somebody or the one they're gonna send up. If it's a present you bought
from somebody, I'm sure it will. If it's the one that they're sending up, I have doubts
to be honest. It's not looking good. Are all three going on four-year-olds? Anti-vaccine?
Yeah, pretty much.
What's the influence of regional barbecue in partisan divides?
People who like Memphis style are more liberal.
I don't know if that's true, it's just my personal experience.
If you could talk to your younger self for an hour, what would you tell yourself?
And how do you think your younger self would react to the person you ended up becoming?
Let's see.
It depends on how much younger.
Talking about me in my 20s, I would think that I had gone soft.
Talk about me as a kid, I'd probably be pretty happy with how I turned out.
You could save any animal species from extinction.
Which one would you save?
Wow.
I mean, that's hard, I don't know.
There's a lot.
a lot that need help. I don't know that I have a direct answer to that. And that looks
like the end of the questions. What's in that container behind you? No, not that one. It's
apparently the last one. Okay. All right. So I hope this helps pass some time for you
if you needed to pass some time, needed to get away, so we'll be back to our
regularly scheduled programming tomorrow. Anyway, it's just a
thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}