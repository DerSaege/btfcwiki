---
title: Let's talk about when you aren't the target....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=45YNPPZlMv8) |
| Published | 2021/12/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recounts a story about a friend and another friend getting pulled over in a remote area in Alabama, where the driver, who is black, immediately displayed fear and compliance towards the police.
- Observes various animals in a peaceful setting, including bunnies, squirrels, and birds, with a horse nearby.
- Notes that all the animals, except for the horse and himself, get spooked and scared when a hawk flies overhead.
- Draws a parallel between the fear response of the animals to the presence of a predator and the fear experienced by individuals who are typically targeted by authorities.
- Suggests that understanding others' fears and concerns requires acknowledging one's own privilege and position in society.
- Concludes with a reflection on the need for empathy and acceptance of differing experiences and perspectives.

### Quotes

- "If you see somebody who is concerned about a certain thing and it just doesn't register with you why they're worried, it might be because you're not the target."
- "Seems like that might be a reality that some people need to accept."

### Oneliner

Beau recounts a story of police interaction, paralleling it with animal behavior to illustrate privilege and understanding others' fears.

### Audience

Empathy Seekers

### On-the-ground actions from transcript

- Acknowledge and validate the fears and concerns of those who may be targets of discrimination or profiling (implied).

### Whats missing in summary

The full transcript provides a deeper understanding of privilege, fear, and empathy through storytelling and animal behavior analogies.

### Tags

#Empathy #Understanding #Privilege #Fear #PoliceInteractions


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about a story my friend told me.
And me seeing some animals.
Me seeing some animals.
And how it might help to understand where other people are coming from.
So my friend tells me about him and another one of our friends
going up to this little area north of Dothan, that's in Alabama.
And they're out in an area that, I mean, even I would call it the sticks.
It's way out there.
The guy who's telling me the story was in the passenger seat
and they get pulled over.
He's like, Matt, he says the guy's name, who was driving.
And he said, man, he got shook.
He got scared.
Immediately went to, yes sir, no sir.
It's like, I've never seen anybody like that before in my life.
He's never been in trouble with the law.
I'm like, well, yeah, he's black.
He's like, kind of blows it off, waves me off with his hand.
And he's like, yeah, I don't get it.
I mean, the cop wasn't doing anything.
Didn't do anything to make me nervous.
And I just didn't have it in me, honestly, to have one of those conversations that day.
But it had been stuck in my head for two weeks.
And I'm out walking by one of the pastures and it's, there's like two little bunnies,
a bunch of squirrels playing, birds out and everything.
And then a horse off to my right.
And I'm just sitting there kind of enjoying the moment, you know, waiting for like a Disney
princess to come out and start singing.
And as I'm looking around, this hawk does that thing where it has its wings out and
just glides over, you know, real pretty.
And I look back, all the animals are gone except for me and the horse.
Why?
They all got spooked.
They all got scared.
The horse and I didn't because we're not the hawk's typical prey.
We don't have a learned instinct to be concerned.
Because a hawk isn't going to do anything to us, more than likely.
Even if it does, we'll make it through.
Seems like that might be a reality that some people need to accept.
If you see somebody who is concerned about a certain thing and it just doesn't register
with you why they're worried, it might be because you're not the target.
You're not the one that is typically the prey of that particular predator.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}