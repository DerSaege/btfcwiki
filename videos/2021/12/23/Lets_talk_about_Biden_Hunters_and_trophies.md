---
title: Let's talk about Biden, Hunters, and trophies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RqlWcFH1Ti8) |
| Published | 2021/12/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Boris Johnson proposed banning imports of trophies from at-risk animals, a move supported by Beau.
- The United States imports more wildlife trophies than any other country due to exemptions in the Endangered Species Act.
- Wealthy individuals can pay for permits to hunt endangered species under the guise of conservation.
- These "canned hunts" often involve hunting animals in enclosed spaces or with no natural fear of humans.
- Biden could make changes to the Endangered Species Act through executive order to ban such imports.
- Biden faces challenges in Congress due to Republican obstructionism and may need wins to show progress.
- Taking action through executive orders could pave the way for future legislative changes.
- Democrats need to push through wins to show progress before the midterms.
- Beau suggests Biden should focus on achievable causes like banning imports of wildlife trophies.
- Thinking creatively and acting promptly is necessary for the administration to make progress.

### Quotes

- "This could give Biden some wins."
- "The administration is going to have to start thinking outside the box if it wants to get anywhere."
- "It's just a thought."
- "You're not going to get a lot of pushback either."
- "You all have a good day."

### Oneliner

Beau suggests Biden take executive action to ban imports of wildlife trophies, a move that could yield wins amidst congressional challenges.

### Audience

Conservation advocates

### On-the-ground actions from transcript

- Ban imports of trophies from at-risk animals through executive order (suggested)
- Push for wins through achievable causes (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the loopholes in the Endangered Species Act and how Biden could address wildlife conservation issues through executive action.

### Tags

#WildlifeConservation #EndangeredSpeciesAct #ExecutiveAction #BidenAdministration #RepublicanObstructionism


## Transcript
Well howdy there internet people. It's Beau again.
So today we're going to talk about President Biden,
hunters and their trophies,
and how President Biden might be able to get a few trophies of his own.
And we're going to talk about Boris Johnson
and how all that stuff goes together.
Boris Johnson over in the UK
has sent a proposal to Parliament for its consideration.
I don't know the terminology they use.
He sent a bill over, is how we would say it here.
And I can't believe I must say this,
but I fully support Boris Johnson.
The proposal bans the imports of trophies from at-risk animals,
imperiled, endangered, threatened,
whatever term you want to use.
Stops them from being imported.
It's a great idea. It's a good thing.
Seems like something that should already exist to be honest,
but see here I stand in the United States
where we have the Endangered Species Act
that stops that from happening here in the US, right?
No, it does not. Not really.
In fact, the United States imports more wildlife trophies
than any other country on the planet.
And a lot of them are from the species that should be protected,
are from different species that should be protected.
Because in the Endangered Species Act,
there's an exemption, an exception for trophies
that somehow enhance the survival of the species.
What does that mean?
It means that some rich person paid a whole lot for the permit.
That's really what it means.
Those canned things that they go on,
I'm not going to call them hunts.
Absolutely refuse.
That's where a lot of them come from.
If you pay enough, you can take a piece of an endangered species
and turn it into an end table.
So you can tell stories about how you went over there
and you engaged in this great hunt like it's the 1800s,
while you used the most advanced technology on the planet today.
And you were hunting in a space where the animals probably fenced in.
And more than likely, it was either already sick
or it had been raised by humans its entire life
and had no reason to fear you.
It doesn't actually send the image of a tough guy,
just to let you know.
But that exemption, that exception exists.
So the US imports more wildlife trophies
than any other country on the planet.
That's where we're at.
Even though we have the Endangered Species Act,
it's not actually effective.
Needs to be changed, right?
That should be fixed.
Problem is Biden can't get anything through Congress right now.
Good news, he doesn't have to.
Because of the way the Endangered Species Act is structured,
this is one of those things that can be done with a pen.
Few changes in the Federal Register and this is done.
It might be a really good idea
because Biden is having trouble getting stuff through Congress.
He's having a lot of issues.
The administration is having issues getting his agenda through, right?
Has obstructionist Republicans standing in his way.
He's expending a lot of political capital
and not getting a whole lot back for it right now.
It might be a good idea to start racking up some wins.
Start racking up some trophies.
Get some stuff on the shelf.
We did this, this, and this.
Even if it's just done with executive order.
Sure, it can be undone later by another administration.
Or it could be done by executive order now
and then two years from now,
he could come in and say, hey, you know what?
It's been like this for two years.
Why don't we just go ahead and make this official
and get the legislation through and get it changed for good?
The Democrats are going to have a hard time in the midterms
if they don't start pushing through some wins.
If they don't start getting something to show.
Because while people who watch this channel,
people who are up to date on a lot of politics,
they understand that Republicans and Senator Manchin
are obstructing his agenda.
They get that.
They understand that.
They understand that even if the filibuster was gone,
with Manchin's behavior, it doesn't even mean anything anymore.
So if you're up to date, you understand
it's the Republican Party standing in the way.
If you're not, you don't know that.
A lot of people aren't up to date.
A lot of people only pay attention to politics
during the election cycles.
All they know is negative coverage, negative coverage,
negative coverage.
This could give Biden some wins.
I mean, it helps that it's a cause that I care deeply about as well.
I'm not going to pretend that's not the case.
But I'm sure there are other things similar to this
that he could also do.
This is just one that's easy.
You're not going to get a lot of pushback either.
The administration is going to have to start thinking outside the box
if it wants to get anywhere.
And it should probably start now.
Anyway, it's just a thought. You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}