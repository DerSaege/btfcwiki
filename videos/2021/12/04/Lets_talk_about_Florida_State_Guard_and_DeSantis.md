---
title: Let's talk about Florida State Guard and DeSantis....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t_WBSZKCozg) |
| Published | 2021/12/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- DeSantis' program in Florida is a political stunt, not designed to be effective at creating a paramilitary organization.
- The allocated budget of three and a half million dollars is insufficient to establish any significant operation.
- The program appears to be a training initiative similar to FEMA's CERT program, focusing on basic emergency response skills.
- Beau suggests that there is no need to be overly concerned about this program currently.
- He points out that the allocated budget is far too low to support any meaningful paramilitary organization.
- With the budget constraints, the program is likely to be just a small office in Tallahassee teaching first aid.
- DeSantis may be using this program as a political tool to appeal to certain authoritarian desires within his base.
- Beau implies that the program could potentially become more concerning with increased funding in the future.
- Setting up task forces within existing law enforcement agencies like FDLE might be a more worrisome scenario.
- The current program lacks the necessary resources to be a substantial operational force, even if fully equipped.

### Quotes

- "Its goal was to be a political stunt to appeal to the less informed members of his base who actually want that."
- "That's not enough money to do anything."
- "This is a joke. It's a political stunt."
- "But he's going to market it as something else, as a political tool, because he knows his base wants General DeSantis."
- "That's not enough money to really run one, even if they already had everything, even if they already had the equipment."

### Oneliner

DeSantis' Florida program is a political stunt with an insufficient budget, likely just a training initiative at present, not a major concern.

### Audience

Florida residents

### On-the-ground actions from transcript

- Keep informed about any developments related to the program (implied)
- Monitor the allocation of funds and resources towards community emergency response training (implied)

### Whats missing in summary

Beau's detailed analysis and insights into the potential implications of DeSantis' program.

### Tags

#DeSantis #Florida #PoliticalStunt #Budget #EmergencyResponse


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Florida and DeSantis and his new little program or
whatever.
I know there's a lot of people who are concerned about it and the coverage of this little announcement
did exactly what DeSantis wanted it to do.
This is a joke.
It's not going to be effective.
It can't be effective.
It wasn't designed to be effective at actually producing some kind of paramilitary organization
that the governor can call out.
Its goal was to be a political stunt to appeal to the less informed members of his base who
actually want that.
That's easy for me to say, but how do I know that that's what's going on?
Because I looked at the budget.
To start this up, they allocated three and a half million dollars.
Anybody who has ever worked security, been involved in the military, anything like that
already knows where this is going.
That's not enough money to do anything.
Three and a half million dollars, if you wanted to set up some kind of paramilitary organization,
you would need at least one paid person in every county and then like a supervisor for
southern Florida, central Florida, and northwest Florida.
That's how it would be divided up.
If they paid those people just the average pay of a normal cop in Florida, they are already
out of money.
That doesn't include any higher ups.
That's the base staff, the bare minimum.
They're out of money just on the salary.
That doesn't include benefits.
That doesn't include the stuff they would need such as cars, radios, gas money, offices,
phones, anything like that.
The money's not there.
This is a joke.
It's a political stunt.
So he can set himself up as General DeSantis or whatever.
And then aside from that, if you actually look into what this is designed to be, it
really looks like he's just recreating FEMA's CERT program, Community Emergency Response
Training, team, something like that.
And basically what that was or is, people go from FEMA and they teach basic first aid,
first responder skills, stuff like that, how to help out after a disaster.
That appears to be more what this is, is a training program.
Now could later it be expanded into something that people would actually need to worry about?
Sure.
But with the budget it has, this isn't something to be concerned about.
I would be far more concerned if Governor DeSantis decided to start setting up task forces
inside FDLE, that's the Florida Department of Law Enforcement, or the state troopers,
something like that.
That would be more likely to be used the way the coverage is kind of indicating, which
is like DeSantis' secret police or something like that.
I don't really think this program is necessary.
I think it's a giant waste of money, but it's also not something I'm going to lose sleep
over.
It doesn't have any funding.
Three and a half million dollars when you start talking about a paramilitary organization,
that's nothing.
That's not enough money to do anything.
That's not enough money to really run one, even if they already had everything, even
if they already had the equipment.
That's certainly not enough to purchase the equipment and establish one.
At base pay of what an average cop is, they can hire 70 people.
There are 67 counties in Florida.
It doesn't even cover the administrative costs that would exist in Tallahassee or something
like that.
This is probably going to be a tiny office in some building in Tallahassee that has a
few people working in it, and they travel around and teach first aid.
But he's going to market it as something else, as a political tool, because he knows his
base wants General DeSantis.
They want that authoritarian, militaristic whatever.
And this is the way he can appeal to them.
Again, maybe later, with an increase in funding, it's something to watch.
But as it currently stands, I'm not going to be losing sleep over this.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}