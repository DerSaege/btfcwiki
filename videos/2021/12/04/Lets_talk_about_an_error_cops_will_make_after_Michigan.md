---
title: Let's talk about an error cops will make after Michigan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Eoq5XPWGuY4) |
| Published | 2021/12/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- An incident involving students sheltering in a class and being told it's safe to come out by a person claiming to be a cop.
- Students hesitated when the person used the term "bro" and ultimately escaped through a window.
- Law enforcement agencies are looking for ways to prevent similar incidents but need to be cautious about the information they provide.
- Simply asking for department name and badge number might not be the safest approach as suspects could exploit this information.
- Beau suggests a safer alternative where officers provide a number that is not publicly visible, and students confirm the officer's identity with dispatch before opening the door.
- Training students on safety measures can inadvertently train suspects as well, so it's vital to have secure protocols in place.
- Beau warns against using information that suspects could easily exploit in such situations.
- Parents should communicate with their kids about any new procedures being taught and ensure they understand the safest course of action.
- Beau advocates for reevaluating the training methods for students in dealing with potentially dangerous situations.
- He questions the necessity of teaching middle schoolers about sign and countersign tactics.

### Quotes

- "You don't feel safe, get out."
- "Can't use information that is publicly visible like that."
- "Because anything that you're going to train the students to do, you're also probably training the suspect."
- "That won't work. That will go badly."
- "We should probably really start to re-evaluate stuff now that we're training kids how to deal with stuff like this."

### Oneliner

Law enforcement agencies need secure protocols to verify officers' identities without compromising safety in potential threat situations.

### Audience

Parents, educators, law enforcement

### On-the-ground actions from transcript

- Contact the Sheriff's Department if your child is being taught to ask for department name and badge number to verify an officer's identity (suggested).
- Communicate with your kids about the safer alternative where officers provide a unique number, not publicly visible, for verification before opening the door (suggested).
- Advocate for secure protocols in place to ensure students' safety in potential threat situations (implied).

### Whats missing in summary

Importance of maintaining safety protocols while verifying officers' identities in potential threat situations.

### Tags

#SafetyProtocols #LawEnforcement #Students #CommunitySafety #TrainingEvaluation


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about an error
that I think is in the making.
If you have school age kids, watch this.
OK, so there was yet another, another incident.
And this one produced some footage that went viral.
There were students, they're in a class,
and they're doing everything that they're supposed to do.
They're sheltered, right?
And somebody knocks on the door, says, I'm a cop.
It's safe to come out now.
And one of the students is like, yeah, I'm not feeling that.
And the person on the other side of the door presses the issue.
No, it's OK.
Come on out.
Students are like, no.
Come to the door and look at my badge, bro.
When the students heard the word bro,
they're like, yeah, red flag, that's not a cop.
And they went out the window.
First, good for the students.
You don't feel safe, get out.
Yeah, you are under no obligation to do that.
If you don't feel safe, get out.
Now, it did turn out that it was a cop
on the other side of the door, but they didn't know that.
And they were going off the information they had.
They made a call, and good for them.
The error that I think is about to be made
is by law enforcement agencies that saw this footage.
They realize there's an issue.
And they want to come up with a way
so this doesn't happen again in the future.
They want to come up with something that they can say
through the door, a conversation they can have through the door,
to let the students know that it really is a cop.
This is a good idea.
However, what I have seen go out in the media
is for the students to ask department name and badge
number.
I guess from there, they'll call dispatch.
And dispatch will say, yeah, that person's there.
Or maybe they radio them and say, hey,
are you trying to get in the door?
And the person says, yes.
And dispatch gives the students the OK to open the door.
And then they do.
And then they all get shot because Officer Smith is
laying in the hallway.
And all of the information that is being passed back and forth
is visible on his uniform.
It's a good idea.
That cannot be the information used.
Aside from that, something that law enforcement continuously
forgets when they're talking about training students
on how to deal with this is that when they do it,
they are more than likely also training the suspect.
The people responsible for this type of stuff,
they typically have a planning period.
If they know that name, badge number, and department
will get them through the door, I'm
fairly certain they will go look at the SRO's badge number.
They know that person's going to be there.
It can't be that information.
It's a good idea, but it can't be that information.
So if your kid is told this, you need
to call the Sheriff's Department and explain that.
That is not the way to do it.
The idea itself is very sound.
But what should probably happen is for the officer
to provide a number.
And that number is coming from a place the students don't know.
It could be the last four of the serial number
on their radio or weapon.
It could be their car number.
It could be anything.
But the students don't know what the number is related to.
They provide the number through the door.
The students call dispatch.
Dispatch says, this is the person's name.
The students ask what the name is.
And if they get the right number or they get the right name,
then they go look to the door, but not before then.
Can't use information that is publicly visible like that.
It doesn't do any good.
Because anything that you're going to train the students
to do, you're also probably training the suspect,
because they're probably sitting in the class.
So talk to your kids, find out if that is a new procedure
they're being taught.
If it is, call the Sheriff's Department.
Because that won't work.
That will go badly.
I also think that we should probably really
start to reevaluate stuff now that we're training kids
how to deal with stuff like this.
And they're having to learn sign,
countersign stuff in middle school.
That's, I think, in most societies,
that's something we should probably see as unacceptable.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}