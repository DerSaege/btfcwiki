---
title: Let's talk about Brandon, Biden, and Jared....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cLM5Nj2IfYA) |
| Published | 2021/12/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the trend of saying "Let's go Brandon" as a way to express displeasure with President Biden within the conservative circle.
- He acknowledges that criticizing leaders, including President Biden, is valid but suggests doing it with actual criticism rather than juvenile chants.
- Beau finds humor in the irony of people who criticized liberals for saying "orange man bad" now using a similar chant against Biden.
- The incident that prompted the recent surge in "Let's go Brandon" chants involved President Biden responding casually to a family member saying it on a call during a Santa tracker event.
- Beau speculates on the various interpretations of Biden's response, ranging from being classy to being unaware of the context.
- He shares his belief that Biden's PR team likely informed him of the chant previously, but it may have been forgotten due to its irrelevance.
- The main takeaway from Beau is to not waste opportunities for meaningful communication like the individual, Jared, who said "Let's go Brandon" in a moment with a wide audience.
- He encourages thinking about what one might say if given a platform to reach millions and avoiding wasting such a chance.
- Beau expresses disappointment in the missed potential for Jared to advocate for something he deeply cares about instead of uttering a seemingly frivolous statement.
- In summary, Beau views the whole "Let's go Brandon" situation as irrelevant and a missed chance for constructive communication.

### Quotes

- "Don't be like Jared."
- "It's irrelevant. It's silly. It's a real-life version of Orange Man bad."

### Oneliner

Beau addresses the "Let's go Brandon" trend, advocating for meaningful communication instead of wasted opportunities like Jared's frivolous chant.

### Audience

Social media users

### On-the-ground actions from transcript

- Advocate for meaningful messages when presented with a platform (implied)
- Think about what you want to convey to millions before speaking in public (implied)

### Whats missing in summary

Beau's tone and delivery nuances are missing in the summary.

### Tags

#Brandon #PoliticalChant #Communication #Opportunity #Criticism


## Transcript
Well howdy there internet people, it's Beau again.
So today, I guess we're going to talk about Brandon.
Because that's super important.
I've been asked to talk about Brandon a few times in the past and haven't, but because
of what happened over the holidays and the number of requests I've gotten to provide
some kind of take on this, I guess we will talk about it.
If you have no clue what I'm talking about, which is probably most people watching this
video, I guess in the cool kids conservative club, saying let's go Brandon is a method
of expressing displeasure with President Biden.
Okay as far as that goes, yeah he's the President of the United States.
Criticizing him is good.
You're allowed to.
It's important to criticize your leaders.
I mean I would prefer people do it with actual criticism rather than some juvenile chant,
but whatever.
I don't really have a problem with the overall thing.
I do find it kind of funny that the same people who are walking around for the last five years
or so going all liberals do is say orange man bad are quite literally chanting something
like that.
But nobody's ever going to accuse them of being self-aware, so it doesn't matter anyway.
The event that happened over the holidays was that President and Dr. Biden, they were
doing one of those tracker things where they follow Santa's path and there were call-ins
and the President of the United States took the time to talk to this family and at the
end of it the dad said let's go Brandon.
And yeah, Biden, he's like I agree.
Let's go Brandon.
Some people are taking that a whole bunch of different ways.
Some people are saying well he was just being classy.
Some people are suggesting it's proof he's some doddering old man that doesn't know what's
going on.
Some people are blaming it on his PR folks for not informing him of this massive movement
of people saying somebody else's name, whatever.
And some people are suggesting that it was more of yeah, why don't you come do that.
My personal belief is that when this started forever ago that his PR people probably did
brief him on it and say hey, this happened.
This was said, this was chanted, it was caught on the news and what with him being President
of the United States and all and it being so long ago because conservatives have a good
sense of humor and there's no way they would just run something into the ground.
They probably forgot about it, not because he's old but because it happened forever ago
and it's kind of irrelevant.
It's not really a big deal.
That would be my guess.
Now as far as a takeaway from this, the only thing I can think of is don't be like Jared.
That's the guy.
Don't be like him.
And I'm not saying don't say something if you feel it's important.
I'm suggesting that if you have the opportunity to have millions of people hear what you're
going to say, perhaps you might want to say something a little bit more valuable than
let's go Brandon.
If that was me, if I did that, any time I thought about that for the rest of my life,
I would cringe.
I would be so embarrassed.
I would be ashamed.
Had the world watching, could have said anything and had that message reach a million ears
and that's what he said.
I would have more respect for him if he like plugged his business or something.
It doesn't even have to be some great political statement or some statement of support for
anything that matters.
It just seems like a giant wasted opportunity to me.
So if there is one takeaway, don't be Jared.
Don't be Jared.
Take the time now to figure out what you would say.
If you knew what you were about to say was going to reach millions of people so you don't
end up wasting an opportunity like that.
I don't know this guy.
I'm sure that there is something that he cares deeply about that he could have advocated
for in those few seconds, but maybe not.
So there's my take on Brandon.
It's irrelevant.
It's silly.
It's a real life version of Orange Man bad.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}