---
title: Let's talk about origin stories and you....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3ACwRQPrVzc) |
| Published | 2021/12/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the origin of a term and its current usage, pointing out a common error he noticed.
- While assisting with tornado relief efforts in Kentucky, Beau encountered someone using the term "rule 303" frequently.
- This encounter led Beau to research the phrase, discover people doing positive things, and eventually find his channel.
- Beau expresses his interest in parasocial relationships and how influencers and their audience interact.
- He clarifies that he did not create the current usage of the term in question.
- Beau advocates for servant leadership and taking action to make the world a better place if you have the means.
- He explains that the term's origin, involving military contractors and law enforcement, was not intended to describe moral standards.
- Beau acknowledges that his audience transformed the term into a force for good, not him.
- He mentions that his early video, "Let's Talk About Rule 303," did not suggest the term should be used as it is today.
- Beau credits his audience for turning something of questionable origin into a positive force and expresses curiosity about their future endeavors.

### Quotes

- "How does it feel to create something that people use as their standard of morality? Wow."
- "If you have the means, you have the responsibility."
- "Y'all turned it into the force for good."

### Oneliner

Beau clarifies he didn't create the term's current usage but acknowledges his audience's role in transforming it into a force for good.

### Audience

Content Creators

### On-the-ground actions from transcript

- Create content that inspires positive change (exemplified)
- Amplify positive initiatives within your community (exemplified)

### Whats missing in summary

The full transcript provides deeper insights into the audience's influence and the power of community-driven positive change.

### Tags

#OriginofTerm #ParasocialRelationships #CommunityInfluence #PositiveChange #ContentCreation


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about the origin of a term,
or at least the origin of a usage of the term today.
If you are a longtime viewer of this channel,
definitely stick around for this because it's about you.
Because it is about you.
And I've seen a lot of people make the same error
that's contained in this message.
And this might be a good time to kind of clear something up.
I met someone in Kentucky helping after the tornadoes
who said rule 303 a lot.
It led me to look up the phrase, which
led me to hashtags of people doing wonderful stuff, which
led me to your channel, which led me to watching
hundreds of your videos.
I'm very interested in parasocial relationships
and how influencers and their audience interact.
How does it feel to create something
that people use as their standard of morality?
Wow.
That is a wild sentence right there.
Well, I don't know.
I'll let you know if I ever do that.
I didn't.
I didn't.
I didn't create the current usage of that term.
That didn't come from me.
Don't get me wrong.
Your research is right.
You're on the right channel.
But you're looking at the wrong side.
It didn't actually originate here.
Yeah, I advocate for that style of servant leadership.
I advocate for looking at the world in that way
and seeing what you can do.
If you have the means, you have the responsibility.
Sure.
But I didn't turn it into what it is today.
This isn't me being modest.
Go back and watch the first video.
It's one of the few that's easy to find on the channel.
It's called Let's Talk About Rule 303.
The video is me explaining the very questionable origin
of the term, how it got used by military contractors,
how it then entered active duty, and how it started
entering law enforcement.
And I was taking issue with law enforcement using that term.
That's the video.
That's the video.
There is nothing in that video to suggest
that it should become a description of how
we interact with the world.
That wasn't me.
After that video, when I would cover something that was good,
people who were taking it upon themselves
to make the world a better place,
comments would say Rule 303.
It resonated with the audience, to answer your question.
But they, y'all, turned it into that.
Y'all turned it into the force for good.
Not me.
Once I saw that that's how it was being received,
I had the means to amplify it.
So I had the responsibility.
But it didn't originate here, not on this side of the camera.
So if you see that hashtag on something,
I don't get credit for that.
Y'all do.
Y'all did it.
And the thing is, it's not the first time
that y'all have taken something of questionable origin
and turned it into something good.
It's not the last time y'all have done it.
In many ways, you could say you did it with the whole channel.
It's y'all.
It wasn't creating a standard of morality
or something to measure your morality by.
That was the morality that y'all had.
Just helped give it a name.
Didn't really even do much in that regard.
And again, if you think this is just me being modest,
go back and watch that video.
There's nothing in it to suggest that it
should go the way it did.
Wasn't me doing that.
Y'all did that.
Y'all heard that idea that if you have the responsibility,
or if you have the means, you have the responsibility to act.
And it resonated with y'all.
And it resonated with y'all, and y'all put it out there.
I don't think that anything like this
should be personified in one person.
And I certainly don't think it should be personified
in one person if it's not true.
That's not really what went down.
I didn't come out and say, hey, this is how we fix everything.
I told the story, explained the term and that phrase,
and y'all took it and ran with it.
Honestly, I can't wait to see what y'all come up with next.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}