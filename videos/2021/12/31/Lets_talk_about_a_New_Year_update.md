---
title: Let's talk about a New Year update....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CWJzMIWes0c) |
| Published | 2021/12/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Wishing a happy new year and giving updates on the channel's direction.
- No major changes planned for the main channel as it's working well.
- Second channel, "The Roads with Beau," will have more content, including long-format documentaries and how-to videos on various topics like gardening and emergency preparedness.
- Viewers are encouraged to subscribe to "The Roads with Beau" for future content.
- Patreon subscribers will soon receive an announcement of something special as a token of appreciation.
- Cautioning viewers against purchasing merchandise from bootleg companies duplicating Teespring products, as they are of lower quality and more expensive.
- Plans to increase video uploads to three a day during midterms, but overall content will remain consistent.
- Encouraging viewers to make resolutions, have fun, and stay safe in the upcoming year.

### Quotes

- "Make your resolution. Have fun. Be safe."
- "The only place I have merchandise is on Teespring."
- "So just bear that in mind."
- "Now, over the next year, you'll probably end up going back to three videos a day."
- "It's just a thought."

### Oneliner

Beau provides updates on channel plans, merchandise caution, and future content, urging viewers to subscribe and stay safe in the new year.

### Audience

Content Creators

### On-the-ground actions from transcript

- Subscribe to "The Roads with Beau" for upcoming content (suggested).
- Support Beau's work on Patreon (suggested).
- Purchase merchandise only from Teespring to ensure quality (suggested).

### Whats missing in summary

Details on the specific content of the upcoming videos and potential collaborations.

### Tags

#ContentCreation #YouTube #Updates #Merchandise #NewYear


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, first, let me start by saying, happy new year.
Today is just going to kind of be updates on the channel
and what's going on.
I realize that I normally only talk
about this type of stuff on the live streams.
So if you don't watch those, you have no idea about some of it.
So we're going to kind of go through what's
going on with the channel and where we're headed,
what we're going to be doing over the next year.
Pretty simple.
This channel is going to stay pretty much the same.
There's no plans to change this.
It works pretty well, and people like it.
And I enjoy it.
So that's a win all the way around.
The second channel, the roads with Beau,
I'm tired of that being in a holding pattern because
of the whole public health thing.
So we started experimenting with other things that we can do.
There's going to be a lot more content pushed out
on that channel over the next year.
There will still be the long format documentary style
stuff.
There will also be the on the road stuff, which
is what the channel was initially for.
And then we're going to do a lot of how to's, a whole lot
of how to's on everything from gardening, sustainability,
homesteading, emergency preparedness,
all kinds of stuff.
You're going to see a lot of the footage
about how we conduct our mutual assistance stuff
is going to be over there.
When we are able to be on the road,
that's also going to be on that channel.
If you're not subscribed, you probably want to go do that.
That's the roads with Beau.
For Patreon, we finally have something for you all.
When all of this started, I was adamant
that we weren't going to put anything behind a paywall.
No content was going to go behind a paywall.
And it was really hard to come up with something
to give back to the Patreon subscribers.
And I mean, y'all kind of fund everything, so there's that.
But we finally figured something out,
and you'll be getting an announcement about that soon.
And then other than that, the last thing,
kind of entertaining, I think, is
that around Christmas, I found out
that there are websites, companies,
that are making duplicates of the shirts
that we offer on Teespring.
And to hear the people who ordered them describe them,
they are not of the highest quality.
I don't have anything to do with those companies.
I know nothing about them.
The only thing I know is that they
are not of the highest quality.
I know nothing about them.
The only place I have merchandise is on Teespring.
Incidentally, they're cheaper on Teespring
than they are from the bootleg places.
So just bear that in mind.
I don't have any t-shirts on any other websites, just Teespring.
So if you're going to order something,
you should check out Teespring links,
because the other stuff apparently is not great.
OK, that's pretty much it.
Now, over the next year, you'll probably
end up going back to three videos a day
as the midterms start to heat up.
But other than that, not a whole lot's going to change here.
So I wish you all the best over the next year.
I know most of y'all are probably celebrating right now,
and you're not going to remember any of this anyway.
But that's OK.
Make your resolution.
Have fun.
Be safe.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}