---
title: Let's talk about a British question about statues....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=849iARfOWQc) |
| Published | 2021/12/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau shares a humorous exchange he had with a British person over Twitter regarding statues of traitors in the South and across the country.
- The chat sparks an amusing dive into the story of Benedict Arnold during the American Revolution.
- Beau gives a brief overview of Benedict Arnold as a major general in the Continental Army who later became a traitor.
- Arnold's betrayal was driven by feelings of being overlooked for promotions and honors.
- The story goes that Arnold offered to surrender West Point to the British but ended up with a minor command instead.
- An anecdote is shared where Arnold jokes with an American officer about what they'd do if they captured him, referencing his wounded leg.
- A statue at Saratoga commemorates Arnold's left leg without explicitly naming him, showcasing history's sense of humor.
- Beau humorously appreciates the level of pettiness in creating a statue of Benedict Arnold's leg.
- History's irony is noted as Confederate statues are repurposed into memorials for civil rights leaders.
- Beau concludes with the importance of being on the right side of history and wishing everyone a good day.

### Quotes

- "History has a sense of humor."
- "There are people who see it who don't really understand what it represents because it isn't labeled."
- "It's a lot more prominent down here though, and it's nowhere near as funny."
- "History does have a sense of humor just like today."
- "It's just a thought."

### Oneliner

Beau shares humor and history, discussing Benedict Arnold and the irony of traitor statues with a lesson on being on the right side.

### Audience

History enthusiasts

### On-the-ground actions from transcript

- Visit historical sites to learn about significant events and figures (suggested)
- Support initiatives that repurpose controversial statues for positive purposes (implied)

### Whats missing in summary

The full transcript captures Beau's engaging storytelling style and his knack for blending humor with historical anecdotes.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to kind of wind down
the year on a humorous note.
I had this person message me over Twitter, British guy.
And he had seen the article about the time capsule that
was found under that statue of Lee that was taken down.
And it kind of reminded him of when
he had traveled there and seen it before it was removed.
And he says, you know, I understand what that represents
and everything that goes along with it
and how horrible it was,
but it really was a pretty statue.
I'm just wondering, is that a thing just in the South
or do y'all have statues of traitors all over the country?
Do you have like statues of Benedict Arnold, funny question.
And it got me thinking about the funniest story
from the American Revolution
because it involves Benedict Arnold.
So today we're going to talk about that.
Now, for those who only know that Benedict Arnold was
a traitor, let me just fill you in real quick.
Very brief overview.
He was a major general with the Continental Army.
And by major general, I mean he was a major figure,
and his rank was major general.
He was distinguished.
He was a good general.
And he was wounded a couple of times in the left leg.
One of them happened at the Battle of Saratoga.
And the thing is that the fact that he was distinguished was, well, part of the reason
he became a traitor.
He felt that other generals who were younger, who hadn't done as much, well, they were
getting promoted ahead of him.
They were getting honors and awards that he felt he should have got.
felt slighted. So he reached out to the British and decided he was going to flip.
He was going to change sides. He came up with this plan, was going to give him West
Point and all kind of other stuff. None of that came to be. At the end he was
just riding towards British lines. When he gets there they give him six grand
and then they give him a token command and say hey you can go do hit-and-run
raids against the Colonials, you know. And the story says, and to be clear, I doubt
the historical accuracy of this story, but it's a long-running story. Story says
that he captures an American officer and Arnold walks up to him and says, what
What would you do if you had captured me?"
And the officer's like, well, we'd give that leg a burial, full military honors, and we'd
hang the rest of you.
And that's funny, right?
That's a funny statement.
If you go to the battlefield there at Saratoga, you will see a statue of a leg.
left leg. It doesn't say whose leg it is. It doesn't specifically spell it out. It says the
most brilliant soldier who won a decisive victory here and won himself the rank of
major general. We actually do have a statue of Benedict Arnold, well at least part of him,
the part that was on our side, his left leg.
To me, to be honest, I aspire to that level of pettiness.
That's always cracked me up. There are people who see it who don't really
understand what it represents because it isn't labeled.
It doesn't explain what it is. But that is, in fact, a statue
of Benedict Arnold. So the answer is yes. We do in fact have statues of other
traitors, not just a southern thing. It's a lot more prominent down here though,
and it's nowhere near as funny. So one of the things to note about this is that
history has a sense of humor. History does have a sense of humor just
like today, a lot of those places that were set aside to honor Confederate
generals, well, when the statues get taken down, they get melted into statues for
civil rights leaders or they get replaced. History has a sense of humor.
It's important to be on the right side.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}