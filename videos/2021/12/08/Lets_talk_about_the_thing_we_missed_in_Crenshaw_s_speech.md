---
title: Let's talk about the thing we missed in Crenshaw's speech....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UqqXWsIeYqA) |
| Published | 2021/12/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the Crenshaw clip and shifts focus from the funny part towards a more serious aspect.
- Crenshaw criticizes the Freedom Caucus and refers to Marjorie Taylor Greene as "space laser lady."
- Crenshaw's main point revolves around how fear is utilized to motivate the Republican Party's base.
- The Republican Party's campaign strategy is based on instilling fear and directing blame.
- Beau points out that creating fear and identifying who to blame is a recurring pattern in Republican strategies.
- Examples are given, such as the misinformation about undocumented immigrants causing harm to Americans.
- Beau contrasts the fear-mongering tactics with the lack of concern over public health crises.
- He stresses the importance of education in combating fear and uncertainty.
- The manipulation through fear and blame is emphasized as a method to control and win elections.
- Beau encourages conservatives to seek knowledge and understanding to counter fear-based manipulation.

### Quotes

- "Fear. I've said it over and over again. That is how the Republican Party motivates their base. Fear."
- "They tell them what to be afraid of and who to blame for it."
- "A real leader, if they're telling you to be afraid of something, they're going to give you a solution."
- "Education, it destroys fear."
- "Stop being afraid. Stop letting them control you that way."

### Oneliner

Beau breaks down the Republican Party's fear-based strategy, urging for education to combat manipulation and fear.

### Audience

Conservatives

### On-the-ground actions from transcript

- Educate yourself on political strategies of fear and manipulation (implied)
- Seek knowledge about issues causing fear or uncertainty (implied)
- Encourage others to prioritize education over fear (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of fear-based manipulation within the Republican Party and the importance of education in combating such tactics.

### Tags

#RepublicanParty #FearTactics #Education #PoliticalManipulation #Conservatives


## Transcript
Well, howdy there internet people, it's Bo again, so today
We're gonna talk about that Crenshaw clip like I'm assuming everybody else is gonna be talking about
But we're gonna do a little bit different. We're not gonna talk about the funny part
Now if you don't know what I'm talking about, I will have a link to this video down below
so
The congressperson from Texas Crenshaw
He's talking to a bunch of conservatives, Republicans, and he goes off.
He goes off.
He takes swipes at the Freedom Caucus.
That's Bo Burton, the space laser lady, calls people performance artists and grifters.
And that's what everybody's focused on.
And don't get me wrong, I laughed.
It's funny.
That's the juicy part, right?
But see, he says something else towards the end.
He says, and it's far more important, see, after he takes swipes at everybody, talking
to Republicans, talking to conservatives, he's talking about the grifters and the performance
artists and he says, we hear lie after lie after lie after lie because they understand
something about the psychology of the conservative heart, that we're worried.
We're worried about what they're going to do to us.
Fear.
I've said it over and over again.
That is how the Republican Party motivates their base.
Fear.
They tell them what to be afraid of and who to blame for it.
And if they don't have anybody to blame for it, or it's one of them that would catch
to blame, you shouldn't be afraid of it. They create a monster to get their base
to do what they want. Jinkies. It's what they do. Now anytime I say that I get a
whole bunch of nasty messages, but now it's their own party admitting that's
the campaign strategy. That's all they have to do to win. Tell them what to be
afraid of and who to blame for it. That's it. And I'm sure that even with it being
repeated by somebody in their own party, somebody's gonna want some kind of
evidence, right? Some kind of demonstration. What was Trump's big thing?
Build the wall. Build the wall, right? Why? Those families put up on display.
had those social media posts going around. 10,000 Americans were taken out
by undocumented people. It wasn't true. They made that up. Created the monster. The
real numbers fluctuate between 14 and 1600. But they created the monster.
And that allowed people to be afraid of something, be worried about something.
And they told them who to blame.
And as soon as they did that, oh, build the wall, send out the troops, have a task force,
roam the country to snatch them up.
Over 1,500 people.
What to be afraid of, who to blame.
What about that public health thing that has taken out hundreds of thousands?
Oh, don't be afraid of that. Don't worry about that, right?
Because who would catch the blame? Probably the person who downplayed it.
Anything that would mitigate it? Oh, that's authoritarian.
They want you to get a shot and wear a mask.
authoritarian, as opposed to calling out the troops, a secret task force, and
militarizing the border. They played you, they tricked you, they created a monster,
told you to be afraid of it, and manipulated you. It's what they do. It's
how they win elections. What to be afraid of and who to blame for it. If you want
to stop being manipulated. All you have to do is look for that. A real leader, if
they're telling you to be afraid of something, they're going to give you a
solution. They're not going to tell you to blame somebody who has less power than
you. Only people who are trying to manipulate you will do that. I would
suggest that conservatives stop worrying so much. Stop being led by fear. Those
things that you are scared of, not scared of, uncertain of, because we know you're
not going to admit you're afraid of something. Those things you're uncertain
of. Learn about them. Education, it destroys fear. Learn about it. Find out
what it is. You have all those people terrified of what's being taught in
schools, adamantly opposed to some theory. And then when somebody asks them, well
what is it? I don't know. What to be afraid of, who to blame for it. It's a
pattern. It's what they do. It's what they've done for a really long time. I'm
I'm going to suggest you to stop being afraid.
Stop letting them control you that way.
Don't be the fearful.
Be the meddling kids in the home of the brave.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}