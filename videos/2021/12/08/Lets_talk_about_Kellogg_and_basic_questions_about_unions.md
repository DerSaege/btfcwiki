---
title: Let's talk about Kellogg and basic questions about unions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=g8ypJjlwld0) |
| Published | 2021/12/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses questions about Kellogg and the ongoing situation with the union.
- He points out that he doesn't personally know anyone in the union, so his insights are secondhand.
- The issue at Kellogg revolves around a two-tiered system where newer employees do not receive the same benefits as longer-tenured employees.
- Public support by not consuming Kellogg products is seen as significant to support the union's cause.
- Beau stresses the importance of public support for union activity and its long-term impacts on companies.
- Referencing the John Deere strike, Beau illustrates how public support can influence future consumer choices and impact companies for a prolonged period.
- Supporting organized labor activities can help prevent wages from losing buying power over time.

### Quotes

- "I don't cross a picket line. There's nothing more to that sentence. Period. Full stop. I don't do it."
- "Public support is important, it is always helpful for the union."
- "The key part here, when you're talking about it, is the company saying they're just going to hire new employees."
- "Organized labor activity is good, actually."
- "Y'all have a good day."

### Oneliner

Beau addresses the Kellogg union situation, focusing on the two-tiered system and the importance of public support for unions to maintain fair wages and benefits.

### Audience

Workers, consumers, activists

### On-the-ground actions from transcript

- Support the union by not consuming Kellogg products (implied)
- Show public support for organized labor activities (exemplified)

### Whats missing in summary

Insights on the potential long-term consequences for companies and consumers when public support for unions is lacking.

### Tags

#Kellogg #Union #LaborRights #PublicSupport #OrganizedLabor


## Transcript
Well, howdy there, internet people. It's Beau again. So today we're talking about
cereal again. We're going to talk about Kellogg. A whole bunch of questions have come in and
basically trying to get a grasp on what's going on there. The one that sums
it up the best is, hey, if anybody would know, it's you. What's going on with Kellogg?
When I talk about what the union's doing there, people are saying, hey, no, it
doesn't make sense. Those employees get paid really well and they have a decent
package. So I'm just wondering if you have any idea what's actually going on.
Aside from that, is this a situation where public support, not consuming their
product, would help? Okay. So first thing I want to point out is, yeah, in the past
I've been able to give really good insight into what unions are thinking.
That's because I can pick up the phone and call somebody in that union. I don't
know anybody in this union. So all of this is secondhand. This is playing the
union game of telephone, calling somebody who knows somebody in that union to get
a better read. I'm just saying this because I don't want to misrepresent the
workers' position by accident. But this is all to the best of my knowledge as it
was explained to me. It doesn't have anything to do with the wages that
people are pointing to. In fact, if people think those are good wages, they should
support the union. It has to do primarily, at least from what I can tell, the main
hang-up is a two-tiered system where the new employees, oh, they don't get that
package that everybody's pointing to is good. So the more experienced employees,
the employees who have been there longer, they're trying to stand up for the new
employees and make sure that they do get that package. The company is using terms,
I guess, transitory or transitional for new employees and legacy for the older
employees who get the deal everybody's pointing to. So this is a good
way for... we've all seen those charts where wages stay the same and
productivity goes up and corporate profits go up. This is how you make sure
wages don't look like that or those charts don't look like that. How wages
don't stay static or in this case even go down. So that's what's going on.
Basically, if you run into somebody who points to that package and what they're
getting paid and all that stuff and says, yeah, that's good, it is. The union just wants to make sure
everybody gets it. Everybody gets a similar deal. That's as near as I can tell.
That's the main hang-up. Okay, now as far as the next question, the public support
angle to it. I don't cross a picket line. There's nothing more to that sentence.
Period. Full stop. I don't do it. Because public support is important, it is
important for union activity. And companies seem to have forgotten how
important it is and the long-term impacts of it. Right now, if a whole bunch
of people switch from Kellogg products to something else, odds are their kids
are also going to use something else. It impacts profits for a very, very long
time. A good example of this was the John Deere strike. During the John Deere
strike, we had to get something. And more than likely, had that strike not been
going on, it would be John Deere green sitting outside. It's not. It's orange.
Because of that, the implements, stuff that gets purchased in the future, it's
going to be orange. And because of that, my kids, they're not going to have any
nostalgic feeling when it comes to John Deere. Because it's not what they grew up with.
So as far as public support and whether or not you want to do that, I mean, it's
up to you. That's a choice you make. But it's always helpful for the union. It is
always helpful for the union. The key part here, when you're talking about it,
is the company saying they're just going to hire new employees. And when
they do that, if the public stands for it, those charts where wages keep
having less buying power, they stay the same but they mean less, those charts are
going to continue to look like that. Organized labor activity is good,
actually. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}