---
title: Let's talk about China and Russia helping each other....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JVWzCT6t-E4) |
| Published | 2021/12/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the advanced foreign policy dynamics between China, Russia, and the United States.
- Points out that the drum beating and nationalistic framing are not indicative of actual intentions.
- Mentions that China and Russia do not want war with the United States due to high risks involved.
- Suggests that in hotspot situations, a power might back down to avoid conflict.
- Notes that both China and Russia have reasons to test U.S. resolve, particularly due to past administration actions.
- States that politically, China and Russia might back each other up, but limited involvement in small conflicts is expected.
- Anticipates diplomacy power moves, sanctions, espionage, covert operations, and possibly proxy wars, but a direct conflict is unlikely.
- Emphasizes the importance of mature intelligence agencies in preventing major conflicts.
- Concludes that the geopolitical posturing seen is not a desire for war but rather strategic framing.

### Quotes

- "It's geopolitical posturing. It's framing. There's not really the desire to go to war from any party."
- "They know what they're doing."
- "It's better to be up against somebody who has been there."
- "I don't see a direct contest between these nations occurring."
- "They don't really want it to occur."

### Oneliner

Beau explains the complex dynamics of advanced foreign policy between China, Russia, and the United States, indicating a lack of desire for direct conflict despite geopolitical posturing.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Monitor diplomatic relations and actions between countries (implied)
- Advocate for peaceful resolutions to international conflicts (implied)
- Support efforts to prevent escalation through diplomacy and communication (implied)

### Whats missing in summary

In-depth analysis and historical context behind the geopolitical dynamics discussed by Beau.

### Tags

#ForeignPolicy #Geopolitics #China #Russia #UnitedStates


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk a little bit more
about China, Russia, foreign policy,
near-pure contests in the future, so on and so forth.
Because I got a message that was kind of direct
with what they wanted to know.
In your video where you explained
first, second, third, and fourth world,
you seemed like you didn't really believe
that Russia and China were actually allies.
Do you think that they would back each other up
in hotspot situations like Ukraine or Taiwan?"
In a word, no, no.
The thing to keep in mind here is that this
is advanced foreign policy.
The drum beating and nationalistic framing that's going to be provided, it's just that
it's framing.
It's not real.
Neither China nor Russia wants to go to war with the United States.
The United States doesn't want to go to war with Russia or China.
They don't want to be involved in that.
It's not good for power.
too much risk involved. The hot spots that you're talking about, they're flash
points. Yeah, and theoretically sure they could spin out of control and it could
become an actual conflict. But more than likely that's not what's gonna happen.
What will more than likely happen is that somebody will blink. One of the
powers will tip their hand and allow the other one to have the winning position.
Somebody will back down.
Both powers, both China and Russia, have reason to test U.S. resolve.
last administration sold out ally after ally after ally. Our standing on the
world stage isn't great when it comes to this. So both powers may have the belief
that they have the upper hand. Now, I don't think that they would push that...
I don't think that them probing U.S. resolve would extend to the point of actually triggering
conflict because that would be a bad move.
Luckily, China and Russia, they have pretty good intelligence services and the people
in charge understand foreign policy.
So I don't think they would make that mistake.
Now as far as them backing each other up, politically, sure.
And if it ever did escalate into a real shooting war, like a real one, yeah, they'd end up
on the same side.
But I don't think that a small conflict like Taiwan, if it was limited to there, I don't
think that that would prompt the Russians to get involved.
In fact, even with all of the posturing
they're doing right now, neither China nor Russia
has endorsed the other's position.
When it comes to those hot spots,
they've expressed support and solidarity.
China said, yes, NATO shouldn't come that close.
but they didn't say that Russia should advance.
There's,
both countries left themselves some wiggle room,
so they wouldn't be committed.
If they're not willing to commit to it in rhetoric,
they're probably not willing to commit to it
when it comes to actually sending troops.
What you can probably expect
is a lot of diplomacy power moves, like sanctions.
Not going to the Olympics, stuff like that.
The same stuff we saw during the first Cold War.
A lot of espionage, a lot of covert stuff.
If it gets really hot, maybe some proxy wars.
But a direct contest between China and the United States
or Russia and the United States.
The odds of that are pretty slim.
It's theoretically possible because things can spin out of control, the fog of war and
all of that.
But what you have are mature nations that have mature intelligence agencies.
And that's good.
If one side gets the read that the other is misreading the situation and is going to advance
too far they'll probably back down. So it's a game of cat and mouse. It's not
quite brinksmanship, but it's close. That's probably what we're going to see.
Now, Putin may be willing to test Biden and see how far he can get.
China, I think, is going to be less likely to because of the trade aspects with the United States.
Putin, he might get a little reckless in advance, but even then, I don't think so.
I don't think it would prompt a conflict, I think a lot of hand-wringing and meetings
at the U.N. and so on and so forth, but I don't see a direct contest between these
nations occurring.
It doesn't really seem likely.
These aren't amateurs when you're talking about these games.
They know what they're doing.
And as weird as it may seem, this type of contest and these situations, it's better
to be up against somebody who has been there.
It's better to be up against a country that has a good intelligence agency, that has good
foreign policy, because they're less likely to make a mistake at the same time you make
a mistake and then it spins out of control.
For these countries to actually engage in a shooting conflict, it would take both sides
to make a mistake at the same time because neither side has any interest in it.
They don't really want it to occur.
It's geopolitical posturing.
It's framing.
There's not really the desire to go to war from any party.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}