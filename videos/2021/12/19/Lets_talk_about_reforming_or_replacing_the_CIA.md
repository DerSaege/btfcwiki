---
title: Let's talk about reforming or replacing the CIA....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=C9CRDYKJzA4) |
| Published | 2021/12/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Proposes a hypothetical scenario where a group of people is discussing how to address issues with the Central Intelligence Agency (CIA).
- Considers two options: replacing the CIA with a new agency or reforming the existing one.
- Explores the challenges of replacing the CIA, including the transfer of institutional memory and potential continuity of questionable activities.
- Examines the limitations of reforming the CIA, focusing on its core mission of intelligence gathering.
- Contemplates the option of not having an intelligence agency at all, but acknowledges the political reality that necessitates such an agency.
- Argues that the behavior of intelligence agencies worldwide is a symptom of deeper issues related to international affairs and foreign policy.
- Emphasizes that intelligence agencies are designed to secure the power and position of their home countries in a competitive international environment.
- Suggests that true change can only come from shifting towards a more cooperative international framework, reducing nationalism and competition.
- Concludes that any temporary fixes to intelligence agency issues are insufficient without addressing the underlying competitive nature of international relations.

### Quotes

- "The behavior of intelligence agencies all over the world is a symptom of the problem. It's not the problem itself."
- "Until nation states cease to be or that international poker game where everybody's cheating, until that changes into everybody sitting at the table playing with Legos and trying to build something together, that's what intelligence agencies are going to be."
- "Anything short of that, it's a temporary fix at best by the very definition of intelligence work."

### Oneliner

Beau weighs the challenges of replacing or reforming the CIA, ultimately arguing that true change requires shifting from competitive to cooperative international relations.

### Audience

Policy Makers, Activists, Advocates

### On-the-ground actions from transcript

- Advocate for a shift towards cooperative international relations by engaging in diplomacy and promoting collaboration (implied).
- Support policies that prioritize cooperation over competition in foreign affairs (implied).

### Whats missing in summary

Deeper insights into the historical context of intelligence agencies, the impact of US foreign policy on global affairs, and the necessity for systemic change in international relations.

### Tags

#CIA #Reform #InternationalRelations #Cooperation #ForeignPolicy


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about a unique and shadowy topic.
It's a thought exercise.
A group of people are having a hypothetical discussion.
They're talking about how to solve a problem that I think most people
watching this channel would agree exists.
And they've settled on two real options.
They want to deal with an agency within the United States government
that has a long history of, well, let's just call it
extremely questionable behavior.
They have described it as lawless and unaccountable.
They want to do something about how the Central Intelligence Agency operates.
They have settled on two options that they see as most feasible.
There are two more options that we're going to go over as well.
But the two that they have settled on, well, it's either reform it or replace it
with a new agency that doesn't have that institutional memory.
Just a brand new agency that is just going to do intelligence gathering.
You know, it's not going to have all that direct stuff.
So let's start with replacing it.
Let's take a look at that.
Okay so you snap your fingers and tomorrow that big office building you see in all the
spy movies, that's a homeless shelter.
The farm has been converted to a literal communal farm.
New intelligence agency arrives.
Who's going to staff it?
Former CIA.
Institutional memory transfers.
Okay so new rule.
Former CIA need not apply.
Who staffs it then?
People from state, other agencies within the intelligence community, military intelligence,
so on and so forth.
All of which were in some way modeled after, trained by, or amended by the CIA.
Institutional memory transfers.
Okay, new rule.
Nobody from the intelligence community in any way, shape, or form can come to work for
the new agency.
They're starting from scratch.
They're going to universities and they are recruiting PhDs who can win a bar fight just
like the old days.
Okay so institutional memory is gone from the agency itself.
But what about the policy makers?
Intelligence agencies like to provide policy makers with plausible deniability.
They like to put them in a position so they can say, oh I didn't know that was happening.
But intelligence agencies get their marching orders from the policy makers.
Those policy makers elected by people in the United States tend to be older because they're
seen as wiser and more experienced.
Part of that experience is an expectation of what intelligence agencies do and what
they're capable of.
The requests, they won't change.
They'll be the same thing.
And within one or two years, your brand new intelligence agency is just a rebranded CIA
engaged in the same kind of direct activities.
That doesn't seem like something that would work long term.
At best, let's say a new president shows up and makes that reform, makes that change right
then, switches it all over, new agency.
And let's say he does it all in a day or she does it all in a day, right?
At best you have eight years before there's a new president, new policy makers, new administration
who are going to be making those requests.
Doesn't seem tenable.
So let's go to reform.
You're going to remold the CIA and you're going to get rid of that direct activity.
That's the stuff that causes the problems, right?
That's where the lawlessness and the unaccountable behavior, that's where it comes from.
So it's just going to focus on its core mission, intelligence gathering, in producing an intelligence
product for policy makers.
It's going to get information, analyze it, and produce the product so policy makers have
the best information that they can get, right?
So write a job description for that in your head real quick.
Pretend you're hiring the people you want to do that job.
To go and just be normal spies, not the wild crazy stuff.
What's the job description?
You'd want to make it sound really patriotic.
So it would be something like looking for people who are willing to lay down their life
for their country, travel to strange and exotic lands under a fake passport using a false
identity, exploit the weaknesses in opposition counterintelligence structures, and gather
information and get back to the United States without getting caught.
That's the job.
Now let's say you're not writing it as a job description.
You're not trying to church it up.
What is it?
The whole job, quite literally, is to travel under a cover, right?
Lie.
Exploit weaknesses in the opposition's defenses.
Cheat.
Gather information that you're not supposed to have and take it and get it back to your
employer, steal, and do all of this without getting caught.
Because it's illegal.
It's espionage.
The literal job of intelligence officers in its purest, best sense is to lie, cheat, steal,
and break the law.
That's what they're trained to do.
Yeah, it's going to be lawless.
It's going to be unaccountable because they're trained to be that way.
They don't want to be held to account for their actions when they're overseas.
That happens.
You end up as a star in the hallway.
So in its best sense, you create an environment that is full of people who are trained to
break the law, to push the boundaries, to step over that line that says you can't do
this.
That's the job.
So even if you were to reform it and get it to where everybody in the agency was behaving
in that manner, how long until they step over those lines, those guidelines, whatever new
laws or regulations are put into place?
You're quite literally trained to circumvent them.
So that doesn't really seem like it would do much good either.
So what about getting rid of it and just not having one?
That's an option.
The United States didn't have a designated intelligence agency for a very, very long
time.
And while historically, sure, that's true, and the United States could devote a whole
lot more effort towards counterintelligence and could use just the military's apparatus
so it wouldn't be quite as involved, theoretically it's possible.
But what's the political reality?
As soon as something happens, some politician is going to say, you know what?
This wouldn't have happened if we had a proper intelligence agency.
It's not politically tenable, not for any length of time, because it's just going to
come back and it will start the whole process all over again.
Seems kind of hopeless, doesn't it?
It's almost like this may not actually be the real problem.
It might be a symptom, because it is.
The behavior of intelligence agencies all over the world is a symptom of the problem.
It's not the problem itself.
In the US, we focus on the activities of the CIA.
Now I would point out that there are a whole bunch of other agencies within the intelligence
community most people have never heard of.
Who knows what the NRO is?
Intelligence agencies all over the world engage in this exact behavior, the same thing the
CIA does.
And for those agencies that are in countries that are less politically active on the international
scene, typically those agencies just kind of rely on a stronger power with a more developed
intelligence agency to handle that stuff for them.
It's the nature of foreign policy.
That's what it's all about.
If you want to fix this problem, listen to intelligence officers.
Something you almost never hear from one is that we do this for American freedom.
That's not what they say.
Not most times.
We do this to protect the American way of life.
And that may not sound like a big difference, but it is.
It's huge.
It's huge.
The purpose of most intelligence agencies, once they mature, once they become an established
thing, is to secure the power of its home country.
That's its job.
It's a part of that international poker game where everybody's cheating.
Their job is to be somebody walking around the table with a mirror and maybe here or
there pick up a chip or two from another player, put it in their pocket, and return it to their
player.
It's not actually the problem.
It's the symptom.
Protect the American way of life.
As long as international affairs exists in the way it does, intelligence agencies will
exist in the way they do.
Until nation states cease to be or that international poker game where everybody's cheating, until
that changes into everybody sitting at the table playing with Legos and trying to build
something together, that's what intelligence agencies are going to be.
And they're always going to push the bounds.
They're always going to do stuff illegal.
They're always going to engage in activities that are morally wrong because they will find
a way to justify it to themselves because it's about protecting the American way of
life.
It's about securing the position of the United States in the world.
And as long as international affairs and foreign policy, as long as it is competitive instead
of cooperative, that attitude's going to exist.
By the time that attitude goes away, that would require less nationalism.
If there's less nationalism and it's not treated as a game with winners and losers
out there playing foreign policy, it would be more cooperative.
This is one of those cases where you're going to have to change thought before you're going
to see any real results.
You're not even talking about a law at this point in playing catch-up.
You're talking about an entire establishment altering the way it does things because enough
people in the United States and all over the world have decided we're going to be cooperative.
We're not going to be competitive.
We're going to try to build something instead of take stuff from each other.
That's actually your solution.
Anything short of that, it's a temporary fix at best by the very definition of intelligence
work.
Lie, cheat, steal, break the law, remain unaccountable.
That's the job.
That's what they're trained to do.
It doesn't matter if you establish a new agency with a kinder, gentler coup d'etat hand.
It will eventually always morph back into that until the game changes because they're
just a supporting actor in the game.
They're not the main player.
The main players are the people in power, those people who encourage those of us on
the bottom to be competitive, to dislike people we've never met because they were born on
the other side of the line.
The Central Intelligence Agency has a long history, but one of the things that I really
want to point out and to demonstrate that it isn't actually the agency that's the disease,
it's the symptom.
If you go back and watch those videos about why Central America got so disrupted and why
we have so many migrants coming from there, it all has to do with US foreign policy, with
the activities that the US engaged in down there that disrupted things.
The same type of stuff that people talk about today as being the questionable activities
of the CIA.
It's important to note that all of those timelines, every single one of them, began before the
CIA was ever formed.
Don't get me wrong, this isn't a defense of the CIA's activities in any way.
It's an explanation of why they occur.
If you want to fix the problem, you have to know what it is.
And it's not really the fact that there's an intelligence agency.
The fact that we have a system that is competitive on the international scene rather than cooperative.
That requires the tool of an intelligence agency.
And because of the job of intelligence officers, they will always go outside the lines.
They will always break the rules.
They will always go further than they're allowed to go because we literally trained them to
do it.
If you want to solve this problem, you have to change the world.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}