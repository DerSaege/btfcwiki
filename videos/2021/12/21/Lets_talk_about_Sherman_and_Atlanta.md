---
title: Let's talk about Sherman and Atlanta....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SB0xZWyQHkE) |
| Published | 2021/12/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- Debunks the inaccurate beliefs surrounding General Sherman's infamous march.
- General Sherman's actions were not as commonly portrayed; the reality is different.
- Sherman's march wasn't just about burning everything in his path.
- September 2nd marked the fall of Atlanta, leading to Sherman's strategic decisions.
- Sherman divided his troops and executed a plan to sustain his campaign.
- Atlanta was partially destroyed before the march to the sea commenced.
- The march involved battles and strategic acquisition of supplies.
- Sherman's aim was to disrupt morale in the South.
- Many of the destructive actions were carried out by Confederate troops, not solely Sherman's army.
- The liberation of slaves was a part of Sherman's campaign, with complex consequences.
- Special Field Order Number 15 granted land to refugees following Sherman.
- Historians debate whether Sherman's actions constitute total war.
- The accuracy of historical narratives is often distorted by immediate spin and political agendas.
- Civil conflicts have severe consequences, especially for civilians.
- The transcript coincides with the historical end date of Sherman's campaign.

### Quotes
- "Almost immediately spin comes into it, and it obscures the facts."
- "The accuracy of historical figures' stories is normally not accurate."
- "If you're going to get to the bottom of something, you need to do it quickly."
- "At the end of it, it's normally the civilians that pay the heaviest price."
- "The other reason I'm bringing this up today is because it happened today."

### Oneliner
Beau debunks common myths surrounding General Sherman's march, showcasing the complex reality behind historical narratives and the heavy toll of civil conflict.

### Audience
History buffs, educators, Civil War enthusiasts

### On-the-ground actions from transcript
- Research and learn about historical events from multiple perspectives (suggested)
- Support accurate historical education and narratives in schools and communities (exemplified)
- Engage in civil discourse about complex historical figures and events (implied)

### Whats missing in summary
Beau's detailed breakdown offers a nuanced view of historical events often oversimplified, urging quick pursuit of truth before narratives are distorted.

### Tags
#CivilWar #HistoricalNarratives #GeneralSherman #CivilConflict #DebunkingMyths


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
Sherman. A guy named Sherman. You've probably seen the memes, don't make me
come down there again, type of stuff. So we're just going to kind of go over the
story because I overheard a conversation and then me being me I started talking
to people and asking what they knew and the general consensus of Sherman's march
is less than accurate. What people think happened isn't really what went down and
I don't know if they're huge material differences but it might be worth
examining at this point. Most people seem to believe that General Sherman was
sitting up north and he's like you know what I'm gonna go down south and I'm
just gonna break the back of the Confederacy. I'm gonna burn everything.
And he went to Atlanta and torched it and then he went to Savannah and torched
it, torched everything along the way. And the reality is pretty much none of that
is what actually went down. So what did go down? What happened?
September 2nd, Atlanta fell and Sherman's there. This isn't even part of the
march to the sea. This is part of the Atlanta campaign and he is standing in
Atlanta. He's got about 120,000 troops and he knows he can't really hold it, not
for any length of time, but he's also not giving it back. So he doesn't know what
to do. He wants to wait it out but he has two problems. One is named Hood and the
other is a fellow named Nathan Bedford Forrest. Yeah that one. And they're
nipping at his supply lines and if they break them, he's done. So in October he
comes up with a plan and he tells his troops to start gathering supplies. They
don't really know why yet, but they start gathering stuff up and he decides to
send half of his troops north, 60,000 of them. They go up to Nashville. The other
60,000, well, they're going to go on a 285 mile journey on foot to Savannah. They're
going to take it. But he still has the problem of what to do with Atlanta. So he has
battering rams constructed to destroy the industrial section and then he does
in fact torch the business districts. Destroys about 40 to 50 percent of the
city. But this isn't part of the march to the sea. This is right before it. Atlanta
was already taken when he had conceived of his little idea. So his 62,000 men on
November 15th, they leave Atlanta and it is a smoldering ruin behind them. But
keep in mind, they didn't really go after the residential areas. That wasn't the
plan. That idea, that image that gets thrown out there, not accurate. And
Atlanta wasn't even part of it. That was just before it. So they start marching
and they have battles along the way. There's Balls Ferry, Honey Hill,
Waynesboro, Fort Mitchell, McAllister, Fort McAllister, and a really important
one called Griswoldville, which we'll come back to. His overall strategy at
this point is to get food and livestock along the way. Supply his army from the
land around him. This way he doesn't have to worry about a supply line. They didn't
intend necessarily on torching everything they came in contact with. In
fact, if you didn't put up a fight, they probably weren't going to. If you did
fight back, you were losing your house and your barn. He said that he thought
that rich and poor, young and old, needed to feel the hard hand of war. He wanted
to degrade morale in the South, and it definitely worked. But a lot of what
happened, at least some of it, wasn't even him. At Griswoldville, he ran into some
Confederate troops and beat them. And the Cav units, well, they wrote off. They were
like, no, not dealing with this. We need to get away from this guy. And they went
in the route he was headed, ahead of him. And as they traveled, they realized they
were passing barns with food and livestock and supplies. And they torched
them, so he couldn't get them. A lot of what was done, particularly in this area,
wasn't even him. It was Confederate troops doing it to the Confederacy. But
as time went on, and even in the immediate aftermath, I mean, that's not a
good story. You can't paint Sherman as this evil person if your own troops
were doing it, right? So you had to deny that happened, pin what they did on your
opposition. And that's what they did. And it worked for the most part. So they move
along, and along the way, they free slaves. That's true. That part of the
story is accurate, but there's some stuff that's left out there. Slaves had mixed
opinions when he arrived. They didn't know what to do. His men were wild at
this point. They didn't know if they should run away from him or run towards
him. Some ran towards him, and they followed his march. There were thousands
upon thousands, tens of thousands, of newly liberated former slaves following.
And hundreds were lost to hunger, exposure, disease. At a place called
Ebenezer Creek, hundreds were lost to the creek. He wasn't quite the caring,
liberating man that he gets painted out to be. Then there's also the issue of
special field order number 15, which he gave after he took Savannah. When he got
to Savannah, by the way, he didn't torch it. He showed up and he was like, hey, I'm
ready to give you pretty good terms to surrender, be very liberal, very fair with
all of this, or, I mean, you can still see the smoke from Atlanta, your choice. And
if I'm not mistaken, he took the city without a shot. But he still had all of
these refugees following him. So he needed a place to put them, and that is where special field
order 15 comes in. And he basically gave them all land, said they could have land, they
could settle. There's no mention of a mule in this, in his order, but the idea
of 40 acres and a mule, it comes from there. It never materialized. Now that's
not really his fault, to be honest. Johnson, President Johnson, kind of
overrode all that. At the end of this, you read the story and you look at him and
what he did, and one of the interesting things about it is he probably wouldn't
like those memes, because he didn't really enjoy what he was doing. He just
felt it needed to be done. Now, as far as debating whether or not what he did was
right, you're in the best of company. Among historians, they argue over whether
to classify this as total war or just really, really hard war. Total war is
pretty indiscriminate. It's where the forces view the civilians, industry, in
general, as all supporting the war effort. So, they're all legitimate targets.
Generally speaking, we don't condone total war. In this case, given the moral
implications of the war, I think people might be excused for condoning it, but
it's a debate that occurs. At the end of this, there's a couple of key points. One
is that the stories that we have of historical figures, they're normally not
accurate, because almost immediately, just like after Griswoldville, almost
immediately spin comes into it, and it obscures the facts. That's why, if you're
going to get to the bottom of something, you need to do it quickly. You can't wait
for the political machinery to create other narratives. The longer you wait, the
more obscure it becomes. The harder it is to get to the truth. So, there's that
element to it. And then the other element is, you have a lot of people today who
seem enamored with the idea of civil conflict. The one thing that is true
about every civil conflict is that at the end of it, it's normally the
civilians that pay the heaviest price, those who aren't involved. If you are
really somebody who views yourself as a patriot, as somebody who loves the
country, would you really want that? The other reason I'm bringing this up today
is because it happened today. Well, depending on the historian, it
ended December 21st or December 22nd, 1864. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}