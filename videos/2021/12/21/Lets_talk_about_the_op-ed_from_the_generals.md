---
title: Let's talk about the op-ed from the generals....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VUTuBI3j2pg) |
| Published | 2021/12/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Three generals penned an op-ed expressing concerns about the 2024 elections and the potential involvement of elements within the U.S. military in supporting a losing candidate who refuses to concede.
- The generals' concerns are based on the fear that some military units may go rogue and not respect the election outcome.
- They point to statistics suggesting that 10% of the individuals involved in the Capitol incident were service-connected, but Beau questions the significance of this figure.
- Beau argues that the demographic of veterans among the general population is not significantly higher than expected, downplaying the relevance of the mentioned stat.
- Beau is not alarmed by the support of some former military officials for Trump, as most of them lack significant influence over current military institutions.
- The idea of lower-level officers leading their units rogue is seen as a legitimate concern by Beau, but he does not believe it should raise major alarms.
- The generals suggest various actions to mitigate their concerns, including holding leaders of the Capitol incident accountable, providing constitutional training, and conducting more counterintelligence checks within the military.
- Beau agrees with the suggested actions, viewing them as necessary regardless of the specific concerns raised.
- He expresses more concern about non-military organizations supporting a potential insurrection rather than military involvement.
- Beau believes the generals may be exaggerating their concerns to push for necessary changes within the Department of Defense (DOD) and suggests that monitoring the situation is prudent but not cause for immediate panic.

### Quotes

- "They got the math wrong, but they got the right answer."
- "This isn't something I would be too concerned about at the moment."
- "Their suggestions are really good, all of them."
- "DOD is typically an organization that does not change until it fails."
- "Y'all have a good day."

### Oneliner

Beau breaks down concerns from generals' op-ed on 2024 elections, downplaying military involvement fears but supporting suggested actions for transparency and accountability.

### Audience

Political analysts

### On-the-ground actions from transcript

- Hold leaders accountable for incidents like the Capitol breach and push for Department of Justice action (suggested).
- Implement ongoing training on the Constitution, civic duty, and refusal of illegal orders within the military (suggested).
- Conduct more counterintelligence checks on military personnel to ensure transparency and mitigate risks (suggested).
- Combat disinformation within military ranks through proactive measures (suggested).
- Advocate for war gaming insurrections and coups after thorough counterintelligence checks (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the concerns raised by three generals in their op-ed regarding potential military involvement in the 2024 elections and Beau's nuanced perspective on the significance of these concerns.

### Tags

#Military #ElectionConcerns #OpinionPiece #DepartmentOfDefense #Accountability


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about that op-ed.
Three generals wrote an op-ed expressing their concerns about the 2024 elections.
And we get tons, tons of questions about it.
Rather than go through and answer the questions,
we're talking about how to look at something like this,
how to look at an op-ed like this, and figure out how much weight you should put behind it.
The key part when you're looking at something like this is to break it apart into two sections.
Reasons for the concerns and the suggestions.
How they are saying the problem could be remedied.
Their problem, as they've identified it, is that they're worried elements within the U.S. military
might back a losing candidate who refuses to concede the election.
That some units may go rogue. Right?
Okay. So what are their reasons?
What prompts them to believe that this is something worth talking about?
One of the things they point to is the stat that says one in 10 of those people
who were busted up at the Capitol on the 6th, well, they're service-connected,
meaning they're veterans or active duty. One in 10. So 10%.
Okay. Yeah, that seems like it would be a high number.
But if you were to pull a thousand adults at random out of your local Walmart,
what would their veteran status be?
More than likely just under 8%. Just the veterans.
Then they're going to get a little bump for the active duty.
That's not far outside of what you would expect from a normal group.
That's just about the demographics of it. It makes sense.
And that's not even factoring in the rest of it.
So I don't see this as a concern without even getting into the fact that, well,
many of the people who are going to be busted in the future, who haven't been caught yet,
most of them will not be service-connected.
Why? Because the service-connected people got caught pretty quickly
because being the super operators that they were,
the one time in history where everybody's supposed to wear a mask,
they didn't and they forgot that DOD has photos of them in a database from their ID card.
They got identified relatively quickly.
So you're probably not going to find a whole bunch more out of those who haven't been caught yet
that are service-connected, which would bring that percentage down.
Aside from that, you're talking about a group of people who self-selects for veterans.
You're also talking about an event that self-selects for people who are comfortable with violence.
And then you're talking about an event when you're talking about going inside
that self-selects for people who would charge ahead.
I'm actually surprised the percentage is that low, to be honest.
I would have thought it would have been a little higher.
So I don't see that as a concern.
I don't see that as a reason for their concerns.
The next thing they point to is the flag officers for America,
which was like 120-something former military people who put together a letter backing Trump.
Yeah, OK, I get it.
They're brass.
However, things to note, there is one four-star on that list.
One.
Most of them are not officers who could raise the division with a tweet.
They really aren't.
A lot of them retired in the 80s or 90s.
They don't have sway over the institutions anymore.
If you look to why I had issues with Austin's appointment,
it wasn't because I didn't think he could do his job.
It was because he had just recently separated from the military,
which means he still has a lot of people who he may have mentored in those positions.
And that's a risk.
The people on this list, not so much.
When the flag officers for America put out their letter,
Defense One, which is an outlet that covers military affairs,
they ran an article, and I want to say it was actually called,
Why Should We Care About This Letter?
Or Should We Care About This Letter?
Something like that.
And they were pretty spot on in it.
Not a huge risk.
If you look at the people who have the level of respect necessary to get troops
and the skill set necessary to actually pull off a coup,
all of them who spoke out,
they were very much in favor of maintaining the constitutional process.
I think that trend will continue.
This, not really a concern for me.
I don't think this is reason.
This is cause to be alarmed.
Then they pointed to the idea of lower level officers
maybe taking their units rogue.
That's a concern.
That's something that could legitimately happen.
And we talked about it.
I think we actually talked about it before the 6th occurred.
So you have one, but that's always going to be that way.
So as far as raising a huge alarm, I don't see it.
But now let's go to their suggestions.
What they think should happen to mitigate these concerns.
They really have kind of five things.
The first is that the leaders of the 6th should be held accountable
and DOJ needs to get on the ball.
Well, yeah.
Okay.
The next is that the Pentagon should provide training
on the Constitution, civic duty, and how to refuse an illegal order.
Yeah.
Yeah, that's a great idea.
Let's do that too.
This seems like something that should really be ongoing education.
The third one, more counterintelligence reviews of everybody in the military.
They don't call them counterintelligence reviews.
They said soldier reviews or something like that.
Yeah, that should totally happen.
That's a good idea.
Four, curbing disinformation within the ranks.
Yeah, that should also happen.
The fifth one, DOD should war game insurrections and coups.
Maybe.
If the Pentagon follows through with the counterintelligence reviews first,
then yeah, that's probably a good idea too,
but not until the CI reviews are done.
You know, one of the concerns I have about the drills that occur at schools
is that the bad guy is sitting in the room.
You don't want to create the same situation here.
You don't want to allow a would-be insurrectionist,
somebody who would plot a coup, to be able to use that war game to plan.
So you would have to do the counterintelligence reviews first.
And that's it.
Those are their suggestions.
Regardless of their reasoning,
these are all things that should be happening anyway, to be perfectly honest.
This is one of those situations where to me,
they got the math wrong, but they got the right answer.
I'm not concerned about this aspect of it.
I am far more concerned about organizations that would be loyal to a,
quote, Trumpian figure from their op-ed, that weren't related to the military.
That would be much more of a concern for me.
So I don't think that the alarm that is being generated by this op-ed
needs to be generated.
At the same time, their suggestions are really good, all of them.
Maybe these are three older, wiser generals who know that
the only time you can get DOD to do anything is if something goes wrong,
or you scare them.
So there is a part of me that believes that maybe they're intentionally
exaggerating their reasoning because they know that their solutions are good ideas.
And I wouldn't hold that against them,
because DOD is typically an organization that does not change until it fails.
So, but for people at home, I wouldn't worry about this.
This isn't something I would be too concerned about at the moment.
Is it something to watch? Sure.
But I wouldn't start panicking over this.
I would just keep an eye on it.
The odds are those people who have the skill set necessary to pull something like this off,
and who have the respect of the troops, those who could tweet out,
hey, we're meeting here, bring your kit, and have people show up,
they wouldn't do something like this.
As the military stands today, that could also change.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}