---
title: Let's talk about not talking about race....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gkq8PjQUnYg) |
| Published | 2021/12/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received a message from a soldier claiming that if we stopped talking about racism, it will be eliminated in America.
- Soldier is Hispanic and believes that not acknowledging racism will make it disappear.
- Explains the individual versus systemic impact of not talking about racism.
- Suggests using statistics often favored by racists to demonstrate systemic racism.
- Points out the need to address institutional issues that contribute to disproportionate crime rates among certain groups.
- Emphasizes the importance of acknowledging societal pressures that create unequal outcomes.
- Recommends discussing and addressing institutional racism before considering stopping the discourse on race.
- Mentions the popular Morgan Freeman meme that suggests stopping talking about race as a solution to racism.
- Urges for leveling the playing field before ceasing to address race issues.
- Encourages engagement in difficult but necessary dialogues to combat racism effectively.

### Quotes

- "How do you demonstrate systemic racism to somebody? Use the statistics that racists use."
- "I've actually never had that not work. That's always been pretty successful."
- "We can't stop talking about it until the playing field's level."

### Oneliner

A soldier's belief that ignoring racism will eliminate it prompts Beau to explain the importance of addressing systemic issues through familiar statistics favored by racists.

### Audience

Activists, Educators, Allies

### On-the-ground actions from transcript

- Challenge misconceptions by using statistics to demonstrate systemic racism (implied)
- Engage in dialogues that address institutional issues contributing to racial disparities (implied)

### Whats missing in summary

Beau's thoughtful insights on using familiar statistics to illustrate systemic racism and the necessity of addressing institutional issues before considering ending race-related discourse.

### Tags

#Racism #SystemicRacism #InstitutionalIssues #CommunityEngagement #Dialogues


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about what would happen
if we stopped talking about it.
I got a message, and it points to a common belief
that a lot of people probably run into,
and it's actually way easier to overcome
than you might imagine.
So we're going to go through it.
OK.
I was told by another soldier today that racism isn't real.
And if we simply never talk about it or acknowledge it,
then racism in America will be eliminated.
So for context, this is not a white soldier,
nor is he black.
He is Hispanic, but unless you asked him,
you would assume he is white.
He's an officer like me, and he's educated.
Goes on to say that he tried to respond,
but couldn't really hammer it home.
So if we stop talking about it, it'll go away.
It's that Morgan Freeman meme, right?
You know, how do we fix racism?
We stop talking about it, that meme.
OK.
So first, is there any truth to this?
If we all just came together and said,
hey, we're never going to talk about racism again,
we're never going to talk about race again, would it go away?
I mean, on the individual level, yeah.
Yeah.
If everybody stopped talking about it,
on the individual level, it would go away.
It would turn into, it would be like the way
white people look at hair color.
You notice it, but there's no huge preconceived notions.
So on some level, on the individual level,
this would work.
But there's a huge assumption going on here.
What about the systemic stuff?
What about the things that are wrong?
Systemic and institutionalized racism.
What about that?
If we stop talking about it, well, it never gets fixed.
Right?
The problem is, if you have somebody
who is saying something like this,
just stop talking about it.
They probably don't believe institutional racism exists.
They don't believe systemic racism exists.
Right?
So how do you demonstrate it?
What is the easiest way to demonstrate systemic racism
to somebody?
Use the statistics that racists use.
For real.
There's crime stats they always love to point to.
Use those for a number of reasons.
One, they're readily available because they've
plastered them everywhere.
Because they've been plastered everywhere,
everybody's familiar with them.
They know it.
They won't be shocked by these numbers.
Right?
So all you have to do is get them
to think about information that they're already
comfortable with.
You're not introducing anything new.
So these statistics say 13% of people
commit whatever amount of crime.
I don't remember what it is.
But you point to those.
13% of people, they're responsible
for a disproportionate amount of crime.
Now, you have options.
Why?
Why is this statistic true?
Why does this exist?
Right?
Is there a causal link between skin tone
and whether or not you're likely to commit a crime?
Ask them that.
Odds are they're going to say no.
And if they say yes, well, congratulations.
You demonstrated racism in another way.
There is no causal link between skin tone and likelihood
of committing a crime.
That's not a thing.
Skin tone doesn't make you commit crimes.
Right?
That's silly.
So then what could it be?
Could it be higher incidence of poverty,
being more surrounded by income inequality,
less access to opportunities, less access to education,
more enforcement from law enforcement,
driving those crime rates up, all the things that actually
do have a causal link, which are systemic issues.
They're institutional issues.
Those stats, those are the easiest way to show it.
Those are the easiest way to show it.
So you get them to acknowledge that there are institutional,
there are society-wide things that apply pressure
that create this outcome.
And because it is delivered more heavily on a specific race,
it is institutional racism.
That's the easiest way to demonstrate it.
I've actually never had that not work.
That's always been pretty successful.
So once you get them there, you can get them to the point of,
well, we can't stop talking about it
until the playing field's level.
Then, sure, we'll give your idea a shot.
I mean, it can't hurt, right?
So once we get to the point where
we've gotten rid of the institutionally racist policies,
then we can stop talking about race.
That's where I would go with it.
I would also point something out,
because most people really did get this idea from that Morgan
Freeman meme.
And I would point out that I've never actually,
I never looked into this.
I have no idea if he actually said this,
or somebody just put this quote on a picture of him.
How do we stop racism?
Stop talking about it.
I'm going to stop calling you a white man,
and I'm going to ask you to stop calling me a black man.
Even in the meme, until you're on level footing,
you talk about it.
That's where I would go.
That seems to be the easiest way to address it.
Now, whether or not you want to take the time
to try to explain this to the soldier,
that's entirely up to you.
But it's not, I don't think that this would be a hard battle
to win.
Anyway, it's just a thought.
You guys have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}