---
title: Let's talk about a letter from Texas.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sl2qYasoGg0) |
| Published | 2021/12/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A young person in Texas is facing difficulties due to their liberal ideology in a red state, where they get picked on and called a communist for wearing a mask.
- Beau explains the difference between being liberal and communist, clarifying that communism falls on the left-right spectrum, not the liberal-conservative spectrum.
- He mentions that being liberal does not equate to being communist, nor does encouraging mask-wearing.
- Beau addresses the conflation of liberal and leftist beliefs, mentioning that leftist ideologies reject capitalism.
- He refers to a video by Mexie to explain the distinction between liberal and leftist ideologies further.
- Beau suggests that public health, like wearing masks, should transcend politics and be based on common sense and necessity.
- He advises the young person to meet others where they are when discussing the importance of mask-wearing, even referring to biblical references to support the practice.
- Beau touches on the Overton Window concept, explaining how the acceptable range of discourse in the US can shift over time, leading to certain ideas being labeled as communist.
- He underscores that the founding fathers were liberals and advocates for embracing liberalism when learning about the Constitution and US history.
- Beau encourages forming independent ideologies and opinions, applauding the young person for developing their own beliefs rather than conforming.

### Quotes

- "Is being liberal and encouraging people to wear a mask really communist, like those around me are saying?"
- "There's a thing called the Overton Window."
- "If you're feeling isolated in the way you believe, what that means is that you're not being led around."
- "I know what I believe, but I also know that I've been wrong about stuff before."
- "It may be frustrating, it may be depressing, but for what it's worth, I think you're doing a good job."

### Oneliner

A Texan facing backlash for liberal beliefs learns from Beau about the distinction between liberalism and communism, the Overton Window concept, and the importance of forming independent ideologies.

### Audience

Young adults in conservative environments.

### On-the-ground actions from transcript

- Share educational resources on the differences between ideologies with those who may misunderstand them (implied).
- Engage in respectful and informative dialogues with others to explain the importance of issues like mask-wearing (implied).
- Encourage critical thinking and independent ideology formation among peers (implied).

### Whats missing in summary

The full transcript provides a comprehensive understanding of ideological differences, historical contexts, and personal growth insights that are best explored through the complete reading.

### Tags

#Ideology #Liberalism #Communism #MaskWearing #OvertonWindow #IndependentThinking


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about a letter I received.
And by a letter, I mean a letter, paper in the mail.
It's from a young person in Texas
who is running into difficulties just being because
of the climate around them.
It has three main questions in it,
and we're going to go through them all.
So there's the introduction, and then it
kind of starts off in a relevant part with,
I need to know something.
Do you consider yourself liberal, conservative,
or neutral?
I'm only asking because I'm a blue dot in a red state.
When I go to school, I get picked on for wearing a mask,
and I get called a communist.
I'm very liberal and can relate to your ideas and views.
I just feel lonely in the sense of ideology
because I'm in a very red state, and watching your videos
helps me ensure I'm not insane.
There are a lot of people in my state
who watch right-wing media outlets
and constantly attack me for not feeding into their rhetoric.
It frustrates and depresses me so much to the point
where I second guess myself.
Is being liberal and encouraging people
to wear a mask really communist, like those around me are saying?
I also like your video where you explain the First Amendment
because I made one of my friends watch it so he could
show his Republican parents.
Now his parents told him to stop being my friend
because I'm making him accept liberalism and communist ideas.
OK, so you have three main questions.
The first is the obvious one, liberal, conservative, neutral.
It feeds into something else, though.
See, in the United States, we like to conflate.
We confuse the term liberal and left, conservative and right.
That's not really the same.
Those aren't the same terms.
See, communism doesn't exist on the liberal conservative
spectrum.
That's not where it is.
It's on the left-right spectrum, and we'll
get to that in a second.
When it comes to liberal conservative,
that's relative to who's around you, to what's around you.
Where I'm standing at right now, there is no doubt in my mind
I am the most liberal person in a 50-mile radius.
Yeah.
However, if you were to pick me up and drop me
into the middle of a lot of countries in Europe,
I'm suddenly a moderate.
I'm neutral.
So it's relative to what's around you.
Living in Texas, it's a pretty conservative state.
So even if you're just slightly liberal,
you're going to feel a little more out of place.
But that's OK.
There's actually nothing wrong with that.
Just, I mean, it's a byproduct of the way
the country is growing.
OK, now as far as liberal leading to communism, no.
They're not the same thing.
You have that spectrum, liberal conservative,
and then you have left and right.
The general dividing line, as seen by most people,
crossing into left territory, leftism,
which is where communism exists, you
reject capitalism, the current economic system
that we have in the United States.
That's what it takes to cross the line into actual leftism.
I have a video that I will put down below.
It's a conversation between me and a woman named Mexie.
She's had a PhD in political economy.
Nobody explains this better than she does.
And for the record, she's a Marxist.
She is definitely left.
I mean, in the US, because we conflate liberal and leftist,
people then take liberal and say, oh, well,
you're a communist.
Doesn't have anything to do with that.
Nancy Pelosi is a liberal.
She is definitely not a leftist.
She embraces capitalism.
She doesn't cross that line.
If you ask most leftists about Nancy Pelosi,
they dislike her more than most Republicans.
It's not the same.
So is being liberal communist?
No, no, not at all.
Is wearing a mask communist?
That's another part of that second question.
No, of course not.
Communism is a political and economic theory.
Has nothing to do with public health,
except maybe in the philosophical sense of,
you know, from each according to their ability
to each according to their need.
Most people have the ability to wear a mask,
therefore they should.
So maybe they mean it like that, but I doubt it,
because that would require them having actually understood
communism.
You could make that same argument,
in fact, I have with the US Constitution.
Provide for the common defense.
We're all supposed to wear masks.
Contrary to what's happened since the beginning of this,
public health isn't the political issue.
It should exist outside of politics.
It doesn't, because some people would try to use it
for political gain.
Now, you have that term communist being thrown at you.
From what it sounds like, it sounds like the way
when I was growing up, people would say, oh, that's gay.
Has nothing to do with communism,
just like it didn't have anything to do
with being gay back then.
It's just an insult used by people
who don't understand something.
I wouldn't put a lot of stock in it.
Now, if you're wanting to talk about masks
with the people around you,
I would remember that you have to meet people where they are.
You could point them to studies about how effective masks are.
You could point them to the long history of them being used.
You could talk about all kind of medical stuff.
Or, remember you're in Texas,
rather than pulling out medical textbooks,
I'd pull out the Bible.
Leviticus 13, chapter 13, 45 and 46.
It might be chapter 16, 45 and 46.
I can't remember.
I would direct them there and ask them
if the Bible is communist.
Because if you don't know, in the Bible,
it talks about how if you have, I think it was leprosy,
you should leave your hair unkempt, wear torn clothes,
and cover the bottom half of your face.
Wear a mask.
Wearing a mask is biblical.
So, that's how I would address that if you wanted to.
Now, there's also a part of me that's just like,
yeah, ignore these people.
My guess, based on this letter,
is that you're probably going to college out of state.
It's going to be my guess.
But, if you want to engage, there's a tool for you.
My favorite part in this, though,
is this bit about the First Amendment.
The video about the First Amendment
causing your friend to accept liberalism and communist ideas.
That shows you how far right we have gone in the United States.
There's a thing called the Overton Window.
And the Overton Window is what is the acceptable range
of discussion in the United States.
And this window can be pushed left or right or up or down
or however.
And the stuff inside that window,
well, it's okay to talk about.
But what happens is, if, as has happened
over the last few years,
that window gets pushed really far to one way,
things that are normal,
things that are in fact the founding principles
of the country, start to fall outside of that window.
And then they get labeled communist.
The founding fathers of the United States
were in fact liberals, period, full stop.
You cannot argue that, that they were liberals.
So if you want to learn about the Constitution,
you want to learn about the people who founded this country,
yes, you are going to embrace liberalism.
As far as it being communist,
I mean, that's entirely something else,
again, because it doesn't exist on that spectrum.
That being said, along with the video from Mexie
that I'll put down below,
I'm going to put down a video from the other channel
that I did on a person named Thomas Paine.
You probably studied him in history class.
He wrote Common Sense,
the spiritual and philosophical founder
of the United States.
This is the person who told the colonists
that they were actually fighting a revolution,
not just rebelling and trying to get more rights.
This is without the pen of Thomas Paine,
Washington's sword would have been willed in vain.
Paine, he kind of was a leftist.
The terminology as we use it today didn't exist back then
as far as rejecting capitalism and all of that stuff.
But if you actually sit down and look at what he wrote,
he was definitely left.
So, yes, there are a lot of left ideas
in the founding of this country
because it was founded by a bunch of liberals
and kind of led and steered by somebody
who embraced a lot of forward-thinking, progressive ideas
that fell on the left side of the political spectrum.
So, I hope that actually answers the questions.
Now, as far as feeling alone in your ideology, that's okay.
Yeah, I know it may not seem okay, but that's okay.
If you feel isolated in the way you believe,
if you feel like those around you
do not believe the way you do,
what that means is that you're not being led around.
You're forming your own opinions.
You're developing your own ideology.
You are becoming your own person
rather than just another cog in the wheel.
I think that's good.
I would be proud of my kids for doing that.
Now, as far as the question of whether I'm liberal,
conservative, my ideology, it is a long-running thing
on this channel. I actually don't say.
The reason I haven't been able to find it
is because I don't say what I am,
and it's because I think one of the most important things
is for people to do what you're doing,
for people to form their own ideas,
to develop their own ideas of what is right and good and true.
I know what I believe,
but I also know that I've been wrong about stuff before.
It would be better if everybody came up
with their own opinions,
and then we kind of worked through them
rather than having a bunch of people on right-wing TV
tell them what to think.
I know it may be frustrating.
It may be depressing, but for what it's worth,
I think you're doing a good job.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}