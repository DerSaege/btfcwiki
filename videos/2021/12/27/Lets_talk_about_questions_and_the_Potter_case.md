---
title: Let's talk about questions and the Potter case....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dwYI6tdNrNc) |
| Published | 2021/12/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring two concepts arising after the Potter verdict and its fallout regarding the future steps and developments.
- The first concept is whether the verdict will lead to more aggressive prosecution of law enforcement.
- Lawyers may be more likely to take on cases against law enforcement if a strong case can be made.
- A possible outcome is that more officers may opt to take plea deals.
- The second concept questions whether the verdict will prompt law enforcement to change their ways.
- While lessons may be learned, the optimism around significant change is not shared by Beau.
- Lessons should focus on addressing the use of force, over-reliance on force, and moving towards consent-based policing.
- Beau believes that the main lesson some may take is to not admit wrongdoing on camera.
- Beau doubts that changing this behavior will significantly impact future prosecutions.
- The idea that doctors don't face charges for mistakes is debunked, as they can be charged with manslaughter.
- Comparisons between medical professionals and law enforcement accountability are often misleading.
- The concept of removing qualified immunity to hold officers accountable through civil processes is discussed.
- The argument that officers should be held personally accountable through civil processes is deemed rare and unlikely.
- Beau questions if the Potter verdict will lead to more aggressive prosecution or just plea deals for severe cases.
- Speculating the future actions of prosecutors based on one incident is seen as unreliable.
- Beau concludes by expressing doubt that this case alone will bring about significant change in law enforcement behavior.

### Quotes

- "More officers may opt to take plea deals."
- "The optimism around significant change is not shared by Beau."
- "Comparisons between medical professionals and law enforcement accountability are often misleading."
- "Removing qualified immunity for officers is discussed."
- "Speculating the future actions of prosecutors based on one incident is seen as unreliable."

### Oneliner

Beau questions the impact of the Potter verdict on law enforcement behavior, expressing doubt about significant change and discussing accountability comparisons with medical professionals.

### Audience

Reform advocates, legal professionals.

### On-the-ground actions from transcript

- Advocate for reforms to address over-reliance on force and move towards consent-based policing (implied).
- Support efforts to remove qualified immunity for law enforcement officers (implied).

### Whats missing in summary

The emotional impact on communities and the need for ongoing advocacy and accountability measures are best understood by watching the full transcript.

### Tags

#Accountability #LawEnforcement #QualifiedImmunity #Reform #PleaDeals


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about two questions,
two things that have arisen, two ideas, concepts,
whatever you want to call them,
that have popped up after the Potter verdict
and the fallout from that.
And as people try to figure out where things go from here
and what the next steps are,
what the next developments are going to be.
There are two takeaways
that are getting a whole lot of discussion.
We're just going to kind of go through them real quick.
One is that this verdict is going to lead
to more aggressive prosecution of law enforcement
and that it's going to lead law enforcement
to change their ways
because this seemed to have been a very fair prosecution,
charged correctly, everything done by the numbers,
and they got a guilty verdict against a cop.
So let's kind of run through it.
Is this going to lead to more aggressive prosecution
of law enforcement?
Maybe, maybe.
You know, lawyers don't like to lose.
So if for a long time a certain demographic
has been held unaccountable,
they tend not to pick up the case unless it's really strong.
This does show that the minds of juries
are changing a little bit,
but it also shows this to defense attorneys.
So what you may see are more officers
willing to take a plea deal.
I think that's probably going to be
your most likely outcome from this.
Now, the other idea is that it's going to get better
because this is going to show law enforcement
that they need to change their ways.
There are going to be lessons learned.
And there will be.
However, I am nowhere near as optimistic
as a lot of people who are making this claim.
The lessons learned have to do with, you know,
ensuring that officers don't make errors,
don't make mistakes when it comes to the use of force.
The idea that just because you're frustrated
doesn't actually warrant an escalation in the use of force.
The addressing the over-reliance on the use of force overall,
a move towards consent-based policing.
These should be the lessons learned.
That's not going to be the lesson learned, though.
More than likely, I'm sure there are already trainers now
who have their takeaway from it.
And the takeaway is do not admit on camera
that you were wrong after you made a mistake.
That's what they're going to take away from it.
They're going to pin it all on that.
Now, the reality is that isn't going to change
the outcome of future prosecutions.
I really don't think it's going to matter that much.
But I think before law enforcement gets that idea
and understands that,
there's going to be more officers convicted.
Then the next one is coming from an entirely different way.
It's coming from those who would back the blue.
And the idea is, you know, doctors, they make mistakes
and they don't get charged with manslaughter.
What's that about?
Well, first, that's not true.
They totally do.
That happens.
Medical professionals that are negligent
go against best practices, so on and so forth.
If it is severe enough,
they will, in fact, be charged with manslaughter.
That's true.
That happens.
The flip side to that is they can be held accountable
through the civil process,
lawsuits and so on and so forth.
Individual officers, really,
it's really hard to do that.
Now, the crowd that wanted to reform law enforcement,
you know, one of the things they wanted to do
is get rid of qualified immunity
so they could be held accountable
through the civil process.
And it would basically end up with law enforcement
carrying what amounts to malpractice insurance.
That's probably how that would end up working out.
But those who back the blue twisted everything,
lied and made it sound like, you know,
then cops would be going to jail if that went away,
which incidentally led to cops going to jail.
Bumper sticker politics,
when it comes to complex issues,
slogans when it comes to complex issues,
never really benefit anybody.
They really don't.
And that's what happened.
The idea of getting rid of qualified immunity,
that was floated.
Those attempting to reform law enforcement tried that,
and law enforcement freaked out
because at the time they had a pretty decent shield
from criminal prosecution.
The average person was going to give them
the benefit of the doubt.
That isn't occurring anymore
because they see that they're unaccountable.
So anybody who makes that comparison
between medical professionals and law enforcement,
they're drawing on the idea that there's some way
within the civil process
to hold the officer personally accountable.
And in some jurisdictions that's possible,
but it's rare.
It is incredibly rare.
Most times they are shielded from personal liability.
So it's a bogus argument.
It's not a legitimate one.
At the end of the day, when you're looking at this case,
it was charged very well.
The prosecution didn't try to turn it into a thing
where she intended to go do this horrible thing
and get that other charge put on there.
Negligence, reckless.
They went with the reality of the case
and they got a conviction.
So I don't necessarily see that
leading to more aggressive prosecution.
What I see it is when there is somebody
who is acting with bad intent
where a more severe charge might be warranted,
I see them taking a plea.
But we'll have to wait and see.
Trying to guess the future decision-making process
of hundreds, if not thousands of prosecutors
across the country based off of one incident,
that doesn't really seem like a sound strategy.
We're just going to have to wait and see what happens.
But overall, I do not think that this case in and of itself
is going to be a game changer
because I don't think law enforcement
is going to be learning the right lessons.
They're still going to be looking for a way to circumvent it,
to circumvent being held accountable
rather than adjusting the behavior
that needs to be changed.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}