---
title: Let's talk about political theories of everything....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RPl7t8kRCTI) |
| Published | 2021/12/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes a common trend of trying to tie together various aspects of life into a unified conspiracy theory due to fear of chaos and disorder.
- Shares a personal anecdote of someone who spiraled down a conspiracy theory and lost everything in their life.
- Points out the fallacy in believing that a single group of people controls everything behind the scenes.
- Compares the idea of a grand conspiracy to bumper sticker politics and sloganism for oversimplifying complex issues.
- Emphasizes that the world is chaotic and scary, but believing in a grand conspiracy is reductionist and misleading.
- Warns about the steep costs individuals may pay to support such theories and urges people to prioritize real connections over internet theories.
- Expresses concern about the long-lasting impact of these conspiracy theories on society.

### Quotes

- "There is no man behind the curtain."
- "It's the ultimate reduction when it comes to trying to understand which way the world actually works."
- "It's comforting because you feel you have a handle on it."
- "I kind of started scrolling and going through different threads where people are believers in these theories."
- "The US is going to be dealing with the fallout from this for a really long time."

### Oneliner

Beau explains the dangers of falling for grand conspiracy theories, urging people to prioritize real connections over internet theories. The US will grapple with the fallout from these theories for a long time.

### Audience

Internet users

### On-the-ground actions from transcript

- Reconnect with family and friends for comfort and support (implied)
- Encourage critical thinking and fact-checking to avoid falling for harmful conspiracy theories (implied)
- Support individuals who may be entangled in conspiracy theories with empathy and understanding (implied)

### Whats missing in summary

Importance of critical thinking and media literacy in combating the spread of harmful conspiracy theories.

### Tags

#ConspiracyTheories #CriticalThinking #InternetCulture #MediaLiteracy #CommunityBuilding


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a political version
of the unified theory of everything.
Because we see it a lot.
We see people try to make sense of all of the disorder
by tying everything together.
Things that really can't be tied together.
You see people try to link politics and business
and social affairs, changes in the way society is run,
foreign affairs, and link it all together
through the idea that there's one group of people
controlling it all.
Because the chaos, the disorder is scary to a lot of people.
So they want something that they can lean on
and say this is what's going on.
And feel comforted by the fact that there is some plan.
I saw a screenshot and it was somebody cataloging
the impacts in their life as they spiraled down
one of these theories.
Lost their family, their house,
their daughters won't talk to them anymore, their job.
Everything seems like they're living in their truck.
And they say that they don't think
it was worth anything anymore.
They don't feel like it was worth making that stand.
Because now former President Trump is out there
advocating for people to get vaccinated as he should.
That makes him feel like it wasn't worth it.
It wasn't, it wasn't.
There is no man behind the curtain.
Doesn't work that way.
People who fall prey to these theories,
they tend to view our betters, those people up at the top,
those people with lots of money and power,
the political elites, they tend to view it
as like an ant colony where everybody's working together
to achieve the same goal.
That's not the case.
It's more akin to walking through the woods
and seeing a whole bunch of spider webs
grouped together on one part of a path.
They're so close together
and they're all kind of the same thing.
So you could be forgiven for believing
that they were on the same team,
that they worked together, that they were friends.
Now they all just congregate there
because that's where there's a high amount of insect traffic.
Insects, and the little metaphor here would be money and power.
They all gravitate to that to get more.
So they end up around each other.
There is no man behind the curtain.
The world's just scary.
The world is chaotic.
There's disorder.
Falling for the idea that one group of people
or two groups of people control the fate of the entire world
is a little silly.
It's the ultimate reduction
when it comes to trying to understand
which way the world actually works.
It's the pinnacle of bumper sticker politics, of sloganism.
It's comforting because you feel you have a handle on it.
Oh, that bad thing that happened?
Oh, well, it's that bad secret group.
That good thing that happened?
Oh, that was our team.
No.
It's just people that are in possession of a lot of money and power
engaging in practices to get more.
That's generally what most of it could be summed up as.
There's no grand conspiracy.
There never has been.
And if you look at the ones that have been tailored recently,
they were definitely politically motivated
because they tapped in to the harmless theories
that people of an older generation tended to widely believe.
And they used those and tied them all together
to create this theory of everything.
And then you could just put your faith in the people you believe to be on your side.
And you could just do whatever they told you to
and infer whatever you wanted to about their actions
because it was all part of that grand theory.
It made it to where their actions were accepted without criticism.
Things that this demographic of people would under normal circumstances
never support, they did.
Because, oh, it was part of that plan.
I know it looks bad, but it's 4-D chess.
It's not.
It's not.
It's just people with money and power
making deals with other people with money and power.
You know, after seeing that screenshot,
I kind of started scrolling and going through different threads
where people are believers in these theories
and looking for stories where it detailed the costs
that they had to pay to support this,
to be comforted by this theory.
It's pretty steep.
It's pretty steep.
I would suggest that people really weigh it out.
Are you more comforted by your family and your friends
or by some theory on the internet
that has you trying to connect dots that don't connect?
The US is going to be dealing with the fallout from this
for a really long time.
I think it's probably way more widespread
than most of us want to believe.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}