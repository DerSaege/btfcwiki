---
title: Let's talk about nosy neighbors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qYgzRULatDQ) |
| Published | 2021/12/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a situation where neighbors on apps like Nextdoor brag about calling the police on people for minor reasons like driving up and down the street or sitting in a car for too long.
- Acknowledges the potential dangers of introducing armed police into non-threatening situations.
- Suggests curbing this behavior by appealing to self-interest or greed rather than solely relying on moral arguments.
- Points out that crime rates, not just crime itself, impact property values and that constant police presence can lower property values.
- Proposes that neighbors who excessively call the police may actually be harming property values in their own neighborhood.
- Recommends using monetary consequences as a deterrent for such behavior, as it may be more effective than moral appeals.
- Encourages looking up studies on the correlation between crime rates and property values to present factual evidence in such situations.

### Quotes

- "You should really only call the law if you think that lethality is an appropriate possible response."
- "Aside from that, that's happening to every house in the neighborhood."
- "If you could suggest that they could be removing tens of thousands of dollars of equity from themselves and all of their neighbors, they might stop."
- "It's statistics. It's the constant police presence. That's what drops property rates."
- "But if they call long enough on enough different people, there's going to be crime."

### Oneliner

Beau addresses nosy neighbors calling the police unnecessarily, suggesting monetary consequences over moral arguments to deter this behavior and protect property values.

### Audience

Community members

### On-the-ground actions from transcript

- Share statistical studies on crime rates and property values to raise awareness about the impacts of excessive police calls on property values (suggested).
- Encourage neighbors to reconsider their approach by discussing the potential financial consequences of their actions on property values (implied).

### Whats missing in summary

Importance of understanding the financial implications of excessive police calls on property values and neighborhood dynamics.

### Tags

#NosyNeighbors #CommunityPolicing #PropertyValues #CrimeRates #MonetaryConsequences


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about nosy neighbors.
I had somebody ask.
Short version is they use one of those apps,
like Nextdoor or whatever.
I don't really know how they work.
We don't really use them out where I live,
because even if you got everybody on the street to sign up,
it's like four people.
But basically they're on this app and they find out that,
hey, their neighbors are kind of bragging
about calling the law on people for no reason.
Oh, this car drove up and down the street three times.
Better call the cops.
They're probably looking for an address.
Oh, this car is sitting somewhere
for an extended period of time.
Better call the law.
That person over there, even though we
don't have any evidence, we think they're dealing.
Call the law, right?
The person doesn't like this behavior.
The person understands that this can lead to really bad things.
I mean, obviously it's wrong to harass people.
But aside from that, the introduction
of people with guns to an otherwise stable situation
is not really a good thing.
So they want to curtail this behavior.
Instinctually, you want to make the moral argument.
You want to say, hey, it's wrong to do this.
And it can have really bad consequences.
You should really only call the law
if you think that lethality is an appropriate possible
response, because it is possible.
But the problem with that is, if you're
talking about people like this, they're not going to care,
right?
Probably have a little bit of bigotry.
Probably going to be like those people while they got it coming.
And I have to keep my neighborhood safe.
The thing is, even though you're trying to achieve a moral goal,
you are not limited to the moral argument.
You can make a different argument.
You could make an argument that appeals to self-interest
or greed.
Pretty powerful motivators.
Crime reduces property values.
False.
Crime rates do.
Crime in and of itself doesn't actually
do anything to the property value.
It's not until it's recorded and it becomes a statistic
that it does.
If there is a one standard deviation increase in crime,
increase in crime rates, depending on the study,
you will see a 3% to 10% decrease in property values.
Now, in an area where something like this is happening,
you're probably talking about what?
$200,000 to $500,000 homes?
So that's anywhere from $6,000 to $50,000.
They're removing from their own pocket
so they can play Magnum PI or whatever.
Aside from that, that's happening
to every house in the neighborhood.
Because if you call the cops long enough,
eventually they're going to find crime.
And all of a sudden, even though there
isn't any real tangible crime to be seen,
well, maybe they find something in somebody's car.
Maybe because people are getting harassed,
they vandalize something or something like that.
The end result of this is the people
who think they're protecting the neighborhood
are going to create a record of crime in that neighborhood
that otherwise wouldn't have been recorded
because it wouldn't have been reported.
And in some areas, just the constant calls
and police presence can drop property values.
I would go that route.
People who are that concerned with their neighborhood
probably have the idea that they're protecting it.
If you could suggest that they could
be removing tens of thousands of dollars of equity
from themselves and all of their neighbors, they might stop.
You're probably going to have more luck with that
than trying to make this case on moral grounds.
These are probably not people who
are going to go for the moral argument,
but I'm willing to bet they'll go for a monetary one.
That's the route I would go.
It isn't crime in and of itself.
It's crime rates.
It's statistics.
It's the constant police presence.
That's what drops property rates.
And from all of the messages, it doesn't actually
appear that there is any crime.
It's just that they think there is or they're worried about it.
But if they call long enough on enough different people,
there's going to be crime.
The cops will find something.
So I would suggest going that route.
And you can find statistics for your state.
I'll drop some studies down below,
or I'll try to remember to drop some studies down below
about it.
But you could Google one standard deviation, crime
rates, property values, and you'll
get information about it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}