---
title: Let's talk about semantics, Shepherd's Pie, liberals, and leftists....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9NE9BI-OpgE) |
| Published | 2021/12/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Defines semantics as the search for meaning in words and linguistics.
- Explains that when people dismiss something as "just semantics," they mean it is an irrelevant argument over definitions.
- Illustrates the concept with an example of a train conductor and a passenger arguing over the destination.
- Shares his love for cooking shepherd's pie, a dish loved by many except for his five-year-old.
- Describes making shepherd's pie with various meats like chicken or bacon, acknowledging the material change in the dish.
- Differentiates between shepherd's pie and cottage pie based on the meat used, not just for semantics but due to a substantial difference.
- Emphasizes that correcting someone over material changes is not irrelevant but educational.
- Draws a parallel between his son not eating shepherd's pie due to misunderstanding and the intentional conflation of liberals and leftists by Republicans.
- Breaks down how the Republican Party's labeling of liberals as leftists creates fear and motivates their base through scare tactics.
- Calls for Democrats to challenge and correct the misrepresentation of liberals as leftists for political benefit.

### Quotes

- "Arguing over material changes, sometimes it's important."
- "They will tell their base, their constituents, that liberal and leftist mean the same thing."
- "Allowing them to use the term leftist, radical left, when they're talking about liberals, that's just handing them a way to scare their base."
- "That gap between being a liberal who is in support of capitalism and being a leftist who is generally against capitalism, that's a pretty big gap there."
- "It's probably more important for liberals to make that distinction."

### Oneliner

Beau explains semantics through cooking shepherd's pie, illustrating the importance of clarifying distinctions like liberals vs. leftists to combat political fear-mongering.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Challenge misrepresentations in political discourse (suggested)
- Educate others on the differences between liberals and leftists (implied)

### Whats missing in summary

In watching the full transcript, viewers can gain a deeper understanding of how semantic arguments and misrepresentations in political labels can impact public perception and drive political agendas.

### Tags

#Semantics #Liberals #Leftists #PoliticalDiscourse #Misrepresentation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about semantics, what it is,
and what people generally mean when they say,
oh, that's just semantics.
That's a semantic argument.
And we'll end up talking about the fact that I like to cook.
And we'll talk about the dish I like to prepare the most.
So we'll end up talking about shepherd's pie.
And we will end up talking about what shepherd's pie can teach us
about liberals and leftists.
OK, so what is semantics?
It's the search for meaning in words, language, linguistics.
In and of itself, semantics isn't a bad thing.
It's what creates definitions that we use all the time.
But that's not what people mean when they say that's semantics.
What they generally mean is that you're discussing
something that's irrelevant.
You're discussing a variation in a definition
that doesn't really matter.
Good example would be a train conductor saying,
the final stop is the destination, and that's Berlin.
Because that's the train's destination.
And somebody on the train saying, no, the destination is Munich.
Look, it's here on my ticket.
Because they're getting off two stops before.
Not an argument worth having, right?
And there's no material change in what the word destination means.
It's the end of the trip.
Just one person's thinking about themselves,
one person's thinking about the train.
Difference of perspective.
That's not an argument worth having.
Gets you off from whatever the main point is.
OK.
So I like to cook.
I make shepherd's pie that is, not to toot my own horn, delicious.
Anytime I go to a potluck or some family gathering
where everybody's expected to bring a dish, that's what people ask for.
The whole time I've been making it, there's only one person
that won't eat it, right?
So how do I make this delicious entree?
Well, I take whatever meat I'm going to use.
And right now, there's somebody in the comments section typing out,
if you're not using lamb, it's not shepherd's pie.
And they're going to assume that I'm going to use ground beef.
So it's cottage pie, right?
Is that semantics?
No, because it reflects a material change in the thing you're talking about.
It's literally different things.
There's a change in substance, right?
Now, for me, I use shepherd's pie for all of it,
because I don't just use ground beef.
Sometimes I'll use chicken, or I'll even throw in bacon and eggs
and make like a breakfast version.
So in my little circle, people will say shepherd's pie with,
and then specify the meat.
There's still a clarification of the material change in it, right?
I can't just limit myself to shepherd's pie and cottage pie,
because these other things, I don't know the name of them.
Like Bo's Breakfast Pie?
Nobody knows what to call it.
So it's all shepherd's pie with the clarification of the material change.
If somebody was to correct me and say, this is really cottage pie,
it's not semantics.
It's not irrelevant.
It's them trying to educate me.
So arguing over material changes, sometimes it's important.
Not so much here, but we're going to get there.
The one person who wouldn't eat my shepherd's pie is my five-year-old.
And he wouldn't eat it from the time he could talk,
from the time he could say no, he was saying no, right?
Why?
Put yourself in the mind of a little person.
If cherry pie is made with cherries, and apple pie is made with apples,
and you have had a giant German shepherd hanging over your shoulder
your entire life, what is shepherd's pie made with?
He wasn't eating it.
Makes sense to me explaining, hey, that's not what's in this.
That's not semantics.
It's education.
What does this have to do with liberals and leftists?
Those on the right, Republicans in particular,
will intentionally conflate the two.
They will tell their base, their constituents,
that liberal and leftist mean the same thing.
And a lot of liberals see this as harmless.
It's not.
Not to the Democratic Party.
It matters a lot.
If a person is certain that their base, their constituents,
don't know the difference between liberals and leftists,
so they just call them leftists, what happens
when that base, the member of that base, goes home and Googles that term?
What do they get?
A whole bunch of other isms that fall under leftism, right?
One of them is going to jump out at them
because they had decades of anti-Soviet propaganda,
and they're going to be scared of it.
How does the Republican Party motivate its base?
Always, fear.
Allowing them to use the term leftist, radical left,
when they're talking about liberals, when they're talking about Democrats,
that's just handing them a way to scare their base,
to drive voter turnout for their side.
It boggles the mind to see Republicans constantly
use the term leftist, and no Democrat challenges them on it,
asks them to define it, because they can't.
Most Republicans can't, to be honest.
They've said this so long, I actually believe it.
That gap between being a liberal who is in support of capitalism
and being a leftist who is generally against capitalism,
that's a pretty big gap there.
But when you're just talking about raw political power,
allowing the Republican Party to say these liberals are leftists,
and therefore, you have a whole bunch of their base saying,
oh, they're communists, and being terrified and driving voter
turnout, that's bad.
This is something the Democratic Party should probably
work to correct.
The term leftist being thrown out there
to reflect run-of-the-mill liberals
is probably something the Democratic Party
should work to correct.
Normally, right now, most of the time,
it's leftists who are like, no, we are not
the same as those liberals.
It's probably more important for liberals
to make that distinction.
If you're talking about any kind of benefit politically
for the Democratic Party.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}