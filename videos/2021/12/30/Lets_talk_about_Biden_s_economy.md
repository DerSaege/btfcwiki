---
title: Let's talk about Biden's economy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0ihsmA_HUms) |
| Published | 2021/12/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden's first year in office has shown significant economic growth, with gross domestic product doubling the record of the last 40 years.
- The U.S. financial markets are outperforming world markets by the largest margin since 2000.
- When comparing various economic metrics, Biden's administration ranks number one or two in nine out of ten categories, with per capita disposable income being the only area where he lags behind.
- The narrative that the economy is failing under Biden is not accurate, as his administration's economic performance surpasses that of the previous seven presidencies.
- While some credit goes to Biden's policies, a significant portion of the economic gains can also be attributed to former President Trump's shortcomings in his last year.
- The future economic growth is uncertain without the passing of Build Back Better, as projections have been revised downwards due to political hurdles.
- Despite positive economic trends, the dramatic increases seen in Biden's first year may not continue at the same pace in the following year.

### Quotes

- "This is probably going to be recorded in the future as the Biden boom."
- "The narrative that the economy is circling the drain, well, that's just not true."
- "Had Trump even just been average as a president, it wouldn't have been as easy for Biden to make these large gains."

### Oneliner

President Biden's economic performance in his first year has exceeded expectations, with significant growth in various metrics, challenging the narrative of a failing economy.

### Audience

Economic analysts, policymakers

### On-the-ground actions from transcript

- Monitor the impact of policy decisions on the economy (suggested)
- Advocate for policies that support economic growth (suggested)
- Stay informed about economic trends and projections (suggested)

### Whats missing in summary

Insights on the specific policies and initiatives that led to the economic growth under President Biden.

### Tags

#PresidentBiden #EconomicGrowth #BuildBackBetter #Policy #Performance


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about the economy under President Biden.
First year, we're going to see how well things have gone.
You know, you look to the media, look to the news and most news reports,
especially on non-business, non-economic reports.
It looks pretty bad.
I mean, they're painting a lot of doom and gloom all the time.
How accurate is that?
Okay, so let's start with the big indicator, gross domestic product.
For the last 40 years, an incoming administration,
well, they haven't topped 2.74% in economic growth.
Biden, well, he looks to be coming in at 5.5.
Double. Double the record for the last 40 years or so.
That kind of runs counter to the idea that the economy is going bad right now.
Now, you also have a metric that is used is how the U.S. financial markets
are comparing to world markets.
And currently, the U.S. markets are outperforming world markets
by the biggest margin since 2000.
That's also not so doom and gloom.
If you were to take the performance, first year performance,
of every incoming administration, the last seven of them,
last seven presidencies, and you were to compare 10 different metrics,
these, gross domestic product, profit growth, S&P 500 performance,
consumer credit, nonfarm payrolls, manufacturing jobs,
business productivity, dollar appreciation, S&P 500 relative performance,
and per capita disposable income.
The Biden administration is number one or number two in nine of those 10.
Only one he is lagging on is per capita disposable income,
which is trailing, I want to say, just over 1%.
Which that is, well, that's just a trend in the United States at the moment.
And for a while, you know, rich get richer, poor get poorer.
Wages aren't keeping up.
So there's not as much disposable income.
Don't get me wrong, I think he should go for 10 for 10 on this one.
The narrative that the economy is circling the drain, well, that's just not true.
In fact, if you look at the last seven presidencies,
nobody comes close in their first year in office.
Nobody.
This is probably going to be recorded in the future as the Biden boom.
Now, that's all the good news.
Let's put it into context.
It's a whole lot easier to go up and increase the GDP when it kind of contracted before.
When the previous administration, let's say they downplayed something major
and it caused a lot of economic problems.
And since it was down, it was easier to create an increase.
So while some of this is related to Biden's policies and his legislation that he's pushed through,
a lot of the credit also goes to former President Trump for failing so hard in his last year.
Made it easier for Biden to make substantial increases in gains.
And I know that sounds like a joke, but it's not.
Had Trump even just been average as a president,
it wouldn't have been as easy for Biden to make these large gains.
The other thing that's worth noting is that without Build Back Better, don't expect this to continue.
We've covered it already.
As soon as news broke that Manchin was kind of torpedoing the whole thing,
the estimates got revised down for next year.
There's still increases, but they're not as big as they could have been.
So this probably won't continue through next year.
You'll still see increases, but they won't be this dramatic.
This is something to keep in mind as you hear politicians rail about the economy.
When you look to Bloomberg Business, when you look to Goldman Sachs,
they were the ones who revised after Manchin.
The reality is that the people who really kind of look into the economy and that is their business.
Well, he's had the best first year of any of the last seven administrations.
I don't know what more they really want, to be honest.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}