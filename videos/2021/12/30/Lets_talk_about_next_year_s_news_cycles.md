---
title: Let's talk about next year's news cycles....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GBrNKttAfkQ) |
| Published | 2021/12/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Recent statistics comparing viewership in 2020 to 2021 show significant drops across major news outlets like CNN, Fox News, MSNBC, ABC, NBC, CBS, New York Times, and Washington Post.
- The decline in viewership could lead news outlets to produce more sensationalist content to attract viewers and boost ratings.
- News outlets, being businesses, need to increase viewership to stay profitable, possibly by focusing on sensational stories or expanding readership overseas.
- Anecdotal evidence from YouTube channels with over 50,000 subscribers shows an overall increase in viewership, possibly due to providing context and moving away from sensationalism.
- Americans in 2021 reportedly feel more comfortable and safer under President Biden compared to President Trump, potentially impacting how they respond to news and events.

### Quotes

- "News outlets are businesses. They have to make money."
- "Americans are more comfortable in 2021 than they were in 2020."
- "You watch the news because something scary happened."

### Oneliner

Recent stats show news viewership decline, leading to potential rise in sensational content; Americans reportedly more comfortable in 2021 under President Biden.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Support independent media outlets and platforms (suggested)
- Seek news sources that provide context and avoid sensationalism (suggested)

### Whats missing in summary

The full transcript provides a deeper insight into the changing landscape of news consumption and the potential impact of sensational content on viewers' perceptions and behaviors.

### Tags

#NewsConsumption #MediaTrends #Sensationalism #Viewership #PresidentBiden


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk a little bit about the news
and try to give you a heads up
on what's probably gonna happen next year
because some interesting statistics have come out
and it is probably going to shape
a lot of what you see on the news over the next 12 months.
Stats comparing 2020 to 2021 have come out.
At CNN, viewership dropped weekday primetime 38%.
At Fox News 34%.
At MSNBC 25%.
Broadcast news outlets, well they did a little bit better
with their flagship shows but not by much.
ABC is down 12%. NBC is down 14%.
CBS is also down 12%.
The New York Times and Washington Post,
now they used the month of November which was a bad month to use
because the election last year.
But the number of unique viewers dropped 34% at the Times and 44% at the Post.
Even if you were to adjust for the election and the impact that has on viewers,
it's still going to be a pretty big drop.
But it's not really that bad.
Okay, so what does that mean?
It means they're going to have to find some way to get more eyes on the screen.
That they have to get these numbers up, right?
I mean, news outlets are businesses. They have to make money.
So what's their most likely solution?
If they compare these numbers and they look at them,
their options are follow the Wall Street Journal
and look for increased viewership, readership overseas.
Or they have to duplicate what was going on in 2020.
Give you something sensational to latch on to.
Stories that they can milk and keep in the news cycle.
That's what I've got my money on.
I would be prepared for a lot of sensationalist content to start coming out
because these numbers, they're not going to work for the people upstairs.
Now, these are real statistics. This was put together by the AP.
I have something anecdotal that I think might should weigh into this conversation.
Now, by anecdotal, I mean very small sample size.
Six people, including myself.
On YouTube, when it comes to people on YouTube, channels on YouTube
that have a subscriber base of more than 50,000 people who cover the news,
their numbers all went up.
They all went up over the last year, about 8 to 12%.
I'm the exception there. I only went up 1.2 or something like that.
Statistically insignificant, but also keep in mind that, you know, in 2020,
I was putting out three videos a day, whereas in 2021, I'm only putting out two,
and my numbers ran basically flat.
It's also worth noting that all of these channels provide a context.
It might be that people got tired of the sensationalism
and started looking elsewhere.
That seems like a pretty safe guess.
But again, anecdotal.
So, you can expect to see a lot of sensationalist content
start coming out over the media.
They're going to find stories they can latch onto
and try to keep people glued to their screen for weeks on end
so they can get these numbers up.
Now, the good news that comes out of all of this
is that Americans are more comfortable in 2021 than they were in 2020.
They feel more comfortable, safer under President Biden
than they did under President Trump.
Let's be honest. You watch the news because something scary happened.
So there is that. The people of the United States are starting to feel more comfortable,
and that can be either good or bad, depending on how they react.
Whether they view that comfort as motivation to make things better,
or they become complacent.
That really determines whether or not that's good news or bad news at the end of the day.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}