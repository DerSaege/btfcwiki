---
title: Let's talk about Texas voting rights....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GavKH2RQxDA) |
| Published | 2021/12/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Department of Justice filed suit against Texas over new maps.
- Alleges Texas in violation of Voting Rights Act section two.
- Republicans accused of redrawing maps to negatively impact specific groups.
- Suit claims Republicans diluted votes of Black and Latino Americans.
- Republicans broke up predominantly black districts to dilute their voice.
- Did the same with Latino districts, impacting their voting ability.
- Suit likely to succeed, but Republicans may attempt similar tactics elsewhere.
- Republicans shifting from outdated ideas to undermining democracy.
- Predicted by voting rights experts and those familiar with gerrymandering.
- Be prepared for other states to face similar challenges.

### Quotes

- "Republicans have realized it cannot win on outdated, bad ideas."
- "They have decided to give up on the founding ideas of this country instead."
- "They can't win through the democratic process, so they have given up on democracy itself."

### Oneliner

Department of Justice sues Texas over alleged violation of Voting Rights Act, accusing Republicans of diluting Black and Latino votes to undermine democracy.

### Audience

Voters, activists, community members

### On-the-ground actions from transcript

- Stay informed and engaged with redistricting processes (implied)
- Support organizations fighting for fair voting practices (implied)
- Advocate for transparency and fairness in redistricting (implied)

### Whats missing in summary

The full transcript provides a deeper dive into the implications of redistricting and the potential erosion of democracy, offering a comprehensive understanding of the challenges at hand.


## Transcript
Well, howdy there, Internet people, it's Bo again.
So today, we're going to talk about Texas
and the Department of Justice filing suit against Texas
over its new maps.
So all over the country right now,
there is redistricting going on.
There are a lot of people suggesting
that Republicans are going to take this opportunity
to redraw the maps in ways that will negatively impact different groups, it will undermine
their power, it will dilute their vote.
The Department of Justice suit is saying, yes, that's occurring.
They are alleging that Texas is in violation of section two of the Voting Rights Act, which
is basically saying, hey, you're attempting to hamper this group's ability to vote because
of a bunch of reasons.
The one in question here is skin tone, because it appears that the Republican Party in Texas
found a way to dilute the votes of Black Americans and Latino Americans, and what they did was
they went to districts that were predominantly black where their voice would actually count.
And they broke that district up and they took a little piece of that district and stuck
it in the district with a bunch of white people and did it to the entire district.
And then they did the same thing with Latinos.
So it took away their voice.
it impacted their ability to vote. If you dilute it to a point that there is no
way they get any say in anything because they will always be overridden,
that's the same as taking away their ability to vote. Now a cursory glance?
Yeah, that looks like what happened and it is probably happening in
other locations. I believe this suit in Texas will succeed. It certainly looks as though that's the
case, but it's not going to stop Republicans from trying this in a different way. There aren't any
real penalties for attempting to do this, not for the individuals involved, really.
So what people should probably acknowledge at this point is that the Republican Party
in large part has realized it cannot win on outdated, bad ideas.
So rather than giving up on those ideas, they have decided to give up on the founding ideas
of this country instead.
They can't win through the democratic process, through failed ideas, so they have given up
on democracy itself and are attempting to actively undermine the representative democracy
that we have in this republic.
Probably worth noting, it's also not unexpected.
This was kind of predicted by everybody who is involved in this, who looks at voting rights,
who looks at the maps, who talks about gerrymandering.
We think of that as something that doesn't really occur anymore.
It obviously does.
And they all predicted it, and yeah, they're right.
It's probably going to happen in other locations as well.
So be ready for your state's name to be in the news over this.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}