---
title: Let's talk about the TV show Yellowstone....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7VKTtMswwS4) |
| Published | 2021/12/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau is discussing the TV show Yellowstone, a popular show among certain demographics.
- Beau explains his decision not to watch the very last episode of Yellowstone due to the show becoming "woke."
- The introduction of an animal rights activist character in Yellowstone led Beau to avoid the final episode.
- Beau suggests that if a show wants to introduce woke topics, it should prepare the audience with consistent social commentary throughout.
- He outlines various societal issues that could have been addressed in Yellowstone to ease the introduction of woke themes.
- Beau mentions the importance of addressing topics like social safety nets for orphans, the juvenile justice system, criminal justice system, and prison system.
- He also suggests incorporating themes related to reservations, missing indigenous women, race, law enforcement misconduct, and gender equality.
- Beau proposes showcasing strong female characters to challenge toxic masculinity and discussing family planning openly.
- He recommends including political elements in the show, such as running for office as an independent in a red state.
- Beau contrasts his suggested approach with Yellowstone's actual portrayal, stating that the show did not incorporate woke themes throughout.
- He points out that Yellowstone features tough characters and criminal elements but didn't follow the woke narrative he described.
- Beau expresses surprise at the portrayal of right-wing MAGA groups as the villains in Yellowstone, suggesting it wasn't meant for that audience.
- He addresses the reaction of viewers who felt offended by the introduction of an animal rights activist character in the show.
- Beau humorously distinguishes between viewers who may identify with different characters from the show, implying a lack of true understanding for some.

### Quotes

- "Yellowstone was never meant to be something for the Make America Great Again crowd."
- "If you didn't catch any of this and all of a sudden got offended and got upset because somebody on Twitter told you to be mad about an animal rights activist character, you're not RIP, you're Jimmy."

### Oneliner

Beau explains how Yellowstone could have introduced woke topics through consistent social commentary, contrasting it with the show's actual direction and audience perception.

### Audience

TV Show Viewers

### On-the-ground actions from transcript
- Analyze and incorporate relevant social commentary into media productions (suggested).
- Address societal issues in storytelling to raise awareness (suggested).
- Challenge toxic masculinity in narratives by featuring strong female characters (suggested).
- Promote open dialogues on race, gender equality, and political issues in media (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of how media can introduce woke topics effectively, urging for nuanced storytelling and social awareness in TV shows like Yellowstone.

### Tags

#TVShows #SocialCommentary #WokeTopics #MediaAnalysis #SocietalIssues


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about a TV show.
We're going to talk about a TV show called Yellowstone.
Now, I have watched every episode of Yellowstone,
except for the very, very last one.
I haven't seen that one yet.
And the reason I haven't watched it yet
is because of what happened on Twitter.
Now, if you don't know, this show,
it's really popular among people who sound and look like me.
But it did that thing where it suddenly
went woke from out of nowhere.
So I didn't watch that last episode because it went woke.
It introduced a character that's an animal rights activist.
And that's just, you can't do that.
Not in a show like that.
If you want to introduce an animal rights activist,
you want to make sure that your audience is prepared for that.
You have to have a whole bunch of social commentary
throughout the entire show.
You can't just spring it on them out of nowhere.
I mean, so what we're going to do today
is we're going to talk about what it would be like
if you were getting your audience ready
to have a woke topic thrust upon them.
So you're talking about up there in Yellowstone.
Okay, so what you would want to do
is make sure that you do a whole lot of shows
that highlight the lack of social safety nets for orphans
because they often end up as ranch hands.
want to do that. And while you're doing that, you could also highlight the failing juvenile
justice system. And since you're doing that, you could probably highlight the entire criminal
justice system and all the issues that there is with that. And you can talk about the prison
system and how people who are getting out of prison, even if they want to walk away, that maybe
because of a lack of opportunities, they end up working at a ranch where a whole bunch of
people try to push them back into crime. That could be a thing. And since it is located where
it is if you want to get people ready for woke topics you have to talk about the struggles that
occur on the reservation. You have to talk about the res and while you're doing that you should
probably highlight the you know the widespread issue when it comes to missing indigenous women.
Maybe have an episode about that. That'd be a good idea and since you're doing that you should
probably also talk about race in general. Perhaps you can have a college professor,
let's make her native. And you could take the show into her classroom where they talk about history
and they discuss race in a pretty critical manner. That would be a good idea. And since
you're talking about race, you should probably talk about law enforcement and perhaps the way a
lot of justified shootings are actually completely unwarranted. Maybe with a kid that was about to
put his gun down. That'd be a good idea. You get him ready for woke topics by
doing stuff like this. And since you're talking about police misconduct, you
should highlight other kinds. Perhaps parallel something that occurred in
Baltimore to Freddie Gray using a cattle trailer. You'd want to do that too.
That'd be that probably be a good idea. But you don't just want to stick to race
and social issues. You probably want to talk about gender equality as well. So
So maybe make sure that the toughest character on the show is a woman, and go ahead and talk
about the objectification of women and highlight that by introducing women to the bunkhouse
to cause a whole bunch of issues among the men who suffer from toxic masculinity.
And because you're doing that, you should probably also have a character who's doing
something just to prove that he's a man and joins a sport where you normally just end
up with a bunch of broken bones and belt buckles.
That would be a good idea too.
would help get the audience ready. And since you're talking about all of this
stuff and talking about male and female relationships, you should probably also
maybe make sure that all of the leading characters are like very openly in favor
of having a lot of choice when it comes to family planning. That would, that'd be
a good idea. And all of this is really political. So you should probably have
somebody run for political office, but since it's a red state, you want to make
sure that they run as an independent. Big, big, big good idea. That's all to get
people ready to have an animal rights activist character. So you should
probably have something about animal conservation already, perhaps through a
storyline involving the natives, or maybe talk about how ranchers use the
deprecation program and kind of abuse it and say that wolves attacked their cattle.
When it wasn't their wolf, it was them with a weed whacker, and perhaps they
hide the weed whacker in the barn and that could lead to the unjustified shooting. Be a good idea.
Do it like that. Now luckily Yellowstone didn't do any of this. You know, Yellowstone isn't...
they're not woke. They don't make social commentary at all throughout the entire show in literally
every episode. Now just to further drive the point home that this wasn't a show
for MAGA people, understand in the universe of Yellowstone, it's already an
accepted fact that the ranches have have their own gun thugs. They have their own
their own goons. The men who wear the brand at Yellowstone, right? There's
There's probably other ranches that have the same thing.
So you have a ready source of tough guys
that the bad guys can use to go after the family.
And since it also did the totally un-woke thing
of highlighting how people who had substance issues,
even after they clean up,
they still have to deal with a lot of their past.
And it already framed the fact
that there's a whole bunch of illicit substance production
in the area.
also have that criminal element that you could tap into. But that's not what
happened, is it? When the villain, whoever it was at the time, whether it be the
hedge fund owner, or the equity company, or the people who just wanted to be the
landlord, some form of hyper capitalist, when they needed muscle, who did they
tap into? A right-wing MAGA group as the bad guys. What show were y'all watching?
Yellowstone was never meant to be something for the Make America Great Again crowd.
It's not what it is.
Look, I understand that everybody that watches this show, every guy who watches this show,
you think you're RIP.
If you didn't catch any of this and all of a sudden got offended and got upset because
somebody on Twitter told you to be mad about an animal rights activist character, you're
not RIP, you're Jimmy.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}