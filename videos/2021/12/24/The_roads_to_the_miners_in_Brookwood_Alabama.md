---
title: The roads to the miners in Brookwood, Alabama....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jQ7FBjrnSqk) |
| Published | 2021/12/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Updates on events in Canada, Alabama, and another location.
- Money raised for a homeless camp in Canada and children of minors on strike in Alabama.
- Money sent to Aaron for shopping in Canada; photos posted on Twitter.
- Heartwarming experience in Canada, where Beau was invited to have a meal.
- Beau drove to Brookwood, Alabama, for the minor strike.
- Signs of community support with yard signs all over town.
- Visited a meeting hall with organized supplies for strikers.
- Resemblance of the supply depots after Hurricane Michael.
- Importance of organization for long-term strikes.
- Stories shared by people who worked in the mine since the 1980s.
- Sense of community and support during strike rally.
- Lack of support from US politicians for the labor movement.
- History of labor movements in the United States being cyclical.
- Every kid who needed help received it during the events.
- Emphasis on community building and organization for a better society.

### Quotes

- "If you have the ability to foster that kind of camaraderie, that kind of community, that kind of network around you, it can only make your community better."
- "It's worth doing because you can weather storms, whether they be hurricanes or they be, well, other kinds of issues that impact the entire community."
- "Solidarity Santa came through. And you all played a part in that."
- "That's kind of the way the history of the labor movement in the United States works."
- "It makes it a whole lot easier to bargain for what you want and to create the kind of world you want."

### Oneliner

Beau catches up on events in Canada and Alabama, showcasing the power of community support and organization in times of need.

### Audience

Community organizers, activists, supporters

### On-the-ground actions from transcript

- Support local community initiatives (implied)
- Foster camaraderie and networks in your community (implied)
- Organize aid drives for those in need (implied)

### Whats missing in summary

The full transcript provides deeper insights into the importance of community solidarity and organization during challenging times.

### Tags

#CommunitySupport #LaborMovements #Solidarity #Organization #Activism


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to catch you up on some events
that took place in Canada, Alabama,
and over on the other channel.
Some of you may remember or may have participated
in the live stream we did over there
to raise money for a homeless camp in Canada
and Christmas presents for the children of minors
who are on strike in Alabama.
Those two little projects have been completed,
so we're going to kind of give you a rundown on it.
Now when it came to Canada, I didn't drive up there.
We just sent the money to Aaron over on the channel
Re-Education, and he went and did all the shopping
and dropped it off.
He posted photos to Twitter about it.
It was pretty heartwarming.
They kind of insisted that he sit down
and have a meal with them.
It's good stuff.
When it came to Brookwood, Alabama,
came to the minor strike, I drove it up there.
Got there a little bit earlier than Hayden told me to,
so I had some time to waste and drove around town.
Yard signs everywhere, all over town.
The minors definitely have support in that community.
Stopped and got some fried pickles because it's Alabama.
And then went over to the hall, to their meeting hall.
Still wasn't open yet, so I had to wait around a little bit,
and then they all showed up at once.
That coordination, it was apparent
from the very beginning.
Doors opened, and we started unloading stuff,
unloading supplies.
And then when I walked into that room, into that hall,
looked around, food all over the walls.
Canned food, stockpiled, grocery bags prepared
so people could come in and grab them if they were in a hurry.
School supplies, hygiene.
Looked really familiar to me.
Looked exactly like the little supply depots
that sprung up around here after Hurricane Michael.
That's how devastating a strike is.
That's how much planning and logistics
it takes to pull it off for months on end.
You have to have that kind of organization to back it up.
You have to have the organization that can keep
people fed and clothed and getting
the necessities they need.
You know, I sat there and I talked
to people who had worked for that company,
worked in that mine since the 1980s.
Heard stories about the miners that they'd lost.
Saw kids running around that union hall playing.
Really reinforced the sense of community that they have.
And after I delivered the funds that were raised,
they invited me to go to a strike rally.
And it's right around the corner.
It's a very small town.
It's on a baseball diamond.
And I showed up, and that sense of community,
that level of coordination and organization, it was there.
You could see it.
All these people standing out in that field,
talking, just kind of experiencing
that shared struggle that they have.
And while people were talking, and when the speakers started
kind of riling everybody up, they
were talking about all the support
that they had from all over the world, different unions
expressing solidarity.
Didn't see a lot of US politicians, though,
which is weird to me because it's political gold.
It is political gold.
You know, striketober, that was a thing.
So much union and labor movement activity.
It seems like there should have been more support coming
from the elected officials down there in Alabama,
but didn't really seem forthcoming.
And that's the sad part about it,
because that's kind of the way the history of the labor
movement in the United States works.
There's a spark, something that sets it all into motion,
that reignites the movement, right?
Then there's a bunch of activity, a surge of activity
that occurs.
And then as they get concessions from the companies,
well, it kind of dies out.
It's the way it has always worked.
It's very cyclical.
The striketober for this mine, it's
been going on like eight months.
They were the spark this time.
They were the thing that set all of this in motion.
I have no doubt that some of the people I met
will have their names written down
when the next chapter in the labor movement history
is written here in the United States.
Now, for your part, it is worth noting
that every kid, every kid who asked for, who had a family,
who asked for help got it.
So Solidarity Santa came through.
And you all played a part in that.
But a big takeaway here is that sense of community,
the logistics, the planning that it
takes to pull something like this off
can be applied to anything.
If you have the ability to foster
that kind of camaraderie, that kind of community,
that kind of network around you, it
can only make your community better.
It can only improve the lives of the people around you.
It's definitely one of those things
that even if it is difficult to start,
it's worth doing because you can weather storms,
whether they be hurricanes or they be, well,
other kinds of issues that impact the entire community.
And it makes it a whole lot easier
to bargain for what you want and to create
the kind of world you want.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}