---
title: Let's talk about fun supply chain chaos on Christmas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pv8lP1AtUYA) |
| Published | 2021/12/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Annual fundraiser for teens in domestic violence shelters during the holidays, ensuring they receive gifts specifically for older kids who are often overlooked.
- Unexpectedly, the shelter had no teens this year, a first-time occurrence prompting redirection to another shelter.
- The journey to fill bags with gifts turned complicated due to supply chain issues and scarcity of items like tablets, accessories, and consoles.
- Despite challenges, the community's generosity and support made it possible to provide gifts and contribute $5,000 for teen programs.
- Beau expresses gratitude for the community's support and teamwork, turning a channel initially seen as a joke into a force for good.

### Quotes

- "That problem, we're going to fix it."
- "Y'all have taken a channel that really did start as kind of a joke and turned it into a real force."
- "This is the type of thing that can happen when people come together."

### Oneliner

Beau navigates supply chain issues to ensure teens in shelters receive gifts, showcasing community's generosity and problem-solving spirit.

### Audience

Community members, supporters.

### On-the-ground actions from transcript

- Support fundraisers for teens in domestic violence shelters (suggested).
- Donate gifts or funds to shelters supporting older kids during the holidays (implied).
- Contribute to teen programs by donating or volunteering (implied).

### Whats missing in summary

The full transcript provides a detailed account of overcoming supply chain issues to ensure teens in shelters receive gifts, showcasing the power of community support and problem-solving in helping those in need.

### Tags

#CommunitySupport #Fundraiser #Teens #DomesticViolence #Gifts


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about supply chain issues,
unexpected twists and turns, good news that kind of
complicates things, and our fundraiser.
Give you something to put you in the holiday spirit here.
So if you don't know, you don't watch the live streams,
you may have no idea this goes on.
But every year, we do a fundraiser for teens who find themselves in a domestic violence shelter over the holidays.
We make sure that they get gifts.
We do this for teens specifically because the Marines, through Toys for Tots, they take care of the younger kids.
So, you know, we fill in the gap because older kids tend to get left out.
We've done this for years, and the pattern is the same.
You know, we do the live stream.
We call, hey, how many do you have?
We set up the bags full of goodies and deliver it,
and everything's fine.
Any excess money, they give us a list
what they want us to get.
So this year, the live stream that somebody
has aptly described as y'all trying to figure out
How many times y'all can make me say thank you and say
somebody's name while trying to fit in content.
It went perfectly, no problems.
Raised a bunch of money in a pretty short amount of time.
Everything went great.
The next day, called the shelter, maybe the day after,
either way, called the shelter, said, hey, it's Bo again.
How many teens do you have in the shelter
over the holidays?
None, I'm sorry, what?
That's great news, but what?
For context, I think last year the number was 12.
There has never been a year where there were none.
That was a new development.
Now they were nice enough to point me in the direction
of another semi-nearby domestic violence shelter
over in Pensacola, called them, and they said,
yes, we definitely have teens.
We would love this.
And it would be great if you could find us
a used PlayStation or Xbox for the common area.
And I'm like, OK, we got you.
And we're just going to get them a PlayStation or Xbox.
So we then go out and start looking to fill the bags.
Now, how this normally works when there aren't supply chain issues is I walk in and say,
hey, I need seven of this, this, this, and this, or however many we need.
And the person's like, okay, and we get them, and we pay for them, and we leave.
It's not a big deal.
When I walked in and did it this time, I was like, hey, I need seven of those.
And the person started laughing.
He was like, I got two.
And it turned into something that should have been filmed jaunt to try to find everything
that we needed.
What we normally get are tablets with accessories that go along with it, cases, earbuds, gift
cards that they can use with their tablet.
Basically it gives them a space of their own, something they have control over.
And rather than it being a one-stop shop this time, it did turn into a little bit of a jaunt,
but we eventually got all that taken care of.
We got the five requested bags for the Pensacola place, and then two additional bags that we
put together in case there are new arrivals, you know, in between now and then.
If they don't use them, they can use them for birthdays or any other holiday where gifts
are typically given, you know, doesn't matter.
And so we get that part taken care of.
And then I start looking for a console, an Xbox,
a PlayStation.
Anybody who is looking for one right now, I guess,
already knows how this went.
I'm still looking for one.
I still have five days.
As of yet, I have not located one.
But if I can't find one by the time we go drop this off,
we'll just give them a check to buy one
when they come back in stock and get some games.
So that's where we're at with that.
So everything really, despite a lot of twists and turns,
it has gone OK.
A little complicated, but it's all gotten done.
And the best part is because of y'all and your generosity
And on top of all of this, we will also be giving Shelterhouse a check for $5,000.
And that will be earmarked for teen programs of some kind.
So all told, everything went all right.
But it was definitely a more complicated journey than any other that we have done so far.
This is one of those things, and I hope it puts you in the right spirit with everything
going on.
I hope this is something that can kind of keep everybody in the right mood.
This is the type of thing that can happen when people come together.
When people decide, yeah, you know what?
That problem, we're going to fix it.
Oh, well, that problem's not so big this year.
We'll fix it somewhere else.
channel and I say this a lot but I really can't thank y'all enough. Y'all have
taken a channel that really did start as kind of a joke and turned it into a real
force. Something that accomplishes a lot throughout the year and I
always like to kind of top it off with this. So I really appreciate y'all giving
me that opportunity.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}