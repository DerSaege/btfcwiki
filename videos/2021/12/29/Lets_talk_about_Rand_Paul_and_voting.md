---
title: Let's talk about Rand Paul and voting....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5BvL2XcfU1w) |
| Published | 2021/12/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses Rand Paul's tweet discussing how to "steal an election" by increasing voter participation through absentee ballots, targeting potential voters, and counting votes.
- Beau argues that increasing voter participation is vital for a functioning democracy and not equivalent to voter fraud or stealing an election.
- He suggests that conservatives like Rand Paul fear increased voter participation because it may shift outcomes against dated policies that benefit the working class.
- Beau points out that claims of widespread voter fraud are baseless and used to suppress votes to maintain power.
- He mentions gerrymandering as a tactic used by the Republican Party to suppress votes and influence election outcomes in their favor.
- Beau criticizes the Republican Party for failing to adapt to societal changes, leading to alienation and loss of voter support.
- He explains that Republicans rely on voter suppression tactics because they cannot win elections based on fair participation due to their declining support.
- Beau concludes that the Republican Party's resistance to change and reliance on voter suppression tactics stem from their inability to win without such measures.

### Quotes

- "That's not stealing an election, that's winning one."
- "They can't win without suppressing the vote."
- "It has nothing to do with stealing an election."

### Oneliner

Beau explains how increasing voter participation is not stealing an election but a necessity for democracy, criticizing the Republican Party's reliance on voter suppression.

### Audience

Voters, Activists, Politically Engaged

### On-the-ground actions from transcript

- Increase voter participation by encouraging absentee ballots and voter registration (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how increasing voter participation is vital for democracy and criticizes the Republican Party's reliance on voter suppression to maintain power.

### Tags

#VoterParticipation #RepublicanParty #VoterSuppression #Democracy #ElectionFraud


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Rand Paul again.
We're gonna do this because he sent out a tweet.
He tweeted out an article and typed out a quote from it.
And we're gonna talk about that quote, what it means,
and then we're going to talk about why the conservatives
and Republicans in the United States tend
to view this the way they do.
So what did he say?
He tweeted out, quote, how to steal an election.
And then this is where the beginning of the quote
from the article comes in.
Seeding an area heavy with potential Democratic votes
with as many absentee ballots as possible,
targeting and convincing potential voters
to complete them in a legally valid way,
and then harvesting and counting the votes.
That's not stealing an election, that's winning one.
That's just, you know, rocking the vote,
getting the vote out, increasing voter participation.
Something that most people believe is inherent
to like a functioning democracy.
You want to have a whole bunch of people
participating in the system.
Therefore, they have faith in the system.
Therefore, the system continues to function.
And you don't have a bunch of people say,
I don't know, on the Capitol lawn.
That's what that is.
Voter fraud, these claims are baseless.
They've been litigated over and over again.
They're not real.
There's no widespread voter fraud.
There's no way to say that.
And I don't think that he actually believes that there is.
See, I think that he's very, very concerned
that if you get votes from people
who are typically non-voters,
that it will affect and change the outcome
in a way that's bad for his side,
because his side has a bunch of dated policies
that are generally bad for the working class.
Those people who have a hard time getting to vote
because they have to be at work
and they basically rely on voter suppression
to maintain their position of power.
Now, the reason I believe this is because last December,
he said, I'm very, very concerned
that if you solicit votes from typically non-voters,
that you will affect and change the outcome.
He knows that this isn't fraud.
He knows it's not stealing an election.
It's winning one.
This is why the Republican Party
is so determined to gerrymander.
They have their ideas, their aggressive nature,
failing to move forward with the rest of the world,
with society.
Those positions have alienated a lot of people.
And then through a lot of bad talking points
and political miscalculation,
they wound up getting hundreds of thousands of their voters
off the voter rolls permanently,
and they're in a bad way.
So now the Republican Party has to find some way
to gerrymander, to suppress the vote,
to affect the outcome of the election
before it happens through legislation,
because they know that if they had
widespread voter participation,
they would lose in a landslide
because their votes are not what they once were.
They don't have the support that they used to.
They have a very vocal minority of people
who support them.
And then they have a whole bunch of people
who can't make it to vote.
So making it easier for those people to vote,
to participate, to exercise their rights,
to have a voice in the government,
well, that's bad because it subverts their hold on power.
It has nothing to do with stealing an election.
It has to do with the Republican Party
refusing to grow up, refusing to move forward,
sticking with dated ideas that nobody wants.
They can't win without this.
They can't win without suppressing the vote.
They can't win if there's widespread participation.
It's that simple.
There's no voter fraud.
There's no attempt to steal an election.
That's not a thing.
They just can't win in a fair one.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}