---
title: Let's talk about information silos and theories....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PucdTO7srLg) |
| Published | 2021/12/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of information silos using the analogy of a grain silo, where information is self-contained and walled off from everything else.
- Describes the dangers of going inside a grain silo, with the risk of getting stuck due to pressure from the grain.
- Draws parallels between exploring theories and being trapped in a grain silo, where one might appear stable on the surface but have hidden dangers.
- Compares being trapped in a grain silo to being trapped in misinformation or conspiracy theories, where individuals may need professional help to get out.
- Emphasizes the importance of not going into a grain silo and avoiding getting sucked into dangerous information silos.
- Mentions the profit motive behind spreading sensationalized information, leading to more people getting stuck in harmful narratives.
- Raises questions about the abundance of information in silos and the pressure to keep the audience engaged with increasingly sensational content.
- Suggests that like being trapped in a grain silo, breaking free from harmful information silos may require intervention from those with the right tools.
- Concludes by reflecting on the challenges of navigating through information silos and the potential need for professional assistance to escape them.

### Quotes

- "Don't go into a grain silo."
- "It's a game of telephone, where the more likely your information is to cause something horrible to happen, the more you get paid."
- "You may need to call in people with the right tools."
- "Y'all have a good day."

### Oneliner

Beau explains the dangers of information silos using the analogy of a grain silo, warning against getting trapped in harmful narratives and the need for professional help to break free.

### Audience

Internet users

### On-the-ground actions from transcript

- Avoid falling into information silos (implied)
- Seek professional help if trapped in harmful narratives (implied)

### Whats missing in summary

Beau's engaging storytelling and vivid analogy make the dangers of information silos and conspiracy theories tangible, urging caution and seeking help when needed.

### Tags

#InformationSilo #Misinformation #ConspiracyTheories #Analogies #ProfessionalHelp


## Transcript
Well, howdy there internet people. It's Beau again. So today we're going to talk about
information consumption, information silos, and a little bit of farm safety.
Because I use that term information silo a lot. And I've had people ask, you know,
about the term recently. Now the reality is as soon as I heard it, I loved it.
Because I immediately drew a comparison to something else. Now come to find out
the person who came up with the term, that they may not have known that they
had stumbled on to the absolutely perfect analogy. They may have just meant
that the information was self-contained. It was walled off from everything else.
But when I heard it, I thought about a grain silo immediately. Because it's
the exact same thing. Before I go into this, for all of the reasons I'm about to
say, do not go into a grain silo. And if you are one of those people who walks
down the grain, stop. Okay, so if you don't know what one is, that playset you had as
a kid, the red barn, and then the the giant tube with the dome on top. That
giant tube with the dome on top, that's a silo. Inside of it is a grain. And up at
the top, you know, you can go in, don't, but you could go in and stand on the
grain. And it's a lot like your first glance at a theory. It looks good, looks
sturdy, looks stable, just like a theory. But you know, if you get to poking around
in that theory, you're going to find holes down below that nice crusty
veneer, right? Same thing in a grain silo. There are cavities, there are voids. And if
one of those collapses, you go down. If it's big enough, you end up going in
over your head, just like in a theory. And you're done. In a grain silo, you are done.
I want to say you have 90 seconds from the time it covers you. But that's not
the only risk. Because even if you only get down to, say, chest-deep, you're stuck.
Just like with the theory. The grain of the information, the little bits of
theory, there's so much pressure on you that you can't get out. You can't go
anywhere. You can't pull yourself out. Your friends and family and co-workers,
they can see you, but they can't really help either. Because a lot of times, even
if they were to get a rope to you, they don't have the force to get you out. And
if they do, sometimes it's more than the human body can take. Just like with
theories, it often takes professional help. And what they do is they show up and
they literally wall you off from the information, from the grain. They build
retaining walls around you to get the pressure off of you until they can get
you out. It is the perfect analogy because that's how it works. People don't
dive into these theories thinking that they're going to end up the way a lot of
people are right now. They think they're on firm ground. They're not. Something
gives way, they get sucked down, and then they're gone. And sometimes even if they
want out, they can't do it alone. And oftentimes the people around them, they
don't have the tools to help. So it has always been the perfect analogy to me.
Now, for all the reasons I just said, I'm going to say this again, don't go in a grain silo.
And there are still people, I guess, that walk down the grain, which is the
process of having the bottom open. And yeah, don't do that.
I'd point out that the trope of how dangerous farming is, in almost all
other aspects of farming, things are getting a whole lot safer except for the
silos. Stop. Okay, so that's where that term comes from. And then you have to
wonder, when it comes to the theories, why is there that much
grain in the silo? Why is there that much pressure? Because somebody else
described it perfectly recently. It's a game of telephone. A whole bunch of
different theorists, storytellers, and the goal is to make the story a little bit
more interesting, make it more appealing, make it something that people can't let
go of, so they keep coming back to your website. So it's a game of telephone,
where the more likely your information is to cause something horrible to
happen, the more you get paid. As long as that profit models around, we're going to
have a whole lot of people stuck. We're going to have a whole lot of people getting
sucked down. So that's where the term comes from, at least in my mind. Again,
come to find out the person who came up with it may not have even known that
part. One of those things where, much like in conspiracy theories, your mind
assigns meaning to something, because you see a pattern. You can
relate it to something in your own life. It happens all the time. I don't know
that there is an easy answer to this. I think it's probably for a whole lot of
people, it's going to end up like being entrapped in grain. You may need to call
in people with the right tools. You may need to call in the professionals. Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}