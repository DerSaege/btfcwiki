---
title: Let's talk about which way the Overton window in the US is headed....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=N1Bv4jW7_Ok) |
| Published | 2021/12/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of the Overton window as the range of acceptable debate on a topic.
- Republicans view the country as moving left, while people on the left see a dangerous shift towards far-right ideas.
- The discrepancy in perceptions arises from differing definitions of terms like "left" and "liberal."
- Society's thought progression tends to shift left, becoming more liberal, while the government moves right to maintain stability.
- Provides an example with the Supreme Court to illustrate the government's shift towards conservatism.
- Describes a pattern of two steps forward and one step back in societal progress.
- Anticipates that the government will eventually bounce left as societal progressivism prevails in the long run.

### Quotes

- "On a long enough timeline, the social progressive always wins."
- "Society moves forward. Thought changes. The law catches up."
- "The government is moving to the right. It is becoming more right-wing, more authoritarian."

### Oneliner

Beau explains how society's liberal shift contrasts the government's rightward movement to maintain stability, illustrating the concept through the Overton window and the Supreme Court.

### Audience

Citizens, Activists, Advocates

### On-the-ground actions from transcript

- Challenge authoritarianism by advocating for progressive societal changes (implied).
- Stay informed about societal shifts and government actions to ensure alignment with societal values (implied).
- Engage in civil discourse to clarify definitions and bridge gaps in understanding (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the Overton window, societal progression, and government reactions, offering insights into the dynamics between societal thought and governmental actions.

### Tags

#OvertonWindow #SocietalProgress #GovernmentReactions #Authoritarianism #SocietalChange


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk
about systems and society and two steps forward and one step back. We're going to
talk about the Overton window and why there's a debate as to which direction
that window is headed. Now for those who don't know, the Overton window is the
range of acceptable discussion on a topic. If you ask Republicans, ask people
on the right, conservatives, they'll say the country's moving left. The Overton
window is shifting left. If you ask people on the left, they're over there
like yeah we are moving dangerously close to some far-right ideas that are
super bad. They can't both be right, or can they? Objectively, you would think that
one has to be wrong, but that's only true if they're both using the same
definitions, and they're not. When we got that letter from Texas, we talked about
it. The terms left and liberal get conflated. Republicans could say the
country's becoming more liberal. The Overton window is shifting to become
more liberal, especially around social issues, and that would be a true
statement, but that's not what they say. They say the country's moving left. Then
those on the left, they could say, well the government is moving right. Actually
right and up. It's becoming more right-wing and more authoritarian, and
that would be true. They're not mutually exclusive. In fact, throughout history
this is kind of how it works. One of the main things that we've talked about
on this channel recently is how to change society. You don't have to change
the law. You have to change thought. Thought is shifting. It's moving left in
the way they say it. More liberal, and then the government catches up, but it's
not a rope. It's more like a piece of elastic connecting the two. What is the
government's job? To maintain stability, the status quo, to keep the system as it
is, to maintain power, because that is better for those at top, right? Stability
is good for business. So if society, if thought moves left, becomes more liberal,
what is government likely to do to try to create equilibrium? It's going to move
right. The government itself may become more right-wing, more authoritarian, which
is what's happening. Do you want an example of this playing out in front of
your eyes? Think about why everybody's talking about the Supreme Court this
weekend. They're reviewing a case, right? Super controversial, except it's not. 60
to 70 percent of people want it upheld. They want that case in place, right? But
it's kind of likely that the Supreme Court overturned previous decisions.
Moving right, more conservative, more authoritarian. It's the way it works, and
it's the way it's worked in this country a very, very long time. Two steps forward,
one step back. The government reacts to the way society thinks, tries to make
sure that it doesn't move too fast down that lane of social progress, and to be
honest, they've been really effective at it. So the reality is we have two windows.
We have what the government's doing and what the people want, and right now
they're moving in opposite directions. So those who are saying the country's
moving left, what they mean is people are becoming more liberal, and yeah, that's
true. People are more accepting of, well, pretty much everything, right? Attitudes
towards race, orientation, identity, they're all shifting, becoming more
liberal. The government is moving to the right. When did that start? I don't know.
Probably around the time corporations became people. So we can expect this to
kind of play out and continue to play out until eventually that elastic
between what the people want and the way society thinks and the government's
desire to maintain stability, it stretches to the end, and then the government
bounces left a little bit. On a long enough timeline, the social progressive
always wins. It's the way history works. There are setbacks. There always are, and
we're in the middle of one right now. But on a long enough timeline, the social
progressive always wins because it's what humanity does. So when this debate
arises, make sure that everybody's on the same page when it comes to definitions.
Everybody knows what they're actually discussing because this debate
is often people arguing over something that they would agree on pretty quickly.
The government is moving to the right. It is becoming more right-wing, more
authoritarian. Society is becoming more liberal, and in some ways there are
pockets of society that are actually moving left as well. Those two facets,
those two little weights, those poles, they're connected. Society moves forward.
Thought changes. The law catches up. But those at the top want stability. So they
may try to move the other way, try to pull people back, and that only works if
people give up. Hopefully they won't. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}