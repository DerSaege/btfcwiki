---
title: Let's talk about Burlington and LAPD....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=elLXwaz5j5k) |
| Published | 2021/12/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the incident at Burlington and the Los Angeles Police Department involving a person with a bike lock hitting people and shots being fired.
- Describes cops arriving, suspect leaving and re-entering the building to beat a woman, and cops making a messy entry.
- Points out that the cops involved were not a SWAT team but regular police officers.
- Details the officer with a rifle firing three rounds, accidentally killing a 14-year-old girl in a dressing room.
- Criticizes the media for not accurately portraying what the officers encountered and questioning the officers' behavior.
- Raises concerns about the justification for shooting someone without a gun and the lack of de-escalation tactics used.
- Suggests that while the officers may be ruled justified within policy, it doesn't mean their actions were necessary or right.
- Calls for changes in policy, more training, and a reevaluation of the use of lethal force.
- Urges people to watch the body camera footage to understand the reality of such situations.
- Warns about the dangers of using firearms in home defense scenarios, citing risks of overpenetration in household layouts.
- Questions the necessity of shooting when nobody was in imminent danger, surrounded by cops.

### Quotes

- "It expands it a little bit."
- "That's what it demonstrates."
- "It's all fun and games until it's real."
- "The rounds are going into your kids' bedrooms."
- "The question is, was it necessary to shoot?"

### Oneliner

Beau provides a critical analysis of an incident involving police shooting, raising questions about policy, necessity, and the use of lethal force.

### Audience

Community members, activists, police reform advocates.

### On-the-ground actions from transcript

- Watch the body camera footage and listen to the officer's voice (suggested).
- Advocate for policy changes and more training (implied).

### Whats missing in summary

Deeper insights into police conduct and policy implications.

### Tags

#PoliceShooting #PoliceReform #LethalForce #PolicyChange #CommunitySafety


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about what
happened at Burlington and the Los Angeles Police Department.
I haven't made a video like this in a while,
because honestly I burned out on watching the footage that it
takes to make videos like this.
This case caught my attention when I heard what happened
prior to seeing the footage, because I already
knew the point that I was going to make,
because there is an important point to make.
And we'll get to that at the end,
because the problem is what I thought
was going to be on the footage isn't what's there.
OK, if you don't know what I'm talking about,
I will run you through what's on the footage real quick.
There's a person in a store, and they are swinging a bike
lock, hitting people.
Some people start to shelter in place and hide.
Some people start to evacuate.
The cops get called about it.
Some of the calls describe what's happening.
A person with a bike lock swinging and hitting people,
and that there are people hiding and sheltering.
Some of the calls say that shots have been fired.
So cops show up.
Before they show up, the suspect has left the building,
re-entered, grabbed a woman out of an aisle,
and beat her pretty badly.
So the cops show up.
They make entry to answer a whole bunch of questions that
came in about this when it comes to the absolute mess of them
moving, and the weapons handling, and the formation,
and pretty much everything.
That's not a SWAT team.
The current theory when you're dealing
with an active situation like that
is that the first cops to show up, well, they enter.
It's a good theory.
It really is.
And so that's what you see.
Those are beat cops.
Those are just the first cops to show up.
So as they're moving in the body camera footage,
you can hear one of them, let me take point.
Let me take point.
I got the rifle.
Let me take point.
And this is the officer who eventually fires.
So they approach where the suspect and the woman
he has beat are at.
She's on the ground.
When the officer with the rifle turns to the corner,
the suspect is at the end of the aisle, not by her anymore.
He fires three rounds.
One of those rounds finds its way
into a dressing room where it kills a 14-year-old girl.
See, that's not what I expected to see.
What I expected to see was the suspect still attacking
the woman when they fired.
That's not what was on the video.
That changes the teachable moment here.
It expands it a little bit.
The media, for its part, is doing a very good job
of parroting whatever LAPD says.
CNN, in particular, ran a little segment
where they showed surveillance footage from the store.
And they're like, we're showing you this
so you can understand what the officers encountered.
No, no, you're not.
That's not what the officers encountered.
What the officers encountered is on their body camera
footage.
That other stuff, they didn't see any of that.
They didn't see any of that.
And the media is doing a very good job of repeating,
well, we heard shots fired.
We thought it was an act of shooting.
Therefore, that justifies the behavior.
Well, yes and no.
It justifies the movement, why you formed up the way you did,
why you went in with all the force.
What it doesn't justify is why you shot somebody
that didn't have a gun.
If you believed it was an act of shooting,
why did you shoot someone who didn't have a gun?
If you understood it was a bike lock,
why'd you shoot once nobody was in danger anymore?
The video doesn't actually do a great job
of exonerating the officers as far as behavior,
decision making.
To me, it raises more questions than it answers.
Now, to be clear on this, I don't expect charges
in this case.
Because while there are definitely things wrong,
it's all pretty much within policy.
And as we've talked about in other videos,
that's really what decides whether or not
an officer gets charged.
The officer is supposed to take into account civilians
that might be around.
It's going to be really hard to prove negligence in this case.
Because what occurred could occur even
in the best case scenarios.
Even if you do everything right, rounds can overpenetrate
and stuff like that.
So it's going to be really hard to prove negligence
in this case.
And it's going to be really hard to prove negligence
in the best case scenarios.
So it's going to be really hard to make that case.
But my question is, why did they fire to begin with?
What was the motivation?
Had they taken the moment to switch from kill to capture,
or the reality is at the time the shots were fired,
nobody was in danger.
The suspect could have been talked down, pepper sprayed,
tased, a whole bunch of stuff could have happened.
It didn't require that.
Now, will it be ruled justified?
Absolutely, had a weapon.
But justified isn't the same thing as necessary.
And that's probably what needs to be discussed.
Because while it's all within policy,
that doesn't mean it's right.
This shows that the policy is bad.
That's what it demonstrates.
It demonstrates that the policy is wrong.
There needs to be changes.
And there definitely needs to be more training.
One other thing, now, normally when it comes to these,
I watch these videos so you don't have to.
In this case, I think pretty much everybody
should watch the body camera footage
from the officer who fired and listen to his voice.
Listen to the change in tone from,
I've got the rifle, let me take point,
to when he sees the blood on the ground.
It's all fun and games until it's real.
I think there's probably a lot of people
that need to understand that.
The other side to this, the point
that I wanted to make initially is that there
are a whole lot of people right now who
are hanging onto those ARs so they can defend their home
against home invaders or whatever.
And I don't think they understand
that those dressing rooms are all over their houses.
The way a lot of houses are laid out today
with those open floor plans, the master bedroom,
well, that's on one side of the living area, which
is where people would enter.
On the other side of the living area are the other bedrooms.
If you fire that thing, you have over penetration or you miss,
the rounds are going into your kids' bedrooms.
It's not a game.
So again, I seriously doubt there will be charges in this.
Because by policy, that's going to be justified.
But I think anybody who watches it
is going to see a suspect feet away from anybody else,
nobody in imminent danger, surrounded and outnumbered.
The question is, was it necessary to shoot?
And I don't think anybody can honestly say that it was.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}