---
title: Let's talk about the X factor at Burlington....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Fc7XvTKfgyg) |
| Published | 2021/12/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Revisiting a video to address comments questioning the effectiveness of police training and justifying their actions in critical situations.
- People argue that cops shouldn't need training to avoid fatal outcomes and defend their actions by claiming they'd do the same.
- Emphasizing the importance of training for police to improve decision-making and reduce mistakes in high-pressure situations.
- Explaining that training is often lacking in scenarios where officers need to switch from lethal to non-lethal tactics.
- Criticizing the lack of comprehensive training programs for police, especially in scenarios involving potential use of force.
- Suggesting that training should focus on de-escalation tactics and alternative approaches to handling armed suspects.
- Advocating for the implementation of best practices to prevent unnecessary use of force by law enforcement.
- Encouraging officers to constantly reassess situations and make informed decisions based on new information.
- Arguing that the mindset of "I would have done the same thing" is detrimental and ignores opportunities for improvement through training.
- Stating that accepting fatal outcomes as inevitable without exploring preventive measures is irresponsible and callous.
- Stressing the need for police to prioritize de-escalation and conflict resolution over a militarized approach.
- Urging individuals to rethink their perspective if they believe replicating past fatal outcomes is justifiable or inevitable.
- Proposing that officers should be trained to react appropriately in situations involving armed individuals to minimize harm.
- Emphasizing the importance of training and practice in preventing unnecessary use of force by law enforcement.
- Calling for a shift in police culture towards prioritizing peacekeeping and de-escalation strategies over aggressive tactics.

### Quotes

- "If you know the outcome of this and you would have done the same thing, you're a pretty horrible person."
- "They need that practice. They need to be able to switch, switch modes."
- "You're not supposed to be that anyway. You're supposed to be peace officers."
- "You are willingly accepting that collateral when there are very, very simple steps to stop that."
- "You want to mitigate stuff like this. You want to stop it."

### Oneliner

Beau stresses the critical need for comprehensive police training to prevent unnecessary use of force and advocates for a shift towards de-escalation strategies over aggressive tactics to ensure public safety.

### Audience

Police departments, policymakers, advocates

### On-the-ground actions from transcript

- Implement comprehensive and ongoing training programs for law enforcement officers to enhance decision-making skills and prioritize de-escalation tactics (suggested).
- Advocate for the adoption of best practices in policing to prevent unnecessary use of force and prioritize community safety (implied).
- Support initiatives that focus on turning critical incidents into teachable moments for law enforcement officers to learn and improve their responses (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the importance of police training and the need for officers to prioritize de-escalation tactics over aggressive approaches to prevent fatal outcomes effectively. Watching the full transcript can provide additional insights into the nuances of these arguments.

### Tags

#PoliceTraining #DeEscalation #LawEnforcement #BestPractices #CommunitySafety


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to revisit this morning's video
to address two distinct sets of comments.
One set is basically, I don't understand
how training, more training, could have stopped this.
I'm tired of hearing about how cops need more training.
They shouldn't need training to avoid this.
And the other set is, I would have done the same thing.
I don't see how it could have been done differently.
That's how I would have reacted, which
indicates there are people who believe nothing could have been
done to alter that outcome.
Knowing the outcome, you have people saying,
I would have done the same thing.
These people are apparently unaware that best practices,
if best practices had been followed, if stuff that I'm
sure they've heard before, if it had been implemented,
it would have drastically altered the outcome.
I personally refuse to believe that a 14-year-old girl is just
the cost of doing business.
OK, so let's start with the training aspect of it.
Why do cops need more training?
Why do you want cops to have more training when
it comes to this type of stuff?
Why do you want them to be better at it?
If you are more proficient, you are less scared.
If you are less scared, your thought process is clearer.
You are less likely to make mistakes, make rash decisions,
so on and so forth.
There are those people who say, well, cops really
shouldn't need training to switch from kill to capture.
They just should never be in the kill mode.
The reality is, in a situation like the report they had,
oh, yeah, they're walking in saying,
this person is leaving horizontally.
They need to have a good enough basis of training
to be able to reevaluate as new information presents itself.
And they need to be able to make that switch.
Cops do not switch.
They don't train for that.
Go look at their little promo videos for their SWAT teams.
What are they?
They're either cops moving like operators through a building,
practicing room clearing, and they're not even shooting.
There's nothing going on inside of it.
It's just practicing the tactics.
Or it's in an actual training facility,
and there are paper targets up.
And all the cop is deciding as they move through the building
shooting stuff is whether or not that paper target is
the one that they're familiar with that's the bad guy,
or it's one of the civilian targets.
That's it.
That's the only decision that's being made.
They don't have any training that switches their mode.
Why?
Because it's expensive.
It's really expensive to do it right.
You need live people on the inside.
And they need to be going about different scenarios
when the people enter.
The thing is, if you're doing that,
you can't use live weapons.
That would be irresponsible, and eventually you
would run out of volunteers to play the bad guys.
You need simunition, miles gear, paint gun, something.
All of that costs, and most cops don't see the value in it
because their actions are always defended,
no matter what they do.
They don't see why it's necessary.
Because like some people in the comments,
14-year-old girl, that's just the cost of doing business.
Even if you do everything right, something like that can happen.
But there's a whole bunch of stuff that can mitigate
and make sure that you've reduced the possibility
of that happening.
They need that practice.
They need to be able to switch, switch modes.
I personally don't think they should have fired
in that situation.
I don't see it as necessary in the Burlington situation.
Not necessary.
But let's make it to where it is necessary.
Let's say instead of a bike lock,
the person did have a gun.
Let's just go ahead and put it in that scenario.
Everything else is the same, but the person has a gun.
Now the general populace, they're
not going to say, oh, he shouldn't have fired.
But with everything else being the same,
the same thing happens in that dressing room.
Right?
What's best practices if you encounter somebody
with a weapon, with a firearm?
You need to get off the, there are people at home right now
that just said X. Get off the X. It means you move.
Plain and simple, it means you move.
The reason is when you encounter somebody,
they've made their decision, right?
That they're going to do this action.
If you move, they have to reorient.
They have to decide on a new course of action
because you are no longer where you were.
It buys you a little time.
It messes up their OODA loop, right?
Resets it.
If you were to look on YouTube for people practicing this,
pretty much every video is the same.
The person takes a step to the left.
Why it's always the left, I don't know.
But you don't have to go to the left.
You could go to the right.
Or you could drop to the ground.
You could drop to one knee.
You could drop prone, right?
Conduct an experiment with me.
Stand up, point your finger at a picture on the wall,
a light switch, doesn't really matter.
A few feet away.
Now drop to one knee or drop prone.
Weird.
If you want to keep your finger pointed at that,
if you want to stay on target, it's now angled up, right?
So if you encounter somebody, if you encounter opposition,
and there's a high likelihood that there are civilians
behind them, dropping to the ground, dropping to one knee,
which is best practices to get off the X,
that also sends your rounds at an upward angle.
At that distance, at that close range, I don't know what,
two feet, maybe three, the rounds
are above most people's heads.
They're not in the dressing room.
They're up.
They'll miss most people because they'll continue
on that trajectory if you lose a round, if you miss.
Don't tell me that there aren't things that can be done.
There always are.
The problem is that a lot of law enforcement
wants to cast the image of being a warrior cop,
but they don't want to spend any time
on learning to be a warrior.
You're not supposed to be that anyway.
You're supposed to be peace officers.
But if you're going to cast that image
and you're going to be that gung ho,
you should follow through, become so proficient at it
that you don't do stuff like this.
Best practices suggest that when you encounter somebody
with a firearm, you move.
You get off the X. You mess up their OODA loop.
Had that been followed and they dropped
to angle their rounds up,
none of these videos would have been made.
I would suggest that you really think about your mindset
if you're one of those people that's saying,
well, I would have done the same thing.
Knowing that outcome, knowing what happened,
I would have done the same thing.
I would have reacted the same way.
You are willingly accepting that collateral
when there are very, very simple steps to stop that.
And it just requires training.
It requires practicing.
It requires taking situations like this
and turning them into teachable moments
and learning from them rather than just defending them
and saying, well, I would have done the same thing.
If you know the outcome of this
and you would have done the same thing,
you're a pretty horrible person.
You want to mitigate stuff like this.
You want to stop it.
Sheepdogs.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}