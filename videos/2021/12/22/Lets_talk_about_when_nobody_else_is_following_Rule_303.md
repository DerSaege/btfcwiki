---
title: Let's talk about when nobody else is following Rule 303....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=F5yK7s27iuw) |
| Published | 2021/12/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau receives a message with philosophical and moral questions about following a rule when no one else around honors it.
- The message raises questions about volunteering at the hospital to honor a rule (Rule 303) and potentially exposing a dying father to COVID.
- Beau talks about the importance of maximizing good even when others don't seem to care or want it.
- He addresses the immediate question of continuing to volunteer in a risky situation when caring for an immunocompromised father.
- Beau stresses the concept of integrity, doing the right thing even when nobody else is, regardless of recognition or approval.
- Taking a break is encouraged to prevent burnout and enjoy time with loved ones.
- He acknowledges the importance of perseverance in doing good, even when faced with apathy from others.
- Beau reassures that volunteers will still be needed and encourages staying true to one's values.
- The message ends with a reminder to not let the world change you, even when faced with challenges.
- Beau advocates for taking breaks but getting back in the fight to continue making a difference.

### Quotes

- "Integrity is doing the right thing when nobody else is."
- "Sometimes you're not going to be able to change the world, but you should do everything you can to make sure that the world doesn't change you."
- "It's okay to take a break, you're allowed, everybody's allowed."

### Oneliner

Beau addresses moral dilemmas, stressing integrity, taking breaks, and continuing to do good even when faced with apathy.

### Audience

Helpers and volunteers

### On-the-ground actions from transcript

- Take a break to prevent burnout and spend quality time with loved ones (implied)
- Continue volunteering if possible, but know it's okay to take a break (implied)
- Stay true to your values and continue doing good even when faced with apathy (implied)

### Whats missing in summary

Advice on balancing volunteer work with personal well-being and the importance of staying true to one's values despite challenges.

### Tags

#MoralDilemmas #Integrity #Volunteering #Breaks #CommunitySupport


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're gonna talk about a message I got
that has some philosophical and moral questions in it.
And they're worth addressing.
I'm not gonna go through the whole message
because it's really long, it's really detailed,
and it has a lot of medical information in it
about people and stuff like that.
So I'm not gonna do that.
But in relevant part, what happens to rule 303 when no one else around you honors it?
Why should you follow it when it is no longer a gateway to a stronger community because
the community itself no longer cares for its own survival?
Do I go volunteer at the hospital to honor this rule and expose my dying dad to COVID
when I'm caring for him?
To what end?
does that matter if no one cares or wants it? Do I isolate myself so that I
can care for him in spite of what's happening to the rest of the world? What
is the right thing to do when it all seems to be falling down anyway? My gut
tells me that the right way to act is to maximize good, but there doesn't seem to
be much left here, and what there is, no one else seems to want.
There's really two questions here.
The first is the immediate.
You continue volunteering in the current situation.
If you didn't pick it up from the context, her father is immunocompromised at the moment,
And continuing to volunteer may put him at a lot of risk.
So that one's pretty easy.
If you have the means, you have the responsibility.
And when you hear that, most times people think means, material stuff, money, or the
ability, skill set, something like that. But the other side to that is that if you
have the means, it means you do what you can. When you can, where you can, for as
long as you can, which means you're allowed to take a break, especially if
you're starting to burn out. And by this message, yeah, you are, even if you don't
know it. Believe me, I recognize it. There's nothing wrong with taking a break and enjoying
the time that you have with your dad. Nothing wrong with that at all.
And then the next question is,
And then the next question is,
why, why do it if nobody else is going to?
Integrity is doing the right thing when nobody else is.
Just doing the right thing when nobody else is looking.
It's doing the right thing when nobody's
gonna know you did it.
It's doing the right thing when people are gonna
know you did it, but you know they're gonna
misconstrued it and villainize you for it. It's doing the right thing, regardless of
the impression it leaves people with. So when it comes to that aspect of it, after you've
enjoyed your time, you can get back in the fight. But it's okay to take a break. It's
It's okay to take a break, you're allowed, everybody's allowed.
Believe me, in three to six months, they'll still need volunteers at the hospital.
Because you're right, there are large portions of our global community even, that don't seem
to care about their own survival.
still be needed, make no mistake about it.
And what can keep you going when you're faced with those moments where you realize you're
kind of doing it on your own or with a limited group of people?
Just know that you're still doing it.
Sometimes you're not going to be able to change the world, but you should do everything you
can to make sure that the world doesn't change you. That you don't become one of
those people who doesn't care anymore. So take your break, please, and then get
back in the fight.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}