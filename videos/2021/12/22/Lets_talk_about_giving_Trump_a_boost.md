---
title: Let's talk about giving Trump a boost....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=os2TQ1IOQrc) |
| Published | 2021/12/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump is on tour, and there are moments where he's been booed for mentioning his vaccination status.
- Trump's attempts to encourage people to get vaccinated indicate a shift in his approach, as he never really asked his base to do much beyond giving them permission to act negatively.
- The necessity for Trump to tour and push vaccination is a result of his administration's failure in managing public health, leading to a situation where the Republican Party struggles to compete without resorting to questionable tactics like gerrymandering.
- There seems to be a disconnect between the actions of the Republican Party and the interests of their voters, especially regarding public health measures and vaccination.
- Beau questions why Republicans would support a party that has lost so many voters due to their inaction and downplaying of critical issues.
- Trump's motivations for promoting vaccination now seem more self-serving, as it directly impacts him and his need to retain influence.
- The emphasis on vaccination as a life-saving measure, even if not perfect against all variants, is stressed as a critical step towards public health protection and overall benefit to the country.
- Beau calls for those who have previously followed harmful rhetoric to now take positive action in getting vaccinated for the greater good.
- There's a suggestion that had Trump been more proactive in promoting vaccination and public health measures from the beginning, his current efforts might have been more successful.
- The overall theme revolves around the consequences of political inaction and the necessity for individuals to prioritize public health for the common good.

### Quotes

- "All he did was give them permission to be their worst."
- "He's doing this because it's finally impacting him."
- "Getting vaccinated is something that will help."
- "It still mitigates a whole lot."
- "It's still very worth getting."

### Oneliner

Former President Trump's shift towards promoting vaccination reveals the consequences of political inaction and the necessity for prioritizing public health for the common good.

### Audience

Republicans

### On-the-ground actions from transcript

- Get vaccinated for the common good (implied)
- Support public health measures (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the motivations behind Trump's current vaccination efforts, urging individuals to prioritize public health and vaccination for the greater benefit of society.

### Tags

#Trump #Vaccination #PublicHealth #RepublicanParty #PoliticalInaction


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about giving former President Trump a little boost.
He's probably feeling a little down right now, so we're going to try to give him a pick-me-up.
If you don't know, he's on tour, putting on performances, going around the country.
And something that has come up is his status.
Him and Bill O'Reilly, one of the recent stops, both of them, both of them were like, yeah,
I'm double vaccinated and I got my booster.
And when he said it, when Trump admitted it, he got a little bit of a boo from the audience,
just like happened in Alabama.
And he quieted it down pretty well, but you have to think about that.
You have to think about that chain of events and what's occurring now and why they're doing
this.
Going to those stadiums that they can't fill up, encouraging people to get vaccinated.
You know, the boos, they say a lot about his base.
You know, the whole time they were acting like he was everything.
They would do anything that President Trump wanted.
But now, not so much.
Now he's getting booed.
Maybe it's because he never asked them to do anything.
All he did was give them permission to be their worst.
Now that he is asking for the slightest bit of personal responsibility, of concern for
the country, well, he's getting booed.
And the thing is, it's got to be tough on him.
It really does.
Because it's an overt admission that his administration failed so hard at managing the public health
issue that he has to go on tour trying to get people vaccinated, trying to get them
to engage in basic measures.
The rhetoric backfired to such a degree that this is now necessary.
The reason it's necessary has nothing to do with him caring about his audience.
Has to do with the fact that they're finally starting to realize the numbers that we've
been talking about on this channel for months.
The partisan divide between those being lost.
He has to acknowledge the fact that his failure of an administration, his lackluster response
to public health, created a situation where the Republican Party can't even remain competitive
unless it cheats and gerrymanders.
Unless it actively attempts to subvert the Constitution.
So he's out there trying to get people vaccinated so it doesn't get worse.
Problem is he's speaking to crowds that, well, they're not exactly full houses.
I wonder if those venues would be more full if he started that from the beginning.
If he really pushed it from the beginning.
If he said, yes, get the vaccine, wear a mask, do your part, provide for the common defense.
That's what a patriot would do.
I wonder if he'd sell more tickets today.
The reality is he's not doing this because he cares.
He's doing this because it's finally impacting him.
That's all you really need to know about former President Trump and his motivations.
He is having to go on tour and mention his vaccination status in an attempt to get people
vaccinated because he failed that badly.
Because it costs the Republicans so many voters that they have to gerrymander and cheat.
My question is, if you are a Republican, why would you want that crop to be able to retain
power?
If they're inaction, their downplaying created a situation that lost them so many voters,
they have to gerrymander to stay competitive?
I don't know that I'd want them in power.
I don't know if I could really throw my loyalty to a group like that.
And make no mistake about it.
Trump, he knew the rhetoric that was coming out from his people, those who aligned with
him, and he said nothing.
He didn't stop it.
Just like he didn't stop the spread of a lot of theories that have caused a lot of harm.
And he won't do anything about that until it starts to impact him as well.
It's the man's character.
That's where he's at.
The reality is every American needs to get vaccinated.
Everybody in the world needs to get vaccinated.
But because of our America first attitude that Biden is showing when it comes to the
vaccines, we have to get everybody here first.
Getting vaccinated is something that will help.
You know, right now people say, well, you know, it's less effective against this variant
or whatever.
Yeah.
Okay.
That's, that's, that's kind of true in some ways.
I get it.
I understand.
But it's still life saving.
It's still very worth getting.
It still mitigates a whole lot.
And your chosen leader is telling you the same thing.
It would probably do everybody a lot of good if the people who were just looking for permission
to be their worst chose this moment to actually do something to benefit the country that they
claim they care so much about.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}