---
title: Let's talk about new polling and bad news for republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GXq5OK-EBY0) |
| Published | 2021/12/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- New polling reveals trouble for the Republican Party due to a significant disparity in vaccination rates between Trump and Biden voters.
- The poll focuses on whether the government should encourage vaccination, not on mandates or policies.
- 40% of Trump voters are unvaccinated, compared to only 7% of Biden voters.
- Republicans are becoming more opposed to encouraging vaccination, with the number rising from 35% to 48% in three months.
- Democrats and independents show higher support for vaccination encouragement compared to Republicans.
- The increasing opposition among Republicans may lead to surprise victories for Democrats in elections.
- The trend of more people opposing vaccination encouragement is expected to continue.
- The disparity in vaccination rates between party supporters may impact future elections significantly.

### Quotes

- "40% of Trump voters are unvaccinated, compared to only 7% of Biden voters."
- "There are going to be surprise victories for Democrats because of these numbers."
- "It's unnecessary, and these numbers are getting bigger."

### Oneliner

New polling shows a significant vaccination divide between Trump and Biden voters, posing trouble for the Republican Party as increasing opposition may lead to surprise victories for Democrats.

### Audience

Voters, party supporters

### On-the-ground actions from transcript

- Encourage vaccination within your community (suggested)
- Support positive messaging around vaccination (implied)
- Advocate for bipartisan efforts to increase vaccination rates (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the impact of vaccination attitudes on political parties, urging for a shift towards bipartisan efforts for increased vaccination rates.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about some new polling
and why the numbers spell serious trouble
for the Republican Party.
Even though the polling, it's not about candidates,
it's not about policy, it's not about party,
it's about something else.
But there's definitely a political divide.
It's not a political poll, it shouldn't be.
But it's clear that the political divide is there
and it's gonna cost Republicans big.
Okay, so what's the poll about?
Whether or not people believe
that the government should encourage vaccination.
We're not talking about mandates right now,
not talking about anything like that.
We're talking about like positive messaging.
Go get your shot type of thing.
As it stands, according to the new Yahoo YouGov poll,
40% of people who voted for Trump are unvaccinated
compared to 7% of Biden voters.
That's huge.
That is a massive disparity.
And given the fact that we are going to end up losing
a couple hundred thousand more people before the next elections,
this is going to impact the Republican Party.
There are places where just this statistic alone
will swing elections.
The real trouble for the Republican Party isn't this.
It's not this part of it, it's something else.
Three months ago at the start of Delta,
only 35% of Republicans were opposed
to encouraging vaccination.
Okay.
According to the new poll, that number has climbed to 48%.
They are becoming more entrenched in their position,
which means it's gonna be harder for Republicans
to turn the tide on this,
which means it's going to increase that disparity
when it comes to those lost and which party they belong to.
Now, while Republicans are...
It's being measured in opposing it,
the rest of the country,
85% of Democrats are in favor of encouraging vaccination,
60% of Americans in general, and 59% of independents.
This is going to be a huge line.
This is going to be one of those things
that causes polling to be off
because the polls are going to say,
hey, the Republicans are ahead by a full point
based on an extrapolation from the survey
and a lot of those people won't be voting
because they're not around.
This is going to swing elections.
I'm not about doing the Republican Party any favors,
but I do disapprove of waste, and that's what this is.
It's unnecessary, and these numbers are getting bigger.
The trend is for more people to oppose it as time goes on,
even though the numbers are continuing to climb.
Make no mistake about it.
There are going to be surprise victories for Democrats
because of these numbers.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}