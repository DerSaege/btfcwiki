---
title: Let's talk about Republicans winning over Biden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HJEB0SH62O0) |
| Published | 2021/12/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans are celebrating their victory in causing the Biden administration to withdraw from settlement negotiations with families separated at the border.
- Biden's decision to pull out was influenced by the Republicans making the position untenable due to potential costs.
- The purpose of settlements is to avoid further embarrassment and for families to accept less money than they might receive in court.
- With cases now going to trial, criminal actions may be uncovered, prolonging public scrutiny on the treatment during the Trump administration.
- Biden's move to pull out might lead to families receiving more money through trials than the $400,000 settlements Republicans were concerned about.
- Republicans unintentionally ensured that families get paid more and that their treatment remains a public focus, contrary to their initial goal.
- Beau criticizes Biden's decision and suggests the Department of Justice resume negotiations to avoid lengthy court battles for families wanting to move forward.
- The government's legal strategy might involve admitting it can't win in court, prompting quicker compensation for affected families.
- Beau believes that families willing to settle seek closure and the government should respect their wishes to move on.

### Quotes

- "Republicans won by ensuring that the families of those separated at the border will be paid more and there will be constant public discussion of their treatment."
- "The purpose of a settlement is to avoid further embarrassment and to get the families to accept less money than they would get if they went to trial."
- "I think Biden is wrong for this."
- "If the families were willing to settle, it's because they want to get on with their lives."
- "Y'all have a good day."

### Oneliner

Republicans celebrate as Biden pulls out of settlement negotiations, leading to potential trials uncovering more and families receiving increased compensation, all against the intended goal. Biden's move may prolong public scrutiny on Trump-era treatment at the border, prompting criticism and calls for DOJ to resume negotiations. Families seeking closure face uncertain court battles ahead.

### Audience

Advocates, Activists, Voters

### On-the-ground actions from transcript

- Resume settlement negotiations to provide closure for families seeking to move on (suggested)
- Explain to the American people the reasons behind settlement negotiations and the challenges faced in court (suggested)

### Whats missing in summary

Insights on the potential long-term impacts of Biden's decision and the necessity for transparency in legal processes.

### Tags

#Immigration #SettlementNegotiations #BidenAdministration #RepublicanStrategy #PublicScrutiny


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about Republicans taking a well-earned victory lap
because they succeeded.
They won.
They got Biden to cave.
And they're going to celebrate it.
And they earned it.
They really did.
If you don't know what I'm talking about,
the Biden administration has decided that it would be in their best interests
to pull out of settlement negotiations
with the families of those people who were separated at the border.
The reason he's doing this is because Republicans succeeded
in making that position untenable.
Making those payments, well, that was going to cost him.
So he backed out.
The administration backed out of these settlement negotiations.
Republicans are cheering.
They have won, and they have.
That was their goal.
They wanted to stop the settlements.
And I do believe that Biden was wrong for pulling out like that.
I think that was a bad move.
But now let's talk about what happens next.
If you are currently running a victory lap, you better slow down.
Grab some water or something.
Now the cases go to trial where the plaintiffs get discovery.
And more than likely, criminal actions will be uncovered.
But what this ensures is that there will be headlines for years to come
talking about the treatment that occurred under Trump.
That's what's going to happen because of the constant court cases
that could have been settled.
Speaking of which, do you know the purpose of a settlement?
The reason the government was willing to settle
is because it can't win in court.
What happened?
It happened.
Trump bragged about it.
It happened.
It occurred.
They can't win.
The purpose of a settlement is to avoid further embarrassment
and to get the families to accept less money
than they would get if they went to trial.
That's not going to happen now.
The families will go through trial,
and then they will be awarded more than the $400,000 payments
Republicans were freaking out about.
And this way, Biden doesn't take the hit,
doesn't take the political hit.
Now, I don't think that that was some noble gesture by Biden.
I think that he didn't want to take the political hit,
decided to let the courts deal with it,
and figured, hey, they're going to get more money anyway.
I think these people would like to get on with their lives.
I don't think they want to spend the next year and a half in court.
But that does appear, at least for the moment,
like that's what's going to happen.
Republicans won by ensuring that the families of those
separated at the border will be paid more
and there will be constant public discussion of their treatment.
Probably not what they planned.
This is the danger of bumper sticker politics.
This is the danger of repeating a sound bite over and over again.
I do, all jokes aside, I think Biden is wrong for this.
I think Biden is wrong for this.
They want to get on with their lives.
I think that DOJ should resume negotiations,
and it might be wise to get out there and explain to the American people
why the settlement negotiations are occurring,
and just say flat out, we can't win in court.
I understand that that's not a great legal strategy,
because if it goes to court, you've said that.
But the reality is the government can't win in court on this.
The payments are going to be made.
Compensation will be made.
It's just a matter of time and how much now.
If the families were willing to settle,
it's because they want to get on with their lives.
I would suggest the government let them do that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}