---
title: Let's talk about something good from something bad....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2iGOJj1Ehe0) |
| Published | 2021/12/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the idea of discussing good news for a change, especially when it stems from something negative.
- He expresses a preference for good coming from bad rather than good from good, as it signifies real progress and change.
- Beau mentions the origin of the term "rule 303" and how it was transformed into something positive by people, signifying a change caused by collective action.
- There have been recent developments in the culture war surrounding statues in the United States.
- The offensive statue of Nathan Bedford Forrest in Nashville, covered in graffiti, has been removed.
- The controversial statue of Robert E. Lee in Charlottesville, a significant source of tension, has also been taken down.
- The government has decided to melt down the Robert E. Lee statue and turn it into a piece of public art at the Jefferson School African-American Heritage Center under the swords to plowshares proposal.
- Beau finds this transformation of the statue into public art fitting and a positive step towards change.
- He acknowledges that while some may have issues with this decision, he personally sees it as a beautiful and wonderful idea.
- Beau views this transformation as a small step in changing the southern United States and sees it as more than just political pressure but a significant message of impending change.

### Quotes

- "Something that's good creating good offspring."
- "I think it's beautiful. I think it's a wonderful idea."
- "It sends a pretty direct message that change is coming."

### Oneliner

Beau talks about transforming offensive statues into public art as a significant step towards change, sending a direct message of progress and transformation in the southern United States.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Support and advocate for the transformation of offensive statues into public art (exemplified)
- Engage in dialogues and actions that contribute to positive change in your community (implied)

### Whats missing in summary

The emotional impact and personal reflection that Beau shares throughout the transcript are missing in the summary. These elements can provide a deeper connection to the message of transformation and progress. 

### Tags

#PositiveChange #Statues #CultureWar #Transformation #CommunityProgress


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to
talk about some good news for a change.
I love it
when something good
comes from something bad.
I actually like that more than when something good
comes from something good.
Something that's good creating
good offspring.
Well I mean that's just
the status quo.
But something
good coming from something bad,
that's real change, that's real progress.
This is one of those things that came up recently
because somebody was like,
you know I just found out the origin
of the term rule 303.
That's got kind of a messed up origin. And I'm over there like, kinda? No, it's got a
really messed up origin.
And it became a good thing
because of y'all.
When y'all heard the term,
y'all made it that.
Positive change in the world.
So
to me,
I've always enjoyed
watching something transform
from something bad to something good.
Now, the
culture war
that has been going on
over statues in this country,
there's been a couple of developments.
One
is that truly offensive statue up there in Nashville. And by offensive I mean
not just the subject matter,
but also the way it looks. It was just
horrible.
Of Nathan Bedford Forrest,
yeah, that's been removed.
And it's worth noting that it was covered in graffiti
at the time it was removed.
Now,
when it comes to the culture war over statues,
the big one
is that statue of Robert E. Lee
in Charlottesville.
That was the one that created the most tension,
caused the most problems.
It was taken down as well.
That happened a while back.
But the
government just decided what they're going to do with it.
It's going to be melted down
and turned into a piece of public art
by the Jefferson School
African-American Heritage Center.
That seems incredibly fitting.
And it's under their
swords to
plowshares proposal.
Something good from something bad.
I think it's fantastic.
And I'm sure there are going to be some people
that have an issue with this.
I don't. I think it's beautiful. I think it's a wonderful idea.
It's another small step
in
the transformation of the southern United States.
This isn't just removing it
out of political pressure,
transforming it
in this way.
It sends a pretty direct message
that change is coming.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}