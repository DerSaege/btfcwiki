---
title: Let's talk about what Republicans get wrong about guns....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eXKxSI8mnIQ) |
| Published | 2021/12/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques a tweet from Madison Cawthorne about the Second Amendment and Australia.
- Points out the misinformation and misunderstandings in the tweet.
- Explains that leftists are not out to take guns from people.
- Mentions that it was conservatives, not leftists or liberals, who implemented the gun ban in Australia.
- Debunks the myth that Australians had all their guns taken away after the ban.
- Emphasizes the importance of understanding the truth rather than relying on manipulated talking points.
- Indicates that the Republican establishment may prefer to keep their voters ignorant and easy to manipulate.
- Stresses that the whole theory about needing the Second Amendment to protect freedom is manufactured and not based on facts.
- Encourages Republicans to pay attention to the manipulation happening within their own party.
- Summarizes by reiterating that leftists don't want your guns and that the misinformation surrounding the Second Amendment needs to be addressed.

### Quotes

"Leftists don't want your guns. That's not a thing. That is made up. That's not real."

"Australia today has more guns than it did before Port Arthur, before the ban."

"Conservatives took guns in Australia. Not liberals, not leftists."

"The whole theory, this whole talking point, it's a straw man that was created for ignorant people to use."

"May God have a good day."

### Oneliner

Beau exposes the inaccuracies in a tweet about the Second Amendment and Australia, clarifying that leftists don't aim to confiscate guns and debunking myths propagated by the Republican Party.

### Audience

Republicans, Gun Owners

### On-the-ground actions from transcript

- Fact-check information shared by political figures (implied)
- Educate yourself and others on the reality of gun control and leftist beliefs (implied)
- Challenge misinformation within your political circles (implied)

### What's missing in summary

In-depth analysis of the manipulation tactics used by political parties to influence public opinion on gun control and leftist ideologies. 

### Tags

#SecondAmendment #GunControl #RepublicanParty #Leftists #Misinformation


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a tweet from Madison Cawthorne.
We're going to do this because
honestly I don't think I've ever seen so much wrong packed into such tight space before.
But aside from that,
it also kind of encapsulates everything that
the Republican Party has wrong right now.
Everything that's going on that's wrong,
from their talking points
to the way the leadership tries to motivate their base.
And in this case, it's about something that the Republican Party holds dear.
It's about something they try to create single issue voters about.
So today we're going to talk about what the Republican Party gets wrong
about the firearms debate in this country.
Here's the tweet.
Next time a leftist asks you why we need the Second Amendment,
say Australia and walk away.
Okay, whatever.
I mean, I guess.
The intent of this tweet is to hand out a talking point.
And that talking point is that if you're confronted by a leftist
who want to take your guns, you should bring up Australia.
Okay, but why?
Because it doesn't make any sense.
Let's start with this.
Leftists, generally speaking,
could be seen as people who support Marx,
like Karl Marx.
Now that's not entirely accurate,
but it's far more accurate than the working definition
that most Republicans have.
Marx was famously against firearms.
He's the person that said,
there's no reason why on the street today,
citizens should be carrying loaded weapons.
Right? No, that's Ronald Reagan.
Karl Marx said under no pretext should arms and ammunition be surrendered.
And he went on to explain that he means under no pretext
and that you stop it no matter what.
Leftists are not out for your guns.
That's not a thing.
That is made up.
That's not real.
I think the easiest way to demonstrate this is that most gun owners
who are watching this video,
you probably know what the NRA is, right?
Google the SRA.
Leftists don't want your guns.
That's not a thing.
That's something that, well, the Republican leadership made up.
They lied to you. They manipulated you.
They tricked you.
So the whole idea doesn't make sense,
starting with that premise.
But sure, maybe we can just assume that Madison Cawthorn is ignorant
and doesn't know the difference between a liberal and a leftist.
Okay? I mean, it's fair.
We can give him that mistake, right?
But why does he say talk about Australia?
Because they had the gun ban there after Port Arthur, right?
And obviously it was leftists or liberals who did that.
No, it was conservatives.
It was conservatives who did that.
It wasn't liberals. It wasn't leftists.
It was the conservative government.
Now, in and of itself, that should stop this whole thing.
But let's go a little bit further with it.
The whole idea of bring up Australia is that they had the gun ban.
They had the guns taken away.
That is what is portrayed in the American mind when it comes to Port Arthur.
What if I told you there were more guns in Australia today
than there were before the quote ban?
Because there are.
There are. It's been that way for a few years.
The idea that the Australian people were totally disarmed is made up.
It's manufactured. They lied to you.
They are playing on your ignorance.
It's just not real.
But that's what they want their talking point based on.
And it makes sense because if you were to say this to a leftist
who knows anything about Australia,
yeah, you definitely want to walk away afterward.
And the person who handed you that talking point wants you to walk away too.
Because they don't want you to know the truth.
Now, so we have covered the fact that the idea that
Australians had their guns all taken away.
That's not true.
We've covered the fact that leftists don't want your guns.
That's not a thing.
Why do Americans believe they need the Second Amendment?
To protect the freedom, right?
To protect the freedom. The freedom they don't have
down there in Australia because they don't have guns even though they do.
The Cato Institute, I don't think many people would call that a leftist organization,
releases an index,
rankings of countries
based on human freedom.
Australia's five.
I don't know what the United States is because it's not even in the top ten and I didn't go look.
It's made up. It's manufactured.
This whole theory, this whole talking point,
it's a straw man
that was created
for ignorant people to use.
And I don't mean ignorant as an insult. I mean it as
uneducated as to the topic.
But the Republican establishment at this point,
they want their voters ignorant.
They want them uneducated.
They want them easy to manipulate.
And now they're even doing it with
causes that are
seen as Republican staples.
You know, they've done this with a lot of other stuff for a long time,
but now they're attempting to do it with the Second Amendment
and just make stuff up about it.
That's something
Republicans might want to pay attention to.
I mean I can't imagine what would be next.
But in short,
leftists don't want your guns.
Not a thing.
Conservatives took guns in Australia.
Not liberals, not leftists.
Australia today has more guns
than it did before Port Arthur,
before the ban,
and, well,
Australia has a higher freedom ranking.
So,
if you were to tell a leftist
Australia and walk away,
they'd be pretty confused.
Anyway,
it's just a thought.
May God have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}