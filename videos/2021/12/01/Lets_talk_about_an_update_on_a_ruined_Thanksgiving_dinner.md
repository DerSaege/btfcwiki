---
title: Let's talk about an update on a ruined Thanksgiving dinner....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9lBBtW5883o) |
| Published | 2021/12/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Providing an update on a Thanksgiving dinner where a woman sought help for ruining the dinner by making the case for reparations.
- The woman stuck to Beau's formula and engaged in a heated exchange with Uncle Bob.
- Uncle Bob brought up Biden's settlement payments, triggering the woman to change the topic to Agent Orange compensation.
- Uncle Bob was personally affected by Agent Orange and became defensive during the argument.
- The woman mentioned reparations for slavery and segregation, causing Uncle Bob to react negatively.
- Grandma intervened, shutting down Uncle Bob's offensive comments.
- Despite tension, dinner proceeded smoothly with a large table and plenty of food.
- The woman confronted Uncle Bob about his behavior, leading to a reflective moment for him.
- After the dinner, the woman expressed regret for causing a scene but was affirmed by Grandma for standing up for her beliefs.
- The woman reflected on the incident and received praise from Grandma for her actions.

### Quotes
- "I had spent time arguing with somebody. I should have just slapped the second he called me a B word."
- "Never apologize for standing up for what I believe in."
- "Those hateful people have changed you. I like grandma."
- "Tell the internet people I said hi and that it didn't ruin Thanksgiving."
- "It might have been the best one I've ever had."

### Oneliner
Beau provides an update on a Thanksgiving dinner where a woman stands up for reparations, leading to a confrontational yet insightful family exchange.

### Audience
Family members

### On-the-ground actions from transcript
- Stand up for what you believe in, even if it causes tension (exemplified)
- Shut down offensive or disrespectful comments in your presence (exemplified)
- Engage in difficult but constructive dialogues with family members (exemplified)

### Whats missing in summary
The full transcript offers a nuanced look at navigating challenging family dynamics while standing up for beliefs.

### Tags
#Thanksgiving #FamilyDinner #Reparations #StandingUp #ChallengingConversations


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to provide a little update on a Thanksgiving dinner.
The woman whose uncle called her princess and a stupid girl provided us with an update.
Now I'm just going to tell the beginning of the story and then I'll directly read the
end for you.
But if you don't know what I'm talking about, a woman sent in a message asking for help
with ruining Thanksgiving dinner.
She wanted to make the case for reparations and I gave her a formula to try to make that.
She more or less stuck to it.
She said as soon as she showed up and walked in that Uncle Bob started in on her asking
her what she thought about Biden's settlement payments down on the border.
She didn't take the bait because that wasn't part of the plan.
So her and her cousin went over to her dad's house who is right next door to grandma and
they hung out over there for a while and she told her cousin what she was planning.
And then when she went back over she found Uncle Bob and said hey you know what I've
kind of changed my mind on government spending and started talking about Agent Orange and
how she didn't think that those benefits, those payments, that compensation should go
to the survivors.
Now that hit a little bit closer to home than I had actually planned.
It wasn't something that he was just aware of.
Apparently one of his friends was lost to Agent Orange and the payment compensation
went to the spouse just before she passed.
So he went off on her and berated her and basically said that if it wasn't for that
payment well those kids that they'd be in a real bad way.
And she said well you know they could just pull themselves up by their bootstraps.
Which went over exactly as you would imagine.
She said she was entitled, ungrateful, and spoiled and called her a female dog.
At which point grandma walked in the room and sat down in a chair.
Now the author of our story, out of respect for grandma, stopped talking.
Uncle Bob did not.
Uncle Bob went off about how she didn't understand anything because she was part of
the younger generation that had everything handed to them and doesn't understand that
it can take 20 years with everything going right to get out of poverty.
At which point she deviates from the script a little bit and just says good, now do reparations
for slavery and segregation.
Uncle Bob turned red.
Uncle Bob did not like this at all, did not like having his own words used against him.
And he went on a little rant that ended with asking her if she was really comparing American
soldiers to lazy black people.
Only he didn't say black people.
Grandma shot up out of her chair and informed him that he was not going to talk like that
in her presence, didn't matter how old he was.
And that this conversation was over until after dinner.
So they went to their separate corners and just kind of stayed out of each other's way
until dinner.
They had dinner and everything was fine.
Nice big table, lots of food, past the gravy and all of that.
And then she pushed her plate away and asked him if he knew why what I said bothered him
so much.
He said no ma'am, very politely, because he was going to play the victim.
She said it's because she used your own words to shove a mirror into that ugly face of yours
and you didn't like what you saw.
You haven't been happy or done anything since you left the army and you need to look down
your nose at people to feel better.
And those idiots on TV will always tell you who you're better than as long as they can
sell you something else along with it.
Those hateful people have changed you.
I like grandma.
Then she turned to me and asked if I knew what I'd done wrong.
I said no.
She said I had spent time arguing with somebody.
I should have just slapped the second he called me a B word.
Then she got up from the table and went to the living room without another word.
I felt bad afterward for causing a scene and I told her I was sorry.
She told me to never apologize for standing up for what I believe in.
Then she told me she was proud of me.
I don't think she'd ever said that to me before.
My cousin told Uncle Bob, she actually wrote Uncle Bob here, that I had asked someone for
help in planning how to make the argument.
He told her I was a devious B word.
I'll take that over stupid girl any day.
Tell the internet people I said hi and that it didn't ruin Thanksgiving.
It might have been the best one I've ever had.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}