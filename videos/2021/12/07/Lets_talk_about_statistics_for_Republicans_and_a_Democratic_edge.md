---
title: Let's talk about statistics for Republicans and a Democratic edge....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=F-bpASAVEPU) |
| Published | 2021/12/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The vaccination status is most influenced by political affiliation.
- Counties with higher vaccination rates tended to vote for Biden, while those with lower rates voted for Trump.
- Republican-leaning counties have significantly lower vaccination rates compared to Democratic-leaning counties.
- The Democratic Party has a much higher vaccination percentage compared to the Republican Party.
- Unvaccinated individuals are significantly more likely to contract and not recover from COVID-19.
- The gap in vaccination rates between Democrats and Republicans results in a higher number of Republican deaths.
- These statistics could impact election outcomes in tightly contested areas.
- Misinformation allowed by the Republican Party has led to these concerning numbers.
- Beau hopes Republicans question other misinformation spread by their party.
- Encourages everyone, regardless of political affiliation, to get vaccinated.

### Quotes

- "Your political party is the most important factor in determining whether or not you are vaccinated."
- "If you are unvaccinated, you are 5.8 times as likely to get it. You are 14 times as likely to not recover from it."
- "Most of those lost will be Republicans."
- "Misinformation allowed to circulate by the Republican Party is costing their own constituents."
- "Anyway, go get vaccinated."

### Oneliner

The influence of political affiliation on vaccination rates and COVID-19 outcomes reveals stark disparities and potential electoral impacts, underscoring the consequences of misinformation spread.

### Audience

Republicans, Democrats, All

### On-the-ground actions from transcript

- Get vaccinated (exemplified)

### Whats missing in summary

The full transcript provides detailed insights into the correlation between political affiliation, vaccination rates, and COVID-19 outcomes, urging reflection on the impact of misinformation and the importance of vaccination.

### Tags

#Vaccination #PoliticalAffiliation #Misinformation #COVID19 #Elections


## Transcript
Well, howdy there, internet people.
It's Bo again.
If you're a Republican, you need to watch this whole thing.
Today, we're going to talk about statistics at the county level
and what those statistics are leading to
and how those statistics are going
to give the Democratic Party a massive edge in tightly
As of right now, the single most important factor in determining your vaccination status, whether you are vaccinated or
not vaccinated, is your political affiliation.  Not age, not race, not level of education, not insurance status, none of
that stuff.  Not age, not race, not level of education, not insurance status, none of that stuff.
Your political party is the most important factor in determining whether or not you are vaccinated.
And this is incredibly consistent.
In areas that had higher vaccination rates, those counties voted for Biden at a higher percentage.
percentage. In counties that voted for Trump at a high percentage, they have low vaccination
rates. It is incredibly consistent. This is something we've talked about on the channel
before, but now we have a little bit of new information. We're going to talk about the
The rates per 100,000 of those lost, not cases, those that are gone.
If the county voted 60% in favor of Trump, the rates of those lost are 2.7 times as high
as a county that voted 60% for Biden.
If you go to the top 10% of counties that are Republican-leaning and compare those to
the top 10% of counties that voted for Biden, right?
Those rates are six times as high for Republicans, for Republican-leaning counties.
This is consistent across the country.
The Democratic Party is roughly 91% vaccinated.
The Republican Party is roughly 59% vaccinated.
If you are unvaccinated, you are 5.8 times as likely to get it.
You are 14 times as likely to not recover from it.
Think about those 60% numbers.
2.7 times as high.
Imagine what it is at 51% Republican.
It's going to be just a little bit higher, just a little bit
more likely.
However, because we can tell there
is a gap between Democrats and Republicans,
most of those lost will be Republicans.
If you are a Democrat running in a tightly contested area, congratulations, you're probably
going to win.
This is producing numbers large enough to swing elections.
So if you've ever watched this channel before, you know that the Republican Party is not
a party that I am particularly fond of at the moment.
So why am I saying this?
reasons. One, I don't like the idea of losing anybody to something that can be
mitigated. That these numbers shouldn't exist. These numbers exist because of
misinformation that the Republican Party allowed to circulate, that they didn't
counteract, that they didn't put out counter messaging against, that they
just let it roll right and it is costing their own constituents. That to me is
pretty unacceptable. I don't like waste and this is a giant waste. The other
reason is that I'm hoping that if you're a Republican and you can acknowledge
that the Republican Party allowed this misinformation to circulate the
way it did, even with these effects, that you might start to question, well, what else
are they lying to me about?
And if you look into it, you're going to find out it's a lot.
Anyway, go get vaccinated.
It's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}