---
title: Let's talk about altering the Supreme Court....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MDBaOIcUtt0) |
| Published | 2021/12/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about an expected decision by the Supreme Court and the response to it.
- Receives a message urging the audience to drop the idea of expanding or reducing the Supreme Court.
- Argues that the Court should be impartial and non-partisan, not a political tool.
- Questions the timing of cases coming up now, linking it to new Republican-appointed justices.
- Expresses concerns about the Supreme Court losing its institutional integrity.
- Believes that the Supreme Court became a political tool when unqualified nominees were confirmed.
- Criticizes Republicans for wanting to use the Court for political gain while expecting Democrats to respect it.
- Challenges the notion of recent constitutional changes that could warrant overturning precedents like Roe v. Wade.
- Suggests that the Trump administration undermined various institutions, including the Supreme Court.
- Advocates for replacing all Supreme Court justices to restore impartiality.
- Foresees potential backlash if Roe v. Wade is overturned or reduced, given public support for upholding it.
- Points out that state-level cases may arise due to trigger bans already in place.
- Comments on how Republican actions could inadvertently boost voter turnout for Democrats in upcoming elections.
- Asserts that the Supreme Court's claim of being impartial is no longer valid, advocating for its alteration irrespective of the decision.

### Quotes

- "The institutional purity is gone."
- "You made it a political tool. It's going to be one now."
- "Congratulations."
- "I think that at this point, we probably just need all new justices."
- "The Supreme Court ceased to be the institution you are describing."

### Oneliner

Beau questions the impartiality of the Supreme Court, advocating for the replacement of all justices to restore integrity and non-partisanship.

### Audience

Political activists, concerned citizens

### On-the-ground actions from transcript

- Advocate for judicial reform by supporting initiatives aimed at altering the structure of the Supreme Court (implied).
- Get involved in local politics and elections to influence the appointment of judges and justices (implied).

### Whats missing in summary

Insight into the potential consequences of a politically charged Supreme Court decision and the importance of public engagement in judicial matters.

### Tags

#SupremeCourt #JudicialReform #PoliticalActivism #Partisanship #RoevWade


## Transcript
Well, howdy there internet people. It's Beau again.
So today we're going to talk about an expected decision by the Supreme Court,
the response to it, and what the Supreme Court is supposed to be.
I got this message.
Will you please tell your audience to drop the idea of expanding or reducing the Supreme Court?
The Court is an impartial, non-partisan entity and shouldn't be used as a political tool.
The overturning of Roe v. Wade shouldn't cause a political reaction from liberals.
This is an institutional argument.
You have to respect the institution.
It exists outside of politics, so on and so forth.
And truthfully, that's the way it's supposed to be.
It is supposed to be like that.
But see, I've got a lot of questions.
Why is it, again, that all these cases are coming up now?
Because there are new Supreme Court justices appointed by Republicans, right?
Seems pretty partisan.
Seems pretty partisan.
Let's be clear on something.
I don't think that they should expand or reduce the size of the Supreme Court based on what
happens in this case and whether or not that's overturned.
I think they should do it anyway.
The Supreme Court ceased to be the institution you are describing when a nominee for a lifetime
appointment at the Supreme Court was asked during their confirmation hearings what the
First Amendment was and couldn't answer, and then Republicans confirmed them anyway.
At that point is when it became a political tool.
That's when it lost all pretext of being anything other than that.
What you're asking for is for Republicans to be able to use it as a political tool,
but Democrats have to respect the institution.
I don't think that's going to work.
Again, what's the job of the Supreme Court?
To interpret the Constitution, right?
What constitutional change has occurred since Roe v. Wade to now that is going to allow
it to be overturned?
Nothing right?
Just the feelings of the judges?
Just the political leanings of the judges?
Now, Republicans chose to go this route.
This is yet another institution undermined by the Trump administration and those who
enabled him.
I think that at this point, we probably just need all new justices.
If you want to get back to that idea of it being an impartial institution that is nonpartisan,
you're probably going to have to get rid of everybody.
You're probably going to have to start fresh.
These cases were brought for partisan reasons at this time because there were partisan judges appointed.
You don't get to complain about what Democrats are going to do in response.
You made it a political tool.
It's going to be one now.
Congratulations.
As I have a feeling that this is likely to backfire because the reality is that there
isn't a real basis to change the ruling.
The reality is that an overwhelming majority of Americans want Roe v. Wade upheld.
So if it is overturned, if it is reduced in scope, it's going to probably upset a lot
of people, especially when all of those trigger bans, the laws that were put on the books
in case this happens, in case Republicans did get a majority and were able to make it
political, those laws are already on the books.
So once those bans go into effect, there's going to be a whole bunch of state-level cases
about it, right?
If I was the Democratic Party, I mean, if you really think about it, when are those
cases going to start making their way through the courts?
Right about midterms.
The Republican Party has done an outstanding job of providing a really good way to improve
voter turnout for the Democratic Party.
Now, the argument about the Supreme Court being an impartial institution exempt from
partisan politics, that ship has sailed.
When you had justices who said, oh, this is settled law, when you had justices get confirmed
without really showing that they should be on the bench, because they had the right political
leanings, because people assumed what they would do in this case for political purposes,
and that's why they got appointed, you don't get to say, oh, don't make it political.
I am in favor of altering the Supreme Court now, regardless of the decision.
The institutional purity is gone.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}