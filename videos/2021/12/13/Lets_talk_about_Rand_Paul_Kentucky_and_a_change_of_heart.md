---
title: Let's talk about Rand Paul, Kentucky, and a change of heart....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KVGkFqeGDPE) |
| Published | 2021/12/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Rand Paul sent President Biden a letter asking for emergency relief funds for tornado disaster in Kentucky.
- Paul has a history of opposing such funding and has voted against it multiple times.
- Beau generally doesn't criticize someone who changes their wrong decisions to right ones, but...
- Senator Rand Paul's past actions contradict his recent statement about uniting to help and heal after disasters.
- Beau believes in giving people the benefit of the doubt but questions Paul's sincerity based on his history.
- Beau finds relief work after disasters transformative and hopes people understand Paul's prior stance against such relief.
- He shares a lawyer's insight that people change their position on issues like law and order only when they are personally affected.
- Beau hopes Senator Paul's recent actions indicate a genuine change in position for future disaster relief votes.

### Quotes

- "Politicizing that suffering would be low for even the deepest partisan."
- "Everybody's for law and order until they get caught."
- "I hope I'm wrong."

### Oneliner

Senator Rand Paul's history of opposing disaster relief funding raises doubts about his recent change of heart, prompting Beau to question his sincerity and hope for consistency in future votes.

### Audience

Kentuckians, voters

### On-the-ground actions from transcript

- Contact Senator Rand Paul's office to express support or concerns about his stance on disaster relief funding (suggested).
- Participate in local disaster relief efforts to help those affected by natural disasters in Kentucky (implied).

### Whats missing in summary

The full transcript provides deeper insights into the dynamics of changing political stances in response to disasters and the importance of consistency in policy decisions.

### Tags

#SenatorRandPaul #DisasterRelief #Kentucky #PolicyChange #Consistency


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we are going to talk about Senator Rand Paul and changes of heart and what happened
up there in Kentucky.
And to be honest, I had no intention of talking about this.
If you don't know what's going on, Senator Rand Paul sent President Biden a letter asking
him to do something he was going to do anyway.
Asking him, hey, can you approve emergency relief funds because of the disaster, the
tornadoes?
And he took a lot of heat for that because Senator Rand Paul has kind of a lengthy history
of actively opposing this kind of funding.
He's voted against it more than once.
I wasn't going to talk about it because generally speaking, if somebody is making the wrong
decision over and over and over again and then they change and start making the right
one, I don't want to come down on them for it.
If he had said nothing, I'd have thought nothing.
But see, Newsweek asked him about it.
Kentuckians across the Commonwealth are suffering and grieving today.
This is a statement from his office.
The tragedy is uniting everyone around the common goal of helping and healing.
Politicizing that suffering would be low for even the deepest partisan.
So was Senator Rand Paul himself low for even the deepest partisan when he was ranting and
raving about the nation's credit card limit when it came time to approve funding for those
suffering and grieving, Sandy and Maria?
See I'm one of those people who like to give people the benefit of the doubt.
Assume the best.
You'll be let down a lot.
And when it comes to natural disasters, I have a very weird view of them because honestly
when I have friends that come and stay with me down here and it's in hurricane season,
there's a small part of me that well, kind of hopes in a weird way that if a hurricane
is going to come, it comes while they're there.
Because I believe that doing relief work after a disaster, it is transformative.
It is something that alters the way you look at the world.
So I would have chalked it up to that, him seeing it with his own eyes, if it wasn't
for that statement.
I think it would be a good idea for the people of Kentucky to understand while you are grieving
and suffering, your senator voted against and actively spoke out against the kind of
relief that you're going to get for other Americans.
You know, I had a really brilliant lawyer once tell me, everybody's for law and order
until they get caught.
Then they change their position and they start to understand things a little bit more.
Senator Paul just got caught, but it certainly doesn't seem like he intends on changing
his position.
I hope I'm wrong.
I hope that when the next disaster happens, he won't be a no vote for helping other Americans.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}