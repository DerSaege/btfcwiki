---
title: Let's talk about coup by PowerPoint.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U4p6QlrQU3c) |
| Published | 2021/12/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Reveals that he has covered the points in the PowerPoint on his channel over the past ten months.
- Mentions posting documents related to the PowerPoint on his Twitter months ago.
- Points out the sudden media reaction to the PowerPoint and the use of the term "coup."
- Describes the alleged PowerPoint circulated among the top of the Trump camp outlining options for a coup.
- Explains the term "auto coup" or "self coup" where a person comes to power through legitimate means and circumvents checks on their power.
- Emphasizes that the coup attempt is ongoing, not something of the past.
- Analyzes Trump's political strategy in backing candidates who support his agenda, particularly in Georgia.
- Expresses concern that people are viewing the coup attempt as a fluke rather than ongoing practice.
- Asserts that future attempts are likely if those behind the coup have another chance.
- Stresses the importance of individuals within the Republican party standing up against Trump's influence as a defense against future coup attempts.

### Quotes

- "A coup attempt in which people said, oh, it can't happen here, and looked the other way, and the media didn't want to call it a coup. That's not a failed coup. It's practice."
- "The fate of the republic, the fate of the American experiment, lies in the hands of the people Donald Trump called rhinos. Republicans in name only."
- "With the current media coverage, although I'm glad they're finally acknowledging it as a coup attempt, the way it's being presented as past tense and something that we need to look into what happened and not what's happening. It's concerning."

### Oneliner

Beau reveals ongoing coup attempts and stresses the importance of vigilance against political manipulation for the future of democracy.

### Audience

Political analysts, concerned citizens

### On-the-ground actions from transcript

- Watch political developments closely to identify any signs of ongoing attempts to manipulate power (implied)
- Speak out against any undemocratic actions or attempts to circumvent established processes (implied)

### Whats missing in summary

In-depth analysis of the potential consequences of downplaying ongoing coup attempts and the need for continued vigilance to protect democracy.

### Tags

#CoupAttempt #Trump #PoliticalManipulation #Democracy #Vigilance


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about that PowerPoint.
If you've been watching the channel for the last ten months, none of that's a surprise
really.
We've covered pretty much every point in the PowerPoint.
A lot of the documents that are kind of being presented as breaking now, I posted them on
my Twitter account months ago.
So people are curious about the sudden media reaction and why the media is starting to
use a certain term.
The PowerPoint that circulated, is alleged to have circulated, among the top of the Trump
camp, outlines the options for a coup.
To them, this is the smoking gun evidence.
I think most people already knew that's what it was.
They just didn't want to call it that because you know, that can't happen here.
Not in the United States.
Not in the Great American Experiment and all that.
So now with this PowerPoint, the media has started using the term coup.
In fact, I saw a clip and I was trying to find it again and couldn't.
I think it was from MSNBC, but the commentator is standing there and he's like, it was a
coup.
You know, it's not the way we normally think about it because he kind of did it to himself.
I don't know if that's, maybe we should call it an auto coup or something like that.
And it seemed as though he was just throwing out a term.
For the record, that is one of two acceptable terms.
That is the term, auto coup or self coup.
That's when the person comes to power through legitimate means and then somehow circumvents
the legislative branches or any branch that could have a check on their power.
That is called a self coup, which is certainly what it appears he attempted.
The problem with the media coverage, even though it has finally admitted there was a coup attempt,
is that they're referring to it in the past tense.
It was a coup attempt.
It's ongoing.
It's ongoing.
The way I see this, if you look at where Trump is putting his political capital,
he's backing candidates who are primarying people who didn't go along with his little plan.
That's why he's interested in Georgia.
He's trying to get the pieces in place.
So at least it appears to me that if he gets another shot, he's more likely to be successful.
To my way of thinking, and this isn't entirely fair, but anybody who's accepting a Trump
endorsement at this point is in on it.
And now realistically they're not really in on it.
They probably don't understand what they are being involved in.
But to the Trump camp, they're somebody who's weak enough to do whatever he says.
The real concern here is that people seem to be viewing this as a fluke.
It's not.
A coup attempt in which people said, oh, it can't happen here, and looked the other way,
and the media didn't want to call it a coup.
That's not a failed coup.
It's practice.
And it seems reasonable to believe that if the people behind it had another opportunity,
well they would try again.
In a very strange way, the fate of the republic, the fate of the American experiment, lies
in the hands of the people Donald Trump called rhinos.
Republicans in name only.
See if Trump's chosen candidates, if they win in the primaries, well it's going to re-energize
that movement.
So it's those who didn't do whatever he said, those who were strong enough to stand up to
him, even though he was part of their party, those are the people who are kind of the first
line of defense in stopping any future developments along this line.
With the current media coverage, although I'm glad they're finally acknowledging it
as a coup attempt, the way it's being presented as past tense and something that we need to
investigate into what happened and not what's happening.
It's concerning.
This hasn't failed yet.
It's ongoing.
If you look at that powerpoint, you see that they attempted to circumvent it.
They attempted to get around the election every step of the way.
Every step of the way, they tried.
They had something up their sleeve for each development.
I don't know why people believed or believe that that stopped.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}