---
title: Let's talk about FEMA, the CDC, zombies, and wet wood....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_M0o3DBr_g4) |
| Published | 2021/03/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing his second channel, The Roads with Beau, which features long-format content on historical events, community networking, and on-location experiences.
- Due to travel restrictions, a project focused on community networking was put on hold but is now coming back as restrictions ease.
- Explains why FEMA and the CDC use zombies as a teaching tool in emergency preparedness to make information more accessible and engaging.
- Touches on the importance of planning for surviving zombies as a way to prepare for other disasters like snowstorms or hurricanes.
- Addresses a question about starting a fire with wet wood, explaining that even wet wood may have a dry core that can be utilized with more effort.
- Encourages viewers to think about emergency preparedness and skills required for various scenarios like starting a fire with wet wood.
- Emphasizes the need to be prepared for future emergencies and disasters by honing necessary survival skills.
- Suggests setting wet wood near a fire to dry out and mentions that wet wood tends to be harder to keep burning and produces more smoke.
- Commends those who ask questions and seek to be prepared for future emergencies by developing necessary skills.
- Encourages viewers to contemplate what they would do in a zombie apocalypse scenario as a mental exercise in preparedness.

### Quotes

- "If you plan to survive zombies, you will make it through a snowstorm or a hurricane or whatever, because zombies disrupt everything."
- "But in that situation, what else do you have to do?"
- "You realized that there was an issue and you are trying to be prepared for next time."
- "And rest assured, there will be a next time, just as sure as there's going to be another hurricane where I live."
- "So just maybe take this afternoon and think about what you would do in the event of a zombie apocalypse."

### Oneliner

Beau introduces his new channel and explains why FEMA and the CDC use zombies in emergency preparedness, while also addressing survival skills like starting a fire with wet wood and encouraging preparedness for future disasters.

### Audience

Community members

### On-the-ground actions from transcript

- Contemplate emergency preparedness scenarios and develop necessary survival skills (implied)

### Whats missing in summary

Beau's engaging storytelling and practical advice on emergency preparedness through unconventional teaching tools.

### Tags

#EmergencyPreparedness #CommunityNetworking #ZombieApocalypse #SurvivalSkills #DisasterPreparedness


## Transcript
Well howdy there internet people, it's Beau again.
So today's going to be a little bit different.
We're really just going to answer two common questions that
came in, one to this channel and one to the other channel.
If you don't know, the second channel is up and running.
It's called The Roads with Beau.
It's long format content, so they are much longer videos.
Some are deep dives into historical events or people.
Some are about community networking
and how to do various things with your community network.
And some are going to be on location.
We have examples up over there of all three types.
The original plan from more than a year ago
was to get out on the road and do some real force
multiplication stuff.
But then this thing happened and we couldn't travel or have
groups of people together, so it got put on hold.
Now that we can finally see the light at the end of the tunnel,
that project is coming back.
Hopefully we'll have it all up and running by this summer.
So the two questions that came in, the one to this channel,
was why is FEMA and the CDC talking about zombies?
If you don't know, they released some training material.
And it's about how to survive zombies.
This is a really common thing in the emergency preparedness
community, so common, in fact, you
can find videos on this channel using zombies
as a teaching tool.
If you plan to survive zombies, you
will make it through a snowstorm or a hurricane or whatever,
because zombies disrupt everything.
If you plan for that, you'll be OK.
That's what they're doing.
They've actually done this for a couple of years
that I know of.
I think now, just with everything going on,
people are paying attention to what shows up on their website.
And if you don't know that they do that,
that could be a little concerning, I guess.
But that's why.
It's just a good teaching tool.
You can sit around and talk with your kids
about what they would do to deal with zombies,
and they will be far more interested, and oddly enough,
less scared than if you were talking
about a hurricane or an earthquake
or whatever is happening in your area.
So that's why.
Nothing to worry about.
It's not a prelude to anything.
It's just them trying to come up with a way
to make the information more accessible.
Now, one of the videos we did on the other channel
was a introduction to emergency preparedness, to bugging out.
And it goes over food, water, fire, shelter, all that stuff.
One of the questions, probably because
of what happened in Texas, was that's great.
It's cool to start a fire, but you're in Florida
and everything is dry.
What do you do if it's wet?
What do you do if the wood's wet?
I have in front of me a piece of wet wood.
I've already split it.
Generally speaking, when wood gets wet,
it doesn't get soaked all the way through.
Inside, it's dry.
It takes a lot to soak large pieces of wood
all the way through.
So if you cut it long ways, you will
find an inner core that is still dry or at least usable.
It may still be a little bit damp.
It takes a lot more work because you also
have to process it down to make the kindling,
because the little sticks that are
going to be on the ground around you,
they will be soaked all the way through.
The bigger pieces won't.
So the larger the diameter of the piece of wood,
the more likely you are to have a big usable core
in the center.
So it can be done.
It just takes a lot more energy and a lot more work,
a lot more time.
You have to process it.
But it is good that people are thinking about it,
because my guess is people were looking around
at all the snow on the wood and thinking it was useless.
It could still be used.
You just had to cut off the outside of it.
And it is a time-consuming process.
But in that situation, what else do you have to do?
So that's an easy thing to do.
Once you actually get the fire started,
remember to take other pieces of wood
and set it near the fire so it dries them out
and it's not as hard the next day.
It's also worth remembering that wet wood is harder
to keep going, even if it's just a little damp.
And it tends, at least around here,
to be a whole lot more smoky.
So just something to bear in mind.
But to everybody who sent that question in, good on you.
You realized that there was an issue
and you are trying to be prepared for next time.
And rest assured, there will be a next time,
just as sure as there's going to be another hurricane where
I live.
There will be something else that
will require you to have those skills.
So just maybe take this afternoon
and think about what you would do in the event
of a zombie apocalypse.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}