---
title: Let's talk about Trump trying to take over Republican finances....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8qSw7sZ50qs) |
| Published | 2021/03/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump sent cease and desist letters to Republican organizations to stop using his name for fundraising.
- Trump wants to control the Republican Party's finances and have money funneled through his PAC or himself.
- Large companies and donors donate for influence, and they might not see value in donating to Trump's PAC.
- The Republican establishment and donors may have concerns about Trump controlling a large portion of their budget.
- This move by Trump could have long-term implications for the Republican Party and their efforts to win back the House and Senate.

### Quotes

- "Trump is attempting to seize control of the Republican Party's finances."
- "If he gets his way, it very well may cost them the House and the Senate."

### Oneliner

Former President Trump is trying to control Republican Party finances through cease and desist letters to stop using his name for fundraising, potentially impacting their efforts to win back the House and Senate.

### Audience

Party members, donors, political analysts

### On-the-ground actions from transcript

- Contact Republican organizations to express concerns about Trump's control over finances (implied)
- Monitor how this situation unfolds within the Republican Party and its implications for future elections (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's attempt to control the Republican Party's finances and its potential impact on their future electoral success.

### Tags

#RepublicanParty #Trump #Fundraising #Politics #Influence


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some interesting news coming out of the Republican Party.
Former President Trump has reportedly sent cease and desist letters to the Republican
National Committee, the National Republican Congressional Committee, and the National
Republican Senate Committee.
And basically he told them to stop using his name and likeness to raise money.
These are the organizations that are tasked with coming up with a unified message for
the Republican Party and are generally seen as critical to the Republicans' effort to
win back the House and the Senate.
First I want to point out, I don't know that he has a good case here.
I'll let a lawyer chime in on that.
But the political implications are pretty clear.
Trump is attempting to seize control of the Republican Party's finances.
That's his goal.
The rumor says that he was just absolutely irate that organizations that had used his
name to raise money were going to hand some of that money over to help candidates who
voted to impeach or convict him.
That bothered him.
So his goal is to have all of that money funneled through his PAC or through him.
I am not sure how the Republican establishment is going to respond to this.
I think that they may see this as a much larger and long-term issue than his normal grandstanding.
I'm also not sure how the donors are going to look at this.
Let's be real honest for a second.
The reason large companies and large donors donate is to get influence.
If it goes through Trump's PAC, they get influence with a twice impeached one-term president.
I don't know that that's worth it to them.
Aside from that, the Republican Party itself is probably going to have some major issues
with this because given the former president's financial history, they may not be thrilled
at the idea of him controlling a large chunk of their budget.
This is something that will probably continue to make headlines and continue to be an issue
for the Republican Party for quite some time.
If he gets his way, it very well may cost them the House and the Senate.
It will certainly damage their efforts to retake the House and the Senate.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}