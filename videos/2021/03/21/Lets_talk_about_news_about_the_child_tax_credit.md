---
title: Let's talk about news about the child tax credit....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_DpvnJSskV4) |
| Published | 2021/03/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Public service announcement about the child tax credit cash in the relief bill.
- Child tax credits amount to $3,000 for children over six and $3,600 for those under six.
- Initially, news broke that people were to receive monthly checks starting in July.
- However, the IRS moved the filing deadline from April 15th to May 17th, potentially delaying the payments.
- The final legislation doesn't specify monthly payments, only "periodically," which could change the payment frequency.
- There might be a shift from monthly payments to receiving larger sums less frequently, like $600 every three months.
- Beau advises against making financial decisions based on expecting this money in July due to the uncertain timeline.
- To be eligible for this credit, individuals must have filed for the 2020 tax season.
- People who usually don't file taxes may need to do so this year to receive this benefit.
- The extension of the filing deadline was aimed at helping small business owners have more time to settle their tax obligations.

### Quotes

- "This is more of a public service announcement than a normal video."
- "Be aware that there have been some pretty significant changes that don't seem to be getting any press."
- "Just be aware that there have been some pretty significant changes."

### Oneliner

Beau shares updates on the child tax credit cash, cautioning against banking on July payments due to filing deadline shifts and potential changes in payment frequency.

### Audience

Taxpayers, parents, individuals eligible for child tax credit.

### On-the-ground actions from transcript

- File for the 2020 tax season to be eligible for the child tax credit (suggested).
- Inform others about the potential changes in payment frequency for the child tax credit (implied).

### Whats missing in summary

Detailed instructions on how to file for the child tax credit and updates on the relief bill.

### Tags

#TaxCredit #ChildTaxCredit #IRS #FilingDeadline #PublicService


## Transcript
Well, howdy there internet people. It's Beau again.
So today we're going to talk about
the tax credit cash, the child tax credit money.
This is more of a public service announcement than a normal video.
There have been some changes. If you don't know what I'm talking about
in the relief bill, there's
child tax credits. It's $3,000
if the child is over six and $3,600 if they are
under six. When it
was, when the news broke on this, everybody pointed out that people would
be getting
monthly checks starting in July. That news was covered
on every outlet because it's a big deal. There have been some alterations
and I've only seen one outlet cover it. So
that may not happen, not in that way. What occurred
was the IRS moved the filing deadline for everybody
back from April 15th to May 17th.
That means all of those employees are going to be doing that for an extra month.
The IRS has finite resources,
therefore everything else is going to be moved back.
I would also note
that the final language doesn't say monthly.
It says periodically. So while the dollar amounts
don't change, how you receive them might.
So if you were looking at getting $200 a month,
you may get $600 every three months,
however it works out for you. That seems like news people
would need. I wouldn't make any financial decisions
based on this money coming in July
because while it is still possible, it doesn't look likely.
The other thing to note is that
this isn't a stimulus. You have to file
in 2020 to be eligible for this.
You have to file for the 2020 tax season, otherwise you won't get it.
So if you are one of those people
who forever, for whatever reason, doesn't have to file, you might want to this year.
You might elect to do that.
Again,
not a normal video, no overarching point here.
Just this is news that I think people need
and I haven't really seen it being covered
like at all.
So why did they move it back is obviously a question.
That's more to benefit small business owners
and like actual small business owners, we're not talking about millionaires here,
it's giving small business owners an extra month
to come up with the money they owe the IRS. That's really what it boils down to.
So
that's where we're at.
Just be aware that there have been some pretty significant changes
that don't seem to be getting any press. It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}