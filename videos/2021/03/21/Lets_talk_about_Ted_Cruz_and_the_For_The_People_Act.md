---
title: Let's talk about Ted Cruz and the For The People Act....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=j0a6IF0VG1A) |
| Published | 2021/03/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about HR 1, the For the People Act, in response to statements made by Ted Cruz against it.
- Ted Cruz expressed the Republican Party's intention to stop HR 1, claiming it aims to ensure Democrats maintain control for the next century.
- Contrary to Cruz's claims, HR 1 includes provisions to combat partisan gerrymandering, increase voter access, and reform campaign finance.
- Beau interprets Cruz's opposition to HR 1 as a tacit admission that Republicans can't win without gerrymandering and voter suppression.
- Republicans in state legislatures are using Trump's baseless claims to tighten voting regulations, hinting at a strategy to suppress votes.
- Cruz's remarks imply that Republicans believe they can't succeed without denying people their voice through undemocratic practices.
- The opposition to HR 1 by Republican senators necessitates Democrats to eliminate the filibuster in order for it to pass.
- Beau criticizes the Republican Party for prioritizing culture wars over policy development and undermining representative democracy.
- He suggests that the Republican Party's reliance on divisive tactics is a threat to the United States' democratic system.

### Quotes

- "It appears that Senator Cruz is saying, unless the Republican Party can gerrymander and suppress the vote, they can't win."
- "The Republican Party is lost without the ability to deny the people their voice."
- "Republicans can't win and won't be able to for the next century if this passes."
- "The Republican Party is actively attempting to undermine the representative democracy we have in the United States."
- "It's how it appears."

### Oneliner

Beau analyzes Ted Cruz's opposition to HR 1, revealing Republican fears of losing without undemocratic tactics, urging Democrats to eliminate the filibuster for its passage.

### Audience

Activists, Voters, Legislators

### On-the-ground actions from transcript

- Rally support for HR 1 among your community and representatives (suggested)
- Advocate for eliminating the filibuster to ensure the passage of HR 1 (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of Ted Cruz's opposition to HR 1, shedding light on the Republican Party's reliance on undemocratic methods to maintain power.

### Tags

#HR 1 #ForThePeopleAct #TedCruz #RepublicanParty #VoterSuppression


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about HR 1.
That's the For the People Act.
We're going to do this because Ted Cruz talked about it.
And he said some interesting things
that I think merit attention.
He said it on a phone call that was organized
by the American Legislative Exchange Council.
And he made it very clear that the Republican Party
is going to do everything they can to stop this bill.
Because he says, HR 1's only objective
is to ensure that Democrats can never again lose
another election, that they will win and maintain control
of the House of Representatives and the Senate
and of the state legislatures for the next century.
What is in this bill?
I mean, when you hear this, the only thing you can picture
is that it's a bill designed to gerrymander districts,
you know, divide them up so conservative voices are
diluted so they can't be heard.
Because that way, Democrats will win,
and they'll be able to control all of these things.
Or it has a bunch of stuff to suppress the vote,
make it harder for people to vote,
specifically targeting demographics
that tend to vote conservative.
Or maybe it makes it easier for Democrats
to get some of that secretive campaign money
that nobody knows about so they can just outspend Republicans.
I mean, that's the only thing that I can picture
that would accomplish this.
So what's in the bill?
Literally the exact opposite.
It has a bunch of provisions designed
to eliminate partisan gerrymandering.
It has a bunch of provisions to increase voter access
to make sure that people's voices get heard.
And it has campaign finance reform.
That's what's in it.
This statement, compared with what's actually in the bill,
that may be the biggest indictment
of the Republican Party I've ever heard.
It appears that Senator Cruz is saying,
unless the Republican Party can gerrymander and suppress
the vote, they can't win.
Unless they cheat, they can't win.
Unless they engage in practices that
are designed to get rid of those low-quality voters
that we were talking about, they can't win.
I would point out that this conversation is taking place
at the same time as Republicans in state legislatures
all over the country are attempting
to lean into Trump's baseless claims as an excuse
to enhance voting regulations and make it harder for people
to vote.
It certainly seems like Cruz is saying
that the Republican Party is lost without the ability
to deny the people their voice.
It's a, I mean, that's a wild statement
when you really think about it.
Maybe he meant something else.
Maybe he spent too much time with Trump
and he's developed a flair for the dramatic
or a habit of being economical with the truth.
But this statement compared to what's in the bill,
it makes it that much more important that it gets through.
It should also make people wonder why
Republican senators oppose it.
I mean, you don't actually have to wonder why.
You can look and see what they're
doing in the state legislatures.
But Republicans in the Senate are not
going to let this through.
They're going to fight it every step of the way.
They're going to do everything they can to stop this, which
certainly means that the Democrats really
only have one option, and it's to get rid of the filibuster.
Otherwise, this won't get through.
And we know what they're doing in the state legislatures
already.
Ted Cruz, on a phone call designed
to rally support against this, provided the best argument
for it because apparently Republicans can't win
and won't be able to for the next century if this passes.
Because we all know they won't actually develop a platform
and focus on policy because it's too much fun and too easy
to lean into the culture wars, which
is what they're doing.
In fact, those were kind of the talking points
they got handed out on this phone call.
It seems as though the Republican Party
is actively attempting to undermine
the representative democracy we have in the United States.
It's how it appears.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}