---
title: Let's talk about how Republicans manipulate the emotional....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Zay1sV7MVVY) |
| Published | 2021/03/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how those in power provoke emotional reactions to divert blame onto marginalized groups.
- Responds to a viewer request regarding the Republican Party's tactics of demonizing and blaming marginalized groups.
- Provides an example of Republican senators requesting information on vaccinated migrants to manipulate public opinion.
- Points out the dual tactics the Republican Party will use regardless of the information received: vaccine distribution criticism or disease fear-mongering.
- Raises the irony of senators downplaying public health suddenly showing concern to provoke outrage against demonized groups.
- Notes how blaming marginalized groups diverts attention from the government's failure in managing public health crises.
- Emphasizes the strategy of directing anger towards marginalized groups rather than those in power to scapegoat them.
- Illustrates how marginalized groups are unfairly targeted and scapegoated despite having no decision-making power.
- Stresses that problems often stem from those in power using emotional and divisive rhetoric to shift blame.
- Concludes with a metaphorical comparison of blaming immigrants for problems to the idea of immigrants taking vaccines.

### Quotes

- "People who have less influence and less institutional power are never the source of your problems. The source of your problems pretty much always comes from the people who have power."
- "It's easier to point to a class of people that have already been demonized."
- "That immigrant's going to take your vaccine."
- "Blame it on people who have absolutely no power."
- "The rich guy with all the cookies, one cookie left in the center of the table. That immigrant's going to take your cookie."

### Oneliner

Beau explains how the Republican Party manipulates public opinion by demonizing marginalized groups to deflect blame for policy failures onto those with no power.

### Audience

Viewers

### On-the-ground actions from transcript

- Challenge demonization of marginalized groups (implied)
- Advocate for accurate information sharing (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of how political parties manipulate public opinion through scapegoating and diversion tactics.

### Tags

#PoliticalManipulation #Scapegoating #PublicOpinion #MarginalizedGroups #PowerDynamic


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we have a request from one of
you that we're going to respond to. A very common theme on this channel is that those in power tend
to provoke emotional reactions in large portions of the populace in hopes of getting them to look
at a group that doesn't have a lot of power and blame them for the failures of those who actually
have power. Something we point out a lot, we talk about a lot, and I got this message today.
Have a challenge for your crystal ball. You often say the Republican Party doesn't have policy and
instead relies on demonizing and kicking down at marginalized groups. When I point out examples,
people say it's after the fact. Can you give us an example before it happens so we can point to it
and say this is how they play ignorant emotional people? And the thing is, when I first read that,
I was like, that's going to be really hard. But then it turned out not to be because
a Republican senator sent out a letter. This is signed by Grassley, Blackburn, Tillis, Cruz, Howley,
Lee, Cotton, Kennedy. The usual suspects. It's a letter requesting information on the numbers of
migrants who were processed and then released into the United States and how many of them were
vaccinated. Okay, so how is this going to play out? They're going to get the numbers. If there was a
large number that was vaccinated, you see the Biden administration, they care more about those
people than they do you. They're giving them your precious vaccine. However, if it turns out that
not a lot of them were vaccinated, they will turn it into a C. They're releasing those people and
you know they have disease even though statistically they're doing better than the United States.
They're releasing those people among you. That's how it's going to work. It doesn't matter what
information comes out. The Republican Party is going to use one of those two talking points.
It's even more entertaining when you realize that most of the people who signed on to this list,
generally speaking, have been downplaying the whole public health issue to begin with.
Open up. It'll all be all right. Don't worry about it. But if it suits their needs and they
can provoke outrage against a class of people that they have demonized,
all of a sudden they're all about public health.
Doing this allows them to shirk responsibility for their failure over the last year of actually
mitigating this problem. They can blame it on somebody else. Now, really their target
is to get people upset with the Biden administration. But it's hard to get people mad at Biden because
he's boring. So it's easier to point to a class of people that have already been demonized.
A group of people that are seen by their base as lesser, as not belonging, as somehow not really
worthy of any consideration. That's how it works. So when this letter gets answered,
when they get the information they're looking for, that's what they're going to do. It doesn't matter
what the outcome of the information is. It doesn't matter what information they get.
They already have the plan to blame it on people who have absolutely no power.
The migrants didn't make any decisions related to this, but they're the scapegoat.
People who have less influence and less institutional power and less standing as a
demographic are never the source of your problems. Ever. The source of your problems pretty much
always comes from the people who have power. And they're the people who use emotional rhetoric,
divisive rhetoric, to get you to blame it on somebody else. It's like that image you see all
over social media. The rich guy with all the cookies, one cookie left in the center of the
table. That immigrant's going to take your cookie. Only this time it's going to be that immigrant's
going to take your vaccine. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}