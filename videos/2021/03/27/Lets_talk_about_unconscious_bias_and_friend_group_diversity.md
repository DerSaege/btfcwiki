---
title: Let's talk about unconscious bias and friend group diversity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZQA3eLm6SvE) |
| Published | 2021/03/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Advocates for diversifying friend groups for societal benefits.
- Acknowledges hurdles in diversifying friend groups, such as geographic limitations and unconscious bias.
- Warns against tokenizing people from different demographics.
- Emphasizes the importance of genuine friendships with diverse individuals.
- Addresses unconscious biases and the need to overcome them in diversifying friend groups.
- Encourages accepting corrections and growth through interactions with diverse friends.
- Shares a personal experience of realizing and eliminating biased vocabulary.
- Stresses the value of diverse friendships in gaining different viewpoints and understanding social issues.
- Mentions the enrichment and societal benefits of having a diverse friend group.
- Cautions against tokenization as it can hinder the benefits and worsen the situation.

### Quotes

- "You can't tokenize people."
- "If you do this, your life becomes significantly enriched."
- "Accept it. Don't get defensive. They're trying to correct you."
- "The big thing that you have to watch out for though is tokenizing people."
- "It's just a thought."

### Oneliner

Beau advocates for diversifying friend groups, warns against tokenization, and stresses the value of genuine, diverse friendships for personal growth and societal benefits.

### Audience

Individuals seeking to diversify their friend groups.

### On-the-ground actions from transcript

- Make genuine efforts to befriend individuals from different demographics (suggested).
- Overcome unconscious biases by acknowledging and addressing them within yourself (implied).
- Accept corrections and feedback graciously when interacting with friends from diverse backgrounds (implied).

### Whats missing in summary

Beau's engaging storytelling and personal anecdotes that make the importance of diversifying friend groups relatable and actionable.

### Tags

#Diversity #Friendship #UnconsciousBias #Acceptance #SocietalBenefits


## Transcript
Well howdy there internet people it's Beau again. So today we're going to talk about diversifying
your friend group because I had a question about it. It's something I advocate. It's something that
I think is not just beneficial to you as an individual. I think it benefits society at large
if people from different demographics become friends, actual friends. And I got a question
about how to do it. Basically the person looked around and was like wow all of these people look
like me and they want to fix that and that's good. There are hurdles that have to be overcome. Now
for some people it's hard because you live in a geographic area where there is no diversity. That
happens. But that's not most people. Most people there is diversity around you. However because
different demographics do different things, you know the generalities that exist, you don't often
run into each other. You know in the southern United States this is all a holdover from the
1960s and before. It's why towns are the way they are. They're divided up more so than in a lot of
other places. That extends to social activities as well and you have to be aware of it because
you're going to have to step outside your comfort zone. Now one of the things that is a huge
pitfall and something you have to be very aware of, especially if you're setting out to do this
like intentionally, you can't tokenize people. You can't tokenize people. Make sure that the
person from this other demographic that you are making friends with, you actually have something
in common. They're your friend, not just you know the black dude you hang out with. They're your
friend and I think the easiest way to get this across to people who look like me is I want you
to picture a black restaurant and there's two white guys who work there, me and Ben Shapiro.
Odds are there aren't going to be a whole lot of people who like us both. Make sure that the person
you're making friends with is somebody that you're actually friends with. Now another downside,
something that you have to overcome, is probably one of the reasons your friend group doesn't
already look like this. Your friend group isn't already diverse. Unconscious bias. You know,
if you're setting out to do this, you're probably not an overtly racist person. That doesn't mean
you don't have unconscious bias. Everybody does. You know, we all like to pretend that we're
enlightened but we all have biases. Since people don't like to admit it, I'll give you one from me.
For a while I was very uneasy around Arab males and it wasn't the reason that people like would
normally give for that. It had nothing to do with that and I couldn't figure out what it was but it
was something that I noticed. I became very standoffish. It wasn't the final aha moment but
one of the big clues for me as to why this was happening was one of my friends whose family came
over from Italy. They came over to visit and I met him and I got the exact same feeling. If you are
familiar with the generalities of these two groups of people, you probably already know what it is.
In the southern United States, we stand pretty far apart. We don't get close to each other. If
you pull up to an ATM like the kind you slide your card into and somebody's already there,
you're gonna leave a car length of space between you and the next car. We don't get close. They
did. It was that simple. It doesn't necessarily have to be something racist. It could just be a
cultural thing that you're not comfortable with, you're not familiar with and then you get to go
to get over it which is one of the benefits of having a diverse friend group. You're also gonna
say things that you shouldn't. Guaranteed, it's gonna happen. When they tell you, you know, you're
out of line, accept it. I have never had one of my friends say, you know, that that wasn't right and
and me be in the right. It has never occurred. So accept it. Don't get defensive. They're trying to
correct you. Take the correction, you know, and everybody can agree with this theoretically,
you know. Everybody agrees with that statement. If you don't look back on the person you were 10,
20, 30 years ago and just cringe, you haven't grown as a person. That's true of yesterday too.
Growth is a process. It takes place over time. I'll give you something that you can actually see
happen on this channel. I was very fond of the word blind when this channel started. The word
blind. I used it a lot and I had somebody tell me just kind of offhand, you need to eliminate that
from your vocabulary. Okay, whatever. But I did it and then what I found out when I was trying
to find another word to use in its place is that I literally never meant blind when I was saying that.
I meant unaware, willfully ignorant. I meant a bunch of things, none of them blind and none of
them good except the criticism when it comes in and change. That's all anybody's going to ask.
So, doing this and having a group of friends that is very diverse is incredibly valuable.
And it's not just the better food jokes, which I mean for real though, if you look like me,
you're going to get some better food in your life. It's also the different viewpoints,
the way of looking at life in general. You understand a lot of social issues a lot more
because you get to hear one of your friends just be completely fatalistic
about an encounter with law enforcement. They're going to do what they're going to do.
It helps you understand this stuff a little more. You get different stories.
Your life becomes significantly enriched if you do this.
If you do this. And it does benefit society as a whole because it normalizes
those friendships, which I mean we pretend like it's completely normal, but it's really not.
The big thing that you have to watch out for though is tokenizing people. Because if you do that,
not just are you not going to get all the benefits, you're actually going to make things worse.
You're going to set the exact opposite example of what you want.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}