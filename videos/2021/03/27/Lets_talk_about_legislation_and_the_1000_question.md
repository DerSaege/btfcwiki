---
title: Let's talk about legislation and the $1000 question....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lp0uOXFBYIs) |
| Published | 2021/03/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the challenge he received from a friend to define what the gun control crowd wants to ban.
- Sets conditions for defining the term and sharing an alternative data-driven solution.
- Defines the specific firearms the gun control crowd aims to ban: semi-automatic firearms chambered between 5.45mm and 7.62 with detachable magazines holding over 19 rounds.
- Points out that focusing on banning these specific firearms may not be effective as manufacturers can adapt and criminals may find alternatives.
- Advocates for focusing on prohibiting people with a history of domestic violence from owning firearms based on data showing a link between domestic violence and incidents.
- Emphasizes the importance of closing loopholes in existing laws related to domestic violence and firearm ownership.
- Suggests that prohibiting individuals with a history of domestic violence from owning firearms is a more effective and actionable solution than regulating firearm design.
- Mentions an additional observation about the correlation between people who are cruel to animals and those who commit violent acts.
- Encourages focusing on actionable and data-driven solutions that are attainable and likely to receive support.

### Quotes

- "What you are talking about when you are talking about these large incidents, what you're wanting to get rid of is semi-automatic firearms that are chambered somewhere between 5.45 millimeter and 7.62 that accept a detachable magazine that holds more than 19 rounds. That's it."
- "I've advocated this for a long time based off personal observation. There's now data. 749 incidents were looked at about 60 percent of them have one thread one common thread, domestic violence."
- "If it's about saving lives, that's where you need to look."

### Oneliner

Beau sets conditions for defining gun control terms, advocates focusing on domestic violence prevention over regulating design, and suggests an attainable, data-driven solution for reducing gun violence.

### Audience

Advocates, policy makers, activists

### On-the-ground actions from transcript

- Advocate for prohibiting individuals with a history of domestic violence from owning firearms (advocated)
- Support efforts to close loopholes in existing laws related to domestic violence and firearm ownership (advocated)
- Raise awareness about the correlation between domestic violence and incidents involving firearms (suggested)

### Whats missing in summary

The full transcript provides detailed insights and data-driven solutions on addressing gun control issues effectively.

### Tags

#GunControl #DataDriven #DomesticViolence #Advocacy #Prevention


## Transcript
well howdy there internet people it's Beau again so today we're going to talk about data
we're going to talk about perception we're going to talk about options
and we're going to answer the thousand dollar question quite literally in this case
um i had a friend who has heard me say something over the years and it finally
made sense to him after something that recently happened uh so he has offered to give me a
thousand dollars to accurately define what the gun control crowd wants to ban because he realized
that the terminology that often gets used doesn't actually cover it now i get questions like
this a lot because although i don't own a gun anymore i used to a lot of them actually
but i haven't owned in more than a decade but i'm still very familiar with the gun community
so i put two conditions on it one he had to give the money to a shelter and for those who saw this
on twitter the it's going to make more sense here in a second um and two he had to watch the whole
video because i'm going to give the definition and then i'm going to rain on your parade but
then i'm going to give you something that is data-driven that's going to be more effective
and not only will the gun crowd not oppose it most of them will support it actively
the definition what you are talking about when you are talking about these large incidents
what you're wanting to get rid of is semi-automatic firearms that are chambered somewhere between 5.45
millimeter and 7.62 that accept a detachable magazine that holds more than 19 rounds that's
it it doesn't have to be 100 pages long that is what the gun control crowd is imagining
when they talk about this now that's not actually data-driven just so you know but the
most covered events have led to this perception that's what you're wanting to get rid of
what happens if you do that they're gone tomorrow disappeared two things one the firearms manufacturers
start producing stuff just outside that criteria and two the people who are intent on doing horrible
things they also move outside of it which in this case is may make it worse
um the media will tell you that a lot of the stuff that gets used is high-powered it's not
not really um in fact the one that everybody you know talks about the most that that's actually not
high-powered imagine if these incidents occurred with something that was
so i don't believe that even if you got that which i'll tell you now that's going to be an
on-starter politically but even if you got it i don't think it would do much good i mean it
wouldn't hurt but i don't think it would actually really solve the problem but if you want something
that's data-driven and that is actionable that you can actually get in this political climate
i've advocated this for a long time based off personal observation there's now data
749 incidents were looked at about 60 percent of them have one thread one common thread
domestic violence either the the suspect had a history of it or they had a history of it
had a history of it or the incident started that way
60 i would focus on that
that seems like it would do a whole lot of good it's definitely worth looking into
and i know right now the gun crowd is going to say well we have laws on this
yeah and they are full of loopholes full
we need to focus on prohibiting people who have a history of domestic violence
from owning that needs to be the goal not somebody who got that charge
because a lot of times they could commit an act of domestic violence but get charged with simple
assault then it doesn't matter if it's a misdemeanor
then it doesn't matter if it's a misdemeanor
that's where the focus needs to be no exemptions for professions no exemptions for age if you're a minor
and this happens sure you can get your record expunged but the prohibition sticks
i cannot imagine many gun owners saying no no that's a horrible idea we're against that
they're going to be all about getting those weapons out of those people's hands
that is something that is actionable that will do more good than anything you can do regulating the design
now i'll add into this from personal observation i haven't seen a study on this
one people who are mean to animals same boat
if you want something that works that's data-driven that's where you need to look
it's going to be more effective and most importantly it's attainable
it's something that can happen you aren't going to get opposition on that
if it's about saving lives that's where you need to look
anyway it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}