---
title: Let's talk about the children coming here....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QoyHXyXB7LY) |
| Published | 2021/03/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responding to a message critiquing his views on parents subjecting their children to trips to the US.
- Describes the terrible conditions and hardships faced during these trips, likening them to historical European immigration.
- Points out the historical correlation where groups enduring the worst trips contribute significantly to building the country.
- Argues against demonizing and marginalizing these asylum seekers, stating they are trying to save lives.
- Distinguishes between migrants and asylum seekers, advocating for supporting those willing to make sacrifices to build a better future.
- Challenges the mythology surrounding early American immigrants and encourages reading more on the topic.

### Quotes

- "Do I think these kinds of people will build this country? Well, yeah. It's kind of the only thing that ever has."
- "At the end of the day, they are trying to save their children's lives, trying to save their own lives."
- "Somebody who is willing to leave their home country, go to a place where they don't have connections, don't really speak the language, have very little chance of being able to get too far ahead, to me, that's not somebody you turn away."
- "They will build this country. They'll be a benefit, just like every group before."
- "The mythology that surrounds early American immigrants who came here from Europe, it's not real."

### Oneliner

Responding to criticism, Beau defends asylum seekers' sacrifices and contributions to building the country, debunking myths about early American immigrants.

### Audience

Advocates for Immigration Rights

### On-the-ground actions from transcript

- Support asylum seekers in your community (implied)
- Educate others on the realities and challenges faced by asylum seekers (implied)

### Whats missing in summary

The full transcript provides a deeper understanding of the historical context and challenges faced by asylum seekers, shedding light on the importance of supporting these individuals in building a better future.

### Tags

#Immigration #AsylumSeekers #MythBusting #History #CommunitySupport #DebunkingNarratives


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a message I received, a little critique, some questions,
some comments.
Came in after the most recent video about what's going on down south.
And we're going to go through it and talk about some underlying things in it.
Okay so, I have always had a problem with you.
I almost never agree, but watch for a unique take.
However, I can't remain silent.
I'm very disappointed in how easily you brushed aside the unfitness of parents who would subject
their children to the trip to the US.
Do we really want these kinds of people?
You honestly seem like you want a stronger built country.
Do you really think they'll help?
If they don't care about their kids, will they care about anything else?
If you're going to use my name and respond to this, please use my name.
And then he gives his name.
Before we get into this, let's get a real clear picture of the kinds of trips we're
talking about, what they are.
We have a good narrative from a school teacher who got to witness one with his own eyes.
And this is how he describes it.
Terrible misery, stench, fumes.
Doesn't sound so good.
He goes on.
Fever, dysentery, headache, heat, constipation, boils, scurvy.
Scurvy?
I'm on the wrong page.
This is how Europeans got here in the 1750s.
I'll put this narrative down below in the comments.
It's definitely worth looking at.
It dispels some myths.
You know, we talk a lot about the difference between American history and American mythology.
American mythology says European immigrants in the early days, they showed up here, they
got off the boat skipping and holding hands and then they went and made the Liberty Bell
or whatever.
That is not what happened.
That's mythology.
American history is very different.
They had a horrible trip as well.
And then when they got here, they waited off the coast until people came to purchase them
off the boat.
In this narrative, he talks about baptizing children who were in distress.
Do I think these kinds of people will build this country?
Well, yeah.
It's kind of the only thing that ever has.
You want to know a real bizarre historical correlation?
The worse the trip was to get here, the more work that group did.
There aren't really exceptions to this.
If you think about the absolute worst trips to get to the United States, they did most
of the work.
It's how it's always been.
I think we need to get rid of the idea of trying to grasp at straws to demonize and
other and marginalize these groups of people.
At the end of the day, they are trying to save their children's lives, trying to save
their own lives.
There really isn't a whole lot that is out of bounds as far as acceptable behavior when
that's your goal.
Sending your kid from immediate harm into an admittedly bad situation, but better than
what they were in, I don't think that's a sign of being an unfit parent.
We also have to acknowledge, again, there is a difference between migrants and asylum
seekers.
The people who are doing this, they're asylum seekers.
You want to talk about those who are just coming here for a job, those that get belittled
as economic refugees, fine, talk about them.
As far as I'm concerned, I want them too, because somebody who is willing to leave their
home country, go to a place where they don't have connections, don't really speak the language,
have very little chance of being able to get too far ahead, to me, that's not somebody
you turn away, that is somebody you give a small business loan to, because they have
drive.
They will build this country.
They'll be a benefit, just like every group before.
I really do suggest reading this, because the mythology that surrounds early American
immigrants who came here from Europe, it's not real.
It's not even close to accurate.
What we envision is not what happened.
Certainly it was better than a lot of trips here, but it wasn't easy, and those people
certainly put their kids at risk doing it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}