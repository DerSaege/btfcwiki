---
title: Let's talk about Biden, McConnell, and the filibuster....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8AMyq9DYHew) |
| Published | 2021/03/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Major Democratic Party figures support altering or repealing the filibuster, with some advocating for a return to a talking filibuster.
- The filibuster allows the minority party, currently Republicans, to block legislation unless 60 votes are secured in the Senate.
- Democratic leaders are pushing for changes to the filibuster, aiming to encourage healthy debate.
- Biden plans to advocate for a return to a talking filibuster, where senators must speak to hold up legislation.
- Beau suggests requiring senators to stay on topic during filibusters to prevent irrelevant speeches.
- Republicans, led by Mitch McConnell, strongly oppose filibuster changes, warning of drastic measures if it's altered.
- McConnell threatens to pass unpopular proposals if filibuster rules change, but this move may not deter Democrats.
- Beau points out that threats to pass divisive legislation could backfire on the Republican Party, revealing their true agenda.
- He notes that many Americans are unaware of the Republican Party's platform, which now centers around loyalty to Trump.
- Beau predicts potential movement on the filibuster debate in the coming weeks unless Republicans cooperate on Biden's agenda.

### Quotes

- "If I was rewriting the rules I would make sure that they also have to stay on topic while they are debating."
- "That's what you want. That's what you want. That's what you want to put out there."
- "It might inform people about what the Republican party really is."

### Oneliner

Major Democratic figures advocate altering the filibuster, while Republicans resist, risking revealing unpopular agendas.

### Audience

Legislative aides, political activists

### On-the-ground actions from transcript

- Contact your senators to express support for or against filibuster reform (suggested)
- Stay informed on the ongoing debate over the filibuster and its potential impact on legislation (implied)

### Whats missing in summary

Deeper insights into the potential consequences of filibuster changes and the importance of public awareness in shaping political decisions.

### Tags

#Filibuster #PoliticalDebate #DemocraticParty #RepublicanParty #Legislation


## Transcript
Well howdy there internet people it's Beau again. So we've had some developments in the campaign to
change or alter or repeal the filibuster. Some major Democratic Party heavy hitters have come
out in favor of changes. Not many going all the way to repeal as of yet but they're saying it's
still on the table. Quick recap for those who don't know the filibuster is a process that exists
in the Senate that allows the minority party, in this case the Republicans, to hold up legislation
unless the other side can get 60 votes. Short version that's what it boils down to. It's a
Senate rule. It's not part of the Constitution. It's something that exists theoretically to
encourage healthy debate. That's why it's there. Now a whole bunch of Democratic Party members and
now a lot of the leadership are advocating for changes. Biden himself has said that he plans
to advocate to switch back to a talking filibuster which means if they want to hold up the legislation
for debate purposes they have to get up and talk. I think that's a great idea. I think that's a
great idea. If I was rewriting the rules I would make sure that they also have to stay on topic
while they are debating which means they can't just get up there and read a menu or talk about
what they had for breakfast. It has to be related to the topic at hand. Now predictably the Republican
Party is less than happy about this. They have leveled some warnings. The most prominent coming
from Mitch McConnell who says that if the filibuster is changed well then it is scorched
earth in the Senate and the next time that the Republicans retake the Senate well they will push
through a bunch of proposals and pretty much everything he named is wildly unpopular with
the American people and trending to be more unpopular as the years go on.
I mean if that's what you feel you have to do Senator, if you're that upset about this and
you just decide you want to torpedo your party even further I don't think anybody's going to stop you.
But I'm fairly certain that threatening to pass a bunch of anti-immigrant, anti-choice legislation
isn't going to have quite the effect you think it is because what it's done is shown your hand.
That's what you want. That's what you want. That's what you want to put out there and it's
more motivation to push Biden's agenda through for the average person. It's not quite the
deterrent you think it is. You can promise all of the fireworks and shenanigans you want
in the Senate but at the end of the day the policies that the Republican party has used
to divide this nation they're no longer really divisive. You don't have a large percentage of
the American populace in support of most of these. Most people who are in favor of the
Republican party they have no idea what your positions are. They don't know what your platform
is because the Republican party doesn't have a platform anymore. It's all about loyalty to Trump.
If you come out and openly state all of these wild positions and try to push them through,
I mean I would suggest that it might inform people about what the Republican party really is.
Where they have gone and how they aren't really representing the average American anymore.
So I mean it's an interesting turn of events and I think it might we might see movement on this
in the next week or two unless Republicans come to the table on large pieces of Biden's agenda.
Anyway it's just a thought. Y'all have a good day. Stay safe tonight.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}