---
title: Let's talk about us, them, and how we talk about foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BWuai501WG4) |
| Published | 2021/03/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration gearing up for active foreign policy.
- Urges acknowledgment of the disconnect between government actions and average citizens in a representative democracy.
- Emphasizes the distinction between the government's actions and the general population when discussing foreign policy.
- Illustrates the issue using a hypothetical scenario involving Ireland to show the absurdity of collective blame.
- Stresses the importance of recognizing that foreign policy decisions are made by a select group in power, not the entire nation.
- Points out the danger of holding whole countries accountable for the actions of a few in power.
- Notes the common desires for safety and stability among people globally.
- Warns against collectivizing entire groups of people in foreign policy discourse.
- Compares the need for awareness in foreign policy to historical issues of race collectivization in the United States.
- Recommends reading foreign policy articles to see how nations are often homogenized in discourse, despite diverse populations.

### Quotes

- "We have more in common with the average person in Dublin or Kabul than in most cases we do with our own representatives."
- "If enough people recognize this, recognize the similarities, we can move towards accommodating those universal wants which will lead us to peace."
- "The Irish, we're not actually talking about the people."
- "It's not going to change the language of foreign policy."
- "We need to be very conscious of that team mindset."

### Oneliner

Be aware: Foreign policy talks often blur the line between government actions and the people, urging recognition for global peace.

### Audience

Global citizens

### On-the-ground actions from transcript

- Recognize the distinction between government actions and general population in foreign policy discourse (implied).
- Read foreign policy articles to see how nations are often homogenized in discourse (implied).

### Whats missing in summary

The full transcript expands on the importance of individual awareness in understanding foreign policy dynamics and the potential for global unity through recognizing common desires for peace and stability.

### Tags

#ForeignPolicy #GovernmentActions #GlobalUnity #Peace #Awareness


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about us, we, them.
We're gonna talk about a really important concept
when it comes to foreign policy
and the way we talk about foreign policy.
We're gonna do this because the Biden administration
has made it pretty clear that they plan
on being a whole lot more active on the foreign policy scene.
So there's something that we need to consciously acknowledge
before that happens,
because we're gonna be talking about foreign policy a lot
for the next couple of years.
In the United States, we have a representative democracy.
We are theoretically a government of the people.
So we say we.
We are doing this.
When the decision to do that was made,
were you at the table?
I wasn't.
Nobody asked me.
But we collectivize that
when we are talking about international affairs.
And on some level, that's okay.
It makes speech a little easier.
But we have to acknowledge
that when we are talking about American foreign policy
and we say the Americans,
that doesn't mean the average person in Birmingham
or Cleveland or LA.
It's a small group of people
who are using the tools of foreign policy
to enact an agenda.
That's what it is.
We're going to use a country
that doesn't often cause waves on the foreign policy scene
to show how absurd this practice can get.
If Ireland decided to seize a bunch of grain shipments
and we said the Irish are doing this,
that's how we would talk about it on the foreign policy scene,
does that mean that every person in Ireland is doing that?
Of course not.
The people in power using the tools of foreign policy
to enact an agenda.
However, what if the US decides to intervene in this practice?
The Irish are the ones who are doing it.
Who gets held accountable?
Who are we okay with becoming collateral?
We have to be sure that we make that distinction,
that we are very aware that when we are talking about foreign policy,
even though the words we're using are interchangeable,
the Irish, we're not actually talking about the people.
It's an incredibly important concept
that if enough people recognize it and draw that line,
it will quite literally save lives
because we won't be okay with it.
And even though we don't have a whole lot of power
over our government, we have some.
Another thing to acknowledge is that in most countries,
most people have even less control over their government
and therefore their foreign policy than we do.
It's not right to hold them accountable for it,
not the whole nation.
We also tend to collectivize entire countries.
Go ahead and tell you now, any sentence ever uttered
throughout history that started with,
Afghans want, is inaccurate to some degree.
All generalities are, but that's especially true there
because when we say that we're talking about foreign policy,
what we really mean is the Afghan government, those in power.
There's more than a dozen ethnicities there.
They're not all aligned.
The average person in Afghanistan wants the same thing
the average person in Ireland or the average person
in the United States wants, safety and stability.
People all over the world are pretty united on that
and we need to acknowledge it because it will curtail
our willingness to look away when in a sense are lost.
We can't collectivize entire groups of people like that.
In the United States, we know this is bad.
We did it with race in this country
and nothing good has ever come of it.
We need to be aware of it when we're talking about foreign policy.
It's not going to change the language of foreign policy.
People have talked this way about foreign policy forever.
It's not going to change any time soon,
but we need to consciously draw that line in our minds
because we have more in common with the average person in Dublin or Kabul
than in most cases we do with our own representatives.
We need to be aware of that.
Another thing to do if you really want to make this point clear to yourself,
read some foreign policy articles.
They're written by people all over the world.
You will see the sentence, the United States, the Americans,
America is doing this and you may not like what you see.
It's not an attack on you.
It's an attack on the policies of the government.
We have to be very conscious of that team mindset
because while in a lot of ways it can be good,
in a whole lot of ways it can lead to some really bad things.
If enough people recognize this, recognize the similarities,
we can move towards accommodating those universal wants
which will lead us to peace,
which theoretically should be the goal of foreign policy.
Anyway, it's just a thought. I'll have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}