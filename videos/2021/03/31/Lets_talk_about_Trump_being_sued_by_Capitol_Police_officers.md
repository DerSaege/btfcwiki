---
title: Let's talk about Trump being sued by Capitol Police officers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_hVj316jLkA) |
| Published | 2021/03/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Donald Trump faces a lawsuit brought by two Capitol Police officers seeking $75,000 in compensation for injuries sustained on January 6th.
- The lawsuit holds Trump responsible for the events of January 6th, pointing to his history of encouraging violence and tensions with baseless election claims.
- The suit specifically mentions a tweet by Trump on December 19th, which encouraged people to attend the event on January 6th, stating it will be "wild."
- The lawsuit is part of a series of civil actions seeking accountability for the events of January 6th.
- The Republican Party is still embroiled in a power struggle, with many members following Trump's lead despite the legal challenges he faces.
- Beau questions the long-term implications of the power struggle within the Republican Party and the abandonment of democratic ideals by some members.
- He urges Republicans to step up and take control of their party to prevent open authoritarians from gaining power.
- There is an active effort in the country to undermine foundational elements and promises, requiring Republicans to make a stand.
- Beau calls on Republicans to support those facing primary challenges for not supporting Trump and to take ownership of the direction of their party.

### Quotes

- "Former President Donald Trump faces a lawsuit by two Capitol Police officers seeking compensation for injuries on January 6th."
- "The lawsuit holds Trump responsible for encouraging violence and tensions that led to the events of January 6th."
- "Republicans must step up and prevent open authoritarians from taking control of the party."

### Oneliner

Former President Trump faces a lawsuit from Capitol Police officers seeking accountability for January 6th, urging Republicans to prevent authoritarian control.

### Audience

Republicans

### On-the-ground actions from transcript

- Support Republicans facing primary challenges for not supporting Trump (implied)
- Take ownership of the Republican Party to prevent authoritarian control (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the ongoing legal challenges against Trump and the internal power struggle within the Republican Party, urging action to uphold democratic ideals and prevent authoritarian influence.

### Tags

#DonaldTrump #CapitolPolice #RepublicanParty #Accountability #Authoritarianism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about former President Donald Trump's latest headache,
his latest issue.
It's another lawsuit.
This one is a little different than most of his lawsuits.
This one is being brought by two cops, two Capitol Police officers, been with the force
17 years and 11 years.
They are seeking $75,000 as compensation for injuries they are said to have sustained on
the 6th.
Now up to this point, the former president has accepted exactly zero responsibility for
the events of the 6th.
The suit lays that entire day squarely at his feet.
So the suit begins here.
This is a complaint for damages by US Capitol Police officers for physical and emotional
injuries caused by the defendant Donald Trump's wrongful conduct in citing a riot on January
6th, 2021.
It goes on to detail his history, talk about how he inflamed tensions with his baseless
claims about the election, the claims that inflamed the entire nation.
It alleges that his previous history of encouraging violence from his supporters directly led
to the 6th.
It points specifically to a tweet on December 19th that encourages people to attend the
6th and says something to the effect of, be there, it will be wild.
It was wild.
Five people, including one cop, gone.
And at this point, regardless of this suit, we have to watch them all because this is
a, just the most recent attempt in a long string of civil actions attempting to obtain
some kind of accountability for what occurred that day.
Meanwhile, the Republican Party is still in this power struggle, still taking marching
orders from the former president.
I'm wondering how that's going to play out as these cases make their way through the
courts.
It's going to be very hard for the Republican Party to pretend to be the party of law and
order when you have cases like this going forward.
I imagine there will be more, very similar.
You know, when Biden got elected, there was a pretty big sigh of relief.
And I get it, immediate harm, right?
Reduced.
Got it.
But the thing is, this power struggle that's occurring within the Republican Party, it
has long-term implications.
Who wins?
It matters.
Because about half of the Republican Party has abandoned the republic.
Abandoned representative democracy, abandoned the basic ideals this country was founded
on.
You have Republicans talking about quality voters and attempting to limit those that,
well, they're not really of quality, meaning they're not going to vote for me.
Is that the United States we want?
And are we really ready to relax when those are open statements?
Are we ready to just say, OK, everything's back to normal because the Biden administration
has taken over?
I don't think that's necessarily a good idea.
Doesn't seem to be the way things are playing out.
There is an active effort in this country to undermine, undermine the things that are
seen as foundational elements, the promises that were made.
It's time for Republicans to step up.
You know, there are a lot of people, well, we're going to primary you if you don't support
Trump.
Those people being primary for that reason, they probably need support.
It's your party.
If you want to maintain control of it and you don't want open authoritarians in control
of it, it's up to you as a Republican.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}