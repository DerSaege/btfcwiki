---
title: Let's talk about Biden, the stimulus, and the midterms....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=reHjvbSRuTM) |
| Published | 2021/03/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Shares his evolving opinion of Biden post-election.
- Expected a reboot of the Obama administration but notes Biden is doing more than that.
- Acknowledges progressive expectations of Biden but personally only expected minimal changes.
- Explains why Biden didn't criticize Republicans in his speech.
- Talks about the stimulus bill and Republican opposition to it.
- Details leftist provisions in the stimulus bill like child tax credit and healthcare subsidies.
- Mentions the impact of these provisions on working-class Americans.
- Remarks on Biden's approach to more progressive policies than anticipated.
- Speculates on Biden's strategy for pushing through progressive policies.
- Comments on the potential outcomes of Biden's approach.

### Quotes

- "I do have a higher opinion of Biden today than I did before the election."
- "Biden is running on this theme of unity or whatever. We all know it's not real."
- "He is working to try out ideas that are far more progressive than anything I ever expected from Biden."
- "It's surprising, surprising to me. I didn't expect it from him."
- "I do have a higher opinion of it."

### Oneliner

Beau explains his evolving opinion on Biden, noting unexpected progressiveness amidst strategic political maneuvering.

### Audience

Political observers

### On-the-ground actions from transcript

- Share information about the provisions in the stimulus bill with working-class Americans (implied).
- Stay informed about Biden's policies and their potential impact on various groups (implied).
- Pay attention to the details and long-term goals of political leaders' actions (implied).

### Whats missing in summary

Insights on Beau's perspective and analysis on Biden's administration can be best understood by watching the full transcript.

### Tags

#Biden #ProgressivePolicies #StimulusBill #PoliticalAnalysis #EvolutionaryOpinion


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about Biden.
What he has done, what he's planning on doing, what he learned from democratic
presidencies of the past and how he plans to avoid the same mistakes they made.
And, uh, me, we got two, uh, questions here.
One is really more of a comment, but it kind of leads into the other thing.
It said that I was the only person on YouTube who had a higher opinion of Biden today than
I did before the election.
I don't know if that's true.
I don't know the opinions of everybody on YouTube, but if I seem like I have a higher
opinion of Biden today than I did before the election, it's because I have a higher
opinion of Biden today than I did before the election. Yeah, that's an accurate read.
I did not expect a whole lot from Biden. Like nothing, really. If you were around then,
you know, numerous times I said, he's not my man. He is not my people. We don't share the same
politics. We don't share the same ideology. He's not my guy. He's just better than the other guy.
He's a stopgap.
Honestly, I expected a reboot of the Obama administration, and I think there's a lot
of people who think that's what we're getting.
It's not.
Not if you're paying attention.
That's not what's happening.
I also think there's a habit among people who talk about politics to talk about what
should be rather than what is first.
You can talk about what should be, but you have to acknowledge the reality of the world that you're in.
And I think there were a whole lot of progressive people who wanted things from Biden and expected things from him that
he never agreed to.  I never did.
I just expected him to do kind of the bare minimum, revert and get rid of the
worst of Trump's policies. That was my expectation of him. He's done more than
that already, so yeah, I do have a higher opinion of him. And we're gonna talk
about why, and I think a lot of people's opinions about him are gonna change.
because it goes into that next question, and that was why didn't he trash the
Republicans in his speech? Democrats don't know how to wield power. He
should have pointed out that they voted against the stimulus and all of this
stuff, and they've been obstructionist, and yeah, I mean, I get it, and that would
make sense if you weren't doing what Biden's trying to do. First, he
He doesn't actually need to do that because Republicans have been trained by Trump to
call everything radical left and distance themselves and be obstructionist and point
out that they're being obstructionist.
Biden is running on this theme of unity or whatever.
We all know it's not real.
There's no unity up there.
And Republicans are pointing it out, so he doesn't have to.
can get up there and take the high road and not really suffer for it because the Republicans
are doing the work.
And in that stimulus bill, there's a whole lot of stuff that Republicans are going to
have to answer for.
And they've done it themselves.
They have done it themselves.
have pointed out that they didn't support it. They pointed out that it was
left. It's not. But there are some breadcrumbs in it. Some little hints of
leftist stuff. Social Democrat stuff. People have talked about the child tax
credit and they're like it's gonna lift kids out of poverty. It's great. Standard
talking point. And yeah, I mean, that sounds like DC stuff. This is a little
different. It's going to be an unusual tax credit in some ways, because some
people will be able to collect a portion of their tax refund each month from July
on. They'll be able to just go ahead and take a couple hundred bucks a month from
And the impact of this is if you are a minimum wage worker, have two kids, you're about to see
50% increase in your take-home pay, roughly 47-ish, somewhere in there.
That sounds kind of UBI-ish, because it is. It is. There's some healthcare provisions. People who
lost their job, they're going to get free health care for a couple months, and
there's more subsidies. It's not socialized, that would be bad. Subsidies.
Free health care. There are little bits and pieces of left-wing stuff throughout
it. Not real left, but American left. And it's a test run. Working-class Americans
are going to get to experience it.
And most of this stuff ends right before the midterms.
And the Republicans who are now sitting there and have been sitting there saying, this is
left, this is left, it's bad, we're going to vote against it, are going to have to explain
they voted against it, and convince the working class Americans, who I would assume would grow
to like some of this stuff, that they will vote to keep it. There have been a lot of democratic
presidencies in the past that suffered major setbacks because of losses at the midterms.
Biden has set the stage to make sure that doesn't happen to him.
He is working to try out ideas that are far more progressive than anything I ever expected from
Biden, to be completely honest. And I don't think there are many people on this channel watching
this that expected anything like that from him. Status quo. And it is. I mean, it's still not
real left policies, but it's a whole lot further than I think anybody genuinely
expected. Not what they wanted, not what should be, but in reality what is I don't
think anybody expected this kind of stuff. And the only reason you would run
a test run using these things is if you planned on pushing more through after
the midterm, Biden, uh, it's been said that Biden wants a FDR style presidency.
If this little test run works, if the American people respond to the little
bits and pieces that are embedded in all of this various stuff, he might be on his
way to it. It's surprising, surprising to me. I didn't expect it from him. He never
hinted that that's what he was going to do. But that is certainly where it seems
to be heading. Now, maybe we're wrong. Maybe working-class Americans are going
to get a little taste of unions and healthcare and social democratic
policies, and they don't like it.
Maybe that happens.
He suffers a big loss at the midterms, or maybe there's some scandal.
I don't think that's going to happen though.
I think Biden's plan is to do exactly what he's been doing.
Stay out of sight, let everybody underestimate him, push through bills that are very vague
and have little bits and pieces of stuff in it.
Stuff that will have a tangible impact on a whole lot of people.
And then Republicans, in their obstructionist nature, will have voted against it and they
will have to defend those positions come the midterms.
And it is far beyond anything I expected from Biden.
So yeah, I do have a higher opinion of it.
I do.
And this doesn't just extend to stuff like this.
I know the way people feel about the foreign policy, but it's the details.
It's the intent.
It's the long-term goal that you have to look at.
And it's not the same.
I mean, it really isn't.
There are a lot of signs that show that he is testing the waters for a far more progressive
administration than anybody expected.
But he's doing it in a way that allows him to backtrack very quickly into normal democratic
establishment mode if it doesn't go well.
So anyway, it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}