---
title: Let's talk about Tucker Carlson and women in the military....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mlO-r6OeCWA) |
| Published | 2021/03/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tucker Carlson criticized the military for women getting proper maternity uniforms and relaxed hairstyles.
- Carlson's opinion on combat readiness holds no weight with those who have experienced combat.
- Women needing maternity uniforms for active duty activities don't need a lecture on toughness from Carlson.
- Beau suggests women veterans participating in a charity boxing event with Carlson.
- Beau challenges Carlson to an obstacle course and PT test against women from each branch.
- Chinese military is mentioned as becoming more masculine, but they have about 100,000 women in their military.
- Beau points out Carlson's lack of understanding when criticizing Lloyd Austin's qualifications.
- The idea of maintaining Roman Legion standards in the military is deemed ridiculous by Beau.
- Beau suggests PT standards should be more job-based rather than uniform across the board.
- The Army responded to Carlson's comments by tweeting images of women soldiers.

### Quotes

- "Nobody could honestly argue that he wasn't qualified."
- "After the last couple of years, I suggest it's vital that it changes."
- "Fox News is a detriment to morale, and that might change."
- "I think it'd be entertaining to watch."
- "It's just a thought, y'all have a good day."

### Oneliner

Beau challenges Tucker Carlson's criticism of the military's standards, calls out his lack of understanding, and suggests a showdown with women veterans to prove a point.

### Audience

Military advocates, Women in the military

### On-the-ground actions from transcript

- Challenge harmful narratives: Address misinformation and stereotypes (suggested)
- Support women in the military: Advocate for fair treatment and respect (suggested)
- Boost morale: Share positive stories and images of women soldiers (implied)

### Whats missing in summary

The full transcript provides detailed insights into the criticism faced by the military and the pushback against it, showcasing the importance of understanding and supporting women in the military.


## Transcript
Well howdy there internet people it's Beau again.
So today we're going to talk about Tucker Carlson begrudgingly and the military and standards
and tough guys all that stuff. If you are unaware Carlson went on his show to bemoan
the way the military is right now and to suggest that it is a bad thing that women in the military
are getting maternity uniforms that fit properly and slightly more relaxed hairstyles.
He talks about how the military is supposed to be a meritocracy.
He talks about standards, talks about how the Chinese military is becoming more masculine.
Ours isn't. Let me just start by saying there are precisely zero people who have ever heard
a shot fired in anger who think that Tucker Carlson's opinion on combat readiness matters.
Not a single one. Nobody really thinks that his opinion should count for anything.
Furthermore, I want to just point out that I am 100% certain that women who need proper
fitting maternity uniforms, meaning they are going to be engaging in active duty activities
while pregnant, so pregnant that they're showing and need a maternity uniform,
I'm 100% certain that they do not need a lecture on toughness from Tucker Carlson.
That doesn't seem very likely to me. He doesn't know what he's talking about. He has no clue what
he's talking about. Honestly, to me, it seems like he just reads whatever they put on a teleprompter
in front of him. I know one or two women vets. I have a roster of them who would like to
participate in a charity boxing event with Carlson after this. I don't think that that's fair.
They would wipe the floor with him. That's not a joke because they're women and he's going to
get beat up by a girl. I used to teach unarmed defense and there's a couple of them I'm fairly
certain could take me. It's just that it wouldn't be entertaining. Little bits of bow tie and blue
blazer flying everywhere. Since he is so concerned about standards, I would suggest that he put on
his boots, deck shoes, whatever he has, and come on down and we'll arrange an obstacle course and he
can do a PT test. We'll bring in one woman from each branch and we'll see how many of them beat
him. Not if they do, just how many. As far as the masculine Chinese military, because what the
United States needs right now is more rhetoric aimed at Asian people. That's a good idea. That's
good for the country. As far as the idea that they're more masculine, I would just like to point
out that even with a conservative estimate, their military has about 100,000 women in it.
He doesn't know what he's talking about. Furthermore, to completely illustrate this, he goes on in his
defense after DOD rightfully responded to this attack, he goes on to talk about a man named Lloyd
Austin who was plucked from the private equity world, who became Secretary of Defense. But you're
not supposed to notice that he came from the private equity world. You're only supposed to
notice that he's black. He says this after going on about meritocracy. So if meritocracy is important,
I would suggest that you addressed Lloyd Austin as General Austin, former commander of CENTCOM,
something that was left out of his little rant that seemed to paint him as unqualified.
Now, to be clear, I actually wasn't in favor of Austin being confirmed, not because he didn't
know what he was doing, but because he knew too much. He's a recently separated general.
He still has a lot of contacts. He could theoretically, he would be a Secretary of Defense
that could trigger a coup because he has contact with regional commanders.
Not saying he would. It's not like he's a Trump appointee or anything.
Just that he could, and it's not a good habit to get into.
And there's a video on this. This isn't like me after the fact saying this.
And in it, I actually say that literally nobody could honestly argue that he wasn't qualified.
But we have Tucker Carlson kind of insinuating that he is.
This idea that the military has to maintain the standards of a Roman Legion is ridiculous.
The modern battlefield doesn't require that. Those standards, the PT standards,
at this point in time, they should probably be more MOS based, based on the job, which is true
in some ways. That's how it should be. I personally do not care if a surgeon can run.
That's not a big concern of mine.
For their part, DOD did respond, and the Army responded in the classiest of ways.
They just tweeted out images of women soldiers all day.
Maybe Carlson needs some new writers.
Because I can't imagine a new writer.
Because I can't imagine that Fox News thinks it's a good idea to attack the military,
to go after soldiers, active duty soldiers, when on a whole lot of bases and a lot of common areas,
Fox News is the station they allow to be on. But see, now Fox News is a detriment to morale,
and that might change. It should change.
After the last couple of years, I would suggest it's important that it changes.
So I hope Mr. Carlson takes me up on my offer, comes down, we can do an obstacle course,
he can put it on Fox News, we can put it on the channel, I don't care.
I think it would be entertaining to watch.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}