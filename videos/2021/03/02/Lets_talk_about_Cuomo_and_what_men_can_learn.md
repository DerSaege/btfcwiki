---
title: Let's talk about Cuomo and what men can learn....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nn8jWioayXc) |
| Published | 2021/03/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the confusion among men in the United States regarding flirting at work post-Cuomo situation.
- Points out that the motivation behind changing behavior doesn't matter as long as the behavior changes.
- Emphasizes the shift in responsibility from women avoiding vulnerable situations to men not creating them.
- Simplifies the rules for men by stating that they shouldn't flirt at work to avoid putting women in vulnerable positions.
- Advises to keep distance, not close doors, or block exits to maintain a professional environment.
- Urges men not to exploit power dynamics or vulnerabilities in interactions with women.
- Suggests having HR present for private interactions to ensure appropriateness and avoid misconduct.
- Criticizes the excuse of not knowing the rules, implying that men do understand but may be looking for ways to bend them.
- Argues that true flirting is subtle and not graphic or overt as often claimed in harassment cases.
- Encourages watching a video on consent and reiterates the simple rules of respecting boundaries and keeping interactions professional.

### Quotes

- "It's your society."
- "Don't exploit vulnerabilities and keep your hands to yourself."
- "Don't say anything you wouldn't say if HR was standing there."
- "The confusion, I don't buy it either because we know the rules."
- "How's that for PC?"

### Oneliner

Men in the US should refrain from flirting at work to prevent creating vulnerable situations for women, maintaining professionalism and respect.

### Audience

Men in the US

### On-the-ground actions from transcript

- Keep distance, avoid blocking exits, and refrain from inappropriate advances (implied)
- Have HR present for private interactions to maintain professionalism (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the rules and behaviors that men should follow to ensure a safe and respectful work environment for women. Watching the full transcript can provide a deeper understanding of these guidelines and the importance of respecting boundaries in professional settings.

### Tags

#WorkplaceBehavior #Professionalism #Respect #GenderDynamics #Boundaries


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about what men in the United States can learn from the Cuomo situation.
Because apparently there is some confusion.
And I have an email.
Gonna read it.
I don't actually like you.
And I certainly don't like Cuomo.
He's not my man.
Never even met him.
But I watch you because you break down PC crowd issues into plain English.
Please, can you tell me the rules for flirting at work now?
Stuff has changed and I really don't know the rules anymore.
I'm not saying poor me.
Stuff changes and we have to adapt.
But I don't really know what I'm allowed to say or do anymore.
Okay, I'm going to take this at face value.
There is absolutely no indication in this as to whether you want to alter the behavior
because you just don't want to get in trouble or you don't want women to be harassed.
But it does seem to indicate that you are willing to change behavior.
At the end of the day, the motivation doesn't matter.
The outcome is the same.
The behavior has changed.
It's your society.
Okay, this whole idea that, well, I don't know the rules.
Yeah, you do.
You have given them as advice your entire life.
For decades in this country, men have told women, hey, you need to be situationally aware.
Be aware of your surroundings.
Don't be an easy victim.
Don't be a hard target.
Don't put yourself in a vulnerable situation.
The only thing that has changed is that it is now no longer their responsibility to avoid
those situations.
It is your responsibility to avoid creating them.
When you think about it in those terms, the rules become really simple.
What's the first thing that you say when you're talking about telling somebody to be situationally
aware?
Be aware of their surroundings.
What's the first piece of advice?
Never go into a place you don't know how to get out of, right?
Always have that escape route.
They're at work.
They can't leave.
There's no escape.
Therefore, they're vulnerable.
Logic then dictates, don't flirt at work.
They have no way out, right?
So don't put them in that situation.
They cannot get away from your advances, your flirtations, and we're going to talk about
that word in a minute.
So don't do it.
Now realistically, judging by the email, you're going to completely ignore that.
And I imagine that other people will too because it isn't always that simple.
Real life, it's not always that simple.
There's a joke, and it is a joke, but it actually hammers home the point pretty well.
Don't say anything to women that you wouldn't want a man to say to you in prison.
That helps.
Helps put focus on it, right?
It's really simple stuff.
Keep your hands to yourself, right?
If you were giving advice, you would say to keep your distance.
Now it's up to you to keep your distance.
If you were giving advice, you would say, you know, always have an escape route.
That means in your office, don't close the door.
Don't go to a supply closet.
Don't block the door.
Don't lean up against something so they can't get away.
All the stuff you read in those how to pick up women books, don't do any of it because
almost all of that is about exploiting a power dynamic, exploiting a vulnerability.
It is now up to you to avoid creating the situation.
It's not up to them to get away from it.
You're subordinate.
Definitely don't do it.
Because not only can they not leave, you have power over their career, even if it's just
a perception of it.
Even if technically you're outside of their chain.
Don't do it.
Other advice, what is it?
Don't drink so much, right?
You probably shouldn't ask them out for drinks.
Any situation that you would advise against, don't present.
It's really that simple.
Now let's say you actually have to have a conversation that needs to be in private.
So you want to close the door, right?
Have HR be there.
You're not going to say anything inappropriate with HR there.
And that in and of itself proves that this talking point of, oh, I don't know the rules,
isn't a real one.
Because if you had every conversation the way you would with HR standing right beside
you, you'd never get in trouble.
You know what you're supposed to say and what you're not.
The real question here is, how do I get away with it?
That's really what's being asked.
It's not like that.
It's not like that.
And you certainly wouldn't get that advice from me.
The goal is for you to avoid creating those situations now.
Not finding out how you can create them and get away with it.
Let's talk about flirtations as well.
Now I'm not familiar with the Cuomo thing, to be honest.
Have no idea what he did or what he's accused of.
I literally know nothing other than he was accused of something.
That's the entire base of my knowledge on this.
But generally speaking, when this talking point comes up, some public figure got accused
of harassing somebody.
And then they say, oh, I was flirting.
And it was misinterpreted.
Yeah, and then you look at the text messages or the quotes of what was said.
It's never flirting.
Flirting is subtle.
The key to flirting is subtlety.
Most times, the comments in question are not subtle.
They're overt and they're gross most times.
A photo, an inappropriate photo, that's not flirting.
Anything that is graphic in detail is not flirting.
This whole attempt to recast stuff as just simple flirtation that you didn't know any
better.
Again, would you say it?
Would you do it with HR standing there?
I don't buy the talking point to begin with.
And the confusion, I don't buy it either because we know the rules.
We gave it as advice for decades.
It honestly really just seems like a hidden attempt to say, well, I don't like this.
Yeah, okay, so you don't like it.
Nobody cares, to be honest.
How's that for PC?
If you want to get into this, there's a good video about T, the modern concept of this
crazy thing called consent.
I'll put it down below in the comments section.
It's worth watching.
But at the end of the day, this is really simple.
Don't exploit vulnerabilities and keep your hands to yourself.
Don't say anything you wouldn't say if HR was standing there.
It's not a secret.
It's not hard.
Again it boils down to people not wanting to do it.
Not that they don't understand it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}