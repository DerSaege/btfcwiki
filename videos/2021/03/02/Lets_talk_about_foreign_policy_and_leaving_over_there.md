---
title: Let's talk about foreign policy and leaving over there....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nO2FypF5LOQ) |
| Published | 2021/03/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Foreign policy is about power, not patriotism or ideology.
- American foreign policy is complex and cannot be simplified into slogans.
- The discrepancy between what should happen in foreign policy and what actually happens is vast.
- When discussing leaving the Middle East, the reality is more complex than just withdrawing troops.
- Beau explains the power dynamics in the Middle East, focusing on Iran, Saudi Arabia, Israel, and Turkey.
- Biden's foreign policy centerpiece involves reshaping the balance of power in the Middle East by bringing Iran to the forefront.
- The goal is to create a system with multiple equal poles of power in the region.
- Deprioritizing military presence in the Middle East and focusing on humanitarian aid is suggested.
- Beau stresses the importance of understanding the power dynamics and motives involved in foreign policy decisions.
- Cooperation over subjugation is presented as a more progressive approach to international relations.

### Quotes

- "Foreign policy is about power, not about right and wrong."
- "It's about power. And when you're talking about the Middle East, you're talking about unimaginable power."
- "Cooperation rather than subjugation will move humanity further faster."

### Oneliner

Foreign policy is about power, not patriotism; Biden aims to reshape the Middle East's power balance by elevating Iran and fostering cooperation over subjugation.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Advocate for deprioritizing military presence in the Middle East through humanitarian aid efforts (implied)
- Understand the power dynamics and motives behind foreign policy decisions (implied)
- Push for cooperation over subjugation in international relations (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of American foreign policy, focusing on power dynamics, the Middle East's geopolitical landscape, and Biden's goals for reshaping the region. Watching the full transcript gives a nuanced understanding of these complex issues.

### Tags

#ForeignPolicy #MiddleEast #PowerDynamics #Biden #Cooperation


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to have a very
candid conversation about over there. The history of American policy over there,
the future might be if Biden gets his way, and what foreign policy really is.
With that in mind, we need to start off with some caveats.
Foreign policy is almost never discussed honestly, ever. It's normally shrouded
and camouflaged with ideas of patriotism and ideology. That's not what foreign policy is.
Foreign policy is power. It is about power and nothing but power.
None of the other stuff matters. It's about power.
We're also going to be talking about what is, not what should be. Those two things are almost
never the same in any conversation on any topic. When it comes to foreign policy, they are literally
never the same because even when what should be happening is happening, it's probably happening
for a reason it shouldn't be. American foreign policy, foreign policy in general, is not something
that can be boiled down to slogans, and that's where there's another disconnect.
The way we talk about things,
the way you or I talk about things versus the way foreign policy people talk about things.
If you or I were to say the United States needs to leave the Middle East, what do we mean by that?
The U.S. needs to leave the Middle East. You can't put it in simpler terms, right?
You can't put it in simpler terms, right? What does a foreign policy person mean when they say that?
We need to deprioritize the strategic importance of the region so we can focus our attention and assets
on applying pressure elsewhere while leaving a network of bases, embassies, consulates, and covert forces
so we can continue to influence events within the region that we are leaving so we can maintain
the American empire because that is quite literally our job. It means very different things.
Those two sentences really couldn't be much further apart.
When you hear somebody talk about foreign policy and they are a foreign policy person and they say
we need to leave the Middle East, understand they are not really talking about leaving.
They are not talking about actually withdrawing from all of these bases. We don't do that. We are
the United States. That is not our foreign policy. We still have bases in Germany.
Slogan, we need to leave the Middle East. Yeah, I mean we should, absolutely,
but realistically is that going to happen anytime soon? No, of course not. Of course not.
And I would point out that that slogan is one of those things that sounds good
until you really look into it. We need to leave the Middle East. Yeah, we should. That is the
moral thing to do, absolutely. Self-determination is important. It is something that is highly
valued in this country, but what happens if tomorrow we withdraw all of our bases,
all of our forces, all of our assets, all of our presence? It creates a power vacuum and millions
are lost. That is the harsh reality of it. Does that mean that our presence and our foreign policy
in the Middle East is good? No, absolutely not. It is definitely not. It is more akin to a car
accident and now you have a piece of metal stuck through you. You do not want it there. It should
not be there. It never should have been there in the first place. It needs to come out,
but you probably want to get to the hospital before it comes out because there is going to be
lost. That is the reality. It does not make it right. We are not talking about right and wrong.
We are talking about foreign policy, which is about power. Conversations are not the same thing.
Another caveat is that when we get into this, we are going to be talking about
various countries. When I am talking about them, I am talking about the leadership of those
countries and how those countries have behaved on the international scene, not the people of
those countries because those are not the same thing. They are not the same thing. I have more
in common with the average person in Tehran or Riyadh or Tel Aviv than I do with my own
representative. They are not the same thing. That is something that people in the United States
should really take to heart because it becomes very easy to demonize the people of a country
because of the actions of its leadership. As American citizens, we really should be careful
about that because we are going to be in real trouble if that standard is ever applied to us.
Our leadership has not been great over the years.
We are going to be saying the quiet part aloud.
General overview of the Middle East when it comes to nation states. I understand this is going to
be a really rough sketch. We are not even going to get into some stuff that really factors into
a lot of this, but we are going to try to provide a good overview. When it comes to nation states,
there are three real poles of power in the Middle East. You have Iran, Saudi Arabia, and Israel.
Iran has its power because it is the Shia superpower, number one. Number two is it has
been very oppositional to the United States and that gives them clout, gives them power.
It gives them respect within the region. Then you have Saudi Arabia. They are the Sunni
powerhouse. They have Mecca. They are the birthplace. They have a lot of respect and
power because of that. Then you have Israel. Israel has a lot of money and a lot of arms
and generally speaking a blank check from the international community because of obvious
historical reasons. Those are the three centers of power. Now you have a fourth that factors
into it occasionally. Now that is Turkey. Turkey, however, because of its ties via NATO,
it tends to pursue global interests in the region rather than regional ones. This is changing right
now. The leadership in Turkey is trying to exert themselves on a more direct regional level.
In the process of that, they are greatly overestimating their strategic importance to
the West. It appears they believe they have a lot more operational latitude than they really do.
They think that they have a lot more operational latitude than they really do.
It appears they believe they have a lot more operational latitude than they really do.
They think the West is going to overlook a lot more behavior than it will because this isn't
the Cold War. Turkey isn't as strategically important as it used to be. If they continue
down this road, realistically, they'll probably end up a Russian puppet as opposed to a NATO one.
I'm going to say the quiet part out loud in this as much as I can.
It's about power, not about right and wrong.
So for a very long time, the Western stance in general, and we are speaking in generalities here,
has been to prop up the Sunni side of the Shia-Sunni divide.
So we've been really nice to Saudi Arabia. We've realized this isn't incredibly effective,
and we theoretically want to leave by the foreign policy definition, not by what you and I mean.
By the foreign policy definition, we want to deprioritize the region so we can focus
our assets elsewhere. Elsewhere being Africa, in case you're curious.
So what do we have to do to do that? How do we stop that power vacuum from developing?
We have to bring Iran out. We have to bring them back onto the international stage.
We have to get them seen as equals in the eyes of the international community,
equals as far as equal to Saudi Arabia and Israel.
That's going to be quite a challenge. This is what Biden chose to be the centerpiece of his
foreign policy. It's a huge battle. Realistically, I give it like a 30% chance of success,
because it's a huge goal. He's trying to reshape the balance of power in the Middle East,
a region that has been a hot spot forever. It's going to be a huge battle.
It's going to be a hot spot forever. It's going to be hard. It's going to be especially hard
because we have spent decades demonizing Iran. They have a very oppositional relationship
to Saudi Arabia, Israel, and the United States. It's going to be difficult, but that's the goal.
Just like with the Trump-McConnell power struggle, if you start looking at things through this lens
of this being the overall goal to bring Iran out to create a system where there are multiple
poles of power that are equal so the region stays stabilized, it makes sense. It will make sense.
All of Biden's actions make sense. When it comes to Saudi Arabia, we have to send the message that,
well, we're no longer just going to look the other way every time they'll do something,
but not to the point where we actually upset them, which explains the recent actions.
They got a slap on the wrist, but those in power, the real leadership who make decisions,
well, they're not in trouble because we can't upset them because it's not about right and wrong.
It's about power. When it comes to Iran, that's why they got a proportional response,
a warning to use the president's words
because the goal isn't to demonize them at this point for once. We're trying to bring them out.
Now, the reality is this is going to be a very hard strategy to pull off.
It is going to be incredibly difficult to accomplish this because any of the poles of
power that we talked about, any of them that do something erratic that isn't planned for
can mess up the whole thing. And then any of the lesser powers in the region,
they can upset it too by doing something erratic that causes a response from one of the poles of
power. Or the non-state actors in the region who we haven't even talked about, many of whom
are actually more powerful than many of the nation states, they can also upset it.
There are a lot of moving parts to this. It cannot be boiled down to slogans.
It's not realistic that we're actually going to leave the Middle East the way you and I talk about
it. Does that mean you should stop advocating for it? No, absolutely not. Please continue to do so
because that should be the ultimate goal. The reality is we shouldn't be doing all of this
stuff. It's wrong to do all of this stuff. If we want to maintain a presence in the Middle East,
a presence somewhere, we can do it through humanitarian means. We can be the world's
EMT instead of being the world's policeman. That should be the goal.
Pulling out of the Middle East militarily, deprioritizing it, and even when I say it like
that, pulling out militarily, even that's not accurate because realistically the U.S. is going
to maintain bases. It's going to happen. Deprioritizing it and creating a situation
where the regional powers have autonomy, that's the goal. What does that look like? Europe.
That's what it looks like. Saudi Arabia becomes the U.K., Iran becomes Germany,
Israel becomes France. That's the idea. There are still major powers within the region,
but the region cooperates for their own best interests. That's the goal.
If you are a foreign policy person, when you say it like that, you realize how
challenging and ambitious this goal is, but that's what he wants to do.
Realistically, if you are somebody who is anti-imperialism, you don't like this,
but why are you against imperialism? Because it's wrong, right? Self-determination. And
because it hurts the people in these countries, right? If they're treated like Europe,
there's going to be a whole lot less innocents caught in crossfires.
Doesn't make it right. It's still a piece of metal, but you're shaving off an edge.
The reality is, people want to talk about morals and what's right and wrong when they get to
foreign policy. There is no morality in foreign policy. It's about power. And when you're talking
about the Middle East, you're talking about unimaginable power and unimaginable amounts
of money. Hundreds of billions, if not trillions of dollars up for grabs. That's not money,
that's motive. We can't do that. We can't do that. We can't do that. We can't do that.
We can't get companies in this country to act in a reasonable manner concerning climate change
mitigation because of money. The odds of getting nation states to behave in a manner that helps
their people, it's slim. It's slim. That doesn't mean you stop advocating for it. It doesn't mean
that that's not the goal. It doesn't mean that that's what you just accept the way it is. Yeah,
I'm a big advocate of choosing battles that are big enough to matter and small enough to win.
Those are the ones you want to fight. Sometimes you fight the ones you have to fight.
And this is that case. That's the goal. Yeah, to completely disengage,
to not be using military force all the time. Absolutely, that's the goal.
But you can't walk into it with your eyes closed. You have to understand what you are up against,
and you are up against an international machine that is focused on profit and power. The people,
they don't matter. The people on the ground, doesn't matter. That's not the right thing.
That's not how the world should be, but it's how the world is right now. It's up to us to change
that. And the only way that we can really achieve any change is to understand what the problem is.
And the problem is that our foreign policy is about power. It is about exerting influence,
which sure, I mean, that's what nation states do. It's what they've always done.
But doing something simply because it's always been done that way,
that's the worst reason to do something.
Cooperation rather than subjugation will move humanity further faster.
That's got to be the goal.
But until then, we have to understand what's happening.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}