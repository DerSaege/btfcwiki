---
title: Let's talk about Dershowitz's legal take on and Floyd....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zfo0XwIMBvc) |
| Published | 2021/03/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques Dershowitz's theory on suggesting previous actions of the cop as evidence in defense of Floyd's death.
- Questions the logic behind the defense, giving examples of murder scenarios.
- Recalls a prediction from a viewer about the defense strategy, referencing the "skull egg theory" used in personal injury cases.
- Disagrees with the defense strategy and its application to the case.
- Disputes the idea of changing the trial venue due to events related to the incident.
- Shares personal rural living experience to counter the argument for a venue change.
- Comments on the defense possibly teaching the jury too much about the concept of due process.
- Concludes with a thought on the defense strategy and wishes the audience a good day.

### Quotes

- "Normally if something is true you can take it out of the context it's being presented in and put it in something else very similar and it would still be true."
- "I'm pretty sure that's still murder."
- "I don't think this is a strong defense."
- "It seems to me like he wants to move on to a different country."
- "Certainly a bold legal strategy."

### Oneliner

Beau questions the logic behind Dershowitz's defense strategy, providing examples to challenge its validity, while also expressing skepticism towards changing the trial venue and cautioning against teaching the jury too much about due process.

### Audience

Legal observers, activists

### On-the-ground actions from transcript

- Analyze and question legal defense strategies (implied)
- Stay informed and engaged in legal proceedings (implied)

### Whats missing in summary

Beau's insight and perspective on legal defense strategies and trial venue changes. 

### Tags

#LegalDefense #TrialVenue #DueProcess #Critique #Activism


## Transcript
well howdy there internet people it's boe again so today we're going to talk about dershowitz
and his uh theory on a case i saw it last night now if you don't know dershowitz is known
for his wonderful legal takes i am not a lawyer but i have watched a lot of matlock
and i have some questions about his uh his proposed defense it is his belief that the the real strength
that lies in the in a good defense there would be to suggest that well this cop had done this before
multiple times in fact and nobody ceased to be and therefore that's kind of evidence that it wasn't
the knee that did it but rather floyd's preconditions
again not a lawyer that doesn't make much sense to me normally if something is true you can take
it out of the context it's being presented in and put it in something else very similar and it it
would still be true so i'm wondering what would happen if somebody walked around throwing rocks
at people and they did this dozens of times and then all of a sudden somebody ceased to be from it
it caught him in the head or something just right not a lawyer but i'm pretty sure that's murder
maybe that's just a fluke let's do it a different way if somebody walks around poking people
and does it dozens of times and everybody makes it but on the 28th time
well that person they have a clotting issue and they they cease to be
i'm pretty sure that's still still murder this doesn't make a whole lot of sense to me
and what's funny is that one of y'all actually sent me a lengthy message predicting this exact defense
and i apologize i was obviously wrong but i was like there's no way they would do that nobody's
that bad at their job um i was wrong problem is i couldn't go back and find all of the wonderful
research that had been included in that message because i couldn't remember what platform it came
in on but what i do remember was reference to something called a skull egg theory and what this
basically boils down to and from what i remember it's only often used in personal injury but
basically victims are used car lot there's no warranty they come as is as you found them
and if you get a lemon well that's just on you
i don't think this is a strong defense but again i'm not a lawyer he goes on to suggest
in a different part of this interview that it's some grave constitutional injustice
for there not to be a change of venue that the trial really should be held somewhere else
because people in the area well they had an event
near them because of this incident something happened out in the streets
and because of that it needs to be held in a more rural area by by dershowitz is thinking here
okay i don't know if you can tell but i live in a pretty rural area like so rural that if i was to
go build a 10-foot fence i don't need a permit if i was to build a pond i don't need a permit
i'm i'm an hour from a starbucks don't have city water the nearest town to me
has less than 5 000 people in it
there was a march there
up main street with more than a hundred people in it here in the south i don't know that you're
going to find a place in the united states where there was not some event in fact i'm fairly certain
that this footage sparked events on other continents
it seems to me like he wants to move on to a different country
and i'm not a lawyer but as not a lawyer i would also suggest that it may not be in the defense's
best interest to teach the jury too much about the concept of due process you know the idea that
before you're going to get a verdict you're going to have to prove that you're not guilty
and that's a fair trial that may not be a good move certainly a bold legal strategy
anyway it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}