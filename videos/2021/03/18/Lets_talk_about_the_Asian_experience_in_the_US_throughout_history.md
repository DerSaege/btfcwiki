---
title: Let's talk about the Asian experience in the US throughout history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rFbHml5ba0M) |
| Published | 2021/03/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how the current issues in American society are not new and have historical roots.
- Talks about how Trump didn't create the anti-Asian sentiment but capitalized on it for his base.
- Provides an abridged timeline of anti-Asian actions and legislation in the United States.
- Mentions various discriminatory laws and actions against Asians throughout history.
- Points out the importance of not falling for divisive tactics and normalizing the Asian community.
- Emphasizes the significance of exposure and integration to combat ignorance and fear.
- Urges for bringing Asian Americans into communities to prevent marginalization.
- Stresses the need to acknowledge American culpability in perpetuating anti-Asian sentiments.
- Encourages taking action to change the current narrative and not allowing politicians to exploit prejudices.

### Quotes

- "It's probably way past time to stop falling for this."
- "Exposure ends that ignorance."
- "If you have Asian friends, bring them out with you."
- "We can set the example for younger people today that this isn't okay."
- "Then we can say it's not us."

### Oneliner

Beau outlines the historical context of anti-Asian sentiment in America, urging for integration to combat division and ignorance.

### Audience

All Americans

### On-the-ground actions from transcript

- Bring Asian friends out with you, frequent Asian businesses to integrate them into the community (suggested)
- Reach out to older Americans and set the example for younger generations by making the Asian community part of our communities (implied)

### Whats missing in summary

Full understanding of the historical roots of anti-Asian sentiment and the importance of integration and exposure in combating ignorance and fear.

### Tags

#AmericanHistory #AntiAsianSentiment #Integration #Community #Action


## Transcript
Well howdy there internet people, it's Bo again.
So today we're gonna talk about something that isn't new.
But for some reason, we wanna pretend like it is.
It's something that's woven into the fabric
of American society.
But for some reason, I keep hearing this isn't us.
This isn't America.
History says otherwise.
And there's another framing of this topic
that suggests Trump created it.
It's not true, he didn't create it.
He tapped into it.
He leaned into it.
He used it.
He exploited it to maintain control of his base,
to give them somebody to look down on,
the same way politicians have
throughout most of American history.
He didn't create it.
He's responsible for the most recent surge
in anti-Asian sentiment in the United States,
but he did not create it.
I think most people in the United States
know about internment.
And I think most, if they gave it any thought,
would realize that throughout most of the 20th century,
there was a systematic propaganda effort
against Asians in the United States
because we dehumanize our opposition during wartime.
World War II, followed shortly thereafter by Korea,
followed shortly thereafter by Vietnam.
I think people would recognize that
if they put any thought into it.
However, I'm not sure that they know how far back it goes.
So I'm going to present an incredibly abbreviated timeline
of some things.
This may include 10% of what should be on
a timeline like this,
but the goal isn't to talk about every incident.
It is to demonstrate how systematic
and how prevalent it has been
throughout United States history.
Okay.
So the first documented appearance of Asians
in the United States,
or what would become the United States that I'm aware of,
was in 1763, when a group jumped off
some Spanish galleons in New Orleans.
And then they took off and fled into the bayous to hide out.
They were here from the beginning,
but they weren't here in numbers.
That didn't start until 1848,
because there's gold in them there hills out in California.
So they brought in as miners.
Most of the sentiment comes from California.
So they arrive in 1848.
It took precisely two years
before the force of government was brought down upon them
to other them and treat them as lesser.
Two years.
Started with the foreign miners tax in 1850.
It was used primarily against Chinese people.
And it was a tax that was applied once or twice
or three times or more.
However many times the powers that be in the area
felt they needed to shake somebody down.
In 1852, 20,000 Chinese people enter California.
Well, those are numbers.
The power structures that exist,
they felt they had to protect themselves.
In 1854, in the people versus hall,
Chinese people were barred from testifying
against white people.
1859, Chinese kids can't go to school
with white kids in San Francisco.
If you are from the Southern United States,
this is all gonna sound really familiar
because it's the exact same playbook.
Keep people separated, exploit their labor,
but make sure that we keep them seen as lesser, as other.
They didn't have a poll tax,
but they had a police tax in 1862, $2.50 per month
for every Chinese person to offset the cost, I guess.
In the 1870s, there was a whole lot of anti-Chinese violence.
A good example of that occurred in Los Angeles
on October 24th, 1871.
I want to say the night before,
a white guy had been shot.
So 500 people get together and go into Chinatown
and take out 19 in reprisal.
Eight people were convicted of their actions that night,
and eight people had their convictions overturned
because the justice system worked pretty much
the same way it did in the Southern United States
at the time.
1875, Page Law was introduced,
barred entry of Chinese, Japanese, or Mongolian laborers.
Put them alongside in the same category as felons.
1879, the new California Constitution prohibits cities
from hiring Chinese people.
See, by this point, they're starting to get a good foothold.
Like, they're becoming a real community of people there.
So, well, if you're an American politician,
you want to make sure that you can maintain control,
so you have to keep them separated
from the white folk, right?
So in 1880, it became illegal to issue marriage licenses
if it was a white person and a Chinese person.
Again, it all sounds really, really familiar.
1882 was the beginning of the Chinese Exclusion Laws.
Ten-year suspension on laborers entering.
1884, there was a lawsuit trying to get a Chinese child
enrolled in public school.
1885, San Francisco builds what they called
an Oriental School, because again,
gotta keep them separate.
I wonder if it was equal.
1885, Rock Springs, Wyoming.
Large flare-up of violence started between minors.
28 dead, 78 homes burned to the ground.
The cities of Tacoma and Seattle in 1886
forcibly removed Chinese people from the cities.
In 1892, the Chinese Exclusion Law was renewed
for another 10 years, and it required registration.
Papers, please.
America, land of the free, home of the chain.
In 1894, a Japanese man applies for citizenship.
US Circuit Courts refuse,
because he is neither white nor black.
Other.
1902, Chinese Exclusion Laws were renewed again
for another 10 years.
Two years later, just to be sure
that they didn't have to worry about it anymore,
they went ahead and made that indefinite.
In 1905, San Francisco attempts
to segregate Japanese students as well.
California forbids whites from marrying Mongolians.
In 1906, there is a small reprieve
when an earthquake struck San Francisco
and took out immigration records.
Made it harder to,
harder to other them,
because they could just say, no, no, no, no, I'm a citizen.
It was hard to prove otherwise.
In 1907, Indians were driven forcibly
out of Bellingham, Washington.
In 1908, the exact same thing happened in Livo, California.
1910 was the beginning of Angel Island,
which was the West Coast version of Ellis Island,
only it was very different.
That will probably get a video of its own.
In 1924, the Immigration Act,
which basically barred entry
of pretty much everybody from Asia, few exceptions.
Shortly thereafter was internment.
Shortly thereafter was Korea.
Shortly thereafter was Vietnam
and the propaganda that went along with it.
You can say this isn't us
if you mean some of the American people,
but if you were talking about the institution
of the United States, oh, this is definitely us.
So how do we change it?
Because there will always be people like Trump
who want to tap into it.
The first thing we have to do is stop falling for it.
The next thing we have to do is normalize,
bring them into the community.
If you have Asian friends, bring them out with you.
If there are Asian businesses in your area, frequent them.
Make them part of the community.
These tactics only work if people agree to be separated.
It's the only time they do.
If you have contact with people,
you realize they aren't lesser, they aren't other.
That's the tactic that authoritarians throughout history
and pretty much all politicians
in the United States have used.
As long as they can convince the lowest white person
that they're somehow better than that group
that they have marginalized,
well, they'll probably get reelected.
They'll probably have them as a voting bloc
because the people who fall for this,
they don't look at policy, they look at fear.
And it's that ignorance that causes fear.
Exposure ends that ignorance.
I don't know if saying don't be a bigot
on social media helps.
Just in case it does, don't be a bigot.
But normalization does.
And there are a lot of people who want to reach out
to older Americans.
I don't know how much good that's gonna do
with the amount of propaganda that they've absorbed
over the course of their life.
I don't know that it's gonna do any good.
By the time you reach them,
they will probably have aged out of society.
But what we can do is set the tone for the future.
We can set the example for younger people today
that this isn't okay.
And the way to do that is through exposure,
through bringing the Asian community in,
making them part of our communities.
That's how we can stop this.
Once we do that, then we can say, no, this isn't us.
Until then, history says it very much is.
You can lay the latest upsurge at Trump's feet.
Absolutely, that's fair.
But to suggest that he created it,
it's just not accurate, it's not true.
And the only thing that is going to stop something like this
is to make sure that we don't try to downplay
American culpability in it.
The institution of the United States is complicit in this.
It has been since the 1850s.
It's been a never-ending cycle
of marginalizing and othering the Asian American community.
It is us.
So, this is where we're at.
If you wanna change it,
it's up to us.
Then we can say it's not us.
This is probably the right time to make this move.
It's probably way past time
to stop falling for this,
to stop allowing politicians to lean into
the leadership of the United States.
Allowing politicians to lean into prejudices
and separations that the government created
to maintain power.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}