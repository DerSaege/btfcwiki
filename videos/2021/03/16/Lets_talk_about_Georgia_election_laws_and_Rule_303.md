---
title: Let's talk about Georgia election laws and Rule 303....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=I4trkvkJ0O0) |
| Published | 2021/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Republican Party nationwide is pushing to restrict voting, including in Georgia, following baseless accusations by the Trump campaign.
- Texas spent 22,000 hours looking for voter registration fraud and found only 16 out of 17 million voters.
- Georgia, with 7.6 million voters, faces similar restrictions.
- A suggestion to end the issue: Democrats spot Republicans eight votes in each election.
- Coca-Cola and Home Depot oppose the new voting restrictions in Georgia, which could be a significant change.
- Coca-Cola's Political Action Committee traditionally leans Republican but values areas like equality and inclusion in candidate evaluations.
- Restricting voting ability contradicts inclusivity and equality.
- Voting restrictions aim to disenfranchise certain groups, especially those who turned Georgia blue.
- Coca-Cola, with its power and influence in Georgia, has the means and responsibility to prevent these voting restrictions.
- If Coca-Cola and Home Depot use their political power effectively, these restrictive bills might not succeed.

### Quotes

- "Georgia, with 7.6 million voters, faces similar restrictions."
- "A suggestion to end the issue: Democrats spot Republicans eight votes in each election."
- "Voting restrictions aim to disenfranchise certain groups, especially those who turned Georgia blue."

### Oneliner

The Republican Party's push to restrict voting faces opposition from corporations like Coca-Cola and Home Depot in Georgia, potentially changing the game against disenfranchisement.

### Audience

Georgia voters

### On-the-ground actions from transcript

- Contact Coca-Cola and Home Depot to thank them for opposing voting restrictions in Georgia (suggested).
- Stay informed about candidates and political contributions using tools like OpenSecrets.org (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the Republican Party's efforts to restrict voting in Georgia and the potential influence of corporations like Coca-Cola and Home Depot in opposing these restrictions.

### Tags

#VotingRestrictions #Georgia #CocaCola #HomeDepot #PoliticalPower #Equality #Inclusion


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Georgia and their push to restrict voting.
And what might be a game changer for it?
If you don't know, the Republican Party all over the country is trying to place restrictions
on voting locations, times, how you can vote, who can vote, a bunch of stuff.
This is happening in Georgia and they are relying heavily on the totally baseless accusations
made by the Trump campaign after they lost.
Texas just spent 22,000 hours under Attorney General Paxton looking for this myth out there.
After 22,000 hours, they found 16, count them, 16 voter registrations that had false addresses.
Texas has 17 million registered voters.
It is literally less than one in a million.
Georgia has 7.6 million voters.
Maybe we could end this all right now if Democrats are offered to spot Republicans eight votes
in each election.
Start them off eight votes ahead.
Make up for any possibility of it.
Now in what might be a game changer, Coca-Cola and Home Depot have come out against these
new regulations, these new laws, these new restrictions.
Company saying something like that, that normally doesn't mean much.
However, the massive company that is Coca-Cola is headquartered in Atlanta.
They have a lot of sway.
Not just do they have sway because they're a giant business in the state, they also have
sway through the Coca-Cola Political Action Committee.
They give away a lot of money.
They give away a lot of money to people in the Georgia House and Senate.
Now traditionally, the Political Action Committee leans Republican.
They tend to give more money to Republicans than Democrats since the turn of the century.
There have been two cycles in which they gave more money to Democrats.
That was in 2008 and 2020.
You can find this information on OpenSecrets.org.
I suggest everybody go there every once in a while and find out who owns your representative
or senator.
You can just type in their name or you can do it the other way and type in the name of
a donor, a Political Action Committee and find out who they give money to.
It's an incredibly useful tool.
So how does Coca-Cola decide, how does their Political Action Committee decide who to give
money to?
It isn't based on party, we can see that because although they do lean Republican,
it's not a huge disparity most years.
Their website says they have five areas that they use to evaluate candidates.
Environmental sustainability, business, members who represent system facilities and large
employee bases, leadership, and equality and inclusion.
I would suggest trying to restrict people's voting ability isn't very inclusive.
It goes on.
No single issue or criteria category determines whether a candidate does or does not receive
a contribution.
However, candidates will not be eligible for a political contribution from the Coca-Cola
company or the Coca-Cola pack if they have made egregious remarks in the equality and
inclusion area.
Areas that comprise equality and inclusion include but are not limited to the following.
Social justice, racial and gender equality.
There's a whole bunch more but we can stop there because that's what it's about, right?
It's about stopping a certain group of people from voting.
People who don't look like me.
People who turned that state blue.
I would suggest any support for voting restrictions on those people would be pretty egregious.
At the end of the day, through their power as the giant company that is Coca-Cola and
their political action committee, Coca-Cola has the means to stop these restrictions.
They have the ability.
They have the means.
They have the responsibility to do so.
It wouldn't take much.
Coca-Cola has a whole lot of power in Georgia.
Home Depot may as well.
I'm just more familiar with Coca-Cola's.
These two companies coming out in opposition to it along with apparently the Chamber of
Commerce there may be the end of this.
If they offer more than just words, if they actually use the political power that they
have, these bills will go away because it's going to be hard to gain any real support
with Home Depot and Coca-Cola going against you in Georgia.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}