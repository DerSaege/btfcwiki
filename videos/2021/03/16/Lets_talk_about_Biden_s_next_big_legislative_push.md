---
title: Let's talk about Biden's next big legislative push....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-2YYEe7v67c) |
| Published | 2021/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions arise about Biden's upcoming legislative agenda, including comparisons to the Green New Deal, infrastructure programs, and tax increases.
- Details of the agenda are yet to be released, leading to speculation based on campaign promises and advisors' recommendations.
- Predictions include a potential 6 to 7 percent corporate tax increase, aiming to encourage reinvestment or higher employee wages.
- Biden's tax plan does not advocate raising taxes for individuals earning less than $33,000 a month.
- The agenda may focus on taxing the wealthy, in line with Biden's campaign promises and advisors' suggestions.
- Beau argues that a tax increase on the wealthy is a fundamentally American concept, supported by the country's philosophical founders.
- He challenges the idea of no taxes, stating it eliminates the need for a government and relies on individuals' voluntary social responsibility.
- Beau points out the potential benefits of the tax increase, such as increased business investment or support for large foundations.
- The infrastructure package is speculated to be an environmentally friendly build-out, but not a full-fledged Green New Deal.
- Uncertainty surrounds the agenda's details and passage, with potential strategies including budget reconciliation and filibuster amendments.

### Quotes

- "The idea of increasing the corporate tax rate is to get companies to reinvest the money."
- "If you don't make more than $33,000 a month, none of it applies to you."
- "It's all guessing. So that's where we're at."

### Oneliner

Beau speculates on Biden's legislative agenda, focusing on potential tax increases for the wealthy and an environmentally friendly infrastructure package.

### Audience

Policy analysts, political enthusiasts

### On-the-ground actions from transcript

- Stay informed about Biden's legislative agenda and its potential impact (suggested).
- Advocate for socially responsible tax policies (implied).

### Whats missing in summary

Further insights on Biden's legislative agenda and its implications can be gained from watching the full video. 

### Tags

#Biden #LegislativeAgenda #TaxPolicy #Infrastructure #GreenNewDeal


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Biden's next big legislative agenda.
Mainly because I have a whole lot of questions about it.
People have sent in questions.
Is it going to be like the Green New Deal?
Is it going to be an infrastructure program?
Is it going to be a tax increase?
The answer to all of that is yes.
But we don't know exactly what it is going to be because the details haven't been released.
There's a lot of people curious about it because people are already pushing back against it.
But there's no details out.
What they're doing is guessing based off of his campaign promises and what his advisors
tend to recommend.
Because so far Biden has listened very closely to his subject matter experts.
With that we can guess.
We can do a pretty good guess on what the tax side of it would be.
Probably a corporate tax increase of 6 to 7 percent.
So if you have a hundred grand in profit each year, you're looking at an extra five or six
hundred dollars a month in taxes.
That's really what it breaks down to.
The idea of increasing the corporate tax rate is to get companies to reinvest the money
or pay their employees more because corporate, basically short version, corporate taxes are
based off what you make.
Your profit, what you spend doesn't get taxed.
So the idea is to encourage corporations to spend more.
Your personal income tax, the key part that people need to be very aware of is that yeah,
there's going to be an increase.
But Biden does not have a single advisor, not one, who advocates raising taxes on those
who make less than $33,000 a month.
I didn't say that wrong.
A month, not a year.
If you don't make more than $33,000 a month, none of it applies to you.
For capital gains tax increases, I think you have to make a million a year or more based
on most of what his advisors say.
But again, we don't have the details.
If his campaign promises and his advisors, if that blends together, it's basically the
tax the rich talking point come to life.
That's what the tax side of this would look like.
There will be those who say that is un-American and those people are wrong.
This is incredibly American.
The philosophical founder of this country advocated for that kind of tax system.
The guy who wrote the pamphlet that Washington had read aloud to his troops at Valley Forge
advocated for this.
I'll put a video down below.
There are those who say we shouldn't have any taxes at all.
Yeah, that sounds great.
I mean, understand you don't have a government if you do that.
And it also requires people to be socially responsible on their own.
They have to exercise that social responsibility voluntarily.
I know a lot of small business owners.
I know three, three that actively invest in their community in ways that don't directly
benefit them or their business.
Three.
And incidentally, they would all be called the radical left by the Republicans who oppose
this type of stuff, even though one of them is actually right wing, just really anti-authoritarian
right anyway.
If you want to have this kind of system, this is a facet of it.
This is part of it.
What do you get for this tax increase?
Theoretically, more investment from business, from large companies trying to get out from
under that increase in corporate taxes.
So more pay or the large foundations that exist.
Most of them came into being when there were really high corporate taxes.
It was a way to avoid them.
Now as far as the infrastructure package, people are saying that it's going to be the
Green New Deal.
It's not.
It will be a watered down version of it.
The odds of him going full blown Green New Deal, it's not going to happen.
I don't think it's going to happen.
I hope he proves me wrong, but I don't see it as likely.
It will be an environmentally friendly infrastructure build out.
As far as the pushback against it, I don't know why it's coming this early other than
this is really going to impact the wealthy.
So they're trying to get out in front of it.
But we don't have the details yet.
It kind of surprised me to have that many messages about something.
I understand people being curious.
It's good.
But we don't know yet.
Nobody does.
It's all guessing.
So that's where we're at.
As far as whether or not it will get through, there are those saying that he'll attempt
to use budget reconciliation again.
Maybe.
Maybe.
But some of this I think would be outside the scope of it.
And there are also a lot of very powerful Democrats now in the Senate who are seeming
to be in favor of amending the filibuster.
If the filibuster gets amended, all of the dynamics change.
The difference between what is possible and impossible, all the math changes.
Because if they don't need 60 votes, they can get a lot more stuff through.
So that's it.
There's no hard information about this yet, at least not at the time of filming.
I would assume that something is going to come out in the next couple of days about
this.
But we'll have to wait and see.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}