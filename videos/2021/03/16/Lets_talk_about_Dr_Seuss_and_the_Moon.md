---
title: Let's talk about Dr. Seuss and the Moon....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ybNlFcxqxh4) |
| Published | 2021/03/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the importance of backup plans, like the Seed Vault, to preserve historic artifacts and samples in case of any eventuality.
- Mentions a proposed ultimate backup plan to place samples of 6.7 million species on the moon in cryopreservation facilities.
- Points out the significance of decentralization in preserving samples to ensure their availability even in catastrophic events like Yellowstone erupting.
- Describes the amazing logistics involved in the lunar backup plan.
- Emphasizes the contrast between people's priorities on Earth, from preserving old books to preserving the genetic code of Earth on the moon.
- Raises the question of tradition versus future good as a key talking point that divides mindsets.
- Suggests that the way things have always been done may necessitate extreme measures like an ark on the moon.

### Quotes

- "Or we can explore the galaxy."
- "When you look at it like that, it really shows the difference in mindset, what the priorities are."
- "The way we’ve always done it may end up leading to the need for an ark on the moon."

### Oneliner

Beau talks logistics, decentralization, and contrasting priorities between preserving old books and genetic codes on the moon.

### Audience

Space enthusiasts, conservationists

### On-the-ground actions from transcript

- Preserve biodiversity locally through community gardens and conservation efforts (implied)
- Support initiatives that focus on environmental preservation and space exploration (implied)

### Whats missing in summary

The full transcript provides a deeper dive into the implications of contrasting priorities and the necessity of forward-thinking conservation efforts.

### Tags

#BackupPlans #Logistics #Priorities #Preservation #Decentralization #SpaceExploration


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about, well,
two things that kind of fit into the themes that have been hit this week.
Logistics and people being in different places.
If you don't know, I'm a huge fan of backup plans of any kind,
like the Seed Vault, I think it's a great idea.
Over my shoulder, the thing holding up the elephant,
is actually put out by a company that allows you to get your hands on
historic artifacts, little pieces of it, little samples.
I like this because it decentralizes it, and if something ever happens,
hey, samples still exist somewhere.
I mean, they may be in some guy's shoe box or
in a shop in the middle of nowhere, but they exist.
I want to talk about the ultimate backup plan,
because it was just proposed, and
the idea is to take 6.7 million species,
and put samples of them on the moon,
in lava tubes, in cryopreservation facilities.
So if something was to ever happen, well, they'd still be available.
Anything that impacted the biodiversity of Earth, well,
we just go there and retrieve it, bring it back.
So if there was some cataclysmic event, like Yellowstone or
something like that, we would have a backup.
The logistics involved with this are amazing.
There are a bunch of articles out there about it.
Read a couple of them, it's just wild.
We have all the technology to do it right now, with the exception of robots
that can work in the temperatures required.
Basically, they would freeze to the floor, and
they're trying to figure out a way around it.
It's probably something we should look into,
given the fact that it's basically a miracle that we
haven't destroyed ourselves by this point.
Aside from that, it really puts that thing about people being in different spaces,
different spots on the path, puts that into perspective.
Right now here on Earth, there are people paying thousands of dollars so
they can get their hands on an old racist Dr. Seuss book so they can preserve it.
And elsewhere, there are people doing their best to
figure out how to preserve the genetic code of Earth on the moon.
There's that saying, and it's normally referencing religious texts,
but we can stay here and argue about old books.
Or we can explore the galaxy.
When you look at it like that, it really shows
the difference in mindset, what the priorities are.
And that's a big part of it, tradition versus future good.
And that's one of those key talking points, one of those key discussions.
That is really what separates a lot of people.
This is the way we've always done it.
The funny part is the way we've always done it may end up leading
to the need for an ark on the moon.
Anyway, it's just a thought. I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}