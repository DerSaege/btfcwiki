---
title: Let's talk about what Kentucky's senate just told us....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Rnbg7rnM9UQ) |
| Published | 2021/03/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Kentucky state senate passed a bill making it a misdemeanor to insult or taunt law enforcement officers.
- The bill is a blatant violation of the First Amendment and is likely to be overturned in court.
- The bill raises questions about the senators' authoritarian views and their perception of the people of Kentucky.
- This legislation empowers law enforcement to use force more frequently, especially in a climate where excessive force is already a concern.
- The bill could lead to officers using violence based on subjective interpretations of gestures or words.
- Senators who supported this bill are seen as authoritarians giving law enforcement a blank cheque for excessive force.
- This legislation is an attempt to provide cover for excessive force and could result in numerous lawsuits.
- Beau urges Kentuckians to contact their senators and the governor's office to prevent this bill from becoming law.
- He predicts that if the bill passes, there will be significant legal repercussions and financial costs for the state.
- Beau stresses the importance of citizens taking action to prevent the potential negative consequences of this bill.

### Quotes

- "Any senator who voted for this needs to be voted out of office."
- "If this becomes law, I give it no more than 10 arrests under this statute before there is a million dollar payout."
- "The people of Kentucky probably need to get a hold of their senators."

### Oneliner

Kentucky bill criminalizing insults towards officers reveals authoritarian views and threatens excessive force empowerment, urging citizen action.

### Audience

Kentuckians

### On-the-ground actions from transcript

- Contact your senators and the governor's office to oppose the bill (suggested).
- Advocate for preventing the bill from becoming law by reaching out to lawmakers (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the problematic bill and urges citizens to take action to prevent its harmful effects from becoming reality.

### Tags

#Kentucky #FirstAmendment #Authoritarianism #LawEnforcement #ExcessiveForce


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about some
news out of the great state of Kentucky. A bill made it through the state senate there. Still
has to go to the house. Still has to get signed by the governor. It's not law. And on one level,
it's not even worth talking about because realistically, as soon as this gets challenged
in court, well, it's gone. It's gone. This is a blatant first amendment issue.
But on another level, it's worth talking about because it tells us a whole lot
about the people who voted for it. The people who read this language and were like, you know what,
that's a good idea. We should do that. The law would make it a class B misdemeanor.
If an individual accosts, insults, taunts, or challenges a law enforcement officer with
offensive or derisive words, or by gestures, or other physical contact, they would have a direct
tendency to provoke a violent response from the perspective of a reasonable and prudent person.
I have so many questions now. I mean, not about the law. This is just a disaster of legislation.
I have so many questions about the senators in Kentucky.
What insult exactly would provoke a violent response from a reasonable and prudent person?
What gesture? What taunt? The state senate there just showed themselves to be authoritarians
and that they view the people of Kentucky as lesser. You know, I joke all the time and say,
our betters in DC. They have clearly taken this to heart there in Kentucky.
The idea that in the middle of this climate regarding law enforcement, where pretty much
everywhere has recognized that law enforcement is using excessive force just a wee bit too much to
say the least, Kentucky has created, or is attempting to create, a situation where law
enforcement gets to use force more often than they have ever done before. And that's a big
where law enforcement gets to use force more often. And then you have to wonder about the intent.
What's the actual goal here? Because I can really only think of one,
that is to empower law enforcement to attack whomever they wish.
Anytime, if this becomes law, anytime an officer gets mad because of something somebody said
or the way they move their hands, however they gestured, they can visit violence upon them.
That's not what that says. Yeah, it is. Because they can arrest them
and they can use the amount of force necessary to affect the arrest.
This is giving officers cover for excessive force. This is trying to create a mechanism
where they can say, oh, I wasn't really just angry and being unreasonable. A reasonable person would
have gotten mad, therefore I didn't really attack them. I was arresting them and used the amount of
force necessary to affect the arrest. This is excessive force and lawsuits all over Kentucky,
contained in a statute. That's what this is. Any senator who voted for this needs to be voted out
of office. They showed who they are. They are authoritarians who believe law enforcement
is incapable of behaving in a reasonable and prudent manner because they're giving them
a blank check to say, well, if you get mad, you know, you're reasonable and prudent. So if you
want to use violence, well, arrest them and then use however much violence you need to use.
I don't know that I have ever seen a statute this bad and that's saying a lot.
If this becomes law, I give it no chance. I don't know if I'm going to be able to
do that. And that's saying a lot. If this becomes law, I give it no more than 10 arrests
under this statute before there is a million dollar payout.
The people of Kentucky probably need to get a hold of their senators,
need to get a hold of those in the governor's office, wherever, and make sure that this doesn't
become law because it's going to be your tax dollars paying for the people who were attacked
when law enforcement felt they were justified by this. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}