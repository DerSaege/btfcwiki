---
title: Let's talk about the quality of votes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5yh5rgmHXaI) |
| Published | 2021/03/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Voting in the United States is taken for granted, with voting on various aspects being a common occurrence from an early age.
- The real power in the government lies with lobbyists and the money they have, making citizens' votes more of a suggestion.
- China is attempting to diminish the autonomy of Hong Kong through methods like adding 300 informed "quality voters" to the election committee, a move opposed by the GOP.
- State representatives and Republicans across the country are actively involved in voter suppression campaigns, including limiting polling stations and mail-in voting access to certain demographics.
- Beau criticizes the pretext of voter security used to pass legislation that suppresses votes, labeling it as an abandonment of democracy.
- He calls out a state representative in Arizona, John Kavanaugh, for focusing on the quality of votes and using security as a guise for voter suppression.
- The voter suppression efforts disproportionately impact certain groups, particularly those who helped flip Georgia blue.
- Beau urges those who support electoralism and want to participate in elections to pay attention to the nationwide Republican efforts to suppress votes.
- He warns that if these suppression tactics succeed, the voice of the people in government will be severely compromised.

### Quotes

- "Not everybody wants to vote. And if somebody's uninterested in voting, that probably means they're totally uninformed on the issues."
- "It's an abandonment of the idea of democracy."
- "I have a lot of issues with low-information voters. I have more issue with low-information representatives."
- "It's not the votes that count. It's who determines the quality of the votes."
- "If stuff like this gets through, even the pretext that the people have some voice in government is gone."

### Oneliner

Beau warns of nationwide Republican efforts to suppress votes, calling it an abandonment of democracy and a threat to the people's voice in government.

### Audience

Voters, Activists, Community Members

### On-the-ground actions from transcript

- Pay attention to and actively oppose voter suppression efforts by joining local initiatives and organizations (exemplified)
- Support voter education and outreach programs to ensure all citizens are informed and able to participate in elections (exemplified)

### Whats missing in summary

The full transcript provides a deeper insight into the ongoing challenges and threats to democracy posed by voter suppression efforts.

### Tags

#Voting #Democracy #VoterSuppression #GOP #Elections


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about voting.
You know here in the United States we kind of take voting for granted.
I mean it's everywhere.
We vote on everything.
And I mean it starts early.
You vote for high school class president or whatever.
We vote for where we're going to go eat.
We vote all the time.
Now when it comes to our government, we generally understand that our vote is more of a suggestion
that the real power lies with lobbyists and the money that they have.
But you know other countries it's not the same way.
Right now the GOP, they are condemning the activities of China.
Because China is trying to get rid of the autonomy of Hong Kong.
One of the ways they're doing it, and this has been a long campaign, which the GOP is
opposed.
One of the ways they're doing it right now is they have a thing in Hong Kong called the
election committee.
It normally has 1,200 members.
Well Beijing's decided they're going to add another 300 people to it.
Good quality voters.
Voters who are informed properly so we know how that vote's going to turn out.
You know, let me read you this quote because this is wild.
Don't mind putting security measures in that won't let everybody vote, but everybody shouldn't
be voting.
Not everybody wants to vote.
And if somebody's uninterested in voting, that probably means they're totally uninformed
on the issues.
Security is important, but we have to look at the quality of votes as well.
Can you imagine that?
Can you imagine somebody in the United States saying something like this in Arizona?
A state representative by the name of John Kavanaugh?
Because that's who did.
The quality of the votes.
You know, all over the country right now, Republicans and state legislators are actively
engaged in a campaign to suppress the vote.
They are trying to limit polling stations, remove drop-off boxes, make sure that only
certain demographics can vote by mail, all kinds of stuff.
Anything they can do to make sure they get quality votes, those votes that they want,
those votes that, well, help them.
It's an abandonment of the idea of democracy.
That's what it is.
It can't be framed any other way.
And I have to suggest that somebody who has the nerve to call somebody else uninformed
while using the pretext of security, voter security, election security, to pass this
kind of legislation can't be taken seriously.
Uninformed.
Coming from somebody who is repeating a debunked claim pushed by a twice-impeached, one-term
president who just couldn't handle the fact that he lost.
Calling somebody else uninformed.
I have a lot of issues with low-information voters.
I have more issue with low-information representatives.
This isn't a real issue.
It's been demonstrated.
It's not a real problem.
It's just a pretext that the Republican Party can use to suppress the vote.
And as is typical lately from the Republican Party, it disproportionately impacts a certain
group of people.
Those people who turned out in droves to flip Georgia blue.
People who don't look like me.
I'm willing to bet I'll still have a polling station right up the road.
This is something that we have to pay attention to.
When we talk about local activities and getting involved locally, if you are somebody who
supports electoralism, if you want to get involved in elections and you think that that
method of getting your voice heard is the most effective, you have to pay attention
to this.
Because they are doing this nationwide.
It is a concerted effort by the Republican Party to make sure that certain groups of
people are not going to be able to vote in numbers significant enough to matter.
It's not the votes that count.
It's who determines the quality of the votes.
If stuff like this gets through, even the pretext that the people have some voice in
government is gone.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}