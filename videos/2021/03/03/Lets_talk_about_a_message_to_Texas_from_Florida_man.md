---
title: Let's talk about a message to Texas from Florida man....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qx9RUQpLHbo) |
| Published | 2021/03/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Message to Texas from Florida on removal of mandates.
- Florida operates independently from state mandates.
- Governor DeSantis' orders ignored by counties in Florida.
- Lack of reliance on Florida state government due to past experiences.
- Common sense needed despite no legal requirement for masks.
- Warning against potential consequences of not following safety measures.
- Importance of considering vaccine effectiveness and preventing mutations.
- Advocating for basic safety measures like handwashing and mask-wearing.
- Urging Texans to uphold common defense and general welfare responsibilities.
- Emphasizing the mutual protection aspect of wearing masks.
- Encouraging responsible freedom that considers the well-being of all.
- Plea to avoid prolonging the pandemic by adhering to safety guidelines.

### Quotes

- "My mask protects you. Your mask protects me."
- "Freedom comes with a responsibility."
- "Y’all are supposed to be tough."
- "Please don't prolong this."
- "Wash your hands. Don't touch your face."

### Oneliner

Beau urges Texans to exercise common sense, warning against ignoring safety measures and reminding them of the shared responsibility in protecting public health.

### Audience

Texans, Public Health Advocates

### On-the-ground actions from transcript

- Wear a mask or two or three and practice common sense in public places (exemplified).
- Wash hands regularly, avoid touching face, and stay at home as much as possible (exemplified).

### Whats missing in summary

The emotional appeal and the urgency conveyed through Beau's message can be better grasped by watching the full transcript.

### Tags

#Texas #Florida #COVID19 #PublicHealth #Responsibility #SafetyGuidelines


## Transcript
Well howdy there internet people it's Beau again. So today we have a message to the people of the
great state of Texas from Florida man. That's me. The state government in its infinite wisdom
in Texas has decided to remove all of its mandates and that's fine. Y'all can do what
you want. It's your state. The problem is y'all are pointing to us. First, this is the first time
in my memory that any state has pointed to Florida and said oh let's follow their lead
when it comes to safety or public health or anything really. Maybe that's not a good idea.
Aside from that, I would point out that I don't know that people outside of Florida
understand the way Florida really works. We're a county-run state for the most part.
We don't listen to our state government. We barely even acknowledge they exist. That's why we don't
have state mandates. But if Governor DeSantis was to hypothetically walk out and say I want schools
open, I want kids in classes, do you know what could happen and by could happen I mean did happen
and this isn't hypothetical at all? Miami-Dade, Broward, and a whole bunch of little red counties,
they shut down their schools. We don't listen to the governor. He's a guy who won a popularity
contest in Florida. That doesn't necessarily scream wisdom. We have learned through long
experience after hurricanes that we can't rely on our state government. That they make decisions
based on economic interest and what is politically expedient the same way the state government in
Texas does. That's why they protected your power grid so well. Same group of people giving you this
advice now. Maybe they don't know what they're doing. You might want to entertain that idea for
a minute. You don't have to do it according to the law now. It isn't illegal to go out without a mask.
I looked in the entire state of Texas. I couldn't find a single law saying it was illegal to walk
into a public restroom and lick the floor. But I'm willing to bet you don't do that. You exercise
some common sense. Probably be a good idea to do that here. And I get it. I mean, I get the logic.
I get what people are saying. I get the idea behind this. The idea is that, you know, the vaccines,
they're out. They're doing what they're supposed to. They're working well. Our older people are
getting them. Those are the people who are most vulnerable. The rest of us, we're Texas tough.
We'll be fine. We're not going to worry about it anymore. And that makes sense until you actually
think about it. Every transmission is a chance for mutation. If it mutates far enough, the shots we
have, they don't work anymore. Which means that Texas could theoretically start this whole thing
all over again. I'm going to ask y'all not to do that. Means you have to exercise some common sense.
To do what you know you should do. Wash your hands. Don't touch your face. Stay at home as
much as you can. If you go out, wear a mask or two or three and don't whine about it.
That is very disturbing to the rest of the nation. It doesn't fit your state reputation.
Y'all are supposed to be tough. You know, the strong silent type and all of that. It's confusing
the rest of us. It would be like if everybody in Florida started acting normal. We have had
enough stressors over the last 12 months. We don't need y'all adding to them. I get it. You want to
stand up and scream freedom at the top of your lungs. That's fine. But freedom comes with a
responsibility. And the key responsibility right now is to provide for the common defense and
promote the general welfare. Wear a mask. My mask protects you. Your mask protects me. Please don't
prolong this. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}