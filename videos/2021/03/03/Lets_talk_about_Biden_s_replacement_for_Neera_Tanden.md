---
title: Let's talk about Biden's replacement for Neera Tanden....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2VAxyoEmyXU) |
| Published | 2021/03/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Neera Tanden's nomination for Director of the Office of Management and Budget has been or will be withdrawn soon because she is considered very establishment and status quo.
- Possible replacements for Tanden include Gene Sperling, Ann O'Leary, and Shalanda Young, with Young being a wild card choice.
- Gene Sperling and Ann O'Leary have ties to the Clinton-Obama administration, with O'Leary being a bit more progressive in her politics.
- Shalanda Young, a potential wild card replacement, is well-liked by both Republicans and left-leaning Democrats, but not much is known about her stances.
- Young is seen as a viable candidate due to her lack of political baggage, even though she may not fit the typical Democratic advisor mold.
- Biden's choice for Tanden's replacement will reveal his intentions - whether he aims for a status quo candidate or someone with a more progressive approach.
- The final decision will depend on Biden's working relationship with the candidates and how willing he is to elevate Young to the top slot.

### Quotes

- "Neera Tanden's nomination for Director of the Office of Management and Budget has been or will be withdrawn soon because she is considered very establishment and status quo."
- "Young is seen as a viable candidate due to her lack of political baggage, even though she may not fit the typical Democratic advisor mold."

### Oneliner

Neera Tanden's replacement options range from establishment figures to a wild card candidate, revealing Biden's potential direction towards status quo or progressiveness.

### Audience

Political observers

### On-the-ground actions from transcript

- Support a candidate who represents progressive values in government (implied)
- Stay informed about the potential replacements for Neera Tanden (implied)

### Whats missing in summary

Further analysis and insights on how the chosen replacement for Tanden could impact Biden's administration.

### Tags

#NeeraTanden #PoliticalAppointments #BidenAdministration #ProgressivePolitics #EstablishmentPolitics


## Transcript
Well, howdy there, Internet people. It's Beau again. So today, we're going to talk about
the possible replacements for Neera Tanden. She's out. She had been nominated for Director
of the Office of Management and Budget. That nomination either has been or will be withdrawn
officially soon. So we have to talk about the possible replacements. When we talked
about her, we pointed out she is status quo. She is status quo. Very establishment, very
status quo. Clinton, Obama advisor, Center for American Progress, helped with the Affordable
Care Act. All of the right qualifications to be a Democratic advisor. So there are three
names that are currently being floated that we know about. There could be others, but
these are the three that we know of. One is Gene Sperling, who was a Clinton-Obama advisor,
helped with the Affordable Care Act. Brookings Institute, rather than the Center for American
Progress. There's a tie to Goldman Sachs in there. He was on the National Economic Council.
Very status quo, very establishment. All the credentials, all the little boxes checked.
Another option would be Ann O'Leary, who was an advisor to the Clintons, helped advise
Obama's transition team. Center for American Progress, Chief of Staff for Newsom. Very
establishment as well. However, her politics are a bit more progressive. Not a whole lot,
but enough to be mentioned. If Biden recognizes the opportunity that he has been presented
because of the Republican infighting, if he wants to pursue that dream of an FDR type
of administration, O'Leary is probably going to be the one he pushes forward, because she
could fill the status quo role, but she would also be a little bit more comfortable if he
made some more progressive moves. And then we have a wild card. Shalanda Young. She was
put forth by Biden as the deputy director for the same place, for the Office of Management
and Budget. She is incredibly well-liked by like everybody. Even Republicans are like,
hey, what about her? And she's gotten the nod from left-leaning Democrats. We don't
know a whole lot about her. She was a staffer on the Committee for Appropriations in the
House, became the director of that staff. And that's really all we know. Can't tell
a whole lot about her stances based on that information. I think a whole lot of people
are going to be rooting for her, simply because she doesn't have any baggage. She isn't
the standard Democratic advisor. She doesn't have all of those boxes checked, but she is
incredibly well-liked. And if you're Biden right now, you don't want to follow up with
another candidate who may have a tough confirmation. And she seems like she would just sail through
it. So while she may not have all of the boxes checked, she's definitely still in the running,
I think. It all depends on her working relationship with Biden, what Biden knows about her that
we don't, and whether or not he's willing to bump her up to the top slot. But those
are the three most likely candidates that we know of. There could be somebody he just
pulls out from nowhere and puts forward, but those are the most likely. From that, we'll
be able to tell a little bit about Biden's intentions. If he puts forth somebody that
is completely status quo, we can assume that he either doesn't see the opening he's
been given, or he doesn't intend on pursuing it. So that's where we're at. As far as
a prediction, I don't have one. No clue where it's going to go from here. Anyway, it's
just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}