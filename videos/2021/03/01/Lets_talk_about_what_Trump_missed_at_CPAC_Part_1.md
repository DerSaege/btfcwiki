---
title: Let's talk about what Trump missed at CPAC (Part 1)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0JlbPsG-fFg) |
| Published | 2021/03/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- CPAC was a three-day event that turned out to be a Trump praise-a-thon, with hand-picked speakers solely to commend Trump.
- 95% of CPAC attendees supported carrying forward Trump's policy and agenda, but only 55% preferred Trump as their candidate.
- The Republican Party is likely to adopt Trump's policies but have someone else deliver the message, possibly DeSantis or Hawley.
- Trump's policies were unsuccessful, lacking substance and filled with lies about successes during his term.
- DeSantis and Hawley, potential messengers for Trump's policies, lack his charisma and ability to spin failures positively.
- The Republican Party may face political ineffectiveness if they continue to follow Trump's policy direction without control from figures like McConnell or Romney.
- Trump's children, who have political aspirations, may sow discord within the party by blaming others for failed policies to pave the way for their own political ambitions.
- The Republican Party is currently at a weak point, and the Democrats could capitalize on this vulnerability.

### Quotes

- "Trump doesn't have policy. He never did. He has sound bites."
- "The reality is our biggest trade deficit with China was in the 300s, not $500 billion as claimed."
- "DeSantis and Hawley were, even if you like them, copies of a losing candidate."
- "The Republican Party is at its weakest point in decades."
- "If the Democrats can muster the courage and seize upon this, Biden can get the one thing he truly wants."

### Oneliner

CPAC was a Trump praise-a-thon with lackluster policy support, potentially leading the Republican Party into a losing recipe for political ineffectiveness while Trump's children's political aspirations could sow discord.

### Audience

Political observers

### On-the-ground actions from transcript

- Support political figures with comprehensive policies and integrity to avoid falling into the trap of charisma-based politics (implied)
- Advocate for a diverse range of voices within the Republican Party to prevent political stagnation and ineffectiveness (implied)
- Stay informed and engaged in political discourse to understand the dynamics within parties and potential impacts on policies (implied)

### Whats missing in summary

Analysis of how the Democratic Party can capitalize on the Republican Party's vulnerabilities to advance their own agenda.

### Tags

#CPAC #RepublicanParty #Trump #PoliticalAnalysis #Policy #DemocraticParty


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about CPAC, what it means for the Republican Party in the future,
what we missed because we missed something in the conversations that occurred over the
weekend.
That's this.
Part two will be coming this afternoon and it's what it means for the Democratic Party.
Okay so if you don't know, CPAC is an incredibly influential conservative conference.
Held over the weekend.
Three days of fun and sun and just amazing intellectual discourse and conservative thought.
Okay, what was it really?
A three day Trump praise-a-thon with hand-picked speakers who were going to praise the man
for the crowd.
If you were a Republican but you were not a Trump supporter, this event wasn't for you.
It was billed as being pro-Trump.
Everything about it was set up to be pro-Trump.
The speakers, the attendees, the decor.
They had, I'm not joking, a little golden Trump idol thing.
I don't know what to call it.
I'm sure that people are going to talk about the decor at length so we're going to kind
of skip over that.
The basic series of events was it was three days of warm-up, people praising Trump, and
then a speech from dear leader himself.
That's what got covered.
There were polls that were taken and two that matter.
These polls were taken of the attendees, pro-Trump people.
Ninety-five percent of those in attendance want the Republican Party to carry Trump's
policy and agenda.
They want to carry that forward.
But only 55 percent said Trump was their preferred candidate.
That's important because this is a very influential conservative conference.
That's important.
The most likely outcome from that is that the Republican Party will take policy points
and agenda directives from Trump, but it's going to be somebody else carrying the message.
DeSantis or Hawley, somebody like that.
That's the most likely outcome, unless somebody intervenes.
There's a problem with that, and it's a big one.
Trump doesn't have policy.
He never did.
He has sound bites.
He doesn't have policy.
If he had policy, if he had successful policy, he wouldn't have had to get on stage at this
event in front of his supporters and lie about successes, talking about how we had a $500
billion trade deficit with China and he fixed it and all of that.
The reality is our biggest was in the 300s, and it was in the middle of his term.
His policies were an unmitigated failure.
But Trump himself, well, he could spin that.
He could talk in a way that would make a certain group of people feel warm and fuzzy about
his sound bites.
They believed him.
They didn't believe in his policies.
Most of them don't even know what that was.
They don't know what he did.
They know what he told them.
But now they want Trump's policies, but they want DeSantis or Hawley to carry the message.
DeSantis and Hawley were, even if you like them, they were Trump yes men.
They were copies of a losing candidate.
They don't have his charisma.
They're not going to be able to make people feel warm and fuzzy about failed policies
the way Trump could.
This is a losing recipe for the Republican Party, and it appears like they're leaning
into it.
It certainly looks like they put all of their eggs in one basket and then dropped it and
stepped on it and then beat it with an American flag.
That's what it looks like.
If McConnell or Romney doesn't get control of that party and soon, they're done.
They're going to be politically ineffective for a while because they're going to be taking
policy points from somebody who has no idea what policy is.
He's going to bark orders into a phone.
They're going to try to obey, and when they mess up, well, he'll blame them.
It's what he's always done.
And then there's something else that might sow some discord within the Republican Party.
Trump's kids have political aspirations of their own.
DeSantis, Hawley, they polled higher.
Ivanka and the brother, whatever his name is, they didn't poll very well, single digits.
If we know anything about the Trumps, it's that they're in it for themselves.
So when they start handing policy points and directives to DeSantis or Hawley, and then
they inevitably fail because it's bad policy, who do you think they're going to blame?
Who do you think they're going to turn on?
Probably DeSantis, probably Hawley, because realistically, they're going to be hoping
for it because they have to get them out of the way so we can have President Ivanka one
day.
The Republican Party is at its weakest point in decades.
If the Democrats can muster the courage and seize upon this, Biden can get the one thing
he truly wants.
And we will talk about that in part two.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}