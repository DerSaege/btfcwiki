---
title: Let's talk about what Biden missed at CPAC (Part 2)....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=UCooPhleXSk) |
| Published | 2021/03/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- CPAC events have a significant impact on Biden, with Republicans appearing less united, carrying Trump's talking points.
- Trump's base is expected to focus on culture war issues like Oreos, Dr. Seuss, and Mr. Potato Head, which may be ineffective.
- Lack of unity in the Republican Party provides Biden with an opening to pursue his ambitious goals.
- Biden aims to emulate FDR by delivering policies that benefit the working class, unions, and infrastructure, albeit without labeling it as the Green New Deal.
- To secure support for his vision, Biden must appeal to TikTok teens and the real left, including figures like AOC.
- Failure to deliver for the younger generation may result in disengagement and political consequences.
- Biden needs to act swiftly, without making excuses or catering to billionaire interests, to secure the support he needs for his long-term goals.

### Quotes

- "They're going to be out there talking about Oreos, Dr. Seuss, and Mr. Potato Head."
- "He wants to be the next FDR. He kind of has to deliver what FDR delivered."
- "He has to deliver for the TikTok teens."
- "If you do not deliver for them, they will not show up."
- "He has to deliver for voters who can't even vote yet."

### Oneliner

CPAC events create a window of disunity in the Republican Party for Biden to pursue ambitious FDR-like policies, appealing to TikTok teens and the real left for long-term success.

### Audience

Politically Engaged Citizens

### On-the-ground actions from transcript

- Appeal to teenagers and deliver policies that resonate with them (implied)
- Engage with the real left, including figures like AOC (implied)
- Advocate for policies that support the working class and infrastructure development (implied)

### Whats missing in summary

Insights on balancing policy delivery with engaging younger generations for sustained political support.

### Tags

#Biden #CPAC #RepublicanParty #FDR #TikTokTeens #AOC


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about what CPAC means for Biden because it has, the events
of this weekend have a pretty big impact for him.
If you haven't watched the first part to this, go watch it.
It lays out why I'm about to say everything I'm about to say about the Republican Party.
Third version, they are going to be less than united, to say the least.
We're going to have figures like DeSantis and Hawley on the national stage carrying
talking points from Trump.
The thing is, they're not Trump.
They can't spin stuff the way that Trump did.
So he's not going to be able to give them any real policy.
I mean he doesn't have any anyway, but even if he did, he wouldn't be able to give it
to them.
He's going to have to give them what amounts to culture war stuff.
They're going to be out there talking about Oreos, Dr. Seuss, and Mr. Potato Head.
And if you have no idea what those three things have to do with each other, neither does most
of America.
It's going to be completely ineffective.
And it is going to give Biden the opportunity to get the one thing that he really, really
wanted.
If you go back to October, you will find article after article saying that Biden wants to be
the next FDR.
That is ambitious.
Then November came.
He didn't get the blue wave he needed.
Didn't get the support that he has to have to be the next FDR in the House and the Senate.
Barely scraped by.
But the lack of unity in the Republican Party gives him an opening.
He can do it if he acts now.
If he lays the groundwork now and is ready to go the second we see DeSantis and Hawley
out there with talking points from Trump.
Because from that point forward, the Republican Party is going to be fighting each other.
And then Biden has to deliver for a surprising group of people.
Sixteen and seventeen year olds.
Biden has to deliver for them first.
He wants to be the next FDR.
He kind of has to deliver what FDR delivered.
Labor stuff, unions, stuff like that.
Economic policies that help the working class, poor people.
And then infrastructure.
Not the Green New Deal.
Don't call it that because that will give them ammunition for the culture war.
But infrastructure.
It just happens to be environmentally friendly and move us to a different kind of energy.
Trump's base, the base that they're going to be appealing to, they're not going to care
about mundane policy stuff.
Because DeSantis and Hawley, they're going to be wanting to provoke outrage.
Don't give it to them.
If he steers clear of the culture war items, he can pretty much do anything else he wants.
But he has to start by delivering for the TikTok teens.
Because he's going to need them in 2022 and 2024.
If he wants to be the next FDR, he needs two terms.
You want to reach those teens?
You need the left.
The real left.
Not Pelosi.
AOC.
And further left.
That's who you're going to need.
That's who Biden is going to need if he wants to achieve his dream.
He can do it.
It's within his grasp.
He got an opening.
Five days ago, wouldn't have said this.
The reality is he has to deliver for them, though.
Because they're not engaged.
They're engaged politically in the sense that they know everything is wrong.
They grew up in this mess that we left them.
They do not care about minor wins.
They don't care about pragmatic politics.
They don't care about excuses.
And they don't care about civility.
Certain Senate traditions may have to fall by the wayside.
If you do not deliver for them, they will not show up.
They have grown up in this very unforgiving world that we have left them.
And it has made them very unforgiving.
If he wants his dream, it is within his grasp.
But he has to lay the groundwork now.
No excuses.
No stalling.
No concerns about the feelings of billionaires.
He has to move to appeal to teenagers.
He has to deliver for voters who can't even vote yet.
If he does that, if he gets them engaged 2022, 2024, and 2026, they're a lock.
If he doesn't, I guess we'll say hello to President DeSantis.
That's going to be the only way for him to achieve his goal.
He may scrape by and get a second term without it.
But if he wants to be compared to a president that fundamentally altered this country, he
has to deliver a lot and quickly.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}