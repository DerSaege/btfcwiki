---
title: Let's talk about Biden's mental state....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=p4-fxOZIVhU) |
| Published | 2021/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains a bad faith argument surrounding accusations of Biden having dementia.
- Argues that there's no need to respond in good faith to baseless claims.
- Points out the futility of trying to prove a negative.
- Mentions the presumption of innocence and the endless shifting of goalposts.
- Describes how people latch onto any evidence to support their claims about Biden's dementia.
- Talks about how mispronunciation doesn't indicate lack of intelligence.
- Suggests leaning into the claim that Biden's advisors make decisions for him.
- Mentions the theory that Harris has the final say over Biden.
- Comments on Trump being beaten in the polls by a non-white woman.
- Concludes by discussing methods to handle such arguments effectively.

### Quotes

- "You cannot prove a negative."
- "Lean into that."
- "A non-white woman is beating him in the polls? That's gotta bother him."
- "We elected a guy with dementia over him. That's how bad he was doing."
- "Shutting them up is the next best thing."

### Oneliner

Beau explains the futility of proving baseless claims and suggests tactics to handle bad faith arguments effectively.

### Audience

Debaters, truth-seekers, critical thinkers.

### On-the-ground actions from transcript

- Lean into wild accusations (exemplified).
- Repeat process to shut down baseless claims (implied).

### Whats missing in summary

The importance of shutting down baseless claims effectively in debates or arguments.

### Tags

#Debunking #BadFaithArguments #Biden #Dementia #PresidentialPolitics


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about the guy in the Oval Office, the president and
his state, because I got a question from somebody who wants to talk to their
family about something that is being said about the current president and how
they can prove it's not true and it goes to the whole idea that Biden has lost
the plot, that he has dementia and they want to know how they can prove that he
doesn't to their family members.
I've had this conversation before.
This is a bad faith argument.
The accusation isn't made in good faith.
There's no reason to respond to it in good faith.
You cannot prove a negative.
You can't prove something isn't true.
That's not really how it works.
That's why we have the presumption of innocence.
We don't have to prove our innocence, at least in theory.
Because once the allegation alone
enough to require a rebuttal, it never ends. I think you knocked over this gas
station. No, no, no. I was on camera here at this other store. Those are fake. I need
you to prove they aren't. The goal posts constantly get shifted and that's what
happens here and they latch on to any evidence to support their claim while
demanding evidence that can't exist. As an example, with Biden they will latch
onto the fact that he has a stutter. Now something that people with a stutter do
at times to get over it is if they get hung up on a word they'll switch to a
different word. So if they were gonna say the White House they might switch to
Oval Office. If they were going to say Biden, they might switch to the president, they will latch on
to that, they'll latch on to any time a name escapes him, they will latch on to any cafe he makes
where he mispronounces a word. Incidentally, I would point out, as I've pointed out in other videos,
Because normally when somebody mispronounces a word, it means that they are well read.
They read beyond their peers, so they see words in print and they never have a chance
to discuss them.
So they don't know how to pronounce them.
It's not actually a sign that somebody isn't smart.
So you will never get anywhere asking them for evidence.
Unless you can change their desire to believe it's true.
Their claim, Biden has dementia.
You have to make them not want to believe that's true.
That sounds hard, it's not.
The running theory with this is that his advisors are actually making all the decisions and
he's a running rubber stamp.
Man, I mispronounced a word. I need to go to the doctor.
Um, where was I at?
Okay, so his advisors make the decisions.
He's just a rubber stamp. Lean into that.
Don't say it's not true. I do this a lot on this channel when there's a wild
accusation that's made.
like with Abbott recently. Just lean into it for a second.
You know it's not true, but go ahead and pretend it is.
So his advisors are making the decisions.
So like his foreign policy person, the person that would normally advise him on
foreign policies making the foreign policy decision
and the person who would normally advise him on economics
is making the economic decisions. Yeah!
So rather than a politician
We have a team of experts making the decisions, huh?
And you think that's a bad thing?
At this point in the conversation, it turned to,
no, no, well, Harris actually has the final say.
Man, that's gotta bother Trump, really does.
I mean, that's gotta bother him a lot,
being bested by her in the polls,
quiet part out loud for a second
for those who don't deal with these sorts of people often.
A non-white woman is beating him in the polls?
That's gotta bother him.
And you know what?
This whole thing, that really just shows
what a bad job he was doing.
I mean, the whole country,
we elected a guy with dementia over him.
That's how bad he was doing.
And he's doing better in the polls now.
Has a higher approval rating than Trump ever did.
Wild, wild stuff.
So anyway, what makes you think he has dementia?
They're not gonna be interested
in selling this point anymore.
They're not going to want to prove
that everything you just said is accurate.
So they lose,
they lose the desire to make this claim.
Is it effective overall?
No, they'll probably believe whatever random talking point comes up next, but you can repeat
this same process. And if you do it enough, what happens, at least with me, is that these people
just stop talking to you about that kind of stuff. Which, if you can't change their mind,
shutting them up is the next best thing so anyway it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}