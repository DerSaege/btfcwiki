---
title: Let's talk about Biden's boldest move yet....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xTsb493st_A) |
| Published | 2021/03/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration hinted at involving Iran in Afghanistan, which could be a beneficial move.
- The U.S. involvement in Afghanistan has evolved over time, leading to a complex situation.
- Iran's capabilities make them a suitable candidate to take over from the U.S. in Afghanistan.
- Involving Iran could help stabilize the region and prevent further Western intervention.
- The move benefits all parties involved and has significant implications for the Middle East's power balance.
- Beau expresses skepticism about the Biden administration's willingness to pursue this strategy due to potential political backlash.
- Republican opposition to such a move may stem from a desire to obstruct Biden rather than genuine concerns.
- Beau argues that involving Iran is a viable path to finally withdrawing U.S. troops from Afghanistan.
- The priority should be on preventing chaos and instability in Afghanistan post-U.S. withdrawal.
- The potential downside of Iran's involvement could lead to chaos if they are unable to stabilize the situation.

### Quotes

- "This is the right move for the people that everybody likes to pretend they care about."
- "The only reason to oppose this is if you care more about giving Biden a black eye than innocent lives."
- "Foreign policy is never about right and wrong."
- "Once we go back in, the exact same thing will happen."
- "It benefits them both."

### Oneliner

Be ready to support involving Iran in Afghanistan for the sake of stability and troop withdrawal, despite potential political opposition.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Support the move to involve Iran in Afghanistan to stabilize the region and bring troops home (suggested).
- Advocate for a strategic withdrawal from Afghanistan to prevent chaos and further intervention (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the potential benefits and challenges of involving Iran in Afghanistan, urging support for a strategic move that prioritizes stability and troop withdrawal.


## Transcript
Well howdy there internet people it's Beau again so today I need your attention
the Biden administration made an announcement at least publicly said something I don't know
that it was intended to be an announcement but we need to talk about it because you need to be ready
to be in favor of it basically even if you even if you really don't like Biden.
Okay I'm going to reference two videos during the course of this they'll both be down below
the first is one from a couple of weeks ago where we were talking about Biden's first
real foreign policy test and it was Iran. In that video I laid out a whole bunch of
things that the Biden administration could do to help bring Iran out of isolation.
One thing I mentioned and I referred to it as pie in the sky I said that I didn't think it was likely
to happen publicly because quite frankly I didn't think the Biden administration had the
courage to attempt it because while it is an incredibly good move
politically here at home it may have costs.
They announced or at least said that they want to do that they want to have Iran help us
in Afghanistan and by help us I mean relieve us.
In another video we talk about foreign policy in depth and we talk about slogans and how people
will say stuff like we need to bring our troops home from the Middle East and how it's not
really that simple especially in the case of Afghanistan. We went in to destabilize a couple
of groups that's why we went in. Mission creep, goals changed, next thing you know we are propping
up the host government in Kabul. Once we started doing that we really couldn't leave not realistically
because if we left we create a power vacuum. We're a piece of metal. We're that piece of metal.
We got to get to the hospital before we take it out. The problem is there hasn't been a hospital around.
It's not like a western power would want to step in and take our place.
Iran can. They have the force multipliers. They have the fire support. They have all the assets
that they would need to take over for us. Aside from that while we were there helping
we were also the opposition's best recruitment tool. Aside from the obvious reasons they were also
able to cast the government in Kabul as a western puppet. Nobody is going to be able to accuse Iran
of being a western puppet. That's not a thing. That's not going to happen. So it provides
the ability to fill that power vacuum and save a whole bunch of innocents and for us to leave
and not encourage more western powers to get involved all at once. It's a winning move
for everybody. The national government there gets the assistance they need. We get to leave.
Iran gets a seat at the big table. They get to do something that is unanimously seen as a good thing
for the international community. Aside from that they get to strengthen their ties with Kabul.
It helps them project and gives them political influence so they don't have to rely on their
non-state actors. It helps shift the entire balance of power in the Middle East.
This one little move would have far-reaching impacts.
With all of that said, why didn't I think the Biden administration would do it?
Well, I mean, realistically, because of the impacts here at home politically.
The Republican Party has taken a very obstructionist view towards everything
that the Biden administration is attempting. I would imagine that they will come out and say
something to the effect of, we fought there for 20 years and Biden is handing Afghanistan to
Iran. We do not conquer countries territorially anymore. That is not something we do anymore.
Afghanistan isn't ours to give. This is a neighboring country stepping in to help and fill
a role so we can leave. And a whole bunch of innocents are going to be really happy with that.
Because while the U.S. was not always careful with its fire and Iran will not always be careful
with its fire, the opposition will directly target them. So it's a net win for them. It gets the U.S.
out. It helps stabilize the entire region. It's a win for everybody. But politically, it's a move
that looks like or could be painted as selling out the troops to benefit Iran. That's not what it is.
If that becomes a talking point, it needs to be very clearly understood that the Republican Party
is putting their political ambitions over the lives of American troops, the lives of innocents,
over us getting out, over the stability of the Middle East, over everything.
In 20 years, there have been two plans that had even a remote chance of success of getting us
out once we got in. The other one, we'd still be there for another 10 years if we did it.
This is the door. This is how we get out. This is how we bring our troops home.
We have to fill that power vacuum. We talked about it in the video. Foreign policy is never
about right and wrong. It's not really that American foreign policy cares about the innocents,
to be real clear on that. It's that we can't be in a position where we leave yet another country
and it descends into utter chaos. That's the reason it's important to us. That's the reason
it's important for us to have an orderly withdrawal. Because if it does descend into chaos,
they're going to be those people who want to go back in. Once we go back in, the exact same thing
will happen. Mission creep will occur. We will end up propping up the national government,
and we will be stuck there yet again. The only possible downside to this is that if Iran takes
over and can't hold their own, can't stabilize it, then it may descend into chaos anyway.
But realistically, if Iran can't do it, neither can we. And we're just wasting years staying there.
So it doesn't matter if you are so far to the left you don't like Biden. It doesn't matter if you're
a Republican. It doesn't matter. This is the right move for the people that everybody likes to
pretend they care about. The innocents on the ground and the troops. It benefits them both.
The only reason to oppose this is if you care more about giving Biden a black eye
than you do a whole bunch of innocent lives. I don't know that the Biden administration could
pull this off. Even though they apparently do have the courage to attempt it, it's going to be
it's going to be difficult. But if they try, we as the American people need to give them every bit
of support we can because the defense contractors and everybody who's making money right now,
they're going to be against it. And they have them. They own a lot more senators than we do.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}