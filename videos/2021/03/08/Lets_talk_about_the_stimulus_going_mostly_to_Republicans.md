---
title: Let's talk about the stimulus going mostly to Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=c47rKtuJl_Q) |
| Published | 2021/03/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- There are two stimuli being discussed: the popular one with $1400 checks, and the lesser-known one primarily benefitting Republicans.
- The popular stimulus helps those left behind by the system due to factors like job loss and educational barriers.
- The second, less publicized stimulus aims to assist individuals who acknowledge the flaws in the system and need regular government aid.
- Republicans who support this second stimulus may experience a shift towards compassion and human decency.
- The year has shown that many people are not as secure as they believe, and anyone could suddenly find themselves in need of assistance.
- Beau suggests that more individuals should recognize the brokenness of the current system that frequently leaves people behind.

### Quotes

- "That second stimulus, that one nobody's talking about, that's gonna go primarily to Republicans who will acknowledge this, perhaps for the first time."
- "It might be wise for a whole lot of people to realize this system is broke."
- "Because it's not always something that's predictable. It can come out of nowhere. They're no fault of your own."

### Oneliner

There are two stimuli: popular aid for those left behind, and a less known one to stimulate compassion among Republicans for ongoing government assistance.

### Audience

American citizens

### On-the-ground actions from transcript

- Acknowledge flaws in the system and advocate for more comprehensive government aid (suggested)
- Support policies that aim to assist individuals in need of regular government assistance (implied)
- Advocate for a more inclusive and supportive system for all individuals (implied)

### Whats missing in summary

The full transcript provides a detailed insight into the contrasting stimuli being discussed, urging people to acknowledge systemic flaws and advocate for more comprehensive government assistance.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the stimulus,
but not the one you're probably thinking about
because there's the one that everybody's talking about
right now and then there's the other one,
the one that isn't really being advertised too much.
And it's odd because even though we have a house
and a Senate and a White House controlled by Democrats,
the second part of this, that stimulus,
the one nobody's talking about,
that's gonna primarily go to Republicans.
The one that everybody knows about is the $1,400 checks,
the extended unemployment benefits, all of that stuff.
And that's gonna go to people who,
through no fault of their own,
they got left behind by the system,
is what it boils down to.
I mean, there aren't a whole lot of people out there
whose investment portfolio includes a plan
to deal with a massive public health issue
that puts them out of work or limits their income
for a pretty extended amount of time.
That's not a normal thing.
So a whole bunch of people, through no fault of their own,
wound up in a situation where they didn't have enough food
to put on the table, they couldn't make ends meet,
they couldn't get a job,
there was an educational barrier barring them
from the new jobs that opened up,
like in nursing or whatever.
So they got left behind by the system
and they needed a little bit of help.
That's the stimulus everybody knows about.
Now, the other one, the one they're keeping quiet,
is gonna go to people who, because of that stimulus,
acknowledge that the system is broke,
that there are a whole bunch of people
who get left behind through no fault of their own
because of an educational barrier,
because of being unable to get a job,
because of their geographic location, whatever.
They wind up in a situation where they can't make ends meet.
They don't have enough food to put on the table
and they need a little help.
They need a little assistance from the government,
perhaps on a more regular basis.
That second stimulus, that one nobody's talking about,
that's gonna go primarily to Republicans
who will acknowledge this, perhaps for the first time,
and hopefully it will stimulate their human decency,
their compassion towards people
who need a hand up every once in a while,
those people who are on SNAP, those people who get help.
Because if this year has taught us anything,
it's that most people aren't as secure
as they like to think they are,
and that it doesn't take very long
to go from being the one that is looking down on everybody
to being somebody who needs a little help.
It might be wise for a whole lot of people
to realize this system is broke.
It doesn't function.
It leaves people behind all the time.
And as those being left behind, as their numbers grow,
you have to wonder how long it's gonna be until you're
in that group being left behind.
Because it's not always something that's predictable.
It can come out of nowhere.
They're no fault of your own.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}