---
title: Let's talk about food security and planting your yard....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Rs9IKs_2FF4) |
| Published | 2021/03/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Advocates for planting food in yards annually for food security, exercise, environmental benefits, and community building.
- Encourages planting fruit trees, nut trees, bushes, and gardens tailored to USDA growing zones.
- Points out misconceptions about difficulty, time, cost, and space constraints for planting food.
- Notes that planting trees and bushes requires minimal effort once established.
- Mentions the availability of seeds or plants for those on SNAP or EBT programs.
- Suggests using containers for limited space and replacing ornamental bushes with fruit-bearing plants.
- Emphasizes the community-wide benefits of sharing fresh produce with neighbors.
- Proposes a simple and enjoyable way to strengthen local communities through gardening.

### Quotes

- "Food not lawns."
- "It helps foster that community."
- "It's not hard if you're just focusing on trees and bushes."
- "Generally, it's not expensive."
- "It's just a thought."

### Oneliner

Beau advocates for planting food in yards annually for food security, exercise, and community building, debunking common misconceptions about difficulty, time, cost, and space constraints. 

### Audience

Community garden enthusiasts

### On-the-ground actions from transcript

- Plant fruit trees, nut trees, bushes, or garden in your yard tailored to your USDA growing zone (exemplified).
- Share excess produce with neighbors to foster community (implied).

### Whats missing in summary

The full transcript includes detailed explanations of the benefits of planting food in yards and debunks common misconceptions, providing a comprehensive guide for those interested in community gardening.

### Tags

#CommunityGardening #FoodSecurity #EnvironmentalBenefits #CommunityBuilding #USDAZones


## Transcript
Well howdy there internet people it's Beau again. So today we're going to talk about a little bit of
advocacy I do every year. If you have been here since the beginning of this channel you have heard
this message twice. If you're here next year at this time you'll hear it a fourth time. I think
it's important. It's that time of year. It's getting time to get ready to plant your yard,
food not lawns, whatever slogan you want to assign to it. There are a number of reasons
people support this idea. And the first is the obvious one, food security. You put in some fruit
trees or nut trees or bushes or garden, anything that helps create just a little bit of food
security is a good thing. Second it helps the environment. Third it gives you exercise. And
fourth you end up meeting your neighbors. How many times have you said we don't even know our
neighbors anymore? This gives you a reason and it kind of forces you to because generally if you put
in a few different types of plants that produce food one of them is going to over perform. For me
it's lemons. I have two lemon trees and they're mutants. They produce more. We can't give them
away. We can't give them away. It produces a whole lot more than expected. Now the four reasons
people generally don't want to do this is one it's hard. It's not really. If you're putting in stuff
that comes back year after year it's not hard. If you're going to garden it's a little bit more
time-consuming. The key part is to look at your USDA growing zone. It's a number.
Get that number and you get stuff that is designed for your climate that works well in your climate.
Don't try to go out of it even by one or two. I mean I know you will find things that say you can
do it if you do x y and z. That makes it a lot harder. I happen to be in USDA growing zone 8
which means I get to grow all kinds of cool things. It's not hard if you're just focusing
on trees and bushes. You put them in. You tend to them every once in a while after that. It doesn't
take much. This year I will do nothing because we're getting ready to go on the road and all of
that stuff. So I'm not going to put in anything new but because of the work of years past we will have
low-quat and kumquat and bananas and oranges and lemons and grapes, peaches and pears and asparagus,
blueberry raspberry and blackberry. It's no work. It's just going to come back year after year
if you do it. The other one is time. It doesn't take much time. Not really. Once they're in,
they're kind of in if you're talking about that. If you're talking about gardening,
it's a little bit different. That is more time consuming. The third is money.
So far with the exception of the apple trees which aren't doing well, anything I've put in
is not going to be a problem. But then we got back as far as the amount of produce we got back.
Generally it's not expensive. With trees it takes longer, it takes a couple of years
for them to start producing anything. But you normally get your money back.
The other thing I would point out is that if you are on SNAP, EBT, you can get seeds or plants,
but the amount of produce generally qualifies. So you can do it that way if it's within your means.
If you have that extra to use, most times you don't with the current budgets on that stuff.
And then the last one is space. I don't have space for that. You can do it in containers.
Trees really don't take up as much space as people would think. And most people have a bunch
of stuff around their house. Bushes, Japanese boxwoods are real common up against your windows.
The whole point of those bushes is to keep people away from your windows.
If you were to replace those with raspberry bushes as an example, nobody's going near your
windows because nobody wants to mess with those thorns. At the end of the day, doing this,
it helps your whole neighborhood. And if you were to do pear and your neighbor does apples
and the other neighbor does peaches, you have a bunch of fresh fruit in the area and it doesn't
take a whole lot and it helps foster that community. And it helps kind of build things
at the local level in a fun way. So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}