---
title: Let's talk about Biden's request for an extension....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BVn4pCkgMEo) |
| Published | 2021/03/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains Biden administration's request to extend the withdrawal date from Afghanistan.
- Believes extension is unnecessary due to professional militaries ready to take over quickly.
- Points out that the current perception of troops all over Afghanistan is outdated.
- Emphasizes the importance of leaving Afghanistan at the soonest possible moment.
- Acknowledges the need for the U.S. to honor agreements despite criticizing past decisions.
- Advocates for letting the people of Afghanistan decide the timeline for withdrawal.

### Quotes

- "The best course is to get the U.S. out at the absolute soonest opportunity."
- "We still need to leave. I don't believe the extension is really necessary."
- "The U.S. has about 2,500 troops in Afghanistan. That's a token force."
- "We already are in a token force. Does that mean that it is unimportant to remove that token force? Absolutely not."
- "It's bad if a deal is made and then the next president comes along and breaks it."

### Oneliner

Beau believes the U.S. should leave Afghanistan promptly, as an extension is unnecessary, and the decision should lie with the Afghan people.

### Audience

Policy makers, Activists, Citizens

### On-the-ground actions from transcript

- Let Afghan people decide on the withdrawal timeline (implied)
- Advocate for prompt withdrawal from Afghanistan (suggested)

### Whats missing in summary

Detailed analysis of potential consequences of a prolonged presence in Afghanistan.

### Tags

#Afghanistan #USWithdrawal #BidenAdministration #ForeignPolicy #Peacekeeping


## Transcript
Well, howdy there, internet people. It's Beau again. So today we're going to talk about Biden's
request for an extension because it prompted some questions and they're good questions. I want to
answer them. However, I want to make something very clear before I do. Nothing I'm about to say
changes the reality that the United States needs to leave as soon as possible. The second
another stabilizing force shows up, we need to be gone. Nothing I'm about to say changes that.
I want that to be very clear. I will probably repeat that later.
If you have no idea what I'm talking about, the Biden administration
has kind of put in a request to extend the withdrawal date to get out of Afghanistan.
I do not believe it is necessary to extend the date. I don't think it's necessary. The countries
that appear to be willing to step in and take over the stabilizing force role, they're professional
militaries. They can move the troops quickly. To their credit, it doesn't seem that the Biden
administration believes it is necessary either, just that it would be easier. One of the public
statements kind of said that, you know, even if we get the extension, we're still hoping to be out
by the current withdrawal date. So it seems like they're more interested in getting the
replacement force in and letting them settle in before the U.S. leaves. I don't think that's
necessary. And I don't think that's necessary because one of the questions is, would this just
be us saying we're staying and more of the same? No, this is a request that was put into the
national government and to the opposition. The opposition did not reject it out of hand,
at least not as of time of filming. The fact that they didn't just say no immediately
leads me to believe they may be more interested in peace. They may have realized that the
situation on the ground has changed a lot, and if they are more interested in peace,
they are less likely to test the new stabilizing force, test those countries.
It seems likely that the opposition has looked at the current political landscape and realized
that that power vacuum that will occur if the U.S. withdraws before a new force arrives,
it may not benefit them necessarily. 2003? Oh yeah, that would have been entirely in their favor.
Not anymore. It would be pretty evenly matched. That's good news because it's more likely to
bring them to the table. It's bad news because if fighting does happen, it's going to be more
fierce, and that's bad news for the average civilian. But given the fact that they're
giving the fact that they didn't reject it out of hand, that really, it seems like they may be
willing to do the fighting in a conference room rather than on the battlefield, and that's the
best news that's come out of Afghanistan in a long time. Okay, so even though we won't leave by
the current deadline, can we just withdraw down to a token force and get rid of the occupying army?
Okay, so it's not decided that this is going to happen yet. The other part of this that
should probably be pointed out is that we already are a token force. The United States
has about 2,500 troops in Afghanistan. That's a token force. The common perception, the common
image that people have of what's going on in Afghanistan, that doesn't really match the
reality anymore. It's no longer troops all over the country running patrols and going into villages,
and that's really not occurring very much anymore. We have 2,500 troops there.
That is a token stabilizing force. To put it in perspective, I could point to a dozen countries
that are at peace that have more than 2,500 U.S. troops in them, and I'm not talking about like
the obvious ones like Germany or Japan or South Korea. We have more troops in Spain than we have
in Afghanistan. I think the most poignant example for people who are completely unfamiliar with how
many troops it takes to do something, we have more troops currently guarding the U.S. Capitol
than we have in the entire country of Afghanistan. We already are in a token force. Does that mean
that it is unimportant to remove that token force? Absolutely not. We need to get out. Absolutely.
The reality is that even though what was happening isn't happening anymore,
it's the same uniform that is present. Any country that would be willing to step into that role
would probably do a better job simply because of that fact. The animosity that exists among some
groups within Afghanistan directed towards American troops wouldn't be there because the
new troops, they didn't occupy the country. If you are looking at this from the perspective
of what's best for Afghanistan in the sense of what is going to stop conflict, what is going to
save the most lives, you're not looking at it through national ego, you're not looking at it
through economic interests. The best course is to get the U.S. out at the absolute soonest
opportunity. The second the new stabilizing force arrives, we need to leave. And it does appear that
there are a couple of countries that are willing to step up that are regional players, that are
local, that are willing to step up and provide that assistance. So what does all this mean?
We still need to leave. I don't believe the extension is really necessary.
The Biden administration appears to believe that it would make it easier.
At the end of the day, it's going to be decided by the people in Afghanistan,
the national government and the opposition. If they decide it is necessary, fine, I won't say a
word. But I don't think it is. I think that it can be done in the time frame that currently exists.
I would also hope that the Biden administration understands. Yeah, the withdrawal date, all of
these artificial timelines and stuff like that, that was a bad move. It was a bad deal.
The Trump administration shouldn't have done it. Agreed. However, given the current situation in
the Middle East, the U.S. has to honor that deal. The U.S. has to honor that deal. It's bad if a
deal is made and then the next president comes along and breaks it. It would be worse to continue
that tradition. If the opposition or the national government says, no, we have a withdrawal date,
we want you out by then, it needs to happen. This needs to be entirely in their hands.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}