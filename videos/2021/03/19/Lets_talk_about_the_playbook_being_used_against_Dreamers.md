---
title: Let's talk about the playbook being used against Dreamers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=w-G2_Hbq2iQ) |
| Published | 2021/03/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Receiving a message critiquing his historical conclusions on America.
- Expressing disbelief in the idea of significant progress in the United States.
- Questioning whether the same discriminatory playbook is still in use today.
- Drawing parallels between past discriminatory practices and current attitudes towards undocumented workers.
- Mentioning the subtle ways discriminatory policies are implemented today.
- Bringing up issues like public school access for undocumented workers and scapegoating them for various problems.
- Referencing past instances of justifying reprisals against undocumented individuals based on isolated incidents.
- Pointing out how immigrants are blamed for various societal issues despite statistical evidence.
- Mentioning the lack of a direct "police tax" on immigrants, but suggesting the government's willingness to implement one if possible.
- Explaining how discriminatory policies are still institutionalized, such as the economic benefit requirement for legal immigrants.
- Providing historical context on immigration control in the United States and the perpetuation of exploiting and scapegoating different groups.
- Criticizing the continued use of discriminatory tactics in American politics, using Dreamers as a contemporary example.
- Emphasizing the cyclical nature of scapegoating and exploitation in American history.
- Stating the need to acknowledge past injustices to move forward and fulfill promises made to all Americans.

### Quotes

- "Have we really come that far?"
- "This isn't us. Yeah, it is us."
- "The institution of the United States is built on scapegoating people while exploiting their labor."
- "It hasn't changed. Just shifts the group of people being exploited and scapegoated."
- "The only way we can actually really achieve the promises that were made a couple hundred years ago is to acknowledge everything that's happened and make sure it doesn't happen again."

### Oneliner

Beau questions the progress of the United States, drawing parallels between historical discrimination and current policies towards immigrants, urging acknowledgment of past injustices to move forward.

### Audience

Activists, Advocates, Immigrant Rights Organizations

### On-the-ground actions from transcript

- Advocate for comprehensive immigration reform to protect the rights of all immigrants (suggested).
- Support organizations working towards fair treatment and opportunities for undocumented individuals (implied).
- Educate others on the historical and present-day discriminatory practices in immigration policies (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of historical discrimination in immigration policies and urges reflection on how acknowledging past injustices is vital for progress.

### Tags

#Immigration #Discrimination #Scapegoating #UnitedStates #History


## Transcript
Well, howdy there internet people, it's Beau again.
So today, we're going to talk about a message I received in regards to yesterday's video.
I am new to the channel and love the historical bits you include.
Yesterday was no exception, however, I vehemently disagree with your conclusions.
This isn't the United States today.
We do not use this same playbook today, and seeing somebody say that we do and failing
to acknowledge how far we have come is damaging to America.
Have we though?
I mean, have we really come that far?
This sounds good.
I wish this was true.
And I guess, I mean, maybe we have.
Maybe the same playbook isn't used today.
Or maybe it is.
Literally used today, like today.
And the plays are just called out in a more subtle manner to the American people.
I mean, I guess you're right.
We don't say that that demographic over there, their kids can't go to school with ours.
We don't say that.
Unless you're talking about undocumented workers.
Then there's that whole issue.
Should they really be able to go to public school?
Do we really want them with our kids?
Because I mean, if that happened, our kids might find out that their kids are just people.
Nah I mean, I guess we don't have laws saying don't marry them.
Don't start a family with them.
Anchor babies.
We probably don't single them out, single one of them out and use that as a justification
for reprisals against all of them.
I mean, I guess we kind of do that too.
Remember that whole thing under Trump where they showcased some horrible thing that an
undocumented person had done as an attempt to justify the reprisals against all of them
at the border.
Yeah I mean, I guess we don't blame them for disease, except the governor of Texas just
got finished doing that.
Even though the immigrants have a lower rate than the people of Texas.
We don't say that they overburden law enforcement.
Except that we do.
Despite all statistical evidence to the contrary.
I guess we don't have a police tax on them, but that's only because the government can't
figure out how to do it.
Believe me, if they could figure out how to do it, they would.
We probably don't say that, you know, we don't want lower class people.
We don't want Chinese laborers.
Except that we do.
It's actually policy.
This one's institutionalized.
Immigrants who come legally, they have to show that they're economically beneficial
to the United States.
That one's pretty out in the open.
And I'm sure that's going to be the response.
Using this playbook is okay now, because the Asians that came in the early 1800s, well
they came legally.
They got that permission slip, and therefore it's all different.
I mean, yeah, that's true, they came legally, but I would point out that the federal government
didn't assert control over immigration until 1891.
It was left up to the purview of the states, because there's actually nothing in the Constitution
of the United States giving the power to control immigration to the feds.
But they found a way to do it in 1891.
Shortly after, they lost their permanent underclass due to the Civil War.
I would point out that one of the things I hit on in that timeline yesterday was in 1894,
a Japanese man trying to obtain citizenship, and him being told that he couldn't because
he was neither white nor black.
They needed that new underclass, that new group of people to kick down at, because they
thought they had lost their standby.
No, the playbook is definitely still in use today.
Absolutely.
So much so, and so quite literally today, by the time y'all watch this it will be yesterday,
but the House passed a bill to benefit people who have lived pretty much their entire lives
in the United States.
They may not even remember their home country, the Dreamers.
And it passed in the House, but without significant reform to the filibuster, it probably won't
make it through the Senate, because there are still politicians using this playbook
today.
That group of people over there, they are lesser than you.
You're better than them.
But for me, and I'll keep it that way, it's a good way to energize a base.
It has been used throughout American history, and as much as somebody may want to vehemently
disagree with it, it is still being used today.
Just a new demographic, a new group of people to kick down at.
And undocumented people are not the only people subject to this.
It always gets used, because it's effective, because people keep falling for it, because
they will always find some reason to say, no, this time it's different.
This isn't us.
Yeah, it is us.
Individual people, we may have changed, but the institution of the United States, it is
built on scapegoating people while exploiting their labor.
It's literally our entire history.
It hasn't changed.
Just shifts the group of people being exploited and scapegoated.
This will probably continue, because we have this idea that in order to make America great,
we have to defend the actions of the past.
I'm going to suggest that's not true.
I think that the only way we can actually really achieve the promises that were made
a couple hundred years ago is to acknowledge everything that's happened and make sure it
doesn't happen again.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}