---
title: Let's talk about the most important question from Tucker Carlson....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pbcZTsIXmTY) |
| Published | 2021/03/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responding to criticism from a fan of Tucker Carlson regarding not answering the "only question that matters."
- Explaining the importance of active duty soldiers getting reassignment surgery paid for by the Pentagon.
- Addressing the significance of retention in the military, particularly for trained and experienced personnel.
- Detailing the high costs associated with training pilots and the retention bonuses offered to keep them in the military.
- Providing insights into the Army's challenges in tracking training costs and the value placed on retention.
- Emphasizing the critical role of retaining trained individuals in enhancing readiness and safety.
- Pointing out the intention behind Tucker Carlson's question and the underlying bias against certain individuals in the military.
- Offering a link to a video discussing how soldiers impact readiness.
- Clarifying the statistical insignificance of the issue raised in the video regarding soldiers seeking reassignment surgery.
- Concluding that the most vital question raised actually contributes to increasing readiness by retaining skilled military personnel.

### Quotes

- "The only question that matters apparently is how does active duty soldiers getting their reassignment surgery paid for by the Pentagon, how does that make the country safer?"
- "It helps keep the country safe, which really, that just means it increases readiness, by keeping trained and experienced people in the military."
- "The most vital question, like ever, apparently, there's your answer."

### Oneliner

Beau responds to criticism, educates on military retention, and clarifies the real importance behind a provocative question by Tucker Carlson.

### Audience

Journalists, Activists, Military Personnel

### On-the-ground actions from transcript

- Contact military recruiters to understand the importance of recruitment and retention (implied)

### Whats missing in summary

The full transcript provides a detailed explanation on the importance of military retention and how it contributes to readiness and safety in the country.

### Tags

#Military #Journalism #Retention #TuckerCarlson #Readiness


## Transcript
Well howdy there internet people, it's Beau again.
So today, we're going to talk about the only question that matters.
I got a rather colorful message from a fan of Mr. Carlson's.
And he had issue with the way that I responded to Mr. Carlson.
And it's full of a bunch of stuff that I just would not say on this channel.
A bunch of stuff that's just not really suitable for public consumption.
But in relevant part, it says that I didn't answer Tucker's most important question.
He said it was the only question that matters and you didn't answer it.
You failure of a journalist.
Well allow me to retort.
First I want to point out that Tucker's job as a master journalist is to answer questions,
not ask them.
But since this isn't exactly cracking Watergate, sure I'll do his job for him.
The only question that matters apparently is how does active duty soldiers getting their
reassignment surgery paid for by the Pentagon, how does that make the country safer?
That's the all important question right there.
Okay Tucker, since you seem to have an affinity for flight suits, we'll start there.
The cost to train an F-16 pilot is $5.6 million.
For an F-22 it's $10.9 million.
Holy cow, that's like really expensive.
It would be bad if they got that training and then left.
We probably have all sorts of benefits that encourage them to stay, one of those being
health care.
That's why.
It's about retention.
Once somebody is trained, you don't want them to walk out the door.
Now to put emphasis on how important this is to the military and how it keeps the country
safe, I want to talk about the retention bonuses for a second.
Now for the Air Force, the most recent numbers I have are from 2019.
But the retention bonuses cap out for pilots, for fighter pilots, bomber pilots, mobility,
spec ops pilots, even unmanned.
They cap out at $420,000 if they sign on for another 12 years or something like that.
That is money on top of their normal pay and benefits.
$420,000, almost half a million dollars.
Here, take it.
Please stay.
Because it is that important to readiness to have that experience and that training
stay in the military.
Now granted, pilots, they may actually be the most expensive.
I'm not 100% sure on that, but if they're not the most expensive, they're right on up
there.
But let's talk about the Army for a second.
Now the Army is ridiculously bad at keeping track of what stuff costs.
Their estimates for training soldiers, I want to say it's like $15,000, which is really,
really low.
But it doesn't include all of the training that occurs along the way.
Doesn't include the FTXs and stuff like that.
And those can get incredibly expensive.
It only covers that initial burst of training.
And while we don't have really good numbers on the training costs, we know how much value
they put on retention.
Because even in the Army, where the numbers for training are much less, even in the Army
currently, right now, the cap is, I want to say, $80,000 in the Army.
And that sounds like really, really super high.
It's not, because I can remember in 07-ish, people trying to decide whether to stay in
or go private because they were looking at six-figure retention bonuses.
It is that important to keep the training in the military.
So if somebody joins and decides, after getting the training, that they want reassignment,
which I think they call confirmation now, and I could be wrong on that, that they want
the surgery, well, it's a whole lot better to pay for it and keep them in the military.
It makes people safer, keeps the country safer, increases readiness if you do that, than letting
them leave and having to train their replacement and hope they get the same experience along
the way.
This is information that master journalist Tucker Carlson could have obtained by calling
literally any recruiting station and asking why the benefits packages are so good, because
they would answer recruitment and retention.
But let's be honest.
That's not really what this is about, is it?
That's not really why this is the most important question.
It's really, we don't want those people in the military.
I have a video that actually goes through all the studies on how these soldiers impact
readiness, and I will put it down below.
I want to point out, though, that in it, I actually point to something.
I'm like, this is a readiness issue.
Since then, I have found out that even though in that video I make it incredibly clear that
this is not something that happens very often, it's even more rare than I make it seem in
that video.
If I was making that video today, I wouldn't even bring it up, because it is statistically
insignificant.
So the most important question, like ever, apparently, there's your answer.
It helps keep the country safe, which really, that just means it increases readiness, by
keeping trained and experienced people in the military.
That's not hard to find out.
It's just a question he asks his audience to provoke outrage and make people think that
it's some unanswerable question, as long as they can blame somebody that they can kick
down at.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}