---
title: Let's talk about why Secretary Austin was in India and Raytheon....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fsCdztmtf9w) |
| Published | 2021/03/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Secretary Austin's trip to India aimed at convincing the Indian government not to purchase the Russian S-400 air defense system, but a U.S. design instead.
- Austin, a retired general, had ties to Raytheon, a company that manufactures missile systems.
- The U.S. wants India to standardize its equipment with American-made systems for geopolitical alignment against China.
- The defense industry is often perceived as corrupt, though standardization of equipment among allied nations is common.
- The appearance of conflict of interest arises when a government official with ties to a company advocates for their products.
- Standardization of defense equipment is vital for interoperability and mutual support during crises.
- Despite the perception, the push for standardization may not necessarily be corrupt in this context.
- Perception in foreign policy can overshadow reality, making it critical to avoid any appearance of impropriety.
- Recommendations include having lower-ranking officials handle situations that could be perceived as conflicts of interest to maintain a positive image.
- Conflicts of interest in government appointments can accumulate and tarnish the integrity of decision-making processes.

### Quotes

- "The appearance is that the United States Secretary of Defense is out there hawking missile systems for a company he has ties to."
- "In foreign policy, perception is often way more important than reality."
- "Regardless of the realities, the outside perception, that's not what it looks like."
- "It just looks like good old-fashioned corruption."
- "Those little conflicts of interest, they add up."

### Oneliner

Secretary of Defense pushes U.S. missile systems in India, raising concerns of conflicts of interest and the importance of perception in foreign policy.

### Audience

Government officials, policymakers, citizens

### On-the-ground actions from transcript

- Ensure lower-ranking officials handle situations that could be perceived as conflicts of interest (suggested)
- Advocate for transparency and ethical decision-making in government appointments (exemplified)

### Whats missing in summary

Further insights on the implications of conflicts of interest in foreign policy and defense procurement decisions.

### Tags

#ConflictOfInterest #ForeignPolicy #DefenseProcurement #Ethics #Standardization


## Transcript
Well, howdy there, Internet of People. It's Beau again. So today we're going to talk about
Secretary Austin's trip to India and his agenda and what it was about.
When Austin was confirmed, we talked about two issues with him. Nobody doubted his qualifications,
but there were two issues. One is that he was a recently retired general, which is,
it's actually not good for the Secretary of Defense to be a recently retired general.
And the second was that he had ties to a company called Raytheon that does a whole lot of business
with the Department of Defense. In Austin's favor, he has recused himself of any and all
dealings with Raytheon. He won't have anything to do with the decisions. So what was he doing
in India? Get ready to learn more about air defense systems than you ever wanted to know.
The Indian government is on track to purchase the S-400 air defense system. It is of Russian
design. He was there to convince them to not purchase that and instead purchase one of a U.S.
design. The most likely candidates are the THAAD and the PAC-3, the Patriot. The Patriot is made
by Raytheon. Now the THAAD is made by Lockheed Martin. However, the eyes of the system,
the radar of it, is made by Raytheon. So if India does what the Secretary of Defense wants,
it doesn't matter which system they purchase, a company he has ties to benefits.
The appearance is that the United States Secretary of Defense is out there hawking
missile systems for a company he has ties to. It appears very corrupt. Is it? Probably not. Not
really. It is incredibly common for militaries that are geopolitically aligned to standardize
their equipment. Given the United States' current posture of retooling to face off geopolitically
against China, it is like super important that India is on our team. We want them on the same
page. We want them using our equipment. We want everybody, everybody compatible there.
And that sounds good. People have an image of the defense industry that basically says it's all
corrupt and it's all about money. That image is entirely earned. That is a well-earned image.
So are there examples of this kind of standardization occurring where money isn't a
motivating factor? Yes. Tons. The easiest one that I can point to is the fact that I could put you in
a room with the standard infantry rifle from the Canadian military, the British, the French,
the Belgian, the German, Austrian, U.S., Australia, everybody who is geopolitically aligned.
One bullet will chamber in all of those rifles. We learned it during World War II. You had a
bunch of different militaries cooperating, a lot of them using different types of equipment,
and it wasn't always compatible. So we standardized. To take it completely out of
the capitalist world, the Soviet Union and Warsaw Pact did the exact same thing. It really is that
important. This is probably not corruption. In this scenario, having everybody using the same
air defense systems is a good idea. One, we have extra projectiles. Two, if something happened
and U.S. forces had to help India, they would already know how the equipment works.
So it's not corrupt, more than likely. However, the appearance is that the current Secretary of
Defense is out there hawking missile systems for the company that he has ties to. As we have talked
about before, in foreign policy, perception is often way more important than reality,
and that is what it looks like. It would probably be a smart move for the Undersecretary to handle
anything else that comes up like this, because this isn't a good look. This is not a perception
the United States needs, because regardless of the realities, and people in the military,
or people who are familiar with the military, they're going to know what's going on. But the
outside perception, that's not what it looks like. It just looks like good old-fashioned corruption.
It's something that we should probably avoid, and this is why, when we're looking at candidates,
those little conflicts of interest, they add up. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}