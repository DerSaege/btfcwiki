---
title: Let's talk about the Tennessee GOP's heroes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WTMtllnaVzE) |
| Published | 2021/03/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau, a Tennessee native, expresses his opinion on the Republican Party's plan to terminate the historical commission in Tennessee.
- The historical commission decided to remove a statue of Nathan Bedford Forrest from Capitol grounds, prompting claims of cancel culture from the Republican Party.
- Beau acknowledges Forrest's historical significance but questions his deservingness of a prominent statue due to his controversial actions during the Civil War.
- Beau suggests that monuments to Forrest's legacies already exist in Tennessee, like the Fort Pillow park where Forrest's troops committed atrocities during the Civil War.
- He believes there are better heroes in Tennessee more deserving of being honored in the Capitol than Forrest.
- Beau expresses disappointment in the Republican Party's stance on keeping the Forrest statue, feeling that it doesn't represent the views of the people of Tennessee.

### Quotes

- "There are better heroes. There are people more deserving of being in that spot."
- "His legacy is secure. He will be remembered, no doubt."
- "I'm appalled that the Republican Party has a problem with removing it."
- "Maybe I'm wrong. Anyway, it's just a thought."

### Oneliner

Beau from Tennessee questions the Republican Party's defense of Nathan Bedford Forrest's statue, suggesting better heroes for Capitol grounds. 

### Audience

Tennessee residents

### On-the-ground actions from transcript

- Advocate for the removal of controversial statues in your community (suggested)
- Support the recognition of better heroes who unite rather than divide (implied)

### Whats missing in summary

Full context and emotional depth can be experienced by watching the entire video.

### Tags

#Tennessee #NathanBedfordForrest #CivilWar #HistoricalCommission #Monuments


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some news coming out of the great state of Tennessee.
If you don't know, I grew up on the Tennessee-Kentucky line.
I have a whole lot of fond memories of Tennessee.
And I definitely have an opinion about this.
The Republican Party there has come up with a plan to end the employment of the entire
historical commission because they reached a conclusion that the Republican Party doesn't
agree with.
And as I say, Republican Party, I do want to acknowledge that there are a handful, not
many, but a handful of Republicans in Tennessee that have the exact same face I do.
The historical commission decided to remove a statue from Capitol grounds.
This has prompted claims of cancel culture from the Republican Party.
Say that they don't want to erase it.
They don't want to lose that piece of history.
The statue, it's of Nathan Bedford Forrest.
If you don't know who that is, hit up your favorite search engine.
I am surprised a statue of him is still prominently displayed.
I'm appalled that the Republican Party has a problem with removing it.
From a history standpoint, sure, I get it.
He's an important historical figure.
He shaped the southern United States for like 150 years.
I get it.
However, I don't know that he really deserves a place of honor in the Capitol, and I would
suggest that if you want monuments to his legacies, they already exist.
See in the state of Tennessee, there's a park called Fort Pillow.
During the Civil War, there was some, let's just call it unpleasantness at Fort Pillow.
Forrest had his troops open fire on surrendering, fleeing, and wounded American warriors.
The incident was so outrageous that Lincoln kind of said, do it again and find out.
Issued this proclamation that was basically an eye for an eye.
Said that anything that was done to a surrendering American soldier, well, they were going to
do it to a southern POW.
So if you want a monument to his military legacy, it already exists in Tennessee.
And if you want a monument to his activities after the war and to the organization that
he championed in its infancy that created the structural racism all over the southern
United States, I would say those monuments exist too.
At the final resting places of David Jones, Joseph Reed, Bill Gilmer, Eliza Woods, Amos
Miller, Jim Taylor, Tom Moss, Calvin McDowell, this list goes on.
And there are like monuments outside of Tennessee too, just like it, all over the south.
Yeah, maybe his organization, the one that he championed in its infancy, the one that
he definitely led to become the organization that we all know, maybe they weren't responsible
for every single one of them, but they were certainly responsible for the attitude.
Those monuments exist.
His legacy is secure.
He will be remembered, no doubt.
We don't need a bust of him in the Capitol.
I would suggest that the people of Tennessee have given a lot to this country.
A lot.
There are better heroes.
There are people more deserving of being in that spot.
There are people who would unite rather than divide.
The thing about it is I don't even understand it.
It's appalling that this statue is still there.
It's not going to be a politically popular opinion to keep it.
It's a personal one.
Because I am fairly certain that the people of Tennessee don't want that.
Don't want that representing their state.
Maybe I'm wrong.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}