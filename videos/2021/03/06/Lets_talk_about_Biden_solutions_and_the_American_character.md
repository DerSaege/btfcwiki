---
title: Let's talk about Biden, solutions, and the American character....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=z52Yj1wkSIs) |
| Published | 2021/03/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden's leaked plan aims to reform the failed American asylum system.
- The plan involves asylum seekers being processed and sent to their planned destinations with sponsors.
- Beau supports the focus on processing rather than detaining asylum seekers.
- Raises the question of why previous administrations didn't implement similar solutions.
- Criticizes those creating a crisis at the border, stating it just requires planning.
- Believes Americans excel at handling chaos and crisis due to their confidence and innovation.
- Challenges the notion of a line of people coming to the U.S. as a fundamental issue.
- Argues that the American character is characterized by the confidence to deal with any challenge.
- Emphasizes that every demographic that has come to America has made the country stronger.
- Calls for addressing real issues like climate change, systemic racism, and lack of leadership.

### Quotes

- "Do they actually have a real tangible common sense solution to whatever issue it is that they are campaigning on, or do they just have cool talking points?"
- "The idea that a line of people wanting to come here is somehow a major issue is laughable."
- "It is not a crisis, except for maybe a crisis of leadership."

### Oneliner

President Biden's leaked asylum plan prompts Beau to question the lack of previous solutions, criticize the manufactured crisis at the border, and celebrate the American character's ability to innovate and tackle challenges confidently.

### Audience

Advocates for change

### On-the-ground actions from transcript
- Address real issues like climate change and systemic racism (implied)
- Focus on finding solutions and putting in effort to lead (exemplified)

### Whats missing in summary

The full transcript offers deeper insights into the need for genuine solutions over political rhetoric and the importance of addressing real issues like climate change and systemic racism.

### Tags

#AsylumReform #AmericanCharacter #CrisisManagement #Leadership #RealSolutions


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about President Joe
Biden's plan being released a little bit earlier than he might have liked. We're going to talk
about that, we're going to talk about solutions, and we're going to talk about the American character.
We're going to do this, in case you don't know, President Biden's plan to fundamentally change,
alter, transform, reform, fix the completely and utterly failed American asylum system got leaked.
It appears that this was something they were trying to do kind of under the radar, better to
ask for forgiveness than permission kind of thing. A rough sketch of it, and this may sound familiar
to some of you, is that asylum seekers would be allowed to come here, they would have contact with
Border Patrol, they would claim asylum. Border Patrol and Health and Human Services would kind
of check them out, they'd get a health check up and a background check, and then they would be
sent on their way to the destination they had planned to go to, where their friends or family
would kind of act as sponsors until their paperwork was processed. They would focus
more on processing than detaining people. Sounds like a good idea. I'm fully in support of this.
I wish I had thought of something like that. Now, to be clear, the only tangible difference I could
find between what we discussed a couple weeks ago and Biden's plan is that I had intended on
just allowing people to make their own way from the processing center to their final destination,
whereas the Biden administration wants to team up with non-profits to make sure they get bus tickets
or whatever. If this plan gets enacted, if this really becomes a thing, we will fundraise
to help with those tickets. That is a wonderful idea. Now, while I do find the jokes funny and
the image of Biden in the cabinet sitting around watching these videos, I do think that's
entertaining. In the interest of honesty, I do feel I need to point out that the whole theme of
that video was that literally anybody could come up with a better plan than the way we were doing
it, and that anybody who had a base understanding of the issue and a basic understanding of logistics
would come up with the common sense solution of processing people rather than detaining them and
costing the American people billions. That was the whole point. I don't necessarily believe that
somebody on the Biden team is watching this channel, and I don't think that should be the
question. I don't think we should be asking, is somebody in the Biden administration watching
this? I think we should be asking, why has no administration that came before come up with this?
This issue has been here a while, and this solution was here the entire time.
Why didn't it get enacted? That's something we might want to keep in mind as we look at future
candidates. Do they actually have a real tangible common sense solution to whatever issue it is
that they are campaigning on, or do they just have cool talking points? Beyond that, I would suggest
we start trying to figure out whether or not they actually want to create a solution, solve the
problem, or if they want to manufacture and create a crisis. Because right now you have a whole bunch
of Republicans pointing at the border saying that it's a crisis. It's a line of people who want to
come in. That's not a crisis. That's something that happens every day at Disney World. It just
needs a little bit of planning. The funny part is, the biggest offenders of this, the people who do
this the most, are those who like to draw some lineage back to the great Americans of old and
pretend that they're the sons of the pioneers or whatever. I'm going to suggest that the great
Americans of old would not look at the border and see a crisis. I'm willing to bet they wouldn't
even be surprised. I mean, our whole marketing here is that we're the greatest country on the
planet. Of course a whole bunch of people want to come here. That just stands to reason, and I don't
think that they would feel it was a major issue. It would probably be something they solved over
drinks on a Tuesday afternoon when they had nothing else to talk about, because it's not
a crisis. It's just something that needs a little bit of planning. That's it. I would suggest that
anybody who is claiming that a line of people wanting to come here could somehow bring America
to its knees fundamentally misunderstands the character of the American people. We do a lot
on this channel to get rid of a lot of American mythology, but there is one thing that we
can't take away from the American people, and that is that they are supremely confident in their
ability to deal with anything at any time. Chaos, crisis, that's our thing. That's what we do.
That's what we excel at. Most times we don't feel like we have enough on our plate. Yeah, I heard
there's that whole Civil War thing going on, but maybe we should build a transcontinental railroad
too. Yeah, I know we're deep in Vietnam, but let's still go ahead and put a man on the moon.
That's the character of the American people. They look for solutions. They look to innovate.
They feel that they can handle anything. They're confident in that. It's not even confidence.
It's outright arrogance. The idea that a line of people wanting to come here is somehow a major
issue is laughable. It's only an issue because nobody wants to solve the minor problems that
may arise. It is not a crisis, except for maybe a crisis of leadership.
The other thing I want to point out is that while not everybody got a fair shake,
it didn't go smoothly, and overall there were a lot of demographics that had a rough time.
At the end of the day, every demographic that came to this country made it stronger, made it better.
There is no exception to this statement. This group of people will be the same way.
I would suggest that the great Americans of old would not be trying to manufacture a crisis.
They would be looking at the actual issues we have. If they had the morals and the standards
of the people of today, they would be addressing things like climate change, systemic racism,
the actual issues we have in this country. They wouldn't look at climate change and say,
hey, you know what? I mean, I understand we have a whole lot of research and information on this,
and we could probably do a whole lot to mitigate it, but it just seems really hard. We'll just
leave that for our kids to deal with. In fact, I'm going to tell you a story about a group of
great Americans of old who have a quote about that. Something to the effect of, if trouble comes,
let it come in my day. I think that's what they would be like. I don't think they would be trying
to manufacture a crisis where there's really not one. It's just a lack of leadership. It's a lack
of leadership. It's a lack of planning. It's a line of people who want to cross. That's it. That's
all it is. The only holdup is a lack of planning and a whole bunch of people who, well, let's be
honest, there's a little bit of racism here too. We don't want to solve the problem because we
don't want them to come in, right? I think that's probably true for a large group of people who are
complaining about this. But, pointing it out again, they will make this country stronger,
like every other demographic that has come here. I think we should probably focus on people who
want solutions. People who are willing to put in the time and the effort to lead,
rather than those that focus on talking points. Now, on the off chance that there is somebody
from the Biden administration watching this channel, good. Now do healthcare, green
infrastructure, the minimum wage, and address systemic racism. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}