---
title: Let's talk about Fox News being sued for $1.6 billion....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7iLcKXmEc6M) |
| Published | 2021/03/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Fox News is hit with a 1.6 billion dollar lawsuit for defamation by a company featured in their coverage post-election.
- The company accuses Fox of promoting false claims to boost ratings, leading to multiple lawsuits filed against them.
- Future lawsuits may target political figures and smaller personalities who echoed baseless theories.
- The typical defense of claiming no reasonable person believes what is said as fact may not work for Fox due to branding themselves as news.
- Settlement rather than a prolonged case may be an option for Fox News.
- This marks the beginning of financial accountability for those spreading baseless claims.
- Some public officials still use baseless claims to influence elections, particularly in Georgia.
- American people should pay attention to politicians promoting such claims and hold them accountable.
- Certain political figures should retire due to their actions causing long-term damage.
- It's vital for Americans to not overlook the actions of these politicians.

### Quotes

- "It's the consequences of my own actions coming up to pay a visit."
- "This is the beginning of financial accountability for those who pushed these wild, baseless theories and claims."
- "American people should pay attention to any political figure who leans into that, specifically those in Georgia."
- "There's going to be a whole lot of political figures who really should become retired because of their actions."
- "It's not something the American people should forget."

### Oneliner

Fox News faces a 1.6 billion defamation lawsuit for spreading false claims, marking the start of financial accountability for baseless theories, urging Americans to hold politicians promoting such claims accountable.

### Audience

Media consumers, political activists.

### On-the-ground actions from transcript

- Hold politicians promoting baseless claims accountable by staying informed and vocal (implied).
- Support accountability movements against spreading false information (implied).

### Whats missing in summary

The emotional impact and detailed consequences of baseless claims spreading.

### Tags

#FoxNews #DefamationLawsuit #BaselessClaims #PoliticalAccountability #MediaResponsibility


## Transcript
Well howdy there internet people, it's Bo again.
So today, we're going to talk about the very high likelihood
that right now over at Fox News,
there's a whole bunch of people sitting around
going well, well, well,
it's not the consequences of my own actions
coming up to pay a visit.
If you haven't heard the news yet,
Fox has been hit with a 1.6 billion,
that is with a B, dollar lawsuit for defamation.
It was filed by a company who was featured very prominently
in their coverage after the election.
The company is basically saying that Fox promoted
and echoed a bunch of false claims
in an effort to boost their failing ratings.
This company, who I'm not naming
because they're suing everybody,
this is the latest in a string of suits filed by them
because they were named, they were villainized
by a whole bunch of baseless theories
and they suffered because of it.
There's a string of lawsuits.
This is just the most recent.
We talked about another one with the lawyer recently.
I have a feeling there are going to be a lot more
and some of them are going to be political figures
rather than media personalities.
But I would also expect for this company
to kind of work its way down.
They're starting with the larger companies,
those that did the most damage by their way of thinking,
and then they're gonna work down the list.
They're going to, I would imagine,
go after smaller personalities who echoed these claims,
who promoted these baseless theories,
and they're going to attempt
to hold them financially responsible for that.
Now, normally, today's go-to defense
if you're hit with something like this is to say,
well, no reasonable person would believe what I say is fact.
That's the go-to defense, and it's been used a lot lately.
It is in the filings from the lawyer.
I don't know that that's gonna work with Fox News
because they brand themselves as news,
and they have put a lot of effort
into casting themselves as fair and balanced.
We report, you decide.
The most trusted name in news.
They've put a lot of effort into convincing people that,
yes, in fact, you can accept what we say is fact.
I haven't seen all of the specifics of the lawsuit yet,
but I would find it very hard to believe
that that defense is going to be effective here.
It would not surprise me if we find out
that Fox News decides to settle this case
rather than allow it to linger.
It's an interesting development.
This is the beginning of financial accountability
for those who pushed these wild, baseless theories and claims.
I want to point out that at the same time this is happening,
you still have people in public office
using these wild, baseless claims to enact an agenda
designed to suppress the vote,
designed to influence the next election.
I think that's worth noting,
and I think it's worth paying close attention
to any political figure who leans into that,
specifically those in Georgia.
I think that might be something
that the American people need to do.
There's going to be a whole lot of political figures
who were active in the last couple of years
who really should become retired,
who need to get out of political life
because of their actions, because of what they did,
and because of the long-term damage that they caused,
and some are still causing.
It's not something the American people should forget.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}