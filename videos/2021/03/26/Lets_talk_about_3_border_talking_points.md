---
title: Let's talk about 3 border talking points....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_yyMdFPNQO0) |
| Published | 2021/03/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Groups in the South use arm bands to identify individuals who have paid and obtained permission to travel through their territory, sparking moral outrage among Americans.
- He points out the moral similarity between this practice and the actions of the US, despite technical differences.
- Republicans emphasized the financial aspect, claiming these groups make $500 million annually, questioning the narrative that these people are poor and a threat to the economy.
- Democrats' response of "don't come" is criticized as ineffective, akin to telling someone in a burning house not to leave as fire departments are on the way.
- Beau mentions the need for the US to address the problems it created in Central America and expedite processing for those who have already arrived.
- There is mention of efforts to improve processing, such as deals with hotels and potential transfer to northern border facilities for faster processing.
- The distinction between asylum seekers, migrants, and unaccompanied minors is discussed, along with the impact of Senator Biden's bill from 2008.
- While standards for treatment have slightly improved, more progress is needed to meet the necessary standards.
- Beau notes the increasing number of unaccompanied minors due to the situation at the border, with families sending children alone for safety.
- He expresses skepticism about the effectiveness of simply telling people not to come and stresses the ongoing monitoring of the situation.

### Quotes

- "It's kind of the same."
- "Talk is cheap."
- "They're doing what they can."
- "That's what's occurring."
- "Y'all have a good day."

### Oneliner

Americans' outrage over arm bands in the South mirrors similarities in US practices, while political responses and processing challenges at the border continue to unfold.

### Audience

Policy advocates, humanitarian organizations

### On-the-ground actions from transcript

- Monitor and advocate for improved treatment and processing of migrants and asylum seekers (implied)
- Stay informed and engaged with developments at the southern border (implied)

### Whats missing in summary

The full transcript provides in-depth insights into the immigration situation at the southern border and the complex dynamics surrounding it.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about three talking points that have come out in regards
to what's going on down south.
We're going to address each one and see how they kind of all flow together and show the
American perception of what's going on versus the reality.
Okay so the first part is that Americans have apparently just found out that the groups
down there use arm bands, wrist bands, like you find at amusement parks, to identify people
who have paid and obtained permission to travel through the territory they control.
The moral outrage from Americans has just been deafening that this was shocking to a
lot of people.
And I agree, it's horrible.
I think that a group of men with guns demanding to see documentation that somebody has paid
and obtained permission to cross through a territory for harmless travel is pretty immoral.
Think about it as long as you need to.
It's kind of the same.
And I know, you can say no no no, there's differences.
There are differences in the systems.
And I mean yeah, sure, I mean on a technical level, but when you get to a moral level,
there is no difference between that armband, that wristband, and what the US does.
Not really.
I will grant you the technical differences.
One is a well-defined territory ran by mostly unaccountable men who use institutionalized
violence to collect money from the people in that area that are under their control
with the primary motivation of enriching themselves.
And the other is the criminal enterprise down south.
Those guys, they really need to get a flag and a song, because it would change the whole
dynamic, the whole perception.
Morally it's the same thing.
Now the follow-up talking point to this, as soon as Republicans realized that this was
bothering people, they're like, did you know they make $500 million a year off of this
practice?
And that's a low estimate.
It is the low end of accurate, but almost so low it is inaccurate.
But see that doesn't tell me what I think they want it to tell me.
$500 million a year.
More than a million dollars a day just to travel.
I thought these people were poor and going to bankrupt our economy.
Doesn't seem like that's true.
Seems like that's made up.
Seems like it's just a talking point, a propaganda piece made to marginalize people.
What it seems like.
And then we have the Democratic Party saying sternly, don't come.
Yeah, okay, sure, that's going to be effective.
That's about like telling somebody who's in a house that is currently on fire, don't leave
the fire departments on the way.
I don't think that's going to matter much.
It's a political thing.
It's them trying to show that they don't want a giant surge of people.
And they genuinely don't want a giant surge of people.
But that isn't going to do anything.
Talks cheap.
Simultaneously the US has to begin fixing the problems it created in Central America
and processing the people who have already arrived faster.
And they're working on the processing side.
They are working on the processing side.
They have, they're inking deals with hotels.
You watch the video, you can imagine my laughter at that.
They brought in people to speed up processing.
They are talking about flying some of them to the northern US border so they can use
those facilities and process them faster.
I will tell you right now, that type of suggestion that is that costly and that visible, that
occurs when there's a whole bunch of people in a room and the commander walks in and says,
give me anything you've got to fix this.
That's not a politically motivated solution.
That's one that is aimed at just solving the problem as quickly as possible.
The fact that those ideas are being thrown out, they are working on it.
They're doing what they can.
There's a giant backlog and it's going to take time.
I would also point out that there are, there's a mix of terminology.
All asylum seekers are migrants.
Not all migrants are asylum seekers.
And there are two very different sets of rules in place.
And then you have unaccompanied minors as well.
And unaccompanied minors, the reason they're being treated the way they are is because
a senator introduced a bill back in 2008 to protect them, to mandate a certain kind of
treatment.
It was Senator Biden, by the way.
It's, it's just a funny twist of fate that it is actually his bill that is creating his
biggest political headache right now.
The current standards, they have slightly improved since the last time we talked about
it.
Not much, not enough.
They are not up to standards yet, but it's going the right way.
Can it stay at this level and be okay?
No, it has to get better.
They're instituting things to attempt to make that happen.
We have to see how it goes.
The U.S. government is a very unwieldy machine.
So I don't know.
This is something I will continue watching and see how it develops.
But where we are at right now, the number of unaccompanied minors is going to continue
to increase because there are a whole bunch of people who were right on the other side
of the border because of Trump's programs who realize they can get their kids somewhere
safe if they send them alone.
That's what's happening.
They are in that burning house and they're getting their kids out first.
That's what's occurring.
And saying no, don't come, that is not going to do any good.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}