---
title: Let's talk about questions prompted by Black History Month....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IL_cqtIaYJo) |
| Published | 2021/03/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- He was repeatedly asked about the absence of other heritage months, like Irish, Asian, Native, Jewish, and Latino, during Black History Month.
- Explains the origins and formalization of each of these heritage months, pointing out that they already exist in federal law.
- Notes the lack of Latino Heritage Month, but mentions Hispanic Heritage Month, which was formalized into law in 1988.
- Questions the intentions behind the repeated inquiries, suggesting they were not in good faith.
- Emphasizes the importance of Black History Month for black Americans to connect with their history and heritage.
- Acknowledges the lack of cool origin stories for black Americans due to the impact of slavery on their lineage.
- Criticizes those who attempt to diminish the significance of Black History Month.
- Calls for addressing inequality and injustice that have resulted from historical practices.
- Expresses confusion over the desire to detract from something meaningful to others for no valid reason.
- Concludes by wishing a happy Irish-American Heritage Month and Women's History Month.

### Quotes

- "They all exist."
- "The goal wasn't to find anything out. The goal was to try to take away from Black History Month."
- "It's more important for black Americans to be able to look into their history a little bit."
- "I will never understand the desire of wanting to take something away from somebody that obviously matters to them for no reason."
- "Inequality and a lot of injustice that is eventually going to have to be addressed."

### Oneliner

Beau addresses the presence of various heritage months, debunking misconceptions and underscoring the importance of Black History Month amidst attempts to diminish it.

### Audience

Advocates for social justice

### On-the-ground actions from transcript

- Celebrate and uplift different heritage months within your community (suggested)
- Educate others about the significance of heritage months and the importance of recognizing diverse histories (suggested)
- Challenge misconceptions and misconstrued narratives surrounding heritage months (implied)

### Whats missing in summary

In-depth exploration of the impact of systemic inequality and historical injustices on marginalized communities.

### Tags

#HeritageMonths #BlackHistoryMonth #Inequality #SocialJustice #CommunityAwareness


## Transcript
Well howdy there, Internet people.
It's Beau again.
So today we're going to talk about something
I've been holding for a while.
Haven't talked about it yet because of the timing.
Throughout the month of February,
I was asked the same questions over and over and over again
by mostly the same people.
Now the first time the questions popped up,
I assumed they were honest questions, so I answered them.
But as they started to come up more often,
I realized that they weren't really
being asked in good faith.
And that the goal wasn't to actually get
an answer to the question.
The goal was to take away from the events of February,
to diminish Black History Month.
That was the goal.
Once I figured that out, I stopped engaging
because I didn't want to play along with it.
I certainly wasn't going to talk about it in February.
It is now March.
So let's talk about it.
Why don't we have these other months, specifically
the five I was asked about?
Irish, Asian, Native, Jewish, and Latino.
Why don't we have Heritage Months for them?
It's a good question.
It's a good question.
I understand why people would be confused.
So we'll go one by one.
The reason we don't have an Irish-American Heritage Month
is because its roots run back to 1762,
before the United States even existed.
It came into its own in 1812 and was formalized
into federal law in 1990.
We do have that.
It exists.
It's a thing.
Asian and Pacific American Month that has its origins in 1977
also formalized into federal law in 1990.
That exists as well.
Native American Heritage Month has its roots in 1982.
It started as a week, just like Black History Month,
formalized into law in 1990.
Jewish American Heritage Month has its origins in 1980,
formalized as a week in 1987, formalized as a month in 2006.
You do have me on Latino Heritage Month.
That doesn't exist.
That's not a thing.
However, using the language that was prominent at the time,
accurate or not, in 1968 lies the roots of Hispanic Heritage
Month, which was formalized into law in 1988.
You are 0 for 5.
They all exist.
They all exist.
And that's kind of proof that they weren't good faith
questions, right?
Because if you cared about having these months,
you would probably know that they already exist.
Not just exist in tradition, but are actually federal law.
There are laws on the books about these months.
And to be clear, there are other months as well.
Some unofficial, still in their origin phase,
and some formalized.
These were just the five I was asked about.
And every single one of them already exists.
The goal wasn't to find anything out.
The goal was to try to take away from Black History Month.
Because black folk in the United States,
they haven't had enough taken away from them, I guess.
And then we have the obvious follow-up question.
Well, why is Black History Month made such a big deal out
of when the other ones aren't?
I mean, I would start with some of them
actually are kind of big things.
And then the second reason would be they care.
Enough to know it exists, I would start there.
And it's more important for black Americans
to be able to look into their history a little bit.
It's funny that this was asked here,
because the single most watched video on this channel
is on this topic.
They don't have the cool origin story.
They can't be like, you know, my granddad, yeah,
he left Ireland in 1916, right before the Reddish
were going to put him on the wall.
They don't have that.
The institution of slavery ripped that from most of them.
A lot of them don't have the ability
to trace their lineage back to the old country the way
that we do.
So they have to rely on their history, their heritage,
here in the new world.
And see, we just wrote them out of that.
We didn't include it.
We didn't put that in the books.
Eventually, we realized that was a mistake.
So we have Black History Month.
It's important to them.
It's important to us, too.
But I think that's probably a lesson that
might be a little bit beyond the people I'm talking to right
now.
I will never understand the desire
of wanting to take something away from somebody that
obviously matters to them for no reason.
So you can continue to play the victim
while also pretending to be superior.
We're eventually going to have to address all of this.
You can put it off as long as you want to.
But there's a lot of stuff that we're
going to have to deal with.
Because all of these practices that still go on today
have led to a lot of inequality and a lot of injury
and a lot of suffering.
Inequality and a lot of injustice
that is eventually going to have to be addressed.
It's going to have to be corrected,
or it will continue to cause problems.
I will never understand the fascination
with blaming those who have less institutional power, less
influence for your problems.
Never understand the idea of kicking down
for no good reason.
Blame anybody who's different, right?
Anyway, it's just a thought.
Happy Irish-American Heritage Month and Women's History
Month.
That's March 2.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}