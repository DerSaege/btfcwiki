# All videos from March, 2021
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2021-03-31: Let's talk about us, them, and how we talk about foreign policy.... (<a href="https://youtube.com/watch?v=BWuai501WG4">watch</a> || <a href="/videos/2021/03/31/Lets_talk_about_us_them_and_how_we_talk_about_foreign_policy">transcript &amp; editable summary</a>)

Be aware: Foreign policy talks often blur the line between government actions and the people, urging recognition for global peace.

</summary>

"We have more in common with the average person in Dublin or Kabul than in most cases we do with our own representatives."
"If enough people recognize this, recognize the similarities, we can move towards accommodating those universal wants which will lead us to peace."
"The Irish, we're not actually talking about the people."
"It's not going to change the language of foreign policy."
"We need to be very conscious of that team mindset."

### AI summary (High error rate! Edit errors on video page)

Biden administration gearing up for active foreign policy.
Urges acknowledgment of the disconnect between government actions and average citizens in a representative democracy.
Emphasizes the distinction between the government's actions and the general population when discussing foreign policy.
Illustrates the issue using a hypothetical scenario involving Ireland to show the absurdity of collective blame.
Stresses the importance of recognizing that foreign policy decisions are made by a select group in power, not the entire nation.
Points out the danger of holding whole countries accountable for the actions of a few in power.
Notes the common desires for safety and stability among people globally.
Warns against collectivizing entire groups of people in foreign policy discourse.
Compares the need for awareness in foreign policy to historical issues of race collectivization in the United States.
Recommends reading foreign policy articles to see how nations are often homogenized in discourse, despite diverse populations.

Actions:

for global citizens,
Recognize the distinction between government actions and general population in foreign policy discourse (implied).
Read foreign policy articles to see how nations are often homogenized in discourse (implied).
</details>
<details>
<summary>
2021-03-31: Let's talk about Trump being sued by Capitol Police officers.... (<a href="https://youtube.com/watch?v=_hVj316jLkA">watch</a> || <a href="/videos/2021/03/31/Lets_talk_about_Trump_being_sued_by_Capitol_Police_officers">transcript &amp; editable summary</a>)

Former President Trump faces a lawsuit from Capitol Police officers seeking accountability for January 6th, urging Republicans to prevent authoritarian control.

</summary>

"Former President Donald Trump faces a lawsuit by two Capitol Police officers seeking compensation for injuries on January 6th."
"The lawsuit holds Trump responsible for encouraging violence and tensions that led to the events of January 6th."
"Republicans must step up and prevent open authoritarians from taking control of the party."

### AI summary (High error rate! Edit errors on video page)

Former President Donald Trump faces a lawsuit brought by two Capitol Police officers seeking $75,000 in compensation for injuries sustained on January 6th.
The lawsuit holds Trump responsible for the events of January 6th, pointing to his history of encouraging violence and tensions with baseless election claims.
The suit specifically mentions a tweet by Trump on December 19th, which encouraged people to attend the event on January 6th, stating it will be "wild."
The lawsuit is part of a series of civil actions seeking accountability for the events of January 6th.
The Republican Party is still embroiled in a power struggle, with many members following Trump's lead despite the legal challenges he faces.
Beau questions the long-term implications of the power struggle within the Republican Party and the abandonment of democratic ideals by some members.
He urges Republicans to step up and take control of their party to prevent open authoritarians from gaining power.
There is an active effort in the country to undermine foundational elements and promises, requiring Republicans to make a stand.
Beau calls on Republicans to support those facing primary challenges for not supporting Trump and to take ownership of the direction of their party.

Actions:

for republicans,
Support Republicans facing primary challenges for not supporting Trump (implied)
Take ownership of the Republican Party to prevent authoritarian control (implied)
</details>
<details>
<summary>
2021-03-30: Let's talk about contractors complicating the withdrawal.... (<a href="https://youtube.com/watch?v=x6I7qIRb6bU">watch</a> || <a href="/videos/2021/03/30/Lets_talk_about_contractors_complicating_the_withdrawal">transcript &amp; editable summary</a>)

Beau explains the complications arising from U.S. contractors in Afghanistan, urging a focus on finding a stabilizing force over financial concerns.

</summary>

"It shouldn't have been in there, but it is. I mean, it is what it is and it's there."
"The concern should not be about a billion dollars. It should be about finding the stabilizing force to replace us and leave."
"That vacuum can suck us right back in."
"For once, when we are making decisions about defense, can we please forget about the money and the contracting firms?"
"The best move is to get a stabilizing force in and then get out and do it all before that deadline."

### AI summary (High error rate! Edit errors on video page)

The complication of the U.S. withdrawal from Afghanistan due to contractors being included in the deal.
The majority of contractors in Afghanistan are support workers, not combat personnel.
The significance of honoring the deal made, despite it being a Trump foreign policy decision.
The dilemma of paying contractors to stay in violation of the deal or bringing them home.
The importance of finding a stabilizing force to replace the U.S. presence in Afghanistan.
The risks of leaving a power vacuum and the potential consequences.
Emphasizing the need to focus on finding a stabilizing force rather than the financial aspects.

Actions:

for foreign policy advocates,
Contact your representatives to advocate for prioritizing finding a stabilizing force in Afghanistan over financial considerations (suggested).
Join organizations working towards a peaceful transition in Afghanistan (implied).
</details>
<details>
<summary>
2021-03-30: Let's talk about Biden, midterms, jobs, and green infrastructure.... (<a href="https://youtube.com/watch?v=qEbZLj525q8">watch</a> || <a href="/videos/2021/03/30/Lets_talk_about_Biden_midterms_jobs_and_green_infrastructure">transcript &amp; editable summary</a>)

Biden's ambitious infrastructure plan aims to create jobs and achieve carbon neutrality, pushing for bipartisan support while preparing for potential solo action in Congress.

</summary>

"Biden's dream of an FDR-style presidency, it is slowly coming into focus."
"It's going to take this program and a whole lot more like it to make that happen."
"He's open. Congress, Democrats in Congress, are setting the stage to do it by themselves."
"Do they want to come to the table and try to be a part of the next big package that is going to create 32,000 jobs just in time for the midterms?"
"This alone is not ambitious enough to fulfill some of his campaign promises, but it's a good start."

### AI summary (High error rate! Edit errors on video page)

Biden's infrastructure and jobs program includes a focus on wind turbines, set to create a minimum of 32,000 jobs from 2022 to 2030 during the construction phase.
The program aims to have the turbines generate 30,000 megawatts in the short term, expandable to 110,000 megawatts.
Biden's goal is to have the grid be carbon neutral by 2035, requiring building out 70,000 megawatts a year of solar and wind.
While the media praises the program, more initiatives like this are needed to achieve Biden's ambitious goal.
Biden is open to bipartisan collaboration on infrastructure with Republicans, but Democrats in Congress are prepared to proceed without them.
Biden strategically plans to execute these initiatives in summer when he is expected to have significant political capital.
The successful implementation of these plans could give Biden leverage with voters and pressure Republicans to participate in job creation efforts.
The current infrastructure and jobs program is a good start but may not be ambitious enough to fulfill all campaign promises.
Biden may be pushing these initiatives through in stages to gradually achieve his objectives without alarming opposition.

Actions:

for politically active citizens,
Contact your representatives to voice support for Biden's infrastructure plan (implied)
Organize community forums on sustainable energy and infrastructure projects (implied)
</details>
<details>
<summary>
2021-03-30: Let's talk about Biden checking Trump's science homework.... (<a href="https://youtube.com/watch?v=nFVAwkGOFA8">watch</a> || <a href="/videos/2021/03/30/Lets_talk_about_Biden_checking_Trump_s_science_homework">transcript &amp; editable summary</a>)

President Biden forms a task force to scrutinize Trump's science handling, but Beau questions its efficacy, stressing voter responsibility in choosing leaders for a better future.

</summary>

"The biggest safeguard, the biggest safety valve there, is you."
"Rather than looking for people who tell you what you want to hear, perhaps it
would be better to look for people who are interested in making the country and the world better."

### AI summary (High error rate! Edit errors on video page)

President Biden's administration is forming a task force to go over former President Trump's actions regarding science.
Federal agencies have until April 2 to nominate members for this task force.
The task force aims to analyze the past four years and determine what went wrong, how to fix it, and prevent similar mistakes.
It will focus on instances where the former president ignored reality, science, data, and evidence.
Beau questions the effectiveness of the task force, as future presidents could override its policies.
Despite doubts, Beau believes it's worth investigating mixed messaging that impacted the country's response to crises.
He anticipates the task force's efforts being politicized as a way for Biden to capitalize on recent statements by Dr. Birx.
Beau mentions that this task force has been planned for a while, likely accelerated by recent revelations.
He underlines the importance of American voters as the ultimate safeguard against potential misuse of power by future presidents.
Beau suggests choosing leaders focused on bettering the country and the world, rather than those who simply echo desired sentiments.

Actions:

for american voters,
Nominate individuals for the task force before April 2 (suggested)
Stay informed about the task force's progress and recommendations (suggested)
Make informed decisions during elections, prioritizing candidates focused on positive change (implied)
</details>
<details>
<summary>
2021-03-29: Let's talk about Birx, Fauci, Trump, and advising the former president.... (<a href="https://youtube.com/watch?v=Y1y2xX8Zfz4">watch</a> || <a href="/videos/2021/03/29/Lets_talk_about_Birx_Fauci_Trump_and_advising_the_former_president">transcript &amp; editable summary</a>)

Dr. Birx and Dr. Fauci faced criticism for not speaking up earlier about preventable COVID deaths, revealing a troubling dynamic of treating President Trump like a child, ultimately leading to a team-focused mentality causing significant loss of life.

</summary>

"Maybe we should be more focused on the country rather than our team."
"They started treating him like a child."
"That team mentality led to hundreds of thousands of excess loss."

### AI summary (High error rate! Edit errors on video page)

Dr. Birx implied the first hundred thousand COVID deaths were inevitable, but everything after that could have been mitigated.
There was immediate criticism towards Dr. Birx for not speaking up earlier if she knew the severity of the situation.
Dr. Fauci and Dr. Birx adopted a "good cop, bad cop" approach with President Trump, treating him like a toddler due to his perceived lack of understanding.
Some believe the advisors' claims of guiding Trump gently are just an excuse after the fact.
Trump's advisors, not just Birx and Fauci, struggled with the decision-making process, as seen in foreign policy blunders like targeting an Iranian general.
The Pentagon included extreme options in briefings to make other choices seem more reasonable to President Trump.
Trump's tendencies to make wrong decisions led his advisors to treat him like a child, resulting in questionable choices being presented.
Despite criticism, Trump initially chose a less extreme option in a foreign policy decision but later escalated tensions.
Trump's advisors' normalization of his poor decision-making contributed to a team mentality in American politics rather than prioritizing the country's well-being.
Blind support for Trump based on party lines and talking points rather than results may have led to significant loss of life during his presidency.

Actions:

for american citizens,
Question political loyalties and prioritize the country over party affiliations (implied)
Advocate for transparency and accountability from leaders in crisis situations (implied)
</details>
<details>
<summary>
2021-03-28: Let's talk about why peace takes so long after the US shows up.... (<a href="https://youtube.com/watch?v=dhMegh67pU8">watch</a> || <a href="/videos/2021/03/28/Lets_talk_about_why_peace_takes_so_long_after_the_US_shows_up">transcript &amp; editable summary</a>)

Why does it take countries so long to recover from US interventions, especially in diverse, multi-factional settings like Afghanistan, prompting a need for transitioning responsibilities to enable effective recovery efforts?

</summary>

"Why does it take so long for a country to recover from a US intervention?"
"I think that is worth more thought and more discussion than why it takes them so long to recover from us."
"Regardless of whether or not the US continues to have a presence there because right now, we're trying to get out."
"We don't need to have a hand in reasserting the national government."
"We need to stop that now."

### AI summary (High error rate! Edit errors on video page)

Questions why it takes so long for a country to recover and establish control after a US intervention.
Illustrates the challenge through the lens of different factions with varying loyalties and goals.
Compares the establishment of governing rules in the US post-independence to the situation in Afghanistan.
Points out the linguistic, ethnic, and cultural diversity in Afghanistan that complicates the recovery process.
Emphasizes the time required for a nation to reach consensus amidst differing interests.
Suggests that the focus should be on why US interventions lead to government falls rather than the length of recovery.
States that Afghanistan still has a long journey ahead despite ongoing peace negotiations.
Advocates for transitioning the US role in Afghanistan to another nation or coalition for better outcomes.
Argues against prolonging US presence and involvement in reasserting the Afghan government.
Calls for prioritizing relief efforts and transitioning responsibilities to other nations or groups.

Actions:

for policymakers, activists, global citizens,
Transition responsibilities in Afghanistan to another nation or coalition for better outcomes (implied).
</details>
<details>
<summary>
2021-03-28: Let's talk about peace possibilities on the peninsula.... (<a href="https://youtube.com/watch?v=DW4uSd4EGgk">watch</a> || <a href="/videos/2021/03/28/Lets_talk_about_peace_possibilities_on_the_peninsula">transcript &amp; editable summary</a>)

Biden's shift in foreign policy offers a slim glimmer of hope for ending the Yemen conflict, with Saudi Arabia pressured to initiate peace talks amid escalating tensions.

</summary>

"There is reason for hope, which is, I mean, that's a nice change."
"Saudi Arabia is kind of on its heels there."
"For the first time in half a decade, though, peace actually appears possible."
"It doesn't go far enough. It's not a real peace deal. But it's the starting point to get to real talks."
"There is a slim glimmer of hope that that conflict can finally be brought to an end."

### AI summary (High error rate! Edit errors on video page)

Biden administration reviewing Trump-era deals involving equipment supply to certain countries.
Biden administration decided to stop supplying offensive equipment to Saudi Arabia.
Saudis offered a peace deal for Yemen, criticized for not going far enough.
Saudis facing pressure due to lost support from major suppliers like the UAE.
Peace is on the table, but the offered deal seems more like a ceasefire.
Expect short-term escalation before real peace talks.
Possibility of peace in the region for the first time in half a decade.
Conflict in Yemen exacerbated by US support, turning into a Saudi-Iranian proxy war.
Biden's foreign policy aims to end the conflict in the Middle East.
US likely pressured Saudi Arabia to issue the initial peace proposal.
Both sides intensifying efforts to strengthen their positions for negotiations.
Saudi Arabia may end up in a weaker position at the negotiating table.
Slim hope for finally ending the conflict in Yemen.

Actions:

for advocates for peace,
Pressure governments for peaceful resolution (suggested)
Support efforts for genuine peace talks (implied)
</details>
<details>
<summary>
2021-03-27: Let's talk about unconscious bias and friend group diversity.... (<a href="https://youtube.com/watch?v=ZQA3eLm6SvE">watch</a> || <a href="/videos/2021/03/27/Lets_talk_about_unconscious_bias_and_friend_group_diversity">transcript &amp; editable summary</a>)

Beau advocates for diversifying friend groups, warns against tokenization, and stresses the value of genuine, diverse friendships for personal growth and societal benefits.

</summary>

"You can't tokenize people."
"If you do this, your life becomes significantly enriched."
"Accept it. Don't get defensive. They're trying to correct you."
"The big thing that you have to watch out for though is tokenizing people."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Advocates for diversifying friend groups for societal benefits.
Acknowledges hurdles in diversifying friend groups, such as geographic limitations and unconscious bias.
Warns against tokenizing people from different demographics.
Emphasizes the importance of genuine friendships with diverse individuals.
Addresses unconscious biases and the need to overcome them in diversifying friend groups.
Encourages accepting corrections and growth through interactions with diverse friends.
Shares a personal experience of realizing and eliminating biased vocabulary.
Stresses the value of diverse friendships in gaining different viewpoints and understanding social issues.
Mentions the enrichment and societal benefits of having a diverse friend group.
Cautions against tokenization as it can hinder the benefits and worsen the situation.

Actions:

for individuals seeking to diversify their friend groups.,
Make genuine efforts to befriend individuals from different demographics (suggested).
Overcome unconscious biases by acknowledging and addressing them within yourself (implied).
Accept corrections and feedback graciously when interacting with friends from diverse backgrounds (implied).
</details>
<details>
<summary>
2021-03-27: Let's talk about the children coming here.... (<a href="https://youtube.com/watch?v=QoyHXyXB7LY">watch</a> || <a href="/videos/2021/03/27/Lets_talk_about_the_children_coming_here">transcript &amp; editable summary</a>)

Responding to criticism, Beau defends asylum seekers' sacrifices and contributions to building the country, debunking myths about early American immigrants.

</summary>

"Do I think these kinds of people will build this country? Well, yeah. It's kind of the only thing that ever has."
"At the end of the day, they are trying to save their children's lives, trying to save their own lives."
"Somebody who is willing to leave their home country, go to a place where they don't have connections, don't really speak the language, have very little chance of being able to get too far ahead, to me, that's not somebody you turn away."
"They will build this country. They'll be a benefit, just like every group before."
"The mythology that surrounds early American immigrants who came here from Europe, it's not real."

### AI summary (High error rate! Edit errors on video page)

Responding to a message critiquing his views on parents subjecting their children to trips to the US.
Describes the terrible conditions and hardships faced during these trips, likening them to historical European immigration.
Points out the historical correlation where groups enduring the worst trips contribute significantly to building the country.
Argues against demonizing and marginalizing these asylum seekers, stating they are trying to save lives.
Distinguishes between migrants and asylum seekers, advocating for supporting those willing to make sacrifices to build a better future.
Challenges the mythology surrounding early American immigrants and encourages reading more on the topic.

Actions:

for advocates for immigration rights,
Support asylum seekers in your community (implied)
Educate others on the realities and challenges faced by asylum seekers (implied)
</details>
<details>
<summary>
2021-03-27: Let's talk about legislation and the $1000 question.... (<a href="https://youtube.com/watch?v=lp0uOXFBYIs">watch</a> || <a href="/videos/2021/03/27/Lets_talk_about_legislation_and_the_1000_question">transcript &amp; editable summary</a>)

Beau sets conditions for defining gun control terms, advocates focusing on domestic violence prevention over regulating design, and suggests an attainable, data-driven solution for reducing gun violence.

</summary>

"What you are talking about when you are talking about these large incidents, what you're wanting to get rid of is semi-automatic firearms that are chambered somewhere between 5.45 millimeter and 7.62 that accept a detachable magazine that holds more than 19 rounds. That's it."
"I've advocated this for a long time based off personal observation. There's now data. 749 incidents were looked at about 60 percent of them have one thread one common thread, domestic violence."
"If it's about saving lives, that's where you need to look."

### AI summary (High error rate! Edit errors on video page)

Explains the challenge he received from a friend to define what the gun control crowd wants to ban.
Sets conditions for defining the term and sharing an alternative data-driven solution.
Defines the specific firearms the gun control crowd aims to ban: semi-automatic firearms chambered between 5.45mm and 7.62 with detachable magazines holding over 19 rounds.
Points out that focusing on banning these specific firearms may not be effective as manufacturers can adapt and criminals may find alternatives.
Advocates for focusing on prohibiting people with a history of domestic violence from owning firearms based on data showing a link between domestic violence and incidents.
Emphasizes the importance of closing loopholes in existing laws related to domestic violence and firearm ownership.
Suggests that prohibiting individuals with a history of domestic violence from owning firearms is a more effective and actionable solution than regulating firearm design.
Mentions an additional observation about the correlation between people who are cruel to animals and those who commit violent acts.
Encourages focusing on actionable and data-driven solutions that are attainable and likely to receive support.

Actions:

for advocates, policy makers, activists,
Advocate for prohibiting individuals with a history of domestic violence from owning firearms (advocated)
Support efforts to close loopholes in existing laws related to domestic violence and firearm ownership (advocated)
Raise awareness about the correlation between domestic violence and incidents involving firearms (suggested)
</details>
<details>
<summary>
2021-03-26: Let's talk about Fox News being sued for $1.6 billion.... (<a href="https://youtube.com/watch?v=7iLcKXmEc6M">watch</a> || <a href="/videos/2021/03/26/Lets_talk_about_Fox_News_being_sued_for_1_6_billion">transcript &amp; editable summary</a>)

Fox News faces a 1.6 billion defamation lawsuit for spreading false claims, marking the start of financial accountability for baseless theories, urging Americans to hold politicians promoting such claims accountable.

</summary>

"It's the consequences of my own actions coming up to pay a visit."
"This is the beginning of financial accountability for those who pushed these wild, baseless theories and claims."
"American people should pay attention to any political figure who leans into that, specifically those in Georgia."
"There's going to be a whole lot of political figures who really should become retired because of their actions."
"It's not something the American people should forget."

### AI summary (High error rate! Edit errors on video page)

Fox News is hit with a 1.6 billion dollar lawsuit for defamation by a company featured in their coverage post-election.
The company accuses Fox of promoting false claims to boost ratings, leading to multiple lawsuits filed against them.
Future lawsuits may target political figures and smaller personalities who echoed baseless theories.
The typical defense of claiming no reasonable person believes what is said as fact may not work for Fox due to branding themselves as news.
Settlement rather than a prolonged case may be an option for Fox News.
This marks the beginning of financial accountability for those spreading baseless claims.
Some public officials still use baseless claims to influence elections, particularly in Georgia.
American people should pay attention to politicians promoting such claims and hold them accountable.
Certain political figures should retire due to their actions causing long-term damage.
It's vital for Americans to not overlook the actions of these politicians.

Actions:

for media consumers, political activists.,
Hold politicians promoting baseless claims accountable by staying informed and vocal (implied).
Support accountability movements against spreading false information (implied).
</details>
<details>
<summary>
2021-03-26: Let's talk about 3 border talking points.... (<a href="https://youtube.com/watch?v=_yyMdFPNQO0">watch</a> || <a href="/videos/2021/03/26/Lets_talk_about_3_border_talking_points">transcript &amp; editable summary</a>)

Americans' outrage over arm bands in the South mirrors similarities in US practices, while political responses and processing challenges at the border continue to unfold.

</summary>

"It's kind of the same."
"Talk is cheap."
"They're doing what they can."
"That's what's occurring."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Groups in the South use arm bands to identify individuals who have paid and obtained permission to travel through their territory, sparking moral outrage among Americans.
He points out the moral similarity between this practice and the actions of the US, despite technical differences.
Republicans emphasized the financial aspect, claiming these groups make $500 million annually, questioning the narrative that these people are poor and a threat to the economy.
Democrats' response of "don't come" is criticized as ineffective, akin to telling someone in a burning house not to leave as fire departments are on the way.
Beau mentions the need for the US to address the problems it created in Central America and expedite processing for those who have already arrived.
There is mention of efforts to improve processing, such as deals with hotels and potential transfer to northern border facilities for faster processing.
The distinction between asylum seekers, migrants, and unaccompanied minors is discussed, along with the impact of Senator Biden's bill from 2008.
While standards for treatment have slightly improved, more progress is needed to meet the necessary standards.
Beau notes the increasing number of unaccompanied minors due to the situation at the border, with families sending children alone for safety.
He expresses skepticism about the effectiveness of simply telling people not to come and stresses the ongoing monitoring of the situation.

Actions:

for policy advocates, humanitarian organizations,
Monitor and advocate for improved treatment and processing of migrants and asylum seekers (implied)
Stay informed and engaged with developments at the southern border (implied)
</details>
<details>
<summary>
2021-03-25: Let's talk about Korea, that test over the Pacific, and perception.... (<a href="https://youtube.com/watch?v=PqS29hy6Egw">watch</a> || <a href="/videos/2021/03/25/Lets_talk_about_Korea_that_test_over_the_Pacific_and_perception">transcript &amp; editable summary</a>)

Beau examines the international test over the Pacific, revealing the commonality of such tests and the importance of perception over reality in shaping foreign policy, proposing Harris as a potential catalyst for initiating talks with North Korea.

</summary>

"It's only a big deal when North Korea does it."
"This is a cry for attention. This is them sticking their hand out that window and waving. They want to talk."
"She might be the right candidate to do it."
"It's not really even a big deal. We do it all the time and nobody cares because perception is more important than reality when it comes to foreign policy."
"Anyway, it's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addressing the recent international test over the Pacific and the significance it holds.
Detailing the events of the projectile launch, its purpose, and the destination.
Contrasting the international attention given to this test with the regular testing done by various countries.
Emphasizing that such tests are common and not necessarily acts of aggression.
Suggesting that the perception around these tests often shapes foreign policy more than the reality.
Speculating on Vice President Harris's potential role in initiating talks with North Korea.
Expressing hope that Harris's unique approach to foreign policy may lead to positive gains.
Stating that such tests are not as significant as they are often portrayed to be.
Commenting on the importance of perception in foreign policy decisions.

Actions:

for foreign policy analysts,
Initiate talks with North Korea (implied)
Maintain awareness of the commonality of international tests to avoid unnecessary panic or misconceptions (implied)
</details>
<details>
<summary>
2021-03-25: Let's talk about Duckworth, Walsh, and systemic issues.... (<a href="https://youtube.com/watch?v=_fkuvr31rhI">watch</a> || <a href="/videos/2021/03/25/Lets_talk_about_Duckworth_Walsh_and_systemic_issues">transcript &amp; editable summary</a>)

Beau examines the importance of diversity in government and dismantling biases for true equality and equity.

</summary>

"It's almost like requesting diversity is the exact opposite of saying, it's the opposite."
"Once the biases are gone, and I can't wait till we get to that point, when everybody is just people, then yeah, that would be a wild statement for her to make."
"She's making a play to get some equality and equity in this country, to fulfill those promises."

### AI summary (High error rate! Edit errors on video page)

Apologizes for missing a video due to a sudden rainstorm, with animals seeking shelter in the background.
Talks about Tammy Duckworth's demand for diversity in Biden's cabinet, sparking controversy.
Mentions the response from both Democrats and Republicans regarding Duckworth's statement.
Compares requesting diversity to having a preferential bias, discussing the historical biases in the U.S.
Explains the importance of increasing representation for groups facing systemic biases.
Addresses the need to break down barriers for underrepresented groups in the political system.
Analyzes the demographic makeup of the Biden administration and the U.S. Senate.
Emphasizes the necessity of actions and programs to achieve better representation in government.
Stresses the irony of questioning the need for quotas when biases still exist.
Concludes by supporting Duckworth's efforts for equality and equity in the country.

Actions:

for advocates for equality,
Advocate for increased diversity in political appointments (implied)
Support programs and actions aimed at breaking down barriers for underrepresented groups (implied)
</details>
<details>
<summary>
2021-03-23: Let's talk about reasonable Republicans and reasonable people.... (<a href="https://youtube.com/watch?v=0TdNqtJyDVs">watch</a> || <a href="/videos/2021/03/23/Lets_talk_about_reasonable_Republicans_and_reasonable_people">transcript &amp; editable summary</a>)

Beau explains the "Tucker Carlson defense," discussing the impact of wild claims and how the Republican machine trains supporters to reject reality.

</summary>

"No reasonable person could believe the things that I've said."
"Sometimes they were just trying to dupe folks, energize the base."
"The Republican machine has trained its supporters to reject reality."
"Don't look any further into it than what your chosen leader has said."
"They played on patriotism and found the unreasonable."

### AI summary (High error rate! Edit errors on video page)

Explains the "Tucker Carlson defense" where individuals claim they shouldn't be held accountable because no reasonable person could believe their statements.
Mentions Sidney Powell, a lawyer known for making wild claims about the election, and how she's being sued by a company for those statements.
Talks about the impact on individuals who believed these claims, leading to consequences like losing jobs, standing among friends, and even getting arrested.
Points out the blow to one's ego when they're told no reasonable person could believe the claims they supported.
Describes how some claims may have been made in good faith, while others were meant to energize the base without caring about the harm caused.
Criticizes the Republican and conservative machine for training supporters to reject reality and rely solely on sound bites from their chosen leaders.

Actions:

for community members, political observers.,
Fact-check claims and statements before believing or supporting them (implied).
Encourage critical thinking and research among friends and family (suggested).
</details>
<details>
<summary>
2021-03-23: Let's talk about DC statehood, the founders, and a tweet.... (<a href="https://youtube.com/watch?v=fW1NEj_BVNg">watch</a> || <a href="/videos/2021/03/23/Lets_talk_about_DC_statehood_the_founders_and_a_tweet">transcript &amp; editable summary</a>)

Beau dissects the DC statehood debate, addressing historical intent, political motives, and concerns over representation in a democracy.

</summary>

"It's not really that they have a concern with people being represented. They wouldn't have a problem with it. It's that they're low-quality voters."
"I'm fairly certain that the founders might have an issue with a population the size of a little bit bigger than the populations of Virginia and Pennsylvania at the time, not having representation."
"The only thing that the Republican Party currently has in common with the founders is the willingness to deny people the right to vote."

### AI summary (High error rate! Edit errors on video page)

Beau delves into the debate on DC statehood, prompted by a tweet from Senator Mike Rounds of South Dakota.
The intention behind creating Washington DC as a neutral federal jurisdiction was to avoid state influence on the federal government.
Over time, the concerns of the founding fathers have evolved and largely been addressed.
Beau explains the linguistic shift from "the United States are" to "the United States is" as the federal government's authority grew.
While the founders didn't intend for DC to be a state, there's nothing prohibiting it constitutionally.
DC was initially allocated 100 square miles but now stands at 68.3, indicating flexibility in its size.
The debate also touches on the political implications of DC statehood, with a focus on party affiliations in voter registration.
Republicans' opposition to DC statehood is linked to concerns about the political leanings of its residents.
Beau criticizes the Republican Party for potentially denying representation based on residents not voting in their favor.
He accuses the Republican Party of straying from the principles of representative democracy and the republic.

Actions:

for political activists, voters,
Advocate for fair representation and democracy in all aspects of governance (implied)
Support initiatives that uphold equal voice and representation for all citizens (implied)
</details>
<details>
<summary>
2021-03-22: Let's talk about why Secretary Austin was in India and Raytheon.... (<a href="https://youtube.com/watch?v=fsCdztmtf9w">watch</a> || <a href="/videos/2021/03/22/Lets_talk_about_why_Secretary_Austin_was_in_India_and_Raytheon">transcript &amp; editable summary</a>)

Secretary of Defense pushes U.S. missile systems in India, raising concerns of conflicts of interest and the importance of perception in foreign policy.

</summary>

"The appearance is that the United States Secretary of Defense is out there hawking missile systems for a company he has ties to."
"In foreign policy, perception is often way more important than reality."
"Regardless of the realities, the outside perception, that's not what it looks like."
"It just looks like good old-fashioned corruption."
"Those little conflicts of interest, they add up."

### AI summary (High error rate! Edit errors on video page)

Secretary Austin's trip to India aimed at convincing the Indian government not to purchase the Russian S-400 air defense system, but a U.S. design instead.
Austin, a retired general, had ties to Raytheon, a company that manufactures missile systems.
The U.S. wants India to standardize its equipment with American-made systems for geopolitical alignment against China.
The defense industry is often perceived as corrupt, though standardization of equipment among allied nations is common.
The appearance of conflict of interest arises when a government official with ties to a company advocates for their products.
Standardization of defense equipment is vital for interoperability and mutual support during crises.
Despite the perception, the push for standardization may not necessarily be corrupt in this context.
Perception in foreign policy can overshadow reality, making it critical to avoid any appearance of impropriety.
Recommendations include having lower-ranking officials handle situations that could be perceived as conflicts of interest to maintain a positive image.
Conflicts of interest in government appointments can accumulate and tarnish the integrity of decision-making processes.

Actions:

for government officials, policymakers, citizens,
Ensure lower-ranking officials handle situations that could be perceived as conflicts of interest (suggested)
Advocate for transparency and ethical decision-making in government appointments (exemplified)
</details>
<details>
<summary>
2021-03-22: Let's talk about the Tennessee GOP's heroes.... (<a href="https://youtube.com/watch?v=WTMtllnaVzE">watch</a> || <a href="/videos/2021/03/22/Lets_talk_about_the_Tennessee_GOP_s_heroes">transcript &amp; editable summary</a>)

Beau from Tennessee questions the Republican Party's defense of Nathan Bedford Forrest's statue, suggesting better heroes for Capitol grounds.

</summary>

"There are better heroes. There are people more deserving of being in that spot."
"His legacy is secure. He will be remembered, no doubt."
"I'm appalled that the Republican Party has a problem with removing it."
"Maybe I'm wrong. Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Beau, a Tennessee native, expresses his opinion on the Republican Party's plan to terminate the historical commission in Tennessee.
The historical commission decided to remove a statue of Nathan Bedford Forrest from Capitol grounds, prompting claims of cancel culture from the Republican Party.
Beau acknowledges Forrest's historical significance but questions his deservingness of a prominent statue due to his controversial actions during the Civil War.
Beau suggests that monuments to Forrest's legacies already exist in Tennessee, like the Fort Pillow park where Forrest's troops committed atrocities during the Civil War.
He believes there are better heroes in Tennessee more deserving of being honored in the Capitol than Forrest.
Beau expresses disappointment in the Republican Party's stance on keeping the Forrest statue, feeling that it doesn't represent the views of the people of Tennessee.

Actions:

for tennessee residents,
Advocate for the removal of controversial statues in your community (suggested)
Support the recognition of better heroes who unite rather than divide (implied)
</details>
<details>
<summary>
2021-03-21: Let's talk about news about the child tax credit.... (<a href="https://youtube.com/watch?v=_DpvnJSskV4">watch</a> || <a href="/videos/2021/03/21/Lets_talk_about_news_about_the_child_tax_credit">transcript &amp; editable summary</a>)

Beau shares updates on the child tax credit cash, cautioning against banking on July payments due to filing deadline shifts and potential changes in payment frequency.

</summary>

"This is more of a public service announcement than a normal video."
"Be aware that there have been some pretty significant changes that don't seem to be getting any press."
"Just be aware that there have been some pretty significant changes."

### AI summary (High error rate! Edit errors on video page)

Public service announcement about the child tax credit cash in the relief bill.
Child tax credits amount to $3,000 for children over six and $3,600 for those under six.
Initially, news broke that people were to receive monthly checks starting in July.
However, the IRS moved the filing deadline from April 15th to May 17th, potentially delaying the payments.
The final legislation doesn't specify monthly payments, only "periodically," which could change the payment frequency.
There might be a shift from monthly payments to receiving larger sums less frequently, like $600 every three months.
Beau advises against making financial decisions based on expecting this money in July due to the uncertain timeline.
To be eligible for this credit, individuals must have filed for the 2020 tax season.
People who usually don't file taxes may need to do so this year to receive this benefit.
The extension of the filing deadline was aimed at helping small business owners have more time to settle their tax obligations.

Actions:

for taxpayers, parents, individuals eligible for child tax credit.,
File for the 2020 tax season to be eligible for the child tax credit (suggested).
Inform others about the potential changes in payment frequency for the child tax credit (implied).
</details>
<details>
<summary>
2021-03-21: Let's talk about how Republicans manipulate the emotional.... (<a href="https://youtube.com/watch?v=Zay1sV7MVVY">watch</a> || <a href="/videos/2021/03/21/Lets_talk_about_how_Republicans_manipulate_the_emotional">transcript &amp; editable summary</a>)

Beau explains how the Republican Party manipulates public opinion by demonizing marginalized groups to deflect blame for policy failures onto those with no power.

</summary>

"People who have less influence and less institutional power are never the source of your problems. The source of your problems pretty much always comes from the people who have power."
"It's easier to point to a class of people that have already been demonized."
"That immigrant's going to take your vaccine."
"Blame it on people who have absolutely no power."
"The rich guy with all the cookies, one cookie left in the center of the table. That immigrant's going to take your cookie."

### AI summary (High error rate! Edit errors on video page)

Explains how those in power provoke emotional reactions to divert blame onto marginalized groups.
Responds to a viewer request regarding the Republican Party's tactics of demonizing and blaming marginalized groups.
Provides an example of Republican senators requesting information on vaccinated migrants to manipulate public opinion.
Points out the dual tactics the Republican Party will use regardless of the information received: vaccine distribution criticism or disease fear-mongering.
Raises the irony of senators downplaying public health suddenly showing concern to provoke outrage against demonized groups.
Notes how blaming marginalized groups diverts attention from the government's failure in managing public health crises.
Emphasizes the strategy of directing anger towards marginalized groups rather than those in power to scapegoat them.
Illustrates how marginalized groups are unfairly targeted and scapegoated despite having no decision-making power.
Stresses that problems often stem from those in power using emotional and divisive rhetoric to shift blame.
Concludes with a metaphorical comparison of blaming immigrants for problems to the idea of immigrants taking vaccines.

Actions:

for viewers,
Challenge demonization of marginalized groups (implied)
Advocate for accurate information sharing (implied)
</details>
<details>
<summary>
2021-03-21: Let's talk about Ted Cruz and the For The People Act.... (<a href="https://youtube.com/watch?v=j0a6IF0VG1A">watch</a> || <a href="/videos/2021/03/21/Lets_talk_about_Ted_Cruz_and_the_For_The_People_Act">transcript &amp; editable summary</a>)

Beau analyzes Ted Cruz's opposition to HR 1, revealing Republican fears of losing without undemocratic tactics, urging Democrats to eliminate the filibuster for its passage.

</summary>

"It appears that Senator Cruz is saying, unless the Republican Party can gerrymander and suppress the vote, they can't win."
"The Republican Party is lost without the ability to deny the people their voice."
"Republicans can't win and won't be able to for the next century if this passes."
"The Republican Party is actively attempting to undermine the representative democracy we have in the United States."
"It's how it appears."

### AI summary (High error rate! Edit errors on video page)

Talking about HR 1, the For the People Act, in response to statements made by Ted Cruz against it.
Ted Cruz expressed the Republican Party's intention to stop HR 1, claiming it aims to ensure Democrats maintain control for the next century.
Contrary to Cruz's claims, HR 1 includes provisions to combat partisan gerrymandering, increase voter access, and reform campaign finance.
Beau interprets Cruz's opposition to HR 1 as a tacit admission that Republicans can't win without gerrymandering and voter suppression.
Republicans in state legislatures are using Trump's baseless claims to tighten voting regulations, hinting at a strategy to suppress votes.
Cruz's remarks imply that Republicans believe they can't succeed without denying people their voice through undemocratic practices.
The opposition to HR 1 by Republican senators necessitates Democrats to eliminate the filibuster in order for it to pass.
Beau criticizes the Republican Party for prioritizing culture wars over policy development and undermining representative democracy.
He suggests that the Republican Party's reliance on divisive tactics is a threat to the United States' democratic system.

Actions:

for activists, voters, legislators,
Rally support for HR 1 among your community and representatives (suggested)
Advocate for eliminating the filibuster to ensure the passage of HR 1 (suggested)
</details>
<details>
<summary>
2021-03-20: Let's talk about Dershowitz's legal take on and Floyd.... (<a href="https://youtube.com/watch?v=zfo0XwIMBvc">watch</a> || <a href="/videos/2021/03/20/Lets_talk_about_Dershowitz_s_legal_take_on_and_Floyd">transcript &amp; editable summary</a>)

Beau questions the logic behind Dershowitz's defense strategy, providing examples to challenge its validity, while also expressing skepticism towards changing the trial venue and cautioning against teaching the jury too much about due process.

</summary>

"Normally if something is true you can take it out of the context it's being presented in and put it in something else very similar and it would still be true."
"I'm pretty sure that's still murder."
"I don't think this is a strong defense."
"It seems to me like he wants to move on to a different country."
"Certainly a bold legal strategy."

### AI summary (High error rate! Edit errors on video page)

Critiques Dershowitz's theory on suggesting previous actions of the cop as evidence in defense of Floyd's death.
Questions the logic behind the defense, giving examples of murder scenarios.
Recalls a prediction from a viewer about the defense strategy, referencing the "skull egg theory" used in personal injury cases.
Disagrees with the defense strategy and its application to the case.
Disputes the idea of changing the trial venue due to events related to the incident.
Shares personal rural living experience to counter the argument for a venue change.
Comments on the defense possibly teaching the jury too much about the concept of due process.
Concludes with a thought on the defense strategy and wishes the audience a good day.

Actions:

for legal observers, activists,
Analyze and question legal defense strategies (implied)
Stay informed and engaged in legal proceedings (implied)
</details>
<details>
<summary>
2021-03-19: Let's talk about the playbook being used against Dreamers.... (<a href="https://youtube.com/watch?v=w-G2_Hbq2iQ">watch</a> || <a href="/videos/2021/03/19/Lets_talk_about_the_playbook_being_used_against_Dreamers">transcript &amp; editable summary</a>)

Beau questions the progress of the United States, drawing parallels between historical discrimination and current policies towards immigrants, urging acknowledgment of past injustices to move forward.

</summary>

"Have we really come that far?"
"This isn't us. Yeah, it is us."
"The institution of the United States is built on scapegoating people while exploiting their labor."
"It hasn't changed. Just shifts the group of people being exploited and scapegoated."
"The only way we can actually really achieve the promises that were made a couple hundred years ago is to acknowledge everything that's happened and make sure it doesn't happen again."

### AI summary (High error rate! Edit errors on video page)

Receiving a message critiquing his historical conclusions on America.
Expressing disbelief in the idea of significant progress in the United States.
Questioning whether the same discriminatory playbook is still in use today.
Drawing parallels between past discriminatory practices and current attitudes towards undocumented workers.
Mentioning the subtle ways discriminatory policies are implemented today.
Bringing up issues like public school access for undocumented workers and scapegoating them for various problems.
Referencing past instances of justifying reprisals against undocumented individuals based on isolated incidents.
Pointing out how immigrants are blamed for various societal issues despite statistical evidence.
Mentioning the lack of a direct "police tax" on immigrants, but suggesting the government's willingness to implement one if possible.
Explaining how discriminatory policies are still institutionalized, such as the economic benefit requirement for legal immigrants.
Providing historical context on immigration control in the United States and the perpetuation of exploiting and scapegoating different groups.
Criticizing the continued use of discriminatory tactics in American politics, using Dreamers as a contemporary example.
Emphasizing the cyclical nature of scapegoating and exploitation in American history.
Stating the need to acknowledge past injustices to move forward and fulfill promises made to all Americans.

Actions:

for activists, advocates, immigrant rights organizations,
Advocate for comprehensive immigration reform to protect the rights of all immigrants (suggested).
Support organizations working towards fair treatment and opportunities for undocumented individuals (implied).
Educate others on the historical and present-day discriminatory practices in immigration policies (suggested).
</details>
<details>
<summary>
2021-03-19: Let's talk about Biden's request for an extension.... (<a href="https://youtube.com/watch?v=BVn4pCkgMEo">watch</a> || <a href="/videos/2021/03/19/Lets_talk_about_Biden_s_request_for_an_extension">transcript &amp; editable summary</a>)

Beau believes the U.S. should leave Afghanistan promptly, as an extension is unnecessary, and the decision should lie with the Afghan people.

</summary>

"The best course is to get the U.S. out at the absolute soonest opportunity."
"We still need to leave. I don't believe the extension is really necessary."
"The U.S. has about 2,500 troops in Afghanistan. That's a token force."
"We already are in a token force. Does that mean that it is unimportant to remove that token force? Absolutely not."
"It's bad if a deal is made and then the next president comes along and breaks it."

### AI summary (High error rate! Edit errors on video page)

Explains Biden administration's request to extend the withdrawal date from Afghanistan.
Believes extension is unnecessary due to professional militaries ready to take over quickly.
Points out that the current perception of troops all over Afghanistan is outdated.
Emphasizes the importance of leaving Afghanistan at the soonest possible moment.
Acknowledges the need for the U.S. to honor agreements despite criticizing past decisions.
Advocates for letting the people of Afghanistan decide the timeline for withdrawal.

Actions:

for policy makers, activists, citizens,
Let Afghan people decide on the withdrawal timeline (implied)
Advocate for prompt withdrawal from Afghanistan (suggested)
</details>
<details>
<summary>
2021-03-18: Let's talk about the Asian experience in the US throughout history.... (<a href="https://youtube.com/watch?v=rFbHml5ba0M">watch</a> || <a href="/videos/2021/03/18/Lets_talk_about_the_Asian_experience_in_the_US_throughout_history">transcript &amp; editable summary</a>)

Beau outlines the historical context of anti-Asian sentiment in America, urging for integration to combat division and ignorance.

</summary>

"It's probably way past time to stop falling for this."
"Exposure ends that ignorance."
"If you have Asian friends, bring them out with you."
"We can set the example for younger people today that this isn't okay."
"Then we can say it's not us."

### AI summary (High error rate! Edit errors on video page)

Explains how the current issues in American society are not new and have historical roots.
Talks about how Trump didn't create the anti-Asian sentiment but capitalized on it for his base.
Provides an abridged timeline of anti-Asian actions and legislation in the United States.
Mentions various discriminatory laws and actions against Asians throughout history.
Points out the importance of not falling for divisive tactics and normalizing the Asian community.
Emphasizes the significance of exposure and integration to combat ignorance and fear.
Urges for bringing Asian Americans into communities to prevent marginalization.
Stresses the need to acknowledge American culpability in perpetuating anti-Asian sentiments.
Encourages taking action to change the current narrative and not allowing politicians to exploit prejudices.

Actions:

for all americans,
Bring Asian friends out with you, frequent Asian businesses to integrate them into the community (suggested)
Reach out to older Americans and set the example for younger generations by making the Asian community part of our communities (implied)
</details>
<details>
<summary>
2021-03-17: Let's talk about what Biden can learn from Trump about foreign policy.... (<a href="https://youtube.com/watch?v=-XhV7x914Uc">watch</a> || <a href="/videos/2021/03/17/Lets_talk_about_what_Biden_can_learn_from_Trump_about_foreign_policy">transcript &amp; editable summary</a>)

Beau suggests Biden learn from Trump's failed foreign policy by engaging in high-level talks with North Korea without preconditions to initiate peace efforts.

</summary>

"The precondition of North Korea giving up its program has produced no positive results."
"Kim Yo-jong's statement opened the door to a possible conversation with the United States."
"Starting talks without preconditions is key to any real progress."

### AI summary (High error rate! Edit errors on video page)

Beau talks about what President Biden can learn from former President Trump's foreign policy.
Trump's foreign policy was viewed as a failure, but he attempted high-level talks with North Korea.
Beau believes Biden might not have the skill set to make high-level talks work by himself, but he has a strong foreign policy team.
The precondition of North Korea giving up its program has hindered progress and produced no positive results.
Beau dispels fears of U.S. invasion of North Korea, stating it's all posturing, and a stalemate exists.
He suggests dropping the precondition and engaging in high-level talks with North Korea over minor issues to show progress.
Beau mentions Kim Yo-jong, Kim Jong-un's sister, as a powerful figure who recently made a statement directed at the U.S.
He recommends high-level talks with North Korea involving heads of state or their representatives to start the peace process.
Beau acknowledges that the process of denuclearization will be long but believes starting talks without preconditions is key.
Biden's team is reviewing Trump's North Korea policy and aiming to develop a centralized stance that hopefully includes talks without preconditions.

Actions:

for policy makers, diplomats, analysts,
Initiate high-level talks with North Korea over minor issues to show progress (implied).
Develop a centralized policy stance that includes talks without preconditions (implied).
</details>
<details>
<summary>
2021-03-17: Let's talk about Biden, McConnell, and the filibuster.... (<a href="https://youtube.com/watch?v=8AMyq9DYHew">watch</a> || <a href="/videos/2021/03/17/Lets_talk_about_Biden_McConnell_and_the_filibuster">transcript &amp; editable summary</a>)

Major Democratic figures advocate altering the filibuster, while Republicans resist, risking revealing unpopular agendas.

</summary>

"If I was rewriting the rules I would make sure that they also have to stay on topic while they are debating."
"That's what you want. That's what you want. That's what you want to put out there."
"It might inform people about what the Republican party really is."

### AI summary (High error rate! Edit errors on video page)

Major Democratic Party figures support altering or repealing the filibuster, with some advocating for a return to a talking filibuster.
The filibuster allows the minority party, currently Republicans, to block legislation unless 60 votes are secured in the Senate.
Democratic leaders are pushing for changes to the filibuster, aiming to encourage healthy debate.
Biden plans to advocate for a return to a talking filibuster, where senators must speak to hold up legislation.
Beau suggests requiring senators to stay on topic during filibusters to prevent irrelevant speeches.
Republicans, led by Mitch McConnell, strongly oppose filibuster changes, warning of drastic measures if it's altered.
McConnell threatens to pass unpopular proposals if filibuster rules change, but this move may not deter Democrats.
Beau points out that threats to pass divisive legislation could backfire on the Republican Party, revealing their true agenda.
He notes that many Americans are unaware of the Republican Party's platform, which now centers around loyalty to Trump.
Beau predicts potential movement on the filibuster debate in the coming weeks unless Republicans cooperate on Biden's agenda.

Actions:

for legislative aides, political activists,
Contact your senators to express support for or against filibuster reform (suggested)
Stay informed on the ongoing debate over the filibuster and its potential impact on legislation (implied)
</details>
<details>
<summary>
2021-03-16: Let's talk about Georgia election laws and Rule 303.... (<a href="https://youtube.com/watch?v=I4trkvkJ0O0">watch</a> || <a href="/videos/2021/03/16/Lets_talk_about_Georgia_election_laws_and_Rule_303">transcript &amp; editable summary</a>)

The Republican Party's push to restrict voting faces opposition from corporations like Coca-Cola and Home Depot in Georgia, potentially changing the game against disenfranchisement.

</summary>

"Georgia, with 7.6 million voters, faces similar restrictions."
"A suggestion to end the issue: Democrats spot Republicans eight votes in each election."
"Voting restrictions aim to disenfranchise certain groups, especially those who turned Georgia blue."

### AI summary (High error rate! Edit errors on video page)

The Republican Party nationwide is pushing to restrict voting, including in Georgia, following baseless accusations by the Trump campaign.
Texas spent 22,000 hours looking for voter registration fraud and found only 16 out of 17 million voters.
Georgia, with 7.6 million voters, faces similar restrictions.
A suggestion to end the issue: Democrats spot Republicans eight votes in each election.
Coca-Cola and Home Depot oppose the new voting restrictions in Georgia, which could be a significant change.
Coca-Cola's Political Action Committee traditionally leans Republican but values areas like equality and inclusion in candidate evaluations.
Restricting voting ability contradicts inclusivity and equality.
Voting restrictions aim to disenfranchise certain groups, especially those who turned Georgia blue.
Coca-Cola, with its power and influence in Georgia, has the means and responsibility to prevent these voting restrictions.
If Coca-Cola and Home Depot use their political power effectively, these restrictive bills might not succeed.

Actions:

for georgia voters,
Contact Coca-Cola and Home Depot to thank them for opposing voting restrictions in Georgia (suggested).
Stay informed about candidates and political contributions using tools like OpenSecrets.org (suggested).
</details>
<details>
<summary>
2021-03-16: Let's talk about Dr. Seuss and the Moon.... (<a href="https://youtube.com/watch?v=ybNlFcxqxh4">watch</a> || <a href="/videos/2021/03/16/Lets_talk_about_Dr_Seuss_and_the_Moon">transcript &amp; editable summary</a>)

Beau talks logistics, decentralization, and contrasting priorities between preserving old books and genetic codes on the moon.

</summary>

"Or we can explore the galaxy."
"When you look at it like that, it really shows the difference in mindset, what the priorities are."
"The way we’ve always done it may end up leading to the need for an ark on the moon."

### AI summary (High error rate! Edit errors on video page)

Talks about the importance of backup plans, like the Seed Vault, to preserve historic artifacts and samples in case of any eventuality.
Mentions a proposed ultimate backup plan to place samples of 6.7 million species on the moon in cryopreservation facilities.
Points out the significance of decentralization in preserving samples to ensure their availability even in catastrophic events like Yellowstone erupting.
Describes the amazing logistics involved in the lunar backup plan.
Emphasizes the contrast between people's priorities on Earth, from preserving old books to preserving the genetic code of Earth on the moon.
Raises the question of tradition versus future good as a key talking point that divides mindsets.
Suggests that the way things have always been done may necessitate extreme measures like an ark on the moon.

Actions:

for space enthusiasts, conservationists,
Preserve biodiversity locally through community gardens and conservation efforts (implied)
Support initiatives that focus on environmental preservation and space exploration (implied)
</details>
<details>
<summary>
2021-03-16: Let's talk about Biden's next big legislative push.... (<a href="https://youtube.com/watch?v=-2YYEe7v67c">watch</a> || <a href="/videos/2021/03/16/Lets_talk_about_Biden_s_next_big_legislative_push">transcript &amp; editable summary</a>)

Beau speculates on Biden's legislative agenda, focusing on potential tax increases for the wealthy and an environmentally friendly infrastructure package.

</summary>

"The idea of increasing the corporate tax rate is to get companies to reinvest the money."
"If you don't make more than $33,000 a month, none of it applies to you."
"It's all guessing. So that's where we're at."

### AI summary (High error rate! Edit errors on video page)

Questions arise about Biden's upcoming legislative agenda, including comparisons to the Green New Deal, infrastructure programs, and tax increases.
Details of the agenda are yet to be released, leading to speculation based on campaign promises and advisors' recommendations.
Predictions include a potential 6 to 7 percent corporate tax increase, aiming to encourage reinvestment or higher employee wages.
Biden's tax plan does not advocate raising taxes for individuals earning less than $33,000 a month.
The agenda may focus on taxing the wealthy, in line with Biden's campaign promises and advisors' suggestions.
Beau argues that a tax increase on the wealthy is a fundamentally American concept, supported by the country's philosophical founders.
He challenges the idea of no taxes, stating it eliminates the need for a government and relies on individuals' voluntary social responsibility.
Beau points out the potential benefits of the tax increase, such as increased business investment or support for large foundations.
The infrastructure package is speculated to be an environmentally friendly build-out, but not a full-fledged Green New Deal.
Uncertainty surrounds the agenda's details and passage, with potential strategies including budget reconciliation and filibuster amendments.

Actions:

for policy analysts, political enthusiasts,
Stay informed about Biden's legislative agenda and its potential impact (suggested).
Advocate for socially responsible tax policies (implied).
</details>
<details>
<summary>
2021-03-15: Let's talk about persuading people and video games.... (<a href="https://youtube.com/watch?v=ERNidN-cju8">watch</a> || <a href="/videos/2021/03/15/Lets_talk_about_persuading_people_and_video_games">transcript &amp; editable summary</a>)

Beau explains the importance of providing people with the right tools at the right time when persuading them politically, cautioning against overwhelming with too much information and stressing the effectiveness of small, digestible bits in shifting viewpoints gradually.

</summary>

"It's dangerous to go alone. Take this."
"You can't teach algebra to people who can't act."
"Either that initial source is incompetent and can't gather basic information or it's intentionally misleading."
"We should probably try to frame our arguments so they can be more easily accepted."
"For a lot of people it's too much to take in at one time."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of giving people the tools they need in the moment when persuading them politically.
Compares this approach to how video games provide players with necessary tools at critical moments.
Advises against overwhelming individuals with too much information all at once.
Emphasizes the effectiveness of presenting small, digestible bits of information.
Points out the necessity of meeting people where they are on their personal paths.
Warns that going beyond what someone is ready to accept may do more harm than good.
Stresses the significance of sticking to a specific topic when discussing with others.
Suggests that focusing on objectively false topics can erode trust in misleading sources.
Shares personal experience of gradually shifting people's views by providing information over time.
Encourages framing arguments in a way that can be easily accepted by individuals with varying perspectives.

Actions:

for political activists,
Provide small, digestible bits of information to individuals to persuade them politically (exemplified).
Frame arguments in a way that can be easily accepted by those with different perspectives (suggested).
Gradually shift people's views by providing information over time (exemplified).
</details>
<details>
<summary>
2021-03-15: Let's talk about Biden's border issue.... (<a href="https://youtube.com/watch?v=Sh7tcjZL5a4">watch</a> || <a href="/videos/2021/03/15/Lets_talk_about_Biden_s_border_issue">transcript &amp; editable summary</a>)

Conditions are dire, focus on expedited processing to improve overcrowding and poor conditions for unaccompanied minors, with Biden's administration taking steps to rectify the situation.

</summary>

"Just better than Trump is not good enough in this regard."
"The goal is to get them processed."
"Full stop."
"Bad news is that more than likely, as soon as this happens, there may be a backlog at the next station."
"The long line is Trump's fault."

### AI summary (High error rate! Edit errors on video page)

Conditions are dire down south, focusing on finding solutions rather than dwelling on current conditions.
The problem lies in processing delays, causing overcrowding and poor conditions for unaccompanied minors.
The solution is to expedite processing to alleviate overcrowding and improve conditions.
Biden's plan involves processing within 72 hours, but delays have caused a backlog.
Bringing in FEMA to assist with processing is a positive step taken by Biden.
The goal is swift processing to transfer minors to Health and Human Services and then to sponsors promptly.
While conditions may be better than under Trump, the current standard is still unacceptable.
Swift processing is key to improving conditions and reducing overcrowding.
Biden's administration aims to process minors quickly and get them out of custody, contrasting with Trump's approach.
Any backlog issues should be anticipated and addressed proactively by the administration.

Actions:

for advocates, policymakers, community members,
Support expedited processing of unaccompanied minors to alleviate overcrowding and improve conditions (implied).
Stay informed about the situation and advocate for proactive solutions within your community (implied).
</details>
<details>
<summary>
2021-03-14: Let's talk about the most important question from Tucker Carlson.... (<a href="https://youtube.com/watch?v=pbcZTsIXmTY">watch</a> || <a href="/videos/2021/03/14/Lets_talk_about_the_most_important_question_from_Tucker_Carlson">transcript &amp; editable summary</a>)

Beau responds to criticism, educates on military retention, and clarifies the real importance behind a provocative question by Tucker Carlson.

</summary>

"The only question that matters apparently is how does active duty soldiers getting their reassignment surgery paid for by the Pentagon, how does that make the country safer?"
"It helps keep the country safe, which really, that just means it increases readiness, by keeping trained and experienced people in the military."
"The most vital question, like ever, apparently, there's your answer."

### AI summary (High error rate! Edit errors on video page)

Responding to criticism from a fan of Tucker Carlson regarding not answering the "only question that matters."
Explaining the importance of active duty soldiers getting reassignment surgery paid for by the Pentagon.
Addressing the significance of retention in the military, particularly for trained and experienced personnel.
Detailing the high costs associated with training pilots and the retention bonuses offered to keep them in the military.
Providing insights into the Army's challenges in tracking training costs and the value placed on retention.
Emphasizing the critical role of retaining trained individuals in enhancing readiness and safety.
Pointing out the intention behind Tucker Carlson's question and the underlying bias against certain individuals in the military.
Offering a link to a video discussing how soldiers impact readiness.
Clarifying the statistical insignificance of the issue raised in the video regarding soldiers seeking reassignment surgery.
Concluding that the most vital question raised actually contributes to increasing readiness by retaining skilled military personnel.

Actions:

for journalists, activists, military personnel,
Contact military recruiters to understand the importance of recruitment and retention (implied)
</details>
<details>
<summary>
2021-03-13: Let's talk about what Kentucky's senate just told us.... (<a href="https://youtube.com/watch?v=Rnbg7rnM9UQ">watch</a> || <a href="/videos/2021/03/13/Lets_talk_about_what_Kentucky_s_senate_just_told_us">transcript &amp; editable summary</a>)

Kentucky bill criminalizing insults towards officers reveals authoritarian views and threatens excessive force empowerment, urging citizen action.

</summary>

"Any senator who voted for this needs to be voted out of office."
"If this becomes law, I give it no more than 10 arrests under this statute before there is a million dollar payout."
"The people of Kentucky probably need to get a hold of their senators."

### AI summary (High error rate! Edit errors on video page)

Kentucky state senate passed a bill making it a misdemeanor to insult or taunt law enforcement officers.
The bill is a blatant violation of the First Amendment and is likely to be overturned in court.
The bill raises questions about the senators' authoritarian views and their perception of the people of Kentucky.
This legislation empowers law enforcement to use force more frequently, especially in a climate where excessive force is already a concern.
The bill could lead to officers using violence based on subjective interpretations of gestures or words.
Senators who supported this bill are seen as authoritarians giving law enforcement a blank cheque for excessive force.
This legislation is an attempt to provide cover for excessive force and could result in numerous lawsuits.
Beau urges Kentuckians to contact their senators and the governor's office to prevent this bill from becoming law.
He predicts that if the bill passes, there will be significant legal repercussions and financial costs for the state.
Beau stresses the importance of citizens taking action to prevent the potential negative consequences of this bill.

Actions:

for kentuckians,
Contact your senators and the governor's office to oppose the bill (suggested).
Advocate for preventing the bill from becoming law by reaching out to lawmakers (suggested).
</details>
<details>
<summary>
2021-03-13: Let's talk about the quality of votes.... (<a href="https://youtube.com/watch?v=5yh5rgmHXaI">watch</a> || <a href="/videos/2021/03/13/Lets_talk_about_the_quality_of_votes">transcript &amp; editable summary</a>)

Beau warns of nationwide Republican efforts to suppress votes, calling it an abandonment of democracy and a threat to the people's voice in government.

</summary>

"Not everybody wants to vote. And if somebody's uninterested in voting, that probably means they're totally uninformed on the issues."
"It's an abandonment of the idea of democracy."
"I have a lot of issues with low-information voters. I have more issue with low-information representatives."
"It's not the votes that count. It's who determines the quality of the votes."
"If stuff like this gets through, even the pretext that the people have some voice in government is gone."

### AI summary (High error rate! Edit errors on video page)

Voting in the United States is taken for granted, with voting on various aspects being a common occurrence from an early age.
The real power in the government lies with lobbyists and the money they have, making citizens' votes more of a suggestion.
China is attempting to diminish the autonomy of Hong Kong through methods like adding 300 informed "quality voters" to the election committee, a move opposed by the GOP.
State representatives and Republicans across the country are actively involved in voter suppression campaigns, including limiting polling stations and mail-in voting access to certain demographics.
Beau criticizes the pretext of voter security used to pass legislation that suppresses votes, labeling it as an abandonment of democracy.
He calls out a state representative in Arizona, John Kavanaugh, for focusing on the quality of votes and using security as a guise for voter suppression.
The voter suppression efforts disproportionately impact certain groups, particularly those who helped flip Georgia blue.
Beau urges those who support electoralism and want to participate in elections to pay attention to the nationwide Republican efforts to suppress votes.
He warns that if these suppression tactics succeed, the voice of the people in government will be severely compromised.

Actions:

for voters, activists, community members,
Pay attention to and actively oppose voter suppression efforts by joining local initiatives and organizations (exemplified)
Support voter education and outreach programs to ensure all citizens are informed and able to participate in elections (exemplified)
</details>
<details>
<summary>
2021-03-12: Let's talk about Tucker Carlson and women in the military.... (<a href="https://youtube.com/watch?v=mlO-r6OeCWA">watch</a> || <a href="/videos/2021/03/12/Lets_talk_about_Tucker_Carlson_and_women_in_the_military">transcript &amp; editable summary</a>)

Beau challenges Tucker Carlson's criticism of the military's standards, calls out his lack of understanding, and suggests a showdown with women veterans to prove a point.

</summary>

"Nobody could honestly argue that he wasn't qualified."
"After the last couple of years, I suggest it's vital that it changes."
"Fox News is a detriment to morale, and that might change."
"I think it'd be entertaining to watch."
"It's just a thought, y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Tucker Carlson criticized the military for women getting proper maternity uniforms and relaxed hairstyles.
Carlson's opinion on combat readiness holds no weight with those who have experienced combat.
Women needing maternity uniforms for active duty activities don't need a lecture on toughness from Carlson.
Beau suggests women veterans participating in a charity boxing event with Carlson.
Beau challenges Carlson to an obstacle course and PT test against women from each branch.
Chinese military is mentioned as becoming more masculine, but they have about 100,000 women in their military.
Beau points out Carlson's lack of understanding when criticizing Lloyd Austin's qualifications.
The idea of maintaining Roman Legion standards in the military is deemed ridiculous by Beau.
Beau suggests PT standards should be more job-based rather than uniform across the board.
The Army responded to Carlson's comments by tweeting images of women soldiers.

Actions:

for military advocates, women in the military,
Challenge harmful narratives: Address misinformation and stereotypes (suggested)
Support women in the military: Advocate for fair treatment and respect (suggested)
Boost morale: Share positive stories and images of women soldiers (implied)
</details>
<details>
<summary>
2021-03-12: Let's talk about Biden, the stimulus, and the midterms.... (<a href="https://youtube.com/watch?v=reHjvbSRuTM">watch</a> || <a href="/videos/2021/03/12/Lets_talk_about_Biden_the_stimulus_and_the_midterms">transcript &amp; editable summary</a>)

Beau explains his evolving opinion on Biden, noting unexpected progressiveness amidst strategic political maneuvering.

</summary>

"I do have a higher opinion of Biden today than I did before the election."
"Biden is running on this theme of unity or whatever. We all know it's not real."
"He is working to try out ideas that are far more progressive than anything I ever expected from Biden."
"It's surprising, surprising to me. I didn't expect it from him."
"I do have a higher opinion of it."

### AI summary (High error rate! Edit errors on video page)

Shares his evolving opinion of Biden post-election.
Expected a reboot of the Obama administration but notes Biden is doing more than that.
Acknowledges progressive expectations of Biden but personally only expected minimal changes.
Explains why Biden didn't criticize Republicans in his speech.
Talks about the stimulus bill and Republican opposition to it.
Details leftist provisions in the stimulus bill like child tax credit and healthcare subsidies.
Mentions the impact of these provisions on working-class Americans.
Remarks on Biden's approach to more progressive policies than anticipated.
Speculates on Biden's strategy for pushing through progressive policies.
Comments on the potential outcomes of Biden's approach.

Actions:

for political observers,
Share information about the provisions in the stimulus bill with working-class Americans (implied).
Stay informed about Biden's policies and their potential impact on various groups (implied).
Pay attention to the details and long-term goals of political leaders' actions (implied).
</details>
<details>
<summary>
2021-03-11: Let's talk about changing society and a lesson from Nevada.... (<a href="https://youtube.com/watch?v=KE-dO6pYMRQ">watch</a> || <a href="/videos/2021/03/11/Lets_talk_about_changing_society_and_a_lesson_from_Nevada">transcript &amp; editable summary</a>)

Beau touches on defensive and offensive strategies for societal change, advocating for Community Networks and constructive planning over mere opposition.

</summary>

"Building a just world is hard. Being against injustice, it's even easy socially to take that stance."
"Being against something doesn't actually get you the society you want. Doesn't get you the world you want. It just slows the world you don't."
"You have to put in the work to build the society you want, not just be against what exists."

### AI summary (High error rate! Edit errors on video page)

Introduces the concept of "breaking the hammer" as a defensive strategy to achieve societal change.
Attributes Trump not being in office to four years of activist efforts through campaigning, organizing, and educating.
Emphasizes that merely being against something doesn't bring the desired societal change; it only slows down what you oppose.
States that building a just world is challenging compared to opposing injustice, which is relatively easy.
Advocates for going on the offensive by establishing Community Networks to create political change rapidly.
Cites the example of Nevada where the progressive part of the Democratic Party took over through years of organizing.
Stresses the importance of offering solutions and building something better rather than solely opposing existing systems.
Contrasts the approach of Trump, who capitalized on grievances without offering substantial solutions, with the need for a constructive plan to create real change.
Points out that being against systemic issues or policies is insufficient; one must have a plan to address and replace them.
Concludes by urging people to put in the work to build the society they desire rather than merely opposing the current state of affairs.

Actions:

for change-makers, activists, community leaders,
Establish Community Networks to create rapid political change (implied)
Offer solutions and build something better to attract support (implied)
Organize and mobilize for systemic change (implied)
</details>
<details>
<summary>
2021-03-11: Let's talk about a one year anniversary, failure, and hope.... (<a href="https://youtube.com/watch?v=ajvVjq2XZx4">watch</a> || <a href="/videos/2021/03/11/Lets_talk_about_a_one_year_anniversary_failure_and_hope">transcript &amp; editable summary</a>)

One year into the pandemic, Beau questions the US's failure in handling COVID-19 and calls for leadership and community resilience to prevent future disasters.

</summary>

"The United States failed. Five percent of the world's population and a little less than 20% of the loss."
"We're nearing the end of this, hopefully. And when we come out of it, we have to be ready to get to work."

### AI summary (High error rate! Edit errors on video page)

One year since the global mess began officially with 118 million cases and 2.6 million lives lost globally.
The United States, with less than 5% of the world's population, has lost over 500,000 lives, almost 20% of the total.
Despite having infrastructure, research, plans, and capabilities to mitigate, the US failed badly in handling the pandemic.
Lack of leadership was a significant factor, with some downplaying the situation even with evidence against them.
People in the public eye failed the country by ignoring the severity of the situation and pushing false narratives.
Beau questions how the US, with all its resources, became the leader in COVID-19 deaths.
The US failed to recognize danger due to comfort and complacency, leading to catastrophic consequences.
Beau criticizes the focus on trivial issues like Dr. Seuss books instead of addressing the nation's failures.
Despite the failures, Beau sees a glimmer of hope in the readiness for change and improvement post-pandemic.
He calls for strong leadership, community building, and resilience to prevent such failures in the future.

Actions:

for general public, policymakers,
Build community resilience to tackle future crises (implied)
Advocate for strong leadership in handling crises (implied)
Focus on addressing critical issues rather than distractions (implied)
</details>
<details>
<summary>
2021-03-10: Let's talk about the cry of freedom, the Revolution, and public health.... (<a href="https://youtube.com/watch?v=9DLPfH-8e4M">watch</a> || <a href="/videos/2021/03/10/Lets_talk_about_the_cry_of_freedom_the_Revolution_and_public_health">transcript &amp; editable summary</a>)

Beau dives into how Washington's proactive public health measures debunk the notion of founders prioritizing freedom over science in today's context.

</summary>

"Washington embraced science and ordered strict quarantines during smallpox outbreak."
"The idea of founders disregarding science for freedom in public health is inaccurate."
"If Washington were in charge today, we probably have had massive lockdowns."

### AI summary (High error rate! Edit errors on video page)

Governors across the country are removing mandates while invoking the image of the founders and the fight for freedom.
Washington embraced science and ordered strict quarantines during the smallpox outbreak in Boston, showing a proactive approach to public health.
Washington faced opposition from close-minded and uninformed politicians regarding inoculation, a form of early vaccination.
Despite Congress banning inoculation, Washington went ahead and inoculated his troops, leading to Congress repealing the ban later due to its effectiveness.
The idea of the founders disregarding science for freedom in public health matters is historically inaccurate.
During Valley Forge, many troops received inoculation, showing the founders' commitment to public health even in crude forms.
If Washington were in charge today, there likely would have been massive lockdowns, mask mandates, and large-scale vaccine drives.
Washington's action of ordering troop inoculation against Congress's wishes was the first major public health initiative in the United States.

Actions:

for history enthusiasts, public health advocates.,
Join or support organizations advocating for evidence-based public health measures (implied).
Advocate for mask mandates and large-scale vaccine drives in your community (implied).
Educate others on the historical importance of public health initiatives in shaping the nation's health policies (implied).
</details>
<details>
<summary>
2021-03-10: Let's talk about the PRO Act, Unions, contractors, and the filibuster.... (<a href="https://youtube.com/watch?v=nR1RuC1H52E">watch</a> || <a href="/videos/2021/03/10/Lets_talk_about_the_PRO_Act_Unions_contractors_and_the_filibuster">transcript &amp; editable summary</a>)

Beau explains the PRO Act, a union bill protecting the right to organize, facing Senate opposition but potentially leading to filibuster reform, with key support from labor organizers.

</summary>

"Protects the right to organize."
"Every single labor organizer I know is very in support of this."
"It's only going to impact you if you try to start a union."

### AI summary (High error rate! Edit errors on video page)

Explains the significance of the PRO Act passing the House and heading to the Senate, where it may face strong opposition but also has powerful allies.
Describes the PRO Act as Protecting the Right to Organize, a union bill that penalizes big businesses for retaliating against workers trying to unionize.
Mentions that labor organizing is not his expertise and consulted labor organizers who strongly support the PRO Act.
Shares insights from Abby at Working Stiff USA, mentioning the importance of solidarity strikes, collective bargaining for gig workers, and debunking myths about the impact on independent contractors.
Points out that the ABC test in the PRO Act only applies to workers seeking to organize under the NLRA.
Notes the need for 60 votes in the Senate to overcome a filibuster, with unions having significant influence that could potentially lead to filibuster reform.
Suggests that organized support for the PRO Act could counter the opposition's financial influence and sway over senators.

Actions:

for advocates, workers, activists,
Organize in support of the PRO Act (suggested)
</details>
<details>
<summary>
2021-03-09: Let's talk about Biden's mental state.... (<a href="https://youtube.com/watch?v=p4-fxOZIVhU">watch</a> || <a href="/videos/2021/03/09/Lets_talk_about_Biden_s_mental_state">transcript &amp; editable summary</a>)

Beau explains the futility of proving baseless claims and suggests tactics to handle bad faith arguments effectively.

</summary>

"You cannot prove a negative."
"Lean into that."
"A non-white woman is beating him in the polls? That's gotta bother him."
"We elected a guy with dementia over him. That's how bad he was doing."
"Shutting them up is the next best thing."

### AI summary (High error rate! Edit errors on video page)

Explains a bad faith argument surrounding accusations of Biden having dementia.
Argues that there's no need to respond in good faith to baseless claims.
Points out the futility of trying to prove a negative.
Mentions the presumption of innocence and the endless shifting of goalposts.
Describes how people latch onto any evidence to support their claims about Biden's dementia.
Talks about how mispronunciation doesn't indicate lack of intelligence.
Suggests leaning into the claim that Biden's advisors make decisions for him.
Mentions the theory that Harris has the final say over Biden.
Comments on Trump being beaten in the polls by a non-white woman.
Concludes by discussing methods to handle such arguments effectively.

Actions:

for debaters, truth-seekers, critical thinkers.,
Lean into wild accusations (exemplified).
Repeat process to shut down baseless claims (implied).
</details>
<details>
<summary>
2021-03-09: Let's talk about Biden's boldest move yet.... (<a href="https://youtube.com/watch?v=xTsb493st_A">watch</a> || <a href="/videos/2021/03/09/Lets_talk_about_Biden_s_boldest_move_yet">transcript &amp; editable summary</a>)

Be ready to support involving Iran in Afghanistan for the sake of stability and troop withdrawal, despite potential political opposition.

</summary>

"This is the right move for the people that everybody likes to pretend they care about."
"The only reason to oppose this is if you care more about giving Biden a black eye than innocent lives."
"Foreign policy is never about right and wrong."
"Once we go back in, the exact same thing will happen."
"It benefits them both."

### AI summary (High error rate! Edit errors on video page)

The Biden administration hinted at involving Iran in Afghanistan, which could be a beneficial move.
The U.S. involvement in Afghanistan has evolved over time, leading to a complex situation.
Iran's capabilities make them a suitable candidate to take over from the U.S. in Afghanistan.
Involving Iran could help stabilize the region and prevent further Western intervention.
The move benefits all parties involved and has significant implications for the Middle East's power balance.
Beau expresses skepticism about the Biden administration's willingness to pursue this strategy due to potential political backlash.
Republican opposition to such a move may stem from a desire to obstruct Biden rather than genuine concerns.
Beau argues that involving Iran is a viable path to finally withdrawing U.S. troops from Afghanistan.
The priority should be on preventing chaos and instability in Afghanistan post-U.S. withdrawal.
The potential downside of Iran's involvement could lead to chaos if they are unable to stabilize the situation.

Actions:

for concerned citizens,
Support the move to involve Iran in Afghanistan to stabilize the region and bring troops home (suggested).
Advocate for a strategic withdrawal from Afghanistan to prevent chaos and further intervention (implied).
</details>
<details>
<summary>
2021-03-08: Let's talk about the stimulus going mostly to Republicans.... (<a href="https://youtube.com/watch?v=c47rKtuJl_Q">watch</a> || <a href="/videos/2021/03/08/Lets_talk_about_the_stimulus_going_mostly_to_Republicans">transcript &amp; editable summary</a>)

There are two stimuli: popular aid for those left behind, and a less known one to stimulate compassion among Republicans for ongoing government assistance.

</summary>

"That second stimulus, that one nobody's talking about, that's gonna go primarily to Republicans who will acknowledge this, perhaps for the first time."
"It might be wise for a whole lot of people to realize this system is broke."
"Because it's not always something that's predictable. It can come out of nowhere. They're no fault of your own."

### AI summary (High error rate! Edit errors on video page)

There are two stimuli being discussed: the popular one with $1400 checks, and the lesser-known one primarily benefitting Republicans.
The popular stimulus helps those left behind by the system due to factors like job loss and educational barriers.
The second, less publicized stimulus aims to assist individuals who acknowledge the flaws in the system and need regular government aid.
Republicans who support this second stimulus may experience a shift towards compassion and human decency.
The year has shown that many people are not as secure as they believe, and anyone could suddenly find themselves in need of assistance.
Beau suggests that more individuals should recognize the brokenness of the current system that frequently leaves people behind.

Actions:

for american citizens,
Acknowledge flaws in the system and advocate for more comprehensive government aid (suggested)
Support policies that aim to assist individuals in need of regular government assistance (implied)
Advocate for a more inclusive and supportive system for all individuals (implied)
</details>
<details>
<summary>
2021-03-08: Let's talk about food security and planting your yard.... (<a href="https://youtube.com/watch?v=Rs9IKs_2FF4">watch</a> || <a href="/videos/2021/03/08/Lets_talk_about_food_security_and_planting_your_yard">transcript &amp; editable summary</a>)

Beau advocates for planting food in yards annually for food security, exercise, and community building, debunking common misconceptions about difficulty, time, cost, and space constraints.

</summary>

"Food not lawns."
"It helps foster that community."
"It's not hard if you're just focusing on trees and bushes."
"Generally, it's not expensive."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Advocates for planting food in yards annually for food security, exercise, environmental benefits, and community building.
Encourages planting fruit trees, nut trees, bushes, and gardens tailored to USDA growing zones.
Points out misconceptions about difficulty, time, cost, and space constraints for planting food.
Notes that planting trees and bushes requires minimal effort once established.
Mentions the availability of seeds or plants for those on SNAP or EBT programs.
Suggests using containers for limited space and replacing ornamental bushes with fruit-bearing plants.
Emphasizes the community-wide benefits of sharing fresh produce with neighbors.
Proposes a simple and enjoyable way to strengthen local communities through gardening.

Actions:

for community garden enthusiasts,
Plant fruit trees, nut trees, bushes, or garden in your yard tailored to your USDA growing zone (exemplified).
Share excess produce with neighbors to foster community (implied).
</details>
<details>
<summary>
2021-03-07: Let's talk about Trump trying to take over Republican finances.... (<a href="https://youtube.com/watch?v=8qSw7sZ50qs">watch</a> || <a href="/videos/2021/03/07/Lets_talk_about_Trump_trying_to_take_over_Republican_finances">transcript &amp; editable summary</a>)

Former President Trump is trying to control Republican Party finances through cease and desist letters to stop using his name for fundraising, potentially impacting their efforts to win back the House and Senate.

</summary>

"Trump is attempting to seize control of the Republican Party's finances."
"If he gets his way, it very well may cost them the House and the Senate."

### AI summary (High error rate! Edit errors on video page)

Former President Trump sent cease and desist letters to Republican organizations to stop using his name for fundraising.
Trump wants to control the Republican Party's finances and have money funneled through his PAC or himself.
Large companies and donors donate for influence, and they might not see value in donating to Trump's PAC.
The Republican establishment and donors may have concerns about Trump controlling a large portion of their budget.
This move by Trump could have long-term implications for the Republican Party and their efforts to win back the House and Senate.

Actions:

for party members, donors, political analysts,
Contact Republican organizations to express concerns about Trump's control over finances (implied)
Monitor how this situation unfolds within the Republican Party and its implications for future elections (implied)
</details>
<details>
<summary>
2021-03-07: Let's talk about FEMA, the CDC, zombies, and wet wood.... (<a href="https://youtube.com/watch?v=_M0o3DBr_g4">watch</a> || <a href="/videos/2021/03/07/Lets_talk_about_FEMA_the_CDC_zombies_and_wet_wood">transcript &amp; editable summary</a>)

Beau introduces his new channel and explains why FEMA and the CDC use zombies in emergency preparedness, while also addressing survival skills like starting a fire with wet wood and encouraging preparedness for future disasters.

</summary>

"If you plan to survive zombies, you will make it through a snowstorm or a hurricane or whatever, because zombies disrupt everything."
"But in that situation, what else do you have to do?"
"You realized that there was an issue and you are trying to be prepared for next time."
"And rest assured, there will be a next time, just as sure as there's going to be another hurricane where I live."
"So just maybe take this afternoon and think about what you would do in the event of a zombie apocalypse."

### AI summary (High error rate! Edit errors on video page)

Introducing his second channel, The Roads with Beau, which features long-format content on historical events, community networking, and on-location experiences.
Due to travel restrictions, a project focused on community networking was put on hold but is now coming back as restrictions ease.
Explains why FEMA and the CDC use zombies as a teaching tool in emergency preparedness to make information more accessible and engaging.
Touches on the importance of planning for surviving zombies as a way to prepare for other disasters like snowstorms or hurricanes.
Addresses a question about starting a fire with wet wood, explaining that even wet wood may have a dry core that can be utilized with more effort.
Encourages viewers to think about emergency preparedness and skills required for various scenarios like starting a fire with wet wood.
Emphasizes the need to be prepared for future emergencies and disasters by honing necessary survival skills.
Suggests setting wet wood near a fire to dry out and mentions that wet wood tends to be harder to keep burning and produces more smoke.
Commends those who ask questions and seek to be prepared for future emergencies by developing necessary skills.
Encourages viewers to contemplate what they would do in a zombie apocalypse scenario as a mental exercise in preparedness.

Actions:

for community members,
Contemplate emergency preparedness scenarios and develop necessary survival skills (implied)
</details>
<details>
<summary>
2021-03-06: Let's talk about Biden, solutions, and the American character.... (<a href="https://youtube.com/watch?v=z52Yj1wkSIs">watch</a> || <a href="/videos/2021/03/06/Lets_talk_about_Biden_solutions_and_the_American_character">transcript &amp; editable summary</a>)

President Biden's leaked asylum plan prompts Beau to question the lack of previous solutions, criticize the manufactured crisis at the border, and celebrate the American character's ability to innovate and tackle challenges confidently.

</summary>

"Do they actually have a real tangible common sense solution to whatever issue it is that they are campaigning on, or do they just have cool talking points?"
"The idea that a line of people wanting to come here is somehow a major issue is laughable."
"It is not a crisis, except for maybe a crisis of leadership."

### AI summary (High error rate! Edit errors on video page)

President Biden's leaked plan aims to reform the failed American asylum system.
The plan involves asylum seekers being processed and sent to their planned destinations with sponsors.
Beau supports the focus on processing rather than detaining asylum seekers.
Raises the question of why previous administrations didn't implement similar solutions.
Criticizes those creating a crisis at the border, stating it just requires planning.
Believes Americans excel at handling chaos and crisis due to their confidence and innovation.
Challenges the notion of a line of people coming to the U.S. as a fundamental issue.
Argues that the American character is characterized by the confidence to deal with any challenge.
Emphasizes that every demographic that has come to America has made the country stronger.
Calls for addressing real issues like climate change, systemic racism, and lack of leadership.

Actions:

for advocates for change,
Address real issues like climate change and systemic racism (implied)
Focus on finding solutions and putting in effort to lead (exemplified)
</details>
<details>
<summary>
2021-03-05: Let's talk about Abbott, Neanderthal thinking, Biden, and immigrants.... (<a href="https://youtube.com/watch?v=5W7_52-xIPw">watch</a> || <a href="/videos/2021/03/05/Lets_talk_about_Abbott_Neanderthal_thinking_Biden_and_immigrants">transcript &amp; editable summary</a>)

Governor Abbott's blame game on immigrants diverts attention from real issues, despite statistics showing countries of origin faring better in preventing COVID-19 than Texas.

</summary>

"Blame the brown person."
"This is bad for everybody."
"They are still better at preventing new cases than Texas."

### AI summary (High error rate! Edit errors on video page)

Governor Abbott used a political move to shift blame onto immigrants for potential COVID-19 spread in Texas.
Abbott's tactic of blaming immigrants is a common political strategy in American history to deflect responsibility.
Despite the blame game, statistics show that countries immigrants are likely from, like Honduras, Nicaragua, El Salvador, and Mexico, are doing better at preventing new COVID-19 cases compared to Texas.
Even with potentially underreported numbers, these countries still fare better in controlling the virus than Texas.
Abbott's playbook move of blaming immigrants is harmful and diverts attention from real issues, affecting everyone negatively.
The tactic of blaming immigrants rather than accepting responsibility is outdated and needs to change.
The blame game only serves to harm marginalized groups and society as a whole.
Immigrants are often scapegoated instead of holding powerful figures accountable for their actions.
Despite poorer healthcare systems in countries like Nicaragua, El Salvador, and Honduras, they are still more successful at preventing new COVID-19 cases than Texas.
The blame game perpetuates harmful narratives and fails to address the real challenges effectively.

Actions:

for texans, policymakers, activists,
Confront harmful narratives against immigrants (exemplified)
Advocate for accountability rather than scapegoating (exemplified)
</details>
<details>
<summary>
2021-03-04: Let's talk about questions prompted by Black History Month.... (<a href="https://youtube.com/watch?v=IL_cqtIaYJo">watch</a> || <a href="/videos/2021/03/04/Lets_talk_about_questions_prompted_by_Black_History_Month">transcript &amp; editable summary</a>)

Beau addresses the presence of various heritage months, debunking misconceptions and underscoring the importance of Black History Month amidst attempts to diminish it.

</summary>

"They all exist."
"The goal wasn't to find anything out. The goal was to try to take away from Black History Month."
"It's more important for black Americans to be able to look into their history a little bit."
"I will never understand the desire of wanting to take something away from somebody that obviously matters to them for no reason."
"Inequality and a lot of injustice that is eventually going to have to be addressed."

### AI summary (High error rate! Edit errors on video page)

He was repeatedly asked about the absence of other heritage months, like Irish, Asian, Native, Jewish, and Latino, during Black History Month.
Explains the origins and formalization of each of these heritage months, pointing out that they already exist in federal law.
Notes the lack of Latino Heritage Month, but mentions Hispanic Heritage Month, which was formalized into law in 1988.
Questions the intentions behind the repeated inquiries, suggesting they were not in good faith.
Emphasizes the importance of Black History Month for black Americans to connect with their history and heritage.
Acknowledges the lack of cool origin stories for black Americans due to the impact of slavery on their lineage.
Criticizes those who attempt to diminish the significance of Black History Month.
Calls for addressing inequality and injustice that have resulted from historical practices.
Expresses confusion over the desire to detract from something meaningful to others for no valid reason.
Concludes by wishing a happy Irish-American Heritage Month and Women's History Month.

Actions:

for advocates for social justice,
Celebrate and uplift different heritage months within your community (suggested)
Educate others about the significance of heritage months and the importance of recognizing diverse histories (suggested)
Challenge misconceptions and misconstrued narratives surrounding heritage months (implied)
</details>
<details>
<summary>
2021-03-04: Let's talk about Congress limiting Biden.... (<a href="https://youtube.com/watch?v=ivDc53KNAa0">watch</a> || <a href="/videos/2021/03/04/Lets_talk_about_Congress_limiting_Biden">transcript &amp; editable summary</a>)

Congress may restrict President Biden's military authority, creating an organic chain of events that benefits US leadership and foreign policy goals, but defense contractors might not be pleased.

</summary>

"Hold me back, boys."
"America is back, baby."
"Supporting this is definitely the right thing to do."
"Yanking the authorization for the use of military force is a good thing."
"Hold me back, boys. I wouldn't do it too hard because I want to get through because I want this image if I'm Biden."

### AI summary (High error rate! Edit errors on video page)

Congress is considering legislation to limit President Biden's authority to use military force in response to recent events.
This legislation aims to revoke Biden's authorization to respond independently, forcing him to seek congressional approval.
The intention behind this legislation is to reprimand Biden for his actions and restrict his ability to act unilaterally.
By stripping Biden of this authority, Congress sends a message that he was willing to use military force without restraint.
Despite potential partisan concerns, this move could actually benefit both Democrats and Republicans by allowing Biden to achieve his foreign policy goals.
Limiting Biden's authorization could also signal a return to the rule of law and a stronger U.S. leadership position.
The restriction on military force authorization may discourage Iran from provocative actions, as they could interpret Biden's restraint as a victory.
Some view this legislative action as a strategic move that plays into Biden's desired image as a strong leader reined in by Congress.
Supporting this limitation on military force authorization is seen as a positive step for the country as a whole, except for defense contractors.
Ultimately, this legislation could set the stage for a more deliberate and restrained approach to foreign policy decisions.

Actions:

for policy analysts,
Support the legislation to limit President Biden's authorization to use military force (suggested).
Advocate for a more deliberate and restrained approach to foreign policy decisions (implied).
</details>
<details>
<summary>
2021-03-03: Let's talk about a message to Texas from Florida man.... (<a href="https://youtube.com/watch?v=qx9RUQpLHbo">watch</a> || <a href="/videos/2021/03/03/Lets_talk_about_a_message_to_Texas_from_Florida_man">transcript &amp; editable summary</a>)

Beau urges Texans to exercise common sense, warning against ignoring safety measures and reminding them of the shared responsibility in protecting public health.

</summary>

"My mask protects you. Your mask protects me."
"Freedom comes with a responsibility."
"Y’all are supposed to be tough."
"Please don't prolong this."
"Wash your hands. Don't touch your face."

### AI summary (High error rate! Edit errors on video page)

Message to Texas from Florida on removal of mandates.
Florida operates independently from state mandates.
Governor DeSantis' orders ignored by counties in Florida.
Lack of reliance on Florida state government due to past experiences.
Common sense needed despite no legal requirement for masks.
Warning against potential consequences of not following safety measures.
Importance of considering vaccine effectiveness and preventing mutations.
Advocating for basic safety measures like handwashing and mask-wearing.
Urging Texans to uphold common defense and general welfare responsibilities.
Emphasizing the mutual protection aspect of wearing masks.
Encouraging responsible freedom that considers the well-being of all.
Plea to avoid prolonging the pandemic by adhering to safety guidelines.

Actions:

for texans, public health advocates,
Wear a mask or two or three and practice common sense in public places (exemplified).
Wash hands regularly, avoid touching face, and stay at home as much as possible (exemplified).
</details>
<details>
<summary>
2021-03-03: Let's talk about Biden's replacement for Neera Tanden.... (<a href="https://youtube.com/watch?v=2VAxyoEmyXU">watch</a> || <a href="/videos/2021/03/03/Lets_talk_about_Biden_s_replacement_for_Neera_Tanden">transcript &amp; editable summary</a>)

Neera Tanden's replacement options range from establishment figures to a wild card candidate, revealing Biden's potential direction towards status quo or progressiveness.

</summary>

"Neera Tanden's nomination for Director of the Office of Management and Budget has been or will be withdrawn soon because she is considered very establishment and status quo."
"Young is seen as a viable candidate due to her lack of political baggage, even though she may not fit the typical Democratic advisor mold."

### AI summary (High error rate! Edit errors on video page)

Neera Tanden's nomination for Director of the Office of Management and Budget has been or will be withdrawn soon because she is considered very establishment and status quo.
Possible replacements for Tanden include Gene Sperling, Ann O'Leary, and Shalanda Young, with Young being a wild card choice.
Gene Sperling and Ann O'Leary have ties to the Clinton-Obama administration, with O'Leary being a bit more progressive in her politics.
Shalanda Young, a potential wild card replacement, is well-liked by both Republicans and left-leaning Democrats, but not much is known about her stances.
Young is seen as a viable candidate due to her lack of political baggage, even though she may not fit the typical Democratic advisor mold.
Biden's choice for Tanden's replacement will reveal his intentions - whether he aims for a status quo candidate or someone with a more progressive approach.
The final decision will depend on Biden's working relationship with the candidates and how willing he is to elevate Young to the top slot.

Actions:

for political observers,
Support a candidate who represents progressive values in government (implied)
Stay informed about the potential replacements for Neera Tanden (implied)
</details>
<details>
<summary>
2021-03-02: Let's talk about foreign policy and leaving over there.... (<a href="https://youtube.com/watch?v=nO2FypF5LOQ">watch</a> || <a href="/videos/2021/03/02/Lets_talk_about_foreign_policy_and_leaving_over_there">transcript &amp; editable summary</a>)

Foreign policy is about power, not patriotism; Biden aims to reshape the Middle East's power balance by elevating Iran and fostering cooperation over subjugation.

</summary>

"Foreign policy is about power, not about right and wrong."
"It's about power. And when you're talking about the Middle East, you're talking about unimaginable power."
"Cooperation rather than subjugation will move humanity further faster."

### AI summary (High error rate! Edit errors on video page)

Foreign policy is about power, not patriotism or ideology.
American foreign policy is complex and cannot be simplified into slogans.
The discrepancy between what should happen in foreign policy and what actually happens is vast.
When discussing leaving the Middle East, the reality is more complex than just withdrawing troops.
Beau explains the power dynamics in the Middle East, focusing on Iran, Saudi Arabia, Israel, and Turkey.
Biden's foreign policy centerpiece involves reshaping the balance of power in the Middle East by bringing Iran to the forefront.
The goal is to create a system with multiple equal poles of power in the region.
Deprioritizing military presence in the Middle East and focusing on humanitarian aid is suggested.
Beau stresses the importance of understanding the power dynamics and motives involved in foreign policy decisions.
Cooperation over subjugation is presented as a more progressive approach to international relations.

Actions:

for foreign policy analysts,
Advocate for deprioritizing military presence in the Middle East through humanitarian aid efforts (implied)
Understand the power dynamics and motives behind foreign policy decisions (implied)
Push for cooperation over subjugation in international relations (implied)
</details>
<details>
<summary>
2021-03-02: Let's talk about Cuomo and what men can learn.... (<a href="https://youtube.com/watch?v=nn8jWioayXc">watch</a> || <a href="/videos/2021/03/02/Lets_talk_about_Cuomo_and_what_men_can_learn">transcript &amp; editable summary</a>)

Men in the US should refrain from flirting at work to prevent creating vulnerable situations for women, maintaining professionalism and respect.

</summary>

"It's your society."
"Don't exploit vulnerabilities and keep your hands to yourself."
"Don't say anything you wouldn't say if HR was standing there."
"The confusion, I don't buy it either because we know the rules."
"How's that for PC?"

### AI summary (High error rate! Edit errors on video page)

Explains the confusion among men in the United States regarding flirting at work post-Cuomo situation.
Points out that the motivation behind changing behavior doesn't matter as long as the behavior changes.
Emphasizes the shift in responsibility from women avoiding vulnerable situations to men not creating them.
Simplifies the rules for men by stating that they shouldn't flirt at work to avoid putting women in vulnerable positions.
Advises to keep distance, not close doors, or block exits to maintain a professional environment.
Urges men not to exploit power dynamics or vulnerabilities in interactions with women.
Suggests having HR present for private interactions to ensure appropriateness and avoid misconduct.
Criticizes the excuse of not knowing the rules, implying that men do understand but may be looking for ways to bend them.
Argues that true flirting is subtle and not graphic or overt as often claimed in harassment cases.
Encourages watching a video on consent and reiterates the simple rules of respecting boundaries and keeping interactions professional.

Actions:

for men in the us,
Keep distance, avoid blocking exits, and refrain from inappropriate advances (implied)
Have HR present for private interactions to maintain professionalism (implied)
</details>
<details>
<summary>
2021-03-01: Let's talk about what Trump missed at CPAC (Part 1).... (<a href="https://youtube.com/watch?v=0JlbPsG-fFg">watch</a> || <a href="/videos/2021/03/01/Lets_talk_about_what_Trump_missed_at_CPAC_Part_1">transcript &amp; editable summary</a>)

CPAC was a Trump praise-a-thon with lackluster policy support, potentially leading the Republican Party into a losing recipe for political ineffectiveness while Trump's children's political aspirations could sow discord.

</summary>

"Trump doesn't have policy. He never did. He has sound bites."
"The reality is our biggest trade deficit with China was in the 300s, not $500 billion as claimed."
"DeSantis and Hawley were, even if you like them, copies of a losing candidate."
"The Republican Party is at its weakest point in decades."
"If the Democrats can muster the courage and seize upon this, Biden can get the one thing he truly wants."

### AI summary (High error rate! Edit errors on video page)

CPAC was a three-day event that turned out to be a Trump praise-a-thon, with hand-picked speakers solely to commend Trump.
95% of CPAC attendees supported carrying forward Trump's policy and agenda, but only 55% preferred Trump as their candidate.
The Republican Party is likely to adopt Trump's policies but have someone else deliver the message, possibly DeSantis or Hawley.
Trump's policies were unsuccessful, lacking substance and filled with lies about successes during his term.
DeSantis and Hawley, potential messengers for Trump's policies, lack his charisma and ability to spin failures positively.
The Republican Party may face political ineffectiveness if they continue to follow Trump's policy direction without control from figures like McConnell or Romney.
Trump's children, who have political aspirations, may sow discord within the party by blaming others for failed policies to pave the way for their own political ambitions.
The Republican Party is currently at a weak point, and the Democrats could capitalize on this vulnerability.

Actions:

for political observers,
Support political figures with comprehensive policies and integrity to avoid falling into the trap of charisma-based politics (implied)
Advocate for a diverse range of voices within the Republican Party to prevent political stagnation and ineffectiveness (implied)
Stay informed and engaged in political discourse to understand the dynamics within parties and potential impacts on policies (implied)
</details>
<details>
<summary>
2021-03-01: Let's talk about what Biden missed at CPAC (Part 2).... (<a href="https://youtube.com/watch?v=UCooPhleXSk">watch</a> || <a href="/videos/2021/03/01/Lets_talk_about_what_Biden_missed_at_CPAC_Part_2">transcript &amp; editable summary</a>)

CPAC events create a window of disunity in the Republican Party for Biden to pursue ambitious FDR-like policies, appealing to TikTok teens and the real left for long-term success.

</summary>

"They're going to be out there talking about Oreos, Dr. Seuss, and Mr. Potato Head."
"He wants to be the next FDR. He kind of has to deliver what FDR delivered."
"He has to deliver for the TikTok teens."
"If you do not deliver for them, they will not show up."
"He has to deliver for voters who can't even vote yet."

### AI summary (High error rate! Edit errors on video page)

CPAC events have a significant impact on Biden, with Republicans appearing less united, carrying Trump's talking points.
Trump's base is expected to focus on culture war issues like Oreos, Dr. Seuss, and Mr. Potato Head, which may be ineffective.
Lack of unity in the Republican Party provides Biden with an opening to pursue his ambitious goals.
Biden aims to emulate FDR by delivering policies that benefit the working class, unions, and infrastructure, albeit without labeling it as the Green New Deal.
To secure support for his vision, Biden must appeal to TikTok teens and the real left, including figures like AOC.
Failure to deliver for the younger generation may result in disengagement and political consequences.
Biden needs to act swiftly, without making excuses or catering to billionaire interests, to secure the support he needs for his long-term goals.

Actions:

for politically engaged citizens,
Appeal to teenagers and deliver policies that resonate with them (implied)
Engage with the real left, including figures like AOC (implied)
Advocate for policies that support the working class and infrastructure development (implied)
</details>
