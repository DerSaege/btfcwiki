---
title: Let's talk about a one year anniversary, failure, and hope....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ajvVjq2XZx4) |
| Published | 2021/03/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- One year since the global mess began officially with 118 million cases and 2.6 million lives lost globally.
- The United States, with less than 5% of the world's population, has lost over 500,000 lives, almost 20% of the total.
- Despite having infrastructure, research, plans, and capabilities to mitigate, the US failed badly in handling the pandemic.
- Lack of leadership was a significant factor, with some downplaying the situation even with evidence against them.
- People in the public eye failed the country by ignoring the severity of the situation and pushing false narratives.
- Beau questions how the US, with all its resources, became the leader in COVID-19 deaths.
- The US failed to recognize danger due to comfort and complacency, leading to catastrophic consequences.
- Beau criticizes the focus on trivial issues like Dr. Seuss books instead of addressing the nation's failures.
- Despite the failures, Beau sees a glimmer of hope in the readiness for change and improvement post-pandemic.
- He calls for strong leadership, community building, and resilience to prevent such failures in the future.

### Quotes

- "The United States failed. Five percent of the world's population and a little less than 20% of the loss."
- "We're nearing the end of this, hopefully. And when we come out of it, we have to be ready to get to work."

### Oneliner

One year into the pandemic, Beau questions the US's failure in handling COVID-19 and calls for leadership and community resilience to prevent future disasters.

### Audience

General public, policymakers

### On-the-ground actions from transcript

- Build community resilience to tackle future crises (implied)
- Advocate for strong leadership in handling crises (implied)
- Focus on addressing critical issues rather than distractions (implied)

### Whats missing in summary

The emotional impact and personal reflection from watching Beau's message.

### Tags

#COVID19 #PandemicResponse #Leadership #CommunityResilience #USFailure #Change


## Transcript
Well howdy there internet people, it's Beau again.
So today marks one year since this giant mess started officially.
A year ago today, all of this was declared a giant global mess.
And it has been a wild ride.
In that time, globally, there have been 118 million cases.
2.6 million lost.
In the United States, we have lost more than 500,000.
Little less than 20%.
You know the thing is, we have less than 5% of the world's population.
So we have 20% of the lost.
How did that happen?
How did that occur?
We have the infrastructure.
We have the research.
We have the plans.
We have the capability to mitigate.
So how did we fail this badly?
What happened?
What were we missing?
Leadership.
And I'm not just talking about the one guy, although he certainly had a lot to do with
it.
Talking about other people who were in the public eye.
People who downplayed it at every opportunity.
Even once evidence emerged that they were wrong.
Even once it started to spread.
Even once it became apparent that the plans that we've had on the books that had protected
us in the past either weren't being enacted or weren't going to work.
They still downplayed it.
Those people failed this country.
They failed 500,000 people.
And I know somebody's going to say, well, the other countries are obviously underreporting.
Yes.
But the entire world is lying.
The entire world is lying.
It's not because in the United States we had a whole bunch of people convincing others
that masks were the enemy.
That everybody should just go about their lives, sacrifice themselves on the altar of
the stock market.
Couldn't be that.
Everybody else is lying.
And even if they were, understand, the amount of underreporting, it would have to take place.
It would have to take place to even remotely make this okay.
It's not realistic.
It's not realistic.
We failed.
The United States failed.
Five percent, less than five percent of the world's population and a little less than
20 percent of the loss.
In the most powerful country on the planet, we're number one.
USA.
And people will wave their flags.
How did that happen?
Because there are people in this country that have become experts at pushing people's buttons
and getting them to ignore what matters.
Because we're the United States.
We've been so comfortable for so long that we can't recognize danger on danger's face.
Everything will be all right.
It can't happen here.
USA.
And these are the same people today that, while all of the problems in this country
have been exposed over the last year, they have their followers focusing on Dr. Seuss
books and getting them to say that there's no way there's racist imagery in it.
Five of the six books that were published before segregation ended.
Because that's what matters.
Not the fact that we're just a complete failure.
As a nation, something very basic that a nation should be capable of dealing with.
We failed.
There is a bit of good news in this.
All of the problems were light bear.
And on some level, I feel incredibly lucky to be alive right now.
And not because I'm not one of the 500,000, but because the ground is ready.
This country is ready for change.
The question is, are we going to have the leadership?
Are we going to have the people step up?
And move this country forward?
And tackle the tough issues?
Or are we going to continue to just be against stuff?
Are we going to come up with a plan?
Are we going to work to make our communities stronger and better and more resilient so
this doesn't happen again?
So this kind of failure can't occur because of some talking heads.
I hope so.
I hope that's where we're headed.
I would like to see that myself.
We're nearing the end of this.
Unless people can be convinced to put themselves in situations where it just reignites another
surge.
But we're nearing the end of this, hopefully.
And when we come out of it, we have to be ready to get to work.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}