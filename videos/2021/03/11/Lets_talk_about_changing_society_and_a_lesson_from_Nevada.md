---
title: Let's talk about changing society and a lesson from Nevada....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KE-dO6pYMRQ) |
| Published | 2021/03/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the concept of "breaking the hammer" as a defensive strategy to achieve societal change.
- Attributes Trump not being in office to four years of activist efforts through campaigning, organizing, and educating.
- Emphasizes that merely being against something doesn't bring the desired societal change; it only slows down what you oppose.
- States that building a just world is challenging compared to opposing injustice, which is relatively easy.
- Advocates for going on the offensive by establishing Community Networks to create political change rapidly.
- Cites the example of Nevada where the progressive part of the Democratic Party took over through years of organizing.
- Stresses the importance of offering solutions and building something better rather than solely opposing existing systems.
- Contrasts the approach of Trump, who capitalized on grievances without offering substantial solutions, with the need for a constructive plan to create real change.
- Points out that being against systemic issues or policies is insufficient; one must have a plan to address and replace them.
- Concludes by urging people to put in the work to build the society they desire rather than merely opposing the current state of affairs.

### Quotes

- "Building a just world is hard. Being against injustice, it's even easy socially to take that stance."
- "Being against something doesn't actually get you the society you want. Doesn't get you the world you want. It just slows the world you don't."
- "You have to put in the work to build the society you want, not just be against what exists."

### Oneliner

Beau touches on defensive and offensive strategies for societal change, advocating for Community Networks and constructive planning over mere opposition.

### Audience

Change-makers, Activists, Community Leaders

### On-the-ground actions from transcript

- Establish Community Networks to create rapid political change (implied)
- Offer solutions and build something better to attract support (implied)
- Organize and mobilize for systemic change (implied)

### Whats missing in summary

The full transcript provides additional depth on the importance of proactive efforts, community organizing, and the necessity of constructive solutions for societal transformation.

### Tags

#SocietalChange #CommunityNetworks #Activism #BuildingBetterSociety #ProactiveApproach


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to touch a little bit more on how to change the world, how to create
that society we want.
And we're going to talk a little bit more about breaking the hammer.
It is a concept that we've touched on a couple of times on this channel.
The general idea of it is that the average person, well they're the anvil, and if they
can just hold the form long enough, no matter how many hits they take, eventually they'll
break the hammer.
It's a defensive strategy.
It is a defensive strategy and it's one that was used for the last four years.
Why isn't Trump in office right now?
Did we wish him out of office?
Of course not.
Trump not being in office is the result of four years of concerted effort from activists
in the field and by those who are engaged in electoralism.
Campaigning, petitioning, organizing, agitating, educating, these things got rid of Trump.
That's what happened.
At that point, it's defensive.
People were able to identify his specific brand of authoritarianism, they knew the danger
and they were willing to hold the line against it.
Eventually the hammer broke.
That's what happened.
Here's the bad news.
Being against something doesn't actually get you the society you want.
Doesn't get you the world you want.
It just slows the world you don't.
That's it.
Being against an unjust world is easy.
It is easy.
Building a just world is hard.
Being against injustice, it's even easy socially to take that stance.
Sometimes you don't even have a choice because you are literally the one who's going to be
impacted.
You're the piece of the anvil that's going to get hit.
So of course you're going to be against it.
That's easy.
To take a public stand against something that everybody knows is wrong, or at least a majority
of people knows is wrong, is easy.
Against something that people think is right, that's hard.
Because you have to put yourself out there with ideas and solutions and you have to work
a lot.
How do you do this?
How do you actually affect change by going on the offensive?
We talk about it all the time on this channel, Community Networks.
When I normally talk about it, most times I tend to try to appeal to people's self-interest
because it typically takes self-interest to get motivated and start something like that.
But understand, Community Networks, once established, can create a lot of political change very
quickly.
Don't believe me?
Look at Nevada.
If you don't know what happened there, the progressive part of the Democratic Party just
took over the state apparatus, like all of it.
So upsetting to the Democratic establishment that all the employees quit.
How did that happen?
How did they get rid of the machine, the Democratic machine that existed there?
Years of organizing.
It wasn't done all at once.
It was step by step.
And it culminated in them sweeping.
And now they control all the offices.
They control the Democratic establishment there, the Democratic Party, the structure.
And they did it by building what they wanted, not by being against what existed.
I mean, yeah, that's part of it.
You know it's wrong.
But you have to have a plan to draw people to your side.
You have to offer something better.
You can't just say, this is bad.
Everybody knows it's bad.
And if you take that approach, if you take the approach of just being against whatever
is in existence and peddling to people's grievances, you get Trump.
That's what he did.
He took the grievances that everybody had and blended them with tradition.
That's how he was able to establish his base.
He didn't offer them anything better.
He offered them a myth.
If you want to create something better, you have to have a plan.
You have to build it.
Being against systemic racism, that doesn't actually end it.
You have to have a plan to end it.
Being against our foreign policy doesn't actually end it.
You have to have a plan to end it.
Being against the corporate nature of various political parties doesn't stop them from catering
to that corporate interest.
You have to build it.
You have to build the infrastructure, a separate power structure that will attract people.
And then you can create real change.
This current trend of just opposing everything, it's not going to work.
I mean, sure, it doesn't require any real risk.
It doesn't require any political capital to say, I'm against something that's bad.
You have to be for something as well.
You have to put in the work to build the society you want, not just be against what exists.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}