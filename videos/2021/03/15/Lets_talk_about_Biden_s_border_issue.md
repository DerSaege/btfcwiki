---
title: Let's talk about Biden's border issue....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Sh7tcjZL5a4) |
| Published | 2021/03/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Conditions are dire down south, focusing on finding solutions rather than dwelling on current conditions.
- The problem lies in processing delays, causing overcrowding and poor conditions for unaccompanied minors.
- The solution is to expedite processing to alleviate overcrowding and improve conditions.
- Biden's plan involves processing within 72 hours, but delays have caused a backlog.
- Bringing in FEMA to assist with processing is a positive step taken by Biden.
- The goal is swift processing to transfer minors to Health and Human Services and then to sponsors promptly.
- While conditions may be better than under Trump, the current standard is still unacceptable.
- Swift processing is key to improving conditions and reducing overcrowding.
- Biden's administration aims to process minors quickly and get them out of custody, contrasting with Trump's approach.
- Any backlog issues should be anticipated and addressed proactively by the administration.

### Quotes

- "Just better than Trump is not good enough in this regard."
- "The goal is to get them processed."
- "Full stop."
- "Bad news is that more than likely, as soon as this happens, there may be a backlog at the next station."
- "The long line is Trump's fault."

### Oneliner

Conditions are dire, focus on expedited processing to improve overcrowding and poor conditions for unaccompanied minors, with Biden's administration taking steps to rectify the situation.

### Audience

Advocates, policymakers, community members

### On-the-ground actions from transcript

- Support expedited processing of unaccompanied minors to alleviate overcrowding and improve conditions (implied).
- Stay informed about the situation and advocate for proactive solutions within your community (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the current conditions, the importance of swift processing, and the comparison between Biden's and Trump's approaches to immigration policies.

### Tags

#Immigration #BidenAdministration #ProcessingDelays #Conditions #Overcrowding


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about what's going on down south, how the problem might
be solved, what Biden's plan is, and in the process we're going to do a little hip pocket
class on troubleshooting logistical issues.
Okay so what's going on?
The conditions are bad.
Yes, they are.
They are bad.
We will talk about how bad here in a little bit.
But we can't focus on that right now because we want to come up with a solution.
Talking about the conditions does not help right now because we're trying to come up
with a plan.
We're trying to fix it.
The conditions are the symptom.
The problem is further down the line.
So to diagnose the problem you have to know what the plan was.
What's the plan?
They get picked up, they're processed within 72 hours, they're out to help in human services,
and then they go to their sponsors.
That's the plan.
There's nothing wrong with that plan.
It is a good plan.
The plan doesn't need to change.
So what's happening?
They're not getting processed within 72 hours.
That's what's occurring.
Because they are not getting processed within 72 hours, there's a backlog which leads to
overcrowding, which leads to bad conditions.
So as weird as it is, right now we don't want to say fix the conditions.
We want to get them processed out faster.
Because if you say fix the conditions, what's the government solution?
Build more facilities.
Who would those facilities be run by more than likely?
Contract companies.
Once those kids are a stream of revenue, it is over.
It will always be a stream of revenue.
Can't do that.
The goal is to get them processed and over to Health and Human Services and then processed
from there and on to their sponsor quickly.
Right now, my understanding is that rather than three days, it is taking five to eight.
Doesn't sound like a lot.
But if the number you have coming in is more than you have going out, it doesn't take long
for it to become an issue.
Now for the record, Biden has already made his decision and he made the right one.
They're bringing in FEMA to assist with processing.
That's going to be their primary thing.
At least that's what we've been told.
If that's the case, then this should sort itself out pretty quickly.
He does have another card to play.
There are other agencies that can be brought in to help with this.
However, optics wise, it's not something you want.
It's not something the Biden administration wants and most people don't want these other
agencies dealing with the kids.
So at this point, let's see if this works.
Okay, so then the next question is, how bad are the conditions?
Bad.
They're bad.
When I see people talk about this, they say that it's not as bad as it was under Trump.
I mean, yeah, that's true, but that is not a standard.
That's not a standard.
We can't go by that.
Just better than Trump is not good enough in this regard.
It is better than Trump.
It's actually a lot better than Trump, but it's still bad because Trump's policies were
that bad.
In this case, the cruelty isn't the point.
These aren't kids who were ripped from their parents.
These are kids who showed up alone.
Goal is to get them processed.
If you process them quickly and get them to their sponsors, their friends and family,
they will no longer be there taking up a spot.
It will no longer be overcrowded.
The conditions will by default get better because the poor conditions right now are
a result of them being overcrowded.
The standard of where it's at right now, it isn't acceptable.
Full stop.
How do we fix it?
This way, processing.
That's got to be the goal.
With Trump, it was a little different because he actually wanted to keep them in custody.
In this case, the Biden administration doesn't want to.
They want them processed and they want them out.
It's the right move.
So good news is that he has made the right moves.
So it should work.
Bad news is that more than likely, as soon as this happens, there may be a backlog at
the next station.
And that's where I'm hoping the Biden administration is thinking ahead because they weren't here.
If you go back and look, and not just at mine, at pretty much anybody's videos who was talking
about what happened when the Trump administration stopped processing asylum claims and slowed
all of this down and was intentionally not doing what we are obligated to under international
law, treaty and the Constitution, everybody said that as soon as we reopened to do what
we were supposed to, there would be a long line.
Maybe they just weren't prepared for this particular demographic of unaccompanied minors.
It seems like something the Biden administration should have planned for.
I'm hoping that they understand that once they're being processed quickly out of Border
Patrol, that means they're showing up at Health and Human Services.
They're going to be in their realm now.
So they're going to have more to process because they're getting them faster than they were
before.
There's going to be multiple phases to this.
We do not have information on whether or not the people from FEMA are coming in to do both.
We hope they are.
That's the right move.
So is this Biden's fault?
No.
The long line is Trump's fault.
The backlog, yeah, you can lay that at his feet.
You can, fairly.
I think that somebody on his team should have seen this coming.
And it should be, it should have been dealt with.
Now is it a mistake?
Yeah.
Is it a forgivable one?
All depends on what happens now.
If the problem gets fixed, yeah, it's completely forgivable.
It happens.
If it doesn't, it's not an oversight, it's apathy.
So we just have to wait and see.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}