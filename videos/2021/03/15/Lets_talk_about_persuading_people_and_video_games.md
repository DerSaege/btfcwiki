---
title: Let's talk about persuading people and video games....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ERNidN-cju8) |
| Published | 2021/03/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the importance of giving people the tools they need in the moment when persuading them politically.
- Compares this approach to how video games provide players with necessary tools at critical moments.
- Advises against overwhelming individuals with too much information all at once.
- Emphasizes the effectiveness of presenting small, digestible bits of information.
- Points out the necessity of meeting people where they are on their personal paths.
- Warns that going beyond what someone is ready to accept may do more harm than good.
- Stresses the significance of sticking to a specific topic when discussing with others.
- Suggests that focusing on objectively false topics can erode trust in misleading sources.
- Shares personal experience of gradually shifting people's views by providing information over time.
- Encourages framing arguments in a way that can be easily accepted by individuals with varying perspectives.

### Quotes

- "It's dangerous to go alone. Take this."
- "You can't teach algebra to people who can't act."
- "Either that initial source is incompetent and can't gather basic information or it's intentionally misleading."
- "We should probably try to frame our arguments so they can be more easily accepted."
- "For a lot of people it's too much to take in at one time."

### Oneliner

Beau explains the importance of providing people with the right tools at the right time when persuading them politically, cautioning against overwhelming with too much information and stressing the effectiveness of small, digestible bits in shifting viewpoints gradually.

### Audience

Political activists

### On-the-ground actions from transcript

- Provide small, digestible bits of information to individuals to persuade them politically (exemplified).
- Frame arguments in a way that can be easily accepted by those with different perspectives (suggested).
- Gradually shift people's views by providing information over time (exemplified).

### Whats missing in summary

The full transcript provides a detailed guide on effectively persuading individuals politically through gradual information sharing and framing arguments for better acceptance.

### Tags

#Persuasion #PoliticalActivism #InformationSharing #CommunityEngagement #ProgressiveMessaging


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about persuading people,
politically,
how it might be best
to approach those that you are trying to change the world view of
and what video games can teach us about that.
We're doing this because I had a couple of people ask why I didn't push back on
certain ideas that were contained in
Carlson's little rant that they know I don't
believe are true but I just kind of let them slide.
I would imagine that most people have played a video game
and you are probably aware that at some point
a character or an event will happen that will give you the exact tool you need
just before you need it.
It's dangerous to go alone.
Take this.
You should probably try to mimic that
when you are trying to persuade somebody politically.
Give them the tool they need at the moment they need it.
Don't give them too much at once.
Small digestible bits of information
are a whole lot more effective.
It's been my experience anyway.
A lot of times
if you are talking to somebody who
is dead set on one particular issue
such as the retention thing
and they're using that as an excuse
because they want to kick down and marginalize
this group of people.
Pushing back on a wider idea
such as our foreign policy is good and that's what makes American safe
that's probably going to be very ineffective
if you go beyond that. You're talking about somebody who doesn't
they want to marginalize people within the United States.
They are not going to care about the motivations of those overseas.
So it's going to be very hard
to make the argument
that our foreign policy
really isn't that great
and that in fact a lot of
a lot of the bad things that have made Americans unsafe
are because of it.
That's probably beyond
the scope of what they're ready to talk about.
We have to acknowledge that everybody is on a path.
Me, you,
the person who sent that message,
everybody is on a path. We're all in different spots.
We have to meet each other where we're at.
If you try to go too far beyond
what
the person is ready to accept
it may do more harm than good
because not only will they discount the extra stuff that you've added
they may use that
as a reason
to blow off your main point about the question they asked.
It's that old thing, what is it, informed people sound like insane people to ignorant people,
something along those lines.
It's what it boils down to.
They will be able to disregard
everything that you've said
if you go too far, if you get too far outside their comfort zone.
You can't teach algebra to people who can't act.
And everybody,
doesn't matter how
informed you are,
there is somebody on the path ahead of you
and behind you.
You have to frame your arguments
in that way. You have to meet them where they're at.
And I have found it to be more effective
if it's small digestible pieces of information.
Something you can tell them something one day
and then maybe the next day add something else
and then something else.
And as time goes on
their view shifts.
But most importantly
when you're talking about something with
like the retention thing,
it's most important
because that's a perfect one.
It's objectively false.
Like you can show it. Here you go. This is why it happens. Boom, boom, boom.
Stick straight
to that topic.
Don't deviate.
Because at the end of it
they're left with only a couple of options about their initial source of information.
Either
that initial source, that propaganda source
is incompetent
and can't gather basic information
or it's intentionally misleading.
In either way it helps erode
the trust they have
in that propaganda source that has got their hooks in them.
So that's the reason I do it that way.
And I do this with a lot of topics. There are a lot of times when
I actually have to bite my tongue on some of them
because it's
when it brushes up against topics that I really care about
I kind of have to
consciously hold back
because I want to go into other stuff.
However, I want the information
that is most useful, the tool they need at that moment
to be handed to them
in a way they can't
refuse to accept.
Now that's all based on my experience
as to what is most effective.
Other people may have more success other ways.
And if you
aren't doing it
to actually change the person's mind or persuade them,
if you're just doing it to feel better,
that's an entirely different story. Unload.
Because while it may not help them,
somebody who is in the comments section or somebody who overhears it,
it may be giving them the tool they need,
that extra information you provide.
We should probably try,
given the fact that
people who are more progressive
tend to look at bigger pictures,
tend to have more nuance,
we should probably try to frame our arguments,
our talking points, our discussions
so they can be
more easily
accepted
by people who maybe don't always look at the bigger pictures.
I think that's
probably one of the biggest issues
with progressive messaging
is trying to paint
the whole thing at once,
trying to provide that whole picture at once.
For a lot of people it's too much to take in at one time.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}