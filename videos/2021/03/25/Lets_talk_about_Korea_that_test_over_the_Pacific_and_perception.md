---
title: Let's talk about Korea, that test over the Pacific, and perception....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PqS29hy6Egw) |
| Published | 2021/03/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the recent international test over the Pacific and the significance it holds.
- Detailing the events of the projectile launch, its purpose, and the destination.
- Contrasting the international attention given to this test with the regular testing done by various countries.
- Emphasizing that such tests are common and not necessarily acts of aggression.
- Suggesting that the perception around these tests often shapes foreign policy more than the reality.
- Speculating on Vice President Harris's potential role in initiating talks with North Korea.
- Expressing hope that Harris's unique approach to foreign policy may lead to positive gains.
- Stating that such tests are not as significant as they are often portrayed to be.
- Commenting on the importance of perception in foreign policy decisions.

### Quotes

- "It's only a big deal when North Korea does it."
- "This is a cry for attention. This is them sticking their hand out that window and waving. They want to talk."
- "She might be the right candidate to do it."
- "It's not really even a big deal. We do it all the time and nobody cares because perception is more important than reality when it comes to foreign policy."
- "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

Beau examines the international test over the Pacific, revealing the commonality of such tests and the importance of perception over reality in shaping foreign policy, proposing Harris as a potential catalyst for initiating talks with North Korea.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Initiate talks with North Korea (implied)
- Maintain awareness of the commonality of international tests to avoid unnecessary panic or misconceptions (implied)

### Whats missing in summary

Analysis on the impact of media portrayal in shaping public perception and foreign policy decisions.

### Tags

#InternationalTests #ForeignPolicy #NorthKorea #PerceptionVsReality #VicePresidentHarris


## Transcript
Well howdy there internet people, it's Bo again.
So I guess we've got to talk about that test over the Pacific because it's international
news, everybody's paying attention to it, so we should probably talk about it and figure
out what it means.
Figure out if there's any real significance to it or if it's maybe just messaging and
it's not really a big deal at all, although the US is going to try to make it a big deal.
Okay, so first, what actually happened?
The projectile was taken apart inland.
It was inland to begin with.
They took it apart, moved it out to the coast, launched it over the Pacific, traveled roughly
4,200 miles, landed out near Kwajalein Atoll.
It was designed to kind of test the re-entry vehicle.
That was its purpose.
The high command, the global strike command, sorry, they said the purpose of the test launch
program is to validate and verify the safety, security, effectiveness, and readiness of
the weapon system.
Now what I'm talking about happened four weeks ago.
Was Minuteman III, was taken apart in Montana, taken out to Vandenberg Air Force Base and
launched.
What did you think I was talking about?
Think of Korea, right?
North Korea launching, yeah.
Recently I mentioned that there was a lot of propaganda about North Korea, a lot of
things that were designed to shape American opinion about the country.
This is a good example.
I had a lot of questions about what exactly I meant.
Perfect, perfect example.
Every country on the planet that has these capabilities tests them pretty regularly.
Only one country makes international news when they do it.
This didn't even make news in the US really.
And I'm willing to bet that most people don't know that at the end of this month, we're
testing some new hypersonic gadget.
It's not a big deal.
It's only a big deal when North Korea does it.
As we've talked about before, when it comes to foreign policy, perception is often more
important than reality.
The reality, this isn't a big thing.
It's really not.
But the perception among the American populace says it is because it doesn't match reality
and that's what shapes foreign policy.
So when this gets talked about in the United States, it often gets framed as some kind
of an act of aggression.
It's not.
It's not.
This is a cry for attention.
This is them sticking their hand out that window and waving.
They want to talk.
That's what this is.
Now Biden has said that Harris speaks for him and it looks as though she's going to
get to cut her teeth on the international stage down south.
Good move.
Good move.
When she is done, send her to North Korea.
I have a feeling that she might be able to initiate talks that create gains a little
bit faster than most people.
She is not the typical American politician on the international stage.
She's going to be viewed very differently.
They're not going to know how to take her.
They may even be a little bit more relaxed around her.
She might be the right candidate to do it.
And I'm hoping that her new role in foreign policy might be a sign that they're moving
in that direction.
I think it's a good move.
But please keep in mind when this happens, it's not the Cuban Missile Crisis all over
again.
It's not really even a big deal.
We do it all the time and nobody cares because perception is more important than reality
when it comes to foreign policy.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}