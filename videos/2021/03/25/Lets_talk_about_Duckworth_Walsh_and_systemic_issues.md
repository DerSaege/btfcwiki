---
title: Let's talk about Duckworth, Walsh, and systemic issues....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_fkuvr31rhI) |
| Published | 2021/03/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Apologizes for missing a video due to a sudden rainstorm, with animals seeking shelter in the background.
- Talks about Tammy Duckworth's demand for diversity in Biden's cabinet, sparking controversy.
- Mentions the response from both Democrats and Republicans regarding Duckworth's statement.
- Compares requesting diversity to having a preferential bias, discussing the historical biases in the U.S.
- Explains the importance of increasing representation for groups facing systemic biases.
- Addresses the need to break down barriers for underrepresented groups in the political system.
- Analyzes the demographic makeup of the Biden administration and the U.S. Senate.
- Emphasizes the necessity of actions and programs to achieve better representation in government.
- Stresses the irony of questioning the need for quotas when biases still exist.
- Concludes by supporting Duckworth's efforts for equality and equity in the country.

### Quotes

- "It's almost like requesting diversity is the exact opposite of saying, it's the opposite."
- "Once the biases are gone, and I can't wait till we get to that point, when everybody is just people, then yeah, that would be a wild statement for her to make."
- "She's making a play to get some equality and equity in this country, to fulfill those promises."

### Oneliner

Beau examines the importance of diversity in government and dismantling biases for true equality and equity.

### Audience

Advocates for equality

### On-the-ground actions from transcript

- Advocate for increased diversity in political appointments (implied)
- Support programs and actions aimed at breaking down barriers for underrepresented groups (implied)

### Whats missing in summary

The emotional impact of systemic biases and the urgency to address them for a more inclusive society.

### Tags

#Diversity #SystemicBias #Representation #Equality #Advocacy


## Transcript
Well howdy there internet people, it's Beau again.
So we're back.
Sorry about yesterday, no video.
We had a sudden rainstorm that came out of nowhere.
And it was so wet that when I walked out here today,
just a little while ago,
there were three birds, two squirrels, and a mouse
just hanging out in here trying to stay dry.
It looked like a scene from Disney.
So if you see anything scurrying behind me,
that's what it is.
So today we're going to talk about Tammy Duckworth's request,
statement, demand, whatever you want to call it.
Basically she said that she wasn't going to vote for anybody
that didn't increase diversity on Biden's cabinet.
That prompted a lot of conversation
from all over the place.
Democrats were mad because she would be impeding
Biden's nominations, and Republicans were mad
because we'll get there.
But we're going to talk about the overall theme,
the overall idea behind what she was saying.
And we're going to see if it plays out.
So over on Fox, a guy named Walsh
on our favorite show over there, he says,
imagine a white Republican saying,
I'm not going to vote for any nominee who isn't white,
and oh yeah, straight.
I'm looking for a white, straight male,
and I'm not going to vote for anyone besides that.
If a Republican were to say that, we can't imagine it,
because it would never, ever be uttered
by an American politician.
I don't know about the never, ever part.
I mean, that was law for a long time.
So I mean, I imagine it got said.
Maybe he just means in the future.
Yeah, I guess if a Republican came out and said
they would only vote for a white, straight male,
that would probably cause,
I don't know if it would cause any surprise,
but it would cause a backlash, yeah.
But it probably would if a Democrat said it too.
It's almost like requesting diversity
is the exact opposite of saying,
it's the opposite.
It's a completely different statement
than going with the group
that has a preferential bias in this country.
It's almost like those two statements
are inherently different.
Would it carry the same weight
if a Republican came out and said,
I'm only going to vote for a gay black woman?
I mean, that would actually be surprising.
And oddly enough, the only people
mad about that would be Republicans.
You know, the people who are currently fighting
to keep Nathan Bedford's forest statue
in the Tennessee Capitol, people like that.
That's who would be mad.
It's almost like there's difference
because there's a long history in this country
of different kinds of biases,
and it creates a system, a systemic bias.
You have historical biases,
things that keep groups down
because of what happened a long time ago
and the issue never got corrected.
You have legislative bias that still exists today,
like gerrymandering that limits the amount
of black representation.
You have cultural stuff, such as the president,
the former president, constantly demonizing
and scapegoating different demographics.
And then you have unconscious bias,
which is what led Walsh here to go straight white male,
to say straight white male.
Because that is the default when it comes to politicians
in the United States, that they have preferential bias.
We like to pretend all this is behind us.
It's not.
That's why these quotas, even though this isn't a quota,
that's why stuff like this exists.
That's why stuff like this happens.
The idea is to increase representation
among those people who don't have enough.
How do you determine what's enough?
It's pretty simple.
If none of these things existed,
if these biases weren't in play,
then the makeup of the top 100 people
in the Biden administration would roughly reflect
the demographic makeup of the United States,
because it wouldn't be a factor.
So by default, that's what would occur.
It really works out that way.
I know some people are gonna say, well, you know, no,
because some of them, they're just not good enough
to do that.
They don't have the qualifications.
If you say that and you don't attribute it
to a systemic bias, you are quite literally saying
that a group of people is inferior.
I'm pretty sure that's not a statement
you wanna make publicly.
If you say that, well, no, just, you know,
this group of people, they don't run very often
because they can't win,
because the deck's stacked against them.
If all of the barriers were eliminated,
statistically, the top 100 people
in the Biden administration would roughly reflect
the demographic makeup of the United States.
Now, I can tell by eyeballing, just by looking at it,
the Biden cabinet is one of the most diverse
I've ever seen, if not the most diverse.
I don't have actual numbers on that,
but it's almost impossible to figure out
who the top 100 people are in the Biden administration.
If only there was some body amongst
the American political elite that had 100 people in it,
then it would be really easy.
If only there was some body like the Senate.
So let's take a look.
How close are we to not needing this kind of action?
The demographic makeup of the United States,
60% white, 18 Hispanic or Latino,
13% black or African-American,
6% Asian or Pacific Islander,
3% two or more races and 1% Native American.
These are all rough.
So what's the Senate look like?
We should have one Native American.
How many do we have?
None.
We should have three people with two or more races.
How many do we have?
One.
We should have six Asian or Pacific Islander.
How many do we have?
Two.
We should have 13 black or African-American.
How many do we have?
Three.
We should have 18 Hispanic or Latino.
How many do we have?
Five.
Which leaves almost 90 white.
Man, it seems like we're a long way off from that.
It would be roughly 50-50 men and women.
We have 24 women, I think.
There would be five LGBTQ, and I don't have the numbers on how many are in the Senate.
So we still need actions and programs that are meant to break down these barriers
to get more representation at the top for groups that don't have it
in our representative democracy.
The ultimate irony behind this is most people, when they ask,
you know, why do we even have the quotas like this?
Why do we have to do this?
Normally, the answer is you.
Once the biases are gone, and I can't wait till we get to that point,
when everybody is just people, then yeah, that would be a wild statement for her to make.
Until then, as a member of a group,
that is underrepresented, she's doing exactly what she's supposed to.
Even if you don't like her tone, you don't like the way she did it,
she's making a play to get some equality and equity in this country,
to fulfill those promises.
All men created equal.
All people created equal today.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}