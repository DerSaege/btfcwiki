---
title: Let's talk about the cry of freedom, the Revolution, and public health....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9DLPfH-8e4M) |
| Published | 2021/03/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governors across the country are removing mandates while invoking the image of the founders and the fight for freedom.
- Washington embraced science and ordered strict quarantines during the smallpox outbreak in Boston, showing a proactive approach to public health.
- Washington faced opposition from close-minded and uninformed politicians regarding inoculation, a form of early vaccination.
- Despite Congress banning inoculation, Washington went ahead and inoculated his troops, leading to Congress repealing the ban later due to its effectiveness.
- The idea of the founders disregarding science for freedom in public health matters is historically inaccurate.
- During Valley Forge, many troops received inoculation, showing the founders' commitment to public health even in crude forms.
- If Washington were in charge today, there likely would have been massive lockdowns, mask mandates, and large-scale vaccine drives.
- Washington's action of ordering troop inoculation against Congress's wishes was the first major public health initiative in the United States.

### Quotes

- "Washington embraced science and ordered strict quarantines during smallpox outbreak."
- "The idea of founders disregarding science for freedom in public health is inaccurate."
- "If Washington were in charge today, we probably have had massive lockdowns."

### Oneliner

Beau dives into how Washington's proactive public health measures debunk the notion of founders prioritizing freedom over science in today's context.

### Audience

History enthusiasts, public health advocates.

### On-the-ground actions from transcript

- Join or support organizations advocating for evidence-based public health measures (implied).
- Advocate for mask mandates and large-scale vaccine drives in your community (implied).
- Educate others on the historical importance of public health initiatives in shaping the nation's health policies (implied).

### Whats missing in summary

The full transcript provides a detailed historical perspective on how Washington's proactive public health measures challenge the notion of founders prioritizing freedom over science.

### Tags

#PublicHealth #FoundingFathers #Inoculation #Lockdowns #VaccineDrives


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're gonna kind of dive into something.
All across this country right now,
you have governors removing mandates.
And as they remove them, they rail and rant about freedom.
And in the process, they end up evoking
the image of the founders and that fight for freedom.
So it kind of leads us to wonder,
how would the founders have responded to this situation?
What would Washington have done
if he had to deal with lockdowns and quarantines
and essential workers, stuff like that?
How would he have reacted?
Well, I mean, he wouldn't have reacted
because he's the one who ordered it.
Washington embraced science
more so than most of his contemporaries.
In Boston, as an example, there was smallpox,
and Washington had had that in Barbados.
He knew how devastating it could be to an army.
So he locked down Boston, told civilians,
kind of, you don't have to go home,
but you can't stay here.
They couldn't come into the military zone.
He ordered strict quarantines of his troops.
When his troops had to go into Boston
or the surrounding areas,
only those who had already had it
and only those that had a real reason to go could go.
You might call them essential workers.
But he had a lot of the same problems
that those who embrace science do today.
Had a bunch of close-minded, uninformed politicians
at the local and in Congress
standing in his way.
They didn't have real vaccines at the time,
not the way we think of them today,
but they did have forms of inoculation.
Congress banned it.
Couldn't do it, it was too risky.
Now, Washington, he knew that in some ways that was true
because it did kind of put troops
out of commission for a while.
So he held off on it as long as he could.
But once it got to the point where it was deemed necessary,
he just went ahead and did it.
Told Congress that they could figure it out later, I guess.
Better to ask for forgiveness than permission.
He inoculated his troops.
And the effects were so pronounced
that Congress repealed the ban.
And then average citizens could do it.
This idea of evoking the founders
and that cry of freedom when it comes to public health,
it's just historically inaccurate.
In fact, we all talk about Valley Forge.
One of the reasons it was so bad
was that a large number of troops
got their inoculation during that period.
It wasn't like a shot of today.
It was done in a much more crude manner.
But this whole idea, this whole concept
that somehow the men who fought in the Revolution
would disregard science and best evidence simply
because of some vague notion, it's just completely inaccurate.
If Washington was in charge today,
we probably would have had massive lockdowns.
We definitely would have had mask mandates.
We would probably have, let's just say, large vaccine drives.
Because Washington's action in ordering his troops inoculated
against the wishes of Congress, that
wound up being the first major public health
initiative in the United States.
So perhaps if you want to draw a parallel to the Revolution,
maybe you should draw a parallel to the people who were wrong.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}