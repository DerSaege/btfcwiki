---
title: Let's talk about the PRO Act, Unions, contractors, and the filibuster....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nR1RuC1H52E) |
| Published | 2021/03/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the significance of the PRO Act passing the House and heading to the Senate, where it may face strong opposition but also has powerful allies.
- Describes the PRO Act as Protecting the Right to Organize, a union bill that penalizes big businesses for retaliating against workers trying to unionize.
- Mentions that labor organizing is not his expertise and consulted labor organizers who strongly support the PRO Act.
- Shares insights from Abby at Working Stiff USA, mentioning the importance of solidarity strikes, collective bargaining for gig workers, and debunking myths about the impact on independent contractors.
- Points out that the ABC test in the PRO Act only applies to workers seeking to organize under the NLRA.
- Notes the need for 60 votes in the Senate to overcome a filibuster, with unions having significant influence that could potentially lead to filibuster reform.
- Suggests that organized support for the PRO Act could counter the opposition's financial influence and sway over senators.

### Quotes

- "Protects the right to organize."
- "Every single labor organizer I know is very in support of this."
- "It's only going to impact you if you try to start a union."

### Oneliner

Beau explains the PRO Act, a union bill protecting the right to organize, facing Senate opposition but potentially leading to filibuster reform, with key support from labor organizers. 

### Audience

Advocates, Workers, Activists

### On-the-ground actions from transcript

- Organize in support of the PRO Act (suggested)

### Whats missing in summary

In-depth analysis of the potential implications and broader impact of filibuster reform in passing other legislation.

### Tags

#PROAct #UnionBill #FilibusterReform #LaborRights #SenateOpposition


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the PRO Act.
We're going to do this because it just made it through the House, it's on the way to the
Senate where it is likely to run into some pretty heavy opposition.
It also has some very powerful allies.
So it may be the thing that leads to filibuster reform.
So even if you don't care about this bill in particular, you might want to watch it
anyway because it may open the door to moving other legislation along.
So what is the PRO Act?
One of the few bills that has a title that accurately reflects what the bill is about.
PRO Act, Protecting the Right to Organize Act.
What does it do?
Protects the right to organize.
It's a union bill.
It provides penalties for big business that retaliates against workers who are trying
to unionize, stuff like that.
It really is a union bill.
There's nothing I could find that was hidden in it.
Now labor organizing is not my area of expertise.
It's not something I know a whole lot about.
I was an independent contractor most of my life.
So I reached out to some labor organizers that I know.
If you want the short version of this video, every single labor organizer I know is very
in support of this.
Every association that represents the super wealthy is opposed to it.
That kind of told me what I needed to know, to be honest.
However, Abby from Working Stiff USA was nice enough to give us a little bit more insight.
She says the most important parts are that it would allow for solidarity secondary strikes,
which would open the door to a more general kind of strike at a later date.
It allows gig workers to access collective bargaining.
And she also gave us the talking point that those with deep pockets are going to have
distributed to you to get you to oppose it.
And it's that the ABC test will somehow ruin independent contractors.
I had never heard of this, so I went and looked.
As somebody who was an independent contractor most of his life, this doesn't apply to you.
If you are actually an independent contractor, this doesn't have anything to do with you
really.
This is more about employers who classify their employees as independent contractors
so they can take advantage of them.
This is for people who are classified as independent contractors but show up, work 9 to 5, get
paid by the hour really, and work at the boss's place.
If you're really an independent contractor, this doesn't have anything to do with you.
But she also wanted to point out that we're going to hear over and over how the ABC test
is going to ruin careers, but the PRO Act only applies the test to reclassify workers
under the NLRA, meaning only workers that are actively seeking to organize will be impacted.
So if you are an independent contractor, or you are one of those who for whatever reason
likes being classified as a contractor when you're really an employee, or you're in the
gig economy, or whatever, it's only going to impact you if you try to start a union.
Seems like a pretty weak talking point to me, but that's apparently what they're going
to use.
So that's where we're at on the bill itself.
Now it's headed to the Senate.
It needs 60 votes to get over that filibuster.
Realistically, right now, it doesn't have it.
However unions have a lot of sway.
People when they organize tend to have a lot of sway.
They have a lot of power that way.
So there is the possibility that this is the thing that pushes the Democratic Party to
amend the filibuster.
If that happens, other legislation will get through a little bit easier.
So even if they don't totally get rid of it, even if they just amend it somehow, it would
help with other legislation that may get held up by the filibuster.
The people who are opposed to this, they have a lot of money.
They own a lot of senators.
It might be helpful if people, well, organized in support of it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}