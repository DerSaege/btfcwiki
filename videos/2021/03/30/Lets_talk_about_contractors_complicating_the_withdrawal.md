---
title: Let's talk about contractors complicating the withdrawal....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=x6I7qIRb6bU) |
| Published | 2021/03/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The complication of the U.S. withdrawal from Afghanistan due to contractors being included in the deal.
- The majority of contractors in Afghanistan are support workers, not combat personnel.
- The significance of honoring the deal made, despite it being a Trump foreign policy decision.
- The dilemma of paying contractors to stay in violation of the deal or bringing them home.
- The importance of finding a stabilizing force to replace the U.S. presence in Afghanistan.
- The risks of leaving a power vacuum and the potential consequences.
- Emphasizing the need to focus on finding a stabilizing force rather than the financial aspects.

### Quotes

- "It shouldn't have been in there, but it is. I mean, it is what it is and it's there."
- "The concern should not be about a billion dollars. It should be about finding the stabilizing force to replace us and leave."
- "That vacuum can suck us right back in."
- "For once, when we are making decisions about defense, can we please forget about the money and the contracting firms?"
- "The best move is to get a stabilizing force in and then get out and do it all before that deadline."

### Oneliner

Beau explains the complications arising from U.S. contractors in Afghanistan, urging a focus on finding a stabilizing force over financial concerns.

### Audience

Foreign policy advocates

### On-the-ground actions from transcript

- Contact your representatives to advocate for prioritizing finding a stabilizing force in Afghanistan over financial considerations (suggested).
- Join organizations working towards a peaceful transition in Afghanistan (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of including contractors in the U.S. withdrawal from Afghanistan and the necessity of finding a stabilizing force.

### Tags

#Afghanistan #USWithdrawal #ForeignPolicy #StabilizingForce #Contractors


## Transcript
Well, howdy there internet people. It's Beau again. So today we're going to talk about how
contractors are complicating the withdrawal. There have been some wrinkles in in the withdrawal.
We've talked about it a few times on the channel. The U.S. is trying to leave. They're trying to get
out and they're trying to do it in the right way for once. Something has come up that is being used
as partially as an excuse and partially because some people genuinely don't like it. So we're
going to talk about that and talk about how it really doesn't change the math. It doesn't really
alter the situation at all when you really get down to it. Okay, so we have a deal to be out
of Afghanistan pretty soon actually. The deal not only applies to U.S. forces, it also applies to
U.S. paid contractors. There are about more or less 6,500 in-country. Now one thing I should point out
is these contractors are probably not what you're picturing. The overwhelming majority of them are
support. They're like fixing helicopters, providing technical expertise and advice. They're not the
Oakley wearing kind of contractors. However, they're included in the deal. The two wrinkles
are one, there's a whole bunch of people who feel like that never should have been in the deal
and they're right. It never should have been part of the deal. However, it is. It's in the deal.
It was a bad move. It never should have been agreed to. It's a Trump foreign policy deal. Of
course, it's an absolute disaster, but it's in the deal. And if the U.S. government wants to
regain any credibility in the region, it has to honor the deal. That's incredibly important when
we are trying to bring Iran back to the table. We have to show that a new administration can't just
about the deals that were made by the administration before because that's exactly
what happened to them. If the U.S. wants countries in the region to believe we will honor our deals,
we have to actually honor them. So that really doesn't change anything. Yeah,
it shouldn't have been in there, but it is. I mean, it is what it is and it's there.
The next argument is DOD, in its infinite wisdom, has contracts running out to 2023
with some of these firms. Overall, these contracts are worth about a billion dollars.
The American people have a choice to make. Do we want to pay these contracting firms a billion
dollars and have them there in violation of the deal, or do we want to pay them a billion dollars
and bring them home? Those are really the options. Most of these firms will probably take less
to just come home, but the U.S. is still going to pay. We're probably talking $500,000,000,000,000,000.
They'll get a discount, but not much. Now, an alternative to that is that more than likely,
the host nation, the national government there, wants them there. They could take over the
contracts. They're not U.S. paid anymore. I don't know if that's necessarily a good idea,
but it would at least stick to the terms of the deal. At the end of the day,
at the end of the day, none of this changes the math. We need to find a stabilizing force to replace
us and get out. It's that simple. The concern should not be about a billion dollars. It should
not be about these contractors. It should be about finding the stabilizing force to replace us and
leave. Those are our steps. I've had a lot of people ask, well, what happens if we leave without
a stabilizing force there? A power vacuum is created. That's what happens. The most likely
chain of events, if that occurs, is that the opposition goes on the offensive. The images
from that offensive are beamed back to the United States, where the American people find them
offensive. The next thing you know, there's airstrikes. No boots on the ground. Then,
in about 180 days after that, there's boots on the ground, starting with contractors and
special operations. A lot of people, when they hear about that power vacuum, they're like, well,
I mean, it's bad for the people of Afghanistan, but it doesn't impact us. We need to leave.
That vacuum can suck us right back in. It is incredibly important to find another token force
that can hang out there to deter further conflict. It really doesn't matter who it is.
I mean, pretty much anybody would do a better job than we would. That needs to be the goal.
For once, when we are making decisions about defense,
can we please forget about the money and the contracting firms?
The best move is to get a stabilizing force in and then get out and do it all before that deadline,
which is coming up pretty quick. Anyway, it's just a thought. You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}