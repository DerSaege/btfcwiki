---
title: Let's talk about Biden, midterms, jobs, and green infrastructure....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qEbZLj525q8) |
| Published | 2021/03/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's infrastructure and jobs program includes a focus on wind turbines, set to create a minimum of 32,000 jobs from 2022 to 2030 during the construction phase.
- The program aims to have the turbines generate 30,000 megawatts in the short term, expandable to 110,000 megawatts.
- Biden's goal is to have the grid be carbon neutral by 2035, requiring building out 70,000 megawatts a year of solar and wind.
- While the media praises the program, more initiatives like this are needed to achieve Biden's ambitious goal.
- Biden is open to bipartisan collaboration on infrastructure with Republicans, but Democrats in Congress are prepared to proceed without them.
- Biden strategically plans to execute these initiatives in summer when he is expected to have significant political capital.
- The successful implementation of these plans could give Biden leverage with voters and pressure Republicans to participate in job creation efforts.
- The current infrastructure and jobs program is a good start but may not be ambitious enough to fulfill all campaign promises.
- Biden may be pushing these initiatives through in stages to gradually achieve his objectives without alarming opposition.

### Quotes

- "Biden's dream of an FDR-style presidency, it is slowly coming into focus."
- "It's going to take this program and a whole lot more like it to make that happen."
- "He's open. Congress, Democrats in Congress, are setting the stage to do it by themselves."
- "Do they want to come to the table and try to be a part of the next big package that is going to create 32,000 jobs just in time for the midterms?"
- "This alone is not ambitious enough to fulfill some of his campaign promises, but it's a good start."

### Oneliner

Biden's ambitious infrastructure plan aims to create jobs and achieve carbon neutrality, pushing for bipartisan support while preparing for potential solo action in Congress.

### Audience

Politically active citizens

### On-the-ground actions from transcript

- Contact your representatives to voice support for Biden's infrastructure plan (implied)
- Organize community forums on sustainable energy and infrastructure projects (implied)

### Whats missing in summary

Analysis of potential challenges and criticisms facing Biden's infrastructure plan.

### Tags

#Biden #Infrastructure #GreenJobs #Bipartisanship #JobsCreation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about Biden's infrastructure and jobs program that is green-ish.
The big selling point on it right now is the wind turbines.
That's what everybody's talking about.
Wind turbines, not windmills, because y'all got mad at me last time I said that.
In the short term, from 2022 to 2030, it looks to create a minimum of 32,000 jobs.
Thirty-two thousand jobs for eight years.
That is the low end of the estimates I could find.
That's the construction phase.
That's where stuff's being built, ports are being revamped to accommodate everything,
that kind of stuff.
There are 6,000 permanent jobs in it.
All that sounds good.
The turbines themselves will generate 30,000 megawatts in the short term, expandable out
to 110,000 megawatts.
That's good.
Biden set a very ambitious goal.
He wants our grid to be carbon neutral by 2035, I think.
It's possible.
There are studies that have been done on it, and in order to make it happen, we have to
build out 70,000 megawatts a year of solar and wind.
So while the media is referring to this as ambitious, audacious, dramatic, massive,
whatever, it's going to take this program and a whole lot more like it to make that
happen.
Biden's dream of an FDR-style presidency, it is slowly coming into focus.
All of this is possible.
Is it politically likely?
We don't know.
We have no idea.
Biden has indicated that he is willing to be bipartisan on this.
Republicans can come to the table.
They can talk about it.
They can negotiate.
As long as it's about infrastructure, they can talk about how to pay for it, all of that
stuff.
He's open.
Congress, Democrats in Congress, are setting the stage to do it by themselves.
They don't believe Republicans will come to the table on it.
Biden is saying he wants all this done this summer rather than now, and I don't think
that's an accident.
This summer, if everything goes according to plan, Biden is going to have a whole lot
of political capital.
Everybody will be vaccinated and taking road trips.
The economy will be back on track, and the perception???the economists can tell you whether
or not it's really what happened???but the perception will be that the stimulus package
is what turned the economy around.
Maybe it is.
I don't know.
That's going to get him a lot of leeway with the American voter.
So then Republicans are going to have to decide.
Do they want to come to the table and try to be a part of the next big package that
is going to create 32,000 jobs just in time for the midterms, or are they going to sit
this one out like they did the stimulus package?
I don't think that the timing of that's an accident.
That just seems to???it seems like it would take a lot of work to have that many coincidences
line up.
So what are they going to do?
I have no idea.
No clue.
But overall, what we know about the package, and it is in the very rough sketch phase,
looks good.
This alone is not ambitious enough to fulfill some of his campaign promises, but it's
a good start, and I think that's what he's looking for right now.
I think they're trying to push this through in pieces so nobody really recognizes exactly
how far he's going each time.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}