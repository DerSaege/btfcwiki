---
title: Let's talk about Biden checking Trump's science homework....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nFVAwkGOFA8) |
| Published | 2021/03/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden's administration is forming a task force to go over former President Trump's actions regarding science.
- Federal agencies have until April 2 to nominate members for this task force.
- The task force aims to analyze the past four years and determine what went wrong, how to fix it, and prevent similar mistakes.
- It will focus on instances where the former president ignored reality, science, data, and evidence.
- Beau questions the effectiveness of the task force, as future presidents could override its policies.
- Despite doubts, Beau believes it's worth investigating mixed messaging that impacted the country's response to crises.
- He anticipates the task force's efforts being politicized as a way for Biden to capitalize on recent statements by Dr. Birx.
- Beau mentions that this task force has been planned for a while, likely accelerated by recent revelations.
- He underlines the importance of American voters as the ultimate safeguard against potential misuse of power by future presidents.
- Beau suggests choosing leaders focused on bettering the country and the world, rather than those who simply echo desired sentiments.

### Quotes

- "The biggest safeguard, the biggest safety valve there, is you."
- "Rather than looking for people who tell you what you want to hear, perhaps it
  would be better to look for people who are interested in making the country and the world better."

### Oneliner

President Biden forms a task force to scrutinize Trump's science handling, but Beau questions its efficacy, stressing voter responsibility in choosing leaders for a better future.

### Audience

American voters

### On-the-ground actions from transcript

- Nominate individuals for the task force before April 2 (suggested)
- Stay informed about the task force's progress and recommendations (suggested)
- Make informed decisions during elections, prioritizing candidates focused on positive change (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the task force formed by President Biden to scrutinize his predecessor's scientific decisions and Beau's concerns about its long-term impact.

### Tags

#PresidentBiden #TaskForce #Science #Voters #Leadership


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about how President Biden will
be checking over former President Trump's science
homework.
The Biden administration has announced
they are putting together a task force.
Federal agencies have until April 2
to make nominations for it.
This task force will be going back
through the last four years and attempting
to figure out exactly what went wrong and how to fix it,
how to make sure that it doesn't happen again.
They will be looking at instances
where the former president disregarded reality, science,
data, best evidence, and substituted
what he substituted.
The goal is to try to prevent it from happening again.
And it's a good goal.
It makes sense.
But realistically, I don't know how effective it's going to be.
Because anything that the White House can establish,
any office that the White House can put together,
is going to be under the control of the executive, which
means the next president we have, like Trump,
can override it and change the policies.
I'm not sure if this is going to do any good.
It's worth pursuing.
It's worth figuring out what happened.
It's worth looking at every time there was mixed messaging that
severely impacted this country's ability to respond to perhaps
a public health issue.
But again, I'm not sure how effective any
of the recommendations will be.
I am certain that this will be spun into something that is
Biden trying to capitalize on Dr. Birx's recent statements.
I just want to point out, before that starts,
there's a memo about this.
And I'm not sure if it's going to be
a memo about the task force from January, I think.
This is something they've had planned for a while.
I would imagine that the recent revelations might have sped up
their desire to make this happen.
But it's been in the works.
At the end of the day, when it comes to something like this,
there's not a whole lot of safeguarding
that the White House is going to be able to do to protect
from the next occupant of the White House.
The biggest safeguard, the biggest safety valve there,
is you.
It's the American voter.
Rather than looking for people who tell you
what you want to hear, perhaps it
would be better to look for people
who are interested in making the country and the world better.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}