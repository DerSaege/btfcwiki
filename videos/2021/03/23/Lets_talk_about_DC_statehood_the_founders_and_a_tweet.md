---
title: Let's talk about DC statehood, the founders, and a tweet....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fW1NEj_BVNg) |
| Published | 2021/03/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau delves into the debate on DC statehood, prompted by a tweet from Senator Mike Rounds of South Dakota.
- The intention behind creating Washington DC as a neutral federal jurisdiction was to avoid state influence on the federal government.
- Over time, the concerns of the founding fathers have evolved and largely been addressed.
- Beau explains the linguistic shift from "the United States are" to "the United States is" as the federal government's authority grew.
- While the founders didn't intend for DC to be a state, there's nothing prohibiting it constitutionally.
- DC was initially allocated 100 square miles but now stands at 68.3, indicating flexibility in its size.
- The debate also touches on the political implications of DC statehood, with a focus on party affiliations in voter registration.
- Republicans' opposition to DC statehood is linked to concerns about the political leanings of its residents.
- Beau criticizes the Republican Party for potentially denying representation based on residents not voting in their favor.
- He accuses the Republican Party of straying from the principles of representative democracy and the republic.

### Quotes

- "It's not really that they have a concern with people being represented. They wouldn't have a problem with it. It's that they're low-quality voters."
- "I'm fairly certain that the founders might have an issue with a population the size of a little bit bigger than the populations of Virginia and Pennsylvania at the time, not having representation."
- "The only thing that the Republican Party currently has in common with the founders is the willingness to deny people the right to vote."

### Oneliner

Beau dissects the DC statehood debate, addressing historical intent, political motives, and concerns over representation in a democracy.

### Audience

Political activists, voters

### On-the-ground actions from transcript

- Advocate for fair representation and democracy in all aspects of governance (implied)
- Support initiatives that uphold equal voice and representation for all citizens (implied)

### Whats missing in summary

Beau's emotional investment and deep analysis can be best experienced by watching the full transcript.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about DC statehood.
Whether DC should become a state and the intent of the founders.
We're going to do this because I saw a tweet and I'm going to be completely honest, I thought
it was satire.
I thought it was a joke.
I thought it was really good satire.
Even the name, Mike Rounds.
My grounds.
You know, DC is my grounds, statehood.
And I found out, no, he's a senator from South Dakota.
And it wasn't funny anymore.
In fact, I got kind of sad.
But we're going to go through his tweet.
There's two parts to it.
And talk about some of the assertions that he made.
The first is, the founding fathers never intended for Washington DC to be a state.
That's true, kind of.
I mean, it's accurate in a way.
The intent behind creating DC was not to create a federal jurisdiction for no reason.
It wasn't just for giggles.
There was a worry that if we put the capital in another state, the federal government would
become beholden to that state, whatever state it was.
So it needed to be neutral ground, because at the time, the states weren't all unified.
So they needed, yeah, a place of neutrality.
That concern doesn't really exist anymore.
Like a lot of the concerns of the founders from 200 years ago, we've addressed the issue.
Today, states understand that federal property is federal jurisdiction.
You have to look no further than any military base in the United States.
If you take Fort Benning, for example, cops from Columbus don't go in there and smack
people around in the barracks.
That's not a thing.
This issue, the issue that brought all of this about, isn't really a concern anymore,
because it is no longer the United States are, it's the United States is.
Cool little linguistic thing for you.
If you go back to the Constitution, you will see that the United States is plural.
Look in the section on treason.
Levy war against them, because it was a bunch of states united, but they were all still
individuals.
It's not like that anymore.
We don't view it that way because over time the federal government became supreme.
It exerted more and more authority.
It really came into its own after the Civil War, and that's when you can see this little
linguistic change occur primarily.
It takes time, but you see the slow switch from the United States are to the United States
is.
It's not really a concern anymore.
Aside from that, in the Constitution there is authority for it.
It says 10 miles square.
That is not 10 square miles.
It is 10 miles square.
So 10 miles by 10 miles, 100 square miles.
But DC isn't 100 square miles.
It's 68.3.
Even though they were given 100 square miles of land to begin with, they gave that extra
land back.
Turned it back to the states.
Because they didn't need it.
It doesn't have to be a certain size.
It can't exceed a certain size.
There is nothing in the Constitution or as far as the founders intended that would prohibit
DC from just being the Capitol complex, the White House, and the Supreme Court.
I guess if you wanted to, you could include the roads that connect them or the other federal
buildings.
But all of the people that live there, all those homes, that doesn't need to be part
of the District of Columbia.
That could be the state of Columbia.
Or there are other alternatives.
So while accurate, it's not really true.
It's an accurate statement.
The founders didn't intend for DC to become a state.
That's true.
The founders also didn't intend for South Dakota to become a state.
It wasn't part of their plan.
They had no intention on barring a whole bunch of people from having representation.
The intent was to ensure the autonomy of the federal government.
No longer a concern, really.
Next part of his tweet.
DC statehood is really about packing the Senate with Democrats in order to pass a left-wing
agenda.
Let's look at the DC voter registration data.
76.4% Democrat, 5.7% Republican.
That is mask off.
That makes it really clear why Republicans oppose it.
It's not really that they have a concern with people being represented.
They wouldn't have a problem with it.
It's that they're low-quality voters.
They wouldn't vote the way that the Republicans want them to.
So therefore, they don't deserve representation.
That's a pretty bold statement.
I mean, just throwing it out there like that, this is the real reason I'm opposed to it.
Because they won't vote the way I want them to.
That seems kind of at odds with talking about the founders and what they intended, the promises
that they made, the idea of a representative democracy.
I'm fairly certain that the founders might have an issue with a population the size of
a little bit bigger than the populations of Virginia and Pennsylvania at the time, which
were the two most populous colonies not having representation.
I think they had an issue with taxation without representation.
Pretty sure that was the thing.
I don't think that the founders ever envisioned almost 700,000 people going without representation
because of something like this, because of their desire to make sure that the federal
government wasn't beholden to a state.
I don't think they'd be okay with that as just collateral.
I also want to point out that this is another example of the Republican Party openly trying
to deny people their voice, trying to suppress votes that won't go their way.
It's a running theme.
Now, as far as the Republican Party being able to invoke the founders, you know, when
you try to subvert an election, when you try to suppress the vote, deny people their voice,
and then when things don't go your way, you attempt to stage a coup, you kind of lose
the ability to talk about the founders.
You don't get to lay claim to them anymore because the Republican Party has given up
on the idea of the republic, on the idea of representative democracy.
They've abandoned it.
They don't get to use that as a crutch.
The only thing that the Republican Party currently has in common with the founders is the willingness
to deny people the right to vote.
That's about it.
Because they won't vote the right way.
They don't make enough money.
They're not the right color.
That they can claim, the negative stuff.
Nothing about the promises that were enshrined in the Constitution or the Declaration of
Independence.
None of that has anything to do with the Republican Party anymore.
They gave that up.
There is another option, though, rather than creating a new state.
Do what happened before.
Vote the residential areas, give them to Virginia.
I wonder if that would turn the state blue forever.
Because that's the concern, right?
Political power.
Not people.
Not their voice.
Not representative democracy.
Not the republic.
Just power.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}