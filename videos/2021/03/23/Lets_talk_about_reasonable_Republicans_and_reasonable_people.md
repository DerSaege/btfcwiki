---
title: Let's talk about reasonable Republicans and reasonable people....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0TdNqtJyDVs) |
| Published | 2021/03/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the "Tucker Carlson defense" where individuals claim they shouldn't be held accountable because no reasonable person could believe their statements.
- Mentions Sidney Powell, a lawyer known for making wild claims about the election, and how she's being sued by a company for those statements.
- Talks about the impact on individuals who believed these claims, leading to consequences like losing jobs, standing among friends, and even getting arrested.
- Points out the blow to one's ego when they're told no reasonable person could believe the claims they supported.
- Describes how some claims may have been made in good faith, while others were meant to energize the base without caring about the harm caused.
- Criticizes the Republican and conservative machine for training supporters to reject reality and rely solely on sound bites from their chosen leaders.

### Quotes

- "No reasonable person could believe the things that I've said."
- "Sometimes they were just trying to dupe folks, energize the base."
- "The Republican machine has trained its supporters to reject reality."
- "Don't look any further into it than what your chosen leader has said."
- "They played on patriotism and found the unreasonable."

### Oneliner

Beau explains the "Tucker Carlson defense," discussing the impact of wild claims and how the Republican machine trains supporters to reject reality.

### Audience

Community members, political observers.

### On-the-ground actions from transcript

- Fact-check claims and statements before believing or supporting them (implied).
- Encourage critical thinking and research among friends and family (suggested).

### Whats missing in summary

The emotional toll and consequences of believing false claims pushed by public figures.

### Tags

#Republicans #SidneyPowell #TuckerCarlson #Reality #PoliticalAnalysis


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about reasonable people, well reasonable Republicans anyway.
Because if you don't know that lawyer, she's using the Tucker Carlson defense.
I shouldn't be held accountable for my actions.
No reasonable person would believe the things that I've said.
To use the exact wording, reasonable people would not accept such statements as fact.
Now if you don't know who I'm talking about, I'm talking about Powell, the lawyer who pushed
all the wild claims about the election, about the voting.
The company that kept getting named, yeah, they're suing her.
That's the defense.
That's the defense.
On one hand, I feel for the people who believed her.
On one hand I feel for the people who believe these claims, I really do.
On the other hand, it is an incredibly valid defense.
I feel for them because the reality is that if they believed this, if they believed these
claims that were being pushed, it probably got pretty consuming.
And they probably lost standing among their friends, maybe created a gulf between them
and their family.
They could have lost their job, might have lost their freedom.
Some of them got arrested.
For some of them, believing these claims quite literally ruined their lives.
And now, well, they're unreasonable.
They can't tell fact from fiction.
And then imagine the blow to your ego.
After spending months saying stuff like, well, if you believe reality, you're just a sheep.
And then to be told, well, no reasonable person would have believed this stuff.
That's got to hurt a lot.
On the other hand, it is an incredibly valid defense.
Because even right now, I'm certain that there are those who are saying, oh, no, no, no,
that's just legal maneuvering.
And they'll still believe it.
They will still support these people.
And I'm not just talking about Powell and Carlson.
And if you don't know, this gets joked about as the Tucker Carlson defense, because he's
used it too.
The general tenor of his show means that reasonable people arrive with skepticism, something along
those lines.
You just can't believe what he says are facts.
You believed it.
And you still support them and those who amplified their voices like the former president.
That kind of shows that you are, in fact, unreasonable.
Because despite all evidence to the contrary, you still believe that they value you.
They don't.
They do not.
Sometimes I'm sure some of these claims were made in maybe even in good faith.
Sometimes they were just part of the adversarial nature of a courtroom.
And I'm sure for at least a few of the people making the claims, they were just trying to
dupe folks, energize the base.
They didn't care about who got hurt along the way.
It's my read on it.
You know, be a reasonable person, determine whether or not you think that's true for yourself.
It's sad, but it's not really surprising.
Because the Republican machine, the conservative machine in general in this country, has trained
its supporters to reject reality, to go by sound bites and nothing more.
Don't look any further into it than what your chosen leader has said.
Because they waved a flag.
They mentioned a founding father that more than likely they know nothing about.
They played on patriotism and found the unreasonable.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}