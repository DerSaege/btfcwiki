---
title: Let's talk about Birx, Fauci, Trump, and advising the former president....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Y1y2xX8Zfz4) |
| Published | 2021/03/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Dr. Birx implied the first hundred thousand COVID deaths were inevitable, but everything after that could have been mitigated.
- There was immediate criticism towards Dr. Birx for not speaking up earlier if she knew the severity of the situation.
- Dr. Fauci and Dr. Birx adopted a "good cop, bad cop" approach with President Trump, treating him like a toddler due to his perceived lack of understanding.
- Some believe the advisors' claims of guiding Trump gently are just an excuse after the fact.
- Trump's advisors, not just Birx and Fauci, struggled with the decision-making process, as seen in foreign policy blunders like targeting an Iranian general.
- The Pentagon included extreme options in briefings to make other choices seem more reasonable to President Trump.
- Trump's tendencies to make wrong decisions led his advisors to treat him like a child, resulting in questionable choices being presented.
- Despite criticism, Trump initially chose a less extreme option in a foreign policy decision but later escalated tensions.
- Trump's advisors' normalization of his poor decision-making contributed to a team mentality in American politics rather than prioritizing the country's well-being.
- Blind support for Trump based on party lines and talking points rather than results may have led to significant loss of life during his presidency.

### Quotes

- "Maybe we should be more focused on the country rather than our team."
- "They started treating him like a child."
- "That team mentality led to hundreds of thousands of excess loss."

### Oneliner

Dr. Birx and Dr. Fauci faced criticism for not speaking up earlier about preventable COVID deaths, revealing a troubling dynamic of treating President Trump like a child, ultimately leading to a team-focused mentality causing significant loss of life.

### Audience

American citizens

### On-the-ground actions from transcript

- Question political loyalties and prioritize the country over party affiliations (implied)
- Advocate for transparency and accountability from leaders in crisis situations (implied)
  
### Whats missing in summary

The full transcript expands on the implications of blind partisan loyalty and the need for a shift towards prioritizing the country's well-being over political affiliations.

### Tags

#COVID19 #PoliticalAdvisors #TrumpAdministration #PartisanPolitics #PublicHealth


## Transcript
Well, howdy there, Internet of People. It's Beau again. So today we're going to talk about
Dr. Birx and Dr. Fauci, former President Trump, and being an advisor to the president and what
that entails and what that requires and what it required in Trump's case because he might have
been a little different than other presidents. Now, if you don't know what happened over the
weekend, an interview came out in which Birx kind of said that, you know, that first hundred
thousand, well, that was going to happen. There wasn't much we could do about that.
Everything after that could have been mitigated, severely reduced. When you do the math on that,
you end up with a pretty wild figure, a lot of extra loss. Now, immediately there was criticism
of her. Hey, you know, if you knew it was going to be that bad, you could have gone public.
That's a fair argument. Fauci has kind of indicated that they were running good cop,
bad cop on the president. You know, Fauci was being stern and she was trying to subtly,
gently guide him in the right direction. They were basically treating the former president
like a toddler because the implication being that he was that far out of his depth. There
are a lot of people who don't believe that that was happening, who kind of feel that it,
just saying that now after the fact. If you want to make the argument that they should have come
forward earlier, that's fair. That is a fair argument to make. But in the interest of fairness,
I feel I should point out they weren't the only advisors who felt they needed to do that.
One of Trump's biggest foreign policy blunders had to do with an Iranian general.
And when news broke of what happened, people who are familiar with the process,
everybody sitting around going, why was that even on the menu? Why was that even an option he could
choose? That's not proportional. That's, it's an incredibly provocative move. It shouldn't have
been an option. Later on, we find out that the Pentagon put it on the menu, quote,
mainly to make other options seem reasonable. They were doing the same thing. They had become
so accustomed to the former president making the wrong move that they were treating him like a
child, giving him extreme options, hoping he would pick the right one. And incidentally, it worked
at first. The first option that he selected was to hit Iranian-backed non-state actors who happened
to be in Syria at the time. Yes, he did the exact same thing he criticized Biden for. And then
he turned on the TV and he didn't like the protests, so he authorized the option that was put
on there because no reasonable person would pick it. There is certainly a lot of discussion that
can take place over this. But one thing I don't think should be glossed over is the fact that we
had a president for four years whose advisors were so accustomed to him making the wrong choice
that they started treating him like a child. That's bad enough. But then that same president
had a whole lot of support at the end of those four years because people weren't looking at
results, they were listening to talking points. They were listening to them being told what they
wanted to hear and they were looking for an R at the end of the name. It appears, if you listen to
the doctors, it appears that that team mentality that has just entrenched itself in American
politics led to hundreds of thousands of excess loss. That might be something we want to consider
as a country moving forward. Maybe we should be more focused on the country rather than our team.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}