---
title: Let's talk about why peace takes so long after the US shows up....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dhMegh67pU8) |
| Published | 2021/03/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions why it takes so long for a country to recover and establish control after a US intervention.
- Illustrates the challenge through the lens of different factions with varying loyalties and goals.
- Compares the establishment of governing rules in the US post-independence to the situation in Afghanistan.
- Points out the linguistic, ethnic, and cultural diversity in Afghanistan that complicates the recovery process.
- Emphasizes the time required for a nation to reach consensus amidst differing interests.
- Suggests that the focus should be on why US interventions lead to government falls rather than the length of recovery.
- States that Afghanistan still has a long journey ahead despite ongoing peace negotiations.
- Advocates for transitioning the US role in Afghanistan to another nation or coalition for better outcomes.
- Argues against prolonging US presence and involvement in reasserting the Afghan government.
- Calls for prioritizing relief efforts and transitioning responsibilities to other nations or groups.

### Quotes

- "Why does it take so long for a country to recover from a US intervention?"
- "I think that is worth more thought and more discussion than why it takes them so long to recover from us."
- "Regardless of whether or not the US continues to have a presence there because right now, we're trying to get out."
- "We don't need to have a hand in reasserting the national government."
- "We need to stop that now."

### Oneliner

Why does it take countries so long to recover from US interventions, especially in diverse, multi-factional settings like Afghanistan, prompting a need for transitioning responsibilities to enable effective recovery efforts?

### Audience

Policymakers, Activists, Global Citizens

### On-the-ground actions from transcript

- Transition responsibilities in Afghanistan to another nation or coalition for better outcomes (implied).

### Whats missing in summary

Beau's detailed explanation and insights on the challenges faced by countries post-US interventions and the importance of transitioning responsibilities for effective recovery efforts.

### Tags

#USIntervention #Afghanistan #Recovery #TransitionalResponsibilities #GlobalPolicy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to answer a question, a cool question, genuinely asked.
Why does it take so long?
Why does it take so long for a country to get back on its feet, to get a peace, to get
a government that reestablishes control and can assert itself and take care of everything?
Why does it take so long after, I mean not to put too fine a point on it, the US shows
up.
Why does it take so long for a government to reassert itself after a US intervention?
Okay so this is what I want you to picture.
You are part of group one and you are allied with a dozen other groups and y'all all share
similar goals but you're all kind of different.
You hold your loyalty to a different group.
You're pretty similar but maybe there's a little bit of a different religious background,
little bit of a different culture, the way you grew up.
You hold your loyalty to a different group.
That's the key element.
Now you and your comrades, you fight a seven or eight year long campaign to oust the most
powerful military on the planet and you win.
How long should it take you to get your act together?
Come up with a set of governing rules that everybody can agree to.
If you're the United States, it takes from 1783 to 1790 before all former colonies ratified
the constitution.
It's always taken a long time.
This isn't new and when we are specifically talking about Afghanistan, it's even more
difficult because in the United States, everybody spoke the same language.
There were slight religious differences but not major ones and at the end of the day,
it was a bunch of rich old white guys deciding what was best for rich old white guys.
They had a general idea of where they wanted the country to go but at the end of the day,
they had those monetary interests that aligned and that, well, that made things a lot easier.
When you're talking about Afghanistan, there are two official languages.
I can think of three more that are used by a significant number of people.
Off the top of my head, there are probably more.
I'm not an expert on Afghanistan.
There's at least a dozen ethnicities and everybody has their own idea of what's best.
It's going to take them time to come up with something that everybody's going to be happy
with or is more likely the case, everybody is going to be equally unhappy with.
It always takes a long time.
The question I think that is more important is why is it assumed in this question that
they're recovering from a US intervention because it's become that common.
I don't know if there's any studies on it but I would be willing to bet we're one of
the main reasons a government falls.
I think that is worth more thought and more discussion than why it takes them so long
to recover from us.
But it always does take a long time.
Even if you have everybody united and in Afghanistan's case, they aren't.
They're still working out the peace deal to end the conflict and then they have to work
out the power sharing agreement.
Regardless of whether or not the US continues to have a presence there because right now,
we're trying to get out.
Regardless of whether or not that happens in the way most people want it to, Afghanistan
still has a long way to go.
It's not back on its feet yet and it won't be for a while.
The thing is currently, if there's anybody, any nation that can fill the US role there,
it's better for Afghanistan if we get out.
So that's when people talk about wanting to stay until the end of the peace deal, wanting
to stay until the national government asserts itself, all of that stuff, no, we don't need
to do that.
We don't.
We need to get relief in.
We need to get a group, another nation or a coalition of nations to fill the role that
the US is doing there right now, which is really just a token force to deter more conflict.
That's it.
We don't need to have a hand in reasserting the national government.
That is beyond the scope of what we have already committed to.
We're there because we keep expanding the scope of what we're doing there.
We need to stop that now.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}