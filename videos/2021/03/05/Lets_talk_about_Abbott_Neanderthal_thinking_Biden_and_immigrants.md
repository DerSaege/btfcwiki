---
title: Let's talk about Abbott, Neanderthal thinking, Biden, and immigrants....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5W7_52-xIPw) |
| Published | 2021/03/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor Abbott used a political move to shift blame onto immigrants for potential COVID-19 spread in Texas.
- Abbott's tactic of blaming immigrants is a common political strategy in American history to deflect responsibility.
- Despite the blame game, statistics show that countries immigrants are likely from, like Honduras, Nicaragua, El Salvador, and Mexico, are doing better at preventing new COVID-19 cases compared to Texas.
- Even with potentially underreported numbers, these countries still fare better in controlling the virus than Texas.
- Abbott's playbook move of blaming immigrants is harmful and diverts attention from real issues, affecting everyone negatively.
- The tactic of blaming immigrants rather than accepting responsibility is outdated and needs to change.
- The blame game only serves to harm marginalized groups and society as a whole.
- Immigrants are often scapegoated instead of holding powerful figures accountable for their actions.
- Despite poorer healthcare systems in countries like Nicaragua, El Salvador, and Honduras, they are still more successful at preventing new COVID-19 cases than Texas.
- The blame game perpetuates harmful narratives and fails to address the real challenges effectively.

### Quotes

- "Blame the brown person."
- "This is bad for everybody."
- "They are still better at preventing new cases than Texas."

### Oneliner

Governor Abbott's blame game on immigrants diverts attention from real issues, despite statistics showing countries of origin faring better in preventing COVID-19 than Texas.

### Audience

Texans, policymakers, activists

### On-the-ground actions from transcript

- Confront harmful narratives against immigrants (exemplified)
- Advocate for accountability rather than scapegoating (exemplified)

### Whats missing in summary

The detailed statistics and comparison of COVID-19 cases between Texas and countries of origin of immigrants. 

### Tags

#Immigration #COVID-19 #BlameGame #GovernorAbbott #Texas


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about something the governor of Texas said.
Governor Abbott.
He dusted off an old American political playbook and used it to great effect.
It's a political move that politicians will often use in the United States when they have
truly failed in their duties, like say during an ice storm.
Or when they engaged in decision making that they thought was going to be politically expedient
but instead it causes an uproar, like say removing mandates.
President Biden made a joke, said that it was caveman thinking to remove the mandates,
Neanderthal thinking, something like that.
In response, Governor Abbott, well he said no no no, it's the Biden administration's
fault.
It's really going to be their fault if the numbers go up because they dumped immigrants
into our communities and some of them had it.
Okay, now to be fair, I didn't check to see if that's true.
It could be a bald-faced lie.
I mean, it may not have happened, but realistically it doesn't matter because it will.
If it hasn't happened yet, it will occur.
Testing is not 100% accurate.
At some point that is going to happen.
So even if he is making it up now, eventually it'll be true.
But that's not really the question.
The question is, does that introduce a new risk to Texans?
Does that somehow make it worse than removing the mandates?
To answer that, we'd have to look at the countries that they come from, right?
Get some statistical analysis going on.
I happen to have pulled the numbers from Honduras, Nicaragua, and El Salvador, which are the
three countries these people are most likely to have come from.
I also pulled the numbers from Mexico because my guess is that's what Governor Abbott
would call them.
All four countries are doing better than Texas at preventing new cases.
All four.
By a lot.
So much so that if you were to say, oh, they're underreporting, it doesn't matter.
They're doing that much better.
Now the country that has the closest comparable raw numbers is Mexico.
Texas has 2.6 million cases.
Mexico has 2.1 million cases.
So even if you were to say they were underreporting and you added 50% or doubled it, it still
doesn't matter because Texas only has 29 million people and Mexico has 127 million.
So you could triple it and it wouldn't matter.
They're still doing better.
But even though this information is widely available, no doubt this move will work.
It has worked throughout American history.
Blame the brown person.
Oh, no, no, no.
It was the foreigner who has the disease.
It's pretty common.
So there will be people who choose to look at the person with no institutional power,
no influence, who just arrived in the country and blame them rather than the most powerful
man in the state because that makes complete sense.
The world is changing and people who use tactics like this try to shift blame rather than accept
responsibility.
They are going to have to change the way they are or retire because this information is
out there.
It's easy to find.
It's the information age.
This is garbage.
This is the governor of Texas pulling out a playbook that has been used in this country
for a long time that has been incredibly harmful, not just to the groups involved, the groups
that get the blame, but everybody else.
This is bad for everybody.
I know that there will probably be somebody who looks at the numbers and says, well, you
know, it's not entirely true because, you know, these countries, they have worse rates
of people who don't recover.
And yeah, I mean, that's true.
That is true.
They have worse health care systems.
The United States is ranked consistently in the 30s.
The closest is Mexico at 61st.
Nicaragua is 71.
El Salvador is 115 and Honduras is 131.
And even though their health care systems are that much worse, they are still better
at preventing new cases than Texas.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}