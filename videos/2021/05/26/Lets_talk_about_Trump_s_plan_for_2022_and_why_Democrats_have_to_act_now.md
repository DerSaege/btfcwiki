---
title: Let's talk about Trump's plan for 2022 and why Democrats have to act now....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=P0-FeH1Otk0) |
| Published | 2021/05/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump has reportedly enlisted Newt Gingrich to create a Trumpism version of the Contract with America, a political strategy from the 1990s.
- The Contract with America was a party platform that allowed voters to vote for something rather than against something, credited for giving Republicans a victory in 1994.
- Trump understands the importance of the upcoming midterms for his legacy and the future of the Republican Party.
- Democrats face challenges with Trump's strategy as Biden's approach involves appealing to the center and left, but major legislation delivery is a concern.
- Democrats need a plan to respond to a potential Contract with America from Trump.
- Historically, the original Contract with America didn't fare well in terms of getting enacted, and some policies wouldn't be well-received today.
- Uniting voters behind something positive rather than just against Biden could be effective for the GOP.
- The main liability is Trump himself, known for not sticking to talking points or policies.
- The Democratic Party needs to prepare to address this strategy now to avoid being caught off guard.
- The midterms present an opportunity to challenge Trumpism and its influence within the Republican Party.

### Quotes

- "Trump understands the political landscape right now."
- "If they can peel off some of the center with a platform of some kind, it might be pretty effective."
- "The midterms are a chance to truly root out Trumpism."

### Oneliner

Former President Trump enlists Newt Gingrich to create a Trumpism version of the 1990s Contract with America, posing challenges for Democrats in the upcoming midterms and the future political landscape.

### Audience

Political analysts and strategists

### On-the-ground actions from transcript

- Prepare a strategic response plan to counter a potential Contract with America from Trump (suggested)
- Engage in proactive political campaigning and messaging to unite voters behind positive policies (implied)

### What's missing in summary

The full transcript provides a detailed analysis of the potential impact of a Trumpism version of the Contract with America on the upcoming midterms and the future political landscape. Watching the full video can provide a deeper understanding of the nuances involved. 

### Tags

#Politics #Elections #Trumpism #ContractWithAmerica #Midterms


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we're going to talk about the nineties, at least a little bit.
Um, we're going to have to do that because it appears that former
president Trump has, uh, come up with a plan to help the GOP win the midterms.
And it comes from the nineties.
He has reportedly been talking to Newt Gingrich, a name you probably haven't heard in a while.
Gingrich was responsible for something called the Contract with America.
And it was basically a party platform full of a bunch of little things that they honestly didn't care about actually
getting enacted anyway.  But it was something that allowed voters to vote for something, rather than against something.
And it appears as though Trump has enlisted him to help create a Trumpism version of the
contract with America.
It should be noted that the contract with America is kind of credited for giving
republicans a pretty decisive victory in 1994.
Trump understands the political landscape right now.
He knows that if the GOP does not do well in the midterms, specifically his candidates,
if they don't do well, he's done.
The Trump legacy ceases to be.
However, if they do succeed, well, he'll be back or his heir will be back and he will
have a continued sway over the Republican Party.
This poses a lot of problems for Democrats because overall, sure,
Biden's not Trump that's voting against something.
And Biden is doing a relatively decent job of piecemealing a lot of little
policies through that are appealing to the center, which is what he planned to do.
And occasionally he throws something out for those that are a little bit more on the American
left side of the political spectrum.
But if he doesn't really deliver on major legislation between now and then it's a concern.
The Democratic Party is going to have to respond to a contract with America coming from Trump.
They have to have a plan.
It is, when you're talking about somebody like Trump, sure, it's easy to motivate a
group of people to vote against him.
And let's be honest, that's a large part of how Biden got in office.
But when you're talking about the midterms and you are talking about a group of people
who appear to have a plan, even if you go back and look,
historically, the contract with America
didn't actually do too well.
They didn't get most of it enacted.
A lot of it got vetoed.
It didn't actually do too well.
And even the stuff that did get enacted was stuff like, hey,
we're not going to give teen moms welfare, not stuff that
would go well today.
But that idea of uniting somebody or uniting a group of voters behind voting for something
rather than just against Biden, if they can do that, because they already have that base
that is energized to oppose Biden no matter what he does, if they can peel off some of the center
with a platform of some kind, it might be pretty effective.
The biggest liability to it is Trump himself, to be honest, because he's not known for sticking to talking points or
platforms or policy even.  But this is a concern, and it is something that the Democratic Party has to begin
getting ready to deal with now.
There is a little debate over how successful it was in 94, but not much.
Most people will say that it had a lot to do with that decisive victory.
If Trump gets one, the Republican Party is his.
The midterms are a chance to truly root out Trumpism.
If it doesn't happen, we're going to be stuck with it for a while.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}