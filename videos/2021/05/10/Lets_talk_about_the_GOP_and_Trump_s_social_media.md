---
title: Lets talk about the GOP and Trump's social media....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZJtnwzudd3Q) |
| Published | 2021/05/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Pointing out the GOP's efforts to maintain Trump's base and keep those voters engaged.
- Describing the complete disarray within the GOP, with attempts to oust leading politicians and internal censorship.
- Expressing skepticism about the existence of the base Trump inspired after the Capitol incident.
- Noting a significant decline in Trump's social media interactions post-January 6th.
- Mentioning the lack of enthusiasm from Trump's fervent supporters, despite access to his social media platform.
- Observing a decrease in Trump's influence in fueling ideologies like fascism and authoritarianism.
- Commenting on Republicans causing chaos within their party, leading it towards a dumpster fire.
- Quoting Lindsey Graham's acknowledgment of potential destruction within the GOP.
- Noting that GOP's disarray is preventing them from presenting a platform opposing Joe Biden, hindering progress in the country.
- Stating that Republicans oppose Democratic proposals in an effort to please Trump's base, which is now significantly diminished.

### Quotes

- "We will get destroyed and we will deserve it."
- "It's kind of going out."
- "And it is glorious to watch."
- "Nothing can get done."
- "Tapping into a base that at best is 10% of what it used to be."

### Oneliner

The GOP struggles to maintain Trump's dwindling base, facing internal chaos while hindering progress in opposition to Biden, leaving the country stagnant.

### Audience

Political observers, activists

### On-the-ground actions from transcript

- Monitor and analyze political developments within the GOP to understand the shifting landscape (implied).
- Stay informed on how internal party dynamics affect national governance (implied).

### Whats missing in summary

Insight into the potential long-term impacts of the GOP's focus on Trump's base and the implications for future political strategies.

### Tags

#GOP #Trump #SocialMedia #RepublicanParty #PoliticalAnalysis


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about somebody we haven't talked about in a while.
It's been nice, but something has occurred, some information has been released,
and I think it's important to point it out.
We're going to talk about the GOP's quest to maintain Trump's base,
to keep those voters active, to tap into that base that he inspired.
Right now, the GOP is in complete and utter disarray.
They are trying to oust one of their leading politicians,
they are censoring each other, all because those people did not bend the knee
and kiss Trump's pinky ring.
I have made no secret of the fact that I don't think the base that they're trying to tap into
even exists anymore.
I think it disappeared the day of their little attempt on the Capitol.
I think that was the end of it.
Doesn't mean that he couldn't restart it and rekindle those flames.
But at the moment, it's gone.
Here's the thing. We now have data to back that up.
Trump's power was social media.
His power, his ability to inflame that base,
was directly derived from social media.
The week of the 6th, he had 300 million interactions on Facebook.
Things that mentioned his name that were liked or shared or whatever. 300 million.
This week, 30. 90% reduction.
They don't care about him anymore.
Twitter's the same way. The week of the 6th, it was 50 million.
Right now, 3.5. More than a 90% reduction.
It's not just because of the ban. It's not just because of the bans either.
Because his fervent supporters, well, they still have access to his social media platform thing,
blog, whatever that is.
And they could be using that.
If anything, he should be able to inspire them to talk about him even more.
But he can't. He can't.
He's getting pre-candidacy numbers. They're over him.
Without him out there to fan the flames of fascism, authoritarianism, whatever you want to call it,
it's kind of going out.
Meanwhile, there are a whole bunch of Republicans that are fanning the flames within their own party
and turning the GOP into a straight up dumpster fire.
And it is glorious to watch.
Because I think Lindsey Graham said it best when he was talking about Trump.
We will get destroyed and we will deserve it.
And that's where they're at right now.
The only bad part about this is that because they're in such disarray,
they don't have a platform at all of their own.
They don't oppose Joe Biden.
The problem with that, not only is it bad for their party, it's bad for the country.
Because nothing can get done.
Because it doesn't matter what Democrats propose,
Republicans, in an effort to appease a failed candidate,
well, they'll oppose it.
They'll fight against it.
And it's a case of tapping into a base that at best is 10% of what it used to be.
Anyway, it's just a thought.
Thank you.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}