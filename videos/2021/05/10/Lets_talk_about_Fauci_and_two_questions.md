---
title: Let's talk about Fauci and two questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tHX6lltxAoE) |
| Published | 2021/05/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the seasonal aspect of mask usage and the need to potentially get used to it.
- He expresses his willingness to continue wearing a mask as long as experts deem it beneficial to society.
- Beau shares that wearing a mask doesn't bother him and, in fact, he finds it somewhat enjoyable as he hasn't had to interact with unwanted high school acquaintances.
- He dismisses the notion that mask-wearing is a control mechanism, attributing such beliefs to a desire for order in a chaotic world.
- Beau references the effectiveness of facial recognition software demonstrated by the federal government, suggesting that any authoritarian group with such technology wouldn't encourage face coverings for control.
- He believes that the official COVID-19 numbers are likely undercounted based on personal experiences and lack of widespread testing in his area.
- Beau reiterates his stance that masks are not a form of control and expresses his comfort with wearing masks, even citing previous experiences with gas masks.

### Quotes

- "The world is a chaotic place and these theories attempt to make sense of the chaos."
- "I definitely believe the official numbers are low."
- "I do not believe masks are some kind of control mechanism."
- "It doesn't make sense and yeah it's not a big deal."
- "If it helps yeah I'll keep doing it."

### Oneliner

Beau addresses seasonal mask usage, expresses willingness to wear one based on expert consensus, and dismisses mask-wearing as a control mechanism.

### Audience

Individuals concerned about mask usage and control narratives.

### On-the-ground actions from transcript

- Follow expert recommendations on mask-wearing (implied).
- Advocate for widespread testing and accurate reporting of COVID-19 cases (implied).
- Encourage others to wear masks based on scientific evidence (implied).

### Whats missing in summary

The detailed nuances and personal anecdotes shared by Beau in the full transcript.

### Tags

#MaskUsage #COVID19 #ControlNarratives #ExpertConsensus #FacialRecognition


## Transcript
Well howdy there internet people it's Beau again. So today we're going to talk about the news from
the good doc. He said that mask usage may be seasonal as in we might should get used to it.
I've had two questions come in about it. The first is are you going to continue to wear a mask
you know when the time comes even though you're vaccinated. I will wear a mask as long as there
is a general consensus among experts that it is even remotely beneficial to society.
Wearing a mask does not bother me in the least at all. In fact in some ways I kind of like it
since this whole thing started I have not had to talk to a single person from my high school
that I didn't want to talk to. To me it's been an overall net win. I don't mind it. I don't think
it's a big deal. If it is even remotely beneficial yeah I'll do it. It doesn't bother me.
The other question was from the other side of this.
Don't you think that the continued push to wear masks is actually proof
that it's just a way to keep people under control?
Dun dun dun. No I don't believe that at all. In fact that doesn't even make sense.
I've said before I think people believe a lot of wild theories because it's comforting. It is
comforting to believe that somebody somewhere or some group of people is in control of everything.
The world is a chaotic place and these theories attempt to make sense of the chaos. This one
doesn't even make sense. The federal government just demonstrated very clearly exactly how effective
facial recognition software is. Because of the events at the capitol they showed the entire
country what they can do with that. I am 100% certain that no authoritarian group
that has that technology will ever encourage people to cover their faces again as a control
mechanism. That doesn't it just doesn't even make sense. No I don't believe that's what it is.
I think that he brought it up because in that type of weather it spreads easier
and this would help mitigate it. I think it's that simple.
And another part to that question was something about the numbers being revised up. Do I believe
that study? I believe that it was undercounted. That the total lost was undercounted. I don't
know that I think it's almost to a million yet but I'm fairly certain that the official numbers
are undercounted because when my family got sick and by all signs that's what it was. It wasn't
even supposed to be in Florida yet and then it turned out later that yes in fact it was.
And there were no cases in my area but people in this area don't really go to the doctor. There
were some in all the major towns around us though. So it stands to reason that yes there were cases
here and because it wasn't even supposed to be here yet they weren't counted.
I definitely believe the official numbers are low. I don't know that I believe they're
as high as that study shows but at the same time I haven't really looked into that study that much.
It could be. What I will say is I do not believe masks are some kind of control mechanism. That
doesn't even make sense and yeah I don't know. I don't know. I don't know. I don't know.
It doesn't make sense and yeah it's not a big deal. I've worn like gas masks and stuff before
for extended periods. Now I'm not the least bit concerned about wearing a mask when I go into a
store. If it helps yeah I'll keep doing it. Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}