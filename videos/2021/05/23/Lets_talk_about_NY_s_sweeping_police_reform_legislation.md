---
title: Let's talk about NY's sweeping police reform legislation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wSX-6N_j3-s) |
| Published | 2021/05/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the legislation from New York regarding police reform, questioning whether it's accurately described as sweeping.
- Emphasizes the importance of using the minimum necessary force to effect an arrest as the gold standard of policy.
- Points out that the legislation clearly defines the minimum amount of force needed for an arrest.
- Stresses that the legislation prohibits escalating a situation and then using force or using force on someone already subdued.
- Notes that while the union is pushing back against the legislation, it's a unique template that could be replicated in other states.
- Clarifies that if officers are unsure if they can use force, they shouldn't, as they should always aim to use the minimum necessary force.
- Explains that the legislation requires officers to deescalate, exhaust other means before force, and doesn't create a strict step-by-step process.
- Mentions that some attorneys believe the legislation may make it easier to create reasonable doubt, but the legislation will still make it easier to charge officers.
- Shares feedback from a former cop and deputy who found the legislation to be common sense.

### Quotes

- "The minimum necessary to effect the arrest. That's what you should use. That's the gold standard of policy around the country."
- "If you don't know if you're allowed to use force, you're not."
- "It's good legislation. And it doesn't handcuff law enforcement. It makes them try to deescalate."
- "This is just common sense."
- "Y'all have a good day."

### Oneliner

Beau explains the New York legislation on police reform, focusing on using the minimum necessary force and promoting de-escalation. 

### Audience

Lawmakers, Police Departments

### On-the-ground actions from transcript

- Read through and understand the legislation (suggested)
- Advocate for similar legislation in other states (suggested)

### Whats missing in summary

The full transcript provides additional context and personal anecdotes that enrich the understanding of the legislation and its implications.

### Tags

#PoliceReform #Legislation #UseOfForce #Deescalation #LawEnforcement


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about some legislation out of New York, how that legislation is being
described and whether that description is accurate.
We're going to talk about policy and gold standards and we'll go from there.
So the legislation is being described as sweeping police reform legislation.
I don't know if that's true.
If you have watched my videos on this topic you have heard me say the phrase, the minimum
amount of force necessary to effect the arrest.
That's what policy should be when it comes to use of force.
The minimum necessary to effect the arrest.
That's what you should use.
That's the gold standard of policy around the country.
That's what it should be everywhere.
What does this legislation do?
It clearly defines the minimum amount of force necessary to effect the arrest.
That's what it does.
I don't know that this is sweeping.
This is taking good policy and turning it into legislation.
That's unique.
It really is.
Plain and simple, it basically says, hey, you can't escalate a situation and then use
force.
You can't use force on somebody who has already been subdued because that's been an issue
lately.
That's really what it boils down to.
I don't know that this is sweeping.
This is standard.
This is probably what's already written down as policy.
The difference is now it's law.
And since it's law, there's criminal penalties that go along with violating it.
This is a unique template that can probably be reproduced in other states.
It's a good idea.
The union, of course, is pushing back against it.
They're basically saying that, well, under this, we won't even know if we're allowed
to use force, so our only option will be to avoid confrontation.
Okay, let me clear this up for you.
If you don't know if you're allowed to use force, you're not.
You're supposed to be using the minimum.
So if there is confusion, you're not looking to use the minimum.
You're trying to find what you can justify.
So the minimum amount of force necessary to effect the arrest.
And it defines it in a couple of different ways.
And when it comes to the actual definition and what's going to matter for charges, it
basically means you have to actually believe that was the minimum and a reasonable person
has to believe that was the minimum.
So it's no longer just the officer's perception.
That's good.
It's good legislation.
And it doesn't handcuff law enforcement.
It makes them try to deescalate.
It requires them to exhaust other means before using force.
And it stops short of creating a step-by-step thing that has to be followed.
Because while that sounds like a good idea, it's really not when you get into the different
scenarios that can occur.
You can try deescalation, right?
That would be the first thing.
Terrible warnings.
But if somebody comes out swinging a bat at someone, you don't have time to run through
the steps.
So what happens is the officer ends up going to that final step and using lethal force
rather than just jumping to pepper spray.
That pattern, locking it into a step-by-step process isn't actually good.
This is good.
It's written well.
I've heard some attorneys say that it may be easier for cops, attorneys to create reasonable
doubt.
But given the fact that this is going to make it easier to charge them, I don't know that
that's going to matter as much.
It's going to curtail the behavior.
Now as a side note, I sent this to somebody who used to be a cop, used to be a deputy
down here.
I've talked about him before.
The guy who said that if he did that, his sheriff would meet him in the parking lot
with a mag light.
It's a big flashlight.
There were people that had questions about it last time.
Down here they used to hit you with that instead of a baton.
He read through it and said, this is just common sense.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}