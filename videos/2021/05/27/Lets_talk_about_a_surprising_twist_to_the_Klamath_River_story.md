---
title: Let's talk about a surprising twist to the Klamath River story....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_FRyuoTbfMc) |
| Published | 2021/05/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides an update on the Klamath River story in Oregon and Northern California.
- Addresses a situation involving low water levels in the river, endangered species, native fishing communities, and irrigation for farmers.
- Mentions the closure of the canal to preserve water levels, fishing, and endangered species.
- Two farmers have bought land near the head gates and set up an information center to petition for grievances.
- Notes the connection of the farmers to Amon Bundy, known for involvement in public lands issues.
- Farmers express concerns about potential government intervention and mention past instances of forcing open head gates.
- Points out a historical similar situation where farmers had clashed with US Marshals over the head gates.
- Hints at a potential national story developing in Oregon with the first demonstration scheduled to happen soon.

### Quotes

- "The only thing separating us from the head gates is a chain link fence."
- "It's worth noting that about 20 years ago there was a similar situation."
- "We may all be turning our eyes to Oregon for a national story."

### Oneliner

Beau provides an update on the Klamath River situation, hinting at potential conflict with federal intervention and past history repeating itself, setting the stage for a developing national story.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Join or support demonstrations near the head gates to show solidarity (suggested)
- Stay informed about the developments in the Klamath River situation (implied)

### Whats missing in summary

Full context and depth of the developing conflict around the Klamath River situation can be better understood by watching the full video.

### Tags

#KlamathRiver #Oregon #NorthernCalifornia #AmonBundy #NationalStory


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to provide a surprising
update to the Klamath River story. Earlier this month I did a video about a situation
in Oregon and Northern California surrounding the Klamath River. I'm going to do a quick
recap on what was going on and then we'll move on to the new developments. Okay, so
there's a drought. This river, its water levels are low. The river has endangered species
in it. It also flows through a native area and fishing is their way of life. In addition
to this, there's a canal off of the river that feeds irrigation to farmers. The question
at the time was what's going to happen? They did in fact close the canal. They cut
off that irrigation to keep the water levels up to preserve the fishing and the endangered
species and all of that. When I covered the story, I did it because it's an interesting
story with a lot of moving parts and it fit the theme at the time. I saw it developing
sometime next month. I imagined something would occur with it. The developments have
started now. Two farmers have bought land right near the head gates. Now the head gates
are what would be used to open the canal. And they have set up an information center
there. The idea is it's a place for them to gather and petition for a redress of their
grievances. These are all fundamental rights in this country. You're allowed to assemble
like this. This is kind of what I expected to occur next month. What I didn't expect
was for the two farmers to be connected, reportedly connected via some group to Amon Bundy. You
may remember that name from other recent stories involving public lands. It's also worth
noting that one of the farmers has said, the only thing separating us from the head gates
is a chain link fence. It's good access, alright. And they got the land because they
didn't want to be run off by the federal government. This is a place where they can
gather because they own it. They can't be dispersed. It's worth noting that about
20 years ago there was a similar situation and the farmers forced the head gates open
multiple times and the US Marshals had to be called out. So, we may all be turning our
eyes to Oregon for a national story. It is also worth noting that the first demonstration
is supposed to take place today. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}