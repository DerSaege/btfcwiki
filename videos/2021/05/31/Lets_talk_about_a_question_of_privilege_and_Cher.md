---
title: Let's talk about a question of privilege and Cher....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-899grNICzQ) |
| Published | 2021/05/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Receives a message about being mindful of offensive terms and implications.
- Learns about the impact of a term he used casually on a specific group, leading to discovering more about the Roma people.
- Emphasizes the value of understanding why terms are offensive and the knowledge gained from this process.
- Raises a question about when white people will receive the same consideration in avoiding offensive speech.
- Explains the different interpretations of the question posed, indicating the ongoing challenge of being edgy and potentially offensive.
- References Cher's song "Gypsies, Tramps, and Thieves" as an example of addressing sensitive topics indirectly to navigate censorship.
- Mentions how the song was altered to remove potentially offensive terminology towards white people, reflecting on societal dynamics.

### Quotes

- "If you find out why terms are offensive, you will definitely stop using them."
- "When you are being edgy, you risk offending people. If you're going to do that, you better have a point."
- "Because nobody wants to risk offending a group with a whole lot of buying power."
- "There's perhaps a tendency to blow things that would be less offensive out of proportion."
- "Y'all have a good day."

### Oneliner

Beau receives a message prompting reflection on offensive terms, leading to insights on the Roma people and questioning when white people will receive the same consideration in speech.

### Audience

Social activists, Allies

### On-the-ground actions from transcript

- Engage in respectful communication and learn about the implications of certain terms (suggested)
- Share knowledge about marginalized groups to increase understanding and empathy (implied)

### Whats missing in summary

The nuances and historical context discussed in the full transcript provide a deeper understanding of societal dynamics related to offensive speech and privilege.

### Tags

#OffensiveTerms #CulturalAwareness #SocialJustice #MarginalizedGroups #Understanding


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about a message I received,
share some music history, and how things sometimes just all
tie together.
I got this message, and the gist of it
is, hey, I took advice from you and a bunch of other people,
and I started really paying attention to what I was saying
and going out of my way to make sure that I didn't offend
anybody for no reason.
And it would be good of you to share with everybody
that if you do this and you find out
why the terms are offensive, you learn all kinds of cool things
along the way.
Because I just found out a term I have used my entire life
to just mean that somebody was scammed,
somebody was ripped off, relates to a certain group of people,
and that a whole lot of them don't even
like being called that.
And then I wound up finding out a whole bunch of stuff
about the Roma people.
It's a cool message.
And yeah, that's a really good point.
If you find out why terms are offensive,
one, you will definitely stop using them.
And two, you do learn a lot of cool little bits
of information along the way.
And you'll get exposed to other cultures
that you normally wouldn't.
And then the last sentence.
If everybody does this, when do you
think white people will get the same consideration?
And I honestly don't know what that means.
Because it can be interpreted a couple of different ways.
It could mean, when will different ethnicities
that are white stop saying offensive stuff to each other?
It could mean that.
It could mean, when will everybody
stop saying offensive stuff to each other like all the time?
When will white people get it and stop doing it?
And the answer to that is never.
That's never really going to happen.
Because there's always going to be that drive to be edgy.
And it doesn't matter what the topic is.
That drive is going to be there.
And when you are being edgy, you risk offending people.
That's part of it.
If you're going to do that, you better have a point.
And then there's the obvious one,
that this person doesn't believe that white people get extended
that same courtesy of trying not to offend them at large.
That doesn't happen.
So the thing is, Cher kind of answered this question
a long time ago.
And it does.
It all ties together with that message.
She performed a song called Gypsies, Tramps, and Thieves.
And this is a long time ago.
And given its age, it's actually aged OK, even with that title.
It was designed to be edgy.
It was designed to be edgy.
There's a whole lot of stuff in it that she talks around.
Because at the time, if she actually just
said what she was talking about, well,
it wouldn't have gotten the airplay.
So she talks around it.
And in this very short song, less than 180 seconds,
she brushes up against racism, classism,
the cycle of poverty, all kinds of stuff.
All things being equal, I mean, it aged OK.
But here's the twist to that.
Her producer sent the original song back to the songwriter
because the original title had the term white trash in it.
Because white people are the majority population
in the United States, by default, under capitalism,
they will always get that consideration.
Because nobody wants to risk offending a group
with a whole lot of buying power.
That song got reworked way back then
to make sure it wasn't offensive to white people.
At least that's how it's reported.
I wasn't there when it happened.
But that's what's said.
I think if that was the meaning of that last sentence,
if that was the question being asked,
it might go more to the fact that because white people
really don't actually experience a whole bunch
of offensive stuff being thrown at them,
there's perhaps a tendency to blow
things that would be less offensive out of proportion.
And if you are part of a group that
doesn't experience something, that
might be a privilege of sorts that is granted to you
based on how you appear.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}