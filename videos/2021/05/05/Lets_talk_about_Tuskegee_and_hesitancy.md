---
title: Let's talk about Tuskegee and hesitancy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HBnMN4nDefY) |
| Published | 2021/05/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the hesitancy of a certain group to get the COVID shot.
- Grandma's reluctance to get vaccinated due to fears related to the Tuskegee experiment.
- Explaining that Grandma's concerns are not irrational but based on historical events.
- The challenge of using rhetoric to persuade Grandma to get vaccinated.
- The impact of historical events like Tuskegee on individuals who experienced them firsthand.
- Emphasizing the importance of presenting factual information to address Grandma's concerns.
- Comparing past events like Tuskegee to the current COVID situation in terms of study and choice.
- Pointing out that Grandma's perspective is shaped by her life experiences and memories.
- Acknowledging the difficulty in finding common ground due to differences in experiences.
- Suggesting that having a grandchild around might help in addressing Grandma's concerns.

### Quotes

- "It's not irrational. It's not irrational."
- "You get to choose whether you're in the group that gets treatment, its protection or not."
- "She's not a rational. She's got a reason."
- "She has a reason to believe what she believes is to come at it with facts."
- "There are people alive today who were alive when it happened."

### Oneliner

Beau addresses the hesitancy of a specific group to get vaccinated, focusing on Grandma's valid concerns rooted in historical events and the importance of presenting facts.

### Audience

Family members

### On-the-ground actions from transcript

- Have open and honest conversations with hesitant family members about vaccination (suggested)
- Present factual information and historical context to address concerns (exemplified)

### Whats missing in summary

The full transcript provides a detailed and empathetic exploration of addressing vaccine hesitancy rooted in historical trauma, offering insights on approaching the issue with understanding and facts.

### Tags

#VaccineHesitancy #HistoricalTrauma #FamilyCommunication #FactualInformation #Empathy


## Transcript
Well, howdy there, internet people.
It's Bo again.
Happy birthday, Jackson.
Today, we're going to talk about a different group of people
that are hesitant to go get the shot.
Because I got a question.
And to be honest, I'll go ahead and tell you now,
I don't know that I have a solution to this one.
Um, basic statement is,
Grandma's being irrational.
She won't go get the shot.
She's talking about the Tuskegee experiment.
Grandma's black.
Irrational.
No, not in this case.
It's not irrational.
Irrational means without reason.
She has a reason.
One of the reasons I can normally come up
with good arguments is because I take people's rhetoric,
the stuff that they say, and I turn it back on them
to get them to question what they're saying.
I can't get grandma to question
whether or not the Tuskegee experiment happened.
It did.
It did, it's a historical fact.
That occurred.
And to be clear, that's the best-known
example of that happening. Not the only one. There are events like that throughout American
history. And there's the other part. To me, to you, that's history. To Grandma, that's a memory.
She was alive when that happened. Almost guaranteed.
It's not irrational. It's not irrational.
The good news is that since she isn't irrational, you may just want to use facts.
You may just want to come at it from the point of view of
of, yeah, yeah, that absolutely happened.
And the thing that makes those events so horrific
is not just did people who should be trusted,
people in position of authority,
not just did they fail to provide
or flat out deny treatment and protection,
once they did that, they studied it
Or they came back later and studied it.
Study the effects.
Typically, to benefit people who didn't look
like those who were impacted.
That's what makes it more horrific.
And this is going to be just like that.
They're going to study this.
The difference is today, you get to choose
which group you're in.
You get to choose whether you're in the group that gets treatment,
its protection or not. But make no mistake about it, it's going to be studied. It's
going to be very similar. They'll run the same type of stuff to see how it
progressed and who it impacted. I'd also point out that when history books talk
about the Tuskegee experiment and there are articles about it. They normally use
black-and-white photos and talk about when it started. It didn't end until the
70s, until the 1970s. It's not ancient history. It is incredibly likely that
Grandma was alive when that happened. To her it's a memory. She's going off of her
life experiences. That's a whole lot harder to overcome than somebody
who's just doing something for social capital or repeating what they heard or
They bought into a Facebook meme.
She's not a rational.
She's got a reason.
Now, the other reason that I can normally come up
with good arguments is because I can typically
find some way to relate to the argument being presented.
I can't.
I don't have anything in my life experience
that puts me in a position to relate
to where she's coming from because the people she can't trust because of her life experience
throughout all of mine, I could trust.
And I don't have anything that would allow me to see it through her eyes or even come
close to it.
So there may be a better argument out there, but I'm not going to be able to find it.
The normal tools that I use to come up with those arguments,
neither one of them are going to work here.
I think your best bet is given the fact
that she isn't being irrational.
She has a reason to believe what she believes
is to come at it with facts.
Might help to have a grandbaby around too.
That never hurts.
Just, when you're talking about this stuff, understand it always gets presented like it
happened a really long time ago.
There are people alive today who were alive when it happened.
It's not ancient history.
So it's not an unfounded fear.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}