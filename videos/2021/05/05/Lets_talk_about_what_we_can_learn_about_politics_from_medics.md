---
title: Let's talk about what we can learn about politics from medics....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nAHQMvrp6ic) |
| Published | 2021/05/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the unique role of street medics at demonstrations expected to get wild.
- Talks about how street medics experience a lull in activity during cold weather and start cooperating and coordinating during warm weather.
- Describes the challenges faced by street medics when they get bored, leading to cliques, in-fighting, and decreased effectiveness.
- Suggests ways to maintain effectiveness in groups like community networks or mutual assistance groups during lulls.
- Recommends training, staying on mission, and incorporating fun activities to prevent cliques and maintain focus.
- Advocates for healthy competition and mixing opposing groups to foster camaraderie.
- Encourages political engagement beyond election seasons, focusing on ongoing issues and causes.
- Stresses the importance of staying on mission, avoiding infighting, and maintaining effectiveness in achieving goals.
- Urges individuals to reach out and stay connected with their circle to remain effective in their activism.

### Quotes

- "Street medics, well they don't have anything to do. They get bored and when they get bored, well that's when cliques start to form."
- "Find something to focus on. Don't just rally around the politician during election season."
- "You have more contacts. And it makes you and your circle a little bit more politically valuable."
- "You can't let up."
- "If you want to stay effective, if you want to actually accomplish stuff, you can't let up."

### Oneliner

Beau explains the importance of staying focused, avoiding cliques, and maintaining effectiveness in activism, drawing lessons from street medics during demonstrations.

### Audience

Community activists

### On-the-ground actions from transcript

- Reach out to your circle of activists to stay connected and effective (suggested).
- Train and stay focused on mission objectives in community groups (implied).
- Organize fun activities or events within your activist network to build camaraderie (implied).
- Campaign for ongoing causes beyond election seasons (implied).

### Whats missing in summary

The detailed examples and nuances of Beau's storytelling and insights.

### Tags

#CommunityActivism #MaintainingEffectiveness #PoliticalEngagement #ActivistNetworks #StayingFocused


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're gonna start off talking about
a pretty narrow group of people,
but the lesson there expands to everybody.
We're gonna talk about what we can learn from medics
and how to stay on mission, even if you start to get bored.
Okay, so first, if you don't know, at demonstrations that are expected to get wild, there are medics,
street medics, who will show up.
People whose explicit purpose for being there is to pull you out of the tear gas.
They are a unique group of people, they're pretty wild, and they're kind of weird too,
they're cool. One of my friends does this and she asked me a while ago, you know,
how do we stop the dissolving when the bad weather comes? Because if you're not
aware, demonstrations, there's a lot more of them when the weather's nice. When it
gets cold, nobody wants to go out and march. So that means that during the
wintertime, when the weather is not nice, street medics, well they don't have
anything to do. When the weather is nice, when it first starts to warm up and
everybody gets out and starts talking about their causes and demanding a
redress of their grievances, they have a lot to do. And these medics, they end up
getting to know each other. They start coordinating, they start cooperating, and
and they make sure that somebody is at every demonstration, just in case, it turns into a
unit. They develop a lot of camaraderie and it lasts until it starts to get cold. Then they
don't have as much to do. And you are talking about high energy people. You're talking about
the kind of people that will rush into tear gas to pull you out. They don't do well without something
to do. They get bored and when they get bored, well that's when cliques start to form. Purity testing,
in-fighting, personality conflicts, and the effectiveness of the whole group starts to do
great because, well, they don't have anything better to do so they end up fighting each other.
So how do you avoid that? How do you keep up that effectiveness so you don't have to reboot
the next year. There's a bunch of ways, and this applies if you're running a
community network, a mutual assistance group, whatever. If you have some calls,
some group that you're active with, and it's a focused thing, when there's a
lull, that's when things start to degrade. That's when things start to
fall apart. Now, if you're talking about street medics, you could always train, you
know if it's illegal in your area because I'm sure somewhere there's some
law saying you can't train for first aid unless you're credentialed, but you could
train. Keep everybody on mission. Keep everybody focused. It's also important to
do something fun every once in a while. Binge watch a TV show, have a cookout, go
canoeing, whatever. If something that you're doing is a group that isn't really related to what you
normally do because it allows for there to be a personal bond that isn't directly related to
running into teargash. Now if you start to see clicks develop that that's when a little bit of
a healthy competition is normally good, but not between clicks. Split them up. Take half
of each opposing click and put them on the same team and pit them against each other.
So people from different clicks have to work together. In the past, I have found that paintball
is incredibly useful for this because it reinstills that camaraderie, puts them back on the same
team. So what about everybody else? If you're not involved in this type of stuff,
what about you? Every two to four years, people who are watching this channel,
you're probably pretty politically engaged. But once that season is over,
once election season ends, a lot of people go back to brunch. Brunch is still
canceled. We still have problems. So how do you stay on mission? The same way. Find
something to focus on. You know, don't just rally around the politician during
election season. This seems like it should go without saying, but maybe don't
trust the politician to do what they said they were going to do because they
may not. So you can keep up pressure on them. If you find one that actually is
effective and is representing you, you can fundraise for them. Or you could shift to
other causes. You could campaign for the environment. You could campaign for the homeless if you
happen to live in Austin. That seems like it might be needed right now. There's a
bunch of stuff out there. Help plant community gardens. Start a community network. Because
Because then when election season rolls back around, well, you're still effective.
And you have actually gotten better.
You have more contacts.
And it makes you and your circle makes you a little bit more politically valuable.
Because you'll be able to accomplish more because you have learned stuff along the way.
We have a lot going on, and we have to stay on mission.
The habit after elections of the infighting and the personality conflicts being elevated
to the status of something that matters probably isn't good.
It probably degrades overall performance and mission effectiveness.
This is probably something you want to avoid.
If you have a circle of people that during the election, they were your people, and you
haven't talked to them really since, you might want to reach out.
Because if you want to stay effective, if you want to actually accomplish stuff, you
can't let up.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}