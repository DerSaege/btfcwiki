---
title: Let's talk about how the GOP proved me wrong....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nqfSbY2Cf0Y) |
| Published | 2021/05/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Some members of the GOP convinced him he was wrong about social programs creating freeloaders.
- The GOP's point is that social programs without an incentive create freeloaders.
- Beau realized they were referring to themselves, not patriotic, civic-minded people.
- They resist masks and vaccines because there's no incentive.
- Beau got his second vaccine dose to exercise civic-mindedness and protect his community.
- He felt unwell for about 16 hours post-vaccine but believes it's worth it.
- Beau stresses the importance of not being a freeloader and doing your part for society.
- Those opposing masks and vaccines are in a demographic catered to their whole life.
- Beau got vaccinated not for himself but to protect others.
- He encourages people to understand they're not talking about you, but themselves.

### Quotes

- "They are talking about themselves."
- "I don't want to be a freeloader."
- "I want to do my part."

### Oneliner

Some GOP members convinced Beau social programs create freeloaders, but they're really talking about themselves, as seen with vaccine refusal.

### Audience

Community members

### On-the-ground actions from transcript

- Get vaccinated to protect your community (exemplified)
- Exercise civic-mindedness by getting vaccinated (implied)

### Whats missing in summary

The emotional journey and personal reflection of Beau in realizing the GOP's perspective on social programs and vaccine refusal.

### Tags

#GOP #SocialPrograms #VaccineRefusal #Community #CivicResponsibility


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about how some members of the GOP
convinced me I was wrong about something.
Doesn't happen often, but it does happen occasionally.
They made me realize that one of the points they use a lot is true.
I just hadn't thought about it enough to understand what they were saying,
what they were really getting at when they said it.
You know, when you talk about social programs,
you will have members of the GOP, people who have that outlook.
They'll say, you know, if you have programs like that,
where people get a benefit without contributing,
well, it just creates freeloaders.
They have to have an incentive to do anything.
Otherwise, the system will be overburdened by freeloaders,
people who just won't contribute to society.
I never believed this, and I always dismissed it out of hand.
But I'd also never thought about it.
Never thought about it because it runs counter
to everything I have ever seen in my own life.
The working class people I know that have made it big,
that have made a lot of money, they always, without exception,
help out the little guy.
Those people I know who have free time on their hands,
they always help out.
They always continue to exercise that social responsibility,
that civic-mindedness, that patriotism even, without an incentive.
It wasn't until this week that I realized
they're not talking about the worker.
They're not talking about patriotic people.
They're not talking about people who are civic-minded.
They aren't talking about those who have social responsibility.
They are talking about themselves.
Don't believe me?
That's exactly what they're doing with the masks and the vaccine.
Society is doing this thing together so everybody can benefit.
But nobody's forcing them to go and get the shot.
There's no incentive for them to do it, so they're not going to.
They're going to count on society at large to take care of them.
They'll come up with some excuse as to why they shouldn't have to wear a mask.
They shouldn't have to get the shot.
They're not talking about people who care about the country.
They're not talking about people who want to be part of the solution.
They're talking about themselves.
The reason they know these freeloaders will exist
is because if they had the chance, they would do it.
If they didn't have that economic incentive, well, they wouldn't work.
They wouldn't contribute.
It's wild, isn't it?
These are often the same people who will go on and on about patriotism.
Few months ago, they were talking about laying down their life in some imaginary civil war.
You don't have to get shot to help your country.
You can go get a shot and help your country.
But they have some excuse, some reason why they shouldn't have to participate.
See, most of those who are saying this, they're in that demographic
that's been catered to their entire life.
They expect that trend to continue, and they have no incentive.
They're in that demographic where they'll probably be okay.
They don't care if they get it and give it to somebody who won't.
I just wrapped up.
I got my second dose.
And I do have to be honest.
That was something else.
My arm felt like it had been hit with a baseball bat.
And I was down, like, didn't feel well, I don't know, probably 16 hours.
I think it's worth it to exercise that civic-mindedness, to help protect my community, to make sure
I don't hand it off to somebody else.
I'm not in an at-risk demographic for this.
But I don't want to let society down.
I don't want to let my community down.
I don't want to be a freeloader.
I want to do my part.
When you hear this argument, understand they're not talking about you.
They're not talking about the workers that you know.
They're talking about themselves.
And they're proving it right now.
Anyway, it's just a thought. And I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}