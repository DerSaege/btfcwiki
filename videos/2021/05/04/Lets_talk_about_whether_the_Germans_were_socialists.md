---
title: Let's talk about whether the Germans were socialists....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=odfUE7EnidA) |
| Published | 2021/05/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A friend reached out for help in explaining socialism to someone terrified of it due to World War II associations.
- The challenge was to explain without using history, politics, or ideology.
- Beau accepted the challenge but struggled to come up with an approach for two weeks.
- While looking for quotes by George Orwell, Beau stumbled upon a video where he quotes a piece of poetry.
- Beau recites a well-known poem that starts with "First they came for the socialists."
- The poem progresses to mention trade unionists and Jews, with the speaker not speaking out until it was too late for them.
- The point made is that if the persecutors were socialists, they wouldn't have targeted socialists first.
- Beau suggests that this simple and powerful poem can help explain the misconception about socialists during World War II.
- The poem provides a straightforward way to illustrate the issue without delving into complex historical or political details.
- Beau concludes by offering this poem as a tool for having a meaningful and accessible conversation on the topic.

### Quotes

- "First they came for the socialists and I did not speak out because I was not a socialist."
- "Your answer is in poetry."
- "It's not history-based. It doesn't have to require a whole lot of knowledge of different ideologies."
- "If this is true, then why would they do this?"
- "Y'all have a good day."

### Oneliner

A friend seeks help in explaining socialism without history, politics, or ideology; Beau uses a powerful poem to debunk misconceptions easily.

### Audience

Educators, Activists, Students

### On-the-ground actions from transcript

- Share the poem "First they came for the socialists" to debunk misconceptions about socialism (implied).

### Whats missing in summary

Exploration of the impact of using poetry as a tool for addressing misconceptions in political history.

### Tags

#Socialism #History #Poetry #Education #Misconceptions #GeorgeOrwell


## Transcript
Well howdy there internet people, it's Beau again.
So today
we're going to talk about
a seemingly impossible challenge
and poetry.
uh...
A couple of weeks ago
one of y'all reached out and said, hey I need some help.
I have a friend
who just is utterly terrified
of socialism
because the bad guys during World War II were socialists.
He doesn't know anything about politics or history or ideology so all he knows
is that the word socialist
was in the party name.
But because he doesn't know anything about these subjects
using them to talk to him isn't going to be helpful.
Can you give me a way to explain
that they weren't really socialists
without using
history,
politics, or ideology?
Sure, I will
try to come up with a
way to
talk about a historical political party
with ideology in the name without using history, politics, or ideology.
Challenge accepted, you know.
It's been two weeks
and I had nothing.
Couldn't come up with anything.
And then last night one of y'all asked for a video on quotes by George Orwell.
And I already had one.
When I went back to
find it
and
clicked on it
to be able to share it, it started playing.
And it started playing
at a part where I am quoting a piece of poetry.
It's a poem everybody knows.
First they came for the Jews and I said nothing because I was not a Jew.
The thing is
that's not actually
how it goes.
First they came for the socialists
and I did not speak out because I was not a socialist.
Then they came for trade unionists and I did not speak out because I was not a trade unionist.
Then they came for the Jews and I did not speak up because I was not a Jew.
came for me and there was no one left to speak for me." There you go. Your answer is in poetry.
If they were socialists, they probably wouldn't have come for the socialists. Now, to be clear,
there are multiple versions of this. One, I think, starts with, first they came for the communists,
socialist is always in it, in every version that I've seen. So, there's your answer.
It's not history-based. It doesn't have to require a whole lot of knowledge of different ideologies.
It doesn't have to do with politics. It's something simple and easy that you can point to
and say, if this is true, then why would they do this? And that seems like the level of
conversation you're looking to have. So, anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}