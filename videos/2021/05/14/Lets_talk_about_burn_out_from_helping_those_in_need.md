---
title: Let's talk about burn out from helping those in need....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-Vrk2vWeQUk) |
| Published | 2021/05/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about providing assistance to those in need and the emotional toll it can take on individuals involved in community networks.
- Describes a situation where a person's involvement in helping someone with substance abuse issues ended tragically.
- Emphasizes the importance of staying emotionally detached when providing direct assistance to avoid burnout.
- Shares a personal experience of a community helping a vet with substance abuse problems, which ultimately led to a heartbreaking outcome.
- Stresses the need to recognize one's emotional limitations and take a step back when feeling drained.
- Acknowledges that not everyone is suited for direct involvement in emotionally taxing situations, and that it's okay to take a different role in supporting a cause.
- Advises taking breaks when feeling overwhelmed to prevent burnout and maintain effectiveness in helping others.
- Mentions the importance of soldiers having tours and coming back, comparing it to the need for breaks in high-stress situations.
- Encourages individuals to continue supporting causes in different ways if direct involvement becomes too emotionally taxing.
- Reminds viewers that recognizing personal limitations and taking breaks doesn't indicate weakness, but rather self-awareness and self-care.

### Quotes

- "You have to try to remain detached on some level."
- "You don't have to be the tip of the spear."
- "When you start getting taxed, when you start feeling like you just can't do it anymore, it's time to take a break."
- "You can't stay on point all the time."
- "Recognize your limitations."

### Oneliner

Providing assistance to those in need can be emotionally draining; recognizing your limitations and taking breaks is vital to prevent burnout and continue making a difference.

### Audience

Community volunteers and activists

### On-the-ground actions from transcript

- Take breaks when feeling overwhelmed to prevent burnout (implied)
- Recognize personal emotional limitations and step back when needed (implied)
- Continue supporting causes in alternative ways if direct involvement becomes taxing (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the emotional challenges of providing mutual assistance and the importance of self-care in community activism.

### Tags

#CommunitySupport #EmotionalHealth #BurnoutPrevention #MutualAssistance #SelfCare


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about providing assistance to those in need, being the tip
of the spear, burnout, a whole bunch of stuff.
It's kind of a question and it's long, I'm not going to read the whole thing.
But basically the person has started getting involved in community networks and providing
mutual assistance, helping those in need.
And as a human being they've become attached and it's emotionally draining, it becomes
depressing.
You see the way people are living and you can't fix it all.
You can't right every wrong.
In some cases the best you can do is keep them where they're at.
And it can be pretty disheartening to look into the actual comments, something about
losing hope for humanity.
And yeah, absolutely, that can happen.
There's no doubt about it.
When you're doing that stuff, when you are the person who is in direct contact with those
you are helping, you have to try to remain detached on some level.
Because a lot of those cases, a lot of the people you're helping, you're not going
to help, not all the way.
And in fact it can end in tragedy at times.
This wasn't really mutual assistance type stuff, this was just something that was happening.
I worked at an office once.
Everybody who worked there was a contractor or a security consultant.
Very detached people.
But this guy, he was a vet and he had a substance abuse issue and he had reasons for self-medicating.
So everybody understood where he was at.
And by issue I mean it was so bad he was on the street.
He knew somebody who worked at this office.
And he started coming by.
And people would get him food, open up an office, let him go to sleep because what he
had an issue with caused him to not sleep for long periods of time.
And even once his friend quit, he still came around and everybody still helped him.
And it was going well.
We got weight back on the guy, got him to where he was at least somewhat functional
again, but he wasn't ready to quit.
He wasn't ready to give that up yet.
And then one day a guy who had just started working there, who was vaguely aware of him,
met him.
And was just like, you know what, here, gave him a lot of cash.
Was like, hey, get a hotel room, clean yourself up.
You have people that care about you.
We'll help you get a job, get back on your feet.
This guy wasn't ready to quit.
So that cash didn't go to a hotel room.
That cash went to something else.
And it was so much of it that that was it.
He was done.
That bothered me more than a whole lot of things have bothered me in my life.
And it hit everybody.
And you're talking about a group of people who are very detached, who are just like,
yeah, well, I mean, it is what it is kind of people.
But there is no psychological profile.
There is no type of person that is immune to everything.
And if you're going to be the tip of the spear, you're going to be that person who is actually
making contact.
You have to stay as detached as possible.
And if it starts to become a drain, and it starts to become emotionally taxing to the
point where you're starting to lose faith in humanity, remember, you don't have to be
the tip of the spear.
You can back out.
Maybe you're not the person distributing the food directly.
Maybe you're the person packing the bags, doing the fundraising.
You can move further away from being that point of contact.
Doing that isn't for everybody.
And that's not an insult.
To be super clear, if you watch this channel, you know I'm a huge supporter of domestic
violence shelters.
It's like a big thing with me.
Raise money, get supplies, do whatever they need.
I do not go pick people up.
And it's not because it depresses me.
I get angry, and that doesn't help anybody.
So I stay, I remove myself from that situation.
Your benefit is limited by how well you can control your emotions on that.
And if you can't do it, the right thing to do is to step back a little bit.
Let somebody else take point for a while.
And it's not, that's not an insult.
The reality is doing that for any period of time is taxing.
It can get depressing.
You're entitled to a break.
When you're talking about stuff like this, you do what you can, when you can, where you
can for as long as you can.
But when you can't do it anymore, take a step back.
Otherwise you will burn out and you will never do it again.
And that's not what anybody wants.
That doesn't help.
All of the stuff that you see, doing this kind of stuff, hearing the stories of the
people you're helping, yeah, it can cause you to lose faith.
It can also cause you to recommit.
Because while you can't save everybody, you can't right every wrong, you can fix a lot
of it.
And you can help the situation that those people are in at that moment.
And in some cases, that's the best you can do.
So when you start getting taxed, when you start feeling like you just can't do it anymore,
it's time to take a break.
That doesn't mean quit.
That doesn't mean stop completely.
Just take a step back.
There's other stuff you can do to support that same mission.
You just may not be the right person to be interacting personally with them.
And that doesn't say anything about you.
It just means that you have recognized your limitations.
Everybody has them in some way.
I feel for you.
Because you're in the fight, you're out there doing what you can, and you're paying the
price for it.
There's a reason that soldiers have tours and come back.
There's a reason that if you are doing high-stress stuff, you do it in short bursts.
There's a reason it works that way.
You can't stay on point all the time.
It will burn you up to the point where you cannot do any good.
So if you start to feel that way, it's time to take a break.
And there is nothing wrong with it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}