---
title: Let's talk about 1033 and nuance....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2IWT7mjMS6U) |
| Published | 2021/05/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the 1033 program, which transfers excess military equipment to law enforcement.
- Acknowledges the program's problems, like the militarization of police departments.
- Notes that rural counties sometimes benefit from the program for life-saving equipment.
- Stresses that the program's impact depends on the intent of the department and user.
- Mentions that while there are good uses, there are also cases of misuse leading to dangers in communities.
- Suggests that Biden could provide temporary relief but a real fix requires Congressional action.
- Proposes a restructured program accessible to various community entities beyond law enforcement.
- Points out the need for changes in how the program operates, especially in preventing militarization of police departments.
- Considers the possibility of scrapping the program entirely and rebuilding it from scratch for real change.
- Emphasizes that the issue is not just about the program being inherently bad but about how it is utilized.

### Quotes

- "It definitely facilitates the desire of departments to cast the image of the warrior cop."
- "The program itself is neither inherently good or bad. It's the intent of the department. The intent of the user."
- "It's not as simple as 1033 is bad."
- "That's what needs to change."
- "The real solution here would be Congress changing it completely."

### Oneliner

Beau explains the nuances of the 1033 program, advocating for changes to prevent its misuse in militarizing police departments.

### Audience

Congress, Biden administration

### On-the-ground actions from transcript

- Advocate for Congress to make significant changes to the 1033 program (suggested)
- Support initiatives that aim to prevent the militarization of police departments (implied)
- Push for a restructured program accessible to various community entities (suggested)

### Whats missing in summary

The full transcript provides a detailed exploration of the 1033 program, offering insights into its potential benefits and dangers, and calls for comprehensive changes to prevent its misuse.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a federal program,
nuance, and how to fix it.
There's a program pops up in the headlines pretty often and it is
certain to pop up again soon. And we got a question about it and I
really like the way it's phrased.
What is the 1033 program?
Is it good or bad? A lot of political organizations I trust
say that it's horrible and it needs to be ended now.
My professor says that it's not that simple.
Who's right and what can Biden do about it?
They're both right. They are both right.
And we'll get into that. But what can Biden do about it?
Biden by himself, he can provide some temporary relief to the problems.
If you want an actual fix, it's going to need Congress.
Okay, so what is it? It is a program run by the Defense Logistics Agency that
transfers excess military equipment to law
enforcement. Immediately you're like, that's horrible.
That's why we have MRAPs on the streets. Yeah, it is. It is. And that's how it gets
used. A lot. Way more than it should.
It definitely facilitates the desire of departments to
cast the image of the warrior cop. No doubt.
It has a lot of problems. However, it isn't that simple.
Because rural counties, they need this program.
And it's not for MRAPs. It's not for stuff like that.
The county next to me, years ago, it was not a wealthy county. It didn't
have a lot of tax revenue. But it did have a lot of beach. And when
the developers showed up and started developing down there, and
then the tourists showed up, started going out onto that beach, and then out
in the gulf, not realizing that even when it's calm,
it can be rough underneath, we started losing people.
The county there, they never could have afforded the equipment
they needed to save people. They didn't have the money.
Plain and simple. The tax revenue had not caught up yet.
They used 1033 to get the biggest zodiac I have ever seen. A giant rescue boat.
And some ATVs. Saved lives. Saved lives.
The program itself is neither inherently good or bad. It's the intent
of the department. The intent of the user. The problem is the intent of a lot of
departments right now isn't good. When you are talking about rural
counties though, here, if a kid goes missing out in the
woods, you know in the movies where the sheriff's like, you better call up Billy
and have him bring his dogs. That still happens. It would be great if
FLIR was available without having to call it an
infrared. And they could get it through this
program. Now realistically, I don't think my county could
actually even afford the gas for the helicopter, but the equipment would be
there if they could. So it isn't inherently bad. There are
good uses. The problem is that for every county, I can point to and say, hey,
they used it responsibly. I can point to 10 counties that are
basically Mayberry that fully outfitted an MRAP and a SWAT
team with it. And that's
not good. Because while they can get a lot of equipment through this program,
they don't get any training. So they get this stuff and they don't know how to
use it. And it winds up being a danger to the
community rather than saving lives. Okay, so
he could limit those transfers. He could stop those transfers. I actually think
he has the power to recall equipment that's already been
dispersed via executive order. However,
anything he does by executive order can be
undone by executive order. So the next president could come through
and start dispersing everything again. The real solution here would be
Congress changing it completely. What I would like to see
would be a single program that EMS, fire departments, law enforcement,
schools, libraries could all access. Because military equipment isn't just
limited to the stuff you're thinking about. The
military has filing cabinets. They have desks. They have audiovisual
equipment that they give away.
The idea of disposing of excess equipment in this way,
it's good. It is good. It's a good use of resources
and that taxpayer money doesn't get wasted.
The problem is it gets used a lot
to militarize police departments. That's not good. It ends up being a danger
to the community rather than something to help it.
That's what needs to change. Now realistically, in order to do that,
even though I think this program still divides
everything up between controlled and uncontrolled,
meaning like filing cabinets would be uncontrolled,
magazines for weapons would be controlled.
I think it's still divided up that way. I'm not sure, but
I think if you wanted real change, you'd probably have to scrap this whole
program and build it again from the bottom up.
I think that's probably what needs to happen.
Your professor's right. It's not as simple as 1033 is bad.
It just gets used in a bad way more than it gets used in a good way.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}