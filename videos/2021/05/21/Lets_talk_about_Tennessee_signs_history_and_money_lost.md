---
title: Let's talk about Tennessee, signs, history, and money lost....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=87cQct5J5Ak) |
| Published | 2021/05/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tennessee plans to require signs for restroom identity policies.
- Beau questions the necessity and impact of these signs.
- Draws a historical comparison to discriminatory signs in the South.
- Beau argues against moral approaches to bigots.
- Mentions the economic impact of military installations like Fort Campbell, Kentucky.
- Installation commanders have power over off-limits areas based on command.
- Beau suggests economic repercussions for businesses not displaying the required signs.
- Stresses the financial importance of being accepting and inclusive.
- Predicts repercussions for establishments without the signs, affecting tourism and business.
- Urges voters in Tennessee to reconsider supporting short-sighted leaders.
- Warns about the consequences for small businesses due to political decisions.
- Encourages reflection on the implications of discriminatory practices.

### Quotes

- "It is more profitable to be a good person."
- "You may not care about morality, but I bet you care about that money."
- "A whole lot of that situation has to do with their faulty leadership."

### Oneliner

Tennessee plans restroom identity policy signs, sparking economic and moral debates, urging reconsideration of leadership's shortsightedness.

### Audience

Tennessee voters

### On-the-ground actions from transcript

- Vote thoughtfully in Tennessee elections (implied)
- Support businesses displaying inclusive signs (implied)

### Whats missing in summary

The full transcript provides additional details on the economic impact of discriminatory practices in Tennessee.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Tennessee and signs.
Signs that shouldn't exist but apparently they're going to and as long as they're going
to, we want to have as many of them up as we can.
So we're going to talk about those signs, we're going to talk about the historical comparison
that a lot of people want to make about those signs, then we're going to talk about the
argument that I want to make.
Okay so if you don't know what I'm talking about, the state of Tennessee in a quest to
find a made up solution to a made up problem has decided that if you're going to allow
people to use the restroom that corresponds to their identity, you are going to have to
hang up a sign that announces that policy.
Outside looking in, this seems like something that is just designed to create an atmosphere
of discrimination within the state.
Cast a certain group of people as less than as other and politicians capitalize on that,
score a few political points.
That's what it seems like.
Could be wrong but that's how it appears from outside.
The comparison that people want to make is to other signs that hung in the south.
These bathrooms, those are for those other people.
You need to go to the good bathrooms.
And I get it.
I understand the comparison.
I don't like it for a number of reasons.
Some are really nuanced but one is incredibly pragmatic.
That is a moral argument.
That is a moral argument.
And generally speaking, moral arguments do not work on bigots.
And that's who you're trying to reach here.
So I want to make a different argument.
I want to talk about military installations for a second.
Just outside of Tennessee sits Fort Campbell, Kentucky and even though it is outside of
Tennessee it generates 58,000 jobs and $10 billion in economic activity for the state
of Tennessee.
Those are pretty big numbers.
I would imagine that people there may not like this because in case you don't know,
trans soldiers, oh that's a thing.
The Department of Defense has joined this century.
Maybe you should too.
And I know if you're in Clarksville, the town outside of Campbell, you're probably thinking
well I mean not everybody at Campbell is going to be mad if I don't hang up one of those
signs if I'm not accepting.
They're not all going to care.
And I'll give you that.
They probably wouldn't care on their own.
But see you're operating under the assumption that they will have a choice in the matter.
See, installation commanders, they got this wild power.
I want to read you a passage.
The establishment of off-limits areas is a function of command.
It may be used by commanders to help maintain good discipline and an appropriate level of
good health, morale, safety, morals, and it goes on.
The morals of the US Army dictate at this point that trans rights, well those are human
rights because they are people after all, and I would imagine that if a group of soldiers
went out and one of them was discriminated against right outside the gate, was treated
differently, that might cause a morale issue.
I'd be really surprised if commanders did not issue off-limits lists for establishments
that don't have these signs up.
See you want to be accepting of their money.
So you better be accepting of all of them.
And it's worth noting that there are other installations in the state of Tennessee.
I just went with Campbell because I happen to know the numbers for it off the top of
my head, didn't have to do any research.
It's wild that I know that this is at stake, but apparently these politicians didn't, or
they don't care.
Because all they care about is scoring a few points, treating people as less than.
Billions of dollars at stake, and they're going to pursue a made-up problem.
And see we're just talking, when we say billions of dollars, we're just talking about the military
at this point.
I go up to Tennessee pretty often.
Well, I did before the world stopped.
I can assure you that I will not go into any establishment that doesn't have one of those
signs up.
Not one.
I would imagine that those theme parks, yeah, they're going to need those signs too because
schools from out of state, they're not going there.
You may not care about morality, but I bet you care about that money.
And that's what it really boils down to.
It's sad, I often say that it is cheaper to be a good person.
In this case, it is more profitable to be a good person.
I don't know that this is the image that Tennessee wants to cast to the rest of the world, but
here we are.
If you are a voter in Tennessee, you might want to really think about whether or not
you want to be led by the people who were this short-sighted.
Because if you're in Montgomery County, if you're in Clarksville, this is going to matter
to you.
You're going to have to make some choices.
The big companies, they've already run the numbers.
They're going to have those signs up.
It's going to be those little companies being pressured by their friends.
It'll be those mom and pop places that go out of business because a bunch of people
in Nashville had no clue what they were doing.
All in an attempt to create yet another group of people to look down on.
So you can feel better about the situation that you're in.
A whole lot of that situation has to do with their faulty leadership.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}