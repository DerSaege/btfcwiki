---
title: Let's talk about DOD and the CIA's new recruitment practices....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=d-OzGU3dTG4) |
| Published | 2021/05/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responding to a comment on Ted Cruz's Twitter feed about recruitment practices in government entities and modern art.
- Criticizes the idea that the Department of Defense (DOD) and CIA are becoming "woke" through their recruitment practices.
- Points out that the dictionary definition of "woke" is being alert to injustice, particularly racism.
- Mentions that the DOD has a history of recruiting people from marginalized groups, contributing to social change.
- Explains how the special forces and historical CIA operations have involved cultural exchange and appreciation.
- Talks about the CIA's recruitment strategies, including using abstract art to recruit intellectuals.
- Argues that both the DOD and CIA have always recruited individuals who understand ethnic divisions and class issues.
- Emphasizes that progress and intellectual acceptance do not lead to the downfall of a country.
- Concludes that the DOD and CIA are not actually "woke" but rather use the concept to attract recruits.
- Urges viewers to recognize the historical context of recruitment practices in these agencies.

### Quotes

- "They recruit people that you view as lesser."
- "Neither the DOD or the CIA is actually woke."
- "Progress and intellectual acceptance do not lead to the downfall of a country."

### Oneliner

Responding to misconceptions about government recruitment practices, Beau dismantles the idea that the Department of Defense and CIA are becoming "woke," explaining their historical recruitment strategies and the concept's misinterpretation.

### Audience

Social commentators, activists

### On-the-ground actions from transcript

- Research and analyze historical recruitment practices of government entities to understand their context and implications (suggested).
- Educate others on the historical roles of the Department of Defense and CIA in recruitment (suggested).
- Challenge misconceptions about government recruitment practices by sharing accurate information with others (suggested).

### Whats missing in summary

The full transcript provides a detailed analysis of the historical recruitment practices of the Department of Defense and CIA, debunking the misconception that these agencies are becoming "woke."

### Tags

#RecruitmentPractices #GovernmentEntities #Misconceptions #HistoricalContext #SocialChange


## Transcript
well howdy there internet people it's Beau again so today we are going to talk about certain
government entities recruitment practices and modern art and a John Wayne movie and a whole
bunch of stuff because I got a message and I want to respond to it because it's well
after running across your comment on Ted Cruz's Twitter feed you know it's going to be good when
it starts with that I watched several of your videos I find it hilariously interesting that
while you mocked him for falling for propaganda you did not refute his main point none of your
videos address the newfound and sudden wokeness of the Department of Defense or the CIA none of
your videos address how intellectuals being recruited and woke mobs being recruited into
these agencies will lead to the immediate downfall of the United States okay first I'm
pretty sure things aren't that serious second I'm fairly certain we have very different definitions
of the word woke the the dictionary definition is just alert to injustice particularly racism
that's what woke means I personally do not find much of what DOD or the CIA does as woke
however that's not what you're talking about you're you're talking about the recruitment
of intellectuals and you know being vaguely aware of other cultures and recruiting people
that you look down on let's just cut to the chase here not to put too fine a point on it
that's what we're talking about they recruit people that you view as lesser okay sure we
could talk about it DOD has been woke by your definition since the end of World War two since
the end of World War two DOD desegregated before the South gay soldiers were allowed to serve
before they were allowed to get married in many ways the Department of Defense has become a
vehicle for social change if you have a marginalized group within the United States that you want
seen as part of the American tapestry service in the military helps speed that process along
because in the United States nobody wants to talk bad about a soldier what you are describing
as new is literally from the 40s DOD has done this for a long time they have recruited people
that you don't like for a long time I would point out that the special forces the army's
elite their entire job is to be woke by the way you would look at it you probably haven't read
anything but you might remember the the John Wayne movie Green Berets and even way back then you had
all those soldiers of different colors at the beginning talking about all the different languages
they speak and historically they will go and live with the locals and engage in cultural exchange
and cultural appreciation rather than appropriation because they're trying to win hearts and minds
you know you don't want to offend your allies they are quite literally trained to be woke by
the way you would look at it this isn't new at all as far as the CIA goes when it was founded
in 1947 it had something called the propaganda asset inventory was the name of it as the name
suggests it ran propaganda operations and it had a distant camouflaged relationship with the Museum
of Modern Art was set up through a bunch of phony companies and foundations mainly because the
artists involved with the art that they wanted well they were more my kind of woke and they
wouldn't have had anything to do with this had they known but they actually set up fake foundations
so they could take abstract art and put it on exhibition all over Europe to basically show
the Soviets that we were woke so we could recruit their intellectuals so we could cause dissent
within their artists communities and basically cause cultural upheaval yeah the CIA's recruited
people like that since literally day one even really before then the first ads recruitment
slogans for the OSS the predecessor to the CIA was that they wanted PhDs who could win a bar
fight the CIA also ran literature into Warsaw Pact countries that was banned in again again
in hopes of encouraging intellectuals to defect because contrary to what you apparently believe
intellectuals do not cause the downfall of a country as far as the dictionary definition of
woke alert to injustice particularly racism that's like a big chunk of the CIA's mission
that's their job when they you know go around and like stage coups and stuff which is not woke to
be clear they tend to exploit ethnic divisions and class issues stuff like that stuff that you
would definitely consider as woke topics because they need to understand that stuff they have
always recruited since the 40s people who understand it that that's it's literally their
job when you're talking about the definition and the context you are using it in the CIA has
always been woke AF okay progress intellectuals accepting other people historically these things
don't actually lead to the downfall of a country rejecting progress staying very closed off failing
to recognize reality a bad understanding of history these things they have led to the downfall of
nations just throwing that out there again I understand the context in which you're using it
but just to be clear neither the DOD or the CIA is actually woke that's just them co-opting language
to to appeal to the people they're trying to recruit as they've done forever and if that
wouldn't work they would literally set up foundations bogus charity groups and stuff like
that to recruit intellectuals because it doesn't cause the downfall of a country anyway it's just
a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}