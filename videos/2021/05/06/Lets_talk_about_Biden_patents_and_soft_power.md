---
title: Let's talk about Biden, patents, and soft power....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cTE8ldxIXE0) |
| Published | 2021/05/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the Biden administration's decision to support ending vaccine patents as an exercise in soft power rather than altruism.
- Differentiates between soft power (attracting countries to you) and hard power (going after them) in foreign policy.
- Advocates for the U.S. to be the world's EMT (emergency medical technician) rather than the world's policeman to illustrate soft power over hard power.
- Mentions the U.S. countering China and Russia's vaccine diplomacy through soft power.
- Describes how China and Russia have been successful in using soft power to gain influence internationally.
- Suggests that the U.S. is playing catch up with China and Russia in exercising influence through vaccine diplomacy.
- Points out the potential benefits of a global vaccine race triggered by countries trying to exert influence.
- Emphasizes that the U.S.'s move to support ending vaccine patents is about obtaining influence overseas, which coincidentally benefits everyone involved.
- Compares the immediate results of hard power with the longer-lasting benefits of soft power in foreign policy.
- Raises the possibility that the U.S.'s actions may be a response to counter Russian and Chinese soft power rather than a purely altruistic gesture.

### Quotes

- "We should be the world's EMT rather than the world's policeman."
- "This is about obtaining influence overseas."
- "Just don't let it be framed as we're out there being the world's savior."
- "All of this is good."
- "Y'all have a good day."

### Oneliner

Beau explains how the U.S.'s support to end vaccine patents is about obtaining global influence through soft power, not altruism, in response to China and Russia's actions.

### Audience

Foreign policy enthusiasts

### On-the-ground actions from transcript

- Support efforts to provide necessary supplies for vaccine production in other countries (implied)
- Advocate for equitable distribution of vaccines globally (implied)
- Stay informed about international relations and soft power dynamics (implied)

### Whats missing in summary

Analysis of how the U.S.'s shift towards exercising more soft power in foreign policy could impact global dynamics and relationships.

### Tags

#ForeignPolicy #SoftPower #VaccineDiplomacy #GlobalInfluence #USPolicy #China #Russia


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about patents
and soft power and hard power and foreign policy
because some news broke.
And as soon as the news broke, everybody's like, wow, that's really humanitarian.
That's a good move. It's amazing.
We have talked about it before when something is happening on the international stage
if it relates to foreign policy.
Even if it's a good thing, it may not necessarily be altruistic.
And that's what's happening here.
The Biden administration's decision to support ending the patents on our vaccines,
that is not a case of we have the means, therefore we have the responsibility.
That's something else.
It's an exercise in soft power.
Soft power as opposed to hard power.
Soft power is getting other countries to come to you.
They want your involvement and it buys you influence.
Hard power is going after them.
And the United States has used a mix for a very long time,
but we heavily rely on hard power.
You've probably heard me say we should be the world's EMT rather than the world's policeman.
That is a visualization of the difference between soft power and hard power.
Think about it. If you find out an EMT is coming to pick you up and throw you in their vehicle,
you may not be happy about the situation you're in, but you're pretty glad they're coming.
If the police is coming, going to throw you in their vehicle, you're probably not happy about it.
That same feeling happens on the international stage as well.
I'm a supporter of the idea of being the world's EMT because there is,
there aren't a lot of songs criticizing EMTs the way there are police.
And that's the international feeling on it as well.
So why are we really doing this?
It's not like the United States to give a profit what's happening.
We are countering near peers. China and Russia, they're already doing this.
Recently, Russia sold a bunch of vaccines to Peru.
It wasn't long after that they were in talks about a new lithium mine.
China gave a bunch of vaccines to Algeria.
Algeria has promised to support China's core interests
and oppose people meddling in their internal affairs,
something China is going to need international support on pretty soon.
This is soft power at work. Now, the reality is that China, they're really good at this.
They are really good at this and they've been doing it a while.
The United States is going to be playing catch up, not just with vaccines,
but overall. That is one of the good things that will come from the current situation
with China, Russia, and the United States and the reality that none of them
really want to go to war with each other.
They're going to have to find other ways to exercise their influence
and this is going to be one of them.
This is a good move for U.S. foreign policy to start taking small steps
towards exercising a lot of soft power.
It just so happens this is also really good for humanity,
especially if there could be some kind of vaccine race that gets triggered by this,
where all of these countries are trying to exert influence by supplying the most vaccines.
That would be a really good thing for everybody because a lot of these countries,
it would be at least a year before they get them without this.
So let's hope that happens.
This isn't the grand humanitarian gesture that it's probably going to be painted as
here in the United States.
This is about obtaining influence overseas.
It just so happens in this case it's good for everybody involved.
That's normally not the case, but this is a good illustration of it
because you can see how it plays out and how other countries are already doing it
and getting rewards from it.
Generally speaking, hard power, military force, coercion, they obtain more immediate results.
Soft power, stuff like this, it typically takes longer to get any return on the investment,
but the benefits are a lot longer lasting.
If this is the beginning of a shift, it's a really good thing.
I don't know that it is.
This may just be an attempt to counter Russian and Chinese soft power.
Again, that's not to say what's happening is bad.
It is a good thing.
We definitely should be helping other countries get the supplies they need to make vaccines,
to get them vaccines, to make sure they have the research to produce the vaccines.
All of this is good.
Just don't let it be framed as we're out there being the world's savior.
We're hoping to get something in return the same way China and Russia have.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}