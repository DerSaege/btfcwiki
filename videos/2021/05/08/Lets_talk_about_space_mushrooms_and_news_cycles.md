---
title: Let's talk about space mushrooms and news cycles....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JXjP6W_Pydg) |
| Published | 2021/05/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Encountered an article claiming mushrooms on Mars, sparking interest.
- Rover photographs mushrooms, but subsequent article debunks claim as rocks.
- Mention of a scientist with dubious reputation fuels skepticism.
- Media cycle of groundbreaking study followed by debunking confuses the public.
- Disagreements among scientists can erode public trust in experts.
- Peer-review process is vital for scientific credibility and consensus.
- Emphasizes the importance of verifying new information before acceptance.
- Encourages reliance on experts for accurate evaluation of studies.
- Warns against believing unverified information without expert validation.
- Advocates for following up on studies to avoid falling for misleading narratives.

### Quotes

- "Why should we listen to the experts? They can't agree."
- "Those disagreements, they don't show that the experts don't know what they're talking about. They show that the system works."
- "Make sure you don't fully commit to believing [new information] until other people have reviewed it, people that have expertise in that area."
- "Otherwise, space mushrooms."
- "It's just a thought."

### Oneliner

Beau cautions against blindly accepting sensational news, stressing the importance of expert validation to combat misinformation like space mushrooms.

### Audience

Science enthusiasts

### On-the-ground actions from transcript

- Verify scientific claims before sharing (suggested)
- Seek expert opinions on new studies (suggested)

### Whats missing in summary

The full transcript provides a detailed reflection on the impact of sensational news and the importance of scientific scrutiny in preventing the spread of misinformation.


## Transcript
Well, howdy there, Internet of People.
It's Beau again.
So today, we are going to talk about mushrooms on Mars
and what we can learn from mushrooms on Mars.
This whole little saga I'm about to detail
took place over the last couple days.
It started when I saw this article, Life Found on Mars.
Mushrooms.
What?
Cool.
And I go to look.
And sure enough, it details this whole idea
that the rover photographed these mushrooms.
And you look at the images, and it
appears as though the rover drove over the mushrooms.
And you can actually see them start to regrow.
You see these little white balls.
I mean, to me, yeah, sure, they look like space mushrooms,
whatever.
And it makes sense.
And I'm like, OK, cool.
That's interesting.
In the article, I see one name that I vaguely recognize,
but can't place where.
Sure enough, OK, cool.
Next day rolls around.
There are no mushrooms on Mars.
Headline.
Man.
I go and read it.
And it basically says, no, what you're seeing,
those little white balls, those are rocks.
And when the rover drove over them,
it pushed them down into the loose soil.
And then the soil fell off, and they appeared to be growing.
Yeah, I mean, that kind of does make a little bit more sense.
Seems more likely than space mushrooms.
And then the name that I recognized,
I remembered why, because the article pointed it out.
I once read an entire article that was, well,
let's just say pointing out that this person is not necessarily
well-respected within the scientific community.
So what happened?
It's a slow news week.
New study came out, seemed kind of groundbreaking,
and it got headlines.
And then they covered the fact that it got debunked.
And that's fine.
That's all right.
But what happens when news starts
to break in between that first study and it being debunked?
Well, that information goes out.
They don't follow up on it.
And large segments of the population
continue to believe in space mushrooms.
So what happens?
These little disagreements between scientists,
these also add fuel to the idea that, well,
science doesn't know what they're talking about anyway.
Why should we listen to the experts?
They can't agree.
I mean, a consensus developed pretty quickly
that, no, those weren't mushrooms.
Those were rocks.
And they backed it up pretty well.
That process is peer review, information and ideas put out.
The person's peers, although I'm not
sure that those who reviewed it would
like to be called this person's peers,
judging by some of the comments I saw about this individual,
they look at it and they say no or yes or needs more study,
whatever.
These disagreements, they don't show
that the experts don't know what they're talking about.
They show that the system works.
They show that when new information comes out,
it gets reviewed.
And a consensus develops.
People from different fields look at it and debunk it
or lend their credibility to it.
That's how it's supposed to work.
And that's how it did in this case.
And it all got covered, which was cool.
So the key takeaway from this is that when
you hear about a new study, particularly if you don't
normally read studies and you just
happen to stumble across an article,
make sure you follow up on it.
If it's new information, make sure you
don't fully commit to believing it
until other people have reviewed it, people that
have expertise in that area.
Otherwise, space mushrooms.
Anyway, it's just a thought.
I have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}