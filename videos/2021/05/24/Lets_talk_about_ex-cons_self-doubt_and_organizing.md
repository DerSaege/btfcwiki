---
title: Let's talk about ex-cons, self-doubt, and organizing....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kM010qUUl5M) |
| Published | 2021/05/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Receives a message narrating someone's life story, leading up to seeking encouragement.
- Describes the backstory of a kid growing up in a neighborhood and getting involved in an organization.
- The individual goes to prison but uses the time to read and grow intellectually.
- After release, he aims to make his neighborhood better despite facing challenges.
- Expresses self-doubt due to past actions and worries about being judged.
- Mentions how rich white college kids come to the neighborhood for short-term fixes.
- Encourages the individual not to be deterred by past mistakes or external judgments.
- Emphasizes the unique skills and knowledge the person gained from their past experiences.
- Urges the person to push through self-doubt as their contributions are valuable.
- Stresses the importance of persevering despite uncomfortable moments for the greater good.

### Quotes

- "You're going to get that hate no matter what. That's not a reason. That's an excuse."
- "What you say is that you were in a gang. What I hear is that you know how supply routes work."
- "If you've already put in the groundwork and now you're just having second thoughts about it, a little bit of self-doubt, you've got to push through that."
- "There may not be somebody else for a long time that gets that Goldilocks sentence, that has the skills and the connections and the understanding that right now you have."
- "It probably resonated with you for another reason. The hero in that video is not the dude that had gas money because he had a YouTube channel. The hero were the people that got their neighborhood supplied."

### Oneliner

Beau receives a life story message, encouraging perseverance despite past mistakes, urging the individual to utilize their unique skills for community betterment.

### Audience

Community members

### On-the-ground actions from transcript

- Continue efforts to improve the neighborhood despite challenges (exemplified)
- Push through self-doubt and external judgments to make a positive impact (exemplified)
- Utilize unique skills and knowledge gained from past experiences for community organizing (exemplified)

### Whats missing in summary

Full understanding of the narrator's encouragement and motivation behind community betterment efforts.

### Tags

#CommunityBuilding #OvercomingChallenges #UtilizingSkills #Encouragement #Perseverance


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a message I got.
A message, a novel I got.
Basically somebody's life story.
Their entire life story, from the time they were a kid to today.
And it all, it's all leading up to the idea of needing some kind of encouragement,
needing some kind of reason to press forward.
I'm not going to go through the whole thing, one because it is really long,
and two because there's a lot of details in it that would allow people to identify
that person.
But it's a back story.
You've seen a dozen movies.
Kid growing up in a neighborhood, some people in that neighborhood have money and respect,
so he joins at 14, 15, something like that.
As he gets older, he becomes more involved with this little organization.
And then when he is 19, he gets in a car to go look for somebody with a whole bunch of
other people, and they see him.
And I have no idea whether or not the person who sent the message pulled the trigger or
not, but they didn't get the right person.
He wound up going to prison, a pretty long time.
Described it as long enough to make him better, but not long enough to make him bitter.
Goldilocks sentence.
But he's out.
While he was in, he said that he read constantly.
Count of Monte Cristo, Tale of Two Cities, a whole bunch of stuff.
And then from there, read a bunch of other classics, read about philosophy, all kinds
of stuff.
Grew.
And he made the decision to make his neighborhood better when he got out.
And once he got out, he, like a lot of people, wasn't really out.
Still had a lot of controls on where he could go and what he could do.
So he spent a lot of time at home watching YouTube, learning about organizing his community.
And eventually he ran across a quote racist looking white dude, thank you, and a video
about a Tale of Two Cities and he watched it.
Said he couldn't find it again, but that it kind of spoke to him.
And he's been following the channel ever since.
But now he's run into an issue because he doesn't think people will let him live his
past down.
He provides this giant list of good things that I've done, half of which y'all really
did.
And he says he doesn't have any redeeming stuff like that.
And he talks about how his neighborhood gets used as an experiment for rich white college
kids to come in and try to fix things.
And they're there about a semester and then they leave.
And part of the video that he watched, and one of the things that motivated him, was
that he didn't like the idea that some white guy was playing hero to a black neighborhood.
I guess it paralleled what he saw.
He put forth the effort and he's got all the groundwork done.
But now he's getting that self-doubt because of his past.
I get it.
I get it on some level.
And you're right, there are going to be people who aren't going to let you live that down.
Fact.
Absolutely.
Those people, they're going to be like that no matter what though.
You're going to get that hate no matter what.
It doesn't matter if you're trying to make your neighborhood better or you're working
at McDonald's.
It's still going to be there.
That's not a reason.
That's an excuse.
And as far as the fear or the reluctance, the worry that those rich white kids aren't
going to network with you because of your past.
Yeah, that may be true.
But at the same time, it doesn't matter if they want you, they need you.
Ever think the reason that they're only there a semester is because they're afraid?
They come in to play savior and realize it's not a dangerous mind.
That it can get pretty dangerous pretty quickly.
They don't know the neighborhood.
You do.
Doesn't matter if they want you, they need you.
Aside from that, I think it has to be you.
And not for any ideological or self-help reason.
Nothing about personal growth or turning over a new leaf or any of that stuff.
That's all fine and good, but those are those moral arguments.
The pragmatic one.
They need you.
What you say is that you were in a gang.
What I hear is that you know how supply routes work.
You understand distribution networks.
You understand the personality conflicts that come with an organization.
You understand that if you have a whole bunch of people working towards a goal like that,
they still got to eat.
So there's finances behind it.
You understand the power of a group of people that are tightly knit and working towards
the same goal.
Different products, different cause, but all of those skills are completely transferable.
Aside from that, you know the most important thing, the neighborhood.
You're worried that somebody's going to give you some static and you still have a little
bit too much of the streets in you to be able to just let it go.
And yeah, I get it.
I can relate.
But at the same time, that's a benefit and a curse.
Because yeah, you're going to have to control it, but there's going to be a whole lot of
people that know you still have streets in you.
That video that you referenced, by the way, the reason you can't find it is because it
isn't a tale of two cities.
It's a tale of two neighborhoods.
During that same time period, me and my little group, we were going and cutting trees that
had fallen across roads in poor neighborhoods.
Because they were poor neighborhoods, of course they were last on the list to get cleared.
Any time after a hurricane, chainsaws are in high demand.
They sell out pretty quickly.
But because of some of the people that are in my little circle, even in the most crime-ridden
neighborhoods that we were in, nobody was taking one of those chainsaws.
Yeah, you got a past.
You want to make your neighborhood better so other people don't get that past.
Do it.
Don't let what other people say stand in your way, so you're going to get hate for it.
And if you're right, the people you're worried about, they're going to be gone in a semester
anyway.
It's going to be a new crop.
It's got to be.
If you've already put in the groundwork and now you're just having second thoughts about
it, a little bit of self-doubt, you've got to push through that.
Because there may not be somebody else for a long time that gets that Goldilocks sentence,
that has the skills and the connections and the understanding that right now you have.
Yeah, I understand.
There are definitely going to be some uncomfortable moments.
It's probably worth it.
It is probably worth it.
There is, beyond the pep talk, there's one thing I want to point out.
That video, A Tale of Two Neighborhoods, not cities.
Go back and watch it.
It probably resonated with you for another reason.
The hero in that video is not the dude that had gas money because he had a YouTube channel.
The hero were the people that got their neighborhood supplied.
They were wearing colors and carrying guns.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}