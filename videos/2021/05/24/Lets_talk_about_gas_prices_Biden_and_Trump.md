---
title: Let's talk about gas prices, Biden, and Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=H6-nS69W1wU) |
| Published | 2021/05/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Gas prices began rising before Biden took office.
- Summer brings higher-quality gas and increased travel, raising demand and costs.
- The pandemic led to decreased travel last year, creating a pent-up demand for travel now.
- Stimulus checks and tax credits have increased disposable income, leading to more travel and higher costs.
- Crude oil prices dropped significantly last year and are now rebounding, contributing to increased costs.
- Several U.S. refineries closed under the previous administration, reducing production and increasing costs.
- The recent pipeline hack caused short-term disruptions but is not a long-lasting factor.
- Gas prices are rising in the UK and Canada as well, indicating a global trend.
- Biden's stance on fossil fuels may contribute to market instability but not solely to blame for gas price increases.
- Understanding basic economics, particularly supply and demand, is key to grasping the situation.
- Blaming Biden for gas prices without proper understanding is unjustified.

### Quotes

- "You can't blame this on Biden, not if you want to do it, honestly."
- "Because if your politician does not understand supply and demand, they probably shouldn't be your representative."

### Oneliner

Gas prices rose before Biden, summer travel demand, stimulus checks, global trend - Biden not solely to blame.

### Audience

Consumers

### On-the-ground actions from transcript

- Monitor gas prices and try to carpool or use public transportation when possible (implied).
- Educate others on the factors affecting gas prices and discourage unjustified blame (implied).

### Whats missing in summary

The full transcript provides a comprehensive breakdown of the factors influencing gas prices and challenges the narrative of solely blaming Biden for the increase.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about gas prices and who's to blame for them.
Because on a lot of news shows, it's being cast as though it is Biden's fault.
So what we're going to do is we're going to run through the reasons this is
happening and we're going to see if we can track any of them back to Biden.
Um, and spoiler, some you might be able to.
Okay.
The first thing to note is that the upward trend in gas prices started
last year before Biden took office.
So it's important to note just for framing sake that it started before he took
office, that alone should tell you it's probably not all his fault, but let's
see if there are any things that we can track back down.
The primary reason that I think is that it's summertime.
We're moving into summer and all of that.
If you don't know, the gas you put in your car in winter
is different from the gas in summer.
The gas in summer is of higher quality to reduce emissions.
That's just a normal thing that happens all the time,
every year.
It being summer also means that there is more projected travel because people like to travel
in the summer.
Supply and demand.
There is more demand so the cost will go up.
Number two, there was this, I don't know if you all heard about it, but there was like
this public health thing last year and a whole lot of people stayed home.
Not everybody and that's why it lasted so long, but a whole lot of people stayed home
And because of that, they are projecting more travel because they haven't been anywhere
in a year, which would increase demand, which would increase costs.
Then the third one is that people have money.
People have money.
This one, you can track back to Biden.
You can.
stimulus checks, the tax credits for kids that are going to be coming in soon.
All of this means that people that normally don't have disposable income might, which
would increase travel, increase demand, increase costs.
Last year, crude prices dropped like a rock.
And then they started to rebound, which increased costs.
It's also worth noting that by the end of last year, because of everything going on,
again, under Trump, a whole lot of refineries within the United States closed.
A lot, more than 10.
cut U.S. production by a billion barrels a day. Less supply, more demand, higher cost.
Then you do have the pipeline thing, the hack, and then they shut down the pipeline. Yeah,
that has something to do with it short-term, but that's not going to be a long-lasting
thing. And then there is one key piece of information that might help settle the
debate over whether or not it has to do with Biden. Gas prices in the UK and in
Canada are also going up. Now I know that Trump was at least toying with the idea
of purchasing other countries and territories and stuff. I don't think any
of those deals went through, so I'm fairly certain that Biden doesn't have
any control over any of that.
This is basic supply and demand.
This is basic economics.
That's what's at the heart of this.
Now you could make the argument that because Biden is seen, at least, as being tougher
on fossil fuels, that there's some market instability because of that.
I don't know that that has anything to do with it, but at least it's a reasonable argument.
But at the end of the day, no, you can't blame this on Biden, not if you want to do it, honestly.
And if your favorite talking head or politician is doing that and they can't explain how it's
his fault, you probably want a new talking head or politician.
Because if your politician does not understand supply and demand, they probably shouldn't
be your representative.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}