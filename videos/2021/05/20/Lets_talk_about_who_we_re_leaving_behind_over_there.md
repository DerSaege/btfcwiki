---
title: Let's talk about who we're leaving behind over there....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=LgN89_u2cpM) |
| Published | 2021/05/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questioning why the U.S. is leaving their allies behind, particularly the interpreters and Afghan people who assisted U.S. forces.
- Traditionally, the U.S. military has left allies behind in conflicts, but Congress is urging Biden to speed up the process of helping these individuals.
- Congress could pass legislation to airlift the 18,000 applicants and their families to the U.S. for processing, which seems more efficient than the current process in Afghanistan.
- Beau believes that Congressman Waltz from Florida, an ex-Green Beret, genuinely cares about the situation due to his personal experience working with these individuals.
- Beau expresses a cynical view that leaving these allies behind could serve as a motivation for them to join the opposition, knowing the consequences if the national government falls.
- He points out the pattern of the U.S. leaving allies behind worldwide, making it difficult to recruit local help.
- Despite the risks taken by these allies for U.S. interests, there is a delay in fulfilling promises made to them, with a wait time of possibly a couple of years.

### Quotes

- "Leave no man behind, right?"
- "We're leaving, we're heading out, we're not helping them but we know they could help the national government."
- "And now, when it's time to make good on the promises that we made, well, we'll get around to it."
- "Doesn't mean it's not true."
- "That's honestly what it seems like."

### Oneliner

Beau questions why the U.S. is leaving allies behind, urging for quicker action to fulfill promises made to those who risked their lives for U.S. interests.

### Audience

Advocates for allies

### On-the-ground actions from transcript

- Push Congress to pass legislation authorizing an airlift of the Afghan allies and their families to the U.S. for processing (implied).
- Advocate for expedited processes to fulfill promises made to allies who assisted U.S. forces (implied).
- Support organizations working to assist Afghan allies in relocating to safety (implied).

### Whats missing in summary

The emotional impact and urgency conveyed by Beau's words is best experienced by watching the full transcript.

### Tags

#US #Afghanistan #Allies #Congress #Promises #Support


## Transcript
Well, howdy there, Internet of People. It's Beau again.
So today, we're going to talk about a question that I got a few days ago,
but I wanted to wait until I got that
over-the-horizon video out
because I think
the answer to the question
may possibly
be related to that
in some cynical way.
I believe there may be a connection.
The question, pretty simply,
is why is the U.S. leaving
their allies behind?
Why aren't we helping
the interpreters and all of the people in Afghanistan
who helped U.S. forces?
Now, the obvious easy answer there is tradition
because it's what the U.S. military has done for a very long time
in a lot of different conflicts.
But again,
there are a lot of people in Congress
who are wringing their hands over this.
Most of them are looking to Biden saying, hey, speed up the process.
The process requires people in Afghanistan
to travel across the country with all the documentation they need
to get a U.S. visa
and get to the embassy.
Documentation that if they're caught with it
at a checkpoint,
well, I promise you they will never make that appointment.
That's not a...
that's not a process that is workable.
But people in Congress,
they did send a strongly worded letter to the White House asking them to do
something as if they forgot
their Congress.
They could totally pass legislation authorizing, I don't know,
an airlift of the 18,000 applicants and their families
and bring them to the U.S. and process them here. It seems like it would be a whole lot
easier to process them if they were all in one spot here
than trying to do it over there.
There are a lot of people talking about this and to be honest the only one that
I actually believe
really cares
is a guy named Waltz
from Florida,
a Republican.
Now he's an ex-Green Beret
so he worked with them,
like personally worked with them.
And I am certain that he is aware
of what is going to happen to them
if the opposition there goes on the offensive.
And see there's the thing,
there's a very cynical part of me
that looks at what's happening,
looks at the ease
at which it could be corrected
and acknowledges that it's not being corrected.
And the only conclusion I can come up with
is that those 18,000 applicants,
that's 18,000 people that know what they're doing,
that know what they're doing.
And I cannot think of a better motivation
to join the side of the national government
and suit back up
than the knowledge
that if the national government falls,
you and your family are gone with it.
It seems cynical,
and it is.
Doesn't mean it's not true.
That's honestly what it seems like.
A backdoor draft.
We're leaving,
we're heading out,
we're not helping them
but we know they could help the national government.
That's what it seems like.
We have done this all over the world
and it always becomes surprising
that it's hard to recruit local help.
If we stop selling them out,
it might get a little easier.
At the end of the day,
these people, they fought alongside our forces.
They risked themselves and their families
for U.S. national interests,
for their own country.
And now, when it's time to make good
on the promises that we made,
well we'll get around to it.
I think the wait time is a couple years.
Leave no man behind, right?
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}