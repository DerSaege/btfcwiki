---
title: Let's talk about over the horizon in Afghanistan....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YfLYtOzCZIo) |
| Published | 2021/05/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The withdrawal of US forces from Afghanistan is happening, but no country has stepped up to provide a token security force.
- The US military plans to maintain over-the-horizon capabilities, primarily through airstrikes.
- US forces are looking for countries to host them near Afghanistan despite leaving the country.
- Most airstrikes in Afghanistan are currently carried out by the Afghan Air Force, maintained by US contractors who will likely leave.
- The Afghan military has changed in the last 20 years and may struggle to hold its own without US contractor support.
- General McKenzie hinted at the possibility of US forces going back into Afghanistan for special operations and drone strikes.
- The reliance on drones for surveillance and airstrikes raises concerns about civilian casualties and accuracy.
- The use of drones without human intelligence on the ground poses challenges.
- Beau suggests that without a token security force, there may be a return of some US special operations troops within a year.
- The withdrawal from Afghanistan appears more like a pause than an end, with uncertainties about the national government's ability to stand alone.

### Quotes

- "It's going to be worse than a US presence, as bad as that was."
- "The real issue here is that the US was real quick to go in without any clear idea on how to get out."
- "I don't think when people were talking about we need to leave Afghanistan, that this is what they were picturing."

### Oneliner

The US withdrawal from Afghanistan leaves uncertainties about security and the future of the Afghan military, relying heavily on drones for over-the-horizon capabilities.

### Audience

Foreign Policy Analysts

### On-the-ground actions from transcript

- Support organizations providing aid and assistance in Afghanistan (implied)
- Stay informed on developments in Afghanistan and advocate for responsible foreign policy decisions (implied)

### Whats missing in summary

Analysis of potential humanitarian impacts and civilian safety concerns in Afghanistan due to increased reliance on drone strikes.

### Tags

#Afghanistan #USForces #OverTheHorizon #Drones #ForeignPolicy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about what's over the horizon
and what that term means and how it is likely to play out.
OK, so the withdrawal is happening.
US forces are leaving.
However, as of yet, there has been
no country willing to step up and say,
we'll provide the token security force.
There is no regional coalition that has stepped up
to say that either.
So while US troops are leaving Afghanistan,
the US military is developing plans
to maintain over-the-horizon capabilities.
What does that mean?
It means short version airstrikes.
It's really what it boils down to.
It's going to be the bulk of it.
There's a number of ways they can achieve this.
One is from carriers.
The issue with that is that the planes
would have to fly over either Iran or Pakistan.
One of those is not going to happen,
and the other one is really, really unlikely.
So the US military is currently shopping
for countries that would be willing to host US forces just
outside of Afghanistan.
So they're leaving Afghanistan, but they're not really
coming home.
Now, as it stands currently, roughly 90%
of airstrikes in Afghanistan are being conducted
by the Afghan Air Force.
However, those aircraft are maintained
by a giant contingent of US contractors, contractors
who will, probably all of them, at least
the overwhelming majority, will be leaving with US forces.
Whether the national government can keep those aircraft up
once those contractors are gone, well, that's anybody's guess.
The current Afghan military is not the military of 20 years
ago.
It does have a chance of holding its own.
Most estimates suggest that it won't.
There is a high likelihood that the opposition there
will go on the offensive.
Now, General McKenzie was talking about this,
and first I would like to point out that we're not even out yet
and he has already used the term go back in.
But more importantly, he used the terminology
of find, fix, and finish.
So primarily, this is going to be drones
and special operations.
That's how it's going to be maintained.
So while there won't be a large US presence, at times,
there might be US forces inside the country.
Using drones is not good.
They'll use surveillance for find and fix,
and then the drones that everybody's familiar with
to finish.
They'll be heavily relying on the surveillance drones
because there won't be human intelligence from Afghanistan
because there aren't any assets there.
The drones do not have a good record.
They do not have a good record of being surgical.
They don't have a good history of only hitting
what they're supposed to.
At the end of the day, this is probably
going to be worse than a US presence, as bad as that was.
Short of another country or a regional coalition
stepping up to provide that token security force,
I would imagine that in less than a year,
we will have at least some special operations troops back
on the ground.
This withdrawal, it's more of a pause than an end,
at least by the signs that we're seeing right now.
Yeah, there's not a whole lot of good news in this, to be honest.
Now, there is the chance that the national government
can hold its own and not need US assistance
beyond intelligence and logistics.
That is possible.
I don't think it's likely, but it is possible.
At the end of the day, the real issue here
is that, like many other problems
that we've had in the past, the US was real quick to go in
without any clear idea on how to get out.
And there is going to be a strong reluctance
within the military to just abandon
the national government.
So politically, that pressure will be pretty palpable.
They're not going to want to do it.
And nobody in the foreseeable future
is going to want Afghanistan to fall on their watch.
So they're going to do whatever they can to assist them.
I don't think when people were talking about,
we need to leave Afghanistan, that this
is what they were picturing.
But in the absence of a token security force,
this is probably what's going to happen.
So anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}