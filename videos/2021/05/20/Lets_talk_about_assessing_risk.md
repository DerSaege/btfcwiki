---
title: Let's talk about assessing risk....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IRFUsf-mdwc) |
| Published | 2021/05/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of assessing risk and how it factors into official statements regarding the use of force.
- Challenges the notion that fear alone constitutes a threat by providing examples of intent, capability, and opportunity.
- Describes the traditional formula for determining a threat as intent plus capability, and how an additional criteria of opportunity has been added over time.
- Illustrates the concept with a scenario of someone having intent and capability but lacking the current opportunity to pose a threat.
- Introduces a newer terminology of ability jeopardy, which equates to intent and opportunity in assessing threats.
- Emphasizes the importance of evaluating whether a situation truly warrants the use of force based on morality rather than justifiable fear.
- Advocates for a shift towards considering the necessity of using force instead of relying on fear-based justifications.

### Quotes

- "Fear doesn't make a threat."
- "This is how you determine what a threat is."
- "Maybe we should start leaning back towards the morality side of it."
- "Y'all have a good day."

### Oneliner

Beau explains assessing risk, challenges fear as a threat indicator, and advocates for morality over fear-based justifications in using force.

### Audience

Decision-makers, law enforcement.

### On-the-ground actions from transcript

- Revisit risk assessment protocols (implied).
- Challenge fear-based justifications for use of force (implied).

### Whats missing in summary

In-depth examples and further exploration of the impact of morality in assessing threats.

### Tags

#RiskAssessment #UseOfForce #Morality #ThreatAssessment #LawEnforcement


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about risk
and assessing risk and how it factors into
official statements because we hear a lot
that some official had to do something
because there was a risk and had to mitigate it. They had to
engage in some kind of behavior to stop that risk. They had to
use force to stop that risk,
specifically that threat.
A lot of times what you hear is, well, I was in fear.
Fear doesn't make a threat.
You could be afraid of stuffed animals. A stuffed animal isn't a threat.
So how do you determine what a threat is?
There's formulas. The old-school way of doing it
was intent plus capability, well,
that equals a threat. Over time
another criteria got added. Intent
plus capability plus opportunity equals threat.
So let's say there's somebody who doesn't like you
and intends to do you harm.
They certainly have the intent, right?
And capability, they're a boxer.
Well, they probably can. They have the capability.
But they're currently locked in a cell. They don't have the opportunity.
So they're not a threat. That's how it works. Now there's a newer way,
newer terminology anyway, that has come out which is ability
jeopardy, which is the same thing as intent, and opportunity.
It works the same way. This is how you determine what a threat is.
So when you just hear that, well, they were a threat,
I was in fear, run through that little cycle
that they have intent, capability,
and opportunity. Because if they don't,
well, they're not a threat.
Now as far as I know, every advanced
military and law enforcement agency in the world
uses this or something just like it. It may be different terminology,
but this is how threats are assessed. This is how you determine whether or not
something is
a threat.
I'll give you another example. Let's say there's a
kid who has a BB gun,
and they are about to shoot it at you.
They have the intent to do harm,
and they kind of have the capability,
and they have the opportunity. But what if you're wearing a vest
and have goggles on, and you know the likelihood of you actually being harmed
by that BB gun
is, well, slim to none.
Is it really a threat?
This little formula, this is based on morality.
Legality differs all over the world
and in different contexts,
but as we've become a society that is so willing to use violence
for pretty much anything, maybe we should start leaning
back towards the morality side of it and determining whether or not it was
actually necessary to use force,
rather than just something that could be justified out of fear.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}