---
title: Let's talk about mothers and minimum wages....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uafRZDRvgcM) |
| Published | 2021/05/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- This video delves into the connection between raising the minimum wage to $15 an hour and the impact of moms on consumerism in the United States.
- Major companies rely on consumerism to thrive and have often opposed raising the minimum wage.
- The fertility rate in the U.S. is below the replacement level, potentially impacting the consumer base needed for these companies.
- Surveys indicate that moms desire to have more children than they actually do, with financial considerations being a significant barrier.
- To sustain growth and beat previous financial records, companies may need to increase wages, ultimately leading to a potential rise in the minimum wage.
- The focus on economic considerations suggests that the bottom line of large companies will be the driving force behind changes in the minimum wage.
- President Biden's actions in support of a $15 minimum wage can contribute positively, but the economic bottom line remains the key factor.
- Failure to address the gap between desired and actual children could result in a decrease in consumers, impacting companies' profitability.
- Ultimately, the growth of companies hinges on a growing population and economic factors.
- Beau concludes with a thought-provoking message and wishes everyone a Happy Mother's Day.

### Quotes

- "Moms may be the reason it changes."
- "It's always the bottom line that matters."
- "If companies want to continue to grow, they can't have a shrinking population."

### Oneliner

The impact of moms on consumerism may drive a potential rise in the minimum wage, as companies rely on a growing population for sustainability.

### Audience

Policy makers, activists, economists

### On-the-ground actions from transcript

- Engage in advocacy for raising the minimum wage (implied)
- Support policies that benefit working families (implied)

### Whats missing in summary

The full video provides a deeper exploration of the implications of consumerism on wage policies and the economic considerations tied to population growth.

### Tags

#MinimumWage #Consumerism #PopulationGrowth #EconomicImpact #PolicyChange


## Transcript
Well howdy there internet people, it's Beau again.
So today we've got a little bit of a Mother's Day special, in an odd way.
Today we're going to talk about the minimum wage and why the climate to get the minimum
wage raised to $15 an hour, why that may be changing, why that may become more likely,
because it's for a very odd reason, not one that you might really think of.
Moms, moms may be the reason it changes.
The United States thrives on consumerism.
These major companies that have often pushed back against raising the minimum wage, they
survive on consumerism.
What is the one thing consumerism has to have?
Consumers.
In order to maintain the current level, we would need a fertility rate of 2.1, that's
replacement level.
Some estimates put it as low as 1.6.
Now that's not going to do for these big companies.
They're not going to make it on that.
But see here's the weird part.
If you talk to moms and you ask them how many kids they'd want to have, and you figured
the fertility rate based off of those surveys, it would be 2.6.
We'd actually have a growing population.
So why do we have a gap between the number of children being had and the number of children
moms want to have?
Biggest reason, financial considerations.
It's one of those weird things where if these large companies want to continue to beat L.Y.,
make more money than they did last year, well they're going to have to spend more.
They're going to have to raise wages.
So in a really bizarre twist of fate, it may not be President Biden signing an executive
order to raise the minimum wage of federal contractors, or him saying that nobody who
works 40 hours a week should live below the poverty level, or getting behind a $15 minimum
wage.
That may not be what matters.
What matters is probably going to be the thing that always matters.
The bottom lines of these massive companies.
And if things don't change and change quick, they're going to start to see shrinkage, because
there will be less consumers to purchase their products and services.
Biden's statements and stances and symbolic actions, yeah, sure, it'll help.
Certainly won't hurt.
But at the end of the day, when you are talking about economic considerations, it's always
the bottom line that matters.
And if companies want to continue to grow, they can't have a shrinking population.
Anyway, it's just a thought.
Y'all have a good day, and Happy Mother's Day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}