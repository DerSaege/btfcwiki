---
title: Let's talk about the future of this channel....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PSeWmF5CHO4) |
| Published | 2021/05/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Channel started as a joke, turned into a force for good with impact in the world.
- Plans to set up community networks, film documentaries on the road were put on pause due to the world stopping.
- Equipment ready, thanks to Patreon, plans to restart community projects.
- Working on setting up a production space due to running behind schedule.
- Facing challenges of noise and interruptions while filming at home.
- Production space will allow for more consistent, higher quality videos.
- Plans to restart interviews, improve live streams, and create more skits and documentary content.
- Aim to impact things in the real world and meet viewers in person over the next year.
- Expresses gratitude to viewers for their support and involvement in channel's growth.
- Looking forward to stepping up and increasing the channel's impact.

### Quotes

- "This channel started as a joke on a whim a couple of years ago. But over time it has turned into a force for good."
- "I can't thank y'all enough for facilitating all of this."
- "I actually look forward to meeting a whole lot of y'all in person."
- "Whatever. Anytime it said, Beau did this, y'all did that."
- "We are hopefully going to be able to put out more consistent, higher quality videos."

### Oneliner

Beau shares plans to enhance the channel's impact, restart community projects, and improve video quality with a new production space.

### Audience

Content Creators, Viewers

### On-the-ground actions from transcript

- Support Beau's channel through Patreon to help fund community projects and endeavors (implied).

### Whats missing in summary

Details on how Beau's channel has grown and evolved since its inception as a joke to becoming a platform for positive impact and community engagement.

### Tags

#YouTube #CommunityProjects #ContentCreation #ProductionSpace #ChannelEvolution


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the future of this channel.
If you've been around for a while, you may find this really interesting.
If not, you'll at least get a heads up on what's coming.
This has been a wild ride.
This channel started as a joke on a whim a couple of years ago.
But over time it has turned into a force for good.
We do all sorts of good things through this channel.
We fund all sorts of projects and endeavors.
We make a material impact in the world.
And it's thanks to y'all.
And I can't thank y'all enough for that.
If you've been around a while, you know that last year we had plans.
We were going on the road and we were going to help set up community networks and help
people with their projects and raise funds out on the road, film documentaries along
the way.
A whole lot of stuff.
And then the world stopped and it didn't seem like a good idea for me to be traipsing
across the country spreading joy and whatever I picked up along the way to large groups
of people who didn't know each other.
So that got put on pause.
The world is opening back up and that is going to restart thanks to the people on Patreon.
We already have the equipment.
We've tested out the formats on the other channel and worked out all the bugs.
If you've been around for a while, you knew all that was coming.
We've also been working on something quietly.
And if you're watching this right now, it didn't go according to plan.
We're running behind schedule.
That's why this video is made.
If you have noticed a slowdown in video output or I missed a day or something like that,
you're about to find out why.
This isn't a set.
I know there's a lot of jokes about that.
This isn't a set.
It's really a shop.
Over on the other channel you can actually see it from different angles.
It's also not soundproof, which means I have to basically film in the middle of the night
most of the time.
If the ranch behind me is running a tractor, you're going to hear it because we're out
in the country and sound carries.
Working inside my house is not easy either because I've got a bunch of kids and some
little girl may decide that Frozen has to be turned on right that second or I may get
drafted into building a Lego toy or something like that.
So quietly we have been putting together a production space.
Now for y'all, for the most part on this channel, these daily videos, not much should change
other than I'll be able to record whenever and there may be a slightly different framing.
Hopefully you'll be able to see the whole t-shirt.
I know that's a big request out there.
But from my side, I'll be able to record whenever.
So I'll be able to respond to breaking news and hopefully get on some kind of regular
pattern.
Aside from that, we'll be able to restart the interviews that we used to do.
And in theory at least, the live streams will be a little less choppy, assuming that the
internet companies cooperate with all of that.
There will also be a space to do the skits, the upside down patch skits that a lot of
you like.
They're hard to do in our current environment.
There should be more of those.
There will also be the second channel, the documentary stuff.
That'll be a whole lot easier to do because there will be a separate space to work on
it.
And I will also have a spot to, I don't know, answer messages, which is hard with children
crawling on you.
So at the end of the day, what's happening is we are kind of upping our game.
We are hopefully going to be able to put out more consistent, higher quality videos.
We won't have to worry about the weather.
Right now if it rains, I can't record because the roof is tent.
So all of this is going to, in theory, get better.
And that's where I've been.
That's what I've been doing.
If I've missed a day or I'm only putting out a video a day or something along those lines,
that's what's happening.
This was going to be announced via live stream and then I realized that if we fall behind
schedule that may not happen.
So this is kind of a filler video to let everybody know what's going on.
Because anytime I miss a day, I get a whole bunch of messages asking if I'm okay.
So if I wind up missing two days, I have no idea what would happen.
I can't thank y'all enough.
Over the next year, we're really going to step everything up.
I actually look forward to meeting a whole lot of y'all in person.
And we should be able to increase the level of how much we're affecting things out there
in the real world.
Again I can't thank y'all enough for facilitating all of this.
Because, yeah, whatever.
Anytime it said, Bo did this, y'all did that.
Like 90% of the time.
So that's where we're headed.
And I can't wait to see it all play out.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}