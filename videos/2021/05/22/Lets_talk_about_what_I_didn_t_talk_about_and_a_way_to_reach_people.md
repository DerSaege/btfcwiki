---
title: Let's talk about what I didn't talk about and a way to reach people....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8eedJCa-62c) |
| Published | 2021/05/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Acknowledges not addressing a topic discussed widely by others last week, focusing on closely held beliefs and truth.
- Explains the challenge of triggering cognitive dissonance when introducing contradictory information to individuals with entrenched beliefs based on propaganda and slogans.
- Advocates for allowing individuals to realize the truth on their own rather than trying to force acceptance through beratement.
- Emphasizes the importance of examining closely held beliefs that contradict propaganda and slogans to prompt individuals to come to the truth themselves.
- Differentiates between moral and practical arguments in addressing politically divisive issues.
- Stresses the need to address practical concerns for real change in international relations, foreign policy, and war.
- Points out the ineffectiveness of solely relying on moral arguments in contexts where foreign policy decisions are made outside a moral framework.
- Suggests reframing arguments to avoid triggering cognitive dissonance and rationalization based on propaganda and slogans.
- Advocates for discussing universal truths and core beliefs as a method to reach people effectively.
- Expresses concern over the unsustainable nature of the current cycle of violence and advocates for change towards a more sustainable future.

### Quotes

- "I don't believe the truth can be told, think it has to be realized."
- "You don't have to give them new information."
- "The side that I'm on is the civilians. It's the side I'm always on."

### Oneliner

Beau addresses the challenge of navigating closely held beliefs and advocates for allowing individuals to realize the truth on their own by examining contradictory beliefs rather than forcing acceptance through beratement, stressing the importance of practical concerns for real change in politically divisive issues, particularly in international relations and conflict.

### Audience

Activists, Advocates, Educators

### On-the-ground actions from transcript

- Initiate open dialogues and discussions to help individuals realize truths on their own (suggested)
- Advocate for practical solutions and considerations in addressing politically divisive issues (implied)

### Whats missing in summary

The full transcript provides a comprehensive guide on navigating closely held beliefs, truth realization, and practical considerations in addressing politically divisive issues. The complete text offers detailed strategies and insights that may be missed in a brief summary.

### Tags

#CloselyHeldBeliefs #TruthRealization #PoliticalDivisiveness #PracticalSolutions #InternationalRelations


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about what I didn't talk
about last week.
I've had a whole bunch of people point out that I did
not talk about something, something that every other
channel that is even remotely like mine talked about.
Everybody did.
That's where the views were.
But I didn't.
So we're going to talk about that.
We're going to talk about closely held beliefs.
We're going to talk about truth.
And I'm going to run through a strategy
that you can use in your own life
to reach people that maybe have bought
into propaganda and slogans.
And they have developed a closely held belief
based on propaganda and slogans that
contrary to the way they normally are. See, that thing from last week, politically
divisive. Very much so. And because of that, people are very entrenched in their
positions. They have their talking points, they have their slogans, and they hold
those beliefs closely. If you provide somebody with a closely held belief, if
if you provide them contradictory information,
new information that contradicts that closely held belief, what happens?
You trigger cognitive dissonance
and it's uncomfortable. And normally what happens
is the people reject the new information out of hand.
They don't consider it because it flies in the face of something they believe.
It makes it very hard to have forward movement on politically divisive issues because nobody
is really listening to each other.
So how do you get through that?
You don't talk about it and you don't provide them new information.
I don't believe the truth can be told, think it has to be realized.
You can't berate somebody into accepting your position.
If that works, it's going to be incredibly superficial and temporary.
If you want to actually change somebody's mind, you have to provide them with the opportunity
to realize the truth on their own.
Something else that I believe is that if something is true, it is true across all systems.
And most people believe that.
That's why a lot of argumentation is, what about ism?
Oh, well, what about that over there?
People innately know that hypocrisy shouldn't exist if something is true.
Should always be true.
So don't give them new information.
Don't try to berate them into accepting your position.
Put them in a position where they're examining their own closely held belief that is contradictory
to the one that is based on slogans and propaganda.
Allow them to experience it on their own.
them come to the truth themselves. Sounds great, right? How do you do it? With this situation,
you have moral arguments and you have practical arguments to just about everything.
With the situation over there, moral arguments, as far as affecting change, they don't really matter.
If moral arguments mattered when it came to the treatment of marginalized people, the United States would not look the
way it does.  However, you still want to make those arguments, because that's where that contradictory belief may lie.
Okay, so I think that most people would believe that dividing people up, kind of sectioning them off, that's uncool.
You shouldn't do that. I think most people would believe that. And if something is true, it's true across all systems.
If you do do that, you also have the moral obligation to make sure that the people who are
separated off can continue their way of life. You can't wall them off and leave
them in abject poverty, right? But you can't talk about it like that because if
you do it will get rationalized. They'll come up with a reason that it's okay in
this one situation because of the propaganda and the slogans. So don't talk
that situation. Instead of talking about it in that framing, talk about, I don't
know, maybe the Klamath River running through a native reservation and how
people outside are looking at diverting some of the water and impacting their
way of life, violating that moral obligation. Maybe they make the
connection. Another big one is the disproportionate use of force, unnecessary
force. The big one last week that caught everybody's attention was the AP building
getting hit. But see, you can't talk about that. If you do, it will be rationalized, so
you can't talk about the Associated Press. So don't talk about Alexandra
Peterson, a journalist, and how her apartment got blown up by the cops going after somebody
downstairs. If something is true, it's true across all systems. You can trigger that cognitive
dissonance within their own beliefs. You don't have to introduce new information that will be
rejected. This is something they already believe. I would suggest that most people believe that
that militarization of a community is bad.
So maybe talk about the 1033 program.
Kind of highlight that.
It's also worth noting that in this situation over there,
one side has a whole lot more power than the other.
One is capable of dealing a whole lot more damage.
So maybe talk about risk assessment.
Talk about how, if you have a bulletproof vest on and goggles,
and somebody's going to shoot a BB gun at you,
well, that's not really a threat.
And maybe somebody makes the connection
to that multi-billion dollar system, the defense system,
you're trying to get them to realize they
hold contradictory beliefs.
You don't have to give them new information.
Now, in this case, there's a both sides thing.
And if you've watched this channel long enough,
you know I don't like the both sides anything.
But in this case, one objectively exists.
There are international norms and international standards
of conduct when it comes to conflict.
In this case, both sides can legitimately
say, because of what happened to my people, those laws, well, I kind of view myself as
a little bit exempt from them.
Doesn't matter which side you're on.
You know that that's true, historically.
You may not believe that that's justification, but you know that that rationale exists and
And there's a legitimate reason for the rationale to exist.
So acknowledge it.
Acknowledge it.
Yeah, legality isn't always morality.
Maybe talk about pirates and emperors.
But at the same time, you don't want to encourage them to go further.
So maybe point out that you don't need a law to be a good person.
Highlight that by talking about how you're going to wear a mask, even though there's
no legal mandate to. It's all little things that may work on one person.
All of these things, they're not going to hit everybody. One of them may work on a
whole lot of people. One of them may only work on one. Those are the moral arguments.
And then you get to the practical ones. And if you're talking about real change,
these are the ones you have to keep in mind, because while people like discussing
the moral arguments because there's a lot of clarity there, what is that
situation? It's one of international relations, foreign policy, and war.
Morality is not a component in any of those discussions. Foreign policy decisions are
not made within a moral framework. Not really. And if you take the stance of
only addressing the moral arguments, well you only strengthen the hand of those
people who know they don't really matter. Not in this sense. So what are the
practical concerns? And these are going to be things that people know and they
know them to be true but maybe they're willing to overlook them because of
slogans and propaganda. Firing without having the intelligence to know what
you're hitting and by intelligence I'm talking about information. That's bad. I
think most people would agree with that. However, again if you talk about it in
that sense over there, well what about the other side? They do it too. That's
what you're going to get. So don't frame it there. Instead of talking about it in
relationship to that conflict, talk about it in relationship to Afghanistan and
our preparations for maintaining over-the-horizon capabilities and
what's likely to happen. You might want to provide a hard foreign policy reality
check too because people know this to be true as well. The dominant power over
there gets a lot of leeway from world powers because it is strategically
important. A lot like South Africa. A lot like South Africa. And if you're going to
kind of point that out you might also want to do a follow-up conversation
where you mentioned that there is a foreign policy option on the table right
now that would lower the strategic importance of the entire region, but
Biden is running out of time to do it. When you're talking about militarization
and the money that it takes to do that and the equipment, it might be worthwhile
to point out that using that on schools and EMS might be more beneficial. You
could do this in the 1033 conversation because people know that more military
power doesn't normally bring peace. Education does. Opportunities do. Getting
past it is what creates a lasting peace. Then the big one. They're doing it wrong.
The dominant power is doing it wrong. Everybody who has ever watched any movie
about this type of thing knows that a security clampdown only strengthens the
resistance. If you've watched this channel you certainly know it. I've
talked about it a lot but it's a common theme in Hollywood as well. People know
it, they know this innately. So point out that that's occurring. Those security
clampdowns, those excesses of force, they only prolong the conflict. They don't end
it and maybe point out that it is on the dominant power to make sure that negotiations happen.
It's their responsibility.
You can do both those in the same conversation, talking about slogans and pirates.
And then when it comes to negotiations, maybe point out that from the outside, if one group
has made concessions, it kind of appears like they just want a fair shake, and you
could talk about that in relationship to miners who are currently on strike.
I talked about it all week, and I just didn't talk about it in that context.
I added other context.
And if you want to do that, you might also want to point out that those who are in power,
who benefit from slogans, they don't like context.
And you could talk about the Republicans not wanting an investigation into Capitol Hill.
So it drives the point home.
So from start to finish, you have different topics you can use to address the core universal
truths, and if something is true, it's true across all systems, and everybody knows that.
This is a way you can reach people.
This is a method that does work.
Whether it worked in this case, I have no idea, no clue.
I forget it was worth a shot. For once this was getting a lot of coverage.
It normally doesn't.
So that's why I didn't talk about it last week
because I spent a lot of time talking about it without talking about it.
Hoping to dislodge people
from
slogans, from propaganda.
I know
that that's not what a lot of people want to hear. They want to hear those
moral arguments. They want to be told that they're right.
And maybe you are.
Maybe you are.
But all that does
is motivate
that side,
whichever side you're on.
I don't know that it really changes minds
when you're talking about a situation
that is this entrenched.
And one thing we do know is that this is unsustainable.
The side that I'm on is the civilians.
It's the side I'm always on.
This is unsustainable.
It can't last.
And with the current dynamic, this cycle of violence
will continue until the dominant power is no longer
as strategically important.
It would be cool if we could get to the point
where this could be talked about without talking points.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}