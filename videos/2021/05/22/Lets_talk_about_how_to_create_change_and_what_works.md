---
title: Let's talk about how to create change and what works....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jWZijU_E0ls) |
| Published | 2021/05/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions the effectiveness of marches and rallies in creating real change, referencing the situation in North Carolina where they seem to have little impact on decision-makers.
- Emphasizes that all forms of activism work together as a diversity of tactics to create systemic change, including petitioning, marches, direct involvement, calling senators, and electoralism.
- Draws parallels between movements like Black Lives Matter and historical events such as the American Revolution to illustrate the concept of a movement versus a moment.
- Encourages individuals to focus on utilizing their unique skills and strengths to contribute to the broader goal of systemic change.
- Stresses the importance of consistent effort and a diversity of tactics in pushing forward for change, even when individual actions may seem insignificant.
- Advises against counting on any single event or action to make a significant impact, as real change often requires years of behind-the-scenes work and organizing.
- Emphasizes the need for diversity in tactics and promoting individuals who approach the same goal in different ways over creating a clique of like-minded individuals.
- Acknowledges the challenges of staying motivated in activism, noting that each small action contributes to the larger movement for change.
- Urges maintaining forward momentum and focus on the long-term goal of creating a fair and just world through collective efforts.

### Quotes

- "It's not a moment. It's a movement."
- "Each little piece, each little action is one moment in that movement."
- "Whatever you're best at. Whatever you're really good at. Those are the skills you need to use to further that drive for systemic change."

### Oneliner

What works and what doesn't in creating systemic change: diversity of tactics, consistent effort, and focusing on the long-term goal of a movement over individual events.

### Audience

Activists, community organizers

### On-the-ground actions from transcript

- Utilize your unique skills and strengths to contribute to the drive for systemic change (implied).
- Promote individuals who approach common goals in different ways to encourage diversity in tactics (implied).
- Stay engaged in activism, attend marches, rallies, and remain active in pursuing change (implied).

### Whats missing in summary

The full transcript provides a comprehensive understanding of the importance of diversity in tactics, consistency in efforts, and the long-term focus required for creating real systemic change.

### Tags

#SystemicChange #Activism #DiversityInTactics #LongTermFocus #CommunityOrganizing


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about
what works and what doesn't and how we can create real change, that deep systemic change
that we all want. Right? Because I've got a question. What really works, like marching
and rallies, does that actually do anything? In North Carolina we have marches and rallies
all the time. Then Tillis and Burr or the General Assembly just do whatever they want
and that's the end of it. Yeah. Yeah. Okay. So what works and what doesn't? Everything
works. Everything works and all of it will fail. Everything works together. Nothing works
separately. So petitioning, marches, getting directly involved, calling your senator, electoralism
in general, all of it works together. None of it works separately. It's very rare that
one type will be successful. That doesn't happen very often. It's a diversity of tactics.
We talked about this in that video on South Africa. It's not a moment. It's a movement.
Consistent effort being pushed forward to create that change. But when we talk about
South Africa, other countries in general, it's hard for people in the United States
to relate because it is so far removed. So let's talk about stuff we know. Everybody
today is familiar with Black Lives Matter, right? Everybody's familiar with that. The
first time I heard that was in Ferguson in 2014 and it wasn't new. It started the year
before. It was just the first time I heard it. It's been all of this time, all of that
effort, and they're just now starting to get some real change that you can actually
see. They're nowhere near done. And that was not a weak movement. They are still going
strong today. That idea, that concept, it's still there. It's not any individual march
or rally or signature drive. It's not any individual event, any individual action. It's
not a moment. It's a movement. Something that illustrates this really well, especially
for Americans, our revolution, the American Revolution. You ask anybody in the U.S., when
was the American Revolution? 1776. No hesitation on that one. Go ahead and pull up a timeline
of the American Revolution. See when it starts. Odds are it will start 1754. That's conventional
wisdom. It's including the French and Indian War. I actually disagree with that take. I
think that was more of a primer to make the colonists realize that they could do something.
But I don't think that it really fed into it the way it gets portrayed a lot. I would
say 1765 with the Stamp Act. That is when the colonists went wild. They did stuff that
made last summer seem tame. And maybe that's why we don't talk about it so much. That's
why we say 1776 because we need to preserve that American mythology rather than the American
history. Most of the events that you would say are part of the timeline of the American
Revolution occurred before 1776. The Tea Party was 73. The Boston Massacre, 1770. The shot
heard around the world, the start of the Revolution. You remember the song Schoolhouse Rocks? 1775.
It's not a moment. It never is. And then you ask people when it ended because you have
the 1776 thing. Then there was the whole conflict. Most people say that it wrapped up and we're
done. 1783 and that's the end. But that's not true either because it wasn't until 1788
that the Constitution was finally ratified. So you're talking from using my abbreviated
shortened timeline, 1765 to 1788. Almost a quarter of a century. It is never a moment.
It's never a single march. It's never one thing. So what do you do? Whatever you're
best at. Whatever you're really good at. Whatever your skills are, those are the skills
you need to use to further that drive for systemic change. That's where you have to
focus. Yeah, show up to the march. Show up to a rally. Engage. Stay active at all times.
But don't count on any individual event mattering in and of itself because odds are
it won't. Most times when something seems to spring to life and get real traction, there
were people working behind the scenes on that for years. Organizing is hard. Creating change
is hard. Systems are reluctant to change. And that's what you're trying to do. So at
the end of the day, the most important part to remember is that it all works and that
diversity of tactics is the most important thing. You know, another question I got was
how do I decide who I'm going to promote on Twitter or on YouTube or whatever? Because
none of them are like me. Literally the point. That is one of the big criteria right there.
If you are exactly like me, you carry the same message, can reach the same people, there's
no sense in me promoting you. It's got to be somebody else. That diversity of tactics.
Somebody that is moving towards the same goal in a different way. That's more valuable than
creating a clique of people like me. It's hard and it's hard to stay motivated when
you have to acknowledge that what you're going to do tomorrow by itself, it means nothing.
It isn't going to matter in and of itself. But what you're going to do over the next
year, well, each little piece, each little action is one moment in that movement. And
that's what we have to focus on doing. Is keeping up that forward momentum. Keeping
up that drive to create that change. To get that world where everybody gets a fair shake.
Kind of thankless, initially. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}