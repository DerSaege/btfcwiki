---
title: Let's talk about Ted Cruz's wild op-ed on contributions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uAZe8KlF1fY) |
| Published | 2021/05/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about an op-ed by Ted Cruz in the Wall Street Journal, which stirred controversy on Twitter due to implications of taking campaign contributions in exchange for favors to large corporations.
- Cruz criticizes woke companies and their responses to bills, especially regarding Georgia.
- Cruz vows not to overlook issues like Coca-Cola's back taxes or MLB's antitrust exemption.
- He announces that he will no longer accept money from corporate PACs, citing the $2.6 million received over nine years.
- Cruz implies a connection between campaign contributions and favors, leading to a reconsideration of conventional wisdom on political influence.
- Suggests setting up a GoFundMe account to afford a senator based on the relatively low cost.
- Acknowledges Cruz may not have intended to admit to this exchange but understands how it can be interpreted that way.
- Irony in Cruz's statement regarding not accepting money anymore due to perceived mistreatment by corporations.
- Questions the effectiveness of Cruz's message in the op-ed.

### Quotes

- "This time, we won't look the other way on Coca-Cola's $12 billion in back taxes owed."
- "Maybe we can afford a senator."
- "Y'all were mean to me so I'm not going to let you pay me off anymore."

### Oneliner

Beau delves into Ted Cruz's controversial op-ed, hinting at a potential exchange of campaign contributions for favors, leading to a reevaluation of political influence and the affordability of senators.

### Audience

Political enthusiasts, activists

### On-the-ground actions from transcript

- Start a GoFundMe campaign to fund political causes (exemplified)
- Analyze and question the connections between corporate contributions and political favors in your local political landscape (suggested)

### Whats missing in summary

Insights on the impact of corporate contributions on political decision-making and the need for transparency in political funding.

### Tags

#TedCruz #CorporateInfluence #CampaignContributions #PoliticalTransparency #GoFundMe


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about an op-ed from four or five days ago in the Wall Street
Journal written by Ted Cruz.
The reason we're talking about it four or five days after it happened is because I didn't
read it when it came out because I mean, come on, who wants to read an op-ed written by
Ted Cruz?
I was wrong.
I was wrong.
It is wild.
It is being widely received on Twitter at least as him kind of admitting to taking campaign
contributions in exchange for favors to large corporations, which I mean that's something
we all kind of assume goes on, but it was weird to have it kind of put out in the way
that it appears he did.
Again, he may not have meant it in the way it is being received, but that is the reception.
So it starts off with him going off on this wild rant about woke companies and they don't
read the bills before they get all mad and all of this stuff and yeah, they are super
mad about Georgia.
Okay, he goes on to say, this is the point in the drama when Republicans usually shrug
their shoulders and call these companies job creators and start to cut their taxes.
Not this time.
This time, we won't look the other way on Coca-Cola's $12 billion in back taxes owed.
This time, when Major League Baseball lobbies to preserve its multi-billion dollar antitrust
exemption, we'll say no thank you.
This time, when Boeing asks for billions in corporate welfare, we'll simply let the export-import
bank expire.
You know, for people who don't care what woke companies do, they seem really upset about
this.
I mean that slogan, go woke, go broke, remember all of that?
I guess that's not true because they seem to be mounting some opposition to this practice
by these companies.
It goes on and he says, in my nine years in the Senate, I've received $2.6 million in
contributions from corporate political action committees starting today.
I no longer accept money from any corporate PAC.
I want you to remember that $2.6 million number over nine years.
Little less than $300,000 a year.
And then he goes on a little bit later to say, to them I say, when the time comes you
need help with a tax break or regulatory change, I hope the Democrats take your calls because
we may not.
Starting today, we won't take your money either.
And you're looking at these pieces, you can understand why it's being received the way
it is on Twitter.
But this does show a couple of things.
First, apparently the large corporations, speaking out the way they have, does have
an impact because they are super mad.
But second, we might be wrong about something.
If you are, when you read this article, and I'll have it below, if you do read it as him
connecting the favors and the campaign contributions, we may need to rethink some of our conventional
wisdom.
We've always said that we don't get real representation because we don't have enough money to buy
a senator.
Do the math.
2.6 million over nine years, a little less than 300 grand, let's just take the Coca-Cola
thing, $12 billion in back taxes owed that apparently they looked the other way on.
That is 0.0025%.
Maybe we can afford a senator.
I honestly thought it would cost more.
I mean, that's pretty inexpensive.
We could probably do a GoFundMe to get a lot of the stuff we needed if that's all it costs.
And that's just one of the companies.
And he says that the 2.6 is from all of them.
Senators aren't as expensive as we believed, I guess.
Now in Cruz's defense, I'm fairly certain he wasn't attempting to admit to taking campaign
contributions in exchange for favors.
I'm pretty sure that's not what he's trying to convey here.
But at the same time, I also can't blame people for taking it that way because the contributions
and the favors are mentioned in close proximity more than once.
Yeah, I don't think I've ever enjoyed something written by Ted Cruz more than this.
So the takeaway here is we need to set up a GoFundMe account, start our own pack or
something.
And the other one is for as much as they talked about how worthless it was that these
woke companies were speaking out, it certainly appears to be having an impact.
Because if they're putting out op-eds like this in the Wall Street Journal, they're probably
trying to get a message across.
I don't know that this is really going to be that effective because of the way it reads.
It kind of comes across as, y'all were mean to me so I'm not going to let you pay me
off anymore.
Again, I'm certain that's not what he was trying to say.
But I wouldn't blame anybody who read it that way at the same time.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}