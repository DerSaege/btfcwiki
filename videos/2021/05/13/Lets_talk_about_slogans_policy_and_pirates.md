---
title: Let's talk about slogans, policy, and pirates....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5vynxgMs8Zs) |
| Published | 2021/05/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Slogans can mislead citizens into believing they represent actual government policy.
- The United States does negotiate with groups, contrary to popular slogans.
- Modern doctrine aims to remove militant leadership to encourage negotiation.
- Misunderstanding slogans can lead to a mistaken expectation of government behavior.
- Expecting a "get tough" approach can result in security clampdowns and innocent people being harmed.
- Clampdowns can inadvertently strengthen opposition groups.
- Justifying actions based on opposition behavior can lead to dangerous outcomes.
- Blaming protesters for everything can escalate violence and justify unjust actions.
- The story of Alexander the Great and the pirate illustrates the similarity in actions between pirates and emperors.
- Failing to recognize violence as violence leads to justifying any action by one's side and condemning the other's.
- In state versus non-state actor conflicts, a security clampdown often leads to an endless cycle of violence.
- The cycle of violence can only end when the state side commits to negotiation.
- In the United States, there were calls to deploy troops against citizens due to demonization and belief in slogans.
- Without a commitment to negotiation, the cycle of violence persists.
- Recognizing violence as violence is critical to breaking the cycle of violence.

### Quotes

- "Expecting a 'get tough' approach can result in security clampdowns and innocent people being harmed."
- "Failing to recognize violence as violence leads to justifying any action by one's side and condemning the other's."
- "The cycle of violence can only end when the state side commits to negotiation."

### Oneliner

Slogans can mislead citizens into expecting government actions, leading to violence and the perpetuation of conflict until negotiation is truly committed.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Contact local representatives to advocate for peaceful conflict resolution (implied).
- Organize community dialogues on the impact of slogans and policies on government actions (implied).
- Educate others on the dangers of justifying violence based on opposition behavior (implied).

### Whats missing in summary

The full transcript delves deeper into the dangerous consequences of misinterpreting slogans as policy and the necessity of committing to negotiation to end cycles of violence. Watching the full transcript provides a comprehensive understanding of these concepts.

### Tags

#Slogans #Policy #Violence #Negotiation #Government #Commitment #Community #Advocacy


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about slogans and policies and pirates.
You know sometimes a slogan gets said so often and it's so catchy that the average citizen
in the country where the politician says the slogan begins to believe that that's policy,
that that's actually how the government should function.
And that's normally not the case.
Give you an example.
The United States does not negotiate with blank.
You can fill in the blank right?
Because you've heard it over and over and over again.
You might even believe it's true.
Is it?
No, of course not.
We negotiate with them all the time.
All the time.
In fact, policy, modern doctrine, the prevailing theory on how to deal with groups like that,
it's to get rid of the more militant leadership.
Therefore a power vacuum develops and it sucks up the politically savvy.
Because the politically savvy are more likely to go to the negotiating table.
Not just does the United States negotiate with them.
Our current doctrine is to try to get them to negotiate.
Why is this important?
Why does this little distinction matter?
That in and of itself, it's a misunderstanding.
But what happens when the people begin to expect the government to behave in that way?
To not negotiate?
To use that get tough approach that sounds so good?
If you've watched the videos on this topic, you know what happens.
A security clampdown occurs.
Get tough.
Send in the troops.
And during that clampdown, innocent people get caught up in it.
And those innocent people have family members who now join the opposition.
And contrary to the idea of it working, it winds up strengthening the other side.
And then it can get even worse.
When the state actor, let's take Columbia as an example, when they begin to justify
their behavior based on the actions of their opposition.
This just happened down there.
Anything that happened, well they blamed it on the protesters.
Even if it was on video as being their people.
We had to do that because of the protesters.
That is a dangerous road.
Historically that leads to really, really bad things.
Once you can justify anything by blaming it on your opposition, well, you end up doing
things that you probably wouldn't.
You end up justifying things you probably shouldn't.
And then people will make the moral argument.
Obey the law, right?
If the protesters had obeyed the law, that wouldn't have happened.
And that's when pirates come out.
See, St. Augustine told a story.
He said that Alexander the Great had caught a pirate and he was talking to him.
He was like, why are you doing this to the seas?
You're disrupting everything.
You're plundering.
I'm paraphrasing, obviously.
And the pirate's like, what are you doing to the whole world?
You're plundering.
Alexander's like, wow, that's kind of a good point.
The pirate only has one ship.
Alexander had a fleet.
But the action was the same.
That pirates and emperors idea.
It has been explored by Noam Chomsky.
It has been explored by a Schoolhouse Rocks video.
Not really Schoolhouse Rocks, but it's entertaining.
You should watch it.
Once you get to that point where you are failing to see that violence is violence, it's over.
It's lost.
Because by that point in time, you have justified any action that your side does and condemned
any action the other side does.
And when you are talking about something that is state actor versus non-state actor, and
that security clampdown occurs, only one thing can follow.
An endless cycle of violence until the state side believes it's time to negotiate and truly
commits to it.
It keeps going if that doesn't happen.
Now in Columbia, the state side, they decided to go for it.
This same idea took hold in the United States very recently, last summer.
And you had people in the U.S. government calling for the troops to be unleashed on
American citizens because they had demonized the other side and they bought into the slogans.
And then they could justify any action.
And had that happened, the cycle of violence never would have ended.
It would still be going on right now.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}