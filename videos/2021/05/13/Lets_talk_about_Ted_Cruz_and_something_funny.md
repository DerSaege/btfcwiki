---
title: Let's talk about Ted Cruz and something funny....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tKLbKJlREf0) |
| Published | 2021/05/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Feels conflicted about doing something funny or addressing a serious message.
- Mentioned receiving a message questioning his claims about Republican politicians.
- Commented on Ted Cruz's tweet about gasoline outages in North Carolina.
- Explained the irony behind the connection made to the green new deal in Cruz's tweet.
- Noted that the green new deal is proposed legislation, not yet law, and has had no effect.
- Pointed out the potential benefits of the green new deal in reducing reliance on fossil fuels.
- Mentioned Ted Cruz's lack of enthusiasm for certain infrastructure types.
- Criticized Cruz for implying a connection between the pipeline issue and the green new deal.
- Suggested that Cruz's tweet could be seen as misleading and fear-mongering.
- Noted the high number of likes on Cruz's tweet, indicating support without critical analysis.

### Quotes

- "Welcome to the green new deal."
- "It's funny that a sitting senator doesn't know that this isn't law."
- "I find that funny as long as you don't think about it too long."

### Oneliner

Beau feels conflicted between humor and seriousness, critiquing Ted Cruz's misleading tweet about the green new deal's connection to gasoline outages.

### Audience

Social media users

### On-the-ground actions from transcript

- Fact-check misleading information shared on social media (implied)
- Encourage critical thinking and research before accepting claims blindly (implied)

### Whats missing in summary

The full context and nuances of Beau's commentary on the misleading nature of political statements and the importance of critical thinking.


## Transcript
Well howdy there internet people, it's Beau again.
So today, I'm a little conflicted to be honest.
See I want to do something funny because it's been a pretty wild week in the news and I
think we could all use a laugh.
The problem with that is that I got this message and I feel like I should answer that.
It says, Beau, you always say that Republican politicians mislead their base and constituency
all the time and that they fear monger rather than provide policy and that their base always
goes along with it without ever really thinking about it or looking into it.
Yet you, sir, never provide an example of this happening.
See the thing is, I want to answer that but I also want to do something funny because
I mean that's pretty serious.
The problem with doing something funny is that I'm not really a funny person.
I have a very dry sense of humor.
So to look for ideas, I went to the one place that always makes me laugh, Ted Cruz's Twitter
feed and he didn't let me down, he never does.
He retweeted this thing, it says gasoline outages pile up with nearly a quarter of North
Carolina gas stations out of fuel and he captioned it, welcome to the green new deal.
The green new deal being that proposed legislation that isn't law yet and has had absolutely
no effect whatsoever on anything and the ultimate irony being that even if it did go into effect,
we would probably be less reliant on fossil fuels and therefore a pipeline going down
for a little bit wouldn't really be as big a concern because we'd probably have local
renewable sources of energy and infrastructure like that.
Incidentally, Ted Cruz isn't really a big fan of that kind of infrastructure.
It's funny that a sitting senator doesn't know that this isn't law.
I mean he certainly seems to be implying some kind of connection between this pipeline going
down and the green new deal.
I mean that seems a little misleading to me.
I mean it's almost like he's attempting to scare his base, fear monger if you will, about
some proposed legislation rather than relying on policy.
Incidentally, this tweet was liked more than 8,000 times by his base who certainly appears
to have bought into it without looking into it at all.
See I find that, I find that funny as long as you don't think about it too long.
Anyway it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}