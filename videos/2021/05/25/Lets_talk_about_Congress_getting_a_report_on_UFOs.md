---
title: Let's talk about Congress getting a report on UFOs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JZeN-Xmz5T4) |
| Published | 2021/05/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Congress will receive a report next month on unidentified aerial phenomena, commonly known as UFOs.
- The military has had a longstanding interest in UFOs dating back to the late 1940s.
- Programs like Project Blue Book and the Advanced Aerospace Threat Identification Program have investigated UFO sightings over the years.
- Despite recent headlines suggesting a newfound interest in UFOs, military involvement and investigations have been ongoing for decades.
- The upcoming report is unlikely to reveal definitive proof of extraterrestrial life visiting Earth.
- Beau believes in the existence of intelligent life elsewhere in the universe but remains skeptical about aliens visiting Earth.
- He points out that popular theories about extraterrestrial civilizations are largely based on human history and assumptions.
- Beau encourages managing expectations about the upcoming report on UFOs.
- While intriguing footage exists, Beau remains cautious about drawing definitive conclusions from it.
- Regardless of the report's contents, Beau plans to read it and find it interesting.

### Quotes

- "The military's been interested in this for a very very long time."
- "I wouldn't get your hopes up, but at the same time I'm definitely going to read the report."
- "Of course I believe that there is intelligent life out there somewhere."
- "It could be a very cooperative species. It could be one that's very just wants to observe."
- "Regardless of what's out there, I don't foresee us finding that out next month at this hearing."

### Oneliner

Congress will soon receive a report on UFOs, but expectations of groundbreaking discoveries may be unfounded, given the military's long-standing interest in unidentified aerial phenomena.

### Audience

Science enthusiasts, UFO believers

### On-the-ground actions from transcript

- Read the upcoming report on UFOs when it is publicly available (exemplified)
- Manage expectations about potential revelations in the report (exemplified)

### Whats missing in summary

Beau's nuanced perspective on UFOs and extraterrestrial life, cautioning against heightened expectations from the upcoming report.

### Tags

#UFOs #ExtraterrestrialLife #MilitaryInterest #ProjectBlueBook #PublicReport


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the report that's going up to Congress next
month and will be, at least sections of it,
will be publicly available.
I guess we'll find out if there are little green men.
If you don't know what's going on,
next month
Congress will be getting a report
on
unidentified aerial phenomena
is the term they're using. It's what you and I would call UFOs.
They're going to be getting a report on it.
Now there's a lot of headlines
that have come out about this
and they are definitely
raising expectations.
The headlines are stuff like
how the US military started taking UFOs seriously
or how UFOs went from
a punchline to national security concern.
The idea behind all of them
is that this is a new and sudden interest.
That's just not true.
The military's been interested in this for a very very long time, going back to
1947, 1948,
somewhere in there.
A thing was started called Blue Book.
Project Blue Book. You have probably heard it referenced
in science fiction films.
That's a real thing.
That's a real program that actually existed to go out and investigate these
sightings.
It was like a real life X-Files.
It ran
from
the end of the forties
until, I want to say it was
terminated at the end of sixty nine but didn't really shut down until
nineteen seventy,
the beginning of nineteen seventy.
On top of that
there was the Advanced Aerospace Threat Identification
Program
that ran from around 07 to 2012.
And there are a couple others. And these are ones that have been declassified
and that we know existed
and we know the military has been interested in this for a very very long
time.
It's not new.
It's not new.
And it's that portrayal that it is something new
that I think is what's raising everybody's expectations
of what's going to be in this report.
All of the other ones
have pretty much said
hey, like five percent of these thousands of sightings that we've
investigated,
yeah, we have no clue what they are.
We can't explain it.
That's really what it kind of boils down to.
I would imagine
that
that's probably what's going to be in this report.
Now I could be wrong
because
there is also
there's cooperation from other agencies
that weren't involved
in some of the other reports.
So maybe there's some more information.
But I find it highly unlikely
that next month they're going to walk out and say yeah, they're here.
I don't see that occurring.
There is a lot of interesting footage
that the Navy has confirmed
is real.
I have one question about it
because it's all filmed
through infrared.
All of the really interesting stuff, it's filmed the same way
and it's not on any normal
cameras.
That leads me to believe that there may be some kind of anomaly going on that isn't
from up there.
But
we don't know.
So
I wouldn't get your hopes up
but at the same time I'm definitely going to read the report.
It should be interesting.
I guess the obvious follow-up question to this is do I believe they exist?
Is there intelligent life out there somewhere?
Of course. Of course I believe that.
You'd have to be pretty arrogant to believe this is the only planet
that has intelligent life.
Whether or not they visited here,
I have no clue.
I would imagine that
if they have the technology to come here,
they probably have the technology to conceal their visit.
So we
probably wouldn't know.
And if I were them, I would keep a low profile
because there's a bunch of different theories
about what
those civilizations might be like.
But they're all based on the human experience.
So
most of them
have the idea that they would be conquering peoples
because that's what we are.
That's our history.
That kind of assumes that they would have evolved from
their version of primates.
There's nothing to suggest that that's what happened
if it did.
It could be a very cooperative species.
It could be one that's very...
just wants to observe.
But
regardless of what's out there, I don't foresee us finding that out
next month
at this hearing.
Even though...
or with this report,
even though large sections of it are supposed to be automatically declassified and open
to the public.
But if you're wondering
what all of the conversation about UFOs is about lately,
this is why.
So anyway,
it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}