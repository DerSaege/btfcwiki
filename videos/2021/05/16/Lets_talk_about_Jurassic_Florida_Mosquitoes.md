---
title: Let's talk about Jurassic Florida Mosquitoes.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=m51AW863njc) |
| Published | 2021/05/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Scientists are conducting a wild science experiment in Florida involving genetically modified mosquitoes.
- Male mosquitoes have been modified to pass on a gene to their female offspring, requiring them to have an antibiotic to prevent breeding.
- The goal is to reduce mosquito-borne diseases without using pesticides.
- Similar experiments have been done in other countries with positive results.
- The reduction of pesticides, especially in Florida, is seen as a positive aspect of this experiment.
- Beau mentions the famous line from a movie, "life finds a way," hinting at potential unexpected outcomes.
- The impact of mosquito-borne diseases is becoming increasingly significant due to climate change expanding the insects' ranges.
- Despite sounding like a plot from a science fiction movie, the experiment has multiple safety safeguards in place.
- The modified mosquitoes are expected to be self-limiting and last only a couple of generations, providing a safety net in case something goes wrong.
- Beau acknowledges that there might be concerns and invites experts to share their opinions on the experiment.

### Quotes

- "Life found a way."
- "Scientists are conducting a wild science experiment in Florida involving genetically modified mosquitoes."
- "The reduction of pesticides, especially in Florida, is seen as a positive aspect of this experiment."

### Oneliner

Scientists conduct a wild science experiment in Florida involving genetically modified mosquitoes to reduce mosquito-borne diseases without pesticides, despite potential risks.

### Audience

Environmentalists, scientists, community members

### On-the-ground actions from transcript

- Share information about genetically modified mosquitoes with your community (suggested)
- Seek out expert opinions on the subject and start a constructive discussion (suggested)

### Whats missing in summary

The full transcript provides a detailed insight into the science experiment involving genetically modified mosquitoes and the potential impact on reducing mosquito-borne diseases in Florida.

### Tags

#GeneticallyModifiedMosquitoes #FloridaScienceExperiment #PesticideReduction #MosquitoBorneDiseases #ClimateChange #CommunityEngagement


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about
Jurassic
mosquitoes in Florida.
And that's not a joke about their size.
There is a wild science experiment going on this weekend.
Apparently this weekend
mosquitoes will be hatching.
The males
will uh...
well they've been modified.
And they will pass on a gene to their female offspring
that will require their female offspring
to have tetracycline,
an antibiotic.
The theory is that they won't get this antibiotic, therefore they won't
have offspring themselves, therefore it will
lower
the mosquito population,
hopefully
reducing
mosquito-borne disease
without the use of pesticides.
They're doing this down in the Keys.
And I feel like I've seen this movie before.
So to be clear,
you have a genetically modified
organism
that uh...
requires something only the scientists can provide
so they don't breed.
Pretty sure in the movie, life found a way.
I guess we'll have to
wait and see if that happens.
Now for all of the people who are absolutely going to just
freak out over this, and I'm sure there's going to be a lot,
it's worth noting that they have done this
in other countries before
with pretty good results.
The idea of cutting down pesticides, especially here in Florida,
that's a good thing.
I guess we'll have to wait
to find out whether or not this was a case of scientists
being so concerned about whether or not they could, they didn't stop to think if they should.
But overall, I think it's kind of cool.
The idea
of
lessening the impact of mosquito-borne disease is going to be...
it's going to become more and more important.
With climate change,
the ranges
of these insects will increase,
which means the ranges of these diseases will increase.
So it's a unique approach,
despite it sounding like a plot device
from some science fiction movie
that will inevitably go wrong.
It seems to have a lot of safety safeguards.
It appears that it would be self-limiting and only last a couple generations of mosquitoes,
even if something does go wrong, which, I mean, that's always a bonus.
But from what I can tell, it doesn't look...
it doesn't look like they'll be taking over the theme parks down here.
But I thought I'd go ahead and put that out there. If there are any experts on this subject,
I would love to hear your take on it in the comments.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}