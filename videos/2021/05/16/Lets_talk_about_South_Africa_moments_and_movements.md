---
title: Let's talk about South Africa, moments, and movements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wlqPYOWrA2Y) |
| Published | 2021/05/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Gives an overview of the history of apartheid in South Africa, debunking common misconceptions.
- Explains that apartheid was not solely ended by Nelson Mandela but was a result of a combination of factors and events.
- Details the timeline of apartheid laws and events from 1948 to the end of apartheid.
- Mentions key moments such as the Sharpeville massacre in 1960, international pressure, and the township rebellions.
- Talks about the importance of understanding the diverse tactics and factors that led to the end of apartheid.
- Emphasizes that societal change is a result of multiple factors and not a single event or person.
- States that the end of the Cold War played a significant role in the fall of apartheid.
- Advises looking at historical sources like the State Department archives to understand complex societal changes.
- Stresses the necessity of considering economic, geopolitical, and cultural aspects in bringing about real change.

### Quotes

- "If you want real change, it's going to take a diversity of tactics."
- "It's not one thing. It's almost never one thing."
- "That's the deep lesson, if you want real change."
- "You're talking about power. And that is very rarely motivated by morality."
- "It's not always a moral issue."

### Oneliner

Beau explains the complex history of apartheid in South Africa, debunking myths and underlining the diverse factors that led to its end.

### Audience

History enthusiasts, social activists

### On-the-ground actions from transcript

- Study the history of apartheid and understand the diverse tactics that led to its end (suggested).
- Analyze complex societal changes in other countries through accurate historical sources like State Department archives (suggested).

### Whats missing in summary

In-depth analysis of the role of international pressure and the end of the Cold War in the fall of apartheid.

### Tags

#SouthAfrica #Apartheid #History #SocialChange #StateDepartment


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about South Africa.
We're going to go through a timeline,
because I got a whole bunch of questions about South Africa last night.
And I'm going to answer most of them in the process of this, but one in particular,
because I really liked the way it was phrased.
There's a little introduction
that I'm not going to read, and then it says,
I've been trying to figure out what actually ended it.
I've been looking at timelines and reading history books,
and it really seems like they're trying to say
that in 1992 there was a referendum of all white people
and they just voted to end apartheid.
To quote you, that seems super unlikely.
So my question is, what actually ended apartheid?
Got it. Smart.
Fill your thoughts with battles fought, lessons taught.
Makes sense.
Okay, so we're going to go through a timeline,
because that is how history is taught in the United States.
It's either framed as in the 90s,
white people just had a change of heart and voted and kumbaya.
Or it's taught Nelson Mandela did it.
That's the framing.
I don't like either one of those framings.
Mandela was an incredibly important figure
in ending apartheid.
However, teaching it as if it was all linked to him
is great man theory.
And any time you're talking about a major shift in society
and linking it to one person,
well, it's pretty much always wrong.
Mandela happens to be a great example
in proving that it's always wrong.
What's he best known for?
I mean, what's the one thing everybody knows about him?
He spent a whole lot of time in a cell.
It's really hard to change an entire country
from inside a cell.
There were probably other people involved.
Linking it all to Mandela is, it's very short-sighted.
It's very superficial.
That's not to say he wasn't important.
As I go through this timeline,
understand he's getting arrested like constantly.
I think he got charged with treason like twice.
He definitely did more than his fair share.
But linking everything to him, it just doesn't.
It's not accurate.
Okay.
So, 1948, the National Party formally begins apartheid.
I say formally because there were segregationist policies
before that.
But it's about to become really systematic.
1949, a year later, mixed marriages are banned.
1950 is the Population Registration Act.
Papers, please.
Everybody's getting categorized by skin tone.
You're either white, black, or colored.
Colored did not mean the same thing it meant in the US.
It meant like Indians.
Residential segregation begins,
and the Communist Party is banned.
This is more important than you would think
when you're looking at the scope of apartheid.
Doesn't seem like it's related, but it really is.
It's important.
So, just remember that part.
From the very beginning, that was something that was done.
South Africa was anti-communist.
Okay.
1953, the Bantu Education Act.
Education becomes segregated.
Black people are taught to be good workers
because under this system,
that's all they're ever going to be allowed to be.
1959, homelands were created for major black populations.
It was presented as, you know,
a way to give them some kind of autonomy.
They were going to be separate.
If you're in the US, just think separate but equal.
I don't know that those terms were used,
but same general rationalization.
I think they were called Bantu stands.
Now, as I'm going through this timeline,
there are protests happening constantly,
like normal protests like we have today.
1960, Sharpeville at one of these protests.
Well, everybody's supposed to carry papers, right?
Some didn't.
It started a riot.
There was a security clampdown.
About 70, I want to say 69 is the exact number,
but don't quote me.
People were killed.
The ANC gets banned.
This is when international pressure
really starts to kick up, but not too much
because South Africa is strategically important.
They're valuable.
They're anti-communist, and South Africa
gets seen as a staging area to fight the communists
during the Cold War in Africa.
So because they are strategically valuable,
they get a lot of leeway.
1961, South Africa leaves the Commonwealth.
Embargoes start.
They're excluded from the Olympics.
Now, in 1970, the Bantu Homeland Citizenship Act,
those homelands they made, well, now it's forced,
forcible removal.
People are being sent there.
1974, they're kicked out of the UN.
Again, there are protests going on this entire time,
and there are loosely coordinated economic efforts
as well.
And then 1976, there's a real big uptick in violence.
It starts with 600 people getting
killed in clashes over a very short period,
and then there's a low level of violence
that just kind of stays right underneath the surface.
It prompts a security clampdown.
And as you know, that security clampdown
strengthens the resistance, because innocent people
get caught up in it.
Now, in 1984, it is time to kick in the township rebellion.
Black communities, well, they just
decide they're going to be ungovernable.
They're not going to listen.
They're going to use the power that they have
to try to address their concerns.
They are just going to defeat the government by ignoring it.
It triggered a state of emergency.
That authority that government has,
if people don't believe in it, well, it's really shaky.
In 85, musicians get together, and they're like,
we're not playing in South Africa.
We're not doing anything with South Africa.
You have a lot of celebrity power
start to come out of the woodwork and condemn apartheid.
Sports teams do the same thing.
The township rebellions, they go on a while.
And then in 82, or in 89, sorry, DeKlerk
comes to power, new president.
And the ANC, it's no longer banned.
Desegregation starts to begin.
Mandela's released.
They meet.
They start working things out.
They come up with a plan.
In 92, there's a referendum.
And yes, it was all white voters.
And they voted to end apartheid by a huge margin.
It was a landslide.
It was a landslide.
I want to say two to one.
But it was already kind of over.
And everybody knew it.
Everybody knew it was ending.
And the government knew it.
Because the Cold War was also over.
They were no longer as strategically important.
They didn't have the same leeway.
Apartheid didn't get the international pressure
it should have because of the Cold War.
The South African government was allowed to get away
with a lot more than it normally would because it
was strategically important.
It was in a region that had a lot of conflict.
So the world powers kind of looked the other way.
When that situation wasn't occurring anymore,
when they weren't as needed, the South African government
understood that they didn't have that leeway.
So it was beneficial to them to change.
Now, you go through this timeline,
you understand there were protests.
There were little pockets that became ungovernable.
There was widespread discontent.
But there's no moment.
You're looking for a moment that ended apartheid.
There isn't one.
Not really, because it's not a moment, it's a movement.
It's a bunch of little steps.
And it's a diversity of tactics that brought this about.
When you are looking for information
on societal change in other countries,
and it's history, not current events,
because if you're looking at current,
if you look to this place for current events,
you're going to get some pretty biased stuff.
But State Department, the State Department
and their archives, because while their current stuff
is written to further US foreign policy,
their archive stuff, their analysis of stuff
that happened in the past, it has to be accurate,
because the people in State Department,
they're going to use it, need accurate information.
So what does State Department say?
Said it was a combination of things, internal unrest,
weakening white commitment, international security,
internal unrest, weakening white commitment,
international and cultural sanctions,
and the economic issues associated with those sanctions,
and the end of the Cold War.
It was a diversity of tactics that brought it about,
but none of them would be effective
until South Africa wasn't strategically important.
I mean, they could have been in theory,
but it would have taken a lot more.
So if there is a lesson,
and that seems to be what you're looking for,
it's that it's not one thing.
It's almost never one thing.
When you're talking about societal change,
it is normally a perfect storm of people and events
and geopolitical situations that bring about the ability
for that shift to happen.
That's what happened here.
That's the deep lesson, is that if you want real change,
it's going to take a diversity of tactics.
You're going to have to think about it
from the economic standpoint, not just right and wrong.
You're going to have to think about it
from the geopolitical standpoint.
It's not always a moral issue,
because you're talking about power.
And that is very rarely motivated by morality.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}