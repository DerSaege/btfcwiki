---
title: Let's talk about pipeline and supply chain concerns....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MTCx4t2EA8Q) |
| Published | 2021/05/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses questions about the pipeline due to previous video "Let's Talk About What Happens If The Trucks Stop".
- Acknowledges the critical infrastructure of the pipeline with contingency plans in place.
- Compares the current situation to a previous instance where plans were not followed during a crisis.
- Expresses confidence in the Biden administration's actions and implementation of contingency plans.
- Explains the relaxation of safety protocols for truckers to haul fuel and mitigate the pipeline shutdown's impact.
- Mentions plans to maintain refinery operations by bringing in tanker ships for storage.
- Anticipates minimal supply chain disruptions if the pipeline resumes operation by the end of the week.
- Assures that although there may be gas price hikes and shortages, it won't lead to a complete halt in the trucking industry.
- Emphasizes the importance of monitoring the situation but advises against panicking.
- Notes the possibility of major issues if the pipeline disruption persists.

### Quotes

- "The Biden administration apparently reads the Oh No book."
- "There's going to be higher gas prices. There will be shortages in some areas."
- "So there are plans, and it does appear that the Biden administration has read the Oh No book and is starting to enact them."

### Oneliner

Beau addresses pipeline concerns, assures contingency plans are in place, and advises against panic amid potential gas price hikes and shortages.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Monitor the situation for updates and changes (implied).
- Be mindful of truckers' workload and give them space if encountered in impacted areas (implied).

### Whats missing in summary

The full video provides a detailed breakdown of the pipeline situation and the actions taken to mitigate potential disruptions.

### Tags

#Pipeline #ContingencyPlans #GasPrices #SupplyChain #BidenAdministration


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the pipeline.
We're going to do this primarily because we're getting a whole bunch of questions about it,
mainly from people who watched the video,
Let's Talk About What Happens If The Trucks Stop.
If you haven't seen that video, I'm not sure I would suggest watching it right now.
But we'll get to that.
I'm going to start off by saying a sentence that I kind of told myself I'd never say again
because the last time I said it, the world stopped.
We have plans on the books for this.
The pipeline is critical infrastructure.
We have contingency plans to deal with it.
Now the last time I said that sentence, there were some sick people in China,
and the administration at the time did not follow the plans.
I feel comfortable saying it this time for two reasons.
One, I'm feeling lucky, and two, I waited to see if they were actually going to start doing it first,
and they are.
The Biden administration apparently reads the Oh No book,
and they've already started enacting some of the contingency plans.
Unlike last year, this isn't something I was personally trained on.
This is stuff I picked up in conversation, so I don't know the whole plan.
But I know the first step was to relax safety protocol for over-the-road truckers that haul fuel,
and that has already happened.
The idea is that the big rigs will fill in the gaps and mitigate some of the loss
from the pipeline being shut down.
Understand, they won't be able to carry the same load, not even close.
If it goes on for too long, they will relax another set that will allow truckers
who don't have the certification to pull tankers to do it to help fill in even more gaps.
Another thing that they will do is keep the refineries running
so there's not a second break in the supply chain,
and they'll do this by bringing in tanker ships and parking them off coast
and filling them up and using them as storage.
That is also already being done.
I don't know that the ships are there yet, but they've been ordered already.
If it continues to have issues, you'll see those ships move to other locations
to offload what they have stored.
I don't think it's going to get to that point.
The information we have, what they're saying is that this pipeline will be back up
and running by the end of the week.
Let's say it takes till Monday, okay?
Will there be breaks in the supply chain?
Yes, but it won't be like the video describes.
Keep in mind, right now the trucks are still rolling and there's nothing on the horizon
that would stop them from rolling.
So that isn't really a concern.
When you're talking about breaks in the supply chain from this,
think more like toilet paper last year than Mad Max.
The truckers can get gas outside of the impacted area
that are fed by other pipelines or other means and drive in.
So it's not like the trucking industry has ground to a halt.
It's just been interrupted.
It's been slowed.
So there will be problems, but not to the scale described in that video
because this isn't a complete halting of the trucking industry.
Assuming that the pipeline gets back up and running pretty quickly,
this is going to be annoying.
There's going to be higher gas prices.
There will be shortages in some areas and not just those caused by panic buying,
which is what's causing them now.
So it's a concern.
It's something to watch and be aware of, but we don't need to hit the panic button just
yet.
Now if this was to go on for too long, yeah, you would start to have major issues.
But there are a lot of contingency plans.
I think there was even something to do with railways, but I don't really remember.
I think there was something to do with bringing it in via rail.
So there are plans, and it does appear that the Biden administration has read the Oh No
book and is starting to enact them.
And when it comes to relaxing the safety regulations, people who are hauling a whole bunch of gasoline,
they know what they're hauling.
They're not going to be taking a lot of risks.
I don't know that that's a major concern.
Basically, what it's allowing is for them to work overtime.
So if you see them out in the impacted areas, give them a little bit of space just to understand
they're probably very overworked right now.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}