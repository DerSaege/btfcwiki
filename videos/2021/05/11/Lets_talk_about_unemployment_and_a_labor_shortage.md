---
title: Let's talk about unemployment and a labor shortage....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7SRjXmgqLfM) |
| Published | 2021/05/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Companies blaming unemployment for labor shortage, refusing to raise wages.
- Unemployment is survival, not a cash cow; wages should be higher than unemployment benefits.
- Companies forgetting the industrialist rule: pay highest wages possible.
- American companies neglecting to pay high wages to attract labor.
- Labor is vital for creating capital and being a capitalist.
- Companies pressuring governments to reduce unemployment instead of raising wages.
- Working class faces the carrot (hope for social mobility) and the stick (threat of homelessness).
- Companies failing to grasp basic economics: supply and demand.
- Shortage of labor should lead to increased wages.
- Budgeting advice like "pull yourself up by your bootstraps" fails to address real issues.

### Quotes

- "Unemployment is not really a cash cow that's pretty much the bare minimum you can survive on."
- "You have to pay for it. It's really that simple."
- "Maybe it's just a budgeting issue. Y'all shouldn't go to Starbucks so much."
- "If you want it, you have to pay for it."
- "It's really that simple. Anyway, it's just a thought."

### Oneliner

Companies blame unemployment for labor shortage, refusing to pay higher wages, neglecting basic economics and the industrialist rule.

### Audience

Working Class, Employers, Government

### On-the-ground actions from transcript

- Raise wages to attract labor (implied)
- Advocate for fair wages in your workplace (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the challenges of unemployment, wages, and labor shortages in the current economic landscape.

### Tags

#Unemployment #Wages #LaborShortage #Economics #WorkingClass


## Transcript
Well howdy there internet people it's Beau again. So today we are going to talk about unemployment,
wages, and a labor shortage. You have companies all around the country right now saying hey we
can't get people to come back to work because they have unemployment and we're not gonna
pay more than that. Unemployment is not really a cash cow that's pretty much the bare minimum you
can survive on. If your wages are lower than that perhaps you're the problem. If you can't attract
workers back to work to earn more than they are making on unemployment you have forgotten
the number one rule of being an industrialist. There is one rule for the industrialist and that
is make the best quality of goods possible at the lowest cost possible, paying the highest wages
possible. Seems like a lot of American companies have forgotten that last line.
That seems to be the the one that's important. That's how you get that labor that you need
to make the goods that create the capital that allows you to call yourself a capitalist.
That labor is important. It normally comes up front. Instead of raising wages,
these companies are putting pressure on governments to reduce unemployment so they can use the stick
side of our society. You have the carrot and the stick. If you are in the working class,
the carrot is if you work really really hard maybe one day you'll make it out of it. And look,
this one in a million chance over here they got some social mobility and the stick is if you don't
do it you're going to end up homeless and in the street. It doesn't sound like these companies
understand the basic laws of economics, supply and demand. You know when goods are short,
they just tell us well they're going to cost more. There's a shortage of labor. It's going to cost
more. You need to raise wages. It's really that simple. It is basic economics.
Maybe it's just a budgeting issue. Y'all shouldn't go to Starbucks so much. Pull yourself
up by your bootstraps. I hear that that's the standard advice given to people who can't make
ends meet. Typically by those who've never had to deal with it before. And since a lot of American
companies constantly get bailed out by the government, they have never had to deal with
it before. You know, I've been to a lot of American companies. I've been to a lot of
they have never had to deal with it before. You have a shortage of something you need.
It is in high demand and there is low supply. If you want it, you have to pay for it. It's
really that simple. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}