---
title: Let's talk about why Republicans don't want a commission....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-ihqXyttvvY) |
| Published | 2021/05/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans in Congress are resisting a commission to uncover the truth about the events of January 6th.
- A Reuters Ipsos poll showed that 61% of Americans believe Trump is at least somewhat responsible for January 6th, with only 28% of Republicans agreeing.
- Despite a quarter of Republicans blaming Trump, the GOP leadership seems reluctant to set the record straight.
- Many Republicans believe the false narrative that left-wingers were behind the events on January 6th.
- The GOP's refusal to support the commission may stem from fear of their constituents learning the truth and realizing they've been misled.
- The party might be concerned about their lies being exposed and their credibility being damaged.
- The reluctance to uncover the truth could be linked to protecting the current leadership's image and preventing constituents from changing their views.
- Beau suggests that the GOP's resistance to the commission could be due to the risk of their supporters discovering what actually occurred on January 6th.

### Quotes

- "Because right now, half of them believe something that isn't true."
- "The reality is the Republican Party, well they can't handle the truth."
- "Unless of course it is what happened and that's what they're worried about."
- "The reason they don't want a commission is because they can't risk their constituents finding out what actually happened."
- "No matter what excuse they throw out, the reason they don't want to commission is because they can't risk their constituents finding out what actually happened."

### Oneliner

Republicans in Congress resist a commission to uncover the truth about January 6th, potentially fearing exposure of lies that may damage their credibility with constituents.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Contact your representatives to express support for a commission to uncover the truth about January 6th (suggested).
- Spread awareness about the importance of transparency and accountability in political leadership (implied).

### Whats missing in summary

Understanding the potential impact of misinformation and lies on political credibility and public trust. 

### Tags

#Republicans #January6th #PoliticalAccountability #Transparency #Truth


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about why Republicans in Congress are so reluctant to have that
commission to get to the bottom of what happened that day.
Because they're pushing back against it pretty hard.
Doesn't seem like they really want it to happen.
Now I have a theory on why, but before we get into it I want to give you some statistics.
According to a Reuters Ipsos poll from last month, 61% of all Americans believe that Trump
is at least somewhat to blame for the events of January 6th.
28% of Republicans believe that.
One out of four of their own party believe that he's somewhat to blame for what happened
at the Capitol.
You would think that if a quarter of your own party believed that the former president
was responsible for that, you'd want to set the record straight.
You'd want to get the information out.
Especially when by that same poll only about 30% of independents have a positive view of
the man who is now calling the shots in the Republican Party.
I mean it seems wild to think that one in four Republicans believes that the current
leadership of the GOP is taking marching orders from somebody who put them in danger.
That's wild.
I mean it seems like they would want to get to the bottom of it and show that that isn't
what happened.
Unless of course it is what happened and that's what they're worried about.
And what they're really worried about is their own lies coming undone.
Because by that same poll, 55% of Republicans believe that the activities that happened
on the 6th were the work of left-wingers out to make the former president look bad.
A claim that is completely baseless and has been refuted by like everybody including the
people who were there.
The reality is the Republican Party, well they can't handle the truth.
Because if the number of people who believes that it was left-wingers, if that went down,
well that one in four number, that would probably go up.
Might get to one in three or one in two.
And that would just make the current leadership look like liars.
Look weak.
Look incapable.
No matter what excuse they throw out, the reason they don't want to commission is because
they can't risk their constituents finding out what actually happened.
Because right now, half of them believe something that isn't true.
And if they had the truth, they may have a different view of the former president and
those who continue to do his bidding.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}