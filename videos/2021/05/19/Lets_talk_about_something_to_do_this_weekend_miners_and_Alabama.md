---
title: Let's talk about something to do this weekend, miners, and Alabama....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=h3jRen93miM) |
| Published | 2021/05/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Miners in Alabama went on strike after facing deteriorating working conditions and mistreatment from management.
- The United Mine Workers of America in Brookwood, Alabama are leading the strike with over 1100 miners participating.
- The striking miners are in need of a strike pantry to support their families with food.
- The Valley Labor Report is hosting a livestream fundraiser from Friday to Sunday to support the striking miners.
- The Alabama Strike Fest on May 22 will feature music, barbecue, and comedians to raise funds for the miners.
- The event includes comedian Drew Morgan and aims to bring people together to support the cause.
- Donations are encouraged, with a suggested amount of $20, and it's free for UMWA members.
- Even if unable to attend in person, there's a link to donate directly to support the miners.
- Supporting organized labor fights like this can benefit everyone by raising wages in the area.
- Showing support for the miners is not just about being neighborly; it also serves self-interest and can have a wider positive impact.

### Quotes

- "They just want a fair shake."
- "These fights that organized labor engages in, they really do benefit everybody."
- "Showing a little bit of support here is not only the neighborly thing to do."

### Oneliner

Miners in Alabama strike for fair conditions, needing support with a strike pantry and fundraiser, showing solidarity benefits all.

### Audience

Supporters of fair labor practices

### On-the-ground actions from transcript
- Donate to support the striking miners (suggested)
- Attend the Alabama Strike Fest fundraiser event (exemplified)

### Whats missing in summary

The emotional impact of supporting the miners and the broader community benefits from raising awareness and providing aid.

### Tags

#LaborRights #SupportMiners #Solidarity #Fundraiser #FairWages


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about minors and barbecue.
Something y'all can do this weekend if y'all want.
Right now you have a whole bunch of talking heads
running around talking about how
Americans don't want to work, they'd rather just sit home.
There are some minors in Alabama.
Back in 2016, the mine they worked at,
well it went into bankruptcy.
So they made a bunch of concessions
to pay cuts to help keep that mine afloat
so they could work.
Now it's 2021 and they're trying to get better paying
conditions because to hear the miners tell it,
management's just gotten even worse,
penalizing them for getting sick, all kinds of stuff.
So they're on strike, United Mine Workers of America
in Brookwood, Alabama.
They're on strike.
They've been on strike for more than a month,
well over a month.
It's 1,100 miners.
When you're talking about 1,100 people on strike,
that's a big deal.
That is a big deal.
One of the things they need is a strike pantry.
That's a place where the miners' family can get food.
That's something that's kind of needed.
So the Valley Labor Report, starting on Friday,
running through Sunday, is going to have a live stream.
It'll be available on YouTube and over on Twitch.
And they will be raising money to help them out.
Now on Saturday, May 22, starting at noon,
Alabama Strike Fest starts.
There will be music, hundreds of pounds of barbecue,
comedians.
Drew Morgan will be there.
And it's going to be a great night.
And he'll explain to a lot of y'all
why y'all are probably afraid of the wrong Southern accent.
The suggested donation is $20.
If you are a member of UMWA, it's free.
If you cannot make it to the live stream,
if you cannot get there in person,
there will be a link down below so you can donate directly.
Sadly, I can't make it to the in-person event.
I've tried everything I could to adjust some stuff.
But it's just not going to happen.
And I feel a little guilty about it, to be honest.
But this is something, when we talk about working class
people looking out for each other,
this is a moment to do that.
This is a time when you can make a real impact.
Even if you are on the other side of the country,
there's a way to help.
These fights that organized labor engages in,
they really do benefit everybody.
Because when wages get raised for the unions,
it incentivizes other companies to raise wages in the area.
It really does help everybody.
Showing a little bit of support here
is not only the neighborly thing to do.
I mean, there's also an element of self-interest.
So right now, with everything going on,
you have a bunch of miners, people
who engage in some pretty hard work, who
are more than willing to work.
They just want a fair shake.
And I'm pretty sure that everybody in the country
can relate to that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}