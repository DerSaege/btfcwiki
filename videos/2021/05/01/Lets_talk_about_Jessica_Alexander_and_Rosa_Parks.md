---
title: Let's talk about Jessica Alexander and Rosa Parks....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ri35ga4nWIE) |
| Published | 2021/05/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Jessica Alexander from Temecula, California, made a widely ridiculed and unjustifiable comparison of her anti-mask advocacy to Rosa Parks.
- Jessica Alexander also got a key fact wrong about Rosa Parks' story, claiming Parks moved to the front of the bus because she knew it wasn't lawful, when in reality, it was the law that enforced segregation.
- Rosa Parks was seated in the first come, first serve section of the bus, not in the white section reserved for whites.
- Parks' refusal to give up her seat was an act of resistance against the unjust segregation laws, not just mere fatigue or happenstance.
- By placing Parks in the white section of the bus, the true injustice and inequalities of segregation are downplayed and sanitized.
- The comparison between Jessica Alexander's anti-mask advocacy and Rosa Parks' civil rights activism is deemed ridiculous and nonsensical by Beau.
- Beau stresses the importance of getting Rosa Parks' story right and giving her the proper credit she deserves.
- The attempt to equate wearing masks in modern times to the struggles of individuals like Rosa Parks during segregation is dismissed as a false comparison.
- Beau calls out the trend of minimizing historical atrocities while exaggerating contemporary inconveniences as the ultimate victimization.
- Beau encourages viewers to watch a video detailing overlooked aspects of Rosa Parks' story for a more comprehensive understanding.

### Quotes

- "That was the whole problem. You know, the segregation thing, that was an issue."
- "She wasn't somebody who just happened to be in the right place at the right time."
- "When you say it out loud, it doesn't make any sense because it doesn't make any sense."
- "Something being asked to wear a mask to living in Montgomery, Alabama at that time, that's not the same."
- "Please get the story right because this is somebody who I truly believe is never given the proper credit that they deserve."

### Oneliner

Jessica Alexander's comparison of her anti-mask advocacy to Rosa Parks is not only factually incorrect but also dismissive of the true injustices faced during segregation, a comparison Beau deems ridiculous.

### Audience

Community members, activists

### On-the-ground actions from transcript

- Watch the video detailing overlooked aspects of Rosa Parks' story (suggested)
- Ensure accurate and respectful retelling of historical figures' stories (implied)

### Whats missing in summary

Deeper insights into the importance of historical accuracy and honoring the legacies of civil rights icons like Rosa Parks could be gained from watching the full transcript.

### Tags

#RosaParks #CivilRights #HistoricalAccuracy #SocialJustice #CommunityStandards


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about truth and fiction and Jessica Alexander and Temecula
California and Rosa Parks.
If you don't know a council person out there, they made a comparison that has been, well
let's just say widely ridiculed and justifiably so.
She compared her advocacy, her anti-mask advocacy, to Rosa Parks.
And we'll talk about that comparison in a minute.
But she also got something factually wrong and it's an important piece of Rosa Parks'
story.
Her story gets crammed into a narrative constantly and when people tell her story it never does
her justice.
This even more so.
Here's the quote.
Look at Rosa Parks.
She was accommodated on the back of the bus, but she finally took a stand and moved to
the front because she knew that wasn't lawful, it wasn't truth.
Okay, first the whole problem is that it was lawful.
The law did that.
That was the whole problem.
You know, the segregation thing, that was an issue.
Aside from that, it wasn't truth.
This statement isn't true and it really takes away from Rosa Parks' story.
Rosa Parks was not in the white section of the bus.
The first ten rows of the buses, those were reserved for whites.
The last ten were theoretically reserved for blacks.
The middle sixteen, they were first come, first serve.
She was in row eleven.
She was in the first come, first serve section.
It doesn't seem like this would be a big deal, but it is.
See white people liked to frame segregation as separate but equal.
She was following the unjust law.
She was just complying and she still had an injustice visited upon her.
It wasn't equal.
That was the problem.
And this highlighted it.
This showed it, made it clear as if there weren't other examples.
See she was in row eleven and the bus driver told four people in row eleven to move.
Three of them did.
She didn't.
There was one white passenger waiting for a seat.
They had three seats to choose from, but they still wanted her to move.
Wasn't really about seating at that point.
Better obey.
Just comply.
Do what you're told.
Know your place.
I think the actual quote is better make it easy on yourself.
Make it light on yourself.
Was about forcing compliance and it happened not in the white section, but in the first
come, first serve section.
It changes the dynamic when you move her to the white section of the bus.
Because then white folk, well, you know, everything was fine.
It was just separate what was the big deal.
It takes away from it.
She wasn't in the white section of the bus.
Her story always gets cast as, well, she was just tired.
She was a seamstress and just tired and just didn't want to get up and move.
Yeah, that's not at all reality.
She wasn't somebody who just happened to be in the right place at the right time.
Rosa Parks fought injustice for years.
She traveled to another state to learn how to be more effective.
Casting her as somebody who was just didn't want to get up, that takes away from it.
When her story is told, it never does her justice.
Moving her to the white section of the bus, the same thing.
It sanitizes the injustices that occurred under segregation.
Now as far as this comparison, yeah, I can totally see how being a white woman in 2021
in California with enough power to be elected to political office is completely comparable
to being a black woman existing under segregation in Montgomery, Alabama.
When you say it out loud, it doesn't make any sense because it doesn't make any sense.
This comparison is ridiculous.
I am fairly certain that no matter how hard Jessica Alexander advocates her anti-mask
policies, I'm pretty sure that nobody with hoods is going to show up in the middle of
the night and drag her out of her home never to be seen again.
There has, there's a real common thing going on lately where people simultaneously take
some of the most horrid events in history and make them seem a little bit better and
then dramatize everything that is occurring now and making it seem like it is the ultimate
victimization.
Something being asked to wear a mask to living in Montgomery, Alabama at that time, that's
not the same.
It's not the same league.
It's not the same sport.
They're very different.
That's not a comparison that should be made.
But if you are going to talk about Rosa Parks, please get the story right because this is
somebody who I truly believe is never given the proper credit that they deserve.
I have a video that I will put down below that details some of the pieces that often
get overlooked.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}