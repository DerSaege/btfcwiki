---
title: Let's talk about the Republican plan for 2022 and beyond....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6V4J21-DzV8) |
| Published | 2021/05/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican Party's plan is to pivot away from Trump for 2024 due to his failing support and decreased social media mentions.
- McConnell, although disliked by Beau, is recognized as politically savvy and a long-time Capitol Hill player.
- The GOP leadership seems to be letting baseless claims about the election persist and allowing upstarts in the party to do as they please until 2022.
- If the upstarts win in 2022, they will present a more refined, polished version of Trump for the party to rally behind.
- If the upstarts lose in 2022, it becomes easier for the GOP to pivot away from Trump.
- The significance of the 2022 midterms lies in potentially delivering a resounding defeat to Trumpism to send a message to the Republican leadership.
- The hope is to weed out Trump's momentum through the midterms and introduce a more polished version of Trump with similar policies but less public opposition.
- Beau expresses concern about a polished version of Trump getting policies enacted with less opposition and fears the GOP's strategy of courting high-profile individuals for this purpose.
- The midterms are emphasized as critical in combating Trump's authoritarian brand, which could outlast his political career if not addressed now.

### Quotes

- "2022 becomes important. While the GOP may be flipping a coin, the leadership may be flipping a coin as to whether or not they win."
- "You want to get rid of Trumpism? They have to get that resounding defeat, that landslide that Biden needed to completely wipe out Trumpism."
- "The midterms, which most times people tend to forget about, especially if their party is in power, they've taken on a new significance in the battle against Trump's particular brand of authoritarianism."

### Oneliner

Republican Party plans to pivot away from Trump for 2024, grooming a more polished version, making 2022 midterms pivotal in defeating Trumpism and combating authoritarianism.

### Audience

Political analysts, activists

### On-the-ground actions from transcript

- Organize voter registration drives and encourage voter turnout for the 2022 midterms (implied)
- Engage in local political organizing and support candidates who advocate against Trumpism and authoritarianism (implied)

### Whats missing in summary

Detailed analysis of potential strategies to counter the GOP's pivot away from Trump and the importance of grassroots movements in influencing political outcomes.

### Tags

#RepublicanParty #Trump #Midterms #Authoritarianism #PoliticalStrategy


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about
the Republican Party's plan, their pivot,
and how they plan to do it
and the significance of the midterms.
Because I think, like most people,
I've been sitting around trying to figure out
what the Republican leadership is doing.
They know they can't win with Trump nationally in 2024.
They couldn't win with him before,
and his support is failing.
His social media mentions are down 90%.
A lot of his supporters who were in the streets
are now publicly speaking against him.
Those supporters in the streets
were the people who energized the online base.
They can't win with him in 2024, and they know that.
But if you look at them right now,
it doesn't seem like they know that.
The thing is, if you've watched this channel for a while,
you know I don't like McConnell at all,
but I do not underestimate him.
He has made it up on Capitol Hill a very long time.
That is a very politically savvy person.
So I've been trying to figure out what they're doing,
because they are leaning into the baseless claims
about the 6th and the election,
and they're allowing it to go on.
I couldn't figure it out until I was talking with a friend,
and he was talking about how important it is
to get rid of Greene, but how hard it's gonna be.
And it all started falling into place.
The Republican leadership is going to let the upstarts
in the Republican Party do whatever they want until 2022.
If they win, well, then they'll present them
with a new person to rally behind,
a more refined, a more polished version of Trump,
because Trump couldn't win, but they did,
and they'll sell it to them like that.
And if they lose, well, then it becomes very easy
to pivot away from Trump.
So where does that leave the rest of the country?
2022 becomes important.
While the GOP may be flipping a coin,
the leadership may be flipping a coin
as to whether or not they win.
For everybody else, it matters.
You want to get rid of Trumpism?
They have to get that resounding defeat,
that landslide that Biden needed
to completely wipe out Trumpism,
the landslide that didn't happen.
That needs to happen in the midterms.
Needs to send a message to the Republican leadership
that even that polished version of Trump won't win.
And then hopefully, maybe you just get
a semi-normal conservative,
what passes for a normal conservative today,
but they are already weeding out their political opposition,
people like Cheney.
When you look at it through a very cynical lens,
the kind of lens that politically savvy people have,
it all kind of makes sense.
They're hoping to ride Trump's momentum
through the midterms
and then use it to present their own version of Trump,
somebody who doesn't say the quiet part out loud as much.
And to be honest, I've said it before,
that's one of my biggest fears,
because somebody who is more polished,
who doesn't tweet everything they're going to do,
that doesn't telegraph everything,
they can get a lot of Trump's policies enacted
without as much opposition
if they're just a little bit more presentable.
And I think that's what McConnell
and the GOP leadership are looking for.
I think that's why they are courting certain high-profile
people in the Republican Party
and trying to gear them up for it.
So the midterms, which most times people tend to forget about,
especially if their party is in power,
they've taken on a new significance
in the battle against Trump's particular brand
of authoritarianism,
because that brand can outlive Trump's political career.
And it looks like the GOP leadership wants it to.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}