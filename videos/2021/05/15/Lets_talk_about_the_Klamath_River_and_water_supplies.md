---
title: Let's talk about the Klamath River and water supplies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=t0lXuCBkxlg) |
| Published | 2021/05/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Klamath River basin in Oregon and northern California is facing a drought, impacting people along the river and in reclamation programs.
- Farmers are being told there won't be water for irrigation, leading to a potential conflict with endangered salmon species.
- Low water levels in the river are causing bacteria to flourish, already impacting 97% of tested salmon, especially juveniles.
- The upcoming national news coverage might frame the issue as "farmers versus fish," overlooking the Yurok native group dependent on salmon for their way of life.
- The Yurok tribe signed a treaty with the U.S. in the 1850s to maintain their fishing lifestyle within a specific geographic area.
- Despite having good lawyers, the Yurok might be forgotten in national coverage due to powerful interests at play.
- It's vital to recognize and support those on the ground who lack influence over decisions impacting their livelihoods.
- Confined groups like the Yurok have a moral right to maintain their traditional lifestyle without external decisions lowering their standard of living.
- The drought in the Klamath River basin raises questions about water rights and conflicting claims between longstanding farmers and indigenous groups.
- More disputes over water rights are expected with increasing droughts, but it's crucial not to overlook the local communities most impacted.

### Quotes

- "Farmers versus fish."
- "Don't forget that there are people on the ground in these areas that are going to be impacted that have no control over what happens."
- "But who has more of a claim – the Yurok or people who showed up during the gold rush?"
- "There will be more and more disputes over water rights."
- "They're the people that may need the most support."

### Oneliner

The Klamath River basin faces a drought crisis, pitting farmers against endangered salmon and overlooking the indigenous Yurok group's vital connection to the river.

### Audience

Environmental activists, policymakers, community organizers

### On-the-ground actions from transcript

- Support the Yurok native group in maintaining their traditional fishing lifestyle (implied).
- Advocate for equitable water distribution to all affected groups in the Klamath River basin (implied).
  
### Whats missing in summary

The emotional impact on local communities and the urgency of addressing water rights disputes and supporting marginalized groups like the Yurok tribe are key takeaways from the full transcript.

### Tags

#KlamathRiver #DroughtCrisis #WaterRights #IndigenousRights #CommunitySupport #EnvironmentalJustice


## Transcript
well howdy there internet people it's Beau again so today we're going to talk about the
Klamath River basin right um it's a basin along Oregon and northern California there's a river
and there's a drought because of that people along the river or people who are part of
reclamation programs or whatever they're having issues the uh one of the main issues is farmers
basically being told there's not going to be water for irrigation and that's an issue
and they're definitely going to be one of the groups that gets attention when this hits
national news I'm pretty sure this will the other side to it will be an endangered species salmon
and I have a feeling I don't know this for certain but I have a feeling that when it
hits national news that's how it's going to be framed farmers versus fish
who who has uh more claim to to that water
because with low water levels a bacteria tends to flourish in fact there was testing done of the
salmon already that shows like 97 percent of them already have it and it's impacting the juvenile
populations a lot farmers versus fish but see there's another group that's going to be impacted
by this and just because the way the news is I have a feeling when it hits national news they
may not be mentioned very often the Yurok native group along that river and uh they survive their
way of life is heavily dependent on that salmon if the juvenile population goes well that's going
to be bad for a few years that's going to be bad for a few years and they've existed there for
I don't even know I don't even know but I know the U.S. signed a treaty with them
in the 1850s-ish in that range and they were living there then and they kind of promised them
that they'd be able to maintain their way of life that fishing way of life as long as they stayed
in this little geographic area I'd say they'd have a pretty big claim now the Yurok they've got
they've got lawyers from what I understand they actually have good lawyers but I have a feeling
when the national coverage starts to happen well they'll be forgotten about because they typically
are when you're talking about a geographic area and there are powerful groups at play
you have to remember those that are on the ground who may not always have a whole lot of influence
over the decisions it's also important to note that if you're going to take a group of people
and confine them to a geographic area you can't make decisions outside of it that are going to
lower their standard of living impact their way of life when they don't have any control over it
if you're going to confine people and keep them well you can have this little chunk here you have
a moral obligation to make sure they can maintain their way of life and as far as claims go I know
some of those farmers have been there for generations but who has more of a claim the Yurok
or people who showed up during the gold rush these are questions we're going to have to ask
ourselves in the United States a lot that drought is bad and there will be more of them
there are going to be more and more disputes over water rights and it's easy to get caught up
in the fear because you know not having water that's a pretty serious thing
but when the coverage is happening don't forget that there are people on the ground
in these areas that are going to be impacted that have no control over what happens
they're the people that may need the most support
anyway it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}