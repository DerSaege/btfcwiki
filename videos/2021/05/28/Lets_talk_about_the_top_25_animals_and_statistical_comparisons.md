---
title: Let's talk about the top 25 animals and statistical comparisons....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Y-QNwHFGSn8) |
| Published | 2021/05/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic of expectations, perceptions, statistics, and comparisons, using a list of 25 animals ranked from least to most dangerous.
- Asks the audience to think about which animals they expect to be on the list and which they perceive to be more dangerous.
- Presents a comparison of the world's most dangerous animals, revealing surprising statistics like mosquitoes causing 1 million deaths per year.
- Raises questions about the accuracy and implications of such statistics, pointing out flaws in the comparisons made.
- Criticizes the use of statistics without clear criteria for comparison, warning against making decisions based on misleading information.
- Emphasizes the need to scrutinize statistics used to push agendas and make informed decisions.
- Urges the audience to be cautious when interpreting statistics and ensure consistent criteria are applied for accurate comparisons.
- Concludes by reminding viewers to critically analyze information before making decisions.

### Quotes

- "Statistics in the hands of those who want to enact an agenda, they have to be scrutinized pretty closely."
- "When comparisons are made, make sure that the same criteria gets applied to like items throughout it."
- "It's a tool of propaganda. It's a way to push an agenda."
- "You can end up basing your decisions on something that isn't accurate."
- "Y'all have a good day."

### Oneliner

Beau compares statistics on dangerous animals to caution against misleading comparisons and agenda-driven use of data.

### Audience

Statistical analysts, policymakers, researchers.

### On-the-ground actions from transcript

- Scrutinize statistics for accuracy and consistency in comparisons (implied).
- Educate others on the importance of critical analysis when interpreting data (implied).

### Whats missing in summary

The full transcript provides a detailed breakdown of how statistics can be manipulated and misinterpreted, urging viewers to be vigilant against misleading information for informed decision-making.

### Tags

#Statistics #DataAnalysis #AnimalSafety #Propaganda #CriticalThinking


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about expectations,
and perceptions, and statistics, and comparisons of statistics,
and how people can be tricked.
And we're going to talk about animals.
In front of me, I have a list, a composite list
that I made of 25 animals.
And they are ranked from least dangerous to most dangerous.
So before we get into this list, let's go ahead
and measure expectations.
What animals do you expect to be on this list?
Go ahead and get it in your mind.
And then let's talk about perceptions.
Which do you perceive to be more dangerous, an alligator
or a horse, a leopard or a deer, a snail or a scorpion?
Go ahead and get it in your mind.
There are some animals that you perceive to be dangerous
and some that you do not.
OK.
So now let's go through the comparison.
World's most dangerous animals, top 25.
OK.
So name of the animal followed by the number lost each year
on average to it.
Alligators, one.
Bats, two.
Sharks, five.
Wolves, 10.
And I have so many questions about that one.
Horses, 20.
Leopards, 29.
Ants, 30.
Jellyfish, 40.
Bees, 53.
Deer, 130.
Cape Horn buffalo, 200.
Lions, 250.
Elephants, 500.
Hippos, 500.
Crocodiles, 1,000.
Tapeworm, 2,000.
Roundworm, 2,500.
Scorpions, 3,250.
Snails, 10,000.
Assassin bug, 10,000.
Setsea fly, 10,000.
Dogs, 25,000.
Snakes, 50,000.
Humans, 475,000.
Mosquitoes, 1 million.
So a snail is more dangerous than a leopard,
statistically speaking by this list.
If you were to enact policy based on that,
if you were to make decisions based on this comparison,
world's most dangerous animals, if I
was to ask what you'd rather be locked in a room with,
a snail or a leopard, which would you choose?
Based on this, you would choose the leopard.
That doesn't really make sense, though, does it?
Why are mosquitoes on the list?
The number one.
Is it the mosquito bite that's actually lethal?
No, of course not.
It's irritating.
But mosquitoes spread disease, right?
So it makes the list.
There's a lot of stuff on here like that, setsea flies.
They spread sleeping sickness.
Snails are on there because freshwater snails,
they release something that makes people sick.
Ants, is it really the ant bite that's fatal?
Or is it anaphylaxis?
And these seem like silly questions, but they're not.
Because a comparison is being made here, right?
Humans are on it at 475,000.
The thing is, that's only counting intentional acts.
Doesn't count the diseases we give to each other.
Why is deer on it?
Are deer inherently dangerous?
Do you go up near a deer and expect it to attack you?
Or is it on there because of the car accidents deer cause?
Why aren't our car accidents listed on this?
Now, to be clear, this is from this composite.
I added a couple items to not unfairly pick
on a single outlet because they all do this.
But this is from a major outlet.
When the comparison being made is overly broad,
most dangerous animals.
And dangerous isn't defined.
You don't actually know what is being
compared via those statistics.
You can't really use it to enact policy.
It's a tool of propaganda.
It's a way to push an agenda.
We know that this set is not really accurate.
It's not.
But it seems like it would be, as long as you
don't look too close, as long as you
don't think about it too much.
When you are using statistics to make a comparison,
make sure that you know the criteria for each item being
compared and that it's the same.
Because if it's not, you could end up
with something like this.
You can end up basing your decisions
on something that isn't accurate.
This is happening a lot right now in a whole bunch
of different debates.
When comparisons are made, make sure that the same criteria
gets applied to like items throughout it.
Because there are three kinds of lies in the world.
And that's not going to change.
Statistics in the hands of those who want to enact an agenda,
they have to be scrutinized pretty closely.
Because if not, well, you may volunteer
to get locked in a room with a leopard.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}