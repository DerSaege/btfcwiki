# All videos from May, 2021
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2021-05-31: Let's talk about a question of privilege and Cher.... (<a href="https://youtube.com/watch?v=-899grNICzQ">watch</a> || <a href="/videos/2021/05/31/Lets_talk_about_a_question_of_privilege_and_Cher">transcript &amp; editable summary</a>)

Beau receives a message prompting reflection on offensive terms, leading to insights on the Roma people and questioning when white people will receive the same consideration in speech.

</summary>

"If you find out why terms are offensive, you will definitely stop using them."
"When you are being edgy, you risk offending people. If you're going to do that, you better have a point."
"Because nobody wants to risk offending a group with a whole lot of buying power."
"There's perhaps a tendency to blow things that would be less offensive out of proportion."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Receives a message about being mindful of offensive terms and implications.
Learns about the impact of a term he used casually on a specific group, leading to discovering more about the Roma people.
Emphasizes the value of understanding why terms are offensive and the knowledge gained from this process.
Raises a question about when white people will receive the same consideration in avoiding offensive speech.
Explains the different interpretations of the question posed, indicating the ongoing challenge of being edgy and potentially offensive.
References Cher's song "Gypsies, Tramps, and Thieves" as an example of addressing sensitive topics indirectly to navigate censorship.
Mentions how the song was altered to remove potentially offensive terminology towards white people, reflecting on societal dynamics.

Actions:

for social activists, allies,
Engage in respectful communication and learn about the implications of certain terms (suggested)
Share knowledge about marginalized groups to increase understanding and empathy (implied)
</details>
<details>
<summary>
2021-05-30: Let's talk about the future of this channel.... (<a href="https://youtube.com/watch?v=PSeWmF5CHO4">watch</a> || <a href="/videos/2021/05/30/Lets_talk_about_the_future_of_this_channel">transcript &amp; editable summary</a>)

Beau shares plans to enhance the channel's impact, restart community projects, and improve video quality with a new production space.

</summary>

"This channel started as a joke on a whim a couple of years ago. But over time it has turned into a force for good."
"I can't thank y'all enough for facilitating all of this."
"I actually look forward to meeting a whole lot of y'all in person."
"Whatever. Anytime it said, Beau did this, y'all did that."
"We are hopefully going to be able to put out more consistent, higher quality videos."

### AI summary (High error rate! Edit errors on video page)

Channel started as a joke, turned into a force for good with impact in the world.
Plans to set up community networks, film documentaries on the road were put on pause due to the world stopping.
Equipment ready, thanks to Patreon, plans to restart community projects.
Working on setting up a production space due to running behind schedule.
Facing challenges of noise and interruptions while filming at home.
Production space will allow for more consistent, higher quality videos.
Plans to restart interviews, improve live streams, and create more skits and documentary content.
Aim to impact things in the real world and meet viewers in person over the next year.
Expresses gratitude to viewers for their support and involvement in channel's growth.
Looking forward to stepping up and increasing the channel's impact.

Actions:

for content creators, viewers,
Support Beau's channel through Patreon to help fund community projects and endeavors (implied).
</details>
<details>
<summary>
2021-05-29: Let's talk about a second Operation New Life and counter arguments.... (<a href="https://youtube.com/watch?v=6sMEQxLSuhA">watch</a> || <a href="/videos/2021/05/29/Lets_talk_about_a_second_Operation_New_Life_and_counter_arguments">transcript &amp; editable summary</a>)

Beau addresses leaving behind Afghan allies, proposing Operation New Life to bring them to safety, citing past successes and the capability to act now.

</summary>

"We mount an operation to do it, and we will call it Operation New Life."
"The ability to do this is there. That's just the idea that it isn't. That's just fantasy."
"People are going to look to Biden for this. It's not really his call."

### AI summary (High error rate! Edit errors on video page)

Addresses leaving behind people in Afghanistan who helped the US or NATO.
Mentions the visa process backlog and the risks involved in applying for it.
Talks about two counter arguments: moral and practical.
Explains the moral argument against US occupation due to imperialism.
Points out that combatants in conflict may not represent the will of the people.
Stresses that being against imperialism involves self-determination and protecting civilians.
Emphasizes that leaving behind those who helped the West is not just limited to interpreters.
Suggests mounting an operation called Operation New Life to bring these individuals to safety.
Mentions the success of a similar operation in 1975 that brought over 100,000 people to Guam.
Argues that the capability to conduct such an operation exists and it's a matter of will.

Actions:

for advocates for afghan allies,
Mount an operation to evacuate Afghan allies, as proposed by Beau (suggested).
Advocate for Operation New Life to be implemented by the government (suggested).
</details>
<details>
<summary>
2021-05-28: Let's talk about the top 25 animals and statistical comparisons.... (<a href="https://youtube.com/watch?v=Y-QNwHFGSn8">watch</a> || <a href="/videos/2021/05/28/Lets_talk_about_the_top_25_animals_and_statistical_comparisons">transcript &amp; editable summary</a>)

Beau compares statistics on dangerous animals to caution against misleading comparisons and agenda-driven use of data.

</summary>

"Statistics in the hands of those who want to enact an agenda, they have to be scrutinized pretty closely."
"When comparisons are made, make sure that the same criteria gets applied to like items throughout it."
"It's a tool of propaganda. It's a way to push an agenda."
"You can end up basing your decisions on something that isn't accurate."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic of expectations, perceptions, statistics, and comparisons, using a list of 25 animals ranked from least to most dangerous.
Asks the audience to think about which animals they expect to be on the list and which they perceive to be more dangerous.
Presents a comparison of the world's most dangerous animals, revealing surprising statistics like mosquitoes causing 1 million deaths per year.
Raises questions about the accuracy and implications of such statistics, pointing out flaws in the comparisons made.
Criticizes the use of statistics without clear criteria for comparison, warning against making decisions based on misleading information.
Emphasizes the need to scrutinize statistics used to push agendas and make informed decisions.
Urges the audience to be cautious when interpreting statistics and ensure consistent criteria are applied for accurate comparisons.
Concludes by reminding viewers to critically analyze information before making decisions.

Actions:

for statistical analysts, policymakers, researchers.,
Scrutinize statistics for accuracy and consistency in comparisons (implied).
Educate others on the importance of critical analysis when interpreting data (implied).
</details>
<details>
<summary>
2021-05-27: Let's talk about a surprising twist to the Klamath River story.... (<a href="https://youtube.com/watch?v=_FRyuoTbfMc">watch</a> || <a href="/videos/2021/05/27/Lets_talk_about_a_surprising_twist_to_the_Klamath_River_story">transcript &amp; editable summary</a>)

Beau provides an update on the Klamath River situation, hinting at potential conflict with federal intervention and past history repeating itself, setting the stage for a developing national story.

</summary>

"The only thing separating us from the head gates is a chain link fence."
"It's worth noting that about 20 years ago there was a similar situation."
"We may all be turning our eyes to Oregon for a national story."

### AI summary (High error rate! Edit errors on video page)

Provides an update on the Klamath River story in Oregon and Northern California.
Addresses a situation involving low water levels in the river, endangered species, native fishing communities, and irrigation for farmers.
Mentions the closure of the canal to preserve water levels, fishing, and endangered species.
Two farmers have bought land near the head gates and set up an information center to petition for grievances.
Notes the connection of the farmers to Amon Bundy, known for involvement in public lands issues.
Farmers express concerns about potential government intervention and mention past instances of forcing open head gates.
Points out a historical similar situation where farmers had clashed with US Marshals over the head gates.
Hints at a potential national story developing in Oregon with the first demonstration scheduled to happen soon.

Actions:

for community members, activists,
Join or support demonstrations near the head gates to show solidarity (suggested)
Stay informed about the developments in the Klamath River situation (implied)
</details>
<details>
<summary>
2021-05-26: Let's talk about Trump's plan for 2022 and why Democrats have to act now.... (<a href="https://youtube.com/watch?v=P0-FeH1Otk0">watch</a> || <a href="/videos/2021/05/26/Lets_talk_about_Trump_s_plan_for_2022_and_why_Democrats_have_to_act_now">transcript &amp; editable summary</a>)

Former President Trump enlists Newt Gingrich to create a Trumpism version of the 1990s Contract with America, posing challenges for Democrats in the upcoming midterms and the future political landscape.

</summary>

"Trump understands the political landscape right now."
"If they can peel off some of the center with a platform of some kind, it might be pretty effective."
"The midterms are a chance to truly root out Trumpism."

### AI summary (High error rate! Edit errors on video page)

Former President Trump has reportedly enlisted Newt Gingrich to create a Trumpism version of the Contract with America, a political strategy from the 1990s.
The Contract with America was a party platform that allowed voters to vote for something rather than against something, credited for giving Republicans a victory in 1994.
Trump understands the importance of the upcoming midterms for his legacy and the future of the Republican Party.
Democrats face challenges with Trump's strategy as Biden's approach involves appealing to the center and left, but major legislation delivery is a concern.
Democrats need a plan to respond to a potential Contract with America from Trump.
Historically, the original Contract with America didn't fare well in terms of getting enacted, and some policies wouldn't be well-received today.
Uniting voters behind something positive rather than just against Biden could be effective for the GOP.
The main liability is Trump himself, known for not sticking to talking points or policies.
The Democratic Party needs to prepare to address this strategy now to avoid being caught off guard.
The midterms present an opportunity to challenge Trumpism and its influence within the Republican Party.

Actions:

for political analysts and strategists,
Prepare a strategic response plan to counter a potential Contract with America from Trump (suggested)
Engage in proactive political campaigning and messaging to unite voters behind positive policies (implied)
</details>
<details>
<summary>
2021-05-25: Let's talk about Congress getting a report on UFOs.... (<a href="https://youtube.com/watch?v=JZeN-Xmz5T4">watch</a> || <a href="/videos/2021/05/25/Lets_talk_about_Congress_getting_a_report_on_UFOs">transcript &amp; editable summary</a>)

Congress will soon receive a report on UFOs, but expectations of groundbreaking discoveries may be unfounded, given the military's long-standing interest in unidentified aerial phenomena.

</summary>

"The military's been interested in this for a very very long time."
"I wouldn't get your hopes up, but at the same time I'm definitely going to read the report."
"Of course I believe that there is intelligent life out there somewhere."
"It could be a very cooperative species. It could be one that's very just wants to observe."
"Regardless of what's out there, I don't foresee us finding that out next month at this hearing."

### AI summary (High error rate! Edit errors on video page)

Congress will receive a report next month on unidentified aerial phenomena, commonly known as UFOs.
The military has had a longstanding interest in UFOs dating back to the late 1940s.
Programs like Project Blue Book and the Advanced Aerospace Threat Identification Program have investigated UFO sightings over the years.
Despite recent headlines suggesting a newfound interest in UFOs, military involvement and investigations have been ongoing for decades.
The upcoming report is unlikely to reveal definitive proof of extraterrestrial life visiting Earth.
Beau believes in the existence of intelligent life elsewhere in the universe but remains skeptical about aliens visiting Earth.
He points out that popular theories about extraterrestrial civilizations are largely based on human history and assumptions.
Beau encourages managing expectations about the upcoming report on UFOs.
While intriguing footage exists, Beau remains cautious about drawing definitive conclusions from it.
Regardless of the report's contents, Beau plans to read it and find it interesting.

Actions:

for science enthusiasts, ufo believers,
Read the upcoming report on UFOs when it is publicly available (exemplified)
Manage expectations about potential revelations in the report (exemplified)
</details>
<details>
<summary>
2021-05-24: Let's talk about gas prices, Biden, and Trump.... (<a href="https://youtube.com/watch?v=H6-nS69W1wU">watch</a> || <a href="/videos/2021/05/24/Lets_talk_about_gas_prices_Biden_and_Trump">transcript &amp; editable summary</a>)

Gas prices rose before Biden, summer travel demand, stimulus checks, global trend - Biden not solely to blame.

</summary>

"You can't blame this on Biden, not if you want to do it, honestly."
"Because if your politician does not understand supply and demand, they probably shouldn't be your representative."

### AI summary (High error rate! Edit errors on video page)

Gas prices began rising before Biden took office.
Summer brings higher-quality gas and increased travel, raising demand and costs.
The pandemic led to decreased travel last year, creating a pent-up demand for travel now.
Stimulus checks and tax credits have increased disposable income, leading to more travel and higher costs.
Crude oil prices dropped significantly last year and are now rebounding, contributing to increased costs.
Several U.S. refineries closed under the previous administration, reducing production and increasing costs.
The recent pipeline hack caused short-term disruptions but is not a long-lasting factor.
Gas prices are rising in the UK and Canada as well, indicating a global trend.
Biden's stance on fossil fuels may contribute to market instability but not solely to blame for gas price increases.
Understanding basic economics, particularly supply and demand, is key to grasping the situation.
Blaming Biden for gas prices without proper understanding is unjustified.

Actions:

for consumers,
Monitor gas prices and try to carpool or use public transportation when possible (implied).
Educate others on the factors affecting gas prices and discourage unjustified blame (implied).
</details>
<details>
<summary>
2021-05-24: Let's talk about ex-cons, self-doubt, and organizing.... (<a href="https://youtube.com/watch?v=kM010qUUl5M">watch</a> || <a href="/videos/2021/05/24/Lets_talk_about_ex-cons_self-doubt_and_organizing">transcript &amp; editable summary</a>)

Beau receives a life story message, encouraging perseverance despite past mistakes, urging the individual to utilize their unique skills for community betterment.

</summary>

"You're going to get that hate no matter what. That's not a reason. That's an excuse."
"What you say is that you were in a gang. What I hear is that you know how supply routes work."
"If you've already put in the groundwork and now you're just having second thoughts about it, a little bit of self-doubt, you've got to push through that."
"There may not be somebody else for a long time that gets that Goldilocks sentence, that has the skills and the connections and the understanding that right now you have."
"It probably resonated with you for another reason. The hero in that video is not the dude that had gas money because he had a YouTube channel. The hero were the people that got their neighborhood supplied."

### AI summary (High error rate! Edit errors on video page)

Receives a message narrating someone's life story, leading up to seeking encouragement.
Describes the backstory of a kid growing up in a neighborhood and getting involved in an organization.
The individual goes to prison but uses the time to read and grow intellectually.
After release, he aims to make his neighborhood better despite facing challenges.
Expresses self-doubt due to past actions and worries about being judged.
Mentions how rich white college kids come to the neighborhood for short-term fixes.
Encourages the individual not to be deterred by past mistakes or external judgments.
Emphasizes the unique skills and knowledge the person gained from their past experiences.
Urges the person to push through self-doubt as their contributions are valuable.
Stresses the importance of persevering despite uncomfortable moments for the greater good.

Actions:

for community members,
Continue efforts to improve the neighborhood despite challenges (exemplified)
Push through self-doubt and external judgments to make a positive impact (exemplified)
Utilize unique skills and knowledge gained from past experiences for community organizing (exemplified)
</details>
<details>
<summary>
2021-05-23: Let's talk about NY's sweeping police reform legislation.... (<a href="https://youtube.com/watch?v=wSX-6N_j3-s">watch</a> || <a href="/videos/2021/05/23/Lets_talk_about_NY_s_sweeping_police_reform_legislation">transcript &amp; editable summary</a>)

Beau explains the New York legislation on police reform, focusing on using the minimum necessary force and promoting de-escalation.

</summary>

"The minimum necessary to effect the arrest. That's what you should use. That's the gold standard of policy around the country."
"If you don't know if you're allowed to use force, you're not."
"It's good legislation. And it doesn't handcuff law enforcement. It makes them try to deescalate."
"This is just common sense."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the legislation from New York regarding police reform, questioning whether it's accurately described as sweeping.
Emphasizes the importance of using the minimum necessary force to effect an arrest as the gold standard of policy.
Points out that the legislation clearly defines the minimum amount of force needed for an arrest.
Stresses that the legislation prohibits escalating a situation and then using force or using force on someone already subdued.
Notes that while the union is pushing back against the legislation, it's a unique template that could be replicated in other states.
Clarifies that if officers are unsure if they can use force, they shouldn't, as they should always aim to use the minimum necessary force.
Explains that the legislation requires officers to deescalate, exhaust other means before force, and doesn't create a strict step-by-step process.
Mentions that some attorneys believe the legislation may make it easier to create reasonable doubt, but the legislation will still make it easier to charge officers.
Shares feedback from a former cop and deputy who found the legislation to be common sense.

Actions:

for lawmakers, police departments,
Read through and understand the legislation (suggested)
Advocate for similar legislation in other states (suggested)
</details>
<details>
<summary>
2021-05-22: Let's talk about what I didn't talk about and a way to reach people.... (<a href="https://youtube.com/watch?v=8eedJCa-62c">watch</a> || <a href="/videos/2021/05/22/Lets_talk_about_what_I_didn_t_talk_about_and_a_way_to_reach_people">transcript &amp; editable summary</a>)

Beau addresses the challenge of navigating closely held beliefs and advocates for allowing individuals to realize the truth on their own by examining contradictory beliefs rather than forcing acceptance through beratement, stressing the importance of practical concerns for real change in politically divisive issues, particularly in international relations and conflict.

</summary>

"I don't believe the truth can be told, think it has to be realized."
"You don't have to give them new information."
"The side that I'm on is the civilians. It's the side I'm always on."

### AI summary (High error rate! Edit errors on video page)

Acknowledges not addressing a topic discussed widely by others last week, focusing on closely held beliefs and truth.
Explains the challenge of triggering cognitive dissonance when introducing contradictory information to individuals with entrenched beliefs based on propaganda and slogans.
Advocates for allowing individuals to realize the truth on their own rather than trying to force acceptance through beratement.
Emphasizes the importance of examining closely held beliefs that contradict propaganda and slogans to prompt individuals to come to the truth themselves.
Differentiates between moral and practical arguments in addressing politically divisive issues.
Stresses the need to address practical concerns for real change in international relations, foreign policy, and war.
Points out the ineffectiveness of solely relying on moral arguments in contexts where foreign policy decisions are made outside a moral framework.
Suggests reframing arguments to avoid triggering cognitive dissonance and rationalization based on propaganda and slogans.
Advocates for discussing universal truths and core beliefs as a method to reach people effectively.
Expresses concern over the unsustainable nature of the current cycle of violence and advocates for change towards a more sustainable future.

Actions:

for activists, advocates, educators,
Initiate open dialogues and discussions to help individuals realize truths on their own (suggested)
Advocate for practical solutions and considerations in addressing politically divisive issues (implied)
</details>
<details>
<summary>
2021-05-22: Let's talk about how to create change and what works.... (<a href="https://youtube.com/watch?v=jWZijU_E0ls">watch</a> || <a href="/videos/2021/05/22/Lets_talk_about_how_to_create_change_and_what_works">transcript &amp; editable summary</a>)

What works and what doesn't in creating systemic change: diversity of tactics, consistent effort, and focusing on the long-term goal of a movement over individual events.

</summary>

"It's not a moment. It's a movement."
"Each little piece, each little action is one moment in that movement."
"Whatever you're best at. Whatever you're really good at. Those are the skills you need to use to further that drive for systemic change."

### AI summary (High error rate! Edit errors on video page)

Questions the effectiveness of marches and rallies in creating real change, referencing the situation in North Carolina where they seem to have little impact on decision-makers.
Emphasizes that all forms of activism work together as a diversity of tactics to create systemic change, including petitioning, marches, direct involvement, calling senators, and electoralism.
Draws parallels between movements like Black Lives Matter and historical events such as the American Revolution to illustrate the concept of a movement versus a moment.
Encourages individuals to focus on utilizing their unique skills and strengths to contribute to the broader goal of systemic change.
Stresses the importance of consistent effort and a diversity of tactics in pushing forward for change, even when individual actions may seem insignificant.
Advises against counting on any single event or action to make a significant impact, as real change often requires years of behind-the-scenes work and organizing.
Emphasizes the need for diversity in tactics and promoting individuals who approach the same goal in different ways over creating a clique of like-minded individuals.
Acknowledges the challenges of staying motivated in activism, noting that each small action contributes to the larger movement for change.
Urges maintaining forward momentum and focus on the long-term goal of creating a fair and just world through collective efforts.

Actions:

for activists, community organizers,
Utilize your unique skills and strengths to contribute to the drive for systemic change (implied).
Promote individuals who approach common goals in different ways to encourage diversity in tactics (implied).
Stay engaged in activism, attend marches, rallies, and remain active in pursuing change (implied).
</details>
<details>
<summary>
2021-05-21: Let's talk about Tennessee, signs, history, and money lost.... (<a href="https://youtube.com/watch?v=87cQct5J5Ak">watch</a> || <a href="/videos/2021/05/21/Lets_talk_about_Tennessee_signs_history_and_money_lost">transcript &amp; editable summary</a>)

Tennessee plans restroom identity policy signs, sparking economic and moral debates, urging reconsideration of leadership's shortsightedness.

</summary>

"It is more profitable to be a good person."
"You may not care about morality, but I bet you care about that money."
"A whole lot of that situation has to do with their faulty leadership."

### AI summary (High error rate! Edit errors on video page)

Tennessee plans to require signs for restroom identity policies.
Beau questions the necessity and impact of these signs.
Draws a historical comparison to discriminatory signs in the South.
Beau argues against moral approaches to bigots.
Mentions the economic impact of military installations like Fort Campbell, Kentucky.
Installation commanders have power over off-limits areas based on command.
Beau suggests economic repercussions for businesses not displaying the required signs.
Stresses the financial importance of being accepting and inclusive.
Predicts repercussions for establishments without the signs, affecting tourism and business.
Urges voters in Tennessee to reconsider supporting short-sighted leaders.
Warns about the consequences for small businesses due to political decisions.
Encourages reflection on the implications of discriminatory practices.

Actions:

for tennessee voters,
Vote thoughtfully in Tennessee elections (implied)
Support businesses displaying inclusive signs (implied)
</details>
<details>
<summary>
2021-05-21: Let's talk about DOD and the CIA's new recruitment practices.... (<a href="https://youtube.com/watch?v=d-OzGU3dTG4">watch</a> || <a href="/videos/2021/05/21/Lets_talk_about_DOD_and_the_CIA_s_new_recruitment_practices">transcript &amp; editable summary</a>)

Responding to misconceptions about government recruitment practices, Beau dismantles the idea that the Department of Defense and CIA are becoming "woke," explaining their historical recruitment strategies and the concept's misinterpretation.

</summary>

"They recruit people that you view as lesser."
"Neither the DOD or the CIA is actually woke."
"Progress and intellectual acceptance do not lead to the downfall of a country."

### AI summary (High error rate! Edit errors on video page)

Responding to a comment on Ted Cruz's Twitter feed about recruitment practices in government entities and modern art.
Criticizes the idea that the Department of Defense (DOD) and CIA are becoming "woke" through their recruitment practices.
Points out that the dictionary definition of "woke" is being alert to injustice, particularly racism.
Mentions that the DOD has a history of recruiting people from marginalized groups, contributing to social change.
Explains how the special forces and historical CIA operations have involved cultural exchange and appreciation.
Talks about the CIA's recruitment strategies, including using abstract art to recruit intellectuals.
Argues that both the DOD and CIA have always recruited individuals who understand ethnic divisions and class issues.
Emphasizes that progress and intellectual acceptance do not lead to the downfall of a country.
Concludes that the DOD and CIA are not actually "woke" but rather use the concept to attract recruits.
Urges viewers to recognize the historical context of recruitment practices in these agencies.

Actions:

for social commentators, activists,
Research and analyze historical recruitment practices of government entities to understand their context and implications (suggested).
Educate others on the historical roles of the Department of Defense and CIA in recruitment (suggested).
Challenge misconceptions about government recruitment practices by sharing accurate information with others (suggested).
</details>
<details>
<summary>
2021-05-20: Let's talk about who we're leaving behind over there.... (<a href="https://youtube.com/watch?v=LgN89_u2cpM">watch</a> || <a href="/videos/2021/05/20/Lets_talk_about_who_we_re_leaving_behind_over_there">transcript &amp; editable summary</a>)

Beau questions why the U.S. is leaving allies behind, urging for quicker action to fulfill promises made to those who risked their lives for U.S. interests.

</summary>

"Leave no man behind, right?"
"We're leaving, we're heading out, we're not helping them but we know they could help the national government."
"And now, when it's time to make good on the promises that we made, well, we'll get around to it."
"Doesn't mean it's not true."
"That's honestly what it seems like."

### AI summary (High error rate! Edit errors on video page)

Questioning why the U.S. is leaving their allies behind, particularly the interpreters and Afghan people who assisted U.S. forces.
Traditionally, the U.S. military has left allies behind in conflicts, but Congress is urging Biden to speed up the process of helping these individuals.
Congress could pass legislation to airlift the 18,000 applicants and their families to the U.S. for processing, which seems more efficient than the current process in Afghanistan.
Beau believes that Congressman Waltz from Florida, an ex-Green Beret, genuinely cares about the situation due to his personal experience working with these individuals.
Beau expresses a cynical view that leaving these allies behind could serve as a motivation for them to join the opposition, knowing the consequences if the national government falls.
He points out the pattern of the U.S. leaving allies behind worldwide, making it difficult to recruit local help.
Despite the risks taken by these allies for U.S. interests, there is a delay in fulfilling promises made to them, with a wait time of possibly a couple of years.

Actions:

for advocates for allies,
Push Congress to pass legislation authorizing an airlift of the Afghan allies and their families to the U.S. for processing (implied).
Advocate for expedited processes to fulfill promises made to allies who assisted U.S. forces (implied).
Support organizations working to assist Afghan allies in relocating to safety (implied).
</details>
<details>
<summary>
2021-05-20: Let's talk about over the horizon in Afghanistan.... (<a href="https://youtube.com/watch?v=YfLYtOzCZIo">watch</a> || <a href="/videos/2021/05/20/Lets_talk_about_over_the_horizon_in_Afghanistan">transcript &amp; editable summary</a>)

The US withdrawal from Afghanistan leaves uncertainties about security and the future of the Afghan military, relying heavily on drones for over-the-horizon capabilities.

</summary>

"It's going to be worse than a US presence, as bad as that was."
"The real issue here is that the US was real quick to go in without any clear idea on how to get out."
"I don't think when people were talking about we need to leave Afghanistan, that this is what they were picturing."

### AI summary (High error rate! Edit errors on video page)

The withdrawal of US forces from Afghanistan is happening, but no country has stepped up to provide a token security force.
The US military plans to maintain over-the-horizon capabilities, primarily through airstrikes.
US forces are looking for countries to host them near Afghanistan despite leaving the country.
Most airstrikes in Afghanistan are currently carried out by the Afghan Air Force, maintained by US contractors who will likely leave.
The Afghan military has changed in the last 20 years and may struggle to hold its own without US contractor support.
General McKenzie hinted at the possibility of US forces going back into Afghanistan for special operations and drone strikes.
The reliance on drones for surveillance and airstrikes raises concerns about civilian casualties and accuracy.
The use of drones without human intelligence on the ground poses challenges.
Beau suggests that without a token security force, there may be a return of some US special operations troops within a year.
The withdrawal from Afghanistan appears more like a pause than an end, with uncertainties about the national government's ability to stand alone.

Actions:

for foreign policy analysts,
Support organizations providing aid and assistance in Afghanistan (implied)
Stay informed on developments in Afghanistan and advocate for responsible foreign policy decisions (implied)
</details>
<details>
<summary>
2021-05-20: Let's talk about assessing risk.... (<a href="https://youtube.com/watch?v=IRFUsf-mdwc">watch</a> || <a href="/videos/2021/05/20/Lets_talk_about_assessing_risk">transcript &amp; editable summary</a>)

Beau explains assessing risk, challenges fear as a threat indicator, and advocates for morality over fear-based justifications in using force.

</summary>

"Fear doesn't make a threat."
"This is how you determine what a threat is."
"Maybe we should start leaning back towards the morality side of it."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of assessing risk and how it factors into official statements regarding the use of force.
Challenges the notion that fear alone constitutes a threat by providing examples of intent, capability, and opportunity.
Describes the traditional formula for determining a threat as intent plus capability, and how an additional criteria of opportunity has been added over time.
Illustrates the concept with a scenario of someone having intent and capability but lacking the current opportunity to pose a threat.
Introduces a newer terminology of ability jeopardy, which equates to intent and opportunity in assessing threats.
Emphasizes the importance of evaluating whether a situation truly warrants the use of force based on morality rather than justifiable fear.
Advocates for a shift towards considering the necessity of using force instead of relying on fear-based justifications.

Actions:

for decision-makers, law enforcement.,
Revisit risk assessment protocols (implied).
Challenge fear-based justifications for use of force (implied).
</details>
<details>
<summary>
2021-05-19: Let's talk about why Republicans don't want a commission.... (<a href="https://youtube.com/watch?v=-ihqXyttvvY">watch</a> || <a href="/videos/2021/05/19/Lets_talk_about_why_Republicans_don_t_want_a_commission">transcript &amp; editable summary</a>)

Republicans in Congress resist a commission to uncover the truth about January 6th, potentially fearing exposure of lies that may damage their credibility with constituents.

</summary>

"Because right now, half of them believe something that isn't true."
"The reality is the Republican Party, well they can't handle the truth."
"Unless of course it is what happened and that's what they're worried about."
"The reason they don't want a commission is because they can't risk their constituents finding out what actually happened."
"No matter what excuse they throw out, the reason they don't want to commission is because they can't risk their constituents finding out what actually happened."

### AI summary (High error rate! Edit errors on video page)

Republicans in Congress are resisting a commission to uncover the truth about the events of January 6th.
A Reuters Ipsos poll showed that 61% of Americans believe Trump is at least somewhat responsible for January 6th, with only 28% of Republicans agreeing.
Despite a quarter of Republicans blaming Trump, the GOP leadership seems reluctant to set the record straight.
Many Republicans believe the false narrative that left-wingers were behind the events on January 6th.
The GOP's refusal to support the commission may stem from fear of their constituents learning the truth and realizing they've been misled.
The party might be concerned about their lies being exposed and their credibility being damaged.
The reluctance to uncover the truth could be linked to protecting the current leadership's image and preventing constituents from changing their views.
Beau suggests that the GOP's resistance to the commission could be due to the risk of their supporters discovering what actually occurred on January 6th.

Actions:

for politically aware individuals,
Contact your representatives to express support for a commission to uncover the truth about January 6th (suggested).
Spread awareness about the importance of transparency and accountability in political leadership (implied).
</details>
<details>
<summary>
2021-05-19: Let's talk about something to do this weekend, miners, and Alabama.... (<a href="https://youtube.com/watch?v=h3jRen93miM">watch</a> || <a href="/videos/2021/05/19/Lets_talk_about_something_to_do_this_weekend_miners_and_Alabama">transcript &amp; editable summary</a>)

Miners in Alabama strike for fair conditions, needing support with a strike pantry and fundraiser, showing solidarity benefits all.

</summary>

"They just want a fair shake."
"These fights that organized labor engages in, they really do benefit everybody."
"Showing a little bit of support here is not only the neighborly thing to do."

### AI summary (High error rate! Edit errors on video page)

Miners in Alabama went on strike after facing deteriorating working conditions and mistreatment from management.
The United Mine Workers of America in Brookwood, Alabama are leading the strike with over 1100 miners participating.
The striking miners are in need of a strike pantry to support their families with food.
The Valley Labor Report is hosting a livestream fundraiser from Friday to Sunday to support the striking miners.
The Alabama Strike Fest on May 22 will feature music, barbecue, and comedians to raise funds for the miners.
The event includes comedian Drew Morgan and aims to bring people together to support the cause.
Donations are encouraged, with a suggested amount of $20, and it's free for UMWA members.
Even if unable to attend in person, there's a link to donate directly to support the miners.
Supporting organized labor fights like this can benefit everyone by raising wages in the area.
Showing support for the miners is not just about being neighborly; it also serves self-interest and can have a wider positive impact.

Actions:

for supporters of fair labor practices,
Donate to support the striking miners (suggested)
Attend the Alabama Strike Fest fundraiser event (exemplified)
</details>
<details>
<summary>
2021-05-18: Let's talk about if I'll still wear a mask.... (<a href="https://youtube.com/watch?v=Bmo9kE0wzqE">watch</a> || <a href="/videos/2021/05/18/Lets_talk_about_if_I_ll_still_wear_a_mask">transcript &amp; editable summary</a>)

Beau stresses the importance of personal responsibility in wearing masks as a visual reminder to encourage vaccination, especially in areas with hesitancy.

</summary>

"You don't need a law to tell you to be a good person."
"I will continue to wear mine. The guidance is that. It's guidance."
"I really do believe that the social science would suggest more people would get vaccinated if they saw a bunch of masks."

### AI summary (High error rate! Edit errors on video page)

Expresses frustration at Trump's lack of mask-wearing as a failure of leadership and civic duty to set an example for the American people.
Stresses the importance of changing societal mindset over changing laws to effect real change.
Mentions Biden administration's commitment to science-based guidance, indicating that fully vaccinated individuals have low risk and may not need masks in certain scenarios.
Plans to continue wearing a mask personally as a visual reminder that the pandemic is not over and more people need to get vaccinated.
Worries that without establishments checking vaccination status, unvaccinated individuals may exploit the lack of enforcement to avoid wearing masks, especially in areas with vaccine hesitancy.
Believes in the power of visual cues like masks to encourage more people to get vaccinated and sees it as a social science issue.

Actions:

for individuals, community members,
Wear a mask as a visual reminder to encourage others to get vaccinated (implied)
Encourage establishments to enforce mask-wearing or vaccination checks (implied)
</details>
<details>
<summary>
2021-05-17: Let's talk about renewers in the Republican party and Trumpism.... (<a href="https://youtube.com/watch?v=_g82dfkRlB4">watch</a> || <a href="/videos/2021/05/17/Lets_talk_about_renewers_in_the_Republican_party_and_Trumpism">transcript &amp; editable summary</a>)

Beau urges immediate action for Republican renewers to form a new party and make an impact before the 2022 critical period.

</summary>

"You've talked for five years. Five years. It won't mean anything unless you act."
"Because as wonderful as it may be to see yourself as one of the rationals, if you don't convert that belief into action, it's worthless."
"For this to mean anything, you have to act on it now."
"You will determine your place by whether you get off the sidelines before 2022."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the topic of Miles Taylor, a former Trump official, and the concept of renewers advocating for a potential third party.
Miles Taylor, known for the "I'm the resistance" op-ed, believes in splitting the Republican Party to appeal to rational individuals not radicalized by Trump.
Taylor's vision involves forming a new party called the renewers, distinct from the radicals, to represent the rational Republicans.
Beau stresses the importance of taking action now if those supporting a third party want to make a meaningful impact.
He criticizes mere talk without action, urging involvement in midterms and launching candidates against Trumpist elements like Greene.
Beau warns that unless beliefs are translated into action, being seen as rational holds no value.
Individuals within the Republican Party advocating for change must act promptly as the current leadership relies on Trump's momentum for the midterms.
To prevent a Trump-like figure from dominating, immediate action in forming a new party is necessary.
Beau underscores the urgency by pointing out five years of talking without significant action.
He concludes by encouraging prompt action before the critical period of 2022 to secure a place in history.

Actions:

for republican renewers,
Launch candidates against Trumpist figures like Greene before the midterms (suggested)
Start forming a new party and take immediate action (implied)
</details>
<details>
<summary>
2021-05-17: Let's talk about asking the right question and debating the wrong one.... (<a href="https://youtube.com/watch?v=1szx-TRWF2U">watch</a> || <a href="/videos/2021/05/17/Lets_talk_about_asking_the_right_question_and_debating_the_wrong_one">transcript &amp; editable summary</a>)

Beau stresses the importance of asking the right questions, focusing on why excessive force was used by law enforcement rather than blaming collateral damage victims.

</summary>

"The question is not whether or not Alexandra knew about what was happening in her building. The question is why did the SWAT team use too much force?"
"I don't think that when a nation begins debating that amongst themselves, rather than discussing the obvious problem of too much force being used, I don't think that sits well for the future of that nation either."
"There are a lot of questions being asked, and almost none of them are the right question."

### AI summary (High error rate! Edit errors on video page)

Talks about the importance of asking the right question instead of focusing on the wrong question.
Presents a story about a reporter named Alexandra Peterson whose apartment is blown up by a SWAT team.
Points out the issue of debating whether Alexandra knew about the illegal activities happening below her apartment.
Emphasizes that the real question should be about why the SWAT team used excessive force and caused additional damage.
Expresses concern about society shifting blame to collateral damage victims instead of questioning law enforcement's actions.
Criticizes the nation for getting distracted by irrelevant debates instead of addressing the actual problem of excessive force.
Suggests that the country has lost sight of critical issues by engaging in misguided debates.
Urges for a focus on asking the right questions amidst various ongoing debates and controversies.

Actions:

for advocates and activists,
Question law enforcement's use of excessive force (exemplified)
Shift focus to addressing the real issues instead of irrelevant debates (exemplified)
</details>
<details>
<summary>
2021-05-16: Let's talk about South Africa, moments, and movements.... (<a href="https://youtube.com/watch?v=wlqPYOWrA2Y">watch</a> || <a href="/videos/2021/05/16/Lets_talk_about_South_Africa_moments_and_movements">transcript &amp; editable summary</a>)

Beau explains the complex history of apartheid in South Africa, debunking myths and underlining the diverse factors that led to its end.

</summary>

"If you want real change, it's going to take a diversity of tactics."
"It's not one thing. It's almost never one thing."
"That's the deep lesson, if you want real change."
"You're talking about power. And that is very rarely motivated by morality."
"It's not always a moral issue."

### AI summary (High error rate! Edit errors on video page)

Gives an overview of the history of apartheid in South Africa, debunking common misconceptions.
Explains that apartheid was not solely ended by Nelson Mandela but was a result of a combination of factors and events.
Details the timeline of apartheid laws and events from 1948 to the end of apartheid.
Mentions key moments such as the Sharpeville massacre in 1960, international pressure, and the township rebellions.
Talks about the importance of understanding the diverse tactics and factors that led to the end of apartheid.
Emphasizes that societal change is a result of multiple factors and not a single event or person.
States that the end of the Cold War played a significant role in the fall of apartheid.
Advises looking at historical sources like the State Department archives to understand complex societal changes.
Stresses the necessity of considering economic, geopolitical, and cultural aspects in bringing about real change.

Actions:

for history enthusiasts, social activists,
Study the history of apartheid and understand the diverse tactics that led to its end (suggested).
Analyze complex societal changes in other countries through accurate historical sources like State Department archives (suggested).
</details>
<details>
<summary>
2021-05-16: Let's talk about Jurassic Florida Mosquitoes..... (<a href="https://youtube.com/watch?v=m51AW863njc">watch</a> || <a href="/videos/2021/05/16/Lets_talk_about_Jurassic_Florida_Mosquitoes">transcript &amp; editable summary</a>)

Scientists conduct a wild science experiment in Florida involving genetically modified mosquitoes to reduce mosquito-borne diseases without pesticides, despite potential risks.

</summary>

"Life found a way."
"Scientists are conducting a wild science experiment in Florida involving genetically modified mosquitoes."
"The reduction of pesticides, especially in Florida, is seen as a positive aspect of this experiment."

### AI summary (High error rate! Edit errors on video page)

Scientists are conducting a wild science experiment in Florida involving genetically modified mosquitoes.
Male mosquitoes have been modified to pass on a gene to their female offspring, requiring them to have an antibiotic to prevent breeding.
The goal is to reduce mosquito-borne diseases without using pesticides.
Similar experiments have been done in other countries with positive results.
The reduction of pesticides, especially in Florida, is seen as a positive aspect of this experiment.
Beau mentions the famous line from a movie, "life finds a way," hinting at potential unexpected outcomes.
The impact of mosquito-borne diseases is becoming increasingly significant due to climate change expanding the insects' ranges.
Despite sounding like a plot from a science fiction movie, the experiment has multiple safety safeguards in place.
The modified mosquitoes are expected to be self-limiting and last only a couple of generations, providing a safety net in case something goes wrong.
Beau acknowledges that there might be concerns and invites experts to share their opinions on the experiment.

Actions:

for environmentalists, scientists, community members,
Share information about genetically modified mosquitoes with your community (suggested)
Seek out expert opinions on the subject and start a constructive discussion (suggested)
</details>
<details>
<summary>
2021-05-15: Let's talk about the Republican plan for 2022 and beyond.... (<a href="https://youtube.com/watch?v=6V4J21-DzV8">watch</a> || <a href="/videos/2021/05/15/Lets_talk_about_the_Republican_plan_for_2022_and_beyond">transcript &amp; editable summary</a>)

Republican Party plans to pivot away from Trump for 2024, grooming a more polished version, making 2022 midterms pivotal in defeating Trumpism and combating authoritarianism.

</summary>

"2022 becomes important. While the GOP may be flipping a coin, the leadership may be flipping a coin as to whether or not they win."
"You want to get rid of Trumpism? They have to get that resounding defeat, that landslide that Biden needed to completely wipe out Trumpism."
"The midterms, which most times people tend to forget about, especially if their party is in power, they've taken on a new significance in the battle against Trump's particular brand of authoritarianism."

### AI summary (High error rate! Edit errors on video page)

Republican Party's plan is to pivot away from Trump for 2024 due to his failing support and decreased social media mentions.
McConnell, although disliked by Beau, is recognized as politically savvy and a long-time Capitol Hill player.
The GOP leadership seems to be letting baseless claims about the election persist and allowing upstarts in the party to do as they please until 2022.
If the upstarts win in 2022, they will present a more refined, polished version of Trump for the party to rally behind.
If the upstarts lose in 2022, it becomes easier for the GOP to pivot away from Trump.
The significance of the 2022 midterms lies in potentially delivering a resounding defeat to Trumpism to send a message to the Republican leadership.
The hope is to weed out Trump's momentum through the midterms and introduce a more polished version of Trump with similar policies but less public opposition.
Beau expresses concern about a polished version of Trump getting policies enacted with less opposition and fears the GOP's strategy of courting high-profile individuals for this purpose.
The midterms are emphasized as critical in combating Trump's authoritarian brand, which could outlast his political career if not addressed now.

Actions:

for political analysts, activists,
Organize voter registration drives and encourage voter turnout for the 2022 midterms (implied)
Engage in local political organizing and support candidates who advocate against Trumpism and authoritarianism (implied)
</details>
<details>
<summary>
2021-05-15: Let's talk about the Klamath River and water supplies.... (<a href="https://youtube.com/watch?v=t0lXuCBkxlg">watch</a> || <a href="/videos/2021/05/15/Lets_talk_about_the_Klamath_River_and_water_supplies">transcript &amp; editable summary</a>)

The Klamath River basin faces a drought crisis, pitting farmers against endangered salmon and overlooking the indigenous Yurok group's vital connection to the river.

</summary>

"Farmers versus fish."
"Don't forget that there are people on the ground in these areas that are going to be impacted that have no control over what happens."
"But who has more of a claim – the Yurok or people who showed up during the gold rush?"
"There will be more and more disputes over water rights."
"They're the people that may need the most support."

### AI summary (High error rate! Edit errors on video page)

The Klamath River basin in Oregon and northern California is facing a drought, impacting people along the river and in reclamation programs.
Farmers are being told there won't be water for irrigation, leading to a potential conflict with endangered salmon species.
Low water levels in the river are causing bacteria to flourish, already impacting 97% of tested salmon, especially juveniles.
The upcoming national news coverage might frame the issue as "farmers versus fish," overlooking the Yurok native group dependent on salmon for their way of life.
The Yurok tribe signed a treaty with the U.S. in the 1850s to maintain their fishing lifestyle within a specific geographic area.
Despite having good lawyers, the Yurok might be forgotten in national coverage due to powerful interests at play.
It's vital to recognize and support those on the ground who lack influence over decisions impacting their livelihoods.
Confined groups like the Yurok have a moral right to maintain their traditional lifestyle without external decisions lowering their standard of living.
The drought in the Klamath River basin raises questions about water rights and conflicting claims between longstanding farmers and indigenous groups.
More disputes over water rights are expected with increasing droughts, but it's crucial not to overlook the local communities most impacted.

Actions:

for environmental activists, policymakers, community organizers,
Support the Yurok native group in maintaining their traditional fishing lifestyle (implied).
Advocate for equitable water distribution to all affected groups in the Klamath River basin (implied).
</details>
<details>
<summary>
2021-05-14: Let's talk about burn out from helping those in need.... (<a href="https://youtube.com/watch?v=-Vrk2vWeQUk">watch</a> || <a href="/videos/2021/05/14/Lets_talk_about_burn_out_from_helping_those_in_need">transcript &amp; editable summary</a>)

Providing assistance to those in need can be emotionally draining; recognizing your limitations and taking breaks is vital to prevent burnout and continue making a difference.

</summary>

"You have to try to remain detached on some level."
"You don't have to be the tip of the spear."
"When you start getting taxed, when you start feeling like you just can't do it anymore, it's time to take a break."
"You can't stay on point all the time."
"Recognize your limitations."

### AI summary (High error rate! Edit errors on video page)

Talks about providing assistance to those in need and the emotional toll it can take on individuals involved in community networks.
Describes a situation where a person's involvement in helping someone with substance abuse issues ended tragically.
Emphasizes the importance of staying emotionally detached when providing direct assistance to avoid burnout.
Shares a personal experience of a community helping a vet with substance abuse problems, which ultimately led to a heartbreaking outcome.
Stresses the need to recognize one's emotional limitations and take a step back when feeling drained.
Acknowledges that not everyone is suited for direct involvement in emotionally taxing situations, and that it's okay to take a different role in supporting a cause.
Advises taking breaks when feeling overwhelmed to prevent burnout and maintain effectiveness in helping others.
Mentions the importance of soldiers having tours and coming back, comparing it to the need for breaks in high-stress situations.
Encourages individuals to continue supporting causes in different ways if direct involvement becomes too emotionally taxing.
Reminds viewers that recognizing personal limitations and taking breaks doesn't indicate weakness, but rather self-awareness and self-care.

Actions:

for community volunteers and activists,
Take breaks when feeling overwhelmed to prevent burnout (implied)
Recognize personal emotional limitations and step back when needed (implied)
Continue supporting causes in alternative ways if direct involvement becomes taxing (implied)
</details>
<details>
<summary>
2021-05-14: Let's talk about 1033 and nuance.... (<a href="https://youtube.com/watch?v=2IWT7mjMS6U">watch</a> || <a href="/videos/2021/05/14/Lets_talk_about_1033_and_nuance">transcript &amp; editable summary</a>)

Beau explains the nuances of the 1033 program, advocating for changes to prevent its misuse in militarizing police departments.

</summary>

"It definitely facilitates the desire of departments to cast the image of the warrior cop."
"The program itself is neither inherently good or bad. It's the intent of the department. The intent of the user."
"It's not as simple as 1033 is bad."
"That's what needs to change."
"The real solution here would be Congress changing it completely."

### AI summary (High error rate! Edit errors on video page)

Explains the 1033 program, which transfers excess military equipment to law enforcement.
Acknowledges the program's problems, like the militarization of police departments.
Notes that rural counties sometimes benefit from the program for life-saving equipment.
Stresses that the program's impact depends on the intent of the department and user.
Mentions that while there are good uses, there are also cases of misuse leading to dangers in communities.
Suggests that Biden could provide temporary relief but a real fix requires Congressional action.
Proposes a restructured program accessible to various community entities beyond law enforcement.
Points out the need for changes in how the program operates, especially in preventing militarization of police departments.
Considers the possibility of scrapping the program entirely and rebuilding it from scratch for real change.
Emphasizes that the issue is not just about the program being inherently bad but about how it is utilized.

Actions:

for congress, biden administration,
Advocate for Congress to make significant changes to the 1033 program (suggested)
Support initiatives that aim to prevent the militarization of police departments (implied)
Push for a restructured program accessible to various community entities (suggested)
</details>
<details>
<summary>
2021-05-13: Let's talk about slogans, policy, and pirates.... (<a href="https://youtube.com/watch?v=5vynxgMs8Zs">watch</a> || <a href="/videos/2021/05/13/Lets_talk_about_slogans_policy_and_pirates">transcript &amp; editable summary</a>)

Slogans can mislead citizens into expecting government actions, leading to violence and the perpetuation of conflict until negotiation is truly committed.

</summary>

"Expecting a 'get tough' approach can result in security clampdowns and innocent people being harmed."
"Failing to recognize violence as violence leads to justifying any action by one's side and condemning the other's."
"The cycle of violence can only end when the state side commits to negotiation."

### AI summary (High error rate! Edit errors on video page)

Slogans can mislead citizens into believing they represent actual government policy.
The United States does negotiate with groups, contrary to popular slogans.
Modern doctrine aims to remove militant leadership to encourage negotiation.
Misunderstanding slogans can lead to a mistaken expectation of government behavior.
Expecting a "get tough" approach can result in security clampdowns and innocent people being harmed.
Clampdowns can inadvertently strengthen opposition groups.
Justifying actions based on opposition behavior can lead to dangerous outcomes.
Blaming protesters for everything can escalate violence and justify unjust actions.
The story of Alexander the Great and the pirate illustrates the similarity in actions between pirates and emperors.
Failing to recognize violence as violence leads to justifying any action by one's side and condemning the other's.
In state versus non-state actor conflicts, a security clampdown often leads to an endless cycle of violence.
The cycle of violence can only end when the state side commits to negotiation.
In the United States, there were calls to deploy troops against citizens due to demonization and belief in slogans.
Without a commitment to negotiation, the cycle of violence persists.
Recognizing violence as violence is critical to breaking the cycle of violence.

Actions:

for policy advocates,
Contact local representatives to advocate for peaceful conflict resolution (implied).
Organize community dialogues on the impact of slogans and policies on government actions (implied).
Educate others on the dangers of justifying violence based on opposition behavior (implied).
</details>
<details>
<summary>
2021-05-13: Let's talk about Ted Cruz and something funny.... (<a href="https://youtube.com/watch?v=tKLbKJlREf0">watch</a> || <a href="/videos/2021/05/13/Lets_talk_about_Ted_Cruz_and_something_funny">transcript &amp; editable summary</a>)

Beau feels conflicted between humor and seriousness, critiquing Ted Cruz's misleading tweet about the green new deal's connection to gasoline outages.

</summary>

"Welcome to the green new deal."
"It's funny that a sitting senator doesn't know that this isn't law."
"I find that funny as long as you don't think about it too long."

### AI summary (High error rate! Edit errors on video page)

Feels conflicted about doing something funny or addressing a serious message.
Mentioned receiving a message questioning his claims about Republican politicians.
Commented on Ted Cruz's tweet about gasoline outages in North Carolina.
Explained the irony behind the connection made to the green new deal in Cruz's tweet.
Noted that the green new deal is proposed legislation, not yet law, and has had no effect.
Pointed out the potential benefits of the green new deal in reducing reliance on fossil fuels.
Mentioned Ted Cruz's lack of enthusiasm for certain infrastructure types.
Criticized Cruz for implying a connection between the pipeline issue and the green new deal.
Suggested that Cruz's tweet could be seen as misleading and fear-mongering.
Noted the high number of likes on Cruz's tweet, indicating support without critical analysis.

Actions:

for social media users,
Fact-check misleading information shared on social media (implied)
Encourage critical thinking and research before accepting claims blindly (implied)
</details>
<details>
<summary>
2021-05-12: Let's talk about rebirth and the Affordable Care Act.... (<a href="https://youtube.com/watch?v=aDyz9_Njzlw">watch</a> || <a href="/videos/2021/05/12/Lets_talk_about_rebirth_and_the_Affordable_Care_Act">transcript &amp; editable summary</a>)

Beau addresses the Biden administration's healthcare rule change promoting acceptance, citing a friend undergoing transformative surgery for societal inclusivity.

</summary>

"It's a good thing. It's a good thing. It's more accepting. It's the right move."
"So why did I wait to talk about it? Because if I have timed this correctly, right now somebody is going through a bit of confirmation surgery."
"A friend of mine, and maybe a friend of yours, if you are active in the comments here or on Twitter, Zoe Jane Halo is currently going through what she called a rebirth surgery."

### AI summary (High error rate! Edit errors on video page)

Beau addresses recent news involving the Biden administration's directive to the Department of Health and Human Services regarding a rule change affecting the Affordable Care Act and Section 1557.
Section 1557 prohibits discrimination in health care programs based on race, color, national origin, sex, age, or disability, with recent inclusion of orientation and identity due to a Supreme Court ruling.
Beau stresses the importance of this rule change, mentioning that it fosters acceptance and is a step in the right direction.
Many people mistakenly believe the impact is limited to confirmation surgery for transgender individuals, but discrimination can affect access to various types of healthcare.
Beau shares a personal connection by mentioning a friend, Zoe Jane Halo, who is currently undergoing what she describes as a "rebirth surgery" to be more accepting in society.
Zoe Jane Halo is someone active on platforms like Twitter and YouTube, discussing her experiences and journey openly.
Beau encourages support for Zoe and acknowledges the significance of standing by friends during transformative moments.
The message revolves around the importance of supporting friends going through significant life changes and the need for acceptance and understanding in society.

Actions:

for supportive individuals,
Support Zoe Jane Halo on platforms like Twitter and YouTube (implied)
</details>
<details>
<summary>
2021-05-12: Let's talk about Charlie Brown and Trump supporters.... (<a href="https://youtube.com/watch?v=pR8G5qSjZGk">watch</a> || <a href="/videos/2021/05/12/Lets_talk_about_Charlie_Brown_and_Trump_supporters">transcript &amp; editable summary</a>)

Trump supporters urged to focus on community issues and tangible actions rather than chasing unattainable promises, as real change starts locally.

</summary>

"You want to make a difference."
"Your community, your family, your country, they need you."
"But I understand the drive."
"Not chasing a football, because you are never going to kick it."
"They're at their wits end."

### AI summary (High error rate! Edit errors on video page)

Illustrates the analogy of Charlie Brown and Lucy to Trump supporters waiting for certain dates and events, constantly being let down.
Mentions the shifting dates from January 6th to May 15th, with promises of arrests that are unlikely to happen.
Points out the wasted time spent chasing unfulfilled expectations, urging focus on community issues instead.
Emphasizes the need for Trump supporters to redirect their efforts towards making a tangible difference locally.
Encourages taking action in the community rather than getting swept up in baseless claims and divisive rhetoric.
Urges Trump supporters to realize that their time and energy could be better spent on meaningful change.
Stresses the importance of being present for one's family and community instead of being consumed by unattainable goals.
Calls for a shift from online rhetoric to real-life actions that benefit those around them.
Reminds individuals that making America great requires tangible efforts, not chasing elusive promises.
Encourages reflection on the impact of one's actions on family and community relationships.

Actions:

for trump supporters,
Redirect efforts towards addressing community issues (suggested)
Take tangible actions to make a difference locally (suggested)
Focus on real-life initiatives rather than online rhetoric (suggested)
</details>
<details>
<summary>
2021-05-11: Let's talk about unemployment and a labor shortage.... (<a href="https://youtube.com/watch?v=7SRjXmgqLfM">watch</a> || <a href="/videos/2021/05/11/Lets_talk_about_unemployment_and_a_labor_shortage">transcript &amp; editable summary</a>)

Companies blame unemployment for labor shortage, refusing to pay higher wages, neglecting basic economics and the industrialist rule.

</summary>

"Unemployment is not really a cash cow that's pretty much the bare minimum you can survive on."
"You have to pay for it. It's really that simple."
"Maybe it's just a budgeting issue. Y'all shouldn't go to Starbucks so much."
"If you want it, you have to pay for it."
"It's really that simple. Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Companies blaming unemployment for labor shortage, refusing to raise wages.
Unemployment is survival, not a cash cow; wages should be higher than unemployment benefits.
Companies forgetting the industrialist rule: pay highest wages possible.
American companies neglecting to pay high wages to attract labor.
Labor is vital for creating capital and being a capitalist.
Companies pressuring governments to reduce unemployment instead of raising wages.
Working class faces the carrot (hope for social mobility) and the stick (threat of homelessness).
Companies failing to grasp basic economics: supply and demand.
Shortage of labor should lead to increased wages.
Budgeting advice like "pull yourself up by your bootstraps" fails to address real issues.

Actions:

for working class, employers, government,
Raise wages to attract labor (implied)
Advocate for fair wages in your workplace (implied)
</details>
<details>
<summary>
2021-05-11: Let's talk about pipeline and supply chain concerns.... (<a href="https://youtube.com/watch?v=MTCx4t2EA8Q">watch</a> || <a href="/videos/2021/05/11/Lets_talk_about_pipeline_and_supply_chain_concerns">transcript &amp; editable summary</a>)

Beau addresses pipeline concerns, assures contingency plans are in place, and advises against panic amid potential gas price hikes and shortages.

</summary>

"The Biden administration apparently reads the Oh No book."
"There's going to be higher gas prices. There will be shortages in some areas."
"So there are plans, and it does appear that the Biden administration has read the Oh No book and is starting to enact them."

### AI summary (High error rate! Edit errors on video page)

Addresses questions about the pipeline due to previous video "Let's Talk About What Happens If The Trucks Stop".
Acknowledges the critical infrastructure of the pipeline with contingency plans in place.
Compares the current situation to a previous instance where plans were not followed during a crisis.
Expresses confidence in the Biden administration's actions and implementation of contingency plans.
Explains the relaxation of safety protocols for truckers to haul fuel and mitigate the pipeline shutdown's impact.
Mentions plans to maintain refinery operations by bringing in tanker ships for storage.
Anticipates minimal supply chain disruptions if the pipeline resumes operation by the end of the week.
Assures that although there may be gas price hikes and shortages, it won't lead to a complete halt in the trucking industry.
Emphasizes the importance of monitoring the situation but advises against panicking.
Notes the possibility of major issues if the pipeline disruption persists.

Actions:

for concerned citizens,
Monitor the situation for updates and changes (implied).
Be mindful of truckers' workload and give them space if encountered in impacted areas (implied).
</details>
<details>
<summary>
2021-05-10: Lets talk about the GOP and Trump's social media.... (<a href="https://youtube.com/watch?v=ZJtnwzudd3Q">watch</a> || <a href="/videos/2021/05/10/Lets_talk_about_the_GOP_and_Trump_s_social_media">transcript &amp; editable summary</a>)

The GOP struggles to maintain Trump's dwindling base, facing internal chaos while hindering progress in opposition to Biden, leaving the country stagnant.

</summary>

"We will get destroyed and we will deserve it."
"It's kind of going out."
"And it is glorious to watch."
"Nothing can get done."
"Tapping into a base that at best is 10% of what it used to be."

### AI summary (High error rate! Edit errors on video page)

Pointing out the GOP's efforts to maintain Trump's base and keep those voters engaged.
Describing the complete disarray within the GOP, with attempts to oust leading politicians and internal censorship.
Expressing skepticism about the existence of the base Trump inspired after the Capitol incident.
Noting a significant decline in Trump's social media interactions post-January 6th.
Mentioning the lack of enthusiasm from Trump's fervent supporters, despite access to his social media platform.
Observing a decrease in Trump's influence in fueling ideologies like fascism and authoritarianism.
Commenting on Republicans causing chaos within their party, leading it towards a dumpster fire.
Quoting Lindsey Graham's acknowledgment of potential destruction within the GOP.
Noting that GOP's disarray is preventing them from presenting a platform opposing Joe Biden, hindering progress in the country.
Stating that Republicans oppose Democratic proposals in an effort to please Trump's base, which is now significantly diminished.

Actions:

for political observers, activists,
Monitor and analyze political developments within the GOP to understand the shifting landscape (implied).
Stay informed on how internal party dynamics affect national governance (implied).
</details>
<details>
<summary>
2021-05-10: Let's talk about Fauci and two questions.... (<a href="https://youtube.com/watch?v=tHX6lltxAoE">watch</a> || <a href="/videos/2021/05/10/Lets_talk_about_Fauci_and_two_questions">transcript &amp; editable summary</a>)

Beau addresses seasonal mask usage, expresses willingness to wear one based on expert consensus, and dismisses mask-wearing as a control mechanism.

</summary>

"The world is a chaotic place and these theories attempt to make sense of the chaos."
"I definitely believe the official numbers are low."
"I do not believe masks are some kind of control mechanism."
"It doesn't make sense and yeah it's not a big deal."
"If it helps yeah I'll keep doing it."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the seasonal aspect of mask usage and the need to potentially get used to it.
He expresses his willingness to continue wearing a mask as long as experts deem it beneficial to society.
Beau shares that wearing a mask doesn't bother him and, in fact, he finds it somewhat enjoyable as he hasn't had to interact with unwanted high school acquaintances.
He dismisses the notion that mask-wearing is a control mechanism, attributing such beliefs to a desire for order in a chaotic world.
Beau references the effectiveness of facial recognition software demonstrated by the federal government, suggesting that any authoritarian group with such technology wouldn't encourage face coverings for control.
He believes that the official COVID-19 numbers are likely undercounted based on personal experiences and lack of widespread testing in his area.
Beau reiterates his stance that masks are not a form of control and expresses his comfort with wearing masks, even citing previous experiences with gas masks.

Actions:

for individuals concerned about mask usage and control narratives.,
Follow expert recommendations on mask-wearing (implied).
Advocate for widespread testing and accurate reporting of COVID-19 cases (implied).
Encourage others to wear masks based on scientific evidence (implied).
</details>
<details>
<summary>
2021-05-09: Let's talk about mothers and minimum wages.... (<a href="https://youtube.com/watch?v=uafRZDRvgcM">watch</a> || <a href="/videos/2021/05/09/Lets_talk_about_mothers_and_minimum_wages">transcript &amp; editable summary</a>)

The impact of moms on consumerism may drive a potential rise in the minimum wage, as companies rely on a growing population for sustainability.

</summary>

"Moms may be the reason it changes."
"It's always the bottom line that matters."
"If companies want to continue to grow, they can't have a shrinking population."

### AI summary (High error rate! Edit errors on video page)

This video delves into the connection between raising the minimum wage to $15 an hour and the impact of moms on consumerism in the United States.
Major companies rely on consumerism to thrive and have often opposed raising the minimum wage.
The fertility rate in the U.S. is below the replacement level, potentially impacting the consumer base needed for these companies.
Surveys indicate that moms desire to have more children than they actually do, with financial considerations being a significant barrier.
To sustain growth and beat previous financial records, companies may need to increase wages, ultimately leading to a potential rise in the minimum wage.
The focus on economic considerations suggests that the bottom line of large companies will be the driving force behind changes in the minimum wage.
President Biden's actions in support of a $15 minimum wage can contribute positively, but the economic bottom line remains the key factor.
Failure to address the gap between desired and actual children could result in a decrease in consumers, impacting companies' profitability.
Ultimately, the growth of companies hinges on a growing population and economic factors.
Beau concludes with a thought-provoking message and wishes everyone a Happy Mother's Day.

Actions:

for policy makers, activists, economists,
Engage in advocacy for raising the minimum wage (implied)
Support policies that benefit working families (implied)
</details>
<details>
<summary>
2021-05-08: Let's talk about space mushrooms and news cycles.... (<a href="https://youtube.com/watch?v=JXjP6W_Pydg">watch</a> || <a href="/videos/2021/05/08/Lets_talk_about_space_mushrooms_and_news_cycles">transcript &amp; editable summary</a>)

Beau cautions against blindly accepting sensational news, stressing the importance of expert validation to combat misinformation like space mushrooms.

</summary>

"Why should we listen to the experts? They can't agree."
"Those disagreements, they don't show that the experts don't know what they're talking about. They show that the system works."
"Make sure you don't fully commit to believing [new information] until other people have reviewed it, people that have expertise in that area."
"Otherwise, space mushrooms."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Encountered an article claiming mushrooms on Mars, sparking interest.
Rover photographs mushrooms, but subsequent article debunks claim as rocks.
Mention of a scientist with dubious reputation fuels skepticism.
Media cycle of groundbreaking study followed by debunking confuses the public.
Disagreements among scientists can erode public trust in experts.
Peer-review process is vital for scientific credibility and consensus.
Emphasizes the importance of verifying new information before acceptance.
Encourages reliance on experts for accurate evaluation of studies.
Warns against believing unverified information without expert validation.
Advocates for following up on studies to avoid falling for misleading narratives.

Actions:

for science enthusiasts,
Verify scientific claims before sharing (suggested)
Seek expert opinions on new studies (suggested)
</details>
<details>
<summary>
2021-05-07: Let's talk about why Biden turned left.... (<a href="https://youtube.com/watch?v=TnYEZNoKU-0">watch</a> || <a href="/videos/2021/05/07/Lets_talk_about_why_Biden_turned_left">transcript &amp; editable summary</a>)

Beau examines Biden's shift towards social democracy, suggesting it could be driven by legacy concerns, ideological beliefs, or a strategic move for a second term, amidst a backdrop of Republican disarray and a progressive shift in the United States.

</summary>

"Social democracy is still capitalism. It's capitalism with a smiley face."
"More people have more of the things they need to survive."
"We can be like Denmark or we can devolve into fascism."

### AI summary (High error rate! Edit errors on video page)

Explains the context of the American left and social democracy.
Analyzes Biden's shift towards social democracy.
Suggests possible reasons for Biden's leftward turn: legacy concerns, ideological beliefs, or political strategy for a second term.
Considers Biden's move in response to the progressive shift in the United States.
Points out the Republican Party's internal conflicts and lack of inspiring ideas.
Speculates that Biden may be pushing left because he can, given the disarray in the Republican Party.
Clarifies that social democracy is still capitalism with a focus on providing essentials for survival.
Presents a choice between moving towards social democracy or descending into fascism for the United States' future.

Actions:

for political observers,
Choose to support policies that prioritize providing essentials for survival (implied)
Stay informed about political shifts and ideologies (implied)
Engage in political discourse and decision-making processes (implied)
</details>
<details>
<summary>
2021-05-07: Let's talk about Psaki's admission about Biden and the press.... (<a href="https://youtube.com/watch?v=mV_8ywZ6oy8">watch</a> || <a href="/videos/2021/05/07/Lets_talk_about_Psaki_s_admission_about_Biden_and_the_press">transcript &amp; editable summary</a>)

Beau addresses Psaki's admission on managing press interactions and underscores the importance of thoughtful presidential communication for global impacts, contrasting Biden's approach with Trump's.

</summary>

"His job is not to provide fuel and be a sideshow for tabloid news outlets."
"Every statement that comes out should be well thought out, because it has impacts all over the world."
"Biden should totally not talk to the press that much. That's not his job."
"His job is to run the country. His press secretary, well, that's her job."
"I think they're doing a pretty good job of maintaining a flow of information."

### AI summary (High error rate! Edit errors on video page)

Addressing the controversy surrounding Press Secretary Psaki's admission about not wanting the president to take impromptu questions from reporters.
Points out that the context provided by Fox News reveals that the president does, in fact, take impromptu interviews, which Psaki does not appreciate as it makes her job harder.
Emphasizing that a press secretary's role is to provide information from the president to the press, and Biden going off script complicates this process.
Stating that the president's main job is not to entertain tabloid news outlets but to be the chief executive, making decisions based on expert advice within the confines of Congress.
Comparing Biden's approach to press interactions with Trump's, suggesting that Biden's controlled communication style is more appropriate for a president.
Arguing that Biden should carefully think out his statements as they have global impacts, contrasting this with Trump's tendency to make unfounded statements that caused confusion and negative impacts.
Concluding that Biden should focus on running the country while Psaki handles press interactions effectively.
Praising Biden and Psaki for maintaining a good flow of information despite occasional impromptu interviews.

Actions:

for media consumers,
Trust reputable news sources for comprehensive reporting (suggested)
Understand the roles of government officials in communicating with the press (implied)
</details>
<details>
<summary>
2021-05-06: Let's talk about Biden, patents, and soft power.... (<a href="https://youtube.com/watch?v=cTE8ldxIXE0">watch</a> || <a href="/videos/2021/05/06/Lets_talk_about_Biden_patents_and_soft_power">transcript &amp; editable summary</a>)

Beau explains how the U.S.'s support to end vaccine patents is about obtaining global influence through soft power, not altruism, in response to China and Russia's actions.

</summary>

"We should be the world's EMT rather than the world's policeman."
"This is about obtaining influence overseas."
"Just don't let it be framed as we're out there being the world's savior."
"All of this is good."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the Biden administration's decision to support ending vaccine patents as an exercise in soft power rather than altruism.
Differentiates between soft power (attracting countries to you) and hard power (going after them) in foreign policy.
Advocates for the U.S. to be the world's EMT (emergency medical technician) rather than the world's policeman to illustrate soft power over hard power.
Mentions the U.S. countering China and Russia's vaccine diplomacy through soft power.
Describes how China and Russia have been successful in using soft power to gain influence internationally.
Suggests that the U.S. is playing catch up with China and Russia in exercising influence through vaccine diplomacy.
Points out the potential benefits of a global vaccine race triggered by countries trying to exert influence.
Emphasizes that the U.S.'s move to support ending vaccine patents is about obtaining influence overseas, which coincidentally benefits everyone involved.
Compares the immediate results of hard power with the longer-lasting benefits of soft power in foreign policy.
Raises the possibility that the U.S.'s actions may be a response to counter Russian and Chinese soft power rather than a purely altruistic gesture.

Actions:

for foreign policy enthusiasts,
Support efforts to provide necessary supplies for vaccine production in other countries (implied)
Advocate for equitable distribution of vaccines globally (implied)
Stay informed about international relations and soft power dynamics (implied)
</details>
<details>
<summary>
2021-05-05: Let's talk about what we can learn about politics from medics.... (<a href="https://youtube.com/watch?v=nAHQMvrp6ic">watch</a> || <a href="/videos/2021/05/05/Lets_talk_about_what_we_can_learn_about_politics_from_medics">transcript &amp; editable summary</a>)

Beau explains the importance of staying focused, avoiding cliques, and maintaining effectiveness in activism, drawing lessons from street medics during demonstrations.

</summary>

"Street medics, well they don't have anything to do. They get bored and when they get bored, well that's when cliques start to form."
"Find something to focus on. Don't just rally around the politician during election season."
"You have more contacts. And it makes you and your circle a little bit more politically valuable."
"You can't let up."
"If you want to stay effective, if you want to actually accomplish stuff, you can't let up."

### AI summary (High error rate! Edit errors on video page)

Explains the unique role of street medics at demonstrations expected to get wild.
Talks about how street medics experience a lull in activity during cold weather and start cooperating and coordinating during warm weather.
Describes the challenges faced by street medics when they get bored, leading to cliques, in-fighting, and decreased effectiveness.
Suggests ways to maintain effectiveness in groups like community networks or mutual assistance groups during lulls.
Recommends training, staying on mission, and incorporating fun activities to prevent cliques and maintain focus.
Advocates for healthy competition and mixing opposing groups to foster camaraderie.
Encourages political engagement beyond election seasons, focusing on ongoing issues and causes.
Stresses the importance of staying on mission, avoiding infighting, and maintaining effectiveness in achieving goals.
Urges individuals to reach out and stay connected with their circle to remain effective in their activism.

Actions:

for community activists,
Reach out to your circle of activists to stay connected and effective (suggested).
Train and stay focused on mission objectives in community groups (implied).
Organize fun activities or events within your activist network to build camaraderie (implied).
Campaign for ongoing causes beyond election seasons (implied).
</details>
<details>
<summary>
2021-05-05: Let's talk about Tuskegee and hesitancy.... (<a href="https://youtube.com/watch?v=HBnMN4nDefY">watch</a> || <a href="/videos/2021/05/05/Lets_talk_about_Tuskegee_and_hesitancy">transcript &amp; editable summary</a>)

Beau addresses the hesitancy of a specific group to get vaccinated, focusing on Grandma's valid concerns rooted in historical events and the importance of presenting facts.

</summary>

"It's not irrational. It's not irrational."
"You get to choose whether you're in the group that gets treatment, its protection or not."
"She's not a rational. She's got a reason."
"She has a reason to believe what she believes is to come at it with facts."
"There are people alive today who were alive when it happened."

### AI summary (High error rate! Edit errors on video page)

Addressing the hesitancy of a certain group to get the COVID shot.
Grandma's reluctance to get vaccinated due to fears related to the Tuskegee experiment.
Explaining that Grandma's concerns are not irrational but based on historical events.
The challenge of using rhetoric to persuade Grandma to get vaccinated.
The impact of historical events like Tuskegee on individuals who experienced them firsthand.
Emphasizing the importance of presenting factual information to address Grandma's concerns.
Comparing past events like Tuskegee to the current COVID situation in terms of study and choice.
Pointing out that Grandma's perspective is shaped by her life experiences and memories.
Acknowledging the difficulty in finding common ground due to differences in experiences.
Suggesting that having a grandchild around might help in addressing Grandma's concerns.

Actions:

for family members,
Have open and honest conversations with hesitant family members about vaccination (suggested)
Present factual information and historical context to address concerns (exemplified)
</details>
<details>
<summary>
2021-05-04: Let's talk about whether the Germans were socialists.... (<a href="https://youtube.com/watch?v=odfUE7EnidA">watch</a> || <a href="/videos/2021/05/04/Lets_talk_about_whether_the_Germans_were_socialists">transcript &amp; editable summary</a>)

A friend seeks help in explaining socialism without history, politics, or ideology; Beau uses a powerful poem to debunk misconceptions easily.

</summary>

"First they came for the socialists and I did not speak out because I was not a socialist."
"Your answer is in poetry."
"It's not history-based. It doesn't have to require a whole lot of knowledge of different ideologies."
"If this is true, then why would they do this?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

A friend reached out for help in explaining socialism to someone terrified of it due to World War II associations.
The challenge was to explain without using history, politics, or ideology.
Beau accepted the challenge but struggled to come up with an approach for two weeks.
While looking for quotes by George Orwell, Beau stumbled upon a video where he quotes a piece of poetry.
Beau recites a well-known poem that starts with "First they came for the socialists."
The poem progresses to mention trade unionists and Jews, with the speaker not speaking out until it was too late for them.
The point made is that if the persecutors were socialists, they wouldn't have targeted socialists first.
Beau suggests that this simple and powerful poem can help explain the misconception about socialists during World War II.
The poem provides a straightforward way to illustrate the issue without delving into complex historical or political details.
Beau concludes by offering this poem as a tool for having a meaningful and accessible conversation on the topic.

Actions:

for educators, activists, students,
Share the poem "First they came for the socialists" to debunk misconceptions about socialism (implied).
</details>
<details>
<summary>
2021-05-04: Let's talk about how the GOP proved me wrong.... (<a href="https://youtube.com/watch?v=nqfSbY2Cf0Y">watch</a> || <a href="/videos/2021/05/04/Lets_talk_about_how_the_GOP_proved_me_wrong">transcript &amp; editable summary</a>)

Some GOP members convinced Beau social programs create freeloaders, but they're really talking about themselves, as seen with vaccine refusal.

</summary>

"They are talking about themselves."
"I don't want to be a freeloader."
"I want to do my part."

### AI summary (High error rate! Edit errors on video page)

Some members of the GOP convinced him he was wrong about social programs creating freeloaders.
The GOP's point is that social programs without an incentive create freeloaders.
Beau realized they were referring to themselves, not patriotic, civic-minded people.
They resist masks and vaccines because there's no incentive.
Beau got his second vaccine dose to exercise civic-mindedness and protect his community.
He felt unwell for about 16 hours post-vaccine but believes it's worth it.
Beau stresses the importance of not being a freeloader and doing your part for society.
Those opposing masks and vaccines are in a demographic catered to their whole life.
Beau got vaccinated not for himself but to protect others.
He encourages people to understand they're not talking about you, but themselves.

Actions:

for community members,
Get vaccinated to protect your community (exemplified)
Exercise civic-mindedness by getting vaccinated (implied)
</details>
<details>
<summary>
2021-05-03: Let's talk about the US, Iran, and political theater.... (<a href="https://youtube.com/watch?v=nF__aYZ-0Ug">watch</a> || <a href="/videos/2021/05/03/Lets_talk_about_the_US_Iran_and_political_theater">transcript &amp; editable summary</a>)

Beau analyzes the political theater between Iran and Biden, suggesting both are engaging in a show to claim victory and bring back the deal.

</summary>

"I think they're both lying."
"It's a show for you and me and the people over there."
"It's a show for the public."
"The goal here of both sides was to just bring the deal back."
"I think the deal's back."

### AI summary (High error rate! Edit errors on video page)

Talks about feelings, appearances, and shows in relation to recent news about Iran.
Iran announced the end of sanctions and potential prisoner exchanges, while Biden states negotiations are still ongoing.
Beau believes both sides are engaging in political theater to appear as winners to their domestic audiences.
Points out that both Iran and Biden want the deal but need to make it seem like they made the other side concede.
Speculates that there may have been an agreement in principle before the public announcements were made.
Suggests that the bold statements from Iran and Biden's negotiations are part of a show for the public.
Beau predicts that a concession from Iran, not yet publicly disclosed, will allow both sides to claim victory and bring the deal back.
Emphasizes that the goal is to restore the broken deal to its previous state.
Mentions that the political situations in both countries require a certain theatricality in negotiations.
Concludes by expressing a belief that the deal is back in principle, though not officially announced.

Actions:

for foreign policy observers,
Keep an eye on the developments regarding the Iran deal (implied).
</details>
<details>
<summary>
2021-05-03: Let's talk about Ted Cruz's wild op-ed on contributions.... (<a href="https://youtube.com/watch?v=uAZe8KlF1fY">watch</a> || <a href="/videos/2021/05/03/Lets_talk_about_Ted_Cruz_s_wild_op-ed_on_contributions">transcript &amp; editable summary</a>)

Beau delves into Ted Cruz's controversial op-ed, hinting at a potential exchange of campaign contributions for favors, leading to a reevaluation of political influence and the affordability of senators.

</summary>

"This time, we won't look the other way on Coca-Cola's $12 billion in back taxes owed."
"Maybe we can afford a senator."
"Y'all were mean to me so I'm not going to let you pay me off anymore."

### AI summary (High error rate! Edit errors on video page)

Talks about an op-ed by Ted Cruz in the Wall Street Journal, which stirred controversy on Twitter due to implications of taking campaign contributions in exchange for favors to large corporations.
Cruz criticizes woke companies and their responses to bills, especially regarding Georgia.
Cruz vows not to overlook issues like Coca-Cola's back taxes or MLB's antitrust exemption.
He announces that he will no longer accept money from corporate PACs, citing the $2.6 million received over nine years.
Cruz implies a connection between campaign contributions and favors, leading to a reconsideration of conventional wisdom on political influence.
Suggests setting up a GoFundMe account to afford a senator based on the relatively low cost.
Acknowledges Cruz may not have intended to admit to this exchange but understands how it can be interpreted that way.
Irony in Cruz's statement regarding not accepting money anymore due to perceived mistreatment by corporations.
Questions the effectiveness of Cruz's message in the op-ed.

Actions:

for political enthusiasts, activists,
Start a GoFundMe campaign to fund political causes (exemplified)
Analyze and question the connections between corporate contributions and political favors in your local political landscape (suggested)
</details>
<details>
<summary>
2021-05-02: Let's talk about a vote in Texas, seeing it, and unintended consequences.... (<a href="https://youtube.com/watch?v=OqyFhcDBkm4">watch</a> || <a href="/videos/2021/05/02/Lets_talk_about_a_vote_in_Texas_seeing_it_and_unintended_consequences">transcript &amp; editable summary</a>)

Austin's decision to recriminalize homelessness reveals a bigger national issue, showcasing the importance of compassionate solutions over punitive measures.

</summary>

"Criminalizing something doesn't actually make it go away, see every prohibition ever."
"Generally speaking, pretty much always it is cheaper to be a good person."
"Nothing can be done but something was done."
"The bigger problem is one of the moral character of the United States saying that we just can't solve it."
"Imagine how tired they are living it."

### AI summary (High error rate! Edit errors on video page)

Austin, Texas recently voted to recriminalize homelessness, leading to fines for those camping in public areas near downtown.
Criminalizing homelessness doesn't make it go away; it just increases interactions with law enforcement.
This vote will result in more homeless individuals ending up in county jail, costing taxpayers more than implementing social programs or shelters.
It's cheaper and more effective to address homelessness through compassionate and proactive measures.
Some may argue that homeless individuals choose to be homeless, but that doesn't represent the majority.
Returning to policies that worsened the issue won't solve it; progress requires forward-thinking solutions.
The situation mirrors other national issues where people resist change by clinging to outdated methods.
There's a lack of political will and resources to effectively tackle homelessness on a larger scale in the United States.
Homelessness is a significant problem nationwide, reflecting deeper moral challenges within the country.
Ignoring or criminalizing homelessness won't make it disappear; it requires genuine efforts and compassion to address.

Actions:

for community members, advocates,
Advocate for social programs and shelters to address homelessness (suggested)
Support organizations working to provide assistance and resources to homeless individuals (exemplified)
</details>
<details>
<summary>
2021-05-02: Let's talk about Geraldo, black lives, and spurious correlations.... (<a href="https://youtube.com/watch?v=-8r_sMRiakE">watch</a> || <a href="/videos/2021/05/02/Lets_talk_about_Geraldo_black_lives_and_spurious_correlations">transcript &amp; editable summary</a>)

Beau addresses misconceptions around statistics on Black lives matter, urging to focus on causal relationships over spurious correlations.

</summary>

"There is no evidence to suggest a causal link between skin tone and whether or not you are a killer."
"When somebody uses this, it is inherently racist."
"We have to look for the causal relationships and not for spurious correlations."
"If we are basing policy on random information, we are probably not going to get good results."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Responding to a tweet about statistics and Geraldo Rivera's comments on Black lives matter.
Expresses how the focus always seems to be on crime when talking about Black lives matter.
Criticizes the narrative that reinforces the idea that Black people are criminals, making it easier for law enforcement to handle them.
Points out that statistics on crime don't show which skin tone kills the most but rather who gets arrested the most.
Mentions the flaws in statistics, especially when missing significant information.
Questions why statistics are divided by race if they don't provide useful information and may perpetuate systemic racism.
Suggests that economic status might be a more relevant factor to analyze in relation to crime rates.
Emphasizes the importance of establishing causal relationships in statistics rather than relying on spurious correlations.
Concludes by urging to look for causal relationships in statistics to make informed policy decisions.

Actions:

for policy analysts, activists,
Challenge and correct misconceptions about statistics and race (suggested)
Advocate for policies based on causal relationships rather than spurious correlations (suggested)
</details>
<details>
<summary>
2021-05-01: Let's talk about Jessica Alexander and Rosa Parks.... (<a href="https://youtube.com/watch?v=ri35ga4nWIE">watch</a> || <a href="/videos/2021/05/01/Lets_talk_about_Jessica_Alexander_and_Rosa_Parks">transcript &amp; editable summary</a>)

Jessica Alexander's comparison of her anti-mask advocacy to Rosa Parks is not only factually incorrect but also dismissive of the true injustices faced during segregation, a comparison Beau deems ridiculous.

</summary>

"That was the whole problem. You know, the segregation thing, that was an issue."
"She wasn't somebody who just happened to be in the right place at the right time."
"When you say it out loud, it doesn't make any sense because it doesn't make any sense."
"Something being asked to wear a mask to living in Montgomery, Alabama at that time, that's not the same."
"Please get the story right because this is somebody who I truly believe is never given the proper credit that they deserve."

### AI summary (High error rate! Edit errors on video page)

Jessica Alexander from Temecula, California, made a widely ridiculed and unjustifiable comparison of her anti-mask advocacy to Rosa Parks.
Jessica Alexander also got a key fact wrong about Rosa Parks' story, claiming Parks moved to the front of the bus because she knew it wasn't lawful, when in reality, it was the law that enforced segregation.
Rosa Parks was seated in the first come, first serve section of the bus, not in the white section reserved for whites.
Parks' refusal to give up her seat was an act of resistance against the unjust segregation laws, not just mere fatigue or happenstance.
By placing Parks in the white section of the bus, the true injustice and inequalities of segregation are downplayed and sanitized.
The comparison between Jessica Alexander's anti-mask advocacy and Rosa Parks' civil rights activism is deemed ridiculous and nonsensical by Beau.
Beau stresses the importance of getting Rosa Parks' story right and giving her the proper credit she deserves.
The attempt to equate wearing masks in modern times to the struggles of individuals like Rosa Parks during segregation is dismissed as a false comparison.
Beau calls out the trend of minimizing historical atrocities while exaggerating contemporary inconveniences as the ultimate victimization.
Beau encourages viewers to watch a video detailing overlooked aspects of Rosa Parks' story for a more comprehensive understanding.

Actions:

for community members, activists,
Watch the video detailing overlooked aspects of Rosa Parks' story (suggested)
Ensure accurate and respectful retelling of historical figures' stories (implied)
</details>
