---
title: Let's talk about a second Operation New Life and counter arguments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6sMEQxLSuhA) |
| Published | 2021/05/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses leaving behind people in Afghanistan who helped the US or NATO.
- Mentions the visa process backlog and the risks involved in applying for it.
- Talks about two counter arguments: moral and practical.
- Explains the moral argument against US occupation due to imperialism.
- Points out that combatants in conflict may not represent the will of the people.
- Stresses that being against imperialism involves self-determination and protecting civilians.
- Emphasizes that leaving behind those who helped the West is not just limited to interpreters.
- Suggests mounting an operation called Operation New Life to bring these individuals to safety.
- Mentions the success of a similar operation in 1975 that brought over 100,000 people to Guam.
- Argues that the capability to conduct such an operation exists and it's a matter of will.

### Quotes

- "We mount an operation to do it, and we will call it Operation New Life."
- "The ability to do this is there. That's just the idea that it isn't. That's just fantasy."
- "People are going to look to Biden for this. It's not really his call."

### Oneliner

Beau addresses leaving behind Afghan allies, proposing Operation New Life to bring them to safety, citing past successes and the capability to act now.

### Audience

Advocates for Afghan allies

### On-the-ground actions from transcript

- Mount an operation to evacuate Afghan allies, as proposed by Beau (suggested).
- Advocate for Operation New Life to be implemented by the government (suggested).

### Whats missing in summary

The full transcript provides a detailed insight into the moral and practical implications of leaving Afghan allies behind, proposing a feasible solution through Operation New Life.

### Tags

#Afghanistan #OperationNewLife #USoccupation #ForeignPolicy #Evacuation


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
clarity and a little bit of history and good guys and bad guys and I'm going to address
a couple of counter arguments that came up in regards to who we were leaving behind over there.
And to be clear when I say that, I did a video last week I think talking about who we were leaving
behind in Afghanistan. There are people there who helped the US or NATO or the West in general
who when the West pulls out and the opposition there takes the offensive which is a pretty likely
thing, they're going to have bad things happen to them and their families. And as it stands,
we're just kind of leaving them. There is a visa process, however the backlog is incredibly long
and the process for applying for the visa is incredibly risky. So it's not a workable solution.
Now the counter arguments that came up, there were two and they can be divided,
moral and practical, just like pretty much anything else when it comes to foreign policy.
The moral argument comes from those who oppose the US occupation because empire is bad. I mean that
makes sense. At the same time, the argument that is presented doesn't really hold up to scrutiny
because the idea there is that well they helped the occupation, the locals are going to deal with
them, not our problem. Okay, it is entirely possible and in fact is likely that in a conflict
neither side, as far as the combatants go, actually represent the will of the people.
That's a very likely thing. It's not about right and wrong, it's not about the will of the people,
it's about power. And just because one side is doing something wrong, say engaging an empire,
that doesn't mean that the other side are the good guys. In this case they are absolutely not.
The overwhelming majority of people in Afghanistan do not want this sort of thing to happen,
but it likely will. Now, aside from that, the idea of being against imperialism
in theory has to do with self-determination of the people and, you know, civilians,
protecting civilians, who are the people that are at risk. And I think a little bit of clarification
is in order because the loudest voices who are talking about this right now, they're talking
about the interpreters, because the loudest voices are military people, people who have been there
and they worked with the interpreters, so it's personal to them. But as far as the way the
opposition there views it, it's people who helped the West. It's not just limited to the interpreters.
This would include people who assisted Western institutions. This would include
aid workers or school teachers. And I know that sounds like normal propaganda type stuff.
I would just point out that the comeback statement from the opposition recently was to go after a girl's school.
I am not cool with leaving a school teacher behind because of some vague notion of it being somehow
anti-imperialist to do so. Okay, so that's the moral argument. I don't think that holds up to scrutiny.
The practical argument basically boils down to, well, that's just too complicated. We can't do
that. It's too hard. We don't have the capabilities. We don't have the technology.
Whatever. Some reason to say that we can't handle an airlift.
So here's my suggestion. We mount an operation to do it, and we will call it Operation New Life,
because we're going to bring them here and give them a new life, right? And because that was the
name of the operation the last time we did it in 1975. More than 100,000 people.
They were taken to Guam and processed there. I want to say that we're not going to do it.
We're not going to do it there. I want to say from April to November, but don't quote me on that.
We absolutely can do it. We have done it before with far less technology and capability. It can
be done and for a whole lot more people, because we're not talking about 100,000.
The ability to do this is there. That's just the idea that it isn't. That's just fantasy.
It is absolutely there. It's the will. It's the will. I think it's gotten to the point
where the American people are just ready to wash their hands of it completely.
And given the fact that the whole battle cry here to get out was about saving civilians,
I don't understand the logic in leaving people behind to meet the end they are going to.
That doesn't make sense to me. People are going to look to Biden for this. It's not really his
call. I mean, it is in the sense of he would have to approve it, but he can't really approve it
on his own. I would point out in 1975 when it was done, Congress passed the
Indochina Migration and Refugee Assistance Act in May of 1975.
Prior to bringing them here, the U.S. did try to resettle them in nearby countries.
Most of the other countries didn't want them. So they were taken to Guam. They were processed.
Some returned home. Some went to other countries. But most came to the United States.
We can do it, and we have done it before.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}