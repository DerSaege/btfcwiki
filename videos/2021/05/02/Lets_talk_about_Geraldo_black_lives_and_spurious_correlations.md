---
title: Let's talk about Geraldo, black lives, and spurious correlations....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-8r_sMRiakE) |
| Published | 2021/05/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Responding to a tweet about statistics and Geraldo Rivera's comments on Black lives matter.
- Expresses how the focus always seems to be on crime when talking about Black lives matter.
- Criticizes the narrative that reinforces the idea that Black people are criminals, making it easier for law enforcement to handle them.
- Points out that statistics on crime don't show which skin tone kills the most but rather who gets arrested the most.
- Mentions the flaws in statistics, especially when missing significant information.
- Questions why statistics are divided by race if they don't provide useful information and may perpetuate systemic racism.
- Suggests that economic status might be a more relevant factor to analyze in relation to crime rates.
- Emphasizes the importance of establishing causal relationships in statistics rather than relying on spurious correlations.
- Concludes by urging to look for causal relationships in statistics to make informed policy decisions.

### Quotes

- "There is no evidence to suggest a causal link between skin tone and whether or not you are a killer."
- "When somebody uses this, it is inherently racist."
- "We have to look for the causal relationships and not for spurious correlations."
- "If we are basing policy on random information, we are probably not going to get good results."
- "Y'all have a good day."

### Oneliner

Beau addresses misconceptions around statistics on Black lives matter, urging to focus on causal relationships over spurious correlations.

### Audience

Policy analysts, activists

### On-the-ground actions from transcript

- Challenge and correct misconceptions about statistics and race (suggested)
- Advocate for policies based on causal relationships rather than spurious correlations (suggested)

### Whats missing in summary

The full transcript provides a detailed breakdown of the flaws in using statistics to perpetuate racial stereotypes and the importance of focusing on causal relationships over correlations.

### Tags

#Statistics #BlackLivesMatter #Racism #CausalRelationships #PolicyAnalysis


## Transcript
Well howdy there internet people, it's Beau again
So today we are going to talk about Geraldo
and statistics
We're gonna do this because one of y'all tagged me in one of his tweets at like 3 a.m
Because you didn't want me to have a good Saturday night
No, I'm joking. I'm glad you did because this is probably something that I have talked about it before in a way
I've talked about these statistics before but I'm gonna take it a step further today because of this tweet
Okay, so what did he say?
He says black lives matter
But only if the life is taken by a cop hard to hear and easy to ignore
But no one other than the victims families seem to care if the perpetrator is another black life, which most are
No mention of black-on-black crime at the Oscars
Okay, so let's let's go to the whole black lives matter, but they don't you know
Talk about every single thing that impacts black people
All right, let's start there because there's a lot in this tweet
um
Have you ever noticed the people that say that always go to crime?
They never say oh black lives matter, but you never hear them talk about
Diseases that you know predominantly affect black people
Always goes to crime
Because it reinforces the narrative that black people are criminals. Therefore it's okay for the cops
Do you know just handle it?
And then they point to those statistics which most are
if the perpetrator is another black life, which most are I
Have talked about those statistics a lot first thing those statistics do not show what people think they do
It does not show which skin tone
Kills the most
It's not what it shows. It shows who gets arrested the most
and given the fact that law enforcement only clears like 60% of murder cases and
Those are only of cases that were opened and there's a whole bunch of missing persons that we know were probably killed
You really can't establish anything when you're missing half the information
The statistics in and of themselves are garbage
But I've got a whole bunch of videos on this topic. I want to take it a step further
Why are they divided up by race?
The purpose of statistics is to give you useful information
And unless somebody is suggesting that skin tone
Contributes to somebody becoming a killer
They're kind of useless and the idea that skin tone makes somebody a killer
Wow, that's like inherently racist, isn't it?
They're just not civilized yet, I guess
The stats are useless
They really are by this same data set
People with brown eyes are more likely
To be killers and they're more likely to kill people with brown eyes
Wild, right?
I'm willing to bet we're not going to base policy on that
You're not going to have politicians using that as a talking point
Because that doesn't reinforce the systemic racism that exists in the United States
And that's what these stats do
Let's try it a different way
Some stats that we know to be true
We have these stats
That attempt to paint black people as criminals
Disproportionately responsible for more crime
We also know that black people are disproportionately low income
That may be an actual causal relationship
Rather than skin tone
I'm willing to bet if we did this by economic status
It would be more useful
Because if you are of lower means
You're probably more likely to get involved in lesser crimes
And then things might escalate
That would be less of a spurious correlation
That would at least be something worth looking into
There is no evidence to suggest a causal link
Between skin tone
And whether or not you are a killer
When somebody uses this
It is inherently racist
There is no evidence to back that up
What you have is a spurious correlation
And they didn't talk about it at the Oscars
And that's like super important I guess
When you are looking at statistics
Try to figure out if what you are looking at
Actually establishes a causal relationship
One thing causes the other
Or if it's just random information
Because if we are basing policy on random information
We are probably not going to get good results
We have to look for the causal relationships
And not for spurious correlations
Anyway, it's just a thought
Y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}