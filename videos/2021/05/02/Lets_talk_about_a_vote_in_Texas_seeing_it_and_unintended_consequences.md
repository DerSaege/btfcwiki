---
title: Let's talk about a vote in Texas, seeing it, and unintended consequences....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OqyFhcDBkm4) |
| Published | 2021/05/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Austin, Texas recently voted to recriminalize homelessness, leading to fines for those camping in public areas near downtown.
- Criminalizing homelessness doesn't make it go away; it just increases interactions with law enforcement.
- This vote will result in more homeless individuals ending up in county jail, costing taxpayers more than implementing social programs or shelters.
- It's cheaper and more effective to address homelessness through compassionate and proactive measures.
- Some may argue that homeless individuals choose to be homeless, but that doesn't represent the majority.
- Returning to policies that worsened the issue won't solve it; progress requires forward-thinking solutions.
- The situation mirrors other national issues where people resist change by clinging to outdated methods.
- There's a lack of political will and resources to effectively tackle homelessness on a larger scale in the United States.
- Homelessness is a significant problem nationwide, reflecting deeper moral challenges within the country.
- Ignoring or criminalizing homelessness won't make it disappear; it requires genuine efforts and compassion to address.

### Quotes

- "Criminalizing something doesn't actually make it go away, see every prohibition ever."
- "Generally speaking, pretty much always it is cheaper to be a good person."
- "Nothing can be done but something was done."
- "The bigger problem is one of the moral character of the United States saying that we just can't solve it."
- "Imagine how tired they are living it."

### Oneliner

Austin's decision to recriminalize homelessness reveals a bigger national issue, showcasing the importance of compassionate solutions over punitive measures.

### Audience

Community members, advocates

### On-the-ground actions from transcript

- Advocate for social programs and shelters to address homelessness (suggested)
- Support organizations working to provide assistance and resources to homeless individuals (exemplified)

### Whats missing in summary

The full transcript provides deeper insights into the moral and social implications of criminalizing homelessness and the urgent need for compassionate solutions on a larger scale.

### Tags

#Homelessness #SocialJustice #CommunityPolicing #Austin #Texas


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today we're going to talk about a vote in Austin, Texas,
Travis County, and the unintended consequences of that.
We're going to talk about seeing it.
We're going to talk about how this little vote in Austin
actually kind of highlights an issue going on nationwide.
And it may not be the one you think, although that's an issue nationwide as well.
The city of Austin just voted to, well, ban being homeless, recriminalize it.
Just a point of order here.
Criminalizing something doesn't actually make it go away, see every prohibition ever.
And that was the big problem.
People could see it. People could see it. They didn't want to see it.
So the answer
was to recriminalize it. So now if you are camping
in public areas near downtown or near the university or
wherever they deem inappropriate I guess
well you'll be hit with a fine.
A fine for homeless people.
because you know homeless folk are known for having like stacks of cash laying
around. I would imagine if they could pay the fine they wouldn't be homeless. So
what's the actual consequences of this vote? They have more interaction with law
enforcement which means a whole lot of them are going to end up in the county
jail where the people of Austin will pay to house and feed them at a higher rate
than they would if they just, you know, built shelters or instituted social
programs meant to alleviate the issue. Generally speaking, pretty much always it
is cheaper to be a good person. But that's not what happened and I know
there are going to be some people who say, you know, a lot of them, well they
choose to be homeless. Yeah, sure some, but some isn't most and it is certainly not
all and those will end up being housed at taxpayer expense as well at a higher
rate because they didn't want to see it because the people in Austin didn't want
to see it. This isn't going to make it go away. The reason it was decriminalized
was because there was already a problem. Going back to the policy that kind of
helped create the problem isn't going to solve the problem. Moving forward will.
This is just like every other issue facing this nation where you have those
stuck in the past saying it can't be done, we can't solve this problem. It is
exactly like the unaccompanied miners at the border. The only solution is to close
down the border and go back to the old way of doing things. Nothing can be done
but something was done. But something was done. And it's the same thing. It's
logistics. It's time, resources, and the political will to do it. That's what it
takes to fix this. This is a major issue. Nationwide, people without houses, people
without shelter, that's a problem. The bigger problem is one of the moral
character of the United States saying that we just can't solve it. And I get it. I get
it. People are tired of seeing it. Imagine how tired they are living it. Anyway, it's
just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}