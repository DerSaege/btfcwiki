---
title: Let's talk about Charlie Brown and Trump supporters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pR8G5qSjZGk) |
| Published | 2021/05/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Illustrates the analogy of Charlie Brown and Lucy to Trump supporters waiting for certain dates and events, constantly being let down.
- Mentions the shifting dates from January 6th to May 15th, with promises of arrests that are unlikely to happen.
- Points out the wasted time spent chasing unfulfilled expectations, urging focus on community issues instead.
- Emphasizes the need for Trump supporters to redirect their efforts towards making a tangible difference locally.
- Encourages taking action in the community rather than getting swept up in baseless claims and divisive rhetoric.
- Urges Trump supporters to realize that their time and energy could be better spent on meaningful change.
- Stresses the importance of being present for one's family and community instead of being consumed by unattainable goals.
- Calls for a shift from online rhetoric to real-life actions that benefit those around them.
- Reminds individuals that making America great requires tangible efforts, not chasing elusive promises.
- Encourages reflection on the impact of one's actions on family and community relationships.

### Quotes

- "You want to make a difference."
- "Your community, your family, your country, they need you."
- "But I understand the drive."
- "Not chasing a football, because you are never going to kick it."
- "They're at their wits end."

### Oneliner

Trump supporters urged to focus on community issues and tangible actions rather than chasing unattainable promises, as real change starts locally.

### Audience

Trump supporters

### On-the-ground actions from transcript

- Redirect efforts towards addressing community issues (suggested)
- Take tangible actions to make a difference locally (suggested)
- Focus on real-life initiatives rather than online rhetoric (suggested)

### Whats missing in summary

The full transcript provides a detailed and empathetic perspective on the need for Trump supporters to shift their focus towards meaningful community engagement and tangible actions.

### Tags

#TrumpSupporters #CommunityEngagement #LocalAction #Empathy #PoliticalAwareness


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about Charlie Brown
and Lucy and talk to Trump supporters and we're going to talk about dates and we're going to do
this because if you are a Trump supporter, one of your family members reached out. I mean not
really one of your family members, statistically speaking it wouldn't be yours exactly, but
somebody just like one of them. We all know that image, Charlie Brown running towards that football
that Lucy's holding and every time that football gets moved at the last second. But every time
Charlie Brown, well he tries to kick it, gets let down every time.
You remember when everything was supposed to be shown to you on January 6th? Then it became
January 20th, right? Then March 4th, then April 21st, now it's May 15th. And now that that date's
approaching, you got Wood out there saying, you know those politicians in Georgia, they gonna get
arrested. No they're not. No they are not. And so we're going to talk about the dates and we're
going to talk about when that time frame passes, the football will get moved again.
It's never getting kicked. Now I mean realistically that they are politicians from Georgia so there is
a chance that they're going to get arrested. I mean that's, it's Georgia. I mean it is what it is.
It won't be for what Lynn said.
When you think about it though, January, February, March, April, May, now we're talking about June,
six months, half a year of your life wasted.
It's time that could have been spent with your family. And trust me,
they're at their wits end. They are at their wits end.
You know, if you haven't seen the channel before, we're not on the same team. Politically,
we are not on the same team. But I get it on some level. You understand that there are things wrong
with the country and you want them fixed. We disagree a lot about how to get there. But I
understand the drive. You want your country, your community to be better. Your community could use
your help now. Six months you've been chasing this football. There are things in your community that
need your attention now. And if you'd put six months worth of time to them, those problems
probably be solved by now. You want to make a difference. You want to make a difference.
Those problems will probably be solved by now. You want to make America great. You don't need a
leader. You can do it yourself. Most of the problems that you're really concerned about,
they're kitchen table issues, and a whole lot of them can be solved locally.
People who pointed you to these baseless claims and these wild theories, they're not your friend.
To them, you were a means to an ends. Something they could use to hopefully get what they wanted.
And they'll keep you motivated as long as they think they can use you. The whole idea
of chasing this football for more than half a year, at some point you have to realize that
Lucy is never going to let you kick it. It's not going to happen. And while you're chasing
this football, your family is growing further apart from you because you're consumed by this.
Your community, your family, your country, they need you. Not to spout crazy rhetoric
on Facebook, but to actually take a hand in making things better for the average person around you.
Yeah, it doesn't give you the social cred that saying stuff on Facebook does,
but it actually does make America great. If that's the goal, that's what you need to be doing.
Not chasing a football, because you are never going to kick it.
I want you to think what it would take for a daughter to reach out to somebody on YouTube
in hopes of making a video to appeal to somebody that realistically, if we met, we wouldn't be friends.
We would not like each other. They're at their wits end.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}