---
title: Let's talk about rebirth and the Affordable Care Act....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=aDyz9_Njzlw) |
| Published | 2021/05/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses recent news involving the Biden administration's directive to the Department of Health and Human Services regarding a rule change affecting the Affordable Care Act and Section 1557.
- Section 1557 prohibits discrimination in health care programs based on race, color, national origin, sex, age, or disability, with recent inclusion of orientation and identity due to a Supreme Court ruling.
- Beau stresses the importance of this rule change, mentioning that it fosters acceptance and is a step in the right direction.
- Many people mistakenly believe the impact is limited to confirmation surgery for transgender individuals, but discrimination can affect access to various types of healthcare.
- Beau shares a personal connection by mentioning a friend, Zoe Jane Halo, who is currently undergoing what she describes as a "rebirth surgery" to be more accepting in society.
- Zoe Jane Halo is someone active on platforms like Twitter and YouTube, discussing her experiences and journey openly.
- Beau encourages support for Zoe and acknowledges the significance of standing by friends during transformative moments.
- The message revolves around the importance of supporting friends going through significant life changes and the need for acceptance and understanding in society.

### Quotes

- "It's a good thing. It's a good thing. It's more accepting. It's the right move."
- "So why did I wait to talk about it? Because if I have timed this correctly, right now somebody is going through a bit of confirmation surgery."
- "A friend of mine, and maybe a friend of yours, if you are active in the comments here or on Twitter, Zoe Jane Halo is currently going through what she called a rebirth surgery."

### Oneliner

Beau addresses the Biden administration's healthcare rule change promoting acceptance, citing a friend undergoing transformative surgery for societal inclusivity.

### Audience

Supportive individuals

### On-the-ground actions from transcript

- Support Zoe Jane Halo on platforms like Twitter and YouTube (implied)

### Whats missing in summary

The full transcript provides a personal touch and a call for support and understanding for those undergoing significant life changes.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some old news.
Not really old news, it happened earlier this week, but I put off talking about it because
I wanted to time it with something else.
Wanted it to coincide with something.
The Biden administration announced that it had directed the Department of Health and
Human Services to make a rule change, something that directly impacts the Affordable Care
Act and Section 1557.
In relevant part that section says that health care providers can't discriminate based on
race, color, national origin, sex, age, or disability in certain health programs and
activities.
This because of a new Supreme Court ruling, well not new, but a Supreme Court ruling is
now going to be interpreted to include orientation and identity.
It's a good thing.
It's a good thing.
It's more accepting.
It's the right move.
When people talk about this, two things happen.
One, a whole lot of people don't really know anybody it applies to, at least they don't
think they do.
And they automatically assume that it is limited in effect to confirmation surgery, you know,
getting the surgery to match your identity.
Yeah, I mean that has something to do with it, but if you feel discriminated against,
you may delay other care.
You may put it off.
You may not get it at all.
So it has further impacts than just confirmation surgery.
So this is a really good move.
So why did I wait to talk about it?
Because if I have timed this correctly, right now somebody is going through a bit of confirmation
surgery.
A friend of mine, and maybe a friend of yours, if you are active in the comments here or
on Twitter, Zoe Jane Halo is currently going through what she called a rebirth surgery.
And the whole idea of this is to be more accepting, right?
That's where we want society to move to.
But there is that idea that I really don't know anybody that this impacts.
You do.
You probably do.
Especially on Twitter, if you follow on Twitter.
She is on YouTube and actually has a channel where she talks about a lot of this.
So I just wanted to kind of acknowledge it because when one of your friends goes through
something that, as she put it, is giving up everything you've ever been for a chance to
be everything you ever wanted, you want to support them.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}