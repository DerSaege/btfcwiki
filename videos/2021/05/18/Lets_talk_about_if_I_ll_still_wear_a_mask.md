---
title: Let's talk about if I'll still wear a mask....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Bmo9kE0wzqE) |
| Published | 2021/05/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Expresses frustration at Trump's lack of mask-wearing as a failure of leadership and civic duty to set an example for the American people.
- Stresses the importance of changing societal mindset over changing laws to effect real change.
- Mentions Biden administration's commitment to science-based guidance, indicating that fully vaccinated individuals have low risk and may not need masks in certain scenarios.
- Plans to continue wearing a mask personally as a visual reminder that the pandemic is not over and more people need to get vaccinated.
- Worries that without establishments checking vaccination status, unvaccinated individuals may exploit the lack of enforcement to avoid wearing masks, especially in areas with vaccine hesitancy.
- Believes in the power of visual cues like masks to encourage more people to get vaccinated and sees it as a social science issue.

### Quotes

- "You don't need a law to tell you to be a good person."
- "I will continue to wear mine. The guidance is that. It's guidance."
- "I really do believe that the social science would suggest more people would get vaccinated if they saw a bunch of masks."

### Oneliner

Beau stresses the importance of personal responsibility in wearing masks as a visual reminder to encourage vaccination, especially in areas with hesitancy.

### Audience

Individuals, Community Members

### On-the-ground actions from transcript

- Wear a mask as a visual reminder to encourage others to get vaccinated (implied)
- Encourage establishments to enforce mask-wearing or vaccination checks (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on the importance of continued mask-wearing as a visual cue to remind people about the ongoing need for vaccination and COVID-19 precautions.

### Tags

#MaskWearing #Vaccination #PublicHealth #Leadership #CommunityResponsibility


## Transcript
Well howdy there internet people it's Beau again. So today we're going to talk about the new
guidance on wearing a mask and we're going to talk about whether or not I'm going to continue
to wear a mask because a whole bunch of y'all have asked. But before we get into that I want
to go over a couple of things I've said over the last year because it heavily influences my decision.
I was really mad at Trump for not wearing a mask. Not because it really impacts me personally what
happens to his immediate circle but because the American people need a visual reminder
of what's going on. I saw it as a failing of leadership on his part. His civic duty was to
help set that example. That really bothered me about him. I've also said that to change society
you don't have to change the law you have to change the way people think.
The Biden administration made the pledge that they were going to base all guidance on the science
and that's what they're doing. The research indicates that if you are fully vaccinated
in these scenarios your risk is like super low so you don't need to worry about the mask. Cool.
Because of that they're going to reduce mandates. Fine. You don't need a law to tell you to be a
good person. Yeah I'm going to continue to wear mine because I do think that people need a visual
reminder that we aren't out of the woods yet. That we need to get a whole bunch of people
vaccinated still. That this isn't over. I'm also a little concerned that most establishments are
not going to ask whether or not you've been vaccinated. Which means a whole lot of people who
who haven't been vaccinated they will use that lack of concern to not wear a mask
so you will see even less. And I think until we are out of the woods I think it's a good idea
to have that visual reminder out there. Especially in areas where there's a lot of hesitancy
like around me. So yeah I will continue to wear mine. The guidance is that. It's guidance.
And the science says what it says. That science is related to the transmission.
I really do believe that the social science would suggest more people would get vaccinated
if they saw a bunch of masks. Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}