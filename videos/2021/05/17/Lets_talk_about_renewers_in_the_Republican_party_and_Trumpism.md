---
title: Let's talk about renewers in the Republican party and Trumpism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_g82dfkRlB4) |
| Published | 2021/05/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the topic of Miles Taylor, a former Trump official, and the concept of renewers advocating for a potential third party.
- Miles Taylor, known for the "I'm the resistance" op-ed, believes in splitting the Republican Party to appeal to rational individuals not radicalized by Trump.
- Taylor's vision involves forming a new party called the renewers, distinct from the radicals, to represent the rational Republicans.
- Beau stresses the importance of taking action now if those supporting a third party want to make a meaningful impact.
- He criticizes mere talk without action, urging involvement in midterms and launching candidates against Trumpist elements like Greene.
- Beau warns that unless beliefs are translated into action, being seen as rational holds no value.
- Individuals within the Republican Party advocating for change must act promptly as the current leadership relies on Trump's momentum for the midterms.
- To prevent a Trump-like figure from dominating, immediate action in forming a new party is necessary.
- Beau underscores the urgency by pointing out five years of talking without significant action.
- He concludes by encouraging prompt action before the critical period of 2022 to secure a place in history.

### Quotes

- "You've talked for five years. Five years. It won't mean anything unless you act."
- "Because as wonderful as it may be to see yourself as one of the rationals, if you don't convert that belief into action, it's worthless."
- "For this to mean anything, you have to act on it now."
- "You will determine your place by whether you get off the sidelines before 2022."

### Oneliner

Beau urges immediate action for Republican renewers to form a new party and make an impact before the 2022 critical period.

### Audience

Republican renewers

### On-the-ground actions from transcript

- Launch candidates against Trumpist figures like Greene before the midterms (suggested)
- Start forming a new party and take immediate action (implied)

### Whats missing in summary

The full transcript provides additional context on Miles Taylor's vision and the urgency for Republican renewers to act promptly to establish a distinct party identity and impact the upcoming political landscape.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Miles Taylor, a former Trump official.
We're going to talk about renewers and a potential third party.
Miles Taylor is a former Trump official.
He is the person behind that op-ed.
You know, I'm the resistance.
He has openly stated that the way forward for the Republican Party is to split, is to
break away.
He wants to appeal to rational people, to Republicans who didn't become radicalized
by Trump's rhetoric and those who want to look to a post-Trump future.
He wants to call, or he seems to want to call this party, the renewers, who are going to
renew the Republican Party.
And he says we want to give them that new consumer brand identity that says I'm different
from the radicals.
I'm one of the rationals and I wear that as a badge of honor.
We know there are millions of Americans that want to do that.
It's probably not a bad idea.
Probably not a bad idea, but you have to do it now.
You can't wait.
All of this talk that has come from the resistance within the Republican Party, it means absolutely
nothing.
No, unless there's action to go along with it.
If they want to have this third party, if they want to send a message to the Republican
Party, they have to do it now.
They have to get involved in the midterms.
They have to move to launch a candidate against Greene and all of the elements of Trumpism
that are still in office.
If they don't do that, it means nothing.
It means nothing.
All of the words, all of the obstruction that they say that they did for the betterment
of the country, it means nothing.
Because as wonderful as it may be to see yourself as one of the rationals, if you don't
convert that belief into action, it's worthless.
It is worthless.
You are in a position, if you are one of these people, or the hundred or so that are working
with that letter, or you're the guy over the elections out there in Arizona, if you're
somebody who is saying we have to move past it, you have to do it.
The Republican leadership isn't going to.
They're banking on Trump's momentum for that midterm.
And if it works, you're going to get somebody like Trump with his beliefs, with his tendencies,
that's a little more polished, a little more capable, harder to resist.
For this to mean anything, you have to act on it now.
You have to start the party now.
You've talked for five years.
Five years.
It won't mean anything unless you act.
And if you are so motivated by Trump's actions to believe the things that are being put out
by the renewer side of things, you know that this period will be recorded in American history.
You will determine your place by whether you get off the sidelines before 2022.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}