---
title: Let's talk about asking the right question and debating the wrong one....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1szx-TRWF2U) |
| Published | 2021/05/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the importance of asking the right question instead of focusing on the wrong question.
- Presents a story about a reporter named Alexandra Peterson whose apartment is blown up by a SWAT team.
- Points out the issue of debating whether Alexandra knew about the illegal activities happening below her apartment.
- Emphasizes that the real question should be about why the SWAT team used excessive force and caused additional damage.
- Expresses concern about society shifting blame to collateral damage victims instead of questioning law enforcement's actions.
- Criticizes the nation for getting distracted by irrelevant debates instead of addressing the actual problem of excessive force.
- Suggests that the country has lost sight of critical issues by engaging in misguided debates.
- Urges for a focus on asking the right questions amidst various ongoing debates and controversies.

### Quotes

- "The question is not whether or not Alexandra knew about what was happening in her building. The question is why did the SWAT team use too much force?"
- "I don't think that when a nation begins debating that amongst themselves, rather than discussing the obvious problem of too much force being used, I don't think that sits well for the future of that nation either."
- "There are a lot of questions being asked, and almost none of them are the right question."

### Oneliner

Beau stresses the importance of asking the right questions, focusing on why excessive force was used by law enforcement rather than blaming collateral damage victims.

### Audience

Advocates and Activists

### On-the-ground actions from transcript

- Question law enforcement's use of excessive force (exemplified)
- Shift focus to addressing the real issues instead of irrelevant debates (exemplified)

### Whats missing in summary

The emotional impact and Beau's tone throughout the talk.

### Tags

#AskingTheRightQuestions #PoliceBrutality #ExcessiveForce #SocialJustice #NationalDebates


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
asking the right question and whether or not we have fallen into the habit of trying to
get the right answer to the wrong question because it seems to be occurring a lot, especially
in national discussions. Debating an irrelevant question, that in and of itself is not a big
deal. The problem is that when it happens on a national level, the right question, the
question that actually needs to be answered, well that gets overlooked. So I'm going to
present you with a story and you tell me whether or not the right question is being asked.
There's a woman, we're going to say her name is Alexandra Peterson. Alexandra Peterson
is a reporter. She lives in this apartment and the people below her, well they're up
to something nefarious, something illegal, it doesn't really matter what it is, but they're
doing something wrong. Local law enforcement finds out about it and they decide to dispatch
a SWAT team. And when the SWAT team goes in, well, they use too much force and they wind
up blowing up Alexandra's apartment in the process. And then local law enforcement walks
out and says, you know what, Alexandra's a reporter, she probably knew what was happening
downstairs. She knew. And then the country debates whether or not Alexandra knew. That
doesn't seem like something that would happen not in this context, right? That would be
ridiculous. You generally don't hold collateral responsible for becoming collateral. That
doesn't even make sense. The question is not whether or not Alexandra knew about what was
happening in her building. The question is why did the SWAT team use too much force?
Why was the SWAT team so willing to accept the additional damage? That's the question.
That's the question that needs to be discussed. I'm not sure when we crossed over into the
idea that not just is all of the additional damage okay, but those who suffer it, well,
it's their fault. That doesn't sit right with me. I don't think that's the right question.
I don't think that when a nation begins debating that amongst themselves, rather than discussing
the obvious problem of too much force being used, I don't think that sits well for the
future of that nation either. It has lost sight of things that should matter. If the
entire United States is debating whether or not Alexandra knew, nobody is paying attention
to the fact that the SWAT team used too much force. I think that there might be a lesson
in that for the entire country right now, because there are a lot of things being debated,
there are a lot of questions being asked, and almost none of them are the right question.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}