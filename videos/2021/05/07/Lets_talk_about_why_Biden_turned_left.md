---
title: Let's talk about why Biden turned left....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TnYEZNoKU-0) |
| Published | 2021/05/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the context of the American left and social democracy.
- Analyzes Biden's shift towards social democracy.
- Suggests possible reasons for Biden's leftward turn: legacy concerns, ideological beliefs, or political strategy for a second term.
- Considers Biden's move in response to the progressive shift in the United States.
- Points out the Republican Party's internal conflicts and lack of inspiring ideas.
- Speculates that Biden may be pushing left because he can, given the disarray in the Republican Party.
- Clarifies that social democracy is still capitalism with a focus on providing essentials for survival.
- Presents a choice between moving towards social democracy or descending into fascism for the United States' future.

### Quotes

- "Social democracy is still capitalism. It's capitalism with a smiley face."
- "More people have more of the things they need to survive."
- "We can be like Denmark or we can devolve into fascism."

### Oneliner

Beau examines Biden's shift towards social democracy, suggesting it could be driven by legacy concerns, ideological beliefs, or a strategic move for a second term, amidst a backdrop of Republican disarray and a progressive shift in the United States.

### Audience

Political observers

### On-the-ground actions from transcript

- Choose to support policies that prioritize providing essentials for survival (implied)
- Stay informed about political shifts and ideologies (implied)
- Engage in political discourse and decision-making processes (implied)

### Whats missing in summary

Insight into the potential long-term implications of Biden's move towards social democracy.

### Tags

#Biden #SocialDemocracy #Legacy #PoliticalStrategy #ProgressiveShift


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some questions that came in about Biden.
They're all a little different, but they're all pretty much the same in tone and substance.
But before we start, I want to provide a caveat for those of you outside the United States.
In the context of this conversation, when I say left, I mean American left.
So social democracy, that's what we're talking about.
If you are outside of the U.S., that's probably center, maybe even conservative, but that's
what we're talking about here.
There aren't a lot of mainstream American politicians that go beyond that.
We're talking about AOC and Sanders, not Marx, alright.
So the question basically is Biden ran as being the status quo, nothing will fundamentally
change guy.
Why is he turning left?
I think there's a few possible options.
Now again, if you're a radical, you don't look at Biden and see anything even remotely
left because he isn't crossing into true leftist territory.
He's moving towards social democracy.
But I think the options are kind of simple.
The first one could be he's old.
He's been in politics a long time.
He's looking back at the legacy he's going to leave behind and the things that he would
be known for if he doesn't accomplish something major.
They're not really things you want to be known for.
You don't want that to be your legacy.
So he's trying to fix it.
That's an option.
Another option is that he's older.
He thinks he has one term in him and he is going to try to make the best of it for ideological
reasons that we didn't know that he had.
That's a possibility.
The third option, which is the one I'm leaning towards, is that he wants what every first-term
American president wants, a second term.
He's looking at the polling information and he realizes that with every passing day, the
United States becomes more progressive and less conservative.
And he is just setting the tone for the Democratic Party to move towards social democracy because
he sees that as being his best bet to obtain a second term.
That seems like the most likely answer.
Now he could be doing this for his own personal ideological reasons, things that we didn't
know he believed.
That's possible.
But my guess is that it's politics.
He can be known as the candidate who steered the Democratic Party towards successive victories
because that's where this country is headed.
So then we also have to consider one other thing that may factor into this.
He may be doing it because he can.
He may be doing it simply because he can.
Right now, the Republican Party is in complete, total, and utter disarray.
They have so much infighting that they can't get anything accomplished.
Right now they're focused on ousting Cheney because a Cheney isn't conservative enough
anymore.
And then you have the Senate Majority Leader who basically came out and said that his platform
was just to not like whatever the president does.
That doesn't really inspire people.
Right now the Republican Party is the party of no ideas.
So even if the Democrats push a bad idea, it's going to get more support because it's
going to be more inspiring.
He may just be doing it because he can, talking to his advisors and getting a wish list and
seeing what he can push through.
I personally think that it is he wants a second term and he wants that FDR-style legacy.
I think it's a blend of all of this.
But he won't get that legacy unless he can get that second term.
So I think that's what he's moving towards.
Now if you are a conservative, understand, I know that any time the word social is in
something you are picturing the Soviet Union.
Social democracy is still capitalism.
It's capitalism with a smiley face.
More people have more of the things they need to survive.
That's what social democracy is.
It doesn't actually cross into leftist territory.
And you can oppose it if you want.
You can kick and scream, claw at the ground.
But unless this country descends into fascism, it is headed towards social democracy.
Those are our two futures.
So you can choose.
We can be like Denmark or we can devolve into fascism.
Anyway it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}