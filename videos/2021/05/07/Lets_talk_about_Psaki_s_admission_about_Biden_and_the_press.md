---
title: Let's talk about Psaki's admission about Biden and the press....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=mV_8ywZ6oy8) |
| Published | 2021/05/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the controversy surrounding Press Secretary Psaki's admission about not wanting the president to take impromptu questions from reporters.
- Points out that the context provided by Fox News reveals that the president does, in fact, take impromptu interviews, which Psaki does not appreciate as it makes her job harder.
- Emphasizing that a press secretary's role is to provide information from the president to the press, and Biden going off script complicates this process.
- Stating that the president's main job is not to entertain tabloid news outlets but to be the chief executive, making decisions based on expert advice within the confines of Congress.
- Comparing Biden's approach to press interactions with Trump's, suggesting that Biden's controlled communication style is more appropriate for a president.
- Arguing that Biden should carefully think out his statements as they have global impacts, contrasting this with Trump's tendency to make unfounded statements that caused confusion and negative impacts.
- Concluding that Biden should focus on running the country while Psaki handles press interactions effectively.
- Praising Biden and Psaki for maintaining a good flow of information despite occasional impromptu interviews.

### Quotes

- "His job is not to provide fuel and be a sideshow for tabloid news outlets."
- "Every statement that comes out should be well thought out, because it has impacts all over the world."
- "Biden should totally not talk to the press that much. That's not his job."
- "His job is to run the country. His press secretary, well, that's her job."
- "I think they're doing a pretty good job of maintaining a flow of information."

### Oneliner

Beau addresses Psaki's admission on managing press interactions and underscores the importance of thoughtful presidential communication for global impacts, contrasting Biden's approach with Trump's.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Trust reputable news sources for comprehensive reporting (suggested)
- Understand the roles of government officials in communicating with the press (implied)

### Whats missing in summary

Insights on the importance of responsible and calculated communication strategies in political leadership and media relations.

### Tags

#PressSecretary #Communication #PresidentialResponsibility #MediaReporting #PoliticalContrast


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about Press Secretary Psaki's
admission, because she said something
and it has the right wing tabloids all sorts of worked up.
Got a message basically saying, I
notice you haven't talked about Psaki's admission,
that she tells the president not to take questions
from reporters.
What does that say about his fitness?
OK.
So that came from Fox News, undoubtedly,
because admission is in the headline
and also in the message.
I'm just going to start off by saying you're
going to get more substance in your reporting
if you were reading the high school newspaper from Bayside
High.
But if you just feel compelled to read Fox,
you have to read more than the headline.
The context, which surprisingly is actually in the Fox News
article, is that she was asked how
she felt about the fact that the president did, in fact,
take impromptu interviews in the halls.
So he does do it and she doesn't like it, which makes sense
because she's the press secretary.
That's literally her job.
Her job is to take information from the president of the United
States and provide it to the press.
That's what she does.
President Biden, going off of the talking points,
makes her job harder.
So she probably doesn't like that.
I wouldn't.
If I was his press secretary, I would also
tell him not to talk to the press.
That's like her whole purpose for being.
I would point out that the president of the United States
is, in fact, the president of the United States.
His job is not to provide fuel and be
a sideshow for tabloid news outlets
to keep their ratings up.
His job is to be the chief executive of the United States,
which means taking advice from thousands of subject matter
experts and that advice being distilled down into reports
and making decisions based on those reports
as provided within the confines of what Congress has allowed.
That's his job.
His job is not to be out in front of the press
all the time.
I know that may be hard to believe
after the last four years, but the president actually
shouldn't be out ad-libbing.
What does it say about his fitness?
That he understands the job better than Trump?
That he hired better advisors?
A whole lot of stuff, none of which
is where I think you were going with this.
But if Trump had the same sort of press secretary
who actually wanted to manage their duties well,
we probably wouldn't be arguing over
whether or not Trump said something less than accurate
about disinfectants.
Or I don't know, there probably wouldn't
be confusion about whether or not a magic Sharpie can
change the course of a storm.
The president's words impact stuff all over the world.
He shouldn't ad-lib.
He shouldn't respond to impromptu interviews.
He shouldn't answer from the hip.
Every statement that comes out should be well thought out,
because it has impacts all over the world.
It's not a job for somebody who likes to just ramble on,
who likes to make unfounded statements,
who likes to say things to get a rise out of people.
That actually has really negative impacts,
as we saw for the last four years.
Yeah, Biden should totally not talk to the press that much.
That's not his job.
His job is to run the country.
His press secretary, well, that's her job.
So I think they're doing well.
I mean, the obvious impromptu interview
that they were talking about that did happen, I mean,
that aside, I think they're doing
a pretty good job of maintaining a flow of information.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}