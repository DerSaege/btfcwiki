---
title: Let's talk about Biden's plan for the interpreters and more....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U37WMmZ4r_M) |
| Published | 2021/06/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration announced plans to airlift and process visas for individuals who helped the US and NATO in Afghanistan over the last 20 years, including interpreters.
- Concern arises as the Afghan opposition intensifies their offensive, with predictions that the capital may fall within six months.
- Without a token security force in place, the withdrawal from Afghanistan may lead to turmoil and political repercussions.
- Representative Tom Malinowski suggests that helping the Afghan government survive the onslaught is vital to protect those at risk.
- The politicization of the situation may lead to calls for continued US involvement in Afghanistan to prevent a resurgence of conflict.
- Beau warns against a hasty return to military engagement in Afghanistan without a sustainable plan for assistance.
- He advocates for regional security forces and countries vested in Afghanistan's stability to take the lead in providing ongoing support.
- Beau underscores the importance of being prepared to oppose future wars in similar contexts to prevent repeating past mistakes.

### Quotes

- "Leaving them there is leaving them there to be lost."
- "Without a token security force there, this is going to happen."
- "The only way to do that is with force and lots of it."
- "A regional security force, neighbors, countries that are vested, that's who needs to be behind this, not the United States."
- "You need to be ready to oppose the next war before it starts."

### Oneliner

Beau analyzes the Biden administration's plans for Afghan interpreters amidst escalating conflict, warning against hasty US military involvement without sustainable regional support.

### Audience

US citizens, anti-war activists

### On-the-ground actions from transcript

- Mobilize against future wars (implied)
- Support regional security forces for stability in Afghanistan (implied)

### Whats missing in summary

Importance of sustained regional support to prevent future conflicts. 

### Tags

#Afghanistan #USMilitaryInvolvement #Interpreters #RegionalSupport #AntiWar


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the Biden administration's
announcement on what they're going to do,
how they're going to deal with and help
the interpreters over there.
The term interpreters is being widely used.
But to be clear, this does encompass more than just
interpreters.
These are people over there who helped the United States,
helped NATO over the last 20 years.
And leaving them there is leaving them there to be lost.
The Biden administration has announced
that they will airlift them out and take them
to either a third country or a US territory
where the visas will be processed.
They will be in camps until then.
The visas will be processed.
So is that a workable plan?
Is it a good idea?
I, of course, think it's a great idea.
As far as I know, they have not named it Operation New Life 2,
though.
Is it workable?
They're saying they want to get them out by August.
Yeah, they can do that.
And they probably have that much time
because there is another development in Afghanistan
people should be aware of because it
has far-reaching consequences.
There was concern that the opposition
would take the withdrawal as a sign to go on the offensive.
They have.
The most recent assessment suggests
that the capital may fall within six months.
If the current rate of the offensive keeps up
and it does intensify at the rate that it has been,
I don't give them six months.
I don't think they got that much time.
August should be OK.
They should be able to get them out by then.
I think they'll be all right there.
However, it is being politicized now.
People are talking about it being Biden's Saigon moment.
The withdrawal was part of the deal.
It wasn't a good deal, but it was a deal that was made.
It was made before Biden.
Biden actually stayed longer.
Withdrawing in the manner that we are, actually, I've said it.
It's not good.
Without a token security force there, this is going to happen.
As of yet, while there have been tentative agreements
and countries suggesting that they're going to try to help,
there's no boots on the ground.
There is no token force on the ground
to replace the US forces as they leave.
The offensive will probably be successful.
But because it is being politicized,
we have something else happening.
I was concerned that if things started to go bad,
it would be used as an excuse to go back in.
I quote, the best way to help the great mass of Afghans
who are at risk is to help the Afghan government survive
this onslaught.
That is from Representative Tom Malinowski,
Democrat from New Jersey.
We're not even out yet, and the idea of continuing assistance,
going back in, it's being floated.
For those who were all about getting out
as quickly as possible, once we're out,
you have to stay committed, or we will end up going back in.
Politically, it's going to look bad for Biden.
And I know there will be debates,
and people are going to say it's not his fault,
because it is.
I don't believe it is.
His, the deal was made before he took office,
and he stayed longer than the deal allowed.
But not having a token security force before withdrawing,
that's what this is going to cause.
When the footage starts to come out,
there will be those who say, we were there for 20 years
and accomplished nothing.
We can't let all of our hard work go down the drain.
We need to go back in.
It's going to happen.
And they'll mainly be doing it to try
to deflect political blame.
If you're anti-war, you're anti-imperialism,
you need to be ready to get mobilized.
You need to be ready to oppose the next war before it starts,
because it very well might be in the same spot.
As far as getting the people out the way they plan on doing it,
it is a good idea.
It's the right move.
It's the right way to do it.
Assisting, helping the Afghan national government
survive this offensive, the only way to do that
is with force and lots of it.
So unless you're willing to commit US forces to being
continually involved in Afghanistan,
it's not the right play.
A regional security force, neighbors,
countries that are vested, that's who needs to be behind
this, not the United States.
Because if the US does it, they'll go in, stabilize,
and when they leave, the same thing's going to happen again.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}