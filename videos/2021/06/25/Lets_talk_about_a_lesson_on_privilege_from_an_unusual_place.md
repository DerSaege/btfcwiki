---
title: Let's talk about a lesson on privilege from an unusual place....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zCPfQGe-nyg) |
| Published | 2021/06/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about privilege and perception based on statistics.
- Demonstrates how privilege influences perception of reality.
- Mentions the impact of exposure to certain facts on awareness.
- Points out the misconception of being in a post-pandemic state in the United States.
- Provides statistics on hospitalizations and losses due to COVID-19.
- Emphasizes the different impacts of the pandemic on vaccinated and unvaccinated individuals.
- Raises awareness about the disparity in vaccine distribution between wealthy and poorer countries.
- Comments on people's unawareness of the unequal vaccine distribution and its impacts.
- States that the pandemic is far from over globally, despite improvements in some countries.
- Concludes by discussing how privilege can make people unaware of their own advantages.

### Quotes

- "Your reality is pretty broadly defined by what you see around you every day."
- "We see recovery. We see things getting back to normal. Everything's lessening here, but just here."
- "It's a privilege."
- "That's how privilege works."
- "Sometimes you're completely, completely unaware of the fact that you have it because you don't see the other side of it every day."

### Oneliner

Beau talks about privilege, perception, and the global impact of the ongoing pandemic while shedding light on the unequal distribution of vaccines and its implications.

### Audience

Global citizens

### On-the-ground actions from transcript

- Advocate for equitable vaccine distribution (implied)
- Stay informed about the global status of the pandemic and support initiatives for fair vaccine access (implied)
- Educate others about the disparities in vaccine distribution (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the global disparities in COVID-19 vaccine distribution and challenges viewers to reconsider their privilege and awareness of global issues.

### Tags

#Privilege #Perception #COVID19 #VaccineDistribution #GlobalHealth


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about some statistics.
And we're going to use those statistics to demonstrate
something about privilege.
Privilege is widely based on perception.
A lot of people don't realize they have a certain privilege.
You know, your reality is pretty broadly defined by what you see around you every day.
So if what you are exposed to eliminates certain facts, you don't see them, well, it doesn't
exist because you're not aware of it.
Okay.
So in the United States, right, we are in post-pandemic mode.
Pandemic.
It's a global pandemic.
It's what everybody's been saying.
But if you go to any news search engine and type in post-pandemic, you're going to get
article after article after article suggesting that it's over, right?
It's not true.
It's not over.
Last month, there were 853,000 hospitalizations, 18,000 lost.
So in the United States, you have two different kinds of pandemic happening.
Same cause, but they're impacting in a different way.
If you're like me, you're fully vaccinated and you still take precautions because you
like to look like a cowboy in the store so you still wear your mask, you're kind of an
observer at this point, even with those numbers, because out of those 853,000 hospitalizations,
99% were unvaccinated.
Out of the loss, 99% were unvaccinated.
Really doesn't impact you very much if you're part of that group.
You know, you're probably not too worried about it anymore.
Now if for whatever reason you have chosen to be unvaccinated, you probably should still
have some concerns, but the numbers are dropping.
So you're probably a little more relaxed because we're post-pandemic.
But here's the part about it.
We see recovery.
We see things getting back to normal.
Everything's lessening here, but just here.
Even in other wealthy countries and upper middle class countries.
The reality is it's June.
It's June and so far in 2021, we have lost more than we lost in all of 2020.
It's not over.
Not for the world, just for us, because we happen to have won the geographic lottery.
We live in a country that is wealthy.
So we had access to things that we assumed everybody else had.
The reality is it's nowhere near over.
It is nowhere near post-pandemic.
It's still going on.
It's going to go on for a while.
85% of all doses of those vaccines that have been given have been given to wealthy countries
or upper middle class countries.
And you can bet the other 15% that went to the poorer countries, they went to the wealthy
in those countries.
It's a privilege.
I'd be willing to bet that most people don't know that.
Don't know that there is that much disparity in how the vaccines are being distributed
and don't know the impacts.
I would imagine most people would be surprised by the idea that we've already lost more than
we lost last year.
That's how privilege works.
Sometimes you're completely, completely unaware of the fact that you have it because you don't
see the other side of it every day.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}