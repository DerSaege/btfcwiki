---
title: Let's talk about Republicans and not wanting to work....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yzN3ASVXlj0) |
| Published | 2021/06/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governors claim people don't want to work, cutting unemployment benefits.
- Mississippi cut $27 million a month in unemployment benefits, impacting 90,000 people.
- Businesses struggling to reach 50% staffing may have other issues besides people not wanting to work.
- Matt Roberts of Shaggy's Biloxi Beach found a solution by raising starting wage to $15/hour and offering benefits.
- Roberts' approach led to full staffing without insulting potential employees.
- People may not be lazy but tired of being exploited.
- States facing economic issues should look at policies and politicians responsible.
- Emphasis on policy over party is necessary for addressing economic challenges.
- Outdated ideas of blaming the poor for economic issues may no longer be politically viable.
- The world is moving into an era of change where old ideas won't suffice.

### Quotes

- "Imagine that it's not people just being lazy, it's just people being tired of being exploited."
- "Maybe it's time to start looking at policy over party."
- "We are moving into an era where there are gonna be a lot of changes."
- "Let's just blame the poor people, that's probably not gonna work much longer."
- "Y'all have a good day."

### Oneliner

Governors cutting unemployment benefits blame lack of work motivation, but a Biloxi business's success by raising wages shows it's exploitation, urging policy over party for economic solutions in changing times.

### Audience

State Residents, Workers

### On-the-ground actions from transcript

- Advocate for policy changes that prioritize fair wages and benefits for workers (implied).
- Support businesses that implement fair wages and benefits for employees (implied).

### Whats missing in summary

The full transcript provides additional context on the impact of outdated policies and the need for a shift towards prioritizing workers' rights and fair compensation.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're gonna talk about those people that just don't want to work,
cuz we know there's a lot of them.
Been hearing all these governors tell us that,
that people are just unwilling to go back to work.
They are so committed to this talking point that they have voluntarily given up
the unemployment that some of their citizens are relying on,
that additional unemployment, it's 300 bucks a month.
Mississippi was one of the first ones to do this.
There were 90,000 people in Mississippi, roughly, getting that 300 bucks a month.
That's about 3% of the population, more or less.
If there are businesses that can't get to 50% staffed,
maybe that's not the reason.
Maybe that's just something to blame it on, and
maybe that's not actually what's going on, and that seems to be the case.
The governor decided that he wasn't gonna do this,
this wasn't gonna happen anymore.
I want you to imagine being so secure in your political future,
and having the population of your state so
convinced to vote party over policy, that you can say,
I voluntarily took $27 million a month out of the economy because of a talking
point, and not be worried about your re-election chances.
Mississippi is not known as a state that is incredibly wealthy.
It's not a state that is known for having a whole lot of excess funding.
Now, there is at least one person in Mississippi that has
figured out the real solution to this problem.
Matt Roberts, he's the general manager at Shaggy's Biloxi Beach.
They're fully staffed.
They don't have any problem finding people,
because they went a different route with it.
Rather than screaming at potential employees and calling them lazy and
worthless, well, they just decided to raise their starting wage to $15 an hour and
offer medical and dental and vision, and they're fully staffed.
Imagine that.
Imagine that it's not people just being lazy,
it's just people being tired of being exploited.
It could be that simple.
Seems to make a little more sense, given all of the evidence at hand.
It might be time for people in states that are constantly
having economic issues to perhaps realize that the politicians
that enact a bunch of bad policies might be at least partially to blame for that.
Maybe it's time to start looking at policy over party.
At the end of the day, as we leave the situation we've been in for
the last year, as the world gets restarted,
we are moving into an era where there are gonna be a lot of changes.
And sticking to ideas from, well, 30 years ago and
more, where let's just blame the poor people,
that's probably not gonna work much longer.
It's probably not gonna be politically tenable because the policies have been so
bad, there are a whole lot more poor people.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}