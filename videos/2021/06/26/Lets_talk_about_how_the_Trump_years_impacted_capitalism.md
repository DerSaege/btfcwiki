---
title: Let's talk about how the Trump years impacted capitalism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=v31NNiZDZAs) |
| Published | 2021/06/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Acknowledging a contribution former President Donald J. Trump made to the United States, inadvertently shifting views on capitalism and socialism.
- Describing the United States as predominantly right-wing, even the Democratic Party is viewed as center-right, not left or leftist.
- Explaining the difficulty of discussing left-leaning ideas in the U.S. due to negative connotations associated with terms like socialism.
- Noting that Trump's actions, by breaking norms and using terms like socialism, sparked a curiosity that led to understanding leftist ideologies.
- Mentioning a shift in the Republican Party's views on capitalism, with more young Republicans wanting policies to reduce the wealth gap.
- Pointing out that Trump's rhetoric unintentionally helped mainstream socialism by trying to villainize it.
- Emphasizing how Trump's actions inadvertently widened the discourse on alternatives to capitalism.
- Recognizing that Trump's unintended consequences may have long-lasting effects on the dynamics of the GOP and political ideologies in the future.

### Quotes

- "He altered that dynamic because he broke an unspoken rule and he called people socialist."
- "It helped mainstream socialism. It helped mainstream that idea."
- "He didn't mean to. But at the end of the day, when you look at how the Trump years were for the capitalist class, I mean, yeah, overall it was pretty well."

### Oneliner

Acknowledging Trump's unintentional contribution to mainstreaming socialism by sparking curiosity and understanding leftist ideologies, altering Republican views on capitalism.

### Audience

Political Observers, Young Republicans

### On-the-ground actions from transcript

- Engage in political discourse with young Republicans to understand their shifting views (implied).
- Support policies aimed at reducing the wealth gap to resonate with changing perspectives (implied).

### Whats missing in summary

The full transcript provides a nuanced analysis of how Trump's rhetoric unintentionally influenced shifts in views on capitalism and socialism, especially among young Republicans.

### Tags

#DonaldJTrump #Capitalism #Socialism #RepublicanParty #PoliticalIdeologies


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are, uh...
We're going to give credit where credit is due.
You know, when somebody accomplishes something,
even if they didn't mean to,
you want to acknowledge it.
You want to say, hey,
you did this. Good for you.
So today,
we are going to thank
former President Donald J. Trump for something.
We are going to acknowledge
a contribution that he made to the United States
that many have tried and many have failed
to accomplish.
We are going to thank dear leader.
So, the United States is a pretty right-wing country overall.
It's so right-wing, in fact,
that, uh...
the American left,
the major party that is viewed as the left,
is actually still right-wing.
It's center-right. The Democratic Party
is not left. It's not leftist.
It's center-right.
Even those who are viewed
as, uh...
the far left
in the Democratic Party
are generally still center-right.
Somebody like AOC.
Taxing capitalism
to provide people the basic needs that, you know, they have.
That's social democracy.
It's still right-wing.
Now, it's close to actually crossing over into leftism,
but it's still
right-wing.
Because of this fact,
it was really hard to discuss
anything left-leaning
in the United States.
You couldn't really do it. You couldn't talk about leftism in the U.S.
because people
heard words like socialism
and they had a connotation that was associated with it
that was based on rhetoric and propaganda.
They didn't have an understanding of the ideology.
It made it impossible
to talk about leftist causes.
Real leftists.
Couldn't do it.
And that had a whole lot to do with people being firmly rooted
in a positive view of capitalism.
They looked at capitalism
and
they heard the parts that
appealed to them.
If you work hard enough, everybody's going to get what they want, and blah blah blah.
Donald Trump changed all that.
Donald Trump
altered that dynamic because he broke an unspoken rule
and he called people socialist.
He talked about leftists.
Because of that, people wanted to understand
what those terms mean.
It made it possible to discuss it.
It also
is altering the Republican Party.
The party of millionaires and billionaires and temporarily embarrassed
millionaires and billionaires.
It's changing the way they view things.
In 2019,
under Trump,
there was a poll conducted
asking young Republicans
how they viewed the word capitalism.
And 81%
had a positive view of capitalism.
Two years later, today,
that number is down to 66%.
And it's not just
a
desire to be
distanced from that word.
It's not an anecdotal thing. It's not
based on propaganda.
There's some understanding of an injustice.
Because today,
56% of young Republicans, aged 18 to 34,
want the government to pursue policies to reduce the wealth gap.
That's not
super capitalist at all.
A lot of this is due to Trump.
Because he talked about these terms. He used them.
People wanted to understand him.
We joked about it.
I know we did.
We all said that he was doing it. He was accomplishing more than most leftists.
Turns out it was true.
In 2019,
overall, all Americans,
most,
had a positive view of capitalism. 58%.
Today,
it's not even half.
49% have a positive view of capitalism, react positively to that word.
These are pretty big shifts.
These are notable.
And, in large part, it's due to Trump.
Him calling things left and radical left.
Socialism.
His ignorance of ideology
and branding things
whatever he wanted. Apparently socialism is when the government does stuff.
It helped mainstream these words. Now, to be clear,
99.9% of the time
when Trump called something socialism,
it wasn't.
It wasn't.
But, it helped familiarize the American populace with these terms.
Now, the other thing that played into this
was that whole giant public health thing that we had.
And the
wealthier among us in the United States,
they kind of
followed Trump's lead
on the rhetoric.
They leaned into the idea that previous propaganda would make these terms
something that people wouldn't accept. They wouldn't want to be associated with them.
So they came out and, I mean, mask off,
said some pretty wild stuff.
Yeah, we know everything is really bad out there, but you need to get to work.
Nobody cares about grandma.
Sacrifice yourself on the altar of the stock market.
Grandparents should be willing
to go
to preserve this system.
That didn't sit right with a whole lot of people.
That combined
with
Trump
saying that this was the alternative,
it helped mainstream socialism.
It helped mainstream that idea.
He didn't mean to.
We all know that.
But at the end of the day,
when you look at how the Trump years were
for the capitalist class,
I mean, yeah, overall it was pretty well,
it went pretty well for them because, you know, he did a lot for them with tax
breaks and stuff like that,
but he helped expose people to alternatives.
He widened
the discussion
by trying to villainize it.
Like most things
the president accomplishes,
he didn't mean to.
But at the same time, the fact that fifty-six percent
of young Republicans
want policies
to reduce the wealth gap.
That's notable. That's something that,
yeah, it's still going to be right-wing stuff, it's going to be social democracy,
but young Republicans
are warming to these ideas.
And it's because of Trump.
It's because Trump put that rhetoric out there.
Trump cast himself
as the anti-socialist,
and because so many people
were anti-Trump,
the alternative was socialism.
And people are leaning into it now.
It's interesting.
It's definitely worth noting,
and it's something that will probably alter the dynamics
of the GOP
ten years from now.
It's going to be really hard
to
paint yourself as the party of the working man
when people
understand that the policies being pursued
are
those that are designed
to keep them where they're at.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}