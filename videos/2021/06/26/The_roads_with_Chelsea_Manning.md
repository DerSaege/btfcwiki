---
title: The roads with Chelsea Manning....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=j4d7FODMr0g) |
| Published | 2021/06/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Chelsea Manning's shift to creating STEM-focused content on YouTube, aiming to provide nuanced, politically-informed educational videos on technical topics.
- The importance of educating people on complex technical subjects like cryptocurrency and artificial intelligence in a simple, easy-to-understand manner.
- Chelsea Manning's emphasis on the societal impact of technology, addressing common misconceptions and misinformation from various political perspectives.
- The challenge of bridging the gap in technical literacy between different levels of understanding among the general audience.
- Chelsea Manning's journey in learning video production from scratch, including taking film classes and receiving support from a professional editor.
- The significance of community support in making the YouTube channel project self-sustaining through Patreon subscriptions.
- Chelsea Manning's reflections on the pressure and expectations surrounding the launch of the YouTube channel, including considerations related to being a trans public figure.
- The importance of community impact and education over viral success in content creation, focusing on making a difference regardless of viewership numbers.
- Chelsea Manning's message about taking small actions every day to make the world a better place, regardless of the scale or magnitude of the effort.

### Quotes

- "Do one thing every day to change the world and make it a better place."
- "Everybody is in a position to do the small stuff."
- "Do what you can, when you can, where you can for as long as you can."

### Oneliner

Chelsea Manning shifts focus to STEM education on YouTube, aiming to simplify complex technical topics with a nuanced political perspective to bridge the gap in technical literacy for a broader audience.

### Audience

Creators, educators, activists

### On-the-ground actions from transcript

- Start a community education project on STEM topics (implied)
- Support creators through platforms like Patreon (exemplified)

### Whats missing in summary

The emotional impact of Chelsea Manning's journey in creating educational content and the role of community support in making the project possible.

### Tags

#ChelseaManning #STEMeducation #YouTube #CommunitySupport #TechLiteracy #SocialImpact


## Transcript
Well, howdy there, Internet people.
It's Bo again.
And today we are, we have a special guest that we're going to talk to for a little bit.
It's somebody that, well, pretty much everybody at least knows her name and most people will know her story.
So we're not going to talk much about her past.
We're going to talk about what is going on in the future.
We're going to talk about the roads of Chelsea Manning.
How are you doing, Chelsea?
I'm doing pretty good.
It's been a busy month.
It's Pride Month.
So I've been, it's been breakneck.
Every single day for the last three weeks has been something.
And now the big week in New York is coming up.
So I've been stretched doing a bunch of things.
Well, I appreciate you making time for this.
Yeah.
So now where are we headed from here?
We've heard that you're going to be on YouTube.
You're starting a YouTube channel.
I am.
So I, yeah, so this has been going on for a minute.
I am producing long form science, technology, engineering and mathematics.
So STEM field focused long series, long form videos on big issues, like big ticket items
that are like overarching, like cryptocurrency, artificial intelligence.
Right.
And so these videos are, you know, they're not what I think people are expecting from
me because I, you know, I think a lot of people are expecting me to like go into like the
debate Lord space or into video essays, but I'm actually a STEM Lord.
So I want to, I want to really get into, into the areas that I, that I really fascinated
by that also, while also taking it from like a more, you know, from a more nuanced, like
political perspective.
So, you know, I'm not trying to, I'm not trying to sell you the next gaming processor.
I'm not trying to, I'm not sponsored by, you know, IBM, Watson or anything like that.
I am just, I am simply trying to educate people on technical topics that affect society widely
and that are widely talked about, but that I am sensing are not like understood in a
like a very simple technical sense.
And so there's a lot of misunderstanding.
There's a lot of misinformation, a lot of disinformation, a lot of misunderstanding,
a lot of rumors, you know, and that comes from all sides of political spectrum, you
know, left, right, you know, moderate agnostic, you know, agnostic and apathetic people all
seem to have a fundamental misunderstanding or, you know, a limited understanding of these
kinds of topics.
And I just want to get into the weeds of what, of how stuff works.
So it's, think Bill Nye, the science guy, but, you know, from a 2021 social critical
perspective.
So there's, instead of me sitting, you know, sitting around talking about the consequences
of it, which I do, you know, I regularly speak on the topic.
On these topics.
But one of the problems that I've encountered constantly is that I get a lot asked a lot
of the same questions again and again about how, how does cryptocurrency work?
How does artificial intelligence work?
Right.
As opposed to like, as opposed to like, what, what do they, how do they impact society?
And you can't really answer the second part, how they impact society until you understand
that, oh, actually these technologies are not bad.
They're used for bad things and they have consequences, but the technology itself is,
is sort of an empty vessel and can be used for other things.
So, or, you know, so that, that's what I'm, that's what I'm trying to do is I'm trying
to, is I'm trying to just sort of educate people on these topics that are broadly talked
about at an eighth grade level as well.
That's like one of the rules that I have is making sure that this is, that I'm not, because
there are videos out there that talk about these subjects, but they are at the graduate
and post-grad level.
And they're usually like cop, they're usually like videos of, of university level lectures,
where it gets more into the weeds of the technical stuff, but it doesn't, it doesn't have to
be that way.
Right.
Right.
And that's, I mean, that's one of the things when you're talking about tech literacy in
the United States, it's, there's a wide gap between people who are very technically literate
and those that are more like me, you know, need somebody to help get them on a discord
call.
So the, the, I can definitely see it being a value and breaking it down to somebody more
my audience, you know, my, my, my level of understanding when it comes to this stuff,
it would definitely going to be a valuable service.
But so when you say you're going to talk about the politics of it, I'm curious how that's,
how that's going to happen.
It's subtle.
It's, it's not, you know, I, I, I am, I am not making it the centerpiece.
So we spend like the, like with my first video on cryptocurrency, which is basically 85 to
90% done.
I've been slowed down on it mostly due to my editor having computer problems.
It, the video, like we filmed it and we play around with skittles.
We play around with, with playing cards.
There's some references to like Texas Hold'em, you know, you know, there's video game references.
There's a lot of film homages, you know, so like we're, we're, we're mostly, we're mostly
general audience stuff, right?
This is, this is mostly general audience material where I think the, the subtle bits are, is
later on in the video when we talk about the consequences of these things.
So it's, it's, it's, it's not front loaded, you know, so I, and I, and I make it clear
that like I have a perspective on this and it's a, it's an opinion, you know but you
know, it's not the centerpiece.
And, and you know, and I'm a little, and I'm also a little critical of some people who
are not literate but have the right perspective.
But they're, they're like over, you know, like for instance with cryptocurrency, right?
There's a lot of people who think that, you know, cryptocurrency, which cryptocurrency
mining which is a process that involves a lot of processing power, a lot of computation,
a lot of heat, a lot of, a lot of usage of fuel, fossil fuel.
A lot of people kind of exaggerate the, the, the scale of this and make it seem like it,
it, it's like it's a huge dent in sort of greenhouse emissions.
I kind of like poke a hole in that a little bit because it's, it's not, it's not a huge
dent.
It's not great.
Also it's, you know, these things can be avoided, right?
You know, like you, there are, there are methods, there are means that where, where we, where
we can use cryptocurrency and ledgers without necessarily doing, without necessarily using
all of these different, you know, resources and stuff.
And there's also like, you know, there's also, there's also like discussion about like how
renewable energy is used a lot in the, in the cryptocurrency mining space.
So you know, like there's, there's counterpoints to it.
So I'm not, I'm not taking, I'm not taking like a firm, hard position.
I'm trying to lay it all out and let, you know, the viewer decide, but also like make
it clear that, you know, like I, I do have a, I do have a slight slant and a perspective,
but like it's, it's not front loaded.
And it's, it's not, it's not the point of this.
Yeah.
Well, I mean any, everything today is political in some way.
There's always going to be a tieback.
So I mean, it has to be addressed.
It's just, yeah, that, that's a line.
I don't know that I'd want to walk.
That sounds like it's going to be hard to be honest.
Like the more I think about it, the more I'm like, you know, the further you get into it,
you're gonna, you're gonna take heat from everybody on.
Yeah.
Yeah.
I, you know, in the political space, I think so.
But you know, again, like this is like, you know, I, cause I operate mostly in the, in
the, in the tech space offline.
Right.
So I have my day job, like I'm literally just doing my day job and I'm turning into content.
That's what I'm doing.
So my, my, my day job usually consists of reading a lot of white papers, reading a lot
of you know, going line by line through code, doing like a security assessments for like,
you know, workflows of, of, of small to medium organizations looking at, you know, look,
looking at cryptography platforms in their early state before, before they're ready to
go.
So I do a lot of this work.
So that, you know, all everybody in the, every, everybody that's around me that I'll be seeing,
you know, at the, at the conference at sort of the tech conferences all recognize all
this material and know it pretty well, but it's, it's just, just, it, it's not being
what's happening, I think, is that it's not being packaged in a way to where people will
one, have an interest in it and two will actually understand it.
You know, cause I, I don't want to, I don't want to just throw figures at people or throw
charts and graphs of people.
This is almost entirely on location, film shooting experiments in, you know, in different
places, you know, I, you know, it, I think Nova in the mid nineties or like public, you
know, there's like, I think that the lack of public broadcasting and a lot, the lack
of, of sort of, cause like history channel and discovery channel back in the day, whenever
I grew up was a lot more filled with like educational material as opposed to like, you
know, entertainment.
So I need to like walk that line of like providing, you know, useful information that is public
broadcasting quality educational material that is for a general audience without getting
into, without getting into the flashiness, right.
You know, it's hard, it's hard for me to do, and I've never done this before and I have
a ton of help.
And that's been a, that's been really a huge breakthrough in this is that even though like
it's taken me longer to produce the video than I, than I anticipated, cause I plan on
starting this project back in June of last year, like during the, you know, during quarantine,
that's whenever I came up with this idea.
And so it's been a journey over the last year to just produce the first and second videos.
But throughout that journey, I've had help and I've really, and I've learned a ton of
stuff that I had no idea before.
Like I couldn't open it.
Like I'm a technical person and I do photography and I, and I know how to film stuff.
But I have net, I opened up a video editor for the first time, I think July, July of
last year.
And I'm pretty technically savvy and I understand a lot, a lot about software, but I was completely
lost.
So I paused everything and I took film classes.
I just, you know, I, I set aside my ego and I said, I don't know how to do something.
I need to learn.
And you know, so I took film classes and I have, I have like a, I have a professional
editor who's a supervisory editor to like walk me through and guide me through the process
of how to produce a documentary grade.
You know, like this, like this is the production value on this is pretty high, I would say,
which has the downside of it, of it requiring time.
And it's also been a learning curve for me because when I, when I started doing this,
I like had no idea what I was doing and this is like the, the quality has just been going
up and up and up.
But obviously I haven't, I haven't produced a product for anybody yet, but it's paying
off and I want to promise people who are, who are Patreon subscribers that, you know,
that it is the, the, the growth of the Patreon has absolutely been pivotal in upping the
quality and the scale in which I am and, and the production grade of this.
And also like I have an external fact checker now.
So I have somebody who will go over my video and be like, you know, and check to make sure
that I'm not, that I'm not saying or doing anything that is that, that, that is easily
disprovable or wrong or, you know, misinformed or anything like that.
So, you know, even, even just little things like that, like the, the Patreon the, the
Patreon is really helping to smooth over some of these, these things.
Oh, absolutely.
I mean, and there's, and there's definitely a learning curve, you know, when we started
the second channel that actually it's, and it's not even like high production value,
you know?
Right.
I'm thinking, well, you know, it's, the videos are going to be like five times as long, so,
you know, we should be able to get them done in a day.
That's not how that worked out like at all.
There's a lot more to it when you start filming outside of a controlled space.
And I'm curious, so you're definitely looking towards educational content aimed at a general
audience, but when it comes to YouTube, you have to like cultivate that audience.
I'm curious, what is your like ideal demographic?
Who are you trying to, to reach?
I'm trying to reach, honestly, the way I see it as I'm trying to reach me when I was at
14 years old.
Nice.
So that's because it's content that I care about.
It's content that I'm really that I that I'm interested in.
And that, you know, even though I now am at the level that I understand it, I also know
whenever I was curious about this stuff, and when I was fascinated by the stuff, and I
want to like get that spark, like I want to have that I want to feed that, fulfill that
curious need that I had as a teenager, and as a young adult, and I want to fill in that
space because I feel like I feel like, you know, and you know, I feel like infographics
don't really do the job.
So I don't have everything that I'm doing is is real world stuff.
Like there's not gonna like I one of the rules that I have when I was creating this project
was to not have any animations or, you know, because I know that's, that's, that's something
that goes viral in this in on YouTube, and that there's already a lot of, but I don't
think I don't think it has the same impact or effect in certain terms of educational
value as actually going actually going to some place and you know, going on location
and and doing much more fun things like, you know, it's more like the Mentos in a it's
more like dropping the Mentos in a Coke bottle kind of thing.
Like you know, the fun stuff and showing like how fun that this journey, this learning journey
can be and this educational journey can be as opposed to like feeling like you're being
walked through a classroom.
It's a field trip.
Like this, this whole video series is a field trip as opposed to a as opposed to a classroom.
So you're, you're, you're gonna be Miss Frizzle.
Yeah, exactly.
It's, it's, it's magic school bus time.
Oh, I like it.
I like it.
And okay, and it gets to your other point, you know, you know, YouTube is is a very hard
space to get into now.
And I'm late to the game.
And I know that.
And I know that I may it may not be it may not be like, viral successful, right?
You know, I'll be like, I will be fine with, you know, with like 50,000 views on per video.
That's fine.
I don't, I'm hoping that, you know, sort of my other social media can drive traffic to
it, because I don't think I'm going to get on the recommended, right, you know, and I'm
aware of that.
But also like the Patreon subscribers are super, or they'll be they'll be watching it.
And so even, even if it even if it doesn't immediately draw traffic, the other thing
that I'm trying to do is, is I'm trying to make this time is I'm trying to make this
timeless as opposed to timely.
So, you know, I want to make sure that this that you can go back five years from now and
look and still learn a significant amount.
Right.
And that's one of the reasons why I'm not making it so timely.
And so and I've actually pulled out a couple things that that were timely for my initial
from my initial early from an earlier thing, I actually went back and I reshot a couple
portions to make it a little less specific to 2020, 2021.
Right.
Well, that that's that's probably a good idea.
From being on this side and looking at the analytics, I can tell you the videos that
are that I have that are evergreen.
They're the ones that they're applicable whenever, you know, it's like the dry information about
police training or terrorism or whatever it that is.
Those are the videos that end up catching views and stay just a little bit each month.
But if that's also what contributes to long term channel growth and you're right, it doesn't
sound like what you're going to do is going to pop viral initially.
But it does sound like something that could be built into something that matters, which
is, you know, a little more important.
And, you know, I and I understand it's a risk.
You know, I am taking a big I'm taking a huge leap of faith here.
You know, and I don't know, I like I'm part of me is just doing this for myself because
like one of the things that I've always wanted to do is do is do documentaries.
But like, it's just it's not the same world anymore where you can where you where you
can like pitch it to to a studio, like in the 90s and 2000s.
And, you know, get on to the sort of the track, the or going to like the film festivals and
stuff.
You know, and, you know, and I've had it and I've had to scale back.
You know, I've I've had to scale back this project a little bit, mostly just so I can
start to do other so I can make sure that it's I have consecutive videos because I've
already started on my second one, even though I haven't even released the first one.
Yeah, yeah.
I feel you on that.
We had three videos in the can before we released the first one over on the second channel.
And that was mainly because we thought initially I was like, well, we'll do these three.
And then by the time the third one gets released, we'll have the next one ready.
That's not what happened.
So yeah, I can definitely tell you that's that's the right way to go about it.
Take your time getting getting a basic content before you start releasing.
Yeah, yeah, you know, and I'm already doing that.
And I and I just started I started to talk.
I started a tech talk last week.
First video on this on a separate subject.
Right.
So, you know, like if you go to my Twitch, if you go to my Twitch channel, like if you
go to my Twitter, if you go to my Twitch, if you go to my tech talk, they're all different
material.
So but they're all connect.
They're sort of interwoven like, you know, because they're all different aspects of what
I'm interested in.
So I'm going to be doing a bit more peppy, quick coverage of more of more timely things
on tech talk like right now I'm used I did some some like city prepper protest gear,
you know, everyday carry bag that I that I have that I can that I actually carry around
every day when I'm good when I go out in the city.
And that you know, that popped off because tech talk me one tech talk is brand new.
So there are the algorithms are a little they're a little bit more skewed in a different way.
So you know, it's easy for things to go viral.
But you know, my my main focus is getting the long form videos because I feel like those
are the those are where you're going to get the payoff of you're actually going to learn
something significant.
Because on tech talk, I just end up like with a bunch of questions, which is great, because
then I end up with with future points of material.
Like you know, there's a bunch of questions about why I put why I put like, why wrap duct
tape around a lighter or a pen.
And I'm just like, because then you have a roll of duct tape, right?
You know, like a little little little tricks like that, you know, that I that I know I
can like, provide quickly, but you know, it's not the same thing as like going over a huge
broad topic that you will never, you will never be able to explain to big concepts like
artificial intelligence, and like, like AI or cryptocurrency in a tech talk video, it's
just you can't do it, you have to the it is, it is at least 30 minutes of material.
And that's moving at a fast pace.
Like I, you know, I probably have 150 hours of footage, just for the just just for my
first video.
And luckily, it was some overlap, so I managed to like, you know, cannibalize some of that
footage for my second video, but which, which is why I keep all that much, which is why
I keep all that footage on this gigantic, several terabyte drive.
Yeah, yeah, the B roll footage that you'll be able to use over and over again, that should
be yeah.
So the the prepper stuff, I mean, I don't know if you want to call it that the whatever
you emergency preparedness stuff.
This is, is that going to blend into the tech channel at all?
Or is yeah, they overlap, they keep they completely overlap.
And the reason why they completely overlap is because I do this work every day.
And it's not just you know, like, like, I live in New York City, which is not like the
woods.
So I don't like you know, like, I'm not covering like a bug out bag, like, to go out and do
survival, right?
This is more like practical stuff that you actually need in the city and that, you know,
people might might need.
But also, like, you know, there's like little bits and pieces where it's like, also mine,
it's my personal one.
So you know, like, I have a raspberry pie in there, and I use it, you know, and I'm
going to go and that gives me a chance to like, have material for future tech talks.
I'm like, me showing how you can use a raspberry pie to secure yourself while you're traveling,
which is what I do, or to just, you know, do like site projects and stuff.
What is the technical thing?
And I get it, like, most people are going to watch that and be like, Oh, interesting.
That seems fun.
So there is an overlap.
But also, like, you know, I'm not going to be able to explain in 20 to 40 seconds, how
to set this up and how to do this stuff.
But you know, I'm hoping that people who are interested in these things that I that I sort
of like, gloss over very quickly and on Tick Tock will will gravitate towards the YouTube
channel.
So I am trying to find that audience overlap.
Yeah,
it makes sense.
I mean, to me, it sounds like you've got a good plan for a good channel.
So do you have any idea of when you're going to release the first one?
Do you have like a soft date yet that you hope for it to be out yet?
I was trying to get it before the beginning of June.
I was trying to get it done.
I struggled to do that because basically I've had to mothball everything because of Pride.
Pride Week in New York or Pride Month in New York is, you know, is is this is the first
time that I've been able to go outside as much as I have.
There's been a lot of events.
I do a lot of I have a lot of commitments.
Also my day job picked up this month.
So I'm you know, the I'm not me.
I'm not making enough money yet on the Patreon to survive off of it.
So I still have to do day job work.
So I had to.
So once that once that once that day job project is done, which should be beginning of July
and then Pride Month will be over, I can start to get into it.
So I'm expecting this to be like middle to late July.
Definitely before definitely before the conference, before I start having to do conferences in
autumn, because then my travel is going to pick up again and I will have less time to
interface with my editor.
And we're going to get like it is a it is now a hard time because I have no room for
error and we'll you know, we'll be even if it's like 95% we'll probably release it because
whatever it is now it's probably in a state that is that I could release it but you know,
I would not feel comfortable yet.
Which I mean, my maybe maybe it's my perfectionism that's getting in the way.
But also like I want to be cautious and wait for the fact checker as well.
Because I don't want to get my first video wrong and get like somebody who picks because
somebody will you know, I mean, you know this because you produce content, you know, you
could have a 30 minute video and get one fact wrong in one sentence and have the entire
video like like spun as being horrible and wrong and terrible.
So I can't I can't do that.
I have no room for error.
Right.
I almost recorded re-recorded an entire video today because I mispronounced a general's
name because I just I like I'm like, you know, somebody that's going to turn into a thing
and then I was like, it'll be all right.
But eventually you get to that point.
But when it comes to YouTube, yeah, you either have to start off with like a busted camera
and audio only coming out of the left channel and get better or you have to have it right.
Like there's the audience is very unforgiving if you set low expectations.
That's cool because you're improving.
But if you if you're setting them high, you got to meet that.
Yeah.
And I'm an established name.
I have I have the equipment I have, you know, I'm pretty established doing other things.
So, you know, the expectation is pretty high.
And and unlike with the Twitch channel, right, you know, like the Twitch channel, I like,
you know, the Twitch channel actually helped me a lot in terms of like producing these
videos.
Right.
Because like it taught me a bunch of things that I didn't know about, like just the practical
realities of sort of doing live streaming is just so hard.
And it makes it makes it makes recording and editing YouTube video like an ambitious YouTube
video so much easier.
So you know, I feel I'm pretty confident.
I feel much more confident knowing that I that I've been doing Twitch for a while and
the expectations were pretty low on that.
Like I started with a webcam and I had terrible audio, even though I have a good sound card
and even though I have a good camera, like it was just like learning how to like get
the codecs right, learning what the audience wanted, getting and then, you know, like getting
a little bit of income to sort of, you know, allow me to set up a more professional studio
because beforehand it was just me, you know, with a white wall, like with a with just a
webcam and playing games that, you know, you could barely see because, you know, the nothing
was like optimized, right?
So it's been a learning curve, like, you know, and yeah, I think that I think that now that
I've established that sort of branding on Twitter, on Twitch, and now with with the
tech talk, like I really, you know, I think that expectations are high, which is one of
the reasons why I've been hesitating to like, be to like, try to release something too soon,
you know, because it's been it's been, you know, ready ish for at least a month and a
half now.
Yeah, I could definitely I could see it.
I didn't have any of that pressure, you know, like, it was a joke.
So it was okay if it was all messed up.
But I can definitely see where there's there's going to be a lot more critique, there's going
to be a lot more just general, there are going to be people who want you to fail.
You know, I didn't have that going against me.
Yeah, yeah.
And, you know, you know, I've already had media inquiries for like long form pieces
about a video about a about a YouTube channel that hasn't even reached 2000 subscribers,
and it has zero videos, right?
Because like, that's how high the expectations are.
Because you know, I'm getting I'm getting press requests for something I haven't even
done yet.
Right.
So that and I obviously I haven't I haven't accepted those but you know, like people have
heard about this and like I'm getting like I'm getting emails and calls from reporters
wanting to do like long form, you know, both both long and medium form pieces on this.
So you know, like, yeah, there's like, and that that just gives me bubble guts.
Like it just like I am I can't I just it's a lot it's a lot of pressure and you know,
I really don't want to mess it up.
And you know, and I think that being trans is like another aspect of this.
It makes it sort of complicated because like it has nothing to do with the content, but
it's it's going to be discussed as well.
You know, it's like there's going to be like discourse.
And I've seen this with it with like video essayists, you know, in in sort of the YouTube
space.
Because, you know, like, I know that there's already going to be I know that a significant
chunk of people are going to be people who follow me, who are a part of the trans community
and the queer community.
And so like, even though it has nothing to do with like, the material has nothing to
do with that, but just sort of me being me is going to like have some some some complexities
and some and some complicated things.
And you know, I have to like look out for that, too.
You know, I have to be you have to be cognizant and careful because, you know, I just it's
not it's not stuff that I cover.
But you know, like how I but you know, how I present myself and how I and how I cover
this stuff and and the words that I use are going to be like hyper analyzed.
So definitely, definitely.
Because you're whether you want it to be whether you wanted this or not, you are definitely
somebody that is a, I don't know, icon or you're representative of the trans community
to the entire United States.
Yeah.
So yeah, I mean, everything you do is going to be analyzed.
But I mean, don't worry about I'm sure it'll be fine.
It'll be it'll it'll it's going to be fine.
Like, I know it's going to be fine.
And it's going to be fine, because thankfully, I have a wide community of people to get me
started with this.
You know, I have I have somebody helping with editing.
I took class, you know, classes that I have classmates now.
There there there is a you know, I I hadn't even there were there are things that I hadn't
even thought of, you know, like people who know lighting, people who know makeup, you
know, stuff that I hadn't really thought of.
Right.
You know, I've had the I've had I've set up a couple of video crews, like ad hoc video
crews, like with grips and, you know, sound people.
I've learned, you know, I've you know, like, it's been the community that has really made
this project possible.
And it's also benefited the community because like, I've been able to like give I've been
able to give, you know, a bunch of people who have been struggling with during the pandemic
something to do and something to get, you know, a little bit of pay, you know, even
if even if it's just for like a day or two, like like film film job, you know, it's been
it's been able to like help help them and then help me in the process.
So like this has been like this has been a wild journey, like producing this.
These videos have been a journey for me personally already.
And I and I would love, you know, you know, I and I and I kind of regret now like not
doing a little bit of like behind the scenes stuff for some of the stuff because it's just
been such a fascinating journey for me as a as like a content as like a content producer,
like as I as I've like learned how to direct a, you know, a project like this is I'm not
trying to set the expectations too high, you know, on my end, but you know, like it's been,
you know, like this has been a journey.
It's been hard.
I've been out there and you know, I got and I'm not gonna lie, this has been a good excuse
to like get out of out of the apartment to, you know, like to do to film on location to,
you know, go out to the you know, to go out to the beach or to go out downtown or to go
out to I went out to the middle of an I went out to an oil, you know, because we were talking
about fossil fuels.
I'm like, Well, you know, there's let's go to an oil refinery, like, you know, like,
not let's not let's not let's go all in.
And then there was a whole journey of like, of getting harassed by security because we,
you know, like, you don't need a permit to shoot from the sidewalk.
But that doesn't mean that that the that the that Exxon Mobil likes having you there.
And so they harassed my film crew while I, you know, while filming B-roll.
And then I and then I showed up and then I showed up and they were just like, we got
to go.
We got to go.
And it was just just this whole like, I don't know, it's this is like I don't I don't know
what to say.
It's been it's been filming on location is scary.
And it's it's it's always a it's always a chaotic mess, which has made this YouTube
video all the more exciting for me personally to just do.
I don't care if I don't care if it doesn't go anywhere, if the algorithm doesn't pick
it up, if only a couple of people, if only a couple of hundred people watch it, that's
fine because I like it's made my 2021 like just making this video has been like a journey.
I definitely think you're going to get more views than you're anticipating.
Well, you know, I I'm not I'm not I'm not trying to set a goal or and, you know, and
I think that's the thing is like, is like, if I get one person who is more educated because
of this video, it's worth it.
That's how I see this.
Like, I don't I don't need I don't need to be the top YouTuber.
I don't need to be the most recommended.
And it's never going to happen.
Like I just the just the way the algorithm works and sort of the realities of things.
But if I get but if I can just if I can just like give a small inkling of because like
I only needed you know, I only needed PBS or, you know, some really good people in my
life to educate me on certain things in school.
And you know, when I was in the military to change my whole life perspective and make
me who I am and like I do it, you know, it's sort of the the like saving a one starfish
that that's like on the the bit on the beach kind of thing where you know, it's you save
that you know, like sure, there's millions of them.
But if you can save that one, it's worth it means a lot to that one.
And you know, that's how I feel is like, I would feel you know, there are actually and
it's funny because like I've been I've been watching YouTube since 2006.
Right.
I was like one of the first people to post to YouTube.
I actually had a video I had a video log series that got me in trouble when I was in the military
because I was not supposed to be doing that apparently, but I had started earlier than
that.
And so when I was producing this thing, you know, I it was it was just me with a camera.
And it was like, it was like the lowest quality couldn't see anything couldn't hear anything.
But you know, like I've been in this space for a very long time, like as a consumer and
small video producers in the late 2000s.
And yeah, in the late 2000s, had a huge impact on me later, you know, in my in my sort of
journey in life.
And I still can, I can still think of I can still see them and I can still think of them
like off the top of my head.
And they don't post anymore.
And some of them, those channels are have been deleted.
And you know, so small channels had a big impact on me.
So like, I don't care if it's a small channel, who cares?
Right.
It that's from what you're talking about, that doesn't the size doesn't even sound like
that's part of your goal.
Yeah.
It doesn't have to be either.
I mean, as far as for the channel to be successful.
Well, I mean, I mean, you know, and I know that I haven't produced anything yet, but
it's already successful because of the Patreon, because like I it's it's a self sustaining
project.
We have no bills to pay.
We have covered everybody's costs.
Everybody's been paid.
So far, the fact checker and the video editor are the last bits of pay for the project for
the initial costs of the first video.
And we're already budgeting for, you know, the later stages of the second video.
So yeah, you know, like the the the Patreon is already successful.
You know, I already have a couple hundred subscribers.
And that's all it took.
It's all it took to make this a self sustaining project.
And it's not going to it's not it's not enough to support me personally yet.
But you know, it I imagine that that threshold will not take long to pass.
And that already be a huge success.
Like if I if I can get if I can get 800 Patreon subscribers, then I will not only be able
to support the channel, I will be able to support myself.
Mm hmm.
Right.
And that's the the Patreon in general for YouTube is it's critical because you never
know how your videos are going to do.
You never know.
You never know if there's going to be any revenue from that end of it.
But the Patreon like my Patreon that that pays for like our crew like that.
That's how we were able to start the the on the location stuff.
Yeah.
So yeah, that it's a big part of it.
But I actually think from the way you're describing as far as the length of the videos and what
you're going to be talking about, YouTube really, really, really cares about watch time.
Yeah.
The more of a percentage of that video they watch, the better it is, the more it's going
to get recommended.
And if you're talking about if you can get an 80 percent watch rate on a 40 minute video.
Yeah, they're going to recommend it a lot.
Even if it is you.
That's what happened with my TikTok.
Like I've only done one TikTok video that I've put up.
I'm going to put up a second TikTok video after literally in the hours after we're done
here.
On on my lighter arrangement, like just putting duct tape on a lighter.
And like I found that the watch like the if I go to the stats, you know, the watch time
is like already high enough to where it's going viral on on TikTok.
You know, and I've just never produced a YouTube video.
So, you know, I've had the so I have I have I have for the for a political campaign.
We have a YouTube channel, though.
But that was a different thing because that got news coverage.
And that was where I don't I don't I don't know anything about the analytics of that.
That was a that was a totally a campaign thing.
It was mothballed at the end of the campaign because, you know, like it's a separate it's
a separate institution with separate roles funded by, you know, by political donors and
everything else.
And all of those all of those receipts and everything had to be like closed off and everything
like that.
So I have had like a YouTube channel before.
That was like for a very specific purpose.
You know, and we got a lot of press coverage and that was I think we got almost a million
views just from press coverage alone for that, for my for my campaign video.
So, you know, I like that, like six success, success on a video in terms of views doesn't
always mean success of a channel either.
You know, right.
So you have one big you can have one big video.
And I've definitely had big things that are that are on YouTube.
I mean, you know, the the the the the the thing I'm well known for, you know, there's
a famous video that that was posted on YouTube that has, I think, like something like 40
million views at this point, you know.
So, you know, it's but, you know, it doesn't that is it views on YouTube does not equal
the community that comes with it, you know.
So yeah, I can make and that's what I'm focused on, very much more focused on viewers and
on community because I don't you know, I've had stuff go viral before.
I've had stuff become a big deal before.
And I see no inherent value in that from an artistic perspective.
So we've been talking for about 45 minutes now.
I'm going to ask you a question.
I ask pretty much everybody that I interview.
And given the fact that, you know, you you are you and my audience is my audience, it's
going to carry a lot of weight.
So, again, no pressure.
If you could tell people one thing about how they could change the world, how they could
make the world better, one thing that they should do or could do, what would it be?
All right.
So I have a quick story.
When I was in jail, one of the things that I had was I had a was I was I was I posted
a motivational reminder.
That I would wake up and read every single day.
And this emotional reminder, one of those things was that, you know, do one, you know,
like every day you can do one thing to change the world and make the world a better place.
Right.
Just for it.
And it's a small goal.
It's a simple goal.
And it could be it can be the tiniest thing, right?
It can just be the.
And yeah, it sounds like foofy, like motivational speaker stuff.
But like it really it really did.
You know, it built up over time, you know, and it kept me motivated and it kept me going.
And you know that you you.
Not everybody is in a position to do the big stuff.
But you everybody's in a position to do the small stuff.
To do what you can in the space that you're in, because you can make a difference with
what's right in front of you if you just look around.
And that's what I tell people, because I can't tell you like what, you know, is it is it
is it throwing your trash into recycling?
You know, maybe is it being kind to your neighbor, you know, or, you know, like doing a mutual
aid project, you know, in your in your community?
Maybe I don't know what that is.
At one point, my thing was I was placed in a situation where I was able to do a big thing
and change the world.
But that came out of sheer happenstance, you know, and situation.
And you know, like everybody, you know, everybody can do something big or small.
I like it.
Do what you can, when you can, where you can for as long as you can.
Yeah, you know, it's it's pretty simple.
Yeah.
All right.
So anything you want to say, anything you want to add, anything you want to plug, I'll
definitely put a link to your channel down below.
Yeah, I think.
I think the main thing I want to plug is obviously the YouTube channel.
So that is XY Chelsea.
So it's YouTube dot com forward slash XY Chelsea.
The other thing, although I think sometimes it requires a C, I can't tell whether or not
YouTube is trying, whether it's YouTube dot com forward slash C forward slash XY Chelsea.
It's there's like a thing going out.
There are links that they're changing up.
The other thing that I want to plug is my Twitch channel, which is Twitch dot TV forward
slash XY Chelsea 87.
And of course, my Patreon, which is Patreon dot com forward slash XY Chelsea.
And that is increasingly turning into my main source of income, not quite there yet.
But, you know, the more subs that I get, the more the more content I can produce and the
more free time I have, because then I then I can do less and less of my day job.
Makes sense.
All right.
Well, I really appreciate you.
You taking the time to talk to us and get the concept of the channel.
It sounds great.
I mean, I was really interested when I heard about it on Jen's thing.
So yeah, I wish you all the luck in the world with it.
Let me know if there's anything we can do to help.
Oh, yeah.
I forgot.
I didn't even have my my channel on my camera.
Did I?
No, I thought that was intentional.
Oops.
Oh, well, if you want, you can turn it on and wave to everybody.
All right.
Yeah, just me.
It's me having a boomer moment with Skype.
All right, there we go.
I got I got I got I got dressed up and everything.
Oh, well.
All right.
Well, I have the next thing anyway.
I'm doing my tick tock now.
All right.
All right.
Thanks for having me, though.
Big fan of the channel, by the way.
All right.
Thank you.
All right, everybody.
That's it.
It's just a thought. I'm going to go to sleep.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}