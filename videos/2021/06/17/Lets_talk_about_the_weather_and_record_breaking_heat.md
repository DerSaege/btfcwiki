---
title: Let's talk about the weather and record breaking heat....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4KsvYm00ziE) |
| Published | 2021/06/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A heat dome is impacting 18% of the U.S. population, resulting in record-breaking temperatures across the West.
- Some temperature records are the highest ever recorded, with Denver hitting 101 degrees and Anaheim and Palm Springs reaching 119 degrees.
- Texas and California are asking residents to conserve electricity due to overworked plants.
- Heat domes are natural high-pressure ridges that intensify with rising temperatures, leading to longer, hotter periods.
- Prolonged high temperatures increase drought conditions and the risk of wildfires in the West.
- Extreme temperatures can be lethal, so staying hydrated is vital, especially for vulnerable groups like the elderly and pets.
- Community support is critical during such extreme weather events to assist those in need.
- Keeping an eye on neighbors, especially those at risk from the heat, and ensuring they are hydrated and safe is necessary.
- These extreme temperatures are expected to continue and worsen without action to address climate change.
- Beau urges everyone to be vigilant, take care of each other, and prepare for more frequent and severe heatwaves.

### Quotes

- "These temperatures are expected to last for a while."
- "Make sure you're hydrated. [...] This is a time to kind of come together as a community."
- "These temperatures are expected to occur more and more often."

### Oneliner

Be prepared for extreme heat, conserve energy, stay hydrated, and support vulnerable community members in the face of record-breaking temperatures across the West.

### Audience

Residents in areas affected by extreme heat.

### On-the-ground actions from transcript

- Conserve electricity to support overworked plants (suggested).
- Stay hydrated and encourage others to do the same (implied).
- Keep an eye on vulnerable community members, such as the elderly, and offer assistance if needed (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of heat domes, the impact of rising temperatures on extreme weather events, and the importance of community support during such crises.

### Tags

#ExtremeHeat #ClimateChange #CommunitySupport #Wildfires #Conservation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the weather.
We're going to do a little bit of a public service
announcement today.
The US population, 1 8th of it, is currently under a heat dome.
So we are seeing record-breaking temperatures all over the West.
This is affecting tens of millions of people.
Some of these records are the highest
they have ever been since they've been recorded.
Some are the highest in 147 years.
Denver just had temperatures of 101.
Anaheim and Palm Springs had temperatures reach 119 degrees.
It is wild.
Texas is asking its consumers to go easy on electric right now
because the plants are overworked
and some will be offline, can't handle the load.
It seems that there needs to be some infrastructure
projects in Texas.
California is making the same requests as well.
OK, so what is a heat dome?
It's a high-pressure ridge.
A high-pressure ridge kind of clears out the clouds, stuff
like that.
These happen naturally.
They're not caused by climate change.
However, an increase in temperature
makes them stronger, so they last longer,
which increases the temperature.
These long periods of record-breaking temperatures
like this, they increase the conditions for drought.
There's already a pretty bad one out West.
It would be very wise to be on the lookout for wildfires
right now because the increased heat is drying out
the vegetation even faster.
The temperatures that are being reached, they can be lethal.
So make sure you're hydrated.
Check on your pets.
Check on your old folks.
This is a time to kind of come together as a community
because there may be people who need assistance
and don't know it.
Or can't do anything about it.
So now would definitely be a good time
to kind of keep an eye on each other.
Make sure you drink water.
Definitely check on your pets.
Check on those who are most susceptible to heat,
or perhaps those who may have trouble getting out
of the house.
And just kind of keep an eye on everybody.
These temperatures are expected to last for a while.
So it's something that we're going
to have to get used to on some level
because these temperatures are going
to occur more and more often.
And they will get worse if we don't start to act.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}