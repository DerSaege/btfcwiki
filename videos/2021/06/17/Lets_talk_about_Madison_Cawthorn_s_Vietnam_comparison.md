---
title: Let's talk about Madison Cawthorn's Vietnam comparison....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7I9AZjzh7ck) |
| Published | 2021/06/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Representative Madison Cawthorn made a comparison about civilian arms in the US to prevent tyranny.
- He mentioned the idea that civilians with rifles could potentially stop tyranny and defeat a major military force.
- The comparison often overlooks the role of the North Vietnamese Army (NVA) backing up the Viet Cong during the conflict.
- Beau questions the validity of the comparison between civilians with arms and defeating a well-disciplined, conventional military.
- He points out the fantasy versus reality aspect of the comparison when it comes to enduring hardships and sacrifices.
- The image of the citizen-soldier and the VC being willing to endure anything is contrasted with modern societal behaviors.
- Beau challenges the notion of romanticizing conflict and war, especially when considering the actual consequences and sacrifices involved.
- He questions the motivations behind advocating for violent actions that could lead to the destruction of the country.
- The comparison with conflicts in Afghanistan and Iraq is brought up to illustrate the long-lasting and devastating nature of such actions.
- Beau advocates for focusing on constructive roles like engineers, scientists, nurses, and other professionals rather than glorifying combat.

### Quotes

- "Nobody ever won a war by dying for their country. They win a war by making the other guy die for his."
- "This country needs more engineers. It needs more scientists. It needs more nurses, doctors, welders, truck drivers. We need a lot of things in this country. The one thing we don't need is more combat vets."

### Oneliner

Representative Madison Cawthorn's comparison of civilian arms in the US to prevent tyranny is challenged by Beau, who questions the romanticized notions of conflict and war, advocating for constructive contributions to society instead of glorifying combat.

### Audience

US citizens

### On-the-ground actions from transcript

- Support educational programs for engineering, science, healthcare, and vocational training (suggested)
- Encourage youth to pursue careers in constructive fields (implied)
- Advocate for peacebuilding initiatives and conflict resolution strategies (implied)

### Whats missing in summary

The full transcript provides a deeper exploration of the societal attitudes towards conflict, sacrifice, and the importance of focusing on constructive contributions rather than glorifying violence.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a comparison that gets made pretty often.
It was recently made by Representative
Madison Cawthorn, I think.
And I've got to admit, when I first heard it I laughed
because it's entertaining
because while in some ways it's true, the way it is often presented is ridiculous,
he said that
civilian
arms in the United States, well that's to prevent tyranny.
And
that if you don't think
that a bunch of civilians with rifles can
stop tyranny and defeat a major military, you need to ask
the VC what they did with the Marines and the Army.
Yeah, I mean I've got to admit, at first I laughed
and I started thinking about it.
When we were still in the laughter phase, Robert Evans cracked me up
because he made a comment about NVA erasure, the North Vietnamese Army.
When people make this comparison they often forget
that backing up the Viet Cong
was
the NVA,
a well-disciplined, well-equipped, conventional military that had sophisticated equipment.
I'm guessing
in the in-depth studies
of the conflict that the people who make this comparison performed, they must have missed
flight of the intruder.
And people cracking jokes about strategists
because I'm willing to bet that if you were to ask those who make this comparison
who the chief strategist was, they're going to say, oh gee man,
that's not right.
I don't know
of an American
that would support
this kind of thing
that could match
the North Vietnamese chief strategist.
I don't see that as something that's likely.
In some ways the parallels are there
because
that sophisticated weaponry, yeah, it would come, absolutely.
Every opposition nation on the planet, every country
that wants to see the United States ripped apart and fail and be irrelevant
for a hundred years would be flooding the country with arms.
Patriot.
Because you care about the country, you're willing to do this.
And that comparison,
they always use the VC.
And it makes sense on some level,
citizen-soldier,
and it
embodies
the image they want.
Ferocious.
Willing to endure anything, survive on nothing,
never give up. That's the image.
And that image of the VC, that's pretty well founded.
That is pretty well founded.
But I don't see the comparison.
Surviving on a bowl of rice.
Meanwhile,
those who
fantasize
about ripping their country apart,
about hurting their neighbors,
they had full-blown meltdowns
because their favorite restaurant
only had takeout.
The VC, they could endure anything.
Meanwhile, most those having this fantasy couldn't endure wearing a mask for 15 minutes in Walmart.
I don't see the comparison.
And when people talk about this,
they always tend to look at it from the American perspective of a war.
Where it's over there, you know,
somewhere far away.
If you want to know about it, you want to see it,
tune in to the nightly news,
watch clips and cheer for your team.
But that's not what it would be.
And that image,
all those cute sayings that Americans have,
none of them make any sense anymore
if you're on the other side,
if you're not the force that is coming in to occupy.
Nobody ever won a war by dying for their country.
They win a war by making the other guy die for his.
And you can see that's the thing,
that they believe they're going to survive this fantasy conflict.
The reality is the Vietnamese won a war exactly by dying for their country.
If you total up North Vietnamese losses,
all their fighters, NVA, VC, everybody,
it's about 1.1 million.
If you combine American and French combat losses,
it's about 110,000.
Ten to one.
It's run through attrition.
When you're sitting there and making these comments
and you have this image in your head
of your little ragtag fantasy team of you and your friends,
understand if there's 11 of you,
10 of you ain't making it.
It isn't the same.
That image, that fantasy, it's not reality.
And the other part of it is that I don't get it.
I went through my fiery rhetoric phase.
But then at some point you have to ask,
what are you so mad about?
Why are you willing to destroy the country,
rip the country apart and lose millions?
Why?
Because kids are playing sports at high schools?
Bathroom disputes?
You don't like an ad that some multinational company put out?
What are you so angry about?
Why are you willing to do this?
But they always point to the VC.
The other examples that get used occasionally
are Afghanistan and Iraq.
20 years.
These conflicts, they last to decades.
I don't know if it lasts decades in their little fantasies.
You start something like this, the children are finishing it.
And for what?
I've asked that question a lot.
Nobody's ever been able to give me an answer.
Nobody's ever been able to tell me why they're so mad,
why they're willing to do this, why they would even
talk like this.
This country needs more engineers.
It needs more scientists.
It needs more nurses, doctors, welders, truck drivers.
We need a lot of things in this country.
The one thing we don't need is more combat vets.
We've got enough.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}