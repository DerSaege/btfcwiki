# All videos from June, 2021
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2021-06-30: Let's talk about getting started, futility, and serendipity.... (<a href="https://youtube.com/watch?v=M0YtOr5zklI">watch</a> || <a href="/videos/2021/06/30/Lets_talk_about_getting_started_futility_and_serendipity">transcript &amp; editable summary</a>)

Beau talks about serendipity, futility, fires, and getting started, sharing a story of how small actions can ignite collective efforts to tackle community issues.

</summary>

"You don't have to win every day. You just have to fight every day."
"All you have to do is get started."
"Just getting the idea out there."
"You can't solve it, you can't put a dent in it, but you're willing to try."
"Other people will, too."

### AI summary (High error rate! Edit errors on video page)

Received two messages back to back over the weekend, leading to discussing serendipity, futility, fires, and getting started.
One message was from someone needing encouragement to solve a problem they faced in their community regarding food insecurity.
The other message contained a story about a woman named Morgan who tried to put out a brush fire with just a water bottle, inspiring others to join in.
Morgan's actions, though seemingly futile at first, led to others stepping in with more resources until the fire was extinguished.
Emphasizes the importance of getting started, even if the resources at hand may not seem enough to solve the problem.
Starting a community project, even with limited resources, can inspire others to join in and contribute.
It's about showing willingness to try and make a difference, even if the impact may seem small initially.
The story of Morgan illustrates that one person's actions can spark a chain reaction of support and involvement from others.
Encourages taking the first step, as it often leads to more people coming together to address community issues.
In community endeavors, the act of starting and showing commitment is more critical than having all the resources from the beginning.

Actions:

for community members,
Start a community project to address local issues, even with limited resources (exemplified).
Show willingness to try and make a difference in your community by taking the first step (exemplified).
Inspire others to join in and contribute by initiating efforts to tackle community problems (exemplified).
</details>
<details>
<summary>
2021-06-30: Let's talk about Tucker's NSA claim with Deep Goat.... (<a href="https://youtube.com/watch?v=qx_5Z8UN1fA">watch</a> || <a href="/videos/2021/06/30/Lets_talk_about_Tucker_s_NSA_claim_with_Deep_Goat">transcript &amp; editable summary</a>)

Tucker Carlson's claim of NSA surveillance lacks evidence and credibility, casting doubt on the likelihood of the alleged operation.

</summary>

"This is compounded. This problem is compounded if somebody is a known loudmouth and talks a whole lot about what they're doing."
"Intelligence agencies throughout history, throughout the world, yeah, they try to influence media coverage all the time."
"You also have to kind of weigh the probabilities here. Is Tucker Carlson that important?"
"You cannot FOIA the NSA to ask them if you're a target of a black op."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Tucker Carlson claims NSA is targeting him for surveillance to create a scandal and remove him from the air.
Former NSA ops officer believes there is a zero percent chance of spying on a major American media figure unless in contact with a hostile intelligence service.
Deep Goat questions the reliability of statements from both Tucker Carlson and the intelligence community.
Tucker Carlson presented no evidence to support his claim of NSA surveillance.
Deep Goat explains the lack of operational security in Tucker Carlson's communication methods.
The credibility of Tucker Carlson's claims is questioned due to the absence of evidence and the nature of the allegations.
Deep Goat analyzes the implausibility of the NSA operation as Tucker Carlson described it.
Intelligence agencies influence media coverage, but not typically by targeting individual journalists as Tucker Carlson suggested.
Possible scenarios include fabrication of the story, NSA intercepting messages due to contact with a hostile intelligence service, or Tucker Carlson preemptively leaking compromising information.
Deep Goat concludes that while intelligence agencies can break the law, the likelihood of Tucker Carlson's specific claims being true is low.

Actions:

for journalists, media consumers,
Verify information before amplifying it (suggested)
Question and critically analyze claims made by public figures (suggested)
Ensure operational security in communication practices (implied)
</details>
<details>
<summary>
2021-06-30: Let's talk about Justice Breyer retiring.... (<a href="https://youtube.com/watch?v=rc365dbc6sg">watch</a> || <a href="/videos/2021/06/30/Lets_talk_about_Justice_Breyer_retiring">transcript &amp; editable summary</a>)

People debate whether Justice Breyer should resign; Beau supports his retirement without external pressure, trusting he'll do it on his own.

</summary>

"In an ideal world, the Supreme Court is non-partisan. It's not supposed to be a partisan entity."
"He should be out fishing or something."
"I don't think anybody needs to put any pressure on him to resign."
"I think that he's going to do it on his own without influence."
"Come on Justice Breyer, take up fishing."

### AI summary (High error rate! Edit errors on video page)

People are debating whether Supreme Court Justice Breyer should resign for a replacement.
In an ideal world, the Supreme Court should be non-partisan, but that isn't the reality.
Delay in appointing a replacement may lead to a hyper-partisan appointment by Republicans.
Beau believes Justice Breyer, at 82, should resign and enjoy retirement.
He doesn't support President Biden or Congress pressuring him to resign.
Beau thinks Justice Breyer will likely resign at the end of his term without external pressure.
He trusts that Justice Breyer understands the need for a successor to preserve ideals.

Actions:

for politically engaged citizens,
Advocate for a non-partisan Supreme Court (implied)
Respect Justice Breyer's decision and retirement plans (exemplified)
</details>
<details>
<summary>
2021-06-29: Let's talk about the South Dakota National Guard and the missing point.... (<a href="https://youtube.com/watch?v=tEHM0wYZRpw">watch</a> || <a href="/videos/2021/06/29/Lets_talk_about_the_South_Dakota_National_Guard_and_the_missing_point">transcript &amp; editable summary</a>)

Governors deploying National Guard troops for political stunts instead of real security risk their credibility and the lives of service members.

</summary>

"It's a joke."
"It's politics and nothing more."
"Any governor who participates in this can never be president."
"They are showing you who they are right now."
"It's a joke. It's a joke."

### AI summary (High error rate! Edit errors on video page)

Governor of South Dakota sending 50 National Guard troops to the southern border, funded by private donation.
Questions raised about the legality of using private funds for National Guard deployment.
Beau questions the purpose and effectiveness of deploying only 50 troops to secure the border.
Raises the issue of National Guard members signing up to help their community, not for political stunts.
Points out the difference in numbers between National Guard troops and Border Patrol agents.
Criticizes the deployment as a political stunt rather than an effective security measure.
Warns that governors involved in this deployment may not be fit for the presidency.
Emphasizes that deploying troops for political gain sets a dangerous precedent.
Condemns the deployment as a misuse of trust and resources for political purposes.
Urges people to recognize the true intentions behind such deployments.

Actions:

for governors, voters, activists,
Call out misuse of National Guard for political gain (exemplified)
Advocate for ethical deployment of National Guard troops (exemplified)
</details>
<details>
<summary>
2021-06-29: Let's talk about baby bonds and generational wealth.... (<a href="https://youtube.com/watch?v=X48D_RbFPa4">watch</a> || <a href="/videos/2021/06/29/Lets_talk_about_baby_bonds_and_generational_wealth">transcript &amp; editable summary</a>)

Beau explains baby bonds, a cash initiative for children in poverty that aims to break cycles of generational wealth inequality and poverty, while noting its economic benefits and mentioning a federal counterpart.

</summary>

"It's cold hard cash."
"It's a concept that we've talked about before. That's the money that your family has accumulated over the years."
"It's worth noting there is a federal counterpart to this concept."
"It's better economically to do the right thing."
"It is cheaper to be a good person, almost always."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of baby bonds, which involves depositing $3,200 into an account for every child born into poverty.
Describes baby bonds as a substitute for generational wealth that disadvantaged individuals lack access to.
Links the lack of generational wealth to limited opportunities and the perpetuation of poverty cycles.
Connects the concept of generational wealth to the topic of reparations for descendants of slaves.
Emphasizes the economic benefits of baby bonds, suggesting that the invested money will circulate in the economy and eventually pay for itself.
Mentions the Americans Opportunities Accounts Act as a federal counterpart to baby bonds but acknowledges the potential challenges in its implementation.
Argues that investing in initiatives like baby bonds is not just morally right but also economically beneficial for society as a whole.

Actions:

for policy advocates,
Advocate for the implementation of baby bonds at a local level (suggested).
Support initiatives that aim to break the cycle of poverty through economic empowerment (implied).
</details>
<details>
<summary>
2021-06-28: Let's talk about a question on leaving Iraq.... (<a href="https://youtube.com/watch?v=aI1_kkhRpRw">watch</a> || <a href="/videos/2021/06/28/Lets_talk_about_a_question_on_leaving_Iraq">transcript &amp; editable summary</a>)

Beau addresses foreign policy, moral responsibility, and the likelihood of U.S. withdrawal from Iraq, stressing the potential impact on innocents caught in global turf wars.

</summary>

"The American empire wants it to kind of come to a close, at least when it comes to the military side of it."
"My concern in Afghanistan, that's my lens through which I look at most foreign policy decisions, is the innocents."
"You could advocate to totally withdraw from Iraq without risking a moral liability."

### AI summary (High error rate! Edit errors on video page)

Addressing foreign policy and moral responsibility following recent events.
Responding to criticism regarding the need for a security force in Afghanistan.
Clarifying the U.S. presence in Iraq and the misconception of occupation.
Explaining the difference between the situations in Afghanistan and Iraq.
Asserting that the U.S. could withdraw from Iraq without triggering drastic consequences.
Mentioning the slim possibility of group resurgence but overall low likelihood of major issues.
Emphasizing the distinction between moral liability and the feasibility of withdrawal.
Expressing skepticism about the U.S. actually pursuing complete withdrawal due to foreign policy concerns.
Noting the potential shift towards Iranian influence if the U.S. withdraws from Iraq.
Stating that most Americans desire an end to military adventurism while acknowledging other foreign policy goals.

Actions:

for policy analysts, activists,
Advocate for total withdrawal from Iraq without risking moral liability (implied).
Understand the nuances of foreign policy decisions and their impact on innocent civilians (exemplified).
</details>
<details>
<summary>
2021-06-28: Let's talk about Gwen Berry and the flag.... (<a href="https://youtube.com/watch?v=NzEU5K3Y7xU">watch</a> || <a href="/videos/2021/06/28/Lets_talk_about_Gwen_Berry_and_the_flag">transcript &amp; editable summary</a>)

Beau unpacks Gwen Berry's protest, calling out politicians for prioritizing outrage over policy, exposing their hypocrisy in feigned outrage.

</summary>

"That flag, it's a piece of cloth. In and of itself, it means nothing."
"You don't get to pretend to be mad about this, because this isn't new."
"Rather than addressing the issues, you are making them worse."

### AI summary (High error rate! Edit errors on video page)

Gwen Berry drew attention to systemic issues in the US during a national anthem moment, a move unprecedented for athletes.
Politicians have become more focused on generating outrage rather than policy, using the incident for political gain.
The flag is described as a piece of cloth symbolizing the ideals of the country, which some politicians have failed to uphold.
Politicians who have not addressed systemic issues don't get to feign outrage over Berry's actions.
Berry's actions are aimed at policymakers, urging them to address long-standing issues, which many politicians have ignored for years.
Rather than uniting the country and addressing real issues, some politicians prefer to stoke division for their own political careers.
Beau suggests that kneeling may be a more powerful form of protest imagery but respects Berry's choice of protest.
He questions the credibility of those criticizing Berry's actions, especially if they downplayed violent events involving the flag in the past.
Beau points out the hypocrisy of using flag etiquette to criticize Berry when real issues are being ignored for political gain.
He ends by encouraging viewers to think about the message shared and wishing them a good day.

Actions:

for politically engaged citizens,
Contact policymakers to address systemic issues (implied)
Participate in peaceful protests for social justice (implied)
</details>
<details>
<summary>
2021-06-27: Let's talk about Ecocide, a new proposed international law.... (<a href="https://youtube.com/watch?v=_Lpr8CqbUVQ">watch</a> || <a href="/videos/2021/06/27/Lets_talk_about_Ecocide_a_new_proposed_international_law">transcript &amp; editable summary</a>)

Beau introduces the concept of ecocide, a proposed international law aiming to hold governments and entities accountable for severe environmental damage, stressing the urgency for significant changes to address environmental issues before running out of time.

</summary>

"It's called ecocide. It's called ecocide."
"But this is a stopgap. And we are running out of time to use stopgaps."
"A whole lot of environmental damage that's occurring, well, it's just a cost to do business."

### AI summary (High error rate! Edit errors on video page)

Explains the concept of ecocide, a proposed international law that could be added to the Rome Statutes.
Defines ecocide as unlawful acts committed with knowledge of causing substantial and severe damage to the environment.
Notes the challenge of getting new international crimes accepted due to countries losing authority over such matters.
Emphasizes the importance of holding governments and entities accountable for their environmental actions.
Acknowledges that the process of adding ecocide as a crime will be lengthy but believes even the definition alone is useful.
Points out that nonprofits and environmental protection groups can benefit from this legal definition.
Urges individuals to reconsider their actions before the ICC starts holding them accountable under the ecocide definition.
Raises awareness about the environmental damage caused by viewing it as just a cost of doing business.
Stresses the need for significant changes to address environmental issues and the limited time available for implementing solutions.

Actions:

for world citizens, environmental activists,
Advocate for the recognition of ecocide as an international crime (implied)
Support nonprofits and environmental protection groups in using the legal definition to raise awareness and protect the environment (implied)
</details>
<details>
<summary>
2021-06-26: The roads with Chelsea Manning.... (<a href="https://youtube.com/watch?v=j4d7FODMr0g">watch</a> || <a href="/videos/2021/06/26/The_roads_with_Chelsea_Manning">transcript &amp; editable summary</a>)

Chelsea Manning shifts focus to STEM education on YouTube, aiming to simplify complex technical topics with a nuanced political perspective to bridge the gap in technical literacy for a broader audience.

</summary>

"Do one thing every day to change the world and make it a better place."
"Everybody is in a position to do the small stuff."
"Do what you can, when you can, where you can for as long as you can."

### AI summary (High error rate! Edit errors on video page)

Chelsea Manning's shift to creating STEM-focused content on YouTube, aiming to provide nuanced, politically-informed educational videos on technical topics.
The importance of educating people on complex technical subjects like cryptocurrency and artificial intelligence in a simple, easy-to-understand manner.
Chelsea Manning's emphasis on the societal impact of technology, addressing common misconceptions and misinformation from various political perspectives.
The challenge of bridging the gap in technical literacy between different levels of understanding among the general audience.
Chelsea Manning's journey in learning video production from scratch, including taking film classes and receiving support from a professional editor.
The significance of community support in making the YouTube channel project self-sustaining through Patreon subscriptions.
Chelsea Manning's reflections on the pressure and expectations surrounding the launch of the YouTube channel, including considerations related to being a trans public figure.
The importance of community impact and education over viral success in content creation, focusing on making a difference regardless of viewership numbers.
Chelsea Manning's message about taking small actions every day to make the world a better place, regardless of the scale or magnitude of the effort.

Actions:

for creators, educators, activists,
Start a community education project on STEM topics (implied)
Support creators through platforms like Patreon (exemplified)
</details>
<details>
<summary>
2021-06-26: Let's talk about how the Trump years impacted capitalism.... (<a href="https://youtube.com/watch?v=v31NNiZDZAs">watch</a> || <a href="/videos/2021/06/26/Lets_talk_about_how_the_Trump_years_impacted_capitalism">transcript &amp; editable summary</a>)

Acknowledging Trump's unintentional contribution to mainstreaming socialism by sparking curiosity and understanding leftist ideologies, altering Republican views on capitalism.

</summary>

"He altered that dynamic because he broke an unspoken rule and he called people socialist."
"It helped mainstream socialism. It helped mainstream that idea."
"He didn't mean to. But at the end of the day, when you look at how the Trump years were for the capitalist class, I mean, yeah, overall it was pretty well."

### AI summary (High error rate! Edit errors on video page)

Acknowledging a contribution former President Donald J. Trump made to the United States, inadvertently shifting views on capitalism and socialism.
Describing the United States as predominantly right-wing, even the Democratic Party is viewed as center-right, not left or leftist.
Explaining the difficulty of discussing left-leaning ideas in the U.S. due to negative connotations associated with terms like socialism.
Noting that Trump's actions, by breaking norms and using terms like socialism, sparked a curiosity that led to understanding leftist ideologies.
Mentioning a shift in the Republican Party's views on capitalism, with more young Republicans wanting policies to reduce the wealth gap.
Pointing out that Trump's rhetoric unintentionally helped mainstream socialism by trying to villainize it.
Emphasizing how Trump's actions inadvertently widened the discourse on alternatives to capitalism.
Recognizing that Trump's unintended consequences may have long-lasting effects on the dynamics of the GOP and political ideologies in the future.

Actions:

for political observers, young republicans,
Engage in political discourse with young Republicans to understand their shifting views (implied).
Support policies aimed at reducing the wealth gap to resonate with changing perspectives (implied).
</details>
<details>
<summary>
2021-06-25: Let's talk about a lesson on privilege from an unusual place.... (<a href="https://youtube.com/watch?v=zCPfQGe-nyg">watch</a> || <a href="/videos/2021/06/25/Lets_talk_about_a_lesson_on_privilege_from_an_unusual_place">transcript &amp; editable summary</a>)

Beau talks about privilege, perception, and the global impact of the ongoing pandemic while shedding light on the unequal distribution of vaccines and its implications.

</summary>

"Your reality is pretty broadly defined by what you see around you every day."
"We see recovery. We see things getting back to normal. Everything's lessening here, but just here."
"It's a privilege."
"That's how privilege works."
"Sometimes you're completely, completely unaware of the fact that you have it because you don't see the other side of it every day."

### AI summary (High error rate! Edit errors on video page)

Talks about privilege and perception based on statistics.
Demonstrates how privilege influences perception of reality.
Mentions the impact of exposure to certain facts on awareness.
Points out the misconception of being in a post-pandemic state in the United States.
Provides statistics on hospitalizations and losses due to COVID-19.
Emphasizes the different impacts of the pandemic on vaccinated and unvaccinated individuals.
Raises awareness about the disparity in vaccine distribution between wealthy and poorer countries.
Comments on people's unawareness of the unequal vaccine distribution and its impacts.
States that the pandemic is far from over globally, despite improvements in some countries.
Concludes by discussing how privilege can make people unaware of their own advantages.

Actions:

for global citizens,
Advocate for equitable vaccine distribution (implied)
Stay informed about the global status of the pandemic and support initiatives for fair vaccine access (implied)
Educate others about the disparities in vaccine distribution (implied)
</details>
<details>
<summary>
2021-06-25: Let's talk about Biden's plan for the interpreters and more.... (<a href="https://youtube.com/watch?v=U37WMmZ4r_M">watch</a> || <a href="/videos/2021/06/25/Lets_talk_about_Biden_s_plan_for_the_interpreters_and_more">transcript &amp; editable summary</a>)

Beau analyzes the Biden administration's plans for Afghan interpreters amidst escalating conflict, warning against hasty US military involvement without sustainable regional support.

</summary>

"Leaving them there is leaving them there to be lost."
"Without a token security force there, this is going to happen."
"The only way to do that is with force and lots of it."
"A regional security force, neighbors, countries that are vested, that's who needs to be behind this, not the United States."
"You need to be ready to oppose the next war before it starts."

### AI summary (High error rate! Edit errors on video page)

The Biden administration announced plans to airlift and process visas for individuals who helped the US and NATO in Afghanistan over the last 20 years, including interpreters.
Concern arises as the Afghan opposition intensifies their offensive, with predictions that the capital may fall within six months.
Without a token security force in place, the withdrawal from Afghanistan may lead to turmoil and political repercussions.
Representative Tom Malinowski suggests that helping the Afghan government survive the onslaught is vital to protect those at risk.
The politicization of the situation may lead to calls for continued US involvement in Afghanistan to prevent a resurgence of conflict.
Beau warns against a hasty return to military engagement in Afghanistan without a sustainable plan for assistance.
He advocates for regional security forces and countries vested in Afghanistan's stability to take the lead in providing ongoing support.
Beau underscores the importance of being prepared to oppose future wars in similar contexts to prevent repeating past mistakes.

Actions:

for us citizens, anti-war activists,
Mobilize against future wars (implied)
Support regional security forces for stability in Afghanistan (implied)
</details>
<details>
<summary>
2021-06-24: Let's talk about Giuliani's license..... (<a href="https://youtube.com/watch?v=ZQDDLzQhmfE">watch</a> || <a href="/videos/2021/06/24/Lets_talk_about_Giuliani_s_license">transcript &amp; editable summary</a>)

Beau shares that Rudy Giuliani's license is temporarily suspended due to false statements made in connection with Trump's failed reelection, pending further proceedings. It is a rare but significant action that may likely stick.

</summary>

"His license hasn't been permanently revoked; it's temporarily suspended due to demonstrably false statements made in connection with Trump's failed reelection effort in 2020."
"Giuliani communicated false and misleading statements to courts, lawmakers, and the public."
"Interim suspensions like Giuliani's are rare but not unprecedented."
"Given the evidence presented, it is likely that Giuliani's suspension will remain in place."
"The temporary suspension indicates serious concerns about Giuliani's conduct."

### AI summary (High error rate! Edit errors on video page)

Rudy Giuliani's license hasn't been permanently revoked; it's temporarily suspended due to demonstrably false statements made in connection with Trump's failed reelection effort in 2020.
There is uncontroverted evidence that Giuliani communicated false and misleading statements to courts, lawmakers, and the public.
The suspension is pending further proceedings before the attorney grievance committee.
Giuliani's side believes he does not pose a present danger to the public interest and expects him to be reinstated after a hearing.
Interim suspensions like Giuliani's are rare but not unprecedented.
While immediate disbarment may not have happened yet, interim suspensions are significant actions.
Given the evidence presented, it is likely that Giuliani's suspension will remain in place.
This situation is still in its early stages, with more proceedings to follow.
Giuliani's suspension is a result of his actions in his capacity as a lawyer for former President Trump.
The temporary suspension indicates serious concerns about Giuliani's conduct.

Actions:

for legal observers,
Contact legal organizations for updates on Giuliani's case (implied)
Stay informed about the proceedings regarding Giuliani's suspension (implied)
</details>
<details>
<summary>
2021-06-24: Let's talk about General Milley responding to congress.... (<a href="https://youtube.com/watch?v=tyF1P9-4ZdU">watch</a> || <a href="/videos/2021/06/24/Lets_talk_about_General_Milley_responding_to_congress">transcript &amp; editable summary</a>)

Beau breaks down the importance of education for warriors and criticizes politicians dictating military learning to combat indoctrination.

</summary>

"No education is ever wasted."
"The true sign of intelligence is being able to entertain an idea without accepting it as fact."
"Being educated about something is not indoctrination."
"Politicians determining what you can learn is a dangerous concept."

### AI summary (High error rate! Edit errors on video page)

Analyzing General Miley's exchange with Congress.
Importance of diverse backgrounds within special forces.
Emphasizing the value of education for warriors.
Learning about different things to be a better warrior.
Rejecting the notion that learning is bad.
Sign of intelligence is entertaining ideas without accepting them as facts.
Criticizing politicians dictating military education.
Green Berets needing to understand different cultures.
Critical race theory's potential applicability.
Distinguishing education from indoctrination.
Demonization of education and intellectualism in the US.
People manipulated by trivial distractions from real issues.
Politicians influencing what is taught is dangerous.
Indoctrination leads to inability to discern fact from fiction.

Actions:

for educators, policymakers, activists.,
Support educational programs for military personnel (suggested).
Advocate for diverse educational curriculums in the military (suggested).
Combat misinformation by promoting critical thinking skills (suggested).
</details>
<details>
<summary>
2021-06-23: Let's talk about Texas building the wall on its own.... (<a href="https://youtube.com/watch?v=w4l1FQBRsgk">watch</a> || <a href="/videos/2021/06/23/Lets_talk_about_Texas_building_the_wall_on_its_own">transcript &amp; editable summary</a>)

Texas plans a wall with online donations and $1.1M, facing criticisms for inefficacy and legal issues, reflecting misguided political rhetoric influenced by Texas heat.

</summary>

"Spending over a billion dollars on a proven ineffective wall seems unwise."
"Texas building its wall behind federal checkpoints could lead to legal issues."
"The heat in Texas might be affecting politicians' decision-making."

### AI summary (High error rate! Edit errors on video page)

Texas plans to build a wall with online donations and a $1.1 million budget.
The previous wall built by Trump was easily defeated in 15 minutes with a sawzall.
Spending over a billion dollars on a proven ineffective wall seems unwise.
Even if the cost is cut in half, it will only cover about 80 miles of the border.
Texas doesn't own the border, so seizing land for the wall could cause issues.
Another candidate, Huffines, claims credit for the wall idea and plans to close the border to commercial traffic without federal permission.
Huffines' plan faces challenges as Mexican trucks could use alternate routes to cross.
Texas building its wall behind federal checkpoints could lead to legal issues.
Beau believes the wall is not a successful solution and is more rhetoric than pragmatism.
The heat in Texas might be affecting politicians' decision-making.

Actions:

for texans, voters,
Challenge the idea of building an ineffective and costly wall in Texas (implied).
Stay informed about political decisions and hold elected officials accountable (implied).
</details>
<details>
<summary>
2021-06-23: Let's talk about Biden, BBC, and the border.... (<a href="https://youtube.com/watch?v=kno_u5dwt0g">watch</a> || <a href="/videos/2021/06/23/Lets_talk_about_Biden_BBC_and_the_border">transcript &amp; editable summary</a>)

The Biden administration must urgently address ongoing issues and mistreatment at border facilities housing unaccompanied minors seeking asylum, shifting focus from political posturing to real care.

</summary>

"That is not acceptable."
"This needs to be fixed and fixed now."
"They're supposed to be in the care, not custody."
"This has got to get fixed."
"It's been months. This has got to get fixed."

### AI summary (High error rate! Edit errors on video page)

The Biden administration inherited multiple messes at the border, including handling an influx of unaccompanied minors seeking asylum and reunification.
Ad hoc facilities were quickly set up to process and house these minors, reducing processing and holding times initially.
Issues surfaced at these facilities, such as undercooked food, lack of COVID prevention measures, and lice infestations.
Despite initial understanding, problems persist months later, with reports of ongoing issues like undercooked food and lice outbreaks.
Relationships between staff and detainees, especially children, are forming, raising concerns given the nature of the situation.
While applauding improvements in processing times, Beau stresses that the conditions these individuals are subjected to are unacceptable.
Urgent action is needed to address the disturbing evidence of mistreatment and neglect at these facilities.
Beau calls for immediate intervention by the Inspector General to fix the problems, rather than just producing a report.
He criticizes the administration's focus on foreign policy moral authority while such issues persist domestically.
The treatment of individuals in these facilities contradicts the care they should receive and undermines the administration's credibility.
Beau underscores the importance of prioritizing the well-being of real people over political battles or posturing.
He stresses that the situation, initially somewhat understandable, has persisted for months and must be resolved urgently.

Actions:

for advocates, policymakers, concerned citizens,
Contact elected officials to demand immediate intervention and improvement in conditions (suggested)
Support organizations working to provide assistance and oversight at these facilities (exemplified)
</details>
<details>
<summary>
2021-06-22: Let's talk about veterans getting what they deserve.... (<a href="https://youtube.com/watch?v=fUrrUXBP8kM">watch</a> || <a href="/videos/2021/06/22/Lets_talk_about_veterans_getting_what_they_deserve">transcript &amp; editable summary</a>)

Beau addresses the social unacceptability of discrimination based on immutable characteristics when discussing the news about trans vets' access to confirmation surgery through the VA and criticizes the hypocritical backlash in the comment section.

</summary>

"It's socially unacceptable to openly look down on people because of their race, because of their orientation, because of their religion."
"They're entitled to medical care, right? If you actually cared about veterans, this would be a celebratory moment."
"Nobody wants to talk bad about a vet in this country."
"Veterans can get insulin through the VA. You know what veterans couldn't get? Confirmation surgery."
"I read the comments, and in the comments saw stuff like..."

### AI summary (High error rate! Edit errors on video page)

Addresses the news about trans vets being able to get confirmation surgery through the VA.
Expresses excitement about this development, believing it helps integrate marginalized groups into the veteran community.
Notes the social unacceptability of openly looking down on people for their immutable characteristics in the US.
Reads comments criticizing this news, citing examples of veterans not getting hearing aids or insulin.
Calls out the hypocrisy of commenters who claimed previous administrations had fixed VA issues.
Points out that veterans can indeed get hearing aids and insulin through the VA, although the system isn't perfect.
Emphasizes that the entitlement to medical care should make this policy a reason to celebrate if one truly cares about veterans.
Criticizes the tendency to other and view veterans as lesser, perpetuating acceptable discrimination against them.
Hopes that integrating different demographics into the veteran community will lead to broader societal acceptance.
Mentions a trans veteran, Zoe, giving advice in the comments on how to access hearing aids through a tinnitus claim.

Actions:

for veterans, advocates, allies,
Contact trans veteran Zoe for advice on accessing hearing aids through a tinnitus claim (exemplified)
Celebrate and support policies that integrate marginalized groups, like trans vets, into the veteran community (suggested)
</details>
<details>
<summary>
2021-06-22: Let's talk about DeSantis beating Trump.... (<a href="https://youtube.com/watch?v=R7I5XFWzSY4">watch</a> || <a href="/videos/2021/06/22/Lets_talk_about_DeSantis_beating_Trump">transcript &amp; editable summary</a>)

Governor DeSantis beating Trump in a conservative poll leads to potential Democratic advantages if they can deliver on policy and effectively communicate their agenda.

</summary>

"Trump was about himself, he was about ego."
"The bipartisanship isn't going to work."
"They're going to have to do better at getting those points out."
"If the Democratic Party wants to be competitive in 2022 and 2024, they either have to start getting their agenda through or they have to start clearly explaining how it would benefit the average American."
"He is going to have to play hardball if he wants to get anywhere."

### AI summary (High error rate! Edit errors on video page)

Governor of Florida, Ron DeSantis, beat former President Trump in a conservative straw poll, leading to interesting developments.
Conventional wisdom suggests that DeSantis being influenced by Trump is positive, but Trump lacks a clear agenda or policy.
Potential scenarios include Trump attacking DeSantis or trying to co-opt him as vice president.
This situation benefits the Democratic Party for 2024, but they need to deliver on policy and agenda to capitalize on it.
Democrats face obstacles in passing major legislation, with Mitch McConnell obstructing progress.
Lack of effective messaging on key legislation by the Democratic Party is a concern.
Biden administration needs to focus on communicating the benefits of their agenda to the American people.
Bipartisanship is unlikely to work due to one side prioritizing culture war over policy.
Democrats must push their agenda effectively to remain competitive in upcoming elections.
Biden needs to play hardball and move away from nice guy tactics to make progress.

Actions:

for political analysts, democratic activists,
Communicate the benefits of Democratic policies clearly and effectively (implied)
Support efforts to push key legislation through Congress (implied)
Advocate for messaging that resonates with average Americans (implied)
</details>
<details>
<summary>
2021-06-21: Let's talk about my uncle, a storyteller.... (<a href="https://youtube.com/watch?v=BIIkc5PRqAo">watch</a> || <a href="/videos/2021/06/21/Lets_talk_about_my_uncle_a_storyteller">transcript &amp; editable summary</a>)

Beau shares lessons learned from his storytelling uncle, urging vaccination to prevent further loss to COVID misinformation.

</summary>

"Everything was a learning experience. Everything."
"We lost him over the weekend to COVID."
"There's no reason for it to be you. Go get vaccinated."
"It doesn't take long. It's pretty, well, I don't want to say it's painless."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Introduces the topic about his uncle, a storyteller, who taught valuable lessons through his experiences.
Shares how his uncle lost his finger before he was born, with varying stories about how it happened.
Mentions that his uncle always had a story to tell and each story had a point or lesson to impart.
Talks about learning about his family's native traditions and how to run irrigation from his uncle.
Describes his uncle as an adventurer who served in the Navy and briefly expatriated to the Philippines.
Mentions his uncle's three daughters and how every interaction with him was a learning experience.
Expresses the loss of his uncle to COVID over the weekend.
Urges people to get vaccinated, stating that misinformation is discouraging people from getting vaccinated.
Emphasizes the importance of vaccination to prevent further loss of lives due to COVID.
Encourages asking questions about vaccination and seeking answers to address concerns.
Stresses the reality of the ongoing situation and the necessity for vaccination to save lives.
Shares his personal experience of getting vaccinated and the minimal discomfort involved.
Advises scheduling a vaccination appointment and taking the necessary step to protect oneself and others.

Actions:

for individuals, vaccine-hesitant,
Get vaccinated to protect yourself and others (implied)
</details>
<details>
<summary>
2021-06-21: Let's talk about Biden's communion troubles.... (<a href="https://youtube.com/watch?v=lvyl9qOiUKI">watch</a> || <a href="/videos/2021/06/21/Lets_talk_about_Biden_s_communion_troubles">transcript &amp; editable summary</a>)

Beau believes the church denying communion to Biden is an internal matter, critiquing it politically while maintaining it's not his usual cause for speaking out against churches.

</summary>

"That's not my business."
"I don't think it's a good move politically."
"It's a political stunt, in my opinion."

### AI summary (High error rate! Edit errors on video page)

Beau was called out for not discussing the church denying communion to Biden, being reminded of his unique approach to talking about religion without ridiculing religious people.
Beau typically speaks out against churches when they preach harm or attempt to use state power to enforce beliefs, supporting the separation of church and state.
Regarding the church denying communion to Biden, Beau believes it is an internal church matter that doesn't involve the government.
Beau expresses that he wouldn't argue for a religious institution to provide a religious right to a government official like President Biden, as it could violate the separation of church and state.
While Beau sees the denial of communion to Biden as counter to the idea of communion itself, he defers to Reverend Ed Trevers for a theological perspective.
Beau criticizes the church's move politically, believing it will alienate members and set a bad precedent for the church to make more political stances.
He points out the hypocrisy in the church's decision, suggesting it might drive members away and encourage conservative demands for political involvement.
Beau views the church's decision as a political stunt that will lead to less support and cause doubt among members.
Despite his disagreement with the church's move, Beau concludes that it's not something he typically speaks out against since it's contained within the church and doesn't involve enforcing beliefs through state violence.
Beau shares his thoughts and wishes his audience a good day.

Actions:

for religious commentators,
Watch Reverend Ed Trevers' video for a theological perspective on the situation (Suggested)
Encourage open dialogues within religious communities about the intersection of religion and politics (Implied)
</details>
<details>
<summary>
2021-06-20: Let's talk about the Biden admin listing political ideologies as bad.... (<a href="https://youtube.com/watch?v=nTSbjuwMTTI">watch</a> || <a href="/videos/2021/06/20/Lets_talk_about_the_Biden_admin_listing_political_ideologies_as_bad">transcript &amp; editable summary</a>)

Beau clarifies misinterpretations of a document from the Biden administration, reassuring viewers that being an activist doesn't equate to being viewed as a domestic violent extremist.

</summary>

"Just because you're an environmentalist or an animal rights activist or any of the other stuff listed here, that doesn't mean that they view you as a DVE."
"If you don't know, this is my area of study. I have read everything the Biden administration has released on this topic."
"Without a constitutional amendment, the U.S. government does not have the authority to designate a domestic group."
"It's just saying that some DVEs have this as a motivation."
"Just on this one, unless you're hurting people, it doesn't apply to you."

### AI summary (High error rate! Edit errors on video page)

Explains the context of a document released by the Biden administration from the Office of the Director of National Intelligence.
Clarifies that misinterpretations circulating online about the document are causing confusion.
Describes the different ways the document is being presented, including inaccuracies and misrepresentations.
Breaks down the definition of domestic violent extremists (DVE) provided in the document.
Emphasizes that the document does not target individuals based on their beliefs or activism.
Assures that the document is not a cause for concern and follows a similar pattern to previous bulletins.
Addresses the potential legal issues around designating certain groups within the U.S. as illegal.
Notes that certain rhetoric online may lead to being tagged as a sympathizer and subject to surveillance.
Stresses the importance of avoiding violent rhetoric online to prevent unwanted attention.
Concludes by providing reassurance and encouraging viewers to have a good day.

Actions:

for concerned viewers,
Read and understand the actual document from the Office of the Director of National Intelligence (suggested)
Avoid using violent rhetoric online to prevent unwanted attention and surveillance (implied)
</details>
<details>
<summary>
2021-06-20: Let's talk about Biden being on the clock for his foreign policy.... (<a href="https://youtube.com/watch?v=RurzLty-sTI">watch</a> || <a href="/videos/2021/06/20/Lets_talk_about_Biden_being_on_the_clock_for_his_foreign_policy">transcript &amp; editable summary</a>)

Biden admin races against time to finalize key foreign policy amid Raisi's win in Iran, impacting Middle East dynamics and Western legitimacy.

</summary>

"Biden administration is running out of time to enact a key piece of their foreign policy."
"Supreme Leader's desires dictate Iran's actions regardless of the president."
"Biden wants the deal so the Iranian government can be more legitimized in the eyes of the Western world."

### AI summary (High error rate! Edit errors on video page)

Biden administration running out of time to enact a key foreign policy piece central to their plans.
Mentioned Raisi's candidacy for president in Iran, now he's won, raising concerns.
Divided opinions on Raisi: some see his alignment with Supreme Leader as a positive, while others criticize his history.
Biden administration suddenly in a hurry to finalize the deal before Raisi takes office.
Regardless of the president, Supreme Leader's desires dictate Iran's actions.
Uncertainty looms over Iran's commitment to the deal post-election.
Biden administration's delay in prioritizing the deal earlier is criticized.
Deal's completion before the new government transitions is deemed necessary.
Potential implications on political dynamics in the Middle East and Iran's legitimacy post-deal.
Biden's goal is not just the deal but also Western legitimacy for the Iranian government.

Actions:

for foreign policy analysts,
Monitor developments in Iran's political landscape (implied).
Stay informed on the Biden administration's foreign policy decisions (implied).
</details>
<details>
<summary>
2021-06-19: Let's talk about the Biden administration's new asylum rules.... (<a href="https://youtube.com/watch?v=VrAi1Qqj7tg">watch</a> || <a href="/videos/2021/06/19/Lets_talk_about_the_Biden_administration_s_new_asylum_rules">transcript &amp; editable summary</a>)

Biden administration revamps asylum rules to allow fleeing from domestic violence and non-state actors, recognizing the dangers faced by Central American asylum seekers.

</summary>

"Our asylum system is designed to take those who are in danger."
"Non-state actors are often more prevalent than the government."
"It only seems fitting and it only seems right that we would take asylum seekers who are fleeing their actions."

### AI summary (High error rate! Edit errors on video page)

Biden administration is revamping asylum rules previously set by Trump administration, which limited the ability of asylum seekers to apply.
Trump's policies aimed to disallow as many immigrants as possible using the asylum system, operating under the assumption that it was often manipulated.
Trump removed the ability to seek asylum from domestic violence and actions of non-state actors, but Biden administration is changing that.
Central American asylum seekers, especially those fleeing domestic violence or non-state actors, should have legitimate reasons for asylum due to lack of safeguards in their countries.
Non-state actors like drug gangs in Central America often have more influence and control than the government, leading many to flee from them.
The United States' war on drugs played a significant role in empowering these non-state actors, making it fitting for the US to take in asylum seekers fleeing from their actions.

Actions:

for advocates for asylum seekers,
Support organizations aiding asylum seekers (suggested)
Advocate for comprehensive asylum policies (implied)
</details>
<details>
<summary>
2021-06-19: Let's talk about Portland cops resigning and what to do.... (<a href="https://youtube.com/watch?v=dMinD1hP96w">watch</a> || <a href="/videos/2021/06/19/Lets_talk_about_Portland_cops_resigning_and_what_to_do">transcript &amp; editable summary</a>)

A large number of cops in Portland resigned from a specialized team over criticism of excessive force, indicating a culture of avoiding accountability.

</summary>

"If a judge, the mayor, and the prosecutors are all saying you're using too much force, it's time for policy changes."
"That to me suggests that the bad apple has already spoiled the bunch."
"If that is just unimaginable, unthinkable, something that you shouldn't agree to, you probably shouldn't be on this team and you probably shouldn't be a cop."

### AI summary (High error rate! Edit errors on video page)

A large number of cops in Portland resigned from a specialized team dealing with civil disturbance after one member was indicted.
The resignations were not just due to the indictment but also because the mayor, judge, and prosecutor criticized the team for using excessive force, including tear gas.
Beau suggests that if the criticism from multiple officials doesn't prompt policy changes, those cops shouldn't be on the team or even be cops at all.
The refusal to accept policy changes indicates a sense of being above accountability and the law within the team.
The decision to quit over charges being brought against a cop in the Terry Jacobs incident shows a lack of willingness to be held accountable.
Beau argues that if officials are pointing out excessive force, it's time for policy changes that should be accepted as part of the community.
Resigning when faced with the need for policy changes implies a culture within the team of expecting to act without oversight or accountability.

Actions:

for community members, police department,
Advocate for policy changes within law enforcement (implied)
Support oversight and accountability measures for police (implied)
Demand transparency in police actions (implied)
</details>
<details>
<summary>
2021-06-18: Let's talk about the reaction to Juneteenth.... (<a href="https://youtube.com/watch?v=_TY7UiFrCuM">watch</a> || <a href="/videos/2021/06/18/Lets_talk_about_the_reaction_to_Juneteenth">transcript &amp; editable summary</a>)

Beau explains the complementary nature of July 4 and Juneteenth, challenging critics and advocating for understanding historical significance.

</summary>

"July 4 and Juneteenth, these are not competing holidays. They're pretty complementary."
"It's marking a historic event where Union troops got to Galveston and got the news to the last slaves."
"They're not competing holidays. One does not take away from the other. They go together."

### AI summary (High error rate! Edit errors on video page)

Juneteenth becoming a federal holiday sparked predictable responses from the right.
Despite GOP support for the holiday, some individuals have taken issue with it.
Charlie Kirk's response criticizes Juneteenth, claiming it undermines the unity of July 4.
Beau challenges Kirk's view, explaining the complementary nature of July 4 and Juneteenth.
July 4 symbolizes the proclamation of equality, while Juneteenth marks an effort to fulfill that promise.
Beau clarifies that Juneteenth is not about race but about a historic event marking progress.
He points out that Union troops reaching Galveston to inform the last slaves is a significant part of Juneteenth.
Beau dismisses the notion of competing holidays and stresses their harmonious coexistence.
He suggests that future holidays may signify moments when more individuals were included in the promises of equality.
Beau concludes by encouraging reflection on the significance of these historical commemorations.

Actions:

for americans, history enthusiasts,
Celebrate Juneteenth and July 4 together, recognizing their complementary nature (implied).
</details>
<details>
<summary>
2021-06-18: Let's talk about repealing the AUMF.... (<a href="https://youtube.com/watch?v=2bQKba2ejEs">watch</a> || <a href="/videos/2021/06/18/Lets_talk_about_repealing_the_AUMF">transcript &amp; editable summary</a>)

The House voted to repeal the broad Authorization for Use of Military Force, transferring power back to Congress and curbing military involvement.

</summary>

"It is Congress's job to declare war, to make war."
"These authorizations allow presidents to engage in military adventurism."
"There's a lot of people who look at the title of commander-in-chief as meaning that the military is at the total discretion of the president, and that's not the case."

### AI summary (High error rate! Edit errors on video page)

The House voted to repeal the Authorization for Use of Military Force (AUMF), a long process expected to be undertaken by the Biden administration with some willingness.
The 2002 AUMF authorized the Iraq War and has been used to justify other actions due to its broad nature.
Power was transferred from Congress to the executive branch in the early 2000s, which needs to be corrected.
Congress, not the executive branch, should declare and make war.
These authorizations enable presidents to partake in military actions, with Biden supporting the repeal of both the 2002 and 2001 AUMFs.
McConnell opposes the repeal without providing a valid reason, potentially to allow for future military actions justified by the broad authorization.
There are uncertainties surrounding the repeal of the 2001 AUMF due to ongoing dynamics in Afghanistan.
Efforts are being made to restore the military's use to its original intent and reduce military involvement in the Middle East and other regions.

Actions:

for policymakers,
Contact your senators to support the repeal of the Authorization for Use of Military Force (implied).
</details>
<details>
<summary>
2021-06-17: Let's talk about the weather and record breaking heat.... (<a href="https://youtube.com/watch?v=4KsvYm00ziE">watch</a> || <a href="/videos/2021/06/17/Lets_talk_about_the_weather_and_record_breaking_heat">transcript &amp; editable summary</a>)

Be prepared for extreme heat, conserve energy, stay hydrated, and support vulnerable community members in the face of record-breaking temperatures across the West.

</summary>

"These temperatures are expected to last for a while."
"Make sure you're hydrated. [...] This is a time to kind of come together as a community."
"These temperatures are expected to occur more and more often."

### AI summary (High error rate! Edit errors on video page)

A heat dome is impacting 18% of the U.S. population, resulting in record-breaking temperatures across the West.
Some temperature records are the highest ever recorded, with Denver hitting 101 degrees and Anaheim and Palm Springs reaching 119 degrees.
Texas and California are asking residents to conserve electricity due to overworked plants.
Heat domes are natural high-pressure ridges that intensify with rising temperatures, leading to longer, hotter periods.
Prolonged high temperatures increase drought conditions and the risk of wildfires in the West.
Extreme temperatures can be lethal, so staying hydrated is vital, especially for vulnerable groups like the elderly and pets.
Community support is critical during such extreme weather events to assist those in need.
Keeping an eye on neighbors, especially those at risk from the heat, and ensuring they are hydrated and safe is necessary.
These extreme temperatures are expected to continue and worsen without action to address climate change.
Beau urges everyone to be vigilant, take care of each other, and prepare for more frequent and severe heatwaves.

Actions:

for residents in areas affected by extreme heat.,
Conserve electricity to support overworked plants (suggested).
Stay hydrated and encourage others to do the same (implied).
Keep an eye on vulnerable community members, such as the elderly, and offer assistance if needed (implied).
</details>
<details>
<summary>
2021-06-17: Let's talk about Madison Cawthorn's Vietnam comparison.... (<a href="https://youtube.com/watch?v=7I9AZjzh7ck">watch</a> || <a href="/videos/2021/06/17/Lets_talk_about_Madison_Cawthorn_s_Vietnam_comparison">transcript &amp; editable summary</a>)

Representative Madison Cawthorn's comparison of civilian arms in the US to prevent tyranny is challenged by Beau, who questions the romanticized notions of conflict and war, advocating for constructive contributions to society instead of glorifying combat.

</summary>

"Nobody ever won a war by dying for their country. They win a war by making the other guy die for his."
"This country needs more engineers. It needs more scientists. It needs more nurses, doctors, welders, truck drivers. We need a lot of things in this country. The one thing we don't need is more combat vets."

### AI summary (High error rate! Edit errors on video page)

Representative Madison Cawthorn made a comparison about civilian arms in the US to prevent tyranny.
He mentioned the idea that civilians with rifles could potentially stop tyranny and defeat a major military force.
The comparison often overlooks the role of the North Vietnamese Army (NVA) backing up the Viet Cong during the conflict.
Beau questions the validity of the comparison between civilians with arms and defeating a well-disciplined, conventional military.
He points out the fantasy versus reality aspect of the comparison when it comes to enduring hardships and sacrifices.
The image of the citizen-soldier and the VC being willing to endure anything is contrasted with modern societal behaviors.
Beau challenges the notion of romanticizing conflict and war, especially when considering the actual consequences and sacrifices involved.
He questions the motivations behind advocating for violent actions that could lead to the destruction of the country.
The comparison with conflicts in Afghanistan and Iraq is brought up to illustrate the long-lasting and devastating nature of such actions.
Beau advocates for focusing on constructive roles like engineers, scientists, nurses, and other professionals rather than glorifying combat.

Actions:

for us citizens,
Support educational programs for engineering, science, healthcare, and vocational training (suggested)
Encourage youth to pursue careers in constructive fields (implied)
Advocate for peacebuilding initiatives and conflict resolution strategies (implied)
</details>
<details>
<summary>
2021-06-16: Let's talk about how Trump's talking points are made.... (<a href="https://youtube.com/watch?v=WzsxfKtG79w">watch</a> || <a href="/videos/2021/06/16/Lets_talk_about_how_Trump_s_talking_points_are_made">transcript &amp; editable summary</a>)

Beau explains how political talking points can erode fundamental freedoms and pave the way for authoritarianism.

</summary>

"They create the problem then they create the solution."
"That's how authoritarians throughout history have swayed public opinion away from the base principles of the country."
"Trumpist talking points, the talking points that are used by his adherents, see if they undermine the base principles of the country and set the stage for authoritarianism."

### AI summary (High error rate! Edit errors on video page)

Explains how authoritarians use talking points to undermine base principles of a country.
Gives examples like anti-protest laws and purity of the vote laws as ways to erode freedoms.
Talks about border checkpoints and how they infringe on citizens' rights.
Mentions Trump and Miller setting up an office to further marginalize certain groups.
Describes Biden reallocating funds to help immigrants in detention report abuse.
Criticizes the use of talking points to shift public opinion towards authoritarianism.
Encourages listeners to analyze political talking points for their impact on basic principles.

Actions:

for concerned citizens,
Analyze political talking points for their impact on basic principles (implied)
</details>
<details>
<summary>
2021-06-16: Let's talk about Tucker Carlson's FBI theory.... (<a href="https://youtube.com/watch?v=-PXWZri1kCI">watch</a> || <a href="/videos/2021/06/16/Lets_talk_about_Tucker_Carlson_s_FBI_theory">transcript &amp; editable summary</a>)

Tucker Carlson's FBI conspiracy theory lacks evidence, deflecting accountability from real instigators of the Capitol riot.

</summary>

"That's not how that works."
"There's literally no evidence that has been presented to suggest this is true."
"A lot of those people were tricked."
"When we pull off the mask, we know what happens at the end of every episode of Scooby-Doo."
"I think it's funny that they're looking for who to blame for inciting this when pretty much everybody knows who really incited it."

### AI summary (High error rate! Edit errors on video page)

Tucker Carlson theorized that the FBI orchestrated the events at the Capitol on January 6th, claiming undercover agents were involved.
Beau disputes Carlson's theory, explaining that having unindicted co-conspirators in indictments does not prove FBI involvement.
Naming unindicted co-conspirators can damage reputations and is often done when evidence is lacking or when the person is cooperating with authorities.
Beau argues that pushing false narratives like Carlson's deflects accountability from those responsible for events like the Capitol riot.
Right-wing pundits initially praised the Capitol events as Americans fighting for freedom but later shifted blame to groups like Black Lives Matter and Antifa to avoid accountability.
Media outlets that spread baseless election fraud claims share responsibility for the Capitol riot due to the misinformation they promoted.
Beau suggests that those who were misled into believing false information were ultimately tricked by manipulative narratives.
He humorously compares Carlson's narrative to a Scooby-Doo mystery, where the true culprits are often revealed to be wealthy individuals.
Beau implies that the true instigators of the Capitol riot are well-known, despite attempts to shift blame onto entities like the FBI.

Actions:

for media consumers,
Fact-check false narratives and hold media outlets accountable (exemplified)
Advocate for responsible reporting and accountability in media (implied)
</details>
<details>
<summary>
2021-06-15: Let's talk about how Republicans can save the country.... (<a href="https://youtube.com/watch?v=gA3jFOc8kPA">watch</a> || <a href="/videos/2021/06/15/Lets_talk_about_how_Republicans_can_save_the_country">transcript &amp; editable summary</a>)

Republicans can save the country by heeding subject matter experts, avoiding authoritarian tendencies, and making informed voting choices in primaries.

</summary>

"All you have to do is actually stay as a Republican and not venture off into that far right brand of authoritarianism."
"Rank and file Republicans are kind of key to preserving the republic."
"You made a mistake once, you back to this brand of authoritarianism once, don't do it again."

### AI summary (High error rate! Edit errors on video page)

Republicans can save the country by listening to subject matter experts, who are not always PhDs but can be found all around us.
Subject matter experts can predict outcomes based on their experience and recognizing patterns.
In 2017-2019, experts warned about Trump's potential to overturn the election due to authoritarian characteristics he exhibited.
It's vital for Republicans to vote for actual Republicans in primaries and not those pushing for authoritarian regimes.
Beau encourages Republicans to watch videos on his channel discussing authoritarian characteristics and Trump's actions as examples.
Staying true to Republican values and avoiding far-right authoritarianism is key to preserving the republic.
Beau points out the danger of a cult of personality and the importance of differentiating between authoritarian leaders.
He contrasts how actions by Biden or Harris versus Obama might have been perceived by the public and media.
Rank and file Republicans play a critical role in safeguarding democracy by making informed voting choices.
Beau stresses the importance of not repeating past mistakes by falling for authoritarian tendencies again.

Actions:

for republicans,
Watch videos discussing authoritarian characteristics and Trump's actions on Beau's channel (suggested)
Vote for actual Republicans in primaries, not those pushing for authoritarian regimes (implied)
</details>
<details>
<summary>
2021-06-15: Let's talk about 750 million refugees coming to the United States.... (<a href="https://youtube.com/watch?v=2kXKvtRGU7M">watch</a> || <a href="/videos/2021/06/15/Lets_talk_about_750_million_refugees_coming_to_the_United_States">transcript &amp; editable summary</a>)

Beau addresses the misconception around 750 million migrants wanting to come to the US, debunking space and economic concerns and suggesting that the US could feasibly accept them.

</summary>

"We could actually do it."
"Realistically, yes, the United States could accept every single person who wants to come to the US in the world."
"It's just a talking point."
"It's not going to be an economic one."
"All of the stuff that gets thrown up, that's not true."

### AI summary (High error rate! Edit errors on video page)

Addresses the misconception of 750 million refugees wanting to come to the United States.
Mentions the arguments against accepting such a large number, citing space and economic concerns.
Counters the space argument by comparing the landmass of China and the US.
Challenges the economic drain argument by suggesting that more people equal more economic activity.
Refers to a 2018 Gallup study that clarifies the 750 million figure pertains to migrants, not just refugees.
Points out that not all migrants want to come to the US, with only 21% expressing a desire to do so.
Calculates that around 150 million individuals actually want to migrate to the US.
Suggests that the US could feasibly accept every person who wants to come, given its size and resources.
Argues that portraying the issue as insurmountable serves as an excuse for inaction.
Asserts that economic and space constraints are not valid reasons to reject migrants.

Actions:

for policy advocates,
Challenge misconceptions about migrants (implied)
Advocate for more inclusive migration policies (implied)
</details>
<details>
<summary>
2021-06-14: Let's talk about why Harris hasn't gone to the border.... (<a href="https://youtube.com/watch?v=khqp8HXLIzg">watch</a> || <a href="/videos/2021/06/14/Lets_talk_about_why_Harris_hasn_t_gone_to_the_border">transcript &amp; editable summary</a>)

Beau visualizes the Northern Triangle, criticizes PR stunts at the border, and advocates for trade over aid to address root causes of migration.

</summary>

"They don't need aid. They need trade."
"Beyond our borders do not live a lesser people."
"Maybe it's time to stop being the world's policeman and start being the world's EMT."
"It isn't fair to the rest of their country."
"They have nothing left to lose."

### AI summary (High error rate! Edit errors on video page)

Analyzing why Vice President Harris hasn't visited the border, Beau visualizes the Northern Triangle and explains the dynamics.
Beau criticizes the notion that Vice President Harris needs to physically go to the border with binoculars, dismissing it as a PR stunt.
The idea of leadership is questioned, with Beau asserting that true leadership involves addressing root causes and implementing effective solutions.
Beau expresses mixed feelings about immigration, recognizing the talent and initiative of those making the journey while also acknowledging the negative impact of brain drain on their home countries.
Criticism is directed at Vice President Harris for her messaging on discouraging migration and the belief that aid alone is insufficient to address the underlying issues.
Beau advocates for prioritizing trade, investment, and infrastructure development in the Northern Triangle over traditional aid, drawing parallels with China's Belt and Road Initiative.
A call to shift away from an "America First" mindset towards a more globally engaged approach is emphasized, stressing the need to support self-sufficiency and growth in other countries.
Critiques are made of past U.S. foreign policy actions that have contributed to the challenges in the Northern Triangle, suggesting a shift towards constructive engagement rather than intervention.
The transcript concludes with a reflection on the importance of trade, infrastructure, and investment in enabling countries to thrive independently.

Actions:

for global citizens, policymakers,
Advocate for policies prioritizing trade, investment, and infrastructure in countries like Honduras, Guatemala, and El Salvador (implied)
Support initiatives that enable self-sufficiency and growth in communities facing migration challenges (implied)
Educate others on the importance of shifting from aid-based approaches to sustainable trade partnerships (implied)
</details>
<details>
<summary>
2021-06-14: Let's talk about clearing up the UN Arms Trade Treaty.... (<a href="https://youtube.com/watch?v=yH2nqyfi1Ig">watch</a> || <a href="/videos/2021/06/14/Lets_talk_about_clearing_up_the_UN_Arms_Trade_Treaty">transcript &amp; editable summary</a>)

Beau clarifies the misconceptions around the UN Arms Trade Treaty, stressing its focus on illegal international arms transfers and its lack of impact on domestic gun ownership.

</summary>

"It has somehow been suggested that the Biden administration plans to re-sign a treaty from the UN."
"This whole thing is a talking point designed to make people believe that somehow this treaty from the UN has something to do with Americans buying guns."
"So much so that in the treaty it says that it reinforces and affirms the sovereign right of any state to regulate and control conventional arms exclusively within its territory."
"Trump unsigning it did absolutely nothing. That's not an actual diplomatic thing."
"Realistically, this applies to all conventional arms. So rifles and pistols, all the way up to tanks and aircraft."

### AI summary (High error rate! Edit errors on video page)

Explains the misinformation circulating regarding the UN Arms Trade Treaty and the Biden administration's alleged plans to re-sign it.
Clarifies that the treaty regulates illegal international transfer of conventional arms, not American citizens buying guns domestically.
Trump's decision to "unsign" the treaty was purely political as the treaty was never ratified.
Emphasizes that the treaty does not impact the majority of Americans unless they are involved in illegal international arms sales.
States that the treaty affirms a country's sovereign right to regulate arms within its territory and does not involve the UN coming to enforce it.
Speculates that the treaty is back in focus due to the Democrats being in power, not because it affects domestic gun ownership.
Points out that the treaty covers a range of conventional arms, from rifles to tanks, focusing on illegal international transfers.

Actions:

for policy advocates,
Contact policymakers to advocate for accurate information dissemination about international treaties (suggested).
</details>
<details>
<summary>
2021-06-13: Let's talk about whether the US is fundamentally racist.... (<a href="https://youtube.com/watch?v=ZtMtEsrXuMg">watch</a> || <a href="/videos/2021/06/13/Lets_talk_about_whether_the_US_is_fundamentally_racist">transcript &amp; editable summary</a>)

Teaching American history necessitates acknowledging its institutionalized racism, from slavery to modern legislation, shaping a fundamentally racist nation.

</summary>

"United States was founded and built on institutionalized racism."
"You can ban teaching this all you want, but it's reality."
"History is littered with racism, with structural, institutionalized racism."
"You learn history so you don't repeat the mistakes."
"Until people are comfortable with acknowledging the past, we have to assume that there's still a lot of racism."

### AI summary (High error rate! Edit errors on video page)

Addressing the debate on teaching American history through the lens of racism, particularly in relation to Joe Biden and Critical Race Theory (CRT).
Clarifying that CRT is an academic movement, not specific to Biden, and discussing the fundamentally racist nature of the 1994 crime bill.
Exploring the concept of the United States being fundamentally racist by examining historical instances of institutionalized racism from the country's founding to the present.
Describing the long history of structural and institutionalized racism in the U.S., including legislation like the Violent Crime Control and Law Enforcement Act of 1994.
Arguing that the United States' history is intertwined with racism, making it fundamentally racist according to historical evidence.
Emphasizing the importance of acknowledging past injustices and using history to prevent repeating discriminatory patterns.
Drawing parallels between a person's lifespan and the United States' history to illustrate the persistent nature of racism in the country.
Asserting that denying the history of institutionalized racism in the U.S. hinders progress in addressing present-day racial issues.

Actions:

for educators, historians, activists,
Teach history truthfully and inclusively to acknowledge past injustices and prevent their repetition (implied).
Advocate for curricula that address the historical reality of institutionalized racism in the United States (implied).
</details>
<details>
<summary>
2021-06-13: Let's talk about an American hero who doesn't get the credit they should.... (<a href="https://youtube.com/watch?v=izal0V4Mevc">watch</a> || <a href="/videos/2021/06/13/Lets_talk_about_an_American_hero_who_doesn_t_get_the_credit_they_should">transcript &amp; editable summary</a>)

Beau talks about an American hero, Medgar Evers, whose pivotal role in the civil rights movement is often overshadowed by the great man theory, stressing the importance of recognizing collective efforts over individual figures.

</summary>

"You can kill a man, but you can't kill an idea."
"It's never just one person. It's always a movement of people."

### AI summary (High error rate! Edit errors on video page)

Introducing an American hero whose name isn't mentioned enough in history despite being a pivotal figure in the civil rights movement.
Born in 1925, he walked 12 miles to school daily, served in the military during WWII, and fought at the Battle of Normandy.
In 1954, he applied to the University of Mississippi Law School but was denied due to his race.
Became the first field secretary of the NAACP in Mississippi in the same year.
Organized boycotts, marches, and voting drives, putting himself at risk for the cause.
Survived multiple attempts on his life, always aware of the dangers he faced.
Killed on June 12, 1963, carrying a t-shirt that read "Jim Crow must go," becoming the first black man admitted to an all-white hospital posthumously.
His death sparked marches and galvanized the civil rights movement.
The circumstances of his death took decades to bring his killer to justice, but it pushed the movement forward significantly.
Despite being recognized more now, his name isn't as prominent as others due to the great man theory overshadowing collective movements.

Actions:

for history enthusiasts, civil rights advocates,
Research and share more about Medgar Evers' life and contributions (suggested)
Support civil rights organizations and movements in your community (suggested)
</details>
<details>
<summary>
2021-06-12: Let's talk about Trump vs McConnell in 2022.... (<a href="https://youtube.com/watch?v=R97VXKae81M">watch</a> || <a href="/videos/2021/06/12/Lets_talk_about_Trump_vs_McConnell_in_2022">transcript &amp; editable summary</a>)

Trump's interventions in Senate primaries and McConnell's pursuit of power may reshape the political landscape, impacting the focus on President Biden in the upcoming elections.

</summary>

"Trump is intervening in primaries related to the Senate even though he's out of office."
"McConnell's goal is to become Senate Majority Leader again, not necessarily to get rid of Trumpism."
"The Republican establishment wants to make 2022 a referendum on President Biden, but Trump's interventions might shift the focus back to himself."

### AI summary (High error rate! Edit errors on video page)

Trump is intervening in primaries related to the Senate even though he's out of office.
McConnell is pursuing his own interests through a PAC called the Senate Leadership Fund, making determinations on who to endorse.
McConnell's goal is to become Senate Majority Leader again, not necessarily to get rid of Trumpism.
The PAC may support moderate candidates over Trump loyalists based solely on polling to further McConnell's interests in regaining power.
McConnell's actions may set up a contentious relationship with Trump, leading to more spending by candidates during the primary season.
The Republican establishment wants to make 2022 a referendum on President Biden, but Trump's interventions might shift the focus back to himself.

Actions:

for political observers, voters,
Support and get involved with candidates in Senate primaries (implied)
Stay informed and engaged in political developments to make informed decisions during elections (implied)
</details>
<details>
<summary>
2021-06-12: Let's talk about Abbott's wall and the folly of Texas.... (<a href="https://youtube.com/watch?v=C57ci3F0l8s">watch</a> || <a href="/videos/2021/06/12/Lets_talk_about_Abbott_s_wall_and_the_folly_of_Texas">transcript &amp; editable summary</a>)

Governor Abbott's plan to build a wall in Texas is a political ploy that won't address the real issues faced by Texans, shifting blame onto marginalized groups.

</summary>

"Walls have been proven to be historically kind of ineffective."
"It's a waste of money and it's just a vanity project for another authoritarian goon."

### AI summary (High error rate! Edit errors on video page)

Governor Abbott's promise to build a wall in Texas is more about politics than actual border security.
Walls have historically proven to be ineffective as people find ways to bypass them.
Abbott's focus on building a wall is a political strategy to deflect blame onto others.
Despite Texas facing infrastructure issues like poor roads and levees, Abbott's priority is on the wall.
Texas ranks poorly in healthcare access, quality, education, air and water quality, and economic opportunities despite having a strong economy.
The real issue in Texas lies with those in power who have rigged the system for themselves, not with people crossing the border.
Building a wall is just a distraction tactic to shift blame away from the failures of those in government.
Walls throughout history have ultimately failed as they can always be overcome.
The wall proposed by Abbott is a wasteful vanity project that will not address the underlying issues faced by Texans.

Actions:

for texans, activists,
Advocate for better infrastructure projects in Texas (implied)
Support policies that address healthcare, education, and economic opportunities in Texas (implied)
</details>
<details>
<summary>
2021-06-11: Let's talk about acceptance and reaching rural Americans.... (<a href="https://youtube.com/watch?v=V8nLk--EGjw">watch</a> || <a href="/videos/2021/06/11/Lets_talk_about_acceptance_and_reaching_rural_Americans">transcript &amp; editable summary</a>)

Beau explains reaching out to rural Americans by focusing on shared principles and avoiding trying to convince them they're wrong about social issues.

</summary>

"Lean into the fact that every rural American knows that's wrong."
"If you can get a rural American to apply the base principles they use every day in their life to everything, they become socially progressive overnight."

### AI summary (High error rate! Edit errors on video page)

Explains changing opinions and reaching out to rural Americans about social issues.
Mentions a message about understanding the concept of "on a long enough timeline, we win."
Talks about acceptance of gay marriage in the United States reaching an all-time high.
Advises not to try to convince rural Americans they're wrong but to focus on their base principles.
Emphasizes the right to be left alone, a fundamental principle in rural areas.
Describes how small farms in rural areas help each other in mutually beneficial ways.
Suggests leaning into the idea that people should be able to do what they want as long as they're not harming others.
Gives an example of how rural Americans handle situations like kids and pronouns differently.
Points out that rural people don't tell others how to raise their kids and avoid involving the law.
Encourages starting with the shared belief that using government force to regulate private lives is wrong.

Actions:

for social activists, community organizers.,
Start community dialogues on shared values and principles (suggested).
Organize workshops on mutual support and cooperation within rural communities (implied).
</details>
<details>
<summary>
2021-06-11: Let's talk about Trump's DOJ investigating congress.... (<a href="https://youtube.com/watch?v=_6BIKw3ufd4">watch</a> || <a href="/videos/2021/06/11/Lets_talk_about_Trump_s_DOJ_investigating_congress">transcript &amp; editable summary</a>)

Former President Trump's Department of Justice used a secret subpoena process on members of Congress; the focus should be on ensuring equal standards and legislative fixes for information gathering.

</summary>

"The outrage shouldn't be because it happened to members of Congress; the focus should be on whether the Department of Justice followed normal procedures."
"If DOJ followed policies and rules in place to get the subpoena, there isn't a scandal – it's about applying the same standards to everyone."
"The real scandal lies in the low standards applied by DOJ and the need for legislative changes."
"Changing laws and rules with higher standards for gathering information is the solution, not pointless inquiries."
"Policies need to be resilient and capable of withstanding misuse by future administrations."

### AI summary (High error rate! Edit errors on video page)

Former President Trump's Department of Justice used a secret subpoena process to gather information on members of Congress.
The outrage shouldn't be because it happened to members of Congress; the focus should be on whether the Department of Justice followed normal procedures.
If DOJ followed policies and rules in place to get the subpoena, there isn't a scandal – it's about applying the same standards to everyone.
The burden of proof required to gather such information may be too low and needs to be raised.
Politicizing law enforcement and using secretive forms can be dangerous for a free society.
Legislative fixes are needed to ensure equal application of rules and procedures in gathering information.
The real scandal lies in the low standards applied by DOJ and the need for legislative changes.
The focus should not be on how it happened to a member of Congress, but on how it happened in general.
Policies need to be resilient and capable of withstanding misuse by future administrations.
Changing laws and rules with higher standards for gathering information is the solution, not pointless inquiries.

Actions:

for legislators, activists, citizens,
Push for legislative changes to set higher standards for information gathering (suggested)
Advocate for resilient policies that withstand misuse by future administrations (implied)
</details>
<details>
<summary>
2021-06-10: Let's talk about infrastructure in the US and China.... (<a href="https://youtube.com/watch?v=SH5NI1IogPU">watch</a> || <a href="/videos/2021/06/10/Lets_talk_about_infrastructure_in_the_US_and_China">transcript &amp; editable summary</a>)

Beau examines the consequences of "America First" rhetoric on global economics, advocating for strategic investments in infrastructure to ensure competitiveness against China.

</summary>

"There's no long-term planning, just every deal has to be the best and the U.S. has to come out on top on every single one of them."
"Those who are shouting America first at the top of their lungs will be the reason soon it will be China first."
"Trade is what generates economic activity. That's where the money comes from."
"The US economy is slow in part because we don't have the infrastructure to speed it up."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Exploring the new economic landscape in the United States and the world.
Critiquing the "America First" mentality and its impact on the country's global standing.
Addressing the desire for every economic deal to favor the United States.
Pointing out the unrealistic expectations of bringing back outdated jobs due to automation.
Criticizing politicians who perpetuate false promises about job resurgence.
Analyzing China's Belt and Road Initiative and its massive scale involving numerous countries.
Contrasting China's substantial investment in global infrastructure with the US's comparatively limited spending.
Advocating for a foreign policy centered on trade and development over military interventions.
Noting China's soft power influence through economic partnerships and infrastructure development.
Suggesting the US could capitalize on China's shortcomings by promoting green energy solutions and addressing environmental concerns.
Warning that continued neglect of global economic shifts could lead to China surpassing the US.
Stressing the importance of investing in infrastructure to boost economic competitiveness.
Comparing the US's infrastructure spending proposal with other countries' investments in opening markets.
Emphasizing the need for robust infrastructure to enhance the US economy's growth potential.

Actions:

for economic analysts, policymakers,
Push for increased investment in infrastructure projects in your community (suggested)
Advocate for a foreign policy focused on trade, development, and humanitarian efforts (suggested)
</details>
<details>
<summary>
2021-06-10: Let's talk about 1 in 5 thinking Trump will be reinstated.... (<a href="https://youtube.com/watch?v=9Wh0YFFCwT8">watch</a> || <a href="/videos/2021/06/10/Lets_talk_about_1_in_5_thinking_Trump_will_be_reinstated">transcript &amp; editable summary</a>)

One in five Americans believe in a baseless theory of Trump's reinstatement, undermining democracy and exposing gullibility, urging critical thinking education.

</summary>

"That's not real. That's not going to happen. That's not how this works."
"They're being played because those in positions of leadership within this party, within the conservative movement, know that a large percentage of their constituents are gullible."
"Continuing to be pushed by these same types of people are undermining the very foundations of representative democracy in this country."

### AI summary (High error rate! Edit errors on video page)

One in five Americans believes in a theory that claims former President Trump could be reinstated through audits.
There is no legal or factual basis for this theory.
Even if audits showed discrepancies, the process to reinstate Trump is not feasible.
The theory serves as a tactic to keep the base engaged and energized for the upcoming midterms.
Leaders are aware of their constituents' gullibility and exploit it for financial gain.
Continuing to push baseless theories erodes the foundations of representative democracy.
Education on critical thinking and questioning beliefs is vital to combat misinformation.
This situation should be a wake-up call regarding the importance of critical thinking skills.

Actions:

for voters, educators, activists,
Educate others on critical thinking skills and encourage questioning beliefs (suggested)
Remain vigilant against misinformation and baseless theories (implied)
</details>
<details>
<summary>
2021-06-09: Let's talk about which party is most gullible and confirmation bias.... (<a href="https://youtube.com/watch?v=PhXwliDxoy4">watch</a> || <a href="/videos/2021/06/09/Lets_talk_about_which_party_is_most_gullible_and_confirmation_bias">transcript &amp; editable summary</a>)

Beau talks about gullibility, misinformation, and the need for critical thinking to combat partisan divides and susceptibility to misinformation among conservatives.

</summary>

"If we start locking up conservatives every time they get tricked, we're going to run out of space pretty quickly."
"Most of the misinformation that is out there right now on social media is favorable to conservatives."
"Objective reality has a well-known liberal bias."
"We're going to have to start focusing more on teaching critical thinking and questioning your own beliefs."
"This culture war nonsense that's going on, it's just pushing people further into those little divides."

### AI summary (High error rate! Edit errors on video page)

Talks about consuming information, gullibility, partisanship, and realizing mistakes.
Mentions an individual involved in the Capitol incident on January 6th who now realizes he was deceived.
The person is in custody since January 9th, but didn't commit any violent acts.
Advocates for the person to be released on house arrest because he was gullible and tricked.
Expresses concern about keeping someone locked up just because they were deceived.
Mentions the difficulty in discerning truth from fiction for conservatives based on a study from Ohio State University.
Points out confirmation bias and how it influences beliefs.
Suggests that conservative leaders are aware of the gullibility and may exploit it.
Calls for teaching critical thinking and questioning beliefs to combat misinformation.
Warns against culture wars leading to further divides and susceptibility to misinformation.

Actions:

for conservative thinkers,
Teach critical thinking skills to combat misinformation (suggested)
Question your own beliefs and seek diverse perspectives (suggested)
Address the gap in discerning fact from fiction in news stories (suggested)
</details>
<details>
<summary>
2021-06-09: Let's talk about Snake Eyes and woke GI Joe.... (<a href="https://youtube.com/watch?v=Cm_oeD52CFM">watch</a> || <a href="/videos/2021/06/09/Lets_talk_about_Snake_Eyes_and_woke_GI_Joe">transcript &amp; editable summary</a>)

Beau explains G.I. Joe's long-standing diversity and inclusivity, challenging the controversy around its "wokeness" and urging to embrace diversity for strength.

</summary>

"G.I. Joe has always been really woke."
"The reason G.I. Joe resonated with an entire generation is because there was a character that was relatable to everybody."
"It's sad that you missed that."
"The whole point was to embrace diversity."
"I don't think anybody is going to care that Snake Eyes went from white to Asian."

### AI summary (High error rate! Edit errors on video page)

Addresses the controversy around G.I. Joe becoming "woke" with diverse representation.
Points out that G.I. Joe has always been inclusive with characters of different races and genders.
Mentions the introduction of Russian characters during the Cold War era.
Talks about the Eco Warriors released in 1991 who fought pollution, resembling trans Joe's fighting climate change.
Explains how G.I. Joe resonated with a generation due to relatable characters from diverse backgrounds.
Argues that embracing diversity was a strength that built the G.I. Joe teams.
Criticizes the idea of provoking outrage over Snake Eyes being portrayed as Asian.

Actions:

for pop culture enthusiasts,
Watch and support media that embraces diversity and inclusivity (implied)
</details>
<details>
<summary>
2021-06-08: Let's talk about Virginia, teachers, and protected speech.... (<a href="https://youtube.com/watch?v=EkHVyAvBkkU">watch</a> || <a href="/videos/2021/06/08/Lets_talk_about_Virginia_teachers_and_protected_speech">transcript &amp; editable summary</a>)

A teacher in Virginia reinstated after refusing to address children by preferred pronouns, sparking debate on freedom of speech, religion, and the separation of church and state.

</summary>

"It's lying to a child. It's abuse to a child. It's sinning against our God. Wow."
"The United States has a separation of church and state."
"We are moving away from the idea that people in positions of authority can use that authority to make others feel lesser."
"There's no reason to subject a child to that. There's no reason to disrupt the school."
"Once he does violate policy, well, that's a whole different story."

### AI summary (High error rate! Edit errors on video page)

A teacher in Virginia was reinstated after being sent home by the school board for refusing to address children by their preferred pronouns.
The teacher cited religious beliefs as the reason for not affirming that a biological boy can be a girl and vice versa.
The school board tried to be proactive and sent the teacher home before any policy violation occurred.
Beau argues that as a government employee, one does not have absolute freedom of speech or religion.
He explains the separation of church and state in the U.S., stating that using religion to circumvent policy is not acceptable.
Beau refutes the claim that the U.S. is a Christian nation, citing historical evidence like the Treaty of Tripoli.
He stresses the importance of moving away from authority figures making others feel lesser and disrupting schools.
Beau predicts that the teacher may return to school, refuse to abide by policy, and eventually be terminated for it.
He notes that while the teacher's initial statement may be protected speech, violating policy is a different matter.
Beau concludes by suggesting that the situation will be monitored and ends with well wishes.

Actions:

for educators, policymakers, activists,
Monitor and advocate for inclusive policies in schools (implied)
Support efforts to create safe and respectful environments for all students (implied)
</details>
<details>
<summary>
2021-06-08: Let's talk about Australia, the US, and three other countries.... (<a href="https://youtube.com/watch?v=tICfOS_Tflk">watch</a> || <a href="/videos/2021/06/08/Lets_talk_about_Australia_the_US_and_three_other_countries">transcript &amp; editable summary</a>)

Beau shares insights on encryption technologies, the Five Eyes alliance, and civil liberties controversies, debunking wild theories in the process.

</summary>

"Just because something sounds like a wild theory doesn't mean that it is."
"Five eyes definitely exists."
"There is definitely a civil liberties controversy."

### AI summary (High error rate! Edit errors on video page)

Receives a message about a unique reaction to news about encryption technologies.
Shares information about Phantom Secure being taken down by authorities.
Mentions the emergence of a new encrypted app called Anom, which authorities used to monitor and run investigations.
Explains the concept of the Five Eyes alliance involving the US, UK, Canada, Australia, and New Zealand for sharing signals intelligence.
Raises concerns about intelligence agencies potentially circumventing laws by using this alliance to spy on their own citizens.
Acknowledges the existence of civil liberties controversies surrounding the Five Eyes alliance.
Mentions the predecessor program Echelon and its lack of self-regulation by intelligence agencies.

Actions:

for internet users,
Research and stay informed about encryption technologies and surveillance practices (implied)
Advocate for privacy rights and civil liberties in the digital age (implied)
</details>
<details>
<summary>
2021-06-07: Let's talk about blame, leaks, and why we won't be ready.... (<a href="https://youtube.com/watch?v=DzM4VKoOuPE">watch</a> || <a href="/videos/2021/06/07/Lets_talk_about_blame_leaks_and_why_we_won_t_be_ready">transcript &amp; editable summary</a>)

Beau raises concerns about blaming China for past failures instead of focusing on strengthening medical infrastructure to prevent future public health crises.

</summary>

"They did this to us. Therefore, we don't really need to change because that's not going to happen again."
"There is one solution to making sure we don't have this problem again, and that is to increase the resiliency of our medical infrastructure, period, full stop."
"It is the only solution. There is no way to prevent and contain this in every situation."
"If they want to talk about national security and they do not want to talk about increasing medical infrastructure, making sure everybody has medical care, they're lying to you."

### AI summary (High error rate! Edit errors on video page)

Raises concerns about the lack of readiness for future public health crises.
Criticizes the focus on blaming China for past failures rather than addressing underlying issues.
Points out the ulterior motives behind blaming other countries for failures.
Emphasizes the need to increase the resiliency of medical infrastructure to prevent future crises.
Suggests that expanding medical education and ensuring universal healthcare are key steps in improving resilience.
Stresses the importance of increasing the capacity of medical infrastructure to handle future emergencies.
Argues that a strong healthcare system is the only effective solution to prevent and contain public health crises.
Condemns politicizing the issue of national security and public health for personal gains.
Warns that without focusing on healthcare, the same crisis will happen again.
Calls out the hypocrisy of prioritizing national security without addressing healthcare infrastructure.

Actions:

for policy makers, healthcare advocates,
Advocate for increasing medical education and universal healthcare access (implied)
Support initiatives that aim to strengthen medical infrastructure and capacity (implied)
</details>
<details>
<summary>
2021-06-07: Let's talk about Senator Manchin and one party.... (<a href="https://youtube.com/watch?v=etSd4nE0xnE">watch</a> || <a href="/videos/2021/06/07/Lets_talk_about_Senator_Manchin_and_one_party">transcript &amp; editable summary</a>)

Senator Manchin rejects voting protection acts, Beau argues compromising on democracy aids in its undermining by one party.

</summary>

"If your goal is to preserve American democracy and theirs is to undermine it, if you compromise, you are now assisting in undermining it."
"You are either pregnant or you're not. There's not a lot of gray area here."
"It doesn't matter how many stakes you throw to that tiger, it will not turn into a vegetarian."
"There is one party responsible for this."
"If the senators like Manchin and others of his sort do not find their courage, we will end up with one party in this country."

### AI summary (High error rate! Edit errors on video page)

Senator Manchin refuses to support the For the People Act or end the filibuster for voting protections.
Beau compares Manchin's behavior to a Trump supporter for rejecting reality.
Manchin believes partisan voting legislation will harm democracy, justifying his opposition to the For the People Act.
Beau argues that American democracy is not a both sides issue and one party has shown intent to undermine it.
Compromising on preserving democracy is aiding in its undermining, as one party clearly does not support it.
Beau stresses that there can be no compromise on preserving Americans' voting rights.
He points out that one party is actively working to undermine voting rights and American democracy.
Beau criticizes the idea of bipartisan compromise when one party is actively against preserving democracy.
He likens compromising on democracy to aiding in its erosion, drawing a clear line on preserving voting rights.
Beau warns that without courage from senators like Manchin, the country may end up with one dominant party.

Actions:

for voters, democracy advocates,
Contact Senator Manchin to express support or opposition to his stance on voting protections (suggested)
Join local democracy advocacy groups to actively work towards preserving voting rights (implied)
</details>
<details>
<summary>
2021-06-06: Let's talk about Biden's July 4th goal and maps.... (<a href="https://youtube.com/watch?v=7dsHj2MaHr0">watch</a> || <a href="/videos/2021/06/06/Lets_talk_about_Biden_s_July_4th_goal_and_maps">transcript &amp; editable summary</a>)

President Biden set a vaccination goal for July 4th, revealing a partisan divide in vaccination rates and Republican leadership failures, endangering their own base.

</summary>

"Public health has become a partisan issue."
"Republican Party is still failing to lead."
"Imagine how selfish you have to be to quite literally write off your own base."
"Go get your shots. Go get vaccinated."
"Do your part."

### AI summary (High error rate! Edit errors on video page)

President Biden set a goal for 70% of the population to have at least one shot by July 4th.
The New York Times published a map showing states on target to hit the goal, with a clear gap between southern states.
Maps comparing party affiliation of governors to vaccination rates show a clear correlation.
Public health has become a partisan issue, with Republican-led states falling behind in vaccination rates.
Beau criticizes the Republican Party for failing to lead and putting their own voters at risk.
Republicans are rejecting objective reality for social media approval, endangering their base.
Beau predicts a significant difference in COVID-19 losses based on party affiliation.
He questions the selfishness of writing off their base to appease the former president and downplay the severity of the pandemic.
Beau urges people to get vaccinated and do their part in combating the public health crisis.

Actions:

for public health advocates,
Get vaccinated and encourage others to do the same (implied).
</details>
<details>
<summary>
2021-06-05: Let's talk about coming together under China and good news.... (<a href="https://youtube.com/watch?v=56vTBOoUw64">watch</a> || <a href="/videos/2021/06/05/Lets_talk_about_coming_together_under_China_and_good_news">transcript &amp; editable summary</a>)

Beau outlines China's role in fostering peace and development in Afghanistan through a regional coalition, reducing the risk of conflict post-US and NATO withdrawal.

</summary>

"If everybody follows through with this, this is a good thing overall for the country."
"We're not supposed to build empires. They're a country."

### AI summary (High error rate! Edit errors on video page)

Beau expresses reservations about US leadership in China due to lack of regional security force.
Foreign ministers of China, Pakistan, and Afghanistan reached an eight-point consensus for peace.
Parties in Afghanistan agreed to pursue peace through political, not military, means.
China mediates between Pakistan and Afghanistan to foster friendship.
China and Pakistan commit to supporting reconstruction and economic development in Afghanistan.
Belt and Road cooperation will increase for regional connectivity.
Focus on healthcare and education development in Afghanistan by Pakistan and China.
Security cooperation aims to address opposition groups in Afghanistan with a token security force.
Face-to-face meetings planned among the three parties for ongoing collaboration.
Movement from China and Pakistan reduces the risk of loss of life post-US and NATO exit.

Actions:

for foreign policy enthusiasts,
Contact local organizations to support education and healthcare development in Afghanistan (implied).
Join or support initiatives focused on peace-building in conflict regions (implied).
</details>
<details>
<summary>
2021-06-05: Let's talk about Trump running for the House and the speaker's bargain.... (<a href="https://youtube.com/watch?v=vF4ZVbn9TIA">watch</a> || <a href="/videos/2021/06/05/Lets_talk_about_Trump_running_for_the_House_and_the_speaker_s_bargain">transcript &amp; editable summary</a>)

Former president Trump considering running for a house seat and becoming Speaker of the House raises concerns about potential manipulation and power dynamics within Congress.

</summary>

"Transparency in how representatives vote might not be as beneficial as previously thought."
"If Trump becomes Speaker of the House, he has the ability to bring all Republicans in line."
"Progressive representatives may have to compromise their values to get bills they care about onto the floor."

### AI summary (High error rate! Edit errors on video page)

Former president Trump is considering running for a house seat and becoming Speaker of the House.
Transparency in how representatives vote might not be as beneficial as previously thought.
A theory suggests that keeping representatives' votes secret can prevent corruption from lobbyists or the speaker.
The concept of the "Speaker's Corrupt Bargain" involves manipulating new representatives based on their voting behavior.
If Trump were to become Speaker of the House, he could wield significant power over Republicans, influencing their votes and enforcing purity tests.
Progressive representatives may have to compromise their values to get bills they care about onto the floor.

Actions:

for politically engaged citizens,
Pay attention to potential power dynamics and manipulation within Congress (implied)
</details>
<details>
<summary>
2021-06-04: Let's talk about republicans not wanting to debate.... (<a href="https://youtube.com/watch?v=X_BCjN1iBZQ">watch</a> || <a href="/videos/2021/06/04/Lets_talk_about_republicans_not_wanting_to_debate">transcript &amp; editable summary</a>)

Republican Party hesitates to participate in debates, lacking consistency and leadership under Trump's influence.

</summary>

"The Republican Party is still beholden to Trump."
"They don't want to talk about a platform because they don't have one."
"If the Republican Party is just intent on destroying itself and stepping away from objective reality, let them."

### AI summary (High error rate! Edit errors on video page)

Republican Party threatens to boycott debates over format and moderation issues.
Debates serve as a platform for candidates to showcase familiarity with party platform, presidential qualities, and leadership abilities.
Republican Party lacks a consistent platform, as it is dictated by Trump's whims.
Party hesitant to address past statements and defend their record on national TV.
Republican Party struggles to showcase leadership skills, especially outside their core base.
Suggestion to include third-party candidates in debates if Republicans choose to step away from reality.
Party still tied to Trump, lacks clear leadership.
Beau questions the necessity of a press release to state the obvious about the Republican Party's current status.

Actions:

for voters, political activists,
Advocate for including third-party candidates in debates (implied)
Stay informed and engaged in political developments (implied)
</details>
<details>
<summary>
2021-06-04: Let's talk about Trump, Facebook, and the future.... (<a href="https://youtube.com/watch?v=iV3aulf1P68">watch</a> || <a href="/videos/2021/06/04/Lets_talk_about_Trump_Facebook_and_the_future">transcript &amp; editable summary</a>)

Former President Trump will stay banned from Facebook for two years, and politicians will no longer have automatic newsworthy exemptions, potentially changing the global social media landscape.

</summary>

"Former President Trump will stay banned for a couple of years."
"Facebook is no longer going to assume that politicians have newsworthy posts."
"I think that's going to have much more far-reaching effects than people are imagining."
"We may be reaching a point where the Facebook experience, where the social media experience differs even more from country to country."
"As far as the partisan stuff goes, yeah, he's kind of out of the midterms."

### AI summary (High error rate! Edit errors on video page)

Former President Trump will remain banned from Facebook for a couple of years.
Facebook will not automatically assume that politicians' posts are newsworthy.
Politicians will no longer be exempt from certain rules on social media platforms.
Facebook will now apply exemptions for politicians and add a notice below the post.
This change may have far-reaching effects on political discourse.
Tech experts once struggled to remove racist content from Twitter due to potentially catching Republican politicians with algorithms.
The impact of these changes may vary across different regions and countries.
Different standards for newsworthy content may apply in different countries.
The diversity of acceptable speech standards may lead to varying social media experiences globally.
These changes may have more significant implications than Trump's ban for a couple of years.

Actions:

for social media users,
Monitor and participate in the evolving policies and practices of social media platforms (suggested)
Stay informed about how social media regulations could impact political discourse and representation (suggested)
Advocate for transparent and consistent standards for social media content globally (implied)
</details>
<details>
<summary>
2021-06-03: Let's talk about the San Jose footage.... (<a href="https://youtube.com/watch?v=gvJjyggOcZA">watch</a> || <a href="/videos/2021/06/03/Lets_talk_about_the_San_Jose_footage">transcript &amp; editable summary</a>)

Beau explains the slow and deliberate approach taken by officers in San Jose, acknowledging mistakes but ultimately recognizing their success in ensuring no harm to civilians.

</summary>

"If you're watching that footage, when the supervisor comes out, the person they got the key card from, had they been moving quickly, they might have hurt that person by accident."
"At the end of the day, it is almost certain that them choosing to move in when they did saved lives."
"Wasn't perfect. At times, it was kind of ugly. But it worked."
"And nobody got hurt."
"Overall, I think they did fine."

### AI summary (High error rate! Edit errors on video page)

Explains the slow and deliberate approach taken by officers in San Jose during a shooting incident.
Mentions that the officers were not well-trained in room clearing, leading to some cringe-worthy moments.
Points out that the officers from different departments had not trained together, causing some disorganization.
Acknowledges that despite some mistakes, the officers managed to clear the building without hurting anyone innocent.
Expresses satisfaction with law enforcement's actions, stating that moving in when they did likely saved lives.
States that while there were imperfections, the overall outcome was successful and no innocent individuals were harmed.

Actions:

for law enforcement personnel,
Analyze and improve inter-departmental coordination and training (implied)
Recognize the importance of ensuring minimal risk to civilians during law enforcement operations (implied)
</details>
<details>
<summary>
2021-06-03: Let's talk about cutting the mic on Memorial Day's history.... (<a href="https://youtube.com/watch?v=8WGB2xRmlkE">watch</a> || <a href="/videos/2021/06/03/Lets_talk_about_cutting_the_mic_on_Memorial_Day_s_history">transcript &amp; editable summary</a>)

A lieutenant colonel silenced for sharing the untold origins of Memorial Day that began with newly freed slaves honoring fallen troops in Charleston, challenging historical narratives.

</summary>

"Memorial Day began where the war began in Charleston."
"It's incredibly hard not to draw that line."
"The spirit was there."
"I challenge anybody to find an event that is similar in nature prior to the one in Charleston."
"You may not be able to draw that direct line via historical standards, but the spirit was there."

### AI summary (High error rate! Edit errors on video page)

The lieutenant colonel's mic was cut off during a speech about the origins of Memorial Day, specifically the first observance.
The first national observance of Memorial Day was in Arlington on May 30, 1868, but the origins go back further to Charleston in 1865.
Newly freed slaves in Charleston buried Union troops with proper honors and held a parade on May 1, 1865, a year before other observances.
A Civil War Historical Society in 1916 tried to inquire about this event in Charleston but were met with denial from the Charleston Historical Society.
The official birthplace designation of Memorial Day in Arlington was made in 1966 for political reasons with little real scholarship.
The events in Charleston may not have definitively led to Memorial Day observances, but it's hard not to see the connection in spirit.
Many national holidays have origins that are not widely known or admitted, similar to the unique beginnings of Memorial Day in Charleston.

Actions:

for history enthusiasts, memorial day advocates, civil war scholars,
Research and uncover hidden historical narratives (suggested)
Share stories and histories that challenge traditional narratives (exemplified)
</details>
<details>
<summary>
2021-06-02: Let's talk about what Paxton Smith can teach Republicans.... (<a href="https://youtube.com/watch?v=98DRpdYQihw">watch</a> || <a href="/videos/2021/06/02/Lets_talk_about_what_Paxton_Smith_can_teach_Republicans">transcript &amp; editable summary</a>)

Reported concerns on losing future leaders due to GOP's focus on personality over substance lead to a valedictorian's impactful speech challenging legislative norms in Texas.

</summary>

"Strangers in the legislature, how about you keep your laws off my body?"
"She refused to surrender the platform that she had been given, yeah, that's a leader."
"She may not be a Democrat, but she won't be a Republican."
"Republican Party has done more to push the younger crop left than any leftist ever could."
"Illustrated very clearly the dangers of party over policy or personality over country."

### AI summary (High error rate! Edit errors on video page)

Report suggests Republicans are worried about losing the next generation of leaders due to reliance on personality over substance.
Republican Party has lost the current generation as well.
Republicans leaned into the idea that facts don't matter and that alternative facts are acceptable.
Real leaders and ethical people don't want to manipulate others for their base; they want to lead.
Paxton Smith, a valedictorian in Texas, gave a speech at graduation that challenged legislative laws.
She took a stand by giving a speech different from the one approved by authorities.
Her speech focused on keeping laws off her body, met with applause.
Beau believes if Paxton ran for public office, she wouldn't identify as Republican.
Republican Party's reliance on appealing to the lowest common denominator and rejecting reality has caused future leaders to look elsewhere.
People like Paxton may not be Democrats or Republicans, potentially leading in a different way or joining nonprofit organizations.
Republican Party's actions have pushed the younger generation towards more progressive ideologies due to prioritizing party over policy and personality over country.

Actions:

for future leaders,
Support nonprofit organizations focused on family planning (implied)
</details>
<details>
<summary>
2021-06-02: Let's talk about the home team advantage and society.... (<a href="https://youtube.com/watch?v=Bv5E_vz6DjI">watch</a> || <a href="/videos/2021/06/02/Lets_talk_about_the_home_team_advantage_and_society">transcript &amp; editable summary</a>)

Beau explains home team advantage in sports, linking it to societal privilege, advocating for addressing and mitigating systemic inequalities in both realms.

</summary>

"Let's not call it home team advantage. Let's call it home team privilege."
"Privileges of all kinds exist. They're real. You can't deny that they're out there."
"When the outcomes are as different as they are in reality, it's probably worth addressing."
"It's probably worth trying to mitigate and see if we can get to a game that's a little bit more fair."

### AI summary (High error rate! Edit errors on video page)

Explains why he doesn't usually use sports analogies, citing his lack of sports-watching as the main reason.
Introduces the concept of home team advantage in sports, where statistically the home team wins more frequently.
Provides statistics showing that in the NFL, the home team wins 57% of the time.
Explores possible reasons for home team advantage, such as crowd excitement, environmental factors, referee bias, and travel distance for the away team.
Considers how these factors could also apply beyond sports to societal privilege and systemic advantages.
Suggests renaming home team advantage to home team privilege to draw parallels with societal inequalities.
Advocates for addressing and mitigating these privileges to create a fairer playing field in sports and society.
Draws a comparison between addressing bias in sports officiating and bias in societal systems.
Criticizes the prioritization of mitigating bias in sports over bias affecting people's lives.
Raises the point that acknowledging and addressing privileges in society is necessary for promoting fairness and equity.

Actions:

for sports fans, social justice advocates,
Advocate for fair officiating by supporting measures like tracking chips in sports equipment to reduce bias (implied).
</details>
<details>
<summary>
2021-06-01: Let's talk about Republicans and not wanting to work.... (<a href="https://youtube.com/watch?v=yzN3ASVXlj0">watch</a> || <a href="/videos/2021/06/01/Lets_talk_about_Republicans_and_not_wanting_to_work">transcript &amp; editable summary</a>)

Governors cutting unemployment benefits blame lack of work motivation, but a Biloxi business's success by raising wages shows it's exploitation, urging policy over party for economic solutions in changing times.

</summary>

"Imagine that it's not people just being lazy, it's just people being tired of being exploited."
"Maybe it's time to start looking at policy over party."
"We are moving into an era where there are gonna be a lot of changes."
"Let's just blame the poor people, that's probably not gonna work much longer."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Governors claim people don't want to work, cutting unemployment benefits.
Mississippi cut $27 million a month in unemployment benefits, impacting 90,000 people.
Businesses struggling to reach 50% staffing may have other issues besides people not wanting to work.
Matt Roberts of Shaggy's Biloxi Beach found a solution by raising starting wage to $15/hour and offering benefits.
Roberts' approach led to full staffing without insulting potential employees.
People may not be lazy but tired of being exploited.
States facing economic issues should look at policies and politicians responsible.
Emphasis on policy over party is necessary for addressing economic challenges.
Outdated ideas of blaming the poor for economic issues may no longer be politically viable.
The world is moving into an era of change where old ideas won't suffice.

Actions:

for state residents, workers,
Advocate for policy changes that prioritize fair wages and benefits for workers (implied).
Support businesses that implement fair wages and benefits for employees (implied).
</details>
