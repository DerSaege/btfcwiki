---
title: Let's talk about getting started, futility, and serendipity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=M0YtOr5zklI) |
| Published | 2021/06/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Received two messages back to back over the weekend, leading to discussing serendipity, futility, fires, and getting started.
- One message was from someone needing encouragement to solve a problem they faced in their community regarding food insecurity.
- The other message contained a story about a woman named Morgan who tried to put out a brush fire with just a water bottle, inspiring others to join in.
- Morgan's actions, though seemingly futile at first, led to others stepping in with more resources until the fire was extinguished.
- Emphasizes the importance of getting started, even if the resources at hand may not seem enough to solve the problem.
- Starting a community project, even with limited resources, can inspire others to join in and contribute.
- It's about showing willingness to try and make a difference, even if the impact may seem small initially.
- The story of Morgan illustrates that one person's actions can spark a chain reaction of support and involvement from others.
- Encourages taking the first step, as it often leads to more people coming together to address community issues.
- In community endeavors, the act of starting and showing commitment is more critical than having all the resources from the beginning.

### Quotes

- "You don't have to win every day. You just have to fight every day."
- "All you have to do is get started."
- "Just getting the idea out there."
- "You can't solve it, you can't put a dent in it, but you're willing to try."
- "Other people will, too."

### Oneliner

Beau talks about serendipity, futility, fires, and getting started, sharing a story of how small actions can ignite collective efforts to tackle community issues.

### Audience

Community members

### On-the-ground actions from transcript

- Start a community project to address local issues, even with limited resources (exemplified).
- Show willingness to try and make a difference in your community by taking the first step (exemplified).
- Inspire others to join in and contribute by initiating efforts to tackle community problems (exemplified).

### Whats missing in summary

The full transcript provides additional context and emotional depth to the importance of initiating community efforts and the power of collective action.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about serendipity,
and futility, and fires, and getting started.
Because over the weekend, I had two messages back to back.
I love it when stuff like this happens,
when stuff just comes together.
The first message was somebody with an issue,
somebody who basically just needed
a little bit of encouragement that's
in a situation, they want to solve a problem,
but they don't think they can do it.
And the immediately next message was one that was like, hey,
here's this story.
And it is totally on brand for you.
You've got to be able to use this somehow.
And sure enough, that story is the answer to the question.
The question was basically, hey, me and my body,
we have this issue in our community.
Food insecurity is what it boils down to.
And we have this idea.
We want to get started, but we don't have the resources.
We don't have the resources.
They want to put in those refrigerators,
if you haven't seen them.
They've been around forever.
I saw one like 20 years ago at the Civic Media
Center in Gainesville that this idea has
been around a long time.
And basically, you put refrigerators in places,
and people can add and take food as they need it.
It helps out.
They both work jobs that are working class.
One works retail.
One's fast food.
They don't have the resources for the refrigerators.
They think they can get the food donations.
They think they can set that up, but they don't have money
for those fridges.
So it feels futile.
It feels like it's pointless to try,
because they can't put a dent in the actual problem.
The story that was sent to me is a story
of a woman named Morgan.
She's stuck in traffic.
It's 100-something degrees outside.
She looks out her window, and there's a brush fire.
She gets out of her car with a water bottle
and pours the water bottle on this brush fire.
Didn't do anything.
Didn't put a dent in it, but other people saw her do it.
And they started handing water bottles out of their car,
other liquid, to pour on it.
Eventually, somebody who had a whole case of water bottles
stopped and helped out.
And then somebody with the real resources stepped in.
Somebody got a fire extinguisher,
and they put out that fire.
Now, left unattended, we don't know what would happen,
but it probably would have spread.
Probably would have got worse had they not stepped up.
When she got out of the car, when she got started,
the resources she had on hand were not
enough to solve that problem.
But all she had to do was get started,
and other people would join in.
The same is true.
And this is true pretty much any endeavor that is
going to help your community.
All you have to do is get started.
Doesn't matter if there's only two of you.
Doesn't matter if you don't have a lot of money.
Just getting the idea out there.
Showing that even though you can't solve it,
you can't put a dent in it that you're willing to try.
Other people will, too.
You don't have to win every day.
You just have to fight every day.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}