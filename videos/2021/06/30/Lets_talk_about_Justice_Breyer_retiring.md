---
title: Let's talk about Justice Breyer retiring....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=rc365dbc6sg) |
| Published | 2021/06/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People are debating whether Supreme Court Justice Breyer should resign for a replacement.
- In an ideal world, the Supreme Court should be non-partisan, but that isn't the reality.
- Delay in appointing a replacement may lead to a hyper-partisan appointment by Republicans.
- Beau believes Justice Breyer, at 82, should resign and enjoy retirement.
- He doesn't support President Biden or Congress pressuring him to resign.
- Beau thinks Justice Breyer will likely resign at the end of his term without external pressure.
- He trusts that Justice Breyer understands the need for a successor to preserve ideals.

### Quotes

- "In an ideal world, the Supreme Court is non-partisan. It's not supposed to be a partisan entity."
- "He should be out fishing or something."
- "I don't think anybody needs to put any pressure on him to resign."
- "I think that he's going to do it on his own without influence."
- "Come on Justice Breyer, take up fishing."

### Oneliner

People debate whether Justice Breyer should resign; Beau supports his retirement without external pressure, trusting he'll do it on his own.

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Advocate for a non-partisan Supreme Court (implied)
- Respect Justice Breyer's decision and retirement plans (exemplified)

### Whats missing in summary

Beau's nuanced perspective on the Supreme Court dynamics and the potential impact of Justice Breyer's resignation. 

### Tags

#JusticeBreyer #SupremeCourt #Retirement #Biden #Partisanship


## Transcript
Well howdy there, internet people. It's Beau again. So today we're going to talk about
whether or not Supreme Court Justice Breyer should resign. Beau, a lot of people are saying
that Supreme Court Justice Breyer should resign. Do you believe that President Biden and others
in government should pressure him to resign so he can be replaced? Do you think he should
resign so he can be replaced? Okay, let's start with that last bit first. Do I think
he should resign? Yeah, yeah, I do. I would point out that in an ideal world, the Supreme
Court is non-partisan. It's not supposed to be a partisan entity. We don't live in an
ideal world. If we did live in an ideal world, the Supreme Court wouldn't have many cases
because people would do the ideal. The reality is that it is incredibly partisan and if there
is a delay in appointing a replacement for Justice Breyer, it may be made by Republicans
who will appoint another hyper-partisan person or it may be obstructed as has happened in
the past and it will continue to upset the balance that isn't even there anymore. So
yeah, I think he should resign. I do. I think that's the right move. Aside from that, he's
82 years old, I think. I don't think anybody should be working at that point. He should
be out fishing or something. Do I think Biden or others in government should pressure him
to resign? No. No. By others in government, I'm assuming you mean Congress. These entities
are supposed to be separate. There shouldn't be a lot of influence from one to the other.
I don't think that it's wise to apply that pressure. That is an ideal that we can uphold.
So I don't believe that President Biden should pressure him to resign and I don't think he
will. I think people in Congress will because it's something that they're, it's a political
issue they can talk about. So I think that they'll probably put indirect pressure on
him to resign. All of this being said, I don't think anybody needs to put any pressure on
him to resign. I think he's going to. I think he will at the end of this term. I think that's
pretty, I think that's pretty likely. If you, he tends to be somebody who looks down the
road and he understands the situation the court's in. He understands the situation the
country's in. He understands that if he wants to preserve the ideals that are enshrined
in the documents that he's been interpreting, there's going to need to be somebody else
to carry on for him and that appointment is going to be most likely secured if it happens
soon. I don't think that anybody needs to pressure him. I think that he's going to do
it on his own without influence. At least I hope so. We'll see. Because I am not part
of Congress or the executive branch. I can totally say, come on Justice Breyer, take
up fishing. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}