---
title: Let's talk about Tucker's NSA claim with Deep Goat....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qx_5Z8UN1fA) |
| Published | 2021/06/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Deep Goat says:

- Tucker Carlson claims NSA is targeting him for surveillance to create a scandal and remove him from the air.
- Former NSA ops officer believes there is a zero percent chance of spying on a major American media figure unless in contact with a hostile intelligence service.
- Deep Goat questions the reliability of statements from both Tucker Carlson and the intelligence community.
- Tucker Carlson presented no evidence to support his claim of NSA surveillance.
- Deep Goat explains the lack of operational security in Tucker Carlson's communication methods.
- The credibility of Tucker Carlson's claims is questioned due to the absence of evidence and the nature of the allegations.
- Deep Goat analyzes the implausibility of the NSA operation as Tucker Carlson described it.
- Intelligence agencies influence media coverage, but not typically by targeting individual journalists as Tucker Carlson suggested.
- Possible scenarios include fabrication of the story, NSA intercepting messages due to contact with a hostile intelligence service, or Tucker Carlson preemptively leaking compromising information.
- Deep Goat concludes that while intelligence agencies can break the law, the likelihood of Tucker Carlson's specific claims being true is low.

### Quotes

- "This is compounded. This problem is compounded if somebody is a known loudmouth and talks a whole lot about what they're doing."
- "Intelligence agencies throughout history, throughout the world, yeah, they try to influence media coverage all the time."
- "You also have to kind of weigh the probabilities here. Is Tucker Carlson that important?"
- "You cannot FOIA the NSA to ask them if you're a target of a black op."
- "Y'all have a good day."

### Oneliner

Tucker Carlson's claim of NSA surveillance lacks evidence and credibility, casting doubt on the likelihood of the alleged operation.

### Audience

Journalists, Media Consumers

### On-the-ground actions from transcript

- Verify information before amplifying it (suggested)
- Question and critically analyze claims made by public figures (suggested)
- Ensure operational security in communication practices (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the implausibility of Tucker Carlson's claims regarding NSA surveillance, urging critical thinking and scrutiny of information presented by public figures.

### Tags

#NSA #Surveillance #TuckerCarlson #IntelligenceAgencies #Journalism


## Transcript
Well howdy there internet people, it's Deep Goat again.
It's been a while, how y'all doing?
I guess
after Tucker Carlson's latest claim, a lot of y'all reached out to Bo to ask for me to
make an appearance.
So I took time out of my busy schedule
to
find out what y'all were talking about
and then I'll give you a little bit of commentary.
So I went and found this clip
of Tucker Carlson
because I don't watch his show, you know, I don't know what he says.
Doesn't really seem that important.
But, okay, so
Tucker Carlson is claiming that the NSA is directly targeting him
for surveillance, intercepting his text messages and emails
in an attempt to gather information about him that could be leaked
to create a scandal to get him taken off the air.
That is
the summation of his allegation.
Okay.
Now, it's worth noting that the NSA itself probably isn't going to comment on this.
They don't comment on much.
We do have a statement from a former NSA ops officer
and he said that there was
zero percent chance
that the NSA would be spying on a major American media figure
unless, of course, they were in contact with a hostile intelligence service.
I don't know that it's a zero percent chance.
That person does appear to have much more faith in the safeguards
of the NSA than I do.
It is true
that they shouldn't do it legally
even though there are ways around that.
But it is highly unlikely
that this is occurring.
I will give them that.
However, at the end of the day,
that statement came from somebody in the intelligence community
defending an intelligence agency,
which means I trust that statement about as much as I trust
the statement from a ten-dollar snitch in Khartoum.
Now, on the other side of the aisle,
you have respected, intrepid journalist Tucker Carlson
whose statements I trust
about as much as I trust a toilet seat in a bar on the back streets of the
Longepo.
Credibility here is lacking in general
because you're talking about something that is full of lies. You're talking about the
intelligence world.
So let's look at the evidence that Tucker Carlson presented.
I will go over all of it now.
I'm done.
He presented none.
No evidence whatsoever to back up his claim.
A pretty wild claim.
A claim that
should require a whole lot of evidence, but he doesn't have any.
What he said was that a whistleblower, his term,
contacted him
and said that the NSA was spying on him for this reason
and that normally he would be skeptical, but this person was able to repeat back
information
that was contained in text messages and emails.
And Tucker is suggesting that the only place
it could have been obtained,
that information could have been obtained,
was from those text messages and emails.
For that to be true,
Tucker Carlson has to have a high operational security posture and he does not.
It doesn't matter if you use encrypted text messaging. It does not matter if you
use encrypted email.
If your devices are not constantly secure,
if somebody can get access to them,
the messages aren't secure. The information isn't secure.
This is
compounded.
This
problem is compounded if
somebody is a known loudmouth
and talks a whole lot about what they're doing.
We also know that
Tucker's producers
were aware of the story that he was working on, the information that was
repeated back to him.
So for this to be true, for the only way that this information was obtained was
through
intercept of these messages,
all of Tucker's producers
also have to have that high operational security posture.
This is really unlikely.
His evidence is not evidence.
Now we have to go to the other side. And by other side, I don't mean the NSA yet.
We're not talking about them yet. We're talking about the other side of that communication.
Because
text messages, emails, transmissions of all sort
have
what we like to call in the biz,
a recipient.
That recipient also
has to have a high operational security posture. They have to keep their mouth
shut.
In this case, we know they don't.
I don't know who the person is,
but I know
that they were a source for Tucker Carlson.
They were a source. They were somebody who provides information.
They were somebody who talks.
Generally speaking, if somebody's willing to provide information to you, they will
provide it
to others.
They will talk about it.
So
his evidence
that there even was an intercept
is lacking.
Now, this isn't to suggest
that the NSA would not break the law. They absolutely will.
Absolutely.
It's just suggesting
that we don't have any evidence that this has occurred. None.
We have no evidence that this has happened.
Intelligence agencies
will in fact break the law. They do it all the time. That's quite literally their job
if you ever really stop to think about it. We train them
to go overseas and break the law.
They break the laws in that country.
When they come back, yes, they have a general disregard for the law
because they were trained to disregard it.
So, safeguards often fail.
That's true.
That is true.
But let's talk about the operation as Tucker Carlson has laid it out.
This NSA operation.
First, let's point out that this is illegal.
Like super illegal. Like really illegal.
This would be
sensitive compartmentalized information. A limited number of people would know about
this operation
because it's illegal.
Collecting information on an American national with the intent
of leaking it
to influence American media coverage,
that's a big deal.
Very, very few people would know about this.
It would be done in
the utmost secrecy.
Which means his whistleblower,
they're one of those people.
And,
given the fact that he has such a high operational security posture and knows
how to keep his mouth shut,
Tucker Carlson took that information and put it on the air.
Which means,
well, that's...
that source probably won't be contacting him again.
Because if you were part of an SCI operation
that is geared against a person and you disclose that operation to that person,
well that's a really good way to end up in a cell or end up in a row 13, plot 12.
This would be
very secretive.
The odds that Tucker Carlson could find out about it
are pretty limited.
And we have to
acknowledge that
this isn't how it's done.
Intelligence agencies
throughout history, throughout the world, yeah, they try to influence media coverage
all the time.
That's a thing.
That is a thing.
However,
this isn't how it's done.
Tucker's allegation is not only has the NSA lost all security culture,
they completely forgot everything there is to know about Tradecraft.
Because his allegation is that they're trying to get him off the air.
If they're trying to get him off the air, you don't gather information on him.
That's not how it's done.
You gather information on the executives, those in charge of the lineup,
and you get compliance on them.
And they just fire him.
And doing it that way has a lower signature because you end up
taking control of the entire network because the compromising information
that you obtained on them,
it's compromising today,
and it's compromising two years from now.
So you conduct one operation and you gain control of the network's lineup completely
rather than
attempting to create a scandal about a man
who thrives on scandal.
The NSA's operation in this case would probably just boost his ratings.
Generally,
when intelligence agencies go after journalists,
they are attempting
to recruit them.
Now they could do it through a whole bunch of means.
It could be blackmail.
But they're trying to bring them on board.
They're not trying to fire them.
They would go after somebody else to get that done.
Now if you were talking about Tucker Carlson in particular,
you already know
that he has the desire
to be a spy.
Why? Because he applied for the CIA at one time.
So he would probably be pretty easy to recruit.
So the way he's suggesting this operation is being conducted,
that's probably not occurring.
The motive,
the idea of influencing media coverage,
yeah, that's a thing.
All intelligence agencies do that.
Generally speaking,
it's
not
in
the home country.
It does happen.
But not in the way that he's suggesting.
Not really.
That's not really a thing.
Not saying it can't happen
because intelligence officers are trying to break the law.
But that's not normally how it happens.
And we have no evidence to suggest that it is happening this way.
What he is suggesting is incredibly unlikely.
So what is likely?
The first is that it didn't happen.
Like this whole thing's fabricated.
It's all made up.
It's somebody either playing Tucker Carlson,
playing his producers,
whatever.
It's something along those lines.
And it's just not real.
Now another option
is that the NSA did in fact intercept his messages
because he's in contact with a hostile intelligence service
as we have covered
in this conversation.
Intelligence services the world over like to influence media coverage.
So if Tucker was working with a source
and that source was actually being employed
by a hostile intelligence service,
Tucker Carlson's messages would get swept up
from the other end, outside the United States.
And that would be a legitimate function of the NSA.
That's actually quite literally their job.
So that's an option.
And in that case,
because it isn't this highly legal
black op
geared towards influencing domestic media coverage,
it wouldn't be as secret.
More people would have access to that information,
which makes the likelihood of a whistleblower
a little bit higher.
Then we have the last option, another option.
And that's that Tucker himself
is aware that there's going to be leaks
that are compromising to him.
So he's putting this information out there
in an attempt to deflect it.
That's it. Those are your options.
Now, is it possible
that the NSA is directly targeting Tucker Carlson
to smear him in this manner
to get him removed from the air?
Is it possible? Yeah.
Nothing is really a 0% chance.
It's possible, but it isn't likely.
And we have no evidence
to suggest this highly unlikely thing is happening.
Tucker said that he was going to file
a Freedom of Information Act request
to get the information.
That statement alone shows that Tucker Carlson
knows absolutely nothing about how the intelligence world works.
An FOIA, you cannot FOIA the NSA
to ask them if you're a target of a black op.
That's not a thing.
Even if it was happening, they would tell you no.
The idea that he put that out there
as something that would be effective is laughable.
It's a joke.
It shows that perhaps Tucker Carlson
shouldn't be taken seriously by reasonable people.
This whole thing, it's really unlikely.
It's not impossible because intelligence agencies
do go off the rails.
They break the law.
But what he's suggesting isn't just surveillance.
It's not just the sweeping up of metadata.
It's not just capturing his text messages.
It's a concerted effort to gather this information
to influence domestic media coverage.
That's a huge allegation,
and that requires some pretty heavy evidence.
Not just Bob told me, and he knew what was in my emails.
At the end here, what you have to acknowledge
is intelligence agencies break the law.
That's their job.
But you also have to kind of weigh the probabilities here.
Is Tucker Carlson that important?
More importantly, why would it be the NSA?
Now, I could see Tucker Carlson
being investigated by various agencies.
The NSA is one of the few that just doesn't really add up,
to be honest, unless, of course,
he's in contact with a hostile intelligence service,
an agent that is trying to influence his coverage.
He may not even know that that's occurring.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}