---
title: Let's talk about Virginia, teachers, and protected speech....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EkHVyAvBkkU) |
| Published | 2021/06/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A teacher in Virginia was reinstated after being sent home by the school board for refusing to address children by their preferred pronouns.
- The teacher cited religious beliefs as the reason for not affirming that a biological boy can be a girl and vice versa.
- The school board tried to be proactive and sent the teacher home before any policy violation occurred.
- Beau argues that as a government employee, one does not have absolute freedom of speech or religion.
- He explains the separation of church and state in the U.S., stating that using religion to circumvent policy is not acceptable.
- Beau refutes the claim that the U.S. is a Christian nation, citing historical evidence like the Treaty of Tripoli.
- He stresses the importance of moving away from authority figures making others feel lesser and disrupting schools.
- Beau predicts that the teacher may return to school, refuse to abide by policy, and eventually be terminated for it.
- He notes that while the teacher's initial statement may be protected speech, violating policy is a different matter.
- Beau concludes by suggesting that the situation will be monitored and ends with well wishes.

### Quotes

- "It's lying to a child. It's abuse to a child. It's sinning against our God. Wow."
- "The United States has a separation of church and state."
- "We are moving away from the idea that people in positions of authority can use that authority to make others feel lesser."
- "There's no reason to subject a child to that. There's no reason to disrupt the school."
- "Once he does violate policy, well, that's a whole different story."

### Oneliner

A teacher in Virginia reinstated after refusing to address children by preferred pronouns, sparking debate on freedom of speech, religion, and the separation of church and state.

### Audience

Educators, policymakers, activists

### On-the-ground actions from transcript

- Monitor and advocate for inclusive policies in schools (implied)
- Support efforts to create safe and respectful environments for all students (implied)

### Whats missing in summary

Deeper insight into the potential implications of prioritizing personal beliefs over school policies in educational settings.

### Tags

#Virginia #SeparationOfChurchAndState #InclusiveEducation #FreedomOfSpeech #ReligiousFreedom


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about a
teacher being reinstated in Virginia. The teacher was sent home by the school board
and a judge has ordered that teacher reinstated. The teacher gave a rather impassioned speech
which they said they weren't going to abide by policy, by the school board's policy.
They weren't going to address children by the pronoun they wish to be addressed by.
In this speech, they said that he wouldn't affirm that a biological boy can be a girl
and vice versa because it's against my religion.
It's lying to a child. It's abuse to a child. It's sinning against our God. Wow.
I mean that's something else right there. As an employee of the government,
you don't actually really have a freedom of speech. You don't have a freedom of religion.
We have a separation of church and state. Now the school board did mess up here.
They tried to be proactive. They tried to stop the problem before it started.
They said he wasn't going to do it so they said, okay, don't come back.
He hadn't violated policy yet. That's really what it boils down to.
The judge's message here is don't be proactive.
Wait until somebody actually impacts a child before doing something.
The United States has a separation of church and state.
If you are a government agent, government employee, you do not get to use your religion
as a reason to circumvent policy. It's not how it works.
Anytime I say something like that, somebody says, you know, the United States is a Christian nation.
No, it's not. No, it is not. It was not founded as a Christian nation.
The Constitution was ratified in 1789. In 1796, the Treaty of Tripoli was signed.
In the Constitution, it says that treaties are part of the supreme law of the land.
In the Treaty of Tripoli, it says the United States of America is not in any sense
founded on the Christian religion.
That's just a few years difference. The people who were around for the Constitution
being ratified, they were around for that treaty.
The idea that the U.S. was founded as a Christian nation and therefore those should be our laws,
that's just not true. It's made up.
We are moving away from the idea that people in positions of authority can use that authority
to make others feel lesser. We've been moving away from this concept in this country for a while.
It's been really slow going, but it is speeding up. You're going to have to catch up.
You're going to have to catch up. There is no reason
to subject a child to that. There's no reason to disrupt the school.
Now, my guess on what's going to happen here is that the teacher will go back to school.
The teacher will do what he said. He's not going to abide by this policy,
and then he'll be terminated for that. That's probably how it's going to go.
I could be wrong, but that's how I see this going. The judge's ruling makes sense in the sense that
he hadn't yet violated policy. He just said he was going to. I could see that as being
protected speech. Once he does violate policy, well, that's a whole different story.
So we'll keep an eye on this one. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}