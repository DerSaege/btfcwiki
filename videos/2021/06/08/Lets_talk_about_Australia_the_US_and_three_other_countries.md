---
title: Let's talk about Australia, the US, and three other countries....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tICfOS_Tflk) |
| Published | 2021/06/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Receives a message about a unique reaction to news about encryption technologies.
- Shares information about Phantom Secure being taken down by authorities.
- Mentions the emergence of a new encrypted app called Anom, which authorities used to monitor and run investigations.
- Explains the concept of the Five Eyes alliance involving the US, UK, Canada, Australia, and New Zealand for sharing signals intelligence.
- Raises concerns about intelligence agencies potentially circumventing laws by using this alliance to spy on their own citizens.
- Acknowledges the existence of civil liberties controversies surrounding the Five Eyes alliance.
- Mentions the predecessor program Echelon and its lack of self-regulation by intelligence agencies.

### Quotes

- "Just because something sounds like a wild theory doesn't mean that it is."
- "Five eyes definitely exists."
- "There is definitely a civil liberties controversy."

### Oneliner

Beau shares insights on encryption technologies, the Five Eyes alliance, and civil liberties controversies, debunking wild theories in the process.

### Audience

Internet users

### On-the-ground actions from transcript

- Research and stay informed about encryption technologies and surveillance practices (implied)
- Advocate for privacy rights and civil liberties in the digital age (implied)

### Whats missing in summary

Detailed examples and further elaboration on the controversies surrounding intelligence agencies' potential circumvention of laws through the Five Eyes alliance. 

### Tags

#Encryption #FiveEyes #CivilLiberties #Surveillance #DigitalPrivacy


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we've got an interesting one, a fun one even.
I got a message.
And basically, one brother told the second brother
a piece of news.
And the second brother had a very unique reaction.
And that has prompted a request.
For any of this to make any sense,
I have to tell you the news that was shared.
A few years ago, if you were involved in anything
you didn't want the authorities to know about
and you were pretty well connected,
you might have been using an encryption setup called
Phantom Secure.
Now, eventually, Phantom Secure got
taken down by the authorities.
As is typically the case, when something gets taken down,
something new shows up in its place.
In this case, it was an app called a knob.
It, in theory, provided encryption for your phone.
In reality, the authorities put the app out there
and they had a back door into it.
So they could monitor everything and they ran investigations
through it.
This all became public knowledge because they stopped
some pretty horrible stuff from happening.
And along the way, they took some contraband and stuff
off the market.
But what brought it to public attention
was them using it to stop some pretty bad stuff.
So the first brother tells the second brother
this piece of information.
And apparently, the second brother just
goes off on a rant about civil liberties and echelons
and spies and something called the Five Eyes.
So the message I got was explaining all of this
and requesting a debunk on the Five Eyes theory about that.
That's real.
That's real.
That's a real thing.
In the United States, it's not very well known.
In most of the other countries that are participating,
it's pretty common knowledge.
The Five Eyes is picture like NATO for spies,
for signals intelligence, which is telephones, text message,
fax, email, radio signals, stuff like that.
Gets intercepted and it gets shared between five countries.
And those are the Five Eyes.
And by eyes, we're talking about the eyes you see with,
not the letter.
The Five Eyes are the United States, the United Kingdom,
Canada, Australia, and New Zealand.
They all share information.
In and of itself, there's nothing wrong with this.
When you're talking about countries that are this
closely aligned, their spy agencies having an alliance,
that's not unusual.
The controversy arises with the idea
that each intelligence agency can use this alliance
to circumvent the laws in their home country.
As hard as this may be to believe, in most instances,
it's against the law for US intelligence agencies
to spy on Americans or to work domestically.
There are exceptions to this, of course.
They can't target you directly.
But they could ask the British to do it or the Australians.
And then those intelligence agencies spy on you
and share the information.
That's the concern.
Is that happening?
Probably.
Probably.
Echelon, by the way, is the predecessor program
that ran, I think it started in the 70s.
That got way out of hand.
And given that that's the predecessor to this,
there is room to be skeptical about these intelligence
agencies having the ability to self-regulate.
So there's not really much to debunk.
You didn't tell me what the whole theory was.
But there is definitely a civil liberties controversy.
Five eyes definitely exists.
It's a verifiable echelon.
It was really a thing.
So I guess the lesson today is just because something
sounds like a wild theory doesn't mean that it is.
Anyway, it's just a thought. I hope you have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}