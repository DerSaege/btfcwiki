---
title: Let's talk about Portland cops resigning and what to do....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dMinD1hP96w) |
| Published | 2021/06/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A large number of cops in Portland resigned from a specialized team dealing with civil disturbance after one member was indicted.
- The resignations were not just due to the indictment but also because the mayor, judge, and prosecutor criticized the team for using excessive force, including tear gas.
- Beau suggests that if the criticism from multiple officials doesn't prompt policy changes, those cops shouldn't be on the team or even be cops at all.
- The refusal to accept policy changes indicates a sense of being above accountability and the law within the team.
- The decision to quit over charges being brought against a cop in the Terry Jacobs incident shows a lack of willingness to be held accountable.
- Beau argues that if officials are pointing out excessive force, it's time for policy changes that should be accepted as part of the community.
- Resigning when faced with the need for policy changes implies a culture within the team of expecting to act without oversight or accountability.

### Quotes

- "If a judge, the mayor, and the prosecutors are all saying you're using too much force, it's time for policy changes."
- "That to me suggests that the bad apple has already spoiled the bunch."
- "If that is just unimaginable, unthinkable, something that you shouldn't agree to, you probably shouldn't be on this team and you probably shouldn't be a cop."

### Oneliner

A large number of cops in Portland resigned from a specialized team over criticism of excessive force, indicating a culture of avoiding accountability.

### Audience

Community members, Police Department

### On-the-ground actions from transcript

- Advocate for policy changes within law enforcement (implied)
- Support oversight and accountability measures for police (implied)
- Demand transparency in police actions (implied)

### Whats missing in summary

Further insights on the importance of community involvement in holding law enforcement accountable.

### Tags

#PoliceAccountability #ExcessiveForce #CommunityInvolvement #Portland #PolicyChanges


## Transcript
Well howdy there internet people, it's Beau again.
So we're going to try to answer some questions that are circulating in the media today.
If you don't know, a large number of cops in Portland have resigned from a specialized
team they send out to deal with civil disturbance.
They resigned shortly after one member of their team was indicted.
And the question is, well what are we going to do?
What are we going to do?
The response from most people has been to say, well go look at the footage.
Go look at the footage of Terry Jacobs and see if you think that that was warranted.
And if cops are resigning over that, that would indicate that, well maybe they shouldn't
be on the team.
And that's a good answer.
That's a good answer.
I think it's incomplete though, because there's some other things at play.
The reality is, when they're talking about it, when the cops are talking about why they
quit, it's not just the indictment of one of their teammates over the Terry Jacobs incident.
It's also that the mayor said they were using too much force, and a judge.
The judge said they were using tear gas too much.
And these were the reasons.
This all plays together.
So to recap, the elected official, the mayor, said they're using too much force.
A judge said they're using too much force.
And the prosecutor said they're using too much force.
And their response is, well we're not going to change, so we're going to quit.
So what do we do?
That's the question.
Give them a going away fruit basket.
If a mayor, a judge, and the prosecutor are saying you're using too much force, if that's
not enough to get you to accept policy changes, you shouldn't be on the team.
I'm going to go a step further and say you shouldn't be a cop.
That certainly sounds like members of that team feel like they're above accountability.
That they are above the law.
That they can use whatever force they would like.
And that they shouldn't have to answer to anyone, to include the mayor, the judges,
and the prosecutors.
That doesn't sit well with me.
This isn't a case of one officer doing something in the Terry Jacobs incident.
Just the indictment.
Now keep in mind, this person hasn't been convicted.
The cop hasn't been convicted.
Just having charges brought was enough to say no, no, no.
We're not going to play anymore.
We're going to quit.
We're going to take our ball, batons, and go home.
That to me suggests that the bad apple has already spoiled the bunch.
That the culture in that team is one of expecting to be held unaccountable.
To have no oversight whatsoever.
To have their actions retroactively justified, no matter what they do.
That's the way it seems to me.
If a judge, the mayor, and the prosecutors are all saying you're using too much force,
it's time for policy changes.
And you should want to accept that as part of the community.
But if that is just unimaginable, unthinkable, something that you shouldn't agree, or you
wouldn't agree to, you probably shouldn't be on this team and you probably shouldn't
be a cop.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}