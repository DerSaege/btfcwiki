---
title: Let's talk about whether the US is fundamentally racist....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZtMtEsrXuMg) |
| Published | 2021/06/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the debate on teaching American history through the lens of racism, particularly in relation to Joe Biden and Critical Race Theory (CRT).
- Clarifying that CRT is an academic movement, not specific to Biden, and discussing the fundamentally racist nature of the 1994 crime bill.
- Exploring the concept of the United States being fundamentally racist by examining historical instances of institutionalized racism from the country's founding to the present.
- Describing the long history of structural and institutionalized racism in the U.S., including legislation like the Violent Crime Control and Law Enforcement Act of 1994.
- Arguing that the United States' history is intertwined with racism, making it fundamentally racist according to historical evidence.
- Emphasizing the importance of acknowledging past injustices and using history to prevent repeating discriminatory patterns.
- Drawing parallels between a person's lifespan and the United States' history to illustrate the persistent nature of racism in the country.
- Asserting that denying the history of institutionalized racism in the U.S. hinders progress in addressing present-day racial issues.

### Quotes

- "United States was founded and built on institutionalized racism."
- "You can ban teaching this all you want, but it's reality."
- "History is littered with racism, with structural, institutionalized racism."
- "You learn history so you don't repeat the mistakes."
- "Until people are comfortable with acknowledging the past, we have to assume that there's still a lot of racism."

### Oneliner

Teaching American history necessitates acknowledging its institutionalized racism, from slavery to modern legislation, shaping a fundamentally racist nation.

### Audience

Educators, Historians, Activists

### On-the-ground actions from transcript

- Teach history truthfully and inclusively to acknowledge past injustices and prevent their repetition (implied).
- Advocate for curricula that address the historical reality of institutionalized racism in the United States (implied).

### Whats missing in summary

The full transcript provides a detailed exploration of the historical foundation of institutionalized racism in the United States, urging for an honest reflection on the nation's past to address present-day racial inequalities effectively.

### Tags

#AmericanHistory #InstitutionalRacism #CriticalRaceTheory #JoeBiden #HistoricalTruth


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about what the United States
fundamentally is or is not.
Teaching history, a theory, and we're going to do this
because I got a message, got a question to talk about it,
request to talk about it.
Beau, can you talk about the absurdity of liberals
arguing that we should teach American history that says
America is fundamentally racist through Joe Biden's CRT. Biden's support of the
theory means nothing because he passed the fundamentally racist 1994 crime
bill. Can we please move past the idea that everything is racist and just teach
real objective American history? Okay, so just quick little fact checks here. CRT
is not Biden's thing. It's an academic movement. It doesn't have
anything to do with Joe Biden. That's just not a thing. The 1994 crime bill,
that's the Violent Crime Control and Law Enforcement Act, the sender claims it's
fundamentally racist. I happen to agree. I happen to agree with that. And then we
have the actual question, what they want discussed. Is the United States
fundamentally racist. Well, first we have to determine what that means. Fundamentally racist.
You're just talking about people's attitudes. The people of the United States, are they
fundamentally racist? Or generally speaking, are most people racist? Yeah, yeah, yeah. And that
goes through the overwhelming majority of American history. Absolutely. So you could mean that, or you
you could mean is there some structural institutionalized legislation that is
racist? Well let's just run through a timeline and we'll see if it's there. Okay
pick your founding date doesn't matter 1776 1789 does not matter okay from day
one there was institutional legal racism it was enshrined it was a part of the
American structure in slavery that lasted until the 1860s and then it went
through a fundamental change. So you have that legal structure in the
beginning. When it ended almost immediately was replaced with a less
severe structural institutionalized legal form of racism and that lasted
about a hundred years until the 1960s late 1960s. That's just black Americans
right there. If we want to stop at this point, you don't even have to get into
the Exclusion Act or the treatment of Native Americans. The removal of Native
Americans is how it is framed in most American history books. The removal of
Native Americans. That is like a really super polite way to describe ethnic
cleansing, which is what it was. And that started from day one and it doesn't
matter how you want to define it. At the very least that went on until 1918
because up until that point the US military was at arms fighting against
natives. That's a pretty big chunk of American history there, but then we have
something else, thanks to this email. According to this email, they claim, and I
believe, that the 1994 bill, that act, was fundamentally racist. I agree with that.
I think it was. I would note that while some of the effects of that
have been lessened, that law is still on the books. A fundamentally racist piece
of legislation is still there. So from day one from the founding until today
you have racist legislation. I would suggest that that would make a country
fundamentally racist. If other nations that are held up as super racist nations
studied the United States's laws to figure out how to do it, that might be a
good sign too. That happened. The US has a long history of racism. That is objective
reality. That's history. That's the truth. You can argue that it shouldn't be taught
that way all you want, but at that point you're not arguing that you want history taught a
certain way. You're arguing that you want mythology taught a certain way. The United
States its history is littered with racism, with structural, institutionalized racism.
Racism that would suggest the country is fundamentally racist.
It's just there.
This isn't actually a controversial theory as far as the United States being a fundamentally
racist nation.
Now CRT is basically just saying, hey, we need to acknowledge this and acknowledge that
there are injustices that are occurring and using a historical lens to do that.
That's kind of the purpose of history.
You learn history so you don't repeat the mistakes.
If the United States was a person who was a hundred years old and you discount this
email and the 1994 stuff and you just run from the founding to the end of segregation,
that would mean that the person was 80 years old before they stopped being as racist, overtly
racist and just relied on the more subtle forms.
If you met somebody who was 100 years old and throughout 80 years of their life they
were overtly racist, you would probably suggest they were fundamentally racist.
This isn't controversial.
You can ban teaching this all you want, but it's reality.
Those are the facts.
United States was founded and built on institutionalized racism. There's no real
arguing this. It doesn't matter which pieces you want to count or discount.
It's there. It's there underlying the United States' history. Does it get
lessened over time? Yeah. Is it gone? No. If it was gone, we wouldn't be having
having this conversation.
Until people are comfortable with acknowledging the past, we have to assume that there's
still a lot of racism because you're denying something that is objective reality.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}