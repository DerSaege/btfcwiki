---
title: Let's talk about an American hero who doesn't get the credit they should....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=izal0V4Mevc) |
| Published | 2021/06/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introducing an American hero whose name isn't mentioned enough in history despite being a pivotal figure in the civil rights movement.
- Born in 1925, he walked 12 miles to school daily, served in the military during WWII, and fought at the Battle of Normandy.
- In 1954, he applied to the University of Mississippi Law School but was denied due to his race.
- Became the first field secretary of the NAACP in Mississippi in the same year.
- Organized boycotts, marches, and voting drives, putting himself at risk for the cause.
- Survived multiple attempts on his life, always aware of the dangers he faced.
- Killed on June 12, 1963, carrying a t-shirt that read "Jim Crow must go," becoming the first black man admitted to an all-white hospital posthumously.
- His death sparked marches and galvanized the civil rights movement.
- The circumstances of his death took decades to bring his killer to justice, but it pushed the movement forward significantly.
- Despite being recognized more now, his name isn't as prominent as others due to the great man theory overshadowing collective movements.

### Quotes

- "You can kill a man, but you can't kill an idea."
- "It's never just one person. It's always a movement of people."

### Oneliner

Beau talks about an American hero, Medgar Evers, whose pivotal role in the civil rights movement is often overshadowed by the great man theory, stressing the importance of recognizing collective efforts over individual figures.

### Audience

History enthusiasts, civil rights advocates

### On-the-ground actions from transcript

- Research and share more about Medgar Evers' life and contributions (suggested)
- Support civil rights organizations and movements in your community (suggested)

### What's missing in summary

The emotional impact and personal sacrifices made by Medgar Evers for the civil rights movement. It's worth watching the full transcript to truly understand his story and legacy.

### Tags

#CivilRights #MedgarEvers #GreatManTheory #CollectiveMovements #History


## Transcript
Well howdy there internet people, it's Beau again.
So today we are
going to do a little bit more to dispel great man theory.
You know,
this particular case,
songs were written about the person,
movies, books.
It's not an unknown story,
but for some reason when the topic comes up
his name doesn't get mentioned as often as it should.
So today we're going to talk about an American hero.
He was born July 2, 1925,
and he walked twelve miles to school every day, for real.
In 1943,
joined the military, served from 1943 to 1945, during World War II, fought at the
Battle of Normandy.
In 1948,
he enrolled in
Alcorn
Agricultural and Mechanical School. That's Alcorn State University today.
He got married in 1951, had three kids. In 1954 was Brown versus Board of Education.
So he applied
for the University of Mississippi Law School.
He was denied due to his race.
But that same year, he was made first field secretary of the NAACP in
Mississippi.
There he organized boycotts,
marches,
force multiply, made sure that other people could do what he did,
organized
voting drives,
really put himself out there, and he understood the risks of it.
He said that
he's looking to get shot anytime he stepped out of his car,
and in fact did survive
multiple attempts on his life.
He was known for saying, you can kill a man, but you can't kill an idea.
On June 12, 1963,
he pulled into his driveway,
he stepped out of his car
carrying a t-shirt
that said, Jim Crow must go.
And a man with an enfold rifle
put a bullet through his chest.
He
was taken to the hospital,
but they wouldn't admit him
because it was an all-white hospital.
After a little bit of discussion,
they decide to let him in,
and he became the first black man
admitted to an all-white hospital.
He did not survive.
There are a lot of theories
about what happened that day,
because he normally had like a police escort.
FBI was normally around.
There was normally law enforcement anytime he moved,
but not that day.
So much so that it took decades
to
bring his killer to justice.
However, the circumstances of his death
galvanized the movement,
sparked marches,
and it was right after Kennedy had
made a moral argument
in favor of the civil rights movement,
and it really helped push the movement forward.
In 2011,
the United States Naval ship
Medgar Evers
was christened by his wife.
He's buried at Arlington.
This is a man who
should
be
constantly viewed
as a leader in the civil rights movement.
And if you really start to study it,
yeah, his name comes up
a lot.
But that name,
Medgar Evers,
it's not out there like Martin Luther King
because of great man theory,
because it's easier to personify
a
historical movement
through a single person.
When you do that,
you lose a lot of the character,
you lose a lot of the story,
and you lose a lot of history.
It's never just one person.
It's always a movement of people.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}