---
title: Let's talk about veterans getting what they deserve....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fUrrUXBP8kM) |
| Published | 2021/06/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the news about trans vets being able to get confirmation surgery through the VA.
- Expresses excitement about this development, believing it helps integrate marginalized groups into the veteran community.
- Notes the social unacceptability of openly looking down on people for their immutable characteristics in the US.
- Reads comments criticizing this news, citing examples of veterans not getting hearing aids or insulin.
- Calls out the hypocrisy of commenters who claimed previous administrations had fixed VA issues.
- Points out that veterans can indeed get hearing aids and insulin through the VA, although the system isn't perfect.
- Emphasizes that the entitlement to medical care should make this policy a reason to celebrate if one truly cares about veterans.
- Criticizes the tendency to other and view veterans as lesser, perpetuating acceptable discrimination against them.
- Hopes that integrating different demographics into the veteran community will lead to broader societal acceptance.
- Mentions a trans veteran, Zoe, giving advice in the comments on how to access hearing aids through a tinnitus claim.

### Quotes

- "It's socially unacceptable to openly look down on people because of their race, because of their orientation, because of their religion."
- "They're entitled to medical care, right? If you actually cared about veterans, this would be a celebratory moment."
- "Nobody wants to talk bad about a vet in this country."
- "Veterans can get insulin through the VA. You know what veterans couldn't get? Confirmation surgery."
- "I read the comments, and in the comments saw stuff like..."

### Oneliner

Beau addresses the social unacceptability of discrimination based on immutable characteristics when discussing the news about trans vets' access to confirmation surgery through the VA and criticizes the hypocritical backlash in the comment section.

### Audience

Veterans, Advocates, Allies

### On-the-ground actions from transcript

- Contact trans veteran Zoe for advice on accessing hearing aids through a tinnitus claim (exemplified)
- Celebrate and support policies that integrate marginalized groups, like trans vets, into the veteran community (suggested)

### Whats missing in summary

Nuances in Beau's tone and delivery, along with the full emotional impact of his disappointment in the hypocrisy displayed in the comments section.

### Tags

#Veterans #Discrimination #Healthcare #SocialAcceptance #CommunityIntegration


## Transcript
Well howdy there, Internet people. It's Beau again.
So today we are going to talk about some news that broke over the weekend
and the reaction to it and what it demonstrated.
You know, in the United States
it has become socially unacceptable
to openly look down on people because of their race,
because of their orientation, because of their religion,
because of a whole bunch of things that used to be divisive topics.
It's socially unacceptable to do it.
There are still people who do it, no doubt,
and some of these areas have made more progress than others.
But generally speaking, there aren't many immutable characteristics
that it's still okay to look down on people for.
Now news broke that trans vets
were going to be able to get confirmation surgery through the VA.
And I thought that was cool.
I thought it was cool because I'm of the firm belief
that because the United States is so militaristic,
getting an out group, getting a marginalized group
seen as part of the veteran community
is a very effective way of bringing them into the fold, so to speak,
and getting it to where they're less likely
to be looked down upon for these immutable characteristics.
Not saying it's a good thing that the United States is that militaristic,
but saying it is that way.
So I was excited about it,
and I did something everybody always tells you not to do.
I read the comments, and in the comments saw stuff like,
veterans can't get hearing aids, but they can get this.
Veterans can't get insulin, but they can get this.
And you know the thing is, those comments,
not just did they demonstrate pretty clearly
that it's still socially acceptable to be pretty bigoted in this regard,
man it betrays their hypocrisy.
I mean I found all of this a surprise
because all of these people had been telling me
for like the last three years that Trump had fixed the VA
and there weren't any problems anymore,
but apparently that wasn't true because vets can't get hearing aids.
See the thing is they don't care about vets.
They don't, not at all.
The reality is veterans can get hearing aids through the VA.
I know a whole lot that have done it.
It's not a perfect system.
So there are problems.
Veterans can get insulin through the VA.
You know what veterans couldn't get? Confirmation surgery.
Medical procedure they would be entitled to.
They're entitled to medical care, right?
If you actually cared about veterans, this would be a celebratory moment.
This would be a moment to say, hey look,
they're doing something right by vets,
but because it's still socially acceptable to other them,
to cast them out of all other demographics and view them as lesser,
that's not what happened.
Even though they are veterans,
they're not counted because they're different
because we still view it as acceptable to treat them that way.
Now hopefully these new policies will help stem that
because historically that has occurred.
As different demographics get accepted into the veteran community,
they become more accepted by the nation as a whole
because it's the United States.
Nobody wants to talk bad about a vet in this country.
Now to the people in the comment section talking about hearing aids,
I just want to point out that Zoe, who is giving you advice
on how to fill out your tinnitus claim because it's presumptive
and you should totally be able to get your hearing aids that way,
she's a trans vet.
Anyway it's just a thought. You all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}