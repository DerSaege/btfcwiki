---
title: Let's talk about DeSantis beating Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=R7I5XFWzSY4) |
| Published | 2021/06/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor of Florida, Ron DeSantis, beat former President Trump in a conservative straw poll, leading to interesting developments.
- Conventional wisdom suggests that DeSantis being influenced by Trump is positive, but Trump lacks a clear agenda or policy.
- Potential scenarios include Trump attacking DeSantis or trying to co-opt him as vice president.
- This situation benefits the Democratic Party for 2024, but they need to deliver on policy and agenda to capitalize on it.
- Democrats face obstacles in passing major legislation, with Mitch McConnell obstructing progress.
- Lack of effective messaging on key legislation by the Democratic Party is a concern.
- Biden administration needs to focus on communicating the benefits of their agenda to the American people.
- Bipartisanship is unlikely to work due to one side prioritizing culture war over policy.
- Democrats must push their agenda effectively to remain competitive in upcoming elections.
- Biden needs to play hardball and move away from nice guy tactics to make progress.

### Quotes

- "Trump was about himself, he was about ego."
- "The bipartisanship isn't going to work."
- "They're going to have to do better at getting those points out."
- "If the Democratic Party wants to be competitive in 2022 and 2024, they either have to start getting their agenda through or they have to start clearly explaining how it would benefit the average American."
- "He is going to have to play hardball if he wants to get anywhere."

### Oneliner

Governor DeSantis beating Trump in a conservative poll leads to potential Democratic advantages if they can deliver on policy and effectively communicate their agenda.

### Audience

Political analysts, Democratic activists

### On-the-ground actions from transcript

- Communicate the benefits of Democratic policies clearly and effectively (implied)
- Support efforts to push key legislation through Congress (implied)
- Advocate for messaging that resonates with average Americans (implied)

### Whats missing in summary

Insights on the potential impact of effective messaging and policy delivery on upcoming elections.

### Tags

#Politics #2024Election #PolicyDelivery #DemocraticParty #Messaging


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the governor of Florida, Ron DeSantis, beating former President
Trump in a straw poll of conservatives.
That's an interesting development and it plays out in a bunch of ways that are relatively
entertaining for once.
So traditional politics would suggest that this isn't an issue.
Ron DeSantis, well he has pledged his loyalty to Trump.
He's basically Trump Jr.
He's Trump Lite.
And conventional wisdom would suggest that DeSantis and Trump being the top two candidates
in this poll, well that would be a good thing for Trump because Trump would have at least
some influence over DeSantis and Trump's agenda would be carried forward.
That's conventional wisdom about conventional politicians who actually have an agenda and
policy.
Trump doesn't.
Trump was never about agenda or policy.
Trump was about himself, he was about ego.
So the conventional wisdom here doesn't really apply because it's going to play out in a
number of ways.
One is that Trump just decides to start taking swipes at DeSantis and boy that'll be fun.
That'll be entertaining because he'll feel threatened by a younger, more charismatic,
more politically astute, better politician rival.
So he'll try to take him out.
Probably come up with a nickname for him.
That's one option.
Another option is that Trump tries to co-opt him and make him vice president.
I don't know that DeSantis would go for that because DeSantis appears to have a pretty
big ego too.
Why would he be vice president when he could be president?
And if he runs and wins, Trump will undoubtedly try to use a bunch of undue influence over
him.
Which once DeSantis is in office, he is absolutely no use for Trump.
So I don't know that Trump's suggestions or demands will be met.
So it's an interesting little setup.
All of this is pretty good for the Democratic Party.
This is kind of a best case scenario for them for 2024.
But there's an issue.
In order for it to matter, the Democrats have to deliver on policy and agenda.
Because they can frame a Trump-DeSantis team up as, you know, you saw what Trump did last
time.
Be easy to defeat.
Could frame DeSantis alone as just Trump's errand boy.
And it would be pretty easy to do that.
Be pretty easy to frame it that way.
And they could once again run on we're not Trump.
And probably win.
The problem is that's only going to work if they can deliver.
Right now they're hitting roadblock after roadblock when it comes to their major pieces
of legislation.
That roadblock's name is Mitch McConnell.
The obstructionist tactics are working.
And the Democratic Party is not doing a good job of getting its messaging out about the
key pieces of legislation.
I don't know that there's been a lot of talk that has made headlines about what's in the
voting rights bill or the infrastructure bill.
Specifics.
That information isn't really being carried out well enough.
Because the American people are used to controversy.
They're used to things that are, well, just sensationalized.
And that's not this administration.
They're kind of boring.
They're going to have to do better at getting those points out because the bills, the stuff
in the agenda that the Democrats are pushing, that the Biden administration is pushing,
it's stuff the American people want.
That the Biden administration is looking more to the Senate and saying, well, we got to
get through them.
Rather than framing it as making people disaffected with the Republican Senate, with the Republican
senators, with Mitch McConnell.
You could have this, but he's in the way.
The nice guy election portion of this, for the Biden administration, that's over.
He is going to have to play hardball if he wants to get anywhere.
The bipartisanship isn't going to work.
For bipartisanship to work, you have to assume that both groups are at least somewhat in
favor of preserving the American Republic.
One side has proven they're not.
They don't care.
They have devolved into Dr. Seuss and Mr. Potato Head talking points.
They're going to try to win the culture war.
That's the goal.
If the Democratic Party wants to be competitive in 2022 and 2024, they either have to start
getting their agenda through or they have to start clearly explaining how it would benefit
the average American and explaining why it's not getting through.
Because right now, there's a whole lot of people who don't know.
Anyway it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}