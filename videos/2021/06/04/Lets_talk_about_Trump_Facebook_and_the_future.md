---
title: Let's talk about Trump, Facebook, and the future....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iV3aulf1P68) |
| Published | 2021/06/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump will remain banned from Facebook for a couple of years.
- Facebook will not automatically assume that politicians' posts are newsworthy.
- Politicians will no longer be exempt from certain rules on social media platforms.
- Facebook will now apply exemptions for politicians and add a notice below the post.
- This change may have far-reaching effects on political discourse.
- Tech experts once struggled to remove racist content from Twitter due to potentially catching Republican politicians with algorithms.
- The impact of these changes may vary across different regions and countries.
- Different standards for newsworthy content may apply in different countries.
- The diversity of acceptable speech standards may lead to varying social media experiences globally.
- These changes may have more significant implications than Trump's ban for a couple of years.

### Quotes

- "Former President Trump will stay banned for a couple of years."
- "Facebook is no longer going to assume that politicians have newsworthy posts."
- "I think that's going to have much more far-reaching effects than people are imagining."
- "We may be reaching a point where the Facebook experience, where the social media experience differs even more from country to country."
- "As far as the partisan stuff goes, yeah, he's kind of out of the midterms."

### Oneliner

Former President Trump will stay banned from Facebook for two years, and politicians will no longer have automatic newsworthy exemptions, potentially changing the global social media landscape.

### Audience

Social media users

### On-the-ground actions from transcript

- Monitor and participate in the evolving policies and practices of social media platforms (suggested)
- Stay informed about how social media regulations could impact political discourse and representation (suggested)
- Advocate for transparent and consistent standards for social media content globally (implied)

### Whats missing in summary

Insights on the potential long-term impact of social media regulations on political discourse and global communication. 

### Tags

#Facebook #Trump #SocialMedia #Politics #GlobalImpact


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Facebook's big decision.
Former President Trump will stay banned for a couple of years.
No longer be part of the discussion on Facebook.
There's already a little bit of a response coming from people that it's only two years.
I would point out that they don't say he gets his accounts back in two years.
They say that in two years they will review it.
It appears that if metrics involving civil unrest and stuff like that, if they're still
up he won't be going back onto the platform.
In related news that I think is actually maybe even more interesting, Facebook is no longer
going to assume that politicians have newsworthy posts.
Up until now, politicians have been kind of exempt from a lot of rules concerning terms
of service and speech on social media platforms.
That's changing.
They're no longer going to give them that exemption for being newsworthy automatically.
They're going to apply it and if they apply it, they will put like a notice down below.
I think that's going to have much more far-reaching effects than people are imagining.
A couple of years ago there was a discussion about Twitter and why Twitter was having issues
taking racist content off of their platform.
Tech people said the reason they couldn't do it is because if they used an algorithm
to do it, well, they would catch a whole bunch of Republican politicians in it.
If Facebook was to implement that sort of process, if they exempted something, it would
be labeled.
I think that's going to have much bigger consequences and have far-reaching effects on discussion
in this country.
Now, sure, there's a debate to be had about whether or not large social media platforms
should have this much power.
In the system that exists right now, they do.
It is their platform.
They can do with it as they wish.
This is also going to impact politicians overseas and I'm very curious to see how that's going
to play out.
My guess is there will be different standards for different regions and the terms of service
will vary greatly and what is given a newsworthy exemption in, say, the United States, well,
it may not get one in China or vice versa.
We may be reaching a point where the Facebook experience, where the social media experience
differs even more from country to country as different standards of acceptable speech
get applied in different locations.
I think that's much more interesting than the former guy staying banned for a couple
of years.
As far as the partisan stuff goes, yeah, he's kind of out of the midterms.
He's not going to have as large an impact there, especially since he canceled his super
successful blog thing.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}