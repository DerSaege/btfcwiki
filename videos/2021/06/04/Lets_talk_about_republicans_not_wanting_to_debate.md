---
title: Let's talk about republicans not wanting to debate....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=X_BCjN1iBZQ) |
| Published | 2021/06/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republican Party threatens to boycott debates over format and moderation issues.
- Debates serve as a platform for candidates to showcase familiarity with party platform, presidential qualities, and leadership abilities.
- Republican Party lacks a consistent platform, as it is dictated by Trump's whims.
- Party hesitant to address past statements and defend their record on national TV.
- Republican Party struggles to showcase leadership skills, especially outside their core base.
- Suggestion to include third-party candidates in debates if Republicans choose to step away from reality.
- Party still tied to Trump, lacks clear leadership.
- Beau questions the necessity of a press release to state the obvious about the Republican Party's current status.

### Quotes

- "The Republican Party is still beholden to Trump."
- "They don't want to talk about a platform because they don't have one."
- "If the Republican Party is just intent on destroying itself and stepping away from objective reality, let them."

### Oneliner

Republican Party hesitates to participate in debates, lacking consistency and leadership under Trump's influence.

### Audience

Voters, political activists

### On-the-ground actions from transcript

- Advocate for including third-party candidates in debates (implied)
- Stay informed and engaged in political developments (implied)

### Whats missing in summary

Additional context on the importance of political debates and the role they play in shaping public opinion and candidate credibility.

### Tags

#RepublicanParty #Debates #TrumpInfluence #Leadership #PoliticalAnalysis


## Transcript
Well howdy there internet people. It's Beau again.
So today we are going to talk about the Republican Party
suggesting that it will no longer participate in debates
unless they get their way.
Their excuse are some minor issues about format and moderation.
Because of these, they're saying that, well, we won't even participate.
We'll advise our candidates not to participate in the debates.
Okay, first thing that we have to really acknowledge is that
these aren't debates, not really.
The candidates aren't actually debating each other.
This is an opportunity for a politician to show that they're
vaguely familiar with their party's platform,
that they can be somewhat presidential, not embarrassing,
have the quality to lead, and answer questions about their record.
That's what these debates in the United States are.
They're not actual debates between candidates.
So once we acknowledge that, let's maybe guess at the real issues
the Republican Party doesn't want to participate in the debates.
I mean, as far as a candidate's ability to recite their platform,
that's going to be really hard because the Republican Party
doesn't have one right now.
Their platform is whatever Trump says it is.
It's his whims, and they're all beholden to that.
So since he can change his mind at any moment
because he has no consistency,
they don't want to go on record about anything.
They don't want to talk about a platform
because they don't have one and they're not in control of it.
Then when it comes to answering questions about their record,
would you want to get on national TV and defend the statements
that the Republican Party has made over the last few years?
They don't want to talk about the 6th.
They don't want to talk about their statements
during the public health issue.
They don't want to talk about their support
of that failed foreign policy under Trump,
the immigration policies, the economic policies.
They don't want to talk about any of that.
They can't talk about their record until people forget about it.
And it's all still pretty fresh.
That probably has something to do with it.
And then there's the idea that it gives them a chance
to show that they can lead.
Well, that doesn't work either
because outside of their core base, everybody knows they can't.
They haven't been able to lead within their own party
for the last four years.
They couldn't stand up to one entitled billionaire.
What would make anybody think that they could stand up to others
or stand up to world leaders?
They can't.
They've shown that.
If the Republican Party can no longer compete
in the world of objective reality
and they want to rely on alternative facts,
they can do that with their own little fake debate
like their fake audits and their fake hearings.
They can put on a show for that core base
that will continually shrink.
The debate should be opened up to third parties.
Bring in the Green Party, the Libertarian Party,
the People's Party, whoever.
Put them on stage.
If the Republican Party is just intent on destroying itself
and stepping away from objective reality, let them.
This isn't breaking news.
This isn't news.
The Republican Party is still beholden to Trump.
We all know that.
There's not a leader in the Republican Party anymore.
We got it.
We don't need a press release for that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}