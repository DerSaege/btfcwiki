---
title: Let's talk about the reaction to Juneteenth....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_TY7UiFrCuM) |
| Published | 2021/06/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Juneteenth becoming a federal holiday sparked predictable responses from the right.
- Despite GOP support for the holiday, some individuals have taken issue with it.
- Charlie Kirk's response criticizes Juneteenth, claiming it undermines the unity of July 4.
- Beau challenges Kirk's view, explaining the complementary nature of July 4 and Juneteenth.
- July 4 symbolizes the proclamation of equality, while Juneteenth marks an effort to fulfill that promise.
- Beau clarifies that Juneteenth is not about race but about a historic event marking progress.
- He points out that Union troops reaching Galveston to inform the last slaves is a significant part of Juneteenth.
- Beau dismisses the notion of competing holidays and stresses their harmonious coexistence.
- He suggests that future holidays may signify moments when more individuals were included in the promises of equality.
- Beau concludes by encouraging reflection on the significance of these historical commemorations.

### Quotes

- "July 4 and Juneteenth, these are not competing holidays. They're pretty complementary."
- "It's marking a historic event where Union troops got to Galveston and got the news to the last slaves."
- "They're not competing holidays. One does not take away from the other. They go together."

### Oneliner

Beau explains the complementary nature of July 4 and Juneteenth, challenging critics and advocating for understanding historical significance.

### Audience

Americans, History Enthusiasts

### On-the-ground actions from transcript

- Celebrate Juneteenth and July 4 together, recognizing their complementary nature (implied).

### Whats missing in summary

Beau's engaging delivery and nuanced perspective on historical holidays.

### Tags

#Juneteenth #July4 #HistoricalHolidays #Equality #Commemoration


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about
Juneteenth becoming a federal holiday.
We're going to talk about the completely
predictable
response from the right.
Juneteenth becoming a federal holiday was supported
by the GOP.
However, those who
want to show
where they really stand on things, they have taken issue with this.
Those who
really want to show that they're the right kind of patriot, I guess,
and want to continue that culture war.
There's a bunch of responses. My favorite
comes from a fellow named Charlie Kirk.
If you don't know who he is, don't bother looking him up. It's not going to add
anything of value to your life.
Alright, he says,
Lincoln knew America's founding was July 4, 1776.
He knew
that was the day. Our amazing nation made a step from ideal to reality.
Juneteenth is an affront to the unity of July 4.
We now have two summer holidays
and one of them based on race.
Shame on the GOP for supporting this.
Now I have so many questions about that we have two summer holidays thing.
It's not
history's problem to make sure that the historical dates don't interfere with
your vacation planning, Charlie.
And you've got this all messed up.
All messed up.
See, July 4 and Juneteenth, these are not competing holidays.
They're pretty complementary.
They go together.
July 4, that was the day that the people in the colonies loudly proclaimed
that all men are created equal.
Juneteenth, well,
that's the day that
the United States tried to make good on that promise.
Started the process of
making that ideal
a reality.
The Declaration of Independence didn't make the ideal a reality. It just outlined what
the ideal was. It made a bunch of promises.
And it's taken a really long time to fulfill some of them.
We're still working on it.
It's not over.
Because we still have people who think like you, Charlie.
The reality is
this is marking
a day when
it can be clearly defined as a moment
that the United States really did
try to start making good on all of those promises made back in 1776.
That's what it is.
And it's not
based on race, really, either.
It's marking a historic event
where Union troops got to Galveston
and got the news to the last slaves
because it didn't
happen all at one time. There was an email back then, I guess.
Um...
They're not competing holidays. One does not take away from the other. They go
together.
There will probably be similar holidays in the future
that outline
and mark different moments
when more and more people
got included
in those promises.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}