---
title: Let's talk about repealing the AUMF....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2bQKba2ejEs) |
| Published | 2021/06/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The House voted to repeal the Authorization for Use of Military Force (AUMF), a long process expected to be undertaken by the Biden administration with some willingness.
- The 2002 AUMF authorized the Iraq War and has been used to justify other actions due to its broad nature.
- Power was transferred from Congress to the executive branch in the early 2000s, which needs to be corrected.
- Congress, not the executive branch, should declare and make war.
- These authorizations enable presidents to partake in military actions, with Biden supporting the repeal of both the 2002 and 2001 AUMFs.
- McConnell opposes the repeal without providing a valid reason, potentially to allow for future military actions justified by the broad authorization.
- There are uncertainties surrounding the repeal of the 2001 AUMF due to ongoing dynamics in Afghanistan.
- Efforts are being made to restore the military's use to its original intent and reduce military involvement in the Middle East and other regions.

### Quotes

- "It is Congress's job to declare war, to make war."
- "These authorizations allow presidents to engage in military adventurism."
- "There's a lot of people who look at the title of commander-in-chief as meaning that the military is at the total discretion of the president, and that's not the case."

### Oneliner

The House voted to repeal the broad Authorization for Use of Military Force, transferring power back to Congress and curbing military involvement. 

### Audience

Policymakers

### On-the-ground actions from transcript

- Contact your senators to support the repeal of the Authorization for Use of Military Force (implied).

### Whats missing in summary

Detailed explanation of the potential impacts of repealing the Authorization for Use of Military Force. 

### Tags

#AuthorizationForUseofMilitaryForce #AUMF #Congress #ExecutiveBranch #MilitaryInvolvement


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the House voting
to repeal the AUMF,
Authorization for Use of Military Force.
And that's how it's being framed in the media right now,
that it was repealed.
There's two of them and we'll get to all of this.
So this is the first step
in what is going to be a long process,
one that I hope the Biden administration will undertake,
and one it appears they're at least somewhat willing to.
They are supporting this repeal.
The Authorization for Use of Military Force of 2002,
the one that the House voted to repeal,
it still has to go through the Senate,
that was responsible for authorizing the Iraq War.
Because of its language, it's kind of broad,
it's also been used to justify other actions.
One of the main reasons it needs to go away
is that it was overly broad.
There was another one in 2001 that was passed
that was specific to Afghanistan.
This clawing back of power
is something that definitely needs to occur.
A whole lot of power got transferred
in the early 2000s from Congress to the executive branch.
It never should have happened.
It is Congress's job to declare war, to make war.
It is their job.
It's not the executive branch's.
These authorizations allow presidents
to engage in military adventurism.
The Biden administration fully supports repealing the 2002 one.
They seem to be in favor of repealing the 2001 one as well.
We'll have to wait and see how it plays out.
McConnell, of course, opposes it.
Why?
He doesn't have a reason.
He just doesn't want the Democrats to do anything.
There is no legitimate reason to keep the authorization
for the Iraq War on the books
other than to undermine the Constitution
and transfer the power to make war to the executive branch.
This is probably a case of him just being obstructionist McConnell.
We do need to entertain the idea that he wants the next president
to be able to use this authorization for some military adventurism,
some action that can be justified using the very broad language in this act.
I'm fairly certain it will get through the Senate on the 02 one.
Because we're toying with the withdrawal
and there's a lot of dynamics up in the air when it comes to Afghanistan,
the 2001 authorization, I'm not so sure about.
There are people trying.
I don't know how successful they're going to be,
but the attempt is being made.
There's a whole lot when it comes to the use of the military
that needs to kind of get back to its roots.
There's a lot of people who look at the title of commander-in-chief
as meaning that the military is at the total discretion of the president,
and that's not the case.
That's not how it was laid out to be.
That's not what's supposed to be happening,
but it is what has been happening.
These authorizations, these broad authorizations for the use of force
need to go away.
It will curtail our military involvement in the Middle East
and hopefully elsewhere.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}