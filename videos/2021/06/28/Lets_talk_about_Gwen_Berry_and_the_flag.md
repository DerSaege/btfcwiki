---
title: Let's talk about Gwen Berry and the flag....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NzEU5K3Y7xU) |
| Published | 2021/06/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Gwen Berry drew attention to systemic issues in the US during a national anthem moment, a move unprecedented for athletes.
- Politicians have become more focused on generating outrage rather than policy, using the incident for political gain.
- The flag is described as a piece of cloth symbolizing the ideals of the country, which some politicians have failed to uphold.
- Politicians who have not addressed systemic issues don't get to feign outrage over Berry's actions.
- Berry's actions are aimed at policymakers, urging them to address long-standing issues, which many politicians have ignored for years.
- Rather than uniting the country and addressing real issues, some politicians prefer to stoke division for their own political careers.
- Beau suggests that kneeling may be a more powerful form of protest imagery but respects Berry's choice of protest.
- He questions the credibility of those criticizing Berry's actions, especially if they downplayed violent events involving the flag in the past.
- Beau points out the hypocrisy of using flag etiquette to criticize Berry when real issues are being ignored for political gain.
- He ends by encouraging viewers to think about the message shared and wishing them a good day.

### Quotes

- "That flag, it's a piece of cloth. In and of itself, it means nothing."
- "You don't get to pretend to be mad about this, because this isn't new."
- "Rather than addressing the issues, you are making them worse."

### Oneliner

Beau unpacks Gwen Berry's protest, calling out politicians for prioritizing outrage over policy, exposing their hypocrisy in feigned outrage.

### Audience

Politically Engaged Citizens

### On-the-ground actions from transcript

- Contact policymakers to address systemic issues (implied)
- Participate in peaceful protests for social justice (implied)

### Whats missing in summary

The full transcript provides more context and depth on the importance of addressing systemic issues and the hypocrisy of politicians in the face of protests against injustice. 

### Tags

#GwenBerry #Protests #PoliticalHypocrisy #SystemicIssues #FlagSymbolism


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about Gwen Berry.
If you don't know, person qualified
for the US Olympic team, national anthem.
And she did something completely unprecedented,
something no athlete has ever done before.
She took that moment to draw attention
to systemic issues in this country.
And she did it for the first time on Monday.
Now, this didn't happen today.
But many of our politicians have turned into 1990 shock jocks.
So they held it for Monday so they
could generate the most outrage.
Because that's what they're about now.
They're about generating outrage,
not about generating policy.
I would like to clear a few things up.
First and foremost, that flag, it's a piece of cloth.
It is a piece of cloth.
In and of itself, it means nothing.
It's a symbol.
It's a symbol of the ideals this country
is supposed to represent.
It's the symbol of a representative democracy.
If you have spent the last year attempting
to undermine the ideal of a representative
democracy in this country, you don't
get to pretend to be mad about this.
You've already shown us that you don't care about those ideals.
To you, it's not a symbol of those ideals.
It's a bumper sticker.
It's something you can rally your base behind.
That's it.
Nobody cares about your fake outrage.
This is doubly true if you are a politician in the United States,
if you are somebody that has the ability to influence policy,
if you are a member of Congress.
You don't get to pretend to be mad about this,
because this isn't new.
These protests, these events have been going on for years.
You're their audience.
If you're a politician in the United States,
they are aimed at you.
They are trying to get your attention
so you will address some of the systemic issues
in this country.
And you have done nothing.
You don't get to complain about it.
You don't get to pretend to be upset,
because you have done nothing.
Instead of realizing that there are issues in this country that
need to be addressed by you, you would rather
go on some pundit show and stoke outrage.
Rather than unite the country, you
would divide it, because it's better for you,
because you don't care about the country.
You care about your own political career.
Now, I had somebody ask me point blank
if I think the tactics, the way she did it, if I liked that,
or if I liked kneeling.
I think it's better to kneel.
Personally, I think it makes better imagery.
But I would point out a number of things about that.
One is that, well, she didn't ask me.
She worked very hard over the course of her life.
And I would imagine that she knew
what she was going to do when put in this situation.
I'm not going to second guess that, really.
I would also suggest that maybe she
doesn't want to get on the ground around a bunch of people
who might become angry at her exercising the principles
enshrined in the Constitution.
They might decide to beat her with the flag.
If you said nothing about a person being beaten
with the flag at the Capitol, if you've
attempted to downplay that event,
you're going to pardon me for not taking
your advice on flag etiquette.
Because again, you don't actually care.
It's not something that bothers you.
It's just something you can use to make Americans angry,
to make them afraid.
Rather than addressing the issues,
you are making them worse.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}