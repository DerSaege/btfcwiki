---
title: Let's talk about Trump's DOJ investigating congress....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_6BIKw3ufd4) |
| Published | 2021/06/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump's Department of Justice used a secret subpoena process to gather information on members of Congress.
- The outrage shouldn't be because it happened to members of Congress; the focus should be on whether the Department of Justice followed normal procedures.
- If DOJ followed policies and rules in place to get the subpoena, there isn't a scandal – it's about applying the same standards to everyone.
- The burden of proof required to gather such information may be too low and needs to be raised.
- Politicizing law enforcement and using secretive forms can be dangerous for a free society.
- Legislative fixes are needed to ensure equal application of rules and procedures in gathering information.
- The real scandal lies in the low standards applied by DOJ and the need for legislative changes.
- The focus should not be on how it happened to a member of Congress, but on how it happened in general.
- Policies need to be resilient and capable of withstanding misuse by future administrations.
- Changing laws and rules with higher standards for gathering information is the solution, not pointless inquiries.

### Quotes

- "The outrage shouldn't be because it happened to members of Congress; the focus should be on whether the Department of Justice followed normal procedures."
- "If DOJ followed policies and rules in place to get the subpoena, there isn't a scandal – it's about applying the same standards to everyone."
- "The real scandal lies in the low standards applied by DOJ and the need for legislative changes."
- "Changing laws and rules with higher standards for gathering information is the solution, not pointless inquiries."
- "Policies need to be resilient and capable of withstanding misuse by future administrations."

### Oneliner

Former President Trump's Department of Justice used a secret subpoena process on members of Congress; the focus should be on ensuring equal standards and legislative fixes for information gathering.

### Audience

Legislators, Activists, Citizens

### On-the-ground actions from transcript

- Push for legislative changes to set higher standards for information gathering (suggested)
- Advocate for resilient policies that withstand misuse by future administrations (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the politicization of the Department of Justice and the need for legislative fixes to ensure equal application of standards in gathering information.

### Tags

#DepartmentOfJustice #LegislativeFixes #InformationGathering #PolicyChanges #ResilientPolicies


## Transcript
Well howdy there, Internet people. It's Beau with Gown. So today we are going to talk about
the Department of Justice under former President Trump. It appears that the DOJ under Trump
did exactly what we kind of all knew they were doing. They used a secret subpoena process,
basically, to gather information on members of Congress. And that's why it's being framed
as an outrage. Because it happened to members of Congress. Because the Department of Justice
was politicized. That's not the outrage to me. That doesn't even make sense. The people
who created the laws were impacted by the laws. I don't see the huge issue here. It's
being framed as though it is somehow more grievous in nature because it happened to
members of Congress. Because it was politicized. My question, and the thing I haven't seen
in any of the reporting, is whether or not the Department of Justice followed the normal
policies and rules and procedures that are in place. If they did, and they were able
to get the subpoena, then where's the issue? Where's the scandal? I mean there is one,
but it's not a partisan one. It has nothing to do with politicizing the Department of
Justice. If we can all look at this and say, hey, this is wrong. This shouldn't have
happened. It's not going to be solved with an inquiry. It needs a legislative fix. If
it is wrong for them to do this to a member of Congress, it's wrong for them to do it
to everybody. I'm going to suggest that it is entirely possible that the burden of proof
required of the Department of Justice to gather this kind of information in this way is too
low. It certainly seems to be the case. I'm going to suggest that gathering information
like this on a minor that is completely unconnected to this is probably wrong. That's probably
not something that should happen. But that's not a partisan issue. That's a legislative
one. That's one that should be fixed through legislation, not some political inquiry so
they can sling mud. Unless they don't actually want it fixed, they just want to set the standard
that those rules shouldn't be applied to them. As much as I joke about it on this channel,
I do not believe that those in DC are our betters or that we are commoners or that there
should be two sets of rules. If DOJ followed the policies and rules and procedures that
are in place to gather this information, then there's no scandal. They just applied the
same thing that all of Congress was okay with being applied to us, to them. Now I'm going
to suggest that, yeah, those standards are too low and they need to be fixed. That's
the scandal. That's where the outrage should be. The question isn't how did they do this
to a member of Congress. The question is how did they do this? This is why there are supposed
to be checks. There will be another Trump. That's why policies like this have to be pretty
carefully crafted. They have to be resilient. They have to be willing or capable of withstanding
attacks like this. There will be another authoritarian. The secretive forms of law enforcement that
are coming about. Maybe that's not a good idea. You're going to be hard-pressed to find
a free society that fell because it didn't have a domestic secret police force. You're
not going to find a lot of that in history. You will find them that fell because they
had one. They politicized it. Yeah, that's what politicians do. I'm going to suggest
that the burden of proof required to gather this information needs to be higher. That
that's the solution. Not an inquiry that produces nothing. They have to change the laws. They
have to change the rules. And it has to be universally applied. I'm not going to be outraged
because our betters were held to the same standard we were. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}