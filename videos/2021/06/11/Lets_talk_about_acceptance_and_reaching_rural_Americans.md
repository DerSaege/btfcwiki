---
title: Let's talk about acceptance and reaching rural Americans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=V8nLk--EGjw) |
| Published | 2021/06/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains changing opinions and reaching out to rural Americans about social issues.
- Mentions a message about understanding the concept of "on a long enough timeline, we win."
- Talks about acceptance of gay marriage in the United States reaching an all-time high.
- Advises not to try to convince rural Americans they're wrong but to focus on their base principles.
- Emphasizes the right to be left alone, a fundamental principle in rural areas.
- Describes how small farms in rural areas help each other in mutually beneficial ways.
- Suggests leaning into the idea that people should be able to do what they want as long as they're not harming others.
- Gives an example of how rural Americans handle situations like kids and pronouns differently.
- Points out that rural people don't tell others how to raise their kids and avoid involving the law.
- Encourages starting with the shared belief that using government force to regulate private lives is wrong.

### Quotes

- "Lean into the fact that every rural American knows that's wrong."
- "If you can get a rural American to apply the base principles they use every day in their life to everything, they become socially progressive overnight."

### Oneliner

Beau explains reaching out to rural Americans by focusing on shared principles and avoiding trying to convince them they're wrong about social issues.

### Audience

Social activists, community organizers.

### On-the-ground actions from transcript

- Start community dialogues on shared values and principles (suggested).
- Organize workshops on mutual support and cooperation within rural communities (implied).

### Whats missing in summary

In-depth examples and personal anecdotes that provide a deeper understanding of rural Americans' perspectives on social issues.

### Tags

#Opinions #SocialIssues #RuralAmerica #CommunityBuilding #SharedValues


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
changing opinions and how to reach out to rural Americans about social issues. I got
a message saying, hey, for the first time I actually understand what you mean by, on
a long enough timeline, we win. The maximum amount of freedom for the maximum amount of
people. That is going to happen on a long enough timeline. The thing that convinced
this person was the most recent poll about acceptance of gay marriage in the United States.
Attitudes are changing. It is at an all-time high. So then the question becomes, how do
I convince the people where I live, rural Americans, that they're wrong about things?
Oh, don't do that. You're not going to get anywhere. You can't convince rural Americans
they're wrong about anything. That's like our thing. We are right. Don't try to convince
them they're wrong. Convince them that their base principles are right and they just need
to be applied across the board. You know, one of the greatest things about living in
the country is that you get to exercise the fundamental base principle enshrined in the
Bill of Rights. It's the right to be left alone. That's really what all of it boils
down to. And out here you get to do that. You know, small farms engage in assisting
each other in mutually beneficial ways all the time. Now they would never talk about
it in the ideological sense, but that's what they do. And if Bill goes over to Jim's
farm to pick up some extra fencing or whatever, and he happens to look over into a tomato
field and he realizes that not all the plants in that tomato field are tomatoes, is he going
to call the law? Of course not. Because what's on Jim's side of the fence isn't Bill's
problem. And if you don't, I'm sure somebody will explain that in the comments. That fundamental
idea that people should be able to do whatever they want as long as they're not harming
somebody else. That's a big thing out here. Lean into that. You know, when it comes to
the subject of kids and pronouns, next time it comes up, just say the phrase, you know,
you will never catch me telling somebody how to raise their kids. And watch their face.
Because that's a big thing out here. And if it becomes a conversation, ask them what
they think is more traumatic. Words, just names, or parents who take their kid deer
hunting a little too early. Because we all know what happens. First time you take a deer
you have to process it, right? We all know parents that took a kid out to do that a little
too early and they cried through the whole thing. Which do you think is more damaging?
Is one damaging and one's not? But at the end of the day, does anybody correct that
parent? No. Because it's not something that rural people do. We do not tell people how
to raise their kids. We don't really, we may judge it, but we do it privately. We certainly
do not try to involve the law, which is what a lot of this culture war is about. It's
about using the force of government to regulate people's private lives. Lean into the fact
that every rural American knows that's wrong. Start there. Don't convince them they're
wrong about the issues. Say, sure, you know, that's not the way I'd do it, but they're
not hurting anybody. Their attitude will change. If you can get a rural American to apply the
base principles that they use every day in their life to everything, they become socially
progressive overnight. It's the most effective thing I've ever found. Anyway, it's just a
thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}