---
title: Let's talk about Texas building the wall on its own....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=w4l1FQBRsgk) |
| Published | 2021/06/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas plans to build a wall with online donations and a $1.1 million budget.
- The previous wall built by Trump was easily defeated in 15 minutes with a sawzall.
- Spending over a billion dollars on a proven ineffective wall seems unwise.
- Even if the cost is cut in half, it will only cover about 80 miles of the border.
- Texas doesn't own the border, so seizing land for the wall could cause issues.
- Another candidate, Huffines, claims credit for the wall idea and plans to close the border to commercial traffic without federal permission.
- Huffines' plan faces challenges as Mexican trucks could use alternate routes to cross.
- Texas building its wall behind federal checkpoints could lead to legal issues.
- Beau believes the wall is not a successful solution and is more rhetoric than pragmatism.
- The heat in Texas might be affecting politicians' decision-making.

### Quotes

- "Spending over a billion dollars on a proven ineffective wall seems unwise."
- "Texas building its wall behind federal checkpoints could lead to legal issues."
- "The heat in Texas might be affecting politicians' decision-making."

### Oneliner

Texas plans a wall with online donations and $1.1M, facing criticisms for inefficacy and legal issues, reflecting misguided political rhetoric influenced by Texas heat.

### Audience

Texans, Voters

### On-the-ground actions from transcript

- Challenge the idea of building an ineffective and costly wall in Texas (implied).
- Stay informed about political decisions and hold elected officials accountable (implied).

### Whats missing in summary

The full transcript provides additional details on the challenges of building a border wall and the potential legal and practical issues it may face.

### Tags

#Texas #BorderWall #PoliticalRhetoric #Ineffectiveness #LegalIssues


## Transcript
Well howdy there internet people, it's Beau again.
It must be really hot in Texas, because it seems to be affecting the way people are thinking.
So today we're going to talk about the wall that Texas is going to build, apparently.
The current governor of Texas, Abbott, is basically going to solicit online donations
to build the wall in addition to a $1.1 million budget, because the online donation thing
that worked out so well last time.
There's a whole lot of problems with this idea.
Let's start by saying that it may not be a good idea to spend more than a billion dollars
to build something that has already been proven not to work.
That super secure best fence ever, the wall that Trump put up, was defeated in about 15
minutes by a sawzall.
That's a reciprocating saw to people up north.
I'm going to suggest that may not do the job.
I would also point out that that wall, the best that's ever been built down there, that
ran about $26 million a mile.
So even if they were able to cut the cost in half, that $1.1 billion is going to get
them about 80 miles of wall.
Now I don't know how long the border between Texas and Mexico is, but I know how long it
takes me to drive through Texas to do it 90.
I'm pretty sure 80 miles ain't going to cut it.
Aside from that, it needs to be pointed out that Texas doesn't own the border, which means
they're going to have to seize a bunch of land from Texans to build this wall.
Generally speaking, that hasn't gone over well with Texans in the past.
This seems like a really, really bad idea.
Now not to be outdone, a candidate who is going to be primarying the governor there
in Texas, Huffines, he wants everybody to know that this genius plot was actually his
idea and that the current governor stole it from him.
And he wants to take it a step further.
In addition to the wall idea that he's taking credit for, he said that he's going to, without
federal permission, he's not even going to ask the federal government, he's going to
close the Texas-Mexico border to commercial traffic and get Mexico to quote acquiesce
and secure their side of the border.
I've seen this episode, Mexico's going to pay for the wall.
There's a couple problems with this idea.
One is I'm fairly certain the federal government would have something to say about this.
Two, even if you did close all the border crossings between Texas and Mexico, I'm pretty
sure that the Mexican trucks would just drive to the next state over to cross.
I mean that seems like the most likely option.
Aside from that, let's assume that Texas is successful in building its wall.
It will be behind the federal wall, the federal checkpoints at the border crossings, which
means that if they're not going to coordinate this with the federal government, the truck
will have passed by the federal checkpoint before it reaches whatever Texas checkpoint
exists, which means that truck will be on U.S. soil.
I think some lawyers might need to get involved in the planning of this because I'm fairly
certain that that's not going to go the way our aspiring governor believes it will in
court.
Here's the thing.
At the end of the day, the wall doesn't work.
They've built it.
It wasn't even completed, but rather than go around to the open sections, they just
cut through it.
It's not a success.
It's not something that needs to be continued.
It's not something you need to seize a bunch of land over.
It's not something you should spend a billion dollars on.
This just seems silly to me.
Maybe these candidates and the governor should spend a little bit more time in the AC if
they could keep it working.
That might be the best bet because it seems to me that the heat is affecting the way people
are thinking right now.
This doesn't seem to be a successful re-election strategy, really, if people think about it
for a second at all, even, and it won't work pragmatically.
It's already been demonstrated to not be effective.
This seems like a bunch of rhetoric designed to appeal to the uneducated, honestly.
That's what it seems like.
I don't believe that neither the governor nor his primary opponent thinks that this
will work.
I think they know it won't.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}