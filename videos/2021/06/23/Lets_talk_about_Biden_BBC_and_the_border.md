---
title: Let's talk about Biden, BBC, and the border....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kno_u5dwt0g) |
| Published | 2021/06/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration inherited multiple messes at the border, including handling an influx of unaccompanied minors seeking asylum and reunification.
- Ad hoc facilities were quickly set up to process and house these minors, reducing processing and holding times initially.
- Issues surfaced at these facilities, such as undercooked food, lack of COVID prevention measures, and lice infestations.
- Despite initial understanding, problems persist months later, with reports of ongoing issues like undercooked food and lice outbreaks.
- Relationships between staff and detainees, especially children, are forming, raising concerns given the nature of the situation.
- While applauding improvements in processing times, Beau stresses that the conditions these individuals are subjected to are unacceptable.
- Urgent action is needed to address the disturbing evidence of mistreatment and neglect at these facilities.
- Beau calls for immediate intervention by the Inspector General to fix the problems, rather than just producing a report.
- He criticizes the administration's focus on foreign policy moral authority while such issues persist domestically.
- The treatment of individuals in these facilities contradicts the care they should receive and undermines the administration's credibility.
- Beau underscores the importance of prioritizing the well-being of real people over political battles or posturing.
- He stresses that the situation, initially somewhat understandable, has persisted for months and must be resolved urgently.

### Quotes

- "That is not acceptable."
- "This needs to be fixed and fixed now."
- "They're supposed to be in the care, not custody."
- "This has got to get fixed."
- "It's been months. This has got to get fixed."

### Oneliner

The Biden administration must urgently address ongoing issues and mistreatment at border facilities housing unaccompanied minors seeking asylum, shifting focus from political posturing to real care.

### Audience

Advocates, policymakers, concerned citizens

### On-the-ground actions from transcript

- Contact elected officials to demand immediate intervention and improvement in conditions (suggested)
- Support organizations working to provide assistance and oversight at these facilities (exemplified)

### Whats missing in summary

The emotional urgency and call for immediate action is best experienced in Beau's original delivery.

### Tags

#BorderFacilities #Immigration #HumanRights #BidenAdministration #PolicyConcerns


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about understanding
and when understanding runs out.
You know, when the Biden administration took over,
they were handed a mess, a bunch of messes,
a giant collective mess.
One of them was a situation down at the border
and an influx of unaccompanied minors
seeking asylum and reunification.
Pretty quickly, the Biden administration
threw together an ad hoc series of facilities to house them
and to process them.
Pretty quickly, they got the time
that they were being held by border patrol down
to acceptable levels.
They cut the processing time to be reunified by 25%.
I will stand up and applaud that.
That's great.
However, there were issues when these facilities opened.
Undercooked food, not having access to the facilities
to prevent transmission when it comes to COVID,
lice, we heard about all kinds of stuff.
But when you go from housing nobody
to housing more than 10,000, there's
going to be bugs, figurative and literal.
So people were understanding.
In April, in April, there was understanding.
It is now the end of June.
And due to some very brave people
at the facility at Fort Bliss, there is audio and video
and photos that were smuggled out to the BBC.
Undercooked food is still apparently happening.
Lice is still apparently happening.
It has been months.
When these facilities are opened up,
sure, you're setting up medical facilities and restaurants
and hotels all at once, and you're doing it really quickly.
That's what it amounts to.
It amounts to opening several different types of facility
at once.
There's understanding.
But after months, you don't have the cook times right.
You haven't ordered lice kits.
You can get those on eBay.
So you have all of this.
And then you have the main issue.
You have relationships, quote, forming between staff
and the people there, people being housed there, children.
I'm fairly certain that relationship is not
the right term to use.
Aside from the whole adult child thing,
we also need to keep in mind that for all intents
and purposes, these are detainees.
They're not supposed to be.
But given the current situation, that's how they're being treated.
That's what they are.
If you strip away the sanitized language, they're being held.
Now, I'll applaud the Biden administration for cutting it,
for cutting the time.
It's down to like a month now in Health and Human Services
to get reunified with your family.
That's great.
But if during that month they are subject to the type of things
it certainly appears that they're being subjected to,
that's not acceptable.
That is not acceptable.
I don't care about Vice President Harris
going to the border and playing with binoculars.
I do care about the IG walking into these facilities
and fixing this.
We don't need a report.
We need it fixed.
The Biden administration is pushing this idea
that America is back on the foreign policy stage,
trying to assert some moral authority.
That's going to be super hard when there are camps
with COVID-infected, lice-riddled children eating undercooked
meat and being abused.
This needs to be fixed and fixed now.
A large portion of the American population
did not vote for Biden.
They voted against Trump, and they did that
because of stuff like this.
Is it isolated?
We don't know.
We don't have enough information.
What we do know is the risks that
were taken to get this information out,
pretty substantial.
And what that evidence reveals is disturbing,
and it needs to be addressed.
This is more important than a pretend battle
over a border wall.
This is more important.
These are real people having real problems
that are supposed to be in the care of the US government.
They're supposed to be in the care, not custody.
They're being treated like detainees,
and that was understandable.
It wasn't excusable, but it was understandable
in the beginning.
It's been months.
This has got to get fixed.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}