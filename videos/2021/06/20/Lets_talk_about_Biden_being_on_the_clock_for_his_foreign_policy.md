---
title: Let's talk about Biden being on the clock for his foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RurzLty-sTI) |
| Published | 2021/06/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration running out of time to enact a key foreign policy piece central to their plans.
- Mentioned Raisi's candidacy for president in Iran, now he's won, raising concerns.
- Divided opinions on Raisi: some see his alignment with Supreme Leader as a positive, while others criticize his history.
- Biden administration suddenly in a hurry to finalize the deal before Raisi takes office.
- Regardless of the president, Supreme Leader's desires dictate Iran's actions.
- Uncertainty looms over Iran's commitment to the deal post-election.
- Biden administration's delay in prioritizing the deal earlier is criticized.
- Deal's completion before the new government transitions is deemed necessary.
- Potential implications on political dynamics in the Middle East and Iran's legitimacy post-deal.
- Biden's goal is not just the deal but also Western legitimacy for the Iranian government.

### Quotes

- "Biden administration is running out of time to enact a key piece of their foreign policy."
- "Supreme Leader's desires dictate Iran's actions regardless of the president."
- "Biden wants the deal so the Iranian government can be more legitimized in the eyes of the Western world."

### Oneliner

Biden admin races against time to finalize key foreign policy amid Raisi's win in Iran, impacting Middle East dynamics and Western legitimacy.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Monitor developments in Iran's political landscape (implied).
- Stay informed on the Biden administration's foreign policy decisions (implied).

### Whats missing in summary

Insights on potential consequences of delays in enacting the foreign policy piece.

### Tags

#BidenAdministration #ForeignPolicy #Iran #Raisi #MiddleEast


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about how the Biden administration is running out of time again
to enact a key piece of their foreign policy, something that is central to the overall plans
that they have. About a month ago, I made a video saying that the Biden administration was
on the clock, they were running out of time, the deal they wanted was about to turn into a pumpkin.
I said this because a man named Raisi had announced his candidacy for president in Iran.
I was told at the time that that was incredibly unlikely. He just won.
Now you have the talking heads out there right now saying that, well this is good though,
because he is a hardliner the way he is, this means that he's going to do whatever
Supreme Leader wants and Supreme Leader wants back in the deal and that may be true.
There are also a whole bunch of talking heads out there saying that, you know, he's very committed
to the nuclear program and this is the worst thing ever and then there's a whole bunch of people out
there, you know, complaining and pointing out the whole history of the man in relation to
things that many might describe as crimes against humanity. Definitely a complicated figure to say
the least. The Biden administration, for their part, said yesterday that all of a sudden they're
in a hurry. They want to get the deal enacted before he takes office. I would suggest that's
a really good plan and probably should have started a month ago. I feel like they dropped the ball here.
I feel like they dropped the ball here. Now at the end of the day, the Supreme Leader and Iran,
that they, has control. Has control. Whatever the Supreme Leader wants is what is going to happen
on, in regards to the deal. Regardless of who is president, that is what is going to occur.
That is what is going to occur. We have indications that he wants back in the deal,
that he wants to get Iran back in the deal. At the same time, candidates for president in Iran
have to be approved. Raisi was approved. A lot of those who were more liberal weren't.
There is, there's a lot of concern because of that. The Supreme Leader's commitment to getting
back in the deal may not be what it once was. We don't know. There's a whole lot right now that we
don't know and there's a lot of talking heads making, making proclamations about what's going
to happen. The reality is we don't know, but this just got a whole lot more complicated.
The Biden administration's push to get the deal finalized prior to the transition to the new
government is a smart one. It's the right decision. It's a decision that should have been made a month
ago. I'm not sure why it wasn't given a higher priority then, but here we are.
Aside from that, this alters a lot of other political dynamics in the Middle East and as far
as the hopes of bringing Iran out, it seems less likely, which is the overall theme. Biden doesn't
want the deal just to have the deal. Biden wants the deal so the Iranian government can be
more legitimized in the eyes of the Western world so it can come out, so it can act as a third pole
of power with a hardliner that is already sanctioned by the U.S. and who has quite a checkered history
taking the presidency. It makes that second phase, assuming they get the deal, it makes that
that much more difficult. So, I think the Biden administration is going to have to make a decision
about whether or not they want the deal. It makes that that much more difficult. This whole process,
Biden's whole foreign policy, just got tripped up as far as for the Middle East. So, we're going to
have to wait and see. I'm not going to make proclamations about what's going to happen
because nobody knows and anybody who tells you that they do know is lying. Anyway, it's just a thought. Thank you.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}