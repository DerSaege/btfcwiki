---
title: Let's talk about the Biden admin listing political ideologies as bad....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=nTSbjuwMTTI) |
| Published | 2021/06/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the context of a document released by the Biden administration from the Office of the Director of National Intelligence.
- Clarifies that misinterpretations circulating online about the document are causing confusion.
- Describes the different ways the document is being presented, including inaccuracies and misrepresentations.
- Breaks down the definition of domestic violent extremists (DVE) provided in the document.
- Emphasizes that the document does not target individuals based on their beliefs or activism.
- Assures that the document is not a cause for concern and follows a similar pattern to previous bulletins.
- Addresses the potential legal issues around designating certain groups within the U.S. as illegal.
- Notes that certain rhetoric online may lead to being tagged as a sympathizer and subject to surveillance.
- Stresses the importance of avoiding violent rhetoric online to prevent unwanted attention.
- Concludes by providing reassurance and encouraging viewers to have a good day.

### Quotes

- "Just because you're an environmentalist or an animal rights activist or any of the other stuff listed here, that doesn't mean that they view you as a DVE."
- "If you don't know, this is my area of study. I have read everything the Biden administration has released on this topic."
- "Without a constitutional amendment, the U.S. government does not have the authority to designate a domestic group."
- "It's just saying that some DVEs have this as a motivation."
- "Just on this one, unless you're hurting people, it doesn't apply to you."

### Oneliner

Beau clarifies misinterpretations of a document from the Biden administration, reassuring viewers that being an activist doesn't equate to being viewed as a domestic violent extremist.

### Audience

Concerned viewers

### On-the-ground actions from transcript

- Read and understand the actual document from the Office of the Director of National Intelligence (suggested)
- Avoid using violent rhetoric online to prevent unwanted attention and surveillance (implied)

### Whats missing in summary

In watching the full transcript, viewers can gain a comprehensive understanding of the misconceptions surrounding a government document and be reassured about the implications for activists and individuals with different beliefs.

### Tags

#BidenAdministration #NationalIntelligence #DVE #Misinterpretations #Activism


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to try to set some
minds at ease. I have gotten questions from all over the political spectrum about a document
released by the Biden administration from the Office of the Director of National Intelligence.
I read it a few days ago, didn't raise any eyebrows at the time I read it,
then I saw how it was being presented online and it all started to make sense.
Short version, if you don't want to watch the entire video, if you saw a list of political
or ideological beliefs and were told that if you believe this the Biden administration is targeting
you, that is not what this says. It is being presented in one of three ways. One is just a
list of beliefs. That's not even based on this document. It's not in the document and it's not
based on the document. If you see that one, just ignore it. One is a screenshot of half of a page
that has a bunch of political and ideological beliefs. And if you're looking at that, it
certainly does appear to suggest that if you believe these things, they view you as a violent
extremist. That's also not what it says. The third way is the full page. That top paragraph
is like super important to understanding what you're reading. But even if the top paragraph
is there, if you're not used to reading stuff like this, it still might be a little intimidating.
So we're going to go through it. And again, this is from the March bulletin from the Office of the
Director of National Intelligence about the threat level from domestic violent extremists and how
it's heightened in 2021. I will have the link to the document and the link to a video that I'm
going to reference down below. What you are looking at is just a visual representation of
a definition that you have heard before on this channel. I have provided the definition to the
T word numerous times. And when I do, I always say that each clause is important. If it doesn't
have all four clauses, it's not actually terrorism. That's what this list is. That's what you're
looking at is just a visual representation of that definition. What is the definition?
The unlawful use of violence or the threat of violence. Two clauses right there.
To influence those beyond the immediate area of attack to achieve a political, religious,
ideological, or monetary goal. Four clauses. That's the definition. That's a good one. There's
a whole bunch of bad ones out there. But if you actually want to understand the subject matter,
that's the good one. That top paragraph that is missing from some of the screenshots,
it says domestic violent extremists are U.S. based actors who conduct or threaten activities
that are dangerous to human life in violation of the criminal laws of the United States
or any state. So the unlawful use of violence or the threat of violence.
Appearing to be intended to intimidate or coerce a civilian population and influence the policy of
a government by intimidation or coercion or affect the conduct of the government by blah, blah, blah,
blah, blah. Influence those beyond the immediate area of attack to achieve a political, religious,
ideological, or monetary goal. That's that list down below. That's all you're looking at is just
a definition of DVE, which is the new buzzword. They're not homegrown anymore. They're not
domestic. They are domestic violent extremists. That's all you're looking at. This is no different
than every other bulletin like this over the last 30 years. There's nothing to raise your
eyebrows here. It doesn't mean that if you're an animal rights activist, the FBI is going to start
kicking in your door. That's not what this is. In fact, it's even more liberal than most
because it has this whole section. Mere advocacy of political or social positions, political
activism, use of strong rhetoric, or generalized philosophical embrace of violent tactics may not
constitute violent extremism and may be constitutionally protected. While the majority
of DVEs fall into one threat category, racist, it actually says it for once, some draw upon or are
inspired by ideological themes found in other threat categories. That's what you're seeing.
That's all this is. This isn't something to worry about. It's being framed in a pretty
wild way by a lot of people. This is just a visual representation of the definition you have heard
before on this channel. That's it. If you don't know, this is my area of study. I have read
everything the Biden administration has released on this topic. There is only one thing that has
raised eyebrows at all, and that is in some of it, you can see that they are looking into
designating groups within the United States and making membership in them illegal.
Illegal. They're not going to do it. About every six years, somebody suggests this,
they always run into the same roadblocks, because it sounds good in theory, but first the lawyers
come out and the lawyers are like, yeah, you know, in the United States you have the right
to peaceably assemble. We don't do collective punishment. They have freedom of speech.
Short version, without a constitutional amendment, the U.S. government does not have the authority
to designate a domestic group. This is why the Klan has never been designated. We can't do it
in the U.S. without changing the Constitution. The next roadblock they will get is the weirdos.
The subject matter experts will walk in and be like, yeah, I know that sounds good, but that's
actually bad. You want a public-facing organization that attracts members. You want that to exist.
It kind of acts like a lightning rod and makes it easier to counter, theoretically. So it's bad
doctrine to ban the groups. The goal is not to drive them underground. The goal is to get them
to embrace peaceful means. So that's where you're at. Just because you're an environmentalist or an
animal rights activist or any of the other stuff listed here, that doesn't mean that they view you
as a DVE. It's just saying that some DVEs have this as a motivation. So I hope that sets some
people's minds at ease. And just on this one, unless you're hurting people, it doesn't apply to you.
Now I will say that even though it has this section about rhetoric, I would point out that
if you were to use a bunch of violent rhetoric online, you would probably get tagged as a
sympathizer and be subject to surveillance. That would stand to reason, even though that's in there.
So don't say things you don't really mean. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}