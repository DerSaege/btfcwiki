---
title: Let's talk about blame, leaks, and why we won't be ready....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DzM4VKoOuPE) |
| Published | 2021/06/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Raises concerns about the lack of readiness for future public health crises.
- Criticizes the focus on blaming China for past failures rather than addressing underlying issues.
- Points out the ulterior motives behind blaming other countries for failures.
- Emphasizes the need to increase the resiliency of medical infrastructure to prevent future crises.
- Suggests that expanding medical education and ensuring universal healthcare are key steps in improving resilience.
- Stresses the importance of increasing the capacity of medical infrastructure to handle future emergencies.
- Argues that a strong healthcare system is the only effective solution to prevent and contain public health crises.
- Condemns politicizing the issue of national security and public health for personal gains.
- Warns that without focusing on healthcare, the same crisis will happen again.
- Calls out the hypocrisy of prioritizing national security without addressing healthcare infrastructure.

### Quotes

- "They did this to us. Therefore, we don't really need to change because that's not going to happen again."
- "There is one solution to making sure we don't have this problem again, and that is to increase the resiliency of our medical infrastructure, period, full stop."
- "It is the only solution. There is no way to prevent and contain this in every situation."
- "If they want to talk about national security and they do not want to talk about increasing medical infrastructure, making sure everybody has medical care, they're lying to you."

### Oneliner

Beau raises concerns about blaming China for past failures instead of focusing on strengthening medical infrastructure to prevent future public health crises.

### Audience

Policy makers, healthcare advocates

### On-the-ground actions from transcript

- Advocate for increasing medical education and universal healthcare access (implied)
- Support initiatives that aim to strengthen medical infrastructure and capacity (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the failures in public health response and the importance of prioritizing healthcare infrastructure to prevent future crises.

### Tags

#PublicHealth #MedicalInfrastructure #Prevention #Healthcare #NationalSecurity


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about
why it's probably going to happen again
and we won't be ready for it next time either.
That's what we're going to talk about.
There's a push right now to lay the blame
for everything that happened over the last year on China.
Okay.
They're talking about whether or not it was a leak.
Okay, fine.
And then they're framing it as if that
this is the information they need
to make sure it doesn't happen again.
That's not true.
That is a flat-out lie.
They have no intention of making the changes necessary
to ensure this doesn't occur again. None.
If it is determined it came from a leak,
then, well, they'll say we need more funding
for our intelligence agencies
because we're obviously not spying on the Chinese enough.
And by will say, I mean they've already said this.
They will do everything except address the actual issues
that created the unmitigated failure
that was our public health response.
The reality here is that they want to blame China,
A, because it helps out in the near-peer propaganda,
and B, well, if we can blame another country,
then it doesn't undermine American exceptionalism.
It wasn't our fault.
It wasn't a failure of leadership.
It wasn't a lack of medical infrastructure.
It was those other people.
They did this to us.
Therefore, we don't really need to change
because that's not going to happen again.
Realistically, let's say they do confirm
and find out that it came from a leak over there.
What are they going to do?
Say, oh, you're going to have to pay for this.
Put some sanctions on them and tell them
to strengthen their security protocols,
which they already would have done
because they were impacted by it, too.
It's thoughts and prayers.
They are attempting to win the next war
by preparing for the last, and that doesn't do any good.
There will be another public health issue like this.
It's not a question of if.
It's a question of when.
There is one solution to making sure
we don't have this problem again,
and that is to increase the resiliency
of our medical infrastructure, period, full stop.
Nothing else is going to do anything.
Nothing else will change the outcome.
How do you do that?
Medical education, that would be a big one
so people understand the information
that's being passed along.
That they might do.
A little bit of public health messaging, stuff like that.
I could see that happening,
but the thing that actually matters
is they have to increase the capacity
of our medical infrastructure.
That's what they have to do to make sure
that it isn't quite as paralyzing for the nation.
How do you do that?
Increase usage.
Increase usage, you make sure everybody has health care.
So then everybody's going to the doctor.
Everybody's going to the hospital.
Normal checkup, stuff like that.
Then when something like this happens again,
not if, when, all those people stay home.
People for their normal checkups,
their normal routine medical stuff, they stay home.
But because the infrastructure is ready
to handle that capacity, they have the supplies they need
to deal with the emergent public health issue,
to deal with the next bug.
Just like happened this time, people would stay home,
which means they have more masks,
they have more PPE, they have more oxygen,
they have the stuff they need
because the load is typically higher.
That is the solution.
It is the only solution.
There is no way to prevent
and contain this in every situation.
That's not a real possibility.
You can mitigate, but you can't be certain.
So if they want to talk about national security,
they want to talk about making sure
it's not going to happen again,
there is only one solution, and that's health care.
Anything else is them politicizing it
and attempting to use it for their own ends.
That is the solution.
It's the only solution.
This will happen again.
There is no doubt about it.
It will occur again.
And I would point out that every opposition nation,
every opposition non-state actor in the world
just saw this country brought to its knees
by a bug that really,
when compared to weaponized stuff, wasn't that bad.
This was horrible.
More than half a million gone.
But when you're talking about something intentional,
it's not even in the same league.
If they want to talk about national security
and they do not want to talk about increasing
medical infrastructure, making sure everybody
has medical care, they're lying to you.
It is that simple, because that's the only solution
to the problem.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}