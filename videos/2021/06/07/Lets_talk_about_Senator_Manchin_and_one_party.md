---
title: Let's talk about Senator Manchin and one party....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=etSd4nE0xnE) |
| Published | 2021/06/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Manchin refuses to support the For the People Act or end the filibuster for voting protections.
- Beau compares Manchin's behavior to a Trump supporter for rejecting reality.
- Manchin believes partisan voting legislation will harm democracy, justifying his opposition to the For the People Act.
- Beau argues that American democracy is not a both sides issue and one party has shown intent to undermine it.
- Compromising on preserving democracy is aiding in its undermining, as one party clearly does not support it.
- Beau stresses that there can be no compromise on preserving Americans' voting rights.
- He points out that one party is actively working to undermine voting rights and American democracy.
- Beau criticizes the idea of bipartisan compromise when one party is actively against preserving democracy.
- He likens compromising on democracy to aiding in its erosion, drawing a clear line on preserving voting rights.
- Beau warns that without courage from senators like Manchin, the country may end up with one dominant party.

### Quotes

- "If your goal is to preserve American democracy and theirs is to undermine it, if you compromise, you are now assisting in undermining it."
- "You are either pregnant or you're not. There's not a lot of gray area here."
- "It doesn't matter how many stakes you throw to that tiger, it will not turn into a vegetarian."
- "There is one party responsible for this."
- "If the senators like Manchin and others of his sort do not find their courage, we will end up with one party in this country."

### Oneliner

Senator Manchin rejects voting protection acts, Beau argues compromising on democracy aids in its undermining by one party.

### Audience

Voters, democracy advocates

### On-the-ground actions from transcript

- Contact Senator Manchin to express support or opposition to his stance on voting protections (suggested)
- Join local democracy advocacy groups to actively work towards preserving voting rights (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Senator Manchin's stance on voting protections and the implications of compromising on democracy in the face of one party's efforts to undermine it.

### Tags

#SenatorManchin #VotingRights #Democracy #Compromise #PoliticalAnalysis


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about Senator Manchin
and his position when it comes to protecting the rights of Americans to vote.
He has flatly stated he will not support the For the People Act and that he's not going to support
weakening or ending the filibuster to get voting protections through.
I've sat on this a while. I tried to think about it from the position of someone like him.
Somebody who doesn't... not really a champion for progress. Somebody who is center-right,
who leans right. And while that was hard, there's one little passage in what he said
and what he's written that just keeps bouncing around in my head. Because he was called naive,
I don't think that's it. He's behaving like a Trump supporter in the sense that he is rejecting
objective reality. Things that we know to be true, he is assuming them to be false.
Things that we know to be false, he is assuming them to be true. He says,
I believe that partisan voting legislation will destroy the already weakening binds of our democracy.
And for that reason, I will vote against the For the People Act. Furthermore, I will not vote to
weaken or eliminate the filibuster. For as long as I have the privilege of being your U.S. Senator,
I will fight to represent the people of West Virginia, to seek bipartisan compromise,
no matter how difficult, and to develop the political bonds that end divisions and help
unite the country we love. This sounds great. This sounds great. But there is a fundamental
assumption here that is objectively false. This is not a both sides issue. This isn't
a bipartisan issue. You had one party that spread baseless claims in an attempt to undermine the
election. You had one party that pressured election officials to find the votes. You had one party
that tossed around the idea of martial law. You had one party that tried to use the courts to
overturn the election. You had one party that attempted to use force at the Capitol. You have
one party in state legislatures around the country attempting to undermine the right to vote,
attempting to make it harder to preserve, in their words, the purity of the ballot box,
the quality of the voters. They are openly saying that they don't support this goal.
For this to be a statement that makes any sense, both parties have to be equally committed
to preserving American democracy, to preserving the republic, to preserving the American experiment.
It is clear that one party does not. This isn't naive. It's delusional. This isn't reality.
They have shown over and over again that they have no interest in preserving American democracy.
That's clear. It's open. It's in the headlines. It's in their statements. You can't compromise
on this. If your goal is to preserve American democracy and theirs is to undermine it,
if you compromise, you are now assisting in undermining it. You're just not doing it to the
same degree. This isn't an issue that can be compromised on. You are either pregnant or you're
pregnant or you're not. This isn't a... there's not a lot of gray area here. You either want to
preserve the rights of Americans to vote or you don't. They do not and they have been very clear
about this. It doesn't matter how many stakes you throw to that tiger, it will not turn into
a vegetarian. There is one party responsible for this and if the senators like Manchin and others
of his sort do not find their courage, we will end up with one party in this country.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}