---
title: Let's talk about Trump vs McConnell in 2022....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=R97VXKae81M) |
| Published | 2021/06/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump is intervening in primaries related to the Senate even though he's out of office.
- McConnell is pursuing his own interests through a PAC called the Senate Leadership Fund, making determinations on who to endorse.
- McConnell's goal is to become Senate Majority Leader again, not necessarily to get rid of Trumpism.
- The PAC may support moderate candidates over Trump loyalists based solely on polling to further McConnell's interests in regaining power.
- McConnell's actions may set up a contentious relationship with Trump, leading to more spending by candidates during the primary season.
- The Republican establishment wants to make 2022 a referendum on President Biden, but Trump's interventions might shift the focus back to himself.

### Quotes

- "Trump is intervening in primaries related to the Senate even though he's out of office."
- "McConnell's goal is to become Senate Majority Leader again, not necessarily to get rid of Trumpism."
- "The Republican establishment wants to make 2022 a referendum on President Biden, but Trump's interventions might shift the focus back to himself."

### Oneliner

Trump's interventions in Senate primaries and McConnell's pursuit of power may reshape the political landscape, impacting the focus on President Biden in the upcoming elections.

### Audience

Political observers, voters

### On-the-ground actions from transcript

- Support and get involved with candidates in Senate primaries (implied)
- Stay informed and engaged in political developments to make informed decisions during elections (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the power struggle between Trump and McConnell post-Trump's presidency, offering insights into their motivations and potential impacts on the upcoming elections.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the Trump-McConnell power struggle because even though Trump
is out of office, it's continuing.
Trump has decided that he is going to intervene in the primaries, particularly those related
to the Senate.
McConnell is going to do what McConnell always does, which is pursue his own interests at
the expense of, well, everybody.
There is a PAC that is closely linked to McConnell called the Senate Leadership Fund.
They have recently said that they are going to make their own determinations as far as
who to endorse and help during the primary process.
What this means is those candidates who may be moderate in comparison to Trump loyalists,
they'll get backing from McConnell if they're most likely to win because McConnell's goal
here is to become Senate Majority Leader again.
That's what's in his interests.
He doesn't really have a vested interest in getting rid of Trumpism.
He just wants to win, he wants that power.
So in some cases he may back a Trump loyalist, but in others that PAC may oppose him, may
back a moderate, which is going to force these candidates to spend money during the primary
season.
It also might help to set up an even more contentious relationship between McConnell
and Trump, which is always entertaining.
Recently McConnell was asked directly, point blank, how do you feel about Trump intervening
in the primaries?
And he said that Trump has his own agenda, which is true, but so does McConnell.
When you see McConnell and this PAC begin to support moderates, understand it's based
solely on polling, they haven't suddenly found an ideological backbone.
They're just doing what's in the best interest of themselves, and for McConnell that's regaining
power.
The Trump interventions in the primaries may be more beneficial for Democrats than they
might come to realize.
The Republican establishment wants to basically make 2022 a referendum on President Biden.
How is Biden doing?
Look he hasn't been able to get anything done because we've been stopping it all.
That's what they want the debate and discussion to be.
If Trump interjects himself in 2022 and begins airing old grievances and talking about the
last election and doing everything that we know he's probably going to do, it's going
to make it hard to make the midterms a referendum on Biden.
It's going to be yet another referendum on Trump, which will probably swing in favor
of the Democrats.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}