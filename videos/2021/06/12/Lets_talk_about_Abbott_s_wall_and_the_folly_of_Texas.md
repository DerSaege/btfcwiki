---
title: Let's talk about Abbott's wall and the folly of Texas....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=C57ci3F0l8s) |
| Published | 2021/06/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor Abbott's promise to build a wall in Texas is more about politics than actual border security.
- Walls have historically proven to be ineffective as people find ways to bypass them.
- Abbott's focus on building a wall is a political strategy to deflect blame onto others.
- Despite Texas facing infrastructure issues like poor roads and levees, Abbott's priority is on the wall.
- Texas ranks poorly in healthcare access, quality, education, air and water quality, and economic opportunities despite having a strong economy.
- The real issue in Texas lies with those in power who have rigged the system for themselves, not with people crossing the border.
- Building a wall is just a distraction tactic to shift blame away from the failures of those in government.
- Walls throughout history have ultimately failed as they can always be overcome.
- The wall proposed by Abbott is a wasteful vanity project that will not address the underlying issues faced by Texans.

### Quotes

- "Walls have been proven to be historically kind of ineffective."
- "It's a waste of money and it's just a vanity project for another authoritarian goon."

### Oneliner

Governor Abbott's plan to build a wall in Texas is a political ploy that won't address the real issues faced by Texans, shifting blame onto marginalized groups.

### Audience

Texans, Activists

### On-the-ground actions from transcript

- Advocate for better infrastructure projects in Texas (implied)
- Support policies that address healthcare, education, and economic opportunities in Texas (implied)

### Whats missing in summary

The full transcript provides more in-depth analysis on the failures of those in power in Texas and the impact on its residents.

### Tags

#GovernorAbbott #BorderWall #Texas #Politics #Infrastructure #BlameGame


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about Governor Abbott
out there in Texas, his new promise.
They're going to build a wall.
They're going to build a wall.
And I know there's the standard questions.
You know, hey, you know, walls have
been proven to be historically kind of ineffective.
People just go over or under or through or around.
And especially given the fact that there's no way
they're actually going to be able to secure the land
to do the whole border, it just doesn't make any sense.
And even if they were able to do the whole border of Texas,
they would just go one state over or use the Gulf of Mexico.
Yeah, those are all real arguments.
But the reality is Governor Abbott is not
doing this because he cares.
It's a political talking point.
It's red meat for the base.
So they can continue to say, oh, it's those people's fault.
It's their fault.
So let's go over some stuff, because Texas
is a really good state to prove this with,
to demonstrate how those in power
like to blame people with absolutely no power.
All right, let's first, let's address the obvious
infrastructure thing.
Yeah, infrastructure is good.
A wall is not really the best type of infrastructure project,
especially in Texas, where the American Society
of Civil Engineers, they rated roads, levees, waterways,
and dams as a D. On the upside, Texas
did manage to get an A in absolutely nothing.
There's probably better uses of this money.
In fact, the best grade Texas got was in energy with a B plus.
But we're talking about the energy system that
failed when it got chilly because those in power
chose not to winterize the grid.
And it's not for lack of money.
Overall, Texas has the ninth strongest economy in the US
as far as states go.
But what does that translate into for the people of Texas?
How's their health care?
Health care access, they're 45th.
Health care quality, they're 39th.
Basic education, they're 35th.
Air, water quality, 42nd.
Pollution, 40th.
And here's the one that you should pay attention to.
Overall economic health, ninth.
Economic opportunity, 40th.
The state is making plenty of money.
It's just all going up.
Well, let me tell you, if you live in Texas
and this is a concern for you, the people crossing the border,
they have nothing to do with that.
It's the people trying to blame them.
The people saying, hey, look over there.
They're the source of your problem.
No, they're not.
Texas is a wealthy state.
It has money flowing through it.
But they're 40th in economic opportunity
because those in power have rigged the game for themselves.
That's why Abbott wants to build a wall.
It's just a talking point.
It's just something to motivate the base
to get them looking at somebody else to blame.
Because it can't be their fault. It
can't be those people in government's fault.
It's got to be somebody else, somebody
who is lesser. This wall, like all others in history,
will fail.
They always do.
Historically speaking, walls don't actually
succeed because fixed fortification
are a monument to the stupidity of man.
They can always be defeated.
And they will be.
It's a waste of money.
And it's just a vanity project for another authoritarian goon.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}