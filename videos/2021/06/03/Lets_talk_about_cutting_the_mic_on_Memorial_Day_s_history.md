---
title: Let's talk about cutting the mic on Memorial Day's history....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8WGB2xRmlkE) |
| Published | 2021/06/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The lieutenant colonel's mic was cut off during a speech about the origins of Memorial Day, specifically the first observance.
- The first national observance of Memorial Day was in Arlington on May 30, 1868, but the origins go back further to Charleston in 1865.
- Newly freed slaves in Charleston buried Union troops with proper honors and held a parade on May 1, 1865, a year before other observances.
- A Civil War Historical Society in 1916 tried to inquire about this event in Charleston but were met with denial from the Charleston Historical Society.
- The official birthplace designation of Memorial Day in Arlington was made in 1966 for political reasons with little real scholarship.
- The events in Charleston may not have definitively led to Memorial Day observances, but it's hard not to see the connection in spirit.
- Many national holidays have origins that are not widely known or admitted, similar to the unique beginnings of Memorial Day in Charleston.

### Quotes

- "Memorial Day began where the war began in Charleston."
- "It's incredibly hard not to draw that line."
- "The spirit was there."
- "I challenge anybody to find an event that is similar in nature prior to the one in Charleston."
- "You may not be able to draw that direct line via historical standards, but the spirit was there."

### Oneliner

A lieutenant colonel silenced for sharing the untold origins of Memorial Day that began with newly freed slaves honoring fallen troops in Charleston, challenging historical narratives.

### Audience

History enthusiasts, Memorial Day advocates, Civil War scholars

### On-the-ground actions from transcript

- Research and uncover hidden historical narratives (suggested)
- Share stories and histories that challenge traditional narratives (exemplified)

### Whats missing in summary

The emotional impact of uncovering hidden histories and the importance of acknowledging marginalized voices in shaping national observances.

### Tags

#MemorialDay #HiddenHistory #Charleston #CivilWar #HistoricalNarratives


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about a history lesson,
talk a little bit about Memorial Day,
and a lieutenant colonel getting his mic shut off
in the middle of a speech because he was going
to provide a history lesson about what is widely
believed to be the first observance of Memorial Day.
But it's a story that, well, I guess there's some people
don't really want it told.
So we're going to make sure we tell it today.
OK, so the first official national observance
of Memorial Day took place in Arlington on May 30, 1868.
Prior to this point, there was a decoration day,
is what it was called.
A whole bunch of different towns had these days
where they would go out and decorate the burial sites
of fallen troops.
Then in the late 90s, maybe early 90s,
a professor from Yale, Blythe, I think, Blythe,
he was researching a book on the Civil War.
And he found this box.
And it said, first decoration day.
OK, and he starts looking through it.
The thing is, it was from 1865.
And that's a year before any of the other ones.
It was in Charleston, a place that
didn't claim to have the first one, which was odd.
So this is what happened.
There was a racetrack in Charleston during the war.
It was turned into a prisoner of war camp.
Whole bunch of Union troops were lost there, 260 of them.
And they were all buried together.
Like right after the war ended, a bunch of newly freed slaves
decided to give them a proper send off.
They dug them up and buried them separately, properly.
Put flowers on their graves.
And then on May 1st, well, they had a parade.
And they sang John Brown's Body.
It's kind of fitting to think that Memorial Day began
where the war began in Charleston.
And the first people to observe it were newly freed slaves.
But this isn't a story that a lot of people
want told, because we do like to keep our history a certain tone,
keep our history a certain tone, I guess.
Interestingly enough, this lieutenant colonel
who decides to say this, he decides
to basically provide this little lesson during Memorial Day
observance in Hudson, Ohio.
They said they cut his mic because it didn't have anything
to do with Hudson.
That's not true.
John Brown, the song John Brown's Body,
he was born in Hudson.
It completely connects.
That's just an excuse.
That doesn't even make sense.
It's just that in the United States,
we like to cut the mic on stories we don't like.
And there's an interesting footnote to this.
In 1916, a Civil War Historical Society out of New Orleans,
they heard about this parade.
They heard about this thing.
And they sent a letter to Charleston asking about it,
to the Charleston Historical Society.
And like, oh, no, dear, we don't know what you're talking about.
We never heard of such a thing, that good Charleston accent.
The mic got cut on this story right from the beginning.
It wasn't in the interest of the people in Charleston
to have a story that showed exactly how
happy the newly freed slaves were.
And so it disappeared.
Now, there are people who argue this.
The official birthplace is in this location.
Yeah, that designation was made in 1966 by Nixon.
It was a completely political thing.
Had nothing to do with a whole lot of real scholarship.
Can we say definitively that this event in Charleston
led directly to the Memorial Day observances that we have now?
No.
I mean, not really.
We don't know that for certain.
But it's incredibly hard not to draw that line.
Because it's the parade decorating the sites.
It is the origin of Memorial Day,
whether you want to admit it or not.
A lot of national days, holidays,
they have origins that people don't want to admit.
I'm certain that there's a whole lot of people
that don't want to look into why Easter is celebrated
with an egg and a rabbit.
It may not fit your image of what Memorial Day was.
But I find it very hard to believe
that this event in Charleston did not, at the very least,
inspire the events that then became the basis
for Memorial Day.
If you're talking about the origin, yeah, this is it.
If you're talking about the inspiration, the beginning of it,
where it started, yeah.
You may not be able to draw that direct line
via historical standards of having correspondence
between people who were at one event
and then started the other or anything like that.
But the spirit was there.
And I challenge anybody to find an event that
is similar in nature prior to the one in Charleston.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}