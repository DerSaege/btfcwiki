---
title: Let's talk about the San Jose footage....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gvJjyggOcZA) |
| Published | 2021/06/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the slow and deliberate approach taken by officers in San Jose during a shooting incident.
- Mentions that the officers were not well-trained in room clearing, leading to some cringe-worthy moments.
- Points out that the officers from different departments had not trained together, causing some disorganization.
- Acknowledges that despite some mistakes, the officers managed to clear the building without hurting anyone innocent.
- Expresses satisfaction with law enforcement's actions, stating that moving in when they did likely saved lives.
- States that while there were imperfections, the overall outcome was successful and no innocent individuals were harmed.

### Quotes

- "If you're watching that footage, when the supervisor comes out, the person they got the key card from, had they been moving quickly, they might have hurt that person by accident."
- "At the end of the day, it is almost certain that them choosing to move in when they did saved lives."
- "Wasn't perfect. At times, it was kind of ugly. But it worked."
- "And nobody got hurt."
- "Overall, I think they did fine."

### Oneliner

Beau explains the slow and deliberate approach taken by officers in San Jose, acknowledging mistakes but ultimately recognizing their success in ensuring no harm to civilians.

### Audience

Law enforcement personnel

### On-the-ground actions from transcript

- Analyze and improve inter-departmental coordination and training (implied)
- Recognize the importance of ensuring minimal risk to civilians during law enforcement operations (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of law enforcement tactics during a critical incident and underscores the importance of prioritizing civilian safety in such situations.

### Tags

#LawEnforcement #Tactics #CivilianSafety #CriticalIncident #InterdepartmentalCoordination


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about San Jose
and what went down there now that the footage has
been released.
We're going to do this because I've got a bunch of questions
about it.
One is, why were they moving so slowly?
A whole bunch of people asked that.
And then another one that I'm going to answer simply
because it'll allow me to answer a whole bunch of stuff at once
is, I watched the footage with my dad, who
was a ranger in Iraq.
And he said that it was the worst room clearing he
had ever seen in his life.
I think that's a little bit of an exaggeration.
It probably wasn't the worst he'd ever seen in his life.
But they did do a few things that if you're
very well-trained in room clearing,
that yeah, you probably cringed a little bit.
OK, so if you don't know what I'm talking about,
the footage from the shooting out in San Jose,
when the cops showed up and made entry,
the body cam footage had been released.
OK, so why were they moving so slowly?
What people are used to seeing in movies is dynamic entry.
And you're moving at a very quick pace doing that.
The opposite end of the spectrum is called slow and deliberate.
That's what they were doing.
And they did it pretty well, despite what's about to come up.
If you're not incredibly well-trained,
slow and deliberate is definitely the right move.
If you're watching that footage, when the supervisor comes out,
the person they got the key card from,
had they been moving quickly, they
might have hurt that person by accident.
So that's why they were moving slowly.
I think that was probably the right move on their part.
Now, as far as Ranger Dad, when it
comes to securing, taking down and securing a large facility,
that's like Ranger's thing.
Like, that's what they do.
So your dad's grading this at PhD level.
And that's not a fair standard for the patrol officers.
It did appear unorganized, because it was.
It was.
Those officers were from different departments.
Odds are they've never even met, much less trained together.
So it's not realistic to expect them to function
as if they were our team, because they weren't.
Those were five cops, the first five that showed up.
And they were like, hey, we're a SWAT team now.
They formed a contact team.
They moved inside.
They were slow and deliberate.
They more or less cleared things as they went through.
And they kept pressure up on the bad guy
until the threat was over.
Didn't hurt anybody along the way.
To me, that's a win.
That is a win.
Yeah, there are a couple of moments where you're like,
oh, don't do that.
Please never do that again.
But generally speaking, you don't want to announce
that you're about to open the door really loudly,
because that might notify the person
on the other side of the door if they're there.
You don't want to point with your firing hand.
There were quite a few things that aren't excellent.
But as far as the goal they set out to accomplish,
the way they did it, I don't have any problems with it.
The mistakes that they made were mistakes that put them at risk,
not the civilians, not other people in the building.
And if you're going to make a mistake,
that's the kind you want to make.
I am somebody who will rip apart police tactics at every turn.
Yeah, it wasn't great as far as the finer points of this.
But they accomplished it.
They did it safely.
They didn't hurt anybody innocent along the way.
And more or less, they made the right decisions
and stuck to the right tactics for the situation.
So I don't think that we should really rip it apart,
because the finer points, they're
never going to get those.
They're never going to have people come out
and train them on that.
Overall, I think they did fine.
And at the end of the day, it is almost certainty
that them choosing to move in when they did saved lives.
So again, it's start to finish.
This is what we want from law enforcement
in a situation like this.
Wasn't perfect.
At times, it was kind of ugly.
But it worked.
And nobody got hurt.
And I didn't see anything that they did that would have
led to somebody getting hurt.
So I'm OK with it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}