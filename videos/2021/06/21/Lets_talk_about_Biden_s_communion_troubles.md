---
title: Let's talk about Biden's communion troubles....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lvyl9qOiUKI) |
| Published | 2021/06/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau was called out for not discussing the church denying communion to Biden, being reminded of his unique approach to talking about religion without ridiculing religious people.
- Beau typically speaks out against churches when they preach harm or attempt to use state power to enforce beliefs, supporting the separation of church and state.
- Regarding the church denying communion to Biden, Beau believes it is an internal church matter that doesn't involve the government.
- Beau expresses that he wouldn't argue for a religious institution to provide a religious right to a government official like President Biden, as it could violate the separation of church and state.
- While Beau sees the denial of communion to Biden as counter to the idea of communion itself, he defers to Reverend Ed Trevers for a theological perspective.
- Beau criticizes the church's move politically, believing it will alienate members and set a bad precedent for the church to make more political stances.
- He points out the hypocrisy in the church's decision, suggesting it might drive members away and encourage conservative demands for political involvement.
- Beau views the church's decision as a political stunt that will lead to less support and cause doubt among members.
- Despite his disagreement with the church's move, Beau concludes that it's not something he typically speaks out against since it's contained within the church and doesn't involve enforcing beliefs through state violence.
- Beau shares his thoughts and wishes his audience a good day.

### Quotes

- "That's not my business."
- "I don't think it's a good move politically."
- "It's a political stunt, in my opinion."

### Oneliner

Beau believes the church denying communion to Biden is an internal matter, critiquing it politically while maintaining it's not his usual cause for speaking out against churches.

### Audience

Religious commentators

### On-the-ground actions from transcript

- Watch Reverend Ed Trevers' video for a theological perspective on the situation (Suggested)
- Encourage open dialogues within religious communities about the intersection of religion and politics (Implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on the church denying communion to Biden, discussing the implications within the church and politically with a focus on separation of church and state.

### Tags

#Church #Religion #SeparationOfChurchAndState #PoliticalStance #Hypocrisy #CommunityActions


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Biden's latest obstacle,
I guess, where I do this, because I got a message
and I was informed of a new duty that I have apparently.
It says, Beau, I am disappointed and surprised
to see that you haven't talked about the church
denying communion to Biden.
You have mixed spirituality,
but you have consistently taken churches to task
when they step outside of their lane.
As one of the only people on the left
who will talk about religion
without making fun of religious people,
it is your duty to comment on this.
Also ask if I'm Catholic.
Okay, so I have talked about church and religion
in the past, I do it in two instances mainly.
One, when a church is preaching something
that is active harm,
and very contradictory to their doctrine.
I've done it then.
And then the other is when the church
attempts to use the power and violence of the state
to enforce its beliefs,
because I'm a big supporter
of the separation of church and state.
I believe that hedge should be wide.
State can't reach into the church's yard,
the church can't reach into the state's yard.
The two things should remain separate.
In this case, the church is acting wholly within the church.
This is a thing between the church and its members.
In this case, Biden.
It isn't attempting to create law.
Now, the argument can be made
that this is a political move
designed to influence Biden
and therefore influence law.
And yeah, that can be made.
But at this point,
this is a matter that is completely internal to the church.
It is the church refusing to provide a religious right
to one of its members.
That's not my business.
Plain and simple, that's not my business.
I would also point out
that you will never see me argue
that a religious institution has to provide a religious right
to an agent of government.
In this case, President Biden.
It's impossible to separate the man from the position.
He is a representative of government.
Forcing a religious institution
to provide him with a religious right,
to me, would be a violation
of the separation of church and state.
So from the standpoints
that I normally talk about something,
it doesn't have anything to do with it.
It doesn't have anything to do with it.
That being said,
since there's obviously a desire for an opinion here,
I do think it's counter to the idea of communion
and what communion is.
If you want to look at it from a theological standpoint,
I'm going to defer to Reverend Ed Trevers.
I'll put a video from him down below.
He put it out within the last 24 hours
and can explain it far better than I can.
He's an Anglican priest.
It's kind of like his thing.
So that side of it.
Now from the political aspects of this,
because the church has entered politics with this move,
I think it's a horrible move politically.
It doesn't even make sense.
Basically, the church, I guess two-thirds
of the leadership of the church, made this decision.
I don't believe it's going to be reflected
by the overall membership of their church.
That doesn't seem likely to me.
It also sets a bad precedent
because now conservative members of the church
will demand that the church make other political stances.
That's going to push more and more people away from the church
because the country is getting more liberal.
Aside from that, while I don't like whataboutisms,
that's not an argument you can make in politics typically.
When you're talking about a church,
that hypocrisy is a big part of it.
And given the fact that this prohibition against communion
is kind of tailor-made to certain people,
it exposes that hypocrisy and it makes it appear
as though some people may have put a political party over their God.
Sure, it's a basic tenet of the belief system.
Absolutely.
So is taking care of the needy.
But there's not a prohibition for politicians
who vote against that consistently.
I'm fairly certain that capital penalties
are against the belief system.
But there's no like prohibition there either.
That hypocrisy, real or perceived,
is going to drive members away from the church
while simultaneously encouraging conservative members
to make more and more demands of the church
to get involved and make political statements like this.
I don't think it's a good move politically.
And that's how you're going to have to view it
when you're talking about the actual impacts in the country.
What happens inside the church, that's their business.
From the outside, yeah, it's a political move,
but it's ineffective and self-defeating.
I don't think it's going to go the way they plan.
It's a political stunt, in my opinion.
And it leads to less support for the church.
Because eventually, they're probably going to have to change this position
or add new ones.
But when you're talking about the idea that it's based on an entity
that is infallible, when positions change
and it doesn't flow with the idea,
the theological idea of communion,
it causes doubt.
Which is not something a church generally wants to encourage.
So I think it was a bad move.
But as far as the reasons I would normally talk about a church doing something,
this doesn't do it.
It's not an active attempt to get the violence of the state to enforce their will.
And it's not preaching something that's an act of harm.
There's a big difference between this and the pastors
who wanted to organize the armed march to scare gay people.
It's not the same.
This is something that is wholly contained within their church
and therefore isn't my business.
And it's not the government's business.
So, anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}