---
title: Let's talk about my uncle, a storyteller....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BIIkc5PRqAo) |
| Published | 2021/06/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Introduces the topic about his uncle, a storyteller, who taught valuable lessons through his experiences.
- Shares how his uncle lost his finger before he was born, with varying stories about how it happened.
- Mentions that his uncle always had a story to tell and each story had a point or lesson to impart.
- Talks about learning about his family's native traditions and how to run irrigation from his uncle.
- Describes his uncle as an adventurer who served in the Navy and briefly expatriated to the Philippines.
- Mentions his uncle's three daughters and how every interaction with him was a learning experience.
- Expresses the loss of his uncle to COVID over the weekend.
- Urges people to get vaccinated, stating that misinformation is discouraging people from getting vaccinated.
- Emphasizes the importance of vaccination to prevent further loss of lives due to COVID.
- Encourages asking questions about vaccination and seeking answers to address concerns.
- Stresses the reality of the ongoing situation and the necessity for vaccination to save lives.
- Shares his personal experience of getting vaccinated and the minimal discomfort involved.
- Advises scheduling a vaccination appointment and taking the necessary step to protect oneself and others.

### Quotes

- "Everything was a learning experience. Everything."
- "We lost him over the weekend to COVID."
- "There's no reason for it to be you. Go get vaccinated."
- "It doesn't take long. It's pretty, well, I don't want to say it's painless."
- "Y'all have a good day."

### Oneliner

Beau shares lessons learned from his storytelling uncle, urging vaccination to prevent further loss to COVID misinformation.

### Audience

Individuals, Vaccine-Hesitant

### On-the-ground actions from transcript

- Get vaccinated to protect yourself and others (implied)

### Whats missing in summary

Personal anecdotes and emotional connections with Beau's uncle that can be best appreciated in the full transcript.

### Tags

#Family #Storytelling #COVID19 #Vaccination #LessonsLearned


## Transcript
Well, howdy there, Internet people. It's Beau again.
So, today we're going to talk about my uncle and what we can learn from him.
And it's kind of fitting because he's very much a storyteller.
Everything was a learning experience. Everything.
A moment to impart some kind of knowledge.
Funny guy, too.
Sometime before I was born, he lost his finger.
Anytime you'd ask him, he would tell you a different story.
Alligator, rabbit, raccoon, air conditioner.
To this day, I have no idea how he lost it.
But there was always a story.
And it always had a point.
He listened to everything that went on around him.
I wound up learning more
about the ways and traditions of the native side of my family from him
than I did just about anybody else, because he paid attention.
Also learned how to run irrigation from him.
Now, at the same time, he was using me as child labor,
but because of that, I wound up learning a whole lot
about how to run irrigation.
Very much an adventurer in a lot of ways.
He was in the Navy.
I know he took off to the Philippines to expatriate.
And I also know he came back quickly,
because he had an issue with some of the local elements there,
and he needed to get on a plane.
Three daughters, two my age, roughly,
and one that's like 11 or 12.
Very, very interesting man.
But again, everything with him was a learning experience.
I learned a lot from him over the years,
and I'm hoping today you can too.
We lost him over the weekend to COVID.
It's not over.
Go get vaccinated.
If you have questions, ask them in the comments section.
There will be people who will direct you to the answers to your questions.
Most of the stuff out there that is kind of discouraging people
from getting vaccinated, well, it's not grounded in reality.
This will go on.
We will continue to lose people until we reach that threshold.
And even then, there will still be some.
There's no reason for it to be you.
Go get vaccinated.
It doesn't take long.
It's pretty, well, I don't want to say it's painless,
because that second shot, yeah, it hurt my arm.
But it's over with pretty quickly.
So it might be worth taking a moment, scheduling the appointment,
and going and doing what eventually you're going to need to do anyway.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}