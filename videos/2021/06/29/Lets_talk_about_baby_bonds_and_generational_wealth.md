---
title: Let's talk about baby bonds and generational wealth....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=X48D_RbFPa4) |
| Published | 2021/06/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the concept of baby bonds, which involves depositing $3,200 into an account for every child born into poverty.
- Describes baby bonds as a substitute for generational wealth that disadvantaged individuals lack access to.
- Links the lack of generational wealth to limited opportunities and the perpetuation of poverty cycles.
- Connects the concept of generational wealth to the topic of reparations for descendants of slaves.
- Emphasizes the economic benefits of baby bonds, suggesting that the invested money will circulate in the economy and eventually pay for itself.
- Mentions the Americans Opportunities Accounts Act as a federal counterpart to baby bonds but acknowledges the potential challenges in its implementation.
- Argues that investing in initiatives like baby bonds is not just morally right but also economically beneficial for society as a whole.

### Quotes

- "It's cold hard cash."
- "It's a concept that we've talked about before. That's the money that your family has accumulated over the years."
- "It's worth noting there is a federal counterpart to this concept."
- "It's better economically to do the right thing."
- "It is cheaper to be a good person, almost always."

### Oneliner

Beau explains baby bonds, a cash initiative for children in poverty that aims to break cycles of generational wealth inequality and poverty, while noting its economic benefits and mentioning a federal counterpart.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Advocate for the implementation of baby bonds at a local level (suggested).
- Support initiatives that aim to break the cycle of poverty through economic empowerment (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of baby bonds, linking them to generational wealth, poverty cycles, reparations, and economic benefits, offering a comprehensive perspective on addressing inequality.

### Tags

#BabyBonds #GenerationalWealth #PovertyCycle #Reparations #EconomicEmpowerment


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about baby bonds.
Yeah, I didn't know what it was either.
It's cash.
It's cold hard cash.
The state of Connecticut has a plan.
And the idea is to drop $3,200 into an account
for every child that is born into poverty.
And they're going to define that by whether or not
the child will qualify for Medicaid,
or whatever they call it up there.
So when the child is born, they get $3,200
put into a savings account.
They can access it when they turn 18.
At this point, it will be $10,000 or $11,000.
Now this is a ready substitute for generational wealth.
It's a concept that we've talked about before.
That's the money that your family
has accumulated over the years.
You know, your rich aunt, your uncle, your grandpa,
whoever, who you can call up and say, hey, blew out my tire
and don't have a spare.
I need to borrow a couple hundred bucks.
Or, hey, I want to start a business.
I need to borrow some real money.
That's generational wealth.
If you are in poverty, you don't have access to that.
Which means you are limited in opportunities, which
means you may not be able to get out.
This is why we talk about the cycle of poverty.
We've talked about generational wealth
before on this channel when we're talking about reparations.
Because you have the moral argument
that you can make about anything, right and wrong.
The problem is that when it comes to politics,
Americans will always do the right thing as soon as they've
tried everything else.
So you need to have that pragmatic argument
in your back pocket.
One of the strongest I have found for reparations
is the idea that it would create generational wealth.
A lot of the descendants of slaves,
well, because of the economic policies,
meaning slavery and then into segregation,
they haven't had the opportunity to generate
that generational wealth.
Because as the name suggests, it might
take generations to accumulate.
And this speeds up that process.
Now I'm certain that Connecticut has run the numbers.
And Connecticut understands that this money going
into the economy being spent on a college education, trade
school, starting a business, whatever,
is going to generate economic activity.
And it's going to end up paying for itself.
This is the kind of argument you can use
when the moral arguments fail.
When you can't reach somebody on the level of right and wrong.
A pill to, well, in the United States, a pill to money.
It's better economically to do the right thing.
We've said it before.
It is cheaper to be a good person, almost always.
It's worth noting there is a federal counterpart
to this concept.
It's called the Americans Opportunity Act,
Americans Opportunity Accounts Act.
That's probably going to be a hard sell.
Probably going to be something that's
going to be difficult to accomplish.
And there will undoubtedly be a lot of discussion about it.
That's an argument you may want to keep in your back pocket.
That at the end of the day, this money, not just
does it help people escape poverty and get out
of that cycle and break that cycle, which
is a good moral argument, the increased economic activity
is good for everybody.
And it'll end up paying for itself.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}