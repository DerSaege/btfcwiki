---
title: Let's talk about the South Dakota National Guard and the missing point....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tEHM0wYZRpw) |
| Published | 2021/06/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Governor of South Dakota sending 50 National Guard troops to the southern border, funded by private donation.
- Questions raised about the legality of using private funds for National Guard deployment.
- Beau questions the purpose and effectiveness of deploying only 50 troops to secure the border.
- Raises the issue of National Guard members signing up to help their community, not for political stunts.
- Points out the difference in numbers between National Guard troops and Border Patrol agents.
- Criticizes the deployment as a political stunt rather than an effective security measure.
- Warns that governors involved in this deployment may not be fit for the presidency.
- Emphasizes that deploying troops for political gain sets a dangerous precedent.
- Condemns the deployment as a misuse of trust and resources for political purposes.
- Urges people to recognize the true intentions behind such deployments.

### Quotes

- "It's a joke."
- "It's politics and nothing more."
- "Any governor who participates in this can never be president."
- "They are showing you who they are right now."
- "It's a joke. It's a joke."

### Oneliner

Governors deploying National Guard troops for political stunts instead of real security risk their credibility and the lives of service members.

### Audience

Governors, Voters, Activists

### On-the-ground actions from transcript

- Call out misuse of National Guard for political gain (exemplified)
- Advocate for ethical deployment of National Guard troops (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the questionable deployment of National Guard troops for political purposes and raises concerns about the misuse of resources and trust.

### Tags

#NationalGuard #PoliticalStunt #BorderSecurity #Governors #MisuseOfResources


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about the National Guard.
We're going to talk about the National Guard and that deployment down to the border.
A lot of people are talking about it, and most of them are focused on the whole
it's funded by a private donor thing and wondering if that's even legal.
If you don't know what I'm talking about, the governor of South Dakota
has decided to send 50 National Guard troops to the southern border
to assist in securing the border.
And it's being paid for by private donation.
There are people who are wondering if that's legal.
I'll be honest, I have no clue. I have no idea if that's legal.
I would be surprised if Congress had taken the time to say you can't do that.
Because up until a few years ago, this was unimaginable.
And not just in the way people are thinking.
There's something else, and there's an entirely different conversation
that I think needs to be happening.
Sure, definitely talk about the legalities of a private citizen paying
to deploy the National Guard.
Definitely think about the legalities of that.
Definitely talk about it.
If it is legal, I would suggest it needs to be remedied.
But there's another conversation that needs to happen.
Why do people sign up for the National Guard?
What are they envisioning when they walk in and they sign that contract?
What are they picturing in their mind?
What are they trusting their state, their governor, to use them for?
They walk in there and they sign up, and it's basically, yeah,
one week in a month, two weeks a year.
I'll stay sharp enough so if there's a hurricane, flood, tornado, earthquake,
something like that, I got you.
I'll show up and I will help my community.
I will help my state.
That's why they sign up.
That's what they're envisioning.
Now they may sign up for financial reasons or, you know,
to finish out a term or something like that.
But that's how they're picturing they'd be used.
That's what they're trusting that they would be used for.
I don't think many of them sign up envisioning being deployed
to the southern border for a completely ineffective political stunt simply
because some rich dude paid for it.
I know what you're saying.
How do you know it's going to be ineffective?
Because I can add.
I know how numbers work.
We've talked time and time again on this channel about how many troops it takes to actually
accomplish something.
Fifty, huh?
Fifty?
What are they going to do, secure a Walmart?
Fifty troops.
Okay.
We're not even going to talk about ICE.
Not going to talk about state law enforcement.
Not going to talk about local cops, all of whom have some responsibility.
We're just going to talk about Border Patrol.
16,000.
There are 16,000 on the southern border.
But those 50 National Guard troopers, that's going to turn the tide, right?
No.
And they don't expect it to.
It's a political stunt.
It's pure politics.
It's posturing.
It's not designed to be effective.
It's a joke.
It's something to invigorate their base, to cast Biden in a bad light.
It's politics and nothing more.
It won't be effective and it's not designed to be.
It's a joke.
Here's the thing about that.
Any governor who participates in this can never be president.
They can never be president.
They can never have the ability to send troops into combat for the same reasons.
This sure, sure, it's a joke.
It is a political stunt that is only going to resonate with the least informed people.
Absolutely.
But what if the stakes are higher?
Any governor who is sending troops down for this can never be president because if they
get in a scandal, they will absolutely deploy US troops into combat to change the story
or politics because that's what this is.
These governors, they think absolutely nothing of taking people away from their families,
abusing that trust and sending them down there for a joke, for a political stunt.
Doesn't even cross their mind because it's just about politics.
No governor that was involved in this can ever be president.
The American people need to recognize that they are showing you who they are right now.
If anybody who participates in this little show becomes president, understand you will
fill bags over politics.
Not any stated foreign policy goal, not even a bad foreign policy goal, just to secure
their own careers, just to secure their own political futures.
If they start falling in the polls, send in the troops.
That needs to be talked about.
This is not going to be effective and it's not designed to be effective.
50 troops.
It's a joke.
It's a joke.
It's not even a company.
Doesn't raise the available manpower down there by a single percent.
It's a joke.
This time they're not at risk.
You put one of these governors in the Oval Office, some of them will come back in bags
next time it happens.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}