---
title: Let's talk about General Milley responding to congress....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tyF1P9-4ZdU) |
| Published | 2021/06/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing General Miley's exchange with Congress.
- Importance of diverse backgrounds within special forces.
- Emphasizing the value of education for warriors.
- Learning about different things to be a better warrior.
- Rejecting the notion that learning is bad.
- Sign of intelligence is entertaining ideas without accepting them as facts.
- Criticizing politicians dictating military education.
- Green Berets needing to understand different cultures.
- Critical race theory's potential applicability.
- Distinguishing education from indoctrination.
- Demonization of education and intellectualism in the US.
- People manipulated by trivial distractions from real issues.
- Politicians influencing what is taught is dangerous.
- Indoctrination leads to inability to discern fact from fiction.

### Quotes

- "No education is ever wasted."
- "The true sign of intelligence is being able to entertain an idea without accepting it as fact."
- "Being educated about something is not indoctrination."
- "Politicians determining what you can learn is a dangerous concept."

### Oneliner

Beau breaks down the importance of education for warriors and criticizes politicians dictating military learning to combat indoctrination.

### Audience

Educators, policymakers, activists.

### On-the-ground actions from transcript

- Support educational programs for military personnel (suggested).
- Advocate for diverse educational curriculums in the military (suggested).
- Combat misinformation by promoting critical thinking skills (suggested).

### Whats missing in summary

Importance of challenging indoctrination and promoting critical thinking skills to combat manipulation and misinformation.

### Tags

#Education #Warriors #Indoctrination #CriticalThinking #Military


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about that exchange
between General Miley and some people in Congress.
There's a whole bunch to talk about from that very
short conversation.
I was actually in the middle of filming a video explaining
the context of that line, you and I are both Green Berets,
and what that really meant.
And something else popped into my head,
and that seems way more important than explaining
a really good insult.
I know a lot of special people, special forces,
special operations community in general, true elite warriors,
true warriors.
They're incredibly diverse.
They come from all walks of life, all kinds of philosophies.
There's really only one thing that I can assure you
none of them have ever said.
Don't learn that.
That's never been said by any of them, guaranteed.
You know, I know some SF guys that basically, they
went to what amounts to finishing school.
As a group, they decided to go and do this,
learned which fork to use, learned how to pair wine.
One of them went on and went to culinary school.
And it's part of that idea of being well-rounded
and understanding different things
makes you a better warrior.
Yeah, there's that aspect to it.
It also made it to where they could function.
At one of those embassy dinners, they could blend in.
But it's not just about that.
It's not just about the stuff that is immediately
applied to the job.
No education is ever wasted.
The idea that learning about anything is bad,
that's the sign of an uneducated mind.
That's the sign of somebody who is closed off.
The true sign of intelligence is being
able to entertain an idea without accepting it as fact.
The idea that a bunch of politicians
want to tell the military what they should and shouldn't
learn is ridiculous.
The idea is to create people who know a whole lot.
That's what it takes, especially when it comes to Green Berets,
because they do have to be able to ingratiate themselves
into other cultures, a critical part of which
is understanding different systems of oppression.
Critical race theory, probably something
that would be useful, be useful framing,
because it could be applied to other situations
in other countries.
Now, the big argument here is that no, this is indoctrination.
That's suggesting that the person that is being taught
can't entertain an idea without accepting it as fact.
I would also point out that no, it's not.
Being educated about something is not indoctrination.
That's not how that works.
Indoctrination is when you have a bunch of politicians telling
you what you can and cannot learn.
That's indoctrination.
The United States as a whole has demonized education,
demonized intellectualism.
And we're seeing the fruits of that effort.
We're seeing people get energized.
Over things that are pretty easy to tell are lies.
We're seeing people be manipulated by Dr. Seuss
and Mr. Potato Head.
We're seeing people be tricked into sacrificing themselves
on the altar of the stock market.
Don't wear a mask.
It's not good for the economy.
Studies say otherwise.
This idea that politicians should determine what you can
and cannot learn, what can and cannot be taught,
that's a pretty dangerous concept.
That is the basis of indoctrination.
That is how you end up with a populace that
is unable to tell fact from fiction
because they're not used to entertaining an idea
without accepting it as fact.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}