---
title: Let's talk about Giuliani's license.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZQDDLzQhmfE) |
| Published | 2021/06/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Rudy Giuliani's license hasn't been permanently revoked; it's temporarily suspended due to demonstrably false statements made in connection with Trump's failed reelection effort in 2020.
- There is uncontroverted evidence that Giuliani communicated false and misleading statements to courts, lawmakers, and the public.
- The suspension is pending further proceedings before the attorney grievance committee.
- Giuliani's side believes he does not pose a present danger to the public interest and expects him to be reinstated after a hearing.
- Interim suspensions like Giuliani's are rare but not unprecedented.
- While immediate disbarment may not have happened yet, interim suspensions are significant actions.
- Given the evidence presented, it is likely that Giuliani's suspension will remain in place.
- This situation is still in its early stages, with more proceedings to follow.
- Giuliani's suspension is a result of his actions in his capacity as a lawyer for former President Trump.
- The temporary suspension indicates serious concerns about Giuliani's conduct.

### Quotes

- "His license hasn't been permanently revoked; it's temporarily suspended due to demonstrably false statements made in connection with Trump's failed reelection effort in 2020."
- "Giuliani communicated false and misleading statements to courts, lawmakers, and the public."
- "Interim suspensions like Giuliani's are rare but not unprecedented."
- "Given the evidence presented, it is likely that Giuliani's suspension will remain in place."
- "The temporary suspension indicates serious concerns about Giuliani's conduct."

### Oneliner

Beau shares that Rudy Giuliani's license is temporarily suspended due to false statements made in connection with Trump's failed reelection, pending further proceedings. It is a rare but significant action that may likely stick.

### Audience

Legal observers

### On-the-ground actions from transcript

- Contact legal organizations for updates on Giuliani's case (implied)
- Stay informed about the proceedings regarding Giuliani's suspension (implied)

### Whats missing in summary

Insights on the potential impact of Giuliani's suspension and the broader implications on legal ethics and accountability.

### Tags

#RudyGiuliani #LegalEthics #AttorneySuspension #TrumpElection #Accountability


## Transcript
Well, howdy there, internet people, it's Beau again.
So we have some news about Rudy Giuliani.
You have probably heard that he has been disbarred.
That's not true.
It's a temporary suspension.
Why is his license being temporarily suspended?
For the reasons that follow, we conclude
that there is uncontroverted evidence that
Respondent communicated demonstrably false and misleading statements to
courts, lawmakers, and the public at large in his capacity as a lawyer for
former President Donald J.
Trump and the Trump campaign in connection with Trump's failed effort at
reelection in 2020.
These false statements were made to improperly bolster Respondent's
narrative that due to widespread voter fraud, victory in the 2020 United States presidential
election was stolen from his client. Demonstrably false, huh? We conclude that respondents' conduct
immediately threatens the public interest and warrants interim suspension from the practice
of law pending further proceedings before the attorney grievance committee, sometimes AGC,
committee. Now his side says this is unprecedented as we believe that our
client does not pose a present danger to the public interest. We believe that once
the issues are fully explored at a hearing, Mr. Giuliani will be reinstated
as a valued member of the legal profession that he has served so well
in his many capacities for so many years. Okay, so that's actually what happened.
He wasn't disbarred. It's not a permanent thing. It can be overturned. There are
more proceedings to follow, but at present his license to practice law is
suspended because they find uncontroverted evidence that the
correspondent, communicated demonstrably false and misleading statements to courts,
lawmakers and the public at large in his capacity as lawyer for former
President Donald J. Trump." That's what's happened.
I know people want to immediately jump to him being disbarred and that may happen,
but that has not occurred as of yet, this is still pretty early in this
process, but I will say that interim suspensions of this
sort, they're not unprecedented as Giuliani's side contends,
but they're pretty rare.
So it is likely, at least in my very uneducated legal opinion
with no expertise whatsoever, that this will stick.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}