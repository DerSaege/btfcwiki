---
title: Let's talk about Snake Eyes and woke GI Joe....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Cm_oeD52CFM) |
| Published | 2021/06/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the controversy around G.I. Joe becoming "woke" with diverse representation.
- Points out that G.I. Joe has always been inclusive with characters of different races and genders.
- Mentions the introduction of Russian characters during the Cold War era.
- Talks about the Eco Warriors released in 1991 who fought pollution, resembling trans Joe's fighting climate change.
- Explains how G.I. Joe resonated with a generation due to relatable characters from diverse backgrounds.
- Argues that embracing diversity was a strength that built the G.I. Joe teams.
- Criticizes the idea of provoking outrage over Snake Eyes being portrayed as Asian.

### Quotes

- "G.I. Joe has always been really woke."
- "The reason G.I. Joe resonated with an entire generation is because there was a character that was relatable to everybody."
- "It's sad that you missed that."
- "The whole point was to embrace diversity."
- "I don't think anybody is going to care that Snake Eyes went from white to Asian."

### Oneliner

Beau explains G.I. Joe's long-standing diversity and inclusivity, challenging the controversy around its "wokeness" and urging to embrace diversity for strength.

### Audience

Pop culture enthusiasts

### On-the-ground actions from transcript

- Watch and support media that embraces diversity and inclusivity (implied)

### Whats missing in summary

Beau's engaging storytelling and passionate defense of G.I. Joe's inclusive history can be best appreciated by watching the full transcript.

### Tags

#G.I.Joe #Diversity #Inclusivity #PopCulture #Representation


## Transcript
Well, howdy there, Internet people. It's Bo again.
So, uh...
today...
today we're going to talk about G.I. Joe.
We're going to talk about G.I. Joe.
You know, I've always wondered
if the people who
complained about stuff like this, if they were ever actually fans of the series or
the franchise universe, whatever term you want to use,
or if
they just wanted something to whine about
so they could feel like they were the victim.
Okay, so I got a message.
When did G.I. Joe become woke?
I know you're a fan.
Admit it.
It bothers you that the new movie has snake eyes as an Asian.
But you stay silenced out of fear of being canceled.
The comics are canon.
Pick up where they left off.
The only reason they did this is because of the attacks lately.
They cast the movie like two years ago.
You talked about the Little Mermaid and Bond,
but not about something we all know you're a fan of.
Be a man and put on one of your G.I. Joe shirts
and tell us why you haven't talked about this.
Let's talk about woke G.I. Joe
before there's trans-Joes fighting climate change.
This is so funny.
Okay, so
when did G.I. Joe become woke? Let's start with that.
Segregation ended in 1968.
That was the real end of it.
So I'm going to say 1964,
because by 1965
they'd already released a black character.
Now I imagine that's not what you're talking about. You're talking about the stuff from the 80s.
And I want to say from the very beginning,
I want you to
try to think of another
toy line
that had representation
for every race.
Native, Asian, black, white, Latino.
And I don't mean like a token,
like a major part of the universe.
I bet you can't.
Name another boys toy line in the early 80s
where the women characters sold out.
G.I. Joe has always been really woke.
Even like the whole propaganda aspect,
the least woke thing about G.I. Joe is, you know, knowing is half the battle
and the other half is violence.
They had a pacifist character.
As the Cold War thawed,
they introduced Russian characters
and made them good guys.
Kind of help with that.
They took one of the major characters
and gave them a substance issue
and put it into the children's cartoon
and showed that character going through it and going through withdrawal.
They've
kind of always been woke.
That's kind of
a big part of it to be honest.
We'll get to that.
The comics are canon, you say.
Pick up where they left off. That's going to be a really boring movie there, superfan,
because
Snake Eyes has been dead in the comics for years.
Fill him his grave, I guess.
And then you've got this last line here.
Trans Joe's fighting climate change.
In 1991,
G.I. Joe released the Eco Warriors.
Their whole job was cleaning up
pollution
because it was damaging. That already happened
thirty years ago.
That's already a thing.
And if the comics
are canon,
I would point out
that we really still don't have an explanation
as to how
Dial Tone,
Jack Morelli,
became Joe Morelli.
Nobody cared.
Nobody needed an explanation
because that's part of it.
It's sad that you didn't realize this.
The reason G.I. Joe resonated with an entire generation
is because
there was a character
that was relatable to everybody.
Everybody could find a character that they had something in common with.
They came from
different socioeconomic classes, different races,
everything, different cultures. Some were immigrants.
And they all came together
and became an American hero.
That was the whole point.
It's sad that you missed that.
You know, a lot of my friends,
they related to Low Light.
That was the character's name.
Because a lot of their dads came back from Vietnam
and they had
well, let's just say creative
disciplinary tactics,
which was
Low Light's backstory.
His dad was pretty horrible, had nightmares about it, childhood trauma,
pretty woke topic
in the cartoons
in the eighties.
It's always been like that.
The whole point
was to embrace diversity.
That's what made them strong. That's what built the teams.
Um...
If you're looking to provoke outrage for some silly
culture war thing,
you probably need to pick a different fandom.
I don't think anybody is going to care
that Snake Eyes went from white to Asian.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}