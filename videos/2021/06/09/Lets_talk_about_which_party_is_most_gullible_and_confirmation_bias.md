---
title: Let's talk about which party is most gullible and confirmation bias....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PhXwliDxoy4) |
| Published | 2021/06/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about consuming information, gullibility, partisanship, and realizing mistakes.
- Mentions an individual involved in the Capitol incident on January 6th who now realizes he was deceived.
- The person is in custody since January 9th, but didn't commit any violent acts.
- Advocates for the person to be released on house arrest because he was gullible and tricked.
- Expresses concern about keeping someone locked up just because they were deceived.
- Mentions the difficulty in discerning truth from fiction for conservatives based on a study from Ohio State University.
- Points out confirmation bias and how it influences beliefs.
- Suggests that conservative leaders are aware of the gullibility and may exploit it.
- Calls for teaching critical thinking and questioning beliefs to combat misinformation.
- Warns against culture wars leading to further divides and susceptibility to misinformation.

### Quotes

- "If we start locking up conservatives every time they get tricked, we're going to run out of space pretty quickly."
- "Most of the misinformation that is out there right now on social media is favorable to conservatives."
- "Objective reality has a well-known liberal bias."
- "We're going to have to start focusing more on teaching critical thinking and questioning your own beliefs."
- "This culture war nonsense that's going on, it's just pushing people further into those little divides."

### Oneliner

Beau talks about gullibility, misinformation, and the need for critical thinking to combat partisan divides and susceptibility to misinformation among conservatives.

### Audience

Conservative thinkers

### On-the-ground actions from transcript

- Teach critical thinking skills to combat misinformation (suggested)
- Question your own beliefs and seek diverse perspectives (suggested)
- Address the gap in discerning fact from fiction in news stories (suggested)

### Whats missing in summary

The full transcript offers Beau's insightful perspectives on the challenges of consuming information, gullibility, partisanship, and the importance of critical thinking in the face of misinformation.


## Transcript
Well, howdy there internet people. It's Beau again.
So today we are going to talk about consuming information,
gullibility, partisanship, and realizing you've made a mistake.
So one of the individuals who was involved in that thing on the 6th at the Capitol,
well, he realizes now that he's been deceived.
He bought into a pack of lies, quote.
And he's asking the judge to go home.
He's been in custody since the 9th of January.
Now, as far as we can tell, based on the charges,
this guy didn't do anything violent, didn't break anything, didn't steal anything.
He just went inside.
And for whatever reason, he has been in custody since the 9th of January.
He wants to go home on house arrest because he realizes that he is more than likely
going to have to do a little bit more time once he is sentenced.
Now, to be honest, for me, he didn't hurt anybody.
I mean, how long are we going to keep this guy locked up because he was gullible,
because he was tricked, because he was duped?
If we start locking up conservatives every time they get tricked,
we're going to run out of space pretty quickly.
And when I say stuff like that, I always get static.
I do.
Conservatives get mad and they say, we're not gullible.
And liberals get mad and they say, you know, don't give them the answer.
Don't give them the out of that they just got tricked.
They knew what they were doing.
But see, I think to a big degree, there is a fundamental issue when it comes to consuming
information on the conservative side of the aisle.
I do believe that in many ways they are more gullible.
And one of the things that I love is when I believe something, I have this preconceived
idea, and then the data shows up to back it up.
It always makes me happy.
That's happened.
A study out of Ohio State University of over a thousand people took place in 2019,
so before the massive misinformation campaign related to the 2020 election.
And it measured whether conservatives or liberals were better at discerning truth from fiction
when it comes to news stories.
Who was most likely to fall for misinformation?
Surprise, surprise, yes, conservatives.
They have a harder time distinguishing fact from fiction in that context.
But there's something else at play.
I love it when I believe something and then the information shows up to confirm it.
So does everybody else.
It's called confirmation bias.
Everybody enjoys that.
The interesting thing is that most of the misinformation that is out there right now
on social media is favorable to conservatives.
So while yes, they are more gullible, they are more likely to fall for misinformation,
for lies, what I think is more important is those people who they look up to.
The conservative leaders, the people who they trust, the people who they trust.
The people who are the truth-tellers, the people who say stuff like facts, not feelings.
Those who control the conservative movement,
they're aware of the fact that they're more gullible and they're playing on it.
Because most of the information that was out there was favorable to Republicans.
Most of the misinformation, most of the facts, they were favorable to liberals.
And the fact that confirmation bias sits at play there would mean liberals would be more likely
to believe the truth and conservatives would be more likely to believe the lie,
simply because objective reality has a well-known liberal bias.
I know that nobody wants to give these people an out,
and for some of them that this shouldn't apply, especially those who organized it.
But at some point we're going to have to address the fact that there is a gap
when it comes to being able to discern fact from fiction when it comes to news stories.
And we're going to have to do something about it.
We're going to have to start focusing more on teaching critical thinking
and questioning your own beliefs.
This culture war nonsense that's going on,
it's just pushing people further in to those little divides.
And when it comes to the conservative movement,
they're going to fall further and further into those echo chambers,
which means they will become more and more susceptible to misinformation.
And there will be more and more of them led to the point where they do something
and they end up in a cage.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}