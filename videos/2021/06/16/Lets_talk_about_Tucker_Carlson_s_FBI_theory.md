---
title: Let's talk about Tucker Carlson's FBI theory....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-PXWZri1kCI) |
| Published | 2021/06/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Tucker Carlson theorized that the FBI orchestrated the events at the Capitol on January 6th, claiming undercover agents were involved.
- Beau disputes Carlson's theory, explaining that having unindicted co-conspirators in indictments does not prove FBI involvement.
- Naming unindicted co-conspirators can damage reputations and is often done when evidence is lacking or when the person is cooperating with authorities.
- Beau argues that pushing false narratives like Carlson's deflects accountability from those responsible for events like the Capitol riot.
- Right-wing pundits initially praised the Capitol events as Americans fighting for freedom but later shifted blame to groups like Black Lives Matter and Antifa to avoid accountability.
- Media outlets that spread baseless election fraud claims share responsibility for the Capitol riot due to the misinformation they promoted.
- Beau suggests that those who were misled into believing false information were ultimately tricked by manipulative narratives.
- He humorously compares Carlson's narrative to a Scooby-Doo mystery, where the true culprits are often revealed to be wealthy individuals.
- Beau implies that the true instigators of the Capitol riot are well-known, despite attempts to shift blame onto entities like the FBI.

### Quotes

- "That's not how that works."
- "There's literally no evidence that has been presented to suggest this is true."
- "A lot of those people were tricked."
- "When we pull off the mask, we know what happens at the end of every episode of Scooby-Doo."
- "I think it's funny that they're looking for who to blame for inciting this when pretty much everybody knows who really incited it."

### Oneliner

Tucker Carlson's FBI conspiracy theory lacks evidence, deflecting accountability from real instigators of the Capitol riot.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Fact-check false narratives and hold media outlets accountable (exemplified)
- Advocate for responsible reporting and accountability in media (implied)

### Whats missing in summary

Insight into the dangers of spreading misinformation and the importance of holding media accountable for their role in inciting violence.

### Tags

#TuckerCarlson #FBI #CapitolRiot #Misinformation #Accountability


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Tucker Carlson's little theory.
I know, I know people are like, why do we have to talk about Tucker Carlson?
Sadly, because he's influential.
These theories that are pushed by these sorts of people, they end up in the mainstream.
So we have to talk about him coming from them.
Okay so if you don't know what he said, he basically said the FBI orchestrated the events
at the Capitol on the 6th.
No joke.
That was the whole spiel.
His evidence for this is that in some of the indictments there are unindicted co-conspirators
and therefore that means that they're undercover agents.
Let's just stop right here.
That's not how that works.
That's not, no, that's not a thing.
That's not how that works at all.
First for a prosecutor to say, oh yeah, these federal agents here, we're going to put them
in as co-conspirators, in fact they're the ones who organized it.
That would be what legal professionals would call a totally uncool move because that pretty
much guarantees an acquittal on the grounds of entrapment.
No lawyer is going to do that.
They're not going to say, oh yeah, the feds actually incited and organized this whole
thing.
Even if it was true, they wouldn't say that.
And if I'm not mistaken, there's actually a rule saying they can't list undercovers
as unindicted co-conspirators.
It's just not how it works.
That isn't evidence that the FBI incited this.
Now it's not entirely impossible to suggest that the FBI would incite a crime, or at least
those operating under their auspices.
It has happened in the past.
It didn't happen on the 6th though.
That's not a thing.
So why do people get listed as unindicted co-conspirators?
One of the most common reasons is the person presenting to the grand jury, well they believe
this person was involved, person 1 or person 2, they believe they were involved but they
don't have the evidence to indict them.
So they don't name them.
Because if they do name them and that grand jury, that indictment gets released, the people
named don't have a way to defend themselves because they're not charged.
That's just slandering somebody's reputation.
So they don't do that.
They don't name them.
That's why you have unindicted co-conspirators.
Another reason, which seems super likely in this case because we know it's happening,
is that the unindicted co-conspirator is cooperating with federal authorities.
That's another reason that it happens.
So the theory is garbage.
There's literally no evidence that has been presented to suggest this is true.
None.
The one piece of evidence, quote, that they are leaning on is not evidence.
That's not how it works.
Having unindicted co-conspirators is not evidence of there being undercover agents, especially
those that incited something.
But see, here's the thing.
Why are they pushing these narratives?
That's got to be the real question, right?
Because if you think back, when it was happening, when it was actually going on, when there
were people still there at the Capitol, you had a whole bunch of right-wing pundits and
personalities, oh, this is great, look, Americans fighting for their freedom.
And then they found out popular support wasn't there.
It turned violent and failed.
And all of a sudden, what occurred?
Oh, it was Black Lives Matter.
It was Antifa's.
It was the ghost of Hugo Chavez.
It was literally anybody but them because they wanted to avoid accountability.
And I'm not talking about the people who were present.
These theories pushing the different groups that are responsible, anybody other than who
is plainly responsible, they're all pushed by the pundits who pushed the baseless election
fraud claims.
They don't want to be held accountable by their viewers.
In many ways, a lot of these media outlets that pushed these baseless theories about
the election, they're responsible for what happened on the 6th.
There's a direct line between putting this faulty information out there and what occurred.
They probably didn't mean for that to happen.
A lot of them may not even understand that the two things are related.
You got to remember, a lot of these people aren't very smart.
But it's there.
So now they have to find something else to blame it on.
It wasn't the misinformation.
It wasn't the constant incitement.
It wasn't the right-wing talking heads.
It wasn't the president.
It was the FBI.
The FBI tricked them.
Least they're getting closer to the truth now.
A lot of those people were tricked.
They believed what they were told.
I find it entertaining that in Tucker Carlson's little Scooby-Doo mystery here, they just
refused to take off the mask.
Because when we pull off the mask, we know what happens at the end of every episode of
Scooby-Doo.
So an old rich white guy that's ultimately responsible, right?
It's probably going to be the case here.
I think it's funny that they're looking for who to blame for inciting this when pretty
much everybody knows who really incited it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}