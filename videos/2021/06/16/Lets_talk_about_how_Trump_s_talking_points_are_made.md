---
title: Let's talk about how Trump's talking points are made....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WzsxfKtG79w) |
| Published | 2021/06/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains how authoritarians use talking points to undermine base principles of a country.
- Gives examples like anti-protest laws and purity of the vote laws as ways to erode freedoms.
- Talks about border checkpoints and how they infringe on citizens' rights.
- Mentions Trump and Miller setting up an office to further marginalize certain groups.
- Describes Biden reallocating funds to help immigrants in detention report abuse.
- Criticizes the use of talking points to shift public opinion towards authoritarianism.
- Encourages listeners to analyze political talking points for their impact on basic principles.

### Quotes

- "They create the problem then they create the solution."
- "That's how authoritarians throughout history have swayed public opinion away from the base principles of the country."
- "Trumpist talking points, the talking points that are used by his adherents, see if they undermine the base principles of the country and set the stage for authoritarianism."

### Oneliner

Beau explains how political talking points can erode fundamental freedoms and pave the way for authoritarianism.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Analyze political talking points for their impact on basic principles (implied)

### Whats missing in summary

Exploration of the historical context and examples of authoritarian tactics used to manipulate public opinion.

### Tags

#Authoritarianism #PoliticalTalkingPoints #UnderminingFreedom #CommunityAction #Analysis


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about talking points.
How to identify them and how a certain type of politician develops talking
points that are specifically designed to undermine the base principles of the
country that they're trying to gain control over.
We're talking about authoritarians.
In order for them to seize power, in most cases, they have to replace the base principles of a country with their own,
with talking points that will allow them to do whatever they want.
So they have to undermine base ideas of freedom.
This happens a lot in the United States, and a lot of these talking points they've already taken hold.
Some of them have already become legislation.
OK, so let's run through some examples here.
Anti-protest laws.
They took an absurd, and you're going
to see this as a running theme, an absurd example of something
and used that to justify undermining a base
principle of the country.
Anti-protest laws are anti-First Amendment.
You have the right to peaceably assemble.
It doesn't say you have the right to peaceably assemble
on a sidewalk in a designated area from nine to five.
You have the right to peaceably assemble.
These protest laws are designed to undermine the base idea
that you can gather peacefully and demand
a redress of your grievances.
that is the core. That's what they're there for. It's red meat to the base that gets people fired
up about an absurd scenario and therefore they trade away a little bit of their freedom to get
rid of a problem that doesn't really exist. They create the problem then they create the solution.
The purity and quality of the vote. Wow, that's like incredibly anti-democracy, anti-representative
democracy, therefore anti-republic. Again, it's a solution to a problem that doesn't really exist,
but this allows them to, well, set the stage to get rid of votes they don't
want, that aren't pure enough, that aren't of the right quality. That's what it's
there for. This is how authoritarians throughout history have swayed public
opinion away from the base principles of the country. Those border checkpoints
that are inland, where they, you know, you drive up and they demand to see your papers or whatever.
Yeah, I mean, I get it right now. It's just for those other people, right? Those people that they
can view as lesser. But realistically, is it just them impacted? No, it's not. And I'm pretty sure
the base principles of this country suggest that we have the right to be secure in our purses,
Houses, papers, and effects.
That's how they do it.
You want another one?
One that's a little bit more recent, happening right now.
Trump and Miller, they set up that office voice.
I don't remember what the acronym stands for,
and it doesn't matter because it wasn't really about
helping the victims of crimes that were committed
by non-citizens.
It was about getting the information
so they could cherry pick cases
to further marginalize that group, to blame them,
so they're lesser, they're other.
All of the problems are their fault.
This office was obviously closed by the Biden administration.
He doesn't get any points for that, okay?
That's just something that should happen
in the course of due events.
That's just, matter of fact, it was gonna get closed.
It didn't matter who took over, okay?
Anybody other than Trump would have shut that down
because it was that absurd.
Absurd.
Now, what did Biden do with the funds?
Reallocated it.
If you're an immigrant in detention,
you can now call this call center and report abuse.
That's one of the things.
There's a whole bunch of things that you can do
with this call center.
What did Steve Miller say?
He said, well, this is like setting up a call center
so drug dealers could get lawyers
and to a certain group of people to that authoritarian base,
who are already indoctrinated. Yeah, we don't want those people to have lawyers, as if that's
not a fundamental principle, as if every county in the country doesn't have a number that an
accused person could call to get a lawyer called a public defender's office, undermining the base
principles of the country. That's how the talking points work and they'll shift from one to another.
If you look at Trump talking points, Trumpist talking points, the talking
points that are used by his adherents, see if they undermine the base
principles of the country and set the stage for authoritarianism. I'm willing
I bet they do.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}