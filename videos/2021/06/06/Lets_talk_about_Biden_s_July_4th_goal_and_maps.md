---
title: Let's talk about Biden's July 4th goal and maps....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7dsHj2MaHr0) |
| Published | 2021/06/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden set a goal for 70% of the population to have at least one shot by July 4th.
- The New York Times published a map showing states on target to hit the goal, with a clear gap between southern states.
- Maps comparing party affiliation of governors to vaccination rates show a clear correlation.
- Public health has become a partisan issue, with Republican-led states falling behind in vaccination rates.
- Beau criticizes the Republican Party for failing to lead and putting their own voters at risk.
- Republicans are rejecting objective reality for social media approval, endangering their base.
- Beau predicts a significant difference in COVID-19 losses based on party affiliation.
- He questions the selfishness of writing off their base to appease the former president and downplay the severity of the pandemic.
- Beau urges people to get vaccinated and do their part in combating the public health crisis.

### Quotes

- "Public health has become a partisan issue."
- "Republican Party is still failing to lead."
- "Imagine how selfish you have to be to quite literally write off your own base."
- "Go get your shots. Go get vaccinated."
- "Do your part."

### Oneliner

President Biden set a vaccination goal for July 4th, revealing a partisan divide in vaccination rates and Republican leadership failures, endangering their own base.

### Audience

Public Health Advocates

### On-the-ground actions from transcript

- Get vaccinated and encourage others to do the same (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the correlation between party affiliation and vaccination rates, urging individuals to prioritize public health over political allegiance.

### Tags

#COVID19 #Vaccination #PartisanPolitics #PublicHealth #RepublicanParty


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about a few maps
and the partisan response to the public health thing
and how it is still going on and how it's likely to impact things.
Biden, President Biden, set that goal.
July 4th, 70% of the population needs to have at least one shot by July 4th.
New York Times, well, they put out a map showing which states were on target to hit that goal
and which states weren't.
Of course, they captioned it, you know, something like,
The South lags behind again because of course we do.
And yeah, there is a pretty pronounced gap when it comes to southern states,
but to me there is something far more interesting in that map.
If you take the map that the Times put out and you compare it to, I don't know,
a map that shows the party affiliation of the governor of that state,
they are almost identical.
Just a few exceptions, but those states with Republican governors,
they're not hitting the mark.
Those states with Democratic governors are.
Don't like that one? You can go to how the state normally votes for president.
Same thing applies.
Public health has become a partisan issue and these maps clearly demonstrate
that after all of this time, the Republican Party is still failing to lead.
Still failing their own voters.
Still putting them at risk.
That's occurring. It's easily demonstrated.
It's just like the masks or anything else.
Anything else that's had to do with this.
They just refuse to acknowledge that this is the way to do it.
They are rejecting objective reality simply for social media cred.
And in the process, they're endangering their own base.
I am willing to bet that when all this is said and done,
when it's finally over with and we stop tallying the loss,
if we break them down by party affiliation,
there's going to be a very pronounced difference.
Imagine how selfish you have to be to quite literally write off your own base
so you can appease the former president.
So you can carry on that rhetoric of it not being a big deal.
Go get your shots. Go get vaccinated.
Do your part.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}