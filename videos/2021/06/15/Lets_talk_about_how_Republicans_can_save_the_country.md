---
title: Let's talk about how Republicans can save the country....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gA3jFOc8kPA) |
| Published | 2021/06/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans can save the country by listening to subject matter experts, who are not always PhDs but can be found all around us.
- Subject matter experts can predict outcomes based on their experience and recognizing patterns.
- In 2017-2019, experts warned about Trump's potential to overturn the election due to authoritarian characteristics he exhibited.
- It's vital for Republicans to vote for actual Republicans in primaries and not those pushing for authoritarian regimes.
- Beau encourages Republicans to watch videos on his channel discussing authoritarian characteristics and Trump's actions as examples.
- Staying true to Republican values and avoiding far-right authoritarianism is key to preserving the republic.
- Beau points out the danger of a cult of personality and the importance of differentiating between authoritarian leaders.
- He contrasts how actions by Biden or Harris versus Obama might have been perceived by the public and media.
- Rank and file Republicans play a critical role in safeguarding democracy by making informed voting choices.
- Beau stresses the importance of not repeating past mistakes by falling for authoritarian tendencies again.

### Quotes

- "All you have to do is actually stay as a Republican and not venture off into that far right brand of authoritarianism."
- "Rank and file Republicans are kind of key to preserving the republic."
- "You made a mistake once, you back to this brand of authoritarianism once, don't do it again."

### Oneliner

Republicans can save the country by heeding subject matter experts, avoiding authoritarian tendencies, and making informed voting choices in primaries.

### Audience

Republicans

### On-the-ground actions from transcript

- Watch videos discussing authoritarian characteristics and Trump's actions on Beau's channel (suggested)
- Vote for actual Republicans in primaries, not those pushing for authoritarian regimes (implied)

### Whats missing in summary

The full transcript provides a detailed explanation on the importance of recognizing authoritarian characteristics, making informed political choices, and safeguarding democracy against potential threats.

### Tags

#Republicans #Authoritarianism #Voting #PreservingDemocracy #SubjectMatterExperts


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about
well, how Republicans can save the country
and subject matter experts.
You know when you hear that term, subject matter expert,
you picture somebody with a PhD, most times.
The reality is though, there's subject matter experts
all around us.
You know, most of us know that person
who can walk by your car while it's idling
and be like, your AC's going to go out and keep walking.
And you know, you don't believe them at first, and then
you know, three weeks later you're
sitting there sweating, angry that you didn't pay attention.
And we all know that framer, that carpenter,
normally 50-something years old, bad back and bad attitude,
self-medicates a lot, right?
And they walk into the room, look at what you're cutting,
be like, that's not going to be a level, and walks out.
And you're stubborn, so you build it anyway.
Four hours later you're done, and you
put that level on top of it, and you
have some colorful language, right?
Subject matter experts, they're all around us,
different fields.
These people, they're not psychic.
They don't have crystal balls.
They've just seen it over and over again.
They recognize the signs.
They can hear it.
And because of that, they can make predictions
that pretty much always turn out to be right,
because they've been doing it that long.
What were the subject matter experts on authoritarianism
saying in 2017, 2018, 2019?
A whole lot of them were talking about how Trump was going
to try to overturn the election.
They're not psychic.
They know that it's one of 14 characteristics
of authoritarian regimes around the world.
It's a playbook, and it's a playbook
that he checked off all 14 characteristics.
You have to recognize that.
You listen to subject matter experts,
sometimes begrudgingly, in your everyday life all the time.
Might be time to listen to them about this,
because it's pretty important.
Because while Trump is becoming less and less of a political
force as every day passes, well, those
who bought into his brand, those who
are going to get his endorsement,
they're still using that same playbook.
If the Republicans, rank and file Republicans,
want to save this country, want to do their part,
well, it's pretty simple.
You vote for the actual Republican in the primaries,
not the Trumpist, not the person that
is trying to bring about that authoritarian regime.
And if you don't want to go look for it,
there's videos on this channel dating back years,
covering the 14 characteristics, talking
about what Trump was going to do before he did it.
There's one called, let's talk about Trump's accomplishments,
and one that's just titled 14 characteristics,
or something like that.
You can find it.
And I am not the only person.
There's a whole bunch of people who
are much better versed in that topic than I am.
And they got even more specific, and they
were right every step of the way, because it's a playbook.
It's something that has happened throughout history over and over
and over again.
We sit here today and wonder, how did this country
let this happen?
How did this person come to power?
You're watching it happen now.
You don't have to become a Democrat.
You don't have to become a liberal.
You don't have to become a leftist.
All you have to do is actually stay as a Republican
and not venture off into that far right brand
of authoritarianism.
That doesn't mean that Biden or Harris
isn't an authoritarian to some degree,
but there's a big difference.
And the fact that many Republicans
can't see that difference is part of the problem,
because that cult of personality,
that's a big part of how those 14 characteristics are
achieved.
Let's be real honest with each other for a second.
What would have happened if President Obama had walked out
and said, I want to take the guns first,
take the guns early?
It would have been pandemonium.
If he had cozied up with authoritarian dictators
all over the world, people would have
been repeating his middle name on the nightly news every day,
because that's how they would have motivated people,
because that's a big part of it.
If you don't know who the Republican and who
the Trumpism person is in your primary,
whichever one is trying to blame a whole bunch of people
that have no institutional power,
generally don't have a lot of money,
trying to blame them for your problems,
that's the one you want to vote against.
Rank and file Republicans are kind of key
to preserving the republic.
You know, everybody likes to say that,
wants to talk all patriotic, say I'd do anything,
I'd fight and die for my country.
That's not always necessary.
It's not what it takes most times to preserve something,
because it's already there.
All you have to do is not get tricked.
You made a mistake once, you back
to this brand of authoritarianism once,
don't do it again.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}