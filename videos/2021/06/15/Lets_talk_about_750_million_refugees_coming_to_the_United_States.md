---
title: Let's talk about 750 million refugees coming to the United States....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=2kXKvtRGU7M) |
| Published | 2021/06/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the misconception of 750 million refugees wanting to come to the United States.
- Mentions the arguments against accepting such a large number, citing space and economic concerns.
- Counters the space argument by comparing the landmass of China and the US.
- Challenges the economic drain argument by suggesting that more people equal more economic activity.
- Refers to a 2018 Gallup study that clarifies the 750 million figure pertains to migrants, not just refugees.
- Points out that not all migrants want to come to the US, with only 21% expressing a desire to do so.
- Calculates that around 150 million individuals actually want to migrate to the US.
- Suggests that the US could feasibly accept every person who wants to come, given its size and resources.
- Argues that portraying the issue as insurmountable serves as an excuse for inaction.
- Asserts that economic and space constraints are not valid reasons to reject migrants.

### Quotes

- "We could actually do it."
- "Realistically, yes, the United States could accept every single person who wants to come to the US in the world."
- "It's just a talking point."
- "It's not going to be an economic one."
- "All of the stuff that gets thrown up, that's not true."

### Oneliner

Beau addresses the misconception around 750 million migrants wanting to come to the US, debunking space and economic concerns and suggesting that the US could feasibly accept them. 

### Audience

Policy advocates

### On-the-ground actions from transcript

- Challenge misconceptions about migrants (implied)
- Advocate for more inclusive migration policies (implied)

### Whats missing in summary

The full transcript provides a detailed debunking of the misconceptions surrounding the number of migrants wanting to come to the US, offering a nuanced perspective on the feasibility of accepting them.

### Tags

#Migration #Refugees #US #Misconceptions #Immigration


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about a number that gets thrown around a lot any time the
subject of migrants comes up.
It shows up any time somebody wants to say we can't let everybody in, even though there
are very, very few people suggesting that we just let everybody in.
But the idea is that there are 750 million refugees that want to come to the United States.
750 million.
That is a whole lot.
Now there's a number of parts to this little talking point.
The first and most obvious is that we can't take that many.
We don't have the space and there will be a drain on our economy.
The next part of this is it reinforces that idea, that dream that America, land of the
free, home of the brave, all immigrants want to come here and all of that stuff.
So let's just run through the propaganda side of it.
Let's not debunk it yet.
Let's just address it as it is.
We can't take that many.
Yes we can.
We actually could.
China has 9.3 million square miles.
The United States has 9.1 million.
China currently has a population of 1.4 billion.
If we were to take all 750 million refugees that are claimed by this statement, we would
be at about 1.1 billion.
So yeah, we actually could do it.
There'll be a drain on the economy.
No, they will not.
More people means more people consuming, which means more economic activity.
Just go to your favorite search engine and type in the phrase, immigrants net gain for
economy and start reading.
The catalog of information is endless.
Okay, so now that we've talked about this from the stated talking point, let's get rid
of the propaganda aspects of it.
So this is from a 2018 Gallup study.
That's where this number came from, 750 million.
It's not 750 million refugees.
That's silly.
It's 750 million migrants, people who want to migrate from their country.
Some of them would like to migrate from the United States.
They'd like to leave here.
It's all over the world.
It's not talking about refugees.
It's anybody who wants to migrate.
Now the assumption here is that all 750 million want to come to the United States.
That's not true.
That is plainly false.
That isn't accurate.
Well I mean certainly more than half of them do.
No, not even a quarter.
79% of people would like to go somewhere other than the United States.
The US only has 21% of people who want to come here, which means if you do the math,
you're talking about 150 million and change.
That's how many people want to come here.
If you took everybody on the entire planet that wanted to come to the United States and
let them come in, made them part of our population, they would amount to one in three.
Doesn't seem so impossible, does it?
It's just a talking point.
It's one of those things that seems almost unique to the American character.
Rather than just admitting whatever reason you don't want to let them in, well we make
the problem seem too big.
And then we have the perfect excuse to do absolutely nothing about it.
Realistically, yes, the United States could accept every single person who wants to come
to the US in the world.
We absolutely could.
We could actually do it, I mean it would take a long time, but we could do it with the whole
750 million.
We are the United States.
We are an insanely large country.
We certainly could.
We could definitely do it with the 150 million that actually want to come here.
I mean, sure, there would be a whole bunch of those other types of people around, but
as long as that's not an issue, then there's no problem.
Because it's not going to be an economic one.
It's not one of space.
All of the stuff that gets thrown up, that's not true.
That's just an excuse to not have to deal with the others, those from outside.
So there's that number.
I saw that pop up a couple of times over the last few days, so just throwing that out there.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}