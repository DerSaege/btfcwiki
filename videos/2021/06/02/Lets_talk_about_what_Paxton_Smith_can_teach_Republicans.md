---
title: Let's talk about what Paxton Smith can teach Republicans....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=98DRpdYQihw) |
| Published | 2021/06/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Report suggests Republicans are worried about losing the next generation of leaders due to reliance on personality over substance.
- Republican Party has lost the current generation as well.
- Republicans leaned into the idea that facts don't matter and that alternative facts are acceptable.
- Real leaders and ethical people don't want to manipulate others for their base; they want to lead.
- Paxton Smith, a valedictorian in Texas, gave a speech at graduation that challenged legislative laws.
- She took a stand by giving a speech different from the one approved by authorities.
- Her speech focused on keeping laws off her body, met with applause.
- Beau believes if Paxton ran for public office, she wouldn't identify as Republican.
- Republican Party's reliance on appealing to the lowest common denominator and rejecting reality has caused future leaders to look elsewhere.
- People like Paxton may not be Democrats or Republicans, potentially leading in a different way or joining nonprofit organizations.
- Republican Party's actions have pushed the younger generation towards more progressive ideologies due to prioritizing party over policy and personality over country.

### Quotes

- "Strangers in the legislature, how about you keep your laws off my body?"
- "She refused to surrender the platform that she had been given, yeah, that's a leader."
- "She may not be a Democrat, but she won't be a Republican."
- "Republican Party has done more to push the younger crop left than any leftist ever could."
- "Illustrated very clearly the dangers of party over policy or personality over country."

### Oneliner

Reported concerns on losing future leaders due to GOP's focus on personality over substance lead to a valedictorian's impactful speech challenging legislative norms in Texas.

### Audience

Future leaders

### On-the-ground actions from transcript

- Support nonprofit organizations focused on family planning (implied)

### Whats missing in summary

The full transcript provides deeper insights into the consequences of prioritizing party over policy, illustrating a potential shift towards more progressive ideologies among younger generations.

### Tags

#RepublicanParty #FutureLeaders #YouthEmpowerment #ProgressiveIdeologies #PublicSpeaking


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about what Paxton Smith can
teach the Republican Party.
If you missed it, earlier this week,
a report came out suggesting that Republicans
were worried that they were going to lose
the next generation of leaders, that their reliance
on personality over substance has caused them to flee,
or they won't run.
Going to lose the next generation.
They seem completely unaware that they
have lost the current generation as well.
The Republican Party spent the last four years
leaning very heavily into the idea that facts don't matter
and that alternative facts are acceptable
and that we can appeal to those who are easily tricked.
And that will give us our base.
The problem is that real leaders, ethical people,
they don't want to rely on manipulating people
for their base.
They want to lead.
Paxton Smith is a valedictorian in Texas.
And she was recently supposed to give a speech
on media consumption.
She took that speech.
She got it approved through all the proper channels
because even if you're a valedictorian
and you've proven that you're a pretty responsible person
and you probably have ideas of your own,
well, you still need to get them approved
by the authorities in Texas.
So she got her speech approved.
And then she gave a different one.
She gave one that basically said, hey,
strangers in the legislature, how about you
keep your laws off my body?
In Texas, this happened.
And it was met with applause.
Somebody who is a valedictorian, somebody who would do that,
who refused to surrender the platform that she
had been given, yeah, that's a leader.
And I am 100% certain that if she
was to run for public office, she would not
have an R by her name.
The reliance that the Republican Party
has shown on appealing to the lowest common denominator,
rejecting reality, playing into wild theories
because it helps keep their base energized,
this has caused already a lot of people
to be in the same boat as her.
This has caused already past tense future leaders
to look somewhere else, to look in a direction of people
who may actually want to lead rather than just manipulate
people and enrich themselves along the way.
People like Paxton, she may not be a Democrat,
but she won't be a Republican.
She'll go to some third party.
She'll run as an independent, assuming she decides
that's what she wants to do.
She could lead in some other way,
maybe join a nonprofit organization that
helps with family planning.
In many ways, the Republican Party
has done more to push the younger crop left
than any leftist ever could because they illustrated very
clearly the dangers of party over policy or personality
over country.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}