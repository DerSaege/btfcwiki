---
title: Let's talk about the home team advantage and society....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Bv5E_vz6DjI) |
| Published | 2021/06/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why he doesn't usually use sports analogies, citing his lack of sports-watching as the main reason.
- Introduces the concept of home team advantage in sports, where statistically the home team wins more frequently.
- Provides statistics showing that in the NFL, the home team wins 57% of the time.
- Explores possible reasons for home team advantage, such as crowd excitement, environmental factors, referee bias, and travel distance for the away team.
- Considers how these factors could also apply beyond sports to societal privilege and systemic advantages.
- Suggests renaming home team advantage to home team privilege to draw parallels with societal inequalities.
- Advocates for addressing and mitigating these privileges to create a fairer playing field in sports and society.
- Draws a comparison between addressing bias in sports officiating and bias in societal systems.
- Criticizes the prioritization of mitigating bias in sports over bias affecting people's lives.
- Raises the point that acknowledging and addressing privileges in society is necessary for promoting fairness and equity.

### Quotes

- "Let's not call it home team advantage. Let's call it home team privilege."
- "Privileges of all kinds exist. They're real. You can't deny that they're out there."
- "When the outcomes are as different as they are in reality, it's probably worth addressing."
- "It's probably worth trying to mitigate and see if we can get to a game that's a little bit more fair."

### Oneliner

Beau explains home team advantage in sports, linking it to societal privilege, advocating for addressing and mitigating systemic inequalities in both realms.

### Audience

Sports fans, social justice advocates

### On-the-ground actions from transcript

- Advocate for fair officiating by supporting measures like tracking chips in sports equipment to reduce bias (implied).

### Whats missing in summary

Beau's engaging storytelling and thought-provoking insights on addressing systemic privilege in sports and society could be best experienced by watching the full transcript. 

### Tags

#Sports #SocietalInequality #SystemicBias #HomeTeamPrivilege #FairPlay #Advocacy


## Transcript
Well, howdy there internet people. It's Beau again. So today we're going to use a different
kind of analogy. We're going to do this because somebody asked me, you know, why don't you ever
use sports analogies? Now if I want to make myself sound smart, I'll say that it's because
sporting events are contests. They're set up to be competitions, therefore that's going to alter
the outcome. The real reason is that I don't watch a lot of sports and therefore would have to do a
whole lot of research to make the analogies make sense. But in this case, with the topic they want
covered, it's really easy. So today we're going to talk about home team advantage. It's a thing.
It exists. The home team wins more. In the NFL from 2002 to 2019, 57 percent of the wins went
to the home team. The home team wins 57 percent of the time. That is statistically significant
because these are the same teams, the same franchises, just playing each other in different
locations. It should be 50-50. Seven points. That matters. Why? Why does it exist? And everybody
has their theory. You know, it's just the excitement of the crowd being on your side,
the majority of the people in the stands being on your team. Somebody pretty prominent in the
football world says that it's because the away team offense can't hear the snap count. I mean,
yeah, that sounds good, but that doesn't explain why the home team advantage exists in the NBA,
where it's even more pronounced. I want to say it's 60 percent there.
Could be environmental reasons. You know, a team from Denver is going to have a hard time playing
down here in the swamp because of the differences in temperature and elevation. A team from Florida
is not going to do well in the snow. They're not used to it. It's different. They have to step
outside of their comfort zone. It could be the refs. Some kind of implicit bias that leads them
to go in favor of the home team. If you look at 538, they say that 14 of the last 15 controversial
calls went for the home team. So even though the rules are the same at every stadium,
way they get enforced, well, that might be different. Or it could just be that the away
team, the traveling team, well, they had farther to go just to get to the playing field.
Makes sense. And these are things that could apply across different types of sport
because of this advantage. It exists. It's real.
So let's not call it home team advantage. Let's call it home team privilege.
Wow, all that other stuff just transfers right on over.
Because of environmental factors, as far as access to education, access to different stuff,
access to different stuff, well, it makes it harder. They have further to go
just to get to the starting line, just to get to the playing field.
Implicit bias, you know, that's one of those things. There are no racist laws on the books
today. And that could be argued. But even if the rules are the same everywhere,
maybe they're applied differently. Maybe there's an implicit bias.
Maybe it's just the fact that the majority of the people are rooting for you.
Alters your state of mind. I don't think there is one answer to this question as far as home team
advantage or privilege of any kind. It's a systemic thing. It's something that exists,
that's there, and it's probably a whole bunch of different factors all working together to
create this outcome. And if you want to fix it, you're going to have to do what the NFL's doing.
As far as the bias in the referees, they're talking about putting like
tracking chips in footballs to know if something's out of bounds or the snap and all of this stuff.
They're trying to find a way to alleviate the human component there, that bias.
Maybe they'll allow more rest time.
We're willing to mitigate it for a football game,
football game, but not for people's lives. That seems like we may have our priorities a little
mixed up. And it's also an interesting analogy because it carries over in another way.
If you are a fan of a particular team and that team wins the championship,
well all of a sudden you have bragging rights for something that you had nothing to do with.
You get to feel superior than those other fans for something that had nothing to do with you.
In this case, the sports analogy makes sense.
Privileges of all kinds exist. They're real. You can't deny that they're out there.
They permeate our entire society and they probably always will to some degree.
But when the outcomes are as different as they are in reality,
when the outcomes are as different as they are,
it's probably worth addressing. It's probably worth trying to mitigate
and see if we can get to a game that's a little bit more fair.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}