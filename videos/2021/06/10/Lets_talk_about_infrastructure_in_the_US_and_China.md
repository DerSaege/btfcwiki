---
title: Let's talk about infrastructure in the US and China....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SH5NI1IogPU) |
| Published | 2021/06/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the new economic landscape in the United States and the world.
- Critiquing the "America First" mentality and its impact on the country's global standing.
- Addressing the desire for every economic deal to favor the United States.
- Pointing out the unrealistic expectations of bringing back outdated jobs due to automation.
- Criticizing politicians who perpetuate false promises about job resurgence.
- Analyzing China's Belt and Road Initiative and its massive scale involving numerous countries.
- Contrasting China's substantial investment in global infrastructure with the US's comparatively limited spending.
- Advocating for a foreign policy centered on trade and development over military interventions.
- Noting China's soft power influence through economic partnerships and infrastructure development.
- Suggesting the US could capitalize on China's shortcomings by promoting green energy solutions and addressing environmental concerns.
- Warning that continued neglect of global economic shifts could lead to China surpassing the US.
- Stressing the importance of investing in infrastructure to boost economic competitiveness.
- Comparing the US's infrastructure spending proposal with other countries' investments in opening markets.
- Emphasizing the need for robust infrastructure to enhance the US economy's growth potential.

### Quotes

- "There's no long-term planning, just every deal has to be the best and the U.S. has to come out on top on every single one of them."
- "Those who are shouting America first at the top of their lungs will be the reason soon it will be China first."
- "Trade is what generates economic activity. That's where the money comes from."
- "The US economy is slow in part because we don't have the infrastructure to speed it up."
- "It's just a thought. Y'all have a good day."

### Oneliner

Beau examines the consequences of "America First" rhetoric on global economics, advocating for strategic investments in infrastructure to ensure competitiveness against China.

### Audience

Economic analysts, policymakers

### On-the-ground actions from transcript

- Push for increased investment in infrastructure projects in your community (suggested)
- Advocate for a foreign policy focused on trade, development, and humanitarian efforts (suggested)

### Whats missing in summary

Insights into how prioritizing infrastructure investments can bolster economic competitiveness and counter global challenges.

### Tags

#EconomicAnalysis #InfrastructureInvestment #GlobalEconomics #TradePolicy #USCompetitiveness


## Transcript
Well, howdy there Internet people. It's Beau again.
So today
we're going to talk about the new world out there
the new economic situation. It's not really new
but it's becoming more pronounced
and we're going to talk about how those who
like to use the United States as standing
in the
global economic scheme as a talking point
and
how those who scream
America First
may be the reason
that the United States isn't going to be first
for much longer.
Those
who want to
use that chant
when they start talking about economics, what do they want, really?
They want to be told that every deal, every transaction
came out in the United States' favor.
That's what they want to be told.
Every single incident
is transactional.
There's no long-term planning, just every deal has to be the best and the U.S. has
to come out on top on every single one of them.
That's what they mean.
And they mean that they want
jobs to come back to the United States.
And by that they mean
a bunch of jobs that don't even exist anymore because they've been phased out by automation.
And the politicians who pander to that crowd,
they allow them to believe that those jobs do exist and one day if they just vote
for them long enough
they'll come back.
And they don't take the time to explain that that transactional nature
on the international scene
doesn't always work out the way people might think.
The big boogeyman right now
is China.
When you talk about global economics in the United States,
that's the competitor.
Those who scream
America first,
do you think they ever looked
to see what China was doing and how it was making the gains it was?
Probably not.
I mentioned
the Belt and Road Initiative in a video recently. I got a whole bunch of questions about it.
Short answer, what is it?
It's a massive infrastructure project.
And by massive I mean sixty, seventy countries involved.
And the idea
is for China
to subsidize or pay for or loan money
to
develop infrastructure in other countries
to open up those markets
so trade increases and China makes more money.
Because infrastructure is necessary
for trade.
Estimates on what this is going to cost China over the next seven years,
remember that number,
seven years,
ranges from 1.2 trillion
to 8 trillion.
The reason there's such a giant gap
is really how it's figured.
The nearest I can tell it's probably around three.
But that is not my
area.
But at the bare minimum,
1.2 trillion.
It's the lowest estimate I've seen
for infrastructure
in other countries
over seven years.
China's not the only country doing it.
Russia,
they are trying to do something kind of sort of similar in a way
with the Eurasian Economic Union.
And there's been talk
of tying these two things together.
Infrastructure.
Those who scream America first in the United States,
the politicians that back that crowd and pander to that crowd,
what do they want to spend
on infrastructure in our own country?
Half of that 1.2 trillion over eight years,
not seven.
Six hundred billion.
Who do you think is going to come out ahead
seven years from now,
eight years from now,
based on those numbers,
based on that investment?
And that's in the United States.
They want to do the bare minimum.
And they want to do that
because it's better for those people who
help fund their campaigns.
That culture war
allows them to motivate their base.
There's no reason to do it
if you actually care about the health of
the United States economically.
Infrastructure is a necessary thing.
It facilitates trade. Trade is what generates economic activity. That's where
the money comes from.
But it's a whole lot easier just to shout America first.
In addition to the money,
China is exercising soft power.
See, when they open up these markets,
they gain influence in these countries
because you don't want to
become confrontational militarily with a trading partner. That interrupts
business.
And the
wealthy class in every country, well, it's pretty much the same.
They all have the ear of the politicians.
Nobody wants to interrupt
the flow of money.
So they gain influence.
The United States could do this.
It could have a foreign policy based on trade
and
development and humanitarian
efforts
rather than military.
And China's making a lot of errors in this.
They're leaving openings for the US all over the place.
The Belt and Road Initiative,
it has huge environmental issues,
human rights issues,
the debt
that is required.
Some of these countries, they're not really wanting to do it.
But there's no real competitor.
See, those environmental issues, they're putting in like coal plants and stuff like that.
The US could step in
and say, you know, we could do a
renewable, we could do green energy for you.
Of course, it's hard to sell that
when half the politicians in the United States
are pretending it doesn't work
to win some culture war and own the libs.
Those who are shouting
America first at the top of their lungs
will be the reason
soon it will be China first.
Because they're so busy
shouting
that the United States is the greatest country on the planet,
they stopped paying attention to what was going on in the rest of the world.
We're not competitive economically
and if our infrastructure falls behind,
it will take a long time
to get to that point,
to get back to where we were.
The six to eight hundred billion dollars
that the Republicans are throwing out for this infrastructure bill,
yeah, sure,
I mean that's needed, that's a good thing,
but you need to compare that
with what other countries are willing to spend
for infrastructure
in separate nations, just to open up markets.
The US economy is slow in part
because we don't have the infrastructure
to speed it up.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}