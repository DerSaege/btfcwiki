---
title: Let's talk about 1 in 5 thinking Trump will be reinstated....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9Wh0YFFCwT8) |
| Published | 2021/06/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- One in five Americans believes in a theory that claims former President Trump could be reinstated through audits.
- There is no legal or factual basis for this theory.
- Even if audits showed discrepancies, the process to reinstate Trump is not feasible.
- The theory serves as a tactic to keep the base engaged and energized for the upcoming midterms.
- Leaders are aware of their constituents' gullibility and exploit it for financial gain.
- Continuing to push baseless theories erodes the foundations of representative democracy.
- Education on critical thinking and questioning beliefs is vital to combat misinformation.
- This situation should be a wake-up call regarding the importance of critical thinking skills.

### Quotes

- "That's not real. That's not going to happen. That's not how this works."
- "They're being played because those in positions of leadership within this party, within the conservative movement, know that a large percentage of their constituents are gullible."
- "Continuing to be pushed by these same types of people are undermining the very foundations of representative democracy in this country."

### Oneliner

One in five Americans believe in a baseless theory of Trump's reinstatement, undermining democracy and exposing gullibility, urging critical thinking education.

### Audience

Voters, educators, activists

### On-the-ground actions from transcript

- Educate others on critical thinking skills and encourage questioning beliefs (suggested)
- Remain vigilant against misinformation and baseless theories (implied)

### Whats missing in summary

Importance of staying informed and combatting misinformation through critical thinking and education.

### Tags

#Democracy #Misinformation #CriticalThinking #Education #Trump


## Transcript
Well howdy there internet people.
It's Beau again.
So today we are going to talk about a theory that apparently one in five Americans believes
is possible.
It's been said that after a chain of audits concerning the election that it is possible
that former President Trump becomes reinstated like he's an insurance policy sometime in
August.
Let's just start with this.
That's not real.
That's not going to happen.
That's not how this works.
It's not how any of it works.
It's just not real.
There is no legal or factual basis for this to have been said.
But one in five Americans apparently believes it.
Believes that it's possible.
Possibly, somewhat likely.
Okay.
That's not a thing.
It's not going to happen.
But let's just play the game for a second.
Let's say the half-baked audit does actually show something and then it shows another.
There's another audit in another state and dominoes fall and eventually there's enough
so Biden didn't get the number of electoral votes needed.
Let's pretend that all of that happens first, which realistically by the time that worked
its way through the courts it would be after Biden's second term.
But let's just say that it does.
The sitting president doesn't just disappear and the other person go into office.
That's not what happens.
More than likely if something like that was proven, the chain of events would lead to
an impeachment and removal of President Biden and Vice President Harris.
At which point you have President Pelosi.
There is no way for Trump to be reinstated.
This is just them moving the football.
It's Lucy and Charlie Brown.
They're trying to keep this base engaged to continue making money off of them and hopefully
keep them energized until the midterms.
That's what's happening.
They're being played because those in positions of leadership within this party, within the
conservative movement, know that a large percentage of their constituents are gullible.
We just talked about it.
There was just a study showing that they have trouble differentiating fact from fiction.
That's what's happened.
That is the most likely answer to this theory.
That's how it came about.
It's not real.
And these theories continuing to be pushed by these same types of people are undermining
the very foundations of representative democracy in this country.
That's what's happening.
Eventually, these people will wake up to it, but we are so far into it and they still haven't.
If there's anything that should be a wake-up call when it comes to how we are educating
people and teaching them critical thinking skills and encouraging them to question their
own beliefs, it should be this.
It's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}