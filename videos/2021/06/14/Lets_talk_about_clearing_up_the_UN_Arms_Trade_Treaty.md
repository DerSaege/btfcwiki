---
title: Let's talk about clearing up the UN Arms Trade Treaty....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yH2nqyfi1Ig) |
| Published | 2021/06/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the misinformation circulating regarding the UN Arms Trade Treaty and the Biden administration's alleged plans to re-sign it.
- Clarifies that the treaty regulates illegal international transfer of conventional arms, not American citizens buying guns domestically.
- Trump's decision to "unsign" the treaty was purely political as the treaty was never ratified.
- Emphasizes that the treaty does not impact the majority of Americans unless they are involved in illegal international arms sales.
- States that the treaty affirms a country's sovereign right to regulate arms within its territory and does not involve the UN coming to enforce it.
- Speculates that the treaty is back in focus due to the Democrats being in power, not because it affects domestic gun ownership.
- Points out that the treaty covers a range of conventional arms, from rifles to tanks, focusing on illegal international transfers.

### Quotes

- "It has somehow been suggested that the Biden administration plans to re-sign a treaty from the UN."
- "This whole thing is a talking point designed to make people believe that somehow this treaty from the UN has something to do with Americans buying guns."
- "So much so that in the treaty it says that it reinforces and affirms the sovereign right of any state to regulate and control conventional arms exclusively within its territory."
- "Trump unsigning it did absolutely nothing. That's not an actual diplomatic thing."
- "Realistically, this applies to all conventional arms. So rifles and pistols, all the way up to tanks and aircraft."

### Oneliner

Beau clarifies the misconceptions around the UN Arms Trade Treaty, stressing its focus on illegal international arms transfers and its lack of impact on domestic gun ownership.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Contact policymakers to advocate for accurate information dissemination about international treaties (suggested).

### Whats missing in summary

Beau's detailed explanation and debunking of misinformation around the UN Arms Trade Treaty can be best understood by watching the full transcript.

### Tags

#UNArmsTradeTreaty #Misinformation #GunRegulation #InternationalArmsTransfers #PolicyAnalysis


## Transcript
Well howdy there internet people. It's Beau again.
So today we are going to talk about a treaty.
An unsigned treaty that may be re-signed I guess.
I don't know.
Somewhere through the right-wing information mill
it has somehow been suggested that the Biden administration
plans to re-sign a treaty from the UN.
It's called the UN Arms Trade Treaty.
There were a whole bunch of messages sent about it.
Some just asking what it is and some people wondering
if their uncle who thinks this means the UN is coming is right.
Okay so let's go back to where all of this started
to answer what it is.
We'll start there.
It's an arms trade treaty.
Exactly how it sounds.
It regulates conventional arms.
Specifically the illegal international transfer
of conventional arms.
That's what it's about.
Now a while back Trump said, and I quote,
so in the last administration President Obama
signed the UN Arms Trade Treaty
and in his wanting days he sent the treaty to the Senate
to begin the ratification process.
This treaty threatened your subjugate
and you know exactly what's going on here,
your rights and your constitutional
and international rules and restrictions and regulations.
I don't know what that means.
But at the end of the day what he was saying
was that he was unsigning this treaty.
And that's all fine and good.
Except that it was never ratified.
So it was never actually in any way.
This whole thing is a talking point
designed to make people believe
that somehow this treaty from the UN
has something to do with Americans buying guns.
It doesn't.
It doesn't flat out.
In order for the treaty to come into
any kind of effect whatsoever
the arms sales have to be international and illegal.
So if this treaty was to be ratified
and truly be enforced and everything,
it does not stop you from going up to Guns R Us
and buying the newest things so you can look cool at the range.
Has nothing to do with that.
If you wanted to fill out the paperwork
and import a bunch of UGO Mausers or something,
doesn't have anything to do with that.
It has to be illegal and international.
Has nothing to do with private ownership whatsoever.
So how might this apply to an American?
If you, I don't know, owned a compound in Idaho
and worked out some deal with some guy in Germany
to send you a bunch of H&Ks off the books
and label it farm implements,
then it might have something to do with you.
If you bought a bunch of guns in Arizona
and drove to Mexico to sell them,
then it might have something to do with you.
Unless you are already involved
in some form of illegal international arms sales,
it just does not apply to you.
So much so that in the treaty it says
that it reinforces and affirms the sovereign right
of any state to regulate and control conventional arms
exclusively within its territory
pursuant to its own legal or constitutional system.
That's actually in the treaty.
In the treaty it says that the treaty cannot regulate stuff
within a country.
So it has to be illegal and international.
And no, there is no, anywhere in it,
there's no enforcement where the UN is coming.
That's not happening either.
My guess again is that this is coming up
because the Biden administration
may want to restart the ratification process.
Trump unsigning it did absolutely nothing.
That's not an actual diplomatic thing.
It was a political move to appeal to the NRA types
who never read the treaty and have no idea what it's about.
This does not impact 99.99999% of Americans.
Why it's coming up again?
Because Democrats are in power.
So of course the UN is now going to come for your guns.
Realistically, this applies to all conventional arms.
So rifles and pistols, all the way up to tanks and aircraft.
That's what it has to do with.
And it's the illegal international transfer.
That's what it's about.
Has nothing to do with you going up to Walmart
and buying a rifle.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}