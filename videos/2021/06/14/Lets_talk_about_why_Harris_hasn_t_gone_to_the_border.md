---
title: Let's talk about why Harris hasn't gone to the border....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=khqp8HXLIzg) |
| Published | 2021/06/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing why Vice President Harris hasn't visited the border, Beau visualizes the Northern Triangle and explains the dynamics.
- Beau criticizes the notion that Vice President Harris needs to physically go to the border with binoculars, dismissing it as a PR stunt.
- The idea of leadership is questioned, with Beau asserting that true leadership involves addressing root causes and implementing effective solutions.
- Beau expresses mixed feelings about immigration, recognizing the talent and initiative of those making the journey while also acknowledging the negative impact of brain drain on their home countries.
- Criticism is directed at Vice President Harris for her messaging on discouraging migration and the belief that aid alone is insufficient to address the underlying issues.
- Beau advocates for prioritizing trade, investment, and infrastructure development in the Northern Triangle over traditional aid, drawing parallels with China's Belt and Road Initiative.
- A call to shift away from an "America First" mindset towards a more globally engaged approach is emphasized, stressing the need to support self-sufficiency and growth in other countries.
- Critiques are made of past U.S. foreign policy actions that have contributed to the challenges in the Northern Triangle, suggesting a shift towards constructive engagement rather than intervention.
- The transcript concludes with a reflection on the importance of trade, infrastructure, and investment in enabling countries to thrive independently.

### Quotes

- "They don't need aid. They need trade."
- "Beyond our borders do not live a lesser people."
- "Maybe it's time to stop being the world's policeman and start being the world's EMT."
- "It isn't fair to the rest of their country."
- "They have nothing left to lose."

### Oneliner

Beau visualizes the Northern Triangle, criticizes PR stunts at the border, and advocates for trade over aid to address root causes of migration.

### Audience

Global citizens, policymakers

### On-the-ground actions from transcript

- Advocate for policies prioritizing trade, investment, and infrastructure in countries like Honduras, Guatemala, and El Salvador (implied)
- Support initiatives that enable self-sufficiency and growth in communities facing migration challenges (implied)
- Educate others on the importance of shifting from aid-based approaches to sustainable trade partnerships (implied)

### Whats missing in summary

Deeper insights into the historical context and impact of U.S. foreign policy in the Northern Triangle region.

### Tags

#Immigration #ForeignPolicy #Trade #Infrastructure #RootCauses


## Transcript
Well howdy there internet, Big Lisboa again.
So today we are going to talk about why Vice President Harris hasn't gone to the border,
because that's apparently like a super important question.
She needs to go down there with a pair of binoculars or whatever.
People have asked, so I'm going to try to answer this question with a visualization,
because I'm not sure it would work in any other way.
This is the Northern Triangle.
So this is Honduras, Guatemala, and El Salvador, the bottom.
People inside, those are people.
The water is people.
The floor, well that's the US border.
This is Vice President Harris going to the border.
This is her going to the Northern Triangle, theoretically.
Not really, and we're going to get to that too.
The Vice President going to the border, that's nothing.
That's not leadership, that's not getting anything done.
That's a PR stunt worthy of Ted Cruz to suggest that the Vice President of the United States
needs to go down to the border with a pair of binoculars or whatever is ridiculous.
Not just are you saying that Border Patrol and USCIS and everybody else, they're apparently
incompetent and can't write a report, but that she needs to go there in person to see
it because she can't look at footage, I guess.
I don't know.
Those making this claim are so used to faulty leadership, to just shows that are designed
to make them think that the person cares, that they have now started to confuse that
with actual leadership.
She went down south.
She went down there and was trying to figure out a way to actually tip the bottle up, to
actually shift the tide there, to slow it down.
I have a whole lot of mixed feelings about this, to be honest.
On one hand, I truly believe that those who uproot themselves and make that trip, I truly
believe those are the best and brightest.
Those are the people with initiative.
Those are the people we want in this country.
I honestly believe that.
At the same time, we can't continue to rob their talent.
We can't continue to take those countries.
We can't take their best and brightest.
We can't take those that have the most initiative there.
That's not right because then the conditions in those countries just continue to deteriorate.
Now she went down there and she made some mistakes, in my opinion.
That whole do not go thing, do not come, stay at home, all of that, yeah, no.
The Vice President of the United States went down there and she said flat out that people
are fleeing harm or are unable to get the basic needs they need to sustain themselves.
Those sound like super legitimate reasons to leave.
Those sound like grounds for asylum to me.
They have nothing left to lose.
That was a mistake.
I don't believe that should have been said.
Now the idea of going down there and offering aid, historically that hasn't worked.
Aid itself, that's not going to solve the problem.
That's not going to stand the bottle back up.
They don't need aid, they need trade.
They need infrastructure.
That's what they need.
Handing a bunch of money to politicians that are going to trickle down and you know it's
corrupt.
Most of it isn't going to get to where it needs to be anyway.
They need investment.
They need infrastructure.
They need trade.
Man, that might open up new markets.
Sounds a whole lot like what China's doing with the Belt and Road Initiative.
Do you want to remain aggressive, competitive on the international stage?
We're going to have to get out of this America First mindset.
This mindset that we only worry about what happens at our borders.
Beyond our borders do not live a lesser people.
They need help.
They don't need aid.
Handouts.
They need trade so they can become self-sufficient.
So those people who have that initiative, that talent that keeps fleeing the country
to come here so they can stay there, so they can improve their country.
Now I want them here to be honest, but it isn't fair to the rest of their country.
There are a lot of issues that are down there that are our fault, that are the fault of
the United States, either through the war on drugs or through our interventions or through
unfair business practices that we condoned and sometimes enforced with military force.
A lot of the issues down there are the fault of the United States' foreign policy.
Maybe it's time to stop being the world's policeman and start being the world's EMT,
or in this case, the world's construction worker.
They don't need aid.
They need trade.
They need infrastructure.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}