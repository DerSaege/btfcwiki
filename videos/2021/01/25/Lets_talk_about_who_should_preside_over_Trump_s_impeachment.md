---
title: Let's talk about who should preside over Trump's impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-5UGUTUCdpI) |
| Published | 2021/01/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the procedural aspects of the second impeachment process.
- Clarifies that Chief Justice Roberts will not preside over the impeachment of a former president.
- GOP is making a fuss about the absence of Chief Justice Roberts, implying a lack of seriousness in the trial.
- Emphasizes that the Constitution grants the Senate the sole power to try all impeachments.
- Points out that the impeachment is not of the current president but a former one.
- Criticizes the GOP for attempting to undermine the impeachment process and the Constitution.
- Suggests Vice President Harris as a logical choice to preside over the impeachment.
- Notes the importance of accountability to prevent continued undermining of democratic processes.
- Draws parallels between GOP actions and their behavior regarding the election.
- Warns about the consequences of undermining the Constitution for political gains.

### Quotes

- "If nobody's held accountable, they will learn nothing, and this will continue."
- "The Republican Party has learned nothing, and this is why the impeachment is so important."
- "This is yet another attempt to undermine the very foundations of this country."
- "You need to stop pretending that you care about the Constitution and just admit what you are."
- "If they continue with this, their political futures are over, not because they're going to get voted out, but because the country will fall apart."

### Oneliner

Beau explains the procedural aspects of the second impeachment, criticizes GOP for undermining the process, and warns about the consequences of their actions.

### Audience

Citizens, voters, activists

### On-the-ground actions from transcript

- Hold elected officials accountable for their actions (implied)
- Support accountability and transparency in the political process (implied)
- Advocate for upholding democratic values and the Constitution (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the procedural aspects of the second impeachment, along with a strong criticism of the GOP's attempts to undermine the democratic process and the Constitution. The full text also warns about the potential consequences of unchecked political actions.

### Tags

#Impeachment #GOP #Constitution #Accountability #Democracy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some procedural things going on with the second impeachment.
We're going to talk about who's going to preside over the second impeachment of a president.
Not the president.
The president is President Biden.
Trump is a president.
A former president.
Okay, Chief Justice Roberts is not going to preside over it.
The GOP is making a big deal out of this.
House Judiciary GOP tweets, if Chief Justice Roberts doesn't preside, the American people
will know this isn't a serious impeachment trial.
I don't know what other kind of impeachment trial exists.
They all are pretty serious.
So the idea is that if the Chief Justice isn't presiding over the impeachment of a former
president, well then it's not real.
You know where we should go to look to find out about that?
The US Constitution.
That'd be a good spot.
Probably source material there.
So what does it say in Article 1, Section 3, Clause 6?
Maybe.
The Senate shall have the sole power to try all impeachments.
When sitting for that purpose, they shall be on oath or affirmation.
When the President of the United States is tried, the Chief Justice shall preside.
Okay.
So the reason Chief Justice Roberts is not presiding is because he's not required to.
Because this isn't the impeachment of the president.
It's an impeachment of a president.
And I know that sounds like me just kind of playing semantics, right?
Be really helpful if there was somebody on Trump's side who has already made this distinction,
right?
To prove that it exists.
So somebody we've talked about a lot lately is a guy named Howley.
There's only one constitutional process for impeachment, and it is of the president, not
a president.
He understands the difference.
He's pointed it out himself.
I would point out that he's wrong in the beginning of this.
Impeachment can be used against the president or any civil officer, to include judges, which
has happened.
You would think he would know that.
He probably does.
He's probably attempting to, I don't know, confuse the American people, undermine the
Constitution.
Maybe that's what he's trying to do, or maybe he just doesn't understand.
So who should preside?
The logical choice would be the president of the Senate, which would be Vice President
Harris, which incidentally, that would make sense as to why the chief justice should preside
over impeachment of the president.
Because otherwise, it might fall to their vice president.
And that might be a conflict of interest, right?
Might make sense.
The argument can be made, I don't think it's a good one, but the argument can be made that
there's also a conflict of interest with Vice President Harris.
I don't believe that, but the argument's there.
So to get around that, they could just use the most senior member of the Senate to preside
over it, which is exactly what they're doing.
The Republican Party has learned nothing, and this is why the impeachment is so important.
They are once again attempting to undermine the U.S. Constitution, undermine faith in
the democratic process, because nobody's been held accountable for the last time they did
it.
These tweets, this fake outrage, this uproar, this, oh, it's fake, it's not real, it sounds
a whole lot like what they said about the election, which is what got us into this mess
to begin with.
They have learned nothing.
If nobody's held accountable, they will learn nothing, and this will continue.
They will continue to undermine the Constitution because they do not care about what happens
in this country.
They care about their own political futures, and that's it, and they are too short-sighted
to understand that if they continue with this, their political futures are over, not because
they're going to get voted out, but because the country will fall apart.
This rhetoric, it's the same thing Trump used.
This is how all of this started.
Pretending something that is normal and something that is within the Constitution is somehow
outside of it, is somehow wrong, is somehow, well, this is just a reason not to believe
in it, because they're okay with lying to their supporters.
They're okay with presenting half-truths.
They're okay with manipulating things.
Understand that if Chief Justice Roberts was required to preside over the impeachment,
he wouldn't have a choice.
That's what the word required means.
If he was constitutionally bound to do it, he would have to.
He's not.
That's the whole point, because Trump is not the president.
He's a president.
He's a former president.
Now the argument about whether or not you can impeach somebody after they left office,
it has been done.
There's historical precedent for it in the past.
That's an argument they could make in good faith.
They can actually pretend to believe that that's not right.
I would disagree, but it's a sound argument.
This isn't.
This is just more manipulation.
This is yet another attempt to undermine the very foundations of this country.
If you continue to support the Republican Party as they engage in this and what they've
done over the last year, you need to take that We the People banner off of your social
media.
You need to stop pretending that you care about the Constitution and just admit what
you are.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}