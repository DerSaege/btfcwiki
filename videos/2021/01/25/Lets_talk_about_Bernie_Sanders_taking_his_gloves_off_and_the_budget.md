---
title: Let's talk about Bernie Sanders taking his gloves off and the budget....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cRHTHd9PPGU) |
| Published | 2021/01/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans are blocking the stimulus on Capitol Hill, despite Democrats having a slim majority in the Senate.
- Bernie Sanders is the chair of the budget committee and could use the reconciliation process to pass the stimulus with a simple majority.
- Reconciliation doesn't require 60 votes and can't be filibustered.
- Sanders has promised to use this procedural method to get the stimulus through without passing a new package.
- This process could also potentially adjust the minimum wage.
- McConnell won't be able to stop this process, but he may criticize Sanders for his past opposition to it.
- The House will also go through a reconciliation process after the Senate.
- The debate time in the Senate for reconciliation is limited to 20 hours, and the process between the House and Senate is limited to 10.
- This process can't be used for everything but can be pivotal in passing much of the stimulus package.
- Sanders needs support from the Democratic Party to follow through on this.

### Quotes

- "Bernie Sanders promised to take his gloves off yesterday."
- "Whether or not this is what the founders intended on how it's supposed to work, that's up for debate, but it is certainly how it works right now."
- "I just hope that the Democratic Party gives Sanders the backup that he needs."

### Oneliner

Republicans block stimulus, Sanders eyes reconciliation to pass it, needing Democratic support.

### Audience

Politically engaged individuals.

### On-the-ground actions from transcript

- Support Sanders in using the reconciliation process (implied).
- Push for Democratic backing of Sanders (implied).

### Whats missing in summary

The full transcript provides a detailed explanation of how budget reconciliation works and the potential implications of using it to pass the stimulus package.


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about
a showdown on Capitol Hill. Republicans have decided that they don't want to let the stimulus
through. Okay. I know people are saying, but Democrats won't control the Senate. Kind of.
They have 50 votes plus Vice President Harris makes 51. To get most anything passed, you need 60.
So Republicans are going to attempt to stop this. So it has turned into a heavyweight boxing match.
In one corner, you have Mitch the Obstruction McConnell, and in the other corner,
you have Bernie the Mitton Sanders. And Sanders promised to take his gloves off yesterday.
So, back in 1974, Congress created a method to fine-tune the budget.
Fine-tune mandatory spending is what it really gets used for. Mandatory spending includes things
like aid, entitlement programs, stuff like that. All the stuff that the stimulus package would
kind of go under. The person who kind of gets to run this process called reconciliation
is the chair of the budget committee in the Senate, which is currently Bernie Sanders.
Using this process, it doesn't require 60 votes. It requires a simple majority,
meaning all the Democrats have to do is not break ranks. Everybody votes for it.
Harris comes in and, if necessary, casts the tie-breaking vote. It also can't be filibustered.
I truly believe that Sanders has the political will to do this. In fact, I think he's been
waiting a really long time to do this. I do not know that all Democrats do, but we'll see what
happens. Basically, he has kind of promised to use a procedural way to get the stimulus through
without actually passing the stimulus package. They'll just fine-tune the budget and change the
floors on what is going to be spent. It can be used to do a lot of things. Some people are even
suggesting that it could be used to adjust the minimum wage. I'm not completely convinced on
that, but I mean, it's an argument worth making. So, if this process is used, there's really nothing
McConnell can do about it. I mean nothing, except complain that Senator Sanders is being hypocritical
because Sanders has opposed the use of this mechanism in the past. I would suggest that
getting people assistance in the middle of all of this is a little different than giving billionaires
tax breaks. Whether or not this is what the founders intended on how it's supposed to work,
that's up for debate, but it is certainly how it works right now.
It should be pretty interesting. So, once Sanders does it, the same thing will happen in the House.
They will provide their own reconciliation with the budget. The two different versions will meet
and they'll work out a compromise. In the Senate, the debate on doing this is limited to 20 hours
and the reconciliation process between the House and the Senate is limited to 10.
They really can't stall this very long. So, understand this process cannot be used for
everything. It can't be used to move normal legislation, but it can certainly be used
to accomplish a whole lot of that stimulus package that Biden wants.
I want to say it's 1.9 trillion dollars or something like that. So,
I just hope that the Democratic Party gives Sanders the backup that he needs,
because one of the things that has set the Democratic Party back over the years is not
wielding power when they haven't. They don't. They play civility politics.
I guess Sanders is just kind of tired of that. So, we'll see what happens, but that's a brief
overview of a term you're going to hear a lot of soon, I think, which is budget reconciliation.
That's how it works. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}