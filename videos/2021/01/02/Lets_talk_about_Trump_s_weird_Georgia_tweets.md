---
title: Let's talk about Trump's weird Georgia tweets....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JIJbtxuwyBc) |
| Published | 2021/01/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President tweeted inaccurate information undercutting his own message.
- Senate, under McConnell, overrode Trump's veto.
- Trump's tweet claimed Georgia election will be invalid, illegal, and unconstitutional.
- Trump likely lashing out against McConnell for Senate's veto override.
- Trump counting on Senate to secure election victory.
- If voter turnout is depressed in Georgia, McConnell may lose his job as Senate Majority Leader.
- Trump may realize he tied himself to Georgia election and will likely change stance to support Republican candidates.
- McConnell seems to be winning the power struggle against Trump.
- Trump's influence within the Republican Party appears to be diminishing.
- Trump loyalists and Trumpism will continue despite Trump's waning influence.
- Beau warns against becoming too sympathetic towards McConnell, reminding that he looks out for himself primarily.

### Quotes

- "He's not your friend."
- "He does not care about you or your problems."
- "Trump loyalists will still be around."
- "Trumpism will still continue."
- "Don't become too sympathetic."

### Oneliner

President tweets inaccuracies, Senate overrides veto, Trump undercuts own message, McConnell wins power struggle, Trump's influence wanes, but Trump loyalists persist.

### Audience

Political observers

### On-the-ground actions from transcript

- Stay informed on political developments and verify information (implied)
- Support candidates based on their policies and actions, not blind loyalty to a particular individual (implied)

### Whats missing in summary

Insights on the importance of remaining critical of political figures and being vigilant in evaluating their actions beyond surface level appearances.

### Tags

#Politics #Trump #McConnell #Election #Republican #Influence #PowerStruggle #Voting


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some more Game of Thrones type stuff happening up on
Capitol Hill, happening in DC.
Last night the President tweeted some wildly inaccurate stuff.
That's nothing new, we've come to expect that at this point in the game.
But people are wondering why.
Because this time he's undercutting his own message, it seems.
Just before the President went on his little temper tantrum on Twitter, the Senate under
Mitch McConnell overrode his veto.
Trump then gets on Twitter and says that the Georgia election will be invalid and illegal
and unconstitutional, none of which is true.
My best guess here is that he was lashing out against McConnell.
He has been counting on the Senate to kind of hand him the election.
Seems pretty clear from the override that that isn't going to happen.
So if he depresses voter turnout in Georgia, McConnell will probably lose his job and he
won't be Senate Majority Leader.
Now at the same time, Trump will probably realize sometime today, with y'all watching
this today, that he has already tied himself to this Georgia election and if they lose
it looks bad on him.
So expect another change and he will again support the Republican candidates in Georgia
and tell people to vote.
It appears, outside looking in, without any inside information, that McConnell is definitely
winning this little power struggle.
The idea of Trump remaining kingmaker after he leaves the Oval Office is less likely by
the minute.
The more erratic he becomes, the harder it is going to be to get Republicans to follow
his lead while he's in Mar-a-Lago.
I don't foresee him being much of a force within the Republican Party for much longer.
However, Trump supporters will still be around.
Trump loyalists will still be around.
And Trumpism will still continue.
It's something we need to be aware of and no matter how much you may find yourself rooting
for McConnell right now, please remember that it is Mitch McConnell.
He's not your friend.
The reason he has survived up on Capitol Hill for so long is because he looks out for himself,
number one.
He does not care about you or your problems.
So don't become too sympathetic.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}