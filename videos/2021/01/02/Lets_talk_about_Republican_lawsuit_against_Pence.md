---
title: Let's talk about Republican lawsuit against Pence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=o2Twny37Ul4) |
| Published | 2021/01/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Congressperson Gohmert and some Republicans filed a suit against Vice President Pence in hopes of having a judge declare Pence's sole authority over accepting or rejecting electoral votes.
- The case, like many others, was dismissed by the judge.
- The judge emphasized that certain events must happen before Pence could potentially reject electoral votes he doesn't like.
- Legal standing is vital in cases like this, and without it, winning becomes highly unlikely.
- Pence and the House requested the court to reject the suit.
- Even though the case was dismissed without prejudice, it could potentially be reframed and brought back before the court.
- Beau warns of wild claims as the January 6th date approaches from those who refuse to accept Trump's defeat.
- The outcome of the election seems unlikely to be altered by such claims.
- The Republican Party still has factions loyal to Trump or needing his base support, even as Trump's presidency comes to an end.
- The influence of Trump within the Republican Party is expected to persist, despite McConnell having the upper hand in their power struggle.

### Quotes

- "If you can't prove that you have standing, it's highly unlikely that you're going to win the case."
- "It's going to be very hard for them to even get standing."
- "There is still a faction of the Republican Party that is either loyal to him or desperately in need of his base."

### Oneliner

Congressman Gohmert's suit against Pence dismissed, revealing the importance of legal standing and the lingering Trump influence in the Republican Party.

### Audience

Political observers, Republicans, Democrats

### On-the-ground actions from transcript

- Keep informed on legal proceedings and political actions (suggested)
- Stay engaged with updates on election-related news and claims (suggested)

### Whats missing in summary

Full understanding of the legal intricacies and potential implications of the dismissed suit against Pence. 

### Tags

#LegalStanding #ElectionOutcome #TrumpInfluence #RepublicanParty #PoliticalAnalysis


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Congressperson Gohmert, Vice President Pence, and the suit.
If you don't know what I'm talking about, a Congressperson from Texas, Gohmert, and
some Republicans from Arizona filed suit in federal court in front of a Trump-appointed
judge against Vice President Pence in hopes of getting that judge to declare that Pence
has sole authority as to whether or not to accept or reject the electoral votes as certified
by the states.
As you might imagine, this case was dismissed along with all of the other cases that have
been brought.
And while it's entertaining that this happened, there is something in the ruling that I think
that we should kind of take note of.
The judge wrote, plaintiffs presuppose what the vice president will do on January 6th,
which electoral votes the vice president will count or reject from contested states, whether
a representative and a senator will object under section 15 of the electoral count act,
how each member of the House and Senate will vote on any such objections, and how each
state delegation in the House would potentially vote under the 12th amendment absent a majority
electoral vote.
If you have been hearing about this idea that Pence is just going to toss the votes he doesn't
like, those are the steps that have to occur before we get to that point.
And that's not even a point in the line, in the chart, that point doesn't exist.
But all of this has to happen first.
And it all has to go a certain way.
As the judge says, all that makes Congressman Gohmert's alleged injury far too uncertain
to support standing.
Which means what you're going to hear is that the judge didn't rule on the merits of the
case, just that the person who brought the case didn't have the legal standing to bring
it.
And this has become a fun talking point lately, I would just point out that if you can't prove
that you have standing, it's highly unlikely that you're going to win the case.
Just throwing that out there.
It should be noted that Pence asked the court to reject it, as did the House.
At the same time, that list, that list of events that has to go down just so for this
to even matter, that's worth noting.
Because you still have people in the Republican Party who either through actual loyalty to
Trump or a desire to perform for Trump's base are willing to not just entertain ideas like
this, but to file a suit in federal court.
Knowing that it's going to be rejected, Trump appointee or not, it's got rejected, I would
point out that the case was dismissed without prejudice.
Now what that means is that if Gohmert and company can reframe this argument in another
way, sometime in the next few days, it can go back before the court.
That seems unlikely, but it's possible.
So you will probably hear about that.
And as we draw closer to the 6th, there will be more and more wild claims that come from
those who cannot accept Trump's defeat.
So just be prepared for them.
I have yet to hear of any situation that seems even remotely plausible that would alter the
outcome of this election.
They all hinge on stuff like this.
It's going to be very hard for them to even get standing.
And that doesn't mean that the judges are dodging the cases.
That means that the attorneys couldn't even get their act together enough to prove that
they had the ability to sue.
So while it does appear that President Trump is headed out the door, it is worth noting
that there is still a faction of the Republican Party that is either loyal to him or desperately
in need of his base.
So they will pretend to be.
And this doesn't change simply because McConnell definitely has the upper hand in the Trump-McConnell
power struggle.
The effects of Trump inside the Republican Party are going to be felt for a while.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}