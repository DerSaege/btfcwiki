---
title: Let's talk about Trump changing the story this week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kdYl7feE0y4) |
| Published | 2021/01/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump plans to lean into a conflict with social media companies to distract from his failings.
- The purpose of the Bill of Rights is to protect individuals and states from the federal government.
- Private entities like Twitter are not obligated to platform President Trump; it's not a First Amendment issue.
- Trump's attempt to force a private entity to carry his message could be seen as a First Amendment violation.
- Trump's actions post-Capitol breach shouldn't overshadow the severity of allowing the Capitol to fall.
- Trump's tactics aim to manipulate media, emotions, and shift focus from his failures.

### Quotes

- "The president of the United States allowed the Capitol to fall. That's the story."
- "Don't allow yourself to be distracted by this. Don't get sucked into this."
- "He is very good at manipulating the media and manipulating people's emotions and changing the story."

### Oneliner

President Trump aims to distract from failings by sparking conflict with social media companies, but the real story remains: allowing the Capitol to fall.

### Audience

American citizens

### On-the-ground actions from transcript

- Stay informed and focused on the significant events rather than getting distracted (exemplified)
- Be vigilant against attempts to manipulate media narratives (exemplified)

### Whats missing in summary

The transcript delves into the importance of not allowing distractions to overshadow critical events like the Capitol breach and encourages staying focused on the truth amidst manipulation attempts.

### Tags

#Trump #FirstAmendment #SocialMedia #Distraction #CapitolBreach


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about what the president
apparently plans to do this week.
The president is currently Donald J. Trump.
It has been said, kind of indicated,
that he intends on leaning into this whole little thing
he has going on with the social media companies
as a way of changing the story, as a way of distracting
from the giant mess that he created.
He intends on framing it as a First Amendment issue.
So let's just go ahead and get out in front of all this.
What is the purpose of the Bill of Rights?
What was it designed to do?
Protect us, the people, either individually
or as a collective, or the states, either individually
or as a collective, from the federal government.
So if you happen to be a senator or representative
or, hypothetically speaking, the chief executive
of the federal government, it's going
to be really hard to genuinely frame something
as a First Amendment issue.
The goal of the Bill of Rights is to protect us,
the people, from the government, not protect the government
from the people.
None of these private entities have
to platform President Trump.
None.
That's not a thing.
That's not a First Amendment issue.
I don't know that Twitter actually
can violate someone's First Amendment rights on their own.
I would think they need the federal government or a state
government, somebody, to step in and compel them to do it.
I don't know that it's actually possible for them
to violate somebody's First Amendment rights.
As is usually the case, the president,
when he is talking about the Constitution,
appears to be so wrong that he has it literally backwards.
I would suggest that the president's attempt
to force a private entity to carry his message
is actually the First Amendment violation.
Put this into any other time period.
Take it out of today, where you have social media.
This would be the equivalent of the president requiring
every newspaper in the country to carry his message.
That's wild.
That sounds like something some two-bit dictator would want.
Doesn't sound like something that should
happen in the United States.
It's a private entity.
It does not have to carry the president's message, period.
Full stop.
As chief executive, he's going to have a real hard time
making this case legally.
But that doesn't mean he can't attempt
to create fake outrage about it and change the story,
distract from what happened.
At the end of the day, the president of the United States
allowed the Capitol to fall.
That's the story.
I would suggest that that's an important enough story
that it shouldn't be replaced by the president having
his feelings hurt because he can't tweet.
Those things don't seem to be on the same level.
Those things don't seem like the same level of importance.
Do not allow yourself to be distracted by this.
Don't get sucked into this.
He is very good at manipulating the media
and manipulating people's emotions
and changing the story.
The president of the United States
allowed the Capitol to fall.
That's the story.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}