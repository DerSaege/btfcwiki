---
title: Let's talk about Trump's Alamo farewell tour....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iaceur2Y3vA) |
| Published | 2021/01/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits
Beau says:

- President's final week in office discussed.
- Executive orders targeting big tech mentioned.
- President planning a farewell tour, including a stop in Alamo, Texas.
- Symbolism of the Alamo being pointed out.
- Focus of the tour on rehabilitating Trump's legacy after the coup.
- Doubts expressed about the president having self-awareness to change his image.
- Expectation of Trump creating situations to portray his supporters as victims.
- Anticipation of Trump being more vocal without Twitter.
- Reminder to not give attention to Trump's attempts to provoke outrage.
- Emphasis on focusing on facts like the Capitol events rather than Trump's words.

### Quotes
- "Don't give him that victory right at the end."
- "Do not give it to him. If you do, that becomes the story. That becomes what's remembered."
- "At the end of the day, do not remember the Alamo. Remember the Capitol."

### Oneliner
President's final week antics include symbolic tour; focus on facts, not provocations or distractions.

### Audience
Activists, Politically Engaged Individuals

### On-the-ground actions from transcript
- Focus on facts like the Capitol events (implied)
- Do not give attention to Trump's attempts to provoke outrage (implied)

### Whats missing in summary
Analysis of the implications of Trump's actions during his final week in office.

### Tags
#Trump #FarewellTour #Capitol #Legacy #Politics


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So today we're going to talk about the president's final week in office,
what he plans to do.
It does appear that the information we got last night that we talked about this
morning is accurate.
He does intend on leaning into some imaginary First Amendment battle.
It is said that he has some executive orders already prepared to go after big tech.
Ignore them.
They don't matter.
They do not matter.
Regardless of what they say, they probably will have precisely zero effect
with his remaining time in office.
Aside from that, he intends on engaging in a little farewell tour.
He will be going to Alamo, Texas.
I would like to point out that the Alamo is not in Alamo, Texas.
It's in San Antonio.
I doubt the Trump administration knows that, to be completely honest.
The symbolism is pretty apparent, the last stand.
And it's more fitting than he would imagine because it was
mostly a bunch of racist old white guys.
The only real difference is that at the Alamo, they were the illegal immigrants.
The tour's focus is going to be to rehabilitate Trump's legacy,
his image, in light of the coup.
That's the conventional wisdom.
I disagree.
I disagree.
In order for that to be true, the president would have to have some kind of self-awareness.
I don't believe that he does.
I don't think that's what we should expect.
I don't think we should expect him to come out and behave in a presidential manner
and attempt to change his image at the last moment.
I think that he's going to try to change the story.
He's going to try to create situations in which his supporters can still feel like
the people that are downtrodden, that they're the ones being attacked,
despite all video evidence to the contrary.
I think that's what we can expect.
He doesn't have Twitter anymore.
That's how he used to change the story.
He would say something wild on Twitter and the media would follow it.
Now, he's going to have to say it in person.
I would expect Trump unchained.
I would expect him to say some wild stuff and try to provoke outrage.
Do not give it to him.
If you do, that becomes the story.
That becomes what's remembered.
Don't give him that victory right at the end.
It doesn't matter what he says.
Ignore it.
Focus on the facts.
The Capitol fell, his behavior leading up to it.
Whether or not he should even be allowed to remain in office this last week or so.
That's the story.
That's what we have to focus on.
At the end of the day, do not remember the Alamo.
Remember the Capitol.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}