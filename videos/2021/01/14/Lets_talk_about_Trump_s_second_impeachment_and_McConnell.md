---
title: Let's talk about Trump's second impeachment and McConnell....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4x-lvlZiIZw) |
| Published | 2021/01/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President impeached again, heading to Senate.
- Internal power struggle in GOP between McConnell and Trump.
- McConnell could convict Trump to prevent future office runs.
- McConnell likely stalling Senate trial to blame Democrats.
- McConnell's aim is to make Trump irrelevant.
- Holding Senate trial after Trump leaves office is precedent.
- If Trump is convicted and appeals to Supreme Court, he may regain influence.
- McConnell taking a huge gamble with this strategy.
- McConnell hesitant to rush trial due to potential consequences.
- Trump likely to continue seeking attention and influence post-presidency.

### Quotes

- "McConnell could convict Trump at trial and make sure he can never run for public office again."
- "However, McConnell may be making a tactical error."
- "If Trump is convicted in the senate and then takes it to the Supreme Court and wins, he is a force to be reckoned with again."
- "Y'all have a good day."

### Oneliner

President impeached, McConnell's risky gamble could backfire, delaying Senate trial may empower Trump post-presidency.

### Audience

Political observers, activists.

### On-the-ground actions from transcript

- Monitor and stay informed about the impeachment process (suggested).
- Stay engaged with political developments and potential outcomes (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the potential strategies and risks involved in the Senate trial following President Trump's impeachment, offering insights into the internal dynamics of the GOP and McConnell's motivations. Watching the full transcript can provide a comprehensive understanding of the political implications at play. 

### Tags

#Impeachment #SenateTrial #GOP #McConnell #Trump #PoliticalStrategy


## Transcript
Well, howdy there internet people. It's Beau again.
So the president's been impeached again and it's gonna have to go to the senate again.
Trump promised us a presidency like no other. Promises made, promises kept right there.
So what happens next? It goes to the senate. McConnell is saying that he's not gonna rush it,
which seems odd, right? If you've been watching this channel you know there's an internal power
struggle inside the GOP for control of the party between McConnell and Trump. It would seem like
McConnell would just love to convict Trump at trial and make sure he can never run for public office
again. Makes sense. I mean what could be better than that? Stalling it and making the Democrats
do it. If he stalls it long enough the senators from Georgia will be seated. At the very least
he can stall it until Biden's in office and then he can blame it all on the Democrats.
So what happens next? He doesn't upset Trump supporters. Trump supporters do not really,
they're not known for thinking critically most times. They probably will miss out on the fact
that if McConnell wanted to defend the president he could rush it and say it was political in the
house, it's going to be political in the senate and push people to vote to acquit.
But he's not doing that because he wants him convicted. He wants him unable to run for public
office, make him irrelevant. But it is so much better for McConnell if the Democrats do it,
never mind the country. But it's better for McConnell and that's what he's going to go with.
However, McConnell may be making a tactical error. There is precedent to hold a senate trial
after the person leaves office. That's happened. Belknap is the example everybody's citing.
However, I would point out that it is unprecedented to do it with the president
and that it would likely end up in the Supreme Court, a Supreme Court that Trump made a whole
lot of appointments in. If Trump is convicted in the senate and then takes it to the Supreme Court
and wins, he is a force to be reckoned with again. It will re-energize his base and all of
McConnell's work goes up in smoke. This is a huge gamble on McConnell's part, unless he knows
something we don't know. So that's where we're at. That's why you are not going to see McConnell
want to push this, unless he starts to consider what happens after the vote, because there will
it's Trump. He's going to take it to court. I mean, that goes without saying, because it gets
him in the headlines and that's what Trump really cares about. Sadly, I do not think that Trump is
going to disappear after he leaves office. And a delay in the Senate trial is pretty much going to
guarantee that. He'll still be able to get headlines. He'll still feel like he is important.
And if he can overturn a Senate conviction, he will still maintain a lot of influence.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}