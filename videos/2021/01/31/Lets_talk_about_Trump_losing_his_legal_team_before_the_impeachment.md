---
title: Let's talk about Trump losing his legal team before the impeachment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tI38rS_djEM) |
| Published | 2021/01/31|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former President Trump is reportedly losing large portions of his legal team ahead of his second impeachment trial.
- The split occurred because the legal team wanted to present legal arguments, while Trump wanted to focus on baseless claims and complaints.
- There's a theory that the lawyers backed out due to concerns about fallout from representing Trump, but Beau disagrees.
- Despite the allegations against Trump, everyone is entitled to a defense in the United States.
- Attorneys are generally immune to guilt by association when representing clients.
- Trump seems to be using the impeachment proceedings as a platform for a potential future political career.
- His campaign tone could center around claims of the election being stolen from him if he runs again in 2024.
- Trump's camp appears to have become accustomed to losing, celebrating their losses.
- The argument made by 45 senators about impeaching a president after they've left office being unconstitutional is considered a loss.
- Some senators are more focused on their political futures than upholding constitutionality, as seen by their actions.

### Quotes

- "Even if you are alleged to have used the power of your office to foment a spirit of rebellion and then told them to go fight, you're entitled to a defense."
- "One of the current talking points is that 45 senators said that they believed impeaching a president after they're out of office is unconstitutional."
- "If politicians in general understood the constitution and applied it rather than attempting to safeguard their own political futures, the Supreme Court wouldn't have much to do."

### Oneliner

Former President Trump's legal team splits ahead of his impeachment trial, revealing his focus on baseless claims and potential future political ambitions, while celebrating losses and challenging constitutionality.

### Audience

Political Observers

### On-the-ground actions from transcript

- Pay attention to the legal proceedings related to Trump's impeachment trial (implied).
- Engage in critical thinking about the actions and statements of political figures (implied).
  
### Whats missing in summary

Detailed analysis of the potential impact of Trump's actions on the political landscape. 

### Tags

#Trump #Impeachment #LegalTeam #Constitutionality #PoliticalFuture


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the big news of this morning and that is that former
President Trump headed into his second impeachment trial is reportedly losing large portions
of his legal team.
The reporting suggests that the split has occurred because the legal team wanted to
present legal arguments related to the case and the impeachment and Trump wanted to complain
and whine about losing and rehash his baseless claims.
There's a theory that the lawyers backed out because they're worried about fallout from
representing Trump.
I don't buy that at all.
I don't believe that's true.
It's the United States.
Everybody's entitled to a defense even if you are alleged to have used the power of
your office to foment a spirit of rebellion and then told them to go fight.
Even if that's the case and that's what you're accused of, you're entitled to a defense.
Generally speaking, attorneys are immune to that kind of associative guilt.
Now what does this tell us?
This tells us that Trump still has at least one eye towards having a future political
career.
He wants to use the impeachment proceedings as a platform to set the tone for his next
run which the tone would be, you know, the election was stolen from me.
Give me another chance.
That's going to be the campaign if he runs again in 2024.
I would point out that the Trump camp has grown so accustomed to losing they are now
celebrating their losses.
One of the current talking points is that 45 senators said that they believed impeaching
a president after they're out of office is unconstitutional.
45 senators is a loss.
They lost at making this argument.
That's not a majority.
It is certainly not enough to get that passed.
They lost and they are celebrating this loss.
I would also point out that historically speaking when senators are more concerned about their
own political futures than the country, they tend to forget what constitutionality is.
This is evidenced by the fact that the Supreme Court has had a pretty full docket for the
last couple hundred years.
If politicians in general understood the constitution and applied it rather than attempting to safeguard
their own political futures, the Supreme Court wouldn't have much to do.
Just saying.
Those same senators are doing everything they can to make sure that evidence is limited
during the trial.
They are suggesting that we shouldn't hear from the people who were allegedly incited
by Trump during Trump's incitement trial because people who have those kind of theories and
believe that kind of stuff, well, they would just turn Congress into a circus.
If that's the case, why do we allow people who may believe in those theories to be members
of Congress?
Space lasers and all.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}