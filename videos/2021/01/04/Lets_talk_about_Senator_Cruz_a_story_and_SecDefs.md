---
title: Let's talk about Senator Cruz, a story, and SecDefs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=33TW4a59HTI) |
| Published | 2021/01/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Cruz plans to object to the electoral count, sparking heavy criticism for playing political theater.
- Democrats accused Cruz of committing treason for his actions.
- Beau recalls a story about a Marine turned deputy handling a shots fired call from a man with a handgun.
- The deputy de-escalates the situation with the man who believed in aliens, showing empathy and restraint.
- Beau points out that while the deputy's actions were commendable from a policing perspective, it was the wrong move psychologically.
- The man is taken to the hospital where he becomes agitated and violent towards the medical staff.
- Beau warns against playing into people's delusions, drawing parallels to Senator Cruz's actions.
- Ten former secretaries of defense release an open letter expressing concern about the transition, a situation not seen since the 1800s.
- Beau criticizes Senator Cruz for engaging in political theater at the expense of the country's well-being.
- He urges Cruz to change his rhetoric and stop undermining the foundational elements of the country for personal gain.

### Quotes

- "It's a bad idea to play into false beliefs because eventually they're going to figure it out."
- "You are undermining the foundational elements of this country for nothing, for headlines, for retweets."
- "It's not treason, but it is certainly not in the best interest of the country."
- "These little games, they have gone on long enough."
- "I am sorry that people said mean things to you, but I suggest that maybe you stop playing into the delusions of people."

### Oneliner

Senator Cruz's political theater undermines the country's well-being, echoing the importance of not playing into false beliefs.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Advocate for responsible and empathetic political leadership, exemplified
- Encourage public officials to prioritize the country's interests over personal gain, suggested

### Whats missing in summary

The full transcript provides a detailed analogy between a deputy's de-escalation tactics and Senator Cruz's political actions, illustrating the importance of responsible leadership.

### Tags

#SenatorCruz #PoliticalTheater #De-Escalation #ResponsibleLeadership #CommunityConcern


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about Senator Cruz
and a story I heard a while back and some secretaries of defense.
See, Senator Cruz has his little political theater planned. He's going to object to the electoral
count and all of that stuff and he's received some pretty heavy criticism because of his little plan.
He said that people needed to calm down, needed to tone down their rhetoric because it's a tender
box and they're throwing matches in it. Can you believe that some Democrats had the nerve
to say that he was committing treason?
Got to be honest, I felt bad for a minute because of my video.
Then I remembered a story I was told.
I wasn't there for this but I have no reason to believe it's not true.
Guy's a Marine, he gets out, moves back home and he is the definition of a good old boy.
Takes a job as a deputy. One of his first calls is a shots fired call. So he drives out to the
middle of nowhere to this house and when he gets there he sees this guy standing out in front of
the house looking at the tree line and he is holding a hand cannon. He's got nothing on.
The great investigative skills that you pick up at the academy, his theory was that somebody
attempted to get into the house. The homeowner woke up in the middle of the night, came out
waving that 357 around, scared him off. They're in the tree line. Sounds good. He gets out of the
car, starts walking up to the guy from behind. As he gets close he's like, everything all right?
Guy turns around, he's like, can you see him? Can you see him? See who? The aliens.
Yes I can. As a matter of fact, that's why I'm here. I'm here to back you up. He pulls his weapon
out and he keeps it pointed at the tree line. And he asks the guy, is there anybody here with you?
No. Well, you know, I get what you're talking about but these pistols are not going to do
anything. We need to get back to the station, get something a little bit more hot.
We need to get back to the station, get something a little bit more high powered. Why don't you come
with me? I don't know how to use anything else. We'll get you trained, get you some gear. We're
probably going to have to be dealing with this for a while. Okay. And they ease back to the car,
keeping their eyes and weapons trained on the tree line. Gets to the car and he's like, hey,
you know, I got stuff in the front seat here. Just hop in the back. Oh, you can't have that back
there. Here, give me that. Sits him down in there, closes the door. This was late 90s, early 2000s.
You know, we hear a story like that today. We'd be like, great, fantastic. That is amazing
de-escalation. Good for him. We need this cop to train other cops. And on some level,
yeah, that's true. But from a psychologist standpoint, that's the way wrong move.
So the deputy drives him to the hospital. When he gets to the hospital, the guy begins to
understand what's happening and flies into a fit of rage. Hurts one of the docs, hurts one of the
nurses. Winds up having to be restrained. When it's all said and done, this four foot nothing
psychologist comes up and starts poking this mountain of a man in the chest, telling him that
if he ever plays into the delusions of somebody again and then brings them to her hospital,
well, there is going to be the devil to pay. Because it's a bad idea to play into people's
delusions, Senator Cruz. It's a bad idea to play into false beliefs because eventually
they're going to figure it out. You have no idea what's going to happen.
It may also create a situation that is such a tinderbox that 10 former secretaries of defense
feel compelled to release an open letter because they're worried about the transition, a worry we
have not had in this country since the 1800s. It's not a good idea to play into the delusions
of the man with the gun, the person who can set everything off. I am sure to you this is just
political theater. It's fan service for your base. It doesn't mean anything. Get that. And no,
it's not treason. Treason in the United States is incredibly specific. In most countries,
I think this would probably qualify, but not here. But while it is not treason, I would point out
that it is certainly not in the best interest of the country. It is certainly not in the best
interest of those people you are supposed to represent. Yes, rhetoric does need to change, sir.
Yours. These little games, they have gone on long enough. You are undermining the foundational
elements of this country for nothing, for headlines, for retweets. I am sorry that people
said mean things to you, but I would suggest that maybe you stop playing into the delusions of
people. Anyway, it's just a thought. Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}