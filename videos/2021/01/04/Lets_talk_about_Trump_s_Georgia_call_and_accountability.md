---
title: Let's talk about Trump's Georgia call and accountability....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PXN0UX6Bh4I) |
| Published | 2021/01/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses Trump's controversial phone call attempting to influence election results.
- Notes Georgia's one-party consent rule for recording phone calls.
- Points out the danger of ignoring a potential coup over releasing a phone call.
- Acknowledges that many were aware of Trump's character and actions beforehand.
- Commends the Secretary of State from Georgia for upholding integrity in the face of pressure.
- Raises concerns about the future and accountability following the President's actions.
- Stresses the importance of holding the President accountable even after leaving office.
- Emphasizes the need for accountability to protect the institution of the presidency.
- Expresses worry about losing the system's resiliency if Presidents act above the law.
- Concludes by warning that the impact of Trump's presidency will persist beyond his term.

### Quotes

- "Releasing the phone call is certainly the lesser harm if the other option is standing idly by while an attempted coup is going on."
- "Seeing him do it isn't really a surprise."
- "This can't go without accountability."
- "Think about the number of people who were on this call, because there's no fear of accountability."
- "Trump out short-term goal."

### Oneliner

Beau addresses Trump's controversial phone call, stresses accountability, and warns of the lasting impact of his presidency.

### Audience

Voters, activists, citizens

### On-the-ground actions from transcript

- Hold elected officials accountable for their actions (implied)
- Stay informed and engaged in political developments (implied)

### Whats missing in summary

The emotional impact of witnessing the erosion of democratic norms and the resilience of American institutions. 

### Tags

#Trump #Accountability #ElectionInterference #AmericanDemocracy #Resilience


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the phone call.
Trump's less than perfect phone call.
I would assume by now everybody has listened to it, or at least heard the clips.
I will tell you I listened to the whole thing.
Context does not make it better.
It certainly appears to me that the President of the United States attempted to bully a
state elected official into subverting an election.
That is the appearance.
For his part, the President has kind of indicated that he's going to sue over the release of
the tape.
I'll just go ahead and point out Georgia is a one-party state.
The one party has to give consent for the call to be recorded.
Even if this was, as some have suggested, part of some settlement and it should have
been protected, I would suggest that lesser harm would certainly come into play.
Releasing the phone call is certainly the lesser harm if the other option is standing
idly by while an attempted coup is going on.
I do want to stop here and point out that those who watch this channel, most of us,
we knew who he was.
We knew what he was.
We knew what Trump was.
We knew he was going to do this.
He rounded out every one of those 14 characteristics.
We knew that.
Seeing him do it isn't really a surprise.
Honestly, the real surprise to me is how resilient the American system has been at withstanding
him, truthfully.
And while evil may come from authoritarian, top-down power structures, all too often good
comes from the individual.
And I do want to take a short moment and just kind of tip my hat to the Secretary of State
from Georgia.
Set politics aside for a second.
I don't know that he and I would agree on anything, but I can acknowledge that when
this chapter of American history is written, he's going to be on the right side.
This is a person who received a phone call from the President of the United States, the
leader of their party, like, hey, I need a favor.
And realistically, it would have been pretty easy to provide that favor.
Chose not to.
That level of integrity should be the standard.
However, all too often, it is not.
Now the next question.
What happens now?
What do we do?
Many have suggested that the President broke the law.
It would appear that way to me.
Not a legal scholar, but it certainly looks that way.
Should be handled like any other criminal matter.
The President of the United States is no longer the President in a matter of days.
There will be those people who want to kind of let bygones be bygones.
We don't want the scandal.
Protect the institution of the presidency and all that.
I would point out that in order to protect the institution of the presidency, you have
to have an elected President.
This can't go without accountability.
Presidents have skirted the law for a long time.
This was incredibly blatant, was incredibly open.
Think about the number of people who were on this call, because there's no fear of accountability.
I think that that's incredibly important at this point.
We will end up losing some of the resiliency this system has shown if the President is
able to do whatever they wish and behave as if they are above the law.
A lot of Presidents have done it.
This one took it to an extreme.
I would imagine this conversation is going to be going on for a couple of days.
There are some other interesting tidbits in the call beyond what's mainly being reported
is that, obviously.
But there's some other stuff, and we'll talk about it a little bit later today.
But I do kind of want to remind everybody that this isn't over.
Trump out short-term goal.
This doesn't end on January 20th.
The remnants of Trumpism, of his regime, they'll still be in power in a lot of places.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}