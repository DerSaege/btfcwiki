---
title: Let's talk about what Trump's phone call can teach us all....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oPX861UolHk) |
| Published | 2021/01/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzes moments in Trump's phone call to Georgia, noting his belief in his own propaganda and rhetoric.
- Describes how authoritarians like Trump surround themselves with fervent supporters, leading them to believe their rhetoric is true.
- Points out humorous and dangerous situations in the phone call where Trump dismisses contradictory information.
- Mentions cognitive dissonance experienced by Trump during the call when faced with opposing information.
- Critiques Trump's theory of a rigged election by Democrats and questions its feasibility.
- Suggests that many Republicans may have voted against Trump while maintaining party loyalty.
- Draws parallels between Trump's belief in his propaganda and rhetoric to how police sometimes believe they have the most dangerous job.
- Advises being mindful of the slogans and rhetoric one allows in life, as repeated exposure can lead to belief in them.
- Emphasizes the importance of positive slogans that encourage positive action.
- Warns about negative propaganda energizing but needing to be channeled into productive action.

### Quotes

- "Be aware of the rhetoric you use."
- "Make sure they're positive."
- "Words have power."
- "Be careful of the ones you allow in your life."
- "Y'all have a good night."

### Oneliner

Be conscientious of the slogans and rhetoric you allow in your life; words have power.

### Audience

Critical Thinkers

### On-the-ground actions from transcript

- Monitor and be mindful of the slogans and rhetoric you allow in your life (suggested)
- Surround yourself with positive rhetoric that spurs you to positive action (suggested)

### Whats missing in summary

The full transcript provides a deeper analysis of the dangers of believing one's propaganda and the importance of being vigilant about the information and rhetoric one consumes. 

### Tags

#Propaganda #Rhetoric #Belief #CognitiveDissonance #PositiveAction


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about something that we can all learn from Trump's phone call
to Georgia.
There are moments in that phone call where it certainly sounds like the president believes
his own propaganda, believes his own rhetoric.
It's something that is common among authoritarians of his ilk.
They become so removed, they surround themselves with their most fervent supporters, so they
believe their rhetoric is true.
And at times it creates humorous situations.
There were a few on that phone call.
At the same time it's also dangerous.
On that phone call, Trump's talking about a video and the people from Georgia are like,
you know, we can give you the whole video.
We can give you the unedited video.
I have a link for you.
He's like, no, I have a link for you.
Like, I've never actually heard cognitive dissonance before.
I've never had an audible example, but it certainly occurred there.
He knew he was about to get information that directly opposed a closely held belief and
he pushed it away.
Because he believes his own rhetoric, because he believes his own propaganda, he has created
this entire theory that somehow the Dems rigged it so he lost while Republicans overall did
pretty well.
That doesn't even make sense.
They're going to go through the trouble of rigging it, but throw the votes for every
other race to the Republicans?
That would be odd.
It seems really unlikely.
Seems much more likely that a lot of Republicans who aren't always consistent voters showed
up to vote against him, but they still maintained party loyalty.
I know some like that.
I know a guy who voted for Matt Gaetz, but voted against Trump.
That's probably what happened.
George is just not that into him.
But because he believes his own rhetoric, believes his own slogans, his own propaganda,
he can't accept that.
This happens a lot.
Happens to cops.
How many times have you heard it?
Police have the most dangerous job in the world.
No, they don't.
It's not even in the top ten.
But because they've said that so much, they begin to act as if it's true.
So something that we can all take away from that phone call is to be very conscientious
of the slogans and the rhetoric that you allow in your life and the slogans and rhetoric
you use, because eventually you may start to believe them.
You may start to hold it as a core value.
If you repeat something often enough or you hear something often enough, you begin to
accept it as true.
That on some level may have happened to the president.
Doesn't excuse his actions.
Our man still has to be held accountable for his words.
But it certainly appears that that played into it, because some of it you could tell
he truly believed.
His crowd sizes were big.
His audience was energetic.
He was getting fed those polls, those internet polls, as if they were real.
So he believed it.
And then the rug got pulled out from underneath him.
Be aware of the rhetoric you use.
Be aware of the slogans you allow to be said to you repeatedly.
Make sure they're positive.
Make sure they spur you to action in a positive manner.
If you want to be the change you want to see in the world, make sure that you surround
yourself with that kind of rhetoric, that kind of slogan.
The negative slogans, the negative propaganda, it has a purpose to energize the base.
But once they're energized, you have to keep moving.
You have to create something with that energy.
Otherwise it turns in on itself.
Anyway it's just a thought.
Y'all are ready for the video to end, because words have power.
Be careful of the ones you allow in your life.
Y'all have a good night.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}