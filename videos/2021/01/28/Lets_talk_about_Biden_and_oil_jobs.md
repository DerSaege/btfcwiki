---
title: Let's talk about Biden and oil jobs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=GTSQlkIgjZc) |
| Published | 2021/01/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's new plan doesn't account for energy jobs, including those in the oil sector.
- The energy industry has been declining, with companies like BP planning to cut fossil fuel output by 40% by 2030.
- Technology is making many jobs in the industry obsolete.
- The industry has had significant political power and received handouts to stay afloat.
- Manufacturing jobs are unlikely to return as automation takes over.
- Workers in the energy industry should advocate for educational programs to transition to new jobs.
- Rather than supporting billionaires, workers should advocate for their own interests as the industry declines.
- Workers should not expect the industry to take care of them when it disappears.
- Jobs in cleaning up spills are also disappearing.
- Advocating for educational programs can help workers transition to new jobs.

### Quotes

- "Technology is making those jobs obsolete."
- "Manufacturing may come back, but the jobs won't."
- "Advocate for your own interest for a change."
- "They're not going to take care of you."
- "Those jobs are going away too. It's probably a good thing."

### Oneliner

Biden's plan overlooks energy jobs that technology is making obsolete, urging workers to advocate for their own interests amidst industry decline.

### Audience

Workers in the energy industry

### On-the-ground actions from transcript

- Advocate for educational programs to transition to new jobs (implied)
- Call up senators and representatives to push for programs benefiting workers (implied)

### Whats missing in summary

Beau's insightful commentary and call for workers to prioritize their interests amidst industry changes can best be appreciated by watching the full transcript.

### Tags

#EnergyJobs #IndustryDecline #Advocacy #Transition #Automation


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about Biden's new plan
and the concern that he didn't really
take into account a lot of energy jobs,
jobs in the energy sector, those oil jobs, stuff like that.
There's some truth to that.
He didn't, not really.
He didn't take them into consideration.
He didn't think about the typesetters, the video store
clerks, the telegram operators, those people who re-ink
typewriter ribbons, the people who make papyrus, switchboard
operators.
He didn't think about any of them.
Biden is not ending that industry.
That industry has been on its way out the door a while.
BP, BP, when you were talking about an energy giant,
BP is one of them, right?
They announced that they plan to cut fossil fuel output by 40%
by 2030.
Those jobs are going extinct.
They're disappearing.
It has nothing to do with Biden.
It's the story of human civilization.
Technology is making those jobs obsolete.
The reality is that that sector of our economy
has so much sway over the House and the Senate
and politics in general that they've had too much power.
And therefore, they've been able to get handouts
this entire time to kind of keep the industry rolling.
Even after technology had advanced beyond it,
we're there.
We can move beyond it now.
Those leases that they keep talking about,
those are handouts.
Those are ridiculously undervalued.
And it's what keeps a lot of these companies afloat.
It's a lot like the manufacturing jobs
everybody keeps clamoring for.
Bring them back to the United States.
They're not coming, guys.
They are not coming.
Manufacturing may come back, but the jobs won't.
It's going to be automated.
You'll have a few jobs for robot tenders or robots
or whatever, but it's going to be automated.
Those days of the long assembly lines,
they're disappearing.
Those days are numbered.
So if you are in this industry and you're worried about it,
it shouldn't come as a surprise.
I mean, y'all know this.
Y'all know that these jobs have been automated.
You know that these jobs have been heading out
the door for a while.
You got articles about it going back 15 years.
We all knew this was coming.
So you're left with a choice.
You can call up your senator and your representative
and let them fear monger to you and explain how this is all
going to end tomorrow.
It's not.
They're sitting on leases and permits.
They've stockpiled them because they knew this was coming.
You can advocate on behalf of the multi-billion dollar
oil and energy companies, or you can
advocate for your own interest for a change.
Say, fine, if you're going to do this
and you're going to enact these programs,
they're going to create these other jobs
while kind of putting the final nail in ours,
we'd like some educational programs.
Advocate for your class for a change
instead of being fear mongered to by the billionaires
and letting them push you into advocating
to make them even richer because this industry is going away.
And when it does, they're not going to take care of you.
Have they really done a good job up till now?
I'd also point out there's another job that's disappearing.
See, I'm down here on the Gulf Coast.
Those people who have to clean up after those spills,
those jobs are going away too.
It's probably a good thing.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}