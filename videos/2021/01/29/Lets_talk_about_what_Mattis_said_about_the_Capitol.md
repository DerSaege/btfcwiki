---
title: Let's talk about what Mattis said about the Capitol....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=IjExFX7izMg) |
| Published | 2021/01/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former Secretary of Defense, General Mattis, had a private, non-publicized chat with Michael Vickers on the OSS Society webcast.
- They discussed external and internal issues facing the United States, including the erosion of democracy and the events of January 6th.
- General Mattis expressed concern about the erosion of democracy and criticized the actions of a sitting president.
- Michael Vickers emphasized the oath to the Constitution against all enemies, foreign or domestic, and expressed disbelief at the insurrection on January 6th.
- Vickers, less known than Mattis, has an extensive military and intelligence background, bringing credibility to his statements.
- Those well-versed in military matters see the events of January 6th differently from a political or legal perspective.
- Individuals like Mattis and Vickers, critical for such operations, do not support actions undermining democracy.
- Lack of accountability for such actions can lead to further attempts, considering failures as training rather than defeat.
- The Republican Party's prioritization of politics over the nation's well-being is concerning.
- Those who truly understand the situation view internal threats as more dangerous than external ones, questioning the patriotism of individuals putting politics over democracy.

### Quotes

- "They cannot pretend to be patriots. They cannot pretend to care about this country or they wouldn't be putting their own political careers over the underpinnings of a democracy."
- "Those who understand how these things go do not want to see it happen in the United States."
- "Not really something that was designed or intended to be widely distributed."

### Oneliner

Former Secretary of Defense Mattis and Michael Vickers privately discussed the erosion of democracy and the dangers of internal threats, criticizing actions that prioritize politics over the nation's well-being.

### Audience

Citizens, patriots, activists.

### On-the-ground actions from transcript

- Contact local representatives to prioritize democracy over politics (suggested).
- Join organizations advocating for accountability in political actions (implied).

### Whats missing in summary

Insights on the importance of upholding democratic values and accountability in political leadership.

### Tags

#Democracy #Mattis #InternalThreats #Accountability #PoliticalLeadership


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we're going to talk about a
conversation you probably don't know about, but you should. You definitely should. It was between
the former Secretary of Defense, General Mattis, and a guy named Michael Vickers.
This conversation wasn't a PR thing. It wasn't something that was designed to go out over the
wire and make a statement. It was on the OSS Society webcast. The OSS is the Office of
Strategic Services. Back in the day, it was the beginning of the U.S. military's journey
into special operations. All of the cool, special, spooky military units that exist today,
they can all kind of trace their lineage back to the OSS. It was the days of glorious amateurs,
the PhDs who could win a bar fight. Really how they tried to recruit them.
The OSS Society is an organization that just tries to keep their history alive.
Not a huge audience. This wasn't grandstanding, just these two guys having a conversation.
And they discussed everything you would imagine men like them would discuss. The external issues
the United States is likely to face, the move to near peers, all of that stuff.
And then they talked about internal issues. And that's when it got interesting.
General Mattis expressed concern about the erosion of, to use his words,
the underpinnings of our democracy. And what we saw on January 6th,
fomented by a sitting president. Fomented by a sitting president. I guess we know how he would
vote if he was in the Senate. He didn't really pull any punches when he was describing the internal
issues. He specifically mentioned racists and those who believe in wild theories.
For Vickers' part, he pointed out that our oath to the Constitution is against all enemies,
foreign or domestic. And then he went on to say that even in his worst nightmares,
he couldn't imagine that we would witness an insurrection against our government incited
by some of our top leaders. Leaders, plural. You know, Vickers doesn't have the same public
profile that Mattis does. Not as many people know who he is. So a quick biography, because I mean,
if you're going to make a claim like that, you better have some experience, right? In 1973,
he joined U.S. Army Special Forces, started as enlisted, became an officer. He went on
to become a sad boy, Special Activities Division with Central Intelligence.
He was involved in Afghanistan the first time against the Soviets. Under Bush Jr.,
he became Assistant Secretary of Defense for Special Operations. He was a member of the
National Security Council. He became Assistant Secretary of Defense for Special Operations and
Low Intensity Conflicts. That's insurrection. And under Obama, he became Under Secretary of
Defense for Intelligence. 1973 to the Obama administration.
He's been around a while. He probably knows a little bit about this, given that that's
his entire resume. The PhDs who can win a bar fight of today, those who understand this stuff,
they know what they saw. They know what it was, and they know where the blame lies.
It's important to note that when you are talking about it, not from a political standpoint
or a legal standpoint, but from a military standpoint, there's not any debate about what
happened. It's also worth bringing this up because these are the type of people who would be needed
if a sitting president wanted something like this to work. They're not on board with it.
That was a big concern that we had to make a few videos about when all of this was going on.
They're not on board with it. Those who understand how these things go do not want
to see it happen in the United States. That being said, an attempt like this
or an attempt like we saw on the 6th that fails, if nobody's held accountable,
it's not really a failure. It's just training. It's practice.
The Republican Party is putting politics above the nation.
When you are talking about something that in a non-PR setting, not really something that was
designed or intended to be widely distributed, those who really understand it consider it a
greater threat than any foreign power. They cannot pretend to be patriots. They cannot pretend to
care about this country or they wouldn't be putting their own political careers over the underpinnings
of a democracy. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}