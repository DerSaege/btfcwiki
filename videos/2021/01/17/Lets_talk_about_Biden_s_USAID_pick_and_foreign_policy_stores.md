---
title: Let's talk about Biden's USAID pick and foreign policy stores....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OfNECazbkx8) |
| Published | 2021/01/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden filled a significant position in foreign policy, choosing Samantha Power to lead USAID.
- Samantha Power has a history of advocating for humanitarian causes but also has controversies, such as calling Hillary Clinton a monster and influencing military intervention in Libya.
- There is a misconception that USAID is a front for central intelligence, but Beau explains it's more like neighboring stores in a strip mall.
- American foreign policy can be likened to a strip mall with different entities like the State Department, intelligence community, military, and corporations.
- Beau warns against overreliance on the military store in the strip mall analogy, advocating for a more balanced approach.
- He stresses the importance of using all elements of foreign policy to maintain American dominance while avoiding excessive military intervention.
- Beau encourages advocating for the use of the State Department over the military store to prevent potential problems in countering more powerful nations.

### Quotes

- "We colonize with corporate logos."
- "We need a foreign policy that does not rely on military might."
- "We need to get back to patronizing them a little bit more."
- "Even in its best version, it is still about maintaining American dominance."
- "A mistake, a habit of going to the military store could cause a lot of problems."

### Oneliner

Biden’s foreign policy must balance entities like USAID and avoid overreliance on the military store to maintain American dominance effectively.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Advocate for using the State Department more in foreign policy decisions (implied)
- Support a balanced approach in utilizing different elements of foreign policy (implied)

### Whats missing in summary

The full transcript provides additional context on Samantha Power's background and specific examples of the impact of foreign policy decisions.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Biden's foreign policy, what we can tell so far because
he filled another position.
And it's one that people who are interested in foreign policy were waiting to see filled
because it can tell a lot about what the administration plans on doing normally.
This time not so much.
He staffed, chose the lead over USAID.
USAID is the United States Agency for International Development.
Not an agency that gets a lot of headlines.
But even though it is an incredibly small agency, I want to say like 4,000 people, it
wills a significant amount of power overseas.
He chose Samantha Power to fill the slot.
Logical choice.
She has a long history of actually advocating for humanitarian causes.
She is not without controversy though.
She once called Hillary Clinton a monster during an interview.
That may not help her politically.
And she also played a part in advocating or influencing the decision to engage in military
intervention in Libya.
So once again, we're left with what does that mean?
What does that really mean?
What does it mean that they're going to put her in there?
And I think the best way to get to the root of it is to dispel a common misconception.
Those people who are familiar with this agency, most of them will tell you, well, it's a front
for central intelligence.
That's not true.
The sentiment is true.
I mean, the sentiment is definitely accurate.
But that's a bad analogy.
It's not like you walk into a store that says USAID on it and in the back room there's a
bunch of spooky guys playing risk and smoking cigars and drinking brandy.
It's not how it works.
It's more like there's a strip mall.
And there are two stores.
One is the intelligence community and right next door is USAID.
And they sell the exact same stuff.
They just market it differently.
They definitely share a goal.
But casting it as a front isn't really fair.
They go about it in different ways.
Do they work in conjunction?
Yeah, absolutely.
All of American foreign policy can accurately be portrayed as a strip mall.
You have State Department, the legitimate State Department.
You have agencies like USAID.
You have the intelligence community.
You have the military.
And then there's this store off to the side, which is a big box store.
Has a whole lot of power.
But it's more unofficial.
It's not really part of that strip mall.
Are corporations.
That's how we really colonize now.
We don't walk up and plant a flag.
We colonize with corporate logos.
These other four options, it appears as though the Biden administration is making sure that
all of those stores are fully staffed.
Which is a good thing, theoretically.
It makes sense.
Because in a perfect world, you want to have every tool at your disposal.
You want to make sure all those stores are open and ready for business.
The problem is, one of these stores is open 24 hours a day, seven days a week.
It is very easy.
It's always having sales and gets people excited.
And people love it in the United States.
And that's the military.
It would be great if the Biden administration understood that if he uses that store too
much it's going to put the other ones out of business.
We need a foreign policy that does not rely on military might.
I don't know that Biden is planning on going that route.
He is fully staffing all of the stores.
But we need to advocate to make sure that those other stores get used a little bit more.
It might be worth noting that if back in 2001 a kid had just graduated high school and enlisted,
his child could have graduated high school and enlisted and gone over and been stationed
at the same base.
We have been using the military store far too often.
And it is, the other stores are almost out of business.
We need to get back to patronizing them a little bit more.
Using them a little bit more often so they stay in business and so they maintain the
ability to serve the public.
I would also point out that the product that all of these stores are selling is American
dominance.
That's something that we need to keep in mind if you are new to following foreign policy.
Understand that even in its best version, it is still about maintaining American dominance.
Don't assign any other motive to it.
Yes, sometimes good things happen along the way.
Sometimes bad things happen along the way.
But they are all in pursuit of maintaining American dominance.
That in and of itself isn't really the best foreign policy.
But that is what our foreign policy has been pretty much since the beginning.
We just weren't that good at it at first.
So it does appear that Biden is trying to keep all of his options open.
That's good.
We need to advocate to make sure that he is using the State Department more than anything
else because we are now going to be countering near peers, more powerful nations.
A mistake, a habit of going to the military store could cause a lot of problems.
It could bring down the rain.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}