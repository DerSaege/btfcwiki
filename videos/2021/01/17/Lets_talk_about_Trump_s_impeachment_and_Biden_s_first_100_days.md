---
title: Let's talk about Trump's impeachment and Biden's first 100 days....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9xbogyfKOMg) |
| Published | 2021/01/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump's impeachment and Biden's first hundred days set the tone for a new administration.
- Majority of Americans and Biden voters didn't passionately support Biden; he was seen as the best chance to oust Trump.
- Biden's campaign was centered on the idea of undoing the damage done by Trump, not on being a super progressive candidate.
- Beau advises Biden to lean into the impeachment process and not shy away from it.
- Biden should focus on undoing Trump's executive orders and reshaping the political landscape during the impeachment.
- Emphasizes the importance of accountability for those in power and undoing the damage caused by Trump to set the tone for the administration.
- Biden needs to reach across the aisle for unity but should prioritize those who opposed Trumpism, not those who supported totalitarianism.
- The Biden administration must undo Trump's actions and move forward with significant legislative changes to ensure real progress.
- Simply not being Trump won't be enough for a successful Biden re-election or Harris election; real changes and undoing Trump's policies are necessary.
- Beau suggests that Biden needs to show that his administration is more than just "not Trump" and must focus on real legislation and changes.

### Quotes

- "Lean into it. Stick with it. Don't hide from the impeachment."
- "They have to undo the previous administration and then they have to move forward."
- "Reach across to those who opposed Trumpism, not those who brought us to the abyss of totalitarianism."
- "Show us that."

### Oneliner

Beau advises Biden to lean into the impeachment process, undo Trump's damage, and prioritize real changes over simply not being Trump to set the tone for his administration.

### Audience

Political advisors, Biden administration

### On-the-ground actions from transcript

- Lean into the impeachment process and use it to undo Trump's executive orders (implied).
- Focus on reshaping the political landscape during the impeachment (implied).
- Reach across the aisle for unity with those who opposed Trumpism (implied).
- Prioritize undoing Trump's actions and enacting real legislative changes (implied).

### Whats missing in summary

The detailed nuances and explanations behind Beau's suggestions are best understood by watching the full transcript.

### Tags

#Biden #Trump #Impeachment #Accountability #PoliticalChange


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about conventional wisdom.
Trump's impeachment and Biden's first hundred days.
The first hundred days of a new administration, typically it sets the tone.
It establishes how they're going to do.
It's really important.
Conventional wisdom right now suggests that Trump's impeachment is a liability to Biden
because it's going to overshadow his first hundred days.
I disagree.
That's only true if Biden allows it to be true.
Let me be real frank for a moment.
The majority of Americans, the majority of Biden voters, were not like, hey I love this
guy.
This is the guy I want to be president.
That wasn't the math.
The math was this is the guy most likely to get Trump out.
The campaign acknowledged this.
Biden's campaign was not I'm going to be a super progressive person.
It was nothing's going to fundamentally change.
The whole campaign was I'm not Trump.
I just understand how much damage he's done and I want to undo it.
That was the campaign and it worked.
Stick with it.
Stick with it.
Don't hide from the impeachment.
Lean into it.
Yeah, there is the risk of losing political capital if he is acquitted.
Fact.
But it's going to be far less than the amount of political capital gained by walking out
and saying yeah Trump doesn't need to be impeached.
No I'm not going to pardon him.
No I'm not going to sic the AG on him.
I'm going to behave as an American president and I'm going to allow the Justice Department
to make that determination on their own.
And we all know what they're going to do.
That's it.
That statement alone will more than make up for any political capital lost in the event
of a Trump acquittal.
The American people would like to see accountability just once for those at the top.
It would be nice.
You want to set the tone.
Understand that the song that we want to hear is the undoing of all of the damage that Trump
caused and then moving forward.
It has to start with that.
It has to start with undoing the damage that he caused.
Take the entire time of the impeachment.
Use it to undo his executive orders.
Use it to reshape the political landscape.
You want to reach across the aisle for unity.
Fine.
Got it.
Understood.
Makes sense.
Reach across to those who opposed Trumpism, not those who brought us to the abyss of totalitarianism.
There aren't that many of them in the Republican Party that actually stood up to him.
Reach across to them for once instead of allowing the Republican Party to drag us further and
further into the past, elevate those who attempted to at least stay where we were at.
The Biden administration has an insurmountable task.
They have to undo the previous administration and then they have to move forward.
Because I don't think simply not being Trump is going to work for a Biden re-election or
a Harris election.
They're going to have to show more than that.
They're going to have to undo everything that he did and then move us forward with real
legislation, real changes.
Otherwise, nothing will fundamentally change.
And that's how the administration will be remembered.
The worry that a Trump impeachment is going to overshadow Biden's first hundred days puts
a really dark cloud over the Biden administration because we have to wonder what they're going
to do.
Because realistically, that should be part of it.
Accountability and undoing the damage that he caused.
Doing what you can to limit the political capital of those who enabled him.
The Biden administration ran on, I'm not Trump.
Show us that.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}