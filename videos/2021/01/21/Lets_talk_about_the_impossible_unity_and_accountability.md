---
title: Let's talk about the impossible, unity, and accountability....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KuNwPBSZG5M) |
| Published | 2021/01/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the unlikely scenarios and why accountability matters.
- Mentioned making videos detailing the likelihood of Trump's plans to stay in power.
- Emphasizes that Trump was not a leader and lacked understanding of what it means to lead.
- Points out that Trump's failure to recognize opportunities was a key issue.
- Suggests that had Trump shown leadership on January 6th, events at the Capitol could have been very different.
- Raises the question of what law enforcement might have done if Trump had attempted to gain access to the Capitol.
- Acknowledges the resilience of the system despite the lack of backing Trump needed.
- Stresses the importance of holding those responsible for fostering the atmosphere that allowed the events to occur.
- Warns that if accountability is not enforced, similar events will likely happen again.
- Expresses concern that without consequences, the cycle of events like January 6th will be hard to stop.
- Urges for a focus on unity but notes that some may be seeking amnesty instead.
- Asserts that unity is needed to move forward but accountability is equally vital.
- Warns that without consequences for those responsible, history may repeat itself.
- Encourages watching his previous videos on the topic to understand the importance of accountability.

### Quotes

- "Had he shown up, it would have eliminated those minutes and those seconds that were needed to secure the people in Congress."
- "Those at the top need to be held accountable. If they are not, it will happen again."
- "This system is resilient, but there are very, very few things that are actually impossible."
- "We need to keep that fact in mind when people talk about unity."
- "If these people who are responsible are not held accountable, it will happen again."

### Oneliner

Beau covers the unlikely scenarios regarding Trump's power retention and stresses the critical need for accountability to prevent future events like January 6th.

### Audience

Policy Advocates, Activists

### On-the-ground actions from transcript

- Hold those responsible for fostering the atmosphere accountable (implied)
- Encourage watching educational videos on similar topics (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the events surrounding Trump's term end and the Capitol riot, underscoring accountability's significance in preventing such occurrences.

### Tags

#Accountability #Trump #Unity #Leadership #Resilience


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about
the impossible,
the unlikely,
how it can't happen here,
and why accountability matters.
So,
over the course of the last year,
I've made a whole lot of videos detailing
and discussing the likelihood of success
when it comes to some of the various plans
that people thought Trump would use to stay in power
after his term ends.
They all pretty much ended with, this is very unlikely.
Impossible was a word thrown around a lot because I have believed as many as
six impossible things before breakfast.
But,
very unlikely,
a whole lot of them,
because
he wasn't a leader.
He didn't understand what it means to lead,
and that's a key part of it.
He,
he doesn't understand that once you're through the door,
once you have committed to a plan of that magnitude,
you can't stop and you can't hesitate.
It was something that he failed to grasp.
Sometimes you don't even have to be a leader,
you can just be in a situation in which you recognize the opportunity.
Had he been a leader
or recognized the opportunity on the sixth,
things would have gone very differently.
This, by the way, is what I didn't want to say
while he was still in office.
I want to present you with a scenario.
I want you to imagine if he had been a leader
and walked down the avenue
with that crowd
to the Capitol,
or just recognized the opportunity once the crowd had formed
and showed up.
What do you think law enforcement would have done
if it was the President of the United States attempting to gain access?
When you're talking about securing people like that,
minutes and seconds matter.
Had he shown up,
it would have eliminated
those minutes and those seconds that were needed to secure
the people in Congress.
He would have made his way into that room
with that crowd of people,
and that would have been a very, very different event,
because that crowd would have done anything.
It still would have been unsuccessful.
This system is very resilient.
He didn't have the backing that he needed,
but it would have turned something that was settled in hours
into something that could have taken days or weeks or even months.
It would have changed that whole dynamic.
It wouldn't have been the three stooges go to Washington that we saw.
It would have been something a whole lot more dramatic.
And I'm not downplaying the situation or the loss that occurred,
but when you are talking about events of this nature,
that was best-case scenario.
Could have been a lot worse, a lot worse.
We need to remember that, because it can happen here,
and that was way too close for comfort.
It could have spiraled out of control,
and all it would have taken was for him to show up.
Would have been it.
We need to keep that fact in mind when people talk about unity.
Unity is important.
We need to get it, because we need it to move forward.
However, most of the people clamoring for unity right now
don't really want unity, they want amnesty.
And that's up to Biden.
He has absolute power over that at this moment.
That is up to him.
And when you're talking about the little fish down at the bottom,
doesn't matter, realistically, doesn't matter,
them getting in trouble is not going to set a tone.
Those at the top, those who are responsible
for fostering the atmosphere that allowed this to occur,
that encouraged it to occur, need to be held accountable.
If they are not, it will happen again.
It will happen again.
And once it is successful once, it will continue to happen.
Look at every country where it's gone down.
Once that cycle starts, it is very hard to stop.
This system is resilient, but there are very, very few things
that are actually impossible.
And at some point, we will get a Trump that's a leader,
that can manage to see the opportunities,
and who isn't afraid to execute his own plan or move
to satisfy his desires, even if he didn't plan it.
It can happen here, and it almost did.
When it comes to this particular topic,
feel free to go back and look at the videos
that I've done on these subjects.
I'm telling you, if these people who are responsible,
who are truly responsible, are not held accountable,
it will happen again.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}