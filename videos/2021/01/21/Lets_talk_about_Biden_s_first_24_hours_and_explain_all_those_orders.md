---
title: Let's talk about Biden's first 24 hours and explain all those orders....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zbp-0W9aR7g) |
| Published | 2021/01/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains Biden's executive orders and unconventional approach.
- Details the content and significance of each executive order.
- Mentions the extension of eviction and foreclosure moratoriums.
- Talks about pausing student loan payments and interest.
- Addresses rejoining the Paris Accord and abolishing the 1776 Commission.
- Comments on actions regarding diversity training and equity in policymaking.
- Notes the inclusion of undocumented immigrants in the census.
- Talks about preserving DACA and revoking the Muslim ban.
- Mentions changes in immigration enforcement policies.
- Comments on halting the wall construction and ending the emergency declaration.
- Talks about non-deportation of Liberians until 2022.
- Addresses interpreting the Civil Rights Act to include workplace discrimination based on orientation and identity.
- Mentions new ethics rules and reversing Trump's regulatory process changes.
- Explains how Biden fulfilled major goals through executive orders without legislation.

### Quotes

- "It's the patriotic thing to do."
- "Undocumented immigrants will be counted in the census."
- "That's going to have immediate tangible effects."
- "The United States is not a small town."
- "He can't run it in that manner."

### Oneliner

Beau explains Biden's unconventional approach to executive orders, detailing their content and significance while fulfilling major goals without legislation, addressing pressing issues with immediate tangible effects and setting the tone for his administration.

### Audience

Policy advocates

### On-the-ground actions from transcript

- Contact local representatives to advocate for the continuation of policies like eviction and foreclosure moratoriums (implied).
- Join or support organizations promoting diversity training and equity in policymaking (implied).
- Organize events or workshops to raise awareness about workplace discrimination based on orientation and identity (implied).

### Whats missing in summary

Insight into Beau's perspective and analysis on Biden's executive orders beyond the summarized points.

### Tags

#Biden #ExecutiveOrders #Policy #ProgressiveLeft #Equity


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to hold a little town hall meeting,
little town meeting and explain Biden's executive orders
and explain why he did it this way.
Because conventional wisdom says this is all wrong.
Not the content of his executive orders,
but the way in which he did it.
And I have a theory about that.
But first we're going to go through the content.
OK, so the first one is an order stating
that you have to wear masks and social distance
on federal property.
That seems like it should already exist.
The next one is a 100 day mask challenge,
trying to convince Americans that it is the right thing
to do to wear a mask.
It's the patriotic thing to do.
Should be an easy sell, provide for the common defense,
promote the general welfare and all that stuff,
you know, in the Constitution.
Just asking people to do it as individuals.
The next one, we're going back into the who.
The next one, establishing a response coordinator
for our current public health issue.
That seems like it should already exist.
And restoring the Directorate of Health Security and Biodefense
to the National Security Council.
Yay, that's really important.
In fact, there were a lot of people
who kind of assumed that the machinery for this
still existed even after it wasn't there anymore.
And they were wrong.
I'm they.
OK, the next one, the eviction and foreclosure moratorium
has been extended.
The eviction moratorium is now extended through March 31.
The next one, pausing student loan payments and interest
until September 30.
The next one, we are going back into the Paris Accord.
That's climate change.
The next one is getting rid of the 1776 Commission.
That's Trump's little patriotic re-education program.
If you missed my commentary on Twitter about that,
the product they put out was horrible.
Horrible.
It also, the same order also allows federal agencies
to resume diversity training.
Good thing.
There's also something in it saying
that he's looking for recommendations
on how to achieve equity in policymaking
at the federal level and root out systemic racism.
That would be great.
But don't get too excited because we still
have to find out what the recommendations are,
how they're going to be implemented,
the plans of action.
There's a whole bunch of stuff in this.
That part is, we'll see.
That's not necessarily creating any tangible action
at the moment.
Undocumented immigrants will be counted in the census.
That makes sense.
Historically in the Constitution,
it is persons, not citizens.
The next one, preserve and fortify DACA.
That's going to need follow-up legislation to make it matter.
But it's a good start.
The next one ditches the Muslim ban in an interesting part.
It asks the agencies involved to try to address
the harm that the ban caused.
Other than possibly expediting their applications,
I don't really know what that means.
We'll have to wait and see.
The next one revokes Trump's harsh and extreme immigration
enforcement and directs agencies involved
to set new policies more in line with our values,
meaning the Biden administration and the United States
as a whole.
This is realistically a stopgap.
This doesn't really mean anything.
This is more like, hey, wait a second,
because we do know that there is actual legislation following
this up.
Let's see.
The next one pauses the building of the wall
and ends that ridiculous emergency declaration.
It's going to set up legal fights.
You're going to hear about this one over and over again,
because there are already contracts in place.
So that one's not over yet.
But yeah, that needed to be done.
The next one makes Liberians basically non-deportable
if they've been here for a while until 2022.
I didn't write down the year in my notes.
I think it's 2022.
The next one, the Civil Rights Act
is now to be interpreted by federal agencies
to include prohibiting workplace discrimination based
on orientation and identity.
That's going to have immediate tangible effects.
That's a big one.
Let's see.
The next one, new ethics rules.
They have to sign a pledge.
This is kind of symbolic.
And then the final one that was done at the time of filming
is reversing Trump's changes to the regulatory process.
It basically returns it to the status quo.
The status quo wasn't good to begin with.
But it also directs the Office of Management and Budget
to come up with some modern method of handling this
and with a goal towards protecting the environment
and providing social stewardship.
All good.
There's nothing bad in this.
There's no real surprises.
And as far as what he can do with a pen stroke, yeah.
I mean, even the stuff that is symbolic
or is just asking for recommendations, stuff like
that, it's kind of the most he can do without legislation.
So he has fulfilled a lot of his major goals
up front to the extent that he can without Congress.
And that's good.
OK, so why does conventional wisdom say all of this is bad
or that he's doing it wrong?
Because this is three weeks worth of headlines.
He can control the narrative and control
what people are talking about for three weeks with this,
or at least one a day.
That is conventional political wisdom.
And it makes sense.
So why would somebody with a lot of political experience
decide not to do that?
I think it's because of his allies.
Biden knows that the Trump defeat was nowhere
near as decisive as it should have been.
So if he wants to maintain support,
he has to understand that the progressive left is
looking at him.
The progressive left is very quick to point out
inconsistencies or things that they believe are slights.
Which one of these would you do first?
That's it.
By my way of thinking, that's the reason he did it this way.
Because whichever one you do first,
you're going to have to justify why
that one is more important than the one you're
going to do the next day, knowing that all of them
are going to be done in the next three weeks.
I think that's probably why he did it.
I would also imagine that there's going to be more.
There's going to be a lot of follow-ups to this
through actual legislation being pushed.
And I'm going to guess there's more executive orders to come,
because there's a couple of things
that I expected that I don't see on here yet.
So that would be my reasoning.
Or he just wants to come out the gate
and totally gut Trump's legacy.
And he did.
As far as the major changes that Trump made,
they're pretty much all undone or on their way
to being undone.
So that's the reason.
That's the reason he went about it in this way.
The United States is not a small town.
He can't run it in that manner, just
by kind of dictating policy.
But this is as much as he can do in that way.
As far as setting the tone, he's done that.
Day one.
Half a day, really.
And it's all pretty good stuff.
And some of this actually is going to have tangible effects.
But at the end of the day, when you
hear people who are politically savvy criticizing this,
saying, why did he do it this way?
I think that's the reason.
I think it has to do with trying to prioritize this.
When you write it down and you look at it,
you're not going to want to defend who is most important,
what cause is most important, which one you're
going to do day one and which one you're going to do day 15.
I think that's the reason.
Or maybe he's just in a hurry and has a whole lot planned.
Either way, we will find out.
So it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}