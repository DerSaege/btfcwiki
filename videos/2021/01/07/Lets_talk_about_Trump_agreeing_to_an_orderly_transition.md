---
title: Let's talk about Trump agreeing to an orderly transition....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=HupuL7WTYfk) |
| Published | 2021/01/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Trump has agreed to an orderly transition of power on January 20th but Beau expresses skepticism about this statement.
- Beau believes that President Trump is feeling pressure and is not intending to pursue an orderly and peaceful transition of power.
- He calls for the resignation, removal via the 25th Amendment, or impeachment and removal of President Trump because of his erratic behavior and divisive actions over the past four years.
- Beau does not trust President Trump's intentions or believe that he has learned his lesson.
- He criticizes the President for attacking the fundamental principles of American democracy and trying to control the narrative despite losing access to social media.
- Beau doubts President Trump's control over events, especially after the events of yesterday.
- He questions the idea of trusting the President without any reason to do so.

### Quotes

- "President Trump is not in control."
- "I'm not sure that it's a good idea to simply say, okay, we trust you now, because he's given us no reason to trust him."

### Oneliner

Beau expresses skepticism about President Trump's intentions for an orderly transition of power and calls for his resignation, removal, or impeachment due to his divisive actions and lack of trustworthiness.

### Audience

Activists, concerned citizens

### On-the-ground actions from transcript

- Advocate for the resignation, removal via the 25th Amendment, or impeachment and removal of President Trump (suggested)
- Stay informed and engaged in political developments (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of President Trump's recent statement and Beau's concerns about the transition of power, urging action against the President's divisive behavior and lack of trustworthiness.

### Tags

#TransitionOfPower #Skepticism #Impeachment #Resignation #AmericanDemocracy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So, a few minutes ago,
the President of the United States, Donald J. Trump,
has agreed to an orderly transition of power
on January 20th.
Said this in a statement.
Allow me to be the first
of what I can only assume is many people
to express my suspicions.
I am skeptical of this statement.
I don't believe it.
In the same statement, the President said that,
you know, this is just the beginning
of the fight to make America great.
I do not believe that he intends to pursue
an orderly and peaceful transition of power.
I think that because of today's events, he feels pressure.
And he is caving to that pressure,
but he is erratic, and he has been erratic.
He has done nothing for this country
except fill it with division and lies for four years.
It is my belief that the President needs to resign,
be removed via the 25th Amendment,
or be impeached and removed.
I think that it's time to remove President Trump
and get him away from the levers of power.
I don't know that we should gamble on
I don't know that we should gamble on his goodwill,
on the idea that he's learned his lesson,
that he's not going to continue to damage the United States.
The President has laid attack after attack
against the basic principles of American democracy.
The foundational elements of this country.
And now, because he had his Twitter account taken away,
well, now he's grown up, he's a big boy now.
I'm not sure I believe it.
Honestly, to me, having talked about the President
way too much over the last couple of years,
it just seems like this is a way to get
his version of events into the headlines,
because he can't use Twitter right now,
because he doesn't have access to social media.
It seems as though he's still trying to control the narrative
and seem as though he is still in control of events.
If yesterday proved anything,
it proved that the President is not in control.
He's not in control.
He doesn't understand what's going on.
He does not understand the impacts that his words cause.
I'm not sure that it's a good idea to simply say,
okay, we trust you now,
because he's given us no reason to trust him.
Anyway, it's just a thought.
Y'all have a good morning.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}