---
title: Let's talk about what happened on Capitol Hill....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MwvGDfXtehE) |
| Published | 2021/01/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes the events on Capitol Hill as an attempted coup to disrupt the transfer of power.
- Notes the difference between a revolution and a coup, where a coup focuses on changing the head of state, in this case, from Biden back to Trump.
- Mentions that the attempted coup failed due to lack of military support, which is a critical element for a successful coup.
- Urges Americans to acknowledge the seriousness of the event and not downplay it as it could lead to a normalization of authoritarian methods.
- Advocates for holding those responsible at the top levels accountable for fostering an environment that allowed the coup attempt to happen.
- Emphasizes the importance of becoming anti-authoritarian and making it the societal norm to reject authoritarian measures from the government.
- Calls for the removal of President Trump through the 25th Amendment, impeachment, or resignation, citing his erratic behavior as a reason.
- Stresses the need for citizens to set the tone against authoritarianism and work towards walking back from the edge to prevent such incidents in the future.

### Quotes

- "It was an attempted coup."
- "Nobody should suffer under the boot of authoritarianism."
- "For those who propose militarizing the police and militant action against the citizens by the government, that has to be outside the norm."

### Oneliner

Beau stresses the seriousness of the attempted coup on Capitol Hill, urges accountability, advocates for anti-authoritarianism, and calls for the removal of President Trump to prevent authoritarianism becoming the norm.

### Audience

Americans

### On-the-ground actions from transcript

- Hold those responsible at the top levels accountable for fostering an environment that allowed the coup attempt (implied).
- Advocate and work towards making anti-authoritarianism the societal norm by rejecting authoritarian measures from the government (implied).
- Support each other and seek equality and freedom, not oppression (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of the attempted coup on Capitol Hill, the risks of authoritarianism, and the necessary steps for Americans to prevent such incidents in the future.

### Tags

#CapitolHill #CoupAttempt #AntiAuthoritarianism #Accountability #RemovalOfTrump


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about the events on Capitol Hill.
We're going to talk about what they were
and why many Americans do not want to use
the very correct descriptive term for what they were.
Because
there is the idea in the United States that it can't happen here.
But it can,
and it almost did.
So not to put too fine a point on it,
what occurred on Capitol Hill was an attempted coup.
You look at the intent of those people who engaged in it.
They said it on live stream and in videos.
It's a revolution. It's not.
We're here to make sure Trump gets his second term.
Trump is still your president.
Their intent was very clear.
They were to disrupt
the transfer of power.
That's what it was about.
A revolution,
there's a bunch of difference between a revolution and a coup, but to keep it simple in conversation,
a revolution is about an idea,
a change of societal order.
A coup
is about
a person
and changing the head of state.
That's the difference.
The goal was to change the head of state
from Biden back to Trump.
It was an attempted coup.
It failed, yes.
We talked about it. We knew it was going to.
For those who watched that series of videos,
what happened,
that was the chaos.
I said Trump could cause chaos. He couldn't succeed.
That was the chaos.
But the idea that it can't happen here at all,
that's false.
It failed because it's Trump.
A successful coup needs certain things,
things that Trump is
incapable of providing,
most importantly support of the military.
Had
this attempt had support of the military,
it would have been much, much worse.
That may be
strong motivation to stay politically engaged
because the next Trump
may be able to get support of the military.
If we keep walking down the road of authoritarianism
on a long enough timeline,
somebody will attempt it who can get their support.
It's a resilient system,
but it's not perfect.
It can be manipulated.
It withstood this onslaught from Trump,
but it's not invulnerable.
So what can we do as Americans
to make sure that this doesn't happen again?
First, acknowledge what it was.
Don't downplay it.
Don't turn it into,
oh, boys will be boys type of things.
It was an attempted coup.
Understand how close we are to the abyss of authoritarianism
because as soon as a coup succeeds,
that becomes the normal method of transferring power,
and it's just all downhill from there.
The next thing is to hold those responsible accountable.
Now, when I say that,
I mean those at the top,
those who fostered the environment
that allowed that idea to flourish.
The FBI's open investigations,
it appears to me that they're going to be focusing
on what amounts to middle management,
those who were on the ground the day of
and directed events on scene.
Historically, that's not an incredibly effective strategy.
Those at the top can always get new middle management.
It doesn't really act as a deterrent.
The next thing is to become anti-authoritarian.
Now, if you're watching this channel
and you watch it frequently and you enjoy this channel,
this isn't going to be an issue for you.
You probably already are
or at least have anti-authoritarian tendencies,
but we have to make it the societal norm.
Remember, to change society,
you don't have to change the law.
You have to change thought.
We have to create an environment
in which being anti-authoritarian
is a prerequisite to being patriotic,
to being a good American,
and not just for your in-group, but for everybody.
Nobody should suffer under the boot of authoritarianism.
That's got to be a goal.
We have to make it the norm for people to reject
authoritarian measures from the government.
The last thing is the removal of President Trump
via the 25th Amendment, impeachment, resignation.
It doesn't matter.
He has to go.
He has proven himself too erratic
to hold the levers of power.
Sadly, there's nothing we can do about that.
That's up to the representatives.
That's up to other politicians.
Holding them accountable,
maybe law enforcement will do it.
If not, it's up to us at the ballot box.
In the meantime, we have to set the tone
that embracing authoritarianism is wrong.
It's un-American.
It always has been.
And we have to walk back from that edge,
because right now we are close.
And I know that yesterday scared a lot of people,
and it was unnerving to a lot of people.
If we don't walk back, if we don't step back from this edge,
we're going to see it more and more often.
The first step towards walking back
is acknowledging what this was.
It was an attempted coup.
It can't be spun any other way.
It was an attempt to change the head of state
by a small group of people using force.
It was an attempted coup.
It failed spectacularly,
but that doesn't mean the risk is gone.
There will be another ambitious person like Trump,
and as resilient as the American system has proven,
more so than I thought it would be, it's not invulnerable.
It's up to the average citizen
to make being anti-authoritarian the norm.
For those who propose militarizing the police
and militant action against the citizens by the government,
for those who propose that and advocate that,
that has to be outside the norm.
We have to support each other.
We need to seek equality and freedom, not oppression.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}