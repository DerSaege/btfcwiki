---
title: Let's talk about Senator Hawley and the First Amendment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=f2tdAxBQgW0) |
| Published | 2021/01/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senator Howley is in hot water with his publisher over his book deal.
- The senator believes his contract was canceled due to representing his constituents in a debate on voter integrity.
- The senator argues that the issue is not just a contract dispute but a direct assault on the First Amendment.
- Beau questions whether the situation is truly a First Amendment issue or a business decision by the publisher.
- Beau suggests that perhaps the senator doesn't fully understand the First Amendment, referencing a previous incident with a Supreme Court justice confirmation.
- Two possible explanations Beau offers are that the senator misunderstands the First Amendment or is using hyperbole for marketing and sales purposes.

### Quotes

- "This is not just a contract dispute. If it's not a contract dispute, what kind of dispute is it?"
- "What could go wrong with engaging in a little bit of hyperbole and getting people fired up?"

### Oneliner

Senator Howley's book deal controversy raises questions about the First Amendment understanding and marketing tactics.

### Audience

Politically engaged individuals

### On-the-ground actions from transcript

- Contact local representatives to express concerns about freedom of speech. (suggested)
- Support publishers who uphold diverse perspectives and freedom of expression. (implied)

### Whats missing in summary

The full transcript provides a nuanced exploration of the controversy surrounding Senator Howley's book deal, raising questions about the interpretation of the First Amendment and marketing strategies in political discourse.

### Tags

#SenatorHowley #FirstAmendment #BookDeal #FreedomOfSpeech #MarketingTactics


## Transcript
Well howdy there internet people it's Bo again. So today we're going to talk about a senator and
his book deal because it seems like a senator got himself in a little bit of hot water with
his publisher and now his publisher don't want to publish his book anymore. And uh the senator,
Senator Howley, he released a statement. I'm gonna read it because I think there's something
really important in here so y'all stick around. He starts off by saying this could not be more
Orwellian. That gets your attention right off the bat doesn't it? And he says the name of the
publisher here which I'm not going to is canceling my contract because I was representing my
constituents leading a debate on the senate floor on voter integrity which they have now decided to
redefine as sedition. That sounds serious but that's not actually what got my attention here.
He says let me be clear this is not just a contract dispute. That's what got my attention.
If it's not a contract dispute what kind of dispute is it you know? He tells us he says
it's a direct assault on the first amendment. You know I didn't go to Yale Law School
but I do have Google. So I went to Google and I typed in text first amendment and if you do
that it brings up the text of the first amendment. You can read it and it starts off by saying
congress shall make no law and then it lists a whole bunch of things that congress really isn't
supposed to be involved in you know. But then I got confused because that doesn't sound like what's
happening here. It sounds more like there's a business decision being made. It doesn't sound
like government censorship. It's not the government that's censoring him. It's not the government
stopping him from publishing his thing. So I was thinking well that can't be right. You know this
man went to Yale Law School. So I was thinking well maybe one of his publishers is in congress.
I looked and I mean maybe one of them is but I couldn't find them. So that didn't seem like it
to me. So I was thinking well maybe his publisher paid off, sorry, made a campaign contribution
to somebody in congress, got a little something done. You know how stuff works up in DC.
Looked on open secrets, couldn't find that either. Didn't seem like it. And then I found out congress
really hadn't been getting a whole lot done anyway. They were getting interrupted. So it seemed odd
but I figured I'd read the rest of it anyway. He said only approved speech can now be published
by a publisher. Yeah I mean that's true. That's how that works. The publisher approves something
and then publishes it. Yeah I mean it's in the name publisher. That's that's that is definitely
a true statement when you're talking about a publisher. But again it's not like the publisher
is stopping him from putting his work out. It's not like they're using the force of government
to censor him, stop him from speaking, stop him from saying things or getting his work out there
in any way. There's a whole bunch of other publishing companies. Maybe one of them would
be happy to work with him. I don't know. But again it just sounds like a business decision.
Doesn't sound like a first amendment issue but we do have to entertain the idea
that maybe the senator just doesn't understand the first amendment that well
because we do need to remember that this is a senator who voted for supreme court justice,
voted to confirm her after she was asked to kind of outline the freedoms protected by the first
amendment and she couldn't without help. So maybe that's it and you know when you really think about
it there may be other options as to what's going on here but I could really only kind of think of
two. One is that a senator who went to Yale law school doesn't understand the first amendment.
That would be one and again maybe there's other options but the only other one that I can think
of is that he does understand it but he's just engaging in a little bit of salesmanship,
a little bit of high profile marketing, a little bit of advertising, a little bit of
salesmanship, a little bit of hyperbole, getting his supporters fired up. So maybe when he does
get another book deal they'll be more eager to buy it, get him excited. He's just reframing things,
bending you know his understanding of something a little bit, making it more interesting to those
people who would be reading it, be listening to him you know. There's nothing wrong with that.
There's nothing wrong with engaging in a little bit of hyperbole and getting people fired up,
reframing things to make it sound a little bit different than it really is.
I mean what could go wrong with that?
Oh yeah, that's kind of how a lot of this started isn't it?
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}