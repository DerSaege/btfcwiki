---
title: Let's talk about context and the events of Capitol Hill....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8lz702NhyKY) |
| Published | 2021/01/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the importance of context in understanding events and politics.
- Mentions how we often ignore details to reinforce our own beliefs.
- Gives examples of how context determines moral and societal value.
- Contrasts two different events: the protests of this summer and the Capitol Hill riots.
- Emphasizes the significance of looking at the surrounding details to make informed judgments.
- Reminds us of the Civil Rights Movement and the importance of context in assessing actions.
- Points out that riots can be seen as the "voice of the unheard."
- Stresses the need to understand the context behind news and events for a deeper understanding.
- Encourages examining details like who, what, when, where, and why to grasp the full story.

### Quotes

- "Context is what determines moral value, societal value."
- "Riots are the language of the unheard."
- "You have to look at the context."

### Oneliner

Beau stresses the vital role of context in understanding events, contrasting the Capitol Hill riots with this summer's protests and reminding us of the Civil Rights Movement.

### Audience

Students, Activists, Educators

### On-the-ground actions from transcript

- Examine the context behind events to understand the full story (implied).

### Whats missing in summary

The full transcript provides a comprehensive exploration of the impact and importance of context in analyzing events and understanding societal dynamics. Viewing the entire talk offers a deeper insight into the significance of details often overlooked in mainstream narratives.

### Tags

#Context #Understanding #Events #Politics #CivilRights #Protests


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about context,
the surrounding details of an event.
We're going to do this because in the United States we have a habit
of
well, ignoring the details
when it comes to international and national events and politics.
We do this because
we like to reinforce our own beliefs
and it's a whole lot easier to spin something
if you don't look at the details and you just look at the action.
But that's not the right way to do things
because we use context
all day, every day in our normal lives and that's actually how we determine
moral and societal value.
Since the teacher asked me to talk about this
I'm uh...
we'll start off with an example
that's relevant to that group.
The action, the event
is a student
looking at their neighbor's desk.
The context, the surrounding details
determine
whether or not that's good or bad.
If the student is looking at their neighbor's desk
to uh...
get the page number so they can follow along in the book,
well that's a good thing.
If they're doing it in the middle of a test,
that's probably not a good thing.
If I was to take this
and smash through your wall
in the middle of the night,
drag you out of bed unconscious
and throw you into the back of a vehicle,
it seems like it would be a bad thing, right? And it would be
unless I had just gotten off a fire truck and I was putting you in an ambulance.
Then it would be a good thing.
Context
is what determines moral value,
societal value.
The surrounding details.
If you remove those,
you can't make an informed judgment.
What's being compared right now
are the events
of uh...
Capitol Hill recently
and the events of this summer.
People are doing it because the actions look the same.
They appear similar.
But what about context? What about the details?
Are they really the same?
So, this summer, what was it about?
Why was it occurring?
Uh...
systemic racism, a whole bunch of unjust violence being visited upon people that may
really not have deserved it,
and an encroaching authoritarian state.
There were other reasons,
but those were the uniting ones.
What about Capitol Hill?
It was in furtherance
of an authoritarian state.
It was about undermining
the uh...
the people's voice.
Because some people didn't like the outcome and they had been misled by
people in power.
Those don't seem the same to me.
Same way to you, if you're one of those people who believes that
violence is always wrong, no matter what,
then yeah, to you, morally they're the same.
Most people aren't like that, though.
You cannot draw
equivalencies
based solely on the action.
I would imagine,
as high school students, that
your textbook
probably mentions the Civil Rights Movement.
I would hope so, anyway.
Uh...
probably focuses on the nineteen sixties and Martin Luther King. I have a dream in all that.
Very peaceful.
Charismatic Southern preacher, gave some speeches and
everybody held hands and kumbaya and all that.
What's the reality?
There were hundreds of riots.
Hundreds!
What's the context?
A whole bunch of people
who were disenfranchised.
They didn't have a voice
in their government, in their own lives,
and it was institutionalized through law.
I mean, that situation sounds bad.
Getting rid of that would be
a societal good.
You may look at the action
that assisted and acted as a catalyst for that,
and you, man, that's horrible.
Yeah, maybe.
But context
gets to determine the rest of it.
What about Capitol Hill?
Literally the opposite.
It was about removing
people's voice from government.
Throwing out a whole bunch of votes
because people didn't like the outcome and had been misled by a bunch of
powerful politicians.
Doesn't seem the same.
What about the events of this summer?
Systemic racism.
And although
the civil rights movement in the nineteen sixties and seventies,
it got rid of a lot of the institutional laws
that upheld
that racism.
The institutional memory is still there.
Institutions, just like people, they have habits.
And a lot of institutions
are still in the habit
of
treating certain groups of people as lesser.
And therefore,
they end up with unjust violence being visited upon them.
That's a situation that getting rid of
probably a societal good, right?
Again, you may look at the action and say, man, that's horrible. I wish they wouldn't
have done that.
But you have to look at the context.
Riots are language,
riots are the language of the unheard.
And I would point out
that even
that
peaceful, charismatic southern preacher
would not condemn a riot
without condemning
the source, the context,
what created it.
Context is incredibly important if you want to understand what's going on in
the world. You cannot get that
by watching sixty-second headline news clips. You can't get it from footage.
You have to know the details. Who, what, when, where, why, the stuff behind it.
You can fill volumes of books
with information that will never be in a newspaper.
If you want to be effective in the world, you want to be relevant, you want to
matter,
you have to know the story
behind the news.
And you're only going to get that through examining the context.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}