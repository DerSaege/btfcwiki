---
title: Let's talk about Trump, trust, and Twitter....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JqOiMAOyD3U) |
| Published | 2021/01/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau shares a story about his friend's son who got his Twitter account taken away by his mom because he was picking on another kid over Twitter and was subsequently banned from going to a sports camp.
- Drawing a parallel, Beau points out that the President of the United States had his Twitter account taken away, yet still holds immense power as the commander in chief.
- Beau questions why those in power have not taken action to either convince the President to resign, invoke the 25th Amendment, or impeach him given the situation.
- He criticizes Congress for being hesitant to take action, with Democrats worried about political implications and Republicans concerned about appearances.
- Beau argues that regardless of political capital or appearances, it is Congress's duty to rein in the President, especially knowing his behavioral problems.
- Mentioning the recent change in Trump's tone after being convinced to read from a teleprompter, Beau expresses doubt on his sincerity and predicts a return to erratic behavior once the teleprompter is off.
- Beau suggests that Congress should share the blame for whatever actions Trump takes during his remaining time in office, as it is their responsibility to rein him in as the commander in chief.

### Quotes

- "It's their job to rein him in."
- "They know he has a behavioral problem."
- "But he's still commander in chief."
- "I don't know that I can trust that."
- "I don't know that that's a situation they should let stand."

### Oneliner

Beau questions why Congress fails to rein in a President stripped of Twitter, drawing parallels with his friend's son losing camp privileges. Congress shares blame.

### Audience

Congress members

### On-the-ground actions from transcript

- Convince the President to resign, invoke the 25th Amendment, or impeach him (implied)
- Take necessary steps to ensure the President's power is in checked (implied)

### Whats missing in summary

The emotional weight and urgency of the situation as Beau expresses deep concern over the lack of action from those in power. Watching the full video provides a more personal and impassioned perspective on the issue.

### Tags

#Congress #Trump #Twitter #Responsibility #Impeachment


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Trump, trust, Twitter, and my friend's boy.
Last year, he wanted to go to some sports camp.
Can't remember what it was, I want to say baseball, could be wrong about that but it
really doesn't matter.
The point is, he wanted to go to this camp.
And he was all set to go.
Really excited about it.
About a week before he left, his mom found out that he had been kind of picking on this
other kid over Twitter.
Looked like he was trying to start a fight.
So she took away his Twitter account.
Smart move, right?
Then she said, because he couldn't be trusted with Twitter, he couldn't be trusted to go
to the camp.
Makes sense, it's sound logic.
Because if she let him go, knowing he had behavioral problems, anything that he did
at that camp, well, that's on her.
It's her job to rein him in.
The President of the United States has had his Twitter account taken away, his social
media revoked, yet somehow he is still in charge of the most powerful fighting force
the world has ever known.
I would suggest that that's an oversight.
That's a situation that should probably be corrected.
Yet those in power, those who can do something about it, those who can convince him to resign,
invoke the 25th Amendment, or impeach him, well, they're talking about it.
They're talking about it.
Meanwhile, those of us down here on the bottom, we're waiting for them to do their duty, and
we're helpless.
We can't do anything about it.
You would think, after Congress went through what it just went through, where they were
sitting there helpless, waiting for somebody else to do their duty, they might be a little
bit more sympathetic.
But they're concerned about the political implications.
For Democrats, they're worried about appearances and expending the political capital to get
it done.
For Republicans, they're worried about looking bad.
It's not like they're going to look worse.
Go ahead and throw that out there.
And for Democrats, I would point out, it doesn't matter how much political capital has to be
expended, it's your duty.
And as far as appearances go, yes, Republicans are going to say that it's all political.
The average American understands that the election's over, you all won.
Biden's going to be in office in just a few days.
It's not really beneficial for the Democratic Party to do this.
I think your average American will understand that.
It's just a few days.
And we got it.
We understand that somebody got to him, somebody got to Trump and convinced him to come out
and be teleprompter Trump again and say all of the right things that he was told to.
But see, we've seen this routine before.
We know that the second that teleprompter went off, well, he's back to good old erratic
Trump, doing whatever he wants.
Because those who have the responsibility to rein him in aren't doing it.
It's only a few days, what's worse that can happen, right?
I just want to point out that whatever Trump does with his remaining time in office, Congress,
all of Congress, shares part of the blame.
It's on them.
It's their job to rein him in.
They know he has a behavioral problem.
They know he can't be trusted with Twitter.
But he's still commander in chief.
I don't know that I can trust that.
I don't know that that's a situation they should let stand.
I would suggest they have a duty to remedy that.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}