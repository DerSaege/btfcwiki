# All videos from January, 2021
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2021-01-31: Let's talk about Trump losing his legal team before the impeachment.... (<a href="https://youtube.com/watch?v=tI38rS_djEM">watch</a> || <a href="/videos/2021/01/31/Lets_talk_about_Trump_losing_his_legal_team_before_the_impeachment">transcript &amp; editable summary</a>)

Former President Trump's legal team splits ahead of his impeachment trial, revealing his focus on baseless claims and potential future political ambitions, while celebrating losses and challenging constitutionality.

</summary>

"Even if you are alleged to have used the power of your office to foment a spirit of rebellion and then told them to go fight, you're entitled to a defense."
"One of the current talking points is that 45 senators said that they believed impeaching a president after they're out of office is unconstitutional."
"If politicians in general understood the constitution and applied it rather than attempting to safeguard their own political futures, the Supreme Court wouldn't have much to do."

### AI summary (High error rate! Edit errors on video page)

Former President Trump is reportedly losing large portions of his legal team ahead of his second impeachment trial.
The split occurred because the legal team wanted to present legal arguments, while Trump wanted to focus on baseless claims and complaints.
There's a theory that the lawyers backed out due to concerns about fallout from representing Trump, but Beau disagrees.
Despite the allegations against Trump, everyone is entitled to a defense in the United States.
Attorneys are generally immune to guilt by association when representing clients.
Trump seems to be using the impeachment proceedings as a platform for a potential future political career.
His campaign tone could center around claims of the election being stolen from him if he runs again in 2024.
Trump's camp appears to have become accustomed to losing, celebrating their losses.
The argument made by 45 senators about impeaching a president after they've left office being unconstitutional is considered a loss.
Some senators are more focused on their political futures than upholding constitutionality, as seen by their actions.

Actions:

for political observers,
Pay attention to the legal proceedings related to Trump's impeachment trial (implied).
Engage in critical thinking about the actions and statements of political figures (implied).
</details>
<details>
<summary>
2021-01-30: Let's talk about Los Angeles and Caitlin Doughty's request for assistance... (<a href="https://youtube.com/watch?v=gUUisU7VD9Y">watch</a> || <a href="/videos/2021/01/30/Lets_talk_about_Los_Angeles_and_Caitlin_Doughty_s_request_for_assistance">transcript &amp; editable summary</a>)

Beau addresses the lack of response in LA to a situation requiring assistance, discussing the Department of Defense's plan and urging action from President Biden to provide help swiftly.

</summary>

"This should have been the plan from day one. It was. It was the plan since way before day one. Why didn't it happen? Politics."
"If you're having to call in the Department of Defense to assist in disposal, your mitigation efforts didn't go so well."
"Biden is new. He can make this happen very quickly."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the lack of response in LA, California, to a situation requiring assistance.
Caitlin Doughty, a popular personality on YouTube and owner of a funeral home, requested help due to the overloaded network in LA.
The Department of Defense has a plan, known as DSCA, that includes dealing with recovery, identification, registration, and disposal.
Despite the plan being on the books for a long time, it hasn't been implemented due to politics.
The process of assistance can be initiated through a request from California or a directive from the Secretary of Defense or the president.
Budgetary concerns are the only potential obstacle on the checklist for assistance from the Department of Defense.
Beau calls on President Biden to take quick action as he does not bear responsibility for previous inaction.
Beau urges viewers to pressure for assistance as the plans, program, and personnel exist for a rapid solution.
This situation serves as a reminder that the public health crisis is ongoing, and precautions like handwashing and mask-wearing are still necessary.
Beau encourages viewers to watch Caitlin's video, take appropriate action, and help those in need during this challenging time.

Actions:

for advocates for swift assistance,
Contact California officials or urge the Secretary of Defense or President to initiate assistance (suggested)
Pressure for quick action from President Biden to utilize existing plans and resources for assistance (suggested)
</details>
<details>
<summary>
2021-01-29: Let's talk about what Mattis said about the Capitol.... (<a href="https://youtube.com/watch?v=IjExFX7izMg">watch</a> || <a href="/videos/2021/01/29/Lets_talk_about_what_Mattis_said_about_the_Capitol">transcript &amp; editable summary</a>)

Former Secretary of Defense Mattis and Michael Vickers privately discussed the erosion of democracy and the dangers of internal threats, criticizing actions that prioritize politics over the nation's well-being.

</summary>

"They cannot pretend to be patriots. They cannot pretend to care about this country or they wouldn't be putting their own political careers over the underpinnings of a democracy."
"Those who understand how these things go do not want to see it happen in the United States."
"Not really something that was designed or intended to be widely distributed."

### AI summary (High error rate! Edit errors on video page)

Former Secretary of Defense, General Mattis, had a private, non-publicized chat with Michael Vickers on the OSS Society webcast.
They discussed external and internal issues facing the United States, including the erosion of democracy and the events of January 6th.
General Mattis expressed concern about the erosion of democracy and criticized the actions of a sitting president.
Michael Vickers emphasized the oath to the Constitution against all enemies, foreign or domestic, and expressed disbelief at the insurrection on January 6th.
Vickers, less known than Mattis, has an extensive military and intelligence background, bringing credibility to his statements.
Those well-versed in military matters see the events of January 6th differently from a political or legal perspective.
Individuals like Mattis and Vickers, critical for such operations, do not support actions undermining democracy.
Lack of accountability for such actions can lead to further attempts, considering failures as training rather than defeat.
The Republican Party's prioritization of politics over the nation's well-being is concerning.
Those who truly understand the situation view internal threats as more dangerous than external ones, questioning the patriotism of individuals putting politics over democracy.

Actions:

for citizens, patriots, activists.,
Contact local representatives to prioritize democracy over politics (suggested).
Join organizations advocating for accountability in political actions (implied).
</details>
<details>
<summary>
2021-01-28: Let's talk about why Republicans are confused in the Senate.... (<a href="https://youtube.com/watch?v=cZxJ0bbduZU">watch</a> || <a href="/videos/2021/01/28/Lets_talk_about_why_Republicans_are_confused_in_the_Senate">transcript &amp; editable summary</a>)

The Republican Party faces a dilemma in Trump's impeachment trial, urged to prioritize country over politics to expose the truth and move forward.

</summary>

"Put the country first."
"Sunlight's a great disinfectant."
"Imagine what is going to happen if something bad happens."
"They stopped the best mechanism we have to debunk these baseless claims."
"You want to save your party, you want to save your political careers, for once it lines up with doing what's best for the country."

### AI summary (High error rate! Edit errors on video page)

Analyzes the Republican Party's stance on Trump's second impeachment trial.
Questions why the Republican Party is hesitant about witnesses in the trial.
Suggests that senators may fear what the witnesses will reveal.
Points out the dilemma faced by the Republican Party in either convicting or acquitting Trump.
Urges the Republican Party to prioritize the country over political interests.
Stresses the importance of exposing the truth and debunking baseless claims for the country to move forward.
Warns about the consequences of obstructing the trial and not allowing the truth to surface.
Calls for a focus on what's best for the country rather than political gain.

Actions:

for politically engaged citizens,
Prioritize country over political interests by supporting transparency in political decisions (implied).
Advocate for exposing the truth and debunking baseless claims for the country's progress (implied).
Hold elected officials accountable for their actions and decisions, urging them to prioritize the nation's well-being (implied).
</details>
<details>
<summary>
2021-01-28: Let's talk about Biden and oil jobs.... (<a href="https://youtube.com/watch?v=GTSQlkIgjZc">watch</a> || <a href="/videos/2021/01/28/Lets_talk_about_Biden_and_oil_jobs">transcript &amp; editable summary</a>)

Biden's plan overlooks energy jobs that technology is making obsolete, urging workers to advocate for their own interests amidst industry decline.

</summary>

"Technology is making those jobs obsolete."
"Manufacturing may come back, but the jobs won't."
"Advocate for your own interest for a change."
"They're not going to take care of you."
"Those jobs are going away too. It's probably a good thing."

### AI summary (High error rate! Edit errors on video page)

Biden's new plan doesn't account for energy jobs, including those in the oil sector.
The energy industry has been declining, with companies like BP planning to cut fossil fuel output by 40% by 2030.
Technology is making many jobs in the industry obsolete.
The industry has had significant political power and received handouts to stay afloat.
Manufacturing jobs are unlikely to return as automation takes over.
Workers in the energy industry should advocate for educational programs to transition to new jobs.
Rather than supporting billionaires, workers should advocate for their own interests as the industry declines.
Workers should not expect the industry to take care of them when it disappears.
Jobs in cleaning up spills are also disappearing.
Advocating for educational programs can help workers transition to new jobs.

Actions:

for workers in the energy industry,
Advocate for educational programs to transition to new jobs (implied)
Call up senators and representatives to push for programs benefiting workers (implied)
</details>
<details>
<summary>
2021-01-27: Let's talk about Biden's national security priorities.... (<a href="https://youtube.com/watch?v=WscMraOIjnM">watch</a> || <a href="/videos/2021/01/27/Lets_talk_about_Biden_s_national_security_priorities">transcript &amp; editable summary</a>)

President Biden reframes climate and healthcare as national security issues to drive action, utilizing the military's influence for effective change, acknowledging urgency in addressing pressing global challenges.

</summary>

"Increased food insecurity, increased water insecurity, new areas becoming possible battle spaces because of retreating ice caps, migration patterns, rising sea levels that might affect ports. All of these things are national security issues."
"We can't let the fact that we feel icky about it get in the way of saving lives."
"It's probably one of the most effective. And we're running out of time."
"The urgency has been there."
"We are moving out of the alternative facts era and moving into actually addressing the issues that are facing this country and facing this planet."

### AI summary (High error rate! Edit errors on video page)

President Biden plans to issue an executive order shifting the national security framing on climate issues.
Climate change impacts like food and water insecurity, retreating ice caps, and rising sea levels are considered national security issues by the Army.
The use of national security framing is aimed at getting politicians who typically oppose environmental protection measures to support them.
Beau acknowledges ideological concerns about using the military's clout but believes it will be effective in addressing climate change.
He suggests that healthcare can also be framed as a national security issue due to its impact on readiness and economic growth.
Beau advocates for utilizing every tool available, including the Department of Defense's influence, to address urgent issues.
The urgency to address climate change has always been present, and framing it as a national security issue is a practical approach.
Beau stresses the need to focus on addressing real issues rather than dwelling in the era of alternative facts.
Using the Department of Defense's influence to push for environmental and healthcare reforms may not be ideologically pure but is seen as effective.
The urgency to act on climate change and healthcare is emphasized due to the pressing nature of the issues.

Actions:

for policy advocates,
Advocate for framing climate change and healthcare as national security issues (suggested)
Support policies that address environmental and healthcare challenges (implied)
</details>
<details>
<summary>
2021-01-26: The roads of Thomas Paine and Common Sense.... (<a href="https://youtube.com/watch?v=jCGX1AbA85c">watch</a> || <a href="/videos/2021/01/26/The_roads_of_Thomas_Paine_and_Common_Sense">transcript &amp; editable summary</a>)

Beau reveals the progressive and revolutionary legacy of Thomas Paine, challenging stereotypes and showcasing his enduring fight for freedom and societal betterment.

</summary>

"Without the pen of Paine, the sword of Washington would have been wielded in vain." - John Adams
"If to expose the fraud and imposition of monarchy, to break the chains of political superstition and raise degraded man to his proper rank be libelous, let my name be engraved on my tomb." - Thomas Paine
"Death came. Death, almost his only friend." - Robert G. Ingersoll

### AI summary (High error rate! Edit errors on video page)

Thomas Paine, a key figure in American history, was more progressive than commonly portrayed.
Paine played a pivotal role in shaping the United States and inciting the revolution.
His pamphlet, "Common Sense," transformed the focus of the conflict towards independence.
Paine's arguments in "Common Sense" centered around rejecting English authority, criticizing hereditary rule, and advocating for American independence.
He believed that England exploited the colonies and involved them in wars unrelated to their interests.
Paine emphasized the importance of local governance over distant monarchies.
Despite his impact, Paine faced opposition and criticism for his radical views on democracy and social reform.
Paine's later works, such as "Rights of Man," promoted universal education, social safety nets, and progressive taxation.
His activism led to persecution, imprisonment, and eventual exile from multiple countries.
Thomas Paine's legacy endures as a champion of freedom, equality, and societal progress.

Actions:

for history enthusiasts, activists,
Advocate for universal education and equal access to education for all (suggested)
Support social safety nets for the economically disadvantaged (suggested)
Stand against exploitation and advocate for progressive taxation (suggested)
</details>
<details>
<summary>
2021-01-26: Let's talk about controlling the narrative around climate change.... (<a href="https://youtube.com/watch?v=8URGCX4R3bc">watch</a> || <a href="/videos/2021/01/26/Lets_talk_about_controlling_the_narrative_around_climate_change">transcript &amp; editable summary</a>)

Confronting the alarming reality of global ice loss and urging immediate action to combat climate change denial and its catastrophic consequences.

</summary>

"When you're normally talking about weight and you want to visualize a large amount of weight, people compare it to a 747. Eighty billion. Eighty billion 747s."
"We have to start acting now to achieve any tangible results."
"The worst-case scenario from the IPCC, that's a disaster movie."
"The narrative cannot be controlled by those people with deep pockets who have a vested financial interest in going against the scientific consensus."
"Worst-case scenario, we make the world a better place for nothing."

### AI summary (High error rate! Edit errors on video page)

Conducted first global ice loss survey from 1994 to 2017, revealing a loss of 28 trillion metric tons of ice at the caps.
Impacting Antarctica and Greenland the most, losing about 1.2 trillion tons per year.
Less ice means less heat reflected, exacerbating the problem.
Current rate of loss puts us on track for worst-case climate scenarios from the IPCC.
Paris Agreement, based on IPCC, may not be sufficient in addressing the crisis.
Urges individuals to draw parallels with 2020's public health denials to understand climate change denial.
Warns against letting those who deny climate change control the narrative.
Urges immediate action as delays will hinder tangible results.
Calls for political leaders and influencers to prioritize understanding and addressing climate change.
Emphasizes the catastrophic consequences of worst-case climate scenarios.
Stresses the importance of not allowing financial interests to manipulate the narrative around climate change.

Actions:

for climate activists, policymakers, environmentalists.,
Contact political representatives to prioritize climate change action (implied).
Join or support organizations advocating for climate action (implied).
Educate community members about the urgency of addressing climate change (implied).
</details>
<details>
<summary>
2021-01-26: Let's talk about about how the impeachment should go.... (<a href="https://youtube.com/watch?v=kFjC8Wi8GUg">watch</a> || <a href="/videos/2021/01/26/Lets_talk_about_about_how_the_impeachment_should_go">transcript &amp; editable summary</a>)

Senate Democrats aim for a fair and fast trial while understanding the importance of holding Trump accountable for the Capitol incident, shedding light on the truth, and aiding in national healing by bringing witnesses to testify.

</summary>

"We have to have accountability, and the truth is really good at doing that."
"To heal, we have to actually see the wound."
"Seeing these individuals testify could help convict Trump and stop him from ever doing this again."

### AI summary (High error rate! Edit errors on video page)

Senate Democrats aim for a fair and fast trial to not interfere with Biden's first hundred days.
Blocking Trump from holding office could be part of Biden's mission.
Individuals charged in Capitol-related incidents may use loyalty to Trump as a defense.
Testifying in the Senate could force them to admit they acted on Trump's behalf.
Seeing these individuals testify could help debunk false claims and bring accountability.
Bringing witnesses in for the trial is necessary to understand the alleged incitement by Trump.
Many individuals genuinely believed in Trump's baseless claims, even if they were false.
Witness testimonies could assist in convicting Trump, preventing future incidents, and aiding national healing.
Understanding the motivations behind individuals' actions is key to moving forward and healing as a nation.

Actions:

for senate democrats, capitol incident witnesses,
Contact Senate representatives to push for a fair trial with witnesses (suggested)
Attend or support initiatives advocating for accountability and healing post-Capitol incident (implied)
</details>
<details>
<summary>
2021-01-25: Let's talk about who should preside over Trump's impeachment.... (<a href="https://youtube.com/watch?v=-5UGUTUCdpI">watch</a> || <a href="/videos/2021/01/25/Lets_talk_about_who_should_preside_over_Trump_s_impeachment">transcript &amp; editable summary</a>)

Beau explains the procedural aspects of the second impeachment, criticizes GOP for undermining the process, and warns about the consequences of their actions.

</summary>

"If nobody's held accountable, they will learn nothing, and this will continue."
"The Republican Party has learned nothing, and this is why the impeachment is so important."
"This is yet another attempt to undermine the very foundations of this country."
"You need to stop pretending that you care about the Constitution and just admit what you are."
"If they continue with this, their political futures are over, not because they're going to get voted out, but because the country will fall apart."

### AI summary (High error rate! Edit errors on video page)

Explains the procedural aspects of the second impeachment process.
Clarifies that Chief Justice Roberts will not preside over the impeachment of a former president.
GOP is making a fuss about the absence of Chief Justice Roberts, implying a lack of seriousness in the trial.
Emphasizes that the Constitution grants the Senate the sole power to try all impeachments.
Points out that the impeachment is not of the current president but a former one.
Criticizes the GOP for attempting to undermine the impeachment process and the Constitution.
Suggests Vice President Harris as a logical choice to preside over the impeachment.
Notes the importance of accountability to prevent continued undermining of democratic processes.
Draws parallels between GOP actions and their behavior regarding the election.
Warns about the consequences of undermining the Constitution for political gains.

Actions:

for citizens, voters, activists,
Hold elected officials accountable for their actions (implied)
Support accountability and transparency in the political process (implied)
Advocate for upholding democratic values and the Constitution (implied)
</details>
<details>
<summary>
2021-01-25: Let's talk about Bernie Sanders taking his gloves off and the budget.... (<a href="https://youtube.com/watch?v=cRHTHd9PPGU">watch</a> || <a href="/videos/2021/01/25/Lets_talk_about_Bernie_Sanders_taking_his_gloves_off_and_the_budget">transcript &amp; editable summary</a>)

Republicans block stimulus, Sanders eyes reconciliation to pass it, needing Democratic support.

</summary>

"Bernie Sanders promised to take his gloves off yesterday."
"Whether or not this is what the founders intended on how it's supposed to work, that's up for debate, but it is certainly how it works right now."
"I just hope that the Democratic Party gives Sanders the backup that he needs."

### AI summary (High error rate! Edit errors on video page)

Republicans are blocking the stimulus on Capitol Hill, despite Democrats having a slim majority in the Senate.
Bernie Sanders is the chair of the budget committee and could use the reconciliation process to pass the stimulus with a simple majority.
Reconciliation doesn't require 60 votes and can't be filibustered.
Sanders has promised to use this procedural method to get the stimulus through without passing a new package.
This process could also potentially adjust the minimum wage.
McConnell won't be able to stop this process, but he may criticize Sanders for his past opposition to it.
The House will also go through a reconciliation process after the Senate.
The debate time in the Senate for reconciliation is limited to 20 hours, and the process between the House and Senate is limited to 10.
This process can't be used for everything but can be pivotal in passing much of the stimulus package.
Sanders needs support from the Democratic Party to follow through on this.

Actions:

for politically engaged individuals.,
Support Sanders in using the reconciliation process (implied).
Push for Democratic backing of Sanders (implied).
</details>
<details>
<summary>
2021-01-24: Let's talk about the government admitting the assessments were off.... (<a href="https://youtube.com/watch?v=Hb1n2YYnt8U">watch</a> || <a href="/videos/2021/01/24/Lets_talk_about_the_government_admitting_the_assessments_were_off">transcript &amp; editable summary</a>)

Beau warns against giving the government more surveillance tools, stressing the need for better policy and information sharing to avoid authoritarian measures.

</summary>

"We cannot allow the 6th to become another 11th."
"They do not need more tools."
"Better policy, better information sharing, and better use of the tools that exist."
"No more tools."
"We're going to be anti-authoritarian."

### AI summary (High error rate! Edit errors on video page)

Articles suggest federal government's response on the 6th was lackluster.
Government wants to provide a comprehensive threat assessment.
Beau warns against giving government more tools for surveillance.
Lack of effective response on the 6th was not due to a lack of tools.
Lack of information sharing and use of open-source intelligence were the issues.
Beau stresses the need for better information sharing and policies.
Open-source intelligence can develop reasonable suspicion without violating civil rights.
Beau advocates against giving more power to the federal government.
Calls for better policy, information sharing, and use of existing tools.
Urges for an anti-authoritarian stance against authoritarian measures.

Actions:

for activists, concerned citizens,
Advocate for better policy and information sharing (implied)
Stand against authoritarian measures (implied)
Educate others on the importance of civil liberties (implied)
</details>
<details>
<summary>
2021-01-24: Let's talk about Biden's public health response.... (<a href="https://youtube.com/watch?v=gZU4ny0jP9k">watch</a> || <a href="/videos/2021/01/24/Lets_talk_about_Biden_s_public_health_response">transcript &amp; editable summary</a>)

Beau explains Biden's response plans to current events, addressing mask mandates, testing supplies, data surveillance, and vaccination centers while stressing the importance of accurate reporting and vaccine awareness.

</summary>

"It's more like an 18-wheeler. You hit the brakes and it's going to keep rolling for a while."
"Some states may have been less than accurate in their reporting."
"Make sure you know where or when to get your vaccine."
"Wash your hands. Don't touch your face. Stay at home. If you have to go out, wear a mask."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Explains President Biden's plans and actions in response to current events.
Mentions executive orders issued on the first day, primarily focused on mask-related issues.
Details specific orders including speeding up delivery of testing supplies, using National Guard for a health response, creating a testing board, and increasing data surveillance.
Talks about federally funded vaccination centers, guidelines for schools, OSHA involvement for worker protection, and equitable response plans.
Addresses the inevitability of things getting worse before getting better, likening it to an 18-wheeler slowly stopping.
Raises concerns about potential inaccuracies in state reporting leading to increased numbers, but sees increased data surveillance as a positive.
Emphasizes the importance of finding out where and when to get vaccinated.
Concludes with basic COVID-19 safety reminders: handwashing, mask-wearing, and staying informed.

Actions:

for health-conscious individuals,
Find out where and when to get vaccinated (exemplified)
</details>
<details>
<summary>
2021-01-23: Let's talk about the good and bad of Biden's new SecDef.... (<a href="https://youtube.com/watch?v=YMshlxYKhDM">watch</a> || <a href="/videos/2021/01/23/Lets_talk_about_the_good_and_bad_of_Biden_s_new_SecDef">transcript &amp; editable summary</a>)

Beau explains the concerns surrounding General Austin's appointment as Secretary of Defense, from conflicts of interest to the importance of civilian control over the military.

</summary>

"He's the first black Secretary of Defense. All of this is good."
"We can't get into the habit of using retired generals, especially recently retired generals as Secretary of Defense."
"The tradition of civilian control of the military is really important in this country."
"He will probably end up being a pretty embattled Secretary of Defense."
"Civilian control of the military is important."

### AI summary (High error rate! Edit errors on video page)

Explains the pushback against newly confirmed Secretary of Defense, General Austin, despite his qualifications and historic appointment as the first Black Secretary of Defense.
Notes potential conflicts of interest due to Austin's previous role on the board of Raytheon, a company that does business with the Department of Defense.
Raises concerns about the short time span between Austin leaving the military and assuming the position of Secretary of Defense, potentially impacting civilian control of the military.
Emphasizes the importance of maintaining civilian control over the military and avoiding the trend of appointing recently retired generals as Defense Secretaries.
Acknowledges that while he personally doesn't have concerns about Austin in this administration, the broader issue of civilian control is significant.

Actions:

for policy advocates,
Monitor for conflicts of interest and influence related to defense contractors like Raytheon (implied).
Advocate for and support civilian oversight of military appointments (implied).
</details>
<details>
<summary>
2021-01-23: Let's talk about Kevin McCarthy and sharing responsibility.... (<a href="https://youtube.com/watch?v=An5qqoqG094">watch</a> || <a href="/videos/2021/01/23/Lets_talk_about_Kevin_McCarthy_and_sharing_responsibility">transcript &amp; editable summary</a>)

House Minority Leader Kevin McCarthy shifts blame, but Beau calls out Republican Party leadership and demands accountability before unity.

</summary>

"The blame, the fault for this lies at the feet of the leadership of the Republican Party."
"They remained silent as it was ripped apart."
"We need people to accept responsibility, allow for accountability, and then we can talk about unity."

### AI summary (High error rate! Edit errors on video page)

House Minority Leader Kevin McCarthy admitted Trump shared fault for January 6 events.
McCarthy shifted blame to all Americans, but Beau disagrees vehemently.
Beau points out McCarthy's silence as a key factor in the events.
He distinguishes between those who pushed harmful theories and those who enabled them.
Beau criticizes individuals who only speak up when facing political backlash.
Beau stresses that leadership within the Republican Party could have prevented the situation.
He refuses to accept a narrative of shared responsibility for the inaction of certain individuals.
Beau expresses his nonpartisan stance based on principles and policies.
He condemns the Republican Party for prioritizing the party over principles and the country.
Beau calls for accountability and acceptance of responsibility before unity can be achieved.

Actions:

for political activists, concerned citizens,
Hold accountable those in positions of power for their actions (implied)
Advocate for principles and policies over blind party loyalty (implied)
</details>
<details>
<summary>
2021-01-23: Let's talk about Biden's use of the pardon power.... (<a href="https://youtube.com/watch?v=NA1hy7wsHGE">watch</a> || <a href="/videos/2021/01/23/Lets_talk_about_Biden_s_use_of_the_pardon_power">transcript &amp; editable summary</a>)

President Biden holds absolute pardon power under the Constitution; he can commute death sentences to life without much political risk, potentially saving 49 lives in a bipartisan move.

</summary>

"He can save 49 lives with a pinstripe. No big deal."
"There is no waste of political capital here."
"At the end of the day, this is the right thing to do."

### AI summary (High error rate! Edit errors on video page)

President Biden's pardon power is absolute, drawn from Article 2, Section 2, Clause 1 of the U.S. Constitution.
35 Democratic lawmakers have requested Biden to commute the sentences of 49 people facing the death penalty in the federal system.
Commuting the sentences doesn't mean releasing them; it means changing their sentences to something else, likely life imprisonment.
Biden can commute all sentences to life, ensuring they serve life without any political backlash.
Biden's use of the pardon power won't be a significant political issue given the support it may garner.

Actions:

for lawmakers, activists,
Advocate for the commutation of death sentences to life imprisonment (implied)
</details>
<details>
<summary>
2021-01-22: Let's talk about impeachment docs being filed against Biden..... (<a href="https://youtube.com/watch?v=PFmtLg8X7C0">watch</a> || <a href="/videos/2021/01/22/Lets_talk_about_impeachment_docs_being_filed_against_Biden">transcript &amp; editable summary</a>)

Representative Greene's push for Articles of Impeachment against President Biden lacks substance, leading Beau to sarcastically encourage an investigation into baseless claims.

</summary>

"There's no votes to make this a thing. It's not daring, it's not bold, it's not leadership. It is a joke."
"I think this is a wonderful idea and I wish Representative Green the best of luck moving forward with it."
"So I'm assuming that she's going to reach out to the attorney general and go ahead and ask for indictments against every soldier who deployed ever because, I mean, that would make sense."
"I strongly suggest that Representative Greene take a little bit of time to learn how the organization that she is somehow a part of works."
"Anyway, It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Representative Greene is reportedly bringing forth Articles of Impeachment against President Biden, despite lacking the votes to make it happen.
The allegations against Biden regarding Ukraine have already been debunked by Senate Republicans and his political opposition.
Beau suggests running an investigation on these allegations to set a precedent for investigating elected officials solely based on appearance.
He sarcastically mentions that such investigations could also extend to members of Congress who may have personally benefited from their votes.
Beau points out that at the time of the alleged incidents in Ukraine, Biden was acting as a tool of American foreign policy.
He humorously speculates that if Biden can be held personally liable, then every soldier deployed could face indictments.
Beau questions Representative Greene's understanding of government processes and suggests she familiarize herself with how things work.
Despite the absurdity of the situation, Beau sarcastically encourages the investigation to clear things up and move forward.

Actions:

for politically-minded individuals,
Reach out to elected officials urging them to focus on substantive issues rather than baseless political maneuvers (suggested)
Educate others on the importance of understanding government processes and functions (implied)
</details>
<details>
<summary>
2021-01-21: Let's talk about the impossible, unity, and accountability.... (<a href="https://youtube.com/watch?v=KuNwPBSZG5M">watch</a> || <a href="/videos/2021/01/21/Lets_talk_about_the_impossible_unity_and_accountability">transcript &amp; editable summary</a>)

Beau covers the unlikely scenarios regarding Trump's power retention and stresses the critical need for accountability to prevent future events like January 6th.

</summary>

"Had he shown up, it would have eliminated those minutes and those seconds that were needed to secure the people in Congress."
"Those at the top need to be held accountable. If they are not, it will happen again."
"This system is resilient, but there are very, very few things that are actually impossible."
"We need to keep that fact in mind when people talk about unity."
"If these people who are responsible are not held accountable, it will happen again."

### AI summary (High error rate! Edit errors on video page)

Talks about the unlikely scenarios and why accountability matters.
Mentioned making videos detailing the likelihood of Trump's plans to stay in power.
Emphasizes that Trump was not a leader and lacked understanding of what it means to lead.
Points out that Trump's failure to recognize opportunities was a key issue.
Suggests that had Trump shown leadership on January 6th, events at the Capitol could have been very different.
Raises the question of what law enforcement might have done if Trump had attempted to gain access to the Capitol.
Acknowledges the resilience of the system despite the lack of backing Trump needed.
Stresses the importance of holding those responsible for fostering the atmosphere that allowed the events to occur.
Warns that if accountability is not enforced, similar events will likely happen again.
Expresses concern that without consequences, the cycle of events like January 6th will be hard to stop.
Urges for a focus on unity but notes that some may be seeking amnesty instead.
Asserts that unity is needed to move forward but accountability is equally vital.
Warns that without consequences for those responsible, history may repeat itself.
Encourages watching his previous videos on the topic to understand the importance of accountability.

Actions:

for policy advocates, activists,
Hold those responsible for fostering the atmosphere accountable (implied)
Encourage watching educational videos on similar topics (exemplified)
</details>
<details>
<summary>
2021-01-21: Let's talk about Biden's first 24 hours and explain all those orders.... (<a href="https://youtube.com/watch?v=zbp-0W9aR7g">watch</a> || <a href="/videos/2021/01/21/Lets_talk_about_Biden_s_first_24_hours_and_explain_all_those_orders">transcript &amp; editable summary</a>)

Beau explains Biden's unconventional approach to executive orders, detailing their content and significance while fulfilling major goals without legislation, addressing pressing issues with immediate tangible effects and setting the tone for his administration.

</summary>

"It's the patriotic thing to do."
"Undocumented immigrants will be counted in the census."
"That's going to have immediate tangible effects."
"The United States is not a small town."
"He can't run it in that manner."

### AI summary (High error rate! Edit errors on video page)

Explains Biden's executive orders and unconventional approach.
Details the content and significance of each executive order.
Mentions the extension of eviction and foreclosure moratoriums.
Talks about pausing student loan payments and interest.
Addresses rejoining the Paris Accord and abolishing the 1776 Commission.
Comments on actions regarding diversity training and equity in policymaking.
Notes the inclusion of undocumented immigrants in the census.
Talks about preserving DACA and revoking the Muslim ban.
Mentions changes in immigration enforcement policies.
Comments on halting the wall construction and ending the emergency declaration.
Talks about non-deportation of Liberians until 2022.
Addresses interpreting the Civil Rights Act to include workplace discrimination based on orientation and identity.
Mentions new ethics rules and reversing Trump's regulatory process changes.
Explains how Biden fulfilled major goals through executive orders without legislation.

Actions:

for policy advocates,
Contact local representatives to advocate for the continuation of policies like eviction and foreclosure moratoriums (implied).
Join or support organizations promoting diversity training and equity in policymaking (implied).
Organize events or workshops to raise awareness about workplace discrimination based on orientation and identity (implied).
</details>
<details>
<summary>
2021-01-20: Let's talk about Trump's accomplishments.... (<a href="https://youtube.com/watch?v=1M6CXhUS-x8">watch</a> || <a href="/videos/2021/01/20/Lets_talk_about_Trump_s_accomplishments">transcript &amp; editable summary</a>)

Beau outlines positive and negative aspects of Trump's presidency, reflecting on the characteristics of fascism, prompting reflection on the concentration of power.

</summary>

"His supporters loved the fact that under him nobody had to be ashamed that they loved the American flag or the bald eagle."
"He engaged in cronyism. He hired his family members who were less than qualified for those positions."
"That's why there was so much opposition. That's why people were so worried about the amount of power he was gaining."

### AI summary (High error rate! Edit errors on video page)

Beau responds to a request to catalog Trump's accomplishments on his last day to understand his supporters.
Beau outlines six positive aspects of Trump's presidency appreciated by his supporters, such as promoting nationalism and traditional family roles.
Trump's supporters admired his focus on national security, clear identification of enemies, and prioritization of the military.
Despite differing perspectives, Beau acknowledges that Trump's supporters valued his disregard for conventions in the pursuit of national security.
Beau mentions Trump's negative action of engaging in cronyism by hiring family members for unqualified positions.
Trump's refusal to let the media control him, protection of American business interests, and emphasis on law and order were key points for his supporters.
Additionally, Trump's stance on religion and undermining of elections are discussed as positive and negative attributes by his supporters.
Beau concludes by linking the 14 characteristics of fascism to the positive and negative aspects of Trump's presidency, raising concerns about the concentration of power.

Actions:

for supporters and skeptics alike.,
Listen to differing perspectives on Trump's presidency (suggested).
Recognize the potential impact of concentrated power (implied).
</details>
<details>
<summary>
2021-01-20: Let's talk about President Biden and where we go from here.... (<a href="https://youtube.com/watch?v=wPCUHbI_VSc">watch</a> || <a href="/videos/2021/01/20/Lets_talk_about_President_Biden_and_where_we_go_from_here">transcript &amp; editable summary</a>)

President Biden's inauguration marks a shift towards progress, but the people must build stronger communities and power structures to prevent authoritarianism in the future.

</summary>

"We managed to scrape through it and now we can focus on real progress."
"We still have a lot of work to do but yes, take a moment, breathe, enjoy the champagne."
"There's hope for moving forward and making real progress now."

### AI summary (High error rate! Edit errors on video page)

President Biden and Vice President Harris have taken office, ending a national embarrassment after four years of the Trump administration.
Biden aims to return things to the pre-Trump era, although resistance may hinder progressive changes.
The responsibility lies with the people to build stronger communities and power structures to prevent another Trump-like scenario.
The channel will focus on educating, leading, and creating progress while still covering politics and providing historical context.
There's hope for moving forward and making real progress now that the constant chaos of the news cycle may ease.
Beau plans a relaxed live stream to celebrate the moment before returning to work the next day.
While celebrating, it's vital to acknowledge the near miss the country had and focus on preventing authoritarianism.
Beau refrained from fear-mongering during Trump's term but notes the closeness to a worse outcome.
Despite dodging a crisis, there's still much work ahead, but it's okay to take a moment to breathe and celebrate.
Beau signs off, looking forward to the evening and returning to work the next day.

Actions:

for american citizens,
Celebrate the moment with friends and family (implied)
Take time to relax and breathe before getting back to work (implied)
</details>
<details>
<summary>
2021-01-19: Let's talk about Trump's last 24 hours as President.... (<a href="https://youtube.com/watch?v=SbIg43fMVMM">watch</a> || <a href="/videos/2021/01/19/Lets_talk_about_Trump_s_last_24_hours_as_President">transcript &amp; editable summary</a>)

President Trump's impending departure stirs a mix of relief and apprehension, underlining society's fragility and the need for increased political engagement to prevent future authoritarian threats.

</summary>

"That seed of doubt should keep you politically engaged."
"What brought us Trump was a combination of apathy and ignorance."
"We have to completely alter political discourse and political engagement in this country."

### AI summary (High error rate! Edit errors on video page)

President Donald J. Trump's term ends in 24 hours, leaving many feeling relieved or anxious about the transition of power.
Trump's impact on the country, including damaging international standing and domestic policies, will be debated for years.
The seed of doubt about a peaceful transfer of power underlines the fragility of society.
Beau argues that this doubt is beneficial as it keeps people politically engaged and aware of the power dynamics at play.
He believes that Trump's presidency should serve as a wake-up call for the public to be more informed, engaged, and proactive in shaping the country's future.
The rise of authoritarianism was fueled by apathy and ignorance, with many supporters unaware of the implications of their actions.
Beau stresses the importance of altering political discourse and engaging more people to prevent the rise of another, potentially more dangerous, Trump-like figure in the future.

Actions:

for american citizens,
Inform more people about different ideologies (implied)
Engage more individuals in political discourse (implied)
</details>
<details>
<summary>
2021-01-19: Let's talk about Biden's first major act.... (<a href="https://youtube.com/watch?v=OqcOtFbJT4s">watch</a> || <a href="/videos/2021/01/19/Lets_talk_about_Biden_s_first_major_act">transcript &amp; editable summary</a>)

Incoming immigration reform package sparks conservative backlash pre-details, prompting Beau to call out opposition as bigotry and advocate for informed, policy-based decisions.

</summary>

"Base your decisions on policy rather than raw emotion."
"It has nothing to do with policy. It has to do with skin tone."
"Perhaps it would be best to wait and see the bill."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Incoming Biden administration plans immigration reform package post-inauguration.
Includes pathway to citizenship for those out of status.
Conservative movement opposes reform, citing job loss and law-breaking.
Beau questions opposition as bigotry, as bill details are not yet known.
Conservative argument of needing to do things legally contradicts opposition to legal mechanism in bill.
Reform bill details not public, could include stringent background checks and long waiting periods.
Beau advocates for waiting to see bill before forming opinions.
Urges basing decisions on policy rather than emotion or preconceived notions.
Opposes bill if it doesn't go far enough but stresses importance of informed decision-making.
Encourages audience to avoid manipulation and wait for bill details.

Actions:

for advocates, voters, activists,
Wait for the immigration reform bill details before forming opinions (suggested)
Base decisions on policy rather than emotion (suggested)
Advocate for informed decision-making on immigration reform (exemplified)
</details>
<details>
<summary>
2021-01-18: Let's talk about vetting and Capitol security.... (<a href="https://youtube.com/watch?v=8eO-JsGmJyQ">watch</a> || <a href="/videos/2021/01/18/Lets_talk_about_vetting_and_Capitol_security">transcript &amp; editable summary</a>)

Beau explains the difference between perceived and actual security capabilities in DC, addressing the troop presence and FBI vetting without a specific threat.

</summary>

"90% of security is perception."
"As far as I know, as far as the public statements, as far as the activities that we're seeing, nothing indicates that they have a specific threat or that they have flagged anybody."
"Because of the events of the Capitol and the actions of the people there, all of the foreign opposition groups are emboldened."

### AI summary (High error rate! Edit errors on video page)

Explains the difference between perceived capability and actual capability in security measures.
Gives examples of perceived capability (like a stadium on game day) and actual capability (like Disney World).
Talks about the large security presence in DC and why there are so many troops present.
Mentions the rings of security, with the National Guard likely being on the outer rings.
Clarifies that the FBI vetting troops is a common practice, not necessarily due to a specific threat.
States that the FBI is involved in vetting due to their expertise and quick processing abilities.
Addresses the possibility of a threat from the troops but deems it unlikely due to the security measures in place.
Emphasizes the shift from relying on perceived capability to actual capability after the events at the Capitol.
Notes that while there are many troops present, there doesn't seem to be a specific threat identified.
Points out the broader security concerns beyond just the immediate event, considering other opposition groups.

Actions:

for security analysts, event planners.,
Trust the security measures in place (implied).
Be vigilant and report any suspicious activity (implied).
</details>
<details>
<summary>
2021-01-18: Let's talk about Pelosi's laptop and the FBI investigation.... (<a href="https://youtube.com/watch?v=o0i4iAegN14">watch</a> || <a href="/videos/2021/01/18/Lets_talk_about_Pelosi_s_laptop_and_the_FBI_investigation">transcript &amp; editable summary</a>)

Late-breaking news from the Capitol involves a complaint filed by the FBI, alleging Williams took a computer device from Speaker Pelosi's office, intending to send it to Russia, but transfer failed, raising questions about accuracy and importance in the context of a patriotic event.

</summary>

"Complaints like this often contain inaccurate information."
"It's worth being aware of this situation."
"Making the country great and all of that."

### AI summary (High error rate! Edit errors on video page)

Late-breaking news from last night regarding events at the Capitol.
Complaint filed by the FBI, information from a former romantic partner of the person named.
Former romantic partners not always the most reliable source of information.
Mention of theories about laptops being taken from the Capitol.
Witness claimed to have seen a video of Williams taking a computer device from Speaker Pelosi's office.
Williams intended to send the device to a friend in Russia for sale to SVR, Russia's intelligence service.
Transfer to Russia did not happen, and Williams allegedly still has the device or destroyed it.
FBI complaint mentions Riley June Williams.
Pelosi's staff confirmed a laptop was taken, but it contained no sensitive information.
Williams sought by the FBI for remaining in a restricted area.
Complaints may contain inaccurate information, benefit of the doubt advised.
Worth noting and being aware of the situation during the event at the Capitol.
Event emphasized patriotism and making the country great.

Actions:

for concerned citizens,
Stay informed about developments in the investigation (implied)
</details>
<details>
<summary>
2021-01-17: Let's talk about Trump's impeachment and Biden's first 100 days.... (<a href="https://youtube.com/watch?v=9xbogyfKOMg">watch</a> || <a href="/videos/2021/01/17/Lets_talk_about_Trump_s_impeachment_and_Biden_s_first_100_days">transcript &amp; editable summary</a>)

Beau advises Biden to lean into the impeachment process, undo Trump's damage, and prioritize real changes over simply not being Trump to set the tone for his administration.

</summary>

"Lean into it. Stick with it. Don't hide from the impeachment."
"They have to undo the previous administration and then they have to move forward."
"Reach across to those who opposed Trumpism, not those who brought us to the abyss of totalitarianism."
"Show us that."

### AI summary (High error rate! Edit errors on video page)

Trump's impeachment and Biden's first hundred days set the tone for a new administration.
Majority of Americans and Biden voters didn't passionately support Biden; he was seen as the best chance to oust Trump.
Biden's campaign was centered on the idea of undoing the damage done by Trump, not on being a super progressive candidate.
Beau advises Biden to lean into the impeachment process and not shy away from it.
Biden should focus on undoing Trump's executive orders and reshaping the political landscape during the impeachment.
Emphasizes the importance of accountability for those in power and undoing the damage caused by Trump to set the tone for the administration.
Biden needs to reach across the aisle for unity but should prioritize those who opposed Trumpism, not those who supported totalitarianism.
The Biden administration must undo Trump's actions and move forward with significant legislative changes to ensure real progress.
Simply not being Trump won't be enough for a successful Biden re-election or Harris election; real changes and undoing Trump's policies are necessary.
Beau suggests that Biden needs to show that his administration is more than just "not Trump" and must focus on real legislation and changes.

Actions:

for political advisors, biden administration,
Lean into the impeachment process and use it to undo Trump's executive orders (implied).
Focus on reshaping the political landscape during the impeachment (implied).
Reach across the aisle for unity with those who opposed Trumpism (implied).
Prioritize undoing Trump's actions and enacting real legislative changes (implied).
</details>
<details>
<summary>
2021-01-17: Let's talk about Biden's USAID pick and foreign policy stores.... (<a href="https://youtube.com/watch?v=OfNECazbkx8">watch</a> || <a href="/videos/2021/01/17/Lets_talk_about_Biden_s_USAID_pick_and_foreign_policy_stores">transcript &amp; editable summary</a>)

Biden’s foreign policy must balance entities like USAID and avoid overreliance on the military store to maintain American dominance effectively.

</summary>

"We colonize with corporate logos."
"We need a foreign policy that does not rely on military might."
"We need to get back to patronizing them a little bit more."
"Even in its best version, it is still about maintaining American dominance."
"A mistake, a habit of going to the military store could cause a lot of problems."

### AI summary (High error rate! Edit errors on video page)

Biden filled a significant position in foreign policy, choosing Samantha Power to lead USAID.
Samantha Power has a history of advocating for humanitarian causes but also has controversies, such as calling Hillary Clinton a monster and influencing military intervention in Libya.
There is a misconception that USAID is a front for central intelligence, but Beau explains it's more like neighboring stores in a strip mall.
American foreign policy can be likened to a strip mall with different entities like the State Department, intelligence community, military, and corporations.
Beau warns against overreliance on the military store in the strip mall analogy, advocating for a more balanced approach.
He stresses the importance of using all elements of foreign policy to maintain American dominance while avoiding excessive military intervention.
Beau encourages advocating for the use of the State Department over the military store to prevent potential problems in countering more powerful nations.

Actions:

for foreign policy analysts,
Advocate for using the State Department more in foreign policy decisions (implied)
Support a balanced approach in utilizing different elements of foreign policy (implied)
</details>
<details>
<summary>
2021-01-16: Let's talk about what North Carolina can teach the Democratic party.... (<a href="https://youtube.com/watch?v=BE6rIwYUuqo">watch</a> || <a href="/videos/2021/01/16/Lets_talk_about_what_North_Carolina_can_teach_the_Democratic_party">transcript &amp; editable summary</a>)

2,879 people leaving the Republican Party in North Carolina signal a shift in rural American sentiment, offering Democrats a chance to connect by framing social issues as kitchen table topics.

</summary>

"If Democrats want to have a very successful 2022, 2024, they might should work on figuring out how to frame social issues as kitchen table issues."
"Trump was just a symptom, and he's a symptom of the whole system."
"You can't wait. If you do, they will fall back into old habits."

### AI summary (High error rate! Edit errors on video page)

2,879 people left the Republican Party in North Carolina by changing their registration after January 6th.
The number of people leaving the Republican Party is significantly higher than usual, indicating a trend.
Rural Americans are becoming disillusioned with the GOP's scare tactics and condescending approach.
Many rural Americans are realizing that the GOP's actions do not uphold traditional American values.
Republican Party's support of authoritarianism has led to significant defections.
Democrats have the potential to reach out to rural Americans by framing social issues as kitchen table issues.
Many issues championed by the Democratic Party are actually significant to rural Americans.
Democrats often get caught up in divisive culture wars instead of focusing on issues that matter to rural communities.
Progressives have the chance to make real impacts on rural Americans' lives by addressing their needs.
The Democratic Party needs to seize the current moment to connect with rural voters before it's too late.

Actions:

for progressive activists, democratic strategists,
Reach out to rural communities and start meaningful dialogues to understand their needs and concerns (implied)
Frame social issues as kitchen table issues during political campaigns to resonate with rural voters (implied)
Encourage progressive ideas and policies that benefit rural Americans to gain their support (implied)
</details>
<details>
<summary>
2021-01-16: Let's talk about Trump, noon, and the My Pillow Guy.... (<a href="https://youtube.com/watch?v=PAx-co0j-Js">watch</a> || <a href="/videos/2021/01/16/Lets_talk_about_Trump_noon_and_the_My_Pillow_Guy">transcript &amp; editable summary</a>)

Beau clarifies the constitutional process, assuring that Trump's power ends on January 20th, despite alarming notes and theories circulating.

</summary>

"Even if they were to pull off something just wild, it ends on January 20th at noon."
"There is no mechanism left that changes that."
"All of this anxiety, all of these theories, they're moot."
"If Trump wants to retain power beyond that point, he has to shred the Constitution completely."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

The MyPillow guy, a close advisor to Trump, was photographed leaving the White House with alarming notes visible.
The notes mentioned invoking the Insurrection Act and martial law, causing concern.
Despite debunking theories, some missed the urgency of the situation and potential risks.
Even if the Insurrection Act were invoked, Trump's term still ends at noon on January 20th.
The Constitution dictates that the President and Vice President's terms end at that specific time.
The Joint Chiefs' commitment to enforcing the constitutional process further solidifies this fact.
Even in far-fetched scenarios where Biden and Harris are arrested, Trump doesn't retain power.
Any coup plan involving going against Gina Haspel from Central Intelligence seems far-fetched.
The worries and theories circulating are deemed baseless, as Trump's power ends on January 20th.
Speculation around the notes' content lacks clarity, potentially referencing internet information.
The anxiety and theories surrounding Trump's power extension are considered irrelevant and unfounded.
Trump losing power after January 20th is inevitable, barring extreme actions like shredding the Constitution.
Despite uncertainties, there's assurance that the U.S. military won't back any unconstitutional power grabs.

Actions:

for citizens concerned about trump's potential power extension.,
Stay informed on constitutional processes and limitations (implied).
Remain vigilant against baseless theories and misinformation (implied).
Uphold democratic norms and constitutional values within your community (implied).
</details>
<details>
<summary>
2021-01-15: Let's talk about the highly trained at the Capitol and the media.... (<a href="https://youtube.com/watch?v=zXLe-XikgJs">watch</a> || <a href="/videos/2021/01/15/Lets_talk_about_the_highly_trained_at_the_Capitol_and_the_media">transcript &amp; editable summary</a>)

Beau questions the media's portrayal of the Capitol crew as highly trained, warning against elevating untrained individuals to hero status and fueling their movement's growth.

</summary>

"Those guys were play acting. They were pretending."
"Your ratings are not worth ripping the country apart."
"Even though they were unarmed, if they had ever trained, muscle memory..."
"Don't make them out to be heroes."
"That's probably really bad for the country."

### AI summary (High error rate! Edit errors on video page)

Beau questions the portrayal of the Capitol crew as highly trained based on basic skills like using hand signals and getting into a stack.
The crew did not properly execute the stack formation, revealing they lacked real training and experience.
Beau explains the correct way to use the non-firing hand in a stack to maintain control of the weapon.
By falsely portraying the Capitol crew as highly trained, the media may unintentionally elevate them to folk hero status.
Beau warns against giving these groups or individuals airtime as it could serve as a platform for their PR campaigns with violence.
He stresses that allowing inaccurate statements to be broadcast can be detrimental and shift public opinion.
Beau points out that only a very small percentage of individuals had actual training present at the Capitol incident.
He expresses concern that perpetuating the false image of highly trained opposition forces could fuel the movement's growth.
Muscle memory from proper training, such as using the left hand in a specific situation, was absent in the Capitol crew's actions.
Beau urges for a stop to the portrayal of untrained individuals as heroes, as it could have damaging consequences.

Actions:

for media consumers,
Refrain from giving airtime or publicity to groups or individuals with violent intentions (implied)
Be critical of media portrayals and question narratives that could potentially glorify untrained individuals (implied)
</details>
<details>
<summary>
2021-01-15: Let's talk about Senator Hawley's lesson for Democrats.... (<a href="https://youtube.com/watch?v=5hu9uhDN4NQ">watch</a> || <a href="/videos/2021/01/15/Lets_talk_about_Senator_Hawley_s_lesson_for_Democrats">transcript &amp; editable summary</a>)

Addressing political stunts, Beau urges politicians to deliver for their constituents, warning against reverting to the status quo to avoid facing calls for resignation like Howley in Missouri.

</summary>

"The best way to fire up your base? It's not through sensational headline grabbing. It's by delivering for them."
"National politics has become about national policy."
"The people who are sent to DC are sent there to represent a specific group of people."
"If they want to have a successful 2022 or 2024, they have to deliver."
"Cannot go back to the status quo."

### AI summary (High error rate! Edit errors on video page)

Addressing the situation in Howley, Missouri, where calls for resignation are mounting due to his role in objecting to electoral votes.
Criticizing politicians who participate in political stunts solely to energize their base and gain poll numbers.
Noting that the best way to energize a base is through delivering for them, not through headline-grabbing or loyalty displays.
Emphasizing that national politics should focus on national policy and representing the specific group of people who elected officials are meant to serve.
Warning Democrats in power to deliver for those who were marginalized by the previous administration to secure future success.
Stressing the need for Democrats to address the root problems that led to Trump's presidency rather than reverting to the status quo.

Actions:

for politically engaged individuals,
Hold politicians accountable for delivering on promises and representing their constituents (implied)
Advocate for policies that address the needs and concerns of marginalized communities (implied)
</details>
<details>
<summary>
2021-01-14: Let's talk about Trump's second impeachment and McConnell.... (<a href="https://youtube.com/watch?v=4x-lvlZiIZw">watch</a> || <a href="/videos/2021/01/14/Lets_talk_about_Trump_s_second_impeachment_and_McConnell">transcript &amp; editable summary</a>)

President impeached, McConnell's risky gamble could backfire, delaying Senate trial may empower Trump post-presidency.

</summary>

"McConnell could convict Trump at trial and make sure he can never run for public office again."
"However, McConnell may be making a tactical error."
"If Trump is convicted in the senate and then takes it to the Supreme Court and wins, he is a force to be reckoned with again."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

President impeached again, heading to Senate.
Internal power struggle in GOP between McConnell and Trump.
McConnell could convict Trump to prevent future office runs.
McConnell likely stalling Senate trial to blame Democrats.
McConnell's aim is to make Trump irrelevant.
Holding Senate trial after Trump leaves office is precedent.
If Trump is convicted and appeals to Supreme Court, he may regain influence.
McConnell taking a huge gamble with this strategy.
McConnell hesitant to rush trial due to potential consequences.
Trump likely to continue seeking attention and influence post-presidency.

Actions:

for political observers, activists.,
Monitor and stay informed about the impeachment process (suggested).
Stay engaged with political developments and potential outcomes (implied).
</details>
<details>
<summary>
2021-01-13: Let's talk about how the FBI may be countering Trump supporters.... (<a href="https://youtube.com/watch?v=pqS1s2Ua-Jg">watch</a> || <a href="/videos/2021/01/13/Lets_talk_about_how_the_FBI_may_be_countering_Trump_supporters">transcript &amp; editable summary</a>)

Beau explains the Capitol incident fallout, biased group assessments, and potential law enforcement strategies for future events, urging caution amidst media sensationalism.

</summary>

"The Capitol event already occurred. They're playing catch-up."
"Just take the news of the bulletin with the acknowledgment that it's probably being upplayed by the media for the sake of ratings."
"You don't want to be caught up in one of those five."

### AI summary (High error rate! Edit errors on video page)

Explains the need to understand what went wrong at the Capitol to predict future events.
Mentions the marked difference in treatment between recent Capitol events and those from the summer.
Talks about how different groups are assessed and how biases influence these assessments.
Points out the tendency to underestimate white right-wing groups while overestimating black groups.
Addresses the issue of being ill-prepared due to biased assessments and contributing factors.
Assures that the Capitol event has already occurred and authorities are playing catch-up.
Suggests potential steps law enforcement may take, such as disrupting communications and visiting minor participants.
Emphasizes the strategy of separating peaceful individuals from potential troublemakers to influence the movement's motives.
Speculates on the unlikeliness of simultaneous events in all 50 states similar to the Capitol incident.
Advises people to stay indoors during potential chaotic events and not to get involved.
Concludes by mentioning the resources available to law enforcement and the seriousness with which they are taking the situation.

Actions:

for concerned citizens,
Stay indoors during potential chaotic events (implied)
Avoid getting involved in disruptive situations (implied)
Stay informed about local developments and potential risks (implied)
</details>
<details>
<summary>
2021-01-13: Let's talk about a message to you from the Joint Chiefs.... (<a href="https://youtube.com/watch?v=E4yVFyUso2U">watch</a> || <a href="/videos/2021/01/13/Lets_talk_about_a_message_to_you_from_the_Joint_Chiefs">transcript &amp; editable summary</a>)

Beau reveals the Joint Chiefs' message, assuring civilian leadership and the public of a peaceful transition of power amid political turbulence.

</summary>

"The U.S. military is flat out saying they're going to ignore civilian leadership, that's Trump's appointees."
"It is appalling that this message had to be drafted."
"The media did a disservice because they went through and they pulled out the most interesting quotes."
"The word 'will' doesn't just mean will. It is a statement of fact from the future."
"The American people have trusted the armed forces of the United States to protect them and our Constitution for almost 250 years."

### AI summary (High error rate! Edit errors on video page)

The media failed by focusing on sensational quotes rather than the true message.
The message from the Joint Chiefs was intended for civilian leadership and the average American.
Professional soldiers are trained to convey calm in high-stress situations.
Officers with stars on their shoulders use the word "will" as a statement of fact from the future.
The military will obey lawful orders, support civil authorities, and protect the Constitution against all enemies.
Any act to disrupt the constitutional process is against their traditions, values, and oath.
The military will ensure the inauguration of President-elect Biden on January 20th, 2020.
The message was designed to convey calm and reassure the public about a peaceful transition of power.
Beau criticizes political and media figures for creating a situation where the Joint Chiefs had to assure an orderly transition of power.
The U.S. military will ignore civilian leadership (Trump's appointees) and ensure a smooth transition to the new president.

Actions:

for americans,
Support the values and ideals of the nation (implied)
Ensure public safety in accordance with the law (implied)
Uphold the Constitution against all enemies (implied)
</details>
<details>
<summary>
2021-01-12: Let's talk about Pence, the 25th Amendment, and a story.... (<a href="https://youtube.com/watch?v=42jmnZRCGxo">watch</a> || <a href="/videos/2021/01/12/Lets_talk_about_Pence_the_25th_Amendment_and_a_story">transcript &amp; editable summary</a>)

Vice President Pence's pivotal role in acknowledging presidential incapability underscores the need to prioritize country over politics.

</summary>

"If Pence did indeed take such actions, it signifies his acknowledgment that the president is unfit to lead."
"Prioritizing the country over politics is vital."
"All eyes are on the vice president."

### AI summary (High error rate! Edit errors on video page)

Vice President Pence is expected to be asked by the House to invoke the 25th Amendment and take charge of the executive branch.
Pence is aiming to show to the world that the U.S. government is fully operational during his remaining days in office.
Beau questions the effectiveness of conveying a functioning government when current actions don't seem focused on governance.
The executive branch appears to be distracted, leaving Congress to strive for some level of functionality.
Beau suggests that Pence, by taking action, could be the one to address the lack of a fully functioning government.
There are rumors of Pence displaying bravery during the Capitol events, potentially invoking Rule 303 and releasing the National Guard against the president's wishes.
If Pence did indeed take such actions, it signifies his acknowledgment that the president is unfit to lead.
Despite potential political implications, Beau believes prioritizing the country over politics is vital.
The focus is on Vice President Pence as he plays a significant role in the current situation.

Actions:

for political observers, concerned citizens,
Support initiatives that prioritize effective governance over political appearances (implied)
</details>
<details>
<summary>
2021-01-12: Let's talk about Biden's surprise appointment.... (<a href="https://youtube.com/watch?v=wJIfJrvuyR8">watch</a> || <a href="/videos/2021/01/12/Lets_talk_about_Biden_s_surprise_appointment">transcript &amp; editable summary</a>)

Biden's surprising choice of William Burns as DCI sparks speculation on CIA reforms and American foreign policy direction with long-term implications.

</summary>

"Burns has no intelligence experience whatsoever, none."
"He understands foreign policy. He understands the world."
"What Burns does in this chair is going to shape a lot of American foreign policy in the future."
"This is somebody to watch and to pay attention to."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Biden's appointment of William Burns as the Director of Central Intelligence surprises many due to Burns' lack of intelligence experience.
Burns, a diplomatic heavyweight, brings vast experience in high-profile areas like Russia and the Near East.
Biden's choice of Burns for this role raises questions about the future direction of the CIA and American foreign policy.
Burns' expertise lies in consuming intelligence products, not in intelligence operations.
Speculations arise around three possible reasons for Burns' appointment, including reforming the CIA and acting as a second Secretary of State.
The impact of Burns' decisions in this role will significantly influence American foreign policy for the next decade or more.

Actions:

for foreign policy analysts,
Watch and monitor the decisions and actions of William Burns in his role as DCI (implied).
</details>
<details>
<summary>
2021-01-11: Let's talk about the Capitol, accountability, and then unity.... (<a href="https://youtube.com/watch?v=KX_BYWI1B2w">watch</a> || <a href="/videos/2021/01/11/Lets_talk_about_the_Capitol_accountability_and_then_unity">transcript &amp; editable summary</a>)

The party of personal responsibility avoids accountability, while unity and welcoming former adherents are seen as vital steps forward.

</summary>

"We have to welcome them. Now, not just is this important in the sense of dealing with the ideology and reducing their numbers, that's good for society as a whole."
"Those who step away, they have to be welcomed back into normal society, no matter how irritating it is."
"We need to work to make sure it never happens again."
"As we move forward, we need to remember that the country has been through a lot worse than this."
"This was a near miss, and there's still a lot of risk out there."

### AI summary (High error rate! Edit errors on video page)

The party of personal responsibility is not taking any for their part in recent events, making accountability unlikely.
The strategy seems to be targeting middle management with dozens of arrests, but more charges are expected.
Pelosi is introducing a resolution for Pence to invoke the 25th Amendment, followed by impeachment, potentially using the 14th Amendment for votes.
Accountability measures from the free market include the PGA and Stripe cutting ties with Trump, impacting millions, and a social media network facing severe consequences.
DC is heavily surveilled, leading to the capture of individuals involved in recent events trying to leave the area.
Unity is necessary, even for those who were part of the ideology but want to step away and rejoin society.
Welcoming those who step away from harmful ideologies is vital for reducing authoritarianism and preventing unpredictable and tragic events.
Despite the challenges faced, the country has been through worse, and unity will be key in moving forward and ensuring such events do not recur.

Actions:

for policy makers, activists,
Welcome individuals stepping away from harmful ideologies into normal society (implied)
Work towards ensuring such events never happen again by fostering unity (implied)
</details>
<details>
<summary>
2021-01-10: Let's talk about Trump's Alamo farewell tour.... (<a href="https://youtube.com/watch?v=iaceur2Y3vA">watch</a> || <a href="/videos/2021/01/10/Lets_talk_about_Trump_s_Alamo_farewell_tour">transcript &amp; editable summary</a>)

President's final week antics include symbolic tour; focus on facts, not provocations or distractions.

</summary>

"Don't give him that victory right at the end."
"Do not give it to him. If you do, that becomes the story. That becomes what's remembered."
"At the end of the day, do not remember the Alamo. Remember the Capitol."

### AI summary (High error rate! Edit errors on video page)

President's final week in office discussed.
Executive orders targeting big tech mentioned.
President planning a farewell tour, including a stop in Alamo, Texas.
Symbolism of the Alamo being pointed out.
Focus of the tour on rehabilitating Trump's legacy after the coup.
Doubts expressed about the president having self-awareness to change his image.
Expectation of Trump creating situations to portray his supporters as victims.
Anticipation of Trump being more vocal without Twitter.
Reminder to not give attention to Trump's attempts to provoke outrage.
Emphasis on focusing on facts like the Capitol events rather than Trump's words.

Actions:

for activists, politically engaged individuals,
Focus on facts like the Capitol events (implied)
Do not give attention to Trump's attempts to provoke outrage (implied)
</details>
<details>
<summary>
2021-01-10: Let's talk about Trump changing the story this week.... (<a href="https://youtube.com/watch?v=kdYl7feE0y4">watch</a> || <a href="/videos/2021/01/10/Lets_talk_about_Trump_changing_the_story_this_week">transcript &amp; editable summary</a>)

President Trump aims to distract from failings by sparking conflict with social media companies, but the real story remains: allowing the Capitol to fall.

</summary>

"The president of the United States allowed the Capitol to fall. That's the story."
"Don't allow yourself to be distracted by this. Don't get sucked into this."
"He is very good at manipulating the media and manipulating people's emotions and changing the story."

### AI summary (High error rate! Edit errors on video page)

President Trump plans to lean into a conflict with social media companies to distract from his failings.
The purpose of the Bill of Rights is to protect individuals and states from the federal government.
Private entities like Twitter are not obligated to platform President Trump; it's not a First Amendment issue.
Trump's attempt to force a private entity to carry his message could be seen as a First Amendment violation.
Trump's actions post-Capitol breach shouldn't overshadow the severity of allowing the Capitol to fall.
Trump's tactics aim to manipulate media, emotions, and shift focus from his failures.

Actions:

for american citizens,
Stay informed and focused on the significant events rather than getting distracted (exemplified)
Be vigilant against attempts to manipulate media narratives (exemplified)
</details>
<details>
<summary>
2021-01-09: Let's talk about Trump, healing, unity, and stepping back.... (<a href="https://youtube.com/watch?v=qzTbax_1vmk">watch</a> || <a href="/videos/2021/01/09/Lets_talk_about_Trump_healing_unity_and_stepping_back">transcript &amp; editable summary</a>)

Healing and unity in the U.S. require removing those who enabled authoritarianism, prioritizing country over careers.

</summary>

"We're talking about healing the country."
"The United States was stabbed in the back by the dagger of authoritarianism."
"The United States can come back from this."
"If you only care about your political career, this rhetoric about unity and healing, we see right through it."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Healing and unity in the United States is a hot topic among those in power.
There's a disconnect between those in power and common folk regarding healing and unity.
Senators and ambitious individuals prioritize their political careers over healing the country.
Authoritarianism has stabbed the United States in the back, and those who enabled it must be removed for healing to occur.
Resignation from those who enabled authoritarianism is necessary for the country to heal and reassert unity.
Enabling Trump for political gain led to individuals falling in line behind him.
The path to healing involves removing those who wielded the dagger of authoritarianism.
The United States can recover from this dark period, but it will take time.
Public officials who enabled authoritarianism must step down for progress to begin.
Accountability for actions is vital, and rhetoric about unity must match actions.

Actions:

for politically engaged citizens,
Resign from public office to prioritize country over political aspirations (implied)
Match rhetoric about healing and unity with meaningful actions (implied)
</details>
<details>
<summary>
2021-01-08: Let's talk about context and the events of Capitol Hill.... (<a href="https://youtube.com/watch?v=8lz702NhyKY">watch</a> || <a href="/videos/2021/01/08/Lets_talk_about_context_and_the_events_of_Capitol_Hill">transcript &amp; editable summary</a>)

Beau stresses the vital role of context in understanding events, contrasting the Capitol Hill riots with this summer's protests and reminding us of the Civil Rights Movement.

</summary>

"Context is what determines moral value, societal value."
"Riots are the language of the unheard."
"You have to look at the context."

### AI summary (High error rate! Edit errors on video page)

Talks about the importance of context in understanding events and politics.
Mentions how we often ignore details to reinforce our own beliefs.
Gives examples of how context determines moral and societal value.
Contrasts two different events: the protests of this summer and the Capitol Hill riots.
Emphasizes the significance of looking at the surrounding details to make informed judgments.
Reminds us of the Civil Rights Movement and the importance of context in assessing actions.
Points out that riots can be seen as the "voice of the unheard."
Stresses the need to understand the context behind news and events for a deeper understanding.
Encourages examining details like who, what, when, where, and why to grasp the full story.

Actions:

for students, activists, educators,
Examine the context behind events to understand the full story (implied).
</details>
<details>
<summary>
2021-01-08: Let's talk about Trump, trust, and Twitter.... (<a href="https://youtube.com/watch?v=JqOiMAOyD3U">watch</a> || <a href="/videos/2021/01/08/Lets_talk_about_Trump_trust_and_Twitter">transcript &amp; editable summary</a>)

Beau questions why Congress fails to rein in a President stripped of Twitter, drawing parallels with his friend's son losing camp privileges. Congress shares blame.

</summary>

"It's their job to rein him in."
"They know he has a behavioral problem."
"But he's still commander in chief."
"I don't know that I can trust that."
"I don't know that that's a situation they should let stand."

### AI summary (High error rate! Edit errors on video page)

Beau shares a story about his friend's son who got his Twitter account taken away by his mom because he was picking on another kid over Twitter and was subsequently banned from going to a sports camp.
Drawing a parallel, Beau points out that the President of the United States had his Twitter account taken away, yet still holds immense power as the commander in chief.
Beau questions why those in power have not taken action to either convince the President to resign, invoke the 25th Amendment, or impeach him given the situation.
He criticizes Congress for being hesitant to take action, with Democrats worried about political implications and Republicans concerned about appearances.
Beau argues that regardless of political capital or appearances, it is Congress's duty to rein in the President, especially knowing his behavioral problems.
Mentioning the recent change in Trump's tone after being convinced to read from a teleprompter, Beau expresses doubt on his sincerity and predicts a return to erratic behavior once the teleprompter is off.
Beau suggests that Congress should share the blame for whatever actions Trump takes during his remaining time in office, as it is their responsibility to rein him in as the commander in chief.

Actions:

for congress members,
Convince the President to resign, invoke the 25th Amendment, or impeach him (implied)
Take necessary steps to ensure the President's power is in checked (implied)
</details>
<details>
<summary>
2021-01-08: Let's talk about Trump accepting his loss weeks ago.... (<a href="https://youtube.com/watch?v=LjVxm9r9M-E">watch</a> || <a href="/videos/2021/01/08/Lets_talk_about_Trump_accepting_his_loss_weeks_ago">transcript &amp; editable summary</a>)

President Trump accepted defeat weeks ago but continued to fight to maintain his image as a fighter, all for branding and at the expense of undermining faith in democracy.

</summary>

"All the damage that was done undermining faith in the democratic system in the United States was done for nothing."
"Those who engaged in unrest did so while he knew he had lost."
"This might be the one that actually reaches his supporters."
"This reporting may be the reporting you need to show your family members who still don't see Trump for who he is."
"It makes so much more sense than everything else."

### AI summary (High error rate! Edit errors on video page)

President Trump accepted defeat weeks ago, planning to go to Mar-a-Lago with staff.
Trump's fight to appear as a fighter for loyal supporters is all about branding.
Georgia was significant for Trump to portray himself as a fighter, boosting his brand.
Damage to faith in the democratic system was done to enhance Trump's image, if reporting is correct.
People who supported Trump financially or engaged in unrest did so while he knew he had lost.
Attendees at rallies risked their health while Trump knew he had lost, just to build his brand.
Actions over the last four years demonstrate Trump's unsuitability for office.
Knowing he had lost and lying to his supporters may be the breaking point for Trump's followers.
This reporting could be key in revealing Trump's true nature to his supporters.
Waiting for firm details before sharing with family members to avoid dismissal as fake news.

Actions:

for family members, trump supporters,
Show family members the reporting once details are confirmed (suggested)
Share the information to reveal Trump's true nature (suggested)
</details>
<details>
<summary>
2021-01-08: Let's talk about Senator Hawley and the First Amendment.... (<a href="https://youtube.com/watch?v=f2tdAxBQgW0">watch</a> || <a href="/videos/2021/01/08/Lets_talk_about_Senator_Hawley_and_the_First_Amendment">transcript &amp; editable summary</a>)

Senator Howley's book deal controversy raises questions about the First Amendment understanding and marketing tactics.

</summary>

"This is not just a contract dispute. If it's not a contract dispute, what kind of dispute is it?"
"What could go wrong with engaging in a little bit of hyperbole and getting people fired up?"

### AI summary (High error rate! Edit errors on video page)

Senator Howley is in hot water with his publisher over his book deal.
The senator believes his contract was canceled due to representing his constituents in a debate on voter integrity.
The senator argues that the issue is not just a contract dispute but a direct assault on the First Amendment.
Beau questions whether the situation is truly a First Amendment issue or a business decision by the publisher.
Beau suggests that perhaps the senator doesn't fully understand the First Amendment, referencing a previous incident with a Supreme Court justice confirmation.
Two possible explanations Beau offers are that the senator misunderstands the First Amendment or is using hyperbole for marketing and sales purposes.

Actions:

for politically engaged individuals,
Contact local representatives to express concerns about freedom of speech. (suggested)
Support publishers who uphold diverse perspectives and freedom of expression. (implied)
</details>
<details>
<summary>
2021-01-07: Let's talk about what happened on Capitol Hill.... (<a href="https://youtube.com/watch?v=MwvGDfXtehE">watch</a> || <a href="/videos/2021/01/07/Lets_talk_about_what_happened_on_Capitol_Hill">transcript &amp; editable summary</a>)

Beau stresses the seriousness of the attempted coup on Capitol Hill, urges accountability, advocates for anti-authoritarianism, and calls for the removal of President Trump to prevent authoritarianism becoming the norm.

</summary>

"It was an attempted coup."
"Nobody should suffer under the boot of authoritarianism."
"For those who propose militarizing the police and militant action against the citizens by the government, that has to be outside the norm."

### AI summary (High error rate! Edit errors on video page)

Describes the events on Capitol Hill as an attempted coup to disrupt the transfer of power.
Notes the difference between a revolution and a coup, where a coup focuses on changing the head of state, in this case, from Biden back to Trump.
Mentions that the attempted coup failed due to lack of military support, which is a critical element for a successful coup.
Urges Americans to acknowledge the seriousness of the event and not downplay it as it could lead to a normalization of authoritarian methods.
Advocates for holding those responsible at the top levels accountable for fostering an environment that allowed the coup attempt to happen.
Emphasizes the importance of becoming anti-authoritarian and making it the societal norm to reject authoritarian measures from the government.
Calls for the removal of President Trump through the 25th Amendment, impeachment, or resignation, citing his erratic behavior as a reason.
Stresses the need for citizens to set the tone against authoritarianism and work towards walking back from the edge to prevent such incidents in the future.

Actions:

for americans,
Hold those responsible at the top levels accountable for fostering an environment that allowed the coup attempt (implied).
Advocate and work towards making anti-authoritarianism the societal norm by rejecting authoritarian measures from the government (implied).
Support each other and seek equality and freedom, not oppression (implied).
</details>
<details>
<summary>
2021-01-07: Let's talk about Trump agreeing to an orderly transition.... (<a href="https://youtube.com/watch?v=HupuL7WTYfk">watch</a> || <a href="/videos/2021/01/07/Lets_talk_about_Trump_agreeing_to_an_orderly_transition">transcript &amp; editable summary</a>)

Beau expresses skepticism about President Trump's intentions for an orderly transition of power and calls for his resignation, removal, or impeachment due to his divisive actions and lack of trustworthiness.

</summary>

"President Trump is not in control."
"I'm not sure that it's a good idea to simply say, okay, we trust you now, because he's given us no reason to trust him."

### AI summary (High error rate! Edit errors on video page)

President Trump has agreed to an orderly transition of power on January 20th but Beau expresses skepticism about this statement.
Beau believes that President Trump is feeling pressure and is not intending to pursue an orderly and peaceful transition of power.
He calls for the resignation, removal via the 25th Amendment, or impeachment and removal of President Trump because of his erratic behavior and divisive actions over the past four years.
Beau does not trust President Trump's intentions or believe that he has learned his lesson.
He criticizes the President for attacking the fundamental principles of American democracy and trying to control the narrative despite losing access to social media.
Beau doubts President Trump's control over events, especially after the events of yesterday.
He questions the idea of trusting the President without any reason to do so.

Actions:

for activists, concerned citizens,
Advocate for the resignation, removal via the 25th Amendment, or impeachment and removal of President Trump (suggested)
Stay informed and engaged in political developments (implied)
</details>
<details>
<summary>
2021-01-06: Let's talk about the Georgia results and two clean sweeps.... (<a href="https://youtube.com/watch?v=alQ09QDgQPg">watch</a> || <a href="/videos/2021/01/06/Lets_talk_about_the_Georgia_results_and_two_clean_sweeps">transcript &amp; editable summary</a>)

Decision Desk called Georgia races for Democrats, shifting power dynamics; McConnell emerges influential amid Trump's losses.

</summary>

"Democrats have control of the House, the Senate, and the White House."
"Trump walks away from today toxic."
"McConnell will be the new kingmaker."

### AI summary (High error rate! Edit errors on video page)

Decision Desk called both races in Georgia for Democrats around 3 a.m.
Democrats won both Senate seats with one being a thin margin, likely requiring a recount by state law.
Democrats now have control of the House, Senate, and White House.
Senate Democrats lack the 60 votes needed for most actions but can set the legislative agenda.
Senate Majority Leader McConnell's power to stall bills is now diminished.
Despite focus on easier cabinet appointments for Biden, the real executive agenda lies with functionaries.
McConnell gains control of the Republican Party amid Trump's losses.
Trump faced defeats with a veto override, lost stimulus checks, and defeated candidates in Georgia.
Trump's influence within the Republican Party significantly diminishes.
McConnell emerges as a potential new influential figure within the party.

Actions:

for political observers, democratic and republican party members.,
Analyze the political landscape for potential shifts and alignments (implied).
</details>
<details>
<summary>
2021-01-06: Let's talk about Comey's book and prosecuting Trump... (<a href="https://youtube.com/watch?v=CWBK5FyKGYs">watch</a> || <a href="/videos/2021/01/06/Lets_talk_about_Comey_s_book_and_prosecuting_Trump">transcript &amp; editable summary</a>)

Comey's advice on not investigating Trump raises concerns about justice versus fostering trust, leading Beau to advocate for accountability and a full accounting of Trump's actions.

</summary>

"Politicizing the justice system is wrong and it's incredibly dangerous to a free society."
"Nobody should be prosecuted simply because of their political beliefs and nobody should be shielded from prosecution simply because of their political beliefs or their popularity with a segment of the American population."
"If there is no accountability for any crimes the President may have committed, it sends a message to the next would-be tyrant that there's no risk."
"You don't make the determination based on what's politically expedient."
"To those who are politically savvy, we know what President Trump was. We know what he was trying to do, but large portions of the American population do not understand it."

### AI summary (High error rate! Edit errors on video page)

Comey suggests not pursuing criminal investigation of Trump by the next Attorney General, prioritizing fostering trust over justice.
Beau criticizes politicizing the justice system and warns of the dangers to a free society.
He expresses concern about shielding individuals from prosecution based on their political beliefs or popularity.
Beau advocates for the importance of truth over political expediency in investigations.
He warns of the danger of creating a government where some are untouchable and law enforcement can act with impunity.
Beau believes failure to hold Trump accountable for any crimes sets a dangerous precedent for future leaders.
He stresses the need for accountability, investigation, and prosecution if crimes were committed, regardless of political implications.
Beau criticizes Comey's advice and questions its intentions in creating buzz for his book.
He underscores the importance of a full accounting of Trump's actions to help the American population understand the gravity of his presidency.
Beau calls for adherence to judicial principles and the importance of faith in the judicial system.

Actions:

for concerned citizens,
Advocate for accountability and transparency in investigations (implied)
</details>
<details>
<summary>
2021-01-05: Let's talk about today's election in Georgia.... (<a href="https://youtube.com/watch?v=yy9lfU-9aas">watch</a> || <a href="/videos/2021/01/05/Lets_talk_about_today_s_election_in_Georgia">transcript &amp; editable summary</a>)

Today's Election Day in Georgia with absentee voting leading to potential delays and swings in results, impacting Senate control. Expect legal challenges and patient waiting for resolution.

</summary>

"It may appear, if they count the in-person votes first, that Republicans have a healthy lead."
"There's a lot riding on this election, control of the Senate."
"I said it was going to happen. It will probably happen again."
"I am hoping that that person is wrong, but we'll see what happens."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Today is Election Day in Georgia with a significant amount of absentee voting, so results may not be immediate.
Absentee voting may cause the race to initially appear to favor one party before swinging in the other direction.
Democrats tend to take public health issues more seriously, impacting how votes are counted.
Polling suggests a close race without a clear blowout.
Expect legal challenges, recounts, and delays due to the importance of this election for Senate control.
The delays and vote swings may fuel existing theories about the election.
Beau predicted these scenarios before the 2020 election and believes they will likely happen again.
Don't anticipate immediate results; the resolution may take some time, possibly extending beyond the inauguration.
Despite hoping for a quicker resolution, we may be in for a lengthy waiting period.
Stay informed and patient as the situation unfolds.

Actions:

for georgia voters,
Stay informed on election updates and results (implied).
Be prepared for legal challenges and recounts and understand their impact on the election (implied).
Remain patient and vigilant during the waiting period for election resolution (implied).
</details>
<details>
<summary>
2021-01-04: Let's talk about what Trump's phone call can teach us all.... (<a href="https://youtube.com/watch?v=oPX861UolHk">watch</a> || <a href="/videos/2021/01/04/Lets_talk_about_what_Trump_s_phone_call_can_teach_us_all">transcript &amp; editable summary</a>)

Be conscientious of the slogans and rhetoric you allow in your life; words have power.

</summary>

"Be aware of the rhetoric you use."
"Make sure they're positive."
"Words have power."
"Be careful of the ones you allow in your life."
"Y'all have a good night."

### AI summary (High error rate! Edit errors on video page)

Analyzes moments in Trump's phone call to Georgia, noting his belief in his own propaganda and rhetoric.
Describes how authoritarians like Trump surround themselves with fervent supporters, leading them to believe their rhetoric is true.
Points out humorous and dangerous situations in the phone call where Trump dismisses contradictory information.
Mentions cognitive dissonance experienced by Trump during the call when faced with opposing information.
Critiques Trump's theory of a rigged election by Democrats and questions its feasibility.
Suggests that many Republicans may have voted against Trump while maintaining party loyalty.
Draws parallels between Trump's belief in his propaganda and rhetoric to how police sometimes believe they have the most dangerous job.
Advises being mindful of the slogans and rhetoric one allows in life, as repeated exposure can lead to belief in them.
Emphasizes the importance of positive slogans that encourage positive action.
Warns about negative propaganda energizing but needing to be channeled into productive action.

Actions:

for critical thinkers,
Monitor and be mindful of the slogans and rhetoric you allow in your life (suggested)
Surround yourself with positive rhetoric that spurs you to positive action (suggested)
</details>
<details>
<summary>
2021-01-04: Let's talk about Trump's Georgia call and accountability.... (<a href="https://youtube.com/watch?v=PXN0UX6Bh4I">watch</a> || <a href="/videos/2021/01/04/Lets_talk_about_Trump_s_Georgia_call_and_accountability">transcript &amp; editable summary</a>)

Beau addresses Trump's controversial phone call, stresses accountability, and warns of the lasting impact of his presidency.

</summary>

"Releasing the phone call is certainly the lesser harm if the other option is standing idly by while an attempted coup is going on."
"Seeing him do it isn't really a surprise."
"This can't go without accountability."
"Think about the number of people who were on this call, because there's no fear of accountability."
"Trump out short-term goal."

### AI summary (High error rate! Edit errors on video page)

Addresses Trump's controversial phone call attempting to influence election results.
Notes Georgia's one-party consent rule for recording phone calls.
Points out the danger of ignoring a potential coup over releasing a phone call.
Acknowledges that many were aware of Trump's character and actions beforehand.
Commends the Secretary of State from Georgia for upholding integrity in the face of pressure.
Raises concerns about the future and accountability following the President's actions.
Stresses the importance of holding the President accountable even after leaving office.
Emphasizes the need for accountability to protect the institution of the presidency.
Expresses worry about losing the system's resiliency if Presidents act above the law.
Concludes by warning that the impact of Trump's presidency will persist beyond his term.

Actions:

for voters, activists, citizens,
Hold elected officials accountable for their actions (implied)
Stay informed and engaged in political developments (implied)
</details>
<details>
<summary>
2021-01-04: Let's talk about Senator Cruz, a story, and SecDefs.... (<a href="https://youtube.com/watch?v=33TW4a59HTI">watch</a> || <a href="/videos/2021/01/04/Lets_talk_about_Senator_Cruz_a_story_and_SecDefs">transcript &amp; editable summary</a>)

Senator Cruz's political theater undermines the country's well-being, echoing the importance of not playing into false beliefs.

</summary>

"It's a bad idea to play into false beliefs because eventually they're going to figure it out."
"You are undermining the foundational elements of this country for nothing, for headlines, for retweets."
"It's not treason, but it is certainly not in the best interest of the country."
"These little games, they have gone on long enough."
"I am sorry that people said mean things to you, but I suggest that maybe you stop playing into the delusions of people."

### AI summary (High error rate! Edit errors on video page)

Senator Cruz plans to object to the electoral count, sparking heavy criticism for playing political theater.
Democrats accused Cruz of committing treason for his actions.
Beau recalls a story about a Marine turned deputy handling a shots fired call from a man with a handgun.
The deputy de-escalates the situation with the man who believed in aliens, showing empathy and restraint.
Beau points out that while the deputy's actions were commendable from a policing perspective, it was the wrong move psychologically.
The man is taken to the hospital where he becomes agitated and violent towards the medical staff.
Beau warns against playing into people's delusions, drawing parallels to Senator Cruz's actions.
Ten former secretaries of defense release an open letter expressing concern about the transition, a situation not seen since the 1800s.
Beau criticizes Senator Cruz for engaging in political theater at the expense of the country's well-being.
He urges Cruz to change his rhetoric and stop undermining the foundational elements of the country for personal gain.

Actions:

for politically engaged individuals,
Advocate for responsible and empathetic political leadership, exemplified
Encourage public officials to prioritize the country's interests over personal gain, suggested
</details>
<details>
<summary>
2021-01-03: Let's talk about saving lives and a program we've needed in the US.... (<a href="https://youtube.com/watch?v=oRGkqtak7EM">watch</a> || <a href="/videos/2021/01/03/Lets_talk_about_saving_lives_and_a_program_we_ve_needed_in_the_US">transcript &amp; editable summary</a>)

In the US, a voluntary gun storage program called Hold My Guns offers a stigma-free solution, promoting responsible firearm ownership and potentially saving lives.

</summary>

"That image is not worth dying over."
"This program will save lives if people know about it, if people talk about it."
"They're just doing what's right."

### AI summary (High error rate! Edit errors on video page)

People in the US have expressed interest in a European-style mandatory gun ownership program but realize it infringes on constitutional rights.
Compelling individuals to surrender firearms through legislation is challenging and may deter people from seeking help.
The gun community values strength, and stigma around seeking help exists, preventing legislative remedies from being effective.
Hold My Guns is a voluntary program where federal firearms license holders store firearms for various reasons, not just during struggles.
The program provides plausible deniability to safely store firearms off-site without linking it to mental health issues.
Surrendered firearms can be picked back up, with a simple phone call to ensure the person is not prohibited.
Hold My Guns aims to save lives by increasing use without stigma, promoting responsible firearm ownership within the community.
Beau encourages spreading awareness about Hold My Guns by mentioning it to friends in relevant situations.
The program operates outside of legislation, with individuals taking proactive steps towards a collective goal.
Beau stresses the importance of discussing and promoting such initiatives to make them effective.

Actions:

for gun owners, community members.,
Spread awareness about Hold My Guns by mentioning it to friends in relevant situations (suggested).
Look for federal firearms license holders participating in the program through the website provided (implied).
</details>
<details>
<summary>
2021-01-03: Let's talk about Ted Cruz's objection.... (<a href="https://youtube.com/watch?v=hRpckirW4Mk">watch</a> || <a href="/videos/2021/01/03/Lets_talk_about_Ted_Cruz_s_objection">transcript &amp; editable summary</a>)

Senators and Republicans engaging in performative loyalty tests aim to undermine voices of millions over unfounded allegations, endangering democracy and supporting authoritarian power structures.

</summary>

"If people do not have a voice, what kind of power structure do you have? An authoritarian one."
"They have shown where their loyalties lie. They're attempting to do something that's not right."
"The people on this list and those 140 Republicans, they should be a uniting element in this country."

### AI summary (High error rate! Edit errors on video page)

Enumerates senators Cruz, Howley, Johnson, Lankford, Daines, Kennedy, Blackburn, Braun, and senators-elect Loomis, Marshall, Hagerty, and Tuberville, criticizing their performative loyalty test to undermine American values.
Defines authoritarian power structures as objectively evil, pointing out that individuals within those structures are not necessarily evil themselves.
Compares historical examples like the U.S. in the 1860s and Germany in the 1940s to illustrate the dangers of supporting authoritarian power structures.
Emphasizes the danger posed by those who provide support through performative loyalty tests, like the current vote attempting to silence millions of voices over unfounded allegations.
Stresses the attempt to create an authoritarian power structure by undermining democratic processes and removing voices of the people.
Mentions Mitch McConnell's statement that the vote will be consequential, potentially a referendum on democracy itself.
Calls for the people on the list and the 140 supporting Republicans to be precluded from public office for their actions against American values and democracy.
Warns about the potential repercussions of rewarding such behavior by re-electing these individuals.
Urges for these individuals to be held accountable and remembered for their actions against the American people and democracy.

Actions:

for american citizens,
Hold those senators and Republicans accountable for their actions (implied)
Ensure these individuals are precluded from public office (implied)
Unite across party lines to protect democracy and American voices (implied)
</details>
<details>
<summary>
2021-01-02: Let's talk about Trump's weird Georgia tweets.... (<a href="https://youtube.com/watch?v=JIJbtxuwyBc">watch</a> || <a href="/videos/2021/01/02/Lets_talk_about_Trump_s_weird_Georgia_tweets">transcript &amp; editable summary</a>)

President tweets inaccuracies, Senate overrides veto, Trump undercuts own message, McConnell wins power struggle, Trump's influence wanes, but Trump loyalists persist.

</summary>

"He's not your friend."
"He does not care about you or your problems."
"Trump loyalists will still be around."
"Trumpism will still continue."
"Don't become too sympathetic."

### AI summary (High error rate! Edit errors on video page)

President tweeted inaccurate information undercutting his own message.
Senate, under McConnell, overrode Trump's veto.
Trump's tweet claimed Georgia election will be invalid, illegal, and unconstitutional.
Trump likely lashing out against McConnell for Senate's veto override.
Trump counting on Senate to secure election victory.
If voter turnout is depressed in Georgia, McConnell may lose his job as Senate Majority Leader.
Trump may realize he tied himself to Georgia election and will likely change stance to support Republican candidates.
McConnell seems to be winning the power struggle against Trump.
Trump's influence within the Republican Party appears to be diminishing.
Trump loyalists and Trumpism will continue despite Trump's waning influence.
Beau warns against becoming too sympathetic towards McConnell, reminding that he looks out for himself primarily.

Actions:

for political observers,
Stay informed on political developments and verify information (implied)
Support candidates based on their policies and actions, not blind loyalty to a particular individual (implied)
</details>
<details>
<summary>
2021-01-02: Let's talk about Republican lawsuit against Pence.... (<a href="https://youtube.com/watch?v=o2Twny37Ul4">watch</a> || <a href="/videos/2021/01/02/Lets_talk_about_Republican_lawsuit_against_Pence">transcript &amp; editable summary</a>)

Congressman Gohmert's suit against Pence dismissed, revealing the importance of legal standing and the lingering Trump influence in the Republican Party.

</summary>

"If you can't prove that you have standing, it's highly unlikely that you're going to win the case."
"It's going to be very hard for them to even get standing."
"There is still a faction of the Republican Party that is either loyal to him or desperately in need of his base."

### AI summary (High error rate! Edit errors on video page)

Congressperson Gohmert and some Republicans filed a suit against Vice President Pence in hopes of having a judge declare Pence's sole authority over accepting or rejecting electoral votes.
The case, like many others, was dismissed by the judge.
The judge emphasized that certain events must happen before Pence could potentially reject electoral votes he doesn't like.
Legal standing is vital in cases like this, and without it, winning becomes highly unlikely.
Pence and the House requested the court to reject the suit.
Even though the case was dismissed without prejudice, it could potentially be reframed and brought back before the court.
Beau warns of wild claims as the January 6th date approaches from those who refuse to accept Trump's defeat.
The outcome of the election seems unlikely to be altered by such claims.
The Republican Party still has factions loyal to Trump or needing his base support, even as Trump's presidency comes to an end.
The influence of Trump within the Republican Party is expected to persist, despite McConnell having the upper hand in their power struggle.

Actions:

for political observers, republicans, democrats,
Keep informed on legal proceedings and political actions (suggested)
Stay engaged with updates on election-related news and claims (suggested)
</details>
