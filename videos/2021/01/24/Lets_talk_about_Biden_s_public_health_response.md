---
title: Let's talk about Biden's public health response....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gZU4ny0jP9k) |
| Published | 2021/01/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains President Biden's plans and actions in response to current events.
- Mentions executive orders issued on the first day, primarily focused on mask-related issues.
- Details specific orders including speeding up delivery of testing supplies, using National Guard for a health response, creating a testing board, and increasing data surveillance.
- Talks about federally funded vaccination centers, guidelines for schools, OSHA involvement for worker protection, and equitable response plans.
- Addresses the inevitability of things getting worse before getting better, likening it to an 18-wheeler slowly stopping.
- Raises concerns about potential inaccuracies in state reporting leading to increased numbers, but sees increased data surveillance as a positive.
- Emphasizes the importance of finding out where and when to get vaccinated.
- Concludes with basic COVID-19 safety reminders: handwashing, mask-wearing, and staying informed.

### Quotes

- "It's more like an 18-wheeler. You hit the brakes and it's going to keep rolling for a while."
- "Some states may have been less than accurate in their reporting."
- "Make sure you know where or when to get your vaccine."
- "Wash your hands. Don't touch your face. Stay at home. If you have to go out, wear a mask."
- "It's just a thought."

### Oneliner

Beau explains Biden's response plans to current events, addressing mask mandates, testing supplies, data surveillance, and vaccination centers while stressing the importance of accurate reporting and vaccine awareness.

### Audience

Health-conscious individuals

### On-the-ground actions from transcript

- Find out where and when to get vaccinated (exemplified)

### What's missing in summary

The full transcript provides a detailed breakdown of President Biden's executive orders and plans related to COVID-19 response, along with Beau's insights and explanations.

### Tags

#COVID19 #ResponsePlans #Vaccination #MaskMandates #DataSurveillance


## Transcript
Well, howdy there internet people, it's Beau again. So today we're going to talk about
President Biden's plans and what he has done so far and what he plans to do in regards to a
response to everything that's going on. He issued some executive orders that first day and you can
You can go back and look at that video.
Some of that has to do with this,
but it's basically mask stuff, the majority of it.
We're gonna go through the orders he issued
after the first day and explain what he means
by it's gonna get worse.
Okay, so the orders.
There is one that is designed to increase the speed
of delivery and manufacturing of testing supplies
and PPE and all of that stuff.
There is one that will allow the federal government to reimburse states more if they use the National Guard for a public
health response.  There's one to create a board to increase testing. I'll be honest, I don't think this one's going to
matter.
I think this is more setting up for future events.
Um, hopefully by the time this board is up and running, the vaccines are going
to be widely available everywhere.
Okay.
So I don't, that one doesn't seem to have any immediate effect.
Then there's, uh, increased data surveillance.
So they're going to be collecting more data so they can understand where things
are at and how better to respond to them.
They are setting up federally funded vaccination centers.
There will be guidelines released to schools on how to open them up safely.
There's one that gets OSHA in on the act to help protect workers.
There is one that is designed to provide an equitable response, meaning that the response shouldn't be racist, really
what it boils down to.  And then there's wearing masks on certain forms of transportation, and then there are
some things mixed in here that are really more about setting up for a future event,
for a future response.
So that's what he's done.
Now he said that it was going to get worse before it gets better, and that sounded bad
and I think it rubbed people the wrong way.
shouldn't. When you're talking about this, this is not a sedan where you slam on the brakes and
it just stops. It's more like an 18-wheeler. You hit the brakes and it's going to keep rolling for
a while. I think that's what he's talking about. I would also suggest that, and this is me saying
this, not Biden, not the administration, I would imagine that numbers are going to go up simply
because some are going to be revised in some states.
I do believe that some states may have been less than accurate
in their reporting.
And that's going to cause the numbers
to go up due to past events that are just now being cataloged.
And given the fact that he is going to be doing more data
surveillance, that's kind of like Trump said.
Well, if you test more, you're going to find more.
So we will find more.
And that's actually a good thing because we
can get to it earlier, and we can build a better response.
So if you're not actually just trying to hide the problem,
finding out about the information is good.
OK, so that's his plan.
I think that's what he means.
We are at the point where the vaccines are being distributed.
I saw a poll that said, more than half of Americans
do not know where or when to get their vaccine.
Make sure you know.
It could be coming up pretty quickly, depending.
So definitely find out for your area.
Other than that, y'all know what to do.
And you know what I'm going to say.
Wash your hands.
Don't touch your face.
Stay at home.
If you have to go out, wear a mask.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}