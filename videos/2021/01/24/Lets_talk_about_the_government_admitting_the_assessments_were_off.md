---
title: Let's talk about the government admitting the assessments were off....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Hb1n2YYnt8U) |
| Published | 2021/01/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Articles suggest federal government's response on the 6th was lackluster.
- Government wants to provide a comprehensive threat assessment.
- Beau warns against giving government more tools for surveillance.
- Lack of effective response on the 6th was not due to a lack of tools.
- Lack of information sharing and use of open-source intelligence were the issues.
- Beau stresses the need for better information sharing and policies.
- Open-source intelligence can develop reasonable suspicion without violating civil rights.
- Beau advocates against giving more power to the federal government.
- Calls for better policy, information sharing, and use of existing tools.
- Urges for an anti-authoritarian stance against authoritarian measures.

### Quotes

- "We cannot allow the 6th to become another 11th."
- "They do not need more tools."
- "Better policy, better information sharing, and better use of the tools that exist."
- "No more tools."
- "We're going to be anti-authoritarian."

### Oneliner

Beau warns against giving the government more surveillance tools, stressing the need for better policy and information sharing to avoid authoritarian measures.

### Audience

Activists, Concerned Citizens

### On-the-ground actions from transcript

- Advocate for better policy and information sharing (implied)
- Stand against authoritarian measures (implied)
- Educate others on the importance of civil liberties (implied)

### Whats missing in summary

The full transcript provides detailed insights into the risks of granting more surveillance powers to the government and the importance of prioritizing civil liberties and effective information sharing.

### Tags

#Government #Surveillance #CivilLiberties #AntiAuthoritarian #InformationSharing


## Transcript
Well, howdy there internet people. It's Beau again.
So, last Friday, some articles went out.
And it seems as though the federal government is finding the reasons
that their response on the 6th was, let's just say, lackluster.
It was lacking.
As I'm sure that this is going to just surprise y'all,
and y'all have never heard this before,
they have determined that their threat assessments were off.
I know. You're shocked, right?
That's all fine and good.
They've identified that, and they want to provide, quote,
a comprehensive threat assessment.
That's a good move. They need to do that. Absolutely.
However, in a lot of these articles,
it seems as though they also want more tools.
Allow me to translate that.
They want to be able to conduct more surveillance.
They don't need more tools.
They do not need more tools.
In those videos where I said the threat assessments were off,
I also talked about what they were going to do to catch up.
And when I was talking about the different activities,
and I'm like, OK, they're going to do this,
they'll leave the peaceful people alone at this moment,
I made sure to point out that it had nothing to do
with protecting civil liberties,
had nothing to do with the Constitution,
and said not to assign positive motives
to what they were doing,
because that's not what it's about.
The United States needs to be very careful
about giving more tools to target people domestically.
That's not a good move.
That is not a good move. It always goes wrong.
We've been through this before.
One September day, something horrible happened,
and that moment was used to justify a massive power grab.
And it is slowly being unwound.
We cannot allow the 6th to become another 11th.
They have the tools they need.
You knew what was going to happen.
I knew what was going to happen.
We all knew what was going to happen.
If the failure that occurred was due to a lack of tools,
how did we know?
It wasn't.
Had nothing to do with a lack of tools.
It had to do with a lack of information sharing
and a lack of using OSINT, open-source intelligence.
It requires no warrants.
It doesn't violate anybody's civil rights,
and it is incredibly accurate, but it's not cool.
It's not cool.
They don't need more legislation to give them more power.
They need more accounts on social media,
and they need to monitor them.
They have identified the problem.
Their threat assessments are off.
They know how to fix it.
They want comprehensive threat assessments.
Once those are made, they need to share them.
That's it.
This is not an issue of not having the proper tools.
This is an issue of not using the tools that exist,
not having the policies in place to make sure
that the information gets shared with the relevant agencies
immediately, and probably, I don't know this,
but probably not prioritizing open-source intelligence
because it isn't cool.
It isn't cool.
Nobody in that world says, I want to do OSINT
because it's kind of boring.
You're reading social media posts in newspaper articles
and blogs, but it's all out there.
It's plain view.
It doesn't require a surveillance state.
That's how they need to go about it.
That's what's really effective.
They can use the open-source intelligence
to develop reasonable suspicion, which then can develop
a probable cause, and they can go about it the normal way.
The federal government does not need more power.
It does not need more tools.
It does not need to be able to designate
domestic groups as anything.
That will only go bad.
Anywhere it's ever been done, it goes bad.
It creates more of an opportunity
for abuse of power.
They have the tools they need.
They have more tools than they need.
They need to use the ones they have
and properly share the information.
If anything, the lack of a response
that occurred on the 6th, that demonstrates
that they have more power than they can handle.
Not that they need more, because if you knew it
and I knew it, and we don't have special tools,
and we knew what was wrong, why didn't they?
We cannot allow this to become another excuse
for a massive power grab to conduct more surveillance
of more Americans.
And I know right now people are scared, and that's okay.
You're allowed to be.
But just remember, the tone we have to set
is that we're going to be anti-authoritarian.
That means for everybody.
We can't endorse authoritarian measures
simply because right now they're going to be used
against the guys with the pointy hats,
because once the next Trump comes,
they're going to be used against the Panthers.
They're going to be used against any marginalized group
that is attempting to upset the status quo.
No more tools.
Better policy, better information sharing,
and better use of the tools that exist.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}