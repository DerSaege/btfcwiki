---
title: Let's talk about today's election in Georgia....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yy9lfU-9aas) |
| Published | 2021/01/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Today is Election Day in Georgia with a significant amount of absentee voting, so results may not be immediate.
- Absentee voting may cause the race to initially appear to favor one party before swinging in the other direction.
- Democrats tend to take public health issues more seriously, impacting how votes are counted.
- Polling suggests a close race without a clear blowout.
- Expect legal challenges, recounts, and delays due to the importance of this election for Senate control.
- The delays and vote swings may fuel existing theories about the election.
- Beau predicted these scenarios before the 2020 election and believes they will likely happen again.
- Don't anticipate immediate results; the resolution may take some time, possibly extending beyond the inauguration.
- Despite hoping for a quicker resolution, we may be in for a lengthy waiting period.
- Stay informed and patient as the situation unfolds.

### Quotes

- "It may appear, if they count the in-person votes first, that Republicans have a healthy lead."
- "There's a lot riding on this election, control of the Senate."
- "I said it was going to happen. It will probably happen again."
- "I am hoping that that person is wrong, but we'll see what happens."
- "Y'all have a good day."

### Oneliner

Today's Election Day in Georgia with absentee voting leading to potential delays and swings in results, impacting Senate control. Expect legal challenges and patient waiting for resolution.

### Audience

Georgia Voters

### On-the-ground actions from transcript

- Stay informed on election updates and results (implied).
- Be prepared for legal challenges and recounts and understand their impact on the election (implied).
- Remain patient and vigilant during the waiting period for election resolution (implied).

### Whats missing in summary

Full details and context of the Election Day scenario in Georgia, including the significance of absentee voting and potential delays in result announcements.


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today is Election Day in Georgia.
And just want to start off by repeating some things
I said before the 2020 election
that I really wish more people had heard.
First, we are probably not going to know the results tonight.
That's probably not going to happen,
mainly due to the amount of absentee voting.
Then second is that because of the amount of absentee voting,
it is incredibly likely that it appears
the race is going one way
and then swings and starts moving the other direction.
It may appear, if they count the in-person votes first,
that Republicans have a healthy lead.
And then as the absentee ballots get counted,
it will swing towards Democrats.
If they count the mail-in ballots first,
it will look like Democrats have a healthy lead
and then swing towards Republicans.
This is simply because Democrats tend
to take the public health issue a little more seriously.
The polling that I have seen suggests
that it's going to be a relatively close race.
Since it is not going to be a blowout,
there will be inevitable legal challenges
and recounts and stuff like that.
There's a lot riding on this election,
control of the Senate.
So with that in mind, don't expect a result tonight.
I mean, sure, it could happen, but it's pretty unlikely.
The sad part is that because of the delays
that are likely to occur
and because of the swing in votes
that is likely to happen,
it will probably add fuel
to a lot of the theories that are out there.
However, I would point out that you can go to videos
from before the election in 2020.
I said it was going to happen.
It will probably happen again.
So just keep that in mind,
and we'll wait and see what happens.
I've been told that this may not be resolved
before the inauguration,
so we may be in for a healthy wait.
I am hoping that that person is wrong,
but we'll see what happens.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}