---
title: Let's talk about Biden's surprise appointment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wJIfJrvuyR8) |
| Published | 2021/01/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's appointment of William Burns as the Director of Central Intelligence surprises many due to Burns' lack of intelligence experience.
- Burns, a diplomatic heavyweight, brings vast experience in high-profile areas like Russia and the Near East.
- Biden's choice of Burns for this role raises questions about the future direction of the CIA and American foreign policy.
- Burns' expertise lies in consuming intelligence products, not in intelligence operations.
- Speculations arise around three possible reasons for Burns' appointment, including reforming the CIA and acting as a second Secretary of State.
- The impact of Burns' decisions in this role will significantly influence American foreign policy for the next decade or more.

### Quotes

- "Burns has no intelligence experience whatsoever, none."
- "He understands foreign policy. He understands the world."
- "What Burns does in this chair is going to shape a lot of American foreign policy in the future."
- "This is somebody to watch and to pay attention to."
- "Y'all have a good day."

### Oneliner

Biden's surprising choice of William Burns as DCI sparks speculation on CIA reforms and American foreign policy direction with long-term implications.

### Audience

Foreign policy analysts

### On-the-ground actions from transcript

- Watch and monitor the decisions and actions of William Burns in his role as DCI (implied).

### Whats missing in summary

Insights into the potential consequences of Burns' actions on global diplomacy and intelligence operations.

### Tags

#Biden #WilliamBurns #DCI #CIA #ForeignPolicy


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about one of Biden's appointments.
We haven't covered them all because frankly a lot of them have been very expected or they
don't really tell us much.
They're more political than agenda setting.
He just announced one that is very interesting.
So we're going to talk about that.
William Burns is a diplomatic heavyweight.
He has been around a long time.
He has been involved in a lot of high profile stuff.
He has experience everywhere that the US needs experience right now with the exception of
China.
I don't think he's ever done anything there.
But as far as Russia, the Near East, all of that, he's experienced.
He understands that the United States is shifting its foreign policy to counter near peers,
major powers.
When his name came up initially when Biden was building his all-star foreign policy team,
nobody was surprised.
I think a lot of people saw him as a possible Secretary of State.
Biden has chosen to make him DCI.
The Director of Central Intelligence, as somebody who is never surprised by anything on the
foreign policy scene, didn't see that coming, did not see that coming.
Burns has no intelligence experience whatsoever, none.
However, he has been the consumer of the intelligence product for decades.
The intelligence world is really about information.
All of the James Bond, Masters of the Universe type stuff, that's secondary.
The real job is gathering information, distilling it, and disseminating it with the idea of
telling diplomats in the executive branch what the opposition's intent is, what they
plan on doing.
That's the Holy Grail, telling the future.
Burns has been the consumer of that product for a very long time.
Putting him in this chair kind of makes sense in a couple of different ways.
The thing is we don't know which way it makes sense.
There are three options as to why Biden would do this.
The first is that he plans on using Burns to reform the CIA, reforms that have been
desperately needed for decades because they have become much more involved in the Masters
of the Universe James Bond type stuff than they have in the core mission.
Biden has made no indication that he intends on doing that, though.
It would make sense to use Burns for that because he is a person who can say, this is
what I need to know, this isn't what I need to know, we don't need to worry about this
covert stuff, we're focusing on information, our foreign policy is based on diplomacy.
It would make sense to use him for that.
He would be perfect for that.
But there's been no indication that that's the plan.
Another option is that Biden feels he needs a second Secretary of State and he needs somebody
who knows what they're doing in a position that can get a meeting with just about anybody
who has access to the information and who knows what to do with that information.
And that would make sense.
The Trump administration made a mess of our foreign policy.
This would be a way to get another player out there trying to fix it that has enough
institutional clout to get stuff done, but who's really acting as a diplomat.
That would make sense too.
But again, no indication that that's the case.
The third option, the first two are good, the third option is that the Biden administration
plans on using the current capabilities and habits of the intelligence world a lot in
foreign policy.
That's not so good.
That's not a good thing.
But we have no indication that that's what he plans on doing.
I don't think anybody knows why Burns was chosen for this spot.
And to be clear, even if it's business as usual, Burns isn't a bad choice.
He understands foreign policy.
He understands the world.
He understands the areas.
He would have to kind of ingratiate himself into that community a little bit, but he's
a diplomat.
He'll be able to do that.
It's definitely a position to watch.
What Burns does in this chair is going to shape a lot of American foreign policy in
the future because we're coming out of the Trump administration where the entire world
knows that it was just mishandled.
Burns is setting the tone for our foreign policy in a lot of ways with what he does
in this chair.
This is somebody to watch and to pay attention to because the decisions he makes are literally
going to shape the next 10 or 15 years of American foreign policy.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}