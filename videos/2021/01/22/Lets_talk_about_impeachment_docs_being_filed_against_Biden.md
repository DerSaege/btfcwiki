---
title: Let's talk about impeachment docs being filed against Biden.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PFmtLg8X7C0) |
| Published | 2021/01/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Representative Greene is reportedly bringing forth Articles of Impeachment against President Biden, despite lacking the votes to make it happen.
- The allegations against Biden regarding Ukraine have already been debunked by Senate Republicans and his political opposition.
- Beau suggests running an investigation on these allegations to set a precedent for investigating elected officials solely based on appearance.
- He sarcastically mentions that such investigations could also extend to members of Congress who may have personally benefited from their votes.
- Beau points out that at the time of the alleged incidents in Ukraine, Biden was acting as a tool of American foreign policy.
- He humorously speculates that if Biden can be held personally liable, then every soldier deployed could face indictments.
- Beau questions Representative Greene's understanding of government processes and suggests she familiarize herself with how things work.
- Despite the absurdity of the situation, Beau sarcastically encourages the investigation to clear things up and move forward.

### Quotes

- "There's no votes to make this a thing. It's not daring, it's not bold, it's not leadership. It is a joke."
- "I think this is a wonderful idea and I wish Representative Green the best of luck moving forward with it."
- "So I'm assuming that she's going to reach out to the attorney general and go ahead and ask for indictments against every soldier who deployed ever because, I mean, that would make sense."
- "I strongly suggest that Representative Greene take a little bit of time to learn how the organization that she is somehow a part of works."
- "Anyway, It's just a thought. Y'all have a good day."

### Oneliner

Representative Greene's push for Articles of Impeachment against President Biden lacks substance, leading Beau to sarcastically encourage an investigation into baseless claims.

### Audience

Politically-minded individuals

### On-the-ground actions from transcript

- Reach out to elected officials urging them to focus on substantive issues rather than baseless political maneuvers (suggested)
- Educate others on the importance of understanding government processes and functions (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the absurdity surrounding the allegations against President Biden and the need for a more informed approach to governance.

### Tags

#Impeachment #Politics #Government #BaselessClaims #Investigation


## Transcript
Well, howdy there, internet people, it's Beau again. So today we're going to talk
about the apparently forthcoming Articles of Impeachment Against President Biden.
They will reportedly be brought forth by Representative Greene in the House.
If you are not familiar with Representative Greene, she is most notably known
a believer in certain theories that have recently become widely ridiculed. To cut
to the chase, this goes nowhere. There's no votes to make this a thing. It's not
daring, it's not bold, it's not leadership. It is a joke. It is ridiculous.
The allegations center on the stuff in Ukraine. I would point out that I used
the events in Ukraine to do a primer on journalism. I will put the link below. I
would also point out that Senate Republicans back in September released a
report clearing Biden of any wrongdoing. I actually think it cleared his son as
well. So now that we have pointed out that not just have these theories been
debunked by his allies, people just using them as an example and his political
opposition. We've pointed that out. This is ridiculous. This
goes nowhere. It's a joke. I think we should do it. I totally
think we should run an investigation on this because of
the precedent it sets. The precedent would then be there to
investigate elected officials based solely on appearance. I
don't see any way that that could possibly go bad for
Representative Green. There's no way that that would come back to bite her.
Aside from that, we could also look into members of Congress, you know, in the
House and in the Senate and see if they somehow appeared to have personally
benefited from their votes via, I don't know, campaign contributions. And then
once that happens, we can get their text messages and their call logs and their
emails and run an investigation and I'm willing to bet we would find out some very interesting
things not just about campaign contributions but maybe about their activities in regards to the
sixth. I think this is a wonderful idea and I wish Representative Green the best of luck moving
forward with it. Aside from that I would point out that it sets another precedent because at this
moment in time Biden was acting as a tool of American foreign policy. While I am certain
that Representative Green is unaware of this, but there was a lot of stuff going on in Ukraine at
the time, so much so that the Senate-Ukraine caucus, which contained a bunch of Republicans,
sent a letter to the Ukrainian government asking for the same reforms in the Prosecutor General's office.
These requests for reforms were also sent by the IMF and basically every major Western
power.
Like I said, there was a lot of stuff going on, it was kind of important.
So this move by Representative Greenwood set the precedent that Americans acting as a tool
of foreign policy could then be held personally liable for their actions.
So I'm assuming that she's going to reach out to the attorney general and go ahead and
ask for indictments against every soldier who deployed ever because, I mean, that would
make sense.
That would be the precedent set.
Would also work for everybody in the intelligence community and everybody in the State Department.
I'm guessing that perhaps Representative Greene might want to talk to her party leadership
this because I don't think they're going to be on board with it. I would strongly
suggest that Representative Greene take a little bit of time to learn how the
organization that she is somehow a part of works, how it functions, what it does.
I would think that a person in Congress should understand how government works
at a basic level. All that being said, yeah, let's do it. Let's investigate it,
clear it up once and for all, and then move forward. And we can then look at
investigating members of Congress and the Senate because I have a whole lot of
questions about their activities on the 6th and leading up to the 6th. Anyway,
It's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}