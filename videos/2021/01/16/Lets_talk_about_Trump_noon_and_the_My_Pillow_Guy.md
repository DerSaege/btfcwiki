---
title: Let's talk about Trump, noon, and the My Pillow Guy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PAx-co0j-Js) |
| Published | 2021/01/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The MyPillow guy, a close advisor to Trump, was photographed leaving the White House with alarming notes visible.
- The notes mentioned invoking the Insurrection Act and martial law, causing concern.
- Despite debunking theories, some missed the urgency of the situation and potential risks.
- Even if the Insurrection Act were invoked, Trump's term still ends at noon on January 20th.
- The Constitution dictates that the President and Vice President's terms end at that specific time.
- The Joint Chiefs' commitment to enforcing the constitutional process further solidifies this fact.
- Even in far-fetched scenarios where Biden and Harris are arrested, Trump doesn't retain power.
- Any coup plan involving going against Gina Haspel from Central Intelligence seems far-fetched.
- The worries and theories circulating are deemed baseless, as Trump's power ends on January 20th.
- Speculation around the notes' content lacks clarity, potentially referencing internet information.
- The anxiety and theories surrounding Trump's power extension are considered irrelevant and unfounded.
- Trump losing power after January 20th is inevitable, barring extreme actions like shredding the Constitution.
- Despite uncertainties, there's assurance that the U.S. military won't back any unconstitutional power grabs.

### Quotes

- "Even if they were to pull off something just wild, it ends on January 20th at noon."
- "There is no mechanism left that changes that."
- "All of this anxiety, all of these theories, they're moot."
- "If Trump wants to retain power beyond that point, he has to shred the Constitution completely."
- "Y'all have a good day."

### Oneliner

Beau clarifies the constitutional process, assuring that Trump's power ends on January 20th, despite alarming notes and theories circulating.

### Audience

Citizens concerned about Trump's potential power extension.

### On-the-ground actions from transcript

- Stay informed on constitutional processes and limitations (implied).
- Remain vigilant against baseless theories and misinformation (implied).
- Uphold democratic norms and constitutional values within your community (implied).

### Whats missing in summary

Context on the potential consequences of disinformation and unwarranted fears within political discourse.

### Tags

#Trump #MyPillowGuy #Constitution #InsurrectionAct #Democracy


## Transcript
Well howdy there internet people it's Beau again. So today we're going to talk about Trump, Biden,
and the MyPillow guy.
Okay so if you don't know the MyPillow guy, one of the president's most loyal and trusted advisors
apparently was spotted leaving the White House. He was photographed. In the photograph his notes
were visible. In those notes were some pretty alarming terms and it has uh it scared people.
I think those of us who have been debunking all of these little theories along the way
have done everybody a great disservice because we didn't cut to the chase and we should have
done our part to make sure that we didn't get caught. But I don't know that anybody has.
If you don't know what was on the notes, there was mention of invoking the Insurrection Act,
the term martial law appears to be on it, and there's some other stuff.
Instead of going through and explaining that the criteria for invoking the Insurrection Act
publicly said they're not in Trump's corner, would have to play along with it, we're going to skip
over all that. We're going to pretend for a second that it works the way they think it does. Okay.
So he invokes it. The Joint Chiefs decide to play along with it. What can they do in the time they
have remaining? Secure a few major cities. Realistically if they were very motivated,
they could get that done before high noon on January 20th, at which point President Trump
is no longer in power. Invoking the Insurrection Act does not change that. There is no mechanism
left that changes that fact. As soon as the courts ruled, it was over.
The terms of the President and Vice President shall end at noon on the 20th day of January.
That's the U.S. Constitution, the 20th Amendment. There is no mechanism left that changes that.
The Joint Chiefs in that message that they sent out, I have a video on it, they said that they
were going to enforce the constitutional process. That's the constitutional process. Even if they
were to enforce it, that's the constitutional process. Even in the more wackadoodle theories
that exist, where Biden and Harris get arrested or whatever, Trump isn't in line. He still doesn't
retain power. That's not how this works. That's not how any of this works. His term ends at noon
on January 20th. That's the reality. There is no mechanism left that changes that. It doesn't
matter what harebrained scheme they come up with. Nothing changes that. I would also point out that
on those notes, it appears that in order for their little plan to work, they have to go up against
Haspel from Central Intelligence. They have to remove her. I'm going to suggest that if your
coup plan involves going up against Gina Haspel, you get a new coup plan. I'm not a fan of Director
Haspel. I'm not. I think she's a horrible person. So horrible, in fact, there is no way I would go
against her. I am fairly certain that bloody Gina is more than capable of dealing with the MyPillow
guy. These theories are getting more and more ridiculous. There's no reason to be worried about
this. Even if they were to pull off something just wild, it ends on January 20th at noon.
It doesn't matter what they attempt to do. Nothing changes that.
Now, it should be noted that we have no idea what those notes were actually referencing.
Statements like this have been all over the internet for a while. It could be that the
MyPillow guy was just informing the president of what was out there on the internet, what was on
Twitter. But everybody should rest easy. You shouldn't be anxious about this. The Joint Chiefs
are not going to play along with this. They've publicly said that even if they wanted to.
And the theory is that they have to because Trump is Commander in Chief. Sure, let's pretend that's
true. They follow the orders because they're robots and they will obey whatever the Commander
in Chief says. Trump is no longer Commander in Chief at noon on January 20th. There's no
mechanism left that changes that. It's been that way for a while. And I don't know that any of us
that really follow the machinery involved in this have made that really clear. There's really not.
There's nothing left. There hasn't been for a while. All of this anxiety, all of these theories,
they're moot. Nothing will change that fact. If Trump wants to retain power beyond that point,
he has to shred the Constitution completely, which he probably wouldn't have a problem with.
But he also has to have the full backing of the U.S. military. And he doesn't have it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}