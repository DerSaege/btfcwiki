---
title: Let's talk about what North Carolina can teach the Democratic party....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=BE6rIwYUuqo) |
| Published | 2021/01/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- 2,879 people left the Republican Party in North Carolina by changing their registration after January 6th.
- The number of people leaving the Republican Party is significantly higher than usual, indicating a trend.
- Rural Americans are becoming disillusioned with the GOP's scare tactics and condescending approach.
- Many rural Americans are realizing that the GOP's actions do not uphold traditional American values.
- Republican Party's support of authoritarianism has led to significant defections.
- Democrats have the potential to reach out to rural Americans by framing social issues as kitchen table issues.
- Many issues championed by the Democratic Party are actually significant to rural Americans.
- Democrats often get caught up in divisive culture wars instead of focusing on issues that matter to rural communities.
- Progressives have the chance to make real impacts on rural Americans' lives by addressing their needs.
- The Democratic Party needs to seize the current moment to connect with rural voters before it's too late.

### Quotes

- "If Democrats want to have a very successful 2022, 2024, they might should work on figuring out how to frame social issues as kitchen table issues."
- "Trump was just a symptom, and he's a symptom of the whole system."
- "You can't wait. If you do, they will fall back into old habits."

### Oneliner

2,879 people leaving the Republican Party in North Carolina signal a shift in rural American sentiment, offering Democrats a chance to connect by framing social issues as kitchen table topics.

### Audience

Progressive activists, Democratic strategists

### On-the-ground actions from transcript

- Reach out to rural communities and start meaningful dialogues to understand their needs and concerns (implied)
- Frame social issues as kitchen table issues during political campaigns to resonate with rural voters (implied)
- Encourage progressive ideas and policies that benefit rural Americans to gain their support (implied)

### Whats missing in summary

The full transcript provides additional context on the changing political landscape in North Carolina and the potential for Democrats to capitalize on shifting sentiments in rural communities.

### Tags

#NorthCarolina #RepublicanParty #DemocraticParty #RuralAmericans #PoliticalShift


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about North Carolina and framing, and how some events in North
Carolina may be foreshadowing a real problem for the GOP and a pretty large opportunity
for the Democratic Party if they're smart enough to take it.
On January 6th and January 11th, 2,879 people left the Republican Party in North Carolina.
And I don't mean they left it in their hearts, they decided they weren't going to vote that
way anymore, no, they changed their registration.
So the actual number is much higher than this.
Now this happens after every election to some degree, but that's a big number.
That is a very large number.
I want to say in North Carolina during the same period was like 800 for Democrats in
North Carolina.
I think that rural Americans are done with the strategy that the GOP tends to use to
reach them, which is to scare them about tradition and how tradition might be upended.
And well, generally just treat them like they ain't that bright, you know.
I think rural Americans are starting to see through that because even though we may talk
slow, we might be pretty sharp at times.
And as far as tradition goes, the GOP's undermine that.
The whole idea behind the tradition was to preserve American values.
Most of the Republican Party supported an overt authoritarian for four years and didn't
speak out against him until it was them on the line, until his people were in the Capitol.
That's when it mattered.
I think that's going to have a much larger impact than the Republican Party is currently
projecting.
These defections from the Republican Party occurred from January 6th to January 11th.
This is the period in which the Republican Party was out there talking about how sorry
they were and how they were going to make it better.
In the middle of the apologies, nobody cared and they left, which means they're up for
grabs.
If the Democratic Party was smart, they would understand that rural Americans are not unreachable
and most aren't unreasonable.
Some are, trust me.
But they can be reached.
The Democratic Party doesn't really make any effort to, though.
There are a whole lot of places where Democrats don't even run.
They don't even run.
They make no attempt to talk to rural Americans.
And as surprising as it may be, a whole lot of issues that the Democratic Party champions
are important to rural Americans.
But they get bogged down in the culture wars, those hot-button divisive issues.
Some of them the Democratic Party could leave.
Some of them they can't because they're the right thing to do, regardless of whether or
not they're divisive.
But the opportunity is here.
If Democrats want to have a very successful 2022, 2024, they might should work on figuring
out how to frame social issues as kitchen table issues, because most times they're
the same thing.
Most times the advances that progressives want to make, they will benefit and impact
rural Americans more than anybody.
And it might be time to start pointing that out and start convincing them to stop voting
against their own interests, because that's been a really long-running trend down here.
This is the time to do it.
This is the time to do it.
There are already people leaving, and they're switching their party to unaffiliated.
They are tired of the McConnells, of the Cruges, of the Howleys, of the Trumps.
Trump was just a symptom, and he's a symptom of the whole system.
But the Democratic Party has the opportunity to reach out.
Anybody with progressive ideas has the opportunity to reach out to rural Americans right now.
You can't wait.
If you do, they will fall back into old habits.
We are very much people who rely on habits.
We do things the same way all the time.
So if there isn't a move within the next 18 months, this opportunity will be gone.
Anyway, it's just a thought. Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}