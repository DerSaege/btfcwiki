---
title: Let's talk about how the FBI may be countering Trump supporters....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pqS1s2Ua-Jg) |
| Published | 2021/01/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the need to understand what went wrong at the Capitol to predict future events.
- Mentions the marked difference in treatment between recent Capitol events and those from the summer.
- Talks about how different groups are assessed and how biases influence these assessments.
- Points out the tendency to underestimate white right-wing groups while overestimating black groups.
- Addresses the issue of being ill-prepared due to biased assessments and contributing factors.
- Assures that the Capitol event has already occurred and authorities are playing catch-up.
- Suggests potential steps law enforcement may take, such as disrupting communications and visiting minor participants.
- Emphasizes the strategy of separating peaceful individuals from potential troublemakers to influence the movement's motives.
- Speculates on the unlikeliness of simultaneous events in all 50 states similar to the Capitol incident.
- Advises people to stay indoors during potential chaotic events and not to get involved.
- Concludes by mentioning the resources available to law enforcement and the seriousness with which they are taking the situation.

### Quotes

- "The Capitol event already occurred. They're playing catch-up."
- "Just take the news of the bulletin with the acknowledgment that it's probably being upplayed by the media for the sake of ratings."
- "You don't want to be caught up in one of those five."

### Oneliner

Beau explains the Capitol incident fallout, biased group assessments, and potential law enforcement strategies for future events, urging caution amidst media sensationalism.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Stay indoors during potential chaotic events (implied)
- Avoid getting involved in disruptive situations (implied)
- Stay informed about local developments and potential risks (implied)

### Whats missing in summary

Detailed breakdown of security failures at the Capitol and the potential implications for future security measures. 

### Tags

#Security #Bias #LawEnforcement #FutureEvents #MediaSensationalism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about what is likely to occur over the next week,
what is probably already happening behind the scenes, and kind of what we can expect.
We're going to do this because the bulletin went out saying they expect events in all 50 states.
The media is covering that the way the media typically does,
which is whatever way is best for ratings.
However, that's not always the best way to get information across.
Okay, so to kind of get what is likely to happen,
we have to understand what went wrong at the Capitol.
I think that most people in the United States would admit that there was a marked difference
between the way that people at the Capitol recently were treated
and the way people this summer were treated.
And I would suggest that a whole lot of them would say,
well it's because of institutional memory, it's because of institutional racism, and they're right.
But it goes further than that.
It goes further than that.
That institutional memory goes to a higher level.
Every group in the United States that is like this gets an assessment.
They have an assessment done.
One of the key things that that assessment tries to determine is intent.
We've talked about it before.
And one of the main things that they try to quantify is the amount of initiative they have,
how likely they are to do something.
Historically, black groups get overestimated.
They get labeled as having a much higher level of initiative,
that they are much more likely to do something than white groups.
White right-wing groups made up of even a decent percentage of vets and ex-law enforcement
are historically underestimated a lot.
And this has been a long-going trend.
The reason this occurs is because the people doing the assessments have a bias.
They can identify with the people they are doing assessments on.
There's the idea that, well, that guy was in the infantry.
He wouldn't do anything like that.
He was in my old unit.
No way that would occur.
We don't need to worry about him.
There are people in Oklahoma City who would disagree with that logic.
And that's how long this has been going on, longer, really.
This is a real problem.
Those groups are always underestimated, and it has really bad outcomes, a lot.
The funny, not funny part about it is that if you look at the way we do assessments overseas,
the very people that the domestic assessments tend to overlook are the highest priorities.
I've never done a video on it, I don't think, anyway.
But I've definitely mentioned it in passing, how those who are disaffected but trained
are the highest priority overseas.
In fact, I remember a video in which I was acting like I was going through files, and
I was like, well, this guy did six years in the military and got in a little bit of trouble.
This one's got to go.
And that's how it works overseas, because those are the most likely to have high initiative
to act, because they were trained to act.
In the U.S., domestically, for some reason, that always gets overlooked.
That helps explain why they were so ill-prepared.
At the same time, it does appear that there were some other contributing factors to them
being unprepared at the Capitol.
But even without that, rest assured, even if there were no contributing factors, they
still wouldn't have been ready for it, because the assessments would have led them to believe
that, well, they wouldn't really do anything like that.
The good news is that the Capitol event already occurred.
It happened.
They're playing catch-up.
They know they messed up, and they're trying to get ahead of it now.
They won't have the excuse of saying, we didn't think it was going to happen.
Even if there are some who are sympathetic, they will be overridden.
If they're following the playbook, and we know how well this administration follows
the playbooks.
But what is likely to occur if they do their jobs is the very first step would be to disrupt
their communications.
That's occurred.
I don't know that the federal government had anything to do with that, but that's happened.
The next thing would be to go to the little fish.
Those who maybe went to the Capitol went inside, but didn't really do anything too wild.
They would get a visit, but they wouldn't get picked up.
They would get a visit that would go something along the lines of, hey, just to let you know,
we'll probably be back in like a month.
You're a low priority.
Don't worry about it.
You're only going to do like a year or so.
Maybe a little bit more, depending on how judges are.
But in the meantime, what you could do if you wanted to get like a lesser sentence is
keep us posted on what's going on, or even post what we tell you to.
That would look really good in the eyes of the court.
And in fact, if you did a really good job of it, and we wound up arresting a whole bunch
of really bad people, we know that you're not bad.
But if we wound up arresting a whole bunch of the really bad people, we might be so busy
we completely forget about you.
Judging from the reactions that a lot of them had to minor inconveniences traveling home,
a whole lot of people are going to take them up on this offer.
They're going to have a pretty good idea of who is who.
And they may even be able to assist in the planning of events that makes it easier for
them to disrupt them.
I'm fairly certain that that's actually already occurred.
We'll go into more detail about that after the events.
But so once they have that done and they start getting information, they're going to try
to separate the two.
Separate the type of person that they're looking at.
Is this a peaceful person or is this somebody likely to do something?
If they are peaceful, they'll leave them alone.
I mean, that sounds great from a civil liberties standpoint and everything.
That's not why they're doing it.
Do not assign positive motives to this behavior.
The whole goal is to leave the peaceful people intact so they kind of rise within the movement,
the organization.
They gain more influence and it shifts the motives of the group and makes them peaceful.
That's the reason they're doing it.
It's not that they care that people have a right to assemble or anything like that.
Has nothing to do with that.
Those who they believe are likely to cause trouble, they will pick them up the day before
on route to the event.
We actually saw that happen with this event in DC.
They do this so they can pick them up and they won't be out by the time the event occurs.
They also try to get them while they're traveling because they're likely to have stuff that's
incriminating on them at the time.
All that being said, what the media is putting out about there's going to be events in all
50 states, that may or may not be true.
The bulletin itself may be erring on the side of caution after what happened recently.
Aside from that, I would point out that I don't think anybody believes that there is
going to be simultaneous events like what happened at the Capitol in all 50 states at
the same time.
That is incredibly unlikely.
The level of communication and organization that that would take is immense.
It doesn't look like they have it.
It does not look like that they have the capabilities to do that across 50 states and different
locations and different time zones.
It's unrealistic to expect 50 events of that sort occurring at the same time.
That being said, just like before, just because they're not going to be successful in their
goal doesn't mean they can't cause a bunch of chaos.
If it was me and I lived in one of these cities, I would catch up on YouTube, binge watch a
show on Netflix on these days.
I wouldn't go out.
There's no reason to get caught up in it.
Because while it's unlikely they achieve their goals, they can be very disruptive.
And there's no reason to get involved in that.
And while they can cause chaos, I don't think that people should be expecting anything too
wild.
And I don't think that people should have any anxiety about it.
They're playing catch up.
Really they aren't great at this.
Historically speaking, they're not.
But they probably have a whole lot of resources available to them because of what just occurred.
There's probably a whole lot of political pressure to make sure it doesn't happen again.
And we have seen signs that they are taking it seriously.
The bulletin I think was letting state agencies and local agencies know ahead of time that
they need to be prepared because just as federal agencies do assessments, local and state agencies
do the same thing.
And they are even worse when it comes to their accuracy.
They always underestimate these groups.
So just take the news of the bulletin with the acknowledgement that it's probably being
upplayed by the media for the sake of ratings.
While there may be 50 events, I seriously doubt that more than five are going to get
like really wild.
But you don't want to be caught up in one of those five.
So I wouldn't go plan a tour of downtown on the 16th, 17th, 18th, 16th through the 20th
I guess is the period that they're saying stuff could happen, which is basically from
now until the inauguration.
We will do, a whole lot of people have asked for a breakdown of what went wrong on the
ground at the Capitol as far as security is concerned.
And I will do a breakdown on it.
I was waiting to get a little bit more information.
I kind of, after watching a lot of the footage, I kind of suspected that there might have
been people who weren't doing their best and perhaps had some sympathies with their opposition.
Now that that's coming out, we can talk about it a little bit more.
I'll do that sometime in the next day or so.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}