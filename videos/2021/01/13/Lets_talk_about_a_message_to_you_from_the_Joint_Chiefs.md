---
title: Let's talk about a message to you from the Joint Chiefs....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=E4yVFyUso2U) |
| Published | 2021/01/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The media failed by focusing on sensational quotes rather than the true message.
- The message from the Joint Chiefs was intended for civilian leadership and the average American.
- Professional soldiers are trained to convey calm in high-stress situations.
- Officers with stars on their shoulders use the word "will" as a statement of fact from the future.
- The military will obey lawful orders, support civil authorities, and protect the Constitution against all enemies.
- Any act to disrupt the constitutional process is against their traditions, values, and oath.
- The military will ensure the inauguration of President-elect Biden on January 20th, 2020.
- The message was designed to convey calm and reassure the public about a peaceful transition of power.
- Beau criticizes political and media figures for creating a situation where the Joint Chiefs had to assure an orderly transition of power.
- The U.S. military will ignore civilian leadership (Trump's appointees) and ensure a smooth transition to the new president.

### Quotes

- "The U.S. military is flat out saying they're going to ignore civilian leadership, that's Trump's appointees."
- "It is appalling that this message had to be drafted."
- "The media did a disservice because they went through and they pulled out the most interesting quotes."
- "The word 'will' doesn't just mean will. It is a statement of fact from the future."
- "The American people have trusted the armed forces of the United States to protect them and our Constitution for almost 250 years."

### Oneliner

Beau reveals the Joint Chiefs' message, assuring civilian leadership and the public of a peaceful transition of power amid political turbulence.

### Audience

Americans

### On-the-ground actions from transcript

- Support the values and ideals of the nation (implied)
- Ensure public safety in accordance with the law (implied)
- Uphold the Constitution against all enemies (implied)

### Whats missing in summary

Deeper insights into the impact of political and media figures on military messaging can be gained from watching the full transcript. 

### Tags

#Military #TransitionOfPower #USConstitution #ProfessionalSoldiers #MediaSensationalism


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a very special message for you.
Last night the Joint Chiefs released a statement and the media covered it.
However, I think the media did a disservice because they went through and
they pulled out the most interesting quotes.
The quotes that were best for ratings.
The more sensationalized quotes.
The problem is this message was designed to do the exact opposite.
It had two intended recipients.
The first was the civilian leadership.
We talked about it in the video where we talked about the idea that Trump was going to use
the military to retain power because he had just moved a bunch of people around in positions
at DOD.
Those are civilian leadership.
Those people do not matter.
They are political appointees.
They're not even in the military.
The people that matter are the people with stars on their shoulders.
The Joint Chiefs.
The people who made this statement.
That is one intended recipient.
There is a message in there for them.
The other intended recipient is you.
You.
The average American.
Two things you should know about the way professional soldiers speak.
First is that they are taught to convey calm in high stress situations.
That's why if you ever listen to radio transmissions, everything may be going wild around them,
but those transmissions are pretty monotone.
They're pretty flat.
The reason they do this is because if you start speaking in an emotional manner, you
start thinking in an emotional manner, and your judgment gets clouded, and that's when
bad things happen.
The other thing is that officers who have stars on their shoulder, the word will doesn't
just mean will.
It is a statement of fact from the future.
This is going to occur, and it doesn't matter what you say about it.
Think about any movies you may have seen.
The general doesn't look at the major and say, hey, it would be really like super cool
peachy keen if you went over there and took that airfield.
Major you will secure that airfield.
It's an order.
It is a statement.
This is going to occur, and we're going to do everything we can to make sure that it
occurs.
Do not interfere with that.
If you do, you're doing so at your own peril.
The word will means a lot coming from them.
Okay, so here's the message.
The American people have trusted the armed forces of the United States to protect them
and our Constitution for almost 250 years.
As we have done throughout our history, the U.S. military will obey lawful orders from
civilian leadership, support civil authorities to protect lives and property, ensure public
safety in accordance with the law, and remain fully committed to protecting and defending
the Constitution of the United States against all enemies, foreign or domestic.
That lawful order bit, that's in there because it's a heads up.
If you try, we're going to ignore it.
Just so you know.
They're letting them know ahead of time.
It's not going to fly.
They've already made the decision now.
It goes on.
The violent riot in Washington, D.C. on January 6, 2021 was a direct assault on the U.S. Congress,
the Capitol Building, and our constitutional process.
Remember that phrase?
We mourn the deaths of the two Capitol policemen and others connected to these unprecedented
events.
We witnessed actions inside the Capitol Building that were inconsistent with the rule of law.
The rights of freedom of speech and assembly do not give anyone the right to resort to
violence, sedition, and insurrection.
As service members, we must embody the values and ideals of the nation.
We support and defend the Constitution.
Any act to disrupt the constitutional process is not only against our traditions, values,
and oath, it is against the law.
They are flat out saying they are going to protect the constitutional process.
They are going to secure that.
That is now a military objective.
But the problem is, right, the ambiguity arises because both sides, fine people on both sides
and all that, claim that the Constitution is on their side.
So we don't know what this means.
But we do.
The next part, and this is the part for you.
On January 20th, 2021, in accordance with the Constitution confirmed by the states and
the courts and certified by Congress, President-elect Biden will be inaugurated and will become
our 46th Commander-in-Chief.
And then it goes on with the normal closing, go team go thing that ends every DOD message
ever.
This message was meant to convey calm.
It was meant to let you know that everything was going to be okay.
Everything's going to be fine.
Don't worry about it.
Biden is going to be inaugurated.
Anybody who attempts to interfere with that is doing so at their own peril because they
are going to meet the full force of the U.S. military.
And that may be comforting.
It should be.
But at the same time, I would point out that it is appalling that this message had to be
drafted.
Those political figures, those media figures that fed into this, that created a situation
in which the Joint Chiefs have to send out a coded message saying they're not going to
participate in a coup and that they will secure an orderly transition of power, you should
be ashamed of yourselves.
At the end of the day, you got the message.
The U.S. military is flat out saying they're going to ignore civilian leadership, that's
Trump's appointees, and that they are going to make certain that Biden completes his transition
and becomes the next president.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}