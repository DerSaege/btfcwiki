---
title: Let's talk about Biden's use of the pardon power....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NA1hy7wsHGE) |
| Published | 2021/01/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden's pardon power is absolute, drawn from Article 2, Section 2, Clause 1 of the U.S. Constitution.
- 35 Democratic lawmakers have requested Biden to commute the sentences of 49 people facing the death penalty in the federal system.
- Commuting the sentences doesn't mean releasing them; it means changing their sentences to something else, likely life imprisonment.
- Biden can commute all sentences to life, ensuring they serve life without any political backlash.
- Biden's use of the pardon power won't be a significant political issue given the support it may garner.

### Quotes

- "He can save 49 lives with a pinstripe. No big deal."
- "There is no waste of political capital here."
- "At the end of the day, this is the right thing to do."

### Oneliner

President Biden holds absolute pardon power under the Constitution; he can commute death sentences to life without much political risk, potentially saving 49 lives in a bipartisan move.

### Audience

Lawmakers, Activists

### On-the-ground actions from transcript

- Advocate for the commutation of death sentences to life imprisonment (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of President Biden's pardon power and the potential positive impact of commuting death sentences to life imprisonment, stressing the absence of political downside.

### Tags

#PardonPower #PresidentBiden #CommuteSentences #DeathPenalty #PoliticalImpact


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about President Biden's pardon power.
Most people know that the president of the United States has the ability
to pardon, and it is basically absolute.
They draw that authority from Article 2, Section 2, Clause 1 of the U.S.
Constitution. What it says is the president shall have power to grant reprieves and pardons for
offenses against the United States. So we're going to talk about that other part. Reprieves.
Reprieves. Commutations. 35 Democratic lawmakers have asked President Biden to
commute the sentences of 49 people currently in the federal system, who are
facing the death penalty.
So what does that mean if he commutes their sentences?
Do they get out?
No, it means that he commutes their sentence to something else.
He changes it to something else.
There is no political downside to this.
Biden can commute all of their sentences to life and then send them
before a judge for re-sentencing, which under the federal system, it's all done by guidelines, really.
They'll probably all end up with life. However, if there's an extenuating circumstance,
that might be changed. But as far as Biden is concerned, he's commuting them all to life.
And in the federal system, life means life. It doesn't mean you're getting out in 10 years.
It means life. There's no waste of political capital here. There's no
downside to this politically for him. He can save 49 lives with a pinstripe. No big
deal. And he won't be accused of, you know, being soft on crime or anything like
that. All he's doing is making sure that the next president doesn't engage in a
a rush the way President Trump did.
At the end of the day, this is the right thing to do.
There is no political downside to it.
With as many executive orders as he is signing and as quickly as he is sending them out,
he can stick this in the middle of them.
Will barely make a headline.
That's one of the good things about sending out as many as he's doing right now.
It has a lot of support, and I think it would get support from the other side of the aisle, at least some, to make it
appear to be bipartisan, but realistically, it doesn't matter.
President Biden's power in this regard is absolute, and I don't think the Republican Party is going to make much of a
fuss over his use of the pardon power,  given what President Trump did on his way out the door.
I don't think this is going to be a big deal.
I can't think of any real political downside to it,
and morally and ethically, it seems like it would be the right move.
You know, this isn't absolute forgiveness.
This is just taking something off the table
that most Americans realize is not a functioning system, especially after what
happened under Trump. So I would hope that this is something he considers and
an axon.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}