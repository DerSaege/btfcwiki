---
title: Let's talk about the good and bad of Biden's new SecDef....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YMshlxYKhDM) |
| Published | 2021/01/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the pushback against newly confirmed Secretary of Defense, General Austin, despite his qualifications and historic appointment as the first Black Secretary of Defense.
- Notes potential conflicts of interest due to Austin's previous role on the board of Raytheon, a company that does business with the Department of Defense.
- Raises concerns about the short time span between Austin leaving the military and assuming the position of Secretary of Defense, potentially impacting civilian control of the military.
- Emphasizes the importance of maintaining civilian control over the military and avoiding the trend of appointing recently retired generals as Defense Secretaries.
- Acknowledges that while he personally doesn't have concerns about Austin in this administration, the broader issue of civilian control is significant.

### Quotes

- "He's the first black Secretary of Defense. All of this is good."
- "We can't get into the habit of using retired generals, especially recently retired generals as Secretary of Defense."
- "The tradition of civilian control of the military is really important in this country."
- "He will probably end up being a pretty embattled Secretary of Defense."
- "Civilian control of the military is important."

### Oneliner

Beau explains the concerns surrounding General Austin's appointment as Secretary of Defense, from conflicts of interest to the importance of civilian control over the military.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Monitor for conflicts of interest and influence related to defense contractors like Raytheon (implied).
- Advocate for and support civilian oversight of military appointments (implied).

### Whats missing in summary

Detailed analysis and depth from Beau's perspective and insights.

### Tags

#SecretaryOfDefense #CivilianControl #ConflictsOfInterest #MilitaryOversight #GeneralAustin


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we're going to talk about the new Secretary
of Defense, newly confirmed Secretary of Defense,
General Austin.
There are people wondering why he
is getting pushback from what seems to be odd places, people
who you would think would be very
supportive of this appointment, and people who generally do not
care about the position of Secretary of Defense.
OK, so when you're talking about an appointment,
you have three considerations.
First, are they qualified to do the job?
Second, any potential conflicts of interest.
And third, any general ethical considerations
that need to be taken into account.
There are precisely zero people who
will argue honestly that General Austin is unqualified.
He is completely qualified for this job.
They're dragging out his resume, and they're
like, he was in the 82nd, he was in 10th Mountain,
and all that's cool.
But that's not what matters.
He was S3 in G3.
As G3, his main duty was advising on operations,
capability, strategy, stuff like that,
exactly what he's going to be doing as Secretary of Defense.
He was also commander of CENTCOM.
CENTCOM is Central Command.
It's the Middle East.
It's not really the Middle East, but that's how most people
would look at it.
He was the general over CENTCOM.
He knows what he's doing.
He's incredibly qualified for the position.
There is no doubt about that.
There's no discussion about that.
And we should take a moment to acknowledge
that something has happened that has been a very long time
coming.
He's the first black Secretary of Defense.
All of this is good.
So where's the pushback coming from?
Potential conflicts of interest.
He sat on the board of Raytheon.
Raytheon is a company that does a whole lot of business
with the Department of Defense.
And now he is in a position to influence that if he chose to.
This is the normal revolving door that occurs in DC.
There are going to be people who are very watchful of that.
Then you have the overall ethical considerations.
And this is definitely something that raises my eyebrows.
Austin has not been out of the military very long.
General Austin has not been out long.
And now he is Secretary of Defense.
When we were discussing ways in which Trump might retain power,
one of the things I pointed to as a reason you don't really
need to worry about this is because he
didn't have the military behind him, not the real military.
That his civilian appointees, they're not generals.
They don't matter.
They're going to have to walk into the room
with the Joint Chiefs or call up a regional commander
and get something done.
That's not going to go down with normal civilian appointees.
With a recently separated general,
recently retired general, that's a very different story.
That is a very different story.
It is completely plausible that one of the regional commanders
or somebody at the Joint Chiefs, that Austin was their mentor.
And he might have a lot of influence over them.
That's a concern.
The tradition of civilian control of the military
is really important in this country.
And it very well may have just stopped Trump
from retaining power.
This is a habit we can't get into.
We can't get into the habit of using retired generals,
especially recently retired generals
as Secretary of Defense.
I don't know General Austin.
I've never met him.
I know one person who worked with him close enough
to have an opinion.
And he says he's a great guy.
I don't have anything to worry about, and blah, blah, blah,
blah.
And that very well may be true.
But I don't know that.
And we cannot get into the habit of granting these waivers simply
because we like the person.
Because eventually, we could get tricked.
Now, realistically, the people who
would have to have loyalty to Austin
to make something like that come off, that's not really a thing.
Because he was a staff kind of officer.
But it's a concern.
And that's why you have a whole bunch of vets coming out
of the woodwork going, hey, hang on a second.
That's why.
It's not that they don't like Austin.
They completely believe he's qualified.
You'd have to be, there's no way you could honestly say he's
not.
That's the concern.
And then from the progressive left,
the concern is the conflicts of interest.
So he is going to have all eyes on him
from all sorts of corners.
He will probably end up being a pretty embattled Secretary
of Defense.
Because people are going to second guess
his every decision, which is sad.
Because five years from now, nobody would.
Nobody would.
It's the short length of time that has elapsed from him
leaving the military to becoming Secretary of Defense.
That's a concern.
And that's why you have people who would generally
love this guy being very lukewarm about it.
So that being said, do I think this is a concern?
I think it's a concern.
So that being said, do I think this is a concern
with this particular administration,
with this particular general?
No, I don't.
Not really.
However, we cannot get in the habit of this.
Civilian control of the military is important.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}