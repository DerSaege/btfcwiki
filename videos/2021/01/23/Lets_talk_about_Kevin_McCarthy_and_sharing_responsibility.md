---
title: Let's talk about Kevin McCarthy and sharing responsibility....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=An5qqoqG094) |
| Published | 2021/01/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- House Minority Leader Kevin McCarthy admitted Trump shared fault for January 6 events.
- McCarthy shifted blame to all Americans, but Beau disagrees vehemently.
- Beau points out McCarthy's silence as a key factor in the events.
- He distinguishes between those who pushed harmful theories and those who enabled them.
- Beau criticizes individuals who only speak up when facing political backlash.
- Beau stresses that leadership within the Republican Party could have prevented the situation.
- He refuses to accept a narrative of shared responsibility for the inaction of certain individuals.
- Beau expresses his nonpartisan stance based on principles and policies.
- He condemns the Republican Party for prioritizing the party over principles and the country.
- Beau calls for accountability and acceptance of responsibility before unity can be achieved.

### Quotes

- "The blame, the fault for this lies at the feet of the leadership of the Republican Party."
- "They remained silent as it was ripped apart."
- "We need people to accept responsibility, allow for accountability, and then we can talk about unity."

### Oneliner

House Minority Leader Kevin McCarthy shifts blame, but Beau calls out Republican Party leadership and demands accountability before unity.

### Audience

Political activists, concerned citizens

### On-the-ground actions from transcript

- Hold accountable those in positions of power for their actions (implied)
- Advocate for principles and policies over blind party loyalty (implied)

### Whats missing in summary

In-depth analysis of specific examples where Republican Party leadership failed to act promptly.

### Tags

#KevinMcCarthy #RepublicanParty #Accountability #Unity #Leadership


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about what
House Minority Leader Kevin McCarthy said in an interview. He was asked kind of where he thought
the blame lie for the events of the 6th, and he did, to his credit, admit that Trump had some of
the fault. That's amazing. Then he went on to say, I also think everybody across this country has
some responsibility. No, sir. No, sir. Absolutely not. That is not even remotely accurate.
You know, I've been sitting here thinking about how I might share some responsibility for this.
It wasn't me who remained silent as some wannabe dictator ran roughshod over the country.
that that was McCarthy it wasn't me who remained silent as he dog-whistled.
That was McCarthy.
It wasn't me who remained silent when large groups of Americans fell for wild theories.
That was McCarthy.
It wasn't me who remained silent when some of those who fell for those theories began
to show that they had a propensity for violence.
I was making videos explaining how people could stay out of it.
That's what I share responsibility in.
The silence?
That was McCarthy.
This is not a thing where we all share some responsibility.
It's not the case.
The people responsible for this, they're really one of two groups.
who pushed these ideas, these theories, who created this climate, and those who enabled
them.
That's who's responsible.
That's who shares the blame.
It's amazing to me how after four years, all of a sudden, a lot of these people have found
their voices.
They've been real quiet up till now, but they're starting to see the political backlash.
And see, that's what it's really all about.
They didn't care until it was literally at their steps.
Then they thought, you know, well, maybe we should say something.
But that something is apparently not accepting responsibility for their inaction.
It wasn't me who had a whole bunch of people looking to me for leadership and chose not
to show it as they undermined the US election.
That wasn't me.
You are not going to be able to both sides this issue.
There are not fine people on both sides, sir.
The blame, the fault for this lies at the feet of the leadership of the Republican
Party because they could have stopped it at any time had they shown leadership, had they
come out and said, no, none of this is true.
None of this is true.
Everybody needs to calm down.
They could have done it at any time, and they chose not to.
They were too busy jockeying for position, feathering their own political nests.
That's what it boils down to.
And now, we all share responsibility in it.
Nah, uh-uh.
You know, I have a friend who helps me a lot, tell me that I seem to be really deep into
democratic side of the aisle lately, which if you don't know, I'm not a Democrat. I am fiercely
nonpartisan. I'm nonpartisan because I put principle and policy above any party. I want the
maximum amount of freedom for the maximum amount of people. That's what I want. Principle and
policy above party. So it is very hard for me to find any sympathy for the Republican Party. It is
very hard for me to say anything positive about the Republican Party because they put that party
above principle, above policy, and above this country. They remained silent as it was ripped
apart. And now that it's starting to impact them and their political futures, well now they will
at least pretend to care, but not enough to accept responsibility for their inaction.
And that's what we need.
We need people to accept responsibility, allow for accountability, and then we can talk about
unity.
The Republican leadership is not going to find unity by uniting behind the idea that
we're all to blame for their inaction.
That's not going to happen.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}