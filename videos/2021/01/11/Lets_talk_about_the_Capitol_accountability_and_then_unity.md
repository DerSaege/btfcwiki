---
title: Let's talk about the Capitol, accountability, and then unity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KX_BYWI1B2w) |
| Published | 2021/01/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The party of personal responsibility is not taking any for their part in recent events, making accountability unlikely.
- The strategy seems to be targeting middle management with dozens of arrests, but more charges are expected.
- Pelosi is introducing a resolution for Pence to invoke the 25th Amendment, followed by impeachment, potentially using the 14th Amendment for votes.
- Accountability measures from the free market include the PGA and Stripe cutting ties with Trump, impacting millions, and a social media network facing severe consequences.
- DC is heavily surveilled, leading to the capture of individuals involved in recent events trying to leave the area.
- Unity is necessary, even for those who were part of the ideology but want to step away and rejoin society.
- Welcoming those who step away from harmful ideologies is vital for reducing authoritarianism and preventing unpredictable and tragic events.
- Despite the challenges faced, the country has been through worse, and unity will be key in moving forward and ensuring such events do not recur.

### Quotes

- "We have to welcome them. Now, not just is this important in the sense of dealing with the ideology and reducing their numbers, that's good for society as a whole."
- "Those who step away, they have to be welcomed back into normal society, no matter how irritating it is."
- "We need to work to make sure it never happens again."
- "As we move forward, we need to remember that the country has been through a lot worse than this."
- "This was a near miss, and there's still a lot of risk out there."

### Oneliner

The party of personal responsibility avoids accountability, while unity and welcoming former adherents are seen as vital steps forward.

### Audience

Policy Makers, Activists

### On-the-ground actions from transcript

- Welcome individuals stepping away from harmful ideologies into normal society (implied)
- Work towards ensuring such events never happen again by fostering unity (implied)

### Whats missing in summary

In-depth analysis of the potential impact of invoking the 25th and 14th Amendments and the necessity of unity in moving forward are missing from the summary.

### Tags

#Accountability #Unity #Welcome #Surveillance #SocialMedia #CommunityPolicing #PolicyMaking


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today, we are going to talk about accountability and then
unity.
It should come as no surprise that the party
of personal responsibility is taking
none for their part in recent events.
Everybody is just pretending as if they
had nothing to do with it.
And that makes it hard to believe
that there's going to be any accountability.
So let's take stock of the type of accountability
that has occurred so far and what is planned.
OK, so we talked about how the strategy appeared
to be going after middle management, those
who were active on scene and maybe directed events there
or instigated there.
There have been dozens of arrests.
Now, most of the charges right now are pretty minor.
That's typical in a case like this.
I would imagine, I'm certain, that a few of them
are going to see their charges multiply.
Again, going after middle management, it's not effective.
You have to go after the top.
Those at the top can always find new middle management.
So what's happening there?
Today, as you all are watching this,
Pelosi should be introducing a resolution, basically
something asking Pence to invoke the 25th Amendment.
From there, he has 24 hours, and then they
will proceed with impeachment.
Not how I would do it, but it's how it's being done.
It is worth noting that in her statement,
Pelosi mentioned the 14th Amendment.
If you're not familiar with the 14th Amendment,
do not beat yourself up.
It hasn't really been used, to my knowledge,
in like the last century and a half.
Short version, and we'll go into it more in depth
if it becomes relevant, is that anybody
who engages in rebellion against the United States who
formally took an oath, well, they can't be a representative,
a senator, a president again.
I would imagine this is being included
as a way of getting votes for impeachment.
Impeachment would just be about the president.
However, if the 14th Amendment is used,
I would imagine that a lot of people
who played a minor hand in this, maybe slightly less culpable,
I would imagine they would get caught up in it too.
So it might make them a little bit more enthusiastic
about going the impeachment route.
It seems like leverage to me.
If it was attempted to be used, I'm
not sure how it would play out, to be honest.
There haven't been a whole lot of uses in the past.
So I would actually have to go back and look and see
how it would play out.
OK, so what about accountability from the free market?
What about the market deciding?
It is being reported that the PGA will not be using
one of Trump's golf courses.
If this is true and this sticks, you're
talking about Trump losing millions,
millions off of this decision.
There is an internet payment processor called Stripe.
They have stopped processing payments for the Trump
campaign.
That is going to complicate things.
There's a social media network that I'm not
going to name that is alleged to have been utilized
in the planning of recent events.
They've lost everything.
You can no longer download it through the Google Play Store.
You can't get it through the Apple Store.
My understanding is that Amazon is
going to revoke their hosting or has, probably has,
by the time you'll watch this.
The CEO of the company said that, I guess,
even some of their lawyers walked,
that basically all of their vendors
don't have anything to do with them anymore.
That may be enough to end that social media network.
You also had a whole bunch of people
find out that DC is the most surveilled place on the planet,
I think, definitely in the top five.
There isn't much you can do in DC that isn't tracked.
A whole bunch of people who I'm fairly certain
thought they got away and were not
going to be held accountable found out
when they tried to get on a plane
and law enforcement showed up.
If you don't know, DC is one giant camera.
You are probably on film almost the entire time you're there.
Aside from that, a lot of government buildings
have their own networks that ping your cell phone.
And they have the ability to know who owns that cell phone
and track it back.
There are a whole bunch of people who are low priority
right now that, as the investigation progresses,
it may be weeks or months from now, will get a visit.
I imagine that's going to be a tense period for a lot of people.
So that's where we're at as far as accountability at the moment.
We'll see how it all plays out, and I'll
try to keep you updated.
Now let's talk about unity, because I
know a lot of people are not really
ready for this conversation, too soon type of thing.
The problem is we have to have it now.
If people fell for this ideology,
if they were middle management and they got held accountable,
if they were below middle management,
it doesn't matter if they want to step away and rejoin
normal society.
We have to allow it.
We have to welcome them.
Now, not just is this important in the sense
of dealing with the ideology and reducing their numbers,
that's good for society as a whole.
It's good for the entire world to reduce
the number of people who are openly
embracing authoritarianism.
Here's your self-interest.
We have seen what this ideology is capable of.
Imagine if there are people who feel there's no way out
of that ideology, what they might do,
what they might do to themselves.
What they may do if they decide to lash out and act alone.
Events like that are unpredictable.
You can't stop them, really.
And they normally have pretty tragic consequences.
So as much as we may not be ready for it,
we may want more punitive measures to be taken.
Those who step away, they have to be welcomed back
into normal society, no matter how irritating it is.
That's the only way we can move forward.
But we get accountability first and go from there.
I would be really surprised if Pence invokes the 25th Amendment.
It may be possible that he sees this resolution
as the cover he needs to just do it.
But I see that as unlikely.
I actually hope I'm wrong about that.
There is a slim chance, but we'll see.
So as we move forward, we need to remember
that the country has been through a lot worse than this.
I know in recent memory, this is as bad as it gets.
But it's been through a lot worse, so much so
that we have amendments already.
But we will make it.
We'll get through this.
The Trump administration will end at some point,
and we'll be able to move forward.
This was a near miss, and there's still
a lot of risk out there.
We need to remember how close we came if we managed to skate
through the rest of the month without issue.
And we need to work to make sure it never happens again.
And that's going to take all of us.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}