---
title: Let's talk about Los Angeles and Caitlin Doughty's request for assistance...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gUUisU7VD9Y) |
| Published | 2021/01/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the lack of response in LA, California, to a situation requiring assistance.
- Caitlin Doughty, a popular personality on YouTube and owner of a funeral home, requested help due to the overloaded network in LA.
- The Department of Defense has a plan, known as DSCA, that includes dealing with recovery, identification, registration, and disposal.
- Despite the plan being on the books for a long time, it hasn't been implemented due to politics.
- The process of assistance can be initiated through a request from California or a directive from the Secretary of Defense or the president.
- Budgetary concerns are the only potential obstacle on the checklist for assistance from the Department of Defense.
- Beau calls on President Biden to take quick action as he does not bear responsibility for previous inaction.
- Beau urges viewers to pressure for assistance as the plans, program, and personnel exist for a rapid solution.
- This situation serves as a reminder that the public health crisis is ongoing, and precautions like handwashing and mask-wearing are still necessary.
- Beau encourages viewers to watch Caitlin's video, take appropriate action, and help those in need during this challenging time.

### Quotes

- "This should have been the plan from day one. It was. It was the plan since way before day one. Why didn't it happen? Politics."
- "If you're having to call in the Department of Defense to assist in disposal, your mitigation efforts didn't go so well."
- "Biden is new. He can make this happen very quickly."

### Oneliner

Beau addresses the lack of response in LA to a situation requiring assistance, discussing the Department of Defense's plan and urging action from President Biden to provide help swiftly. 

### Audience

Advocates for swift assistance

### On-the-ground actions from transcript

- Contact California officials or urge the Secretary of Defense or President to initiate assistance (suggested)
- Pressure for quick action from President Biden to utilize existing plans and resources for assistance (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the lack of response in LA, the existing plans within the Department of Defense, and the call for swift action from President Biden to provide necessary assistance.

### Tags

#Assistance #DepartmentOfDefense #PublicHealth #CrisisResponse #PoliticalInaction


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about LA, specifically,
California generally, and the response or lack of a response
to a situation that's occurring there.
And a person named Caitlin Doughty, a popular personality
on YouTube, so popular, in fact, that literally 20 of you
sent me a link to her last video, which
was a request for assistance.
They need help.
If you're not familiar with her, she owns a funeral home there.
And apparently, the network in LA is overloaded.
Due to the current public health issue,
they're having problems.
I'll have the link to her video down below.
She will be able to explain the situation far better than I
can trying to summarize it.
However, there is something that I can contribute to this,
and it's some information.
In that video, she lays out a plan on how to deal with this.
It's a good plan.
It's actually the plan, the real plan,
the plan that has been on the books for a very, very long time.
There's a thing called the DSCA, Defense Support
Civil Authorities.
Now, years and years ago, in Directive 3025.1.4.5.4.4,
it's really the number, recovery, identification,
registration, and disposal was specifically
listed as something that the Department of Defense
could assist civil authorities with in situations like this.
Now, eventually, they put out a new directive
that superseded it, 3025.18.
It's not specifically listed anymore,
but it is still well within the purview of doing this.
In fact, last month, they put out a bulletin
and listed responding to the current public health issue
as an example of defense support for civil authorities.
So the plan that they have is almost identical to what
she laid out.
Now, they wouldn't set up shop in an abandoned WeWork,
but other than that, that's what they would do.
I think the only option is cremation, though.
But again, it's been a very long time
since I talked to anybody about this.
So it's like a whole lot of things dealing
with the current public health issue.
We have plans on the books.
Like she says in her video, this should
have been the plan from day one.
It was.
It was the plan since way before day one.
Why didn't it happen?
Politics.
There are two ways this can start, this process can start.
One is a request for assistance.
Now, that's going to come from California.
The other is the Secretary of Defense or the president
can simply say, hey, this needs to happen.
And then DOD runs through a checklist.
You know, does it violate the law?
Is there a likely use of force?
Stuff like this.
The only thing on the checklist that might even remotely cause
a little bit of a bump in the road is the budgetary concerns.
And I don't think it's going to be a big bump.
This can happen, and it can happen quickly.
So why hasn't it happened?
Because anybody that has the authority
to get the ball rolling on this had the responsibility
to mitigate it.
So it looks bad.
It looks like they failed.
If you're having to call in the Department of Defense
to assist in disposal, your mitigation efforts
didn't go so well.
Biden is new.
He doesn't bear any responsibility
for what occurred before him.
He can make this happen very quickly.
He can get them the assistance they need.
And DOD can handle it quickly.
So if you are going to take her up on her challenge
to make the calls and get the pressure built to get them
the help they need and they deserve,
this is something you may want to mention,
because the plans exist.
The program exists.
The authority exists.
It's not like they don't have the personnel for this.
They do.
They can solve the problem pretty quickly.
This is a very sobering reminder.
This isn't over.
So definitely wash your hands.
Don't touch your face.
Stay at home if you have to go out, wear a mask,
and all of that stuff.
At the same time, watch her video
and take whatever action you deem appropriate.
Call if you want to call.
Get involved, because there are people
who are dealing with some of the worst aspects of this
that need help.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}