---
title: Let's talk about Biden's national security priorities....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WscMraOIjnM) |
| Published | 2021/01/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- President Biden plans to issue an executive order shifting the national security framing on climate issues.
- Climate change impacts like food and water insecurity, retreating ice caps, and rising sea levels are considered national security issues by the Army.
- The use of national security framing is aimed at getting politicians who typically oppose environmental protection measures to support them.
- Beau acknowledges ideological concerns about using the military's clout but believes it will be effective in addressing climate change.
- He suggests that healthcare can also be framed as a national security issue due to its impact on readiness and economic growth.
- Beau advocates for utilizing every tool available, including the Department of Defense's influence, to address urgent issues.
- The urgency to address climate change has always been present, and framing it as a national security issue is a practical approach.
- Beau stresses the need to focus on addressing real issues rather than dwelling in the era of alternative facts.
- Using the Department of Defense's influence to push for environmental and healthcare reforms may not be ideologically pure but is seen as effective.
- The urgency to act on climate change and healthcare is emphasized due to the pressing nature of the issues.

### Quotes

- "Increased food insecurity, increased water insecurity, new areas becoming possible battle spaces because of retreating ice caps, migration patterns, rising sea levels that might affect ports. All of these things are national security issues."
- "We can't let the fact that we feel icky about it get in the way of saving lives."
- "It's probably one of the most effective. And we're running out of time."
- "The urgency has been there."
- "We are moving out of the alternative facts era and moving into actually addressing the issues that are facing this country and facing this planet."

### Oneliner

President Biden reframes climate and healthcare as national security issues to drive action, utilizing the military's influence for effective change, acknowledging urgency in addressing pressing global challenges.

### Audience

Policy Advocates

### On-the-ground actions from transcript

- Advocate for framing climate change and healthcare as national security issues (suggested)
- Support policies that address environmental and healthcare challenges (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of framing climate and healthcare as national security issues, urging action leveraging the Department of Defense's influence for effective change.

### Tags

#ClimateChange #NationalSecurity #Healthcare #Urgency #PolicyChange


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we're going to talk about President Biden's
upcoming executive order.
By the time you all watch this, it may have already been released.
He plans on shifting the way the country discusses certain issues.
He plans on shifting the way we talk about the environment
and how we address concerns related to the environment.
He will declare that climate issues are a matter of national security,
national security priority.
He will be accused, undoubtedly, of politicizing and reframing the issue.
That's not true.
It is the accurate framing of it.
Increased food insecurity, increased water insecurity,
new areas becoming possible battle spaces because of retreating ice caps,
migration patterns, rising sea levels that might affect ports.
All of these things are national security issues.
We know that because the Army said it was.
Down below I'll have a video titled AOC versus DOD.
The video really wasn't made for most of the people
who watch this channel regularly.
It was made for your skeptical friends.
If you watch this channel, you probably understand
that climate change is a thing.
Understand that video is a lot like the Trump's accomplishments video.
You have to watch the whole thing to get the point.
In order to mitigate these national security issues,
we have to slow, mitigate, or stop climate change.
It's going to work.
Framing it in that matter will work.
I understand there are going to be people that have ideological issues
with using the clout that the U.S. military has to get this done.
I get that.
I understand it.
I have issues with it myself.
But you can't deny that it will be effective.
A whole bunch of politicians who would normally stand in the way
of anything related to protecting the environment
will get out of the way if it's kind of cast that standing in the way
as being against the troops, because it's the U.S.
We have to support the troops and have that yellow ribbon on our truck.
There is a time for purity tests, and there is a time to get stuff done.
With what is at stake, it isn't just a national security issue.
It is a global one.
We can't let the fact that we feel icky about it
get in the way of saving lives.
It's really that simple.
To the Biden administration, good, now do health care.
Health care can accurately be framed as a national security issue.
Lack of adequate health care for the American populace reduces readiness,
makes us less capable of responding.
It hinders economic growth.
Our medical infrastructure is crumbling.
The easiest way to make sure that it gets repaired is to increase usage.
The easiest way to increase usage is to make sure that everybody has access.
The same accurate framing can be used for health care.
I understand the issues that people are going to have with this.
I really do.
But the clout of the Department of Defense is immense.
They can make sure that a lot of the things that people want enacted
get enacted, because no politician is going
to want to stand in the way of national defense.
It's a tool.
And in this fight, we have to use every tool at our disposal.
This is a way to get it done.
No, it's not the most ideologically pure way to do it.
But it's probably one of the most effective.
And we're running out of time.
So that's why he's framing it this way.
Something didn't happen that suddenly changed the urgency.
The urgency has been there.
The report that's referenced in the video that will be below,
it's been out a while.
That's an older video.
It's just the reality.
And we are moving out of the alternative facts era
and moving into actually addressing the issues that are facing this country
and facing this planet.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}