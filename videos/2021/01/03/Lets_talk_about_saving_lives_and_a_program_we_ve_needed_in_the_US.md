---
title: Let's talk about saving lives and a program we've needed in the US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oRGkqtak7EM) |
| Published | 2021/01/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- People in the US have expressed interest in a European-style mandatory gun ownership program but realize it infringes on constitutional rights.
- Compelling individuals to surrender firearms through legislation is challenging and may deter people from seeking help.
- The gun community values strength, and stigma around seeking help exists, preventing legislative remedies from being effective.
- Hold My Guns is a voluntary program where federal firearms license holders store firearms for various reasons, not just during struggles.
- The program provides plausible deniability to safely store firearms off-site without linking it to mental health issues.
- Surrendered firearms can be picked back up, with a simple phone call to ensure the person is not prohibited.
- Hold My Guns aims to save lives by increasing use without stigma, promoting responsible firearm ownership within the community.
- Beau encourages spreading awareness about Hold My Guns by mentioning it to friends in relevant situations.
- The program operates outside of legislation, with individuals taking proactive steps towards a collective goal.
- Beau stresses the importance of discussing and promoting such initiatives to make them effective.

### Quotes

- "That image is not worth dying over."
- "This program will save lives if people know about it, if people talk about it."
- "They're just doing what's right."

### Oneliner

In the US, a voluntary gun storage program called Hold My Guns offers a stigma-free solution, promoting responsible firearm ownership and potentially saving lives.

### Audience

Gun owners, community members.

### On-the-ground actions from transcript

- Spread awareness about Hold My Guns by mentioning it to friends in relevant situations (suggested).
- Look for federal firearms license holders participating in the program through the website provided (implied).

### Whats missing in summary

The importance of community-driven initiatives outside legislation to address firearm safety and mental health concerns.

### Tags

#GunOwnership #MentalHealth #CommunitySafety #FirearmStorage #VoluntaryProgram


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about a program that
people have expressed in the comments, like man I wish
that it worked that way in the United States.
They do this in Europe.
In fact, in some places in Europe it's mandatory.
And it's something that
people have wanted to achieve through legislation.
And then when it finally comes time to actually write the law,
they realize they can't do it because, well,
it kind of infringes on people's constitutional rights in the US.
Takes a lot to compel somebody to do this.
And because it hasn't been able to be done through legislation,
people think it can't be done.
But it can.
In fact, the program exists already.
When you are talking about gun ownership in the United States,
one of the first things that's gonna come to your mind,
one of the first topics that'll come up is accessibility.
If somebody is struggling,
if they're having an acute mental health issue.
And there's been a push to find some way to legislate
that people give up their weapons if they're in this situation,
give up their firearms.
The problem with that is that one,
it takes a lot to compel somebody to surrender their firearms.
Another is that it may actually serve as a deterrent to
people seeking help because they don't want to give them up.
In the gun community, image is incredibly important.
It's incredibly important to a lot of people in the gun community.
Appearing weak isn't something that most of them want,
because there is that stigma.
It's not true, but the stigma exists.
And it's still pretty prevalent in the gun community.
So they may not seek help, which means the legislative
remedies, well, they won't work.
But what if it's voluntary?
What if there's a network of federal firearms license holders,
gun stores, that will hold your guns?
And they have a program called Hold My Guns.
What's more, they're gonna have to be able to get
your firearms license.
What's more is that this program does not assume that you are struggling.
There are hundreds of legitimate reasons to want to store your
firearms off site, outside of your home for an extended period or short period.
You could be going on deployment.
Since you're not gonna be home, you don't want them stolen.
Your cousin could be coming into town.
He's bringing his kids with him.
You just wanna be extra safe.
This program never assumes that you are struggling.
This program provides the plausible deniability you need to get
the firearms out of your house, out of your immediate accessible reach.
And preserve the image.
That image is not worth dying over.
The problem is, in the United States, tons of people do every year.
Because they don't wanna seek help.
Because that mark, they feel that it would ride with them.
This creates no mark.
There's no paperwork of that sort involved.
You surrender your firearms, give them to the gun store owner, and
you can pick them back up at a later date.
And when it comes time to pick them up,
because it is a federal firearms license holder, they do have to make the phone
call, make sure you're not a prohibited person.
But other than that, that's it.
There's nothing linking it to a mental health issue.
That fact alone would increase use.
That is going to save lives.
This program will save lives if people know about it, if people talk about it.
You may, if you have a friend who is in a situation,
you might want to just mention how you stored your weapons there when you went
on vacation.
You don't have to put it in a book.
You don't have to push the issue.
Present them with the option.
This is the type of responsible firearms community
this country has been begging for.
It exists.
It's just not getting the press it should.
Again, the name of the program is Hold My Guns.
They have a, from my understanding, there's a website that will refer you to
different license holders that participate in this.
And I will find the link and try to put it down below.
Again, this is people acting individually towards a collective goal
outside of legislation.
They're just doing what's right.
They're doing what they should have been doing a long time ago.
And this is one of those things,
it will work if people know about it.
But for people to know about it, we have to talk about it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}