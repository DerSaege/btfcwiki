---
title: Let's talk about Ted Cruz's objection....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hRpckirW4Mk) |
| Published | 2021/01/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Enumerates senators Cruz, Howley, Johnson, Lankford, Daines, Kennedy, Blackburn, Braun, and senators-elect Loomis, Marshall, Hagerty, and Tuberville, criticizing their performative loyalty test to undermine American values.
- Defines authoritarian power structures as objectively evil, pointing out that individuals within those structures are not necessarily evil themselves.
- Compares historical examples like the U.S. in the 1860s and Germany in the 1940s to illustrate the dangers of supporting authoritarian power structures.
- Emphasizes the danger posed by those who provide support through performative loyalty tests, like the current vote attempting to silence millions of voices over unfounded allegations.
- Stresses the attempt to create an authoritarian power structure by undermining democratic processes and removing voices of the people.
- Mentions Mitch McConnell's statement that the vote will be consequential, potentially a referendum on democracy itself.
- Calls for the people on the list and the 140 supporting Republicans to be precluded from public office for their actions against American values and democracy.
- Warns about the potential repercussions of rewarding such behavior by re-electing these individuals.
- Urges for these individuals to be held accountable and remembered for their actions against the American people and democracy.

### Quotes

- "If people do not have a voice, what kind of power structure do you have? An authoritarian one."
- "They have shown where their loyalties lie. They're attempting to do something that's not right."
- "The people on this list and those 140 Republicans, they should be a uniting element in this country."

### Oneliner

Senators and Republicans engaging in performative loyalty tests aim to undermine voices of millions over unfounded allegations, endangering democracy and supporting authoritarian power structures.

### Audience

American citizens

### On-the-ground actions from transcript

- Hold those senators and Republicans accountable for their actions (implied)
- Ensure these individuals are precluded from public office (implied)
- Unite across party lines to protect democracy and American voices (implied)

### Whats missing in summary

The full transcript provides a comprehensive analysis of the dangers of supporting authoritarian power structures and the importance of holding individuals accountable for undermining democracy.

### Tags

#Senators #Republicans #Authoritarianism #Democracy #Accountability


## Transcript
well howdy there internet people it's Beau again so today we're going to talk about senators Cruz
Howley Johnson Lankford Daines Kennedy Blackburn Braun and senators-elect Loomis Marshall Hagerty
and Tuberville probably mispronounced one of those don't really care to be honest
we're going to talk about them and we're going to talk about 140 Republicans in the house
the little show that they want to put on you know I do not often frame things in terms of good and
evil it's not something I do I don't do it because most people view those terms as subjective things
that can be debated see I'm of the opinion that there are a few things that are objectively evil
and they're not necessarily evil
one of them is authoritarian power structures it's not normally people as individuals
that commit great evils however if you look back through history
it's almost always an authoritarian power structure you remove the economic system the flags whatever
that's what you're left with every single time all over the world right now
there are militaries ready to do battle with their opposition whoever their opposition may be
and if you were to ask a soldier they will tell you their opposition is evil however if you
introduce them to a person from the opposition they won't say that because it's not the people
it's the power structure it's what that power structure is capable of accomplishing
this has been a constant throughout history and sure you can look back in the U.S.
say the 1860s and say yeah those people they were part of a power structure like that but
them as individuals they they were evil and yeah sure I'm not going to argue that
argue that you can look to Germany in the 1940s and say yeah they were part of an authoritarian
power structure but they as people were evil and sure I'm not going to argue that
the thing is the scariest of those people to me was not Himmler it was not Eichmann
it was the good German the one who just wanted to show they were loyal that they were patriotic
those willing to engage in little performative tests to show their support because those are
the people that provided power to that power structure through their support those little
performative loyalty tests which is exactly what this vote is the names on this list that's what
they're engaging in a performative loyalty test showing that they're good Americans they're
patriotic by undermining everything this country is supposed to stand for
this little show they don't have the votes to make it succeed yet not this time around
but I want you to think about what they're really trying to accomplish here
they're trying to remove the voices of millions of people over unfounded allegations
allegations even in the way they're saying they're going to frame their objection
they're not saying there was fraud they're saying there were allegations so we're going
to hold a 10-day audit again not a big deal it's performative it's just a show
but if they'll play along with this they will play along with anything
if somebody can get you to believe the absurd
they can get you to do anything the people on this list the people who are supporting this
they need to be remembered and they need to be precluded from public office
they have shown where their loyalties lie
they're attempting to do something that's not right
they have shown where their loyalties lie
they're attempting to remove the voices of millions of people
if people do not have a voice what kind of power structure do you have an authoritarian one
they are knowingly or unknowingly attempting to create one of the few things that is objectively
evil in the world even Mitch McConnell Mitch McConnell not a man known for being a man of the
people said that this was going to be the most consequential of votes Romney said that he believed
that statement indicated McConnell believed it was a referendum on democracy itself
and I can't believe I'm going to say this but I think McConnell's right
the people on this list and those 140 Republicans they should be a uniting element in this country
because Republicans, Democrats, Independents, left wing, right wing doesn't matter
I think pretty much everybody in this country believes that Americans should have a voice
however quiet in their government this is an attempt to undermine that
just to show support for dear leader it's what it's all about it's a show
it's a show but these shows are how authoritarian regimes gain power yeah it's going to miss its
mark this time but it may not next time and if these people are re-elected it's going to send
the message that this kind of behavior will be rewarded
these people need to lose they need to be remembered and it does not matter
what they do after this vote doesn't matter what they do a year from now
nothing changes it they picked their side and it wasn't the side of the American people
anyway it's just a thought y'all have a good day

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}