---
title: Let's talk about Senator Hawley's lesson for Democrats....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5hu9uhDN4NQ) |
| Published | 2021/01/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the situation in Howley, Missouri, where calls for resignation are mounting due to his role in objecting to electoral votes.
- Criticizing politicians who participate in political stunts solely to energize their base and gain poll numbers.
- Noting that the best way to energize a base is through delivering for them, not through headline-grabbing or loyalty displays.
- Emphasizing that national politics should focus on national policy and representing the specific group of people who elected officials are meant to serve.
- Warning Democrats in power to deliver for those who were marginalized by the previous administration to secure future success.
- Stressing the need for Democrats to address the root problems that led to Trump's presidency rather than reverting to the status quo.

### Quotes

- "The best way to fire up your base? It's not through sensational headline grabbing. It's by delivering for them."
- "National politics has become about national policy."
- "The people who are sent to DC are sent there to represent a specific group of people."
- "If they want to have a successful 2022 or 2024, they have to deliver."
- "Cannot go back to the status quo."

### Oneliner

Addressing political stunts, Beau urges politicians to deliver for their constituents, warning against reverting to the status quo to avoid facing calls for resignation like Howley in Missouri.

### Audience

Politically Engaged Individuals

### On-the-ground actions from transcript

- Hold politicians accountable for delivering on promises and representing their constituents (implied)
- Advocate for policies that address the needs and concerns of marginalized communities (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the consequences of engaging in political stunts rather than focusing on delivering for constituents.


## Transcript
Well, howdy there, Internet people.
It's Bo again.
So today, we're going to talk about Howley in Missouri.
You know, most people fully understand that objecting to
the electoral votes was a political show.
It wasn't going to happen.
It was doomed to fail.
Those who engaged in it, those who participated in it,
did so solely to energize their base,
solely to gain political brownie points.
They did it for poll numbers.
And it helped create an atmosphere
that led to what we saw on Capitol Hill.
Howley wanted to fire up his base. He wanted to make sure that they would be behind him.
Well, as fate would have it, that's not how things turned out. He is facing calls to resign
from all over Missouri. A survey conducted by Data for Progress shows that
51% of likely voters would like him to resign. That includes one out of five
Republicans. That's pretty steep when 20% of your own party would like you
to resign. I would point out these numbers don't reflect those who aren't
going to vote for him again. These numbers reflect those who want him to
step down. One out of five Republicans, 51% of likely voters. I would hope that
this becomes a lesson for those in political office.
You know the best way to fire up your base?
It's not through sensational headline grabbing.
It's not through political stunts.
It's not through showing your loyalty to the president.
It's by delivering for them.
National politics has become about national policy.
The thing is, the people who are sent to DC are sent there to represent a specific group
of people.
That's how the system is supposed to work.
They often don't do that.
They represent interests that, well, maybe they can help get reelected because they can
help trick and con the simple people of Missouri.
Doesn't seem like they're that easily tricked.
And you go out of your way to engage in something that everybody knows is a stunt, that everybody
knows is just about grabbing headlines.
And there's a turn of events that maybe you didn't predict.
It's going to have bad outcomes politically.
This is something that Democrats need to pay attention to right now.
As they take the reins of power, they should probably keep in mind the only thing that
is going to keep them holding those reins is if they deliver for the people who were
slighted, those who were othered by the current administration and his enablers.
If they want to have a successful 2022 or 2024, they have to deliver.
They have to do the things that Americans want them to do, that those who sent them
to DC want them to do.
A whole lot of that is undoing the damage that Trump did, but that's only part of it.
We have to actually fix the problems that led to him getting into office to begin with.
Cannot go back to the status quo.
You cannot go back to business as usual or you are going to end up like Halley here with
half of your constituents wanting you to step down.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}