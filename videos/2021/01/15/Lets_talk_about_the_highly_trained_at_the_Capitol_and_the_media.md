---
title: Let's talk about the highly trained at the Capitol and the media....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zXLe-XikgJs) |
| Published | 2021/01/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau questions the portrayal of the Capitol crew as highly trained based on basic skills like using hand signals and getting into a stack.
- The crew did not properly execute the stack formation, revealing they lacked real training and experience.
- Beau explains the correct way to use the non-firing hand in a stack to maintain control of the weapon.
- By falsely portraying the Capitol crew as highly trained, the media may unintentionally elevate them to folk hero status.
- Beau warns against giving these groups or individuals airtime as it could serve as a platform for their PR campaigns with violence.
- He stresses that allowing inaccurate statements to be broadcast can be detrimental and shift public opinion.
- Beau points out that only a very small percentage of individuals had actual training present at the Capitol incident.
- He expresses concern that perpetuating the false image of highly trained opposition forces could fuel the movement's growth.
- Muscle memory from proper training, such as using the left hand in a specific situation, was absent in the Capitol crew's actions.
- Beau urges for a stop to the portrayal of untrained individuals as heroes, as it could have damaging consequences.

### Quotes

- "Those guys were play acting. They were pretending."
- "Your ratings are not worth ripping the country apart."
- "Even though they were unarmed, if they had ever trained, muscle memory..."
- "Don't make them out to be heroes."
- "That's probably really bad for the country."

### Oneliner

Beau questions the media's portrayal of the Capitol crew as highly trained, warning against elevating untrained individuals to hero status and fueling their movement's growth.

### Audience

Media consumers

### On-the-ground actions from transcript

- Refrain from giving airtime or publicity to groups or individuals with violent intentions (implied)
- Be critical of media portrayals and question narratives that could potentially glorify untrained individuals (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of media portrayal and its potential impact on public perception and movements.

### Tags

#Media #Training #Capitol #Violence #PublicOpinion


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about training, folk heroes, the media, the Capitol, and we're
probably going to listen to some drips and bangs on the roof because it just stopped
raining.
Stick with me on this because it's going to seem odd in the beginning, but there is a
pretty big point here.
I was inside reading article after article after article, and they all told me how highly
trained this crew at the Capitol was.
The evidence they're using to support this conclusion is that 1.
They were using hand signals.
That's a basic skill.
That's not highly trained.
And 2.
There was a group that got into a stack.
Okay, I'll give you that.
That typically is indicative of a high speed team getting into a stack and doing it properly.
If you don't know what a stack is, it's where you take your hand and put it on the shoulder
of the person in front of you.
They do the same thing.
The person behind you does it.
Lets everybody know where everybody is.
It does appear that somebody explained that to this crew.
Maybe even explained the squeezes and everything.
But that crew, they've never done it.
Not for real.
And they've never trained because they used this hand.
And unless you're telling me all of those guys that used their right hand to grab the
person in front of them are left handed, they're doing it wrong.
You use your non-firing hand to grab the person in front of you because you still have to
maintain control of your weapon.
Doesn't seem like a big thing, right?
Seems like a minor oversight by the media, not being able to pick that out.
It is a big thing.
Those guys were play acting.
They were pretending.
What they did was the equivalent of holding hands while crossing the street.
They are not high speed.
They are not highly trained.
They are not operators.
I would suggest that putting out the image that they are might elevate them to folk hero
status.
This idea that America's elite, the highly trained, they're out there in opposition.
Man, that might make people interested in their cause.
Might make them want to learn more about them.
Might make them want to follow them.
Might make their movement grow.
And while we're on this topic, I'm going to suggest that if you have to talk about the
groups or the individuals, you don't need to interview them and put them on air.
You don't need to pull a bunch of quotes from them.
We've talked about it on this channel time and time again.
These types of campaigns are PR campaigns with violence.
They're not hoping to achieve a military victory.
They are hoping to shift public opinion.
And when you put them on air, you give them a commercial in which to do that.
Your ratings are not worth ripping the country apart.
That's how we got in this mess to begin with.
Allowing people to say stuff and putting it out there even though the media knows it's
not accurate.
America's elite is not in opposition.
Casting that image is bad.
As far as we know, there were two really well trained people there.
Doesn't appear that either one of them took a trainer's course.
Two out of that entire crowd.
There are tens of thousands of people in this country that have that level of training.
They weren't there.
They were not there.
Casting the image that they were will make their movement grow.
If the media does not stop this and stop it soon, that folk hero A-team image, that's
going to make people interested.
It's going to cause that movement to grow.
Aside from it being bad for the country, it is factually inaccurate.
Even though they were unarmed, if they had ever trained, muscle memory would have made
them use their left hand to do that.
They were pretending.
They were pretending.
Don't make them out to be heroes.
And that's the image that is coming across, that there's this group of America's elite
that is in opposition.
That's probably really bad for the country.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}