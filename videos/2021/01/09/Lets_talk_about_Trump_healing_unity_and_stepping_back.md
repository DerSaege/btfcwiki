---
title: Let's talk about Trump, healing, unity, and stepping back....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qzTbax_1vmk) |
| Published | 2021/01/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Healing and unity in the United States is a hot topic among those in power.
- There's a disconnect between those in power and common folk regarding healing and unity.
- Senators and ambitious individuals prioritize their political careers over healing the country.
- Authoritarianism has stabbed the United States in the back, and those who enabled it must be removed for healing to occur.
- Resignation from those who enabled authoritarianism is necessary for the country to heal and reassert unity.
- Enabling Trump for political gain led to individuals falling in line behind him.
- The path to healing involves removing those who wielded the dagger of authoritarianism.
- The United States can recover from this dark period, but it will take time.
- Public officials who enabled authoritarianism must step down for progress to begin.
- Accountability for actions is vital, and rhetoric about unity must match actions.

### Quotes

- "We're talking about healing the country."
- "The United States was stabbed in the back by the dagger of authoritarianism."
- "The United States can come back from this."
- "If you only care about your political career, this rhetoric about unity and healing, we see right through it."
- "Y'all have a good day."

### Oneliner

Healing and unity in the U.S. require removing those who enabled authoritarianism, prioritizing country over careers. 

### Audience

Politically engaged citizens

### On-the-ground actions from transcript

- Resign from public office to prioritize country over political aspirations (implied)
- Match rhetoric about healing and unity with meaningful actions (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how healing and unity in the United States depend on holding accountable those who enabled authoritarianism, even if it means stepping down from their positions.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about healing and unity in the United States.
It is a hot topic among those in power right now.
But as is typically the case, I feel like they may be coming from a different place
than us common folk down here on the bottom.
Because they seem to be under the impression that we can't have healing,
we can't re-establish unity if we, you know, seek to engage in impeachment or prosecution.
It's because they have different ideas about what we mean when we say healing.
They're talking about their political careers.
There are a whole lot of senators who think that when we say that,
we're talking about healing their career, or healing the Republican Party,
or healing the two-party power structure.
Those of us on the bottom, we don't care about that. That's not what we mean.
We're talking about healing the country.
Part of the problem is that a whole bunch of ambitious people
put their party or their own personal career over the country.
The United States was stabbed in the back by the dagger of authoritarianism.
Those who enabled authoritarianism to rise,
well, they're kind of part of that.
The United States can't heal until that dagger is removed.
It doesn't have to be through impeachment.
It doesn't have to be through prosecution.
Those people who are saying that they really care about healing and unity,
they could resign and for once put the country above their own political aspirations,
over their own ambition, because that's what caused this.
A whole bunch of people fell in line behind Trump
because they thought it would be politically advantageous,
and it was for a short time.
They thought it might help them.
All they had to do was settle for the crumbs from Trump's table for a little bit,
and then they'd be the one in charge.
That's normally not how it works, just so you know.
Those who want to heal the country, those who want to reassert unity,
who also played a hand in wielding that dagger, they need to resign.
That's the only way we're going to get to where we want to go.
The United States can come back from this.
It's going to take a long time.
Once those who enabled that dagger to be firmly planted in the back of the United States
are no longer in public office, then we can start to move back.
But while they're still there, we can't.
We can't because we do not know when one of them is going to make the attempt
to undermine the very foundations of this country again.
So if you are one of those who is currently being held accountable for your actions
for the first time in your life, I would strongly suggest you really take a look
at what you think is important and match your rhetoric to it.
Because if you only care about your political career, this rhetoric about unity and healing,
we see right through it.
Because if you cared about that, you wouldn't be giving speeches except for your resignation.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}