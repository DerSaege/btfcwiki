---
title: Let's talk about Trump's accomplishments....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1M6CXhUS-x8) |
| Published | 2021/01/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau responds to a request to catalog Trump's accomplishments on his last day to understand his supporters.
- Beau outlines six positive aspects of Trump's presidency appreciated by his supporters, such as promoting nationalism and traditional family roles.
- Trump's supporters admired his focus on national security, clear identification of enemies, and prioritization of the military.
- Despite differing perspectives, Beau acknowledges that Trump's supporters valued his disregard for conventions in the pursuit of national security.
- Beau mentions Trump's negative action of engaging in cronyism by hiring family members for unqualified positions.
- Trump's refusal to let the media control him, protection of American business interests, and emphasis on law and order were key points for his supporters.
- Additionally, Trump's stance on religion and undermining of elections are discussed as positive and negative attributes by his supporters.
- Beau concludes by linking the 14 characteristics of fascism to the positive and negative aspects of Trump's presidency, raising concerns about the concentration of power.

### Quotes

- "His supporters loved the fact that under him nobody had to be ashamed that they loved the American flag or the bald eagle."
- "He engaged in cronyism. He hired his family members who were less than qualified for those positions."
- "That's why there was so much opposition. That's why people were so worried about the amount of power he was gaining."

### Oneliner

Beau outlines positive and negative aspects of Trump's presidency, reflecting on the characteristics of fascism, prompting reflection on the concentration of power.

### Audience

Supporters and skeptics alike.

### On-the-ground actions from transcript

- Listen to differing perspectives on Trump's presidency (suggested).
- Recognize the potential impact of concentrated power (implied).

### Whats missing in summary

A deeper understanding of the nuances and implications of Trump's presidency.

### Tags

#Understanding #Trump #Supporters #Fascism #Power


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about understanding. This is a requested video.
Somebody asked me if I would be willing to just once on Trump's last day
to catalog his accomplishments the way his supporters see them in hopes of promoting understanding.
I'm all about understanding and I actually think this is gonna be a good exercise. If you have
a Trump supporting friend, get him near you. Let him listen to this because I'm going to in good
outline things that his supporters really liked about him. But since it is my channel,
I'm going to do it six good things and then I'm going to say one bad thing that I think
of his supporters will be like, yeah he totally did that.
And then I'm going to name six more good things and then one bad thing.
Watch to the end. Okay.
His supporters loved the fact that under him nobody had to be ashamed that they loved the
American flag or the bald eagle, those symbols of the nation. He brought nationalism back. He made
it okay. They liked that. He enforced or at least promoted traditional family roles.
He made national security paramount, really important thing. And he focused on that.
Because he was doing that, he didn't wobble. When it came time to call a bad guy a bad guy,
he did it. He let us know who our enemies were. Because he knew who our enemies were and knew
that we had opposition out there, he didn't care about conventions or rights that they may have.
Now to the liberals watching this, I know y'all like that's not a good thing. To them it is. To
them it is because they want to win and they don't care how they do it. When it comes to something
like that, when it comes to national security issues, they want to win, period. Because of
those opposition groups, he made the military supreme, the most important thing, bar none.
Six positive things. So here's my negative one. He engaged in cronyism. I mean, I think everybody
would admit that. He hired his family members who were less than qualified for those positions.
I don't think anybody would deny that. That's my negative one. He didn't let the media run him.
He ran the media. He called them out. He didn't let them get away with anything. If he thought
they were lying, he said they were lying. And he told his supporters which outlets they could trust.
He protected our business interests, American business interests, at home and abroad with the
full force of the U.S. government. He established mutually beneficial relationships between
government and the private sector and used the U.S. government to make it happen, to make things run
on time. He did not let the lefty labor groups gain an inch. He didn't waste time, money,
on horrible art or impractical, unmarketable, liberal arts type of intellectual activities.
He didn't push that. He focused on law and order, crime and punishment was a big focus of his,
his supporters loved it. And he brought religion back into the forefront,
God back into the White House and all of that. His supporters loved it.
My negative thing, he undermined the elections.
What extent his supporters may argue, but there is no doubt that he did.
So I just named 12 positive things and two negative things that I believe most honest,
supporters of Trump would admit were true. 14 things in total.
So now you have a clear picture of what they believe. Now for the Trump supporters, here's why.
Liberals and leftists and a lot of centrists were so opposed to this.
These 14 points are the 14 characteristics of fascism.
You were nodding your head throughout the whole thing, I imagine.
There will be a link down below with this list to it. You can listen to it again and read along.
I didn't do them in order, but you'll be able to find them. It's a short list.
That's why there was so much opposition. That's why people were so worried about the amount of power he was gaining.
The 12 points that I just mentioned that were great things for Trump
and the two negative things that they would concede are the 14 characteristics of fascism.
Yeah, you were holding the monkeys, Paul, when you asked me to do this video.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}