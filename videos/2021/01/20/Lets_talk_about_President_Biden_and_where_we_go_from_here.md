---
title: Let's talk about President Biden and where we go from here....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wPCUHbI_VSc) |
| Published | 2021/01/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Biden and Vice President Harris have taken office, ending a national embarrassment after four years of the Trump administration.
- Biden aims to return things to the pre-Trump era, although resistance may hinder progressive changes.
- The responsibility lies with the people to build stronger communities and power structures to prevent another Trump-like scenario.
- The channel will focus on educating, leading, and creating progress while still covering politics and providing historical context.
- There's hope for moving forward and making real progress now that the constant chaos of the news cycle may ease.
- Beau plans a relaxed live stream to celebrate the moment before returning to work the next day.
- While celebrating, it's vital to acknowledge the near miss the country had and focus on preventing authoritarianism.
- Beau refrained from fear-mongering during Trump's term but notes the closeness to a worse outcome.
- Despite dodging a crisis, there's still much work ahead, but it's okay to take a moment to breathe and celebrate.
- Beau signs off, looking forward to the evening and returning to work the next day.

### Quotes

- "We managed to scrape through it and now we can focus on real progress."
- "We still have a lot of work to do but yes, take a moment, breathe, enjoy the champagne."
- "There's hope for moving forward and making real progress now."

### Oneliner

President Biden's inauguration marks a shift towards progress, but the people must build stronger communities and power structures to prevent authoritarianism in the future.

### Audience

American citizens

### On-the-ground actions from transcript

- Celebrate the moment with friends and family (implied)
- Take time to relax and breathe before getting back to work (implied)

### Whats missing in summary

The full transcript provides additional context on the importance of community building and staying vigilant against authoritarianism.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So, President Biden and Vice President Harris have taken office. Our long national embarrassment
has ended. We are on the plane home. We managed to make it through four years of the Trump
administration without everything crumbling around us and devolving into a failed state.
That's nice. I mean, that's a win, considering. So the question is, where do we go from here?
Now, on the national level, what's basically going to happen is Biden is going to try to
return things to the pre-Trump era. Now, even if they have the political will to go further
than that, they are going to encounter a lot of resistance. So even if you believe that Biden
really wants to make a lot of progressive changes, it's going to be hard. And that's where we come
in. That is where we come in. We are going to have to do the educating. We are going to have
to build stronger communities. We are going to have to build the power structures that are going
to be able to do the things that will make sure that a second Trump is less likely. I know nobody
ever wants to talk about that man again, but we're going to have to as time goes on. So that's where
we're at nationally. This channel, we will focus more on how to do that, how to lead ourselves,
how to do that. We're still going to cover politics and we will still provide historical
context to things and cover foreign policy and all the stuff that we normally do on this channel.
But given the fact that hopefully the news cycle will not be a constant deluge of you've got to be
kidding me, we'll have time to talk about going on the offensive and actually creating that deep
message that we want. Actually making real progress, moving the country forward. We still
have a lot of work to do. Brunch is still canceled and all of that stuff. However, I'm not going to
be the one to rain on everybody's parade. This evening we will have a relaxed, fun live stream
with absolutely no purpose whatsoever other than to hang out and just enjoy the moment because
tomorrow we have to get back to work. Although I will say that at some point soon I may like
take three days off in a row or something, something I haven't done in literally years.
That'll be nice. We had a near miss. We had a near miss. We had something that could have
gone really bad. We managed to scrape through it and now we can focus on real progress rather than
just dragging the country back from the abyss of authoritarianism. We can focus on making sure
that we don't have another open authoritarian in the White House, in the Oval Office.
There's a lot that we have to talk about. There are a couple of things that I did not say while
Trump was in office about how close we were because I did not want to contribute to the
massive amount of fear-mongering that was already occurring.
In some ways we were a lot closer than I think people want to believe and we can't forget that
simply because we dodged it this time. We still have a lot of work to do but yes, take a moment,
breathe, enjoy the champagne or whatever it is you have going on. We will be back this evening
to relax and then we will be back tomorrow to work. Anyway, I'm going to go ahead and
wrap up this video. It's just a thought. Y'all have a good day. See you tonight.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}