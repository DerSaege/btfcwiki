---
title: Let's talk about about how the impeachment should go....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kFjC8Wi8GUg) |
| Published | 2021/01/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Senate Democrats aim for a fair and fast trial to not interfere with Biden's first hundred days.
- Blocking Trump from holding office could be part of Biden's mission.
- Individuals charged in Capitol-related incidents may use loyalty to Trump as a defense.
- Testifying in the Senate could force them to admit they acted on Trump's behalf.
- Seeing these individuals testify could help debunk false claims and bring accountability.
- Bringing witnesses in for the trial is necessary to understand the alleged incitement by Trump.
- Many individuals genuinely believed in Trump's baseless claims, even if they were false.
- Witness testimonies could assist in convicting Trump, preventing future incidents, and aiding national healing.
- Understanding the motivations behind individuals' actions is key to moving forward and healing as a nation.

### Quotes

- "We have to have accountability, and the truth is really good at doing that."
- "To heal, we have to actually see the wound."
- "Seeing these individuals testify could help convict Trump and stop him from ever doing this again."

### Oneliner

Senate Democrats aim for a fair and fast trial while understanding the importance of holding Trump accountable for the Capitol incident, shedding light on the truth, and aiding in national healing by bringing witnesses to testify.

### Audience

Senate Democrats, Capitol incident witnesses

### On-the-ground actions from transcript

- Contact Senate representatives to push for a fair trial with witnesses (suggested)
- Attend or support initiatives advocating for accountability and healing post-Capitol incident (implied)

### Whats missing in summary

The full transcript provides a nuanced perspective on the importance of witness testimonies in the second impeachment trial of former President Trump, aiming for accountability, truth, and national healing.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the upcoming second impeachment of former president Donald
Trump.
Senate Democrats are saying that they want a fair but fast trial.
They want to move the process along.
And I understand they don't want to step on Biden's first hundred days.
I get it.
I understand the logic behind it.
But Biden was in fact elected to undo Trump.
Blocking him from holding office in the future could be part of that, should be part of that.
Aside from that, a fast trial in the Senate precludes something that really might should
happen.
There have been a whole lot of people picked up for various charges related to the Capitol.
Those who have spoken publicly about it said they were doing it for their president.
Some have said that, well, he duped them.
Some said they were just caught up in the moment.
They're making these public statements because this is going to be their defense.
This is going to be their defense.
This is going to be how they appeal for leniency.
If they were to appear in the Senate trial, they would have to say these things.
They did it because President Trump wanted them to.
He duped them.
They were caught up in the moment.
They were excited by what?
What brought them to that level?
What made them march down to the Capitol?
The options are these individuals who have been charged will either show loyalty to the
man who didn't pardon them on the way out, the man who may have duped them, or they're
going to do what's best for their own defense, which is echo the statements they have made
publicly that they did it for Trump.
And if they say otherwise, most of them have live streamed these statements, and it would
be interesting to ask them about those statements.
Aside from that, I think it might do the country good, and it might help a whole lot of people
who may still be buying into some wilder claims to see these people testify, to understand
what they're going through because they believed these claims, because they bought into it.
It might also help to completely squash one of the really weird claims that it was people
who were opposed to Trump who did that.
That might get rid of that claim altogether.
We have to have accountability, and the truth is really good at doing that.
Putting people on record would be really good at doing that.
Republicans in the Senate say they want a real trial.
They want it to be fair.
Well, let's bring the witnesses in.
If the claim is that Trump incited people, let's talk to the people he is alleged to
have incited.
It would make sense.
Put them on record.
Put them under oath and see what they say.
Odds are they're going to say, it's not my fault.
I didn't do it.
I was just doing what my president wanted me to because that's what's best for their
defense.
That's probably what they're going to say, and in many cases, it's probably the truth.
We have to understand that a lot of these people genuinely believed why they were there.
They believed in it.
Doesn't mean it was right.
People can be, people can genuinely believe something that's false, but I think it would
be good for everybody to see the people that may have been swept up in Trump's baseless
claims.
It might help a lot of people who are still struggling with that fact to kind of see through
it.
Not just would it help convict Trump and stop him from ever doing this again.
It could help with that healing that everybody keeps talking about, but nobody wants to do
anything about.
To heal, we have to actually see the wound.
We have to know what it was.
This would help with that.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}