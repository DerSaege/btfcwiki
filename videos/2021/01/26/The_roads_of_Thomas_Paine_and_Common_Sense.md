---
title: The roads of Thomas Paine and Common Sense....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jCGX1AbA85c) |
| Published | 2021/01/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Thomas Paine, a key figure in American history, was more progressive than commonly portrayed.
- Paine played a pivotal role in shaping the United States and inciting the revolution.
- His pamphlet, "Common Sense," transformed the focus of the conflict towards independence.
- Paine's arguments in "Common Sense" centered around rejecting English authority, criticizing hereditary rule, and advocating for American independence.
- He believed that England exploited the colonies and involved them in wars unrelated to their interests.
- Paine emphasized the importance of local governance over distant monarchies.
- Despite his impact, Paine faced opposition and criticism for his radical views on democracy and social reform.
- Paine's later works, such as "Rights of Man," promoted universal education, social safety nets, and progressive taxation.
- His activism led to persecution, imprisonment, and eventual exile from multiple countries.
- Thomas Paine's legacy endures as a champion of freedom, equality, and societal progress.

### Quotes
- "Without the pen of Paine, the sword of Washington would have been wielded in vain." - John Adams
- "If to expose the fraud and imposition of monarchy, to break the chains of political superstition and raise degraded man to his proper rank be libelous, let my name be engraved on my tomb." - Thomas Paine
- "Death came. Death, almost his only friend." - Robert G. Ingersoll

### Oneliner
Beau reveals the progressive and revolutionary legacy of Thomas Paine, challenging stereotypes and showcasing his enduring fight for freedom and societal betterment.

### Audience
History enthusiasts, activists

### On-the-ground actions from transcript
- Advocate for universal education and equal access to education for all (suggested)
- Support social safety nets for the economically disadvantaged (suggested)
- Stand against exploitation and advocate for progressive taxation (suggested)

### Whats missing in summary
The full transcript provides a comprehensive look at Thomas Paine's life, beliefs, challenges, and lasting impact on society.

### Tags
#ThomasPaine #AmericanHistory #Revolutionary #Progressive #Freedom


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about somebody you know.
You know the name, you know of his common sense.
Somebody who is described as a journalist by trade and a propagandist by inclination.
He was a founder of the United States, the spiritual father of the revolution.
Because of that you may have some ideas about him.
You may have an image because we have a standard image of the founders of this country.
It might surprise you.
He was a pretty progressive guy.
He was a man that, yeah, he was quoted by Ronald Reagan.
He was also described as the chief propagandist and agitator of the revolution by the Communist
Party.
He not only helped shape the United States, to be honest without him the United States
may not exist.
The colonists really didn't know they were fighting a revolution until he told them they
were.
Not just did he shape here, he assisted in the French Revolution.
He really pushed back against the idea of monarchy everywhere.
He's a person who described government, even in its best state, as a necessary evil and
in its worst state an intolerable one.
He was almost executed once.
He led a life of tragedy and at the end his funeral was pretty sparsely attended.
So today we're going to talk about the roads of Thomas Paine.
Thomas Paine was born in England in the 1730s.
Due to a change in dating convention, it could be listed as January 29, 1736 or as February
9, 1737.
His father was a tenant farmer and corset maker.
Paine apprenticed with his father and then served as a privateer, a state-sanctioned
pirate.
He sailed for the British, mainly targeting French warships.
Paine, soldier of fortune, had a financially successful venture and took some time to study
the theories of the Enlightenment after the voyage.
In 1759, he returned to Great Britain and became a corset maker.
In September 1759, he married Mary.
Shortly thereafter, his business fell apart and Mary got pregnant.
She went into early labor.
Paine lost his wife and child.
This shaped some of his views about social safety nets.
By 1762, he was a tax collector.
He was fired in 1765 for claiming to inspect goods he hadn't.
He fought the accusation and worked as a school teacher in London, but eventually wound up
working as an excise officer in Lewes in Sussex.
Sussex had a revolutionary spirit left over from the previous century and a strong undercurrent
of belief in a republic.
He became lightly involved in civic matters and married his landlord's daughter in 1771.
Paine argued for better pay and working conditions for excise officers.
He wrote a 12-page pamphlet called The Case of the Officers of Excise.
It was his first known political rioting.
By 1774, he was again fired from the service for being absent without permission.
He was about to end up in debtors' prison, but sold off almost everything he owned.
His wife and he separated and he moved back to London.
He met Benjamin Franklin, who suggested moving to America.
Franklin even wrote him a letter of recommendation.
Paine left for the Americas in October of 1774.
Without the pen of Paine, the sword of Washington would have been wielded in vain.
John Adams.
He hit the ground running.
Kind of.
When the ship made it to Philadelphia, Paine was sick.
Typhoid had killed five during the trip.
Benjamin Franklin's doctor, who was at the docks to welcome Paine to the New World, had
the ill man carried off the ship.
It took a month and a half to recover from the illness.
He wrote a couple of articles for Pennsylvania Magazine in early 1775 and by March he was
editor.
The owner wanted to keep the magazine apolitical, but Paine had other plans.
He saw a platform that could be used as a nursery of genius for a new nation.
Under Paine, the magazine's circulation grew rapidly.
It became the most widely read magazine in the colonies.
The first month Paine was editor, the magazine published an abolitionist essay called African
Slavery in America.
It was published anonymously, but it is widely believed to be Paine's own writing.
All right, so Paine's been in the colonies like four months.
And in four months he's already identified the fact that a new nation is on the way and
that slavery's going to have to be abolished.
It's an idea that didn't really earn him a lot of friends with a lot of powerful people.
For his part, Paine never owned a slave and he actively spoke out against it pretty much
any chance he had.
When talking about the vengeance that self-liberated slaves sought in Haiti, he said that it was
the natural consequence of slavery and that it should be expected everywhere.
That was a statement that terrified a lot of slave owners in the colonies.
At this point in time, the revolutionaries didn't know they were revolutionaries because
Paine hadn't told them that's what they were yet.
Patrick Henry was just getting ready to say, I know not what course others may take, but
as for me, give me liberty or give me death.
In April, the battles of Lexington and Concord happened.
June was Bunker Hill.
The fighting was real and it was fierce.
But at this point, it was viewed as a civil war over how the British should treat the
colonists.
The anger among colonists was focused primarily at the officers of the crown, those they saw
on a daily basis.
Then in January of 1776, Common Sense was published.
It was signed by an anonymous Englishman.
Paine didn't take credit initially.
He wanted to call the pamphlet Plain Truth, but Benjamin Rush convinced him to rename
it and had cautioned him to keep the language mild.
Of more worth is one honest man to society and in the sight of God than all the crowned
ruffians that ever lived.
Crowned ruffian wasn't exactly mild language when all of the large powers were monarchies.
Common Sense was read aloud in taverns and churches.
The arguments contained in the short text shifted the entire focus of the conflict.
People were no longer concerned with obtaining better treatment.
They wanted a new nation, a nation of their own.
Paine shifted the blame from the visible forces of the crown to the king himself.
He took on the idea of monarchy.
He also chose to meet the reader where they were.
The ideas in Common Sense weren't new, but Paine chose to express those ideas in a manner
that allowed everybody to understand them.
Not everybody was a scholar, and the less hierarchical government he envisioned required
the consent of everybody.
He sought to win hearts and minds by using the common language to express his common
sense.
The popularity of the pamphlet was unparalleled.
It was read everywhere.
Okay, so we've heard it our entire lives.
Common Sense is this great document, this great text.
It stirred the hearts of people.
It triggered the American Revolution because he laid out these great arguments and he spoke
to people in a language that they understood.
And this is why Thomas Paine is the spiritual and philosophical founder of the United States.
We hear that all the time.
Without Thomas Paine, without Common Sense, there would be no United States.
And we hear people talk about how great his arguments were.
But we rarely hear what his arguments were.
We don't hear them summarized very often.
And when people talk about his delivery, they tend to leave one little detail out because
it goes against American mythology.
Okay, so what were the arguments?
The first one's real simple.
England didn't have authority to rule.
Hereditary rule was bad and we just weren't having anything to do with it.
Another was that because of immigration, Americans were their own people.
They were distinct.
They weren't English.
They were American.
They had their own customs.
From the very beginning, immigration was seen as one of the things that made America, America.
He also said that England wasn't a good steward.
They weren't looking out for us the way a good parent should.
They weren't helping us develop.
They were looking to exploit us.
He also pointed out that the king kept dragging us into wars that, well, had nothing to do
with us.
It was stuff going on on the other side of the Atlantic and yet we wound up fighting
in it.
Nobody likes that.
And then on the topic of the other side of the Atlantic, he points out that the king
couldn't govern effectively because of the Atlantic, because of the time delay.
It was just ineffective rule and therefore no rule at all.
And then he pointed out that the king wasn't looking out for our best interests, that London
should look out for the best interest of London and the colonies should look out for the best
interest of the colonies.
That local rule was best.
That those who were immediately available could be more responsive to the needs of the
people.
It all makes sense.
Good arguments.
That's what stirred people.
That's what stirred people's hearts.
That's what got them desiring revolution instead of reform.
Now when people talk about his delivery, they often point out that he spoke in the language
of the common man.
And he did.
This was a man who was very well educated.
And he could have written this in an academic format, but he chose not to.
But aside from that, Paine wasn't religious, but he sure used a lot of quotes from the
Bible, alluded to it a whole lot, because he knew that that was something that would
stir the hearts of people and get them motivated.
That often gets left out because there's the image that the United States was founded by
Christians.
It just wouldn't do for the spiritual and philosophical father of the revolution to
not be a religious person.
It wasn't without its detractors.
James Chalmers said that without a monarch, the country would degenerate into democracy.
Many conservatives of the time viewed democracy the way socialism is viewed by conservatives
today.
Something relatively unknown to be feared.
John Adams, who would later sing the praises of Paine, took issue not with the idea of
democracy itself, but with Paine's extreme version of it, one that included allowing
men who didn't own property to hold office and vote.
While scholars of today will often debate why common sense was so successful and how
much of an impact it had on history, the market left on the American revolutionary and on
the American psyche was immense and survives to this day as Ivy League candidates attempt
to speak to the common man.
As the war progressed, Paine wrote more propagandist texts for the cause.
The American Crisis, published in 1776, is an often quoted motivational text that Washington
had read aloud to his troops.
In it, Paine appealed for the proper application of force and wanted a trained military to
replace the militias.
It should be noted, this later became American policy.
In 1777, Paine was involved in foreign affairs for the Congress.
Paine lost more friends when he spoke against war profiteering by public officials.
The scandal centered on Silas Dean.
Dean demanded a public investigation into his suspected financial ties.
Dean was defended by wealthy public officials, including John Jay, who saw no issue with
holding public positions while also profiting from personal business deals with governments.
Paine, father of the revolution, was denounced as being unpatriotic and attacked by Dean's
supporters in the streets.
Paine left the Committee of Foreign Affairs with his reputation in tatters.
It should be noted that years later, the corruption Paine alleged was widely acknowledged.
He even received apologies from some of those who attacked his name.
Paine continued his relentless assault on the wealthy and powerful in public good.
The pamphlet suggested that land west of the Thirteen Colonies belonged to the U.S. government
and to the people of the new nation.
Thomas Jefferson, George Washington, and James Madison all had personal claims to lands in
the region Paine was suggesting belonged to everybody.
It should be noted the Northwest Ordinance of 1787 agreed with Paine's position.
At the time, however, his long list of attacks against the wealthy and powerful left him
with limited friends.
Only the most forward-thinking of his contemporaries were still around.
He left with critic of slavery, Lieutenant Colonel John Lorenz, for France to obtain
funding for the revolution.
Over the course of six months in 1781, Lorenz and Paine succeeded in securing funding for
the war effort with Benjamin Franklin acting as facilitator for meetings.
Upon return, they were welcomed.
Paine, who donated royalties from Common Sense to the war effort, objected to the idea of
being paid for their services.
By 1787, he was back in London.
When the French Revolution popped off in 1789, Paine was intrigued and in 1790, he was there.
Edmund Burke released a counter-revolutionary pamphlet that appealed to the ruling class.
Paine sought to undermine it with his work, Rights of Man.
Unlike much of his early work, Rights of Man wasn't short.
It was almost 100,000 words long.
It didn't just attack monarchies, but went after many social traditions of the day.
It was so radical, he had trouble finding a publisher at first.
However, once he had, it sold a million copies.
Because of the success, the ruling class kind of had to respond, right?
They slandered him.
They slandered him.
They tried to discredit him.
In response, he published Part Two of Rights of Man.
He wanted to do away with the idea that those who were born into money and power were inherently
better leaders, and he made his case for that.
A lot of what I'm about to say is going to sound real familiar.
He believed that the youth was the real key.
Because of that, he believed in universal education, wanted equal access to education
for the poor.
He believed that if people were educated, they would be less reliant on social safety
nets, and he was going to propose a few of them.
He wanted education because he believed that those people who didn't have it, well, they
would just end up laboring in professions.
They would stay economically disadvantaged, and they'd stay in poverty, and that poverty
would create crime.
It makes sense.
He wanted to alleviate that, and he wanted to create some social mobility.
To help achieve this, he proposed tax cuts for the poor, not the rich.
He advocated for maternity benefits, payments, payments to newlyweds, wanted the state to
pay for some funerals.
He advocated for higher taxes on the wealthy and taxes on luxury.
You know, that ruffled some feathers.
He wanted an old age pension program and total employment.
He described these workhouses where people could go if they were down on their luck.
They would be given room and board and a stipend.
They could stay as long as they want.
They would be hired, no discrimination, and it would help them get back on their feet.
This really angered the ruling classes a lot.
They incited even more hatred against him.
He was chased by mobs, burned in effigy.
The authorities wanted to try him.
He wound up having to flee the country.
They tried him in his absence and found him guilty anyway, but luckily for him, he was
unavailable for the hanging they had in store for him.
In response to the charges of sedition and libel, he said, if to expose the fraud and
imposition of monarchy, to promote universal peace, civilization, and commerce, and to
break the chains of political superstition and raise degraded man to his proper rank,
if these things be libelous, let the name of libeler be engraved on my tomb.
In France, rights of man was well received.
Paine was given honorary French citizenship.
Even though he couldn't speak French, he was elected to a parliament of the French
Revolution.
After being elected, he was placed on the convention's constitutional committee.
He helped in the drafting and voted for a French republic.
However, being opposed to capital punishment, argued that the king should be exiled.
The king was later executed.
Paine was arrested in France in 1793.
The reasoning isn't entirely clear, but that was kind of the times.
A number of prominent Americans attempted to secure his release, but for the moment,
he was headed to the guillotine.
Now this is the point where we could definitely have a different ending to this story, but
luckily Paine kept his cool, kept his head.
Usually the jailer would walk up and down the hallway and put a chalk mark on the door
of prisoners that were to be taken out the next morning.
When the jailer walked by Paine's door, the door was open.
So when the jailer made his mark, and then the door was shut, the mark was on the inside.
So the next morning, Paine wasn't taken out.
He wasn't led to the guillotine.
This saved his neck for a few days, and in a bizarre twist of fate, it was in that time
span that the new American minister to France, James Monroe, was able to secure his release.
In July of 1795, he was readmitted to the French National Convention.
Paine opposed the adoption of the new French Constitution because it did away with the
universal suffrage promised in the 1794 Constitution.
The old saying that the most radical revolutionary becomes a conservative the day after the rebellion
didn't apply to Paine.
In 1796, a bridge he designed was built in England.
His design became the prototype for many single-span iron bridges.
The design obtained a patent in Britain.
He developed a smokeless candle and helped other inventors.
He also provided advice to Napoleon on his planned invasion of England.
Napoleon wound up being a fan of Paine's.
Paine felt betrayed by the American Revolution.
He saw the failing to help revolutionary France as an insult to those Americans who died for
the idea of removing monarchies.
Speaking of George Washington, he said,
The world will be puzzled to decide whether you are an apostate or an imposter, whether
you have abandoned good principles or whether you ever had any.
He only came back to the U.S. in 1802 when President Jefferson requested it.
Paine died on June 8, 1809 in Greenwich Village, New York City.
His funeral was attended by less than a dozen people.
The great agnostic Robert G. Ingersoll wrote,
Thomas Paine had passed the legendary limit of life.
One by one, most of his old friends and acquaintances had deserted him, maligned on every side,
exercated, shunned and abhorred.
His virtues denounced his vices, his services forgotten, his character blackened.
He preserved the poise and balance of his soul.
He was a victim of the people, but his convictions remained unshaken.
He was still a soldier in the Army of Freedom, and still tried to enlighten and civilize
those who were impatiently waiting for his death.
Even those who loved their enemies hated him, their friend, the friend of the whole world,
with all of their hearts.
On the 8th of June, 1809, death came.
Death, almost his only friend.
At his funeral, no pomp, no pageantry, no civic procession, no military display.
So there you have it.
He started with nothing, ended with nothing, but accomplished a whole lot along the way.
He did this and was able to do it because he never sacrificed his principles.
He stayed fighting for freedom his entire life, advocating for the betterment of man,
the betterment of humanity.
He also shatters a lot of stereotypes that we have about the founders of this country.
He avoided the most horrible practices that are associated with many of them, and openly
condemned them, because he wanted freedom, real freedom.
He wanted to move society forward.
He wanted to take society down that road, a society that would lead to something better.
And he was willing to put his own neck on the line, literally, to do it.
He is not the best-known founder of this country, but he's probably one of the most important.
I think we can probably all learn a whole lot from the footsteps in the roads of Thomas
Paine.
The aristocracy are not the farmers who work the land, but are the mere consumers of the rent.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}