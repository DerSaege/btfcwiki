---
title: Let's talk about controlling the narrative around climate change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8URGCX4R3bc) |
| Published | 2021/01/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Conducted first global ice loss survey from 1994 to 2017, revealing a loss of 28 trillion metric tons of ice at the caps.
- Impacting Antarctica and Greenland the most, losing about 1.2 trillion tons per year.
- Less ice means less heat reflected, exacerbating the problem.
- Current rate of loss puts us on track for worst-case climate scenarios from the IPCC.
- Paris Agreement, based on IPCC, may not be sufficient in addressing the crisis.
- Urges individuals to draw parallels with 2020's public health denials to understand climate change denial.
- Warns against letting those who deny climate change control the narrative.
- Urges immediate action as delays will hinder tangible results.
- Calls for political leaders and influencers to prioritize understanding and addressing climate change.
- Emphasizes the catastrophic consequences of worst-case climate scenarios.
- Stresses the importance of not allowing financial interests to manipulate the narrative around climate change.

### Quotes

- "When you're normally talking about weight and you want to visualize a large amount of weight, people compare it to a 747. Eighty billion. Eighty billion 747s."
- "We have to start acting now to achieve any tangible results."
- "The worst-case scenario from the IPCC, that's a disaster movie."
- "The narrative cannot be controlled by those people with deep pockets who have a vested financial interest in going against the scientific consensus."
- "Worst-case scenario, we make the world a better place for nothing."

### Oneliner

Confronting the alarming reality of global ice loss and urging immediate action to combat climate change denial and its catastrophic consequences.

### Audience

Climate activists, policymakers, environmentalists.

### On-the-ground actions from transcript

- Contact political representatives to prioritize climate change action (implied).
- Join or support organizations advocating for climate action (implied).
- Educate community members about the urgency of addressing climate change (implied).

### Whats missing in summary

Detailed breakdown of specific actions individuals can take to combat climate change denial and contribute to environmental conservation efforts.

### Tags

#ClimateChange #GlobalIceLoss #IPCC #ParisAgreement #ClimateAction


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about ice loss.
Not really something that seems interesting, but it is and it's important.
The first global ice loss survey was conducted.
What it found was that from 1994 to 2017 we lost 28 trillion metric tons of ice at the
caps.
28 trillion metric tons.
I'm going to be honest, I don't have a frame of reference for that.
I have no idea how much ice that is.
When you're normally talking about weight and you want to visualize a large amount of
weight, people compare it to a 747.
Eighty billion.
Eighty billion 747s.
I'm going to be honest, I don't really know what that is either.
I looked trying to find a good visualization.
I only found one.
Picture an ice cube that is 10 kilometers across, 10 kilometers deep, and as tall as
Mount Everest.
28 of those.
That's how much ice was lost.
That's concerning.
It's impacting Antarctica and Greenland the most.
We are losing about 1.2 trillion tons per year.
When you understand the dynamics and the fact that those sheets of ice reflect the heat,
it gets even more concerning because the less ice we have, the less heat is reflected, which
kind of just snowballs the process.
This rate of loss puts us on track for the worst case scenarios from the IPCC.
That's the Intergovernmental Panel on Climate Change.
The Paris Agreement that we're going back into, that was based in large part on the
IPCC, but not the worst case.
So while it's good that we're going back into the Paris Agreement, it does not appear
that it is going to be enough.
One of the things that we can do as individuals is to remember 2020, to look back at last
year because a lot of the same mechanics are at play.
We have a group of people who says, this isn't a thing, don't worry about it.
Those scientists, they don't know what they're talking about.
Listen to the people who are financially vested in the decision.
They're the ones that are really going to make the smart choices and be unbiased.
It's the same situation and it will have the same outcome.
Less than a year, what, 400,000 gone?
If we allow the anti-mask community, as it were, to control the narrative around climate
change, we're going to have the same issues.
At the end of the day, there aren't a lot of people who have looked at this and in good
faith say, oh, that's not a thing.
The people saying that are people who are more concerned with their pocketbooks or their
political position than they are literally the entire world.
We need to treat those who deny that this is a thing the same way we treated those who
didn't believe in the public health issue, who didn't think it was a thing, who thought
it was overblown, because if we don't, we're going to have the same problems.
This is just like an 18-wheeler as well.
It doesn't stop just because you hit the brakes.
It takes time.
We have to start acting now to achieve any tangible results.
It does not look like the Paris Agreement is going to cut it, not according to this.
Just like last year, new information is coming out all the time.
Sometimes it's good, sometimes it's not.
Generally, it's not.
So I would err on the side of caution if it was me.
We need to make sure that those who are in political office, those people who have the
ability to influence change in the private sector, those people who have the ability
to influence thought understand that this is important.
The worst-case scenario from the IPCC, that's a disaster movie.
That's not something that anybody wants to be around for.
The narrative cannot be controlled by those people with deep pockets who have a vested
financial interest in going against the scientific consensus.
We have seen in a very small experiment what happens when that occurs.
Comparatively, last year was tiny when you're talking about comparing that to climate change.
But it is the same mechanics.
It's a bunch of people who want to put their pocketbooks over the world, over everybody.
They have manipulated a bunch of people into thinking that there is a real debate, there
is a real discussion about this in the scientific community.
There's kind of not.
That's not really something that occurs.
The number of trained people, of actual scientists who understand this, who have looked at the
evidence who think, oh, this isn't really something we need to worry about.
It is a tiny percentage.
When you are talking about something that can have this bad of outcomes, I would suggest
erring on the side of caution.
Worst-case scenario, we make the world a better place for nothing.
We cannot let the people who disrupted the public health response last year disrupt the
response to something that literally threatens the entire world.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}