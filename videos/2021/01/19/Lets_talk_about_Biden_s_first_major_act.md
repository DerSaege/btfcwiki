---
title: Let's talk about Biden's first major act....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OqcOtFbJT4s) |
| Published | 2021/01/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Incoming Biden administration plans immigration reform package post-inauguration.
- Includes pathway to citizenship for those out of status.
- Conservative movement opposes reform, citing job loss and law-breaking.
- Beau questions opposition as bigotry, as bill details are not yet known.
- Conservative argument of needing to do things legally contradicts opposition to legal mechanism in bill.
- Reform bill details not public, could include stringent background checks and long waiting periods.
- Beau advocates for waiting to see bill before forming opinions.
- Urges basing decisions on policy rather than emotion or preconceived notions.
- Opposes bill if it doesn't go far enough but stresses importance of informed decision-making.
- Encourages audience to avoid manipulation and wait for bill details.

### Quotes

- "Base your decisions on policy rather than raw emotion."
- "It has nothing to do with policy. It has to do with skin tone."
- "Perhaps it would be best to wait and see the bill."
- "Y'all have a good day."

### Oneliner

Incoming immigration reform package sparks conservative backlash pre-details, prompting Beau to call out opposition as bigotry and advocate for informed, policy-based decisions.

### Audience

Advocates, Voters, Activists

### On-the-ground actions from transcript

- Wait for the immigration reform bill details before forming opinions (suggested)
- Base decisions on policy rather than emotion (suggested)
- Advocate for informed decision-making on immigration reform (exemplified)

### Whats missing in summary

The full transcript provides a detailed analysis of the conservative opposition to immigration reform and underscores the importance of informed decision-making based on policy details rather than preconceived notions or emotions.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about what may be one of the incoming Biden administration's
first major pushes for legislation. Word has gone out that shortly after the inauguration,
possibly the day of, a semi-comprehensive immigration reform package will be unveiled.
It will include a pathway to citizenship for those who are currently out of status.
Predictably, the conservative movement in the United States is pushing back against this.
You know, they broke the law, they're going to take our jobs, and so on and so forth.
All of the normal arguments. The thing is, to me, it seems like this time,
these arguments have to be coming from a place of bigotry.
Because, I mean, word just went out. The bill is expected to be hundreds of pages long.
It's not like they read it yet. I know they haven't read it yet because it hasn't been
released yet. So they can't say that this is about policy. This whole time,
the conservative movement in the United States has said that it was, well, they need to do it legally.
This is a legal mechanism being created so they can do that. I don't understand why they would
oppose it unless it has nothing to do with doing it legally and has to do with, well,
them being foreigners. It's what it seems like. Because they have no idea what's in the bill.
For all they know, the bill may require anybody who wants to hop on this path to citizenship
to undergo a stringent background check, submit their biometric data so they can be tracked,
pay taxes, and wait an absurdly long additional five years before they can start their
naturalization process. That's on top of any time they already spent in the country.
That could be the plan. It is the plan. Those are the bullet points.
But you can't support or oppose the bill based on that. You have to know what the details are.
And they don't because nobody does. So this early push against this much-needed reform
has to be coming from a place of bigotry. There's no way it can be based on policy.
There's no way it can be based on policy. So from this point, anybody who's already opposed it,
any argument they make once the bill is unveiled, it doesn't matter because they already showed
their hand. They already made it very clear that it has nothing to do with policy. It has to do
with skin tone. I am a huge proponent of immigration reform. If I dislike this bill,
it's going to be because it doesn't go far enough. But I'm not willing to say I support it yet
because I haven't read it. If you don't want to be manipulated by some guy in a red hat,
perhaps it would be best to wait and see the bill. Base your decisions on policy
rather than raw emotion. Base your decisions on what's best for the country
rather than what your favorite personality told you to believe.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}