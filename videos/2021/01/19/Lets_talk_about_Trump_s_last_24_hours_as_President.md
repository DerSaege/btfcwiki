---
title: Let's talk about Trump's last 24 hours as President....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=SbIg43fMVMM) |
| Published | 2021/01/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- President Donald J. Trump's term ends in 24 hours, leaving many feeling relieved or anxious about the transition of power.
- Trump's impact on the country, including damaging international standing and domestic policies, will be debated for years.
- The seed of doubt about a peaceful transfer of power underlines the fragility of society.
- Beau argues that this doubt is beneficial as it keeps people politically engaged and aware of the power dynamics at play.
- He believes that Trump's presidency should serve as a wake-up call for the public to be more informed, engaged, and proactive in shaping the country's future.
- The rise of authoritarianism was fueled by apathy and ignorance, with many supporters unaware of the implications of their actions.
- Beau stresses the importance of altering political discourse and engaging more people to prevent the rise of another, potentially more dangerous, Trump-like figure in the future.

### Quotes

- "That seed of doubt should keep you politically engaged."
- "What brought us Trump was a combination of apathy and ignorance."
- "We have to completely alter political discourse and political engagement in this country."

### Oneliner

President Trump's impending departure stirs a mix of relief and apprehension, underlining society's fragility and the need for increased political engagement to prevent future authoritarian threats.

### Audience

American citizens

### On-the-ground actions from transcript

- Inform more people about different ideologies (implied)
- Engage more individuals in political discourse (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of Trump's impact on society and the necessity for increased political engagement to prevent authoritarianism in the future.

### Tags

#Trump #TransferOfPower #Authoritarianism #PoliticalEngagement #Society


## Transcript
Well howdy there internet people, it's Beau again.
So uh...
24 hours.
24 hours from the time of publication
and President Donald J. Trump's term comes to a close.
Are you relieved?
Excited?
Have a feeling that everything's going to be okay, right?
Probably not.
Probably not.
You probably have a little bit of apprehension,
a little bit of anxiety, because we are not on the plane home yet.
And we have to keep our guard up
until we are on the plane home.
There's that seed of doubt
that has been planted.
Trump damaged this country
in innumerable ways
from international standing
to scores of domestic policy
to the general political climate.
And the damage that he caused
will be discussed
and debated
for decades.
And I would imagine
that many people looking back are going to say that that seed of doubt
that you have right now
about whether or not there's going to be a transfer of power,
that that's his biggest insult.
That that's the most damaging thing that he did.
Undermining faith
in the very basic functions
of a democratic republic.
I disagree.
I don't. I think it's
possibly one of the best things that he has done for this country.
Because realistically, rationally,
you know that transfer of power is going to occur.
Yeah, it might be chaotic, it might be messy.
We'll have to wait and see.
But you know it's going to happen.
But you have that seed of doubt.
And it makes you realize
exactly
how fragile
our society is.
It makes you realize that one person
changing their job
can cause stress and anxiety
in a majority of the population.
I would suggest that possibly that position has too much power.
But aside from that,
it should
keep you engaged.
That seed of doubt,
the way you feel right now, twenty-four hours out
and still unsure
of what's going to happen,
that seed should be planted.
It should keep you politically engaged.
It should keep you wanting to stay informed,
wanting to better
your community,
wanting to strengthen your community,
wanting to help guide
this country
from the bottom.
What brought us Trump
was a combination of apathy
and ignorance.
That's how we got here.
Yeah, he played to racism.
He whistled all of the right tunes
to get people to go along with him.
But there were many
who didn't see him for the danger that he was.
And we got a four-year reprieve.
We went on our first date
with this
new brand
of authoritarianism.
And
we have a four-year reprieve.
And in that four years,
we have to completely
alter the political landscape.
We have to get more people engaged,
inform more people
about different ideologies.
Because half of the people
who
who backed Trump
have no idea
what brand of authoritarianism
they were supporting.
Even when you hand them a checklist,
they don't see it.
Historically,
a country does not
fall in love
with that brand of authoritarianism
on the first date.
They go out a couple of times
before it really takes hold.
So unless you want to see another Trump,
a more capable Trump,
in four or eight or twelve or sixteen years,
we have to completely alter
political discourse
and political engagement in this country.
We have to plant those seeds.
We have to make sure
that it grows into something that can resist
another Trump.
Because there will be another one.
And
that one may not
end the date with a kiss at the door.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}