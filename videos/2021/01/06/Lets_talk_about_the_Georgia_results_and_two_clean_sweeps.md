---
title: Let's talk about the Georgia results and two clean sweeps....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=alQ09QDgQPg) |
| Published | 2021/01/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Decision Desk called both races in Georgia for Democrats around 3 a.m.
- Democrats won both Senate seats with one being a thin margin, likely requiring a recount by state law.
- Democrats now have control of the House, Senate, and White House.
- Senate Democrats lack the 60 votes needed for most actions but can set the legislative agenda.
- Senate Majority Leader McConnell's power to stall bills is now diminished.
- Despite focus on easier cabinet appointments for Biden, the real executive agenda lies with functionaries.
- McConnell gains control of the Republican Party amid Trump's losses.
- Trump faced defeats with a veto override, lost stimulus checks, and defeated candidates in Georgia.
- Trump's influence within the Republican Party significantly diminishes.
- McConnell emerges as a potential new influential figure within the party.

### Quotes

- "Democrats have control of the House, the Senate, and the White House."
- "Trump walks away from today toxic."
- "McConnell will be the new kingmaker."

### Oneliner

Decision Desk called Georgia races for Democrats, shifting power dynamics; McConnell emerges influential amid Trump's losses.

### Audience

Political observers, Democratic and Republican Party members.

### On-the-ground actions from transcript

- Analyze the political landscape for potential shifts and alignments (implied).

### Whats missing in summary

Further insights on potential future political strategies and alliances.

### Tags

#Georgia #Democrats #Senate #McConnell #Trump #RepublicanParty


## Transcript
Well howdy there internet people it's Beau again. So this morning we're going to talk about two
clean sweeps. I want to say around 3 a.m. Decision Desk called both races in Georgia for the
Democrats. I would point out that one of those races is a very thin margin, so much so that I
think state law requires a recount, but at this moment it does look like the Democrats won both
seats. So what does that mean? Democrats have control of the House, the Senate, and the White
House. They can do whatever they want, right? Not really. In the Senate, most things require 60 votes.
Democrats don't have it. What it allows them to do is set the legislative agenda. We all know that
Senate Majority Leader McConnell was famous for just sitting on bills. He won't be able to do it
anymore. That's the real power that shifted. I know a lot of people are pointing to the appointments,
like cabinet appointments for Biden and stuff like that, and how they'll be easier to get confirmed.
That's true, but realistically they don't matter as much as people might think. Most of Biden's
appointments are political appointments. The real work, the real executive agenda,
is going to be set by people, undersecretaries and stuff like that, functionaries. So
if one of his appointees didn't get confirmed, it's not going to change anything really.
So that's where the Democratic Party stands. Who else won big? McConnell, as weird as that sounds.
As far as the Trump-McConnell power struggle, Trump lost everything. His veto got overridden,
didn't get the stimulus checks he wanted, and his two candidates in Georgia lost.
So although McConnell is going to lose his Senate Majority position,
he kind of won control of the Republican Party.
Trump walks away from today toxic. Everything he touches is turning bad,
and with the theatrics that are planned for later today that are doomed to fail,
most of his supporters are going to burn a whole bunch of political capital today
and accomplish nothing. I don't want to say that Trump is going to win,
accomplish nothing. I don't want to say that Trump is going to disappear, never to rear his head
again, but he has certainly lost a whole lot of influence within the Republican Party.
It appears that McConnell will be the new kingmaker. I mean, I don't know if that's good or
bad, to be honest, but those are the the fights of today, and we'll see how it shapes up later
during the theatrics. If you want to know what to expect there, every state that they object to
is going to be two hours of debate, 30 minutes of voting. It's going to drag on and on and on.
There is an option on the table that would make the Democrats actually object to slates of electors.
I don't know how likely it is, but either way, the end result is going to be Biden being confirmed.
They don't have the votes. Trump supporters do not have the votes to pull this off,
like not even close. I would imagine that even some of those who were
considering supporting yesterday won't today. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}