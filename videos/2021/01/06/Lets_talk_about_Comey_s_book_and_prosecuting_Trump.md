---
title: Let's talk about Comey's book and prosecuting Trump...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=CWBK5FyKGYs) |
| Published | 2021/01/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Comey suggests not pursuing criminal investigation of Trump by the next Attorney General, prioritizing fostering trust over justice.
- Beau criticizes politicizing the justice system and warns of the dangers to a free society.
- He expresses concern about shielding individuals from prosecution based on their political beliefs or popularity.
- Beau advocates for the importance of truth over political expediency in investigations.
- He warns of the danger of creating a government where some are untouchable and law enforcement can act with impunity.
- Beau believes failure to hold Trump accountable for any crimes sets a dangerous precedent for future leaders.
- He stresses the need for accountability, investigation, and prosecution if crimes were committed, regardless of political implications.
- Beau criticizes Comey's advice and questions its intentions in creating buzz for his book.
- He underscores the importance of a full accounting of Trump's actions to help the American population understand the gravity of his presidency.
- Beau calls for adherence to judicial principles and the importance of faith in the judicial system.

### Quotes

- "Politicizing the justice system is wrong and it's incredibly dangerous to a free society."
- "Nobody should be prosecuted simply because of their political beliefs and nobody should be shielded from prosecution simply because of their political beliefs or their popularity with a segment of the American population."
- "If there is no accountability for any crimes the President may have committed, it sends a message to the next would-be tyrant that there's no risk."
- "You don't make the determination based on what's politically expedient."
- "To those who are politically savvy, we know what President Trump was. We know what he was trying to do, but large portions of the American population do not understand it."

### Oneliner

Comey's advice on not investigating Trump raises concerns about justice versus fostering trust, leading Beau to advocate for accountability and a full accounting of Trump's actions.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Advocate for accountability and transparency in investigations (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the implications of not holding Trump accountable for potential crimes, stressing the importance of upholding justice for all individuals.

### Tags

#JamesComey #DonaldTrump #Accountability #Justice #PoliticalBeliefs


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about James Comey's upcoming book.
Some news outlets got advanced copies of it and there are some quotes floating around
that I definitely believe warrant discussion.
In Comey's soon to be released book, he apparently suggests that the next Attorney General, the
one under Biden, should not pursue a criminal investigation of Donald Trump, no matter how
compelling the roadmap, how powerful the evidence.
Although those cases might be righteous in a vacuum, the mission of the next Attorney
General must be fostering the trust of the American people.
You know, when people started chanting, lock him up, lock him up, I didn't like it.
I made a video about it.
I'm one of the few people of my political persuasion that took issue with it.
Politicizing the justice system is wrong and it's incredibly dangerous to a free society.
The United States is not supposed to be a place where it is, show me the person and
then I'll find the crime.
That's not how things are supposed to work.
Politicizing the judicial system is dangerous.
That works both ways.
Nobody should be prosecuted simply because of their political beliefs and nobody should
be shielded from prosecution simply because of their political beliefs or their popularity
with a segment of the American population.
That is a really bad precedent to set.
You know, there's also a problem with the idea that the next Attorney General is supposed
to foster trust by helping to cover up alleged crimes of President Trump.
I would suggest if you want the next Attorney General to foster trust, they would need the
truth.
Maybe I'm just old-fashioned, I don't know.
Seems like those things go together.
I don't think there should be a fishing expedition to find something to charge President Trump
with.
However, there are a lot of activities that seem like they might be crimes.
Those need to be investigated and if they weren't it, they need to be prosecuted.
You know, Trump's real danger, the real danger he posed to the United States was his
desire to create a government that had a group of people at the top who can operate with
impunity and then everybody else was just othered and law enforcement could do whatever
they wanted to them.
If Comey's advice is followed, Trump succeeded.
That's exactly the system he created and he's part of the untouchable class.
I would suggest that's wrong.
I would also suggest that a large segment of the American population needs to understand
what really happened and how they may have been taken for a ride.
There's a lot of people who look at President Trump and refuse to see horror on horror's
face.
You know, if there is no accountability for any crimes the President may have committed,
it sends a message to the next would-be tyrant that there's no risk.
Try all you want, nobody's going to prosecute you afterward because we have to protect the
institution of the presidency, right?
If the presidency becomes a line of would-be dictators, it's not worth protecting.
President Trump damaged the presidency, damaged the foundations of this country.
If he committed a crime while doing that, that needs to be investigated, prosecuted
and he needs to be convicted.
If not, he needs to go free.
That's how our judicial system is supposed to work.
You don't make the determination based on what's politically expedient.
If you do that, we already have the system that President Trump wanted to create and
that would be more undermining to the institution of the presidency than a prosecution would.
It would also destroy everybody's faith in the judicial system because there would be
no accountability for those with political connections.
That's the message that would get sent.
James Comey's wrong on every level.
I wish I could find these quotes in context.
They're not released that way.
My guess is that this was designed to create buzz for his book.
I'm pretty sure that's going to happen.
I'm not sure it's the kind he wants though.
To those who are politically savvy, we know what President Trump was.
We know what he was trying to do, but large portions of the American population do not
understand it.
They don't get it.
And the only way that they are going to is if there is a full accounting of his actions,
especially those that may have been criminal.
Anyway it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}