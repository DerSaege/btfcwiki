---
title: Let's talk about Pelosi's laptop and the FBI investigation....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=o0i4iAegN14) |
| Published | 2021/01/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Late-breaking news from last night regarding events at the Capitol.
- Complaint filed by the FBI, information from a former romantic partner of the person named.
- Former romantic partners not always the most reliable source of information.
- Mention of theories about laptops being taken from the Capitol.
- Witness claimed to have seen a video of Williams taking a computer device from Speaker Pelosi's office.
- Williams intended to send the device to a friend in Russia for sale to SVR, Russia's intelligence service.
- Transfer to Russia did not happen, and Williams allegedly still has the device or destroyed it.
- FBI complaint mentions Riley June Williams.
- Pelosi's staff confirmed a laptop was taken, but it contained no sensitive information.
- Williams sought by the FBI for remaining in a restricted area.
- Complaints may contain inaccurate information, benefit of the doubt advised.
- Worth noting and being aware of the situation during the event at the Capitol.
- Event emphasized patriotism and making the country great.

### Quotes

- "Complaints like this often contain inaccurate information."
- "It's worth being aware of this situation."
- "Making the country great and all of that."

### Oneliner

Late-breaking news from the Capitol involves a complaint filed by the FBI, alleging Williams took a computer device from Speaker Pelosi's office, intending to send it to Russia, but transfer failed, raising questions about accuracy and importance in the context of a patriotic event.

### Audience

Concerned citizens

### On-the-ground actions from transcript

- Stay informed about developments in the investigation (implied)

### Whats missing in summary

The full transcript provides additional context and details regarding the events at the Capitol and concerns about the accuracy of information provided.

### Tags

#Capitol #FBI #Investigation #Patriotism #Williams #Information #Awareness


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some late breaking news from last night that I found
interesting, I'm sure you will too.
To be honest I don't have a whole lot of commentary on it, it seems to speak for itself in a lot
of ways.
It's stemming from the events that occurred at the Capitol.
Now before we get into this I do want to point out that this is a complaint filed by the
FBI and that the source of the information does appear to be a former romantic partner
of the person named.
You don't have to be a lead investigator with the FBI to know that former romantic partners
are not always the best source of accurate information.
So innocent until proven guilty, the matter is still under investigation and all the caveats
that go with it.
Given some of the theories that are floating around about laptops being taken from the
Capitol though, one passage certainly caught my eye.
Witness 1 also claimed to have spoken to friends of Williams, who showed Witness 1 a video
of Williams taking a laptop, computer, or hard drive from Speaker Pelosi's office.
Witness 1 stated that Williams intended to send the computer device to a friend in Russia,
who then planned to sell the device to SVR, Russia's foreign intelligence service.
According to Witness 1, the transfer of the computer device to Russia fell through for
unknown reasons and Williams still has the computer device or destroyed it.
This matter remains under investigation.
Oh I bet it does.
I bet the FBI is really interested in that.
This is coming from a complaint filed by the FBI mentioning Riley June Williams.
The complaint also contains some links to YouTube videos and some screenshots in which
the FBI believes Williams is visible along with a DMV photo and some other stuff.
It should be noted that Pelosi's staff did confirm that a laptop was taken.
However it was a laptop used for like PowerPoint presentations and contained no sensitive information.
That might be relevant.
Again I want to point out that this is alleged to have occurred during the Great Patriot
event at the Capitol.
Williams is currently being sought by the FBI and for I guess to ask about this and
the standard charge of remaining in a restricted area.
Again it kind of speaks for itself.
I do imagine that we will hear more about this as the day goes on but I definitely found
it interesting.
But please for real do keep in mind that complaints like this often contain inaccurate information.
So really do provide the benefit of the doubt on this one.
However it's certainly worth noting, it's worth being aware of this situation given
the purpose of the event, you know, making the country great and all of that and it being
a super patriotic event, it seems like this should be mentioned at least.
Yeah.
Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}