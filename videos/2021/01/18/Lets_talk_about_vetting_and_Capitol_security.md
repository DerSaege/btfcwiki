---
title: Let's talk about vetting and Capitol security....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8eO-JsGmJyQ) |
| Published | 2021/01/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the difference between perceived capability and actual capability in security measures.
- Gives examples of perceived capability (like a stadium on game day) and actual capability (like Disney World).
- Talks about the large security presence in DC and why there are so many troops present.
- Mentions the rings of security, with the National Guard likely being on the outer rings.
- Clarifies that the FBI vetting troops is a common practice, not necessarily due to a specific threat.
- States that the FBI is involved in vetting due to their expertise and quick processing abilities.
- Addresses the possibility of a threat from the troops but deems it unlikely due to the security measures in place.
- Emphasizes the shift from relying on perceived capability to actual capability after the events at the Capitol.
- Notes that while there are many troops present, there doesn't seem to be a specific threat identified.
- Points out the broader security concerns beyond just the immediate event, considering other opposition groups.

### Quotes

- "90% of security is perception."
- "As far as I know, as far as the public statements, as far as the activities that we're seeing, nothing indicates that they have a specific threat or that they have flagged anybody."
- "Because of the events of the Capitol and the actions of the people there, all of the foreign opposition groups are emboldened."

### Oneliner

Beau explains the difference between perceived and actual security capabilities in DC, addressing the troop presence and FBI vetting without a specific threat.

### Audience

Security analysts, event planners.

### On-the-ground actions from transcript

- Trust the security measures in place (implied).
- Be vigilant and report any suspicious activity (implied).

### Whats missing in summary

Insight into how recent events have impacted security measures and perceptions.

### Tags

#Security #PerceivedCapability #ActualCapability #FBI #NationalGuard


## Transcript
Well, howdy there, Internet people. It's Beau again. So today we are going to talk about
the security precautions being taken in DC. I'm going to answer some questions because
a whole lot of them came in last night, mainly because the media appears to be leaning into
a non-story, in my opinion. But to answer the questions, you have to know a little bit
about the cat and mouse game that is security. There are two kinds of capability in the security
world. There is perceived capability, what the public believes the security level is.
And then there's actual capability, what the security level really is. Those two things
are almost never the same. A good example of perceived capability is a stadium on game
day. You walk up, there's somebody there with a wand, there are cops everywhere, you go
through a metal detector, they're looking in bags, you empty your pockets, it seems
really secure. It's not. It's not even close. There is a large security presence at common
entry and exit points. It's perception, it's a deterrence. 90% of security is perception.
But it doesn't achieve actual capability. A good place, a good example of actual capability
is Disney World. Disney World has amazing security, but it doesn't look like a fortress,
with the exception of the castle, of course. The camera coverage that's there is astounding.
The speed at which they can recover a lost child, which is a really good test of security
capability, is amazing. That is actual capability. So, perceived capability keeps the mice away.
Actual capability catches the mice if they show up. DC. Why do they have so many troops?
Because this time they're going for actual capability. Locking down a sizable portion
of a city and actually locking it down requires a lot of people. In the past, they have probably
relied heavily on perceived capability. You see the people out there in uniform, you assume
that it's really locked down. Probably hasn't been. Mainly because that's the outer ring.
When you're talking about an event like this, there are rings of security. The closer you
get to, in this case, the people being protected, the more stringent the security. The National
Guard, they're going to be on the outer two rings, more than likely. They're probably
not even going to be close to the people being protected. That leads to the story. The FBI
is vetting all the troops because they're worried. No. That's really common. That is
incredibly common. I would be really surprised if that didn't always happen. Private details,
private security details. The off-duty cops that form up the outer ring of those details.
So I would be really surprised if that didn't happen with major events like this. Then the
next question is, why is the FBI doing it rather than the Army itself? If you watch
the video about Fort Hood, you know I speak very highly of Army investigators. They're
really good. They're also really slow and we don't have a lot of time. That's one reason.
The other reason is the FBI has an entire division that this is their jam. This is what
they do. The counterintelligence division at the FBI, their job is to investigate foreign
intelligence operations in the United States. So when a foreign power conducts an intelligence
operation in the U.S., most times the goal is getting to a U.S. government employee who
is compromised or turning them, which means this particular division is really good at
investigating our own people. They do it all the time, day in and day out. So they can
process through that massive number of troops very quickly because they already have people
with that skill set. So that's the reason this stuff is occurring. Is there a realistic
threat from this? I mean, is it likely that one of them is sympathetic to Trump's cause?
Yeah, of course. Yeah, that's probably true. Are they going to act on it? I doubt it because
by all signs, they are going for actual capability. And these people, they're going to be the
outer ring. They're going to be outward facing. They're not even going to be near the people
who are being protected. And they would have to go through security to get close. They
would have to go through more stringent security. So I don't see it as likely. Is it worth expending
all of the effort that they're doing? Yes. The events at the Capitol showed that right
now they can't rely on perceived capability. They have to rely on actual capability. So
they are proceeding with an abundance of caution, which is the right thing to do. As far as
I know, as far as the public statements, as far as the activities that we're seeing, nothing
indicates that they have a specific threat or that they have flagged anybody. This is
pretty normal behavior. The only thing that is abnormal is the number of troops. And that's
just because they're going for actual capability rather than the perception of it. Again, right
now I think because the events of yesterday, by the time you'll watch this, weren't heavily
attended, nothing got out of control, the media wanted to keep people's interests up.
So they're leaning into what is kind of a non-story. I wouldn't worry about this too
much. I don't think that if there is some incident, I don't think that it would come
from the National Guard. Is it a possibility? Sure. Is it worth looking into? Sure. Is it
the highest priority? No. And I don't think they're treating it as a high priority. I
think the media has just picked up on this. I would point out that the security detail
for this event doesn't just have to worry about what everybody's been talking about.
The United States has other opposition groups that would be very happy to disrupt this.
And they have to achieve actual capability against all of them. Because right now, because
of the events of the Capitol and the actions of the people there, all of the foreign opposition
groups are emboldened. So it makes the job much more difficult. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}