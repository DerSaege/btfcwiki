---
title: Let's talk about my morning routine and plot holes....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=lstQRIkhWuc) |
| Published | 2021/09/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau shares how he starts his mornings by reading messages with wild theories, finding it entertaining and thought-provoking.
- He describes a theory suggesting that the current public health issue is a facade to cover up a scheme involving the vaccine as a tool for depopulation.
- Beau points out glaring plot holes in the theory, such as the timing of population decline pre-vaccine and the illogical targeting of allies by supposed evil masterminds.
- He hypothetically outlines how a more feasible psyop to achieve such a goal might involve targeting nationalists through controlling information flow and narrative.
- Beau underscores the human tendency to seek patterns where they may not exist, leading to conspiracy theories in times of chaos and uncertainty.
- He cautions against embracing wild theories and encourages critical examination of motives, feasibility, logistics, and evidence.
- Beau expresses concern over the dangerous impact of baseless conspiracy theories that thrive on people's fear and desire for explanations in chaotic times.
- He concludes by reflecting on the historical context of conspiracies arising during times of tragedy and unease.

### Quotes

- "Humans are creatures that seek patterns. And if there isn't one that's available, they make one up."
- "There is no man behind the curtain. The world's just a scary place."
- "Conspiracies show up in times of a lot of tragedy. A lot of unease and a lot of chaos."

### Oneliner

Beau shares wild theories, debunks a dangerous conspiracy, and warns against seeking comfort in fabricated patterns during chaotic times.

### Audience

Critical Thinkers

### On-the-ground actions from transcript

- Examine wild theories critically and debunk dangerous conspiracies by assessing motives, feasibility, logistics, and evidence (implied).

### Whats missing in summary

The full transcript provides a deep dive into the psychology behind conspiracy theories and the human tendency to seek patterns in chaos. Viewing the entire transcript can offer a comprehensive understanding of these phenomena.

### Tags

#ConspiracyTheories #CriticalThinking #Debunking #HumanBehavior #Patterns


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about my morning routine and plot holes.
Most mornings I wake up, I grab my coffee, and I start reading my messages.
Invariably, almost every single day, somebody sends me some wild theory.
And to be honest, I enjoy them. I think it's a good way to start my day.
It forces me to think outside the box and entertain it.
And I have this like multi-step process that occurs when I'm reading one of these theories.
And the first is like, is this feasible? Does it make sense? That kind of stuff.
And most times it's not and it doesn't. There's glaring plot holes almost every single time.
And then my mind switches to, how would I do it?
If this was a goal, the stated goal in this theory, how would I do it?
If I was the powers that be, the evil mastermind behind it all, right?
So that happened this morning.
And this one is definitely worth sharing because it's pretty entertaining when you get to the end of it.
The theory is that our current public health issue, oh, that's not real.
That's not really a thing. And that it is the vaccine that's taking people out.
And this is done by people who want to globalize the world in an effort to depopulate.
There are huge plot holes in this one.
I mean, you can start with the whole psychological aspect of the more people there are,
the easier they are to kind of steer to maintain the status quo,
because you need a larger group of people to coalesce around the idea of overturning the status quo.
So if you wanted to maintain the status quo, which is moving towards globalizing,
you really wouldn't want to depopulate just randomly. That doesn't make sense.
Then there's the huge glaring plot hole of the fact that we started losing people before the vaccine.
That's a huge issue with this story.
Like if you were watching this in a movie, you would be upset by this.
It would totally ruin the suspension of disbelief.
You would just be sitting there saying, how did the writers miss that? Doesn't make sense.
And then the other side of this is that it hits the wrong group of people.
If you are somebody who is in favor of globalizing and you're some evil mastermind,
why would you target your allies? That doesn't make sense.
It just does not add up.
As an example, this channel, most people who watch this channel,
they don't view national borders as some sacred thing never to be crossed.
They're at least on some level receptive to the idea of a global game, of a global society.
I'm willing to bet the vaccination rate for people who watch this channel regularly is almost 100%.
They wouldn't target their allies. That doesn't make sense.
So let's say, hypothetically, for whatever reason, we're the evil masterminds.
How would we actually go about doing this thing that doesn't make sense to begin with?
But let's say we wanted to.
If you wanted to, you would want to target your opposition,
people who were opposed to your idea of globalizing the world.
So you'd be looking at nationalists.
So you have to tailor make a message for them.
That would be the goal. That's how you would do it, right?
So you'd start off by first getting them to slightly reject the main narratives in the country.
You want to make them susceptible to conspiracy theory.
That's what you want to do.
So we'd start off with simple stuff like hollow earth or whatever, weird stuff.
And as time goes on, you would want to narrow it so it is specifically targeting nationalists.
And you want to control the narrative.
So you find some way to seize control of all of the theories at once.
You want to have one messenger.
We're going to call them messenger X.
Now, if you wanted, you could use a different letter of the alphabet.
But messenger X seizes control of the narrative.
We get people in that information silo.
They're only taking information from our messenger, right?
And we make sure that our messenger is super vague, Nostradamus style.
That way they can never be wrong, they can never be disproven,
and they have to have a reason to conceal their identity.
That way there can't really be any digging into who they are.
That's how you would do it.
And then once you have that giant group of nationalists who are just eating out of your hand, right?
You control their flow of information.
Then you would either be the evil mastermind and release something,
or wait for a public health issue to emerge naturally and never let a crisis go to waste.
And you tell them, don't engage in basic mitigation efforts.
Don't take the vaccine.
That's what you would do.
That's how you would do it.
Because then your people, your allies, they're safe.
Because they're going to go out and get vaccinated.
It would just be your opposition that falls to it.
That's what a psyop actually looks like.
Now, the fact that that is actually pretty close to what happened, that's a coincidence.
I don't think that's actually what occurred.
I don't believe there's some man behind the curtain pulling all the strings.
Humans are creatures that seek patterns.
And if there isn't one that's available, they make one up.
That's why you see images in clouds or in toast or whatever.
They make them up.
If the pattern isn't available, they make it up.
However, the reality is when you are talking about millions of people all over the world,
there isn't a pattern.
Not really.
You have a bunch of people acting in their own interests, doing what they believe in.
Sometimes they align with others.
And when people see a certain name show up enough,
well, that person, well, they're obviously part of a conspiracy.
That's how it works.
It's comforting for people to believe that there are entities out there pulling all the strings,
controlling everything.
It's just not the case.
It's a whole bunch of people acting in their own self-interest.
And that's what creates the chaos.
But because people don't like how uncomfortable the chaos is,
they create a pattern.
They see things that aren't really there.
They connect dots that aren't even dots.
And they attempt to draw a picture.
There is no man behind the curtain.
The world's just a scary place.
So when you run across people who have embraced these wild theories,
first start with that pattern.
Start with that process.
Start with, does this even make sense?
Normally the answer is going to be no.
There's going to be giant plot holes in it.
Then, is this logistically feasible?
Like in this case, you are talking about hundreds of governments,
thousands of people, all working together for a goal that doesn't even add up.
That seems odd.
So you look at the feasibility of it.
Then you look at the logistics required to do it.
And then see if the motive actually matches the ends.
Because most times it doesn't.
Now if you get through all of this,
and there actually is, the theory is sound through all of this,
then you have to go to looking and examining their actual evidence.
Normally there's not a lot of it.
I have a video on how to do that as well.
Let's talk about a conspiracy theory and debunk one.
These theories, they're continuing to gain ground.
And they're really dangerous.
They are hurting people.
And most of them are as silly as the one we just went through.
So if you want to take a look at it, I'll put the other one down below.
These things are dangerous.
They really are.
And they're getting out of hand.
And in most cases, they're that easily ripped apart.
They don't make sense to any form of...
They don't stand up to any logic test, any reason test, any feasibility test.
It's just people who are scared.
Who are looking at all the chaos in the world,
and trying to find somebody to blame.
Even though these entities they create, they may be against them.
It's comforting to know that there at least is a plan from somebody.
You look historically, conspiracies show up in times of a lot of tragedy.
A lot of unease and a lot of chaos.
That's when they really take hold.
Anyway, it's just a thought.
I hope you all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}