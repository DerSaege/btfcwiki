---
title: Let's talk about the world's EMT and economic questions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OgBupzA66pI) |
| Published | 2021/09/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the misconception that if the standard of living in other countries increases, the standard of living in the United States must decrease.
- Breaks down the concept of gross domestic product (GDP) and how it relates to economic output.
- Challenges the idea that there is a finite amount of economy to go around and that resource exploitation is necessary for economic growth.
- Counters the belief that there is no money in peace by illustrating how humanitarian efforts and peace-building activities can be profitable.
- Provides examples of how companies, particularly those focused on renewable energy and infrastructure development, can profit from peace.
- Advocates for investing in companies that "hill rather than kill" as a positive alternative to defense contractors.
- Teases future episodes where more questions will be addressed.

### Quotes

- "We live in a world where everything is commodified."
- "There's only so much that can be done."
- "Everything makes money."
- "It's just not defense contractors that are gonna make a lot of money."
- "It's going to primarily go to companies that hill rather than kill."

### Oneliner

Beau explains economic misconceptions about global standards of living, GDP, resource exploitation, peace profitability, and the potential for companies to profit from humanitarian efforts.

### Audience

Global citizens

### On-the-ground actions from transcript

- Support humanitarian organizations (exemplified)
- Advocate for renewable energy and infrastructure development (exemplified)
- Invest in companies focused on peace-building efforts (exemplified)

### Whats missing in summary

In-depth exploration of economic misconceptions and the potential for profit in humanitarian and peace-building efforts.

### Tags

#GlobalEconomics #Misconceptions #Peacebuilding #HumanitarianEfforts #RenewableEnergy


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to answer some questions
that have already started to emerge
about the world's EMT idea.
For those who are overseas, I'm sorry.
EMT is an emergency medical technician, a medic.
Their job is to keep you afloat,
to keep you moving until they can get you somewhere
with infrastructure.
It's a good analogy.
And in the United States,
we've had that world's policeman thing for a very long time.
So it's a turn of phrase.
So I've taken the questions
and kind of broken them down into groups that are similar.
Today, we're going to talk about some economic questions
that came in.
And both of them, interestingly enough,
they are questions because people widely believe
something that isn't true.
So the first question is related to the idea
that if the standard of living in other countries goes up,
by necessity, the standard of living in the United States
has to go down.
This comes from two different points.
This misconception comes from two different points.
The first is a lot of nationalists,
people who want you to view those outside the United States
as lesser, as other, as somebody who is,
you're in competition with,
they're coming for your cookie
and you better be ready to defend it.
It comes from them.
Okay.
And it treats it as though they're tied.
Now, I'm not an economist.
So when it comes to questions like this,
I have to ask other people.
And this is a concept that I'd heard a lot about
over the years.
And I never really got a satisfactory answer,
a way that somebody who wasn't up on world economics
could really grasp until the Trump administration.
And then somebody kind of broke it down for me
and put it in crayons in a way that I could understand.
So I'm gonna give you all that.
This isn't mine.
The gross domestic product of a country
is like the big number.
That's the number that really matters.
And it's the total value of all goods and services
that get exchanged or produced or whatever
within the country or within a set geographic area
over a set period of time.
Okay.
If it was true that the economic output was linked
to the GDP, there's only so much to go around.
Anytime the US GDP went up,
another country's would go down.
That's not what happens.
It's not even close to what happens.
Even in the incredibly exploitive system
that we operate under today,
the trading partners of the United States,
their GDP tends to go up.
It's not true.
That idea isn't true coming from this standpoint.
Now the other place where this gets reinforced
is the idea that there is only a certain amount
of economy to go around.
There's only so much that can be done.
And this comes from environmentalists.
And it comes from them because they're rightly concerned
with environmental degradation.
And over the years, economic growth
and environmental degradation have kind of gone together
a whole lot.
So it stands that they are viewed as having to go together.
But that's not true.
It's not.
And the funny thing about it is that environmentalists
know this on some level,
but they still equate resource exploitation
with economic growth.
But how is the Green New Deal marketed?
It's gonna help with climate change, right?
But how is it sold?
What do people say?
What else is it going to do?
It's a jobs program.
It's gonna generate a lot of economic activity.
We live in a world where everything is commodified.
Everything.
If they could figure out a way to charge you to breathe,
they would.
Everything's commodified.
That means that everything makes money for somebody,
including things that aren't bad for the environment.
This idea that the standards of living have to balance out
and that there's only so much to go around, it's not true.
It is not true.
Okay, the next question is,
well, there's no money in peace.
Also not true.
Go back to that statement before.
We live in a world where everything is commodified.
Everything makes money.
If you don't believe that being a humanitarian
and engaging in relief produces a lot of money,
yeah, take a look at the Red Cross.
Look at that budget.
But beyond that, it goes a different way.
When people say that, what they're saying is
that multibillion-dollar companies aren't gonna make a lot
of money off of peace.
That's not true.
It's just not the companies that make a lot of money off a war.
Different kinds.
Who would make money off of helping build infrastructure?
Energy companies, like new energy companies,
like solar panels, hydro, that kind of stuff.
Windmills.
Sorry, wind turbines.
They would make a lot, because in countries
that don't have a lot of infrastructure,
they don't have an entrenched industry related
to fossil fuels that's gonna require them
to stick to that method.
So they'll be able to go there.
To put it into a real clear, easy picture here
in the United States,
let's say the United States decided to do something good,
altruistic.
Every kid in the country is going to get breakfast
if they want it, right?
The United States government is gonna pay
for breakfast food for kids.
General Mills and Eggo are gonna make a dump truck full of cash.
It's just not defense contractors
that are gonna make a lot of money.
It would go to companies that provide medical infrastructure,
but in short, it's going to primarily go to companies
that hill rather than kill.
I think that's a net win.
There are a bunch of other questions,
and we're gonna go through them all
before we even restart and finish off
the original parts to the series.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}