---
title: Let's talk about Biden, mandates, reactions, and Orwell....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=qu3-fXVMtdk) |
| Published | 2021/09/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring reactions to Biden's mandates and the underlying reasons behind them.
- Three main reactions: support, conflict due to ideological reservations, and calling it Orwellian.
- Majority cheering on Biden's administration for taking action through mandates.
- Some conflicted individuals balancing bodily autonomy beliefs with the necessity of the mandates.
- Others view the mandates as Orwellian and tyrannical, lacking an understanding of the term.
- Media coverage lacks accountability for the role it played in shaping public opinion on vaccination.
- Medical and scientific communities share blame for poor messaging, combating sensationalism.
- Society's susceptibility to emotional manipulation and division contributes to the mandates' controversy.
- Orwellian references not just about dictatorship but manipulation and control of information.
- Criticizes those who deny factual evidence regarding vaccination survival rates.
- Points out the failure to foster critical thinking in society leading to the current division and misinformation.
- Examines the dichotomy between healthcare professionals saving lives and media sensationalizing fear.
- Urges for a society capable of discerning truth from manipulated narratives.

### Quotes

- "The party told you to reject the evidence of your eyes and ears. This was its final and most essential command. That's Orwellian."
- "People are denying the evidence of their eyes and ears."
- "We failed to create a society that can critically think, that can see through fear-mongering media, that can do simple math."

### Oneliner

Beau delves into reactions towards Biden’s mandates, exposing societal vulnerabilities to manipulation and misinformation, urging critical thinking to combat divisive narratives.

### Audience

Public, Voters, Activists

### On-the-ground actions from transcript

- Challenge misinformation spread in your community by engaging in fact-based dialogues and sharing credible sources (implied).
- Foster critical thinking skills by organizing community workshops or educational sessions on media literacy and discerning misinformation (suggested).

### Whats missing in summary

The full transcript provides a detailed breakdown of societal susceptibilities to manipulation and misinformation, advocating for critical thinking and discernment in navigating complex issues.

### Tags

#Biden #Mandates #Vaccination #Orwellian #Misinformation


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about Biden's orders,
the mandates that came out, what they mean.
But more than anything,
we're going to talk about people's reactions to it
and why they exist.
Because generally speaking,
I've been watching since Newsbroke.
I've been watching social media,
and I've been looking at all the different reactions.
And really there's only three.
I think there's three basic reactions to this.
The first, and it is the overwhelming majority of people,
they're cheering it on.
They're like, finally, finally,
the Biden administration is doing something about it.
They are mandating, in a way, vaccination.
Good for them.
And that's a huge number of people.
That is more than 50%.
That's most people right there.
Then you have those who are conflicted,
people who have ideological reservations
about the idea of mandates of this sort
because they believe in bodily autonomy.
They're conflicted because a lot of them are like,
well, I believe in bodily autonomy,
don't think the government should really do stuff like this.
But at the same time, I do understand
that this isn't really wholly contained in the body.
They've hit that intersection of ideology avenue
and reality street.
And if you don't know, that is a very messy intersection.
It's dangerous.
There are car accidents there all the time
because conflicts occur.
And there are people that are conflicted about it.
And believe me, I feel you.
I feel you.
If you don't know, prior to the public health thing ever
starting, I have videos on this topic
when it comes to mandates.
I don't like them.
Now, this is a little different than what
I've laid out when I talked about it before
because this isn't criminal.
It's civil.
And there's ways to opt out.
It's not quite what I describe.
At the same time, it's really close.
It's heading that way.
So I have issues with it.
And then you have the people who are going out there and saying,
well, this is Orwellian, a word I have seen a lot,
saying that Biden has turned into a dictator,
that it is tyranny.
Yeah, I mean, because generally, things that are tyrannical
have a method of opting out.
But it's that word Orwellian that has really kind of got me.
The thing is, when you look at the media coverage of this,
there's something that is just absent.
But it's in all kinds of other coverage.
Any other major topic of the day, there's a story
that goes along with it that they talk about.
In most cases, it gets a week of coverage in and of itself.
Who's to blame for this?
That's not happening in this case.
So we're going to kind of go over it.
As pro-vaccination as I am, I have
to acknowledge the medical and scientific community.
They had bad messaging, horrible messaging.
From the beginning until now, it's been a mess.
It has been a mess.
So they have some of the blame for this.
But you can't pin it all on them, because let's be honest,
they weren't engaging in direct messaging.
They were engaging in counter-messaging.
Because we have an industry that at one time
was theoretically just pursuit of the truth,
and that's how they turned a profit, that has instead
switched to sensationalism, fear-mongering,
and sowing division as their profit model.
And the scientific and medical community
was trying to combat that.
And they share a lot of the blame.
That might be why we don't have a whole lot of coverage
of who's to blame on this, which is a normal facet
of American coverage.
But we're not getting it.
Because there's no way to talk about it
without acknowledging the media's role in it.
So we can definitely blame them a lot.
But there's somebody else to blame.
Society as a whole, because we have created a population that
is so easily manipulated by emotion
that they're falling for it.
That they can be divided that easily.
That they can be convinced to do things against their own
interests that easily.
And they're the ones calling things Orwellian,
which to me is funny.
You know, when people say that, they tend to just mean,
well, it's like dictator-like, you know, it's Orwellian.
That's not what it means.
That is not what it means.
And they reference Orwell.
But it's not really about Orwell.
I mean, let's be honest, nobody says Orwellian
while they're talking about Homage to Catalonia.
And I mean, to be completely frank,
most people who misuse Orwellian probably
have no idea why Homage to Catalonia
is in this conversation.
They're referencing 1984.
And because they didn't read or don't understand the book,
they think it simply means dictatorship.
But that's not what it means.
It references the whole system of being
able to manipulate people, appeal to their emotions,
give them their two minutes of hate via the media, which
is what we get now with that sensationalized fear-mongering
media.
References that.
And it references the way that Big Brother, the government,
the party, controlled the little peons at the bottom.
The party told you to reject the evidence of your eyes and ears.
This was its final and most essential command.
That's Orwellian.
And it's referenced throughout the whole book.
Normally, to show how absurd it is,
it talks about a simple addition.
The party says 2 plus 2 equals 5.
And if you were a good party member,
if you had totally bought in and you were totally co-opted,
and they had already created an obedient lackey out of you,
you would deny simple math, math you could
have learned on Sesame Street.
And that's Orwellian.
I have some math for you.
If you're against vaccination, how many times have you said,
well, you know, it has a 99.99% survival rate?
How many times have you said it?
In the United States, if that was true,
if it had a 99.99% survival rate and we had 100% infection,
the US would have lost about 32,800 people.
We've lost more than that in Florida.
It's not true.
Simple math.
But 2 plus 2 equals 5.
People are denying the evidence of their eyes and ears.
You know, I haven't heard many people say 0.99.
It's normally just 99.9%.
Yeah, that's 320,000.
We've lost twice that.
2 plus 2 equals 5, though, right?
The real secret behind the party's control
is that people didn't know they were being controlled.
People didn't know they had been duped,
because they were genuinely scared.
They rejected the evidence of their eyes and ears.
So many people, the overwhelming majority of people,
are in support of this mandate because the United States
has failed.
We failed to create a society that can critically think,
that can see through fear-mongering media.
It can do simple math.
We failed to create that society.
And now we're paying the price for it.
People are incapable of understanding something
that's incredibly simple.
Right now, you have a whole lot of people
who, in order to believe the fear-mongering media that
is coming out of Fox, Fox News, Fox
is coming out of Fox and like networks.
They have to believe that the health care community, people
who devoted their entire careers to saving lives,
have suddenly stopped.
That's not what they're doing anymore.
They've switched.
They want to put people at risk.
And they believe this because people
whose entire job, their career, is devoted to sensationalism
and fear-mongering and division.
And they can't see that.
They can't see that the most likely scenario is not
that these two groups of people switch sides and switch
to motivations, but that they're doing what they've always done.
And those who have bought in to the right-wing talking points
have been duped.
2 plus 2 equals 5.
99.9%.
It's simple math.
Today's episode has been brought to you by the word Orwellian.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}