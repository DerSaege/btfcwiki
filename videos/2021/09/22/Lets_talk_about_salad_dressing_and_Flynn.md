---
title: Let's talk about salad dressing and Flynn....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KdbY7LC2xLE) |
| Published | 2021/09/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the issue of salads and salad dressing in relation to public health, sparked by a baseless claim made by Michael Flynn.
- Michael Flynn mentioned a conspiracy theory about putting the vaccine in salad dressing, leading Beau to criticize the manipulation of information.
- The theory likely originated from a need to transport mRNA vaccines without deep freezing, but was twisted into misinformation.
- Beau questions the feasibility of targeting Trump supporters with salad, suggesting hamburgers or animal medication as more plausible options if the absurd theory were true.
- He believes that Flynn is intentionally spreading falsehoods to manipulate the most gullible individuals, eroding their trust in reality.
- Beau warns about the long-term consequences of perpetuating such misinformation and calls for media accountability in not promoting sensationalism for ratings.
- He criticizes the current state of the conservative movement in the United States, where manipulation and sensationalism seem to dictate the discourse.
- Beau concludes with a reflection on the need to address these issues and encourages viewers to think critically about the information they receive.

### Quotes

- "These people are seriously thinking about how to impose their will on our society and it has to stop."
- "When you have a group of people who are just intent on making a demographic doubt and fear everything, people who like to fashion themselves as macho tough guys are now going to be scared of salad dressing."
- "We're going to have to come to a reckoning with the way our media operates."
- "There's no end to it. It will just continue to go down further and further as people try to be more and more extreme to gather more of the ratings."
- "This is the state of the conservative movement in the United States today."

### Oneliner

Beau criticizes the spread of baseless vaccine conspiracy theories using salad dressing and warns about the detrimental impact on public trust and media integrity in the conservative movement.

### Audience

Media consumers

### On-the-ground actions from transcript

- Challenge misinformation by fact-checking and holding media outlets accountable for promoting sensationalism (implied)
- Encourage critical thinking and skepticism towards outlandish claims circulating in the media (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how misinformation impacts public health and trust in media, urging viewers to be vigilant and critical of information sources.


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're gonna talk about salads
and salad dressing and public health,
and not because you're supposed to eat salads to be healthy,
but because of something a person who was
very much involved in the Trump administration said,
which means, of course, people are going to kind of believe it,
at least some people, and it will cause issues, I'm sure.
Okay, so what was said, this is from Michael Flynn.
Somebody sent me a thing this morning
where they're talking about putting the vaccine
in salad dressing.
Have you seen this?
I mean, it's, and I'm thinking to myself,
this is bizarro world, right?
This is definitely the bizarro world.
These people are seriously thinking
about how to impose their will on us
our society and it has to stop. That's Michael Flynn. Okay, so more than likely
the origin of this obviously baseless theory comes from an attempt to figure
out how to move the mRNA vaccine without using deep freezing. That's what
it has to do with. That's where it originated and of course from there they
took the ideas and took it into the land of make-believe. But there's a
couple of real issues here. I mean, first we'll run through the feasibility thing.
Let's just be honest for a split second. When you think of a Trump supporter who
views Michael Flynn as an arbiter of truth, do you think salad? Do you picture
a person who's gonna eat a bunch of salad? No. If this was gonna happen this
this would make more sense like using hamburgers or perhaps some medication designed for animals.
That's where it would make sense to do something like this if it was going to happen, but it's not.
The other side to this is that I have to believe that Michael Flynn knows this isn't true,
but it's being floated anyway. It seems to me like this is aimed at the most gullible of the gullible.
Those people that that this crowd that has been manipulating them for so long
knows they'll believe anything and they're putting them in a situation where
they're not going to be able to trust anything around them. They won't be able
to trust reality at all. They have to look to the Michael Flynn's and the
Donald Trump's of the world to tell them whether or not 2 plus 2 really does
equal for. People like to throw that word Orwellian out there. When you have a
group of people who are just intent on making a demographic, doubt and fear
everything, people who like to fashion themselves as macho tough guys are now
going to be scared of salad dressing.
When we continue to just platform people like this and not push back and not say, no, this
is ridiculous, either you need help or you're intentionally manipulating people whose trust
you have for some reason.
We're going to face long-term impacts from this.
You have a group of people who are now conditioned to reject reality, to expect a threat from
everywhere to include the produce aisle.
We're going to have to come to a reckoning with the way our media operates, and the reality
is that we cannot continue to just platform people based on sensationalism for ratings.
We can't continue to do this.
This is a downward spiral.
There's no end to it.
It will just continue to go down further and further as people try to be more and more
extreme to gather more of the ratings.
the process they're helping people who are either unwell or are intentionally
manipulating a gullible population whose trust they've gained for whatever
reason. This is the state of discussion in the conservative movement in the
United States today.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}