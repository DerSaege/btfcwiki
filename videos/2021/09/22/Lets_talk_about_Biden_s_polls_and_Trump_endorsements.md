---
title: Let's talk about Biden's polls and Trump endorsements....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=KEIOdImt-Z4) |
| Published | 2021/09/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's poll numbers have slipped below 50% approval, hitting around 48% for the first time.
- Right-wing pundits see this as the end of the Biden administration, but Biden's lowest poll numbers are still one point above Trump's highest.
- Trump remains an energizing force within Republican circles, with many candidates needing his endorsement to win a primary.
- However, Trump's most significant impact is negative voter turnout, similar to Hillary Clinton's effect.
- People showed up to vote against Trump rather than specifically for Biden.
- While a Republican candidate may need Trump's endorsement to win a primary, they might struggle in a general election due to the negative aspects associated with Trump.
- The belief in the Republican echo chamber is that Trump is widely supported, but in reality, the majority of Americans never backed him.
- Biden falling slightly below Trump's highest approval rating does not necessarily indicate a significant shift.
- Trump loyalists being more vocal may inadvertently swing midterms and the 2024 election towards Democrats.
- Trumpism never gained widespread support or mainstream acceptance among the majority of Americans.

### Quotes

- "People showed up to vote against Trump rather than specifically for Biden."
- "Trumpism never got sold. It never got mainstreamed."
- "Biden falling slightly below Trump's highest approval rating isn't quite the sign that many people may be looking for."

### Oneliner

Biden's poll dip below 50% and the perception of Trump's influence may not play out as anticipated, impacting future elections.

### Audience

Political analysts

### On-the-ground actions from transcript

- Analyze and understand the dynamics of voter turnout and candidate endorsements (implied)

### Whats missing in summary

Insights on the potential implications of voter sentiments and party dynamics in upcoming elections.

### Tags

#Biden #Trump #PollNumbers #Midterms #Trumpism


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about Biden's poll numbers
and Trump's activities and what that may mean
for the midterms and the 2024 race.
The big news is that Biden's poll numbers have slipped.
They are below 50% approval for like kind of the first time.
He hit 48, and that seems to be the general consensus
right now, it's 48%.
The right wing pundits are pushing that as a sign
that it's, well, just the end of the Biden administration.
The reality is that Biden's lowest poll numbers
are one point below Trump's highest.
Now, that's the reality, but the perception
within the Republican circles is that Trump is still
this energizing force, and he is to some degree, that's true.
There are many candidates who won't be able to win a primary
without Trump's endorsement.
But one of the most energizing impacts that Trump has
is negative voter turnout.
He's like Hillary Clinton.
People didn't show up to vote for Biden,
they showed up to vote against Trump.
So while a Republican candidate may not
be able to win a primary without Trump's endorsement,
they're probably not going to be able to win a general
if they haven't, because it makes it very easy for Democrats
to run ads showing the sixth, showing all of his statements
related to the public health issue,
his less than accurate statements about the election,
stuff like that.
The belief in that echo chamber is that Trump
is a driving force, that the majority of Americans
support him.
The reality is the majority of Americans never supported him.
He never broke 50% approval rating.
Biden falling a little bit below Trump's highest approval
rating ever isn't quite the sign that many people
may be looking for.
It may end up being that it encourages Trump loyalists
to be more vocal in their support, which
may swing midterms and the 2024 election to Democrats.
If you are somebody who is looking at this in just
the term of ideas rather than people, the idea of Trumpism,
it never got sold.
It never got mainstreamed.
People never really supported it, a majority of Americans.
Going back to it and calling it as it's a victory
and we're just going to win no matter what because less than 50%
of people support Biden, I don't know
that that's going to play out in reality.
It's probably going to have the exact opposite effect of what
Trump loyalists are hoping.
It may swing even more votes to Democrats
because he's going to be more energized and more
vocal and more visible, which makes it easier for Democrats
to tie into that negative voter turnout that he generates.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}