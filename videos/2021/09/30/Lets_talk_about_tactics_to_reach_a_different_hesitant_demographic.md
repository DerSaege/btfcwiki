---
title: Let's talk about tactics to reach a different hesitant demographic....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tUjTh1RDjqY) |
| Published | 2021/09/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing a demographic that is spiritually inclined and vaccine hesitant or resistant.
- Tactic: Ask hesitant individuals to specify who benefits from the conspiracy theories they believe in.
- Point out the unreliability of intuition by referencing past mistakes.
- Dispel the belief that vitamins alone can protect against COVID-19 by directing them to reliable sources like the Mayo Clinic.
- Dealing with fatalistic beliefs about fate and predetermined decisions.
- Use specific examples to counter misinformation spread by banned individuals.
- Explain the purpose of masks as a way to protect those around you, not just yourself.
- Appeal to the spiritual beliefs of individuals by relating mask-wearing to preventing unintentional harm.

### Quotes

- "Those who are hesitant are the real critical thinkers, and those pro-vaccine aren't."
- "Your intuition is in fact telling you to get vaccinated."
- "It's making sure that you don't cause harm unintentionally."

### Oneliner

Beau addresses vaccine hesitancy in spiritually inclined individuals, offering tactics to overcome resistance and misinformation effectively.

### Audience

Vaccine advocates and community influencers.

### On-the-ground actions from transcript

- Direct hesitant individuals to reputable sources like the Mayo Clinic for accurate information (suggested).
- Use specific examples to counter misinformation spread by influential figures (implied).
- Educate individuals on the purpose of mask-wearing to protect those around them (suggested).

### Whats missing in summary

The full transcript dives deep into strategies for engaging with vaccine-hesitant individuals from a spiritual demographic, providing insights and tactics to bridge the gap effectively.

### Tags

#VaccineHesitancy #Misinformation #CommunityEngagement #PublicHealth #COVID19


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about reaching out
to a different demographic that is hesitant
or in some cases, absolutely resistant.
Normally when we talk about it,
we're really talking about the MAGA loyal,
the Trump supporters who just have bought
into that particular information silo,
they're not coming out of it,
they don't wanna come out of it.
And that is where most of the effort gets expended.
But somebody sent me a message
asking about a very different demographic.
Those people who are more spiritually inclined
in a different way,
those people who are kind of hippie-ish in a way,
who are still vaccine hesitant or resistant, okay?
and they sent a bunch of arguments. They get used by this group of people and
they're having trouble overcoming them. So we're gonna go through them and see
if we can't come up with some tactics to hopefully reach people. Okay, so the
first one is that those who are hesitant, those are the real critical thinkers and
those that are pro-vaccine aren't. This generally comes from an anti-establishment
contrarian attitude which believe me I get I understand it. The easiest way I
have found to reach out to people like this is ask them to describe what they
think is happening but they can't collectivize. They can't use the word
they. Well what they want is for us to do this. They can't use they. They have
to be specific about who the powers are, because in many cases they're assigning a motive to
somebody they can't even identify.
So no big pharma, no anything like that.
They have to be specific and they have to actually be able to say who's benefiting from
this conspiracy that they lay out.
And it forces them to address the fact that in many ways it's just a feeling they have
because they have adopted a contrarian anti-establishment attitude that has shifted into the conspiratorial.
So that's a tactic I have found effective for this particular type of argument.
Let's see.
Checking is unreliable because their intuition is all they really need.
That's easy.
Point to their life, point to the mistakes that they have made in their own life, and
ask why their intuition didn't protect them from it.
Now they'll probably say, well, I didn't listen to it.
Are you really listening to it now?
This is one of those things where you start getting into, even though they would never
admit that it's faith-based, that's what you're getting into now.
So you're not going to be able to break that down with logic.
You're going to have to play within the bounds that they've created.
So use that idea of intuition and show how it's not always accurate, or perhaps maybe
you're just misreading it.
Maybe your intuition is telling you that you need to do this, but you're not ready for
it for whatever reason.
Your intuition is in fact telling you to get vaccinated.
That's why you've spent so much time researching it and go that route.
All they need are vitamins.
Yeah, that's just, that's not true.
That isn't true.
Send them to the Mayo Clinic.
They have a whole thing about it.
There are a lot of people who look to vitamins and supplements as a method of boosting their
immune system.
And it might, but not enough to matter.
That's really what comes across through Mayo.
So I would just send them there.
It doesn't matter what you decide now because the decision was made on whether or not to
get the vaccine before you were even born. It's very fatalistic. These are people who believe in
fate. Honestly, those people make me very nervous just from a different environment. Those who
believe in fate, those who believe that their actions are predetermined, those tend to be people
who are capable of the greatest harm. Those who truly believe in fate and are
using that as a crutch to engage in a socially irresponsible behavior or
something that may put others at risk, honestly, if it was me, I would distance
myself from them. Honest answer. One of their, I guess, thought leaders was banned
on Facebook for spreading theories and they're really upset about this. I've
used this to actually get through to people. Get the specific posts that they
had that led to them being banned and go to those posts and find the actual
misinformation and show them where it's wrong. Pull it out. This is what Facebook
caught, this is why it's wrong, this is why they were banned, this is why you
maybe shouldn't take this person at face value. And this is not just a good tool
for getting through on that point, it erodes trust in whatever that figure
is, whoever that person is that's putting out that information or that organization.
That's the, it's an inroad to actually getting through. So don't view that one
as a bad one. Take advantage of it. Find the specific posts or find their past
posts where you can point to and say this was wrong and it's demonstrably
wrong and here's the evidence. Because at its core the reason they would be upset
that somebody was banned for spreading this kind of stuff is because they
believe it to be true, show that it's not. And the last one is that masks, well
they're useless, they don't actually protect you. And I've struggled with
this because it's really hard to explain to people who are of this mindset that
the mask isn't for you, it's to protect those around you. And then of all
people my kid was like oh it's like covering your your mouth when you sneeze
or cough yeah we know it works we know it works to protect those around us it's
a custom it's a tradition we have done it for a very very long time this is
just a method of making sure that it happens related to that because I would
imagine that they probably do cover their mouth and nose when they sneeze
or cough. And when you are talking about spiritual people like this, you can lean
into the whole idea of, you know, you don't want to harm a bug. You don't eat
meat because, you know, it hurts, you know, this animal. You don't want to do these
things because you want to have good karma or however, whatever that particular belief
system is, they don't want to cause harm.
Wearing a mask is just like sweeping the floor in front of you.
It's making sure that you don't cause harm unintentionally.
those counter arguments can help get through because this is a demographic of people that
kind of gets left out because most people see them as those who would take these steps.
Some of them have spiritual reasons that you'll never overcome.
They're just there and you're not going to be able to get through.
And there has to come a point where you're like, well, it's not going to happen.
You can't continue to expend your effort.
But before you reach that point of just throwing your hands up
in the air and giving up, try these methods.
Anyway, it's just a thought.
You have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}