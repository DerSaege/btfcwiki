---
title: Let's talk about water in the southwest US....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=G048s_OF3VU) |
| Published | 2021/09/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about possible water shortages in the Southwestern United States and how it might impact Lake Powell and Mead.
- Projects a 22% chance of Lake Mead dropping below 1,025 feet by 2023, affecting water usage for 25 million people.
- Mentions a 66% chance of Lake Mead dropping below 1025 feet by 2025, leading to required water usage reductions.
- States a 3% chance of Lake Powell not being able to generate hydroelectric power next year and a 34% chance the following year.
- Points out that these projections are based on historic drought levels in paleoclimate records, indicating severe drought conditions.
- Emphasizes the need for immediate action to mitigate climate change and address impending resource issues.
- Suggests that without action, water scarcity may lead to the costly trucking in of water, exacerbating climate issues.
- Urges the initiation of infrastructure projects now to prevent severe impacts in the future.
- Expresses concern over the lack of media attention given to such projections, despite their historical accuracy.
- Stresses the importance of taking significant and timely action to address the ongoing challenges rather than waiting for a return to normalcy.

### Quotes

- "This isn't going to fix itself."
- "Maybe we should just go ahead and start working on the infrastructure to fix these issues now."
- "We need significant action and we need it soon."

### Oneliner

Beau warns of impending water shortages in the Southwest, urging immediate action to prevent severe impacts and mitigate climate issues.

### Audience

Residents of the Southwestern United States

### On-the-ground actions from transcript

- Start working on infrastructure projects now to address water scarcity (suggested)
- Take significant action immediately to mitigate climate change impacts (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the projected water shortages in Lake Powell and Mead, outlining the urgency for proactive measures to combat climate change and prevent severe consequences.

### Tags

#WaterShortages #ClimateChange #SouthwestUSA #Infrastructure #ResourceManagement


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about water
and possible water shortages
and how it might impact parts
of the Southwestern United States.
Because there are some new projections related
to Lake Powell and Mead.
In Mead, you're looking at a 22% chance
of it dropping below 1,025 feet by 2023.
a 66% chance of it dropping below that by 2025.
That will result in a required reduction of water usage for about 25 million people.
It's not a small thing.
It's not something that can be overlooked.
Now, over at Powell, you have a 3% chance of it not being able to generate hydroelectric
next year, and a 34% chance of it not being able to generate hydroelectric the next year.
That will impact about six million homes and businesses.
2020 and 2021 have been historic droughts, and by that they're saying it's been the most severe
drought levels in paleoclimate or historic records. This isn't something
that's just going to fix itself, it's not going to go away on its own. We might
take this as a sign that action needs to be taken now, that things need to be done
now to mitigate climate change and deal with some of the coming resource issues
that are certain to come. This isn't going to fix itself. If nothing is done,
what is likely is people trucking in water, which is going to make it more
expensive, and it's also going to contribute more to climate issues, which
will require more water to be trucked in. Maybe we should just go ahead and start
working on the infrastructure to fix these issues now and try to mitigate the
the impacts before they get too severe and we are running out of time. These
numbers came out and I don't know normally when stuff like this comes out
the projections come out they don't get a lot of play in the media they don't
get the attention they should because it's a year out it's two years out and
anything can happen. Not really, not really. These are these are pretty
accurate historically speaking. They do a pretty good job and while sure this
drought may end at some point in the future, that's not going to solve the
problem. It's not going to immediately return things to normal. The odds are
we're never going back to what we saw as normal. We need significant action and
and we need it soon.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}