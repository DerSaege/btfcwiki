---
title: Let's talk about Arizona and Brazil....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ZgrDN37kbPU) |
| Published | 2021/09/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the Arizona audit results, stating that the initial theory behind the audit did not hold up to scrutiny.
- He explains that the audit report indicates that Biden not only won but by a larger margin due to counting errors, which Beau finds unsurprising.
- Beau draws a comparison to someone discovering the earth is round after believing it to be flat – indicating the lack of surprise regarding the audit results.
- Shifting focus to Brazil, Beau talks about a theory implicating Jason Miller, a former Trump campaign official, in organizing the influx of Haitians at the southern border to make Biden look bad.
- Beau analyzes the motive behind the theory involving Jason Miller, finding it somewhat plausible given his association with Trump and border security concerns.
- He questions the feasibility of the theory, acknowledging that Miller's former position could lend credibility to such an operation.
- Beau points out the lack of substantial evidence supporting the theory, noting that Miller's presence in Brazil is not sufficient proof of involvement.
- While acknowledging the possibility of political stunts by certain groups, Beau stresses the importance of concrete evidence to lend credence to such theories.
- He underscores the need for substantial evidence beyond mere travel records to support claims and elevate them beyond rumors.
- Beau concludes by suggesting that until more concrete evidence emerges, the theory surrounding Jason Miller's involvement remains merely an entertaining thought exercise.

### Quotes

- "This is like finding out that somebody who believed the earth was flat ran some tests and realized it was round."
- "But without evidence, it's a rumor."
- "When we're looking at these theories, we have to apply the same standard across the board."

### Oneliner

Beau addresses the Arizona audit results, finds them unsurprising, and analyzes a theory involving Jason Miller in organizing the influx of Haitians at the southern border, stressing the need for concrete evidence.

### Audience

Analytical viewers

### On-the-ground actions from transcript

- Investigate and gather concrete evidence to support claims of alleged involvement in political activities (suggested).

### Whats missing in summary

The full transcript provides detailed analysis of theories surrounding the Arizona audit and Jason Miller's alleged involvement, offering a nuanced perspective on the importance of evidence in evaluating such claims.

### Tags

#ArizonaAudit #JasonMiller #ConspiracyTheories #PoliticalAnalysis #EvidenceEvaluation


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about Arizona and Brazil and testing out theories.
I had somebody ask me if I was going to do a video on the results of the quote audit
in Arizona and I hadn't planned on it to be honest.
We talked about it when it first started.
It didn't make it through the tests, you know, the claim that the audit was investigating.
It didn't make it through the, just the normal general, before you even look into it, run
it through this kind of test.
The motive, you know, the reason behind the theory, it didn't really make a whole lot
of sense.
Then you go to the feasibility side of it.
Could this actually be done?
And that really didn't add up, so we never really even dove deep into the proclaimed
evidence because there really wasn't any.
So the fact that the rough draft of the audit report says not only that Biden won, but that
the errors in the counting showed that he won by even a larger margin, it's not really
a surprise and to me not really a story.
This is like finding out that somebody who believed the earth was flat ran some tests
and realized it was round.
Not a huge surprise.
Didn't put a lot of faith in the audit to begin with.
So I don't really see the point in celebrating an audit that I didn't put much stock in to
begin with.
But that brings us to Brazil because right now there is a theory that is being widely
floated that a former Trump campaign official by the name of Jason Miller is somehow responsible
for the Haitians at the southern border.
Now the theory suggests that he went to Brazil and organized the influx in an effort to make
Biden look bad.
Okay.
So let's run it through that same test.
The motive thing.
Does it make sense?
I mean, kind of, sure.
I mean, it kind of makes sense.
A hanger-on of Trump wants to make his boss look good.
This is a way to do it.
It makes Biden look bad.
Border security was a cornerstone of the Trump misadministration.
So sure, that part makes sense.
Then you go to feasibility.
Could somebody go down there and kind of organize it and make it look organic?
I mean, yeah.
Yeah, that could also happen.
It wouldn't even really be that hard because you have that fame, that perceived officialdom
that comes with his former position.
So it wouldn't be difficult to do it.
So now we have to go to the evidence.
What is the evidence to support this theory?
Jason Miller was in Brazil.
As far as I can tell, that is the extent of the evidence.
That's not evidence.
That's not evidence.
Is it possible that this occurred?
Sure, sure.
This crowd, they are known for staging political stunts and for political dirty tricks, sure.
But without evidence, it's a rumor.
It's a wild theory, really not that much different than those from the other side of the aisle.
If this is something that people want to pursue, they have to look beyond simple travel records.
You've got to show me that he was somehow involved with somebody who made the posts
on WhatsApp, that there was some kind of funding.
You've got to show me something in order for me to believe it or to give it any credence
whatsoever.
But just the fact that he was in Brazil a little bit before it happened and a lot of
the Haitians originated from Brazil, that's not enough for me.
When we're looking at these theories, we have to apply the same standard across the board.
Less more evidence comes up, this is an entertaining thought exercise, but nothing more.
It's kind of just a thought.
Anyway, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}