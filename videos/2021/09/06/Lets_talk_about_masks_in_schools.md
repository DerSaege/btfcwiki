---
title: Let's talk about masks in schools....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=x8zXrFIkF44) |
| Published | 2021/09/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the national debate on masks in schools and other clothing articles.
- Explains the lack of a mask mandate in schools is due to the difficulty in proving where a student contracted the virus.
- Argues that schools have the authority to mandate masks for safety reasons, citing examples of dress codes for non-safety issues.
- Questions why some parents oppose mask mandates, attributing it to their willingness to risk students' and staff's health to make a political statement.
- Criticizes the lack of education and understanding among those opposing mask mandates, particularly regarding misinformation about airlines banning masks.
- Concludes that masks work, and the main debate should be about which types are most effective.
- Mentions a charity donation offer related to tying bra straps to masks in schools.

### Quotes

- "Masks work. Period. Full stop."
- "It's not about education. It's about a willingness to risk others, to prove a political point."
- "The problem originates with stuff coming out of people's mouth and noses. The solution is to cover it up."
- "I don't believe they care about education at all."
- "Y'all have a good day."

### Oneliner

Beau lays out the truth about masks in schools, pointing out the authority schools have and criticizing those willing to risk health for political reasons.

### Audience

Parents, educators, students

### On-the-ground actions from transcript

- Support mask mandates in schools by engaging with school boards and advocating for student and staff safety (exemplified)
- Educate yourself and others about the effectiveness of different types of masks to combat misinformation (exemplified)
- Donate to charities supporting causes like ShelterHouseNWFL.org to contribute to community well-being (exemplified)

### Whats missing in summary

The full transcript dives deep into the rationale behind mask mandates in schools, debunking misinformation and urging for responsible actions to protect students and staff.

### Tags

#MaskMandates #SchoolSafety #ParentalResponsibility #CommunityHealth #Misinformation


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about masks in schools
and any other articles of clothing that may pull up,
push up, or pop up along the way.
We're gonna do this because this topic
has somehow re-entered the national discussion
for some reason.
and some of the people involved in the conversation
have become, well, downright frisky
while expressing their opinion.
And it is just that, an opinion.
So today I'm going to lay out some cold hard truths
about this as I see it.
And the first thing we're gonna start off with
is acknowledging that the only reason
there isn't a mask mandate at every school
this country is because it's too hard to show with any degree of certainty that a
student picked up the virus at the school. If it could be shown with any
degree of certainty that a student was infected at the school, we'd have mask
mandates everywhere. But because we can't, they're kind of immune from the
liability. Because if little Jimmy picked it up at the school and then went home
and took out grandma, I'd be willing to bet that a half-decent lawyer could make
the case that the school was being negligent and derelict in their duty for
not requiring masks. So that's the reality. Now there are people making the
argument that schools don't have the power to do this. That's ridiculous. They
absolutely do. To say otherwise is just silly and denying everything that is
reality-based. I went to school in Florida, went to high school in Florida. I
couldn't wear flip-flops in Florida because it was a safety issue. I might
stub my toe. I'm fairly certain that our current public health issue that has
taking out more than 600,000 people is probably a little bit more severe than a
stubbed toe. The school absolutely has the power to do this for safety reasons.
Beyond that, schools regulate dress all the time for things that aren't safety
reasons. They regulate it simply because somebody got their feelings hurt or
Somebody doesn't like someone else's fashion sense.
Pull up your pants, boy.
Right?
School boards across the country perceived that as some
kind of problem rather than an attack on a
certain subculture.
So what was the solution?
Shorts under pants.
That's super offensive somehow.
So cover it up.
Cover it up.
That's the solution.
That's what the school boards made them do.
can absolutely regulate dress. What about bra straps? School boards across the
country got it in their mind that that little itty-bitty piece of fabric that's
showing is the reason that little Jimmy is distracted, not the habit of
objectifying women. So what was the solution? Cover it up. The ultimate haha
moment in this is when you realize that in this case, that really is the solution.
The problem originates with stuff coming out of people's mouth and noses. The
solution is to cover it up. They absolutely have the power. I would say
they have the duty to do it as well. I would suggest that not protecting the
students in this way is negligent. That's the way I see it. But they can't
really be held accountable. So, from here we have to kind of ask ourselves, well, why
isn't it happening? Because a whole bunch of parents walk in and scream, it's not
your job to make health decisions for my child. It's your job to educate. See, I
don't believe that. I don't believe that's a good faith argument on their
because I don't believe they care about education. If they did, they would have
educated themselves on mask usage. If you want your child to be educated, you
yourself have to be educated. You can't tell a child anything, you have to show
them. So if you believed in education, you would certainly educate yourself before
walking into a school board meeting and yelling and screaming and ranting and
raving. I don't believe it has to do with education. I believe it is willing to
risk students, a willingness to risk students, teachers, support staff, and
anybody they come in contact with to own the libs. I think that's really what it's
about. I don't believe it's about health decisions or education. Something that
drove me wild over this weekend was watching the anti-mask crowd say that
airlines had banned masks because they don't work. And sure enough it didn't
take me long to figure out why they were saying this and why these memes
developed. It's because we live in a clickbait world and a whole bunch of
headlines said that airlines banned mask usage. Now the reality is that they're
They're banning cloth masks in favor of N95s and the like.
It's not an anti-mask move, it's a pro-mask move.
They're eliminating the least effective forms of masks and requiring the most effective
forms.
But because they don't care about education, they never actually clicked on the article.
They just read the headline, suffered a little bit of confirmation bias, and created memes
with it.
I don't believe they care about education at all.
Schools throughout history have held it within their power
to regulate dress, to provide for student safety.
It's not even a question of whether or not
they have the power to do it.
It's really more of a question of whether or not they're obligated to do it.
I think a lot of people believe they are, and I'm pretty sure that a half-decent lawyer
could make the case that it is, in fact, the school's responsibility to protect them and
to mitigate in any way they can while they're there.
But we just can't prove that that's where they were infected, so they're off the hook.
distracted Jimmy over a bra strap. Well, that gets immediate attention. I wonder
how distracted Jimmy's gonna be if he's carrying the knowledge that he picked
something up at that school, took it home, and took out grandma. Pretty sure that
would impact his studies a little bit more than a visible bra strap. I don't
believe it's about education. It's about a willingness to risk others, to prove a
political point, and couching it in the idea that there is some debate over this.
There's not. Masks work. Period. Full stop. The only discussion is over which masks
work the best. So it's ShelterHouseNWFL.org. ShelterHouseNWFL.org. If you didn't see the
exchange on Twitter, somebody just basically said they would donate $200 to the charity
of my choice if I could find a way to tie bra straps to masks in schools. So here's
the video. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}