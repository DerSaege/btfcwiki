---
title: Let's talk about patriotic education, dogs,  and civic mindedness....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=flqqTXqdojE) |
| Published | 2021/09/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the connection between patriotic education and civic responsibility.
- Questions the opposition to patriotic education.
- Argues that patriotic education aims to create obedience through conditioning, not civic-mindedness.
- Points out the lack of encouragement for basic community care practices by politicians advocating for patriotic education.
- Criticizes the focus of patriotic education on indoctrination rather than true civic education.
- Emphasizes that civic-minded individuals are harder to manipulate by politicians.
- Calls out the ulterior motives behind pushing for patriotic education, linking it to manipulation and control.
- Expresses skepticism towards the genuine intentions of those advocating for patriotic education.
- Concludes with a critical reflection on the true objectives behind patriotic education.

### Quotes

- "The goal isn't to create civic minded people. The goal is to create Pavlov's dogs."
- "They want to indoctrinate students. They want to create students that are easily manipulated."
- "They want, for lack of a better word, less educated people."
- "They don't care about education. They want indoctrination."
- "Y'all have a good day."

### Oneliner

Beau questions the true motives behind patriotic education, arguing that it aims for obedience through conditioning, not civic-mindedness, criticizing its indoctrination focus and lack of genuine civic education.

### Audience

Educators, activists, concerned citizens

### On-the-ground actions from transcript

- Question the motives behind proposed educational reforms (implied)
- Advocate for comprehensive civic education in schools (implied)

### Whats missing in summary

The full transcript provides a deeper insight into the dangers of using patriotic education for indoctrination rather than genuine civic education.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about patriotic education, civic responsibility, civic mindedness,
and how you create civic minded people.
Because somebody said, your entire channel is basically one giant civics course.
From the idea that you don't need a law to be a good person, to if you have the means
you have the responsibility to act.
It's just a giant civics course.
So don't you think that patriotic education would create more civic minded people?
I don't understand why you oppose patriotic education.
The assumption here is that the patriotism that people feel towards their community is
the same kind of patriotism that would be taught in these classes, and that the goal
is to create civic minded people.
That's not the goal.
That's not the goal.
It's not the desire.
It won't be the outcome.
The goal isn't to create civic minded people.
The goal is to create Pavlov's dogs.
That's the goal.
The goal is to create obedience by conditioning people, by indoctrinating them.
I can demonstrate pretty clearly that this is the case.
Show me a politician that is pushing for patriotic education who is also pushing for masks in
schools.
You're not going to find many, if any at all.
I'm of the firm belief that you can't tell a kid anything.
You've got to show them.
And if these politicians who are so interested in patriotism and so interested in creating
civic minded people are not encouraging students to wear masks, are not encouraging students
to do the little things they can to take care of their community, I don't believe their
calls to fake patriotism.
And if you look at the curriculums, it's not about civics.
It's about conditioning.
The flag, the pledge, stuff like that.
And that's the bell.
They want to indoctrinate students.
They want to create students that are easily manipulated, that can be duped by any politician
who walks out and waves a flag.
That's what they want.
It is not in most politicians' best interest to have civic minded people because civic
minded people don't require a leader.
People who are actually out there caring for their community, they don't need a politician.
They're harder to manipulate.
And look at those who are pushing it, pushing the patriotic education.
They're the same people who pushed the lies about the election, manipulated their base.
They want, for lack of a better word, less educated people.
They want to start indoctrinating them earlier and make it to where they can convince them
to do whatever they want by simply screaming, it's your patriotic duty.
Look at this flag.
Who pledged allegiance to do this?
They don't want thought.
They don't want thought.
They want indoctrination.
They don't care about education.
If they did, they wouldn't have campaigns trying to get rid of certain subjects that
they find offensive because it injures their delicate sensibilities.
They would want people educated.
If this education, if the patriotic education, was actually about teaching civic duty, the
idea that you should look out for your community, I'd be all for it.
But if you look at those curriculums, none of them have anything to do with being civic-minded.
There's no project in which they help their community.
It's all indoctrination.
And it's written by people who, to me, aren't very patriotic, to be honest.
There are some of these bills, some of the legislation, it doesn't even seem that the
author understands that the Bill of Rights is part of the Constitution.
I can't take them seriously in the sense of them having a genuine desire to want to create
better citizens.
I can take them seriously only because they're trying to indoctrinate children.
And it's a pretty serious matter.
But I do not believe their motives.
I don't believe their stated motives at all.
They want to be able to ring that bell and get those kids salivating for war or whatever
it is they want to push on.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}