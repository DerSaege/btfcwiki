---
title: Let's talk about the media's role in foreign policy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=QTuo6Fs1hIE) |
| Published | 2021/09/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Critiques the lack of examination of the media's role in foreign policy, leading to repeated mistakes.
- Shares about putting out a tweet asking for plans on a difficult situation, but the responses dwindled.
- Points out the media's reluctance to address the intricacies of military operations and planning.
- Explains how the blame game in military decisions starts from the executive branch's strategic objectives.
- Expresses frustration that public opinion, shaped by the mass media, influences strategic plans.
- Delves into how the media prioritizes outrage over policy, attracting more viewers and revenue.
- Mentions the unlikelihood of getting detailed policy explanations in mainstream media due to audience preferences.
- Talks about the unlikelihood of receiving advice from media pundits who failed to provide proper education to the public.
- Mentions the impending shift in U.S. doctrine, the "Biden doctrine," and its challenges in implementation.
- Concludes by cautioning against relying on TV commentators from major networks for insights on complex military strategies.

### Quotes

- "It is in their best interest to blame everybody."
- "It's more lucrative for the media to provoke outrage than to discuss policy."
- "If you're doing root cause analysis, their failure to educate the American populace, their willingness to
promote outrage over fact and policy is a huge part of the reason it went the way it did."

### Oneliner

Beau criticizes the media's prioritization of outrage over policy in shaping public opinion on military decisions, urging skepticism towards mainstream commentators' insights.

### Audience

Policy Analysts, Activists

### On-the-ground actions from transcript

- Educate your community on the importance of understanding policy details and advocating for informed decision-making (exemplified).
- Support independent media sources that prioritize policy analysis over sensationalism (implied).

### Whats missing in summary

The full transcript provides a deeper insight into the complex interplay between media, public opinion, and military decision-making.

### Tags

#Media #ForeignPolicy #MilitaryStrategy #RootCauseAnalysis #PublicOpinion


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about the media's role
in foreign policy and how that's not really
being examined right now.
And because we're not really looking at the media's role
in foreign policy, a lot of these same mistakes
are going to be made again.
In fact, right now, the things that led up to recent events
are already playing out again.
If you don't know, if you don't follow me on Twitter,
I got tired of people just saying, oh, this
is how I would have done it.
So I put out a tweet that was basically like, hey,
send me your plan.
I'll review it for you.
And since then, nobody has said anything like that to me.
It's weird, it's almost like if people sit down
and try to actually develop a plan on their own,
they suddenly realize it's a hasty withdrawal
in the middle of a hostile country
that's being taken over by a non-state actor,
and also organizing a massive airlift,
which, I mean, that's hard to say,
it's even harder to plan, right?
So that quieted down.
But somebody sent me a message,
and it had a list of like,
hey, these people seem to understand
this was really difficult, why isn't this being pointed out on major networks?
The answer is they don't care.
They don't care.
And the reason they don't care is because it's really hard to get people to care about
something when their paycheck demands that they don't.
It is in their best interest to blame everybody.
Because if you want to trace something back to the root, you want to do some real root
cause analysis on this? You'd start at the end, right? You would start with the
Department of Defense. Where do they get their plans from? Well, they get their
strategic objectives from the executive branch, civilian control of the military,
right? So they get to throw out general ideas that they want done. Sometimes
they try to be more specific and they get overruled. Sometimes they say things
that are just flat-out ridiculous and get overruled, like take the oil. So the
executive branch sets the tone for a military operation. Where does the
executive branch get its strategic plans from? They're not military planners. They
don't really know what they're doing. They're doing it based on what? Their
campaign promises and poll numbers. What influences that? Public opinion. What is
public opinion shaped by? Mass media. Journalists and those who like to
pretend to be journalists on TV, they didn't ask the questions that could have
prevented this. They didn't ask them 20 years ago. They didn't ask them 15 years ago.
They didn't ask him 10 years ago. They didn't ask him five years ago.
They didn't ask him when Trump put this deal together.
Right?
So to shirk their share of the blame, they're going to lay it on either the
Biden administration or the Trump administration.
I have been pretty clear that I don't think Trump's deal was a good one.
And I've repeatedly said that it set this whole chain of events into motion.
But if you go back and watch videos from that point in time when he was making the deal,
I'm like, this is a bad deal.
But to be fair, I don't know anybody who's going to get us out of here without it being a mess.
One of the very few times that I would even remotely defend Trump, because it's not fair.
It isn't fair.
It was already messed up by that point.
Getting out of Afghanistan was always going to be a mess, and it was always going to be
a mess because it's more lucrative for the media to provoke outrage than to discuss policy.
You generally do not attract a wide audience by getting into policy details or deep elements
of strategy, stuff like that.
Discussing operational details, that tends to lose your audience.
You make a whole lot more money if you just go out there and act mad all the time.
You get more viewers, therefore you get more advertising revenue.
that's what they did. They played into the outrage and the solution was always
more force, more force. Maybe not more troops but more precise force. Let's do
it from the air. It was always encouraging the outrage to justify
furthering a war effort. And then those who understood the moral implications of
it, you know, they would talk about one element of it and say, okay, these unmanned
aircraft, they're really bad. We should leave. But again, don't get into the
policy details. So they don't educate the American pop populace. So it falls flat
and nothing gets done. So if it were me I would not take advice about how they
would have won the war in Afghanistan from current media pundits when they're
the people who lost the war. They're the people who refused to put any effort
into educating the American populace and explaining that, you know, if we walk out of Iraq after
all of this and that country is even remotely politically aligned with the United States,
it will be the first time in modern history that a mature insurgency was put down through
force.
It normally doesn't work.
But they don't say that because that's not American.
We want to run in there like it's the OK Corral.
There's a shift in doctrine that is on the horizon in the United States.
The Biden doctrine, if you will.
He's talking about it already and we are going to go into depth on it this week.
It has support from people you would view as, you know, crystal loving hippies and support
from deep policy thinkers who are very familiar walking around the halls of the Pentagon.
But it's never happened.
It hasn't happened because it's complicated.
And it's hard to sell complicated concepts to the American people because they want sound
bites.
when everything's summed up in 30 seconds.
It can't be done, but we're going to go over what his rhetoric indicates and talk about
what it would look like if he actually implemented it, and then we're going to talk about whether
or not he's actually going to be able to.
To get back to the original topic here, I would not put much faith in the ability of
TV commentators on major networks to tell you how to win the war in Afghanistan or how they would
have done it when the reality is if you're doing root cause analysis their failure to educate the
American populace their willingness to promote outrage over fact and policy is a huge part of
the reason it went the way it did. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}