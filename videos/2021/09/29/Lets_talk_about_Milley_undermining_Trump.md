---
title: Let's talk about Milley undermining Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PA7r0oJgBh4) |
| Published | 2021/09/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- General Milley's actions have sparked a shift in talking points among the right wing.
- The focus has moved from accusing Milley of treason to undermining former President Trump.
- Critics failed to think critically and blindly followed the initial narrative.
- Even if Milley's actions undermined Trump, it was to prevent a catastrophic war, not for personal gain.
- Suggesting Trump's intentions were to start a war reinforces the need for Milley's actions.
- Moving the goalposts portrays Trump as someone not acting in the best interests of the country.
- Beau encourages accepting evidence, admitting faults, and learning from mistakes.
- Some individuals may realize they were misled into believing every action is a scandal.
- Blindly following those against the country's best interest is misguided.

### Quotes

- "Moving the goalposts portrays Trump as a loose cannon, a bad guy, somebody who did not have the best interest of the United States at heart."
- "The better response when faced with evidence to the contrary is to actually look at it, accept it, accept the correction, admit that you were wrong, and go from there."

### Oneliner

General Milley's actions shift the narrative from treason to undermining Trump, revealing the importance of critical thinking and accepting evidence.

### Audience

Critical Thinkers

### On-the-ground actions from transcript

- Fact-check narratives before blindly following them (implied)
- Encourage critical thinking and accepting evidence (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of the shift in talking points surrounding General Milley's actions and the importance of critical thinking in analyzing political narratives.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about General Milley again,
because the goalposts have moved.
We have new talking points.
After his testimony, it seems that the right wing,
those who always cast themselves as truly understanding the military,
have finally begun to understand that he did not, in fact, commit treason,
that he was just doing his job as statutorily obligated.
So now the talking point has shifted.
The goalposts have moved.
It's no longer that he did something illegal.
It's that his actions undermined the President of the United States at the time,
Donald J. Trump.
This is an example of somebody not being willing to let a talking point go.
They believed something because they were told to believe it.
They didn't put any critical thought into it of their own,
so they just kept parroting it.
He did something wrong. He did something wrong. He did something wrong.
And then when faced with evidence to the contrary, they just shifted.
Well, okay, fine, it wasn't treason. It wasn't even illegal.
But it was bad because he undermined the President of the United States.
Okay, let's pretend that's true for a second.
Let's pretend that General Milley's call to his Chinese counterpart
explaining that the United States was not going to engage in a surprise,
unprovoked, preemptive strike triggering a great war power
that would cost tens of millions of lives,
let's say that that call undermined the President of the United States
in his intentions.
That would mean that the President's intentions
were to stage a surprise preemptive strike against a great power
that would trigger a war costing tens of millions of lives.
I don't know that that's quite the talking point you want to go with.
It would be against U.S. doctrine, U.S. policy,
international law. It would just be all bad.
Now, if you want to suggest that that is what former President Trump
was intending, feel free, but it doesn't make the point you
think it does. It actually reinforces the idea
that Milley should have gone even further in activities to mitigate
the President's ability to harm the United States through his actions.
If General Milley's call undermined the President's intentions,
the President's intentions would have been to start a great power war
so he could retain power in the United States and subvert the Constitution.
That would have been his intentions, and if those were his intentions,
General Milley swore an oath to actually undermine those efforts.
You're going to keep looking for reasons why General Milley's activities were wrong,
and you will keep coming up with nothing.
You can move the goalposts as far as you want.
The further you move them from this point,
the more it cast former President Trump as a loose cannon, a bad guy,
somebody who did not have the best interest of the United States
at heart, or the world in this case.
Probably not where you want to shift the goalposts to.
The better answer, the better response when faced with evidence to the contrary
is to actually look at it, accept it, accept the correction,
admit that you were wrong, and go from there.
Seems unlikely, but I have faith that there are some who will be willing to do that.
Some will realize that they have been duped,
that they have been pushed to the point of believing everything's a scandal,
that believing those people who are acting in the best interest of the United States
the way they always have are somehow the enemy,
because they are now following people who aren't acting in the best interest of the United States.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}