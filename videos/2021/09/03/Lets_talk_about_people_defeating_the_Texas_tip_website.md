---
title: Let's talk about people defeating the Texas tip website....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dgGvcx21evE) |
| Published | 2021/09/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Texas passed a law enabling a website for neighbors to report socially undesirable behaviors.
- People on TikTok and Reddit are encouraging tips to overwhelm the system.
- The tips are not factual but aim to flood the system with information.
- Tips include advice like not copying and pasting, using real locations, and avoiding pro-choice slogans.
- Overwhelming the system with information is a tactic used to render mass surveillance less effective.
- This tactic on TikTok may force a reconsideration of how the tip website operates.
- While it may not change legislation, it could make processing information more difficult for authorities.

### Quotes

- "Overwhelming the system with information is a tactic used to render mass surveillance less effective."
- "This tactic on TikTok may force a reconsideration of how the tip website operates."
- "It's going to make it more difficult to process any information they obtained through this tip website."

### Oneliner

Texas law enables reporting on a website; TikTok teens flood it with fake tips to overwhelm surveillance tactics.

### Audience

Online activists

### On-the-ground actions from transcript

- Encourage others to flood platforms with information (exemplified)
- Provide guidance on how to create overwhelming but fake information (exemplified)

### Whats missing in summary

The full transcript provides a detailed look at how online communities can strategically flood platforms with information to thwart surveillance tactics.

### Tags

#Texas #Surveillance #TikTok #Activism #CommunityPolicing


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're going to talk about the TikTok teens tipping
and ticking off to Texas while triumphantly trolling them
with a little trick on their tip line.
Anyway, if you don't know what I'm talking about,
Texas has passed a law that enables a website.
And the purpose of the website is
to get people to rat out their neighbors
if their neighbors engage in a behavior
that the state deems as socially undesirable.
Yet when you say it like that, all of a sudden
it doesn't sound like small government conservative at all.
It sounds like something even the inhabitants of airstrip one
might raise an eyebrow at.
I mean, there's no way that all of those people that just moved
over from California would ever use a similar platform
go after your rifles, right? People in Texas. There's no way that once this
becomes normalized, it will ever impact you, right? Anyway, so currently this
platform is used to encourage people to rat out their neighbors who have engaged
engaged in family planning that the state decided that it didn't like.
Almost immediately, the people who run the website were like, we're already getting tips.
And then they found out that they're mainly coming from people who have been encouraged
by those on TikTok and Reddit to submit tips of their own.
Some of them were tipping off the site to the governor's family planning.
Obviously, that's not true.
They're just providing information to overwhelm the system, and that's the interesting part
about it.
If you look at the advice that's going out from the people that are promoting this, it's
really smart.
They're like, don't copy and paste.
Don't use the same information over and over again.
Make sure the locations are real.
Don't use slogans that are in favor of choice.
Don't and it gives them all of these tools to make sure that it's going to be really
hard to use a computer to filter out the fake tips.
One of the things about mass surveillance is that it's mass.
One of the easiest ways to defeat it or to render it less than effective is to overwhelm
it with information.
This is something that state actors, you're talking about intelligence agencies, they
know this and they've put this to great use over the years.
To me, it's incredibly interesting to see people on TikTok employ this same tactic.
You provide so much information, it just overwhelms your opposition and it makes it really hard
them to conduct whatever it was they were trying to do. I have a feeling this
is probably going to be pretty successful. If it's sustained, this may be
maybe something that causes them to rethink how they're doing it. I mean it's
not going to change the legislation, but they are definitely going to have to
adapt to it. It's going to make it more difficult to process any information
they obtained through this tip website.
It's an interesting little turn.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}