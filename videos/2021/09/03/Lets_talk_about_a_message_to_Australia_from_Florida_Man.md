---
title: Let's talk about a message to Australia from Florida Man....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NzqstDcb44I) |
| Published | 2021/09/03|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Australians are considering relaxing strict measures put in place during the public health crisis.
- Politicians and commentators in Australia are suggesting adopting a US-style approach.
- Australia has a population of about 25 million, similar to Florida's 21 million.
- Florida has lost 45,000 people to the crisis, while Australia has lost just over a thousand.
- If Australia had a population the size of the US, they might have lost around 13,000 people.
- Beau warns Australia not to follow the United States' lead in handling the crisis.
- He points out the poor handling of the crisis in the US, with people resorting to unusual measures like going to a livestock store for medications.
- Beau urges Australia not to view the US as an example to emulate in managing the crisis.
- He cautions against looking to the US for leadership on handling the crisis, as the situation is not being managed well there.
- Florida and several other states have lost a significant number of people to the crisis, indicating a serious impact.
- Beau stresses that decisions on restrictions and measures are up to Australia but advises against seeking guidance from the US.
- He concludes by encouraging Australians to make their own choices but not to look to the US for leadership on handling the crisis.

### Quotes

- "Do not follow the United States' lead. We don't have a clue what we're doing."
- "We are not an example of anything except what not to do."
- "Do not look to the United States for leadership on this."

### Oneliner

Beau warns Australia against following the US lead in handling the crisis, citing poor management and advises making independent decisions on restrictions.

### Audience

Australian policymakers

### On-the-ground actions from transcript

- Make independent decisions on restrictions and measures (suggested)
- Avoid looking to the United States for leadership on handling the crisis (suggested)

### Whats missing in summary

The full transcript provides a detailed comparison between Australia and Florida's populations, urging caution against emulating the US approach to managing the crisis. It also stresses the importance of making informed decisions based on each country's unique situation. 

### Tags

#Australia #US #CrisisManagement #Policy #PublicHealth


## Transcript
Well howdy there internet people, it's Beau again.
So today, we have a special message from Florida man to the people of Australia.
Because a couple of the Australians who watch this channel have sent messages.
I guess politicians there in Australia want to relax things.
When the public health issue started, Australia enacted some pretty strict measures.
Very strict I guess.
And now there are politicians and commentators there suggesting that they adopt more of a
US style approach.
Okay so to figure out if this is a good idea, we're just going to look at the numbers real
quick.
And I think that might be a pretty persuasive argument.
The population of Australia is about 25 million.
That's roughly the population of Florida.
Australia has a little bit more.
Here in Florida, we have 21 million people.
And if you're in Australia and you don't know, Florida is the Australia of the United States.
Similar populations.
We have lost 45,000 people in Florida alone to this.
Y'all just broke a thousand.
You have more people.
If you were to scale your population up to match the entire population of the United
States, 328 million I think.
So what, 13 times?
Y'all would have lost 13,000 people if you were as big as the US.
We have lost 640,000 from Florida man to Australia.
You want to relax things, whatever.
That's your country.
Do with it as you choose.
But please do not follow the United States' lead.
We don't have a clue what we're doing.
We are handling this very poorly.
We have people going to the livestock store to get meds.
It's not going well here.
We are not an example of anything except what not to do.
Do not try to model yourselves after us.
Do not look to the United States and say, hey, they're doing all right.
No, we are not.
The sound bites that may be filtering over there may be painting a very different picture
of what's going on than the reality.
Florida, the state that I'm in, and quite a few others, I want to say five or six, have
lost one out of every 500 people to this.
Do not model yourselves after us.
What y'all want to do as far as restrictions and stuff like that, that's all up to you.
But please do not look to the United States for leadership on this.
We're not handling it well.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}