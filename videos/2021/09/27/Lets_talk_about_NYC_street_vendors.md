---
title: Let's talk about NYC street vendors....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0Fblp2JHhsg) |
| Published | 2021/09/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Saw sanitation workers in New York City throwing away food from a produce stand, which is common in rural areas.
- Mentioned buying seafood and produce from pickup trucks without permits where he's from.
- People defended the permitting process in NYC for food safety after Beau's comments.
- Referenced a 2011 cantaloupe listeria outbreak to explain the focus on food safety.
- Criticized the notion that vendor permits in NYC ensure food safety, calling it more of a theater.
- Argued that business incentives and competition may be the real reasons behind vendor permits.
- Raised concerns about the complex permitting process affecting access to fresh fruit in food deserts.
- Mentioned non-profits like the street vendor project helping navigate the permitting maze.
- Suggested that the permitting process in NYC may be more about revenue for the city than safety.
- Noted a case in the Bronx where community pushback prevented sanitation workers from trashing a stand.

### Quotes

- "Buying veggies and fruit out of the back of a pickup truck is incredibly common where I'm at."
- "Generally what keeps the food safe is the fact that it's really bad business if your customers get sick."
- "So if it's not about food safety what is it about? What's it really about? Bringing in money."
- "This to me is a major issue when you are talking about an area that doesn't have a lot of fresh fruit."
- "Non-profits have been started to help people navigate the maze."

### Oneliner

Beau questions the true motive behind NYC's food vendor permits, suggesting they prioritize revenue over safety, impacting access to fresh produce.

### Audience

Community members, food advocates

### On-the-ground actions from transcript

- Support non-profits like the street vendor project in aiding people to navigate complex permitting processes (suggested).
- Advocate for simplified and fair food vendor permit regulations to improve access to fresh produce in underserved areas (implied).

### Whats missing in summary

Deeper insights on the impact of complex permitting processes on marginalized communities' access to fresh food. 

### Tags

#FoodSafety #VendorPermits #AccessToFreshProduce #CommunityAdvocacy #FoodDeserts


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
New York City. We're going to do this because I saw something that was just so bizarre to
me. I always poke fun when people come down here or go to rural areas and they see something
that is just incredibly common but doesn't happen in cities and you know they're shocked
by it. This is the opposite. I saw footage of sanitation workers in New York City trashing
a produce stand, throwing the food away. That's just so weird to me. That is abnormal. Buying
veggies and fruit out of the back of a pickup truck is incredibly common where I'm at. I
buy my seafood out of the back of a pickup truck and to my knowledge there's not permitting
involved in any of this on the seller's end. So I just found it odd and I tweeted about
it and made some comments about it and people were just like you know this is the permitting
process they have to do this to ensure food safety. That's where it kept going to and
people kept bringing up cantaloupes and what happened back in 2011 and I don't know if
maybe some politician up there mentioned this as a way of justifying the behavior but when
we need to talk about that for a second because that happened in 2011 if you don't know there
was a listeria outbreak traced back to cantaloupes. 28 states, 147 cases, 33 people did not recover.
Was a big deal. The cantaloupes came from Jensen Farms who had a food safety audit six
days before this happened and scored a 96 out of 100. Now sure the FDA said oh well
we need our auditors to do a better job. Yeah sure okay but the reality is most food safety
stuff like this it's theater. It's theater. It's like taking off your shoes at the airport.
It's not really that effective but it makes everybody feel better you know. I think that's
important to note that generally what keeps the food safe is the fact that it's really
bad business if your customers get sick especially if they don't recover. They're probably not
going to buy from you anymore. So the idea that the vendor permit on the side of the
street in New York has something to do with food safety it's kind of silly to me and I
looked it up and basically they check the cart before you start selling and then to
my knowledge from what I can find they really don't follow up on it very much. So again
it's theater. It's to make people feel better. To boost consumer confidence. Not really that
effective at keeping food safe. And generally speaking if it is something that would fail
a food safety audit when you go up to buy it you'll see it in most cases. So if it's
not about food safety what is it about? What's it really about? Bringing in money. It's about
bringing in money for the city. Or maybe about reducing competition for businesses that are
more established. That's really what this is about. You know it's so surprising to me
to see it occur but then I realize it probably shouldn't be surprising because I've seen
you know lots of movies where shopkeepers in New York get their shops busted up because
they weren't you know paying protection money. I mean kicking up to the boss. They don't
get permission of the ruling authority in the area to sell fruit. This to me is a major
issue when you are talking about an area that doesn't have a lot of fresh fruit. You know
you're talking about a food desert and a permitting process that is apparently so complex. Non-profits
have been started to help people navigate the maze. One that everybody keeps recommending
is the street vendor project. I don't know that there is a moral component to this. I
don't know that there's a safety component to this. It seems like just a way for the
city up there to rake in some money. Now it is worth noting that the people in the Bronx
they had an issue with this occurring as well and they raised such a ruckus that the sanitation
workers could not finish trashing the stand and when they came back later, well, all the
food was gone. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}