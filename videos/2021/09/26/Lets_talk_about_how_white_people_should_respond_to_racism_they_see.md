---
title: Let's talk about how white people should respond to racism they see....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=YEbp549A2oA) |
| Published | 2021/09/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring how white people should respond to racism when they witness it.
- Advises using tactics like pretending not to understand bigoted jokes to make the perpetrator explain the underlying racism.
- Suggests using sarcasm to convey that racist behavior is unacceptable within a group.
- Talks about direct correction either by firmly stating not to talk like that in front of them or educating the person about their racist behavior.
- Mentions the exhausting task of explaining basic concepts of humanity to people repeatedly.
- Recommends stepping in to help mitigate risk if a situation escalates and could turn violent.
- Shares a personal experience of witnessing a racist altercation and the importance of immediate response to racism.
- Emphasizes the importance of taking action immediately to make life more tolerable for those facing racism.

### Quotes

- "Do something to mitigate it, to end it, to redirect it, to diffuse the situation."
- "There's a whole bunch of different tactics, and it depends on the kind of person you are."
- "How do you respond immediately?"
- "The answer is immediately."
- "Y'all have a good day."

### Oneliner

When witnessing racism, white people should respond immediately using tactics like misunderstanding bigoted jokes or direct corrections to make a difference and mitigate harm.

### Audience

White allies

### On-the-ground actions from transcript

- Immediately respond to instances of racism by using tactics like sarcasm, misunderstanding bigoted jokes, or direct corrections to combat it and make spaces more inclusive (implied).

### Whats missing in summary

Personal anecdotes and in-depth examples from the speaker's experiences are missing in the summary but provide a real-world context for the advice given.

### Tags

#Racism #Allyship #ImmediateAction #CommunityResponse #Tolerance


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about the exact opposite
of the question we got last week,
one I didn't have an answer to.
I was asked, how should black people respond
when they encounter racism?
Like, I have no idea what the right answer is there.
Somebody asked how white people should respond
when they see it.
That I have an answer to, a bunch of them.
I can give you a bunch of little tactics that can help.
Normally, when white people encounter racism
aimed at other groups, it is typically passive.
It's a joke.
And I've talked about this before.
If somebody says a bigoted joke in your presence,
the best tactic I have ever seen to deal with it
is pretend like you don't get it.
They say something and you're, huh?
I don't understand.
And you force them to explain the underlying
racism in the joke.
It's probably not going to change the way they think,
but it will stop them from doing that around you.
Maybe it has some further impact to let them know
that it's not socially acceptable to do that.
But at the bare minimum, they'll stop doing it around you.
And in some ways, that's all you can control
is what happens in your presence.
Another one is sarcasm.
I've seen that used a lot, especially
among men who are just talking tough type of stuff.
Somebody says something, oh, oh, man, you're here early.
The meeting's not till Sunday night.
We don't break out the cross or anything till then.
Maybe this isn't the right place for you right now.
And it works.
It works.
It sends that message that this is unacceptable with the group
of people that you are with, that it does not
bring you into the end group.
And a whole lot of racism is based on tribalism.
It's based on wanting to fit in with a group
and other people.
So if you can take that motivation away
by saying, if you behave like this,
you're not in this group, it's a pretty strong motivator.
Now, there's direct correction that's firm, as well.
If you have the ability to do it,
you can just be like, yeah, don't talk like that
in front of me and let that go.
There's also direct correction that
is based in education, because there
There are a lot of people who may honestly not realize how racist they're being because
racism is so prevalent in the United States.
There's a lot of accepted statements that sometimes people just need to be educated
on and it's better for you to do it than to force the subject of their bigotry to handle
and force them to educate it.
has got to be exhausting explaining basic concepts of humanity to people over and over
again.
So take some of that pressure off of them.
Help in that way.
You can step in there and you can help.
And then you have mitigation of risk if things start getting out of hand.
If you actually do encounter the kind of racism that could lead to violence.
You know, one, pull out your camera.
Film it.
or you can use a more direct approach. One of the more shocking ones that
happened in my life is I'm sitting in this restaurant with a friend of mine
and we're there celebrating. We had been celebrating for a couple of hours. I
probably was not as aware of my surroundings as I normally am, but all
of a sudden I saw him looking over my shoulder. I saw his eyebrows go up. I saw
him suck in a bunch of air I watched fight or flight get triggered and he was
fighting and he stands up from the table so I stand up and turn around I don't
know what's going on but when I turn around I see an altercation developing
between the bartender who's black and a white guest so I kind of ease around to
the other side of the guest we know the bartender we've known him for a while
and my friend starts talking to him and within seconds he's like man f this and
he goes off with this incredibly racist rant, we need to go somewhere where our
kind's more accepted. And they start walking towards the door and I start to
follow him thinking he's just getting him to the parking lot and he kind of
waves me off. So I'm like, okay. I go back to apologize to the bartender for what
my friend said and he's like, ah man, that's cool, thank you. But doesn't explain
what happened because from my vantage point all of a sudden my friend just
joined in in this racist ranting that was going on. And it takes about an hour
for him to get back and when he comes back in he explains that when the guest
was pointing, you could see his gun in his waistband and it was just a way to
defuse the situation and get him out of there. The answer to the question, how
should white people respond when they see racism immediately, that's the answer.
There's a whole bunch of different tactics, and it depends on the kind of person you are.
Depends on what you're comfortable doing, depends on the situation that presents itself.
But the answer is immediately.
How do you respond immediately?
Right then.
Do something to mitigate it, to end it, to redirect it, to diffuse the situation.
you can to make life just a little bit more tolerable for the people who are
subjected to this.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}