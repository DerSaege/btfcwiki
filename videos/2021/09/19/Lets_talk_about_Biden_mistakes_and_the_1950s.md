---
title: Let's talk about Biden, mistakes, and the 1950s....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MtxXx4gjYB0) |
| Published | 2021/09/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration cleaning up Trump's messes.
- Mistakes made now by Biden administration will need cleaning up by next presidency.
- United States is not like a compact car but an 18-wheeler, slow to change course.
- Policies take time to show results after being enacted.
- Effects of policies, positive or negative, take time to manifest.
- Conservatives often point to the 1950s as the golden age of the United States.
- The golden age reference actually spans from 1945 to early 1960s.
- FDR's administration from 1933 to 1945 laid the groundwork for the perceived golden age.
- Massive government spending, jobs programs, and high tax rates on the top created prosperity in the past.
- Similar policies may create real change and prosperity now post long war and economic issues.

### Quotes

- "United States is not a compact car that can stop on the dime. It's an 18-wheeler."
- "The effects of policies, positive or negative, generally speaking, take years to be shown."
- "The period conservatives point back to was created by somebody who championed all of the policies they oppose."

### Oneliner

Biden cleans Trump’s messes; Biden's mistakes need cleaning; Policies take time; Conservatives' golden age view; FDR's policies laid groundwork.

### Audience

History enthusiasts, political analysts, policymakers.

### On-the-ground actions from transcript

- Advocate for policies that focus on massive government spending, jobs programs, and high tax rates on the top to create real change (implied).

### Whats missing in summary

The full transcript provides a deep dive into historical policies and their long-term effects, urging for a reflection on current policy directions for future prosperity.

### Tags

#Policy #Government #HistoricalAnalysis #Policies #Prosperity


## Transcript
Well, howdy there, internet people. It's Beau again. So today we are going to talk about
how one president often has to clean up another's mess. I think most people watching this channel
would agree that the Biden administration is spending a whole lot of time cleaning up
Trump's messes, right? And I think most would agree that right now the Biden administration
is making mistakes that the next presidency is going to have to clean up. That's how it
works. And we have no problem admitting that when we are talking about the negative. We
have no problem acknowledging that the United States is not a compact car that can stop
on the dime. It's an 18-wheeler. And when you slam on the brakes, it takes time to slow
down. It takes time to change course. Policies take time to show the results once they're
enacted. So oftentimes you won't see the results of an administration until after they're out
of office. And we have no problem admitting this when we're talking about the negative.
What about the positive? We tend to view that as more immediate. It's not true, though.
It's not true. It works the same way. The effects of policies, positive or negative,
it takes time for them to show up. Let's take Trump as an example. Things that he loved
to take credit for, right? The stock market, that's one. When did that climb start? 2009,
before he ever took office. He loved to take credit for the GDP. When did that climb start?
2008. It's true, positive or negative, generally speaking, that the effects aren't shown for
years. Okay, so with this in mind, if you're talking to conservatives and you ask them
what the golden age of the United States is, what do they say? Almost invariably, they
say the 1950s, right? Now we today, having actually read a little bit more history, we
know that that wasn't actually a golden age and that there were a whole lot of problems.
But that image is based on TV. Leave it to Beaver and Andy Griffith, right? So the problems
just weren't represented. They weren't shown. People could ignore them. But they existed.
But that's the period they point to, the 1950s. And when they say the 1950s, they don't really
mean the 1950s either. They mean 1945 to 1962, the period after World War II, but before
the Cuban Missile Crisis. That's the range of what they're talking about. Because once
the Cuban Missile Crisis and Bay of Pigs and all of that, I mean you may be able to extend
this to 1963, but once all of this happened, the whole threat of constant annihilation
kind of put a damper on the golden age thing. It was that period, 1945 to the early 60s.
Who's responsible for creating that? Would be the president preceding 1945. In this case,
it's really easy, because from 1933 to 1945, it was one person. The prohibition against
serving more than two terms wasn't in place. At that point it was just a tradition, and
this person broke with tradition. FDR. FDR broke with tradition, 1933 to 1945. So we
can look to this administration and figure out what created the perceived golden age
that conservatives love to look back at. Massive government spending, massive jobs programs,
and high tax rates on the top. The tax rates back then fluctuated between 82 and 94 percent
for the top bracket. Today they're like 37. The New Deal and the jobs programs that went
along with it, a lot of stuff that gets called the New Deal wasn't, they were part of other
things, but it all blended together. This is what created that age of prosperity that
many conservative Americans look back at with longing. Now the reason these policies got
enacted is because we had a huge issue. We had financial problems. There was like this
Great Depression thing, and these policies were enacted to kind of get us out of it.
Now it's arguable as to whether or not they did, but what isn't arguable is that they
laid the groundwork for the golden age as most conservatives see it. It's something that
you may want to point out every once in a while, because right now we're in a similar
situation. We're coming out of long war. We have economic issues. The economy has been
depressed this time because of the whole global health thing. And we have a chance to institute
policies that could create real change, and that could lay the groundwork for real prosperity,
and you know maybe like fix the environment and address some racial issues and stuff like
that along the way. But those aren't going to be good talking points if you're trying
to reach conservatives. You're going to have to talk about money, because that's what matters.
And the reality is the period that they point back to was created by somebody who championed
all of the policies they oppose. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}