---
title: Let's talk about Idaho and never being now....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=TyMsL169jdk) |
| Published | 2021/09/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- After discussing a woman explaining healthcare standards at a gas station, many argued against restrictions, claiming it was anti-freedom.
- Idaho is currently operating under crisis standards of care, contradicting the belief that such situations wouldn't occur.
- An excerpt detailing a Universal Do Not Resuscitate order for adult patients during a public health emergency under crisis standards of care is shared.
- The order specifies that aggressive interventions should be provided but no attempts at resuscitation should be made.
- The low survival likelihood after cardiac arrest for adult patients and the significant risks to healthcare workers from resuscitation are emphasized.
- Beau urges people, especially in Idaho, to get vaccinated, wear masks, and follow necessary precautions to avoid the situation currently faced by Idaho.
- He warns against risky activities like riding four-wheelers due to the increased danger posed by the current crisis standards of care.
- Beau stresses the preventability of the situation in Idaho through vaccination and following measures known to slow down the spread and increase survival rates.
- Despite the availability of vaccines and knowledge on effective preventive measures, the situation in Idaho exemplifies the consequences of prioritizing "freedom" over public health.
- Beau concludes by encouraging viewers to take necessary precautions and hints at the irony of valuing freedom while endangering lives.

### Quotes

- "Freedom's just another word for nothing left to lose."
- "Never is now. Never is now."
- "But freedom, right?"
- "This is kind of preventable."
- "We know what increases survival rates. But freedom, right?"

### Oneliner

Idaho's crisis standards of care exemplify the consequences of prioritizing "freedom" over public health, urging vaccination and precautions to prevent preventable crises.

### Audience

Public health advocates

### On-the-ground actions from transcript

- Get vaccinated, wear masks, and follow necessary precautions to prevent crises like the one in Idaho (exemplified).
- Avoid risky activities that could lead to injury in areas facing crisis standards of care (implied).

### Whats missing in summary

The emotional impact of witnessing the consequences of prioritizing personal freedom over public health and safety. 

### Tags

#PublicHealth #Idaho #CrisisStandardsOfCare #Vaccination #PreventativeMeasures


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
freedom and Idaho. You know after I put out that video talking about the woman at
the gas station who explained the standards of care that might exist if
the current situation was allowed to get worse, I had a lot of people tell me that
you know that's anti-freedom and that freedom is what really matters and that
this would never happen. So Idaho must be where never exists. That must be where
that time frame is. Idaho is now operating under crisis standards of care.
I would like to read something. Universal DNR order. Adult patients hospitalized
during a public health emergency when crisis standards of care have been
declared should receive aggressive interventions. However, they should
receive no attempts at resuscitation. Compression, shocks, intubation, if not yet
intubated. That's in parentheses. In the event of cardiac arrest, the likelihood
of survival after a cardiac arrest is extremely low for adult patients. As well,
resuscitation poses significant risk to health care workers. And it goes on.
Talking about the waste that's occurring. Freedom's just another word for nothing
left to lose. You have nothing to lose at this point. If you are in Idaho, if you
are anywhere so you don't end up in the situation that Idaho is in, please go get
vaccinated. Wear a mask. Do the stuff that you need to do. Because now, freedom is
definitely going to be curtailed in a way. I wouldn't go, I don't know, riding
four-wheelers. I wouldn't do anything that could cause injury right now. In an
area like Idaho, accidents do happen because they're rural, generally. And all
of that just got a whole lot more dangerous. Because people wanted to
exercise their freedom to disregard everything that anybody with any
experience or any expertise was telling them. Never is now. Never is now. It is
happening in Idaho. They are operating under crisis standards of care. At this
point, this is kind of preventable. This is preventable. There is a vaccine that's
available. We know what slows down the spread. We know what increases survival
rates. But freedom, right? Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}