---
title: Let's talk about trying to debunk a claim about Delta....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fuQEfEvFJfc) |
| Published | 2021/09/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Investigating a claim about vaccines causing the Delta variant due to viral mutation.
- Acknowledging the plausibility of viruses mutating to survive.
- Exploring the concept of proving a negative claim and the challenge it presents.
- Emphasizing the importance of using a timeline as a scientific tool in investigations.
- Pointing out the global nature of the issue and the timeline of the Delta variant's identification in India before vaccines.
- Clarifying that the mechanics of evolution apply to natural immunity as well, leading to various flu strains.
- Asserting that vaccines did not cause the Delta variant, proven by a simple timeline.
- Suggesting fact-checking before making unfounded claims.

### Quotes

- "Life will find a way type of thing."
- "So no, the vaccines did not cause the Delta variant."
- "Have you ever tried to prove a negative?"
- "Why don't you fact-check before you open your ugly blank, blank, blank mouth?"
- "Y'all have a good day."

### Oneliner

Beau investigates a claim about vaccines causing the Delta variant, debunks it with a timeline, and stresses the importance of fact-checking.

### Audience

Science enthusiasts, vaccine skeptics

### On-the-ground actions from transcript

- Fact-check claims before spreading misinformation (suggested)
- Utilize timelines as a tool for investigating scientific claims (suggested)

### Whats missing in summary

The importance of using scientific tools like timelines to debunk misinformation and the global nature of the issue.

### Tags

#Vaccines #DeltaVariant #DebunkingMisinformation #FactChecking #Timeline


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to explore a claim.
We're going to look into a claim,
see if there's any evidence to back it up.
Because on a superficial level, there's
a basis to believe the claim, which
means we kind of have to dig into it a little bit more
than normal, because most times when we look into a claim
like this, they can be rejected pretty easily,
because the premise itself is false.
Not entirely true with this one.
So we need to look into it a little bit further.
OK, so what's the claim?
Beau, I can't believe you're still pushing vaccines.
It's incredibly irresponsible of you
after you started the Delta variant.
I started the Delta variant.
Personally, I started the Delta variant.
Vaccines caused the Delta variant,
and your continued push shows blatant irresponsibility
and inhumanity.
OK, so the idea that's contained in this message
is that vaccines spur viruses to change.
And the reality is there's kind of some basis to that,
not quite like this, but there is some basis to that.
It's a living thing.
It alters itself to survive.
Life will find a way type of thing.
So we can't just reject this out of hand, which means
we have to look into it.
But that's a huge jump, right?
This is plausible because the mechanics kind of exist
for this to occur to this happened.
And that's how conspiratorial thinking works.
There's this huge leap, and it's up to you to prove otherwise.
You have to prove that it's false.
Have you ever tried to prove a negative?
It's normally pretty hard. In some cases, it's impossible,
even when the claim is blatantly false.
You can't prove it to be false.
It's hard to do in most cases, but not in all cases.
OK, see, in this particular case,
we're lucky because we have access
to a scientific tool that can help us
a scientific tool that measures stuff.
And it provides decently accurate measurements.
It's been around for years.
In fact, we wouldn't know that it was around for years
without a calendar.
It's a calendar.
We have calendars.
If you've ever watched one of my videos on journalism,
you know if you're going to investigate something,
you're going to look into it.
The very first thing you should do is put together a timeline.
Now, I know people in the United States,
especially those who tend to only look at the United States,
don't pay attention to stuff outside the US,
but this is a global issue.
It is worth noting that the Delta variant
was first identified in India in October of 2020,
meaning it was around even before then.
But it was first identified in October of 2020.
That's before the vaccines.
So unless this theory involves unmentioned time travel,
that didn't happen.
I would also point out that the mechanics, evolution,
that allow this to be feasible, that also
works with natural immunity.
That's why you have so many different variations of the flu.
It's not the vaccine that causes it.
It's just, I mean, it really is life finding a way.
So no, the vaccines did not cause the Delta variant.
A simple timeline proves that.
I'm sorry.
I forgot a part of your message.
Why don't you fact check before you open your ugly blank,
blank, blank mouth?
I'll try to remember to do that.
Calendar, you can download one from the Google Play Store.
I think they're free.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}