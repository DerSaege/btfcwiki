---
title: Let's talk about triage at a gas station....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xE5N-Z9Ud6A) |
| Published | 2021/09/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Visits a gas station late at night to avoid crowds for personal reasons.
- Observes a woman working there checking for masks, interacts with Jeremy.
- Jeremy, a large man, enters without a mask, prompted to put one on by the woman.
- Woman, a CNA working at gas station, inquires if Jeremy got vaccinated.
- Shares about Dr. Fauci discussing potential healthcare triage situations.
- Explains to Jeremy the misconception about triage and prioritizing patients.
- Encourages Jeremy to understand that being sicker does not necessarily mean getting prioritized.
- Suggests that Jeremy's assumptions about triage may not be accurate.
- Acknowledges that the woman's explanation gave Jeremy something to think about.
- Emphasizes the importance of clarifying misconceptions about healthcare triage to others.

### Quotes

- "Jeremy, you go get your mask on."
- "That may be something you want to put in your toolbox."
- "It's definitely something worth remembering and worth working into..."
- "But it definitely gave him something to think about."
- "Y'all have a good day."

### Oneliner

Beau shares a gas station encounter illustrating the importance of clarifying misconceptions about healthcare triage during a pandemic.

### Audience

General public

### On-the-ground actions from transcript

- Clarify misconceptions about healthcare triage in casual, informative ways (implied).
- Encourage understanding of healthcare procedures among peers (implied).

### Whats missing in summary

The full transcript provides detailed insights into effectively communicating healthcare information in everyday situations.


## Transcript
Well howdy there internet people, it's Beau again.
So today, we're going to talk about a story from the gas station.
Because I think it might be helpful.
Something I overheard, a conversation I witnessed.
And I think it's worth repeating because I think it worked.
I go up to the gas station, and I go late at night because I don't like most people.
It has nothing to do with the public health thing.
I just don't want to be around a lot of people.
When I walk in, the bell on the door rings.
And the woman who works there, she's down behind the counter and she looks up over it
to make sure I have my mask on.
Which I do.
And I go about my shopping.
A few seconds later, the bell on the door dings again.
And I look over my shoulder, it's Jeremy.
I know Jeremy.
Jeremy is a nice guy.
A genuinely nice guy.
He's also huge.
Like he is a mountain of a man.
Probably, I don't know, 6'7", 350 pounds.
Huge dude.
And he's not wearing his mask.
And the woman who works there, she's, I don't know, 5'3", 115 pounds soaking wet.
Jeremy, you go get your mask on.
She yells at him.
And he drops his shoulders like his teacher just got onto him, walks outside, gets his
mask, comes back in.
He's not even through the door yet.
And she's like, have you gotten your shot yet?
Now she takes it seriously because she's almost through nursing school.
She's a CNA, but she works at the gas station because she makes more at the gas station
than she did as a CNA, which is probably something we should talk about as a country at some
point.
But I can tell that they have had this conversation before.
And he's like, no, I haven't got my shot yet.
And she's like, you know, Dr. Fauci was just on TV talking about how some places are closer
to having to triage care.
And he's like, as much as I smoke and as bad as health I'm in, that just means I'm going
to the front of the line, concierge style.
And she looks at me.
No facial expressions, right?
But I know what's coming.
She flips on that patient education voice.
And if you've never been around nurses, you may not know what I'm talking about.
But they all have this voice that they slip into when they're explaining something.
And she's like, oh, honey, that is not what that means.
That's not what that means at all.
In a case like this, it's not the sickest first.
They take the people most likely to survive so they can save the resources and help as
many people as possible.
The reasons you're saying you'll go to the front of the line are the reasons they might
just send you home.
Again, no facial expressions, but you could see it in his eyes.
It registered.
I don't know if it works.
Like I don't know if he's going to go get his shots.
But it definitely gave him something to think about.
That may be something you want to put in your toolbox.
That may be something that more people misunderstand.
People may not exactly understand what that means if they have to start doing that.
It isn't you're the first one there or you're the sickest.
If you're really sick, they may not.
And they may reserve the bed for somebody who has a better chance of making it.
It's definitely something worth remembering and worth working into conversations if people
are uninformed as to what that actually means, if it really does come to that.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}