---
title: Let's talk about the word "boy" in the South....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=NmReupLUvDE) |
| Published | 2021/09/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the racist connotations of the word "boy" when spoken to a black person in the southern United States.
- Mentions that the history of slavery and segregation is deeply embedded in the racial charge behind the word.
- Talks about how the word "boy" was used to belittle and keep black men down during slavery.
- Describes how the term "boy" signifies a lack of equality and is a way of asserting superiority.
- Points out that even when referring to literal children, the word "boy" is used less frequently now due to its historical context.
- Emphasizes that there is never an appropriate way to use the word "boy" when speaking to a black person.
- Mentions that the word "boy" is used cautiously by most people in the southern United States due to its racial history.
- Advises against using the word "boy" in any context when addressing a black person.
- Explains how the tone and context in which the word is said play a significant role in its racial implications.
- Concludes by stating that due to its history, the word "boy" may never be fully rehabilitated.

### Quotes

- "The word boy, when spoken to a black person in the southern United States, especially, is racist."
- "Today, most people in the southern United States avoid the word more than you might imagine."
- "There's never an appropriate way to use it when talking to a black person."
- "The South has a lot of racist history, so there are a lot of words that carry a lot of racist connotation that you may not imagine."
- "Y'all have a good day."

### Oneliner

Beau explains the deeply rooted racism behind the word "boy" when used towards black individuals in the southern United States, advising against its usage in any context.

### Audience

Educators, Linguists, Social Activists

### On-the-ground actions from transcript

- Educate others on the racist connotations of certain words (exemplified)
- Promote awareness and sensitivity towards racially charged vocabulary (exemplified)

### Whats missing in summary

Importance of understanding historical context for words' racial implications.

### Tags

#Racism #SouthernUnitedStates #WordUsage #History #Awareness


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to do another little linguistics lesson on the southern United
States and how we talk down here.
We're going to go through a word, we're going to talk about a word, and go through the appropriate
and inappropriate uses of that word.
Today we are going to talk about the word boy.
My boyfriend and I were watching your episode on masks in schools.
When you said, pull up your pants boy, he said you could be the evil villain in any
movie about the south.
I'm not from here, and he often tells me words mean things they don't as a joke.
He told me the word boy was very racist.
I find this hard to believe that such a simple word could become racist.
Is the word racist?
And if it is, how did it become racist?
So the answer to this, the simple answer to this is yes.
Yeah it's racist.
The word boy, when spoken to a black person in the southern United States, especially,
I would imagine this is kind of true everywhere, but especially in the southern United States
is racist.
It can be more racist with tone, but it's pretty much always racist.
Now in the south, you could also say old boy, which is OLD.
It's old boy, but we don't pronounce the D.
And that just means the person down the road.
Good old boy is somebody that you should be friends with.
Big old boy has nothing to do with size.
That's somebody you don't want to fight.
Boy is racist.
It's all tone.
It's all tone.
It's all how it's said.
There's a lot of context to it.
So how and why did such a simple innocent word become so racially charged?
History.
In the southern United States, from slavery through segregation and up to today, boy,
when spoken from a white person to a black person, is a way of saying you're not on my
level.
You're a child.
This habit, I guess it started during slavery.
They would call adult men boy as a way of keeping them down, as a way of just casting
them as you'll never be my equal type of thing.
Today, most people in the southern United States avoid the word more than you might
imagine with the exception of when it gets attached to old.
If all is attached to it, it's friendly.
But even then, my friends who are remotely racially conscious, they wouldn't say, oh,
it's a big old boy about a black person, even though in that context, that's not what it
means.
The history of the word has led most people, people who aren't bigots, to be very cautious
with it.
Even when you're talking about literal children, boy doesn't get used as much as it used to.
Now it's son or young man, boy doesn't get used, and it's because of that history.
I'm not sure how that word is ever going to be rehabilitated.
It's a word that is in common usage all the time, but because of its history down here,
people do try to avoid it more than you might imagine.
So yes, in the way the tone dictates, in the way it's said, this is not a word you should
ever say to a black person.
If that's what you're looking for, like as far as advice, never.
There's never an appropriate way to use it when talking to a black person.
There's really not.
So he's not playing a joke on you, and in context of that video, in case you don't know
since you're not from here, there was a trend where people would sag their pants, they would
ride low, you'd see a little bit of their boxers.
In the southern United States, it became a thing to harass people over this, and it was
a trend that was typically associated with black people.
And when that harassment started, that racism came out thick.
So much so that to be honest, I don't believe anybody ever actually cared that the pants
were riding low.
I think they were looking for an excuse to harass black people.
The South is weird.
The South has a lot of racist history, so there are a lot of words that carry a lot of racist
connotation that you may not imagine.
So he's not messing with you, he's not playing a joke on you.
It really is racist.
There are very few exceptions.
So as a general rule, I would never say it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}