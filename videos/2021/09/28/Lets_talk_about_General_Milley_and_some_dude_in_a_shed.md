---
title: Let's talk about General Milley and some dude in a shed....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gMV84EZMGGM) |
| Published | 2021/09/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- General Milley testified about his phone call with his Chinese counterpart.
- Someone messaged Beau, comparing General Milley's testimony to Beau's videos.
- The comparison suggests that General Milley and Beau may be sharing talking points.
- Beau argues that General Milley was just doing his job, suggesting the calls are routine.
- Beau mentions having friends in D.C. but denies receiving classified information from them.
- He implies that news outlets sensationalize routine activities for profit.
- Beau points out that calming nerves and de-escalation are key in military communications.
- He criticizes news outlets for not providing the full context of routine military calls.
- Beau believes the scandal narrative is driven by profit-seeking news outlets.
- He suggests that sensationalizing normal activities creates unnecessary panic among viewers.

### Quotes

- "Maybe General Milley was just doing his job."
- "They're trying to get ratings. They are sensationalizing the run-of-the-mill, the normal."
- "Calls like this between militaries probably occur on a weekly basis."
- "It's not abnormal. It's not a scandal. It's not a story."
- "The scandal profit motivator is probably the more likely scenario."

### Oneliner

Beau debunks sensationalism around General Milley's routine military calls, criticizing profit-driven news outlets for creating unnecessary panic.

### Audience

News consumers

### On-the-ground actions from transcript

- Fact-check news stories before reacting (implied)
- Support independent media sources (implied)
- Stay informed about routine government activities (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of how routine military communications are sensationalized by profit-driven news outlets, urging viewers to seek out unbiased information sources and not fall prey to unnecessary panic. 

### Tags

#GeneralMilley #RoutineMilitaryCalls #Sensationalism #NewsConsumers #MediaBias


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are going to talk about General Milley's testimony
and some dude in a shed,
and what connections General Milley may have to that dude in a shed.
If you don't know, General Milley testified today
about his phone call with his Chinese counterpart.
Shortly after that testimony, I got this message.
Hey, Beau, I just watched General Milley testify.
It was like watching your video.
He used every excuse you laid out for him.
He even cited his oath in the exact same way.
This leaves us with one of two options.
The first is that you're not just a dude in a shed
and that you get your talking points from D.C.
The second is that General Milley gets his talking points from you.
Sure, I mean, those are two options.
I mean, they do exist as options.
I'm not sure they're the only options, though.
Admittedly, I still do have friends in D.C.,
precisely none of which would disclose the inner workings
of a classified phone call between General Milley
and his Chinese counterpart to me.
That's not going to happen. We're not that close.
Maybe there's a simpler option.
Maybe General Milley was just doing his job.
Maybe those phone calls are incredibly routine
and have been ever since the Cuban Missile Crisis.
They existed even before then,
but they became very organized after that
because de-escalation is important
and calming nerves is important.
Yeah, I went back and I watched his testimony.
I didn't watch it before I got this message
because I knew what it was going to say.
It's not that I have access to secretive information.
It's that I don't sensationalize the normal.
Perhaps the real issue is that the news outlet you favor,
the one you choose to consume information from,
has a profit model that requires it
to turn the run-of-the-mill, everyday activities
of somebody who is seen as political opposition
into a scandal.
That's the more likely scenario.
That's much more likely than General Milley
taking talking points from me.
Anybody who has any understanding
of the way militaries communicate
would know that this was completely normal,
that it made complete sense,
and that it probably occurred
because the Chinese were worried
about President Trump's intentions
because he did seem a little off,
and that it would be General Milley's responsibility
to convey calm to them.
The fact that your news outlet didn't tell you this
is their failing
because they're not trying to inform.
They're trying to get ratings.
They are sensationalizing the run-of-the-mill, the normal.
Calls like this between militaries
probably occur on a weekly basis.
I would say the US engages in them probably on biweekly.
Every other week, there's a phone call like this.
It's not abnormal.
It's not a scandal.
It's not a story.
It's just functionaries doing what they do
on a day-to-day basis,
but they tried to turn it into a scandal.
They got you worked up,
and now the only options you can see,
rather than, hey, they just didn't tell me this
because they wanted me scared,
is that a moderate-sized YouTuber
is taking talking points from DC
or that General Milley is taking talking points
from some dude in a shed.
I think the scandal profit motivator
is probably the more likely scenario.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}