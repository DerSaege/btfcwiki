---
title: Let's talk about Massachusetts State Police and vaccines....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Xq2qj1kZP3Q) |
| Published | 2021/09/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Acknowledges being critical of law enforcement but praises the local departments for being responsive to the community and quick to address issues.
- Shares an incident where a deputy planting evidence was caught and dealt with swiftly without public outrage.
- Talks about the SWAT team in a nearby jurisdiction made up of ex-military deputies who train for high-risk scenarios to protect the public, despite the dangers involved.
- Critiques the Massachusetts State Police union's claim of officers resigning over vaccine mandates, contrasting the union's rhetoric with the reality of only one officer actually resigning.
- Questions the commitment of officers who refuse to follow mandated policies and suggests that it may indicate biases affecting their work on the streets.
- Argues that officers unwilling to mitigate risks to the public by following policy should not be in law enforcement and sees their resignation as a way to weed out those not fit for the job.
- Expresses that enforcing unjust laws and refusing to follow safety protocols are different and believes that officers who can't follow safety protocols shouldn't be in the force.
- Concludes that even if multiple officers were to resign over policy disagreements, it wouldn't harm the state or its people, seeing it as a potential positive in removing those not adhering to policy.

### Quotes

- "Protect and serve. Mitigate risk to the public. If you're not willing to do that, you shouldn't be a cop."
- "This seems like an easy way to get rid of people who won't follow policy, who have biases they let impact their judgment."
- "At the end of this, I can't see any way in which this actually harms the state of Massachusetts or the people of Massachusetts."

### Oneliner

Massachusetts State Police union's claim of officers resigning over vaccine mandates scrutinized, with Beau questioning their commitment to public safety and policy adherence.

### Audience

Community members

### On-the-ground actions from transcript

- Hold local law enforcement accountable for following mandated policies and ensuring public safety (implied).

### Whats missing in summary

Detailed explanation on how biases and policy non-adherence can affect law enforcement's ability to protect and serve effectively.

### Tags

#LawEnforcement #PoliceUnion #VaccineMandates #PublicSafety #PolicyAdherence


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about the Massachusetts State Police, the union up
there.
We're going to talk about what the union said,
what the rhetoric developed into,
what's actually the case,
and then we're going to
talk about, well let's pretend it's true for a second.
We're going to play that game.
But before we get to that,
I want to
talk about some local departments real quick.
You know, I'm not somebody who has ever been accused of
backing the blue.
I'm a very strong critic of law enforcement.
I've also pointed out
that in my area
the departments are decent.
They're decent because they're small, because of their rule, because they have
to be responsive
to the community or the sheriff
is no longer employed.
We don't have a lot of the issues that other places have.
And when they do arise, they get dealt with pretty quickly, and not because
there's public outrage.
There was an incident pretty recently,
a few jurisdictions over, where a
deputy planted stuff on people.
DA found out about it. DA reached out to the sheriff's department, talked to the
brass. Brass talked to the guy, tricked him, got him out of his car,
had other deputies search his vehicle, found it,
arrested him.
I got a lengthy sentence. Do you know what isn't in that chain of events?
Public outcry.
There was no leaked camera footage showing it or anything like that.
They did it because this is one of the few areas that I'm aware of
where the barrel still tries to get rid of the bad apples.
It does occur.
And yes, there are still issues here,
but when you compare them
to other areas, they're relatively minor, and they get handled
pretty quickly.
Nearby,
another jurisdiction, they have a SWAT team.
Now, it's not a real SWAT team like anybody in any city would picture.
It's deputies who were ex-military,
who knew how to clear rooms and stuff like that,
continuing to train on their off time,
on the off chance they're ever needed.
These guys, they run
kill houses, those
fake houses
where you see them go in and shoot and stuff like that. Live fire stuff. They do
this, I want to say, every other weekend.
If you don't know,
that's not actually a very safe thing.
It's pretty risky.
Slipping,
bumping into a wall, wrong footing,
can cause a flag-covered coffin.
It's not
a safe activity,
but they do it.
And they do it in case they're ever needed,
they can mitigate the risk.
They can mitigate the risk to the public because that's the job,
protect and serve.
Let's talk about Massachusetts.
The union up there, the cop union,
said that
dozens
of state police had already submitted their resignation
over the vaccine mandates,
or they were going to.
The rhetoric, of course, turned into hundreds.
The reality
is,
according to the reporting today,
one, one,
one cop
submitted their resignation.
That doesn't seem like something that really matters,
to be honest.
That doesn't seem like a high percentage at all.
But, let's pretend it's true for a second.
Let's pretend
you are talking about dozens or even hundreds of state police resigning over
this.
What should the response be?
Throw them a going away party.
What's the job?
The job? Protect and serve.
Mitigate risk to the public.
If you're not willing to do that, you shouldn't be a cop. It's kind of simple.
That goes along with it, right?
If somebody isn't willing
to engage in this tiny risk
to
help protect the public,
I don't think that
Massachusetts is going to have a huge loss if they quit. I don't think that's
going to be a hindrance to the department.
And then you look at it from the policy standpoint.
This is something that came down. It's mandated. It is policy. This is what you
were supposed to do. This is the protocol.
And they decide not to over politics
to the point where they're willing to quit.
Do we really believe that those people were following policy when they're out
on the street?
Or do we think that
if a political bias
would shift
their willingness to work for an agency,
do we think that maybe other biases
could have been at play when they were out on the road?
I don't think this is a huge loss, even if it is true. Apparently it's not.
It's one person.
But let's say it is dozens or hundreds.
Good. This seems like an easy way
to get rid of people who won't follow policy,
who have biases they let impact their judgment,
and who aren't willing to mitigate risk to the public.
This seems like a win
to be completely honest.
It seems like this is more effective at
weeding out the bad apples
than any training or retraining that could occur.
The idea
of law enforcement
in theory
should be to mitigate risk to the public,
to help keep the public safe, to protect and serve.
We understand that
it has shifted to law enforcement and there are those who will make the
philosophical argument that if you chose to do this job,
you have
chosen a profession where you agree to enforce unjust laws and therefore
they're all bad.
And I get the philosophical argument and it's kind of hard to argue with
because there are unjust laws.
But I would still
go back to the fact
that these officers or officer
they had no problem enforcing those
but they won't follow this policy
designed to help keep people safe.
At the end of this,
I can't see any way
in which this actually harms
the state of Massachusetts or the people of Massachusetts.
And from the statements that came from the union
versus the reality,
I think we understand why so many people refer to cops taking the stand as
testifying.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}