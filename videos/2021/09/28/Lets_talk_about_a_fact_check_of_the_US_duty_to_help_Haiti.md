---
title: Let's talk about a fact check of the US duty to help Haiti....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=xQnr_s7xnpQ) |
| Published | 2021/09/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the topic of Haiti and the importance of providing assistance to the country.
- Fact-checking a tweet from the Gravel Institute regarding the US's responsibility towards Haitian refugees.
- Corrects the timeline mentioned in the tweet, stating that US involvement in Haiti spans over 100 years.
- Describes the historical context of US involvement in Haiti, including seizing national reserves and installing a president.
- Agrees with the overall theme of the tweet that the US has a duty to help Haiti due to its historical involvement.
- Stresses that assisting Haitian refugees is a moral obligation regardless of past actions.
- Mentions the significance of understanding US involvement in Haiti, particularly related to business interests.
- Notes the parallels between US actions in Haiti and Central America, primarily driven by establishing power and influence.
- Encourages further exploration of the topic due to the extensive history of US involvement in Haiti.
- Concludes by affirming the importance of providing aid to those in need.

### Quotes

- "We broke it, we should fix it, we have a moral and ethical and legal duty to help these people."
- "The reason we have a duty to accept refugees from there is because they're people, they need help, and we have the means to help."
- "Even if the United States didn't have as a pronounced role in the situation that Haiti finds itself in today, we should still help."
- "It's worth reading, but understand, it's a long read, because there is a lot of U.S. involvement over the years."
- "It very much falls in line with the way the United States behaved in Central America."

### Oneliner

Beau addresses US involvement in Haiti, fact-checks a tweet on Haitian refugees, stressing the moral duty to provide aid regardless of past actions.

### Audience

Global citizens

### On-the-ground actions from transcript

- Support organizations aiding Haiti (suggested)
- Educate others on Haiti's history and US involvement (suggested)

### Whats missing in summary

The full transcript provides detailed insights into US involvement in Haiti and the moral obligation to assist Haitian refugees despite historical actions.

### Tags

#Haiti #USinvolvement #Refugees #Aid #MoralResponsibility


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Haiti and why we should help if we should.
And we're going to fact check a tweet from the Gravel Institute.
We're going to do this because I got a message.
Hey Beau, your videos on US involvement in Central America prior to the Cold War really
helped change my opinion on whether or not we should help refugees from that area today.
I found this tweet from the Gravel Institute and was wondering if you could fact check
it and tell us whether or not this is accurate.
Okay, the tweet from the Gravel Institute, it says, the reason the United States has
a duty to accept Haitian refugees is because the United States played the leading role
in destroying Haiti over the last 60 years, supporting dictatorships, funding death squads,
and undermining self-sufficient agriculture.
Okay, so fact check.
Is the general theme of this tweet accurate?
Yeah.
Are the specifics?
Ummm, kinda?
Okay, over the last 60 years.
That's not accurate.
It hasn't been 60 years.
It's been more than 100.
The United States has had designs on Haiti since the 1800s, but really got involved heavily
in the early 1900s, primarily having to do with debt that Haiti had, that the United
States held, and a sugar company.
That led to us seizing their national reserves, occupying the country, and installing a president.
So I mean, yeah, the overall theme of this tweet is the US broke it, therefore we should
help.
Okay?
Yeah, it's true.
It just went on 40 years longer than they say in the tweet.
I wouldn't say that that's a false tweet in any way, shape, or form.
Just didn't go back far enough.
The only problem I have with this tweet, honestly, is the reason the United States has a duty
to accept Haitian refugees is because of this.
The reason we have a duty to accept refugees from there is because they're people, they
need help, and we have the means to help.
It's that simple.
Even if the United States didn't have as a pronounced role in the situation that Haiti
finds itself in today, we should still help.
We're talking about lives.
We're talking about actual people who are in a bad situation that can be assisted with
minimal effort.
So I don't, I totally agree with the overall theme that the Gravel Institute is putting
out here.
They're putting out the idea that, hey, we broke it, we should fix it, we have a moral
and ethical and legal duty to help these people.
I completely agree.
I just think that we are, we're required to do that whether or not we broke it to begin
with or not.
If you want to dig into the U.S. involvement in Haiti, it is interesting, especially the
stuff involving the sugar companies, because basically we did kind of invade this country
on behalf of U.S. business interests.
And it goes back a very, very long time.
It's worth reading, but understand, it's a long read, because there is a lot of U.S.
involvement over the years.
Not all of it good.
Even some of the stuff that was, that had good intentions didn't turn out to be good.
And some of it didn't have good intentions to begin with.
It was about wielding power in that area of the world and establishing a solid U.S. presence.
But yes, it very much falls in line with the way the United States behaved in Central America.
There's a lot of parallels.
The difference is it's more sugar than bananas.
But I mean, other than that, it's kind of the same story.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}