---
title: Let's talk about something in American politics that needs to stop now....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=U-D8RIxz1io) |
| Published | 2021/09/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the distasteful habit in American politics of using tragedies for political gain.
- Refuses to name individuals who have recently passed away, citing the importance of not using their deaths for personal commentary or political points.
- Believes expressing condolences may come off as insincere and prefers to simply say "I'm sorry for your loss."
- Criticizes the idea of approaching grieving families in the future to tell them their loved one's death was in vain.
- Suggests focusing on the positive outcomes of actions taken, like the humanitarian airlift, rather than politicizing tragedies.

### Quotes

- "I typically just say I'm sorry for your loss and I leave it at that because it's not words that dulls the pain. It's time."
- "Please do not talk to their families."
- "The worthless pointless mission gave a hundred thousand people a new lease on life."

### Oneliner

Beau addresses the exploitation of tragedies in politics, refuses to name recent deceased individuals, and suggests focusing on positive outcomes rather than politicizing tragedies.

### Audience

Social commentators

### On-the-ground actions from transcript

- Respect the privacy of grieving families and refrain from approaching them to express opinions on their loved one's death (implied).

### Whats missing in summary

The full transcript provides a nuanced perspective on handling tragedies and respecting the dignity of those who have passed away.

### Tags

#AmericanPolitics #Tragedy #Condolences #Respect #HumanitarianAid


## Transcript
Well howdy there internet people, it's Beau again. So today we are going to talk about
one of the more distasteful things that exists in American politics and loss.
Because I got this and this is actually a very long message. I'm going to cut a
piece out of it, a chunk out of it, and I'm really only making this video to talk
about one specific part. But the whole thing is worth reading with the
exception of the part that's going to be omitted. You've talked about the
evacuation countless times. You've never even expressed your condolences to the
families who lost someone in a worthless pointless mission that nobody understood
because Biden's a failure. Say their names, you coward. And then it has a whole
bunch of information about those lost. Their names, their kids names, their
parents names, stuff like that. Something I'm not going to read. These kids died
not knowing the danger because Biden is an incompetent dementia patient. Say
their names. You can bet your rear if I ever see these parents I'll tell them
that America stands behind them as they deal with the pointless pain Biden has
caused them. Talk about the Americans who died in vain. Or better yet, make a
video about how to explain to a child's name that their parents died simply
because orange man bad.
It's a pretty strong indictment right there.
I don't name people who are lost in recent events. It's not something I do.
You can go back and look at other videos. I don't do it. The reason is because I
express a lot of personal opinion and provide commentary on a lot of different
topics. I think one of the most distasteful things that occurs in
American politics with any frequency is the habit of commentators and
politicians standing on the graves of people far better than themselves using
their headstones to hold the notes for their speech, holding their orphaned
child, and making statements that those lost may not agree with. So I don't do
that. I think that's wrong because I would hate to be wrong about something. I
would hate to make it appear that the loss cosigned what I was saying and be
wrong about it. It would drive me mad if I said that a mission was worthless and
pointless and that nobody understood it and then find out that one of the names
I invoked, one of their last posts to social media, was a photo of them holding
an Afghan child with the caption saying that they love their job. I would be
really bothered if I painted them as a bunch of dumb ignorant kids who didn't
understand the risk when one of the names I invoked said that it's kill or be
killed and I'm trying to be on the kill side. I wouldn't want to be the person
who would do that for political points. So I don't mention names. It's not
something I do and I would certainly not use their children's names. As far as
expressing condolences, it's not really something I do either. It was at a funeral, not a funeral, a
memorial for a whole bunch of people at one time and I watched as the friends of
the people who were lost talked to the parents and the family members of those
who were lost and I watched those friends, their close friends, say all the
right things. Heartfelt stuff, stuff they had really thought about, poetic stuff
coming from people you would not expect poetry from and I watched it fall flat
because it doesn't mean anything, not really. I typically just say I'm sorry
for your loss and I leave it at that because it's not words that dulls the
pain. It's time. That's pretty much the only thing that does it. If I was to get
up here and lament those lost, believing that in most cases it doesn't do any
good and not knowing whether or not the family would ever actually see it, what
is that? Is it actually me expressing my condolences or is it trying to get
clout off a corpse? No thank you. It's not something I'm interested in doing. The
real reason I'm making this video is this line right here where you say that
you can bet your rear if I ever see these parents I'll tell them that
America stands behind them as they deal with the pointless pain Biden has caused
them. Talk about the Americans who died in vain. I'm gonna ask you not to do that.
I'm gonna ask you not to do that. The time, time's what does it. That's what
kind of dulls the pain and the last thing these families need is to be
getting groceries six months from now and some stranger they have never met
who recognizes them from a media report walks up to them and tells them that
their child's death was in vain. But don't worry, America's behind you. I'm
gonna ask you not to do that. Or better yet, make a video about how to explain to
a child that their parent died because orange man bad. I get it because the
mission is worthless and pointless and nobody understood it and they didn't
understand the risk. I understand. I'll actually answer that. The child that
you're referencing, very small. This conversation won't take place for years.
So what I would probably do if I was in this very unfortunate situation is I
would open a history book and I would point to the paragraphs about one of the
largest humanitarian airlifts in modern history and I would talk about the
hundred thousand lives that got a new start because of that operation. Because
of their parent. I understand that to a whole lot of people this type of stuff
is all individual action but they didn't really save them because they didn't fly
them out. I get that. In real life that's not how it works. It's a team effort.
Those planes don't take off unless that perimeter is secure. The worthless
pointless mission gave a hundred thousand people a new lease on life.
That's what it did. I would start there. That's where I would begin the
conversation. If I could explain anything to you it would be to not take away from
what they accomplished, what they did, so you can make some political talking
point. Because whether they agreed with the mission or not they all did their
part and a hundred thousand people got a new lease on life. In fact if they
didn't agree with the mission and they did their part anyway that speaks even
more highly of their character. Because it's a team effort and they wouldn't let
their teammates down. Please do not talk to their families. Anyway it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}