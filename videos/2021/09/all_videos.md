# All videos from September, 2021
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2021-09-30: Let's talk about the other withdrawal... (<a href="https://youtube.com/watch?v=Pd1GA0VtWio">watch</a> || <a href="/videos/2021/09/30/Lets_talk_about_the_other_withdrawal">transcript &amp; editable summary</a>)

Beau provides insights on the upcoming withdrawal from Iraq, discussing its differences from Afghanistan and potential foreign policy implications.

</summary>

"Withdrawals are messy, but caution and planning can make this one cleaner than most."
"Unlikely for Iraq withdrawal to mirror Afghanistan; slim chances of similar devolution."
"Biden administration aims to maintain a U.S. footprint in Iraq for foreign policy, not combat purposes."

### AI summary (High error rate! Edit errors on video page)

Congresspeople, clueless about the withdrawal, question experts post-plan change.
Biden administration looking to swiftly withdraw from Iraq, leaving behind strategic advisors and trainers.
Comparing Iraq withdrawal to Afghanistan: different countries, historical government forms.
Likelihood of the opposition group in Iraq retaking power is close to zero.
Potential for other opposition groups to make small advances, but unlikely to cause significant issues.
Withdrawals are messy, but caution and planning can make this one cleaner than most.
Unlikely for the Iraq withdrawal to produce dramatic footage or talking points like Afghanistan.
Iraq's national government is capable, and Iran may seek to exert influence in the region.
Biden administration aims to maintain a U.S. footprint in Iraq for foreign policy, not combat purposes.
Unlikely for Iraq withdrawal to mirror Afghanistan; slim chances of similar devolution.
Outside actor involvement needed to significantly disrupt the withdrawal process, which seems improbable.
Iraq withdrawal may not receive extensive news coverage due to lack of drama, but it holds foreign policy significance.
Iraq's withdrawal brings Iran into a regional power role traditionally, but may not attract the attention it deserves.
Iraq withdrawal marks the end of a prolonged conflict, with potential implications for Iran's regional influence.

Actions:

for foreign policy enthusiasts,
Contact local representatives to express opinions on U.S. involvement in Iraq (suggested).
Stay informed about developments in Iraq and Iran to understand regional dynamics (implied).
</details>
<details>
<summary>
2021-09-30: Let's talk about tactics to reach a different hesitant demographic.... (<a href="https://youtube.com/watch?v=tUjTh1RDjqY">watch</a> || <a href="/videos/2021/09/30/Lets_talk_about_tactics_to_reach_a_different_hesitant_demographic">transcript &amp; editable summary</a>)

Beau addresses vaccine hesitancy in spiritually inclined individuals, offering tactics to overcome resistance and misinformation effectively.

</summary>

"Those who are hesitant are the real critical thinkers, and those pro-vaccine aren't."
"Your intuition is in fact telling you to get vaccinated."
"It's making sure that you don't cause harm unintentionally."

### AI summary (High error rate! Edit errors on video page)

Addressing a demographic that is spiritually inclined and vaccine hesitant or resistant.
Tactic: Ask hesitant individuals to specify who benefits from the conspiracy theories they believe in.
Point out the unreliability of intuition by referencing past mistakes.
Dispel the belief that vitamins alone can protect against COVID-19 by directing them to reliable sources like the Mayo Clinic.
Dealing with fatalistic beliefs about fate and predetermined decisions.
Use specific examples to counter misinformation spread by banned individuals.
Explain the purpose of masks as a way to protect those around you, not just yourself.
Appeal to the spiritual beliefs of individuals by relating mask-wearing to preventing unintentional harm.

Actions:

for vaccine advocates and community influencers.,
Direct hesitant individuals to reputable sources like the Mayo Clinic for accurate information (suggested).
Use specific examples to counter misinformation spread by influential figures (implied).
Educate individuals on the purpose of mask-wearing to protect those around them (suggested).
</details>
<details>
<summary>
2021-09-29: Let's talk about the debt ceiling and calling the bluff.... (<a href="https://youtube.com/watch?v=_ZYTue0F-uw">watch</a> || <a href="/videos/2021/09/29/Lets_talk_about_the_debt_ceiling_and_calling_the_bluff">transcript &amp; editable summary</a>)

Raising the debt ceiling is vital to avoid catastrophic economic impacts, and political drama surrounds the issue despite its historical inevitability.

</summary>

"Maybe it's time for the Democratic Party to call their bluff."
"This isn't a both sides issue. It will occur the same way it has occurred the last 70 times more than."
"Look at what you made me do."

### AI summary (High error rate! Edit errors on video page)

Explains the importance of raising the debt ceiling to avoid severe consequences like a default.
Points out that not raising the debt ceiling could lead to skyrocketing home and auto loans, job loss, and government dilemmas.
Emphasizes that the impacts of not raising the debt ceiling are significant, affecting Social Security benefits and bondholder payments.
States that the drama surrounding the debt ceiling is unnecessary, as it has been raised over 70 times before.
Suggests that the Republican threats regarding the debt ceiling are akin to threatening to harm their own base.
Indicates that the Republican base will be heavily impacted if the debt ceiling is not raised.
Questions whether the Democratic Party should call the Republican bluff on this issue.
Speculates that the U.S. might lose its leading global position if the debt ceiling issue is mishandled.
Expresses doubt that the Republican Party will allow a default due to self-preservation, despite their negotiation tactics.
Concludes by noting that a failure to raise the debt ceiling could hasten the decline of the U.S. economy.

Actions:

for policy advocates and concerned citizens.,
Contact your representatives to urge them to prioritize raising the debt ceiling to prevent economic turmoil (implied).
Stay informed about the ongoing developments regarding the debt ceiling issue and its potential impacts (implied).
</details>
<details>
<summary>
2021-09-29: Let's talk about Milley undermining Trump.... (<a href="https://youtube.com/watch?v=PA7r0oJgBh4">watch</a> || <a href="/videos/2021/09/29/Lets_talk_about_Milley_undermining_Trump">transcript &amp; editable summary</a>)

General Milley's actions shift the narrative from treason to undermining Trump, revealing the importance of critical thinking and accepting evidence.

</summary>

"Moving the goalposts portrays Trump as a loose cannon, a bad guy, somebody who did not have the best interest of the United States at heart."
"The better response when faced with evidence to the contrary is to actually look at it, accept it, accept the correction, admit that you were wrong, and go from there."

### AI summary (High error rate! Edit errors on video page)

General Milley's actions have sparked a shift in talking points among the right wing.
The focus has moved from accusing Milley of treason to undermining former President Trump.
Critics failed to think critically and blindly followed the initial narrative.
Even if Milley's actions undermined Trump, it was to prevent a catastrophic war, not for personal gain.
Suggesting Trump's intentions were to start a war reinforces the need for Milley's actions.
Moving the goalposts portrays Trump as someone not acting in the best interests of the country.
Beau encourages accepting evidence, admitting faults, and learning from mistakes.
Some individuals may realize they were misled into believing every action is a scandal.
Blindly following those against the country's best interest is misguided.

Actions:

for critical thinkers,
Fact-check narratives before blindly following them (implied)
Encourage critical thinking and accepting evidence (implied)
</details>
<details>
<summary>
2021-09-28: Let's talk about a fact check of the US duty to help Haiti.... (<a href="https://youtube.com/watch?v=xQnr_s7xnpQ">watch</a> || <a href="/videos/2021/09/28/Lets_talk_about_a_fact_check_of_the_US_duty_to_help_Haiti">transcript &amp; editable summary</a>)

Beau addresses US involvement in Haiti, fact-checks a tweet on Haitian refugees, stressing the moral duty to provide aid regardless of past actions.

</summary>

"We broke it, we should fix it, we have a moral and ethical and legal duty to help these people."
"The reason we have a duty to accept refugees from there is because they're people, they need help, and we have the means to help."
"Even if the United States didn't have as a pronounced role in the situation that Haiti finds itself in today, we should still help."
"It's worth reading, but understand, it's a long read, because there is a lot of U.S. involvement over the years."
"It very much falls in line with the way the United States behaved in Central America."

### AI summary (High error rate! Edit errors on video page)

Addressing the topic of Haiti and the importance of providing assistance to the country.
Fact-checking a tweet from the Gravel Institute regarding the US's responsibility towards Haitian refugees.
Corrects the timeline mentioned in the tweet, stating that US involvement in Haiti spans over 100 years.
Describes the historical context of US involvement in Haiti, including seizing national reserves and installing a president.
Agrees with the overall theme of the tweet that the US has a duty to help Haiti due to its historical involvement.
Stresses that assisting Haitian refugees is a moral obligation regardless of past actions.
Mentions the significance of understanding US involvement in Haiti, particularly related to business interests.
Notes the parallels between US actions in Haiti and Central America, primarily driven by establishing power and influence.
Encourages further exploration of the topic due to the extensive history of US involvement in Haiti.
Concludes by affirming the importance of providing aid to those in need.

Actions:

for global citizens,
Support organizations aiding Haiti (suggested)
Educate others on Haiti's history and US involvement (suggested)
</details>
<details>
<summary>
2021-09-28: Let's talk about Massachusetts State Police and vaccines.... (<a href="https://youtube.com/watch?v=Xq2qj1kZP3Q">watch</a> || <a href="/videos/2021/09/28/Lets_talk_about_Massachusetts_State_Police_and_vaccines">transcript &amp; editable summary</a>)

Massachusetts State Police union's claim of officers resigning over vaccine mandates scrutinized, with Beau questioning their commitment to public safety and policy adherence.

</summary>

"Protect and serve. Mitigate risk to the public. If you're not willing to do that, you shouldn't be a cop."
"This seems like an easy way to get rid of people who won't follow policy, who have biases they let impact their judgment."
"At the end of this, I can't see any way in which this actually harms the state of Massachusetts or the people of Massachusetts."

### AI summary (High error rate! Edit errors on video page)

Acknowledges being critical of law enforcement but praises the local departments for being responsive to the community and quick to address issues.
Shares an incident where a deputy planting evidence was caught and dealt with swiftly without public outrage.
Talks about the SWAT team in a nearby jurisdiction made up of ex-military deputies who train for high-risk scenarios to protect the public, despite the dangers involved.
Critiques the Massachusetts State Police union's claim of officers resigning over vaccine mandates, contrasting the union's rhetoric with the reality of only one officer actually resigning.
Questions the commitment of officers who refuse to follow mandated policies and suggests that it may indicate biases affecting their work on the streets.
Argues that officers unwilling to mitigate risks to the public by following policy should not be in law enforcement and sees their resignation as a way to weed out those not fit for the job.
Expresses that enforcing unjust laws and refusing to follow safety protocols are different and believes that officers who can't follow safety protocols shouldn't be in the force.
Concludes that even if multiple officers were to resign over policy disagreements, it wouldn't harm the state or its people, seeing it as a potential positive in removing those not adhering to policy.

Actions:

for community members,
Hold local law enforcement accountable for following mandated policies and ensuring public safety (implied).
</details>
<details>
<summary>
2021-09-28: Let's talk about General Milley and some dude in a shed.... (<a href="https://youtube.com/watch?v=gMV84EZMGGM">watch</a> || <a href="/videos/2021/09/28/Lets_talk_about_General_Milley_and_some_dude_in_a_shed">transcript &amp; editable summary</a>)

Beau debunks sensationalism around General Milley's routine military calls, criticizing profit-driven news outlets for creating unnecessary panic.

</summary>

"Maybe General Milley was just doing his job."
"They're trying to get ratings. They are sensationalizing the run-of-the-mill, the normal."
"Calls like this between militaries probably occur on a weekly basis."
"It's not abnormal. It's not a scandal. It's not a story."
"The scandal profit motivator is probably the more likely scenario."

### AI summary (High error rate! Edit errors on video page)

General Milley testified about his phone call with his Chinese counterpart.
Someone messaged Beau, comparing General Milley's testimony to Beau's videos.
The comparison suggests that General Milley and Beau may be sharing talking points.
Beau argues that General Milley was just doing his job, suggesting the calls are routine.
Beau mentions having friends in D.C. but denies receiving classified information from them.
He implies that news outlets sensationalize routine activities for profit.
Beau points out that calming nerves and de-escalation are key in military communications.
He criticizes news outlets for not providing the full context of routine military calls.
Beau believes the scandal narrative is driven by profit-seeking news outlets.
He suggests that sensationalizing normal activities creates unnecessary panic among viewers.

Actions:

for news consumers,
Fact-check news stories before reacting (implied)
Support independent media sources (implied)
Stay informed about routine government activities (implied)
</details>
<details>
<summary>
2021-09-27: Let's talk about NYC street vendors.... (<a href="https://youtube.com/watch?v=0Fblp2JHhsg">watch</a> || <a href="/videos/2021/09/27/Lets_talk_about_NYC_street_vendors">transcript &amp; editable summary</a>)

Beau questions the true motive behind NYC's food vendor permits, suggesting they prioritize revenue over safety, impacting access to fresh produce.

</summary>

"Buying veggies and fruit out of the back of a pickup truck is incredibly common where I'm at."
"Generally what keeps the food safe is the fact that it's really bad business if your customers get sick."
"So if it's not about food safety what is it about? What's it really about? Bringing in money."
"This to me is a major issue when you are talking about an area that doesn't have a lot of fresh fruit."
"Non-profits have been started to help people navigate the maze."

### AI summary (High error rate! Edit errors on video page)

Saw sanitation workers in New York City throwing away food from a produce stand, which is common in rural areas.
Mentioned buying seafood and produce from pickup trucks without permits where he's from.
People defended the permitting process in NYC for food safety after Beau's comments.
Referenced a 2011 cantaloupe listeria outbreak to explain the focus on food safety.
Criticized the notion that vendor permits in NYC ensure food safety, calling it more of a theater.
Argued that business incentives and competition may be the real reasons behind vendor permits.
Raised concerns about the complex permitting process affecting access to fresh fruit in food deserts.
Mentioned non-profits like the street vendor project helping navigate the permitting maze.
Suggested that the permitting process in NYC may be more about revenue for the city than safety.
Noted a case in the Bronx where community pushback prevented sanitation workers from trashing a stand.

Actions:

for community members, food advocates,
Support non-profits like the street vendor project in aiding people to navigate complex permitting processes (suggested).
Advocate for simplified and fair food vendor permit regulations to improve access to fresh produce in underserved areas (implied).
</details>
<details>
<summary>
2021-09-26: Let's talk about how white people should respond to racism they see.... (<a href="https://youtube.com/watch?v=YEbp549A2oA">watch</a> || <a href="/videos/2021/09/26/Lets_talk_about_how_white_people_should_respond_to_racism_they_see">transcript &amp; editable summary</a>)

When witnessing racism, white people should respond immediately using tactics like misunderstanding bigoted jokes or direct corrections to make a difference and mitigate harm.

</summary>

"Do something to mitigate it, to end it, to redirect it, to diffuse the situation."
"There's a whole bunch of different tactics, and it depends on the kind of person you are."
"How do you respond immediately?"
"The answer is immediately."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Exploring how white people should respond to racism when they witness it.
Advises using tactics like pretending not to understand bigoted jokes to make the perpetrator explain the underlying racism.
Suggests using sarcasm to convey that racist behavior is unacceptable within a group.
Talks about direct correction either by firmly stating not to talk like that in front of them or educating the person about their racist behavior.
Mentions the exhausting task of explaining basic concepts of humanity to people repeatedly.
Recommends stepping in to help mitigate risk if a situation escalates and could turn violent.
Shares a personal experience of witnessing a racist altercation and the importance of immediate response to racism.
Emphasizes the importance of taking action immediately to make life more tolerable for those facing racism.

Actions:

for white allies,
Immediately respond to instances of racism by using tactics like sarcasm, misunderstanding bigoted jokes, or direct corrections to combat it and make spaces more inclusive (implied).
</details>
<details>
<summary>
2021-09-26: Let's talk about Jonathan Swift and reasoning with people.... (<a href="https://youtube.com/watch?v=aA4l8k3WfDw">watch</a> || <a href="/videos/2021/09/26/Lets_talk_about_Jonathan_Swift_and_reasoning_with_people">transcript &amp; editable summary</a>)

Beau challenges the idea that it's impossible to reason with those who weren't reasoned into their beliefs, advocating for the power of reason and urging continued efforts to persuade through logic.

</summary>

"It is useless to attempt to reason a man out of a thing he was never reasoned into."
"People can be reasoned with, most, not all."
"We don't have an option because we're not going to get them out of their position by appealing to their unreasonable nature."

### AI summary (High error rate! Edit errors on video page)

Talks about Jonathan Swift's popular quote regarding reasoning people out of beliefs.
Disagrees with the notion that it's useless to reason with those who weren't reasoned into their beliefs.
Believes in the power of reason and facts to counter blind belief.
Mentions the difficulty in reasoning with certain demographics in the United States.
Argues that most people can be reached with the right combination of reason, fact, truth, and perhaps a bit of fiction.
Points out historical proof that reason can help people overcome unreasonable beliefs.
Encourages trying to reason with others, even when it seems frustrating.
Emphasizes the importance of not giving up on trying to persuade others with reason.

Actions:

for skeptics and advocates for evidence-based reasoning.,
Engage in constructive dialogues with those holding differing beliefs (implied).
Use a combination of reason, facts, and possibly some storytelling to convey your point effectively (implied).
Persist in attempting to reason with others, even when faced with resistance (implied).
</details>
<details>
<summary>
2021-09-25: Let's talk about water in the southwest US.... (<a href="https://youtube.com/watch?v=G048s_OF3VU">watch</a> || <a href="/videos/2021/09/25/Lets_talk_about_water_in_the_southwest_US">transcript &amp; editable summary</a>)

Beau warns of impending water shortages in the Southwest, urging immediate action to prevent severe impacts and mitigate climate issues.

</summary>

"This isn't going to fix itself."
"Maybe we should just go ahead and start working on the infrastructure to fix these issues now."
"We need significant action and we need it soon."

### AI summary (High error rate! Edit errors on video page)

Talks about possible water shortages in the Southwestern United States and how it might impact Lake Powell and Mead.
Projects a 22% chance of Lake Mead dropping below 1,025 feet by 2023, affecting water usage for 25 million people.
Mentions a 66% chance of Lake Mead dropping below 1025 feet by 2025, leading to required water usage reductions.
States a 3% chance of Lake Powell not being able to generate hydroelectric power next year and a 34% chance the following year.
Points out that these projections are based on historic drought levels in paleoclimate records, indicating severe drought conditions.
Emphasizes the need for immediate action to mitigate climate change and address impending resource issues.
Suggests that without action, water scarcity may lead to the costly trucking in of water, exacerbating climate issues.
Urges the initiation of infrastructure projects now to prevent severe impacts in the future.
Expresses concern over the lack of media attention given to such projections, despite their historical accuracy.
Stresses the importance of taking significant and timely action to address the ongoing challenges rather than waiting for a return to normalcy.

Actions:

for residents of the southwestern united states,
Start working on infrastructure projects now to address water scarcity (suggested)
Take significant action immediately to mitigate climate change impacts (suggested)
</details>
<details>
<summary>
2021-09-25: Let's talk about Arizona and Brazil.... (<a href="https://youtube.com/watch?v=ZgrDN37kbPU">watch</a> || <a href="/videos/2021/09/25/Lets_talk_about_Arizona_and_Brazil">transcript &amp; editable summary</a>)

Beau addresses the Arizona audit results, finds them unsurprising, and analyzes a theory involving Jason Miller in organizing the influx of Haitians at the southern border, stressing the need for concrete evidence.

</summary>

"This is like finding out that somebody who believed the earth was flat ran some tests and realized it was round."
"But without evidence, it's a rumor."
"When we're looking at these theories, we have to apply the same standard across the board."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the Arizona audit results, stating that the initial theory behind the audit did not hold up to scrutiny.
He explains that the audit report indicates that Biden not only won but by a larger margin due to counting errors, which Beau finds unsurprising.
Beau draws a comparison to someone discovering the earth is round after believing it to be flat – indicating the lack of surprise regarding the audit results.
Shifting focus to Brazil, Beau talks about a theory implicating Jason Miller, a former Trump campaign official, in organizing the influx of Haitians at the southern border to make Biden look bad.
Beau analyzes the motive behind the theory involving Jason Miller, finding it somewhat plausible given his association with Trump and border security concerns.
He questions the feasibility of the theory, acknowledging that Miller's former position could lend credibility to such an operation.
Beau points out the lack of substantial evidence supporting the theory, noting that Miller's presence in Brazil is not sufficient proof of involvement.
While acknowledging the possibility of political stunts by certain groups, Beau stresses the importance of concrete evidence to lend credence to such theories.
He underscores the need for substantial evidence beyond mere travel records to support claims and elevate them beyond rumors.
Beau concludes by suggesting that until more concrete evidence emerges, the theory surrounding Jason Miller's involvement remains merely an entertaining thought exercise.

Actions:

for analytical viewers,
Investigate and gather concrete evidence to support claims of alleged involvement in political activities (suggested).
</details>
<details>
<summary>
2021-09-24: Let's talk about Rudy being banned from Fox.... (<a href="https://youtube.com/watch?v=w9qTIRE2Yx8">watch</a> || <a href="/videos/2021/09/24/Lets_talk_about_Rudy_being_banned_from_Fox">transcript &amp; editable summary</a>)

Fox News bans Rudy Giuliani due to unsubstantiated claims, signaling a wake-up call for supporters to question beliefs and narratives.

</summary>

"Fox risks financial responsibility if Rudy continues to make unsubstantiated claims."
"Supporters of Rudy and Trump should see this as a wake-up call."
"This should be the moment that you really start to question it."
"They're just other Americans."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Fox News reportedly bans Rudy Giuliani and his son from appearing on the network.
Fox viewers celebrate the ban, but what about those who believe Rudy’s statements?
Fox’s decision could be due to Rudy making slanderous or libelous statements.
Fox risks financial responsibility if Rudy continues to make unsubstantiated claims on air.
The lack of evidence to support Rudy's claims may be the reason behind the ban.
Fox is willing to forego profits, advertising revenue, and viewership to avoid legal trouble.
Supporters of Rudy and Trump should see this as a wake-up call.
Recent events, like the lack of evidence in Arizona, should prompt reevaluation.
Fox's inability to defend Rudy's statements suggests a lack of evidence.
Followers of Rudy should question their beliefs and reexamine their perspectives.
This moment should make people reconsider their views on perceived enemies.
It's a call to question deeply held beliefs and narratives.
Encourages critical thinking and introspection.
Urges viewers to see beyond divisive narratives and propaganda.
Promotes reflection on blind allegiance and misinformation.

Actions:

for supporters, viewers, skeptics,
Question your beliefs and narratives (implied)
Reexamine your perspectives (implied)
Engage in critical thinking (implied)
</details>
<details>
<summary>
2021-09-24: Let's talk about Missouri, a petition, and critical thought.... (<a href="https://youtube.com/watch?v=zfRfRMUb1ic">watch</a> || <a href="/videos/2021/09/24/Lets_talk_about_Missouri_a_petition_and_critical_thought">transcript &amp; editable summary</a>)

Students circulated a petition to reinstate slavery, prompting Beau to question Missouri's stance on critical thinking and race education.

</summary>

"This isn't who we are though, right?"
"I don't know how people can say this isn't who we are, because it certainly seems like that's what Missouri is."
"I think it's time that the elected representatives in the state of Missouri decide whether they want to allow students to be encouraged to critically think."
"Anyway, it's just a thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Students at Park Hill South High School circulated a petition requesting that slavery be reinstated.
The school district is tight-lipped about the petition, calling it a disciplinary issue to protect the students' future.
Missouri recently probed whether students were learning objectionable material about race and proposed an amendment against teaching institutions were racist.
State legislature hearings on race in education mainly consisted of white people.
The state's actions seem to contradict the "this isn't who we are" narrative.
Beau questions if Missouri truly stands against critical thinking and acknowledging the impacts of slavery.
He calls for elected representatives to decide if students should critically think or endorse the pro-slavery petition.

Actions:

for educators, activists, students,
Contact elected representatives in Missouri to advocate for critical thinking and anti-racism education (suggested).
</details>
<details>
<summary>
2021-09-23: Let's talk about why there are more today than in the 1970s.... (<a href="https://youtube.com/watch?v=JtyMHEzOIhE">watch</a> || <a href="/videos/2021/09/23/Lets_talk_about_why_there_are_more_today_than_in_the_1970s">transcript &amp; editable summary</a>)

Beau explains the expansion of vaccinations in the US, attributing it to past successes and effective public health management, not conspiracy.

</summary>

"Everything is commodified. Everything is a business. It absolutely is a business."
"They use it because it works."
"It's nothing conspiratorial."
"Why not use a tool that works?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Exploring the expansion of public health initiatives in the United States over the years.
Responding to a query about the increase in vaccinations from 18 to close to 75 by age 18.
Acknowledging the business aspect of vaccinations in a capitalist system.
Mentioning the immunization of babies before they leave the hospital and even in the womb.
Emphasizing that the rise in vaccinations is not just a business strategy but a response to successful outcomes of past vaccines.
Noting the significant drop in infant mortality rates and childhood mortality over the years, attributing some of it to safety measures like bike helmets and car seat mandates.
Mentioning the decline in cases and deaths due to vaccines pre-1980, prompting the continued pursuit of vaccination as a public health tool.
Quoting a statistic from The Lancet about millions of lives saved by vaccination in low and middle income countries, particularly children under five.
Stressing the effectiveness and non-conspiratorial nature of vaccines in managing public health and mitigating risks.
Concluding with the idea that the use of vaccines is based on their proven success in saving lives and managing public health.

Actions:

for health advocates, policymakers, general public,
Get vaccinated to protect yourself and others (implied)
Support public health initiatives and vaccination programs (implied)
</details>
<details>
<summary>
2021-09-23: Let's talk about our man in Haiti, Daniel Foote.... (<a href="https://youtube.com/watch?v=801iGY0N6FU">watch</a> || <a href="/videos/2021/09/23/Lets_talk_about_our_man_in_Haiti_Daniel_Foote">transcript &amp; editable summary</a>)

Special Envoy to Haiti resigns over treatment of Haitian refugees at the US border, focusing on the imminent danger they face upon return to Haiti while questioning why they are not eligible for asylum.

</summary>

"If you have a US official pointing this out and saying that if they come back they're in imminent danger, how are they not eligible for asylum?"
"You have a US official clearly pointing out that these people coming here looking for help deserve it."

### AI summary (High error rate! Edit errors on video page)

Special Envoy to Haiti Daniel Foote resigned due to the treatment of Haitian refugees at the US southern border.
Foote feels his recommendations were ignored, and his policy initiatives were not given due attention.
He believes that the US government is trying to influence and interfere in Haiti, picking winners and losers.
While media focuses on the inhumane treatment of Haitians at the border, the main issue is the danger faced by those being sent back to Haiti.
American officials in Haiti are in danger due to the security situation, indicating that returning Haitians puts them at risk of loss of life.
Despite this imminent danger, the question arises: why are these individuals not considered eligible for asylum?
Foote suggests that these Haitians qualify for asylum based on the danger they face upon return to Haiti.
The focus should be on saving individual lives at risk rather than just discussing broader policy initiatives for Haiti.
Granting asylum to those who clearly deserve it is a critical duty that the Biden administration and media should prioritize.
Beau questions whether the US is upholding its promises of providing help to those in need as outlined in the Declaration of Independence and the Constitution.

Actions:

for advocates, policymakers, activists,
Advocate for the Biden administration to prioritize granting asylum to Haitian refugees (implied).
Share and raise awareness about the dangers Haitian refugees face upon return to Haiti (suggested).
</details>
<details>
<summary>
2021-09-22: Let's talk about salad dressing and Flynn.... (<a href="https://youtube.com/watch?v=KdbY7LC2xLE">watch</a> || <a href="/videos/2021/09/22/Lets_talk_about_salad_dressing_and_Flynn">transcript &amp; editable summary</a>)

Beau criticizes the spread of baseless vaccine conspiracy theories using salad dressing and warns about the detrimental impact on public trust and media integrity in the conservative movement.

</summary>

"These people are seriously thinking about how to impose their will on our society and it has to stop."
"When you have a group of people who are just intent on making a demographic doubt and fear everything, people who like to fashion themselves as macho tough guys are now going to be scared of salad dressing."
"We're going to have to come to a reckoning with the way our media operates."
"There's no end to it. It will just continue to go down further and further as people try to be more and more extreme to gather more of the ratings."
"This is the state of the conservative movement in the United States today."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the issue of salads and salad dressing in relation to public health, sparked by a baseless claim made by Michael Flynn.
Michael Flynn mentioned a conspiracy theory about putting the vaccine in salad dressing, leading Beau to criticize the manipulation of information.
The theory likely originated from a need to transport mRNA vaccines without deep freezing, but was twisted into misinformation.
Beau questions the feasibility of targeting Trump supporters with salad, suggesting hamburgers or animal medication as more plausible options if the absurd theory were true.
He believes that Flynn is intentionally spreading falsehoods to manipulate the most gullible individuals, eroding their trust in reality.
Beau warns about the long-term consequences of perpetuating such misinformation and calls for media accountability in not promoting sensationalism for ratings.
He criticizes the current state of the conservative movement in the United States, where manipulation and sensationalism seem to dictate the discourse.
Beau concludes with a reflection on the need to address these issues and encourages viewers to think critically about the information they receive.

Actions:

for media consumers,
Challenge misinformation by fact-checking and holding media outlets accountable for promoting sensationalism (implied)
Encourage critical thinking and skepticism towards outlandish claims circulating in the media (implied)
</details>
<details>
<summary>
2021-09-22: Let's talk about Biden's polls and Trump endorsements.... (<a href="https://youtube.com/watch?v=KEIOdImt-Z4">watch</a> || <a href="/videos/2021/09/22/Lets_talk_about_Biden_s_polls_and_Trump_endorsements">transcript &amp; editable summary</a>)

Biden's poll dip below 50% and the perception of Trump's influence may not play out as anticipated, impacting future elections.

</summary>

"People showed up to vote against Trump rather than specifically for Biden."
"Trumpism never got sold. It never got mainstreamed."
"Biden falling slightly below Trump's highest approval rating isn't quite the sign that many people may be looking for."

### AI summary (High error rate! Edit errors on video page)

Biden's poll numbers have slipped below 50% approval, hitting around 48% for the first time.
Right-wing pundits see this as the end of the Biden administration, but Biden's lowest poll numbers are still one point above Trump's highest.
Trump remains an energizing force within Republican circles, with many candidates needing his endorsement to win a primary.
However, Trump's most significant impact is negative voter turnout, similar to Hillary Clinton's effect.
People showed up to vote against Trump rather than specifically for Biden.
While a Republican candidate may need Trump's endorsement to win a primary, they might struggle in a general election due to the negative aspects associated with Trump.
The belief in the Republican echo chamber is that Trump is widely supported, but in reality, the majority of Americans never backed him.
Biden falling slightly below Trump's highest approval rating does not necessarily indicate a significant shift.
Trump loyalists being more vocal may inadvertently swing midterms and the 2024 election towards Democrats.
Trumpism never gained widespread support or mainstream acceptance among the majority of Americans.

Actions:

for political analysts,
Analyze and understand the dynamics of voter turnout and candidate endorsements (implied)
</details>
<details>
<summary>
2021-09-21: Let's talk about nurses in Indiana quitting.... (<a href="https://youtube.com/watch?v=w7Bbe6RT-OQ">watch</a> || <a href="/videos/2021/09/21/Lets_talk_about_nurses_in_Indiana_quitting">transcript &amp; editable summary</a>)

Beau clarifies misconceptions about nurses quitting over vaccines, showing overwhelming support for vaccination among medical professionals.

</summary>

"There isn't real debate about this."
"You're talking about 125 employees."

### AI summary (High error rate! Edit errors on video page)

Addressing claims about nurses quitting due to vaccine refusal in Indiana.
Indiana Health University had 125 part-time employees who didn't provide proof of vaccination.
No indication that the employees were nurses or that they didn't believe in the vaccine.
Even if the claim were true, it's only a small percentage of the total staff.
Over 99.5% of the employees at that location were vaccinated.
Media may create an implied debate where there isn't one.
The majority of medical professionals support vaccination.
The situation with the 125 employees doesn't indicate a real debate within the medical community.
Speculations on reasons for not providing vaccine documentation.
More than 99.5% vaccination rate among 34,000 employees.

Actions:

for healthcare professionals,
Verify information before spreading it (implied)
Support vaccination efforts in your community (implied)
</details>
<details>
<summary>
2021-09-21: Let's talk about a brief history of mRNA.... (<a href="https://youtube.com/watch?v=PuDUHbmEl7E">watch</a> || <a href="/videos/2021/09/21/Lets_talk_about_a_brief_history_of_mRNA">transcript &amp; editable summary</a>)

Beau explains the extensive history of mRNA technology, debunking the notion of rushed research for COVID-19 vaccines and encouraging understanding past research for future progress.

</summary>

"This isn't actually new technology. It's been around for a really, really long time."
"The basis of this research, it started before the Cuban Missile Crisis."
"Sometimes looking back can help you look forward a little bit."

### AI summary (High error rate! Edit errors on video page)

Explains the timeline of research on mRNA vaccines dating back to the 1960s.
Mentions emergency FDA authorization for COVID-19 vaccines in 2020.
Points out that mRNA technology is not as new as perceived.
Describes the history of mRNA vaccine trials on mice and humans.
States that the basis of this research predates major historical events like the Cuban Missile Crisis.
Encourages not to hesitate based on the longevity of research.
Suggests that understanding past research can aid in looking towards the future.
Emphasizes that looking back at historical context is beneficial for understanding current advancements.

Actions:

for research enthusiasts,
Educate others on the historical timeline of mRNA vaccine research, exemplified
Encourage individuals to look into the background of scientific advancements, exemplified
</details>
<details>
<summary>
2021-09-20: Let's talk about Trump vs McConnell.... (<a href="https://youtube.com/watch?v=uft0Mbv0rK8">watch</a> || <a href="/videos/2021/09/20/Lets_talk_about_Trump_vs_McConnell">transcript &amp; editable summary</a>)

Beau analyzes the GOP defensive strategies involving Trump and McConnell, predicting a potential Republican Civil War with McConnell likely coming out on top.

</summary>

"Trump is actively seeking people to depose Mitch McConnell trying to build a coalition to get rid of him."
"McConnell has to respond and treat Trump as opposition rather than an ally."
"Trump could trigger results that are unfavorable to Republican candidates he doesn't like."
"If this proceeds the way it looks like it's going to, they're going to end up fighting each other as much as they're going to be fighting the Democrats."
"I mean, Trump couldn't even get re-elected, much less take down a DC establishment insider."

### AI summary (High error rate! Edit errors on video page)

Analysis of the GOP defensive strategies involving Trump and McConnell.
Trump actively seeking to depose McConnell and build a coalition against him.
McConnell forced to treat Trump as opposition due to public split.
Trump influencing election seats with loyal candidates, potentially impacting primaries.
Potential Republican Civil War between Trump and McConnell.
McConnell's political savvy and ability to keep secrets.
Impact on midterms, with Republicans potentially fighting each other more than Democrats.
Beau bets on McConnell coming out on top in the GOP internal conflict.
Implications for the power dynamics within the Republican Party.

Actions:

for political analysts, republicans, democrats,
Organize or support candidates for election seats with integrity and dedication (implied)
Stay informed and engaged in the upcoming midterms (implied)
</details>
<details>
<summary>
2021-09-20: Let's talk about Biden not following the science.... (<a href="https://youtube.com/watch?v=DPUCQe8DH40">watch</a> || <a href="/videos/2021/09/20/Lets_talk_about_Biden_not_following_the_science">transcript &amp; editable summary</a>)

The Biden administration's prioritization of booster shots over global vaccine distribution raises ethical questions around nationalistic lens on science and medical ethics.

</summary>

"The US government is going to look at the science through a nationalistic lens."
"It's about ethics, about what you do with science, and whether or not that's bad."
"There are ways to get the most vulnerable populations in the U.S., get them their booster shot, and send doses overseas at the same time."
"Just go get it quickly so they can begin getting it overseas."
"They're going to push even more heavily and keep more doses here."

### AI summary (High error rate! Edit errors on video page)

The Biden administration's decision to prioritize booster shots over sending doses overseas is facing pushback from medical professionals and scientists.
The debate is not about the effectiveness of booster shots but about medical ethics and whether it's right to prioritize boosters for Americans over providing initial shots to those overseas.
Beau raises the ethical question of whether fully vaccinated Americans should receive booster shots while many people overseas haven't even had their first shot.
He criticizes the administration's possible nationalistic lens on vaccine distribution, focusing on Americans first.
Beau suggests that getting your booster shot as soon as possible might actually help accelerate overseas distribution rather than delaying it through protest.
Waiting to pressure the government to change its stance on booster shots may backfire and lead to more doses being kept in the US due to increased vaccine hesitancy.

Actions:

for global health advocates,
Get your booster shot as soon as possible to potentially accelerate overseas distribution (implied).
</details>
<details>
<summary>
2021-09-19: Let's talk about Idaho and never being now.... (<a href="https://youtube.com/watch?v=TyMsL169jdk">watch</a> || <a href="/videos/2021/09/19/Lets_talk_about_Idaho_and_never_being_now">transcript &amp; editable summary</a>)

Idaho's crisis standards of care exemplify the consequences of prioritizing "freedom" over public health, urging vaccination and precautions to prevent preventable crises.

</summary>

"Freedom's just another word for nothing left to lose."
"Never is now. Never is now."
"But freedom, right?"
"This is kind of preventable."
"We know what increases survival rates. But freedom, right?"

### AI summary (High error rate! Edit errors on video page)

After discussing a woman explaining healthcare standards at a gas station, many argued against restrictions, claiming it was anti-freedom.
Idaho is currently operating under crisis standards of care, contradicting the belief that such situations wouldn't occur.
An excerpt detailing a Universal Do Not Resuscitate order for adult patients during a public health emergency under crisis standards of care is shared.
The order specifies that aggressive interventions should be provided but no attempts at resuscitation should be made.
The low survival likelihood after cardiac arrest for adult patients and the significant risks to healthcare workers from resuscitation are emphasized.
Beau urges people, especially in Idaho, to get vaccinated, wear masks, and follow necessary precautions to avoid the situation currently faced by Idaho.
He warns against risky activities like riding four-wheelers due to the increased danger posed by the current crisis standards of care.
Beau stresses the preventability of the situation in Idaho through vaccination and following measures known to slow down the spread and increase survival rates.
Despite the availability of vaccines and knowledge on effective preventive measures, the situation in Idaho exemplifies the consequences of prioritizing "freedom" over public health.
Beau concludes by encouraging viewers to take necessary precautions and hints at the irony of valuing freedom while endangering lives.

Actions:

for public health advocates,
Get vaccinated, wear masks, and follow necessary precautions to prevent crises like the one in Idaho (exemplified).
Avoid risky activities that could lead to injury in areas facing crisis standards of care (implied).
</details>
<details>
<summary>
2021-09-19: Let's talk about Biden, mistakes, and the 1950s.... (<a href="https://youtube.com/watch?v=MtxXx4gjYB0">watch</a> || <a href="/videos/2021/09/19/Lets_talk_about_Biden_mistakes_and_the_1950s">transcript &amp; editable summary</a>)

Biden cleans Trump’s messes; Biden's mistakes need cleaning; Policies take time; Conservatives' golden age view; FDR's policies laid groundwork.

</summary>

"United States is not a compact car that can stop on the dime. It's an 18-wheeler."
"The effects of policies, positive or negative, generally speaking, take years to be shown."
"The period conservatives point back to was created by somebody who championed all of the policies they oppose."

### AI summary (High error rate! Edit errors on video page)

Biden administration cleaning up Trump's messes.
Mistakes made now by Biden administration will need cleaning up by next presidency.
United States is not like a compact car but an 18-wheeler, slow to change course.
Policies take time to show results after being enacted.
Effects of policies, positive or negative, take time to manifest.
Conservatives often point to the 1950s as the golden age of the United States.
The golden age reference actually spans from 1945 to early 1960s.
FDR's administration from 1933 to 1945 laid the groundwork for the perceived golden age.
Massive government spending, jobs programs, and high tax rates on the top created prosperity in the past.
Similar policies may create real change and prosperity now post long war and economic issues.

Actions:

for history enthusiasts, political analysts, policymakers.,
Advocate for policies that focus on massive government spending, jobs programs, and high tax rates on the top to create real change (implied).
</details>
<details>
<summary>
2021-09-18: Let's talk about the importance of understanding basic philosophies.... (<a href="https://youtube.com/watch?v=RvFE91UgM6Q">watch</a> || <a href="/videos/2021/09/18/Lets_talk_about_the_importance_of_understanding_basic_philosophies">transcript &amp; editable summary</a>)

Understanding different economic philosophies beyond misconceptions can lead to surprising insights and inform personal beliefs, even for those who may not identify with leftist ideals.

</summary>

"The real problem that no one addresses is the system that allowed the wealth to get sucked up to the top in the first place."
"We have thrown money at these problems, and have only proved they cannot be solved with money."
"You are to the left of me. What you are describing is communism."
"But if you do, you're better informed."
"Don't let the way words get used in common conversation dictate whether or not you're going to look at the philosophy behind that word."

### AI summary (High error rate! Edit errors on video page)

Stresses the importance of understanding different philosophies beyond common misconceptions.
Shares a comment about taxing the rich and the limitations of progressivism.
Talks about the wealth gap and the changing economic landscape in the country.
Describes a homesteader's perspective on value, wealth, and ownership.
Connects the homesteader's ideals to leftist economic philosophies like communism.
Mentions the disconnect between people's actual beliefs and their understanding of economic philosophies.
Encourages gaining a basic understanding of various economic theories and philosophies.
Points out that many rural people unknowingly live by leftist economic ideals in their daily lives.
Addresses the stigma and confusion surrounding leftist economic philosophies in the United States.
Emphasizes the need to look beyond common associations and understand the core philosophy behind terms like communism.

Actions:

for philosophy enthusiasts, economic thinkers.,
Read up on different economic theories and philosophies to gain a better understanding (exemplified).
Challenge common misconceptions about leftist economic ideals in your community (suggested).
</details>
<details>
<summary>
2021-09-18: Let's talk about the GOP dog who caught a car.... (<a href="https://youtube.com/watch?v=vTEo6fchMlQ">watch</a> || <a href="/videos/2021/09/18/Lets_talk_about_the_GOP_dog_who_caught_a_car">transcript &amp; editable summary</a>)

Republicans chase small vocal groups to the ballot box, risking alienating the majority in pursuit of drastic policies with uncertain consequences.

</summary>

"Republicans are a little bit like a dog who finally caught the car and they don't really know what to do right now."
"This is what is a catastrophic success, because long-term I have a feeling this is going to be incredibly damaging to them."

### AI summary (High error rate! Edit errors on video page)

Republicans are likened to a dog who finally caught the car but doesn't know what to do.
Democratic candidates were vocal about police reform pre-election, but backed off when it came to defunding the police.
Only about 20% of Americans support drastic police reforms, while a larger percentage supports smaller reforms.
Republicans pursued policies supported by a small vocal group, like banning certain family planning methods.
The majority, around 80% of people, believe family planning should be legal in some form.
Republicans passed drastic bills on family planning, banking on courts to strike them down, but now face consequences with a conservative Supreme Court.
Politicians often chase small groups to the ballot box on various issues without intending to follow through on drastic policies.
The strategy of appealing to small groups is evident in gun rights issues as well.
Beau questions how Republicans will mitigate the damage caused by their recent actions.
Winning for Republicans in this context is described as a "catastrophic success" with potential long-term damage.

Actions:

for politically aware individuals,
Watch for politicians chasing niche issues and hold them accountable. (implied)
Advocate for policies that represent the majority, not just vocal minorities. (implied)
</details>
<details>
<summary>
2021-09-17: Let's talk about what spears can teach us about creating change.... (<a href="https://youtube.com/watch?v=C4xhJZsmFIY">watch</a> || <a href="/videos/2021/09/17/Lets_talk_about_what_spears_can_teach_us_about_creating_change">transcript &amp; editable summary</a>)

Beau explains the dynamics of social movements through a military analogy, stressing inclusivity and understanding to achieve effective change.

</summary>

"If you're part of the tip of the spear, you're part of the ideological advance. You're always going to be ahead."
"Going after and trashing the people who are helping to carry it out, even if they have a bad take, maybe go a little bit easier on them."
"There shouldn't be a lot of gates thrown up. They should be very accessible."
"You can't hold them accountable for something they literally do not know."
"If you want change, the idea needs to get to as many people as possible."

### AI summary (High error rate! Edit errors on video page)

Addresses the importance of achieving change and how to do it effectively.
Draws an analogy using military terminology, referring to the "tip of the spear."
Explains the different roles within social movements: the tip, the rest of the spear, and the support staff.
Emphasizes the significance of not alienating those who may not fully understand the movement's ideologies.
Talks about the negative effects of trashing individuals who are helping in carrying out the message of change.
Urges for more inclusivity and understanding within progressive movements.
Stresses the need for ideas to reach a wider audience to create real change.
Critiques the infighting and ideological purity that can slow down progress.
Encourages being less critical and creating a more welcoming environment for those willing to support the cause.

Actions:

for change advocates,
Support and embrace individuals willing to carry out the message of change, even if they may not fully understand the ideologies (suggested).
Encourage inclusivity and understanding within progressive movements (exemplified).
</details>
<details>
<summary>
2021-09-17: Let's talk about a simple tactic for inspiring people.... (<a href="https://youtube.com/watch?v=xeHJHxe3VVU">watch</a> || <a href="/videos/2021/09/17/Lets_talk_about_a_simple_tactic_for_inspiring_people">transcript &amp; editable summary</a>)

Inspirational story of an instructor calling everyone "Ranger" to set high expectations, mirroring Beau's approach in encouraging growth and self-improvement.

</summary>

"He wanted to inspire them. He wanted to set the expectation that they could and that they should."
"It doesn't cost me anything to come from the point of view that they can and that they want to."
"He puts it out there that he believes that every single one of them can make it."
"I want to set the expectation that that is what they want to do and that they can."
"He was somebody who was foundational to them changing and to them really seeking to strive to hit that next level."

### AI summary (High error rate! Edit errors on video page)

Describes a story about an inspiring instructor at Fort Benning who called everyone "Ranger," setting the expectation that they could achieve becoming a Ranger.
The instructor's simple tactic of using the term "Ranger" instilled inspiration and motivation in all his students, regardless of their current status.
Becoming a Ranger at Fort Benning is challenging, and the instructor's belief in each individual's potential motivated them.
Beau relates this story to his approach in his videos, assuming viewers are seeking to improve themselves and aiming to set the expectation that they can grow.
Despite knowing that not all viewers will change their perspectives, Beau operates on the belief that they want to better themselves and encourages them to do so.

Actions:

for viewers seeking self-improvement,
Encourage and inspire others by setting high but achievable expectations for them (exemplified).
</details>
<details>
<summary>
2021-09-16: Let's talk about comedians and journalists.... (<a href="https://youtube.com/watch?v=5OSJNOSFHFI">watch</a> || <a href="/videos/2021/09/16/Lets_talk_about_comedians_and_journalists">transcript &amp; editable summary</a>)

Beau questions the blend of journalism and comedy, praises comedians for upholding journalistic ethics, and criticizes the industry's struggle with objectivity and profitability.

</summary>

"They're trying to get off the sidelines."
"Once you acknowledge that objective journalism really isn't a thing, you know it's subjective."
"If you end up with a huge platform like these comedians have, you'd have to be kind of a horrible person not to want to help."
"At some point that got lost."
"They're trying to avoid comments that say, well why don't you tell us the other side to this thing that is an objective fact."

### AI summary (High error rate! Edit errors on video page)

Questions the distinction between journalism and comedy in progressive TV programs like John Oliver, Stephen Colbert, and Trevor Noah.
Points out that comedians often adhere to journalistic ethics better than major networks.
Talks about the challenges of achieving objectivity in journalism and praises the Associated Press for striving towards journalistic ideals.
Mentions the subjective nature of journalism and the importance of making editorial decisions.
References Hunter S. Thompson and Gonzo journalism, which combines facts with a touch of fiction and humor to get to the truth.
Applauds comedians for using their platform to better the world and move beyond being mere spectators.
Addresses the difficulty of balancing objectivity and profitability in journalism.
Criticizes the current industry practice of presenting "both sides" of an issue, which may not always be objective or accurate.
Points out that many issues have more than two sides and some may only have one.
Talks about journalists pursuing independent avenues to get closer to the truth outside traditional media constraints.

Actions:

for content creators, journalists, viewers,
Support independent journalists and content creators who strive for truth and ethics (implied)
Encourage critical thinking and fact-checking in media consumption (implied)
Advocate for diverse perspectives and nuanced storytelling in journalism (implied)
</details>
<details>
<summary>
2021-09-16: Let's talk about NVGs and masculinity.... (<a href="https://youtube.com/watch?v=egmQG5V1UVk">watch</a> || <a href="/videos/2021/09/16/Lets_talk_about_NVGs_and_masculinity">transcript &amp; editable summary</a>)

Beau challenges the American mindset that owning equipment equals capability, stressing the importance of training over tools and critiquing the influence of this concept on foreign policy and domestic rhetoric.

</summary>

"The gun doesn't make the man. The tools don't necessarily mean you know how to use it."
"The reason those special teams, those high-speed teams, the reason they're special is because if need be they can accomplish the mission without all that stuff."
"The problem is this idea is now so ingrained in American culture that it's influencing foreign policy decisions."
"The militarization of the United States, the habit of conflating masculinity with violence, this needs to stop."
"It's not how it works. It's the training. It's the knowledge."

### AI summary (High error rate! Edit errors on video page)

Addressing the concept of owning the night with NVGs left behind in Afghanistan.
Providing examples of equipment like a medic's bag or an electrician's bag to illustrate the importance of training over tools.
Critiquing the American mindset that having equipment equates to capability.
Pointing out the fallacy in thinking that owning NVGs means owning the night.
Emphasizing the significance of training and knowledge over equipment in achieving missions.
Calling out the influence of this mindset on foreign policy decisions and domestic rhetoric.
Challenging the habit of associating masculinity with violence and the militarization of the United States.
Asserting that possessing tools does not automatically translate to proficiency in using them.
Suggesting that opposition forces in Afghanistan are unlikely to dramatically change tactics due to acquiring night vision equipment.
Concluding with the idea that tools do not make one capable, but rather training and knowledge do.

Actions:

for americans,
Challenge the mindset that owning equipment automatically equals capability (implied).
Advocate for investing in training and knowledge over solely focusing on equipment (implied).
Educate others about the importance of skills and expertise in achieving missions effectively (implied).
</details>
<details>
<summary>
2021-09-15: Let's talk about the California recall and what it means for Trump.... (<a href="https://youtube.com/watch?v=1mTODFOCOZo">watch</a> || <a href="/videos/2021/09/15/Lets_talk_about_the_California_recall_and_what_it_means_for_Trump">transcript &amp; editable summary</a>)

Beau questions the narrative of the California recall election being a rebuke of Trumpism and warns against underestimating its influence.

</summary>

"The storyline that's going along with this is that this is a rebuke of Trumpism and it shows that Trumpism is on the decline. I don't know about that."
"I don't see the decline of Trumpism because of this election. Best case scenario, we're holding the line. Worst case, Trumpism is gaining a little bit of ground."
"I certainly don't see a reason to celebrate."

### AI summary (High error rate! Edit errors on video page)

Providing insights on the California recall election results and the storyline accompanying them in reporting.
Newsom, a Democrat, won the recall election with 65% of the vote.
The storyline suggests it as a rebuke of Trumpism and evidence of its decline, which Beau questions.
Only 34.8% (Washington Post) and 35% (FiveThirtyEight) voted in favor of the recall, indicating a small percentage.
Beau doesn't agree that a Democrat winning in California is surprising or indicative of a rebuke of Trumpism.
Trump had received 34.3% of the vote in 2020, suggesting that Trumpism gained ground in California.
Beau warns against underestimating ideologies fueled by cults of personality throughout history.
He sees the election as holding the line against Trumpism at best, with a possibility of it gaining ground.
The percentage of votes may change, but it seems like nothing significant has changed since 2020.
Beau questions the widely reported storyline portraying the election as a victory against Trumpism.
He doesn't believe the outcome in California should be a significant barometer for the national political discourse.
Beau doesn't see a reason to celebrate as the numbers haven't significantly changed compared to 2020.

Actions:

for political analysts,
Analyze the election results and narratives critically (suggested)
Stay informed about political developments beyond surface-level narratives (suggested)
</details>
<details>
<summary>
2021-09-15: Let's talk about General Milley's actions.... (<a href="https://youtube.com/watch?v=b78LChZNiOw">watch</a> || <a href="/videos/2021/09/15/Lets_talk_about_General_Milley_s_actions">transcript &amp; editable summary</a>)

General Milley took necessary actions post-January 6th to protect the Constitution, despite facing accusations of treason for preventing unconstitutional power retention by the former president.

</summary>

"Milley did his job."
"He didn't have a right to do the things that he did. He had a duty to."
"They want to point to this conversation he had with his Chinese counterpart. Yeah, you know, when the President of the United States starts ranting about election fraud and saying that, you know, he should kind of retain power, I woul imagine that other countries get really nervous about the status of the most powerful arsenal in all of human history."

### AI summary (High error rate! Edit errors on video page)

General Milley took actions post-January 6th to prevent the former president from retaining power unconstitutionally.
People are calling Milley a traitor and suggesting charging him with treason for his actions.
Milley, who could have retired comfortably, continued his service for an additional 20 years.
Accusations of treason against Milley overlook the narrow constitutional definition of the crime.
Milley's actions were in line with his oath to support and defend the Constitution against all enemies.
His communication with his Chinese counterpart was to provide assurances due to concerns about the US arsenal under a President talking election fraud.
Milley's actions were not a surprise; he had communicated his intentions in a memo on January 12th.
Punishing or asking for Milley's resignation is a way to cover up failures and lack of duty by others.
The focus on pinning blame on Milley after 40 years of service seems misplaced.

Actions:

for politically aware citizens,
Support leaders who prioritize upholding the Constitution (exemplified)
Defend against misinformation and hold accountable those who neglect their duties (exemplified)
</details>
<details>
<summary>
2021-09-14: Let's talk about how black people should respond to racism.... (<a href="https://youtube.com/watch?v=Apn6wXIO--4">watch</a> || <a href="/videos/2021/09/14/Lets_talk_about_how_black_people_should_respond_to_racism">transcript &amp; editable summary</a>)

Beau addresses the challenges faced by black men when responding to racial slurs and violence, pointing towards internal reflection and positive messaging as potential paths forward.

</summary>

"Nobody likes an angry black man."
"How do we deal with this literal torture?"
"I don't have a solution. I don't have any advice."
"Sometimes the question alone is the thought."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Beau receives a message asking how black men should respond to being called the word "boy" and the subsequent attacks that follow.
The message describes the internal struggle of feeling tense, accused, and pushed towards violent reactions when faced with racism.
Beau shares his personal experience of limited encounters with racism compared to the continuous discrimination faced by black men.
He acknowledges the privilege of being able to respond violently without severe consequences due to his skin tone.
Beau points out the hypervigilance and constant readiness for attacks that result in a form of PTSD from racial discrimination.
Despite not having a clear solution, Beau encourages the message sender to use platforms like YouTube to convey positive messages without filtration.
He suggests that finding solutions to dealing with racism may be an internal journey rather than seeking external advice.

Actions:

for black men, allies,
Utilize platforms like YouTube to convey positive messages without filtration (suggested)
Engage in internal reflection on how to respond to racial discrimination (implied)
</details>
<details>
<summary>
2021-09-14: Let's talk about AOC's dress.... (<a href="https://youtube.com/watch?v=z5oPjR5D35w">watch</a> || <a href="/videos/2021/09/14/Lets_talk_about_AOC_s_dress">transcript &amp; editable summary</a>)

AOC's dress controversy at the Met Gala showcases media spin and false implications, revealing the attempt to fabricate outrage.

</summary>

"Telling a bunch of rich people to their face through a dress that you think they should be taxed more is also not hypocritical. That's remaining principled."
"This just goes to show how the media can spin a story or fabricate one."
"It's designed to provoke outrage where there is none."

### AI summary (High error rate! Edit errors on video page)

AOC wore a dress saying "tax the rich" to the Met Gala, sparking controversy.
Right-wing attacks often target AOC, trying to paint her as an outraged elitist.
Wearing a message like "tax the rich" while being wealthy isn't hypocritical; it's ideologically consistent.
Beau addresses the implication that AOC spent $30,000 on her Met Gala ticket.
He suggests that even if she spent that money, donating it to a cause like a museum wouldn't be a bad thing.
Beau points out the fuzziness between politicians and celebrities, implying that AOC likely got her ticket for free.
The media's spin on the story can distort the reality of the situation.
Beau criticizes the outrage-driven coverage and the attempt to create controversy where there is none.

Actions:

for media consumers,
Support museums or causes like the Costume Institute (exemplified)
Question media narratives and seek out multiple sources (exemplified)
</details>
<details>
<summary>
2021-09-13: Let's talk about soft language and sensationalism.... (<a href="https://youtube.com/watch?v=dUEQveTKH90">watch</a> || <a href="/videos/2021/09/13/Lets_talk_about_soft_language_and_sensationalism">transcript &amp; editable summary</a>)

Beau explains the power of words, advocating for softer rhetoric to foster critical thinking and open debate, aiming to change minds through reason over emotion.

</summary>

"If you make it through the first 30 seconds, you will make it through the main body."
"That's more my goal."
"It just creates people who are thinking on their own."
"If people are thinking reasonably and rationally, there's room for discussion and debate."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Talks about the power of words and their meanings, focusing on keeping people engaged in a discourse.
Mentions receiving a message questioning his avoidance of certain terms, advocating for precise and direct communication.
Explains his choice of using softer and less emotionally charged words to maintain audience engagement.
Shares insights from analytics, particularly the significance of emotionally charged words in retaining viewers' attention.
Describes a shift in his rhetoric from using inflammatory and emotional language to softer tones to encourage critical thinking.
Conveys the importance of reason and rationality in changing minds rather than emotional appeals.
Refers to a poll indicating the impact of softer versus strong rhetoric on public opinion.
Advocates for employing soft language in everyday communication to foster independent thinking.
Acknowledges the limitation of soft rhetoric in creating like-minded individuals but values critical thinking and open debate.

Actions:

for communicators and debaters,
Adjust your communication style to use softer and less emotionally charged words to encourage critical thinking and open debate (suggested).
</details>
<details>
<summary>
2021-09-13: Let's talk about problems big and small.... (<a href="https://youtube.com/watch?v=_b7Uf3ZnoKw">watch</a> || <a href="/videos/2021/09/13/Lets_talk_about_problems_big_and_small">transcript &amp; editable summary</a>)

A reminder from Beau to focus on making small but meaningful impacts, knowing you can't solve every problem in the world.

</summary>

"You do what you can, when you can, where you can, for as long as you can."
"Don't get discouraged because you can't solve every problem."
"Focus on the ones you can solve."
"Once you find it, stick to it."
"And you'll find your calling, the thing that you can do the most good with."

### AI summary (High error rate! Edit errors on video page)

A friend accidentally started a community network to build wheelchair ramps for those in need.
The group faces additional problems at each house they visit beyond accessibility issues.
Often, they can't solve all the problems they encounter, leading to feelings of inadequacy.
Despite the challenges, Beau believes in the impact of small actions on people's lives.
He acknowledges that individuals or small groups can't solve all the world's problems.
Beau stresses the importance of doing what you can with the means and resources available.
He encourages not to get discouraged by the inability to solve every problem.
Small acts of kindness may have a significant impact on someone's life.
Beau urges people to focus on the issues they can solve and not be overwhelmed by the abundance of problems.
He advises taking any starting point and getting active, as more opportunities to help will arise.

Actions:

for community members,
Start a community network to address specific local needs (suggested)
Help build wheelchair ramps for those in need (suggested)
Get involved in food banks or similar initiatives to support communities (suggested)
Focus on solving accessible problems in your community (exemplified)
</details>
<details>
<summary>
2021-09-12: Let's talk about trying to debunk a claim about Delta.... (<a href="https://youtube.com/watch?v=fuQEfEvFJfc">watch</a> || <a href="/videos/2021/09/12/Lets_talk_about_trying_to_debunk_a_claim_about_Delta">transcript &amp; editable summary</a>)

Beau investigates a claim about vaccines causing the Delta variant, debunks it with a timeline, and stresses the importance of fact-checking.

</summary>

"Life will find a way type of thing."
"So no, the vaccines did not cause the Delta variant."
"Have you ever tried to prove a negative?"
"Why don't you fact-check before you open your ugly blank, blank, blank mouth?"
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Investigating a claim about vaccines causing the Delta variant due to viral mutation.
Acknowledging the plausibility of viruses mutating to survive.
Exploring the concept of proving a negative claim and the challenge it presents.
Emphasizing the importance of using a timeline as a scientific tool in investigations.
Pointing out the global nature of the issue and the timeline of the Delta variant's identification in India before vaccines.
Clarifying that the mechanics of evolution apply to natural immunity as well, leading to various flu strains.
Asserting that vaccines did not cause the Delta variant, proven by a simple timeline.
Suggesting fact-checking before making unfounded claims.

Actions:

for science enthusiasts, vaccine skeptics,
Fact-check claims before spreading misinformation (suggested)
Utilize timelines as a tool for investigating scientific claims (suggested)
</details>
<details>
<summary>
2021-09-10: Let's talk about Biden, mandates, reactions, and Orwell.... (<a href="https://youtube.com/watch?v=qu3-fXVMtdk">watch</a> || <a href="/videos/2021/09/10/Lets_talk_about_Biden_mandates_reactions_and_Orwell">transcript &amp; editable summary</a>)

Beau delves into reactions towards Biden’s mandates, exposing societal vulnerabilities to manipulation and misinformation, urging critical thinking to combat divisive narratives.

</summary>

"The party told you to reject the evidence of your eyes and ears. This was its final and most essential command. That's Orwellian."
"People are denying the evidence of their eyes and ears."
"We failed to create a society that can critically think, that can see through fear-mongering media, that can do simple math."

### AI summary (High error rate! Edit errors on video page)

Exploring reactions to Biden's mandates and the underlying reasons behind them.
Three main reactions: support, conflict due to ideological reservations, and calling it Orwellian.
Majority cheering on Biden's administration for taking action through mandates.
Some conflicted individuals balancing bodily autonomy beliefs with the necessity of the mandates.
Others view the mandates as Orwellian and tyrannical, lacking an understanding of the term.
Media coverage lacks accountability for the role it played in shaping public opinion on vaccination.
Medical and scientific communities share blame for poor messaging, combating sensationalism.
Society's susceptibility to emotional manipulation and division contributes to the mandates' controversy.
Orwellian references not just about dictatorship but manipulation and control of information.
Criticizes those who deny factual evidence regarding vaccination survival rates.
Points out the failure to foster critical thinking in society leading to the current division and misinformation.
Examines the dichotomy between healthcare professionals saving lives and media sensationalizing fear.
Urges for a society capable of discerning truth from manipulated narratives.

Actions:

for public, voters, activists,
Challenge misinformation spread in your community by engaging in fact-based dialogues and sharing credible sources (implied).
Foster critical thinking skills by organizing community workshops or educational sessions on media literacy and discerning misinformation (suggested).
</details>
<details>
<summary>
2021-09-09: Let's talk about triage at a gas station.... (<a href="https://youtube.com/watch?v=xE5N-Z9Ud6A">watch</a> || <a href="/videos/2021/09/09/Lets_talk_about_triage_at_a_gas_station">transcript &amp; editable summary</a>)

Beau shares a gas station encounter illustrating the importance of clarifying misconceptions about healthcare triage during a pandemic.

</summary>

"Jeremy, you go get your mask on."
"That may be something you want to put in your toolbox."
"It's definitely something worth remembering and worth working into..."
"But it definitely gave him something to think about."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Visits a gas station late at night to avoid crowds for personal reasons.
Observes a woman working there checking for masks, interacts with Jeremy.
Jeremy, a large man, enters without a mask, prompted to put one on by the woman.
Woman, a CNA working at gas station, inquires if Jeremy got vaccinated.
Shares about Dr. Fauci discussing potential healthcare triage situations.
Explains to Jeremy the misconception about triage and prioritizing patients.
Encourages Jeremy to understand that being sicker does not necessarily mean getting prioritized.
Suggests that Jeremy's assumptions about triage may not be accurate.
Acknowledges that the woman's explanation gave Jeremy something to think about.
Emphasizes the importance of clarifying misconceptions about healthcare triage to others.

Actions:

for general public,
Clarify misconceptions about healthcare triage in casual, informative ways (implied).
Encourage understanding of healthcare procedures among peers (implied).
</details>
<details>
<summary>
2021-09-09: Let's talk about the word "boy" in the South.... (<a href="https://youtube.com/watch?v=NmReupLUvDE">watch</a> || <a href="/videos/2021/09/09/Lets_talk_about_the_word_boy_in_the_South">transcript &amp; editable summary</a>)

Beau explains the deeply rooted racism behind the word "boy" when used towards black individuals in the southern United States, advising against its usage in any context.

</summary>

"The word boy, when spoken to a black person in the southern United States, especially, is racist."
"Today, most people in the southern United States avoid the word more than you might imagine."
"There's never an appropriate way to use it when talking to a black person."
"The South has a lot of racist history, so there are a lot of words that carry a lot of racist connotation that you may not imagine."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the racist connotations of the word "boy" when spoken to a black person in the southern United States.
Mentions that the history of slavery and segregation is deeply embedded in the racial charge behind the word.
Talks about how the word "boy" was used to belittle and keep black men down during slavery.
Describes how the term "boy" signifies a lack of equality and is a way of asserting superiority.
Points out that even when referring to literal children, the word "boy" is used less frequently now due to its historical context.
Emphasizes that there is never an appropriate way to use the word "boy" when speaking to a black person.
Mentions that the word "boy" is used cautiously by most people in the southern United States due to its racial history.
Advises against using the word "boy" in any context when addressing a black person.
Explains how the tone and context in which the word is said play a significant role in its racial implications.
Concludes by stating that due to its history, the word "boy" may never be fully rehabilitated.

Actions:

for educators, linguists, social activists,
Educate others on the racist connotations of certain words (exemplified)
Promote awareness and sensitivity towards racially charged vocabulary (exemplified)
</details>
<details>
<summary>
2021-09-08: Let's talk about the media being surprised by the predictable.... (<a href="https://youtube.com/watch?v=tcRAD-aVry8">watch</a> || <a href="/videos/2021/09/08/Lets_talk_about_the_media_being_surprised_by_the_predictable">transcript &amp; editable summary</a>)

Beau questions the mock outrage over Afghanistan's government formation and challenges media sensationalism for ratings.

</summary>

"Can you believe that? I mean, I can't think of another country on the planet that would do that."
"It's shocking, I tell you. Shocking."
"If you're surprised by this development, if you're shocked and you believe that this is something Americans should be outraged by, I demand to know who you believed was going to be in these positions."
"They just believe that they can convince the American people to be outraged by it, and therefore milk those ratings."
"Anyway, it's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Explains the inclusion of opposition members in the new government in Afghanistan, suggesting it's unusual.
Compares the incorporation of former conflict figures into the government to practices in other countries.
Expresses mock shock at the idea of war heroes being part of a government, hinting at perceived hypocrisy.
Points out the surprise at former prisoners now holding government positions.
Mocks the media's shock and outrage over the situation, implying it's a common occurrence globally.
Questions why commentators are acting surprised and trying to provoke outrage when such scenarios are normal.
Challenges the media's attempt to manipulate public opinion for ratings by sensationalizing common events.

Actions:

for media consumers,
Challenge media narratives (implied)
Be critical of sensationalism (implied)
</details>
<details>
<summary>
2021-09-08: Let's talk about VP Harris and bail.... (<a href="https://youtube.com/watch?v=jaF-l0btSpo">watch</a> || <a href="/videos/2021/09/08/Lets_talk_about_VP_Harris_and_bail">transcript &amp; editable summary</a>)

Beau challenges the existence of bail funds in the US, arguing that they indicate underlying issues and can lead to excessive bail, undermining constitutional rights.

</summary>

"I'm going to suggest that their mere existence is evidence that something is very wrong wherever that fund exists."
"I don't believe that bail funds should exist in the United States. I don't think they should be allowed to exist in the United States."
"The purpose of bail is not to keep people in jail. It's just to ensure that they show up for court."
"If there was evidence of that, that's on the judge."
"It's just a thought. Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Vice President Harris is facing backlash from certain media outlets due to her past donation to a bail fund that bailed out someone now accused of murder.
Bail in the United States is money put up to ensure one shows up for court, not to keep them in jail.
Beau believes that bail funds should not exist in the United States, as their existence indicates something is wrong with the system.
Standard bails have become a thing where specific crimes have set bail amounts, which can be problematic based on one's financial status.
Excessive bail violates the Eighth Amendment, which prohibits requiring excessive bail and suggests that bail funds shouldn't exist if people can't post bail regularly.
The accusation alone against someone is enough to smear others, like Vice President Harris, due to the lack of presumption of innocence in the United States.
Beau argues that if bail was allowed for a person, it should be cheap enough for them to get out, ensuring they show up for court.

Actions:

for advocates for criminal justice reform,
Advocate for bail system reform (exemplified)
Support organizations working towards fair bail practices (exemplified)
</details>
<details>
<summary>
2021-09-07: Let's talk about the world's EMT and economic questions.... (<a href="https://youtube.com/watch?v=OgBupzA66pI">watch</a> || <a href="/videos/2021/09/07/Lets_talk_about_the_world_s_EMT_and_economic_questions">transcript &amp; editable summary</a>)

Beau explains economic misconceptions about global standards of living, GDP, resource exploitation, peace profitability, and the potential for companies to profit from humanitarian efforts.

</summary>

"We live in a world where everything is commodified."
"There's only so much that can be done."
"Everything makes money."
"It's just not defense contractors that are gonna make a lot of money."
"It's going to primarily go to companies that hill rather than kill."

### AI summary (High error rate! Edit errors on video page)

Explains the misconception that if the standard of living in other countries increases, the standard of living in the United States must decrease.
Breaks down the concept of gross domestic product (GDP) and how it relates to economic output.
Challenges the idea that there is a finite amount of economy to go around and that resource exploitation is necessary for economic growth.
Counters the belief that there is no money in peace by illustrating how humanitarian efforts and peace-building activities can be profitable.
Provides examples of how companies, particularly those focused on renewable energy and infrastructure development, can profit from peace.
Advocates for investing in companies that "hill rather than kill" as a positive alternative to defense contractors.
Teases future episodes where more questions will be addressed.

Actions:

for global citizens,
Support humanitarian organizations (exemplified)
Advocate for renewable energy and infrastructure development (exemplified)
Invest in companies focused on peace-building efforts (exemplified)
</details>
<details>
<summary>
2021-09-07: Let's talk about my morning routine and plot holes.... (<a href="https://youtube.com/watch?v=lstQRIkhWuc">watch</a> || <a href="/videos/2021/09/07/Lets_talk_about_my_morning_routine_and_plot_holes">transcript &amp; editable summary</a>)

Beau shares wild theories, debunks a dangerous conspiracy, and warns against seeking comfort in fabricated patterns during chaotic times.

</summary>

"Humans are creatures that seek patterns. And if there isn't one that's available, they make one up."
"There is no man behind the curtain. The world's just a scary place."
"Conspiracies show up in times of a lot of tragedy. A lot of unease and a lot of chaos."

### AI summary (High error rate! Edit errors on video page)

Beau shares how he starts his mornings by reading messages with wild theories, finding it entertaining and thought-provoking.
He describes a theory suggesting that the current public health issue is a facade to cover up a scheme involving the vaccine as a tool for depopulation.
Beau points out glaring plot holes in the theory, such as the timing of population decline pre-vaccine and the illogical targeting of allies by supposed evil masterminds.
He hypothetically outlines how a more feasible psyop to achieve such a goal might involve targeting nationalists through controlling information flow and narrative.
Beau underscores the human tendency to seek patterns where they may not exist, leading to conspiracy theories in times of chaos and uncertainty.
He cautions against embracing wild theories and encourages critical examination of motives, feasibility, logistics, and evidence.
Beau expresses concern over the dangerous impact of baseless conspiracy theories that thrive on people's fear and desire for explanations in chaotic times.
He concludes by reflecting on the historical context of conspiracies arising during times of tragedy and unease.

Actions:

for critical thinkers,
Examine wild theories critically and debunk dangerous conspiracies by assessing motives, feasibility, logistics, and evidence (implied).
</details>
<details>
<summary>
2021-09-06: Let's talk about masks in schools.... (<a href="https://youtube.com/watch?v=x8zXrFIkF44">watch</a> || <a href="/videos/2021/09/06/Lets_talk_about_masks_in_schools">transcript &amp; editable summary</a>)

Beau lays out the truth about masks in schools, pointing out the authority schools have and criticizing those willing to risk health for political reasons.

</summary>

"Masks work. Period. Full stop."
"It's not about education. It's about a willingness to risk others, to prove a political point."
"The problem originates with stuff coming out of people's mouth and noses. The solution is to cover it up."
"I don't believe they care about education at all."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addresses the national debate on masks in schools and other clothing articles.
Explains the lack of a mask mandate in schools is due to the difficulty in proving where a student contracted the virus.
Argues that schools have the authority to mandate masks for safety reasons, citing examples of dress codes for non-safety issues.
Questions why some parents oppose mask mandates, attributing it to their willingness to risk students' and staff's health to make a political statement.
Criticizes the lack of education and understanding among those opposing mask mandates, particularly regarding misinformation about airlines banning masks.
Concludes that masks work, and the main debate should be about which types are most effective.
Mentions a charity donation offer related to tying bra straps to masks in schools.

Actions:

for parents, educators, students,
Support mask mandates in schools by engaging with school boards and advocating for student and staff safety (exemplified)
Educate yourself and others about the effectiveness of different types of masks to combat misinformation (exemplified)
Donate to charities supporting causes like ShelterHouseNWFL.org to contribute to community well-being (exemplified)
</details>
<details>
<summary>
2021-09-06: Let's talk about ideological motivations for the world's EMT.... (<a href="https://youtube.com/watch?v=99mS9tIYa80">watch</a> || <a href="/videos/2021/09/06/Lets_talk_about_ideological_motivations_for_the_world_s_EMT">transcript &amp; editable summary</a>)

Beau explains the ideological motivations behind a pragmatic foreign policy shift to address global inequalities and reduce war, advocating for incremental change starting in the United States.

</summary>

"It's incremental, sure. It's not super radical, sure, but it'll work and it could be implemented like tomorrow."
"This is a doctrinal shift that could occur in American foreign policy and in international foreign policy as a whole."
"You can get behind it. There's not an ideological system that would oppose this with the exception of those who really are colonialists."
"It's pragmatic. It would work."
"This is a way. It's incremental, sure. It's not super radical, sure, but it'll work and it could be implemented like tomorrow."

### AI summary (High error rate! Edit errors on video page)

Explains the ideological motivations behind the idea of a World EMT.
People from different backgrounds support the idea for various reasons.
Love and peace advocates see it as a way to avoid war and unnecessary loss, like Marianne Williamson.
Those like Thomas Barnett support it because it's an effective tool to pursue American national interests without war.
Acknowledges the need for global action to address economic inequalities beyond just the United States.
Points out that many countries, despite being resource-rich, remain poor due to wealth extraction.
Africa is emphasized as a region of increasing importance due to its resource wealth.
Advocates for a shift in doctrine to build up unstable countries so they can pursue their own interests and avoid wealth extraction.
Suggests a soft form of colonialism where wealth isn't extracted but countries are empowered.
Believes the global income inequality can be addressed through such initiatives.
Acknowledges the necessity for U.S. involvement in initiating global change for equality.
Proposes a doctrinal shift in American foreign policy that could improve living standards and reduce war.
Urges for incremental change starting in the United States to achieve a fairer world for everyone.
Argues that this pragmatic approach is more effective than waiting for radical changes in the distant future.
Asserts that the proposed shift in foreign policy could gain support across different ideologies due to its practicality.

Actions:

for global citizens,
Advocate for a pragmatic foreign policy shift that addresses global inequalities and reduces war (suggested).
Support initiatives aimed at empowering resource-rich but economically disadvantaged countries (implied).
Engage in activism to push for incremental changes in foreign policy towards a fairer world for all (implied).
</details>
<details>
<summary>
2021-09-05: Let's talk about what the world's EMT means for DOD.... (<a href="https://youtube.com/watch?v=fCrlOOJphws">watch</a> || <a href="/videos/2021/09/05/Lets_talk_about_what_the_world_s_EMT_means_for_DOD">transcript &amp; editable summary</a>)

Beau proposes reallocating defense budget towards a Department of Peace to handle post-conventional warfare and prevent conflicts, advocating for maintaining military edge while reducing spending.

</summary>

"The largest Air Force in the world is the United States Air Force. The second largest Air Force in the world is the United States Navy."
"We're fighting in a bunch of small conflicts constantly."
"We'll come in and help. And doing it without conflict is better."
"More than likely, what would happen is it just wouldn't grow for a few years."
"The odds of having to go toe-to-toe with Russia and China at the same time is pretty slim."

### AI summary (High error rate! Edit errors on video page)

Beau introduces the concept of shifting from military to a Department of Peace for nation-building.
The United States' defense spending is intentional, with the doctrine requiring capability to fight China and Russia simultaneously.
The US military excels in conventional warfare but struggles in unconventional conflicts.
Countries like Iran and North Korea plan to counter a US attack by transitioning to unconventional warfare.
Beau suggests the need for a second entity, like a Department of Peace, to handle post-conventional warfare situations.
He mentions that this second force could operate independently of the military and could be used to prevent conflicts.
By establishing a successful track record, countries in need could request assistance from this peacekeeping force without conflict.
Beau advocates for reallocating some of the defense budget towards this "world's EMT" entity.
He argues that the current massive defense expenditures are no longer necessary given the geopolitical landscape.
Beau concludes by hinting at the possibility of maintaining military superiority while reducing defense spending.

Actions:

for policy makers, activists,
Advocate for reallocating defense budget towards establishing a Department of Peace (suggested)
Support transparent international efforts for conflict prevention through non-military means (implied)
</details>
<details>
<summary>
2021-09-05: Let's talk about the UN going to Afghanistan and the world's EMT.... (<a href="https://youtube.com/watch?v=kw-RhtDR-gw">watch</a> || <a href="/videos/2021/09/05/Lets_talk_about_the_UN_going_to_Afghanistan_and_the_world_s_EMT">transcript &amp; editable summary</a>)

Exploring broken nations globally, UN's role in providing aid and sparking a call to shift foreign policy towards stable, responsive governments for lasting peace and prosperity.

</summary>

"It's about as effective and most times looks just as silly."
"They don't have a functioning system around them."
"Stability is good for peace."
"The government doesn't collapse in a week."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Exploring broken nations and failed states globally.
UN's mission restart in Afghanistan amidst debates.
UN's role misunderstood - not about fixing countries.
UN provides aid, treats symptoms, doesn't offer cure.
Comparison of UN's actions to American military contractors.
Lack of international agency for fixing broken nations.
Proposal for an entity dedicated solely to nation-building.
Critique of using Department of Defense for nation-building.
Advocacy for stable governments responsive to people's needs.
Support for shifting foreign policy focus towards nation-building.
Mention of Marianne Williamson and Thomas Barnett's proposals.
Advocacy for a shift from military intervention to nation-building.
Importance of stability for peace and economic interests.
Lack of specialized entities for fixing broken nations.
Call for considering a new doctrine for global stability.

Actions:

for global policymakers,
Advocate for a shift in foreign policy towards nation-building (suggested)
Support initiatives for stable, responsive governments in conflict areas (suggested)
</details>
<details>
<summary>
2021-09-04: Let's talk about the media's role in foreign policy.... (<a href="https://youtube.com/watch?v=QTuo6Fs1hIE">watch</a> || <a href="/videos/2021/09/04/Lets_talk_about_the_media_s_role_in_foreign_policy">transcript &amp; editable summary</a>)

Beau criticizes the media's prioritization of outrage over policy in shaping public opinion on military decisions, urging skepticism towards mainstream commentators' insights.

</summary>

"It is in their best interest to blame everybody."
"It's more lucrative for the media to provoke outrage than to discuss policy."
"If you're doing root cause analysis, their failure to educate the American populace, their willingness to
promote outrage over fact and policy is a huge part of the reason it went the way it did."

### AI summary (High error rate! Edit errors on video page)

Critiques the lack of examination of the media's role in foreign policy, leading to repeated mistakes.
Shares about putting out a tweet asking for plans on a difficult situation, but the responses dwindled.
Points out the media's reluctance to address the intricacies of military operations and planning.
Explains how the blame game in military decisions starts from the executive branch's strategic objectives.
Expresses frustration that public opinion, shaped by the mass media, influences strategic plans.
Delves into how the media prioritizes outrage over policy, attracting more viewers and revenue.
Mentions the unlikelihood of getting detailed policy explanations in mainstream media due to audience preferences.
Talks about the unlikelihood of receiving advice from media pundits who failed to provide proper education to the public.
Mentions the impending shift in U.S. doctrine, the "Biden doctrine," and its challenges in implementation.
Concludes by cautioning against relying on TV commentators from major networks for insights on complex military strategies.

Actions:

for policy analysts, activists,
Educate your community on the importance of understanding policy details and advocating for informed decision-making (exemplified).
Support independent media sources that prioritize policy analysis over sensationalism (implied).
</details>
<details>
<summary>
2021-09-04: Let's talk about patriotic education, dogs,  and civic mindedness.... (<a href="https://youtube.com/watch?v=flqqTXqdojE">watch</a> || <a href="/videos/2021/09/04/Lets_talk_about_patriotic_education_dogs_and_civic_mindedness">transcript &amp; editable summary</a>)

Beau questions the true motives behind patriotic education, arguing that it aims for obedience through conditioning, not civic-mindedness, criticizing its indoctrination focus and lack of genuine civic education.

</summary>

"The goal isn't to create civic minded people. The goal is to create Pavlov's dogs."
"They want to indoctrinate students. They want to create students that are easily manipulated."
"They want, for lack of a better word, less educated people."
"They don't care about education. They want indoctrination."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Exploring the connection between patriotic education and civic responsibility.
Questions the opposition to patriotic education.
Argues that patriotic education aims to create obedience through conditioning, not civic-mindedness.
Points out the lack of encouragement for basic community care practices by politicians advocating for patriotic education.
Criticizes the focus of patriotic education on indoctrination rather than true civic education.
Emphasizes that civic-minded individuals are harder to manipulate by politicians.
Calls out the ulterior motives behind pushing for patriotic education, linking it to manipulation and control.
Expresses skepticism towards the genuine intentions of those advocating for patriotic education.
Concludes with a critical reflection on the true objectives behind patriotic education.

Actions:

for educators, activists, concerned citizens,
Question the motives behind proposed educational reforms (implied)
Advocate for comprehensive civic education in schools (implied)
</details>
<details>
<summary>
2021-09-03: Let's talk about people defeating the Texas tip website.... (<a href="https://youtube.com/watch?v=dgGvcx21evE">watch</a> || <a href="/videos/2021/09/03/Lets_talk_about_people_defeating_the_Texas_tip_website">transcript &amp; editable summary</a>)

Texas law enables reporting on a website; TikTok teens flood it with fake tips to overwhelm surveillance tactics.

</summary>

"Overwhelming the system with information is a tactic used to render mass surveillance less effective."
"This tactic on TikTok may force a reconsideration of how the tip website operates."
"It's going to make it more difficult to process any information they obtained through this tip website."

### AI summary (High error rate! Edit errors on video page)

Texas passed a law enabling a website for neighbors to report socially undesirable behaviors.
People on TikTok and Reddit are encouraging tips to overwhelm the system.
The tips are not factual but aim to flood the system with information.
Tips include advice like not copying and pasting, using real locations, and avoiding pro-choice slogans.
Overwhelming the system with information is a tactic used to render mass surveillance less effective.
This tactic on TikTok may force a reconsideration of how the tip website operates.
While it may not change legislation, it could make processing information more difficult for authorities.

Actions:

for online activists,
Encourage others to flood platforms with information (exemplified)
Provide guidance on how to create overwhelming but fake information (exemplified)
</details>
<details>
<summary>
2021-09-03: Let's talk about a message to Australia from Florida Man.... (<a href="https://youtube.com/watch?v=NzqstDcb44I">watch</a> || <a href="/videos/2021/09/03/Lets_talk_about_a_message_to_Australia_from_Florida_Man">transcript &amp; editable summary</a>)

Beau warns Australia against following the US lead in handling the crisis, citing poor management and advises making independent decisions on restrictions.

</summary>

"Do not follow the United States' lead. We don't have a clue what we're doing."
"We are not an example of anything except what not to do."
"Do not look to the United States for leadership on this."

### AI summary (High error rate! Edit errors on video page)

Australians are considering relaxing strict measures put in place during the public health crisis.
Politicians and commentators in Australia are suggesting adopting a US-style approach.
Australia has a population of about 25 million, similar to Florida's 21 million.
Florida has lost 45,000 people to the crisis, while Australia has lost just over a thousand.
If Australia had a population the size of the US, they might have lost around 13,000 people.
Beau warns Australia not to follow the United States' lead in handling the crisis.
He points out the poor handling of the crisis in the US, with people resorting to unusual measures like going to a livestock store for medications.
Beau urges Australia not to view the US as an example to emulate in managing the crisis.
He cautions against looking to the US for leadership on handling the crisis, as the situation is not being managed well there.
Florida and several other states have lost a significant number of people to the crisis, indicating a serious impact.
Beau stresses that decisions on restrictions and measures are up to Australia but advises against seeking guidance from the US.
He concludes by encouraging Australians to make their own choices but not to look to the US for leadership on handling the crisis.

Actions:

for australian policymakers,
Make independent decisions on restrictions and measures (suggested)
Avoid looking to the United States for leadership on handling the crisis (suggested)
</details>
<details>
<summary>
2021-09-02: Let's talk about something in American politics that needs to stop now.... (<a href="https://youtube.com/watch?v=U-D8RIxz1io">watch</a> || <a href="/videos/2021/09/02/Lets_talk_about_something_in_American_politics_that_needs_to_stop_now">transcript &amp; editable summary</a>)

Beau addresses the exploitation of tragedies in politics, refuses to name recent deceased individuals, and suggests focusing on positive outcomes rather than politicizing tragedies.

</summary>

"I typically just say I'm sorry for your loss and I leave it at that because it's not words that dulls the pain. It's time."
"Please do not talk to their families."
"The worthless pointless mission gave a hundred thousand people a new lease on life."

### AI summary (High error rate! Edit errors on video page)

Addresses the distasteful habit in American politics of using tragedies for political gain.
Refuses to name individuals who have recently passed away, citing the importance of not using their deaths for personal commentary or political points.
Believes expressing condolences may come off as insincere and prefers to simply say "I'm sorry for your loss."
Criticizes the idea of approaching grieving families in the future to tell them their loved one's death was in vain.
Suggests focusing on the positive outcomes of actions taken, like the humanitarian airlift, rather than politicizing tragedies.

Actions:

for social commentators,
Respect the privacy of grieving families and refrain from approaching them to express opinions on their loved one's death (implied).
</details>
<details>
<summary>
2021-09-01: Let's talk about that law in Texas and tattoos.... (<a href="https://youtube.com/watch?v=oADVHiFayGU">watch</a> || <a href="/videos/2021/09/01/Lets_talk_about_that_law_in_Texas_and_tattoos">transcript &amp; editable summary</a>)

Beau addresses the ineffective nature of trying to legislate morality, using tattoos to illustrate how such laws disproportionately impact those without means, ultimately serving to provide judgmental individuals with targets rather than solve real issues.

</summary>

"There's a base belief among a lot of people that suggests however you run your life is how you think society as a whole should be run."
"So when you go on vacation, you get your tattoo. You cross state lines, do it somewhere else."
"It's about taking a bunch of judgmental people and giving them somebody to judge."
"There are a whole bunch of things going on in Texas that are way more important than this."
"It's designed to create scandal and give you somebody to look down on."

### AI summary (High error rate! Edit errors on video page)

Addressing tattoos and what they teach about a new law in Texas.
Explaining the base belief behind questioning personal choices.
Criticizing the idea of legislating morality with examples like prohibition and the war on drugs.
Pointing out the ineffectiveness of using laws to control behavior like visible tattoos.
Describing how people of different economic classes navigate around restrictive laws.
Arguing that such laws target and impact those without means and a voice.
Revealing the true intention behind legislation like the new law in Texas.
Stating that the goal is not to end the behavior but to provide judgmental people with someone to judge.
Condemning the prioritization of such laws over more pressing issues in Texas.
Concluding that these laws serve to create scandal and foster judgment rather than solve problems.

Actions:

for policy makers, activists,
Advocate for policies that address real issues impacting communities, rather than targeting and marginalizing vulnerable populations (implied).
Support initiatives that prioritize the voices and needs of marginalized groups in legislative decision-making processes (implied).
</details>
<details>
<summary>
2021-09-01: Let's talk about foreign policy, American vs Soviet style.... (<a href="https://youtube.com/watch?v=MAvGxM1YojQ">watch</a> || <a href="/videos/2021/09/01/Lets_talk_about_foreign_policy_American_vs_Soviet_style">transcript &amp; editable summary</a>)

Beau explains how foreign policy is akin to a poker game with cheating, contrasting American and Soviet views, advocating for the poker analogy's inclusivity and fluidity over the elegant but confrontational chess comparison.

</summary>

"Foreign policy is like a poker game where everybody's cheating."
"Poker analogy includes more players and acknowledges cheating."
"Power is the currency in foreign policy."

### AI summary (High error rate! Edit errors on video page)

Foreign policy is like a poker game where everyone's cheating.
Americans view foreign policy as poker, Soviets view it as chess.
Poker allows for more players and acknowledges cheating.
Chess is elegant, strategic, and confrontational.
Soviets' foreign policy was ideologically driven and confrontational.
Poker analogy includes more players, like Vietnam and Afghanistan.
Acknowledging other countries adds layers of fluidity in foreign policy.
Everyone cheating in poker mirrors foreign policy dynamics.
Chess results in conquest, while foreign policy is ongoing.
Foreign policy is about power, not money.
Power is the currency in foreign policy.
Poker analogy is a better way to understand foreign policy dynamics.
The analogy is a trope but offers a more comprehensive view.
Beau suggests asking the Soviet embassy for their perspective on the analogy.

Actions:

for foreign policy enthusiasts,
Call the Soviet embassy and ask for their perspective on foreign policy analogies (suggested)
</details>
