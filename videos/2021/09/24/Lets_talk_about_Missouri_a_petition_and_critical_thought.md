---
title: Let's talk about Missouri, a petition, and critical thought....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=zfRfRMUb1ic) |
| Published | 2021/09/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Students at Park Hill South High School circulated a petition requesting that slavery be reinstated.
- The school district is tight-lipped about the petition, calling it a disciplinary issue to protect the students' future.
- Missouri recently probed whether students were learning objectionable material about race and proposed an amendment against teaching institutions were racist.
- State legislature hearings on race in education mainly consisted of white people.
- The state's actions seem to contradict the "this isn't who we are" narrative.
- Beau questions if Missouri truly stands against critical thinking and acknowledging the impacts of slavery.
- He calls for elected representatives to decide if students should critically think or endorse the pro-slavery petition.

### Quotes

- "This isn't who we are though, right?"
- "I don't know how people can say this isn't who we are, because it certainly seems like that's what Missouri is."
- "I think it's time that the elected representatives in the state of Missouri decide whether they want to allow students to be encouraged to critically think."
- "Anyway, it's just a thought."
- "Y'all have a good day."

### Oneliner

Students circulated a petition to reinstate slavery, prompting Beau to question Missouri's stance on critical thinking and race education.

### Audience

Educators, activists, students

### On-the-ground actions from transcript

- Contact elected representatives in Missouri to advocate for critical thinking and anti-racism education (suggested).

### Whats missing in summary

The full transcript provides more context on Missouri's recent actions regarding race education and critical thinking. Viewing the entire transcript will give a clearer picture of the state's stance on these issues.

### Tags

#Missouri #Education #CriticalThinking #RaceEducation #Slavery #Petition


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about the state of Missouri and education and curriculums
and petitions and critical thought or the lack thereof.
If you missed the news, some students at a school related activity at Park Hill South
High School circulated a petition requesting that slavery be reinstated.
Now the school district itself is being pretty tight-lipped about the circumstances surrounding
this petition, calling it a disciplinary issue and they're not releasing any real details,
probably to protect these students' future college prospects.
This isn't something that is going to look good on any college application in the future.
They did release the standard, this isn't who we are, type of press release.
In the absence of information, people are filling in the blanks as they want.
They're blaming the school, they're blaming the teachers, they're blaming parents.
This isn't who we are though, right?
The thing is, in Missouri, isn't it Beau?
I would point out that in July and August, two months, the state probed whether or not
students were learning objectionable material at school.
By objectionable, I mean they were learning to critically think about race.
They decided that that wasn't something they wanted taught.
They proposed an amendment to that end to make sure that students weren't told that
any institution was racist.
It's also worth noting that the state legislature there held a hearing on race in education
with pretty much just white people.
If that is the lead that the state is setting, if that's the example they're providing students,
I don't know what they can expect.
And I don't know how people can say this isn't who we are.
Because it certainly seems like that's what Missouri is.
You have months worth of probes and hearings on this topic.
But when it comes to this incident, I'd be really surprised if the state legislature
takes notice.
Because they can't, other people, they can't use it as a divisive issue to, well, try to
keep people in their place, you know.
I think it's time that the elected representatives in the state of Missouri decide whether they
want to allow students to be encouraged to critically think and informed of the impacts
of slavery that still exists today in the other racist institutions throughout American
history or they just want to put their name on this petition that was sent around by these
students.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}