---
title: Let's talk about Rudy being banned from Fox....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=w9qTIRE2Yx8) |
| Published | 2021/09/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Fox News reportedly bans Rudy Giuliani and his son from appearing on the network.
- Fox viewers celebrate the ban, but what about those who believe Rudy’s statements?
- Fox’s decision could be due to Rudy making slanderous or libelous statements.
- Fox risks financial responsibility if Rudy continues to make unsubstantiated claims on air.
- The lack of evidence to support Rudy's claims may be the reason behind the ban.
- Fox is willing to forego profits, advertising revenue, and viewership to avoid legal trouble.
- Supporters of Rudy and Trump should see this as a wake-up call.
- Recent events, like the lack of evidence in Arizona, should prompt reevaluation.
- Fox's inability to defend Rudy's statements suggests a lack of evidence.
- Followers of Rudy should question their beliefs and reexamine their perspectives.
- This moment should make people reconsider their views on perceived enemies.
- It's a call to question deeply held beliefs and narratives.
- Encourages critical thinking and introspection.
- Urges viewers to see beyond divisive narratives and propaganda.
- Promotes reflection on blind allegiance and misinformation.

### Quotes

- "Fox risks financial responsibility if Rudy continues to make unsubstantiated claims."
- "Supporters of Rudy and Trump should see this as a wake-up call."
- "This should be the moment that you really start to question it."
- "They're just other Americans."
- "It's just a thought. Y'all have a good day."

### Oneliner

Fox News bans Rudy Giuliani due to unsubstantiated claims, signaling a wake-up call for supporters to question beliefs and narratives.

### Audience

Supporters, viewers, skeptics

### On-the-ground actions from transcript

- Question your beliefs and narratives (implied)
- Reexamine your perspectives (implied)
- Engage in critical thinking (implied)

### Whats missing in summary

Deeper exploration of blind allegiance and misinformation

### Tags

#FoxNews #RudyGiuliani #TrumpSupporters #CriticalThinking #QuestionBeliefs


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Rudy and Fox and what's reportedly transpired there.
Reporting has come out that Fox News is no longer going to allow Rudy Giuliani or his
son to be a guest on the network.
Now for most people that are watching this channel, this is just a celebratory moment.
But what about for those who are frequent Fox viewers?
Those who may believe a lot of the things that Rudy has said?
You have to wonder why Fox would suddenly decide to stop allowing them on the channel.
I mean they're good for ratings, that's not in dispute.
So why would they do it?
Would Fox suddenly become co-opted by the left?
Why would this occur?
Many times the simplest answer is the right one.
In this case, you have a man, Rudy Giuliani, who has repeatedly made statements that people
would say are slanderous or libelous.
And if Fox was to continue to plan for him while he is making these statements, if they
know they're not true, well they can be held responsible financially as well.
For the viewers of Fox, for those who follow Rudy, for those who truly believe some of
the things that he has said, it's important to note that the most likely reason for this
little ban on his appearances is that Fox has no method of defending his statements.
They don't have any evidence to back up his claims.
Because if they did, they could use that in court as a reason to continue to allow him
and obtain those great ratings that he produces.
But they can't.
Fox News, a very capitalist entity, is foregoing profits, foregoing advertising revenue, foregoing
viewership and ratings.
And the most likely reason for this is that they cannot in any way, shape, or form prove
or provide evidence to support the claims that Rudy Giuliani makes.
That's the most likely reason.
Now if you are a supporter, a follower, a believer, if you are part of the Trump faithful,
this combined with what occurred in Arizona should serve as a wake-up call.
This might be the moment that you really start looking at things through a different lens.
The evidence wasn't there in Arizona.
And Fox News itself is saying, yeah, we can't continue to push this because if we continue
to we might be held responsible for the slanderous and libelous statements.
We might be held responsible financially for platforming things that aren't true.
If they were true, they would have a defense.
But without evidence to back this stuff up, they really don't have an option financially.
They can't continue to support the claims because they can't back them up.
If you are somebody who is following Rudy, if you are somebody who has wasted months
of your life waiting for the Savior to return, this should be a wake-up call.
This should be the moment that you really start to question it and start to take a step
back and maybe consider that the people that you have been convinced are evil aren't.
The people that you have been conditioned to view as your mortal enemy, well, they're
just other Americans.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}