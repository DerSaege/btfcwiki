---
title: Let's talk about our man in Haiti, Daniel Foote....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=801iGY0N6FU) |
| Published | 2021/09/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Special Envoy to Haiti Daniel Foote resigned due to the treatment of Haitian refugees at the US southern border.
- Foote feels his recommendations were ignored, and his policy initiatives were not given due attention.
- He believes that the US government is trying to influence and interfere in Haiti, picking winners and losers.
- While media focuses on the inhumane treatment of Haitians at the border, the main issue is the danger faced by those being sent back to Haiti.
- American officials in Haiti are in danger due to the security situation, indicating that returning Haitians puts them at risk of loss of life.
- Despite this imminent danger, the question arises: why are these individuals not considered eligible for asylum?
- Foote suggests that these Haitians qualify for asylum based on the danger they face upon return to Haiti.
- The focus should be on saving individual lives at risk rather than just discussing broader policy initiatives for Haiti.
- Granting asylum to those who clearly deserve it is a critical duty that the Biden administration and media should prioritize.
- Beau questions whether the US is upholding its promises of providing help to those in need as outlined in the Declaration of Independence and the Constitution.

### Quotes

- "If you have a US official pointing this out and saying that if they come back they're in imminent danger, how are they not eligible for asylum?"
- "You have a US official clearly pointing out that these people coming here looking for help deserve it."

### Oneliner

Special Envoy to Haiti resigns over treatment of Haitian refugees at the US border, focusing on the imminent danger they face upon return to Haiti while questioning why they are not eligible for asylum.

### Audience

Advocates, policymakers, activists

### On-the-ground actions from transcript

- Advocate for the Biden administration to prioritize granting asylum to Haitian refugees (implied).
- Share and raise awareness about the dangers Haitian refugees face upon return to Haiti (suggested).

### Whats missing in summary

The emotional impact of neglecting the immediate danger faced by Haitian refugees and the ethical responsibility to grant asylum to those in need.

### Tags

#HaitianRefugees #USBorderPolicy #AsylumSeekers #HumanRights #BidenAdministration


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about Special Envoy to Haiti Daniel Foote and his resignation
and the letter and the read that's coming across in the major media outlets.
Which yes, what they're talking about definitely needs to be discussed, but there's a more
important point.
If you don't know, he's resigned and the reason he's resigning is the treatment of Haitian
refugees, refugees being his word, at the southern border of the United States and the
fact that they're likely to be sent back to Haiti.
He goes on to discuss how his recommendations have kind of been ignored, how his policy
initiatives aren't really getting the play they should, and he just feels like nothing
is being done in accordance with what he was asked to do.
That's the general tone of his resignation and he points out that he feels like the US
government is trying to pick winners and losers down there and kind of meddle.
And this is what people are focusing on.
That's what the media is paying attention to and they are paying attention to the use
of the word inhumane as far as the treatment of Haitians at the southern border.
But there's a more important point.
You have a US official on the ground in Haiti who is flat out saying returning these people
puts them in imminent danger.
He talks about how American officials have to stay on secure compounds because the security
situation there is less than adequate.
It's dangerous.
There's likely to be loss of life, right?
So the question is, how are they not eligible for asylum?
If you have a US official pointing this out and saying that if they come back they're
in imminent danger, how are they not eligible for asylum?
That's kind of the criteria.
For that piece of this story to be ignored, we're losing the main issue.
Sure, you can talk about the different methods of trying to get Haiti back on its feet, and
foot is definitely in the Thomas Barnett camp trying to be the world's EMT style, and there's
definitely discussion to be had there.
But the most important thing at the moment is the individual lives that are at risk.
And you have a US official flat out saying that they kind of meet the requirements for
asylum.
That's the thing the Biden administration needs to be paying attention to.
That's the thing that the media should be talking about.
Because as we sit here and talk about how it's a major issue at the border and it's
a national security issue and all of this stuff, sure, whatever, have that debate.
But at the end of the day, if the United States does not grant asylum to people who are clearly
qualified for it, are we still living up to those promises?
The promises in the Declaration of Independence and the Constitution?
Are we abiding by our obligations under the Constitution and international law?
You have a US official clearly pointing out that these people coming here looking for
help deserve it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}