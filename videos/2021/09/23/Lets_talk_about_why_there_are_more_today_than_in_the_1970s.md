---
title: Let's talk about why there are more today than in the 1970s....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=JtyMHEzOIhE) |
| Published | 2021/09/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring the expansion of public health initiatives in the United States over the years.
- Responding to a query about the increase in vaccinations from 18 to close to 75 by age 18.
- Acknowledging the business aspect of vaccinations in a capitalist system.
- Mentioning the immunization of babies before they leave the hospital and even in the womb.
- Emphasizing that the rise in vaccinations is not just a business strategy but a response to successful outcomes of past vaccines.
- Noting the significant drop in infant mortality rates and childhood mortality over the years, attributing some of it to safety measures like bike helmets and car seat mandates.
- Mentioning the decline in cases and deaths due to vaccines pre-1980, prompting the continued pursuit of vaccination as a public health tool.
- Quoting a statistic from The Lancet about millions of lives saved by vaccination in low and middle income countries, particularly children under five.
- Stressing the effectiveness and non-conspiratorial nature of vaccines in managing public health and mitigating risks.
- Concluding with the idea that the use of vaccines is based on their proven success in saving lives and managing public health.

### Quotes

- "Everything is commodified. Everything is a business. It absolutely is a business."
- "They use it because it works."
- "It's nothing conspiratorial."
- "Why not use a tool that works?"
- "Y'all have a good day."

### Oneliner

Beau explains the expansion of vaccinations in the US, attributing it to past successes and effective public health management, not conspiracy.

### Audience

Health advocates, policymakers, general public

### On-the-ground actions from transcript

- Get vaccinated to protect yourself and others (implied)
- Support public health initiatives and vaccination programs (implied)

### Whats missing in summary

In-depth analysis of specific diseases targeted by vaccines and their impact on public health.

### Tags

#PublicHealth #Vaccinations #PublicSafety #Healthcare #USHealthcare


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about public health initiatives
in the United States and how they have expanded
over the years and why they have expanded over the years.
We're going to do this because somebody asked,
and while I didn't fact check every detail in their message,
I didn't because the overall point's there.
And the question is worthy of discussion.
So we're going to go ahead and take a look at it.
The comment was, let's talk about kids getting maybe 18
vaccinations by age 18 back in the 1970s,
whereas now it's close to 75.
And they are immunizing babies before they leave the hospital.
Sounds like a business.
OK, so we'll take the individual points.
Sounds like a business.
Yeah, it's the United States.
It's a capitalist system.
Everything is commodified.
Everything is a business.
It absolutely is a business.
Immunizing babies before they leave the hospital.
I'll go a step further.
They're immunizing babies in the womb.
There are practitioners who will tell expectant mothers
to get vaccinated because the antibodies can
be passed on in some cases.
Now whether or not it is 18 vaccinations by age 18
back in the 70s and 75 now, I have no idea.
But it is true that there are more.
So the point stands.
Whether or not the specifics are accurate,
the point is here to every piece of this message.
OK, so the question isn't whether or not
this is happening.
It's why.
Is it just because it's a business?
No, no.
The reason this is occurring, the reason
there are more vaccines today than there were in the 70s,
is because the ones in the 70s worked.
The ones in the 70s worked.
They achieved their goal.
So they targeted other diseases to, again,
to eliminate the risk, to mitigate the risk of them.
That's why.
It's nothing nefarious.
I would point out that during the same period from 1970
to 2020, the infant mortality rate in the United States
dropped from 22 out of 1,000 to 6 out of 1,000.
Now not all of that is attributable to vaccines.
But it shows a trend towards more safety, more mitigation
across the board in the United States.
Childhood mortality also dropped.
I want to say by more than half.
And some of that is having kids wear bicycle helmets,
or seat belt and car seat mandates, stuff like that.
Do we need mandates to do this?
We shouldn't.
People should just take advantage of the issues
or of the innovations that occur to mitigate risk.
But they don't always, right?
And when those mandates came out, yeah,
they were called anti-liberty as well.
And sure, we shouldn't need mandates.
OK.
I would point out that vaccines pre-1980
created a 92% decline in cases and a 99% decline in death.
So of course, they are going to pursue that avenue as a way
of managing public health.
It's effective.
It works.
Why would you not use a tool that works?
So that's why it's happening.
An interesting one that I read in The Lancet recently
said that, I want to say over the last 20 years,
37 million lives were saved by vaccination
in low and middle income countries.
And that 36 million of those lives
were under the age of five.
That's why.
They use it because it works.
There will be more vaccines in the future.
Those numbers will keep going up because it's effective,
because it does its job.
It's nothing conspiratorial.
It's just the way we have found that can help mitigate risk
and help manage public health.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}