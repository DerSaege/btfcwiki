---
title: Let's talk about Trump vs McConnell....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=uft0Mbv0rK8) |
| Published | 2021/09/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analysis of the GOP defensive strategies involving Trump and McConnell.
- Trump actively seeking to depose McConnell and build a coalition against him.
- McConnell forced to treat Trump as opposition due to public split.
- Trump influencing election seats with loyal candidates, potentially impacting primaries.
- Potential Republican Civil War between Trump and McConnell.
- McConnell's political savvy and ability to keep secrets.
- Impact on midterms, with Republicans potentially fighting each other more than Democrats.
- Beau bets on McConnell coming out on top in the GOP internal conflict.
- Implications for the power dynamics within the Republican Party.

### Quotes

- "Trump is actively seeking people to depose Mitch McConnell trying to build a coalition to get rid of him."
- "McConnell has to respond and treat Trump as opposition rather than an ally."
- "Trump could trigger results that are unfavorable to Republican candidates he doesn't like."
- "If this proceeds the way it looks like it's going to, they're going to end up fighting each other as much as they're going to be fighting the Democrats."
- "I mean, Trump couldn't even get re-elected, much less take down a DC establishment insider."

### Oneliner

Beau analyzes the GOP defensive strategies involving Trump and McConnell, predicting a potential Republican Civil War with McConnell likely coming out on top.

### Audience

Political analysts, Republicans, Democrats

### On-the-ground actions from transcript

- Organize or support candidates for election seats with integrity and dedication (implied)
- Stay informed and engaged in the upcoming midterms (implied)

### Whats missing in summary

Insights on the potential consequences of the GOP internal conflict and its impact on future elections.

### Tags

#GOP #Trump #McConnell #RepublicanParty #PoliticalAnalysis


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about Trump
and Mitch McConnell and why the GOP may be going
on the defensive in more ways than one,
not just the traditional obstruct the Democratic Party,
but also having to deal with internal issues
that are starting to boil over
in a couple of different places.
The most obvious is the now very public split between former President Donald Trump and
Mitch McConnell in the Senate.
We've talked about it over the last year.
We knew this was coming.
We knew it was going to happen.
We're just waiting for the element.
Reporting is now widely going out that Trump is actively seeking people to depose Mitch
McConnell trying to build a coalition to get rid of him. That's important to note because now that
it's public, McConnell has to respond and treat Trump as opposition rather than an ally because
the one thing that we know about Mitch McConnell is that Mitch McConnell cares about Mitch McConnell.
He's not going to just kind of look the other way on this out of party unity.
The other aspect at play is that Trump is also trying to get candidates to run
for election seats in various states, the offices that would be overseeing the
elections. And while this may just seem to play into the whole narrative that
he created about the election and election security, it should also serve as a concern
for Republicans because those same people who Trump is installing because they have
loyalty to him, they'll be in charge of the primaries as well.
So Trump could trigger results that are unfavorable to Republican candidates he doesn't like.
Because he is the way he is, he's probably likely to do that.
Now the question is, who do you think is going to win?
Now on one hand, we have former President Trump, who has a pretty loyal base that is
very vocal.
On the other hand, we have Senator Mitch McConnell, who has held his seat since 1985.
I've made it clear I don't really like McConnell, but I will never underestimate the man's
political savvy.
I'm going to suggest the smart money is on McConnell in this little Republican Civil
War.
He has outlasted many other oppositional figures to him.
So I would imagine that McConnell may be organizing his own coalition against Trump.
And the difference between Trump and McConnell is that you don't get leaks out of McConnell's
office very often.
The man knows how to keep his secret.
So that's what we're looking at for the near future.
And interestingly enough, this is going to play heavily into the midterms because Republicans
will end up, if this proceeds the way it looks like it's going to, they're going to end up
fighting each other as much as they're going to be fighting the Democrats.
So it should be interesting, but if you are somebody who wagers on stuff like this, I
would bet that McConnell's going to come out on top.
I mean, Trump couldn't even get re-elected, much less take down a DC establishment insider
who really does have the real power when it comes to the Republican Party.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}