---
title: Let's talk about Biden not following the science....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=DPUCQe8DH40) |
| Published | 2021/09/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Biden administration's decision to prioritize booster shots over sending doses overseas is facing pushback from medical professionals and scientists.
- The debate is not about the effectiveness of booster shots but about medical ethics and whether it's right to prioritize boosters for Americans over providing initial shots to those overseas.
- Beau raises the ethical question of whether fully vaccinated Americans should receive booster shots while many people overseas haven't even had their first shot.
- He criticizes the administration's possible nationalistic lens on vaccine distribution, focusing on Americans first.
- Beau suggests that getting your booster shot as soon as possible might actually help accelerate overseas distribution rather than delaying it through protest.
- Waiting to pressure the government to change its stance on booster shots may backfire and lead to more doses being kept in the US due to increased vaccine hesitancy.

### Quotes

- "The US government is going to look at the science through a nationalistic lens."
- "It's about ethics, about what you do with science, and whether or not that's bad."
- "There are ways to get the most vulnerable populations in the U.S., get them their booster shot, and send doses overseas at the same time."
- "Just go get it quickly so they can begin getting it overseas."
- "They're going to push even more heavily and keep more doses here."

### Oneliner

The Biden administration's prioritization of booster shots over global vaccine distribution raises ethical questions around nationalistic lens on science and medical ethics. 

### Audience

Global Health Advocates

### On-the-ground actions from transcript

- Get your booster shot as soon as possible to potentially accelerate overseas distribution (implied).

### Whats missing in summary

Beau's engaging delivery and nuanced perspective can best be appreciated by watching the full video. 

### Tags

#BidenAdministration #VaccineDistribution #BoosterShots #MedicalEthics #Nationalism


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we are going to talk about
whether or not the Biden administration
is following the science because we're seeing
that in headlines now.
They've made a decision and they're getting pushback
from medical professionals and from scientists about it.
And we're gonna take away whether or not
that's actually what's going on
or this is another case of, well,
the media playing into narratives that already exist
because they don't want to take the time
to explain the real issue.
Okay, so if you don't know what I'm talking about,
the Biden administration has made it pretty clear
that they want Americans to receive their booster shot
before the US starts really pumping doses overseas.
Now, they're getting pushback
from medical professionals on this,
but is it on the grounds of science
in the way that we've been talking about it?
really, what it boils down to is two doses being fully vaccinated as the way we've been talking
about it, that's really effective. You have a greatly reduced chance of suffering the most
negative outcome and you're at lower risk of infection. A booster shot just makes it more
effective. There's no debate over that. The question doesn't really come from that perspective. It
comes from a medical ethics perspective. You're pretty well covered being fully vaccinated here
here in the US, you have a greatly reduced chance
of suffering the most negative outcome.
There are people overseas who have not had the ability
to get a first shot, and we're looking at getting our third.
That's the question, it's about ethics,
about what you do with science,
and whether or not that's bad.
And that's really what this boils down to.
Now the question is, what are we supposed to do?
Most people who watch this channel probably
hold the view that beyond America's borders
do not live a lesser people.
And this is where we run into that intersection of ideology
and reality again.
The US government is going to look at the science
through a nationalistic lens.
So they're probably going to want
to take care of Americans first.
It's not right.
I don't think that that's the best move,
but I also don't think that's something
that's gonna change.
It might, and I'd be happy to see it.
There are ways to get the most vulnerable populations
in the U.S., get them their booster shot,
and send doses overseas at the same time.
We can walk and chew gum at the same time.
Odds are we're not going to, though.
Odds are the Biden administration
is going to focus on Americans first, right?
Oddly enough, they actually will do the America first thing.
So what does that mean for us?
Should we say, no, I'm not gonna do it?
Send it overseas.
You can, from a moral standpoint, sure,
but is that actually gonna help the people over there?
Is that gonna make them get it faster?
No, it's gonna slow it down.
If the political powers in the United States decide
that the move is to vaccinate Americans
with a booster shot before sending it over.
If you want to help those over there,
go get your booster shot as soon as possible.
Speed the process along.
Because if that's what the politicians decide,
if they're gonna view it through that nationalistic lens,
all you're gonna do by waiting in some form of protest
is delay them getting it over there.
So the right move, to me anyway, morally,
would be to get it as soon as possible.
Now, I wouldn't go out and advocate for the booster shots right now, not for everybody, at least,
but when they become available, if the U.S. stance is that we're going to get everybody in the U.S.
first, just go get it. Just go get it quickly so they can begin getting it overseas. If you hold out
in hopes of putting pressure on them to change their tune, it's probably not going to work the
the way you think. They're going to see more vaccine hesitancy. They're going to see it
through an entirely different lens. So they're going to push even more heavily and keep more
doses here. So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}