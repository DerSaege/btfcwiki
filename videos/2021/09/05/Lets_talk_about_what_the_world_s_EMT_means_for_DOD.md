---
title: Let's talk about what the world's EMT means for DOD....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=fCrlOOJphws) |
| Published | 2021/09/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau introduces the concept of shifting from military to a Department of Peace for nation-building.
- The United States' defense spending is intentional, with the doctrine requiring capability to fight China and Russia simultaneously.
- The US military excels in conventional warfare but struggles in unconventional conflicts.
- Countries like Iran and North Korea plan to counter a US attack by transitioning to unconventional warfare.
- Beau suggests the need for a second entity, like a Department of Peace, to handle post-conventional warfare situations.
- He mentions that this second force could operate independently of the military and could be used to prevent conflicts.
- By establishing a successful track record, countries in need could request assistance from this peacekeeping force without conflict.
- Beau advocates for reallocating some of the defense budget towards this "world's EMT" entity.
- He argues that the current massive defense expenditures are no longer necessary given the geopolitical landscape.
- Beau concludes by hinting at the possibility of maintaining military superiority while reducing defense spending.

### Quotes

- "The largest Air Force in the world is the United States Air Force. The second largest Air Force in the world is the United States Navy."
- "We're fighting in a bunch of small conflicts constantly."
- "We'll come in and help. And doing it without conflict is better."
- "More than likely, what would happen is it just wouldn't grow for a few years."
- "The odds of having to go toe-to-toe with Russia and China at the same time is pretty slim."

### Oneliner

Beau proposes reallocating defense budget towards a Department of Peace to handle post-conventional warfare and prevent conflicts, advocating for maintaining military edge while reducing spending.

### Audience

Policy makers, activists

### On-the-ground actions from transcript

- Advocate for reallocating defense budget towards establishing a Department of Peace (suggested)
- Support transparent international efforts for conflict prevention through non-military means (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the current US defense spending rationale, the challenges in transitioning from conventional to unconventional warfare, and the potential benefits of establishing a Department of Peace.

### Tags

#DefenseSpending #MilitaryDoctrine #Peacebuilding #USMilitary #ConflictPrevention


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're gonna talk a little bit more
about being the world's EMT,
or the Department of Something Else,
the Department of Peace, whatever you wanna call it.
Anytime this concept comes up,
and the idea of a doctrinal shift from using the military
to using something else to nation build,
to help countries that aren't doing so well
or nations that have failed.
One of the first questions that always pops up is,
does that mean the DOD is going to get its budget cut?
And the answer to that is probably not.
Probably not.
You know, that stat gets thrown out all the time.
The United States spends more on defense
the next X number of countries combined. That's intentional. That's doctrine.
Current doctrine says the United States needs to be capable of fighting its next
two largest competitors. The United States needs to be able to fight China
and Russia at the same time and at least fight them to a stalemate. That's why we
spend so much money on it. I've said this before and when I said it the comments
reflected that people thought I was joking. The largest Air Force in the
world is the United States Air Force. The second largest Air Force in the world is
the United States Navy. No joke. Now that seems incredibly wasteful and it is, it
It is.
At the same time, this doctrine was adopted after World War II
because we had to fight the Germans and the Japanese
at the same time.
And we were like, wow, this is really hard.
We need to be prepared for it, right?
So that's how it came about.
I would point out there has not been a great power war
since this doctrine was adopted.
large countries, they don't go to war anymore.
This is a case of peace through superior firepower.
That's the idea.
That's the doctrine.
And thus far, it's kind of worked for that purpose.
The problem is people in the United States
want to justify it beyond this doctrine,
because nobody explains the policy.
So, well, we're not going to buy you more missiles
until you use the ones you have.
And next thing you know, we're fighting
in a bunch of small conflicts constantly.
And that leads us directly to the issue at hand.
When the United States goes into a country, it wins.
Conventionally, the United States wins almost immediately, always.
Here's where we're going to divide conventional and unconventional.
When you're picturing conventional warfare, that's World War II.
Lines, territory taken and lost.
There is not a country on the planet that can even come remotely close to competing
with the United States in that type of warfare.
There's not.
Realistically, there's probably not two countries
that could compete.
The United States excels at that.
And that's part of the problem.
The first part of the issue comes
in when the United States just obliterates
opposition military too quickly. No joke. That's what happened in Iraq. So when it
collapses then you have all of the little groups that spring up and then
that second phase of fighting begins. The unconventional side. The United States
doesn't do so well with that. No large power does. The other part to this that's
an issue is that most countries on our quote bad guy list, they know this. If you
look at the doctrines of Iran or North Korea on how they plan to deal with a
US attack, it's basically fight for like three days and then disappear into the
hills and go ahead and start that second phase, that unconventional phase, that
leads to the need for that second force because the skills are different. So
that's where the world's EMT comes in or the department of something else. If
you're looking at it from Thomas Barnett's point of view, this is when it
would show up. Now something I would point out is that I've never seen Barnett
talk about using that second force independently of the first, which is
totally something we could do. We don't actually have to have the war to use
that second entity, that second agency. That's how Marianne Williamson pictured
using it. Showing up in countries and just helping because war is bad. Realistically
it would work both ways. There would be times when it would be used without war.
There would be times when it would be used as a follow-on force to help avoid
that second phase of warfare, which is when the United States loses. So if a
track record of success was established using this world's EMT force, eventually
what would occur is countries that were in peril, countries where the government
was shaky, the people would ask for the force. They would ask to get assistance
and it wouldn't take a conflict. Just like the large military deters a great
power war, this second entity, Department of Peace, Department of Something Else,
world's EMT, it would deter that second phase of warfare. If we did it right, and
this was a transparent international effort, it could avoid the conflict
entirely, because it could be, hey, we'll come in and help. And doing it without
conflict is better from a a standpoint of the person on the ground in this
country. That's obviously the better move and we'll get into the different
ideological motivations for why people support this idea and they're varied but
at the end of the day what really matters are the people on the ground in
in the country that becomes the battle space, that
becomes the operational area.
So realistically, no, DOD would not shrink.
It might lose a little bit of its budget.
More than likely, what would happen
is it just wouldn't grow for a few years.
that budget would be diverted to this other entity. At some point, the United
States is going to have to reckon with its massive defense expenditures. This
doctrine, it made sense. It really did. It made sense after World War II. It made
since throughout the Cold War it doesn't make sense today. The odds of having to
go toe-to-toe with Russia and China at the same time is pretty slim, especially
considering the major competitors are all nuclear powers. We could probably
have our DoD budget and still maintain the edge that we have today. Anyway, it's
It's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}