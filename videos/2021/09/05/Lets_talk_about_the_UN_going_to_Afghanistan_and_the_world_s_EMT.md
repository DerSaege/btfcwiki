---
title: Let's talk about the UN going to Afghanistan and the world's EMT....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=kw-RhtDR-gw) |
| Published | 2021/09/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring broken nations and failed states globally.
- UN's mission restart in Afghanistan amidst debates.
- UN's role misunderstood - not about fixing countries.
- UN provides aid, treats symptoms, doesn't offer cure.
- Comparison of UN's actions to American military contractors.
- Lack of international agency for fixing broken nations.
- Proposal for an entity dedicated solely to nation-building.
- Critique of using Department of Defense for nation-building.
- Advocacy for stable governments responsive to people's needs.
- Support for shifting foreign policy focus towards nation-building.
- Mention of Marianne Williamson and Thomas Barnett's proposals.
- Advocacy for a shift from military intervention to nation-building.
- Importance of stability for peace and economic interests.
- Lack of specialized entities for fixing broken nations.
- Call for considering a new doctrine for global stability.

### Quotes

- "It's about as effective and most times looks just as silly."
- "They don't have a functioning system around them."
- "Stability is good for peace."
- "The government doesn't collapse in a week."
- "It's just a thought."

### Oneliner

Exploring broken nations globally, UN's role in providing aid and sparking a call to shift foreign policy towards stable, responsive governments for lasting peace and prosperity.

### Audience

Global policymakers

### On-the-ground actions from transcript

- Advocate for a shift in foreign policy towards nation-building (suggested)
- Support initiatives for stable, responsive governments in conflict areas (suggested)

### Whats missing in summary

The full transcript provides a comprehensive analysis of the UN's role in aiding broken nations, proposing a shift towards nation-building for lasting stability and peace worldwide.

### Tags

#BrokenNations #UN #ForeignPolicy #NationBuilding #GlobalStability


## Transcript
Well, howdy there, Internet people, Liz Bow again.
So today, we're going to talk about broken nations,
countries that do not really function, failed states, right?
We're going to talk about that, and we're going to talk about an oversight
in foreign policy, not just American foreign policy,
but foreign policy all over the world.
We're going to do this because the UN,
the United Nations is heading into Afghanistan.
They're gonna restart their mission there.
Now, at the time of filming, they're arguing about it
and saying they're not going to,
and they're going to, and back and forth, and back and forth.
They're going to, they're going to
because they don't have a choice.
They're the only option.
So they're gonna have to go.
Why are they gonna go?
Because the new government is, so far seems incapable
so far seems incapable of providing basic services to the people of Afghanistan.
So the UN is going to show up and fix it, right?
Wrong.
No, they're not.
They're going to show up and provide aid.
They're going to alleviate issues.
going to treat the symptoms, not offer a cure.
The United Nations isn't really tasked with fixing broken countries, not really what they
do.
Generally speaking, it's putting a bandaid on a bullet wound.
It's about as effective and most times looks just as silly.
Now for those overseas, yeah, we don't talk about how the UN behaves at times here in
the United States.
It's not something that gets a lot of coverage.
We tend to focus on our military contractors and their behavior.
For Americans, the most ready comparison for how the UN behaves at times are the worst
of American contractors.
It is definitely an imperfect system, doesn't really accomplish the goal of fixing the country,
was never meant to.
There is no agency.
There is no international agency.
is no national department that fixes broken nations. Doesn't exist and that
seems like an oversight because if there's a public health issue in a
nation that doesn't function so well, the World Health Organization shows up,
right? If there's a monetary issue, you have the IMF. But when a nation ceases
to function. There's nothing. There's nothing. Now at some point we'll go
through all of these various international organizations and talk
about what they do. Understand all of them have strings that accompany the
assistance. But the pattern that tends to emerge from a UN presence is the
country breaks. Whether it be an invasion, a war, natural disaster, a unique change
of government, however it happens, the country breaks. The UN shows up and they
just, they really do, they put Band-Aids on things. They alleviate issues for the
average person in the country. They try to anyway. And eventually some
semi-functioning, typically extremely corrupt, government emerges. And most
times the people of that country are back to where they started. They don't
have a functioning system around them. The idea of creating an international
entity that fixes broken nations has been around a long time and it has a lot of
support from a lot of places you might not expect. In the United States, you know,
we engage in nation-building all the time, right? But that doesn't make any sense because how do we
nation-build? What do we use? The Department of Defense, which is weird because that's really the
Department of War. They don't build nations, they break them. It doesn't do any good to attempt to
use them to nation-build, to fix these countries because that's not what they're trained to do.
they're trying to do the exact opposite. It's a very weird flip, especially when
you're asking them to show up, invade the country, engage in the combat, and then
fix it. It doesn't make sense. So the idea has been floated of having an entity
that's sole purpose is nation-building.
Providing assistance to countries all over the world to get them back up on their feet and
operating under a government of the people's choosing rather than one that DOD picks.
Because most times it's like, yeah, you're the Secretary of the Interior now.
I mean, it's not really that simple.
They take the person they want and they have an election against, you know, the guy who
owns the gas station downtown.
They give them options, but they know who's going to get it.
Because we try to pursue American interests rather than allowing the interest of the country
to dictate its needs.
If we did that, the United States would actually be better off in the long run because it would
be a stable government responsive to the people instead of a corrupt one responsive to backroom
deals with the United States. Who are people who have supported this idea, this general
shift in foreign policy from military to building? Marianne Williamson was one. Department of
Peace is what she called it. That's one end of the spectrum. The other side, Thomas Barnett,
geostrategist extraordinaire, right? He called it the Department of Something
Else because he spent so much time in the Pentagon he can't imagine calling
anything the Department of Peace. On this channel, the world's EMT instead of the
world's policeman, it's very well received by people. When I run into
somebody and they talk about something from the channel, if the subject turns
to foreign policy, that phrase almost always comes up. People like the idea
they would rather help than hurt, and the people in these countries they would
certainly rather get help than be hurt. But this entity doesn't exist. The best
that's out there right now is the United Nations, and these operations don't go
well. So the likely pattern will continue. They'll show up, they'll try to help as
much as they can. It's possible that civil conflict emerge between the
current new government and either the old government or somebody else that
wants to make a power grab. And that can spiral out of control at which point you
you end up with different militaries coming back in.
Nobody wants that.
That's kind of worst case.
Best case is that the current government kind of gets its act together enough to provide
basic services.
But there's no room for growth there because it's going to be corrupt.
And this isn't just limited to Afghanistan.
This scenario, this chain of events plays out all the time because there is nobody who
specializes in fixing broken nations.
This might be a shift that needs to occur.
It may be a new doctrine that the United States and the world want to take a look at because
Stability is good for peace.
And when you're talking about trying to convince people in power,
stability is good for money.
It's good for business.
This topic is going to be recurring all week.
We're going to talk about it in different ways
and different elements of it.
But the general idea is to shift from using the United States
military to nation-build and politically reliant countries, shift from that to
actually working to better the country itself and get it to stand on its own so
when the US withdrawals the government doesn't collapse in a week. Anyway, it's
It's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}