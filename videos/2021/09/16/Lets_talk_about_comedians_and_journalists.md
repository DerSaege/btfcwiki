---
title: Let's talk about comedians and journalists....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=5OSJNOSFHFI) |
| Published | 2021/09/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Questions the distinction between journalism and comedy in progressive TV programs like John Oliver, Stephen Colbert, and Trevor Noah.
- Points out that comedians often adhere to journalistic ethics better than major networks.
- Talks about the challenges of achieving objectivity in journalism and praises the Associated Press for striving towards journalistic ideals.
- Mentions the subjective nature of journalism and the importance of making editorial decisions.
- References Hunter S. Thompson and Gonzo journalism, which combines facts with a touch of fiction and humor to get to the truth.
- Applauds comedians for using their platform to better the world and move beyond being mere spectators.
- Addresses the difficulty of balancing objectivity and profitability in journalism.
- Criticizes the current industry practice of presenting "both sides" of an issue, which may not always be objective or accurate.
- Points out that many issues have more than two sides and some may only have one.
- Talks about journalists pursuing independent avenues to get closer to the truth outside traditional media constraints.

### Quotes
- "They're trying to get off the sidelines."
- "Once you acknowledge that objective journalism really isn't a thing, you know it's subjective."
- "If you end up with a huge platform like these comedians have, you'd have to be kind of a horrible person not to want to help."
- "At some point that got lost."
- "They're trying to avoid comments that say, well why don't you tell us the other side to this thing that is an objective fact."

### Oneliner
Beau questions the blend of journalism and comedy, praises comedians for upholding journalistic ethics, and criticizes the industry's struggle with objectivity and profitability.

### Audience
Content creators, journalists, viewers

### On-the-ground actions from transcript
- Support independent journalists and content creators who strive for truth and ethics (implied)
- Encourage critical thinking and fact-checking in media consumption (implied)
- Advocate for diverse perspectives and nuanced storytelling in journalism (implied)

### Whats missing in summary
Beau's insightful analysis on the evolving landscape of journalism and the role of comedians in filling gaps left by traditional media outlets.

### Tags
#Journalism #Comedy #Ethics #ObjectiveReporting #GonzoJournalism


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about journalism
and comedians
and ethical standards
related to journalism
and how
objective journalism
may not actually exist,
may not be a thing.
Ethics in journalism
seems to be a big thing for you. So I was wondering if I could get your opinion on a
matter that's been bothering me. Why do a lot of progressive TV programs call
themselves comedy
rather than news stations? People like John Oliver, Stephen Colbert, and Trevor Noah
all very definitively style themselves as disseminators of information,
yet in interviews always brush off claims that they're journalists,
but instead they're comedians.
Is it just to avoid scrutiny and ethics standards or what?
Now I don't think that's it at all.
The journalistic pieces that those comedians put out
most times hold to the ideals
of journalistic ethics
better than most major networks,
to be completely honest.
Journalistic ethics
are ideals.
They're something you strive for,
but in most cases you'll never achieve.
Being objective, it's really hard.
The largest outfit that I think does a good job with it is the AP,
the Associated Press.
They really
strive
to meet those journalistic ideals. However,
there's nothing saying that
while you're answering the question words,
well maybe this little piece of information right here doesn't need to be in it.
It's not really that important to the story,
and it's a decision that is made
by the journalist
who's writing it, and it's subjective.
And then you have the question of whether or not you should even cover something.
There are reams of information
that you will never find in a newspaper.
I don't believe objective journalism exists.
That school of thought is best personified
in Hunter S. Thompson, Gonzo journalism.
Gonzo journalism acknowledges
that straight reporting,
well that can get you to fact.
But facts
with a little bit of fiction,
a little bit of history
for additional context,
maybe a little bit of humor
stands a better chance of getting to truth.
I think that a lot of the comedians who are doing this,
they're doing it
because they have a platform.
And we live in a broken unethical world and they're tired of being spectators.
So they're trying to
use the platform they were given
to better the world.
I applaud them for it.
They're trying to get off the sidelines.
It's that simple.
I don't think that they're trying to avoid ethical standards
because in most cases
they really do a better job than most
professional journalists.
It's hard to maintain
both objectivity
and profitability.
That's an issue that the industry of journalism is dealing with.
If you don't classify yourself as a journalist,
and there's a lot of journalists who
have dropped that identifier.
In fact, on my
social media, I think the only place it still exists is on YouTube,
and that's only because I haven't gotten around to removing it.
It's hard to do both
because profitability, the current model in the
industry, is to present both sides.
The problem is a lot of times there's not two sides to an issue.
Not if you're being objective.
So to maintain profitability,
you have to sacrifice objectivity.
Some people don't want to do that.
Some people do not want to be
constrained
by
the idea that they should present both sides of an issue.
First off, most issues have way more than two sides.
Some only have one though.
This habit of entertaining things
that objectively aren't true
has pushed a lot of journalists out to where they're
going independent or they're pursuing other avenues
to get to what they perceive as the truth.
And that's the thing, is that once you step outside
of those
ethical standards,
it is perception.
Once you acknowledge
that objective journalism really isn't a thing,
you know it's subjective.
So then you have to be more careful
because you know your opinion is influencing it.
Which in a weird way actually makes you hold to the rest of the ethical
standards
a little bit better.
The United States,
in particular,
has a real issue
with the idea that an uninformed opinion
or an ill-informed opinion
is as valuable
as an informed one
or an objective fact.
And the current business model of journalism
suggests that, well, we should profile them all.
I think that there's a lot of people in journalism who don't want to do that,
who are tired of it and who see how damaging that business model is.
And then there's people
who are outside of journalism,
who may have never been a journalist,
who
see the gap,
the hole that's being created
by this model,
and they want to provide
their information.
And they want to try to help.
They want to try to better the world as they see it. Sure, it's perception, it's
subjective,
but they're trying to help.
And if you end up with a huge platform like these comedians have,
you'd have to be
kind of a horrible person
not to want to help.
I mean, I applaud them.
And for real, if you ever actually sat down
and just fact-checked,
I assure you that the comedians
are more accurate in their information
than the actual information they're presenting.
You know, you have to exclude the jokes
than
at least two major networks that I know of.
I don't think it has to do with them trying to avoid ethical standards. I think
that they are acknowledging that there is a
hole that needs to be filled
when it comes to
content that is put out.
They don't have to both sides something
and the thing is
the American people have been trained to
believe journalists are supposed to do that.
They're not. That's not part of it.
They're not supposed to both sides issues.
If Bill tells me it's raining and John tells me it's sunny,
I'm not supposed to
print both quotes. I'm supposed to walk outside
and see which is true.
At some point that got lost.
It created this hole for information
that is exploring
things objectively
but acknowledges that it's somebody's opinion.
They use comedy to do it.
Hunter S. Thompson just
made stuff up.
I try to insert
historical context to stuff.
There's a whole bunch of different ways people are trying to
to fill this gap
and I don't think any of it has to do with
trying to avoid ethical standards.
I think they're just trying to avoid
comments that say, well why don't you tell us the other side
to this
thing that is an objective fact.
It's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}