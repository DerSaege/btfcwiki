---
title: Let's talk about NVGs and masculinity....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=egmQG5V1UVk) |
| Published | 2021/09/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the concept of owning the night with NVGs left behind in Afghanistan.
- Providing examples of equipment like a medic's bag or an electrician's bag to illustrate the importance of training over tools.
- Critiquing the American mindset that having equipment equates to capability.
- Pointing out the fallacy in thinking that owning NVGs means owning the night.
- Emphasizing the significance of training and knowledge over equipment in achieving missions.
- Calling out the influence of this mindset on foreign policy decisions and domestic rhetoric.
- Challenging the habit of associating masculinity with violence and the militarization of the United States.
- Asserting that possessing tools does not automatically translate to proficiency in using them.
- Suggesting that opposition forces in Afghanistan are unlikely to dramatically change tactics due to acquiring night vision equipment.
- Concluding with the idea that tools do not make one capable, but rather training and knowledge do.

### Quotes

- "The gun doesn't make the man. The tools don't necessarily mean you know how to use it."
- "The reason those special teams, those high-speed teams, the reason they're special is because if need be they can accomplish the mission without all that stuff."
- "The problem is this idea is now so ingrained in American culture that it's influencing foreign policy decisions."
- "The militarization of the United States, the habit of conflating masculinity with violence, this needs to stop."
- "It's not how it works. It's the training. It's the knowledge."

### Oneliner

Beau challenges the American mindset that owning equipment equals capability, stressing the importance of training over tools and critiquing the influence of this concept on foreign policy and domestic rhetoric.

### Audience

Americans

### On-the-ground actions from transcript

- Challenge the mindset that owning equipment automatically equals capability (implied).
- Advocate for investing in training and knowledge over solely focusing on equipment (implied).
- Educate others about the importance of skills and expertise in achieving missions effectively (implied).

### Whats missing in summary

Beau's passionate delivery and nuanced breakdown of the misconception that owning tools equates to skill, urging for a shift towards valuing training and knowledge over equipment.

### Tags

#AmericanMindset #TrainingOverTools #Militarization #ForeignPolicy #KnowledgeIsPower


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about a concept that keeps popping back up
and it is such an American concept
but it only applies in a very narrow scope.
I had somebody tweeted me and I know this conversation is going on in a whole bunch of different places
but I had somebody tweeted me that
you know
the NVGs that were left behind in Afghanistan, well they're a huge deal
because now the opposition owns the night.
Owns the night.
That's wild to me.
That is wild to me.
This is an M39.
It's a medic's bag.
This is basically an urgent care facility in a bag.
This one has had the stuff to deliver a baby and some other stuff added to it.
If I give you this, do you know how to use it?
I mean sure you could probably apply the Ace bandage in it
or you know
the band-aids, the smaller stuff.
But do you know how to use this
to its
best effect?
I'm going to guess you don't.
Not most people.
You may be able to pull a scalpel
out of
a surgery kit
and make an incision.
But can you actually complete a surgery
simply because you have the equipment?
No, of course not.
That's silly.
I have an electrician's bag
and inside it
it has everything you need to pull power from the pole
and wire a house.
If I give you the bag,
would you be willing to do it and then sleep in that house with the power on?
Probably not, right?
But when it comes to the profession of arms,
for some reason
Americans go all John Wayne.
The gun makes the man.
If you have the equipment,
well suddenly you're capable of doing stuff.
I don't know what people are picturing here. Do they think the opposition is now
going to be seizing airfields in the middle of the night without an audible shot?
That doesn't even make sense.
If it did, if this concept was true and you could just hand people the equipment
and then
they're as good as
our elite units,
well then we could increase
US military might
exponentially
simply by giving every soldier the same equipment the SEALs have.
And when you say it like that, I mean all of a sudden you realize how silly it is.
Because it's the training.
It's learning how to use the tools, not having them.
The reason those people, those special teams, those high-speed teams, the reason
they're special
is because if need be
they can accomplish the mission without all that stuff.
Because they're that well trained.
It's not the tools.
Owning NVGs, owning night vision does not mean that you own the night.
That's not how it works.
This concept
is wrong.
The gun doesn't make the man.
The tools
don't
necessarily mean you know how to
use it.
Spending a whole bunch of money with Blackhawk or Condor doesn't actually
make you an operator.
That's not how it works, not how any of this works.
The problem is this idea
is now so ingrained
in American culture
that it's influencing foreign policy decisions.
That's an issue.
And it's influencing
rhetoric at home.
There's a whole bunch of people who
who use very revolutionary rhetoric
that I'm fairly certain
are not really aware of what they're asking for.
The militarization
of the United States,
the
habit of conflating
masculinity
with violence,
this needs to stop. It's getting out of hand and it's funny it's coming from
a silly tweet over NVGs.
This shows how deeply this is entrenched.
It's not how it works.
It's the training.
It's the knowledge.
I do not foresee
opposition forces,
I'm sorry, de facto government forces
in Afghanistan suddenly running and gunning at night.
It's pretty unlikely.
Those who use that stuff, basically you're going to have some field commanders that now have
night vision equipment they use as binoculars.
That's how it's going to be employed.
Just like somebody with a scalpel can make an incision
but not really know how to use that scalpel to its fullest potential.
This isn't a game changer.
And presenting it as if it is reinforces that idea
that the tools
make you capable of doing this.
That the gun
makes the man.
It doesn't
and it never has.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}