---
title: Let's talk about what spears can teach us about creating change....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=C4xhJZsmFIY) |
| Published | 2021/09/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the importance of achieving change and how to do it effectively.
- Draws an analogy using military terminology, referring to the "tip of the spear."
- Explains the different roles within social movements: the tip, the rest of the spear, and the support staff.
- Emphasizes the significance of not alienating those who may not fully understand the movement's ideologies.
- Talks about the negative effects of trashing individuals who are helping in carrying out the message of change.
- Urges for more inclusivity and understanding within progressive movements.
- Stresses the need for ideas to reach a wider audience to create real change.
- Critiques the infighting and ideological purity that can slow down progress.
- Encourages being less critical and creating a more welcoming environment for those willing to support the cause.

### Quotes

- "If you're part of the tip of the spear, you're part of the ideological advance. You're always going to be ahead."
- "Going after and trashing the people who are helping to carry it out, even if they have a bad take, maybe go a little bit easier on them."
- "There shouldn't be a lot of gates thrown up. They should be very accessible."
- "You can't hold them accountable for something they literally do not know."
- "If you want change, the idea needs to get to as many people as possible."

### Oneliner

Beau explains the dynamics of social movements through a military analogy, stressing inclusivity and understanding to achieve effective change.

### Audience

Change advocates

### On-the-ground actions from transcript

- Support and embrace individuals willing to carry out the message of change, even if they may not fully understand the ideologies (suggested).
- Encourage inclusivity and understanding within progressive movements (exemplified).

### Whats missing in summary

The full transcript provides a detailed analysis of the dynamics within social movements and the importance of inclusivity and understanding for effective change.

### Tags

#Change #SocialMovements #Inclusivity #ProgressiveIdeas #IdeologicalAdvance


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about change
and how best to achieve it, maybe.
And Spears.
We're going to talk about Spears.
We're going to do this because I got a message. It was pretty funny.
It was basically like, hey, I really appreciate the way you talk about
historical social movements
and you try to draw analogies,
but you never give us any actually useful advice. Quote.
Thanks for that.
And then it goes on to ask what my biggest criticism
of social movements today is,
and this was specific to progressive-type movements.
So we're going to talk about Spears,
because it ties in very nicely with this morning's video.
If you've ever been around people in the military, you have heard the phrase
tip of the spear.
That's part of an analogy,
and I'm going to briefly explain it.
This is the tip of the spear, right?
Who are these people in the military?
These are people who pull triggers.
Okay. When you think of soldier,
this is who you're thinking of.
Who are these people?
These are people who deploy.
They go downrange.
They participate actively in the mission,
but they may not be actively involved in combat.
Who are these people?
This is most people in the military.
This is most people in the military.
These are people who fix trucks,
listen to radio intercepts,
look at satellite photos,
cook food, just do paperwork.
This is what gets all the attention.
This is what most people associate with the idea of being a soldier,
and this is definitely important.
Without this, you don't win,
but without this, there's no forward movement, right?
This isn't going anywhere if this doesn't exist and it's not strong.
So let's take this and apply this to movements that are looking for change.
Who's the tip?
These are people who run aid networks.
These are people who organize demonstrations.
These are people who participate in advanced demonstrations.
Who's the rest of the spear?
These would be people who show up.
They put in the time and they have an ideological basis.
They believe in that cause and they understand it.
I would put most of online activity here, right?
Who's this?
And if you don't know, don't feel bad,
because I've asked this question 50 times in my life.
I have yet to get an answer.
Who is this?
Who are these people?
Okay, well, let's go back up here.
These are all people actively participating in the mission.
What is the mission if you were talking about change?
The concept, the overall mission would be to get that idea
into normal political discussion.
Who is this?
Who supports that?
Who helps that happen?
These would be people who are not ideologically pure.
These are people with bad takes 30% of the time, right?
They generally agree with the overall idea.
They want forward movement, but they're not quite there on some things.
They're not on point.
Pardon the pun.
You know who else is here?
People who don't look at things in terms of left and right.
People who just don't have an ideological framework.
They are ideologically blank.
They look at things in moral terms of right and wrong.
Progressive movements, they tend to just surrender this battle,
which doesn't make any sense to me,
because most times the progressive stance is the sense
that most people would see as the moral one.
But for some reason, this battle gets ignored.
But even though there's not a lot of active recruitment of these people,
they exist and they help create forward movement.
You know who else is here?
The people who show up at a demonstration,
take a selfie, and then leave.
What's the mission?
To get that idea into normal political discussion, right?
Those normal people, those people who show up,
take the selfie and leave, performative.
You've probably heard that as an insult.
I love the performative.
I love it.
Because that person, they're not involved up here.
Their friends almost certainly aren't.
So when they do that, sure, you can say they're doing it for social credit,
and that's probably true,
but they're cosigning that event, that idea, that movement.
They're helping put it out to the main political discussion
when they slap it on Instagram.
If the idea is to change society, you have to change thought.
These people back here, they're really important to it.
In most social movements today, these people trash these people constantly,
like nonstop, nonstop.
That's probably not great for morale.
It is not great for creating a strong support staff.
And it also doesn't make sense because most progressive or left ideas,
the ideas for them to be egalitarian, they should be open.
There shouldn't be a lot of gates thrown up.
They should be very accessible.
In short, these people are the people who these people would say
need to read some theory.
That's who they are.
And that's how they get described.
People that don't really know.
Yeah, they may not, but they're willing to help.
They're willing to help create that forward movement.
Now, the reason this happens is pretty simple.
These people, they are on the cutting edge of the idea.
Let's take something at random.
Law enforcement, right?
These people are at consent-based policing.
They're at social workers.
They're at abolition, maybe.
These people, they just realize there's a problem.
It's going to take time with that forward movement for them to catch up.
But that's how it's supposed to be.
Because the idea is to always be moving forward.
If you're part of this group and you ever get to the point
where the majority of the people back here think the way you do,
you're moving too slowly.
If the rear has caught up, you need to drive forward.
They're supposed to be behind you.
There's not a reason to trash them.
Performative.
Good example, recently, a lot of people said that AOC's dress was performative.
And as somebody who defended it, yeah, absolutely it was.
Totally was.
But I would ask how many thousands or tens of thousands
or hundreds of thousands of people said,
tax the rich for the first time in their life over the last week?
It works.
It works.
To take it out of this beer analogy and put it into something a little bit more relatable,
whatever industry you're in, you make widgets, right?
You make widgets.
You work for a company that makes widgets
and you are one of the people that actually makes the widget.
Trashing the support staff,
that's kind of like slinging open the door to the marketing department
every time they put out an ad and just screaming,
you didn't do anything.
You didn't make that.
I don't know what good it does because if it doesn't get out there,
you're not going to make more widgets.
Or worse, you could see the ad in public and say something that undermines it.
To me, that's one of the biggest holdups.
That's why it is so slow going to get these ideas into main political discussion
because there are a lot of people who appear, at least in some ways,
to want to keep these ideas on the fringe because it's cooler, I guess.
I don't know.
And some people, they're genuinely angry.
They're upset because those people back there, they haven't caught up yet.
They're not where they need to be.
They're not on point.
They're not going to be.
If you're part of the tip of the spear, you're part of the ideological advance.
You're always going to be ahead.
You're always going to be up there.
It's kind of lonely, I'm sure.
You're always going to have people who are disagreeing with you,
who may be on your own side, who may be supportive of you generally,
but they're going to disagree on an issue because they don't get it yet
because you're further ahead.
Not understanding that, not applying that,
and the level of, I don't want to use the word,
but I can't think of another one, infighting that it creates.
That's a huge detriment.
It slows things down a lot.
If you want change, the idea needs to get to as many people as possible.
That's the mission.
You want as many people as possible to embrace that idea.
Going after and trashing the people who are helping to carry it out,
even if they have a bad take,
even if they're not doing it out of an ideological motivation,
even if they're just taking a selfie,
if they're helping carry that message out,
maybe go a little bit easier on them
because I would imagine that there is a chilling effect.
People don't want to talk about the ideas
because they may not want to get jumped on by people who they know, kind of,
but they're not there ideologically yet.
I don't think it's a good idea to trash people
for not having the perfect ideological take
when deep down you have to acknowledge they, you'd say it.
You would probably say it.
They need to read some theory.
You can't hold them accountable for something they literally do not know.
That to me is the biggest overall hold back.
That's the thing that is slowing things down a lot
and it's pretty easy to fix.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}