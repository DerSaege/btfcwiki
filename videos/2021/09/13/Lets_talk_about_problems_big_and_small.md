---
title: Let's talk about problems big and small....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=_b7Uf3ZnoKw) |
| Published | 2021/09/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A friend accidentally started a community network to build wheelchair ramps for those in need.
- The group faces additional problems at each house they visit beyond accessibility issues.
- Often, they can't solve all the problems they encounter, leading to feelings of inadequacy.
- Despite the challenges, Beau believes in the impact of small actions on people's lives.
- He acknowledges that individuals or small groups can't solve all the world's problems.
- Beau stresses the importance of doing what you can with the means and resources available.
- He encourages not to get discouraged by the inability to solve every problem.
- Small acts of kindness may have a significant impact on someone's life.
- Beau urges people to focus on the issues they can solve and not be overwhelmed by the abundance of problems.
- He advises taking any starting point and getting active, as more opportunities to help will arise.

### Quotes

- "You do what you can, when you can, where you can, for as long as you can."
- "Don't get discouraged because you can't solve every problem."
- "Focus on the ones you can solve."
- "Once you find it, stick to it."
- "And you'll find your calling, the thing that you can do the most good with."

### Oneliner

A reminder from Beau to focus on making small but meaningful impacts, knowing you can't solve every problem in the world.

### Audience

Community members

### On-the-ground actions from transcript

- Start a community network to address specific local needs (suggested)
- Help build wheelchair ramps for those in need (suggested)
- Get involved in food banks or similar initiatives to support communities (suggested)
- Focus on solving accessible problems in your community (exemplified)

### Whats missing in summary

The full transcript provides a detailed narrative on the challenges faced by small community groups and individuals trying to make a difference in their communities, underscoring the importance of persistence and focusing on achievable goals.

### Tags

#CommunityAction #SmallActsBigImpact #LocalInitiatives #CommunityEngagement #ProblemSolving


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about problems big and small.
I got a friend, I talked to him last night,
and he's feeling a little down.
He's feeling a little down.
See, a few months ago he accidentally
started a community network.
Didn't mean to, but it happened.
It's him and two guys from his job site.
He works at a construction site.
And somebody's second cousin's third grandma or something
needed a ramp built at their house, a wheelchair ramp.
And they're like, well, we'll do it.
And the boss, he's like, yeah, I have no problem with you
all taking scrap to go and make people's homes more accessible.
Yeah, have at it.
Do what you need to do.
So they went and did it.
Supposed to be a one-off thing.
Supposed to be just favor for a friend of a friend of a friend.
And then they got a call.
And they did it again the next weekend.
And now they're doing it like every other weekend.
They're helping to make people's homes more accessible.
But he feels down because he says
that almost every time he goes to one of these houses,
there's some other problem that needs to be addressed.
And a lot of times, they can't do anything about it,
something beyond making the home more accessible.
One of the examples he gave, and this one
they were actually able to solve,
they were lowering the counters in a home
and realized that there was nothing in any of the cupboards.
They didn't have any food.
So he wound up reaching out to another community network
that kind of runs a food bank and got food.
In that case, they were able to do something about it.
But a lot of times, they haven't been able to.
They haven't been able to solve all of their problems
with the people that they're helping.
I think that this is one of the main reasons
that people who get active and get out there and get hands on,
I think this is one of the main reasons they burn out.
Because there are so many problems, big ones,
big problems that individuals or small groups of people,
realistically, they can't solve them.
Just keep in mind that there's no problem too small either.
What they're doing is materially improving people's lives.
It's making their lives better.
But because there's so many other issues, he feels down.
And I think for a lot of people, this
is why they never get started.
Forget burnout for a second.
I think there's a lot of people who look at all of the issues
and say, well, what can I do?
You can lower the counters.
You may not be able to solve all of the problems,
but nobody's asking you to.
Nobody thinks you can.
There are a lot of issues in the world
today that people like us, we can't solve.
We don't have the power to solve them.
Even if we do end up coordinating
with some other network that has more resources,
it's still going to be too much.
But nobody's asking for one little group of people
to solve all the problems of the world.
You do what you can.
If you have the means, well, then you
have the responsibility.
You do what you can, when you can,
where you can, for as long as you can.
And that's all anybody can ask.
Don't get discouraged because you can't solve every problem.
The little ones, they may mean more to people than you think.
They may really be altering the way they live.
As we hopefully start to get back to normal,
and more and more people get active again,
and get out there and get hands on,
don't burn out because you can't solve all of the problems.
There's a bunch.
There's more than enough to go around.
Focus on the ones you can solve.
And if you are feeling deterred because you don't know
where to start, just pick any of them at random.
Because once you get active and get hands on, believe me,
more will come up.
And you'll find your calling, the thing
that you're really good at, the thing that you
can do the most good with.
And once you find it, stick to it.
Anyway, I hope you found this helpful.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}