---
title: Let's talk about soft language and sensationalism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dUEQveTKH90) |
| Published | 2021/09/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the power of words and their meanings, focusing on keeping people engaged in a discourse.
- Mentions receiving a message questioning his avoidance of certain terms, advocating for precise and direct communication.
- Explains his choice of using softer and less emotionally charged words to maintain audience engagement.
- Shares insights from analytics, particularly the significance of emotionally charged words in retaining viewers' attention.
- Describes a shift in his rhetoric from using inflammatory and emotional language to softer tones to encourage critical thinking.
- Conveys the importance of reason and rationality in changing minds rather than emotional appeals.
- Refers to a poll indicating the impact of softer versus strong rhetoric on public opinion.
- Advocates for employing soft language in everyday communication to foster independent thinking.
- Acknowledges the limitation of soft rhetoric in creating like-minded individuals but values critical thinking and open debate.

### Quotes

- "If you make it through the first 30 seconds, you will make it through the main body."
- "That's more my goal."
- "It just creates people who are thinking on their own."
- "If people are thinking reasonably and rationally, there's room for discussion and debate."
- "Y'all have a good day."

### Oneliner

Beau explains the power of words, advocating for softer rhetoric to foster critical thinking and open debate, aiming to change minds through reason over emotion.

### Audience

Communicators and debaters

### On-the-ground actions from transcript

- Adjust your communication style to use softer and less emotionally charged words to encourage critical thinking and open debate (suggested).

### Whats missing in summary

The full transcript provides a nuanced exploration of the impact of rhetoric on audience engagement and critical thinking, offering practical insights for communicators and debaters.

### Tags

#PowerOfWords #Rhetoric #CriticalThinking #Communication #Debate


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about words, the power of words,
words and their meaning,
which we've kind of been doing most of last week.
And I'm going to give you some tips
that I use to keep people engaged when they might otherwise shut down.
You know, I'm sure it's happened to you where you've been talking to somebody about a topic
and then all of a sudden,
boom, they just shut down, they're not listening anymore.
And I'm going to give you some tricks that I use.
And hopefully you can use them too.
Over the last week, we have been talking a lot about words and what they mean.
That's been the theme.
And even though the videos don't seem connected, they are by that element.
The Biden mandate video, the video about the gas station,
the word triage, the video about the word boy, the Bail Harris video,
all of these had elements of it in it.
And it's in the middle of making these videos that I get this message.
Appreciate your show. Thanks for being a voice of reason.
One question, why are you so coy when talking about certain terms?
And they actually list a few that they have realized that they're words I don't use.
Why public health crisis?
There are numerous public health issues that predate the current one,
and they give a list of them.
When using an indirect and imprecise term,
you not only give the audience room to misunderstand your meaning,
you also rule out discussing relevant details.
When you say that law in Texas,
when referring to the one deputizing vigilante bounty hunting citizens
for a law too unconstitutional for the state to enforce,
it could also refer to a new gun law or voter suppression measures.
Sorry to be picky, but euphemisms undermine persuasive power
and messages and limit discussion.
I used to 100% agree with this.
I really did.
I don't anymore, and I'm going to tell you why.
Yeah, there are words that I don't use on this channel,
and they're not words you might expect.
We talk about foreign policy and conflicts a lot on the channel.
Ever heard the phrase enemy troops?
Opposition.
It's not emotionally charged.
I use a lot of soft language.
Yeah, I say public health issue rather than the word that everybody else is using.
So why?
Because y'all told me to.
Y'all told me to.
YouTube gives people who have channels a crazy amount of information
in the analytics, like a whole lot of information,
reams and reams of charts and graphs that you can go through.
And if you do that, you can find out some really interesting stuff.
In fact, most people who complain about the algorithm,
if they went through their analytics, they'd find the problem.
One of the most interesting graphs to me
is one that shows the percentage of people
who watch a video through minutes and seconds.
It will give you the exact time stamp and the percentage
of people watching at that point.
I pay real close attention to this one.
What it tells me first is that if I can get you
through the first 30 seconds of a video,
you're probably going to watch the whole thing.
The majority of people do.
The overwhelming majority of people get through the tell them phase.
If you're writing an essay, you have an introduction, body,
and conclusion, right?
In normal conversation, tell them what you're going to tell them.
Tell them.
Tell them what you told them.
The main body.
If you make it through the first 30 seconds,
you will make it through the main body.
Almost everybody, like 90%, something like that.
And then it slowly starts to taper off during the conclusion.
That's what a normal video looks like on that graph.
However, every once in a while, it'll be floating along at 90%,
and then it will drop to 70 or high 60s.
And invariably, I can go to that exact time in a video,
and I will find an emotionally charged word.
In this list of words, it's always changing.
If you go back to the beginning of our current public health issue,
you'll see that I used the word pandemic back then.
But then that documentary quote came out, and that word became divisive.
It became polarized.
And if you used the wrong version of it,
you didn't use the play on words of it, people tuned out.
In this quick case, quite literally, they would tune out.
They'd turn off the video.
This happens in everyday conversation, too.
If you use the wrong term in the wrong spot, people will shut down.
So I use a whole lot of soft language, a whole lot.
And theoretically, it keeps people engaged.
It keeps people watching and therefore thinking,
because I don't use emotional language.
I used to.
I used to completely believe what this message said.
I used a lot of inflammatory rhetoric, stuff like that.
This was years ago.
And what I realized is that that was a really good way
to get a group of people who already think the way you do,
who already think the way you do.
That's what that's useful for.
If you're trying to provoke independent thought,
trying to provoke conversation, that strong rhetoric doesn't work.
Doesn't normally work.
And I think that on the people who it does work on,
people who can be swayed by an emotional argument or strong rhetoric
or whatever, it's superficial and they can be swayed back
by another emotional argument.
However, if you appeal to reason, rationality,
that critical thinking takes hold and it's harder for them
to be moved back through an emotional argument.
And yeah, I used to use a lot of inflammatory rhetoric.
And then one day I made an analogy.
Yeah, go figure, right?
I made an analogy and it was a bad one.
It was bad.
I mean, like the space laser lady bad.
It was just all bad.
It was very easy to take it in a way that I didn't mean it.
And somebody who actually agreed with the point that I was making
was like, you know, that was pretty messed up, dude.
I took the correction.
I didn't do it again.
So I stopped using it.
I stopped using inflammatory rhetoric.
I stopped using strong language.
And I started using soft language.
And it got softer over the years.
And this weird thing happened.
I stopped getting messages that said, I agree with you,
and started getting messages that said,
I changed my mind about something.
And that's more my goal.
So that's why there's soft language.
Again, that strong rhetoric, that appeal to emotion,
it has a place.
But it's for encouraging people who already believe what you do.
I don't think you're going to successfully change anybody's mind
or get them out of the information silo they're in by using strong rhetoric.
Now, it's easy for me to say that, pointing
to two pieces of anecdotal evidence, my analytics and my inbox.
Is there anything that's publicly available that would suggest that's true?
Yes.
Absolutely.
There was a poll done.
And it said that 63% of Americans support teaching our kids, K through 12,
about the lingering effects of slavery.
Now, if you're going to do that, you have
to teach about the power structures that kept those effects in place.
And if you're cool with doing that, odds are you're probably also cool
with examining other issues with race that have occurred in American history.
63% OK with talking about the lingering effects of slavery
and the institutions that kept those in place.
In the same poll, the same respondents, only 49%
were OK with teaching critical race theory because it's a polarized term,
because it's politicized.
Strong language, right?
It's shifting.
Five years ago, it wouldn't have been a politicized word.
It wouldn't be a word that I would avoid, or at least not say
to a way past 30 seconds into a video.
So there is evidence to suggest that if you're going to try to reach people,
you want to use softer language.
If you want to try to reach them through reason and rationality,
you want to use softer language.
Because by that poll, you're talking about more than 10%
of people that are kind of up for grabs as long
as you're not using that strong language.
So that's why I do it.
And I think it's something that you could probably
employ in your everyday life when you're
talking to people who have fallen down into some echo chamber
and they can't get out.
You know, it might work.
Now, the danger of this is that this method, the soft language,
it doesn't create people who think the way you do.
It just creates people who are thinking on their own.
But I believe that if people are thinking reasonably and rationally,
even if they don't think exactly like you,
there's room for discussion and debate.
And then you can move further if you can.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}