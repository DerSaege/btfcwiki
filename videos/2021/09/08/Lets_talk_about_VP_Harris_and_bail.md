---
title: Let's talk about VP Harris and bail....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=jaF-l0btSpo) |
| Published | 2021/09/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Vice President Harris is facing backlash from certain media outlets due to her past donation to a bail fund that bailed out someone now accused of murder.
- Bail in the United States is money put up to ensure one shows up for court, not to keep them in jail.
- Beau believes that bail funds should not exist in the United States, as their existence indicates something is wrong with the system.
- Standard bails have become a thing where specific crimes have set bail amounts, which can be problematic based on one's financial status.
- Excessive bail violates the Eighth Amendment, which prohibits requiring excessive bail and suggests that bail funds shouldn't exist if people can't post bail regularly.
- The accusation alone against someone is enough to smear others, like Vice President Harris, due to the lack of presumption of innocence in the United States.
- Beau argues that if bail was allowed for a person, it should be cheap enough for them to get out, ensuring they show up for court.

### Quotes

- "I'm going to suggest that their mere existence is evidence that something is very wrong wherever that fund exists."
- "I don't believe that bail funds should exist in the United States. I don't think they should be allowed to exist in the United States."
- "The purpose of bail is not to keep people in jail. It's just to ensure that they show up for court."
- "If there was evidence of that, that's on the judge."
- "It's just a thought. Y'all have a good day."

### Oneliner

Beau challenges the existence of bail funds in the US, arguing that they indicate underlying issues and can lead to excessive bail, undermining constitutional rights.

### Audience

Advocates for criminal justice reform

### On-the-ground actions from transcript

- Advocate for bail system reform (exemplified)
- Support organizations working towards fair bail practices (exemplified)

### Whats missing in summary

The full transcript provides a detailed examination of the flaws in the bail system in the US, urging for reform and raising awareness about constitutional rights violations.


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about bail and Vice President Harris.
We're going to bail Vice President Harris out.
That's what we're going to do today.
If you don't know, she has found herself in some hot water with certain media outlets.
They're not happy at the moment.
At some point in the past,
Vice President Harris donated some money to a bail fund.
Then that bail fund went on to bail somebody out, as bail funds tend to do.
The person they bailed out, well, they're now accused of murder.
Fox News has somehow tied this together,
and somehow it's Vice President Harris's fault, I guess, that this happened.
Now, if you don't know what bail is,
because it doesn't exist and operate the same way everywhere,
in the United States, bail is money you put up to ensure that you show up for court.
You give the money in most cases to the Sheriff's Department, to the county jail,
and they hold that money to make sure that you show up.
That's what it is.
It's not an amount of money that is set to keep you in jail.
The purpose is that you can pay it and get out.
That's why it exists, so you can prepare your defense and all that stuff.
I'm going to be honest.
I don't believe that bail funds should exist in the United States.
I don't think they should be allowed to exist in the United States.
Now, I've donated to a whole bunch of them.
I understand why they do exist,
but I'm going to suggest that their mere existence is evidence
that something is very wrong wherever that fund exists.
The purpose of bail is not to keep people in jail.
It's just to ensure that they show up for court.
That's it.
That's its whole purpose.
What has happened is that standard bails have become a thing,
meaning if you're accused of crime X, it's $500.
If you're accused of crime Y, it's $1,000.
Crime Z, it's $10,000, and so on.
Let's take the $1,000 one.
For you or I, more than likely, $1,000,
that's going to make us show up for court.
We post that money, we get out, we show up for court
because that's a pretty sizable amount of money to most people.
But what if you make a whole bunch of money?
Let's say you make $1.1 million to make the numbers easy, right?
Then you don't care.
You post the bail and you don't care about going to court
because it's just $1,000.
In fact, you post the bail, you go home, you go to sleep.
If you sleep for eight hours, by the time you wake up,
you've made that money back.
So you do not care, right?
And that's the problem with set bails for specific crimes.
Not just does it work that way, it works the other way as well.
What if you don't make a lot of money?
Then you can't get out of jail, so you sit in jail.
By definition, I'm going to suggest
that if a person cannot afford bail,
if the judge says your bail is $1,000
and the person can't afford it,
I'm going to suggest that that's an excessive amount of bail
because the whole point is not to keep them in jail,
it's to make sure they show up for court.
For people of lower means, that money is,
for lack of a better word, it's more valuable.
A lower amount would encourage them to show up.
And if they don't have a lot of money,
they're not going to be able to successfully run either.
So that whole concept of a bail fund
indicates that excessive bail is being imposed.
I would point out that the Eighth Amendment
says that that shouldn't happen.
It is a constitutional right to be bailed out.
Being upset that someone was bailed out is,
I mean, no surprise, it's coming from Fox,
but that's kind of undermining a constitutional principle.
You're allowed bail,
and it's written into the Constitution
that excessive bail can't be required.
That means that bail funds shouldn't be a thing,
not because it's not good, but because if they exist,
if people with any regularity can't post bail,
that means that excessive bail is probably being imposed
and a constitutional violation is occurring.
Harris isn't to blame because a fund that she donated to
bailed somebody out who then got charged with another crime.
Now, what I would also point out is that this person
who's being accused of this, right,
the mere accusation is enough to smear Harris,
who never met this person,
because the presumption of innocence
doesn't really exist in the United States.
Now, from what I understand, to be completely fair,
there does appear to be a lot of evidence
that this person did it,
but that's beside the point.
The mere accusation is enough
to go after Harris for posting bail.
Now, if you're saying that Harris,
who never met this person, never interacted with him,
should have known that this person
was a continued danger to society,
you're looking to the wrong place.
If there was evidence of that, that's on the judge,
because if bail was allowed, it can't be excessive.
If it's not allowed, that's determined by the judge,
if the judge determines
that they're a continuing threat to society.
But anybody who is allowed bail,
it should be cheap enough for them to get out.
That's the whole point of it.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}