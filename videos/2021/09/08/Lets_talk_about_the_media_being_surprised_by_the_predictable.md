---
title: Let's talk about the media being surprised by the predictable....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=tcRAD-aVry8) |
| Published | 2021/09/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the inclusion of opposition members in the new government in Afghanistan, suggesting it's unusual.
- Compares the incorporation of former conflict figures into the government to practices in other countries.
- Expresses mock shock at the idea of war heroes being part of a government, hinting at perceived hypocrisy.
- Points out the surprise at former prisoners now holding government positions.
- Mocks the media's shock and outrage over the situation, implying it's a common occurrence globally.
- Questions why commentators are acting surprised and trying to provoke outrage when such scenarios are normal.
- Challenges the media's attempt to manipulate public opinion for ratings by sensationalizing common events.

### Quotes

- "Can you believe that? I mean, I can't think of another country on the planet that would do that."
- "It's shocking, I tell you. Shocking."
- "If you're surprised by this development, if you're shocked and you believe that this is something Americans should be outraged by, I demand to know who you believed was going to be in these positions."
- "They just believe that they can convince the American people to be outraged by it, and therefore milk those ratings."
- "Anyway, it's just a thought. Y'all have a good day."

### Oneliner

Beau questions the mock outrage over Afghanistan's government formation and challenges media sensationalism for ratings.

### Audience

Media Consumers

### On-the-ground actions from transcript

- Challenge media narratives (implied)
- Be critical of sensationalism (implied)

### Whats missing in summary

The full transcript provides a satirical take on media reactions to commonplace political events, urging critical thinking and questioning of sensationalized narratives.

### Tags

#Government #Media #PoliticalCommentary #Sensationalism #PublicOpinion


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we are going to talk about the new government over there and who's in it
and the reaction to it by American commentators as they discuss a completely
just out of the ordinary chain of events.
Now, I don't know if you know this.
In case you don't, let me let you know, clue you in on what's happening.
The new government over there, the new government in Afghanistan, there are people from the
opposition in it.
Can you believe that?
I mean, I can't think of another country on the planet that would do that.
I mean, these people, they were involved in the conflict.
I mean, I can't think of any place on the planet that would take a perceived war hero
and put them in a government office.
I bet you can't either.
In fact, if you can, I have a shiny quarter and a dime for you.
I mean, that's bizarre.
I mean, can you name any country on the planet where like a general then became the first
president?
I mean, prime minister?
I mean, that seems really bizarre.
That's shocking.
I mean, I'm outraged by it.
I mean, the next thing you know, they'll name the capital city and a state after them.
It's shocking, I tell you.
Shocking.
You should be surprised by this development.
And if you don't know, the thing that's really surprising is that there were people that
were held by Western forces, that were held prisoner, that are now in the government.
Unbelievable.
It is so surprising.
I can't believe that happened.
I mean, there's no way that like a former POW in the United States would ever be in
government from, I don't know, January 3rd, 1987 to August 25th, 2018.
I mean, that wouldn't happen.
That would be bizarre.
Right?
I mean, and it's very surprising given the fact that some of these people actually got
let out of cells to participate in negotiations.
Who could have known that they would have had great influence over that group?
You should be shocked.
You should be outraged.
Now to be clear, I'm not drawing parallels between the individuals.
I'm just talking about the situation.
And there are some differences, you know, when it comes to the situation itself.
I mean, it's...
That was more akin to a civil war.
And it's not like Grant became...
Oh.
Yeah I guess that happened too, didn't it?
This thing that the media is acting super surprised about and very shocked and trying
to provoke outrage about, it is completely normal.
It happens in pretty much every country, everywhere, all the time, since the beginning of time.
This isn't surprising.
In fact, as somebody who often consumes major media, I demand to know who y'all expected.
If you're surprised by this development, if you're shocked and you believe that this is
something Americans should be outraged by, I demand to know who you believed was going
to be in these positions.
Because I've been thinking about it, trying to figure out who y'all might have perceived
as being up for the job, and I'm just flummoxed.
I can't come up with any names.
Because this is what always happens.
It doesn't seem like these commentators, these pundits, are actually surprised or outraged
by this.
They just believe that they can convince the American people to be outraged by it, and
therefore milk those ratings.
It's what it seems like.
I mean, it's not like it's an incredibly common practice since the beginning of recorded history.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}