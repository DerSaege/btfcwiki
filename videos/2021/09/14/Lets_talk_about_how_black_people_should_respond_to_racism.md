---
title: Let's talk about how black people should respond to racism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Apn6wXIO--4) |
| Published | 2021/09/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau receives a message asking how black men should respond to being called the word "boy" and the subsequent attacks that follow.
- The message describes the internal struggle of feeling tense, accused, and pushed towards violent reactions when faced with racism.
- Beau shares his personal experience of limited encounters with racism compared to the continuous discrimination faced by black men.
- He acknowledges the privilege of being able to respond violently without severe consequences due to his skin tone.
- Beau points out the hypervigilance and constant readiness for attacks that result in a form of PTSD from racial discrimination.
- Despite not having a clear solution, Beau encourages the message sender to use platforms like YouTube to convey positive messages without filtration.
- He suggests that finding solutions to dealing with racism may be an internal journey rather than seeking external advice.

### Quotes

- "Nobody likes an angry black man."
- "How do we deal with this literal torture?"
- "I don't have a solution. I don't have any advice."
- "Sometimes the question alone is the thought."
- "Y'all have a good day."

### Oneliner

Beau addresses the challenges faced by black men when responding to racial slurs and violence, pointing towards internal reflection and positive messaging as potential paths forward.

### Audience

Black men, allies

### On-the-ground actions from transcript

- Utilize platforms like YouTube to convey positive messages without filtration (suggested)
- Engage in internal reflection on how to respond to racial discrimination (implied)

### Whats missing in summary

The full transcript dives into the internal struggles and external pressures faced by black men in responding to racial slurs and violence, urging for internal reflection and positive messaging as potential ways forward.

### Tags

#Racism #BlackMen #PTSD #PositiveMessaging #InternalReflection


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about a follow-up question
to the video about the word boy.
And this is a really long message,
but I'm going to read the whole thing.
So just hear it out.
Hey Bo, take this thought train down the tracks a little further, could you?
Let's talk about how black men should respond
to a person that chooses to use that word against us, the word boy.
Now I know what you're thinking, and it's probably along the lines of,
nah, I ain't telling any black man how to react to that word.
You called that.
That's exactly what I was thinking when I first read this.
But I think you can pull it off.
The reason I'm asking this is that as a black man,
it's because when someone says that to me,
I automatically get tense and stiffen up a little bit, understandably.
The problem is what immediately happens after that.
I get accused of being soft, a snowflake, a race baiter, or uppity.
You get the idea.
Anything to further shake me up and get me riled.
Anything to push me farther towards doing something
that can get me in a lot of trouble.
And it's always with a smile.
I'd be lying if I didn't have thoughts of absolutely destroying
each and every one of those people.
But I don't.
I stay very, very quiet before I end up in jail or dead.
When they leave, I end up punching something.
Usually it's my own head.
I hit myself to shake myself out of the rage that was building.
Sometimes I cry when I'm alone.
I've been dealing with this for the last 40 years of my life.
I must have been called boy at least 30 or 40 times.
I've been told to leave establishments.
I'm told to move on.
I'm told to shut my mouth by the person doing
the attacks and society at large.
Nobody likes an angry black man.
So how do we deal with this literal torture?
How do we address PTSD?
At what point do we get to defend ourselves?
Is it baked into the proverbial cake
that I must endure it because it's
my lot in life in this country as a black person?
Am I obligated to take these licks
because it's better than bringing myself down
to their level?
Is it unbecoming for a black man to get
mad at the racist and society at large that harbors them?
Because nobody likes an angry black man.
How does a positive message get out for black men
when society won't allow black men to speak up?
I've heard tons of African-American speakers
on the subject.
I'd like to hear it from a guy on the other side of the pool.
I hate questions like this.
I can't even relate to this.
The closest I come to experiencing racism myself
is when somebody offhandedly says something about natives
not knowing I'm a tribal member.
Or when I was a teen and I was partying
with my cousins who look native, and the cops rolled up.
And they all looked at me because I
was the one that needed to go talk to them.
And that gets to something you're pointing out.
And one of the problems with somebody like me giving advice
on something like this, you can't do what I can do.
You can't respond the way I might to some of this.
I mean, it's been a long time.
But I'm not above laying somebody out and spending
the night in jail.
Just because I prefer peace doesn't
mean I forgot how to be violent.
But you can't do that.
Not really.
I mean, I don't do it because it's not the right option.
But if I did, what would happen?
The cops would roll up, tell me to put my hands on my head,
take my hands off my head, put them behind my back, cuff me,
stuff me in the car.
I'd go to county, go to sleep, wake up really early
in the morning for some reason, eat not enough food,
go to my arraignment, and bond out.
That's what would happen to me.
Maybe they rough me up a little bit.
You know what's really unlikely?
That they throw me on the ground and sit on me till I'm dead.
That's probably not going to happen,
because my skin tone isn't viewed as a weapon.
I can't disagree with anything that you're saying here.
I don't think you're using the wrong term.
I'm sure somebody's going to say something about PTSD,
because the idea of a lot of people
is that it only happens during war.
But a big part of it is hypervigilance.
It's being on guard all the time, waiting for that attack,
not knowing when it's going to come
or where it's going to come from,
not knowing how long it's going to last
or how severe it's going to be.
I would imagine that sounds really familiar to you.
I don't think that's the wrong term.
I didn't read this one, because I have an answer to it.
I read it because of doubt.
I can't even relate.
The only thing I have is a suggestion
to one of the follow-up questions.
How does a positive message get out for black men
when society won't allow black men to speak up?
Man, if you speak half as well as you write,
if you can convey ideas like this verbally in this manner,
you have a YouTube account, use it.
You can put that positive message out there,
and it won't be filtered by somebody who looks like me.
I don't have an answer.
I know what the options are.
You can respond the way I might,
but that's probably not good for your health.
You could just ignore it and accept that after all this time,
there are still people like this.
But that seems like it's asking a whole lot.
You can try to educate them as you meet them.
Honestly, that just sounds exhausting.
I don't think you'll get any advice of value
from outside on this one.
I think this is an internal thing.
I don't have a solution.
I don't have any advice.
I read it not because I have an answer, but because I don't.
And sometimes the question alone is the thought.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}