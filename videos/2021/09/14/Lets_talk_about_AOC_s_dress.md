---
title: Let's talk about AOC's dress....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=z5oPjR5D35w) |
| Published | 2021/09/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- AOC wore a dress saying "tax the rich" to the Met Gala, sparking controversy.
- Right-wing attacks often target AOC, trying to paint her as an outraged elitist.
- Wearing a message like "tax the rich" while being wealthy isn't hypocritical; it's ideologically consistent.
- Beau addresses the implication that AOC spent $30,000 on her Met Gala ticket.
- He suggests that even if she spent that money, donating it to a cause like a museum wouldn't be a bad thing.
- Beau points out the fuzziness between politicians and celebrities, implying that AOC likely got her ticket for free.
- The media's spin on the story can distort the reality of the situation.
- Beau criticizes the outrage-driven coverage and the attempt to create controversy where there is none.

### Quotes

- "Telling a bunch of rich people to their face through a dress that you think they should be taxed more is also not hypocritical. That's remaining principled."
- "This just goes to show how the media can spin a story or fabricate one."
- "It's designed to provoke outrage where there is none."

### Oneliner

AOC's dress controversy at the Met Gala showcases media spin and false implications, revealing the attempt to fabricate outrage.

### Audience

Media consumers

### On-the-ground actions from transcript

- Support museums or causes like the Costume Institute (exemplified)
- Question media narratives and seek out multiple sources (exemplified)

### Whats missing in summary

The full transcript provides a deeper understanding of media manipulation and the importance of questioning sensationalized narratives.

### Tags

#AOC #MediaSpin #Controversy #Narrative #Outrage #DressControversy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about a dress that AOC wore,
because that's in the news.
But we are going to get to play one of my favorite games,
and it's called Let's Pretend That's True for a Second.
For those overseas, because I get this question every time
she's in a video, AOC is the nickname
of an up-and-coming politician.
She's in the House of Representatives, and she is the subject of frequent right-wing attacks.
They try to make her the outraged a juror as often as possible.
And this is no exception.
She wore a dress that said, tax the rich, to the Met Gala.
The headlines are AOC wears dress that says tax the rich to event that costs
$30,000 to attend. The implication here is twofold. One, that she personally
dropped 30 grand to go to this thing and two, that it would somehow be
hypocritical to wear a dress saying tax the rich if you were rich or were around
a bunch of rich people? Let's pretend the implication is true for a second, that
that's actually what happened. She did spend this money and all of this stuff.
Now to be clear, it's not. It's probably not what happened and we'll get to that.
First, if you are not rich and you believe that we should tax the rich and
then you become rich and you hold that belief, that is actually not being
hypocritical. That's being ideologically consistent. It's the exact opposite of
hypocrisy. Telling a bunch of rich people to their face through address that you
think they should be taxed more is also not hypocritical. That's remaining
principled. I know it's confusing to a lot of people but that's that's what it
is. Now let's say that she did drop 30 grand to go to this thing. Now to be
honest, I think tickets are actually $35,000. And yeah, I mean, that does
seem kind of wasteful. If she didn't need the money, perhaps she could donate it to
something. I mean, she obviously cares about fashion. Maybe she could donate it
to, I don't know, a museum that protects and catalogs tens of thousands of
artifacts that's called the Costume Institute and is actually the cause that
was benefited by the Met Gala. Maybe she could do that. Because, I mean, that would
be a headline, right? If that actually occurred and it wasn't something that was
just trying to provoke outrage, the actual headline would be that a
congressperson donated about a fifth of their yearly salary to a museum. And when
you say it like that, all of a sudden that doesn't sound like a bad thing. It
almost sounds noble. I mean sure it's probably not the cause that I would
support but I mean the arts are important. I'm sure that somebody's gonna
say that she could have taken that money and given it to causes that support
homeless veterans because that's the cause that everybody says they want to
support anytime they don't want somebody to support a different cause. But let's
be real honest for a second. If all the people who made posts like that just put
quarter in a jar every time they made a post like that, the homeless vets could afford to attend
the Met Gala. But sadly and unsurprisingly, there's still a lack of funding. It's almost
like the people who say that are really just wanting to use them yet again. So there's no
hypocrisy here, and if she actually did do this, which she probably didn't, it's a noble act.
she's literally giving up a fifth of her income to support a museum. That's not a
bad thing. What probably happened though is that the she got her ticket for free
because she's a celebrity. That line between politician and celebrity well
it gets fuzzy at times. So the Met they paired her up with a designer and she
got her ticket for free as long as she wore the article of clothing that the
designer gave her. Because that's what always happens. That's why the Met is
able to charge 35 grand to people to get into this thing. Because they get to meet
a bunch of famous people. They get to rub elbows with them. This event raises 13 or
$14 million for that museum. So what is being implied in these headlines probably
didn't happen, but if it did happen it's not hypocritical and it's actually kind
of noble. This just goes to show how the media can spin a story or fabricate one.
If what they're implying is actually what a card, they're kind of saying that donating
a massive amount of money to a museum is a bad thing.
And I mean, that certainly is a take, I guess.
I don't know that it's a good one.
At the end of the day, this is just like a lot of the coverage that we're getting.
It's designed to provoke outrage where there is none.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}