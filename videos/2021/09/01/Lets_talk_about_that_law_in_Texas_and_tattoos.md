---
title: Let's talk about that law in Texas and tattoos....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=oADVHiFayGU) |
| Published | 2021/09/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing tattoos and what they teach about a new law in Texas.
- Explaining the base belief behind questioning personal choices.
- Criticizing the idea of legislating morality with examples like prohibition and the war on drugs.
- Pointing out the ineffectiveness of using laws to control behavior like visible tattoos.
- Describing how people of different economic classes navigate around restrictive laws.
- Arguing that such laws target and impact those without means and a voice.
- Revealing the true intention behind legislation like the new law in Texas.
- Stating that the goal is not to end the behavior but to provide judgmental people with someone to judge.
- Condemning the prioritization of such laws over more pressing issues in Texas.
- Concluding that these laws serve to create scandal and foster judgment rather than solve problems.

### Quotes

- "There's a base belief among a lot of people that suggests however you run your life is how you think society as a whole should be run."
- "So when you go on vacation, you get your tattoo. You cross state lines, do it somewhere else."
- "It's about taking a bunch of judgmental people and giving them somebody to judge."
- "There are a whole bunch of things going on in Texas that are way more important than this."
- "It's designed to create scandal and give you somebody to look down on."

### Oneliner

Beau addresses the ineffective nature of trying to legislate morality, using tattoos to illustrate how such laws disproportionately impact those without means, ultimately serving to provide judgmental individuals with targets rather than solve real issues.

### Audience

Policy Makers, Activists

### On-the-ground actions from transcript

- Advocate for policies that address real issues impacting communities, rather than targeting and marginalizing vulnerable populations (implied).
- Support initiatives that prioritize the voices and needs of marginalized groups in legislative decision-making processes (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how legislation aimed at morality often fails to achieve its intended goals and instead exacerbates existing inequalities and injustices in society.

### Tags

#Legislation #Inequality #Society #Morality #Texas


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about tattoos and what they can teach us about that law
in Texas, that new law.
Because I got a message and it was pretty entertaining.
I'm sure you're going to get a bunch of people asking you to talk about the new law in Texas.
That's true.
I did.
And I'm sure that if you do it, it's just going to be a rehash of all of the other times
you've covered this topic.
That's also true.
It really would be.
My views on this haven't changed.
So instead of that, can you answer a question?
I know that you're on the choice side, but why then do you have so many kids?
I actually get this question way more than you would think.
But the easy answer here, the easy out, is that we made the other choice.
We're on the choice side.
But that's the easy answer.
That's the easy out here.
It doesn't address the actual sentiment that causes this question.
There's a base belief among a lot of people that suggests however you run your life is
how you feel society as a whole should be run.
And that you should support the state legislating it that way.
That's just all wrong.
That's just all wrong.
I don't like visible tattoos.
I don't have any.
If I'm wearing a shirt and shorts, you don't see any tattoos on me.
My wife, she has a visible tattoo because she has bodily autonomy.
So let's say I decide I'm going to try to take that away because I want to enforce my
viewpoint on the entire society using the power and violence of the state.
So I lobby legislators to pass a law saying in the state of Florida you can't have a visible
tattoo.
You can't tattoo somebody if it's in a visible location.
What happens?
The same thing that always happens when you try to legislate morality.
When you try to legislate things like this, whether it be prohibition, whether it be this
debate, whether it be the war on drugs, anything, the same thing always happens.
Nothing.
Not really.
It doesn't stop whatever the behavior is that the legislation is supposed to end.
It doesn't stop that behavior.
That behavior continues.
In this case, let's take the tattoo case.
What happens if you're middle class?
You have a little bit of means, right?
So when you go on vacation, you get your tattoo.
You cross state lines, do it somewhere else.
That's what happens.
Middle and upper middle class.
If you're really wealthy, well, you just hire an artist to come to you.
Do it in the privacy of your own home.
Nobody has to know.
What if you're poor?
What happens then?
More than likely, there's going to be people who are trying to fill that black market.
Now they may not be as good, they may not be as safe, but they'll provide that service.
And then you end up actually making the whole thing worse.
You make it less safe.
Because they don't have the money.
They don't have those power coupons that can exempt them from the law.
And they don't have the means to get out.
So like many times when people try to legislate morality, what actually ends up happening
is it's just another way to kick down.
Those who are in the right classes, well, they're exempt from it.
They can find a way around it.
It's not a big deal.
But those on the bottom, when they get busted, oh, they're horrible people.
The same thing is going to happen here.
It is the same thing that's going to happen here.
So what's going to happen in Texas with this new law?
It is mainly going to impact people of low income because they can't get around it.
But it's not actually going to stop the behavior.
Just going to make it less safe.
Seems like a weird legislative priority.
But then again, it's not actually a legislative priority.
The goal here with this legislation is not to end this behavior.
It's not to stop it.
They know it won't.
It's about taking a bunch of judgmental people and giving them somebody to judge.
Because if you're looking down, well, you're not paying attention to what the people above
you are doing.
And that's what this is about.
It's going to be pretty restrictive for lower income people.
Low class, wealthy people, not so much.
They'll find a way around it.
So as is typically the case, these laws that are trying to legislate morality like this,
they end up targeting those people and impacting those people who don't have money and therefore
don't have a voice and therefore can't swing an election.
Because there's that desire to judge people, to look down on people.
And politicians know they can lean into that anytime their policies fail.
And that's what's happening in Texas.
There are a whole bunch of things going on in Texas that are way more important than
this.
But then again, this isn't designed to be effective.
It's designed to create scandal and give you somebody to look down on.
Anyway, it's just a thought.
Have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}