---
title: Let's talk about foreign policy, American vs Soviet style....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=MAvGxM1YojQ) |
| Published | 2021/09/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Foreign policy is like a poker game where everyone's cheating.
- Americans view foreign policy as poker, Soviets view it as chess.
- Poker allows for more players and acknowledges cheating.
- Chess is elegant, strategic, and confrontational.
- Soviets' foreign policy was ideologically driven and confrontational.
- Poker analogy includes more players, like Vietnam and Afghanistan.
- Acknowledging other countries adds layers of fluidity in foreign policy.
- Everyone cheating in poker mirrors foreign policy dynamics.
- Chess results in conquest, while foreign policy is ongoing.
- Foreign policy is about power, not money.
- Power is the currency in foreign policy.
- Poker analogy is a better way to understand foreign policy dynamics.
- The analogy is a trope but offers a more comprehensive view.
- Beau suggests asking the Soviet embassy for their perspective on the analogy.

### Quotes

- "Foreign policy is like a poker game where everybody's cheating."
- "Poker analogy includes more players and acknowledges cheating."
- "Power is the currency in foreign policy."

### Oneliner

Beau explains how foreign policy is akin to a poker game with cheating, contrasting American and Soviet views, advocating for the poker analogy's inclusivity and fluidity over the elegant but confrontational chess comparison.

### Audience

Foreign policy enthusiasts

### On-the-ground actions from transcript

- Call the Soviet embassy and ask for their perspective on foreign policy analogies (suggested)

### What's missing in summary

Exploration of the nuances and intricacies of foreign policy dynamics and power struggles beyond traditional comparisons like poker and chess.

### Tags

#ForeignPolicy #Analogies #PowerDynamics #Inclusivity #SovietPerspective


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about an analogy
and foreign policy and poker and chess.
I'm very fond of using the analogy that foreign policy is
like a poker game where everybody's cheating.
I've said it repeatedly on this channel.
I've used it for a really long time.
So I was not surprised when somebody sent me a message
saying, I find it interesting that you use that analogy
because my friends who studied foreign policy
during the Cold War always used to say
that Americans view it as poker and the Soviets view it
as chess.
And I've heard that a lot.
When I first started using the analogy,
somebody else was like, you know it's a trope that Americans
view foreign policy as poker and it's kind of an insult, right?
Whatever, it's a better analogy, you know?
And I didn't back down from it.
Because I do.
I believe it's a better analogy.
If you want people to understand it,
that's the way to look at it, not chess.
Chess has rules.
Acknowledge that people cheat first.
When you look at the Soviet model of viewing it as chess,
what is it?
It's elegant.
It is strategic.
It is well thought out.
And their foreign policy was all of that.
Ideologically driven, long term, and confrontational.
It is confrontational.
Two major powers, because that's how they viewed it.
The Soviet Union versus the United States,
or more broadly, East versus West.
But see, with the poker analogy, you
can still have two main players.
You and I, we may have the most chips at the table.
We have the two biggest piles.
But it doesn't exclude everybody else.
Because if we're not paying attention,
somebody can come out of nowhere and clean us out.
Like, hypothetically, Vietnam or Afghanistan.
Right?
It's a better analogy.
The other countries, they're not just spectators.
They're involved.
And when you acknowledge them, you
acknowledge the different layers of fluidity
that come with foreign policy.
The dynamics that are constantly changing.
Because of the decisions of countries
that may not really seem to matter in the chess analogy.
And when you acknowledge that everybody's cheating,
that just brings it home.
Because showing a card here, or sliding one there,
in hopes of getting something on the back end,
oh, that is foreign policy.
That's what it's like.
That makes more sense.
And then with a chess game, what happens?
You know, you play the game.
Somebody wins, somebody loses.
That's it.
The other person, the opponent, they're conquered.
It's over.
It's not how foreign policy works anymore.
We don't really conquer each other.
We play the game, and we meet up every week to play it.
Yeah, you may lose this week, but you'll be back.
It's not as final, which makes more sense to me.
Because it is never ending.
And then you have the added dynamic of the chips.
There's something that's keeping score as the game progresses.
And with poker, it's money.
When you're playing poker, you're typically
playing for money, especially if you're cheating.
Not so much the case with chess.
Now, foreign policy, it's not about money.
Foreign policy is about power and nothing else.
However, if we're being really honest with ourselves,
what's currency?
Power coupons?
The means with which to obtain power?
It's a better analogy all the way around.
I fully embrace it.
I know it's a trope.
I know it's an insult in some ways, in some circles.
I think it's better.
And I can point to a definitive little element
that shows it's a better way of looking at it.
Call the Soviet embassy and ask them what they think.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}