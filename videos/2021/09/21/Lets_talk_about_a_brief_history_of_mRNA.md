---
title: Let's talk about a brief history of mRNA....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=PuDUHbmEl7E) |
| Published | 2021/09/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the timeline of research on mRNA vaccines dating back to the 1960s.
- Mentions emergency FDA authorization for COVID-19 vaccines in 2020.
- Points out that mRNA technology is not as new as perceived.
- Describes the history of mRNA vaccine trials on mice and humans.
- States that the basis of this research predates major historical events like the Cuban Missile Crisis.
- Encourages not to hesitate based on the longevity of research.
- Suggests that understanding past research can aid in looking towards the future.
- Emphasizes that looking back at historical context is beneficial for understanding current advancements.

### Quotes

- "This isn't actually new technology. It's been around for a really, really long time."
- "The basis of this research, it started before the Cuban Missile Crisis."
- "Sometimes looking back can help you look forward a little bit."

### Oneliner

Beau explains the extensive history of mRNA technology, debunking the notion of rushed research for COVID-19 vaccines and encouraging understanding past research for future progress.

### Audience

Research Enthusiasts

### On-the-ground actions from transcript

- Educate others on the historical timeline of mRNA vaccine research, exemplified
- Encourage individuals to look into the background of scientific advancements, exemplified

### Whats missing in summary

The full transcript provides a comprehensive overview of the extensive history of mRNA technology, debunking misconceptions about rushed research for COVID-19 vaccines and advocating for informed decision-making based on historical context.

### Tags

#Research #mRNA #Vaccines #History #COVID19 #Science


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about research
and how long research normally takes.
Because a common theme, a common question
that's going around right now as far as vaccination goes
is the research that went into it
and whether or not it was rushed.
So we're going to kind of catalog it.
You know, a lot of the vaccines that we rely on today,
the initial research began like in the 1960s.
So relying on something that's a little bit more recent
has raised eyebrows.
So let's take a look at it.
OK, emergency authorization from the FDA occurred in 2020.
And it's the new technology, the mRNA technology,
that has most people questioning it.
The first clinical trials of lipid nanoparticles
delivering mRNA vaccines was in 2015.
So it's not quite as new as everybody's making it out
to be.
And it's also worth noting that prior to that, in 2013,
there was a clinical trial of an mRNA vaccine for rabies.
And before then, the first mRNA vaccines
were tested for mice in 1993.
And liposome delivery of mRNA vaccines
first occurred in 1990, also to mice.
Lab-made mRNA, that first occurred in 1984.
The liposome delivery of mRNA to cells first occurred in 1978.
Liposomes being used to deliver vaccines,
that first occurred in 1974.
Liposomes being used to deliver drugs first occurred in 1971.
The first lab-produced proteins from isolated mRNA was in 1969.
And mRNA was discovered in 1961.
This isn't actually new technology.
It's been around for a really, really long time.
The basis of this research, it started before the Cuban Missile
Crisis.
We are talking about an event that is so old,
it occurred before an event that, when
it's depicted in movies today, they put it in black and white.
It's not really new technology.
It's the application of research that
has been around a very, very long time.
I don't know that this is a good basis for hesitancy.
Sometimes looking back can help you look forward a little bit.
That link to everything that's gone on in the past
typically ends up being pretty helpful.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}