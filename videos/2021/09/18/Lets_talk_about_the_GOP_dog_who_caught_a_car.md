---
title: Let's talk about the GOP dog who caught a car....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=vTEo6fchMlQ) |
| Published | 2021/09/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Republicans are likened to a dog who finally caught the car but doesn't know what to do.
- Democratic candidates were vocal about police reform pre-election, but backed off when it came to defunding the police.
- Only about 20% of Americans support drastic police reforms, while a larger percentage supports smaller reforms.
- Republicans pursued policies supported by a small vocal group, like banning certain family planning methods.
- The majority, around 80% of people, believe family planning should be legal in some form.
- Republicans passed drastic bills on family planning, banking on courts to strike them down, but now face consequences with a conservative Supreme Court.
- Politicians often chase small groups to the ballot box on various issues without intending to follow through on drastic policies.
- The strategy of appealing to small groups is evident in gun rights issues as well.
- Beau questions how Republicans will mitigate the damage caused by their recent actions.
- Winning for Republicans in this context is described as a "catastrophic success" with potential long-term damage.

### Quotes

- "Republicans are a little bit like a dog who finally caught the car and they don't really know what to do right now."
- "This is what is a catastrophic success, because long-term I have a feeling this is going to be incredibly damaging to them."

### Oneliner

Republicans chase small vocal groups to the ballot box, risking alienating the majority in pursuit of drastic policies with uncertain consequences.

### Audience

Politically aware individuals

### On-the-ground actions from transcript

- Watch for politicians chasing niche issues and hold them accountable. (implied)
- Advocate for policies that represent the majority, not just vocal minorities. (implied)

### Whats missing in summary

The full transcript gives a detailed analysis of political strategies and the potential consequences of alienating the majority in favor of catering to vocal minority groups.

### Tags

#Politics #RepublicanParty #DemocraticParty #Policy #Consequences


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
how Republicans are a little bit like a dog who finally caught the car and they
don't really know what to do right now.
They messed up. They messed up at a standard play in American politics and
it exists on both sides of the aisle. It's something they all do. A good
example of how it works is, we'll use the Democratic Party as an example,
alright. In the run-up to the election, Democratic candidates were pretty
supportive of drastic police reform, right? Like we're talking about
reallocation of funding and stuff like that. They were very vocal about it. The
thing is, only about 20% of Americans actually support that. Only about
20% of Americans actually support the drastic reforms. Now a whole bunch
support the smaller ones, right? Once they got elected, what happened the first
time they were asked to put their name next to the idea of defunding it? No,
they didn't do that. And that just makes good political sense because you don't
want to alienate the 80% in favor of the 20%. Now you do have some
ideologues who are actually trying to mainstream that idea, the way that we've
talked about in the past. But for many it was just a political move. We'll talk
about it and we'll chase those voters in their car, we'll chase them to the
ballot box, yipping along behind them. Never having any intention of actually
making that policy go into effect. There are a bunch of constant issues like this
as well. One such issue is family planning. If you look at polls, the
percentage of people who actually believe the kind of family planning that
was more or less banned by Texas's law, the percentage of people that support
banning that, who really believe that should be illegal, it's between 18 and 20%.
It's not actually a large group. They just have a lot of money and they're very
vocal. 80% of people believe it should be legal in some way. Now there's variations
as to how accessible they think it should be. But 80% of people
believe it should be legal. Republicans caught the car. They chased this group to
the ballot box over and over and over again, yipping behind them, acting like
they were going to do something about it. And along the way they propose drastic
bills that they don't really think are going to pass, or if they do pass, they know
that the courts will strike them down. But see, now they've passed one of those
drastic bills and they have a conservative Supreme Court. They can't
count on the courts to rescue them. If this bill isn't struck down, if this law
isn't struck down, it's going to cost Republicans dearly across the country.
They will have alienated that 80% in favor of the 20%. It was a bad move and
now they don't really know what to do. That's why, if you notice, you haven't
had a lot of national politicians chiming in on this, because they know the poll
numbers. They know that most people don't actually want this. This concept of
politicians chasing small cars to the ballot box, it exists in a lot of stuff.
Guns is a good example too. Most of that stuff is decided by the courts. It's not
really about legislation at this point. But they use it as an issue to appeal to
small groups and they talk a big game about it and then don't really do
anything about it. I'm very curious to see how Republicans intend on mitigating
the damage that winning has caused them here. This is what is a
catastrophic success, because long-term I have a feeling this is going to be
incredibly damaging to them. So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}