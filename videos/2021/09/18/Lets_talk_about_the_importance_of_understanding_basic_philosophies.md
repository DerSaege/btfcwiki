---
title: Let's talk about the importance of understanding basic philosophies....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RvFE91UgM6Q) |
| Published | 2021/09/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Stresses the importance of understanding different philosophies beyond common misconceptions.
- Shares a comment about taxing the rich and the limitations of progressivism.
- Talks about the wealth gap and the changing economic landscape in the country.
- Describes a homesteader's perspective on value, wealth, and ownership.
- Connects the homesteader's ideals to leftist economic philosophies like communism.
- Mentions the disconnect between people's actual beliefs and their understanding of economic philosophies.
- Encourages gaining a basic understanding of various economic theories and philosophies.
- Points out that many rural people unknowingly live by leftist economic ideals in their daily lives.
- Addresses the stigma and confusion surrounding leftist economic philosophies in the United States.
- Emphasizes the need to look beyond common associations and understand the core philosophy behind terms like communism.

### Quotes

- "The real problem that no one addresses is the system that allowed the wealth to get sucked up to the top in the first place."
- "We have thrown money at these problems, and have only proved they cannot be solved with money."
- "You are to the left of me. What you are describing is communism."
- "But if you do, you're better informed."
- "Don't let the way words get used in common conversation dictate whether or not you're going to look at the philosophy behind that word."

### Oneliner

Understanding different economic philosophies beyond misconceptions can lead to surprising insights and inform personal beliefs, even for those who may not identify with leftist ideals.

### Audience

Philosophy enthusiasts, economic thinkers.

### On-the-ground actions from transcript

- Read up on different economic theories and philosophies to gain a better understanding (exemplified).
- Challenge common misconceptions about leftist economic ideals in your community (suggested).

### Whats missing in summary

The full transcript provides a comprehensive exploration of the impact of economic philosophies on individual beliefs and societal structures, urging a deeper understanding beyond surface-level associations.

### Tags

#EconomicPhilosophies #Progressivism #WealthInequality #Communism #LeftistIdeals


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about why it's important to get a base
understanding of the various philosophies that are out there.
And not just go off of the preconceived notion that you may have
based on the way the word gets used in common conversation in the United States.
Because a lot of times, the philosophy and the way the word gets used,
they're
not the same. In fact, sometimes they're not even close.
And if you have a base understanding
of the various philosophies, if you come across an issue
and you're looking at something and you're thinking, this is the real problem right here,
you might know that somebody's already devoted a whole lot of time
to talking about that exact issue. And while you may not agree with their take
completely, it may give you a foundation
to come up with your own idea, your own philosophy.
We're going to do this because under that video
where I was talking about Spears, somebody left a message,
somebody left a comment, and I don't mind
saying that because I've seen this person comment for a long time. They have
a very thick skin. They're not going to mind this at all.
And this is the comment. Tax the rich won't work
because there aren't enough rich. The main problem I have with progressivism
is that the only solution it offers is the redistribution of wealth.
The real problem that no one addresses is the system that allowed the wealth to
get sucked up to the top in the first place.
There was a time in this country when a bricklayer, a plumber, a carpenter, an
electrician, or a mechanic
could make a living, buy a house, and raise a family on the income of plying his
trade.
That time is gone, and these are things that can't be outsourced to China.
There was a time in this country when a man who had as little as 40 acres of
land
could provide for himself and raise a family
on what he could produce on that land and contribute
food to the greater society as well.
The system has not adapted to changing conditions
in a way that benefits the majority of citizens.
The progressive solution, some form of socialism,
has already been proven a failure everywhere it has been tried.
We have thrown money at these problems, money at these problems,
and money at these problems, and have only proved
they cannot be solved with money.
Any good ideas out there? I'm at a loss.
Okay, this person is a homesteader, and that's important because it
factors into the rest of it.
People who homestead, they're people who have made the conscious decision
to live without the state
as much as humanly possible.
So we can use that as a basis for where this person is coming from.
On top of that, we know that
money isn't really what they're looking for.
They're looking for a standard of living. They're looking for value,
not necessarily cash,
because that helps narrow down the different philosophies.
And then we're talking about somebody who wants the mechanic
who turns the wrench to retain
most or all of the value, the wealth,
that is created by them working. They don't think it should go to the person
who owns the wrench and the lift. It should go to the mechanic. The mechanic should own that.
The mechanic should own the means of doing their work,
and then they should get the value that they create
by working. And if everybody does this,
well, everybody benefits.
Everybody puts out what they can, and they get what they need.
So the farmer can feed
himself and his family and contribute to greater society.
So this, in essence,
would eliminate that
wealthy class of people, those people who own stuff and just leverage
that ownership to accumulate more wealth.
They would end up being gone. It's not an uncommon
philosophy among rural people, and as a homesteader, this person is probably pretty rural.
So what do we have?
We have a stateless, classless,
moneyless society where the wealth is retained by the workers.
You're to the left of me.
You are to the left of me. There is a person who has written about
this exact problem and has these same solutions that you're kind of
indicating in this message.
You are to the left of me. What you are describing
is communism. Don't take it as an insult. We're talking about
the philosophy. We're not talking about the way it gets used in the United States,
which means totalitarianism. We're talking about the actual philosophy.
That's what you're describing, and it's not really a surprise.
I've said it before. Rural people, we live our lives
by leftist economic ideals every single day,
from the co-op to the union to farm aid to the way that we
trade canned veggies for eggs. It's how we get by.
But many people in rural areas are socially conservative,
and although you didn't say anything in this message to suggest that,
you did constantly say his, his family, the man's doing the work,
and there's a nostalgia looking back, looking back at a better time.
You're probably socially conservative, and that's one of the reasons
people find it hard to lean left. When, if they, unprompted,
with no guidance, whatever, lay out the issues and kind of indicate
where they think the solutions would be, they land on, in this case,
an incredibly far left philosophy.
This happens a lot. You know, when you hear them say stuff like,
well, the workers should own the means of production,
the worker should own the wrench and the lift, therefore they keep the value.
I'm certain that this person has probably never would have considered
themselves as a, adherent to this philosophy. Why?
Because most Americans don't have a good understanding of these various
philosophies, especially when it comes to economics, because, you know,
I'll just go ahead and take the mean message from Maxie.
Economic theory is boring. It's boring. It's homework for most people.
There are some who enjoy reading it, but overwhelmingly,
most people sit down to read it and they're, I mean, they have to take notes
and they have to look up terms. It's homework, but it's worth the effort.
Because even though this homesteader, they may never want to be associated
with that philosophy. If they read it, they'll get a basis for a philosophy of their own.
Even if you don't want to spend the time and read all of the texts that are out there
about the various philosophies and theories, hit the Wikipedia article
for each one and just read through it. It doesn't take much.
It's a base understanding. There are people, as soon as I got to,
doesn't really care about the state and moneyless, they already knew where this was going.
Because, I mean, these are key pieces of that philosophy.
And it's all contained in this message that wasn't intended to lean that way.
The whole purpose of the message is that progressive ideas, left ideas, aren't any good.
Here's the problems as I see them and kind of indicate the solutions.
And it's not just a little left, it's far left.
This happens a lot, a lot, especially among rural people.
Every day. This is how we get by. It is how we survive.
If you're a homesteader, oh, absolutely. Absolutely.
Because the only way you're going to make it is through the kind of cooperation that exists
in leftist philosophy. Again, it doesn't take much to get a base understanding of them.
But if you do, you're better informed.
You can kind of use the information that other people have thought of and inform your own ideas.
I mean, I don't expect this person to suddenly start screaming, workers of the world unite.
But if they dug into this, what I actually think would happen is that they would,
they may even end up further left than they already are.
But it's a philosophy. Don't let the way words get used in common conversation dictate whether or not
you're going to look at the philosophy behind that word.
Because, you know, we associate that with the Soviet Union.
And because of the security measures they put in place and the way many countries behaved,
we associate that term with tyranny, with totalitarianism.
Part of that is grounded in reality and part of it's Cold War propaganda.
But understand that those countries, they never got to where they wanted to go.
That's why you often hear people say, well, we've actually never had a country that really got to this.
They never got to that point. They were trying to get there. That's true.
That's true. They never got to where they wanted to go.
Now, you can chalk that up to human nature. You can chalk it up to corruption.
You can chalk it up to the Cold War itself.
But at the end of the day, they never did really get there.
So we don't know if it would function. We don't know if it's against human nature.
I'm going to suggest at least on a local level, it's not.
Because it's how we survive out here.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}