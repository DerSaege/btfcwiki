---
title: Let's talk about the California recall and what it means for Trump....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1mTODFOCOZo) |
| Published | 2021/09/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Providing insights on the California recall election results and the storyline accompanying them in reporting.
- Newsom, a Democrat, won the recall election with 65% of the vote.
- The storyline suggests it as a rebuke of Trumpism and evidence of its decline, which Beau questions.
- Only 34.8% (Washington Post) and 35% (FiveThirtyEight) voted in favor of the recall, indicating a small percentage.
- Beau doesn't agree that a Democrat winning in California is surprising or indicative of a rebuke of Trumpism.
- Trump had received 34.3% of the vote in 2020, suggesting that Trumpism gained ground in California.
- Beau warns against underestimating ideologies fueled by cults of personality throughout history.
- He sees the election as holding the line against Trumpism at best, with a possibility of it gaining ground.
- The percentage of votes may change, but it seems like nothing significant has changed since 2020.
- Beau questions the widely reported storyline portraying the election as a victory against Trumpism.
- He doesn't believe the outcome in California should be a significant barometer for the national political discourse.
- Beau doesn't see a reason to celebrate as the numbers haven't significantly changed compared to 2020.

### Quotes

- "The storyline that's going along with this is that this is a rebuke of Trumpism and it shows that Trumpism is on the decline. I don't know about that."
- "I don't see the decline of Trumpism because of this election. Best case scenario, we're holding the line. Worst case, Trumpism is gaining a little bit of ground."
- "I certainly don't see a reason to celebrate."

### Oneliner

Beau questions the narrative of the California recall election being a rebuke of Trumpism and warns against underestimating its influence.

### Audience

Political analysts

### On-the-ground actions from transcript

- Analyze the election results and narratives critically (suggested)
- Stay informed about political developments beyond surface-level narratives (suggested)

### Whats missing in summary

Analysis on the potential long-term implications of underestimating ideologies fueled by cults of personality throughout history.

### Tags

#California #RecallElection #Newsom #Trumpism #PoliticalAnalysis


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about the California recall election
and the results
and the storyline that's going along with those results in a lot of the
reporting
and how that is not the story I see.
To catch you up in case you don't know, the governor of California, a Democrat
named Newsom,
was facing a recall election.
At time of filming, the election has been called
and Newsom won. He's staying in office.
And he won big,
sixty-five percent.
I mean, that's sizable.
There's no debate about that.
However, the storyline that's going along with this is that this is a
rebuke of Trumpism
and it shows that
Trumpism is on the decline.
It's not a win.
Trumpism isn't gone.
But this is evidence
that
it's not really gaining ground.
Yeah, I don't know about that.
I don't know about that.
The
Washington Post at time of filming
says that 34.8 percent of people voted in favor of the recall.
Again, that's a small number.
And the election monitoring site FiveThirtyEight,
they have it at a cool 35 percent
in favor of the recall.
So there's no doubt
that Newsom won.
But, okay, a Democrat won in California.
Stop the presses. That's not actually a surprise
at all, really.
So I don't see this story saying
that
this is a rebuke of Trumpism.
That Trumpism lost simply because the Republican candidate was endorsed by
Trump.
Because the reality is
in 2020,
Trump got 34.3 percent of the vote.
If current percentages hold,
Trumpism gained ground
in California.
I don't see the story
that is being reported. I don't see the
the reason that, you know,
champagne bottles are being popped.
Historically speaking,
it's
ill-advised
to underestimate a cult of personality
that has
spawned an ideology.
Throughout history, underestimating
those ideologies
has been a bad move.
I don't see
the decline of Trumpism because of this election.
Best case scenario,
we're holding the line.
Worst case,
Trumpism is gaining a little bit of ground.
Now there's still more votes to count
and that percentage could change a little bit.
But it really does appear as though nothing has changed since 2020.
And while sure,
the Democrats won this,
and in that sense, yes, Trumpism didn't win,
but those numbers really didn't change.
I do not see the storyline
that is kind of being widely reported.
A Trump-backed candidate
lost in California.
Okay.
I don't know that that's a real barometer
for the overall discussion of this.
I don't know that we should be paying
that close of attention to it in the sense that
even if it had gone the other way
and Newsom
picked up a few points
over 2020.
It's California. I don't know that
that's really that important.
But given the fact that the numbers didn't change,
I certainly don't see a reason to celebrate.
Anyway,
it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}