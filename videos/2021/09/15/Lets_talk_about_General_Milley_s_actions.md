---
title: Let's talk about General Milley's actions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=b78LChZNiOw) |
| Published | 2021/09/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- General Milley took actions post-January 6th to prevent the former president from retaining power unconstitutionally.
- People are calling Milley a traitor and suggesting charging him with treason for his actions.
- Milley, who could have retired comfortably, continued his service for an additional 20 years.
- Accusations of treason against Milley overlook the narrow constitutional definition of the crime.
- Milley's actions were in line with his oath to support and defend the Constitution against all enemies.
- His communication with his Chinese counterpart was to provide assurances due to concerns about the US arsenal under a President talking election fraud.
- Milley's actions were not a surprise; he had communicated his intentions in a memo on January 12th.
- Punishing or asking for Milley's resignation is a way to cover up failures and lack of duty by others.
- The focus on pinning blame on Milley after 40 years of service seems misplaced.

### Quotes

- "Milley did his job."
- "He didn't have a right to do the things that he did. He had a duty to."
- "They want to point to this conversation he had with his Chinese counterpart. Yeah, you know, when the President of the United States starts ranting about election fraud and saying that, you know, he should kind of retain power, I woul imagine that other countries get really nervous about the status of the most powerful arsenal in all of human history."

### Oneliner

General Milley took necessary actions post-January 6th to protect the Constitution, despite facing accusations of treason for preventing unconstitutional power retention by the former president.

### Audience

Politically aware citizens

### On-the-ground actions from transcript

- Support leaders who prioritize upholding the Constitution (exemplified)
- Defend against misinformation and hold accountable those who neglect their duties (exemplified)

### Whats missing in summary

The full transcript provides a deeper understanding of the context and reasoning behind General Milley's actions post-January 6th.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today, we're gonna talk about General Milley
and his actions after the 6th.
Some excerpts from an upcoming book came out,
and it has a lot of people questioning the actions
that Milley took in an effort to mitigate the risk
mitigate the risk of the former president retaining control of the United States
through unconstitutional means.
Milley took steps to prevent Trump from staying in power via a self-coup.
That's what he did.
And now because of this, people are calling him a traitor, saying he needs
be charged with treason. Special Forces, Ranger Tab, Three Legions of Merit, Four Bronze Stars,
been in a really long time and you know when I say that I should probably clarify, you know,
normally you retire after 20 years. Milley could have retired a while ago and then got a new
identity and re-enlisted and retired again. He joined in 1980. He has been
eligible for retirement, a nice officer's retirement. Go home and fly fish for two
decades, get paid the rest of his life, never have to work again. He's worked an
additional 20 years. And now people who do not know what the word treason means
are accusing him of it. And I say that because I hear him say they should
charge him with treason. I know those are the words that came out of their mouth
but what I'm actually hearing deep down is I've never read the Constitution.
Treason is a crime that is defined in the US Constitution and unless I miss
General Milley levying war against the United States or adhering or giving aid
to a wartime enemy, I don't know that how treason is part of this conversation.
See, the founders, they made that definition very narrow because they didn't want
a bunch of uninformed, ignorant people to believe that treason just means doing
something that the President doesn't like.
The reality is Milley did his job.
He did his job.
He didn't have a right to do the things that he did.
He had a duty to.
We made him swear that he would do this exact thing.
He didn't swear an oath to the President.
He swore an oath to support and defend the Constitution against all enemies, foreign
and domestic.
He was oath bound to do exactly what he did, to mitigate the risk of the Constitution being
undermined, of the United States turning into a two-bit dictatorship.
That's his job.
They want to point to this conversation he had with his Chinese counterpart.
Yeah, you know, when the President of the United States starts ranting about election
fraud and saying that, you know, he should kind of retain power, I would imagine that
other countries get really nervous about the status of the most powerful arsenal in all
of human history.
They would probably want assurances, and Milley, whose job is to protect against all enemies
foreign, would probably want to provide those assurances.
Because if they're actually worried about it, well they might strike first.
Milley did his job.
He did his job.
And they're acting like they're surprised by this behavior.
Why are we just finding out now that this happened?
This isn't a surprise.
He put out a memo on January 12th.
I read it on the channel on January 13th.
I decoded it.
I mean it was, the memo was written in general speak, but I translated it.
He said he was doing this.
He said that nothing was going to interfere with the constitutional process.
He was doing his job.
You can punish him.
You can ask for his resignation, but we all know it's a cover-up for your own failures,
for those in the Senate who didn't do their duty.
And now you want to pin it on him after 40 years of service.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}