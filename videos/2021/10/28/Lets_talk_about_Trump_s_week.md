---
title: Let's talk about Trump's week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yXi6dkJJq_Y) |
| Published | 2021/10/28|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Trump had a rough week with his endorsements and involvement in Brazilian politics.
- His attempts to oust Republicans who supported his impeachment are backfiring.
- Despite the initial success of his social media company announcement, stocks have plummeted.
- The founder of Cowboys for Trump and a Capitol riot participant have turned against him.
- Trump is resorting to writing letters to the editor and attacking Bill Barr.
- Media outlets allowing Trump to spread election rigging claims are damaging the Republican party.
- Republicans could be more successful without Trump's constant presence.
- Trump's future as a Republican leader is in jeopardy as key members turn against him.

### Quotes

- "What he honestly needs to do is go away."
- "Trump's status as the Republican is in serious question."
- "Realistically, if Republicans in the Senate were doing what they're doing now and just being normal obstructionist Republicans, and they didn't have Trump in the background constantly doing what Trump is doing, Biden's poll numbers would be way lower."
- "One of those involved in the sixth, Thomas Sibyk, wrote a letter to the judge saying that Trump was, quote, not a leader and should be ostracized from any political future."
- "It seems the former president is definitely having one."

### Oneliner

Trump had a rough week with failed endorsements, stock market woes, and Republican backlash, putting his future as a GOP leader in serious doubt.

### Audience

Republicans, Political Analysts

### On-the-ground actions from transcript

- Challenge Trump's damaging narratives by engaging in constructive political discourse (implied).
- Support Republican members distancing themselves from Trump's influence (implied).
- Monitor media coverage and challenge misinformation that could harm the political landscape (implied).

### Whats missing in summary

Insights on the potential impact of Trump's actions on the upcoming elections.

### Tags

#Trump #RepublicanParty #PoliticalAnalysis #Endorsements #StockMarket #GOPLeadership


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're going to talk about Trump's week.
I think everybody's had a bad week every now and then.
It seems the former president is definitely having one.
I'm sure he is tired of winning at this point.
We're going to recap some of it and then get to the biggest
issue that he now faces.
So he expressed his support, kind of endorsed Brazilian President Bolsonaro down there.
And then the Senate in Brazil recommended him for criminal charges for his mishandling
of the public health issue.
And by some reporting, it is suggesting that the charges might include crimes against humanity.
It is generally seen as not a good use of political capital to endorse politicians in
other countries facing criminal charges.
Trump likes to hand out these endorsements because it makes him feel like he's the kingmaker.
He has run into an issue with that domestically as well.
He decided that he was going to try to oust every Republican who voted in favor of his
impeachment and he has backed a bunch of people to try to primary him, get that incumbent
out of there.
In every single race where Trump is trying to get an incumbent out because they supported
impeachment, the incumbent is out raising the challenger, the person that he
endorses. The incumbent is bringing in more money by decent margins. It
definitely challenges his status as kingmaker in the Republican Party. Makes
it seem as though maybe that endorsement isn't worth the heat because we've talked
about it at this point a Republican the logic is anyway that a Republican can't
win a primary without his endorsement but that endorsement comes at a cost in
a general election if his endorsement can't even swing a primary the former
president is nothing but a liability to the Republican Party aside from that he
He started off having a pretty good week with the announcement of his social media company.
Stocks shot through the roof, got up to $175 a share.
Even the space laser lady invested up to $50,000 in it.
And the stocks fell now.
They're about $65 a share.
And some firms are already reportedly shorting it, betting it is going to fail.
Now regardless of that, Trump stands to make some money on that.
No matter what happens really, he'll probably come out ahead.
However, if his supporters are the ones who are investing and losing money, they might
not be so supportive of him.
Speaking of people not being supportive of him, we already talked about how the founder
of Cowboys for Trump turned on him.
One of those involved in the sixth, Thomas Sibyk, wrote a letter to the judge saying
that Trump was, quote, not a leader and should be ostracized from any political future.
What he honestly needs to do is go away.
He said that Trump was ultimately responsible for what happened that day at the Capitol.
And now we have the most embarrassing part.
The former president has been reduced to yelling at clouds and writing letters to the editor,
which the Wall Street Journal published, even though let's just say that Trump's statements
were less than accurate.
In it he suggested that Bill Barr was somehow involved in rigging the election against him.
That may be the funniest thing I've heard all week.
Now it does appear that a lot of media outlets are willing to kind of co-sign the former
president's statements, or at least allow him to get them out.
This is probably one of the most damaging things for the Republican party because Trump
is just obsessed with re-litigating 2020.
That doesn't work.
That doesn't appeal to anyone.
Realistically, if Republicans in the Senate were doing what they're doing now and just
being normal obstructionist Republicans, and they didn't have Trump in the
background constantly doing what Trump is doing, Biden's poll numbers would be
way lower.
They would be gaining significant ground from the Republican standpoint, well,
what he honestly needs to do is go away.
But at this point, they're not calling for that.
just yet.
I believe there's probably still some hope that Trump will re-energize a base.
But from what we've seen from key members, that seems unlikely.
They seem to be turning on him faster than ever before.
Trump's status as the Republican is in serious question.
It may not make it to the 2022 elections, much less make it through it.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}