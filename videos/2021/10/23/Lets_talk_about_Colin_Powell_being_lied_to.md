---
title: Let's talk about Colin Powell being lied to....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=dHhSS6tSskg) |
| Published | 2021/10/23|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responding to questions about Colin Powell, a younger person realizes their father's military background after Powell's passing.
- The theory debated involves Powell being misled about the Iraq invasion, going against his own doctrine.
- The Powell Doctrine, developed by Powell in 1990-91, outlines criteria for military action.
- The narrative suggests the CIA, particularly Tenet, misled Powell, leading to his UN speech supporting the invasion.
- Evidence includes doubts expressed privately by Powell and contradictions in the intelligence presented.
- The State Department's Intel Group raised red flags before Powell's UN speech, questioning the accuracy of information.
- Hussein Kamel's statement that Iraq no longer had prohibited weapons is cited as proof that Powell knew he was lying.
- The theory speculates on Powell being manipulated or knowingly misleading to garner support for the war.
- Despite respected in military circles, Powell's UN speech contained inaccuracies, indicating some level of deception.
- Beau concludes that current evidence points towards Powell lying to fabricate consent for the Iraq war.

### Quotes

- "We have to go off of what we know. What we know for sure is that he lied."
- "The evidence that we have suggests strongly that he lied in some way to help manufacture consent for that war."
- "But what the evidence really shows right now is that he lied."

### Oneliner

Beau examines the theory that Colin Powell was misled about the Iraq invasion, concluding that evidence strongly suggests he lied to manufacture consent for the war.

### Audience

Military members, truth-seekers

### On-the-ground actions from transcript

- Research and fact-check historical events and political decisions (suggested)
- Engage in open dialogues with family members to understand differing perspectives (exemplified)

### Whats missing in summary

Deeper insights into Powell's motivations and potential manipulation in the lead-up to the Iraq invasion.

### Tags

#ColinPowell #IraqWar #Military #Deception #Invasion


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about Colin Powell.
Over the last few days, we've had a bunch of questions
come in about him.
One I found particularly interesting.
And it came from a younger person who, until Powell passed,
had no idea their father was in the military.
And when that happened, they found out
that their father believed that Powell had been lied to
and just carried forth those lies.
And therefore, he really didn't play much of a role
in what happened.
And by what happened, I'm talking
about the invasion of Iraq.
Now, I know for most people watching this channel,
you've probably never heard this theory.
It is widely believed by a lot of people in the military
and adjacent to the military.
I would say that probably 60%, 70% of my friends
believe this theory.
So what we're going to do is we're
going to treat it like any other theory that we examine.
We're going to go through, is this even possible?
We'll go through the evidence.
We'll go through counter evidence.
We'll go through the narratives and just
see where we land on it.
OK, so the normal stuff that we run through with wild theories,
the answer is yeah.
Yeah, it could totally be true.
As far as is it possible and plausible and feasible
and all of that stuff, yeah, it is.
And we'll go through the narrative.
So the storyline that develops from people who believe this,
it basically boils down to the idea
that the CIA in particular lied to him
and that he presented those lives believing they were true.
The first thing you have to know about this
is a doctrine that exists.
And it's a few questions about whether or not
the US should engage in a military action.
The first question is, is it vital to national security?
The next is, does it have clear objectives?
The next is, have the risks and costs
been thoroughly evaluated?
Have all nonviolent methods of policy been exhausted?
Is there a real exit strategy, a way to get out?
Have the consequences of the action on the foreign policy
scene, have they been examined?
Is it truly supported by the American people?
And is there real support internationally?
Unless the answers to all of these questions are yes,
you don't do it.
Now, that's the long format.
The short format of this is assuming that all the questions
are yes, and the plan is the United States shows up,
uses air power to degrade the opposition's fighting
capability, moves in on the ground,
finds as many opposition troops as they can,
pokes holes in them, and then leaves.
This is called the Powell Doctrine
because he developed it.
It's his doctrine.
And the situation in 2003 in Iraq, yeah, it didn't qualify.
It didn't meet this.
So there is the assumption that somebody
had to convince Powell to go against his own doctrine that
had been around for a while, since 91, I think.
His namesake, something he truly believed in.
Somebody had to convince him to go against this.
And that somebody in the narrative
is Tenet, DCI, Director of Central Intelligence.
Tenet.
They met for five days, five days.
And Tenet answered all his questions.
Tenet briefed him before he went to the UN.
Later, when he was asked about it,
once a lot of the information turned out to be false,
he got real defensive.
And he was like, you know, George didn't sit there
and lie to me for five days.
The narrative says that nobody likes admitting
that they got duped.
So he doesn't believe that he was lied to.
And then there are his privately expressed doubts
when it comes to the information, which is funny
because this is also used as evidence
that he knew he was lying.
At one point, I think the most famous quote
is something to the effect of, imagine how we'll all
feel if we put 500,000 troops in Iraq,
march from one end to the other, and don't find anything.
Those privately expressed doubts lend to the idea
that he wasn't certain.
He wasn't certain, so he wasn't lying.
He was just going with what was the best
available information, but he had doubts.
He wouldn't be saying this if he knew he was lying, right?
OK.
So that's the storyline, and that's why people believe it.
That's the narrative that exists.
Now, on the other side, to poke holes in this,
that infamous speech that he gave at the United Nations,
State Department's Intel Group, well, they wrote a memo
prior to it, and they poked holes in it.
And they were like, this is wrong, this is wrong,
this is wrong, this isn't true, this is doubted, you know?
And they went through and they said,
this is doubted, you know, and they went through and did that.
That's used as proof that he knew he was lying.
His private doubts are used as proof
that he knew he was lying, because he
seemed very certain while he was giving that speech.
And then there's Hussein Kamel, who went to Jordan,
and he flat out said, and this is
a person who would know, he flat out
said that Iraq didn't have that program anymore,
that they had destroyed all their stuff.
So those are the key pieces of evidence
that are used to demonstrate for fact
that Powell knew he was lying.
The other side to that is that a lot of the State Department's
Intel Group, a lot of their information
is based on human intelligence, which is not always accurate.
So was Intel he was going off of.
So he had to weigh it.
So he just chose wrong, according to the narrative.
And then when it comes to Hussein, he was a defector.
He defected, came over, told the US, and he went to Jordan,
told the CIA and MI6 a bunch of stuff.
But then he went back to Iraq.
So for a lot of people, they discount it,
because something that is very common
is to have somebody defect and feed
the opposition a bunch of bad information and then go home.
So they're like, well, he had to discount that,
because, well, they didn't know.
That's kind of the back and forth with it.
Now, at the end of this, you have a few options.
You have one.
Powell was lied to.
All right.
And he just carried it forward, referred to it
as a blot on his record, but also never
publicized who lied to him.
Now, some could say he was duty bound not to do that,
especially those in military circles.
They might say that.
Now, another option is that he was just tired of Iraq,
because to younger people, Iraq started in 2003.
To older people, it started in 1991.
People talk about Desert Storm and how quickly it was over.
The reality was it never ended, because there were no fly zones
that ran from 91 until the US was back in there.
So there may have been an element of Powell just being
like, greater good.
We have to get rid of this guy.
He's been a thorn in the United States side all of this time.
So he embellished, lied when he was at the UN.
Another option is that he knowingly manufactured consent
for the war to get items that were
to get items 7 and 8 of the Powell Doctrine,
support at home and broad international support.
Now, why does this theory exist and why has it existed so long?
This started just as soon as it happened.
Powell was not just respected, he was liked among multiple generations
of people in the military.
They want to believe it.
They want to believe that he didn't lie.
I want to believe he didn't lie.
But when we're faced with this, right, this is the narrative,
here's the evidence, the counter evidence going back and forth,
we have to go off of what we know.
What we know for sure is that he lied.
He said stuff that was not true during that speech.
That's what we know.
Some of it, the human intelligence stuff, sure, you can write that off.
Some of it wasn't like that.
The stuff about the tubes wasn't like that.
That was hard fact.
And it got misrepresented.
Some of the other stuff that's hard fact that gets viewed as him lying,
there are alternate explanations for that.
But those tubes, not so much.
You know, there was one section where he's going through a translation
of something that they intercepted.
And there are additions about the fact that he's lying.
And there are additions that occur throughout it,
things that he added that weren't really said.
If you're reading a translation like that, sometimes in brackets,
there is information that came from another report.
It's there for context.
So it's not in the actual statement,
but it's included to let the person know what they're talking about.
It's possible.
But those tubes, him saying that they were manufactured,
you know, well beyond our conventional weapons and all that stuff,
there is no explanation for that.
None.
I'm open to the idea that he was manipulated into supporting it.
I wouldn't be surprised if years from now,
diaries or letters or emails surface that suggest that's true.
I really wouldn't.
However, they don't exist right now.
The evidence that we have suggests strongly that he lied
to in some way help manufacture consent for that war.
That's what the evidence really shows.
Sure, this other thing, it's not some wild theory that is just patently impossible,
but it's not what the evidence really shows right now.
Again, I would be open to it.
I wouldn't be surprised if he was manipulated.
And then once he was manipulated, he felt he had to make the best case possible.
But we don't have the evidence to say that.
Doesn't exist.
So I hope that helps you talk to your dad a little bit
and can kind of understand where he's coming from and perhaps bridge that gap.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}