# All videos from October, 2021
Note: this page is automatically generated; currently, edits may be overwritten. Contact folks on discord if this is a problem. Last generated 2024-02-25 05:12:14
<details>
<summary>
2021-10-31: Let's talk about a special Halloween message.... (<a href="https://youtube.com/watch?v=3raGK1d2ro0">watch</a> || <a href="/videos/2021/10/31/Lets_talk_about_a_special_Halloween_message">transcript &amp; editable summary</a>)

Beau celebrates Halloween as a day of community trust and voluntary unity, envisioning a society where collective participation leads to a better world year-round.

</summary>

"You don't need a law to tell you to be a good person, right?"
"If we could extend the faith we have in each other that we show on Halloween, if we could show that year round, everybody participate in the way that they can, things..."
"Everybody contributes what they can, and everybody gets what they need."

### AI summary (High error rate! Edit errors on video page)

Celebrates Halloween as his favorite holiday, free from violence and coercion.
Believes in people's natural inclination to be good without needing laws.
Notes the consequences of vaccine pushback, with red counties facing higher risks.
Mentions the historical shift in COVID-19 impact from blue to red areas post-vaccine.
Stresses the power of education and collective action to combat challenges.
Describes Halloween as a day of community interaction and trust.
Talks about different ways communities celebrate Halloween, like trunk-or-treating.
Emphasizes the voluntary nature of Halloween traditions without force or violence.
Encourages extending the spirit of unity and participation beyond Halloween.
Envisions a society where everyone contributes what they can for the greater good.

Actions:

for community members,
Participate in community events like trunk-or-treating to foster trust and unity (exemplified)
Contribute what you can to communal activities to support others (implied)
Embrace the spirit of Halloween by engaging in voluntary acts of kindness and sharing (exemplified)
</details>
<details>
<summary>
2021-10-30: Let's talk about roses, history, and a reality check.... (<a href="https://youtube.com/watch?v=WrFL-GnYEKk">watch</a> || <a href="/videos/2021/10/30/Lets_talk_about_roses_history_and_a_reality_check">transcript &amp; editable summary</a>)

Beau explains the historical context of the White Rose group to criticize the misuse of their imagery by anti-vaccine movements, urging a more informed and respectful approach to public health debates.

</summary>

"Claiming the mantle of the White Rose does not make you look like a freedom fighter. It makes you look like you've never read a history book."
"It's just a thought."

### AI summary (High error rate! Edit errors on video page)

Talks about the White Rose group, a historic group from WWII Germany known for opposing the regime.
Admires the structure of White Rose arguments and how they used common imagery to make their points accessible to different groups.
Mentions that White Rose members were all vaccinated, as mandatory vaccination was the norm in Germany at the time.
Notes that White Rose never mentioned vaccines in their writings, focusing on nonviolent passive resistance instead.
Criticizes the misuse of White Rose imagery by anti-vaccine groups, calling it historically illiterate and potentially propaganda.
Points out that mandatory vaccinations were suspended during the Nazi regime, suggesting a disregard for public health.
Urges people to drop WWII references from vaccine debates, as it can be offensive and shows historical ignorance.

Actions:

for history enthusiasts, public health advocates,
Educate yourself on the history of groups like the White Rose and their true messages (suggested)
Use accurate historical references and avoid appropriating symbols for unrelated causes (implied)
</details>
<details>
<summary>
2021-10-30: Let's talk about an update on Biden's foreign policy and over there.... (<a href="https://youtube.com/watch?v=e-3wPrNQ0Uc">watch</a> || <a href="/videos/2021/10/30/Lets_talk_about_an_update_on_Biden_s_foreign_policy_and_over_there">transcript &amp; editable summary</a>)

Beau revisits Iran's regional role and Afghanistan's stability post-U.S. withdrawal, downplaying nationalism and external pressure while noting potential positive outcomes in the Middle East.

</summary>

"It's not going to be anything near what you're hoping for."
"Nationalism is politics for basic people."
"This wasn't a success from the Biden administration."
"It's more likely to work if they're not being pressured into it."
"This is more of a sign of things that might come rather than a sign of things that will."

### AI summary (High error rate! Edit errors on video page)

Beau revisits a topic in the Middle East that hasn't been checked on in a while.
He mentions the importance of Iran becoming a regional player for Biden's foreign policy to succeed.
Beau talks about the necessity of a token security force in Afghanistan post the U.S. withdrawal.
Representatives from surrounding countries met in Iran to address the situation in Afghanistan.
The meeting aimed to establish a more inclusive government in Afghanistan.
Beau notes that the outcome of these efforts may not meet Western human rights standards.
Iran is seen working with China and Russia to stabilize Afghanistan, which may upset some people.
Nationalism in foreign policy is dismissed as irrelevant by Beau.
Beau doesn't attribute this development to the Biden administration, comparing it to finding extra chips after losing at poker.
He views the organic nature of these developments positively, suggesting they are more likely to succeed without external pressure.

Actions:

for foreign policy observers,
Monitor developments in the Middle East and Afghanistan closely (implied).
</details>
<details>
<summary>
2021-10-29: Let's talk about water, taxes, and #TeamSeas.... (<a href="https://youtube.com/watch?v=z58RmpkqTmU">watch</a> || <a href="/videos/2021/10/29/Lets_talk_about_water_taxes_and_TeamSeas">transcript &amp; editable summary</a>)

Americans want their money spent on environmental conservation, supporting Team Seas' initiative to clean rivers, beaches, and oceans.

</summary>

"Americans may actually be enthusiastic about funding initiatives for clean rivers, beaches, and oceans."
"We can send a pretty clear message to DC that this is something that we want our money spent on."
"When they waste our tax dollars on something else, we want it so much we'll pick up the slack."

### AI summary (High error rate! Edit errors on video page)

Taxes are paid without choice, but politicians dictate where the money should not be spent based on contributors' interests.
Politicians may not accurately represent the desires of the country when it comes to environmental spending.
Americans may actually be enthusiastic about funding initiatives for clean rivers, beaches, and oceans.
Team Seas, led by Mr. Beast on YouTube, aims to raise $30 million to remove trash from rivers, beaches, and oceans.
Each dollar donated will remove one pound of trash to prevent it from reaching the ocean.
The funds raised will be split between the Ocean Conservancy and Project Ocean Cleanup with the UN.
To donate to Team Seas' cause, visit teamseas.org.
The website for Team Seas was not published at the time of filming but should go live when the videos are released.
Joining this initiative can send a message to Washington that Americans want their money spent on environmental conservation.
Supporting Team Seas shows a commitment beyond tax dollars, indicating public interest in environmental causes.

Actions:

for youtube viewers,
Donate to Team Seas at teamseas.org (suggested)
</details>
<details>
<summary>
2021-10-29: Let's talk about a Sputnik moment and it not being one.... (<a href="https://youtube.com/watch?v=loXcr5cZaZU">watch</a> || <a href="/videos/2021/10/29/Lets_talk_about_a_Sputnik_moment_and_it_not_being_one">transcript &amp; editable summary</a>)

General Milley's testimony on Chinese developments and hypersonic missiles may trigger an arms race, demanding strategic shifts and significant spending, while Beau calls for a reevaluation of national priorities.

</summary>

"We don't need another arms race. We need to inoculate ourselves."
"If the United States is crumbling from within, what good is defense spending?"
"Maybe we need to focus on things that are more productive and less destructive."

### AI summary (High error rate! Edit errors on video page)

General Milley testified about Chinese developments and hypersonic missiles, potentially signaling a new arms race.
Milley mentioned the Department of Defense needing to make strategic shifts, reprioritize, and spend money in response.
Dr. Cameron Tricy, an expert on nuclear arms control, will provide insights in an upcoming interview.
The new missiles aren't significantly faster, stealthier, or undetectable, challenging the narrative of a game-changing technology.
Despite lacking funding for infrastructure, the defense budget will likely support these new weapons.
Beau warns against another arms race and advocates for preparing to face the evolving threats.
He criticizes the defense industry's potential exaggeration of new threats and the swift funding approval they receive.
Beau questions the focus on defense spending while domestic issues remain unaddressed.
The shift towards near-peer adversaries resembles the dynamics of the Cold War, with propaganda and arms race implications.
Beau suggests a reevaluation of national priorities towards more productive endeavors.

Actions:

for policy makers, activists,
Pay attention to the potential exaggeration of threats by the defense industry (implied)
Advocate for a reevaluation of national priorities towards less destructive endeavors (implied)
</details>
<details>
<summary>
2021-10-28: Let's talk about Trump's week.... (<a href="https://youtube.com/watch?v=yXi6dkJJq_Y">watch</a> || <a href="/videos/2021/10/28/Lets_talk_about_Trump_s_week">transcript &amp; editable summary</a>)

Trump had a rough week with failed endorsements, stock market woes, and Republican backlash, putting his future as a GOP leader in serious doubt.

</summary>

"What he honestly needs to do is go away."
"Trump's status as the Republican is in serious question."
"Realistically, if Republicans in the Senate were doing what they're doing now and just being normal obstructionist Republicans, and they didn't have Trump in the background constantly doing what Trump is doing, Biden's poll numbers would be way lower."
"One of those involved in the sixth, Thomas Sibyk, wrote a letter to the judge saying that Trump was, quote, not a leader and should be ostracized from any political future."
"It seems the former president is definitely having one."

### AI summary (High error rate! Edit errors on video page)

Trump had a rough week with his endorsements and involvement in Brazilian politics.
His attempts to oust Republicans who supported his impeachment are backfiring.
Despite the initial success of his social media company announcement, stocks have plummeted.
The founder of Cowboys for Trump and a Capitol riot participant have turned against him.
Trump is resorting to writing letters to the editor and attacking Bill Barr.
Media outlets allowing Trump to spread election rigging claims are damaging the Republican party.
Republicans could be more successful without Trump's constant presence.
Trump's future as a Republican leader is in jeopardy as key members turn against him.

Actions:

for republicans, political analysts,
Challenge Trump's damaging narratives by engaging in constructive political discourse (implied).
Support Republican members distancing themselves from Trump's influence (implied).
Monitor media coverage and challenge misinformation that could harm the political landscape (implied).
</details>
<details>
<summary>
2021-10-27: Let's talk about justice delayed in Alabama.... (<a href="https://youtube.com/watch?v=FSECaOWeldY">watch</a> || <a href="/videos/2021/10/27/Lets_talk_about_justice_delayed_in_Alabama">transcript &amp; editable summary</a>)

Beau talks about buses, Rosa Parks, and the overlooked Claudette Colvin, questioning why Alabama hasn't pardoned her despite her historical significance and the need for atonement.

</summary>

"It seems like Alabama owes her."
"When somebody is penalized for doing the right thing, at some point, there should be some kind of atonement."
"It seems like there should be that moment where the state says, 'Yeah, we were wrong. We're sorry. We beg your pardon.'"

### AI summary (High error rate! Edit errors on video page)

Talking about buses and justice in Montgomery, Alabama during a specific historical event.
Recalling the scenario where buses were divided and individuals were required to move if an area got too crowded.
Describing the incident where a bus driver asked someone to move, leading to police involvement.
Mentioning Claudette Colvin, a teenager who also resisted giving up her seat before Rosa Parks, but faced media scrutiny due to being unmarried and pregnant.
Updating on Claudette Colvin's current efforts to have her record expunged at the age of 80.
Questioning why Claudette Colvin hasn't been pardoned yet despite the historical significance of her actions.
Expressing support for Claudette Colvin's endeavors while also advocating for a pardon to acknowledge the wrong that was done.
Pointing out the lack of termination of Claudette Colvin's probation from the state of Alabama officially.
Emphasizing the importance of recognizing and atoning for the past events that occurred during the civil rights movement in Montgomery.
Advocating for a more significant gesture beyond record expungement to address the injustices faced by Claudette Colvin.

Actions:

for alabama residents, civil rights advocates.,
Support Claudette Colvin's petition to get her record expunged (implied).
Advocate for a pardon for Claudette Colvin in Alabama (implied).
</details>
<details>
<summary>
2021-10-27: Let's talk about a leading cause, #SaferStorage, and T4CIP.... (<a href="https://youtube.com/watch?v=hBsuqOnb97M">watch</a> || <a href="/videos/2021/10/27/Lets_talk_about_a_leading_cause_SaferStorage_and_T4CIP">transcript &amp; editable summary</a>)

Today is a critical day for child injury prevention advocacy, focusing on the dangers of unsecured firearms and the need for responsible storage.

</summary>

"Hidden doesn't mean safe. Hidden doesn't mean secure. Hidden means, wow, look what I found."
"Lock up your firearms. 3,500 a year lost to guns."
"Either you're going to do it because it's the right thing or it's going to become a law."

### AI summary (High error rate! Edit errors on video page)

Today is a big day for a group called trainees for child injury prevention, aiming to draw attention to the leading cause of childhood injury related deaths.
Firearms, not cars, account for the leading cause of childhood injury-related deaths in the USA.
4.6 million kids live in homes where weapons are stored loaded and unsecured, increasing the risk of unintentional shootings.
Safer storage of firearms is the focus of the group's day of action to encourage responsible gun ownership.
The importance of securing firearms is stressed, as hidden weapons can be dangerous to curious children.
Beau compares securing firearms to a vaccine mandate, stating that people should do it willingly without the need for a law.
He mentions a webinar and chat session organized by the group, mainly comprising doctors, to raise awareness about firearm safety protocols.

Actions:

for parents, gun owners, community members,
Secure firearms in your home to prevent accidental shootings (exemplified)
Educate yourself and others about responsible firearm storage (exemplified)
Join online webinars and chats about child injury prevention (exemplified)
</details>
<details>
<summary>
2021-10-26: Let's talk about Levine, the first 4-star.... (<a href="https://youtube.com/watch?v=6JkGIEqdoFU">watch</a> || <a href="/videos/2021/10/26/Lets_talk_about_Levine_the_first_4-star">transcript &amp; editable summary</a>)

Beau addresses the manipulation through fabricated stories to provoke outrage, urging fact-checking or kindness over falling for manufactured anger.

</summary>

"The fact that you do shows how easily they can manipulate you with something that isn't true."
"Completely manufactured. Completely made up."
"Is it that hard just to fact-check?"
"Just be nice."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addressing information consumption and a message received about the "new four star."
Expressing issues with incorrect name usage and putting "woman" in quotation marks.
Sharing the story of Ann Dunwoody, the first woman four star back in 2008.
Pointing out Admiral Michelle Howard as the first woman four star admiral in Navy history.
Calling out Representative Jim Banks for spreading false information to manipulate anger.
Emphasizing the trend of manipulating people by fabricating stories to evoke outrage.
Questioning the ease with which people are manipulated by untrue narratives.
Encouraging fact-checking or simply being nice as alternatives to falling for manufactured outrage.

Actions:

for social media users,
Fact-check before sharing information (suggested)
</details>
<details>
<summary>
2021-10-26: Let's talk about Alec Baldwin and never letting a crisis go to waste.... (<a href="https://youtube.com/watch?v=sPPVtdWTgTc">watch</a> || <a href="/videos/2021/10/26/Lets_talk_about_Alec_Baldwin_and_never_letting_a_crisis_go_to_waste">transcript &amp; editable summary</a>)

A missed chance for leadership to reinforce gun safety following Alec Baldwin's tragic on-set incident, urging vigilance in firearm handling and education.

</summary>

"Guns don't kill people, Alec Baldwin kills people."
"This incident could have been used to reinforce gun safety, rather than mock it."
"The single most dangerous firearm in the world is the one that you are pretty sure is unloaded."
"They don't have leaders. They have what amounts to internet trolls who found their way to positions of power."
"It's your friends at stake. It's your community."

### AI summary (High error rate! Edit errors on video page)

Alec Baldwin was on set filming a movie called Rust when he was handed a prop gun with a live round, resulting in a woman's death and another person's injury.
The gun community's response involved mocking the incident, with Donald Trump Jr. selling shirts making fun of Baldwin.
Beau criticizes the lack of leadership following the tragedy and the missed chance to reinforce gun safety.
Approximately 42 people end up in the hospital daily due to unintentional gunshot wounds, with about 500 of them not surviving each year.
Beau points out that the incident could have been an educational moment for the gun community to revisit basic gun safety practices.
He suggests that this was an ideal time to remind people, especially those in the gun community, of the importance of gun safety.
There is a call for leadership during such incidents to provide guidance and education on firearm safety.
The lack of leadership in the United States, particularly on the right, is criticized as being filled with internet trolls in positions of power.
Beau stresses the importance of always treating every weapon as if it is loaded and following strict gun safety protocols.
The most dangerous firearm is the one assumed to be unloaded, making vigilance in handling firearms critical.

Actions:

for gun owners, advocates,
Educate your community on basic firearm safety practices (implied)
Remind friends and family about the importance of gun safety (implied)
</details>
<details>
<summary>
2021-10-25: Let's talk about Trump, Cowboys, Rolling Stone, and a bumpy ride.... (<a href="https://youtube.com/watch?v=1A0VD0TrbGg">watch</a> || <a href="/videos/2021/10/25/Lets_talk_about_Trump_Cowboys_Rolling_Stone_and_a_bumpy_ride">transcript &amp; editable summary</a>)

Rolling Stone exposed rally organizers' involvement in Capitol insurrection planning, while Cowboys for Trump founder turns on former president, leaving disaffected supporters feeling betrayed.

</summary>

"They lied to you. And they are going to continue to do so as long as it works."
"Your best defense against the manipulation that they used is to become educated."
"We are in for a very bumpy ride if this crew that has been radicalized ends up feeling sold out."

### AI summary (High error rate! Edit errors on video page)

Rolling Stone reported on rally organizers naming Congress members involved in planning the Capitol insurrection, catching headlines.
Organizers claim innocence, painting themselves in a positive light, saying they had no idea what was going to happen at the Capitol.
Cowboys for Trump founder Griffin turned on the former president in a speech, expressing feeling betrayed and sold out.
The crowd Griffin spoke to, many present at the Capitol on the 6th, are becoming disaffected and feeling abandoned by their leaders.
The Republican Party, especially those embracing revolutionary rhetoric, has heavily influenced this disaffected group.
The radicalized individuals are feeling betrayed and are about to be publicly disavowed by their leadership.
Those who believed in the lies and wild claims of the president are now facing the reality of being misled and betrayed.
The committee investigating the Capitol insurrection must hold the responsible parties accountable to prevent future anger and radicalization.
Beau urges disaffected individuals to put down guns, pick up books, and focus on civic engagement and community involvement.
The manipulation by leaders who preyed on anger and ignorance can be combated by educating oneself and being involved in local community issues.

Actions:

for disaffected individuals,
Put down guns, pick up books, and focus on civic engagement and community involvement (suggested)
Educate oneself and get involved in local community issues to combat manipulation (suggested)
</details>
<details>
<summary>
2021-10-24: Let's talk about Biden shifting on the filibuster.... (<a href="https://youtube.com/watch?v=7KiLdt_Bd78">watch</a> || <a href="/videos/2021/10/24/Lets_talk_about_Biden_shifting_on_the_filibuster">transcript &amp; editable summary</a>)

Beau warns Democrats to follow through on progressive legislation if they remove the filibuster, stressing the importance of taking decisive action for lasting change in the country.

</summary>

"If the Democratic Party amends or does away with the filibuster, they have to push through every piece of progressive legislation they can."
"If they take the gloves off, they have to stay off."
"Once that's gone, there's no excuse."
"And the voters will expect a lot done in a very short period of time."
"They have to be able to experience the effects of these policies at their kitchen table before the next election."

### AI summary (High error rate! Edit errors on video page)

Explains the filibuster in the Senate, requiring 60 votes to pass legislation, a rule not part of the Constitution.
Biden administration is considering changing the filibuster rule to push through legislation with just 51 votes.
Warns that if Democrats remove the filibuster, they must follow through aggressively on progressive legislation without hesitation.
Emphasizes the importance of Democrats staying committed to pushing through significant policies once the filibuster is gone.
Points out the risk that if Democrats fail to publicize the impacts of their policies post-filibuster removal, Republicans could exploit it to pass regressive laws.
Stresses that Democrats need to be prepared to face the consequences of removing the filibuster and push forward on various key issues like climate change, education, police reform, and social justice.
Argues that once the filibuster is removed, there's no turning back, and voters will expect substantial progress before the next election.
Urges for a clear message of moving the country forward and addressing critical societal issues to be conveyed post-filibuster removal.

Actions:

for politically active individuals,
Mobilize to support progressive legislation and policies post-filibuster removal (implied)
Advocate for comprehensive climate change initiatives, education reforms, police reform, and social justice measures in your community (implied)
Ensure voters are informed about the impacts of policy changes and legislation at a local level (implied)
</details>
<details>
<summary>
2021-10-23: Let's talk about Colin Powell being lied to.... (<a href="https://youtube.com/watch?v=dHhSS6tSskg">watch</a> || <a href="/videos/2021/10/23/Lets_talk_about_Colin_Powell_being_lied_to">transcript &amp; editable summary</a>)

Beau examines the theory that Colin Powell was misled about the Iraq invasion, concluding that evidence strongly suggests he lied to manufacture consent for the war.

</summary>

"We have to go off of what we know. What we know for sure is that he lied."
"The evidence that we have suggests strongly that he lied in some way to help manufacture consent for that war."
"But what the evidence really shows right now is that he lied."

### AI summary (High error rate! Edit errors on video page)

Responding to questions about Colin Powell, a younger person realizes their father's military background after Powell's passing.
The theory debated involves Powell being misled about the Iraq invasion, going against his own doctrine.
The Powell Doctrine, developed by Powell in 1990-91, outlines criteria for military action.
The narrative suggests the CIA, particularly Tenet, misled Powell, leading to his UN speech supporting the invasion.
Evidence includes doubts expressed privately by Powell and contradictions in the intelligence presented.
The State Department's Intel Group raised red flags before Powell's UN speech, questioning the accuracy of information.
Hussein Kamel's statement that Iraq no longer had prohibited weapons is cited as proof that Powell knew he was lying.
The theory speculates on Powell being manipulated or knowingly misleading to garner support for the war.
Despite respected in military circles, Powell's UN speech contained inaccuracies, indicating some level of deception.
Beau concludes that current evidence points towards Powell lying to fabricate consent for the Iraq war.

Actions:

for military members, truth-seekers,
Research and fact-check historical events and political decisions (suggested)
Engage in open dialogues with family members to understand differing perspectives (exemplified)
</details>
<details>
<summary>
2021-10-22: Let's talk about nonsense and a Florida man jobs experiment.... (<a href="https://youtube.com/watch?v=3-hGiEMEVhk">watch</a> || <a href="/videos/2021/10/22/Lets_talk_about_nonsense_and_a_Florida_man_jobs_experiment">transcript &amp; editable summary</a>)

Joey's job application experiment challenges the narrative of a labor shortage and lazy employees, revealing employer entitlement to cheap labor.

</summary>

"Employers are so entitled that they expect people to work in Florida doing construction laboring for $8.65 an hour."
"It kind of casts doubt on the narrative that there's a labor shortage, that employees are lazy."
"Employers have become so entitled to having a ready labor pool who will work for apparently $8.65 an hour."
"It's what it sounds like. It's what it seems like."
"Joey's experiment is extended. Anyway, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Joey Holtz conducted an experiment in response to his boss's rant about lazy employees quitting due to $1,200 checks.
Joey applied for 60 jobs in September, receiving nine email responses, one phone call, and only one job interview.
The job interview offered was for a construction laborer position in Florida paying $8.65 an hour.
Employers expecting people to work for such low wages is astounding, especially in a low cost of living area.
This experiment challenges the narrative of a labor shortage and lazy employees, suggesting employers may be the lazy ones.
Some employers might prioritize social media rants over actual hiring efforts.
Joey targeted companies where bosses publicly denigrated employees on social media, raising concerns for potential applicants.
Employers seem entitled to a cheap labor pool, unwilling to put in the effort to attract and retain employees.
Some businesses are not hiring because they took care of their employees during the public health crisis.
Joey's experiment sheds light on the disconnect between employers' expectations and the reality of the job market.

Actions:

for potential job seekers,
Apply for jobs that value your worth and offer fair compensation (implied).
Advocate for fair wages and respectful treatment in the workplace (implied).
Support businesses that prioritize employee well-being and fair pay (implied).
</details>
<details>
<summary>
2021-10-22: Let's talk about Texas Lt. Gov Dan Patrick paying up.... (<a href="https://youtube.com/watch?v=8nQ0UrXMpp0">watch</a> || <a href="/videos/2021/10/22/Lets_talk_about_Texas_Lt_Gov_Dan_Patrick_paying_up">transcript &amp; editable summary</a>)

Beau examines Texas Lieutenant Governor Dan Patrick's million-dollar campaign fund and its implications on election security, revealing it as a political stunt to manipulate public perception.

</summary>

"US elections, they're pretty secure."
"There is no widespread voter fraud. It's not a thing."
"It was a political stunt."
"The election integrity issues we have in the United States come from gerrymandering."
"All this time later, the only payout has gone to an isolated incident that was actually their side."

### AI summary (High error rate! Edit errors on video page)

Talking about Texas Lieutenant Governor Dan Patrick and his campaign focused on election security in the United States.
Dan Patrick was vocal about concerns regarding election security and echoed former President Trump's claims about widespread issues with elections.
Patrick took a million dollars from his campaign fund to pay for tips about election issues.
Despite the fund being around for almost a year, only one payment of $25,000 has been made.
The payment was made to a progressive Democrat poll watcher who reported a Republican for voting twice.
This incident shows that US elections are pretty secure, as the fund has not uncovered widespread issues.
Beau suggests that the fund was more of a political stunt rather than a genuine attempt to uncover voter fraud.
The real election integrity issues in the US stem from gerrymandering and voter suppression, not widespread voter fraud.
Beau concludes that the million-dollar fund was likely a manipulation tactic to create a false sense of seriousness.

Actions:

for voters, election monitors,
Monitor election processes for signs of gerrymandering and voter suppression (implied)
Support efforts to increase voter turnout and combat voter suppression (implied)
Stay informed about election integrity issues and advocate for fair practices (implied)
</details>
<details>
<summary>
2021-10-21: Let's talk about the American Bumblebee.... (<a href="https://youtube.com/watch?v=Q3e8pvrAXF0">watch</a> || <a href="/videos/2021/10/21/Lets_talk_about_the_American_Bumblebee">transcript &amp; editable summary</a>)

The American bumblebee population is dwindling, potentially leading to protection under the Endangered Species Act, as their vital role in pollination impacts our food sources and survival.

</summary>

"If the bees go, we go."
"You shouldn't need a law to tell you to be a better steward of the area around you."
"Bees pollinate 70% of all fruits, veggies, and nuts."

### AI summary (High error rate! Edit errors on video page)

The American bumblebee population is dwindling due to habitat loss, pesticides, and various other factors.
There has been a 90% reduction in the American bumblebee population in several states.
Currently, the American bumblebee is not protected by any state or federal law.
It is highly likely that the American bumblebee will become protected under the Endangered Species Act.
If the American bumblebee gets protected, killing one could cost up to $14,000.
Bees are vital for pollinating fruits, veggies, and nuts, with 70% of these foods relying on bee pollination.
Bees are integral to our ecosystem, and if they disappear, it will have a severe impact on our food sources.
People should be motivated to protect bees based on the importance of their role in our survival.
The profit-driven nature in America often conflicts with environmental conservation efforts.
Commercial beekeepers are also facing colony collapse issues, adding to the challenges bees are currently facing.

Actions:

for environmental enthusiasts,
Advocate for the protection of bees by supporting initiatives and policies that aim to conserve their habitats and populations (implied).
Educate others on the importance of bees in our ecosystem and food production to raise awareness and encourage action (implied).
Support local beekeepers and beekeeping initiatives to help sustain bee populations and combat colony collapse (implied).
</details>
<details>
<summary>
2021-10-21: Let's talk about Trump's Truth network.... (<a href="https://youtube.com/watch?v=01NGe-2o5zY">watch</a> || <a href="/videos/2021/10/21/Lets_talk_about_Trump_s_Truth_network">transcript &amp; editable summary</a>)

Former President's venture into Truth, a failing social media platform, faces criticism for its branding, lack of user-friendliness, and potential to attract FBI attention.

</summary>

"A tweet and a retweet are called a truth and a retruth. Nobody's going to say that."
"At the end of the day, this is going to crash and burn in glorious fashion."
"It's a win."

### AI summary (High error rate! Edit errors on video page)

Former president launches a social media venture called Truth, despite his history of not sticking to the truth on major social media sites.
The site, still not officially live, has already crashed once and experienced a cyber security issue.
People accessed the site via a beta link, revealing that it's an off-brand version of Mastodon, not Twitter.
The branding of the site is criticized, with terms like "truth" and "retruth" for tweet and retweet not being user-friendly.
Despite being touted as a free speech platform, parody accounts were banned, and making fun of the platform is a violation of terms of service.
The Trump team plans to launch Trump Plus, a streaming service to compete with Netflix and Hulu, which raises skepticism due to lack of experience in the field.
Market saturation and lack of new ideas from the far right raise doubts about the potential success of Truth within the Republican Party.
The platform is speculated to attract FBI attention due to its target audience, potentially aiding investigations related to events like January 6th.
The development of Truth raises questions about whether Trump's past rhetoric against big tech was strategic groundwork for his own social media platform.
Beau predicts that Truth will likely fail spectacularly due to various issues, lack of appeal compared to existing platforms like Twitter, and potential for attracting a problematic user base.

Actions:

for social media users,
Monitor developments and potential impacts of Truth platform (implied)
Exercise caution and critical thinking when considering joining new social media platforms (implied)
Engage in open dialogues regarding the implications of political figures launching their own media ventures (implied)
</details>
<details>
<summary>
2021-10-20: Let's talk about when Steve Bannon will get arrested.... (<a href="https://youtube.com/watch?v=I6K7bAs1msI">watch</a> || <a href="/videos/2021/10/20/Lets_talk_about_when_Steve_Bannon_will_get_arrested">transcript &amp; editable summary</a>)

House panel votes Bannon in contempt, signaling tougher stance on obstruction and potential new information.

</summary>

"House panel voted unanimously to hold Steve Bannon in criminal contempt."
"The House panel seems serious about playing tougher with those obstructing the process."
"Bannon might have information that could alter or reinforce existing beliefs."

### AI summary (High error rate! Edit errors on video page)

House panel voted unanimously to hold Steve Bannon in criminal contempt for defying subpoenas.
Bannon has been defiant by not turning over documents or being deposed.
After the house panel vote, it goes to the full house for certification, then to federal law enforcement.
Federal law enforcement decides whether to proceed after reviewing the report.
If they decide to proceed, it then goes to a grand jury.
Only if the grand jury decides to proceed might Bannon get picked up, so it's not immediate.
The charge is not huge, with a minimum sentence of 30 days and up to a year.
The House panel seems serious about playing tougher with those obstructing the process.
They aim to send a message to others considering obstructing.
The House panel believes there's more information they don't know, hence their strong actions.
Bannon being in trouble is a bad sign for former President Trump.
Bannon might have information that could alter or reinforce existing beliefs.
House panel believes Bannon has critical information that could change the narrative.
Cooperation might not stop the proceedings once it reaches the Department of Justice.
House panel believes Bannon holds information that could provide more insight into events.

Actions:

for political observers, justice advocates.,
Stay informed on the developments in Bannon's case (implied).
Support organizations advocating for transparency and accountability in government actions (implied).
</details>
<details>
<summary>
2021-10-20: Let's talk about individual interests and unions.... (<a href="https://youtube.com/watch?v=Yup2rH4OsKY">watch</a> || <a href="/videos/2021/10/20/Lets_talk_about_individual_interests_and_unions">transcript &amp; editable summary</a>)

Exploring collective vs. personal interests, Beau dismantles anti-union rhetoric, advocating for unions as vital for empowering workers against management.

</summary>

"They have somebody who takes a cut of what they make to represent them."
"That collective interest is your individual interest."
"It gives you more power at the bargaining table."
"The only way you're going to be able to do that isn't by putting on your best suit and walking into the manager's office."
"It's through collective power to achieve your individual interests."

### AI summary (High error rate! Edit errors on video page)

Exploring individual interests and collective interests, unions, and representation.
Answering a question that starts off strong but repeats weak arguments from others.
Contrasting the narrative of representing oneself with the reality of right-wing talking heads having agents.
Pointing out the hypocrisy of those who advocate against representation while having agents themselves.
Emphasizing the importance and power of collective representation through unions.
Describing how collective interest through unions benefits both individuals and the group.
Arguing that unions empower workers against management's dominance.
Supporting the idea of unions as vital for balancing power dynamics in today's world.

Actions:

for workers, activists, advocates,
Join a union to collectively advocate for your rights (exemplified)
Educate yourself on the benefits of unions and collective bargaining (suggested)
</details>
<details>
<summary>
2021-10-19: Let's talk about paternity leave.... (<a href="https://youtube.com/watch?v=En7UPgXn4Mw">watch</a> || <a href="/videos/2021/10/19/Lets_talk_about_paternity_leave">transcript &amp; editable summary</a>)

Beau dismantles the GOP's manipulation of working-class voters against paternity leave, exposing toxic masculinity stereotypes and advocating for spending time with kids.

</summary>

"They play into that stereotype that paternity leave is just for gay men and gay men are weak and you don't want to be like them, right?"
"It's wild to me, but this is how they do it."
"This is how they convince you to go against your own interests."
"All they have to do is convince their base that they're in a slightly better position than somebody else of a demographic they don't like."
"Go spend some time with your kids."

### AI summary (High error rate! Edit errors on video page)

Addresses the topic of paternity leave after receiving viewer messages about Mayor Pete and supply chain issues.
Shares his support for paternity leave, having taken time off after the birth of his children.
Criticizes the Republican Party for discouraging spending time with kids despite promoting family values.
Points out how the GOP manipulates working-class voters to go against policies that could benefit them, like paternity leave.
Notes the party's tactic of associating policies they dislike with demographics they've marginalized, such as gay people.
Calls out the stereotype that paternity leave is only for weak men, perpetuated by the GOP.
Mentions the military parental leave program as a counter to the notion that paternity leave is weak or unmasculine.
Criticizes the manipulation tactics used to sway individuals against policies that could personally benefit them.
Encourages spending time with kids and challenges toxic masculinity views perpetuated by societal norms.

Actions:

for working-class parents,
Spend quality time with your kids (exemplified)
Challenge toxic masculinity stereotypes by supporting paternity leave policies (exemplified)
</details>
<details>
<summary>
2021-10-19: Let's talk about how I learned to love hypersonic missiles.... (<a href="https://youtube.com/watch?v=OGbMyIarrn0">watch</a> || <a href="/videos/2021/10/19/Lets_talk_about_how_I_learned_to_love_hypersonic_missiles">transcript &amp; editable summary</a>)

Beau believes the focus on hypersonic missiles overshadows the real threat posed by armed robot dogs, urging for a shift in media coverage priorities towards what truly impacts people's lives.

</summary>

"Worrying about the hypersonic missile technology is a little bit like worrying about whether or not the rifle that shot you in the head had good optics."
"I am far more concerned about those little robot dogs."
"That's not the one you need to worry about the most."
"Prepare for Cold War style coverage in that atmosphere because that's what's coming down the road."
"There's a missed priority when it comes to coverage."

### AI summary (High error rate! Edit errors on video page)

Beau addresses the hype around hypersonic missiles and robot dogs, prompted by recent events involving China's successful test.
He believes the concern over hypersonic missiles may be overblown, especially in the context of nuclear warfare where the outcome is bleak regardless.
Beau points out that the US already has similar technology like hypersonic missiles and suggests that parity will be achieved eventually.
He expresses more concern about the implications of robot dogs armed with rifles, foreseeing a potential shift in warfare tactics.
Beau humorously touches on the choice of rifle caliber for the robot dogs and the likelihood of them being more actively used than hypersonic missiles.
He concludes by warning about the misplaced priorities in media coverage, urging for attention on what truly impacts people's lives, such as the deployment of robot dogs.

Actions:

for military analysts, policymakers,
Prepare for a potential shift in warfare tactics with the deployment of robot dogs armed with rifles (exemplified)
Shift media coverage priorities towards issues that have a more direct impact on people's lives, such as the use of lethal technology like robot dogs (suggested)
</details>
<details>
<summary>
2021-10-19: Let's talk about Manchin selling out coal miners.... (<a href="https://youtube.com/watch?v=Oa9cODx1BVI">watch</a> || <a href="/videos/2021/10/19/Lets_talk_about_Manchin_selling_out_coal_miners">transcript &amp; editable summary</a>)

Beau debunks the myth that Manchin supported coal miners, revealing his actions favored owners' profits over workers' well-being.

</summary>

"He did not do this for the miners."
"He did it for the owners."
"He pushed them under a bus and didn't even tell them it was coming."

### AI summary (High error rate! Edit errors on video page)

Beau debunks the notion that Manchin's actions were for coal miners, asserting that it was actually for the owners.
Using the typewriter industry as an analogy, Beau explains how people switch to better alternatives when they become available, similar to the decline in coal usage.
He presents statistics showing the significant decrease in coal production and usage in the US over the years.
Beau points out that coal now contributes to less than a quarter of electricity in the US, falling behind renewables and nuclear energy.
Despite declining demand domestically, attempts to rely on exports have also been unsuccessful.
He predicts the inevitable demise of the coal industry, despite Manchin's efforts buying a few more years for the owners, not the miners.
Beau criticizes Manchin for not advocating for beneficial programs like re-education, retraining, or tax breaks for coal miners transitioning to other industries.
He expresses concern that miners will face layoffs and financial struggles as the industry continues to decline.
Beau accuses Manchin of prioritizing the owners' profits over the well-being of the miners, leading to a bleak future for those employed in the coal industry.
In conclusion, Beau asserts that Manchin's actions were not in the miners' interests but rather served the owners' agenda.

Actions:

for advocates for workers' rights,
Advocate for programs like re-education, retraining, and tax breaks for coal miners transitioning to other industries (suggested)
Support initiatives that prioritize miners' well-being over owners' profits (implied)
</details>
<details>
<summary>
2021-10-18: Let's talk about Mayor Pete messing up the supply chain.... (<a href="https://youtube.com/watch?v=VkxJuMKZvpw">watch</a> || <a href="/videos/2021/10/18/Lets_talk_about_Mayor_Pete_messing_up_the_supply_chain">transcript &amp; editable summary</a>)

Beau clarifies misinformation blaming Mayor Pete for global supply chain issues and warns about the dangers of information silos.

</summary>

"You can be handed a perception that isn't grounded in reality."
"Whether or not that person or demographic of people is actually responsible for it, that all depends on whether or not you look outside that information silo."
"Mayor Pete has nothing to do with this."

### AI summary (High error rate! Edit errors on video page)

Beau criticizes the blame on Mayor Pete for US supply chain issues, citing right-wing media.
He checks his news app and discovers the supply chain issues are actually in the UK and Ireland, not Mayor Pete's responsibility.
Beau questions why misinformation blaming Mayor Pete is allowed to circulate and blames information silos.
He explains that Mayor Pete's role as Secretary of Transportation doesn't directly influence global supply chain issues.
Beau warns about the dangers of information silos and how they can distort reality.
Despite Mayor Pete's position and responsibilities, Beau clarifies that the supply chain issues are a global problem, not localized to the US.

Actions:

for information seekers, critical thinkers.,
Fact-check news from multiple sources (implied).
Break free from information silos by seeking diverse news outlets (implied).
</details>
<details>
<summary>
2021-10-17: Let's talk about an update on Biden's wind-power push... (<a href="https://youtube.com/watch?v=RZ0uNIxqSIU">watch</a> || <a href="/videos/2021/10/17/Lets_talk_about_an_update_on_Biden_s_wind-power_push">transcript &amp; editable summary</a>)

Biden administration expands offshore wind push, facing challenges in achieving 30 gigawatts by 2030, but moving in the right direction.

</summary>

"Either way, it's a move in the right direction."
"So far, it looks good."

### AI summary (High error rate! Edit errors on video page)

Biden administration expanding offshore wind push from three to seven lease sites.
Goal of achieving 30 gigawatts of offshore wind power by 2030 set by Biden administration.
Environmental studies, military operations impact, and underwater archaeological sites considerations before leasing sites for wind turbines.
Private companies to lease designated areas for offshore wind power production.
Potential opposition from fishing groups and offshore drilling companies, but unlikely to form a united front.
Comparison with China's goal of 73 gigawatts and the UK leading in offshore wind power.
Biden administration opening up the entire US coast for offshore wind projects with certain restrictions.
Anticipated pushback from tourist organizations and businesses reliant on tourism.
Possibility of overcoming challenges with proper planning and execution before Biden's first term ends in 2024.
Overall, a positive step towards clean energy, although more significant steps may be necessary.

Actions:

for environmental advocates, energy policymakers.,
Support offshore wind power initiatives by advocating for clean energy policies (suggested).
Stay informed about offshore wind projects and their impact on the environment and local communities (implied).
</details>
<details>
<summary>
2021-10-17: Let's talk about a crashed tractor and unskilled labor..... (<a href="https://youtube.com/watch?v=40GTOqOyedA">watch</a> || <a href="/videos/2021/10/17/Lets_talk_about_a_crashed_tractor_and_unskilled_labor">transcript &amp; editable summary</a>)

Beau challenges the concept of "unskilled labor," arguing that all work requires skill development and deeming the term as a method to devalue workers.

</summary>

"The idea of unskilled labor, it's a fiction."
"It's just a method of devaluing the workers."
"There aren't a whole lot of jobs you can walk into and just do it."
"If somebody is putting in the work, they should probably have access to a decent life."
"The skills are required."

### AI summary (High error rate! Edit errors on video page)

Addresses the term "unskilled labor" and argues it is a misnomer.
Shares an incident where salaried employees were asked to replace striking workers at John Deere, resulting in a mishap.
Challenges the idea that jobs labeled as unskilled truly require no skill.
Mentions that even service industry jobs like waiting tables require time to develop the necessary skills.
Criticizes the term "unskilled labor" for devaluing the work and justifying low wages.
Asserts that most jobs require learning and skill development, even if they don't demand formal education.
Urges for the retirement of the term "unskilled labor" due to its role in diminishing the value of workers' contributions.

Actions:

for workers, activists, labor advocates,
Advocate for fair wages and recognition of skill in all types of work (implied)
Support labor movements and strikes to ensure workers are valued for their contributions (exemplified)
</details>
<details>
<summary>
2021-10-16: Let's talk about prices and supply chains.... (<a href="https://youtube.com/watch?v=l0juJUD29gc">watch</a> || <a href="/videos/2021/10/16/Lets_talk_about_prices_and_supply_chains">transcript &amp; editable summary</a>)

Beau explains the impact of changing consumer habits on prices, supply chain disruptions, and the lasting inconvenience of current market challenges.

</summary>

"It's just going to be more of a pebble in our shoe, constantly annoying, and it's going to last a while."
"Rather than something severe that is short-lived, it's going to be annoying and it's going to last a long time."
"You should be able to get your basic needs. Maybe not the particular brand of corn or whatever you like, but you'll be fine when it comes to basic survival."
"It's just going to be more of a pebble in our shoe, constantly annoying, and it's going to last a while."
"Maybe steer your children into asking Santa for things that don't have a whole lot of electronics in them."

### AI summary (High error rate! Edit errors on video page)

Explains why prices are going up due to supply and demand dynamics.
Consumer spending habits changed during the pandemic, leading to increased demand for certain items.
Production was down as businesses and factories were closed, resulting in less supply and higher prices.
Gives a personal example of fluctuating prices for building materials like lumber.
Describes how the increase in home improvement projects led to a surge in demand for lumber.
Many consumers unknowingly overpaid for products due to lack of comparison and market forces.
Notes that prices are going up for a variety of products due to changing habits.
Mentions that as people start venturing out more, demand for different products is shifting.
Talks about disruptions in the supply chain due to factories shutting down during the public health crisis.
Companies are now trying to catch up on production, leading to delays in getting products off boats.
Foresees these supply chain issues taking about a year to resolve.
Assures that severe shortages are unlikely but warns of ongoing annoyances due to supply chain hiccups.
Advises being cautious during the holidays, especially with electronics-heavy gifts.
Suggests preparing for potential scarcity of certain items without causing panic.
Concludes by reassuring that the situation is manageable but may be inconvenient for a while.

Actions:

for consumers, holiday shoppers,
Be cautious with holiday shopping, especially items heavy on electronics (implied)
Prepare for potential scarcity by focusing on basic survival needs (implied)
Stay informed about market dynamics and adjust shopping habits accordingly (implied)
</details>
<details>
<summary>
2021-10-16: Let's talk about Benton Harbor or Flint 2.0.... (<a href="https://youtube.com/watch?v=Q28kJtDYxSI">watch</a> || <a href="/videos/2021/10/16/Lets_talk_about_Benton_Harbor_or_Flint_2_0">transcript &amp; editable summary</a>)

Benton Harbor, Michigan faces a water crisis as infrastructure crumbles, demanding urgent action to protect residents and prevent further decay.

</summary>

"There is no safe level of lead, especially when you're talking about kids."
"We have to repair the infrastructure in this country. We don't really have a choice on this."
"If your party claimed that they wanted to make America great for years [...] I doubt your commitment to your cause."

### AI summary (High error rate! Edit errors on video page)

Benton Harbor, Michigan, a town of under 10,000 people, faces lead levels above 15 parts per billion in the water.
Initially, Governor Gretchen Whitmer's response to the crisis was lackluster, with a five-year timeline for resolution.
Due to criticism and pressure, the timeline has been accelerated to 18 months for fixing the water issue.
Residents in Benton Harbor will receive free bottled water during this period.
Qualified homes with children under Medicaid will receive free or reduced-cost abatement services for issues within the home.
The situation in Benton Harbor underscores the crumbling and decaying state of infrastructure in the United States.
Infrastructure is vital for the country to function, and without proper maintenance, it will continue to degrade.
Despite the improved response under Whitmer, there may not be similar legal consequences faced by former governor Rick Snyder.
Continuous monitoring and support for communities like Benton Harbor are necessary as they can be easily forgotten by those in power.
The presence of any level of lead in water poses risks, especially for children, necessitating urgent action.

Actions:

for community members, activists, policymakers,
Monitor and support communities facing infrastructure issues (suggested)
Advocate for swift and effective infrastructure repairs (implied)
</details>
<details>
<summary>
2021-10-15: The Roads to a 7-month strike in Alabama.... (<a href="https://youtube.com/watch?v=cD3CM_RVoDY">watch</a> || <a href="/videos/2021/10/15/The_Roads_to_a_7-month_strike_in_Alabama">transcript &amp; editable summary</a>)

Beau follows up on a strike at Warrior Met Coal in Alabama where workers fight for safety, fair labor practices, and family time amidst company resistance, financial strain, and community support.

</summary>

"It's not just about pay for us. It's about holding the company accountable."
"We're still ready to be here. We're not ready to give this fight up until we win."
"It's really only been through all the mutual aid that we've received and through the union that we have been able to hold on as long as we have."

### AI summary (High error rate! Edit errors on video page)

Provides a follow-up on a strike happening in Alabama at Warrior Met Coal.
Hayden Rot, the auxiliary president for the MWA at two locals, gives insight into the strike.
The mine specifically mines metallurgical coal for steel production, not thermal coal for energy.
Workers have been on strike since April 1st due to safety concerns, unfair labor practices, and family time.
The company has a policy of scheduling workers seven days a week, 12 hours a day, with harsh penalties for absences.
Previous contract changes removed double-time and triple-time pay, limiting time off during holidays for families.
Vehicular assaults on picket lines by company members or scabs have occurred.
Negotiations have not progressed well, with the company offering improvements that favor scabs over union members.
The company aims to hire back only those who they believe did nothing wrong during the strike, excluding vocal union members.
Financial strain is increasing as the strike enters its seventh month, with efforts to support union families through the holidays.

Actions:

for supporters of labor rights,
Donate to UMWH Striking Miners Pantry PayPal account for groceries, baby items, and hygiene products (suggested)
Support the toy drive through the Target registry link provided by Hayden (suggested)
Attend solidarity rallies or events organized by the union to show support (implied)
</details>
<details>
<summary>
2021-10-15: Let's talk about the $600 IRS provision.... (<a href="https://youtube.com/watch?v=ilhx5Mohpb0">watch</a> || <a href="/videos/2021/10/15/Lets_talk_about_the_600_IRS_provision">transcript &amp; editable summary</a>)

Beau explains the ineffective and wasteful nature of the IRS proposal in the reconciliation bill, citing a low $600 threshold and flawed design as major concerns.

</summary>

"This seems like a really dated idea. It seems like it is way out of touch with reality."
"It's a completely pointless provision. It's a completely pointless policy. It's not going to do any good."
"It doesn't seem like it will do anything to curb tax avoidance, which is the stated goal."

### AI summary (High error rate! Edit errors on video page)

Explains the IRS proposal in the reconciliation bill, causing many questions and concerns among people.
Points out the main concerns are the $600 threshold for IRS reports and a misunderstanding about what information the IRS will receive.
Clarifies that the IRS will receive data on how much money goes into and out of an account, not detailed spending information.
Acknowledges privacy concerns but notes that the reported information is more limited than perceived.
Questions the effectiveness of the $600 threshold, deeming it illogical and a waste of resources for minor tax discrepancies.
Argues that the low threshold will flood the IRS with reports, rendering the whole process ineffective against tax avoidance.
Suggests setting a higher threshold to target those with significant funds and offshore accounts, rather than those barely making ends meet.
Criticizes the provision as outdated, out of touch with reality, and likely ineffective due to poor design.
Concludes that the provision is pointless, ineffective, and a waste of taxpayer money, unlikely to achieve its goal of curbing tax avoidance.

Actions:

for taxpayers, policymakers,
Challenge the $600 threshold by contacting relevant policymakers to advocate for a higher threshold (suggested).
Join or support organizations working on tax policy reform to address ineffective provisions like the one discussed by Beau (exemplified).
</details>
<details>
<summary>
2021-10-15: Let's talk about public health at the border and inconsistency.... (<a href="https://youtube.com/watch?v=ePWv7uaPrlk">watch</a> || <a href="/videos/2021/10/15/Lets_talk_about_public_health_at_the_border_and_inconsistency">transcript &amp; editable summary</a>)

Beau addresses right-wing criticism on vaccination, opposing forced measures and challenging the bigotry behind locking up unvaccinated individuals.

</summary>

"I don't believe the government should use force, deny your liberty, to make you get vaccinated. That would be wrong."
"Anything you advocate for them, well, it should be done to you as well, unless, of course, you believe there's two justice systems in the United States."
"Anything that you allow the government to do to a small demographic, you're just opening the door for yourself."
"You will advocate for your own incarceration because they have that much of a hold over you."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Addressing criticism from the right wing about the Biden administration's handling of the border and public health.
Refusing to advocate for forced vaccination at the border, focusing on appealing to protecting oneself and others.
Stating a strong pro-vaccine stance but opposing using force or denying liberty to enforce vaccination.
Criticizing the idea of locking up unvaccinated individuals, pointing out the underlying bigotry in such arguments.
Challenging the notion that unvaccinated individuals won't seek vaccination once released.
Expressing concern about those who advocate for measures they wouldn't want applied to themselves.
Warning against blindly repeating arguments without critical thinking and understanding the implications.
Asserting that people crossing the border have rights under the US legal system.
Emphasizing the paternalistic nature of forcing vaccination on certain groups.

Actions:

for public health advocates,
Challenge and critically analyze arguments before repeating them (implied)
Advocate for equal treatment and rights for all individuals (implied)
</details>
<details>
<summary>
2021-10-14: Let's talk about Trump's message to the GOP.... (<a href="https://youtube.com/watch?v=6xs4ikEXQbo">watch</a> || <a href="/videos/2021/10/14/Lets_talk_about_Trump_s_message_to_the_GOP">transcript &amp; editable summary</a>)

Beau decodes Trump's message, warning of potential GOP division and voter manipulation with fabricated claims.

</summary>

"Back my lies or I'm going to tell my followers not to vote and that's going to hurt you."
"It's not true. There's no evidence. It doesn't exist."
"I don't like underestimating him."
"If Republicans go for it and they start backing this and they start to re-litigate 2020, I think it'll damage them."
"I do believe that he is a negative force for stability in this country."

### AI summary (High error rate! Edit errors on video page)

Analyzing Trump's latest statement, Beau points out the dual meaning of "Republicans" in the message, indicating voters and politicians.
Trump's message implies that if the election fraud issue isn't resolved, Republican voters may abstain from voting in 2022 and 2024.
The message seems like a threat to the GOP to either support Trump's lies or risk losing voter turnout.
Beau expresses concern that GOP members might cave to Trump's demands due to a perceived lack of strong backbone.
Politically, Beau doubts that Trump's tactics will significantly impact Democrats, as most Americans are not swayed by his claims.
Beau questions where the evidence for Trump's claims is, noting that it appears to be non-existent.
He believes Trump is attempting to manipulate the GOP into perpetuating his lies, similar to how he manipulated his base.
Beau predicts that if Republicans continue to support Trump's falsehoods and revisit the 2020 election, it will likely harm them politically.
Despite potential negative outcomes for Republicans, Beau acknowledges that underestimating Trump's influence could be dangerous due to his destabilizing impact.
Beau concludes by urging vigilance in monitoring Trump's efforts to sway politicians into endorsing his fabrications.

Actions:

for political observers,
Monitor and challenge misinformation spread by political figures (suggested)
Stay informed about political developments and potential manipulation tactics (suggested)
</details>
<details>
<summary>
2021-10-14: Let's talk about Chicago cops.... (<a href="https://youtube.com/watch?v=wxGAhd9LQ8g">watch</a> || <a href="/videos/2021/10/14/Lets_talk_about_Chicago_cops">transcript &amp; editable summary</a>)

The Chicago police union's defiance on vaccination status reveals deeper issues with public safety and judgment in law enforcement.

</summary>

"This is a really easy way to weed out bad officers."
"Do you want to protect your neighbor? Or do you want to go with a propagandized talking point?"
"If they're not willing to commit to that mission, they probably need a different profession."

### AI summary (High error rate! Edit errors on video page)

The Chicago police union advised officers not to disclose vaccination status, defying city requirements.
Unions typically oppose non-pre-negotiated issues, defaulting to opposition.
Police unions differ significantly from other unions.
Ignoring vaccination policy raises concerns about following other public safety policies.
Lack of vaccination value in law enforcement questions officers' judgment.
Officers regularly face situations where vaccination is vital for public safety.
Non-compliance with vaccination poses higher infection risk for officers and community members.
The mission of law enforcement is public safety; non-compliance suggests a need for a different profession.
Union concerns about firing officers gradually rather than all at once.
Non-compliance with public safety measures is an easy way to filter out unsuitable officers.
Society faces a choice: be part of the solution or follow propagandized talking points.
The Chicago union made its choice; the city's response is critical.

Actions:

for law enforcement officers,
Advocate for public safety measures within law enforcement (implied)
Support policies that prioritize public health and safety (implied)
</details>
<details>
<summary>
2021-10-13: Let's talk about National Police Week.... (<a href="https://youtube.com/watch?v=9LiZuivD_yg">watch</a> || <a href="/videos/2021/10/13/Lets_talk_about_National_Police_Week">transcript &amp; editable summary</a>)

National Police Week prompts reflection on officer challenges and the disconnect between rhetoric and action in supporting law enforcement.

</summary>

"Maybe 'back the blue' isn't quite what they really mean."
"They certainly don't seem to support law enforcement."
"The loyalty test around COVID precautions has led to preventable deaths among officers."
"Despite the high numbers, there's a lack of outcry."
"Calls to action during this time are common when officers face significant risks."

### AI summary (High error rate! Edit errors on video page)

National Police Week prompts reflection on the challenges faced by officers and deputies.
Calls to action during this time are common when officers face significant risks.
In 2020, 264 officers were lost, with 145 of them due to COVID, the leading cause again in the current year.
Despite the high numbers, there's a lack of outcry, especially from those who claim to support law and order.
Some governors prevent law enforcement leaders from mandating COVID precautions, unlike other safety measures like wearing a vest.
The "back the blue" sentiment may be more of a slogan than genuine support, as actions don't always match the rhetoric.
The loyalty test around COVID precautions has led to preventable deaths among officers.
Politicians and governors who back law and order may not fully support law enforcement if they don't prioritize officer safety.
The disconnect between rhetoric and action raises questions about the true intentions behind supporting law enforcement.
Lack of support for necessary precautions may undermine the image of those in power as protectors of citizens.

Actions:

for law enforcement advocates,
Contact local law enforcement agencies to inquire about COVID precaution mandates (suggested)
Advocate for necessary safety measures within law enforcement agencies (implied)
</details>
<details>
<summary>
2021-10-13: Let's talk about Biden's poll numbers.... (<a href="https://youtube.com/watch?v=XXLFyvQS3Os">watch</a> || <a href="/videos/2021/10/13/Lets_talk_about_Biden_s_poll_numbers">transcript &amp; editable summary</a>)

Biden administration must shift policies leftward to maintain coalition and avoid one-term fate.

</summary>

"It's not about 'I'm not Trump.' It's about policy."
"The position that a progressive held after voting begrudgingly for Biden is already in their rear view."
"Most Democratic politicians are center-right. The market decided."

### AI summary (High error rate! Edit errors on video page)

Biden administration's poll numbers are shaky, especially among Hispanics, Blacks, and women, key demographics for winning elections.
Reasons for Biden's falling numbers include stalled legislative agenda, lack of engagement with key groups, and fear of backlash for policies benefiting these groups.
Democrats lack vocal supporters like the Republicans have in new and social media, leading to missed opportunities in promoting policy successes.
Attempting to maintain the coalition by appealing to the center won't work as progressives are constantly moving forward, not back to the center.
Biden administration needs to shift the center to progressive territory to maintain the broad coalition that got them elected.
Lack of progressive cheerleaders in media is a problem for Democrats as most platforms tend to be more progressive than the party itself.
Democrats need to be stronger in making their case, educating the public, and addressing Republican opposition more assertively.
Focusing on being "not Trump" is no longer a winning strategy now that Biden is in office; policy matters more than just anti-Trump sentiment.
Failure to shift policies towards progressive positions and make a strong case could lead to the Biden administration being one-term.

Actions:

for political strategists, democratic party members.,
Educate and mobilize communities about the benefits of the infrastructure package and other policies. (implied)
Advocate for shifting policies towards progressive positions within the Democratic Party. (implied)
Engage with media platforms to amplify progressive voices and ideas. (implied)
</details>
<details>
<summary>
2021-10-12: Let's talk about the new Superman and the supermad.... (<a href="https://youtube.com/watch?v=6lx2txSsPw4">watch</a> || <a href="/videos/2021/10/12/Lets_talk_about_the_new_Superman_and_the_supermad">transcript &amp; editable summary</a>)

Beau addresses the backlash against Superman's son being portrayed as bisexual, calling out the bigotry and hypocrisy surrounding objections to LGBTQ+ representation in comics.

</summary>

"It isn't that. It's that you don't like this particular kind. That's what it is. Yes, it's bigotry."
"What happens if your kid is bi, and they don't see any representation for how they are, who they are?"
"Most comics have that overriding theme that everybody's a hero, and that all we have to do is support each other."

### AI summary (High error rate! Edit errors on video page)

Addressing the backlash surrounding Superman's son, John Kent, being portrayed as bisexual.
Pointing out the hypocrisy of people claiming they don't care about superheroes' love lives when it has always been a central aspect of comic book characters.
Listing various comic book characters who have had love interests throughout the comics.
Calling out the underlying bigotry in objecting to John Kent's bisexuality.
Emphasizing the importance of representation for individuals who may identify as bisexual.
Questioning the lack of understanding and acceptance from those who object to LGBTQ+ representation in comic books.
Reminding readers that comics often convey messages of overcoming biases and supporting each other.

Actions:

for comic book fans, lgbtq+ advocates,
Support LGBTQ+ representation in comic books (suggested)
Provide positive support to individuals in your life who identify as LGBTQ+ (implied)
Overcome biases, prejudices, and bigotry in your interactions (exemplified)
</details>
<details>
<summary>
2021-10-12: Let's talk about the message from Southwest pilots.... (<a href="https://youtube.com/watch?v=Y6G7DQ68aGY">watch</a> || <a href="/videos/2021/10/12/Lets_talk_about_the_message_from_Southwest_pilots">transcript &amp; editable summary</a>)

Southwest pilots debunk coordinated action narrative; misinformation benefits from blind belief and emotional manipulation.

</summary>

"They have found a way to monetize you."
"Smart people want to empower you to lead from your own community."
"Ignore evidence. And listen to people who never provide the evidence they say they have."
"They have gathered a group of middle-aged, middle-income, middle-America."
"Start asking these people for evidence of their claims. You'll find out they don't have it."

### AI summary (High error rate! Edit errors on video page)

Southwest pilots are sending a message by standing up against misinformation and opposition to mandates, which has been debunked.
The sick rate for pilots at Southwest remains the same, indicating there is no widespread coordinated action.
Some pilots may be using sick time before leaving Southwest, but there is no evidence of a coordinated effort.
Aviation experts explain how Southwest's system is susceptible to cascade failure, leading to the recent flight cancellations.
Despite information debunking the narrative, certain individuals continue pushing it because they know it will work on a segment of the population.
Those perpetuating misinformation rely on a segment of the population trained not to question but to believe blindly.
Leaders like Trump, Boebert, Cruz, and media personalities exploit patriotism and lack of critical thinking in their followers.
Individuals pushing false narratives have a history of misleading the public on various issues for their benefit.
True leaders empower others to lead from their communities rather than manipulate them with false information.
People who continue to push debunked narratives are exploiting and monetizing those who believe them, rather than educating them.
Blindly following without evidence or critical thought is detrimental to society and plays into the hands of those spreading misinformation.
The incident at Southwest is a minor example of a larger trend within the right wing that relies on anger and manipulation.
Individuals should demand evidence from those making claims to avoid being emotionally manipulated and misled.
Emotional manipulation and lack of critical thinking enable certain individuals to profit off spreading misinformation.

Actions:

for community members, critical thinkers.,
Demand evidence from individuals spreading misinformation (implied).
Empower others in your community to lead and think critically (implied).
Stop listening to those who continue pushing debunked narratives (implied).
</details>
<details>
<summary>
2021-10-11: Let's talk about Southwest airlines, disruptions, and evidence.... (<a href="https://youtube.com/watch?v=x_xZ4DN32wo">watch</a> || <a href="/videos/2021/10/11/Lets_talk_about_Southwest_airlines_disruptions_and_evidence">transcript &amp; editable summary</a>)

Beau addresses Southwest Airlines disruptions, warns against unfounded conspiracy theories, and stresses the importance of evidence-based skepticism in the face of misinformation.

</summary>

"The thing is, both of those theories have the same amount of evidence, none."
"When you come across something that is unfalsifiable, you have to treat it with a great deal of skepticism."
"Conspiracy theories turn bad when you start to believe it and act on it without any evidence."
"There's no verifiable evidence to support this. Doesn't exist."
"The absence of evidence in and of itself doesn't mean there is evidence of absence either."

### AI summary (High error rate! Edit errors on video page)

Addressing the disruptions and cancellations faced by Southwest Airlines and the claims surrounding them.
Exploring theories involving classified intelligence vets and pilots calling in sick due to vaccine mandates, both lacking evidence.
Emphasizing the absence of publicly verifiable evidence to support these claims, despite contradicting statements from relevant parties.
Predicting the evolution of unfalsifiable narratives to sustain unsupported claims.
Speculating on the probable scenarios behind the flight cancellations, including operational issues and pilot vaccine concerns.
Warning against blindly believing narratives without concrete evidence and the dangers of spreading unfounded conspiracy theories.
Arguing that the lack of evidence does not necessarily disprove the claims but warrants skepticism, especially when circulated by unreliable sources.
Stating the importance of demanding verifiable evidence before believing and acting on information.
Concluding with a caution against buying into conspiracy theories without concrete evidence, urging critical thinking and skepticism.

Actions:

for critical thinkers,
Verify information before spreading it (suggested)
Demand concrete evidence before believing and acting on information (exemplified)
Refrain from blindly supporting narratives without verifiable evidence (implied)
</details>
<details>
<summary>
2021-10-11: Let's talk about Indigenous Peoples' Day and what you can do.... (<a href="https://youtube.com/watch?v=k0ExsSXUZ28">watch</a> || <a href="/videos/2021/10/11/Lets_talk_about_Indigenous_Peoples_Day_and_what_you_can_do">transcript &amp; editable summary</a>)

Indigenous Peoples Day prompts action, with Oak Flat facing a critical tipping point for change through public support against the proposed copper mining.

</summary>

"Today's a good day to get involved in something."
"Movements like this, they're always slow going."
"Even small support might push it over the edge and get something done."

### AI summary (High error rate! Edit errors on video page)

Indigenous Peoples Day prompts action for groups being recognized.
Native causes have various needs, one being Oak Flat protection.
Oak Flat, ancestral home to Apache, Hopi, and Zuni, faces copper mining proposal.
Native groups oppose the mining proposal at Oak Flat.
Public perception on the mining issue is starting to shift.
The mining operation at Oak Flat will use a significant amount of water.
Majority of likely voters in Arizona oppose the mining proposal.
Legislation like the Save Oak Flat Act aims to stop the mining.
A petition supported by ACLU is circulating to save Oak Flat.
Today is a good day to get involved in supporting causes like saving Oak Flat.
Support for causes like this can create a tipping point for change.
Even small support can contribute to significant change.
Building support for movements like this takes time.
The shift in public opinion can lead to tangible outcomes.
Encouraging support for causes can lead to impactful change.

Actions:

for activists, supporters, voters,
Sign and share the petition supported by ACLU to save Oak Flat (suggested).
Support legislation like the Save Oak Flat Act to stop the mining proposal (suggested).
</details>
<details>
<summary>
2021-10-10: Let's talk about how to take criticism.... (<a href="https://youtube.com/watch?v=4IFSJ6_ctf4">watch</a> || <a href="/videos/2021/10/10/Lets_talk_about_how_to_take_criticism">transcript &amp; editable summary</a>)

Beau shares advice on accepting feedback in a professional setting, focusing on active listening, understanding feedback isn't personal, and viewing it as an asset to improve.

</summary>

"It's not actually about getting better at accepting the feedback. It's about doing better at that evaluation."
"View it as an asset. These are free consultants. These are people who are trying to make you better."
"Train yourself to actively listen and make sure that you understand it's not personal."

### AI summary (High error rate! Edit errors on video page)

Talks about accepting feedback and criticism in a professional setting.
Shares a scenario of a person receiving negative feedback on accepting feedback.
Advises on the standard way of accepting feedback: pause, listen, and apply.
Mentions a personal issue with feedback perception rather than accepting it.
Suggests switching out thanking the critic with asking questions to actively listen.
Emphasizes the importance of understanding feedback is not personal and viewing it as an asset.
Encourages actively listening to feedback and applying it for personal growth.
Acknowledges the challenge of waiting for feedback without appearing dismissive.
Notes the importance of fitting into the standard way of accepting feedback in a professional environment.
Recommends training oneself to ask relevant questions to appear engaged during feedback sessions.

Actions:

for professionals,
Train yourself to actively listen and ask relevant questions during feedback sessions (implied).
</details>
<details>
<summary>
2021-10-10: Let's talk about Biden's monumental move.... (<a href="https://youtube.com/watch?v=b1q06fF74BA">watch</a> || <a href="/videos/2021/10/10/Lets_talk_about_Biden_s_monumental_move">transcript &amp; editable summary</a>)

Biden's administration restores protected areas Trump shrank, celebrating cultural significance and environmental protection despite missed legal battle.

</summary>

"The celebration isn't just a PR thing; there's a lot to celebrate."
"Having them around is incredibly important from a cultural standpoint."
"It's far more critical to protect the sites than to get a ruling."
"The restoration extends protections to areas that need it."
"Undoubtedly, there was damage done during that window that will never be repaired."

### AI summary (High error rate! Edit errors on video page)

Biden's administration restored the sizes of protected areas in the U.S. that Trump had shrunk, using the Antiquities Act.
The impacted areas include Bears Ears, Grand Staircase, Northeast Canyon, and Cimonts.
The celebration isn't just a PR move; there are multiple reasons for cheering the restoration.
The restoration protects artifacts, burial sites, structures, fossil dig sites, and biodiversity.
Trump's actions seemed to pave the way for mining and fossil fuel exploration, which could harm the environment.
Native groups see ancient sites as messages from their ancestors, making their protection culturally vital.
The Biden administration gave a native coalition a significant role in managing the protected areas, a positive step.
Beau believes Trump's actions against the protected areas were against the law and should have gone to court.
While a court ruling was missed, the priority remains protecting the sites rather than legal battles.
The downside is not getting a legal ruling against Trump's actions due to the restoration rendering it moot.
The restoration is vital for protecting areas that were lacking proper safeguards, although some damage done may be irreversible.

Actions:

for environmentalists, conservationists, activists,
Support and advocate for the protection of natural and cultural heritage sites (exemplified)
Get involved with native coalitions working towards safeguarding protected areas (exemplified)
</details>
<details>
<summary>
2021-10-09: Let's talk about the jobs report, early unemployment, and wages.... (<a href="https://youtube.com/watch?v=eyPNchSJG94">watch</a> || <a href="/videos/2021/10/09/Lets_talk_about_the_jobs_report_early_unemployment_and_wages">transcript &amp; editable summary</a>)

Analyzing disappointing job numbers post-benefits expiration, Beau stresses the need for higher wages to attract workers, debunking stereotypes and advocating for fair compensation.

</summary>

"You have to pay more. It is that simple."
"It is not the workers' fault that companies want to extract more and more from them."
"If you're running into this issue, if you own a business and you're trying to get workers and you can't, bump the pay. It's really that simple."

### AI summary (High error rate! Edit errors on video page)

Analyzing the recent jobs report that revealed 194,000 new non-farm payrolls added but a shrinking labor force by 183,000, upsetting expectations.
People anticipated more growth post the expiration of unemployment benefits, leading to the lowest numbers all year.
Suggests the low numbers may not be due to laziness but a blend of the ongoing public health crisis, reluctance to risk it, and inadequate wages.
Points out the struggle of finding workers due to inadequate wages, with some stores having to close two days a week for lack of staff.
Emphasizes the basic economic principle of supply and demand, urging businesses to raise wages to attract workers.
Criticizes those who terminated benefits early and labeled workers as lazy freeloaders, indicating they only ended up harming hard-working individuals.
Asserts that workers should not be blamed for companies seeking to extract more from them, urging business owners facing hiring challenges to simply increase pay.

Actions:

for business owners, workers,
Increase wages to attract workers (suggested)
Support businesses paying living wages (implied)
</details>
<details>
<summary>
2021-10-08: Let's talk about your electric bill and the environment.... (<a href="https://youtube.com/watch?v=EZfP4VurCtg">watch</a> || <a href="/videos/2021/10/08/Lets_talk_about_your_electric_bill_and_the_environment">transcript &amp; editable summary</a>)

Beau challenges the narrative around renewable energy, focusing on cost savings and criticizing politicians for propping up fossil fuel industries at the expense of consumers.

</summary>

"We will always do the right thing as soon as we figure out a way to profit off of it."
"You're destroying the environment so some politician in DC can rip you off."
"It's not that you were tricked via the culture war. It's that you're really concerned about your Senator's stock portfolio."
"You should care about your bank account."
"If you don't care about the environment, understand the infrastructure that would allow us to switch over to renewables."

### AI summary (High error rate! Edit errors on video page)

Challenges the conventional perspective on advocating for renewable energy by shifting the focus from the environment to money and profit.
Points out that it is cheaper to install solar energy infrastructure than gas or coal-fired plants, with costs constantly decreasing due to technological advancements.
Mentions a study from Oxford that underestimated the significant reduction in costs associated with renewable energy.
Emphasizes that putting in renewable energy infrastructure is not only cheaper but also more reliable compared to traditional fossil fuels.
Criticizes politicians who subsidize failing fossil fuel industries, ultimately causing consumers to pay more through their bills.
Urges individuals to care about their bank accounts if they don't prioritize environmental concerns, as switching to renewables could save trillions according to the Oxford report.
Suggests that support for fossil fuels is driven by politicians' personal financial interests rather than genuine concern for the environment.
Distinguishes the issue from nostalgia or cultural wars, stating that it's about politicians profiting at the expense of the public.
Concludes by inviting reflection on the topic and wishes the audience a good day.

Actions:

for environmentally conscious individuals,
Contact local representatives to advocate for policies that support renewable energy (suggested)
Educate yourself and others about the financial benefits of transitioning to renewables (exemplified)
Join community initiatives promoting sustainable energy practices (implied)
</details>
<details>
<summary>
2021-10-08: Let's talk about 44% of republicans and Trump 2024.... (<a href="https://youtube.com/watch?v=f8aWg6UrPOU">watch</a> || <a href="/videos/2021/10/08/Lets_talk_about_44_of_republicans_and_Trump_2024">transcript &amp; editable summary</a>)

Beau reports on a poll showing that while 44% of Republicans support Trump running in 2024, the majority do not, indicating bad news for Trump's future aspirations and suggesting he is not in a favorable position for a 2024 run.

</summary>

"Forty-four percent of Republicans want Trump to run again in 2024."
"Another way to say that would be a majority of Republicans don't want him to."
"These are bad numbers for the former president."
"They're probably going to continue to fall, especially as other political figures are having to put their plans on hold."
"There is no way to honestly look at these numbers and think that the former president is in a good position for a 2024 run."

### AI summary (High error rate! Edit errors on video page)

Report on a poll regarding whether Republicans want Trump to run again in 2024.
Forty-four percent of Republicans support Trump running in 2024.
However, the majority of Republicans do not want him to run again.
Trump's support is not as significant as some portray it.
The numbers indicate bad news for Trump's future aspirations.
As more is revealed about his policies, support for Trump is expected to decrease.
Trump's indecisiveness about announcing his candidacy is not inspiring confidence.
It is unlikely that his support numbers will increase from here.
Other political figures might begin to criticize and undermine Trump due to his lack of leadership.
The most likely scenario is for Republicans to support Trump as a political figure but prefer other candidates.
Right-wing outlets publicizing this poll may be aiming to weaken Trump's position for 2024.
Overall, the numbers suggest Trump is not in a favorable position for a 2024 run.

Actions:

for political analysts,
Analyze and understand the implications of the poll results (implied)
Support political figures who represent your views and values (implied)
</details>
<details>
<summary>
2021-10-07: Let's talk about the costs of not passing the infrastructure bill.... (<a href="https://youtube.com/watch?v=pl1tiy3GYqw">watch</a> || <a href="/videos/2021/10/07/Lets_talk_about_the_costs_of_not_passing_the_infrastructure_bill">transcript &amp; editable summary</a>)

Beau compares infrastructure bill costs to DOD's budget, urging reevaluation of opposition for a resilient future mitigating climate impacts.

</summary>

"If you are in opposition to this, you might want to really rethink your position."
"Not sure why people would be in opposition to this unless they want the United States to fail."
"The future is determined by what happens now, by what we do today."
"You might want to spend it on roads and bridges and cleaner energy or you want to spend it on flag-covered coffins in the future."
"It is worth noting that DOD is spending a whole lot of that 8 trillion to gear up to fight over resources."

### AI summary (High error rate! Edit errors on video page)

Comparative analysis on the infrastructure bill versus the DOD budget over 10 years.
Infrastructure bill aims to accomplish infrastructure development and address climate change impacts.
DOD spending a significant amount on preparing for resource-based conflicts.
The bill will provide necessary infrastructure to help the US cope with climate change impacts.
UN study shows a significant increase in flood and drought-related disasters.
Projection of inadequate water access for 5 billion people by 2050.
Mitigating these effects could save lives and resources.
Questioning opposition to the bill, suggesting it’s vital for national resilience.
Implication that opposition may have ulterior motives benefiting from war machine profits.
Stressing the importance of current actions shaping the future.
Infrastructure bill seen as a means to reduce severity of climate-related impacts.
Urging reevaluation of opposition stance towards investing in infrastructure.
Choice between investing in infrastructure or future military casualties.
Encouraging reflection and reconsideration of positions on the bill.

Actions:

for us citizens,
Support infrastructure bill to invest in sustainable infrastructure and combat climate change (implied).
</details>
<details>
<summary>
2021-10-07: Let's talk about recent coverage of Trump.... (<a href="https://youtube.com/watch?v=zTGQozEWM7o">watch</a> || <a href="/videos/2021/10/07/Lets_talk_about_recent_coverage_of_Trump">transcript &amp; editable summary</a>)

Speculating on whether the Democratic Party is orchestrating media tactics to push the former president to announce his candidacy early, Beau examines the former president's vulnerabilities and suggests that the current events may be organic rather than part of a political operation.

</summary>

"He may be surrounded by opportunistic people who see more benefit in betraying him than sticking by him."
"He's in a weakened state, and his inner circle's disloyalty may lead to his downfall."
"I hope he just fades away, goes and plays golf and stays out of it."
"I think he could very easily become a force that is very bad for stability within the United States."

### AI summary (High error rate! Edit errors on video page)

Speculates on whether the Democratic Party is using the media to push the former president to announce his candidacy early for 2024.
Explains the motive behind such a strategy: negative voter turnout for the former president drives people to vote against him, benefiting the Democratic Party.
Analyzes the former president's characteristics like paranoia, distrust, ego-driven behavior, and susceptibility to manipulation.
Mentions various news stories aimed at triggering the former president's vulnerabilities and weak spots.
Considers the feasibility of a political operation by examining motives, likelihood of success, and media manipulation.
Points out that while there is no concrete evidence of such an operation, circumstantial events seem to line up.
Suggests that the simplest explanation could be the former president's current weakened state and disloyalty from opportunistic individuals in his circle.
States that it's more probable that the current events are organic rather than orchestrated by political operatives.
Expresses concern over underestimating the former president's potential negative impact on U.S. stability if he remains active in politics.

Actions:

for political analysts, concerned citizens,
Monitor political developments and stay informed (implied)
Engage in critical thinking and analysis of media narratives (implied)
Participate in local politics to influence change (implied)
</details>
<details>
<summary>
2021-10-06: Let's talk about the New York police union and narratives.... (<a href="https://youtube.com/watch?v=gkTTnUU_lMQ">watch</a> || <a href="/videos/2021/10/06/Lets_talk_about_the_New_York_police_union_and_narratives">transcript &amp; editable summary</a>)

Beau explains how the right-wing machine shapes narratives before facts are known, using the example of search warrants served in New York, criticizing the premature generation of stories without concrete evidence.

</summary>

"This is how these wild narratives that the right-wing media in the U.S. likes to latch onto, it's how they start, it's how they're created, and it's how they distract from actual events."
"We don't know what it's about yet. There haven't been any real official statements on it at time of filming."
"It's the narrative that's already taking shape. And it's all without evidence."

### AI summary (High error rate! Edit errors on video page)

Explains how the right-wing machine shapes public perception before facts are known.
Mentions FBI serving warrants on the New York Police's Sergeants Benevolent Association and union president's home.
Notes the limited information available, with search warrants being served and involvement of Office of Public Corruption.
Quotes the union's statement indicating the union president may be the target of the federal investigation.
Observes the trending of the story on Twitter and the generation of a narrative by talking heads.
Points out that the focus shifted to the union president's support for Trump and past inflammatory tweets.
Criticizes the creation of a narrative without evidence to suggest political payback.
Comments on mainstream outlets picking up the narrative created by right-wing media.
Expresses skepticism towards the speculation around the search warrants without official statements.
Concludes by reflecting on the narrative already established without concrete evidence.

Actions:

for activists, media consumers,
Fact-check narratives and wait for official statements (implied)
</details>
<details>
<summary>
2021-10-06: Let's talk about Idaho and republican divisions.... (<a href="https://youtube.com/watch?v=yIDdLmQfCgs">watch</a> || <a href="/videos/2021/10/06/Lets_talk_about_Idaho_and_republican_divisions">transcript &amp; editable summary</a>)

Idaho's Republican Party grapples with disunity as power struggles between Governor and Lieutenant Governor play out, prompting questions on authoritarian tendencies and the need for party reform.

</summary>

"If somebody is so overcome with the power granted by the absence of the governor that they begin issuing executive orders, this might not be somebody fit for the governor's office."
"This display of a power grab should weigh pretty heavily on you."
"They're taking shots at each other pretty regularly."
"If there's a time for those people who are just conservatives to take a stand and say, we don't want to have anything to do with this, it's this moment."
"Their party will be overrun with those who are into political grandstanding, those who are in for attaining power for power's sake, and then using it."

### AI summary (High error rate! Edit errors on video page)

Idaho's Republican Party is facing disunity between mainstream conservatives and the far right with authoritarian tendencies.
Governor Little and Lieutenant Governor are not political allies in Idaho.
Lieutenant Governor issued an executive order banning vaccine mandates in Governor Little's absence.
The Lieutenant Governor also tried to mobilize the National Guard without proper authority.
Major General Garshak clarified the National Guard's role and denied the request.
Governor Little plans to undo the orders issued by the Lieutenant Governor upon his return.
This isn't the first time the Lieutenant Governor has acted independently; she previously issued a ban on mask mandates.
Beau questions the Lieutenant Governor's fitness for office if she continues to make power grabs.
The discord within the Republican Party in Idaho is becoming more evident.
Beau urges Republicans to stand against authoritarianism within their party to protect democracy.

Actions:

for republicans, idaho citizens,
Support candidates who prioritize democracy over power grabs (implied)
Advocate for unity within the Republican Party to combat authoritarian influences (implied)
</details>
<details>
<summary>
2021-10-05: Let's talk about fact checking the Battle of Hayes Pond ad..... (<a href="https://youtube.com/watch?v=0njIHlsQWCQ">watch</a> || <a href="/videos/2021/10/05/Lets_talk_about_fact_checking_the_Battle_of_Hayes_Pond_ad">transcript &amp; editable summary</a>)

A political ad referencing the Battle of Hayes Pond in the 1950s sheds light on Klan defeat, prompting questions about its relevance in modern American politics.

</summary>

"What does it say that an account of the Klan being defeated is still a good political ad today."
"The defeat at Hayes Pond drew significant attention."
"It led to the governor denouncing the clan."
"The accuracy of the ad regarding the Battle of Hayes Pond remains largely undisputed."
"I don't see that as being very material to anything."

### AI summary (High error rate! Edit errors on video page)

A candidate in North Carolina, Charles Graham, put out an ad referencing an event from the 1950s, leading to viral attention and questions about its accuracy.
The event in question, the Battle of Hayes Pond, involved a confrontation between James "Catfish" Cole, a Klan leader, and the Lumbee tribe in 1958.
Cole, a WWII veteran and Klan leader, organized motorcades with the police, causing fear and chaos in neighborhoods.
On the opposing side was Robert Williams, supported by the NAACP, who formed the Black Armed Guard for community defense.
In October 1957, Cole's motorcade targeted Dr. Perry's house but was met with armed resistance from the Black Armed Guard.
Cole then shifted his focus to the Lumbee tribe, despite warnings from local police about potential consequences.
The Lumbee tribe planned a strategic ambush near Hayes Pond, surprising and defeating Cole's group.
The defeat at Hayes Pond drew significant attention, leading to the denouncement of the Klan by the governor and Cole's arrest.
The events at Hayes Pond and the defeat of the Klan resulted in a decline in Klan activities in the area.
Despite some minor disputes about details, the accuracy of the ad regarding the Battle of Hayes Pond remains largely undisputed.

Actions:

for history buffs, activists,
Support community defense groups (implied)
Advocate for historical events that showcase resistance against hate groups (implied)
</details>
<details>
<summary>
2021-10-05: Let's talk about a possible Hollywood strike.... (<a href="https://youtube.com/watch?v=iahQ0JHD2Qw">watch</a> || <a href="/videos/2021/10/05/Lets_talk_about_a_possible_Hollywood_strike">transcript &amp; editable summary</a>)

Hollywood unions vote overwhelmingly to authorize a strike for better conditions and benefits, signaling a warning to the industry to negotiate fairly and provide workers with their fair share.

</summary>

"Delays in entertainment could occur due to the strike, impacting viewers but aiming to secure healthcare and retirement for workers."
"Workers behind entertainment projects seek a fair share of the profits, not just crumbs."

### AI summary (High error rate! Edit errors on video page)

Hollywood unions, with 60,000 members, voted 98% in favor of authorizing a strike for better working conditions and benefits.
This strike vote is a warning to the industry to negotiate in good faith rather than an immediate action.
The demands include improved working conditions, larger contributions to pensions and health plans, better rest periods, mill breaks, and a larger share from streaming productions.
Despite common misconceptions about unions always striking, this specific union has not had a nationwide strike in 128 years, indicating serious grievances.
The resolution of the members and the high voter turnout suggest skepticism that the industry will negotiate fairly.
Workers behind entertainment projects seek a fair share of the profits, not just crumbs.
Delays in entertainment could occur due to the strike, impacting viewers but aiming to secure healthcare and retirement for workers.
Collective bargaining with industries holding substantial resources can be challenging as they can withstand strikes by hiring replacements.
Support for the Union from the public could be vital in ensuring a fair negotiation process.
The strike vote represents a fundamental desire for workers to receive their fair share rather than being overlooked.

Actions:

for entertainment industry workers,
Support the Union during negotiations (implied)
Stay informed about the labor rights issues in the entertainment industry (implied)
</details>
<details>
<summary>
2021-10-04: Let's talk about the difference between the parties and Trump's legacy.... (<a href="https://youtube.com/watch?v=-ebDERX32Tk">watch</a> || <a href="/videos/2021/10/04/Lets_talk_about_the_difference_between_the_parties_and_Trump_s_legacy">transcript &amp; editable summary</a>)

Contrasting the Democratic coalition with traditionally conservative Republicans, Beau predicts that Trump's divisive actions may force the Republican Party to develop concrete policy instead of relying on nostalgia.

</summary>

"It's a coalition. What's the Republican Party? Conservatives, right?"
"All Republican candidates have had to do in the past is harken back to the days of leave it to Beaver and Andy Griffith."
"He didn't abide by the 11th Commandment."
"He'll be that afraid. Pushing that division within the party, it's going to force them to develop policy."
"A man who truly cared about his legacy, wanted his name on everything, his legacy will end up being the destruction of the Republican Party's greatest asset."

### AI summary (High error rate! Edit errors on video page)

Contrasts the Democratic Party as a coalition of diverse groups with the Republican Party as traditionally conservative.
Democrats comprise liberals, moderates, leftists, environmentalists, etc., competing for policy influence within the coalition.
Republicans are predominantly conservatives who focus on tradition and the idea of reverting to the past.
Republican branding often relies on nostalgia and a desire to recreate past eras like "leave it to Beaver" or "Andy Griffith."
Historically, Republicans avoided attacking each other and instead focused on attacking Democrats, relying on the "11th Commandment" to maintain unity.
Trump broke this unity by not following the "11th Commandment" and attacking fellow Republicans, leading to internal divisions.
The Republican Party's platform is summarized as pro-second amendment, against family planning, and against anything perceived as socialist.
Beau suggests that without Trump's ability to rely solely on nostalgia and fear, Republicans will be forced to develop actual policy.
Trump's divisive actions, like criticizing DeSantis, are pushing the Republican Party towards developing clear policy positions.
Beau concludes that Trump's legacy may be the destruction of the Republican Party's strength in avoiding the need for substantial policy.

Actions:

for political observers,
Analyze and understand the policy positions of political parties (implied)
Stay informed about internal divisions and policy developments within political parties (implied)
Encourage political engagement and critical thinking about party platforms (implied)
</details>
<details>
<summary>
2021-10-04: Let's talk about context, sources, and links.... (<a href="https://youtube.com/watch?v=VVagD3FVPBg">watch</a> || <a href="/videos/2021/10/04/Lets_talk_about_context_sources_and_links">transcript &amp; editable summary</a>)

Beau addresses criticism about sourcing, stressing the importance of context and caution with provided links, discussing a video by Dr. Kennard and differing views on ensuring accurate information.

</summary>

"If you really want to understand something you can't just follow a link that the person who makes a claim provides you."
"The reporting lines up with the anecdote and the town gossip."
"Just because there's a link down below doesn't mean the information is true."
"There's too much money in misinformation."
"Nobody's right in this case. It's, well, it's just a thought."

### AI summary (High error rate! Edit errors on video page)

Responds to criticism about not providing sources or links below videos.
Believes in viewers' ability to fact-check using search engines.
Acknowledges the importance of context in understanding information.
Points out the dangers of falling into information silos created by misleading links.
Mentions a video by Dr. Kennard critiquing Beau's content on cops resigning in Massachusetts.
Talks about the importance of verifying information and looking at multiple sources.
Mentions a story about a Florida deputy planting evidence and getting caught.
Comments on the unreliability of sourcing alone.
Recognizes Dr. Kennard's effort to ensure accurate information for viewers.
Expresses different perspectives on linking sources in videos and the risks of misinformation.

Actions:

for content creators, information consumers,
Fact-check information from online sources (implied)
Verify information by looking at multiple sources (implied)
Be cautious with provided links and do further research (implied)
</details>
<details>
<summary>
2021-10-03: Let's talk about rural America and public health.... (<a href="https://youtube.com/watch?v=wJWz42aLgbM">watch</a> || <a href="/videos/2021/10/03/Lets_talk_about_rural_America_and_public_health">transcript &amp; editable summary</a>)

Rural areas face twice the impact of the current health issue as cities due to close community gatherings and limited medical access; preventive measures are vital.

</summary>

"Don't disregard this information. Wash your hands. Stay at home as much as you can."
"Even though it seems counterintuitive because we all are so far apart, we all go to the same spots so it can transmit there."
"Rural areas are being lost to this twice as fast as those in cities."

### AI summary (High error rate! Edit errors on video page)

Rural areas are experiencing the current public health issue at twice the rate of those in cities, which initially seems counterintuitive.
People in rural areas are questioning how the issue can spread when they are so far apart from each other.
Rural communities gather in the same locations like churches, gas stations, and food counters, making it easy for the issue to spread.
Lack of access to medical care due to the spread-out nature of rural areas is a contributing factor to the rapid spread.
The same things that initially protected rural areas are now causing issues and aiding in the spread of the current health issue.
Once the issue gets into a rural community, it spreads rapidly due to the close gatherings at common spots.
The lack of medical resources in rural areas exacerbates the impact of the current health issue.
Beau urges people to take the information seriously, despite it seeming counterintuitive due to the rural distance.
Simple preventive measures like handwashing, staying at home, wearing masks, and getting vaccinated are emphasized as effective ways to combat the issue.
Rural areas need to acknowledge and address the higher rate of impact they are facing compared to urban areas.

Actions:

for rural communities,
Wash your hands regularly, avoid unnecessary outings, wear masks, and prioritize getting vaccinated (exemplified).
Encourage community members to adhere to preventive measures and prioritize their health (implied).
</details>
<details>
<summary>
2021-10-02: Let's talk about the historic SNAP increase.... (<a href="https://youtube.com/watch?v=A2OktXnNyoQ">watch</a> || <a href="/videos/2021/10/02/Lets_talk_about_the_historic_SNAP_increase">transcript &amp; editable summary</a>)

Historic increase in SNAP benefits falls short as 42 million Americans struggle with basic necessities on inadequate assistance.

</summary>

"You end up getting the cheapest food available. You end up buying junk food."
"42 million Americans are beneficiaries of the program."
"It's going to be harder than you think."
"Sure, it's a move in the right direction. Yeah, we can get behind that."
"Designate an area in your pantry, stock that area, and you can only eat from there."

### AI summary (High error rate! Edit errors on video page)

Historic SNAP increase in the United States, largest in history.
25% increase replacing temporary 15% increase due to public health issue.
Increase amounts to about $36 per person per month, totaling $157 per month.
$5.23 a day for food isn't enough.
Low budget leads to buying cheap junk food.
Misconceptions about SNAP recipients spending on junk food due to limited budget.
42 million Americans benefit from SNAP, more than 10% of the population.
Many who need assistance don't qualify for SNAP.
Shift in the U.S. from promises of prosperity to struggling with basic necessities.
$157 increase is a step in the right direction but insufficient.
Minimum wage increases often become obsolete by the time they take effect.
Challenging others to live on $157 per month to understand the struggle.

Actions:

for advocates for social welfare,
Stock a designated pantry area and eat only from there to experience living on a limited budget (suggested)
</details>
<details>
<summary>
2021-10-02: Let's talk about Biden's per mile tax and "not real news".... (<a href="https://youtube.com/watch?v=Opxp3u1SGIQ">watch</a> || <a href="/videos/2021/10/02/Lets_talk_about_Biden_s_per_mile_tax_and_not_real_news">transcript &amp; editable summary</a>)

Beau debunks the misinformation around an 8-cent per mile fee in the infrastructure bill, clarifying the actual contents and suggesting a more feasible solution while recommending the AP's "Not Real News" series for debunking false information.

</summary>

"If your favorite source of information has been railing about this the last few days, maybe you want a different source of information."
"It's pretty common. So the fear mongering over it seems a little overdone."
"I want to take this moment to point to something."
"I wouldn't worry about an 8 cent per mile gas or 8 cent per mile fee. That's not a thing."
"Y'all have a good day."

### AI summary (High error rate! Edit errors on video page)

Debunks the misinformation about an 8-cent per mile fee in the infrastructure bill.
Reveals that the misinformation was spread by a former Trump speechwriter.
Explains that the bill actually includes a study on the feasibility of funding highways through a usage fee, not an 8-cent tax per mile.
Points out that implementing a tax on gas usage is a more feasible method to fund highways and infrastructure.
Mentions the existing gas taxes in Florida, proving that the fear over new taxes is unfounded.
Notes that many people were unaware of the current gas taxes in Florida.
Calls out the fear-mongering and unnecessary debate surrounding the non-existent 8-cent per mile fee.
Recommends the AP's "Not Real News" series as a resource to debunk misinformation and understand how false stories develop.
Suggests that higher gas taxes may be implemented in the future to support infrastructure and incentivize a switch to electric vehicles.
Encourages readers to stay informed and not worry about the misrepresented 8-cent per mile fee.

Actions:

for fact-checkers, news consumers,
Read the AP's "Not Real News" series weekly to spot and debunk misinformation (suggested).
Stay informed about legislative proposals and separate fact from fiction to make informed decisions (exemplified).
</details>
<details>
<summary>
2021-10-01: Let's talk about Biden's surprising immigration announcement.... (<a href="https://youtube.com/watch?v=J3MvLKtaMas">watch</a> || <a href="/videos/2021/10/01/Lets_talk_about_Biden_s_surprising_immigration_announcement">transcript &amp; editable summary</a>)

Beau describes a shift in U.S. deportation policy, prioritizing certain groups and defending undocumented community members against political attacks.

</summary>

"This is for those people who are now valued members of our communities."
"It prioritizes DHS's resources and puts them where they matter."
"It's up to them to make this case and say they have been here, that they are part of our communities and defend this decision."

### AI summary (High error rate! Edit errors on video page)

Describes a shift in U.S. deportation policy prioritizing certain groups post November 2020.
Acknowledges 11 million undocumented individuals as contributing members of communities.
Allows DHS to focus on those actively involved in criminal activity or with serious criminal histories.
Recognizes the broken immigration system and the temporary nature of the current policy.
Predicts the Republican Party will use this shift as a talking point, claiming it's akin to open borders.
Emphasizes that the policy targets individuals already in the U.S., not incentivizing new arrivals.
Anticipates court cases but doubts their success due to the legality of establishing enforcement priorities.
Views the policy as a positive indication of potential immigration reform efforts by the Biden administration.
Expects intense political debate leading up to the midterms due to the shift benefiting the Republican Party.
Urges individuals to defend the decision and stand up for undocumented community members.

Actions:

for advocates for undocumented immigrants.,
Defend undocumented community members (implied)
Make the case for their contribution to communities (implied)
</details>
<details>
<summary>
2021-10-01: Let's talk about Biden's 4D chess foreign policy.... (<a href="https://youtube.com/watch?v=dckdj_3vl5g">watch</a> || <a href="/videos/2021/10/01/Lets_talk_about_Biden_s_4D_chess_foreign_policy">transcript &amp; editable summary</a>)

President Biden aimed to establish three poles of power in the Middle East through American foreign policy, but Iran's independent rise as a regional player challenges traditional strategies, signaling a potential shift in regional dynamics.

</summary>

"Foreign policy is an international poker game where everybody's cheating."
"It wasn't because the Biden administration was playing 4-D chess. They lucked into it."
"You can't credit Biden with it. He tried. It didn't work."

### AI summary (High error rate! Edit errors on video page)

President Biden aimed to establish three poles of power in the Middle East to deprioritize the region and leave.
American foreign policy did not successfully pull off the nuclear deal with Iran, but Iran is emerging as a significant regional player.
Biden's team did not achieve their goal of bringing Iran out under U.S. auspices, as Iran is filling a power vacuum independently.
While Iran's rise may stabilize the Middle East, it cuts out U.S. interests, which might not matter to most people who prioritize human life.
Biden's administration did not orchestrate Iran's emergence through 4-D chess; it was more of a stroke of luck.
If history books in the future hail Biden as a genius for this move, it will still be attributed to luck at this moment.
The potential establishment of Iran as a more traditional and regional player could benefit the international community, Iranian people, and the Middle East.
Biden's team's plan did not lead to Iran's current actions, but it might have nudged them towards the possibility.
While not a clear win for Biden, Iran's emerging role is seen as a positive development that could lead to peace in the region.

Actions:

for policy analysts, diplomats,
Monitor the evolving dynamics in Middle Eastern geopolitics (implied).
</details>
