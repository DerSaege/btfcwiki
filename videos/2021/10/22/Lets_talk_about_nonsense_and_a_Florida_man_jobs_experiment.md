---
title: Let's talk about nonsense and a Florida man jobs experiment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=3-hGiEMEVhk) |
| Published | 2021/10/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Joey Holtz conducted an experiment in response to his boss's rant about lazy employees quitting due to $1,200 checks.
- Joey applied for 60 jobs in September, receiving nine email responses, one phone call, and only one job interview.
- The job interview offered was for a construction laborer position in Florida paying $8.65 an hour.
- Employers expecting people to work for such low wages is astounding, especially in a low cost of living area.
- This experiment challenges the narrative of a labor shortage and lazy employees, suggesting employers may be the lazy ones.
- Some employers might prioritize social media rants over actual hiring efforts.
- Joey targeted companies where bosses publicly denigrated employees on social media, raising concerns for potential applicants.
- Employers seem entitled to a cheap labor pool, unwilling to put in the effort to attract and retain employees.
- Some businesses are not hiring because they took care of their employees during the public health crisis.
- Joey's experiment sheds light on the disconnect between employers' expectations and the reality of the job market.

### Quotes

- "Employers are so entitled that they expect people to work in Florida doing construction laboring for $8.65 an hour."
- "It kind of casts doubt on the narrative that there's a labor shortage, that employees are lazy."
- "Employers have become so entitled to having a ready labor pool who will work for apparently $8.65 an hour."
- "It's what it sounds like. It's what it seems like."
- "Joey's experiment is extended. Anyway, it's just a thought."

### Oneliner

Joey's job application experiment challenges the narrative of a labor shortage and lazy employees, revealing employer entitlement to cheap labor.

### Audience

Potential job seekers

### On-the-ground actions from transcript

- Apply for jobs that value your worth and offer fair compensation (implied).
- Advocate for fair wages and respectful treatment in the workplace (implied).
- Support businesses that prioritize employee well-being and fair pay (implied).

### Whats missing in summary

The full transcript provides deeper insights into the dynamics between job seekers and employers in the current labor market.

### Tags

#LaborMarket #JobSeekers #Employers #Wages #Employment


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are going to talk about a world of nonsense.
A world where nothing would be what it is
because everything would be what it isn't.
And an experiment conducted by a Florida man.
So, a fellow named Joey Holtz, that's H-O-L-Z,
if you want to look this up,
well, he's at a medical facility.
And here's the boss ranting about how,
well, employees are just lazy.
They don't want to work anymore.
They all quit, found other jobs,
and it's because they got these $1,200 checks.
That didn't sit right with Joey.
Joey didn't buy that for a second.
So Joey decided to conduct a little experiment.
Started applying for jobs,
two a day through the month of September,
so roughly 60 applications.
Kept track of them through a spreadsheet.
The criteria for the jobs he was applying for,
pretty simple, had to be qualified for them.
They had to be entry-level positions,
meaning less than $12 an hour.
I think $12 an hour was the highest pay.
And specifically went out of his way to apply to companies
where the boss was on social media
whining about not being able to find employees.
What did the spreadsheet reveal?
Applications done, nine email responses,
one phone call, and one job interview.
One job interview.
And it certainly sounds like that interview
was for a construction laborer position
making $8.65 an hour in Florida.
To me, that is the most wild part
about this whole wild story.
Joey lives down near Fort Myers.
I happen to live in one of the lowest cost
of living areas in the state.
And there is no way you're going to be able
to find a construction laborer around me
for $8.65 an hour.
That is wild.
The idea that employers are so entitled
that they expect people to work in Florida
doing construction laboring for $8.65 an hour.
That's beyond me.
But with all of this information,
it does kind of indicate some things.
Yeah, it's not incredibly scientific,
but I'm willing to bet this could be replicated.
It kind of casts doubt on the narrative
that there's a labor shortage,
that employees are lazy.
Really sounds like employers are lazy.
Like they're too busy whining and ranting
and being entitled on social media
to attend to the necessary human resources
functions of their company.
It's what it sounds like.
It's what it seems like.
That or they're not actually hiring anybody
and they're just on social media whining,
attempting to get social cred.
Which I mean, that may be.
I don't think that's it, but it might be.
I mean, I have friends that own businesses.
None of them are hiring.
None of them are hiring,
but they're a little different.
The reason they're not hiring
is because they have the same employees
that they had prior to the whole public health thing started
because they took care of them during that period.
Another interesting aspect of this,
and this is a word of caution to potential employers,
is that Joey was able to target his applications
to reach those who were vocal and whining on social media.
Chose to apply with companies
where the boss was out there
calling employees lazy garbage people.
I wonder how many people choose not to apply
with companies like that.
I mean, the assumption among employees might be
that if somebody was going to publicly bash
and denigrate employees,
that they're probably not a good person to work for.
I mean, that could totally be happening.
At the end of this, what we see is a stark reality
that employers have become so entitled
to having a ready labor pool
who will work for apparently $8.65 an hour
that if they have to do any work,
well, they're just going to whine and complain,
not actually going to pick themselves up by their bootstraps.
I hope Joey continues this important work
because I would love to know what continues to happen
if this little experiment is extended.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}