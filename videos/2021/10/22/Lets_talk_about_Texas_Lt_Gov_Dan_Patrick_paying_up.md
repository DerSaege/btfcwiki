---
title: Let's talk about Texas Lt. Gov Dan Patrick paying up....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=8nQ0UrXMpp0) |
| Published | 2021/10/22|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about Texas Lieutenant Governor Dan Patrick and his campaign focused on election security in the United States.
- Dan Patrick was vocal about concerns regarding election security and echoed former President Trump's claims about widespread issues with elections.
- Patrick took a million dollars from his campaign fund to pay for tips about election issues.
- Despite the fund being around for almost a year, only one payment of $25,000 has been made.
- The payment was made to a progressive Democrat poll watcher who reported a Republican for voting twice.
- This incident shows that US elections are pretty secure, as the fund has not uncovered widespread issues.
- Beau suggests that the fund was more of a political stunt rather than a genuine attempt to uncover voter fraud.
- The real election integrity issues in the US stem from gerrymandering and voter suppression, not widespread voter fraud.
- Beau concludes that the million-dollar fund was likely a manipulation tactic to create a false sense of seriousness.

### Quotes

- "US elections, they're pretty secure."
- "There is no widespread voter fraud. It's not a thing."
- "It was a political stunt."
- "The election integrity issues we have in the United States come from gerrymandering."
- "All this time later, the only payout has gone to an isolated incident that was actually their side."

### Oneliner

Beau examines Texas Lieutenant Governor Dan Patrick's million-dollar campaign fund and its implications on election security, revealing it as a political stunt to manipulate public perception.

### Audience

Voters, election monitors

### On-the-ground actions from transcript

- Monitor election processes for signs of gerrymandering and voter suppression (implied)
- Support efforts to increase voter turnout and combat voter suppression (implied)
- Stay informed about election integrity issues and advocate for fair practices (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the political tactics used in the name of election security, debunking claims of widespread voter fraud and redirecting focus to actual election integrity concerns in the US.

### Tags

#ElectionSecurity #VoterFraud #PoliticalStunt #Gerrymandering #VoterSuppression


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today we're going to talk about
Texas Lieutenant Governor Dan Patrick
and his steadfast resolve to his cause.
And we're going to talk about his campaign
that was geared to make sure that things in the United States
go the way we expect them to go.
He has been very vocal in his concerns
about the security of the elections in the United States.
He's a big vocal supporter of the former president,
and he echoed a lot of Trump's claims
about widespread issues with our elections.
To show how serious he was about this,
about a year ago, a little less than a year ago,
he took a million dollars from his campaign fund
and set it aside to pay for tips
about issues with the elections.
He wanted to motivate people to come forward
and point out those who were being dishonest,
who were breaking the law when it comes to our elections.
It's been almost a year.
They have made one payment.
The first payout just occurred, $25,000.
So you know he's serious about uncovering
this widespread issue of which they've been able to find one
in all this time.
Interestingly enough, it was paid to a progressive Democrat
poll watcher who turned in a Republican for voting twice.
So that happened.
It is funny to know that certainly some of that money
is going to go to fund Democratic candidates.
But what does this mean?
At the end of this, what does all of this mean?
Does it mean that Republicans are out there doing
what they accuse Democrats of, which is often the case?
No, no.
The reality is this demonstrates pretty clearly
that US elections, they're pretty secure.
They are pretty secure.
If there is one thing that will motivate the American populace,
it's a million dollars.
I mean, a giant fund designed to pay out cash for tips.
It is something that works in the US.
It motivates people.
Those kind of political bounties, they work.
They drive people to find their better judgment, I guess.
It provides that financial motivation.
And in all of this time since this fund has been around,
it has made one payout.
It doesn't mean that Republicans are actually out there doing it.
It means that it's not really happening very often.
It doesn't really occur.
They made it up.
This, realistically, it was a political stunt
in hopes of finding a single Democrat who voted illegally.
That's what it was.
There was no reason to have a million dollar fund.
He certainly knew that that money was never
going to be paid out because it's not that widespread.
It was a political stunt.
It was a way to manipulate those people who follow them.
It was a way to say, oh, you know, it's got to be serious
because they set aside a million dollars to pay out for tips.
All this time later, the only payout
has gone to an isolated incident that was actually their side.
I mean, yeah.
It is poetic justice.
We don't have election integrity problems in the United States
when it comes from the voters.
The election integrity issues that we have in the United States
come from gerrymandering.
They come from politicians doing everything
they can to decrease voter turnout of their opposition,
to engage in voter suppression.
Those are the election issues we have in the United States,
not voter fraud.
There is no widespread voter fraud.
It's not a thing.
There was a million dollars on the line,
available to anybody who could prove it existed.
Nobody could.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}