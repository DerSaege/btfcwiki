---
title: Let's talk about National Police Week....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=9LiZuivD_yg) |
| Published | 2021/10/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- National Police Week prompts reflection on the challenges faced by officers and deputies.
- Calls to action during this time are common when officers face significant risks.
- In 2020, 264 officers were lost, with 145 of them due to COVID, the leading cause again in the current year.
- Despite the high numbers, there's a lack of outcry, especially from those who claim to support law and order.
- Some governors prevent law enforcement leaders from mandating COVID precautions, unlike other safety measures like wearing a vest.
- The "back the blue" sentiment may be more of a slogan than genuine support, as actions don't always match the rhetoric.
- The loyalty test around COVID precautions has led to preventable deaths among officers.
- Politicians and governors who back law and order may not fully support law enforcement if they don't prioritize officer safety.
- The disconnect between rhetoric and action raises questions about the true intentions behind supporting law enforcement.
- Lack of support for necessary precautions may undermine the image of those in power as protectors of citizens.

### Quotes

- "Maybe 'back the blue' isn't quite what they really mean."
- "They certainly don't seem to support law enforcement."
- "The loyalty test around COVID precautions has led to preventable deaths among officers."
- "Despite the high numbers, there's a lack of outcry."
- "Calls to action during this time are common when officers face significant risks."

### Oneliner

National Police Week prompts reflection on officer challenges and the disconnect between rhetoric and action in supporting law enforcement.

### Audience

Law enforcement advocates

### On-the-ground actions from transcript

- Contact local law enforcement agencies to inquire about COVID precaution mandates (suggested)
- Advocate for necessary safety measures within law enforcement agencies (implied)

### Whats missing in summary

The emotional impact of the disconnect between expressed support for law enforcement and the lack of action to protect officers. 

### Tags

#NationalPoliceWeek #LawEnforcement #BackTheBlue #COVIDPrecautions #SupportOfficers


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about National Police Week.
If you don't know, that started today.
And during this time, people reflect on the numbers.
And they talk about the things that officers and deputies
face on a daily basis.
And they try to come to terms with it.
And this is normally a time when there are calls to action,
And when there is something that is posing a significant risk to the people who wear
a badge in this country, normally there's an outcry saying, hey, we've got to do something
about this.
You have people with those flags, have the little thin blue line down the middle of them,
and they want to back the blue.
I don't know where that's at though right now because the numbers, they're pretty staggering.
You know, last year, 264 cops were lost, 264, and 145 of them come from the same culprit,
the same thing spreading across this country, and they're not doing anything about it.
Now, under normal circumstances, you would have the unions, you'd have people out there
screaming.
More than half of our officers have been lost to this.
We need to have a war on whatever it is, right?
We got to find these entities and eradicate them, get rid of them.
They're bad for society.
The thing is, that was 2020, 264 loss, 145 from one thing.
That thing is the leading cause again so far this year, but there's no outcry.
The law and order party is nowhere to be found.
In fact, they seem to be siding with the opposition.
It's COVID.
COVID is the leading cause of officers being lost.
And governors who like to fashion themselves as law and order governors who back the blue
are doing everything within their power to make sure that the top law enforcement officer
in a jurisdiction cannot require their their officers, their deputies, to take
the precautions necessary. If it was gunfire, I don't think anybody would
object to a chief of police or a sheriff mandating that you have to wear a vest.
I'm pretty sure that in pretty much every department, even the little ones
around here, it's a requirement that you wear your vest at all times.
preventative measure. But not with this, right? This is different because it
became a loyalty test. Became a loyalty test and they pushed the idea of not
allowing mandates, which is fine, but they did it to such an extreme in such a way
that it undercut efforts to get people vaccinated. Led to 145 flags being in the
or being handed to somebody's family. Maybe back the blue isn't quite what they
really mean. Maybe it's just a slogan. Maybe it's just something that they say
to evoke an emotional response in people and make them think that those
politicians, those law and order governors, those are the only thing, the
only people standing between the average citizen and chaos. In fact, it kind of
feels like it's the only thing it could be because they certainly don't seem to
support law enforcement. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}