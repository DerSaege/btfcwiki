---
title: Let's talk about Biden's poll numbers....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=XXLFyvQS3Os) |
| Published | 2021/10/13|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration's poll numbers are shaky, especially among Hispanics, Blacks, and women, key demographics for winning elections.
- Reasons for Biden's falling numbers include stalled legislative agenda, lack of engagement with key groups, and fear of backlash for policies benefiting these groups.
- Democrats lack vocal supporters like the Republicans have in new and social media, leading to missed opportunities in promoting policy successes.
- Attempting to maintain the coalition by appealing to the center won't work as progressives are constantly moving forward, not back to the center.
- Biden administration needs to shift the center to progressive territory to maintain the broad coalition that got them elected.
- Lack of progressive cheerleaders in media is a problem for Democrats as most platforms tend to be more progressive than the party itself.
- Democrats need to be stronger in making their case, educating the public, and addressing Republican opposition more assertively.
- Focusing on being "not Trump" is no longer a winning strategy now that Biden is in office; policy matters more than just anti-Trump sentiment.
- Failure to shift policies towards progressive positions and make a strong case could lead to the Biden administration being one-term.

### Quotes

- "It's not about 'I'm not Trump.' It's about policy."
- "The position that a progressive held after voting begrudgingly for Biden is already in their rear view."
- "Most Democratic politicians are center-right. The market decided."

### Oneliner

Biden administration must shift policies leftward to maintain coalition and avoid one-term fate.

### Audience

Political strategists, Democratic Party members.

### On-the-ground actions from transcript

- Educate and mobilize communities about the benefits of the infrastructure package and other policies. (implied)
- Advocate for shifting policies towards progressive positions within the Democratic Party. (implied)
- Engage with media platforms to amplify progressive voices and ideas. (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the Biden administration's challenges in maintaining support among key demographics and the necessity of a shift towards more progressive policies to secure future elections.

### Tags

#BidenAdministration #DemocraticParty #ProgressivePolicies #PoliticalStrategy #ElectionCampaigns


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about the Biden administration
and their poll numbers
and the discussion surrounding the poll numbers
and the Democratic Party's strategy
for raising those numbers and what I think they're missing
and why I believe they are in the process
of making a critical error
that is going to cost them in the next election.
If you don't know, Biden's numbers are,
well, let's just call them shaky, to put it mildly, all right?
The polls say he's falling in three key demographics.
Now, I just want to stop for a second and say
that I don't like dividing up the polls this way.
I don't think it's a good idea, generally speaking,
and this isn't me collectivizing in this manner.
It's the way the polls are done.
He's falling in three demographics
that are required to win.
He cannot win without them, Hispanics, blacks, and women,
okay, which if you really read into it,
it pretty much suggests that the only place
he's holding steady is with white guys,
people who are generally more supportive of the status quo
because, well, we benefit from it, right?
Okay.
So why is he falling in these three groups?
There's two, three reasons, really.
One, it's not really fair,
but it's politics and politics isn't fair,
and that's that his legislative agenda has stalled, right?
It's held up in Congress.
Now, really, that's Congress's fault, not Biden's,
but Biden's the head of the party,
so the buck stops with him, okay?
That's one reason.
Another reason is that he's not,
they aren't doing everything that they can for these groups.
They're not bringing them into the conversation.
They're not really addressing their needs
in every single way that they could.
That's one reason.
Another is that when they do engage in things
that benefit these groups,
they don't advertise it out of fear
because they don't have cheerleaders.
The Republican Party has cheerleaders in the,
for lack of a better term, new media, social media, right?
They have cheerleaders who are gonna back the Republican Party
no matter what they do
and oppose the Democratic Party no matter what they do.
The Democratic Party doesn't have that, not really,
so they're afraid of the backlash.
As an example of them not broadcasting something
that I believe they should,
do you know that the Biden administration
just shifted the policy
when it comes to workplace raids for undocumented workers?
They're not gonna happen anymore.
You probably didn't know that
because, yeah, they put out a press release,
AP put an article out about it,
but they didn't go out there and champion that.
They didn't advertise that they did that
because they're worried about the backlash
because they don't have those cheerleaders.
And it goes to the strategy that they're trying to employ
and one that I am pretty certain will fail.
They're trying to maintain the coalition
that got them elected.
The reality is what got them elected was,
I'm not Trump.
That's it.
That's what got them elected.
That's why you had the broad coalition.
A whole lot of people could get behind literally anybody
other than Trump.
But see, now Biden's in power.
He's in office.
It's not about I'm not Trump.
It's about policy.
And the policies are geared in a way
that are trying to bring progressives to center.
That will fail, guaranteed.
Progressives aren't coming back.
The position that a progressive held
after voting begrudgingly for Biden,
the position that they had when he got inaugurated
is already in their rear view
because they're constantly moving forward.
You're never bringing them back to center.
It's not happening.
If the Biden administration wants
an FDR-style administration,
they want an administration that is going to make
that kind of impact,
they have to take center
and move it to the progressive position.
They have to get out there and make the case.
They have to shift thought.
It's harder for them
because they don't have those cheerleaders.
Why?
Because most people with a platform in the new media, right?
Now, you can talk about the cheerleaders on CNN
or MSNBC or whatever,
but that's older media, cable news, right?
That's not where most of the Democratic base
gets their information.
Look where you're watching this right now, right?
They don't have the cheerleaders that they need.
They don't have people that will back them no matter what.
Why?
That's the question the Democratic Party
needs to be asking.
Why don't those cheerleaders exist?
Why is it that everybody who gets a platform
in the new media is far more progressive than they are?
Since they're center-right,
I'll put it in language they'll understand.
Most Democratic politicians are center-right.
The market decided.
In the literal marketplace of ideas,
where ideas get monetized,
the progressive positions are the popular ones.
Those are the people who get platforms.
Those are the people who can help the Democratic Party
if they chose to.
The problem is when you're talking about progressive people,
they're philosophically or ideologically motivated.
They're not going to defend the status quo.
Even if it's an incremental change
in the direction they want,
most times you're like this isn't enough
because most people don't like
supporting incremental change.
It's not marketable.
If an idea is marketable in the new media,
if it is something that can be monetized,
it is something that will get votes.
They're trying to bring progressives to center
when they should be taking the center
and trying to move it to progressive territory.
That's how they can maintain that coalition,
the one that got them elected,
which means they'll end up in the process
doing things that will benefit those three key demographics
that kind of feel left out.
It is so rare to see commentators,
pundits, talking heads like myself in new media
defend Biden policies that in my comment section,
you will find people saying,
well, you're a Biden shill.
My public comments on Biden are that he's not my man.
He represents the status quo.
He doesn't represent your interests.
I've never been a huge fan of Biden,
but because I will support incremental change
if I think it's headed in the right direction,
those who are more progressive,
well, you're a shill for Biden because it's that rare.
They don't have those cheerleaders
because it's not marketable.
When I'm doing those videos,
I know they aren't going to be widespread.
But much like the shift in going after undocumented workers
at their workplaces,
I think it's something people need to know about.
Most commentators don't do that
because there's a problem with the desire to educate,
the desire to put the information out there,
and the Democratic Party has to start doing that.
They have to start making the case,
and they have to stop being so weak
when it comes to dealing with Republicans.
Yesterday, as an example off the top of my head here,
204, 206 Republicans voted against raising the debt ceiling.
And when Democrats talk about it,
that's what they're going to say.
Well, they voted against raising the debt ceiling.
We may have to default.
Weak, weak.
Stop treating them with kit gloves.
200 Republicans just voted to wipe out your 401K.
That's what happens if we default.
200 Republicans voted in favor of a recession.
That's what happens.
200 Republicans voted against paying the United States' bills.
The party of personal responsibility
doesn't want to honor their debts.
But they're not going to do that.
More than likely won't
because they're trying to maintain that coalition,
a coalition that only existed because of Trump.
It won't work.
It won't get re-election.
It won't help in future elections
because they're not marketable ideas.
Running on the idea that I'm not Trump
only works if Trump's running.
The best thing in the world for the Biden administration
would be if Trump ran again
because then, yeah, he'd probably win
because he can use that same tactic.
And the majority of Americans rejected Trump and Trumpism.
But what if the Republican Party
puts up somebody a little bit slicker,
a little bit more polished?
Well, then we're in real trouble, right?
The Biden administration is definitely in trouble.
The idea of moving progressives to center will fail always.
That's not a workable strategy.
The Biden administration has to get out there
and they have to make the case.
That infrastructure package is good.
It is good.
It is good for a lot of people
who are independent but right-leaning.
And if they understood how it would benefit them,
it might change their opinion.
If progressives understood how much of it
was designed to impact things they care about,
it would energize that side of it.
The coalition can be maintained,
but only if the Biden administration
tries to move the center to the progressive position.
If they try to go the other way,
the Biden administration will be a one-term administration.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}