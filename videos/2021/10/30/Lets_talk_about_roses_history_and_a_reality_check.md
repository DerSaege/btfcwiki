---
title: Let's talk about roses, history, and a reality check....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=WrFL-GnYEKk) |
| Published | 2021/10/30|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about the White Rose group, a historic group from WWII Germany known for opposing the regime.
- Admires the structure of White Rose arguments and how they used common imagery to make their points accessible to different groups.
- Mentions that White Rose members were all vaccinated, as mandatory vaccination was the norm in Germany at the time.
- Notes that White Rose never mentioned vaccines in their writings, focusing on nonviolent passive resistance instead.
- Criticizes the misuse of White Rose imagery by anti-vaccine groups, calling it historically illiterate and potentially propaganda.
- Points out that mandatory vaccinations were suspended during the Nazi regime, suggesting a disregard for public health.
- Urges people to drop WWII references from vaccine debates, as it can be offensive and shows historical ignorance.

### Quotes

- "Claiming the mantle of the White Rose does not make you look like a freedom fighter. It makes you look like you've never read a history book."
- "It's just a thought."

### Oneliner

Beau explains the historical context of the White Rose group to criticize the misuse of their imagery by anti-vaccine movements, urging a more informed and respectful approach to public health debates.

### Audience

History enthusiasts, public health advocates

### On-the-ground actions from transcript

- Educate yourself on the history of groups like the White Rose and their true messages (suggested)
- Use accurate historical references and avoid appropriating symbols for unrelated causes (implied)

### Whats missing in summary

The full transcript provides a detailed historical analysis of the White Rose group's stance on vaccines and passive resistance, urging a more informed and respectful approach in public health debates.

### Tags

#WhiteRose #History #Vaccines #PublicHealth #Education


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we are going to talk about a historic group
and how their imagery has found a new life
and whether how that imagery is being used
is even remotely historically accurate or even appropriate.
Along the way, we're also going to talk about another common image in American politics.
And you're going to find out that a common set of imagery that is used is also historically inaccurate.
It's not true.
In fact, the reality is almost the exact opposite of the way it commonly gets portrayed.
Okay, so what's the group?
The group is the White Rose.
It's a group that existed during World War II in Germany to oppose the regime there.
It's a group I hold in incredibly high regard.
The way that I structure my arguments on this channel
is often a poor imitation of how they structured theirs.
I've read all of it, everything White Rose put out.
And the thing that always struck me wasn't even always the arguments themselves.
It was how they were structured.
They used common imagery that allowed everybody to associate with what they were talking about.
They framed their arguments in multiple ways so they could reach different groups of people.
They made it accessible.
Rather than some labor-intensive philosophical debate that requires a master's degree just to even get involved in,
they met everybody where they were at.
And it succeeded.
This group's imagery, and it's also worth noting before we get into it,
that some members of this group quite literally lost their heads for their opposition to the regime in Germany.
This group's imagery is now being used by people opposed to vaccines.
Okay, so first little history fun fact of the day.
Every single member of the White Rose Society was vaccinated.
Every single one of them.
I don't have their vaccination records because I don't need to have them.
See, they were born in a time when mandatory vaccination, well, that was the norm.
That was the norm.
Germany had mandatory vaccination going back to the late 1800s.
So these are people who have been immortalized in books and movies for speaking out against things that they opposed.
I've read everything they put out.
Do you know what word is never used?
Vaccine.
They never talked about it.
Almost like it wasn't even on their radar because it wasn't that big of a deal.
It's never mentioned.
You would think that a group of people who were subjected to mandatory vaccination,
if they had an issue with it, they might have said something.
It's also worth noting that their whole thing was nonviolent passive resistance.
That's what they advocated.
They wanted to make sure that people understood that they shouldn't put themselves in positions
where they would have to do something they found morally questionable.
They could resist passively.
If somebody was opposed to vaccines in general,
and they lived in a country that had a long history of mandatory vaccination,
and they actively said, hey, you know, don't do that.
Don't put yourself in a position where you may have to do something that you're morally opposed to.
They probably wouldn't go to medical school, which most of them did.
Now, with this set of facts, it's safe to say that the use of white rose imagery by people who are opposed to vaccines
is at best historically illiterate.
At worst, it's horrible propaganda and standing on people far better than yourselves.
Here's the other side to this.
I said when they were born, there were mandatory vaccinations.
Germany had a long history of mandatory vaccinations.
Do you know what period didn't have them?
Guess.
That's right.
Just before Adolf took over and all of Adolf's time,
the smallpox vaccination program, all that stuff was suspended.
Now, I'm sure somewhere records exist explaining exactly why,
but I can take a pretty educated guess.
My guess would be that there was an accident in, let's say, 1930,
and the regime didn't want to deal with any unrest.
And knowing what we know about how they viewed the average person, the people at the bottom,
well, we'll just frame it around the idea of it's their freedom.
That's how we'll make, you know, Germany great.
And the only people we'll lose will be the old and those with comorbidities.
It was kind of their thing.
You know, they wanted that Superman, right?
And everybody else, well, not a big deal if they're lost.
Given those two sets of information, do you think it's Sophie that would be opposed to vaccines?
Do you think it's Sophie Scholl?
Or do you think maybe it's somebody else from that same period?
I would strongly suggest dropping all World War II references from this discussion.
Not just is it incredibly offensive to large sets of people
who have been very vocal in pointing out that vaccine requirements are not the same as the things being referenced.
Not just is it incredibly offensive to them, it also makes you appear as though you are historically illiterate.
Claiming the mantle of the White Rose does not make you look like a freedom fighter.
It makes you look like you've never read a history book.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}