---
title: Let's talk about the $600 IRS provision....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ilhx5Mohpb0) |
| Published | 2021/10/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains the IRS proposal in the reconciliation bill, causing many questions and concerns among people.
- Points out the main concerns are the $600 threshold for IRS reports and a misunderstanding about what information the IRS will receive.
- Clarifies that the IRS will receive data on how much money goes into and out of an account, not detailed spending information.
- Acknowledges privacy concerns but notes that the reported information is more limited than perceived.
- Questions the effectiveness of the $600 threshold, deeming it illogical and a waste of resources for minor tax discrepancies.
- Argues that the low threshold will flood the IRS with reports, rendering the whole process ineffective against tax avoidance.
- Suggests setting a higher threshold to target those with significant funds and offshore accounts, rather than those barely making ends meet.
- Criticizes the provision as outdated, out of touch with reality, and likely ineffective due to poor design.
- Concludes that the provision is pointless, ineffective, and a waste of taxpayer money, unlikely to achieve its goal of curbing tax avoidance.

### Quotes

- "This seems like a really dated idea. It seems like it is way out of touch with reality."
- "It's a completely pointless provision. It's a completely pointless policy. It's not going to do any good."
- "It doesn't seem like it will do anything to curb tax avoidance, which is the stated goal."

### Oneliner

Beau explains the ineffective and wasteful nature of the IRS proposal in the reconciliation bill, citing a low $600 threshold and flawed design as major concerns.

### Audience

Taxpayers, policymakers

### On-the-ground actions from transcript

- Challenge the $600 threshold by contacting relevant policymakers to advocate for a higher threshold (suggested).
- Join or support organizations working on tax policy reform to address ineffective provisions like the one discussed by Beau (exemplified).

### Whats missing in summary

In-depth analysis of the potential impacts on individuals and the broader financial system.

### Tags

#IRS #TaxPolicy #ReconciliationBill #TaxAvoidance #PrivacyConcerns


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about the IRS proposal.
It looks like it's gonna be in the reconciliation bill
and people have a lot of questions about it
and a lot of concerns.
There are two main concerns.
The thing is, there's also a pretty big misunderstanding
about what this is.
Now, if you don't know what I'm talking about,
there's a provision that is going to give the IRS reports
about accounts that have $600 in transactions or more.
Okay, so the misunderstanding occurs
in what the IRS is actually getting.
The idea seems to be among a lot of people
that the IRS is gonna get like a printout
of where you spend your money.
That's not really what's happening.
They're going to get a printout
of how much money went into an account
and how much money went out of it.
They're not gonna know that you spend
way too much money at Wendy's.
They're just gonna get gross amounts, gross totals, okay?
Now, from a privacy standpoint,
those people who are concerned about that aspect of it,
yeah, that's different than what's being commonly discussed.
Is it so different that it's going to change
their point of view?
Probably not.
It's still a bank reporting directly to the IRS
about your financial state.
I don't think it's gonna change the conversation,
but in the interest of accuracy,
it's worth pointing out that there does seem
to be a misunderstanding about exactly
what's going to be reported, okay?
Now, the other concern that people have is why $600?
Why is that threshold set so low?
And the answer is, I don't have a clue.
That doesn't make any sense.
It doesn't make any sense at all.
When you're talking about accounts
that have that little in them, what's the IRS gonna do?
Spend tens of thousands of dollars auditing an account
to get $2 in missed tax revenue?
That seems like a giant waste of resources.
That doesn't make any sense.
And aside from that, setting the threshold that low,
it ends up making the whole thing pointless.
You know, when you talk about mass surveillance
and you talk about mass intelligence collection,
when you listen to the people who process it,
the problem they have is that they have too much information
so things get missed.
They get overwhelmed with information.
That is what is going to happen with this
because the threshold is set so low.
This is gonna be completely ineffective.
It's not gonna do anything to mitigate tax avoidance.
Because the threshold is set so low,
the IRS is going to get 100 million reports.
They're not gonna be able to process them.
They're not gonna be able to determine
which ones really need to be looked at.
It will, the threshold being that low
renders this whole thing pointless,
completely ineffective.
It would make more sense to me
to set that threshold at $50,000, $84,000.
Up there where people stop living paycheck to paycheck,
where they have a little bit of a cushion,
where they actually have money to hide.
When you're talking about people that are barely getting by,
there's no point in looking at them.
You know where their money went to live?
That's it.
They don't have offshore accounts.
So it seems pointless.
This seems like a really dated idea.
It seems like it is way out of touch with reality.
And with the threshold being so low,
seems like it would be completely ineffective.
Unless the IRS is just looking to audit poor people
because they know they don't have the resources
to contest it.
I guess that's an option.
But it's worth noting
that this is gonna be in the reconciliation bill.
Me looking at it,
it doesn't seem to be as big a privacy concern
as initially thought.
Is it still one?
Yeah, kinda.
It is.
This isn't something I would support.
But that privacy concern is kinda offset
because it's written so poorly
and they have the threshold so low
that it's gonna be completely ineffective
because the IRS isn't gonna be able to process
all the information they're gonna receive.
So at the end of it, to me, this is a wash.
It's a completely pointless provision.
It's a completely pointless policy.
It's not going to do any good.
It's just gonna waste taxpayer money.
That's really, looking at it,
that's what's gonna happen.
It doesn't seem like it will do anything
to curb tax avoidance, which is the stated goal.
If that's the goal, that threshold needs to be way,
way higher than it is.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}