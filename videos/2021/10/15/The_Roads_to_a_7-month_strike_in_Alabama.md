---
title: The Roads to a 7-month strike in Alabama....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=cD3CM_RVoDY) |
| Published | 2021/10/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Provides a follow-up on a strike happening in Alabama at Warrior Met Coal.
- Hayden Rot, the auxiliary president for the MWA at two locals, gives insight into the strike.
- The mine specifically mines metallurgical coal for steel production, not thermal coal for energy.
- Workers have been on strike since April 1st due to safety concerns, unfair labor practices, and family time.
- The company has a policy of scheduling workers seven days a week, 12 hours a day, with harsh penalties for absences.
- Previous contract changes removed double-time and triple-time pay, limiting time off during holidays for families.
- Vehicular assaults on picket lines by company members or scabs have occurred.
- Negotiations have not progressed well, with the company offering improvements that favor scabs over union members.
- The company aims to hire back only those who they believe did nothing wrong during the strike, excluding vocal union members.
- Financial strain is increasing as the strike enters its seventh month, with efforts to support union families through the holidays.
  
### Quotes
- "It's not just about pay for us. It's about holding the company accountable."
- "We're still ready to be here. We're not ready to give this fight up until we win."
- "It's really only been through all the mutual aid that we've received and through the union that we have been able to hold on as long as we have."

### Oneliner
Beau follows up on a strike at Warrior Met Coal in Alabama where workers fight for safety, fair labor practices, and family time amidst company resistance, financial strain, and community support.

### Audience
Supporters of labor rights

### On-the-ground actions from transcript
- Donate to UMWH Striking Miners Pantry PayPal account for groceries, baby items, and hygiene products (suggested)
- Support the toy drive through the Target registry link provided by Hayden (suggested)
- Attend solidarity rallies or events organized by the union to show support (implied)

### Whats missing in summary
The full transcript provides detailed insights into the ongoing strike, challenges faced by workers, community support efforts, and the importance of unions in advocating for workers' rights.

### Tags
#LaborRights #Strike #Union #CommunitySupport #Alabama


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're gonna do a little bit of a follow-up.
We're gonna find out what's going on,
where,
up at a strike, sorry, up at a strike in Alabama.
And we have somebody who has been there
pretty much the entire time and who can clue us in.
You wanna introduce yourself?
Hi, my name is Hayden Rot.
My husband is on strike at Warrior Met Coal
in Brookwood, Alabama.
I'm the auxiliary president for the MWA
at two locals there at Warrior Met.
That's locals 2245 and 2368.
So we do a lot of work,
so we're pretty much on the ground doing something
several days a week.
And so this mine, it's a coal mine,
but what kind of coal is it?
This way we can go ahead and head something off
in the comments.
Yes, we don't mine any thermal coal.
Thermal coal is what you use to produce energy.
We only mine metallurgical coal,
which is used to produce steel.
So in order to have coke that makes steel,
you have to have this type of coal.
All right.
And why are y'all on strike?
What's going on?
We've been on strike since April 1st.
So we're into the seventh month of our strike
and we're on unfair labor practices strike.
So we went out not for pay,
but for safety reasons, safety concerns, benefits,
time with our families.
So it's not just about pay for us.
It's about holding the company accountable
for not being able to schedule people to work.
So schedule people to work seven days a week,
12 hours a day, to have some accountability there.
They have a really brutal four-strike
in your fired policy.
That means if you're late, you get a strike,
it counts against you.
You're in the hospital and you can't give 24-hour notice,
counts against you.
We've had men whose wives have been in labor,
been having miscarriages that were given strikes
because they had to leave work early
or they couldn't make it in.
We've had people die near the mine sites
because they were trying to speed to get to work
and had an accident.
So they don't accept doctor's excuses.
They don't take into your account your family,
your children, your personal health.
That was one big thing that we want to see addressed
in negotiations.
Time off, like I said,
there was no seventh day optional in this past contract.
You could just be scheduled to work as many days
as many hours as they told you to.
It also, in the last contract,
which happened at the bankruptcy ruling,
it took off things like your double-time pay,
your triple-time pay for things like holidays.
Our families get three holidays a year together
under this contract.
We were able to have Thanksgiving,
Christmas Eve, and Christmas Day.
Those were the only physical holidays
that they got to be home with their families.
The rest of the holidays,
they would give them a day to take during the week,
which when you think about it,
when your family's already celebrated,
you getting a day off during the week
is you sitting at home by yourself
while your kids and your families are at work or at school,
eating cold barbecue alone.
So it's not really giving you a holiday.
It's just giving you an off day.
We want to be able to spend that time with our families.
So this is about safety and standard of living,
more so than wages.
Right.
So what's been going on there?
How's it looked?
Kim filled us in a lot on what had been happening
up to that point, but that was a month ago,
something like that.
We've had now six vehicular assaults on the picket lines
from members of the company,
or scabs going in and crossing the pickets
that have actually struck union members with their vehicles.
One of our auxiliary members, Amy,
was actually at a picket line.
She was actually standing a few feet away from Cecil Roberts
and was hit by a truck that tried to break through the line.
So we've been dealing with that.
And really the company is not wanting
to negotiate in good faith.
The company keeps coming back and offering,
well, we'll make these small improvements,
but the ones scabbing get seniority over the union members.
Well, that's a big no right there.
That's a non-starter, right?
Yeah, that's just a, you know,
we don't need to talk anymore about it.
And then the other thing they keep trying to slide
in the contract is they only want to hire back
who they think hasn't done anything wrong during the strike.
And they don't want the union to be able to fight
for the jobs.
So anybody that's actually been on a picket line,
anybody that's been vocal in expressing what's going on,
they don't want to hire you back.
Right, and if they do that well enough,
I would imagine the contract really wouldn't mean anything
because nobody would complain.
Right.
All right.
So y'all are gearing up,
y'all are heading into the holidays.
How is that looking?
And how are y'all weathering the storm?
How's the pantry looking and all that stuff?
It's getting more difficult.
Like this is month seven.
So things are getting a little bit tougher for everyone.
We're still ready to be here.
We're not ready to give this fight up until we win.
And we will, the union will win.
It's just a waiting game.
The company negotiators have said numerous times
that they don't think that the workers are worth anything,
that we should actually take a cut,
that they can just starve us out.
So it's really only been through all the mutual aid
that we've received and through the union
that we have been able to hold on as long as we have.
But we could really use some help still with the pantry.
And with Christmas coming up,
we're really pushing right now to make sure
that every union child can have a visit from Santa.
You know, right now, money for bills and things are tight.
So we're trying to kind of take some of the stress
from the holidays off of our union families
by being able to provide Christmas for the kids.
Yeah, now when I started, I saw a link pop up on my screen.
Is that the link that you put up there?
Yes, that's the link for,
or we have a registry through Target.
And it's kind of hard to figure out a way to do a registry
for people to look at to be able to pick
kind of what items they would like to purchase
because Amazon was a no go for us
because they're union busters
and hopefully they're gearing back up here in Bessemer.
My husband's actually involved kind of with that too
and this truck because he went there.
And then, you know,
Walmart's not necessarily union friendly either.
So Target was kind of our way
that we could put one together
so everyone could kind of pick.
Because to me, that's just a lot more personable
to be able to see kind of what the kids had asked for
and be able to actually choose your gift.
And when you look, a lot of it that people are asking
for things like winter clothing and shoes.
So it's not necessarily all toys.
A lot of it's necessities that they just need right now.
Yeah. Yeah.
So do y'all have any idea
on how long the strike's gonna last?
Or do y'all have any clue?
Have you gotten any signs from management
that they're even considering shifting positions?
They've been a little bit more willing to negotiate.
Negotiations have lasted a little longer,
but as far as any,
do I think that we'll see a contract,
a tentative agreement in the next couple of weeks?
I really don't.
I feel like we'll be out, you know,
through the first of the year.
I really do. That's just how I feel.
And even if we were to get a contract, say in November,
the way the pace done there,
most of our families wouldn't get a check
until the end of December.
By the time you got started back, hired back in,
did your drug test,
they hold your paycheck for two weeks for biweekly.
So you would still be looking at the end of December,
even if we solve something in November to vote.
All right.
Oh, man.
So realistically, you're talking about being on effectively,
even if there was something soon,
you're talking about almost a year of strike.
Yes. And we do like 200 pantry bags a week for families,
for groceries.
We also do baby items every week, diapers,
formula, baby food, baby wipes,
because those items are expensive.
And when you're struggling, you know,
having that extra thing of formula,
having those diapers makes a huge difference.
Even for our, you know, our own families,
like a lot of us, I have a 10 month old and a seven year old.
Most of the other ladies that are in the auxiliary with me
have young kids as well.
It's just, it takes some of that pressure off
that at least, you know, you have those items
that you need to survive.
Right. Right.
So what can people do to help?
We have a PayPal set up for the auxiliary.
So any money donated to our PayPal account,
which is UMWH Striking Miners Pantry.
If you go there on PayPal,
all of that goes to buy groceries for families,
baby items and hygiene products like toothpaste,
shampoo, conditioner, deodorant, things like that.
You don't think about not having,
but when you've been on strike for seven months,
sometimes you don't have the extra money to buy those things.
Right.
So like with COVID going around, toothbrushes.
I mean, something that simple, you just don't think about,
hey, I've gotten sick and I need these
and I might not have the extra money for it right now.
Right.
And we also, if you're local,
we actually, they're at the hall where we do pantry pickup.
We actually have clothing too.
Like we had people donate racks
and we've been doing clothing drives
so that people can come in and get clothing
from infant to adult.
Because we've had several adults come in
that needed some items to wear for like job interviews
because people are having to get part-time jobs to make it.
The UNWA has been amazing.
I mean, $350 a week strike pay is unheard of.
I mean, that's fantastic.
We couldn't ask for anything more than that.
That's, I mean, I mean, look at it.
I mean, I know a lot of people don't think, well, $350,
but for strike pay, that's actually extremely high.
Right.
But still you're having to get something else
to kind of make ends meet.
Oh yeah.
After seven months, I mean,
it doesn't matter how much of a nest egg you had.
Somebody's, you know.
Right.
And they're still doing their picket duties
because we still have picket lines going
seven days a week, 24 hours a day.
And for us, because the multiple sites,
we don't have just like a front entrance
and a back entrance that you have two pickets.
We have seven pickets to 10 pickets every day.
And just to ride the mine sites, it's over a hundred miles.
You know, when you go between all the different locations.
Right.
How's the local community doing?
Like, are they supportive?
Are they, like, where does the town fall?
Mostly the town is supportive.
And it's hard here in Alabama
because when you think about the mines
being in Brookwood, Alabama,
a lot of people do live in Brookwood,
but it's all these other surrounding areas
that really depend on these mines as well.
Like West Blockton, Oak Grove.
People drive all the way from places
like Walker County to work here.
So it's not just affecting one small chunk of our state.
It's actually affecting a huge chunk of people.
So the people have it mostly supportive.
It is a little bit harder here because let's just be honest.
We're in the South. We're in Alabama.
It's a right-to-work state.
Most people are more conservative
that they don't really understand unions
if they haven't been in one.
So that's something you have to kind of explain to them.
They just don't understand what they are
or why they're important.
So combating that the same way as combating,
like, Alabama being right-to-work,
which is one reason why the strike has lasted this long
because they're hiring in contractors.
They're bringing in scabs from other states,
a lot from West Virginia, Virginia, Kentucky,
are coming down here and going in, crossing a picket line.
And when you're in a right-to-work state,
you can't stop that.
Right. Right.
I mean, in that situation,
basically, you just have to wait for the company
to get tired of paying the contractor pay
and it eating into their profits because it undoubtedly is.
And the reality is, if they've been sustaining that
for seven months, they can certainly afford to pay more
because they've been doing it the whole time.
Exactly. And when you think about
the private security they've hired,
I know I personally, when I go to Brookwood,
get followed around by the guy that drives a Charger,
drives a Dodge Charger as security,
will follow me and my kids around
when I go up to the picket line.
I think he thinks it's intimidating,
like when he rolls his window down
and like gets his phone out.
Personally, I think it's funny.
I just wave and I'm like, I can record you too.
But for some people, I can say that would make them nervous.
But, and they've put up now at Fort Scab,
which is what we call the CMO.
They've started busing the Scabs in now.
So they all park at the CMO and then they get on a bus,
a school bus to drive out to the pick dock,
to the mine site to cross.
So they put up fencing there and put up black tarp.
I mean, you can see through the black tarp.
I'm not really sure why they thought that would be.
Effective or why they would spend money doing that.
They put up cameras and microphones
at all of our picket locations.
So at all the entrances now,
they have cameras and microphones.
So if you have all that money to pay contractors,
to pay private security,
to somehow have state troopers out there
escorting Scab buses,
which is a whole nother misuse of funding.
And then to pay to have all of these cameras,
videos, contractors,
you could have paid for a contract.
Metco prices are over $400 a ton right now.
They're running one mine site
when they could be running two plus a brick plant.
So they're losing a huge amount of money
that they could be making
because the market right now is high.
Right.
And realistically,
it doesn't seem like it's gonna be going down
anytime soon either.
Not with the demands that have been going around.
So at the end of this,
what we have is an ongoing strike.
It's been going on seven months.
It's looking to go on at least another two.
And there hasn't been any change really
since the last time we had somebody on to talk about it.
Other than y'all are in a little bit more dire straits
because it's sometimes past.
Financially, it's a little bit rougher.
We'll see what we can put together
and see what this channel can do to help out.
So I'll send you a message when we get off.
But I really appreciate you giving us the time
and letting us know what's going on.
Now, you mentioned to Amazon.
Are there any other strikes going on that right now
that we need to know about?
Oh, it's striped over.
We have a lot of strikes going on right now.
Actually a few weeks ago,
nurses in Birmingham walked out.
They only stayed out for a few hours,
but still that's a huge step.
In Alabama, when those are non-union nurses
took that step that they staged a walkout and said,
hey, this isn't right.
Our safety is important.
Patient safety is important.
So that was a big one.
I'm getting reports that Amazon has really started
ramping back up on their union busting,
holding some anti-union meetings.
So they're gearing up for a revote
at Amazon and Bessemer right down the road.
My husband actually took a part-time job at Amazon.
He's talking to their union organizers there.
But like he said, he said, if I'm going to be off,
if I'm on strike, where better to be
than somewhere where I can help other workers?
You know, he was like, I can tell them what a union is.
And actually it's not as dire as it is.
And as long as it's been,
it's really brought a lot of us close together.
Like tonight we're having a solidarity rally at Taney Hill.
We have them every Wednesday at six.
Unfortunately I have COVID, so I couldn't be there,
but we're even doing a trunk retreat tonight
for the UOWA kids.
So we pulled together and got decorations,
decorated trunks, got candy,
because we want the kids to have some good memories of this
because from their point of view, they're loving it
because they went from seeing their parent,
their other parent, maybe two hours a day.
Because by the time you work 12 hours,
you come in, you eat, you're going to sleep
and you're exhausted.
So for them, most of them,
if you ask them about the strike, they're like,
well, my dad got to come to my ballgame.
My dad got to come to karate
because they've never had that before.
Right.
So things like the back to school bash
that we did back in August
where we got backpacks and school supplies.
Things like tonight with the trunk retreat,
just where we all came together and made soups.
Like we made 20 crockpots of soups.
All of us just made what we could.
And then we took up and got what we could
and bought candy for the kids.
And it's just putting the time in
to make those special memories
because we're fighting for our families
and for time with them.
So we've been trying not to forget that during the strike
that, hey, this is rough right now,
but we're getting this time that we've never had before.
Right.
And if everything goes well with the strike,
you get more time off.
Right.
You might be able to enjoy this a little bit more often.
So, I truly appreciate you stopping by,
letting us know what's going on.
We'll try to get the word out
and try to help in any way that we can.
Go ahead and give them a way to find you
and find out more information.
Like give me your Twitter handle or whatever.
If you'll go to at Hayden Wright on Twitter,
I have some pinned comments up at the top
that kind of explain the strike,
give where to donate for the auxiliary
and for the toy drive.
So you can find information there.
Or if you don't have Twitter, you can email me.
It's just wrighthayden at gmail.com.
And spell Hayden Wright for them
because it's not a Y, right?
No, Hayden is H-A-E-D-E-N-W-R-I-G-H-T.
Alrighty.
Okay, so that was just a little update.
Thank y'all for stopping in
and listening to what's going on.
And this is, you know, when we talk about unions
and we talk about strikes,
we talk about the one side of it.
We talk about the picket line.
Just remember, anytime you see one of those picket lines,
that's that tip of the spear.
You have the entire staff behind it,
pushing, trying to get forward movement.
And most times, they never get any recognition at all.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}