---
title: Let's talk about public health at the border and inconsistency....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=ePWv7uaPrlk) |
| Published | 2021/10/15|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing criticism from the right wing about the Biden administration's handling of the border and public health.
- Refusing to advocate for forced vaccination at the border, focusing on appealing to protecting oneself and others.
- Stating a strong pro-vaccine stance but opposing using force or denying liberty to enforce vaccination.
- Criticizing the idea of locking up unvaccinated individuals, pointing out the underlying bigotry in such arguments.
- Challenging the notion that unvaccinated individuals won't seek vaccination once released.
- Expressing concern about those who advocate for measures they wouldn't want applied to themselves.
- Warning against blindly repeating arguments without critical thinking and understanding the implications.
- Asserting that people crossing the border have rights under the US legal system.
- Emphasizing the paternalistic nature of forcing vaccination on certain groups.

### Quotes

- "I don't believe the government should use force, deny your liberty, to make you get vaccinated. That would be wrong."
- "Anything you advocate for them, well, it should be done to you as well, unless, of course, you believe there's two justice systems in the United States."
- "Anything that you allow the government to do to a small demographic, you're just opening the door for yourself."
- "You will advocate for your own incarceration because they have that much of a hold over you."
- "Y'all have a good day."

### Oneliner

Beau addresses right-wing criticism on vaccination, opposing forced measures and challenging the bigotry behind locking up unvaccinated individuals.

### Audience

Public health advocates

### On-the-ground actions from transcript

- Challenge and critically analyze arguments before repeating them (implied)
- Advocate for equal treatment and rights for all individuals (implied)

### Whats missing in summary

Importance of critical thinking and advocating for equal rights and treatment for all individuals.

### Tags

#Vaccination #PublicHealth #BorderControl #Criticism #Rights


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about a common refrain
coming out of the right wing right now,
a criticism of the Biden administration.
And it's one that they feel that, well,
myself in particular,
but a whole lot of other people should also have.
So today we're gonna talk about the border
and public health and what the two things have to do
with each other and the brilliant plan
coming out of Republican circles
on how to solve our public health issue.
To personalize it, I'll give you a direct comment
that was left for me.
You constantly bring up the decision of people
choosing not to take the vaccine.
You have yet to bring up not forcing vaccines at the border,
like other liberals, constant flip-flopping.
Okay.
Yeah, see, I don't frame my arguments
in favor of vaccination around force.
I don't do that.
I try to appeal to the desire to protect your family,
your community, your friends, yourself,
professional obligations, things like that.
I probably have a hundred hours of content
that is advocating for people to get vaccinated.
You will never see me suggest
that the unvaccinated get locked up
or have violence visited upon them.
That would be wrong.
That's wild.
To the contrary, on February 19th, 2019,
yeah, before all this started,
I have a very in-depth video on the topic.
I'm incredibly pro-vaccine.
I think you should get vaccinated.
I will argue with you all day
trying to convince you to go get vaccinated,
but I don't believe the government should use force,
deny your liberty, to make you get vaccinated.
That would be wrong.
So the idea here, the idea that's contained in this
is that it's wrong to release people who are unvaccinated.
So therefore, if you are unvaccinated,
you should be locked up.
Yeah, I don't know that this is a cell door
y'all particularly wanna open.
See, I understand that in your mind,
it's not gonna work the way it would
because you haven't thought about this.
This is just something somebody told you and you repeated
because it sound good, because it appealed to your bigotry.
It appealed to your bigotry.
See, I'm certain the people saying this
do not mean for this to apply to white folk,
just those brown people at the border, right?
Once they cross the border, once they're on US soil,
they kind of have rights under our legal system.
Anything you advocate for them,
well, it should be done to you as well,
unless, of course, you believe there's two justice systems
in the United States.
If you have made this argument,
you have, whether you realize it or not,
argued in favor of locking up unvaccinated people.
That's what you're saying, point blank.
We shouldn't release them unless they're vaccinated.
Okay, I mean, that's, lock them up.
Lock them, I mean, y'all can chant it if you want.
I'm not something I'd really get behind.
See, the inherent idea is that when they're released,
they won't go get vaccinated.
I don't know why you would think that was true.
They haven't fallen down an information silo
where they just repeat mindless stuff
without thinking about it.
They're not ignorant.
They seem pretty sharp to me.
A lot of these people made their way from El Salvador
to the United States on foot.
I am fairly certain they can find a CVS.
It's very paternalistic.
Those people, those people over there,
well, they have to be forced to do it.
No, I don't think they do.
I'm fairly certain that somebody who is in fear for their life,
who makes a journey like that,
they'd probably take steps to protect themselves.
Somebody who traveled to try to give their family a better life
would probably take steps to protect them.
I'm not worried about them.
I'm worried about the people who repeat stuff like this.
I'm worried about the people who would say,
oh, we shouldn't let people out if they're unvaccinated
while being unvaccinated.
Anything that you allow the government to do
to a small demographic,
you're just opening the door for yourself.
Your thought leaders,
the people who come up with these arguments,
they don't believe them.
They just know you'll repeat them without thinking about it.
You will advocate for your own incarceration
because they have that much of a hold over you.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}