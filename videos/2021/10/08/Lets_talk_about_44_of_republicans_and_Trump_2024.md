---
title: Let's talk about 44% of republicans and Trump 2024....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=f8aWg6UrPOU) |
| Published | 2021/10/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Report on a poll regarding whether Republicans want Trump to run again in 2024.
- Forty-four percent of Republicans support Trump running in 2024.
- However, the majority of Republicans do not want him to run again.
- Trump's support is not as significant as some portray it.
- The numbers indicate bad news for Trump's future aspirations.
- As more is revealed about his policies, support for Trump is expected to decrease.
- Trump's indecisiveness about announcing his candidacy is not inspiring confidence.
- It is unlikely that his support numbers will increase from here.
- Other political figures might begin to criticize and undermine Trump due to his lack of leadership.
- The most likely scenario is for Republicans to support Trump as a political figure but prefer other candidates.
- Right-wing outlets publicizing this poll may be aiming to weaken Trump's position for 2024.
- Overall, the numbers suggest Trump is not in a favorable position for a 2024 run.

### Quotes

- "Forty-four percent of Republicans want Trump to run again in 2024."
- "Another way to say that would be a majority of Republicans don't want him to."
- "These are bad numbers for the former president."
- "They're probably going to continue to fall, especially as other political figures are having to put their plans on hold."
- "There is no way to honestly look at these numbers and think that the former president is in a good position for a 2024 run."

### Oneliner

Beau reports on a poll showing that while 44% of Republicans support Trump running in 2024, the majority do not, indicating bad news for Trump's future aspirations and suggesting he is not in a favorable position for a 2024 run.

### Audience

Political analysts

### On-the-ground actions from transcript

- Analyze and understand the implications of the poll results (implied)
- Support political figures who represent your views and values (implied)

### Whats missing in summary

The full transcript provides detailed analysis and insights into the implications of a poll regarding Republicans' support for Trump in the 2024 elections.


## Transcript
Well howdy there internet people, it's Beau again.
So today we are going to talk about that poll that is being broadcast in a number of ways.
It's about whether or not Republicans and Republican-leaning independents, whether they
want Trump in the future in 2024.
Now if you go to right-leaning places, they're cheering it.
Forty-four percent of Republicans want Trump to run again in 2024.
Yeah I mean that's one way of looking at it.
If you say it like that, I mean that sounds pretty good.
Another way to say that would be a majority of Republicans don't want him to.
That's the reality.
Forty-four percent of Republicans is not quite as big a number as people are making it out
to be.
That's pretty bad for Trump.
These are bad numbers for Trump.
Forty-four percent want him to run in 2024.
Twenty-two percent want him to be a political figure but let somebody else run.
We're kind of done with him.
And thirty-two percent don't even want him to be a figure in the Republican Party anymore.
These aren't good numbers for the former president.
This is really bad news for his future aspirations because they're only going to go down.
As more comes out, as the impacts of his policies are discovered more and more, they will continue
to fall.
And given his reluctance to actually announce his candidacy, they're going to continue to
slip even further, even faster, because he's not going to be able to energize people.
The wait-and-see method that he's taking, it doesn't inspire a lot of confidence.
So it's unlikely that these numbers ever go up from here.
They're probably going to continue to fall, especially as other political figures are
having to put their plans on hold for an indecisive former president to decide what he's going
to do.
Eventually, they will start violating that 11th Commandment.
They'll start taking swings at him.
They'll start undermining him because he's not leading anymore.
So he can't be the leader of the party.
And with 44 percent of Republicans backing him, it's unlikely that these numbers go up.
The most likely number to go up is those who want him to remain a political figure but
support somebody else.
Because if they switch all the way to just get this guy out of the party, they have to
admit they were wrong.
They have to admit that they made a mistake, that they never should have backed this guy
to begin with.
And people are unlikely to do that.
So what's most likely to happen is the number of people who are saying, yeah, he can still
be in the party, we still want him around, but we want him to support other candidates.
That percentage is going to grow.
Because then they don't have to admit that.
They can say, oh, I'm just being pragmatic.
You know, the libs don't like him, so we need somebody more polished.
And they can try to trick themselves into believing they didn't make a terrible mistake
by supporting him to begin with.
The right-wing outlets that are drawing attention to this poll, they're not doing the former
president any favors.
It wouldn't surprise me to find out that some of the people who are pushing this poll out
the most actually support other candidates for the Republican nomination in 2024.
And this is a way to further undermine the former president and help get him out of the
way.
But there is no way to honestly look at these numbers and think that the former president
is in a good position for a 2024 run.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}