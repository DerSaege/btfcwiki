---
title: Let's talk about your electric bill and the environment....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=EZfP4VurCtg) |
| Published | 2021/10/08|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Challenges the conventional perspective on advocating for renewable energy by shifting the focus from the environment to money and profit.
- Points out that it is cheaper to install solar energy infrastructure than gas or coal-fired plants, with costs constantly decreasing due to technological advancements.
- Mentions a study from Oxford that underestimated the significant reduction in costs associated with renewable energy.
- Emphasizes that putting in renewable energy infrastructure is not only cheaper but also more reliable compared to traditional fossil fuels.
- Criticizes politicians who subsidize failing fossil fuel industries, ultimately causing consumers to pay more through their bills.
- Urges individuals to care about their bank accounts if they don't prioritize environmental concerns, as switching to renewables could save trillions according to the Oxford report.
- Suggests that support for fossil fuels is driven by politicians' personal financial interests rather than genuine concern for the environment.
- Distinguishes the issue from nostalgia or cultural wars, stating that it's about politicians profiting at the expense of the public.
- Concludes by inviting reflection on the topic and wishes the audience a good day.

### Quotes

- "We will always do the right thing as soon as we figure out a way to profit off of it."
- "You're destroying the environment so some politician in DC can rip you off."
- "It's not that you were tricked via the culture war. It's that you're really concerned about your Senator's stock portfolio."
- "You should care about your bank account."
- "If you don't care about the environment, understand the infrastructure that would allow us to switch over to renewables."

### Oneliner

Beau challenges the narrative around renewable energy, focusing on cost savings and criticizing politicians for propping up fossil fuel industries at the expense of consumers.

### Audience

Environmentally conscious individuals

### On-the-ground actions from transcript

- Contact local representatives to advocate for policies that support renewable energy (suggested)
- Educate yourself and others about the financial benefits of transitioning to renewables (exemplified)
- Join community initiatives promoting sustainable energy practices (implied)

### Whats missing in summary

Beau's engaging delivery and passion for promoting renewable energy solutions can best be experienced by watching the full transcript.

### Tags

#RenewableEnergy #CostSavings #PoliticalInterests #ClimateChange #Advocacy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today we're going to talk about energy
and energy production.
But today we're going to do it a little bit different.
We're going to talk about it from a different perspective.
When people talk about the infrastructure required
to switch over to renewables, people normally
advocate for it through the lens of the environment
because it seems like witnessing ecological disaster
after ecological disaster would be enough
to shift people's opinions.
But here we are watching another one
off the coast of California, and nobody really cares,
because we're just used to it.
So we're going to use a different framing.
We're going to talk about money, the almighty dollar,
because that's really what motivates people
in the United States.
It's part of our character.
We will always do the right thing
as soon as we figure out a way to profit off of it.
What if I told you that it was cheaper to put in solar
to serve an area than it was to, say, install a new gas or
coal-fired plant?
Because it is.
That's true.
And the costs are getting less, constantly.
They're constantly dropping because it's a new technology.
New innovations come along.
So they can make the components less expensive.
They can make them last longer.
the innovations lower the cost.
Oxford just released a study, and it
showed that while everybody kind of predicted that,
they grossly underestimated how much costs would be reduced.
So when you're looking at it from that standpoint,
it just stands to reason it is now less expensive
to put the plants in, to put the equipment in.
And it's, I mean, kind of obvious
it's gonna be less expensive to run
because it doesn't cost anything to get the sun,
where it does to get coal or gas.
So the overhead's less.
That would get passed on to you, the consumer.
Your bill would go down, right?
But what's happening instead?
A bunch of politicians have convinced people
to prop up a failing, dying industry.
It's antiquated.
But they use your tax dollars to subsidize it.
So you can pay more through your bills.
That's what's occurring.
If you take the environmental stuff out of it,
I know a whole lot of people don't
want to be tree huggers or whatever, right?
If you take that out of it, it's still better.
because it is less expensive, it is more reliable.
If you do not care about the world you are leaving behind
and you only care about yourself,
well, you probably should care about your bank account.
That's probably the type of person you are.
Right now, because a bunch of politicians are invested
in the supply side of what these plants need.
They're invested in coal, they're invested in gas.
They don't want to get rid of it.
They subsidize keeping these industries up,
propping them up so they can continue to make money.
And they do that at your expense.
If you don't care about the environment,
understand the infrastructure that
would allow us to switch over to renewables
would save trillions.
Trillions, that's what the report from Oxford says.
They've consistently underestimated
how much it would save.
So you are no longer destroying the environment
because it's cheaper.
It's not.
You're destroying the environment
so some politician in DC can rip you off.
That's what it boils down to.
That's why fossil fuels still have your support,
because it's really important to you
that these politicians are able to continue to make money
with their stock portfolio.
Has nothing to do with reaching back and yearning
for a forgotten time.
nostalgia.
The way we did it in the past was better.
It's not that you were tricked via the culture war.
It's that you're really concerned about your Senator's stock portfolio.
And that's why you're willing to use your tax dollars to subsidize an industry and then
pay that industry more than you would.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}