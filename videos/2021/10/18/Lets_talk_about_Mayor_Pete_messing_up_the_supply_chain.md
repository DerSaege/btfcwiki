---
title: Let's talk about Mayor Pete messing up the supply chain....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VkxJuMKZvpw) |
| Published | 2021/10/18|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau criticizes the blame on Mayor Pete for US supply chain issues, citing right-wing media.
- He checks his news app and discovers the supply chain issues are actually in the UK and Ireland, not Mayor Pete's responsibility.
- Beau questions why misinformation blaming Mayor Pete is allowed to circulate and blames information silos.
- He explains that Mayor Pete's role as Secretary of Transportation doesn't directly influence global supply chain issues.
- Beau warns about the dangers of information silos and how they can distort reality.
- Despite Mayor Pete's position and responsibilities, Beau clarifies that the supply chain issues are a global problem, not localized to the US.

### Quotes

- "You can be handed a perception that isn't grounded in reality."
- "Whether or not that person or demographic of people is actually responsible for it, that all depends on whether or not you look outside that information silo."
- "Mayor Pete has nothing to do with this."

### Oneliner

Beau clarifies misinformation blaming Mayor Pete for global supply chain issues and warns about the dangers of information silos.

### Audience

Information seekers, critical thinkers.

### On-the-ground actions from transcript

- Fact-check news from multiple sources (implied).
- Break free from information silos by seeking diverse news outlets (implied).

### Whats missing in summary

The full transcript provides a detailed analysis of how misinformation spreads and the importance of verifying information from various sources. 

### Tags

#Misinformation #SupplyChain #InformationSilos #CriticalThinking #GlobalIssues


## Transcript
Well howdy there internet people, it's Bo again.
So today we're gonna talk about Mayor Pete
and his inexperience and how that is apparently
causing the United States' supply chain issues.
You know, it's his fault that all of this is happening.
I was over at my uncle's house this weekend
and he watches a lot of news outlets
that I don't really frequent.
So I didn't even know that this was his fault.
But ever since the Southwest thing,
we do need to remember, we need to fact check
what comes out of right-wing media
just to make sure that it's even occurring, you know?
So I got my news app open here,
we're gonna see if this really is happening.
Okay, so supply chain.
All right, car sales fall due to supply chain issues.
The broken supply chain, many factors, no easy fix.
Toys, Christmas trees, and electronics
added to hard to get list.
Irish logistics resilience being tested.
Ireland facing supply chain chaos
amid perfect storm of problems.
Ireland will not be immune to British supply chain woes.
Oh, I'm sorry, I got the wrong app open.
This is all about the UK and Ireland.
And they're having supply chain issues there.
That kind of, they didn't mention that at all
on the news reports.
And they're talking blaming on Mayor Pete.
They didn't mention that he was in charge
of the transportation of these other countries.
I mean, he's got a pretty big purview
of being in charge of the United Kingdom,
Ireland, and the United States.
I didn't know that.
I didn't even know that he was doing that.
That's wild.
Can't believe that he messed up three countries.
And really, you could take pretty much any country
and look at, type in the name of the country
and type in supply chain,
and you would get news like this
because it's a global issue
that doesn't have anything to do with Mayor Pete
or even US policy, really.
But see, here's the interesting thing.
While it isn't Mayor Pete's fault,
why do you think they got away with it?
And they're going to.
They'll get away with blaming this whole thing
on Mayor Pete's inexperience.
Because they can.
This is when we talk about information silos.
This is the danger.
What they do is they take something
that is known to be true.
Mayor Pete is inexperienced.
Perhaps wasn't necessarily the best choice
for Secretary of Transportation.
That's not true.
That's not true.
That's not true.
That's not true.
That's not true.
You can confirm that.
You can confirm that.
Right?
And as long as you don't tell them
that a secretary's job is really to read the reports
of the people who've been in that agency
and kind of know what they're doing
and distill them and provide the options
presented to you to the president.
I mean, that story sticks.
That there are supply chain issues all over the world.
Then they can blame it on whoever they want.
This is why information silos,
when you get stuck into a certain small group
of news outlets that all reinforce each other,
this is the danger of it.
You can be handed a perception
that isn't grounded in reality.
Mayor Pete has nothing to do with this.
Sure, he's Secretary of Transportation.
Sure, advice to the president.
All of that's true.
But if you leave out the context,
which is really important,
that it's happening all over the world,
you're not really reporting the news.
You're trying to create a narrative
and you're trying to pin it on somebody,
which is generally what the right wing needs to do.
What the right wing media tries to do.
They find a problem
and then they tell you who to blame for it.
Whether or not that person or demographic of people
is actually responsible for it,
that all depends on whether or not
you look outside that information silo,
because they can certainly convince you that it's true.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}