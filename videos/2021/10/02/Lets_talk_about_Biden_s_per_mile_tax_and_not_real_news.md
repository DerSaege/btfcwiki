---
title: Let's talk about Biden's per mile tax and "not real news"....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Opxp3u1SGIQ) |
| Published | 2021/10/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Debunks the misinformation about an 8-cent per mile fee in the infrastructure bill.
- Reveals that the misinformation was spread by a former Trump speechwriter.
- Explains that the bill actually includes a study on the feasibility of funding highways through a usage fee, not an 8-cent tax per mile.
- Points out that implementing a tax on gas usage is a more feasible method to fund highways and infrastructure.
- Mentions the existing gas taxes in Florida, proving that the fear over new taxes is unfounded.
- Notes that many people were unaware of the current gas taxes in Florida.
- Calls out the fear-mongering and unnecessary debate surrounding the non-existent 8-cent per mile fee.
- Recommends the AP's "Not Real News" series as a resource to debunk misinformation and understand how false stories develop.
- Suggests that higher gas taxes may be implemented in the future to support infrastructure and incentivize a switch to electric vehicles.
- Encourages readers to stay informed and not worry about the misrepresented 8-cent per mile fee.

### Quotes

- "If your favorite source of information has been railing about this the last few days, maybe you want a different source of information."
- "It's pretty common. So the fear mongering over it seems a little overdone."
- "I want to take this moment to point to something."
- "I wouldn't worry about an 8 cent per mile gas or 8 cent per mile fee. That's not a thing."
- "Y'all have a good day."

### Oneliner

Beau debunks the misinformation around an 8-cent per mile fee in the infrastructure bill, clarifying the actual contents and suggesting a more feasible solution while recommending the AP's "Not Real News" series for debunking false information.

### Audience

Fact-checkers, News Consumers

### On-the-ground actions from transcript

- Read the AP's "Not Real News" series weekly to spot and debunk misinformation (suggested).
- Stay informed about legislative proposals and separate fact from fiction to make informed decisions (exemplified).

### Whats missing in summary

The full transcript provides detailed insights into debunking misinformation, understanding legislative proposals, and staying informed about current affairs.


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we're gonna talk about that Biden per mile fee
that everybody's talking about,
saying it's gonna be in the infrastructure bill.
It's gonna be 8 cent a mile for when you drive your car.
You're gonna have to pay 8 cent every mile.
That was wild.
Whole bunch of people here in Florida were talking about it.
So I started looking into it.
I looked in that bill.
I looked around, tried to find something about it.
Couldn't find anything at all about it.
Almost like it doesn't exist
because it's not real and it doesn't exist.
And it's something that just was made up
by a former Trump speech writer
that tweeted it out on September 28th.
It's not real.
What is in the bill, what is part of this,
is a study to look into the feasibility
of funding highways and interstates and stuff like that
through some kind of usage fee.
That's what's in it.
There isn't a tax per mile on there.
It's certainly not 8 cents.
It's just not real.
So if your favorite source of information
has been railing about this the last few days,
maybe you want a different source of information.
Those people in Florida who I talked to about it,
those were fun conversations.
Because realistically, it's gonna be really hard
to track everybody's odometer.
So that's not how they would do it.
If you wanted to impose some kind of usage fee for highways
and pay for infrastructure in that route,
well, you would probably just tack a tax on the gas.
You know, that would make sense
because you would end up kind of paying for it
based on usage.
And when I told people this in Florida,
they said that if that happened,
it would be just the end of the economy.
The whole economy would shut down
if there was, you know, a three or four cent tax per gallon
placed on fuel.
And they got really mad when I pointed out
that we already have it.
In the state of Florida, 18.4 cents per gallon
goes to the feds,
with an additional 26.5 cents per gallon going to the state.
It already exists.
It already exists.
It's a thing.
It's pretty common.
So the fear mongering over it seems a little overdone,
given the fact that all those people who said
the economy would just completely collapse
if this was imposed,
had no clue that it already existed.
And in Florida, it's funny that nobody knows it here
because I think we are the highest, if not.
If we're not, we're one of the highest in the country
when it comes to taxes on fuel.
So this whole scandal,
this whole wave of debate over this topic
is pointless because it doesn't actually exist.
So I want to take this moment to point to something.
A lot of times we debunk information on this channel
and we talk about the various things
that can be used to kind of sift through
the massive information that's on the web
and come away with the right answers.
Somebody pointed out that the way I do it
requires an understanding of logistics.
There has to be a better way
that is more accessible for people.
So I want to point everybody to an AP series.
The Associated Press has a series of articles.
I think they put them out once a week
and it's called Not Real News.
Sounds like something that would be
a Saturday Night Live skit.
And what these articles do is they go through,
they take the various claims from the week
and they explain not just where they're wrong,
where there's misinformation and how to debunk them,
but it explains like how it originated,
how the story developed.
And it is able to kind of guide people
in what to look for
when it comes to faulty information
being circulated widely online.
So AP Not Real News is definitely
something worth reading once a week
if you have the time.
But I would not worry about an 8 cent per mile gas
or 8 cent per mile fee.
That's not a thing.
It's not in there.
That's just made up.
Although I would imagine at some point in the future,
there will be higher taxes on gas,
not just to pay for infrastructure,
but to discourage use
and encourage the switch to electric.
That's probably gonna happen.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}