---
title: Let's talk about the historic SNAP increase....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=A2OktXnNyoQ) |
| Published | 2021/10/02|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Historic SNAP increase in the United States, largest in history.
- 25% increase replacing temporary 15% increase due to public health issue.
- Increase amounts to about $36 per person per month, totaling $157 per month.
- $5.23 a day for food isn't enough.
- Low budget leads to buying cheap junk food.
- Misconceptions about SNAP recipients spending on junk food due to limited budget.
- 42 million Americans benefit from SNAP, more than 10% of the population.
- Many who need assistance don't qualify for SNAP.
- Shift in the U.S. from promises of prosperity to struggling with basic necessities.
- $157 increase is a step in the right direction but insufficient.
- Minimum wage increases often become obsolete by the time they take effect.
- Challenging others to live on $157 per month to understand the struggle.

### Quotes

- "You end up getting the cheapest food available. You end up buying junk food."
- "42 million Americans are beneficiaries of the program."
- "It's going to be harder than you think."
- "Sure, it's a move in the right direction. Yeah, we can get behind that."
- "Designate an area in your pantry, stock that area, and you can only eat from there."

### Oneliner

Historic increase in SNAP benefits falls short as 42 million Americans struggle with basic necessities on inadequate assistance.

### Audience

Advocates for social welfare

### On-the-ground actions from transcript

- Stock a designated pantry area and eat only from there to experience living on a limited budget (suggested)

### What's missing in summary

The emotional impact and personal challenges faced by individuals living on inadequate SNAP benefits.

### Tags

#SNAP #foodassistance #socialwelfare #US #beneficiaries


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So today we are going to talk about the historic SNAP
increase.
For those overseas, SNAP is our food assistance program
in the United States.
And it is historic, no doubt.
It is the largest increase in the program's history, period.
Full stop.
It is.
And I'm normally not one for saying
that a move in the right direction isn't enough.
But a move in the right direction here isn't enough.
So what's going on is there is going
to be a 25% increase over the amounts that
were distributed prior to the public health issue.
There was a temporary 15% increase.
That's going away.
It's being replaced with a 25% increased.
And that's where we're at.
What this boils down to in numbers,
when you talk about it in percentages, it sounds good.
But in numbers, this boils down to about $36
per person per month.
All of a sudden, realizing it's just a little more than $1
a day doesn't really sound revolutionary.
It doesn't sound like a really big deal.
It brings the total average payment up to $157 per month.
Challenge yourself.
See if you can do that.
It's about $5.23 a day, more or less.
That's really not enough.
We're talking about food.
We're not talking about something that's optional.
And when you're talking about having to budget on that level,
what do you end up doing?
You end up getting the cheapest food available.
You end up buying junk food.
And then you have people who see that and say, oh, well,
people on SNAP, they just spend it on junk food.
It's because that's really kind of all they can afford.
Because it's $157 a month.
There's also the idea in the United States
that this is for outliers.
This is for people who just can't hack it here
in the real world, so on and so forth.
A lot of people's minds got changed
during the public health issue when suddenly people
who never thought they would need that kind of assistance
suddenly did.
The reality is that 42 million Americans
are beneficiaries of the program.
That's not 42 million people that need it,
because there are certainly people who need it
who don't qualify.
42 million.
That's a lot for a country that prides itself
on being number one, that prides itself on saying it's
the greatest place in the world.
42 million, more than 10% of the population,
substantially more than 10% of the population,
requires assistance to get base necessities.
This is how far we have shifted in the United States.
Herbert Hoover, Republican president, made that promise.
You know, a chicken in every pot and two cars in every garage,
or whatever it was, a chicken in every pot.
And now we're kind of at, well, you
can have chicken-flavored ramen noodles.
I don't think it's enough.
Sure, it's a move in the right direction.
Yeah, we can get behind that.
But this isn't the end.
It's like most minimum wage increases.
By the time they go into effect, they're already obsolete.
And that's where we're at here.
I would challenge you, if you think $157 is enough,
I would challenge you to try it.
Designate an area in your pantry, stock that area,
and you can only eat from there.
That means you can't use the cooking oil and stuff
that you already have in the kitchen, just that stuff.
It's going to be harder than you think.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}