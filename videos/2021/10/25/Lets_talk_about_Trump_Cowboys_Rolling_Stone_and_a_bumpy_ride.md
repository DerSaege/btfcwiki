---
title: Let's talk about Trump, Cowboys, Rolling Stone, and a bumpy ride....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=1A0VD0TrbGg) |
| Published | 2021/10/25|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Rolling Stone reported on rally organizers naming Congress members involved in planning the Capitol insurrection, catching headlines.
- Organizers claim innocence, painting themselves in a positive light, saying they had no idea what was going to happen at the Capitol.
- Cowboys for Trump founder Griffin turned on the former president in a speech, expressing feeling betrayed and sold out.
- The crowd Griffin spoke to, many present at the Capitol on the 6th, are becoming disaffected and feeling abandoned by their leaders.
- The Republican Party, especially those embracing revolutionary rhetoric, has heavily influenced this disaffected group.
- The radicalized individuals are feeling betrayed and are about to be publicly disavowed by their leadership.
- Those who believed in the lies and wild claims of the president are now facing the reality of being misled and betrayed.
- The committee investigating the Capitol insurrection must hold the responsible parties accountable to prevent future anger and radicalization.
- Beau urges disaffected individuals to put down guns, pick up books, and focus on civic engagement and community involvement.
- The manipulation by leaders who preyed on anger and ignorance can be combated by educating oneself and being involved in local community issues.

### Quotes

- "They lied to you. And they are going to continue to do so as long as it works."
- "Your best defense against the manipulation that they used is to become educated."
- "We are in for a very bumpy ride if this crew that has been radicalized ends up feeling sold out."

### Oneliner

Rolling Stone exposed rally organizers' involvement in Capitol insurrection planning, while Cowboys for Trump founder turns on former president, leaving disaffected supporters feeling betrayed.

### Audience

Disaffected individuals

### On-the-ground actions from transcript
- Put down guns, pick up books, and focus on civic engagement and community involvement (suggested)
- Educate oneself and get involved in local community issues to combat manipulation (suggested)

### Whats missing in summary

Full understanding of the consequences of manipulation, betrayal, and radicalization among disaffected individuals can be best grasped by watching the full transcript.

### Tags

#CapitolInsurrection #Disaffection #Betrayal #Radicalization #CommunityInvolvement #CivicEngagement


## Transcript
Well, howdy there internet people, it's Beau again.
So today we're going to talk about Rolling Stone, Cowboys,
the bumpy ride ahead, and how two seemingly unrelated stories
are very, very related.
They are going to collide in the future.
So the big story that's kind of dominating everything
was the report coming out of Rolling Stone that said,
they talked to two organizers of the rally on the 6th,
the one that was held prior to everybody going to the Capitol.
And those organizers dropped names,
and they said these members of Congress
were involved in the planning.
They said they had pardons dangled in front of them.
That's what's catching the headlines, right?
That's what the coverage is about.
Now these same organizers, in their narrative,
they paint themselves in the best possible light.
We had no idea this was going to happen.
We were just holding a rally.
We didn't know what was going to occur at the Capitol.
Get ready to hear that a lot.
So that's one story.
The other has to do with Cowboys for Trump,
the founder of that group, last name Griffin.
He gave a speech, and it's clear that he is
turning on the former president.
We supported President Trump
because of his fight for justice as well.
And for four years, we cried,
lock her up, lock her up, lock her up.
We know she's a criminal.
What did the president tell us?
If I was in charge of the law, you'd be in jail.
Mr. President, you've been in charge of the law
for four years.
At the end of your four-year time,
the only ones locked up were men like me
and others like me that have stood
by the president the strongest.
This is somebody who was at the Capitol on the 6th.
The people that he was talking to, that crowd,
a whole lot of them were at the Capitol.
They are becoming disaffected.
They are beginning to feel betrayed and sold out.
The story from the Rolling Stone,
that narrative where,
oh, well, yeah, I had something to do
with planning this other rally, but I had no idea.
Those people are just,
there's something wrong with them.
They did that on their own.
Nobody could have known.
You're going to hear that a lot.
This crowd of people that this cowboy was speaking to,
they're going to hear it too.
The Republican Party as a whole,
but specifically that subset, a lot of those named,
they've leaned real heavy into this revolutionary rhetoric.
They have ads where they're firing weapons.
The AR-15 might as well be a symbol
of the Republican Party at this point.
They talk about dying for their cause.
This crowd that the cowboy was speaking to,
they viewed these people as their leaders,
and they're about to watch their leaders sell them out,
disavow them, disown them,
say that what they did was wrong
because it's the only way those people in Congress
are going to save themselves.
So they're going to do that.
They're going to mock them.
Where did the anger come from?
When you're talking about those people
who believed the theories,
who really went all in on it,
who were real supporters of the president
and his wild claims,
where did their anger come from?
From the belief that they had been betrayed,
that everything they had been told was a lie,
that this group of people, they were going to fix that.
They were going to be different.
They weren't going to lie to them.
And as time goes on,
they are going to find out that it was those
who played on their fears,
those who created that anger, who lied to them,
who misled them for their own gain,
and who ultimately will betray them.
That puts us in for a really bumpy ride ahead.
This group of people,
they're in the process of selling out.
They are hyper militant.
They are radicalized.
They are angry and they're about to be betrayed.
That's how they're going to perceive it.
I can't imagine what might happen next.
This group of people,
the people that this cowboy was speaking to,
the people who were at the Capitol on the 6th,
they've already abandoned politics.
They are ready to pursue politics by other means.
And their leadership is about to sell them out publicly.
That is going to create a lot of angry people,
a lot of people who gave up four years with their families,
alienated their families in some cases,
lost friends, lost their jobs.
Some of them lost their freedom because they were duped,
because they got tricked.
And now people who were motivated to engage
in the activities that occurred on the 6th
through the mere suggestion with no real evidence
that they had been betrayed by the system
that they believed in,
they're going to watch live on C-SPAN
as these members of Congress sell them out,
disavow them, disown them,
say that everything that they believed was a lie,
even though in large part these members of Congress
are the people who created the lie.
The smartest thing for the people
who are pursuing this committee,
the committee that's investigating the 6th,
they have to follow this to its ultimate conclusion.
The people who were responsible,
they have to be held accountable for two reasons.
One is that if there's no accountability,
this wasn't a failed coup.
It was practice.
And the second is I think the only thing
that might be able to mitigate the anger
coming from a group that they radicalized
is them being held accountable in some way.
Now, I know there's a whole lot of people
who are going to be like, well, let them reap what they sow.
And I understand that to some degree.
But once that type of stuff starts, it doesn't stop.
We have to stop it from starting.
If you are one of those who is going to be disavowed,
who's going to be sold out,
and that's how you're going to view it, and I understand it,
put down the guns, pick up some books,
learn about civic engagement rather than target engagement.
Most of the problems that you face,
most of the problems that created the dissatisfaction
that those people preyed on and used to manipulate you,
they are problems that can be solved at the local level.
They are problems that could be solved
by you being involved in your community.
Had you been involved in your community
rather than traveling around the country going to rallies,
a whole lot of things might be different.
Those problems might be solved or at least mitigated.
They lied to you.
They lied to you.
And they are going to continue to do so as long as it works.
Your best defense against the manipulation that they used
is to become educated
and to start looking after your local community
rather than being concerned and being angry
about what people in D.C. tell you to be angry about.
They had you convinced that the billionaire
was anti-establishment,
that the man who gave Clinton's funding
was going to arrest them.
They did it by lying.
They did it by manipulating.
And they preyed on the anger and on the ignorance.
The lack of knowledge
when it comes to fixing the issues at the community level,
doing what you can to change things at your kitchen table.
They didn't tell you how to do that.
They just told you to be mad and pointed the finger
because they knew that, well, if you did that,
you'd give them money.
We are in for a very bumpy ride
if this crew that has been radicalized
ends up feeling sold out.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}