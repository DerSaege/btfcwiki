---
title: Let's talk about the jobs report, early unemployment, and wages....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=eyPNchSJG94) |
| Published | 2021/10/09|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Analyzing the recent jobs report that revealed 194,000 new non-farm payrolls added but a shrinking labor force by 183,000, upsetting expectations.
- People anticipated more growth post the expiration of unemployment benefits, leading to the lowest numbers all year.
- Suggests the low numbers may not be due to laziness but a blend of the ongoing public health crisis, reluctance to risk it, and inadequate wages.
- Points out the struggle of finding workers due to inadequate wages, with some stores having to close two days a week for lack of staff.
- Emphasizes the basic economic principle of supply and demand, urging businesses to raise wages to attract workers.
- Criticizes those who terminated benefits early and labeled workers as lazy freeloaders, indicating they only ended up harming hard-working individuals.
- Asserts that workers should not be blamed for companies seeking to extract more from them, urging business owners facing hiring challenges to simply increase pay.

### Quotes

- "You have to pay more. It is that simple."
- "It is not the workers' fault that companies want to extract more and more from them."
- "If you're running into this issue, if you own a business and you're trying to get workers and you can't, bump the pay. It's really that simple."

### Oneliner

Analyzing disappointing job numbers post-benefits expiration, Beau stresses the need for higher wages to attract workers, debunking stereotypes and advocating for fair compensation.

### Audience

Business Owners, Workers

### On-the-ground actions from transcript

- Increase wages to attract workers (suggested)
- Support businesses paying living wages (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the impact of low wages on hiring challenges and advocates for fair compensation to address workforce shortages effectively.


## Transcript
Well howdy there, Internet people. It's Beau again.
So today we're going to talk about the jobs report
and
expectations
and talking points
and how, well, it doesn't seem like that's how things worked out.
The jobs report's out. There were 194,000 new non-farm payrolls added.
The labor force shrank by 183,000
and that upset expectations. That's not what people wanted to happen.
They were expecting much more growth,
mainly because all of those freeloaders,
well they weren't going to be getting those
juicy unemployment benefits
that had been put in place because of the public health issue, right? They ended on
Labor Day,
as a sick joke.
Rather than encouraging
growth,
these are the lowest numbers all year.
Maybe
it wasn't that people were lazy.
Maybe that's not what it is.
Maybe
it's a combination of the current public health issue and people not wanting to
risk it
and the fact
that wages are too low.
It isn't that people don't want to work. It's that they don't want to work for
eight dollars an hour.
There are stores near me
that are shutting down two days a week
because they can't find people to work. That's what they're saying.
Yeah, and none of them,
none of them pay a living wage.
It's supply and demand.
The
demand
for workers is high. The supply is low.
You have to pay more.
It is that simple.
All of that bluster,
cutting benefits for people early,
all of that bluster, calling them
lazy and, you know, freeloaders and all of that stuff,
looks like none of it was true.
It looks like those who pushed that
and terminated those programs early,
all they did was hurt hard-working people.
It is not the workers' fault
that companies want to extract more and more
from them.
If you're running into this issue, if you own a business
and you're trying to get workers and you can't,
bump the pay.
It's really that simple.
You don't need
an advanced degree
to figure that out.
Anyway,
it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}