---
title: Let's talk about the difference between the parties and Trump's legacy....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=-ebDERX32Tk) |
| Published | 2021/10/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Contrasts the Democratic Party as a coalition of diverse groups with the Republican Party as traditionally conservative.
- Democrats comprise liberals, moderates, leftists, environmentalists, etc., competing for policy influence within the coalition.
- Republicans are predominantly conservatives who focus on tradition and the idea of reverting to the past.
- Republican branding often relies on nostalgia and a desire to recreate past eras like "leave it to Beaver" or "Andy Griffith."
- Historically, Republicans avoided attacking each other and instead focused on attacking Democrats, relying on the "11th Commandment" to maintain unity.
- Trump broke this unity by not following the "11th Commandment" and attacking fellow Republicans, leading to internal divisions.
- The Republican Party's platform is summarized as pro-second amendment, against family planning, and against anything perceived as socialist.
- Beau suggests that without Trump's ability to rely solely on nostalgia and fear, Republicans will be forced to develop actual policy.
- Trump's divisive actions, like criticizing DeSantis, are pushing the Republican Party towards developing clear policy positions.
- Beau concludes that Trump's legacy may be the destruction of the Republican Party's strength in avoiding the need for substantial policy.

### Quotes

- "It's a coalition. What's the Republican Party? Conservatives, right?"
- "All Republican candidates have had to do in the past is harken back to the days of leave it to Beaver and Andy Griffith."
- "He didn't abide by the 11th Commandment."
- "He'll be that afraid. Pushing that division within the party, it's going to force them to develop policy."
- "A man who truly cared about his legacy, wanted his name on everything, his legacy will end up being the destruction of the Republican Party's greatest asset."

### Oneliner

Contrasting the Democratic coalition with traditionally conservative Republicans, Beau predicts that Trump's divisive actions may force the Republican Party to develop concrete policy instead of relying on nostalgia.

### Audience

Political observers

### On-the-ground actions from transcript

- Analyze and understand the policy positions of political parties (implied)
- Stay informed about internal divisions and policy developments within political parties (implied)
- Encourage political engagement and critical thinking about party platforms (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of how Trump's actions have affected the Republican Party's reliance on nostalgia and lack of clear policy positions.

### Tags

#PoliticalParties #RepublicanParty #DemocraticParty #TrumpLegacy #PolicyDevelopment


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about the key
differences between the Democratic Party and the Republican Party and what Trump's
legacy within the Republican Party is likely to be. The standard joke about the
differences between the two parties is that the Democratic Party is the party
of new ideas and the Republican Party is the party of no ideas. Now if you're a
Republican, bear with me on this because generally speaking that's totally true.
It is and I could show it. When you think about the Democratic Party, who are they?
Who makes up the Democratic Party? It's a coalition, right? It's not a party based
on tradition. It's a coalition. You have liberals, moderates, economic leftists,
social progressives, corporate interests, environmentalists, a whole bunch of
different facets to the party that are competing with each other in trying to
get their ideas pushed forward and they also need the support of the others in
order to get that policy moved. That's the Democratic Party. That's why there's
not any unity is because it's not united. It's a coalition. What's the Republican
Party? Conservatives, right? Single word, conservatives. They are a party that is
based on tradition. The whole idea behind Republican branding is to say that we
want to turn back the clock. They don't need new ideas. They can be the party of
no ideas. It's literally their branding. What they say is it was better 10 or 20
or 30 years ago. We want to turn back the clock and do this old thing. It's even
in the slogan, make America great again, back then, right? All Republican
candidates have had to do in the past is harken back to the days of leave it to
Beaver and Andy Griffith. That's all they've had to do and they've been able
to get away with this and not have any real policy because of the 11th
Commandment as popularized by Ronald Reagan, thou shalt not speak ill of another
Republican. Because they never took swings at each other and they just
focused solely on attacking Democrats and they never really pushed any
internal differences, they never had to have any policy. They could just appeal
to some image of a time before, some nostalgia, some idealized version of the
past that never existed anyway. They can appeal to that and say, oh, we're going to
make it like that again. And that's all they needed. Then if everybody was
doing that, all they had to do was rely on the R after their name on the ballot.
They used fear. Fear of the unknown, of the future, things changing, something
that's very natural. But see, Trump changed all that. He didn't abide by the
11th Commandment. He's not a fan of the commandments in general, I guess. Because
of that, they have started taking swings at each other.
RINOs, Republicans in Name Only, which really means they weren't supporting
Trump. But, I mean, as far as who's a Republican in Name Only, all of them. All
of them. Because there's no real policy to be united behind. Republican Party is
what? In favor of the second, against family planning, and anything that helps
the middle class or poor people is socialism. That's the whole policy. That's
the whole platform. As long as they abide by that and say that things were better
way back when, they get the votes. But if Trump continues to do what he has done
in the past and is still doing now, they're gonna have to develop policy.
Because they're not going to be able to rely on that. You know, he just took a
swing at DeSantis because he's, you know, Trump's terrified that DeSantis is
going to primary him. He doesn't want to face him in the primary. So he's getting
out there now saying that, you know, if DeSantis runs, the governor of Florida runs,
well, I'll beat him. I'll beat him. He'll just drop out. He'll be that afraid.
Pushing that division within the party, it's going to force them to develop
policy, which is going to be very new for Republicans. And in a way, Trump is
undermining the Republican Party's greatest strength. The fact that they
don't have to do anything. All they have to do is stand in the way of progress
and say that it was better back when it really wasn't. That's it. They play into
the mythology of America and say that we'll somehow turn back the hands of
time and they could win votes that way. But if you have a force like Trump
dividing that and saying that, oh no, it's not like that. They're not abiding by
that policy and dividing that up, violating that 11th commandment, they're
going to have to actually talk about policy eventually because the spell that
Trump had over people, it's fading. So at the end of this, a man who truly
cared about his legacy, wanted his name on everything, his legacy will end up
being the destruction of the Republican Party's greatest asset. Anyway, it's just
a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}