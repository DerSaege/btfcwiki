---
title: Let's talk about context, sources, and links....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=VVagD3FVPBg) |
| Published | 2021/10/04|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Responds to criticism about not providing sources or links below videos.
- Believes in viewers' ability to fact-check using search engines.
- Acknowledges the importance of context in understanding information.
- Points out the dangers of falling into information silos created by misleading links.
- Mentions a video by Dr. Kennard critiquing Beau's content on cops resigning in Massachusetts.
- Talks about the importance of verifying information and looking at multiple sources.
- Mentions a story about a Florida deputy planting evidence and getting caught.
- Comments on the unreliability of sourcing alone.
- Recognizes Dr. Kennard's effort to ensure accurate information for viewers.
- Expresses different perspectives on linking sources in videos and the risks of misinformation.

### Quotes

- "If you really want to understand something you can't just follow a link that the person who makes a claim provides you."
- "The reporting lines up with the anecdote and the town gossip."
- "Just because there's a link down below doesn't mean the information is true."
- "There's too much money in misinformation."
- "Nobody's right in this case. It's, well, it's just a thought."

### Oneliner

Beau addresses criticism about sourcing, stressing the importance of context and caution with provided links, discussing a video by Dr. Kennard and differing views on ensuring accurate information.

### Audience

Content Creators, Information Consumers

### On-the-ground actions from transcript

- Fact-check information from online sources (implied)
- Verify information by looking at multiple sources (implied)
- Be cautious with provided links and do further research (implied)

### Whats missing in summary

Importance of critical thinking and independent verification in navigating online information.

### Tags

#Sourcing #FactChecking #Context #InformationSilo #Misinformation


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
context and sourcing and a video that one of y'all sent me of somebody sitting there
making fun of me. Somebody basically had a real issue, said it was sloppy and that I
needed to do better and it was a yellow flag because I don't put sources down below. I
don't put links down below. It's a person named Dr. Kinnard over on a channel called
Laughing in Disbelief. And you know, he says basic high school stuff. And yeah, I mean
that's true. If I was putting a report together for high school I would probably include proper
citations. But this isn't high school. And I would point out that my kid that's in the
first grade knows how to use Google. Knows how to use a search engine. I would imagine
that most of y'all do too. And if you are a long time viewer of the channel you know
I've already done a video on this topic. I want you to fact check. I think that context
is really important. If you really want to understand something you can't just get, you
can't just follow a link that the person who makes a claim provides you. Because you know
that link is going to back up their story. You know, every channel on YouTube that promotes
just wild theories, they all have links in the description. And it's a way that in some
cases can send you into an information silo that is very hard to get out of. Because you
get a bunch of information that reinforces itself, but none of it's true. So I don't
provide links unless the information is incredibly hard to find. Like a link to an obscure study
or something like that. Most times I just count on your ability to use a search engine.
So you can fact check it for yourself. Now he went through a recent video about cops
in Massachusetts resigning. And he pointed to a couple of different things. The first
was my belief that generally small rural departments have less issues than larger departments.
Specifically those around me. Now is this fact? Can you verify this? Not really. You
really can't. I mean I know it's true actually through the methods that he suggests in his
video. But there's no way for you to because I don't say the exact jurisdiction. So even
if you trust me, how should you treat that information? Is it fact or is it opinion?
I know it's fact but I can't prove it to you. It's opinion at best. And then he points to
an anecdote. A story I told about a deputy who was planting stuff on people and got busted
and got a lengthy sentence. I'm pretty vague in that. I don't mention the jurisdiction.
But that's kind of a big deal. So I'm willing to bet, and by that I mean I already looked,
if you were to go to your favorite search engine and type in Florida deputy planting
sentencing, you'll get the story. And it will even, it kind of closely lines up with town
gossip as well. You know I say that the DA reached out to the sheriff. In the articles
that I saw, it says that there were rumors in the courthouse. And it even mentions how
they kind of tricked the cop to get him out of his car so they could search it. The reporting
lines up with the anecdote and the town gossip. It's not exact but it's pretty close. So that's
verifiable. And it doesn't take much to do it. Even if the starting point is vague, there's
a way to verify the information. And you should always seek to do that. And look at a couple
of different sources. Never just trust the link that's provided. Because you know I happen
to know a guy who did a video talking about that report saying that 20% of cops up there
in Massachusetts were going to resign over the vaccines. It's this guy by the way. And
he had a link to an NPR article. NPR, pretty reliable. But it certainly appears that the
reporting was off in this case. They were just going off of what the union said. Maybe
the union wasn't telling the truth. So the source alone doesn't mean anything. I think
y'all all know a guy who put out a video talking about how the CDC in FEMA handles public health
issues overseas and how you don't need to worry about it coming here because they're
really good at stopping it. And to this day it's probably still one of the most embarrassing
videos I've ever made. The sourcing is there. These are the guidelines. These are the plans
that these agencies had. There's no way to foresee that they're not going to use them
at all. So sourcing alone doesn't actually provide a whole lot of guarantee that it's
true. You need the context. You need to know the other stuff that's going on. And you need
to be able to come back to it. Now there's two things I want to point out about this
Professor Kennard, Dr. Kennard, whoever he is. First is that he has a follow-up video
to the thing about Massachusetts where he corrects it and provides the information that
was provided by the state. And the other is that he totally knows I'm making this video.
This isn't a bad faith criticism. This is a case of diversity of tactics. We look at
this differently. I'm assuming that his point of view is to make sure that anybody who's
watching a video can go down below, click it, follow the link, and get accurate information.
To me, I don't like that because it presumes that the person making the video is going
to link accurate information. And they may not. And those information silos that we have
seen develop over the last few years, they are dangerous. So I don't think that you should
necessarily follow the links that are provided by people on YouTube. Maybe use them as a
starting point. But just because there's a link down below doesn't mean the information
is true. And just because the link, if you follow it, it says the same thing, doesn't
necessarily mean that it's fact. We have to go beyond that now. There's too much money
in misinformation. And I take no offense to this video that was made. Because the goal
of it is to get out accurate information and to keep people from falling into information
silos. I am all about that. I think it's a great idea. We just have a different viewpoint
in how to accomplish it. Nobody's right in this case. It's, well, it's just a thought. Anyway, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}