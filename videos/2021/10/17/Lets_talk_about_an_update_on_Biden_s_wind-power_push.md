---
title: Let's talk about an update on Biden's wind-power push...
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=RZ0uNIxqSIU) |
| Published | 2021/10/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden administration expanding offshore wind push from three to seven lease sites.
- Goal of achieving 30 gigawatts of offshore wind power by 2030 set by Biden administration.
- Environmental studies, military operations impact, and underwater archaeological sites considerations before leasing sites for wind turbines.
- Private companies to lease designated areas for offshore wind power production.
- Potential opposition from fishing groups and offshore drilling companies, but unlikely to form a united front.
- Comparison with China's goal of 73 gigawatts and the UK leading in offshore wind power.
- Biden administration opening up the entire US coast for offshore wind projects with certain restrictions.
- Anticipated pushback from tourist organizations and businesses reliant on tourism.
- Possibility of overcoming challenges with proper planning and execution before Biden's first term ends in 2024.
- Overall, a positive step towards clean energy, although more significant steps may be necessary.

### Quotes

- "Either way, it's a move in the right direction."
- "So far, it looks good."

### Oneliner

Biden administration expands offshore wind push, facing challenges in achieving 30 gigawatts by 2030, but moving in the right direction.

### Audience

Environmental advocates, energy policymakers.

### On-the-ground actions from transcript

- Support offshore wind power initiatives by advocating for clean energy policies (suggested).
- Stay informed about offshore wind projects and their impact on the environment and local communities (implied).

### Whats missing in summary

Detailed insights on the specific environmental impacts and community engagement aspects of offshore wind projects.

### Tags

#OffshoreWind #RenewableEnergy #BidenAdministration #CleanEnergy #EnvironmentalPolicy


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we are going to talk about
the Biden administration's wind push, offshore wind.
We talked about it before.
I want to say when we talked about it the first time,
they were looking at three sites.
That is now up to seven.
That's now up to seven different lease sites.
So the Biden administration set a relatively ambitious goal,
for the United States anyway, of 30 gigawatts.
They want to achieve that and be producing that
via offshore wind power by 2030.
That's enough to power roughly 10 million homes.
Now, the Biden administration is completely aware
of the limitations of counting on the Republican Party
to continue any initiatives they set forth.
So the Biden administration wants all the leasing,
everything to be completed before the end
of Biden's first term in 2024.
That's going to be difficult.
That's going to be hard because there
are a lot of environmental studies that have to be done.
And they have to look at how it impacts military operations,
underwater archaeological sites, so on and so forth.
Right now they're looking at seven lease sites.
Now, what that means is they will
designate these areas and then they will lease them
to private companies to put wind turbines out
to power areas nearby.
There will probably be opposition
from both fishing groups and, at least here in the Gulf Coast,
along the Gulf of Mexico, offshore drilling companies.
The good news is they won't really
be able to form a united front.
The fishing organizations, those that are very political,
they may take a stance against the wind turbines.
However, they won't be able to align with the offshore
drilling, especially down here after, well, I mean,
let's be honest, the offshore drilling
is pretty bad for fishing.
So they won't be able to present a united front.
I don't think they will be able to mount serious opposition
to it.
Not enough to matter anyway.
So 30 gigawatts.
You know, in the United States we are making that seem
like it's a big deal.
China is looking at trying to get to 73.
We are behind when it comes to this.
I want to say the UK is leading us as well.
We're behind.
It's important.
The Biden administration has kind of opened up
the idea of basically the entire coast of the United States.
Pretty much everything is fair game,
as long as it doesn't impact endangered species,
create environmental hazards, interfere
with military operations, disrupt native operations,
stuff like that.
It's fair game.
There will probably be some pushback
from certain tourist organizations as well,
groups that represent businesses that
make their money off of tourism.
In Florida, one of the things that they pushed for
is to make sure that the rigs were so far that you couldn't
see them from the coast.
I don't see why that couldn't be done with the turbines as well.
So I don't think that that's going
to be something that's going to be
hard for the Biden administration to overcome
either.
So far, it looks good.
The real hard part is getting all of the studies,
all of the evaluations, everything done
before the end of his first term.
That's going to be the difficult part.
That's going to be the part they're
going to have an issue with.
We'll see how it plays out.
Either way, it's a move in the right direction.
It's a big step, but still probably not
as big as it needs to be.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}