---
title: Let's talk about a crashed tractor and unskilled labor.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=40GTOqOyedA) |
| Published | 2021/10/17|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the term "unskilled labor" and argues it is a misnomer.
- Shares an incident where salaried employees were asked to replace striking workers at John Deere, resulting in a mishap.
- Challenges the idea that jobs labeled as unskilled truly require no skill.
- Mentions that even service industry jobs like waiting tables require time to develop the necessary skills.
- Criticizes the term "unskilled labor" for devaluing the work and justifying low wages.
- Asserts that most jobs require learning and skill development, even if they don't demand formal education.
- Urges for the retirement of the term "unskilled labor" due to its role in diminishing the value of workers' contributions.

### Quotes

- "The idea of unskilled labor, it's a fiction."
- "It's just a method of devaluing the workers."
- "There aren't a whole lot of jobs you can walk into and just do it."
- "If somebody is putting in the work, they should probably have access to a decent life."
- "The skills are required."

### Oneliner

Beau challenges the concept of "unskilled labor," arguing that all work requires skill development and deeming the term as a method to devalue workers.

### Audience

Workers, activists, labor advocates

### On-the-ground actions from transcript

- Advocate for fair wages and recognition of skill in all types of work (implied)
- Support labor movements and strikes to ensure workers are valued for their contributions (exemplified)

### Whats missing in summary

The full transcript includes Beau's engaging and relatable anecdotes that effectively illustrate his points and add a personal touch to his argument. Viewers can gain a deeper understanding and connection through these stories. 

### Tags

#LaborRights #SkillRecognition #FairWages #ValueOfWork #Activism


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about a term that we hear pretty often.
We hear it on the news all the time, especially during the business segments.
And we might even use it every once in a while.
And we're going to talk about that term and why we should probably stop using it.
Today, we are going to talk about unskilled labor and why it isn't.
It's not unskilled.
We're going to do this because something happened and honestly, it's been cracking me up ever
since I saw it.
I kind of giggle every time I think about it.
If you don't know, the workers at John Deere, well, they're on strike.
Management's response to the strike was to tell the salaried employees, generally white
collar employees, well, y'all just go fill in.
It didn't even take them a full day before they crashed what appears to me, based on
the photo, to be about a $350,000 tractor inside the plant.
I'm certain that whoever has the job of moving that equipment around inside there, I'm certain
that that's unskilled labor.
But it's really not.
It takes skill to do it.
And it's not just manufacturing jobs or blue collar jobs the way we typically think about
them, even in the service industries.
Think about waiting tables.
You go in and sit down at a restaurant and the waiter or waitress, they're just not up
to it.
Something's off.
They're forgetting items on the menu, don't know the special, forget to bring the drinks,
whatever.
What do you normally say?
They must be new, right?
Why?
Because it takes time to develop the skills to do that job right.
The idea of unskilled labor, it's a fiction.
There aren't a whole lot of jobs you can just walk into and do well.
It takes time.
So why does that term get used?
It doesn't match up to reality.
Why does it get used?
Because it helps devalue the work that is done by those people.
It helps devalue it.
Therefore, when you find out they're only making eight bucks an hour, you can say, oh,
well, they should have got a trade.
They should have gotten education.
No.
I mean, I don't think that that's a fair statement.
It's a job that people want done.
And if somebody is putting in the work, they should probably have access to a decent life.
The whole idea of saying something is unskilled labor is to diminish the value and diminish
the pay that the workers get.
It's a term that needs to go away.
There are not a whole lot of jobs you can walk into and just do it.
You have to learn what you're doing.
It is skilled.
May not be credentialed.
You may not have to go to school for it.
But the skills are still required.
Because any time you go somewhere and you know that that person doesn't know what they're
doing because they don't have the skills, you think they're new.
You don't go back.
The skills are required.
It isn't unskilled labor.
It's just a method of devaluing the workers.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}