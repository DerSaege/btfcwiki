---
title: Let's talk about Biden's surprising immigration announcement....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=J3MvLKtaMas) |
| Published | 2021/10/01|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Describes a shift in U.S. deportation policy prioritizing certain groups post November 2020.
- Acknowledges 11 million undocumented individuals as contributing members of communities.
- Allows DHS to focus on those actively involved in criminal activity or with serious criminal histories.
- Recognizes the broken immigration system and the temporary nature of the current policy.
- Predicts the Republican Party will use this shift as a talking point, claiming it's akin to open borders.
- Emphasizes that the policy targets individuals already in the U.S., not incentivizing new arrivals.
- Anticipates court cases but doubts their success due to the legality of establishing enforcement priorities.
- Views the policy as a positive indication of potential immigration reform efforts by the Biden administration.
- Expects intense political debate leading up to the midterms due to the shift benefiting the Republican Party.
- Urges individuals to defend the decision and stand up for undocumented community members.

### Quotes

- "This is for those people who are now valued members of our communities."
- "It prioritizes DHS's resources and puts them where they matter."
- "It's up to them to make this case and say they have been here, that they are part of our communities and defend this decision."

### Oneliner

Beau describes a shift in U.S. deportation policy, prioritizing certain groups and defending undocumented community members against political attacks.

### Audience

Advocates for undocumented immigrants.

### On-the-ground actions from transcript

- Defend undocumented community members (implied)
- Make the case for their contribution to communities (implied)

### Whats missing in summary

Detailed analysis and historical context of U.S. immigration policies. 

### Tags

#USImmigration #DeportationPolicy #BidenAdministration #UndocumentedImmigrants #PoliticalDebate


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about the American Fabric
and what it is.
And we're gonna talk about a shift coming
from the Biden administration
that I don't think a whole lot of people saw coming.
We're gonna talk about the press release
that is describing that shift
and the probable responses
that will come from the Republican Party
because of this shift.
there will be a lot of them. If you don't know, a statement came out from the
Department of Homeland Security that describes a shift in U.S. policy
concerning who is a priority for deportation. And it prioritizes those
people who arrived after November 1st, 2020, those who are actively involved in
criminal activity, and those who have serious criminal histories, those who
impact national security or border security. Those are the priorities. And
it deprioritizes everyone else. The press release acknowledged the 11 million
people who have come to the United States who don't have documentation, who
are now part of the American fabric refers to them as contributing members
of our communities. Doing this allows DHS to focus their resources on those who
are here who are creating victims. If they are not really going after everyone
else, they have more resources to devote to them. It provides a light shield for
those who have come here, who have been here for years, who have found a way to
weave themselves into the American tapestry. They're not a priority. This
is well within the bounds of a policy directive like this. The establishment of
priorities for enforcement is something that can be done via memo, the exact way it was.
Now the problem with this is that it's not law, this isn't something that will stick
around if say a Republican is elected in 2024, it will go away.
But for the time being this exists, it's an acknowledgement of how broken the U.S. immigration
system is.
The Republican Party just got handed a talking point, and they're going to use it
over and over and over again. What they're going to say is that this is akin to open borders.
It's not. And when you hear that, what I would point out is that it prioritizes deportation of
those who arrived after November 1st, 2020, meaning that there's no incentive in this to come now.
This is for those people who have been here. So when you see that talking point, and it will
certainly be used, remember that. The Biden administration was pretty clear. It doesn't
actually cover those who just showed up or those who will show up in the future. Has nothing to do
with that. This is for those people who are now valued members of our communities. There will
will certainly be court cases over this.
I don't think they'll be very successful.
This is something that is allowed.
This is something that can be done.
I would take this as a good sign that the Biden administration does plan on pursuing
real immigration reform because they know as well as anybody else that as soon as the
next administration comes in, this is gone.
If a Republican walks in, this is going to be one of the first things that gets changed.
It prioritizes DHS's resources and puts them where they matter.
They're not going to be going after people who work at hospitals.
They're not going to be going after people who really have become part of the United
States in everything except for their papers.
So I would be ready to hear a whole lot about this as we move towards the midterms.
Because this was a shift that is definitely handing the Republican Party a tool.
It's up to those people who don't believe that outside of the U.S. are a lesser people.
It's up to them to make this case and say they have been here, that they are part of
our communities and defend this decision because it's going to be attacked a lot.
Anyway, it's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}