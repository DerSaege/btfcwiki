---
title: Let's talk about when Steve Bannon will get arrested....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=I6K7bAs1msI) |
| Published | 2021/10/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- House panel voted unanimously to hold Steve Bannon in criminal contempt for defying subpoenas.
- Bannon has been defiant by not turning over documents or being deposed.
- After the house panel vote, it goes to the full house for certification, then to federal law enforcement.
- Federal law enforcement decides whether to proceed after reviewing the report.
- If they decide to proceed, it then goes to a grand jury.
- Only if the grand jury decides to proceed might Bannon get picked up, so it's not immediate.
- The charge is not huge, with a minimum sentence of 30 days and up to a year.
- The House panel seems serious about playing tougher with those obstructing the process.
- They aim to send a message to others considering obstructing.
- The House panel believes there's more information they don't know, hence their strong actions.
- Bannon being in trouble is a bad sign for former President Trump.
- Bannon might have information that could alter or reinforce existing beliefs.
- House panel believes Bannon has critical information that could change the narrative.
- Cooperation might not stop the proceedings once it reaches the Department of Justice.
- House panel believes Bannon holds information that could provide more insight into events.

### Quotes

- "House panel voted unanimously to hold Steve Bannon in criminal contempt."
- "The House panel seems serious about playing tougher with those obstructing the process."
- "Bannon might have information that could alter or reinforce existing beliefs."

### Oneliner

House panel votes Bannon in contempt, signaling tougher stance on obstruction and potential new information.

### Audience

Political observers, justice advocates.

### On-the-ground actions from transcript

- Stay informed on the developments in Bannon's case (implied).
- Support organizations advocating for transparency and accountability in government actions (implied).

### Whats missing in summary

Insights on the potential implications of Bannon's case on broader political narratives.

### Tags

#SteveBannon #Contempt #HousePanel #Obstruction #Justice #Accountability


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about
when Bannon's gonna get picked up, Steve Bannon.
Because honestly that's like 80% of my inbox from last night.
People asking when he was actually gonna be, you know,
placed in the back of a car.
Okay, so if you don't know what I'm talking about,
the house panel that is looking into the sixth,
they voted unanimously to hold Steve Bannon
in contempt, criminal contempt at that.
And this is because he has been pretty defiant
when it comes to the subpoenas.
Not turning over documents, refusing to be deposed,
that kind of stuff.
Not a lawyer, but yeah, I mean that sounds like contempt to me.
So the house panel unanimously voted to hold him
in criminal contempt.
So he gets arrested, right?
No.
The reason there's this confusion is because there was
kind of some lax coverage on this
out of all the articles I read.
I only read one that lays out what happens next.
So after the house panel votes, okay, which that's happened,
then it goes to the full house,
and that's gonna occur on Thursday.
It's probably going to pass.
So at that point, the Speaker of the House
will have the option of certifying it, okay?
Once that happens, he gets arrested, right?
No.
Then it goes to federal law enforcement.
And at that point, federal law enforcement decides
whether or not to go forward with it.
They review the report, and they're like, okay, yes or no.
If they decide yes, that's when he gets arrested, right?
No.
Then it goes to a grand jury.
And if the grand jury decides to proceed,
that's when he might get picked up.
So this isn't an immediate thing.
It doesn't occur quickly.
So those who are waiting for it,
you may be waiting a little bit.
Now, the other thing to note is that this is...
It's not a huge charge.
Realistically, I think the sentencing
is a minimum of 30 days and up to a year.
So it's not a huge deal,
but it's definitely enough to get people's attention,
and I kind of think that's what this is more about.
There are a couple of takeaways from it.
One is that it seems as though the House panel
looking into the Sixth, well, they're done.
They're done playing around.
They plan on playing a little bit tougher with this.
And they're trying, in my opinion,
trying to send that message to anybody else
who might be considering following
the former president's advice
and obstructing this process,
refusing to cooperate, so on and so forth.
Kind of a way of saying,
this is what you're going to be looking at.
Aside from that, it's a pretty bad sign
for the former president, for former President Trump,
because what this really tells us
is that that House panel believes there is...
there's information they don't know.
There's information about the events
that they don't have yet,
and they feel pretty strongly about that,
because they're not going to go through this
and then be embarrassed if Bannon doesn't have anything.
So my guess is that they have strong reason to believe
that there is more to the story than we currently know,
and that's why they're going all out on this one.
So, in short, the former president's
former strategist and advisor,
he's in a little bit of trouble.
I do think that this will proceed through the steps.
Now, maybe if he throws himself on the mercy of the panel today,
he can avoid some of this.
Once it leaves the House and goes to DOJ,
there's not much that can be done.
I don't think, even if he changed his mind at that point
and decided to cooperate,
I don't think it would necessarily stop the proceedings.
So I definitely think that the House panel
believes that Steve Bannon has information
that might alter the story,
that might reinforce what a lot of us may believe,
but can't necessarily prove.
And I think that they believe
Steve Bannon has that information.
So until then, it's kind of a waiting game.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}