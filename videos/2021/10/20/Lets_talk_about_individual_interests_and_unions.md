---
title: Let's talk about individual interests and unions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Yup2rH4OsKY) |
| Published | 2021/10/20|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Exploring individual interests and collective interests, unions, and representation.
- Answering a question that starts off strong but repeats weak arguments from others.
- Contrasting the narrative of representing oneself with the reality of right-wing talking heads having agents.
- Pointing out the hypocrisy of those who advocate against representation while having agents themselves.
- Emphasizing the importance and power of collective representation through unions.
- Describing how collective interest through unions benefits both individuals and the group.
- Arguing that unions empower workers against management's dominance.
- Supporting the idea of unions as vital for balancing power dynamics in today's world.

### Quotes

- "They have somebody who takes a cut of what they make to represent them."
- "That collective interest is your individual interest."
- "It gives you more power at the bargaining table."
- "The only way you're going to be able to do that isn't by putting on your best suit and walking into the manager's office."
- "It's through collective power to achieve your individual interests."

### Oneliner

Exploring collective vs. personal interests, Beau dismantles anti-union rhetoric, advocating for unions as vital for empowering workers against management. 

### Audience

Workers, activists, advocates

### On-the-ground actions from transcript

- Join a union to collectively advocate for your rights (exemplified)
- Educate yourself on the benefits of unions and collective bargaining (suggested)

### Whats missing in summary

The full transcript provides a comprehensive analysis of the importance of unions in representing workers' interests and balancing power dynamics in the workplace.

### Tags

#Unions #CollectiveInterests #PowerDynamics #WorkerEmpowerment #Representation


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk about
individual interests and collective interests and about unions and
representation and stuff like that. We're going to do it because I've got a pretty
entertaining question. It starts off like it's going to ask a really good question
and then it repeats arguments from other people that aren't so good. So what we're
going to do is we're going to answer the questions, the question that's actually
asked and then we're going to steel man the message and ask what I think is
actually the question they want to ask. Okay so, please don't monkey-paw me. That
makes sense if you follow me on Twitter. I'm a right-leaning independent and
don't exist in an information silo. Something you said has stuck in my head
about voting against your own personal interests. How do you square that with
your support of unions? I've never been in a union myself but your talking head
counterparts on the right describe it as somebody taking a cut of your pay to
represent your interests to management when you're always better representing
yourself directly in any deal. They have tons of arguments to justify always
representing yourself. I can provide them if you'd like. No thank you.
See, I know a lot of right-wing talking heads. I know a lot of them and I
know a couple of the bigger ones. The bigger ones, they all have one
thing in common in addition to bad politics. An agent. They all have an agent.
They all, all of the people who are telling you that you don't need
representation, that you should represent yourself directly to your employer, they
all have an agent. They have somebody who takes a cut of what they make to
represent them. It's another case where the right wing, who often tries
to fashion themselves as representative of the working class, where the people at
the top, your betters, well it's okay for them but not okay for you. You, as a
worker bee, you shouldn't have representation. You need to represent
yourself directly and present yourself directly to your betters and beg and
plead for what you want. Where, you know, they can have somebody they reach out to
over a cell phone to negotiate their deals for them. But to me, that's the
weaker part of this. You started off with something you said has stuck in my head
about voting against your own personal interest. How do you square that with
your support of unions? It seems like up to this point you're going to ask about
the difference between collective and personal interests when it comes to
unions. To me that's far more interesting than pointing out the
hypocrisy of right-wing talking heads. You're not actually always better
representing yourself. If you are lower in the chain of a company, well you're
kind of replaceable. They call you unskilled labor, you don't matter.
They'll give you a pizza party, they will call you essential, but they're not going
to give you any extra money. I mean that would be silly. However, if a whole bunch
of you are like, this is what we need, and you designate people to say that to
management, and they're representing a large chunk of their workforce, that
collective interest is your individual interest. It benefits you individually
while it benefits the group. You are putting in your chip and saying, okay I'm
going to go in on this, and everybody else is too. It gives you more power at the
bargaining table. That's literally the whole point behind a union, is to use
collective power to achieve individual interests. So that's why you pay dues.
The idea of taking a cut of your money, understand when you go on strike, you get
strike pay. It isn't quite the way it often gets portrayed by right-wing
talking heads. Yeah, I'm a very strong supporter of unions. I think that
especially in today's world, it's very important for labor to organize, to
use that collective power that they have, when management and the company
itself can exert so much power. You have to be able to match it, and the only way
you're going to be able to do that isn't by putting on your best suit and walking
into the manager's office. It's through collective power to achieve your
individual interests. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}