---
title: Let's talk about Chicago cops....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=wxGAhd9LQ8g) |
| Published | 2021/10/14|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The Chicago police union advised officers not to disclose vaccination status, defying city requirements.
- Unions typically oppose non-pre-negotiated issues, defaulting to opposition.
- Police unions differ significantly from other unions.
- Ignoring vaccination policy raises concerns about following other public safety policies.
- Lack of vaccination value in law enforcement questions officers' judgment.
- Officers regularly face situations where vaccination is vital for public safety.
- Non-compliance with vaccination poses higher infection risk for officers and community members.
- The mission of law enforcement is public safety; non-compliance suggests a need for a different profession.
- Union concerns about firing officers gradually rather than all at once.
- Non-compliance with public safety measures is an easy way to filter out unsuitable officers.
- Society faces a choice: be part of the solution or follow propagandized talking points.
- The Chicago union made its choice; the city's response is critical.

### Quotes

- "This is a really easy way to weed out bad officers."
- "Do you want to protect your neighbor? Or do you want to go with a propagandized talking point?"
- "If they're not willing to commit to that mission, they probably need a different profession."

### Oneliner

The Chicago police union's defiance on vaccination status reveals deeper issues with public safety and judgment in law enforcement.

### Audience

Law enforcement officers

### On-the-ground actions from transcript

- Advocate for public safety measures within law enforcement (implied)
- Support policies that prioritize public health and safety (implied)

### Whats missing in summary

The full transcript provides additional context on the implications of the Chicago police union's stance on vaccination disclosure and its impact on public safety and officer judgment.


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about some news out of Chicago
and what the police union up there has decided.
It is reported that they have told the officers in Chicago
to refrain from disclosing their vaccination status,
despite the city's requirement that they do so.
It's, you know, unions of any kind,
it is standard for them to oppose anything
that isn't pre-negotiated.
Anything involving the workers that isn't negotiated,
they just default to opposing it.
And I get that.
It makes sense on some levels.
However, I would point out that a police union
is very different than other kinds of unions.
This move is, it raises a lot of questions.
And there are three main issues with the idea
that law enforcement should, well, just disregard this.
The first is the obvious.
This is a policy.
This is a policy set out by the city for the department.
It is departmental policy.
If individual officers are willing to ignore this policy,
we have to assume that they are willing to ignore
other policies related to public safety.
That is a problem.
That's how really bad things happen.
Then you have this whole secondary issue
when it comes to the individual officers themselves.
As we talked about, the current public health crisis,
that is the number one threat to law enforcement today.
Last year, it took out more than half
of all law enforcement lost.
If an officer does not see the value in vaccination
when they routinely engage in scuffles,
close contact with people in which droplets are exchanged,
when they routinely engage in welfare checks
on people who may not be competent enough
to make their own medical decisions,
when they routinely seal themselves in vehicles
with other people,
if they don't see the value in vaccination
with those things occurring on a routine basis in their life,
I question their judgment overall.
If they can't wade through the information
that is out there and come out
with the common sense answer on the other side,
how can we expect them to deal with dynamic situations
like DV situations?
I question their judgment.
I question their ability to discern fact.
And then you have the third one.
Everything I just said is true in reverse.
These officers routinely take people against their will
and put them in a car that is sealed with them.
If they're not vaccinated,
they have a higher chance of being infected and spreading it.
They routinely engage in physical altercations
with people who, again, against their will,
and they exchange droplets with them.
They routinely do welfare checks on elderly people
or people who may not be capable
of making their own medical decisions.
It is a public safety issue.
Law enforcement is supposed to be about public safety.
If they're not willing to commit to that mission,
they probably need a different profession.
The union stance is, well, what are they going to do?
They can't fire half of us.
Not at once.
But if this is the move the union makes,
I hope the city does it piece by piece
because with these things out there,
if an officer isn't willing to engage in public safety,
if an officer lacks the judgment
and the ability to discern fact,
and if they're willing to disregard policy,
they don't need to be a cop.
It's kind of simple, really.
This is a really easy way to weed out bad officers.
This is a moment where the entire country
is faced with a choice.
Do you want to be part of society?
Do you want to be part of the solution?
Do you want to protect your neighbor?
Or do you want to go with a propagandized talking point?
That's the choice.
It seems the union in Chicago, they made theirs.
I hope the city makes the right one in response.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}