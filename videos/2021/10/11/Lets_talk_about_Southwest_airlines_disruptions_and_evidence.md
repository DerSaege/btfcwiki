---
title: Let's talk about Southwest airlines, disruptions, and evidence....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=x_xZ4DN32wo) |
| Published | 2021/10/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the disruptions and cancellations faced by Southwest Airlines and the claims surrounding them.
- Exploring theories involving classified intelligence vets and pilots calling in sick due to vaccine mandates, both lacking evidence.
- Emphasizing the absence of publicly verifiable evidence to support these claims, despite contradicting statements from relevant parties.
- Predicting the evolution of unfalsifiable narratives to sustain unsupported claims.
- Speculating on the probable scenarios behind the flight cancellations, including operational issues and pilot vaccine concerns.
- Warning against blindly believing narratives without concrete evidence and the dangers of spreading unfounded conspiracy theories.
- Arguing that the lack of evidence does not necessarily disprove the claims but warrants skepticism, especially when circulated by unreliable sources.
- Stating the importance of demanding verifiable evidence before believing and acting on information.
- Concluding with a caution against buying into conspiracy theories without concrete evidence, urging critical thinking and skepticism.

### Quotes

- "The thing is, both of those theories have the same amount of evidence, none."
- "When you come across something that is unfalsifiable, you have to treat it with a great deal of skepticism."
- "Conspiracy theories turn bad when you start to believe it and act on it without any evidence."
- "There's no verifiable evidence to support this. Doesn't exist."
- "The absence of evidence in and of itself doesn't mean there is evidence of absence either."

### Oneliner

Beau addresses Southwest Airlines disruptions, warns against unfounded conspiracy theories, and stresses the importance of evidence-based skepticism in the face of misinformation.

### Audience

Critical Thinkers

### On-the-ground actions from transcript

- Verify information before spreading it (suggested)
- Demand concrete evidence before believing and acting on information (exemplified)
- Refrain from blindly supporting narratives without verifiable evidence (implied)

### Whats missing in summary

The full transcript delves into the dangers of misinformation and conspiracy theories, urging individuals to question narratives, demand evidence, and avoid spreading unverified claims.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk about Southwest Airlines
and the disruptions, cancellations,
that have occurred with their flights
and the claims that have arisen because of this.
And we're going to talk about evidence and the lack
of evidence and things that are unfalsifiable.
And we're going to run through some of the different claims
and see what we can demonstrate.
And we can back up with evidence and what we can't.
And then we're going to talk about the absence of evidence
and what it means.
OK, so if you don't know, Southwest, they have had,
let's just say, a few cancellations.
And the theory has arisen that the secret classified
intelligence vets, I know you've never heard of that agency,
but that's just because you're not in the know,
that they have taken their airplanes
and are using them to move the debris from Roswell.
And that's why there's so many cancellations.
I know, that's probably not the theory you thought
we were going to go through.
You probably thought we were going
to talk about the claim being pushed by the right,
that all of these cancellations are occurring
because pilots are calling in sick,
because they don't want to take the vaccine.
The thing is, both of those theories
have the same amount of evidence, none.
There is no publicly verifiable evidence
to back up either claim.
To the contrary, the union that has
said to have been organizing this sick out,
they said it's not happening.
They have said that there are no official or unofficial actions
taking place like this.
The airline has said it's not happening.
The FAA has said it's not happening.
The evidence is basically that a while back, the union said,
hey, we don't want a mandate, which
is kind of the union's job.
Generally speaking, default to opposing anything
that isn't pre-negotiated.
It's kind of that simple.
That's the totality of the publicly verifiable evidence.
Now, you have people saying, well, I
have a source at Southwest, and they said it's happening.
I have a source at Southwest that said it isn't.
Neither one of those is verifiable.
So what happens next?
Because right now, you have a whole bunch
of people on social media.
There are hashtags supporting a strike that
may not even be occurring.
There's not even a press release saying that this is happening.
But the right wing propaganda machine
pushed it because it helps their narrative to undermine
the United States, basically.
So what happens next?
How do you keep this narrative alive
when there is evidence coming out to contradict it?
You make it unfalsifiable.
So my guess is the next thing that's going to occur
is that somebody is going to go on Tucker Carlson
or some other show of that ilk, and they're
going to be a former pilot or a former union
rep or a legal analyst.
And they're going to say that the reason that there's
no evidence is because if the pilots admitted
they were doing this, well, that's against the law.
So then the lack of evidence becomes evidence
that it's occurring.
Just like it's too secret for you
to know about them moving the debris from Roswell,
well, that just proves it's true.
There are a lot of theories, one that
involved a letter of the alphabet,
that relied on this to spread.
It's unfalsifiable.
You can't prove it wrong.
And it's designed to be set up that way.
They make it hard to counter the narrative by making it
a secret that you have to be in the know
to actually be able to confirm.
And everybody likes to be in the know.
That's how you get social acceptance, right?
So that's what I think is going to happen.
Now, what is a likely scenario for what's occurring?
Perhaps, let's say hypothetically on August 26,
Reuters released an article saying
that Southwest was going to cut more than 100 flights
on October 7, the day before all this stuff happened.
And then let's say hypothetically there
was a small disruption that impacted other airlines,
but they were able to kind of recover quickly
because their systems were more resilient.
But Southwest, maybe they had, I don't know,
pilots and crews out of place.
So flights had to be canceled.
And then it caused a ripple effect
throughout the entire airline.
That could have happened.
That seems just as likely.
What is clear is that the union that many people are saying
is behind this have released more than one statement saying
they're not, that there is no official or unofficial
activity like this.
It's not occurring.
Now, does that mean that it's not occurring?
No.
The union could be lying.
Or maybe the union doesn't know.
Or perhaps there's a large disruption
based on operational reasons.
And then you have some pilots who
aren't going to get the vaccine, so they're
burning through the sick time that they've accrued.
That seems likely too.
And I wouldn't fault a worker for taking
advantage of benefits that they've accrued over time,
regardless of the reason.
The point here is that there is no evidence.
We can't back this up.
The sources, they're all unnamed.
There is no public statement saying it's occurring.
There are no posts on social media organizing the sick out.
But because of the way the right wing propaganda
machine works in this country, people are invested in it.
So they have to continue that narrative.
This is how people get sucked into information silos.
This is how people end up following theories
that can't be verified.
It starts with something small like this,
something that seems meaningless.
And the next thing you know, they are out to get you.
And where's the evidence?
Oh, the evidence is with my girlfriend in Canada.
You wouldn't know her.
Met her at band camp.
But at the end of this, the thing
is we have an absence of evidence.
But that in and of itself doesn't
mean there is evidence of absence either.
We can't say this isn't occurring
because the narrative is structured in a way
to make it unfalsifiable.
When you come across something that is unfalsifiable,
you have to.
You have to treat it with a great deal of skepticism,
especially when it is primarily being circulated
by a demographic of people who are,
not to put too fine a point on it,
known for just making stuff up.
We don't have evidence that this is even occurring.
So this may be the one strike that I'm not really
going to get behind because I don't believe it's happening.
Again, it could be.
It could be.
And it's set up to maintain that belief that it's possible.
And because it aligns with some people's
political convictions, they want to believe it.
But if pundits can get you to believe something
without evidence, without hard verifiable evidence,
they can get you to do anything.
They can get you to go to the Capitol
and get yourself in trouble.
They can do anything.
They can convince you to do anything
once you start believing them without evidence.
There's no verifiable evidence to support this.
Doesn't exist.
Not at time and filming anyway.
But again, doesn't mean it's not true.
So that hope can still be alive.
Just don't believe it based on a rumor on social media.
The conspiracy, it starts when you believe it.
Conspiracy theories turn bad.
They stop becoming thought exercises.
When you start to believe it and act on it
and allow it to impact your emotional state,
without any evidence.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}