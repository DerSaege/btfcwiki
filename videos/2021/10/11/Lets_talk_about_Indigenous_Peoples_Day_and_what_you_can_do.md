---
title: Let's talk about Indigenous Peoples' Day and what you can do....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=k0ExsSXUZ28) |
| Published | 2021/10/11|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Indigenous Peoples Day prompts action for groups being recognized.
- Native causes have various needs, one being Oak Flat protection.
- Oak Flat, ancestral home to Apache, Hopi, and Zuni, faces copper mining proposal.
- Native groups oppose the mining proposal at Oak Flat.
- Public perception on the mining issue is starting to shift.
- The mining operation at Oak Flat will use a significant amount of water.
- Majority of likely voters in Arizona oppose the mining proposal.
- Legislation like the Save Oak Flat Act aims to stop the mining.
- A petition supported by ACLU is circulating to save Oak Flat.
- Today is a good day to get involved in supporting causes like saving Oak Flat.
- Support for causes like this can create a tipping point for change.
- Even small support can contribute to significant change.
- Building support for movements like this takes time.
- The shift in public opinion can lead to tangible outcomes.
- Encouraging support for causes can lead to impactful change.

### Quotes

- "Today's a good day to get involved in something."
- "Movements like this, they're always slow going."
- "Even small support might push it over the edge and get something done."

### Oneliner

Indigenous Peoples Day prompts action, with Oak Flat facing a critical tipping point for change through public support against the proposed copper mining.

### Audience

Activists, Supporters, Voters

### On-the-ground actions from transcript

- Sign and share the petition supported by ACLU to save Oak Flat (suggested).
- Support legislation like the Save Oak Flat Act to stop the mining proposal (suggested).

### Whats missing in summary

The full transcript provides detailed insight into the urgency of supporting Native causes, specifically focusing on the critical need to save Oak Flat from a destructive copper mining proposal.

### Tags

#IndigenousPeoplesDay #NativeCauses #SaveOakFlat #PublicSupport #Activism


## Transcript
Well, howdy there, Internet people.
It's Beau again.
So it's Indigenous Peoples Day.
Now, one of the things that happens on days like this
is there's a desire to do something
to benefit the group that's being recognized in some way.
And they can be turned and used as days of action,
days to get stuff done.
The problem is with a lot of these groups
and a lot of these days, people don't know exactly what to do.
Now, with Native causes, there's tons.
There's so much that needs to be done.
But I want to highlight one that I think
is kind of at a tipping point, and that if a bunch of signatures
went in, if a bunch of support emerged,
it might actually make the difference that's necessary
and create some change.
So there's a place called Oak Flat.
Now, this is the ancestral home of Apache, Hopi, Zuni,
and others.
And it is currently, there's a proposal
to basically turn it into a two-mile-wide, 1,000-foot
crater for copper mining.
Obviously, this is opposed by Native groups.
That would make sense, you know.
The thing is, right now, public perception
is also beginning to shift for a probably kind of self-serving
reason.
But it doesn't matter.
Use every tool at your disposal.
It has recently been disclosed that the operation
is going to use enough water to supply
a city of 140,000 people.
In Arizona right now, that seems important.
Currently, 74% of likely voters in Arizona oppose it.
That was the latest poll.
So we're at the moment where there's a shift that's possible.
There is legislation pending to stop this,
and it's called Save Oak Flat Act or something like that.
There's a petition that I will link below
that is being floated and supported by the ACLU,
and I'll put that out there.
Today's a good day to get involved in something.
If you don't know what to do, if you
don't want to get involved in any of the other causes,
or you don't know about them, this is something you can do.
It won't take a lot of time, and it may actually
be the moment where change can happen,
because there's enough support that has been built
to shift the discussion here.
Movements like this, they're always slow going,
and they build and build and build and build and build,
and eventually they hit that tipping point.
I think this is one that's right at that tipping point.
So a little bit of support on a day like today
might actually push it over the edge and get something done.
So anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}