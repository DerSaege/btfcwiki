---
title: Let's talk about Trump's Truth network....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=01NGe-2o5zY) |
| Published | 2021/10/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Former president launches a social media venture called Truth, despite his history of not sticking to the truth on major social media sites.
- The site, still not officially live, has already crashed once and experienced a cyber security issue.
- People accessed the site via a beta link, revealing that it's an off-brand version of Mastodon, not Twitter.
- The branding of the site is criticized, with terms like "truth" and "retruth" for tweet and retweet not being user-friendly.
- Despite being touted as a free speech platform, parody accounts were banned, and making fun of the platform is a violation of terms of service.
- The Trump team plans to launch Trump Plus, a streaming service to compete with Netflix and Hulu, which raises skepticism due to lack of experience in the field.
- Market saturation and lack of new ideas from the far right raise doubts about the potential success of Truth within the Republican Party.
- The platform is speculated to attract FBI attention due to its target audience, potentially aiding investigations related to events like January 6th.
- The development of Truth raises questions about whether Trump's past rhetoric against big tech was strategic groundwork for his own social media platform.
- Beau predicts that Truth will likely fail spectacularly due to various issues, lack of appeal compared to existing platforms like Twitter, and potential for attracting a problematic user base.

### Quotes

- "A tweet and a retweet are called a truth and a retruth. Nobody's going to say that."
- "At the end of the day, this is going to crash and burn in glorious fashion."
- "It's a win."

### Oneliner

Former President's venture into Truth, a failing social media platform, faces criticism for its branding, lack of user-friendliness, and potential to attract FBI attention.

### Audience

Social media users

### On-the-ground actions from transcript

- Monitor developments and potential impacts of Truth platform (implied)
- Exercise caution and critical thinking when considering joining new social media platforms (implied)
- Engage in open dialogues regarding the implications of political figures launching their own media ventures (implied)

### Whats missing in summary

Insights into the potential broader societal implications of political figures launching their own media platforms.

### Tags

#SocialMedia #Politics #Truth #Trump #FreeSpeech


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about truth.
Truth being the former president's new social media venture.
Yes, the irony is not lost on me.
The fact that a person who is not on most major social media sites
because they couldn't stick to the truth has launched a site called Truth.
That is Orwellian.
Okay, we're not just going to make fun of the site, although we kind of have to in the
process of getting to the point.
We're going to talk about it a little bit and kind of review what we know about it thus
far.
Okay so to start with it is exactly the fell sandwich that you would imagine it is.
It isn't actually officially live yet, but it has apparently already crashed once and
already had a cyber security issue.
Because we all know that the Trump team knows a whole lot about cyber security so this is
really surprising, you know.
It's not like they haven't been talking about that exact topic for months.
So even though it's not officially announced yet, people got access to it via a beta link
that allowed them to sign up.
When they signed up, they created accounts for famous people to include Donald Trump.
And they gave us an inside look of what it would look like while they were tweeting
out images of animal anatomy from famous people's names.
Basically it's an off-brand Twitter, except it's not really even an off-brand Twitter.
It's an off-brand Mastodon, which is, if you're not familiar with it, it's software that allows
you to kind of run your own little Twitter.
To my untrained eye, it certainly appears as though the Trump team used this software
to develop this site.
Oddly enough, it doesn't appear like they complied with all of the licensing requirements,
which I'm sure that's not true because there's no way the Trump team would open themselves
up to lawsuits.
I mean this early, anyway.
That seems...
Okay.
So the branding is awful.
The branding is awful.
A tweet and a retweet are called a truth and a retruth.
Nobody's going to say that.
That doesn't exactly roll off the tongue.
So what's going to happen is anytime somebody's talking about the platform, they will say,
I tweeted out on truth, constantly advertising for the website that really hurt his feelings
and maybe caused all of this.
But I don't necessarily know that that's the case.
So we're going to get to that.
It's being billed as a free speech website.
I would point out that all the people who made parody accounts, yeah, they got banned.
On top of that, it is also in the terms of service that if you have an account and make
fun of truth, truthiness, whatever, the truth network, whatever it is, that you could...
That's a violation of the terms of service.
You could lose your account over that.
So if I was to truth out this video and you were to retruth it, we would probably both
get banned from the free speech platform.
Now on top of all of this mountain of flaming fail, the Trump team is also apparently planning
to launch Trump Plus, which is going to compete with Netflix and Hulu.
Sure.
Yes, you know, the companies that major production studios can't really compete with and haven't
really been able to contain, Trump with all of his business skill is going to do that.
They're pointing to his success in show business on The Apprentice as proof that he will be
able to challenge Netflix.
Because as we all know, running a massive streaming company is very similar to being
an insufferable boss on a reality TV show.
Those two things, they're the same pretty much.
There's not a big difference there.
On top of all of this, there's some other questions that arise.
First is market saturation.
There are already a bunch of social media networks that cater to the far right.
They're not incredibly popular and they normally don't last very long, mainly because there
aren't new ideas coming out of the right.
So it becomes very stale and monotonous.
I mean, it's no fun to be on social media if you can't own the libs.
And if the libs aren't there, really all you end up doing is arguing or repeating baseless
theories to each other.
So on the off chance it does actually get some kind of following within the Republican
Party, it will accomplish a lot of infighting.
The other concern for people considering signing up for this is that because it is specifically
marketed to the type of person who participated in the events of the 6th, I'm fairly certain
that the people at the FBI at this moment are just like, yes, can't wait to set up accounts.
Because it's like the Trump team built the honeypot for them.
So there's that.
Now to the actual point here, all of this rhetoric that came from Trump while he was
president about big tech, knowing what we know about Trump and how he likes to telegraph
his moves, do we think that it's possible that his rhetoric about big tech was just
laying the groundwork for his visionary social media platform here?
I think that may be the case.
I think it may have all been leading up to this rather than this being a reaction to
what occurred.
I could be wrong about that.
It's pure speculation.
But given what we know about the former president, we can't put that out of the realm of possibilities.
At the end of the day, this is going to crash and burn in glorious fashion.
Having a crash and a breach before you're even officially launched is generally uncool.
The issues that exist as far as the business side of it are pretty big.
The technological things are pretty big.
People really don't want to join an off-brand Twitter when Twitter still exists.
It doesn't seem like it's going to attract anybody except for those people who will create
infighting for the Republican Party or be people who are actively investigated by the
FBI.
So it's a win.
Yeah.
I can't wait to see what the Trump team comes up with next.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}