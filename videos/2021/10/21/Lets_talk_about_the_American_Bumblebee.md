---
title: Let's talk about the American Bumblebee....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q3e8pvrAXF0) |
| Published | 2021/10/21|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- The American bumblebee population is dwindling due to habitat loss, pesticides, and various other factors.
- There has been a 90% reduction in the American bumblebee population in several states.
- Currently, the American bumblebee is not protected by any state or federal law.
- It is highly likely that the American bumblebee will become protected under the Endangered Species Act.
- If the American bumblebee gets protected, killing one could cost up to $14,000.
- Bees are vital for pollinating fruits, veggies, and nuts, with 70% of these foods relying on bee pollination.
- Bees are integral to our ecosystem, and if they disappear, it will have a severe impact on our food sources.
- People should be motivated to protect bees based on the importance of their role in our survival.
- The profit-driven nature in America often conflicts with environmental conservation efforts.
- Commercial beekeepers are also facing colony collapse issues, adding to the challenges bees are currently facing.

### Quotes

- "If the bees go, we go."
- "You shouldn't need a law to tell you to be a better steward of the area around you."
- "Bees pollinate 70% of all fruits, veggies, and nuts."

### Oneliner

The American bumblebee population is dwindling, potentially leading to protection under the Endangered Species Act, as their vital role in pollination impacts our food sources and survival.

### Audience

Environmental enthusiasts

### On-the-ground actions from transcript

- Advocate for the protection of bees by supporting initiatives and policies that aim to conserve their habitats and populations (implied).
- Educate others on the importance of bees in our ecosystem and food production to raise awareness and encourage action (implied).
- Support local beekeepers and beekeeping initiatives to help sustain bee populations and combat colony collapse (implied).

### Whats missing in summary

The full transcript provides a detailed overview of the challenges facing the American bumblebee population and the urgent need for conservation efforts to protect these vital pollinators. Viewing the full transcript can offer a deeper understanding of the interconnected issues threatening bees and our food sources.

### Tags

#AmericanBumblebee #EndangeredSpeciesAct #Pollinators #EnvironmentalConservation #Beekeeping


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're going to talk about the American bumblebee
and honey and why it's about to get really expensive
to mess with an American bumblebee.
At least I think it's going to get really expensive.
The population of this species is dwindling.
And that's due to them losing their habitat,
due to development, it's due to a certain kind of pesticide
that is used to protect crops.
But it also impacts the, for lack of a better word,
the navigation system of bumblebees.
Competition with non-native bees, disease, climate change,
the list goes on and on and on.
The populations are dwindling.
We're talking about 90% reduction
in the states of Idaho, Maine, Wyoming, Rhode Island, Oregon,
New Hampshire, Vermont, and North Dakota.
They're basically gone already.
It's a big deal.
Currently, at this moment, the American bumblebee
is not protected by any state or federal law.
I have a feeling that's about to change.
With these numbers and some of the new statements coming out
of some federal agencies, it seems incredibly likely
that the American bumblebee become protected
by the Endangered Species Act.
If that occurs, killing a bee, killing an American bumblebee
is going to cost you $14,000, $13,000, something like that.
It's going to get real expensive real quick.
And that may seem very drastic, but we're kind of at the point
where all of these things that scientists told us,
these things are going to happen.
They're all starting to happen.
And they're all starting to happen at once.
And it's pretty risky.
And it's going to take drastic action.
At the end of the day, we have to remember that if the bees go,
we go.
And that's not an overstatement.
That's not just some cute slogan.
If a plant has a flower, a bee has something to do with it.
Bees pollinate 70% of all fruits, veggies, and nuts.
Without bees, I think you're pretty much
limited to wheat, rice, and corn,
stuff that's wind-pollinated.
Probably get pretty boring pretty quick.
This is one of those things where it's a law that
shouldn't need to exist.
You don't need a law to tell you to be a good person.
You shouldn't need a law to tell you
to be a better steward of the area around you.
Just the numbers related to their population
and how they are dwindling and the information
related to how important they are to our own survival
should be enough to motivate people
to take a different stance, to help the species make it.
America's profit motivator tends to undercut our better nature
at times.
And that is why the Endangered Species Act is
structured the way it is.
That's why it's about to get really expensive to mess
with one of these bees.
On top of this, it's worth noting
that a lot of commercial beekeepers,
and these are bees that are used in commercial production
of food.
A lot of them are still reporting colony collapse.
So we have a lot of different issues in different areas
that are all coming together at the same time.
We warned about it and we ignored it.
Now we have to deal with it.
And it's going to take some pretty drastic action
to correct this.
So expect that on the horizon.
And maybe look into taking up beekeeping as a hobby.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}