---
title: Let's talk about prices and supply chains....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=l0juJUD29gc) |
| Published | 2021/10/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains why prices are going up due to supply and demand dynamics.
- Consumer spending habits changed during the pandemic, leading to increased demand for certain items.
- Production was down as businesses and factories were closed, resulting in less supply and higher prices.
- Gives a personal example of fluctuating prices for building materials like lumber.
- Describes how the increase in home improvement projects led to a surge in demand for lumber.
- Many consumers unknowingly overpaid for products due to lack of comparison and market forces.
- Notes that prices are going up for a variety of products due to changing habits.
- Mentions that as people start venturing out more, demand for different products is shifting.
- Talks about disruptions in the supply chain due to factories shutting down during the public health crisis.
- Companies are now trying to catch up on production, leading to delays in getting products off boats.
- Foresees these supply chain issues taking about a year to resolve.
- Assures that severe shortages are unlikely but warns of ongoing annoyances due to supply chain hiccups.
- Advises being cautious during the holidays, especially with electronics-heavy gifts.
- Suggests preparing for potential scarcity of certain items without causing panic.
- Concludes by reassuring that the situation is manageable but may be inconvenient for a while.

### Quotes

- "It's just going to be more of a pebble in our shoe, constantly annoying, and it's going to last a while."
- "Rather than something severe that is short-lived, it's going to be annoying and it's going to last a long time."
- "You should be able to get your basic needs. Maybe not the particular brand of corn or whatever you like, but you'll be fine when it comes to basic survival."
- "It's just going to be more of a pebble in our shoe, constantly annoying, and it's going to last a while."
- "Maybe steer your children into asking Santa for things that don't have a whole lot of electronics in them."

### Oneliner

Beau explains the impact of changing consumer habits on prices, supply chain disruptions, and the lasting inconvenience of current market challenges.

### Audience

Consumers, holiday shoppers

### On-the-ground actions from transcript

- Be cautious with holiday shopping, especially items heavy on electronics (implied)
- Prepare for potential scarcity by focusing on basic survival needs (implied)
- Stay informed about market dynamics and adjust shopping habits accordingly (implied)

### Whats missing in summary

The full transcript provides a detailed explanation of the current economic situation regarding prices, supply chain disruptions, and consumer behavior during and post-pandemic, offering insights into how these factors interplay and affect everyday life. 

### Tags

#Economics #SupplyChain #ConsumerBehavior #Pricing #HolidayShopping


## Transcript
Well, howdy there, internet people.
It's Bo again.
So today, we are going to talk about supply and demand
and supply chains and why everything costs so much
and what's going on, how long it's gonna last
and all of that stuff.
Cause we've got a lot of questions about that have come in.
Okay, so first, why are prices going up?
It is supply and demand.
It is basic, basic economics on this one.
Consumer spending habits changed.
We bought different stuff when we were all at home
all the time.
So there was increased demand on certain items.
So they went up in price, right?
Then because production was down,
because everybody was at home, those businesses,
those factories, those manufacturing plants,
they weren't working.
There was less supply, same amount of demand,
prices went up.
A good example from my life,
and if you follow me on Twitter,
you have seen me discussing building a barn
for like seven months.
Had we put it in seven months ago,
it would have cost about $40,000.
Needless to say, we didn't do that.
We're putting it in, I would say, week after next,
and it's gonna cost about 5,000.
That's how much things have fluctuated.
Lumber, the reason it happened, everybody was at home.
Everybody had time to do home improvement projects.
People took up gardening,
so they needed to build raised beds.
Same amount of lumber or less lumber than normal,
more demand, prices went up.
And because many people,
they weren't frequent purchasers of lumber,
they didn't even know
that they were grossly overpaying for stuff
because they had nothing to really compare it to.
So they paid the high prices.
Since the market took it,
they'll charge whatever the market can bear.
Since people were paying that, the prices went up.
They kept going up.
And this is true for a whole lot
of different products across the spectrum.
This is one of the many reasons
that prices are going up.
Now, the habits are changing again
because things are opening back up.
People are starting to venture out more.
So things that there wasn't a lot of demand for,
they weren't making as much of,
now all of a sudden the demand is there
and the supplies are low.
This will all kind of work itself out.
Now, as far as the supply chain stuff,
I talked about it coming a month ago,
two months ago, something like that.
I was like, hey, this is going to happen.
Here we are.
So the basic chain of events here
is we had this whole public health thing.
When that happened, factories,
manufacturing plants, stuff like that,
they shut down.
Nothing was being produced.
Components for products weren't being produced either.
So those products weren't getting made at all
because they were missing something,
a chip, as an example.
Now that things are kind of getting back running,
those things are being made.
So the final product is being made.
The chip is being made.
Therefore, the final product is being made.
And companies are trying to catch up.
So they're producing a lot more faster.
And now they have produced so much
that they can't get it off the boats fast enough.
It's little kinks in the supply chain.
These are the growing pains of things
getting back to normal.
The problem is this could take a year, realistically.
It could take a year for all of this stuff
to work itself out.
Now, is it going to be severe?
Are there going to be severe shortages?
Probably not.
I wouldn't worry too much about that.
Rather than something severe that is short-lived,
it's going to be annoying
and it's going to last a long time.
The only thing that I would really caution about right now,
you should be able to get your basic needs.
Maybe not the particular brand of corn or whatever you like,
but you'll be fine when it comes to basic survival.
I would be a little cautious,
out of an abundance of caution,
when it comes to the holidays.
Maybe steer your children into asking Santa
for things that don't have a whole lot of electronics in them.
That seems like it might be a good idea.
I have a feeling those items will be hard to come by,
even at Santa's workshop.
He may not be able to get all the components.
But overall, this isn't going to be a massive thing
that is going to shut down the country or anything like that.
It's just going to be more of a pebble in our shoe,
constantly annoying, and it's going to last a while.
So don't panic, but just be aware
that there are going to be certain things
that you may not be able to find from time to time.
So anyway, it's just a thought.
Now have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}