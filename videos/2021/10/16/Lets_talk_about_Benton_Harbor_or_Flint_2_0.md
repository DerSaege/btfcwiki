---
title: Let's talk about Benton Harbor or Flint 2.0....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Q28kJtDYxSI) |
| Published | 2021/10/16|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Benton Harbor, Michigan, a town of under 10,000 people, faces lead levels above 15 parts per billion in the water.
- Initially, Governor Gretchen Whitmer's response to the crisis was lackluster, with a five-year timeline for resolution.
- Due to criticism and pressure, the timeline has been accelerated to 18 months for fixing the water issue.
- Residents in Benton Harbor will receive free bottled water during this period.
- Qualified homes with children under Medicaid will receive free or reduced-cost abatement services for issues within the home.
- The situation in Benton Harbor underscores the crumbling and decaying state of infrastructure in the United States.
- Infrastructure is vital for the country to function, and without proper maintenance, it will continue to degrade.
- Despite the improved response under Whitmer, there may not be similar legal consequences faced by former governor Rick Snyder.
- Continuous monitoring and support for communities like Benton Harbor are necessary as they can be easily forgotten by those in power.
- The presence of any level of lead in water poses risks, especially for children, necessitating urgent action.

### Quotes

- "There is no safe level of lead, especially when you're talking about kids."
- "We have to repair the infrastructure in this country. We don't really have a choice on this."
- "If your party claimed that they wanted to make America great for years [...] I doubt your commitment to your cause."

### Oneliner

Benton Harbor, Michigan faces a water crisis as infrastructure crumbles, demanding urgent action to protect residents and prevent further decay.

### Audience

Community members, activists, policymakers

### On-the-ground actions from transcript

- Monitor and support communities facing infrastructure issues (suggested)
- Advocate for swift and effective infrastructure repairs (implied)

### Whats missing in summary

The full transcript provides additional context on the delayed response to the water crisis in Benton Harbor and the importance of continuous monitoring to ensure timely resolution.

### Tags

#Infrastructure #WaterCrisis #LeadPoisoning #CommunitySupport #Advocacy


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to be talking
about infrastructure and Flint 2.0, Benton Harbor, Michigan. It's a very familiar storyline,
right? If you don't know Benton Harbor, Michigan, it's a town of a little under 10,000 people
I think, and recently the lead levels in the water went above 15 parts per billion. Now
there is no safe level of lead, to be clear on that, but at 15 parts per billion, that's
when the feds are like, yeah, you gotta do something. Governor Gretchen Whitmer, initially
there was what we will politely call a lukewarm response, kind of, we'll fix it, and the timeline
was five years. People found that less than acceptable. After criticism and a little bit
of pressure, that timeline has been accelerated. They're now committed to having it done in
18 months. People inside the town, they will get free bottled water for the duration. I
want to say that there's also something in there that allows homes that are qualified
for Medicaid, that have children in them, they will be getting free abatement or reduced
cost abatement for issues within the home. So this is yet another reminder that the United
States' infrastructure is crumbling. It is decaying. It is old. It needs to be fixed.
It needs to be repaired. The infrastructure is what allows the United States to function.
Without it, it will cease. It will not run. It will continue to degrade. Now, with the
speed at which there was a response and then a change under Whitmer, it's unlikely that
we're going to see similar charges like our facing former governor, Rick Snyder. But this
isn't over yet. This is one of those things we have to watch. The area of Benton Harbor,
well that's one of those communities that people in power tend to forget about if eyes
get diverted for too long. So this is something that we need to continue to check in on every
once in a while and make sure that, while things are moving along, the timeline that
they should as quickly as possible. There is no safe level of lead, especially when
you're talking about kids. So this is probably going to be a story that we continue to follow,
that we continue to check in on every once in a while. And it should serve as a pretty
decent reminder, you know, as we talk about the infrastructure bill in DC and people talk
about the debt ceiling and stuff like that. We're kind of at a point here with this,
we don't really have a choice. This isn't something that can be ignored. It's not something
that can be put off. You know, this isn't an elective. This is something that has to
be done. We have to repair the infrastructure in this country. We don't really have a choice
on this. If your party claimed that they wanted to make America great for years, but when
it comes time to actually put in the work and do what needs to be done to continue to
allow the United States to function, you're nowhere to be found or you're playing partisan
games. I doubt your commitment to your cause. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}