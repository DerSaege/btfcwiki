---
title: Let's talk about paternity leave....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=En7UPgXn4Mw) |
| Published | 2021/10/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addresses the topic of paternity leave after receiving viewer messages about Mayor Pete and supply chain issues.
- Shares his support for paternity leave, having taken time off after the birth of his children.
- Criticizes the Republican Party for discouraging spending time with kids despite promoting family values.
- Points out how the GOP manipulates working-class voters to go against policies that could benefit them, like paternity leave.
- Notes the party's tactic of associating policies they dislike with demographics they've marginalized, such as gay people.
- Calls out the stereotype that paternity leave is only for weak men, perpetuated by the GOP.
- Mentions the military parental leave program as a counter to the notion that paternity leave is weak or unmasculine.
- Criticizes the manipulation tactics used to sway individuals against policies that could personally benefit them.
- Encourages spending time with kids and challenges toxic masculinity views perpetuated by societal norms.


### Quotes

- "They play into that stereotype that paternity leave is just for gay men and gay men are weak and you don't want to be like them, right?"
- "It's wild to me, but this is how they do it."
- "This is how they convince you to go against your own interests."
- "All they have to do is convince their base that they're in a slightly better position than somebody else of a demographic they don't like."
- "Go spend some time with your kids."

### Oneliner

Beau dismantles the GOP's manipulation of working-class voters against paternity leave, exposing toxic masculinity stereotypes and advocating for spending time with kids.


### Audience

Working-class parents

### On-the-ground actions from transcript

- Spend quality time with your kids (exemplified)
- Challenge toxic masculinity stereotypes by supporting paternity leave policies (exemplified)


### Whats missing in summary

The full transcript provides a detailed breakdown of how political manipulation and toxic masculinity stereotypes impact working-class individuals' views on paternity leave.

### Tags

#PaternityLeave #PoliticalManipulation #ToxicMasculinity #FamilyValues #WorkingClassParents


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today we're gonna talk about Mayor Pete again.
Shortly after that last video went out,
messages started to come in.
Two variations, one saying,
hey, since you talked about Mayor Pete,
will you talk about paternity leave?
And the other saying,
oh, it's not just that it's his fault
that he caused all these supply chain issues,
it's that once he caused them,
well, he took paternity leave.
Sure, sure, whatever.
Yeah, we can talk about it.
I am a huge supporter of paternity leave.
If you don't know, I have a bunch of kids.
And with the exception of about 36 hours
after my middle son was born,
I took what amounted to paternity leave.
The only reason that 36 hours happened
was because Ferguson popped off and I had to go cover it.
And I came back as soon as humanly possible.
I don't find paternity leave to be weird.
What I find weird is the party of family values
going all out to convince their constituents,
their followers,
that they don't need to spend time with their kids.
That seems weird to me,
except that's not really weird either.
It's how they do it.
If you have ever wondered how the Republican Party
gets blue collar working class folk
to vote against their own interest,
this is how they do it.
It's a case study and how they always do it.
So think about the pundits
that you've heard talk about this.
What did they say?
Did they come out and say paternity leave is bad?
No.
They came out and said, oh,
Mayor Pete's on paternity leave.
What's he doing?
Trying to figure out how to breastfeed.
I don't get it.
The only real job of a dad
after a birth is to take care of the mom.
And there's no mom in this case.
It's how they do it.
They tie the policy that they don't like
to a demographic that has been othered by their base.
In this case, gay people.
This is how they have convinced
a bunch of working class people
to be against the idea of paternity leave.
This is how working class Republicans
are going to go against their own interests.
This would benefit them.
These are policies that would benefit them.
After they have a child,
hey, here's some time off to spend with your kids.
Oh no, I don't want that
because I don't want to be like those people.
That's only for weak men.
I'd rather be here and work with you guys
because that's the stereotype.
Weak men.
They play into that stereotype
that paternity leave is just for gay men
and gay men are weak
and you don't want to be like them, right?
You don't want to have people make fun of you.
You know, in his response,
he said that it was toxic masculinity.
That's exactly what it is.
It's trying to put up this front of being this tough guy.
Oh, I don't care about my kids.
It's wild to me, but this is how they do it.
All they have to do is convince their base
that they're in a slightly better position
than somebody else of a demographic they don't like
and they can convince them
to vote against their own interests.
Now, to shatter the idea that paternity leave
is only for weak men,
I would like to point you
to the military parental leave program.
The army provides leave
after a birth or adoption to mothers and fathers.
And since we are talking about the Republican right,
obviously, you can't say anything bad about the troops.
They're not weak, are they?
This is how they convince you.
This is how they convince you to go against your own interests.
This is how they convince you to go against things
that would personally benefit you
so you can fit into some perceived idea of masculinity.
Anyway, it's just a thought.
Y'all have a good day.
Go spend some time with your kids.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}