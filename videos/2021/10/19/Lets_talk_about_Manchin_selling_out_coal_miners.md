---
title: Let's talk about Manchin selling out coal miners....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Oa9cODx1BVI) |
| Published | 2021/10/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau debunks the notion that Manchin's actions were for coal miners, asserting that it was actually for the owners.
- Using the typewriter industry as an analogy, Beau explains how people switch to better alternatives when they become available, similar to the decline in coal usage.
- He presents statistics showing the significant decrease in coal production and usage in the US over the years.
- Beau points out that coal now contributes to less than a quarter of electricity in the US, falling behind renewables and nuclear energy.
- Despite declining demand domestically, attempts to rely on exports have also been unsuccessful.
- He predicts the inevitable demise of the coal industry, despite Manchin's efforts buying a few more years for the owners, not the miners.
- Beau criticizes Manchin for not advocating for beneficial programs like re-education, retraining, or tax breaks for coal miners transitioning to other industries.
- He expresses concern that miners will face layoffs and financial struggles as the industry continues to decline.
- Beau accuses Manchin of prioritizing the owners' profits over the well-being of the miners, leading to a bleak future for those employed in the coal industry.
- In conclusion, Beau asserts that Manchin's actions were not in the miners' interests but rather served the owners' agenda.

### Quotes

- "He did not do this for the miners."
- "He did it for the owners."
- "He pushed them under a bus and didn't even tell them it was coming."

### Oneliner

Beau debunks the myth that Manchin supported coal miners, revealing his actions favored owners' profits over workers' well-being.

### Audience

Advocates for workers' rights

### On-the-ground actions from transcript

- Advocate for programs like re-education, retraining, and tax breaks for coal miners transitioning to other industries (suggested)
- Support initiatives that prioritize miners' well-being over owners' profits (implied)

### Whats missing in summary

The full transcript provides a detailed breakdown of Manchin's actions and their implications for coal miners and industry workers. Viewing the entire speech offers a comprehensive understanding of the discourse on coal mining and the political decisions affecting workers.

### Tags

#CoalIndustry #MinersRights #PoliticalDecisions #TransitionAssistance #WorkersAdvocacy


## Transcript
Well howdy there internet people, it's Bo again.
So today we're going to talk about Manchin.
And why people need to stop saying that he did it
for the coal miners.
He didn't.
He absolutely did not.
That is not what he did.
You know, people joke about my crystal ball.
I don't have a crystal ball, but it doesn't matter
because you don't tell the future by looking at a crystal ball,
you tell the future by looking at the past.
In this case, looking at the typewriter industry,
because it's the same thing.
See, people didn't really like the typewriter to begin with,
but they needed it.
But realistically, if there was something better,
they'd switch over to it.
And that's what happened.
See, people didn't like the typewriter because they
had to put something into it.
That messy ink ribbon, right?
To get something out, to get what they wanted.
Eventually, when something that was clean came along,
well they switched over.
People that made typewriters, a lot of them
didn't see it coming.
Even though they understood the numbers,
they didn't see it coming.
He did not do this for the miners.
What are the numbers when it comes to coal mining?
2019, 705 million short tons produced in the United States,
lowest level since 1978.
2020, 525 million short tons, lowest level since 1965,
a 24% decrease.
Now keep in mind, less than a decade ago,
it was a billion short tons.
Coal, it fires less than a quarter of electricity
in the US now.
Just so you know, that is less than renewables and nuclear.
Can't count on exports.
It's not working in the US.
Industry is dying here.
The demand is dying in the US.
You know, it's not working in the US, industry's dying here.
The demand for coal is shrinking here.
So you got to look overseas.
But that doesn't help either, because exports are down 26%.
It's over.
It's over.
The industry is gone.
Manchin, he bought a couple more years.
But it's not for the miners.
It's for the owners.
It's for the owners.
I know that miners and owners, they've always
had a really good relationship.
And the owners have always treated the miners very well,
right?
Historically speaking, that's been the case.
What do you all think is going to happen?
See, those mines, they're going to keep
trying to get that money, right?
They're going to keep trying to turn a profit at that mine.
And when it's not profitable anymore,
what are they going to do?
They're going to shut it down.
And there's going to be layoffs.
And along the way, less pay, less workers.
They'll try to squeeze every dime out until they just
can't make money anymore.
And then you're done.
If you are starting in the coal mining industry today,
you're not going to retire from it.
Manchin did not do this for the miners.
You know what he could have asked for, for coal miners,
in exchange for his support?
Anything.
Anything.
Re-education pay, retraining pay, relocation pay,
preferential hiring when it comes to above ground jobs,
working conservation, working for the feds in the parks.
Anything.
Could have asked for anything.
Tax breaks for companies that hire former coal miners.
Could have asked for anything.
Anything to help the miner transition out
of that industry.
That's not what he did.
Because he doesn't care about the miner,
he cares about the owner.
That's what it's about.
And now, it certainly appears as though what's going to happen
is the owner gets to tell the miner when to leave.
Rather than programs that could have helped people leave
the industry as they wanted, when they saw fit,
find different jobs, good paying jobs elsewhere.
That's not the route he went.
Wasn't about you.
Wasn't about the miner.
Wasn't about the worker.
It was about the owner.
Getting them a few more years of profit.
And when they're done, the miners get laid off,
and they're stuck in a place with no industry,
no way to make money.
You can talk about it all you want,
but be very clear that he didn't do this for the miners.
He did it for the owners.
He didn't help the miners.
He pushed them under a bus and didn't even
tell them it was coming.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}