---
title: Let's talk about how I learned to love hypersonic missiles....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=OGbMyIarrn0) |
| Published | 2021/10/19|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Beau addresses the hype around hypersonic missiles and robot dogs, prompted by recent events involving China's successful test.
- He believes the concern over hypersonic missiles may be overblown, especially in the context of nuclear warfare where the outcome is bleak regardless.
- Beau points out that the US already has similar technology like hypersonic missiles and suggests that parity will be achieved eventually.
- He expresses more concern about the implications of robot dogs armed with rifles, foreseeing a potential shift in warfare tactics.
- Beau humorously touches on the choice of rifle caliber for the robot dogs and the likelihood of them being more actively used than hypersonic missiles.
- He concludes by warning about the misplaced priorities in media coverage, urging for attention on what truly impacts people's lives, such as the deployment of robot dogs.

### Quotes

- "Worrying about the hypersonic missile technology is a little bit like worrying about whether or not the rifle that shot you in the head had good optics."
- "I am far more concerned about those little robot dogs."
- "That's not the one you need to worry about the most."
- "Prepare for Cold War style coverage in that atmosphere because that's what's coming down the road."
- "There's a missed priority when it comes to coverage."

### Oneliner

Beau believes the focus on hypersonic missiles overshadows the real threat posed by armed robot dogs, urging for a shift in media coverage priorities towards what truly impacts people's lives.

### Audience

Military analysts, policymakers

### On-the-ground actions from transcript

- Prepare for a potential shift in warfare tactics with the deployment of robot dogs armed with rifles (exemplified)
- Shift media coverage priorities towards issues that have a more direct impact on people's lives, such as the use of lethal technology like robot dogs (suggested)

### Whats missing in summary

Beau's detailed analysis and humorous take on the implications of hypersonic missiles and robot dogs provide insight into modern warfare threats and media coverage biases.

### Tags

#MilitaryTechnology #HypersonicMissiles #RobotDogs #MediaCoverage #ColdWar #WarfareTactics


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about how
I learned to stop worrying and love hypersonic missiles,
and also robot dogs.
We're going to do this because I've had a lot of people send
in messages, not a lot, five or six,
that have sent in messages saying, hey,
why haven't you talked about this?
Shouldn't you be raising the alarm?
That's the general tone of them.
No, I don't believe I should.
Generally speaking, I don't like making military propaganda.
Now, the reason this is in the news
is because the Chinese just conducted a test,
and it was pretty successful.
The reporting, of course, said that the US military was
stunned.
US intelligence was stunned.
They just couldn't believe it.
OK.
Remember when we were talking about the US foreign policy
shift to near peers, near peers being Russia and China,
I said it was going to look a whole lot like the Cold War.
Welcome to the arms race portion of the show.
We cannot allow there to be a mineshaft gap.
All right, so here is, for those that don't know,
this is what a hypersonic missile is.
Basically, it's a missile that goes,
I want to say it's above Mach 5, is the general loose
definition.
Most have a little bob and weave in them,
so it makes it easier for them to get through missile defense
systems.
The real concern is that it is capable of carrying
a nuclear payload.
OK.
I'm not really that concerned about this,
because for this to matter, for this
to be a legitimate issue and something that
is going to impact my life, nuclear missiles
have to be being exchanged between major powers.
If that happens, we're kind of doomed anyway.
To me, worrying about the hypersonic missile technology
is a little bit like worrying about whether or not
the rifle that shot you in the head had good optics.
It really doesn't matter at that point.
Once they start flying, it's over.
The other thing that's kind of missing from the reporting
is, or in some cases, it is very misleading,
is the idea that the US doesn't already
have some of this technology, that there isn't already
some parity, that we're somehow far behind.
Can't allow there to be a mineshaft gap, right?
Go ahead and Google US Army Dark Eagle.
It's not new technology.
It's been around a long time in the sense of people
trying to achieve it.
Over the last few years, the technological hurdles,
they've gotten over them.
So it's being fielded.
I would suggest that beyond what you're
going to find in public reporting,
the US has other technology already fielded.
I can't prove that.
I don't have any evidence to back that up,
other than this is how it historically works.
But even if they don't, even if the US does not
have that capability, it will soon.
Parity will be achieved, and it is what it is.
This is not something that I think
we should be drawing a whole bunch of attention to,
except in ways that suggest, hey, maybe we
should disarm when it comes to nuclear weapons.
Once they go up, it doesn't matter.
It doesn't matter if they're delivered via ICBM,
launched from a sub, hypersonic missile technology.
It does not matter.
If they go up, it's over.
The idea of a limited nuclear exchange is, I don't believe it.
I don't believe that it is something
that is super likely anymore.
If any of the world powers get into a position
where they're going to launch, they're going to launch.
That's where we're at now.
So I'm also not concerned about it,
not just from this standpoint of there being parity,
and if it goes up, we're not really
going to know about it anyway, but also
from the standpoint of they're going
to be very unlikely to use this technology, at least with nukes.
Major powers understand that there's no winning that.
No matter what, it's all bad.
The idea of newer technology generally
is to mitigate risk to your own and inflict
more on the opposition.
This guarantees a massive response.
It runs counter to everybody's doctrine.
So they're unlikely to use it.
I am far more concerned about those little robot dogs.
If you don't know, the US military
has outfitted the little robot dogs,
like you saw in Black Mirror.
They've outfitted some of them with rifles.
To me, that is a much larger concern
because they are far more likely to use it.
And that will change the face of warfare, and not in a good way.
And then on top of them creating this incredibly lethal
technology in the form of this dog
that I am simultaneously in awe of and constantly trying
to figure out how to destroy, they also
chose to chamber the rifle in 6.5 millimeter Creedmoor,
which means on top of it being incredibly scary,
it's also going to be insufferable.
Gun joke, sorry.
How do you know somebody's rifle's chambered in 6.5?
Don't worry, they will tell you.
At the end of this, this is a good example of two things.
One, more confirmation.
Prepare for Cold War style coverage in that atmosphere
because that's what's coming down the road.
Two, these stories, they broke around the same time.
The one that's getting the most attention
is the most dramatic, the hypersonic missiles.
But that's not really the one you
need to worry about the most.
That's not the one that's most likely to impact your life.
There's a missed priority when it comes to coverage.
They're focusing on the extreme.
And the hypersonic missile technology
is extreme, so extreme, in fact, that if it ever
does get used when it comes to nukes,
the coverage isn't going to matter because nobody's
going to be around to read it.
Those dogs, you could see them in action very quickly.
And then you could see them in your local law enforcement.
Far more of a concern.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}