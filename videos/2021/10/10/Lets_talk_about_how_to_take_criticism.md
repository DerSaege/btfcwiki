---
title: Let's talk about how to take criticism....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=4IFSJ6_ctf4) |
| Published | 2021/10/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talks about accepting feedback and criticism in a professional setting.
- Shares a scenario of a person receiving negative feedback on accepting feedback.
- Advises on the standard way of accepting feedback: pause, listen, and apply.
- Mentions a personal issue with feedback perception rather than accepting it.
- Suggests switching out thanking the critic with asking questions to actively listen.
- Emphasizes the importance of understanding feedback is not personal and viewing it as an asset.
- Encourages actively listening to feedback and applying it for personal growth.
- Acknowledges the challenge of waiting for feedback without appearing dismissive.
- Notes the importance of fitting into the standard way of accepting feedback in a professional environment.
- Recommends training oneself to ask relevant questions to appear engaged during feedback sessions.

### Quotes

- "It's not actually about getting better at accepting the feedback. It's about doing better at that evaluation."
- "View it as an asset. These are free consultants. These are people who are trying to make you better."
- "Train yourself to actively listen and make sure that you understand it's not personal."

### Oneliner

Beau shares advice on accepting feedback in a professional setting, focusing on active listening, understanding feedback isn't personal, and viewing it as an asset to improve.

### Audience

Professionals

### On-the-ground actions from transcript

- Train yourself to actively listen and ask relevant questions during feedback sessions (implied).

### Whats missing in summary

The detailed breakdown of Beau's personal experience and how it relates to feedback perception. 

### Tags

#Feedback #ProfessionalDevelopment #ActiveListening #GrowthMindset #Acceptance


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we're gonna talk about a life skill.
We're gonna talk about accepting feedback and criticism
and how to do it.
How to take the correction and move on.
I mentioned that in a recent video,
and one of you reached out.
The person has landed in their dream career field
with the company that they want to work with.
And they're new to the company.
They get their first evaluation.
And it says they're not good at accepting feedback.
And they asked about it.
And they said that it appears that they put up
a wall when they're given feedback.
Remember that.
Put up a wall.
And then the person asked for advice
on how to make sure they do better on that evaluation.
And remember that part.
It's not actually about getting better
at accepting the feedback.
It's about doing better at that evaluation,
because they're results-oriented.
So what we're going to do is we're
going to go through the standard way of accepting feedback.
And then I'm going to provide a little bit of personal advice,
because this person's issue may not actually
be about accepting feedback and criticism,
but more of the perception of their body language
when they're getting it.
And it may have nothing to do with accepting feedback.
So what's the standard when people have trouble accepting
criticism?
It starts with pausing.
Start off by just pausing, realizing there's
feedback or criticism coming, acknowledge it's not personal.
In the business world, generally speaking, people providing you with advice are not trying
to undermine you.
They're trying to make your product, idea, performance, yourself better.
Don't take it personally.
Then listen.
Actively listen to what is being said.
And while you're doing that, decide whether or not it's constructive or destructive criticism.
Take the critic, process the information and then apply it.
Those are the steps.
I had to read books on this because I was told I had the same issue and it turned out
it was something else and I think it's the same thing.
It's not about the criticism.
They said that it looks like you put up a wall.
me they said I put up a face I give off a face that basically was like hurry up
you've already dismissed this idea but that's not what it was. You're a results
oriented person you can determine that based on the message when it comes to
caring about the evaluation not actually about the the behavior and getting better
at accepting feedback. Because you are results oriented you would probably
prefer people to walk up and say this is what you're doing wrong, this is how to
do it right, and this is why one way is better than the other, and then walk off.
That probably wouldn't bother you at all. The thing is that's not how people
provide feedback or criticism in the business world. They use some managerial
technique and they sugarcoat it, they give you a compliment sandwich or whatever.
Because you are results oriented, during this sugarcoating phase, you're waiting for them
to get to the point and it shows.
That's your wall.
For me, that was my face.
I did seem like I was dismissing it because I was.
I was waiting for them to get to the point.
The sugar coating aspect occurs in the business world and there's nothing you can do about
it because most people aren't of the sort who would just prefer somebody to walk up
and be like, this is what you did wrong.
This is how to fix it.
That would bother most people.
That is seen as bad management overall, even though that's what you would prefer.
So what I have found is switch out in those steps, switch out the thank the critic part
for ask questions.
And what you're doing is you're training yourself to actively listen to the whole thing.
So you can ask questions about it.
And since you are actively listening to the whole thing, rather than just waiting for
the important part, the actual feedback and criticism, you don't put off that wall.
You don't give off that face.
My guess, based on your message, is that that's what it is.
You probably don't get offended by the feedback.
You don't get upset that people are criticizing you.
You probably view it as an asset, but you want them to get to the point.
Understand that that's not how most people work.
that you are an outlier in preferring to receive your criticism that way.
So the rest of the world has developed this standard way of doing it that is very different
than what you would prefer.
You have to find a way to fit into that or at least hide the fact that you're waiting
for them to get to the actual point.
So train yourself to ask questions that are relevant to what was said.
So you appear as though you're listening.
You were listening the whole time.
You were waiting for the important part, the actual criticism, the part that matters.
But you don't look like you are.
You look like you're dismissing the whole thing.
And then it gets even worse because you appear as though you're bad at taking criticism.
people sugarcoat it more, which makes it even worse because you stand there with
that face even longer or that wall up even longer, waiting for them to get to
that point. Based on your message, I'm pretty sure that's what it is.
I don't know you, I can't say that for certain, but just based on the way
things were phrased. It really is about results and when people in a business
environment are providing criticism they take their time and you have stopped
what you're doing and you're annoyed at having to sit through these are the
things you're doing right when they could just tell you what you're doing
wrong and so you can move on and get back to work.
So train yourself to actively listen and make sure that you understand it's not personal.
If there is some ego involved, it's not personal.
View it as an asset.
These are free consultants.
These are people who are trying to make you better.
Accept it, listen to it, and then apply it.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}