---
title: Let's talk about Biden's monumental move....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=b1q06fF74BA) |
| Published | 2021/10/10|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Biden's administration restored the sizes of protected areas in the U.S. that Trump had shrunk, using the Antiquities Act.
- The impacted areas include Bears Ears, Grand Staircase, Northeast Canyon, and Cimonts.
- The celebration isn't just a PR move; there are multiple reasons for cheering the restoration.
- The restoration protects artifacts, burial sites, structures, fossil dig sites, and biodiversity.
- Trump's actions seemed to pave the way for mining and fossil fuel exploration, which could harm the environment.
- Native groups see ancient sites as messages from their ancestors, making their protection culturally vital.
- The Biden administration gave a native coalition a significant role in managing the protected areas, a positive step.
- Beau believes Trump's actions against the protected areas were against the law and should have gone to court.
- While a court ruling was missed, the priority remains protecting the sites rather than legal battles.
- The downside is not getting a legal ruling against Trump's actions due to the restoration rendering it moot.
- The restoration is vital for protecting areas that were lacking proper safeguards, although some damage done may be irreversible.

### Quotes

- "The celebration isn't just a PR thing; there's a lot to celebrate."
- "Having them around is incredibly important from a cultural standpoint."
- "It's far more critical to protect the sites than to get a ruling."
- "The restoration extends protections to areas that need it."
- "Undoubtedly, there was damage done during that window that will never be repaired."

### Oneliner

Biden's administration restores protected areas Trump shrank, celebrating cultural significance and environmental protection despite missed legal battle.

### Audience

Environmentalists, Conservationists, Activists

### On-the-ground actions from transcript

- Support and advocate for the protection of natural and cultural heritage sites (exemplified)
- Get involved with native coalitions working towards safeguarding protected areas (exemplified)

### Whats missing in summary

The full transcript provides more detailed insights into the significance of protecting these areas and the potential long-term impacts of environmental degradation if proper safeguards are not in place.

### Tags

#ProtectedAreas #EnvironmentalProtection #CulturalHeritage #NativeCommunities #AntiquitiesAct


## Transcript
Well, howdy there, internet people.
Lidsbo again.
So today we are going to talk about Biden's monuments,
landmarks, what he just did, this big move.
Everybody's cheering it and questions have come in
as to why it's such a big deal
and why nobody's being specific on why it is a big deal
and whether or not it's just a PR thing
that people are celebrating.
Okay, so the Trump administration cut the size
of certain protected areas within the United States.
The Biden administration just restored all of that.
They used the Antiquities Act to do it.
Those being impacted are Bears Ears, the Grand Staircase,
the Northeast Canyon, and Cimonts.
So basically Trump shrank all of these places.
The Biden administration restored it.
So why is it a big deal and is it just a PR thing?
It is definitely not just a PR thing.
The reason people aren't being specific
in why they're celebrating
is because there's a lot to celebrate.
The, let's just off the top of my head,
the Anasazi had a lot of sites there.
So there's a lot of artifacts, burial sites, structures,
stuff like that that are now gonna be protected again.
There are fossil dig sites that were in jeopardy
until the protections were restored.
The biodiversity in general.
And then aside from all of this stuff
that creates a wide coalition of people
that are really happy,
you have the fact that when the Trump administration
did this, it certainly seemed as though
they were gearing it up to allow for mining
and fossil fuel exploration and stuff like that,
which would have further degraded the environment.
So that's why you have a whole bunch of people cheering
and nobody really talking about the specifics
because it really is too much to list,
especially when you're talking about the native groups
that are involved with this.
In many ways, they view the ancient sites
as emails from the past,
messages from their ancestors that are still,
still need to be read every once in a while.
So having them around is incredibly important
from a cultural standpoint.
And then the Biden administration in this action
actually gave a native coalition a pretty central role
in making sure everything is run properly,
which seems like a really good idea.
Are there any downsides to it?
To me, there is one.
I am of the belief that the Antiquities Act
allows presidents to establish things like this,
establish protected areas, establish parks like this.
I don't believe it allows them to reduce them in size.
The whole idea of this was to set it aside and protect it.
I believe that Trump's actions
were actually against the law.
So I would have liked to have seen this go to court.
At the same time, it's far more important
to actually protect the sites
than it is to get a ruling.
Because if it happens again, it will go to court.
To me, that's the only downside to this,
is that we didn't get the ruling in time
because now that the protections have been restored,
courts aren't gonna wanna hear it.
It's moot.
That's it.
That's the downside.
Nothing else associated with this is good.
So it extends protections to areas
that need to be protected and that were lacking it.
And undoubtedly, there was damage done
during that window that will never be repaired.
So anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}