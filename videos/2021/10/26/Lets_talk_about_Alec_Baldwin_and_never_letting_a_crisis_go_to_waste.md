---
title: Let's talk about Alec Baldwin and never letting a crisis go to waste....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=sPPVtdWTgTc) |
| Published | 2021/10/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Alec Baldwin was on set filming a movie called Rust when he was handed a prop gun with a live round, resulting in a woman's death and another person's injury.
- The gun community's response involved mocking the incident, with Donald Trump Jr. selling shirts making fun of Baldwin.
- Beau criticizes the lack of leadership following the tragedy and the missed chance to reinforce gun safety.
- Approximately 42 people end up in the hospital daily due to unintentional gunshot wounds, with about 500 of them not surviving each year.
- Beau points out that the incident could have been an educational moment for the gun community to revisit basic gun safety practices.
- He suggests that this was an ideal time to remind people, especially those in the gun community, of the importance of gun safety.
- There is a call for leadership during such incidents to provide guidance and education on firearm safety.
- The lack of leadership in the United States, particularly on the right, is criticized as being filled with internet trolls in positions of power.
- Beau stresses the importance of always treating every weapon as if it is loaded and following strict gun safety protocols.
- The most dangerous firearm is the one assumed to be unloaded, making vigilance in handling firearms critical.

### Quotes

- "Guns don't kill people, Alec Baldwin kills people."
- "This incident could have been used to reinforce gun safety, rather than mock it."
- "The single most dangerous firearm in the world is the one that you are pretty sure is unloaded."
- "They don't have leaders. They have what amounts to internet trolls who found their way to positions of power."
- "It's your friends at stake. It's your community."

### Oneliner

A missed chance for leadership to reinforce gun safety following Alec Baldwin's tragic on-set incident, urging vigilance in firearm handling and education.

### Audience

Gun owners, advocates

### On-the-ground actions from transcript

- Educate your community on basic firearm safety practices (implied)
- Remind friends and family about the importance of gun safety (implied)

### Whats missing in summary

The emotional impact and tone of Beau's message, as well as the urgent call for responsible leadership and education in the aftermath of tragic incidents.

### Tags

#GunSafety #AlecBaldwin #Tragedy #FirearmHandling #Leadership


## Transcript
Well howdy there internet people, it's Beau again.
So today we're gonna talk about the Alec Baldwin thing.
We're gonna talk about what happened
and we're gonna talk about the response to it.
And we're going to talk about
never letting a crisis go to waste.
And we're gonna talk about leadership and the lack of it.
If you are unaware, if you don't know what occurred,
Alec Baldwin was on set.
He was on set filming a new movie,
I believe it's called Rust.
And he was handed what they are calling a prop gun.
It wasn't, it was a gun being used as a prop.
Reporting suggests that he was told it was empty.
It was not.
There was a live round in it.
He fired it.
And a woman lost her life.
Another person was wounded.
The response to this from the gun community
has been to mock it, to make fun of it.
Donald Trump Jr. started selling shirts that say
guns don't kill people, Alec Baldwin kills people.
I get it, I mean it's like a woman lost her life.
It's super funny, I understand.
There's that saying, you know, you never let a crisis go to waste.
And when people say it, they normally put it into a narrative
that implies something nefarious happening.
You know, people using an event that happened to further their own agenda.
That doesn't have to be the case.
See, this incident, this could have been used to reinforce gun safety,
rather than mock it.
It could have been used to point out to those people in the gun crowd
who are currently making fun of this,
that today, about 42 people, statistically speaking,
42 people will end up in the hospital with an unintentional gunshot wound.
27,000 a year.
Roughly 500 of them do not make it.
One a day is lost to unintentional shootings.
And I get it, right now, the gun crowd, you know,
they don't like Alec Baldwin, so they're mocking it.
But understand the other 499 or so are people in the gun crowd,
or they are at least gun crowd adjacent.
They are friends, family, the children of people in the gun crowd.
This would have been the opportune time to go over basic gun safety stuff.
Remind people of it.
It's your friends at stake.
It's your community.
I don't know if there was any real leadership.
But see, the right in the United States, they don't have leaders.
They have what amounts to internet trolls
who found their way to positions of power, fortune, and fame.
And they act as though they are still internet trolls.
This would have been the opportune time to point to these numbers.
This would have been the opportune time to explain to everybody,
if you are ever handed a weapon, if you are ever handed a firearm,
you keep your finger off the trigger, you keep it pointed in a safe direction,
and you visually inspect the chamber.
You look every single time.
You do that.
The single most dangerous firearm in the world
is the one that you are pretty sure is unloaded. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}