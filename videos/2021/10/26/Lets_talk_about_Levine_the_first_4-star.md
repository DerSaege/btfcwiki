---
title: Let's talk about Levine, the first 4-star....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6JkGIEqdoFU) |
| Published | 2021/10/26|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing information consumption and a message received about the "new four star."
- Expressing issues with incorrect name usage and putting "woman" in quotation marks.
- Sharing the story of Ann Dunwoody, the first woman four star back in 2008.
- Pointing out Admiral Michelle Howard as the first woman four star admiral in Navy history.
- Calling out Representative Jim Banks for spreading false information to manipulate anger.
- Emphasizing the trend of manipulating people by fabricating stories to evoke outrage.
- Questioning the ease with which people are manipulated by untrue narratives.
- Encouraging fact-checking or simply being nice as alternatives to falling for manufactured outrage.

### Quotes

- "The fact that you do shows how easily they can manipulate you with something that isn't true."
- "Completely manufactured. Completely made up."
- "Is it that hard just to fact-check?"
- "Just be nice."
- "Y'all have a good day."

### Oneliner

Beau addresses the manipulation through fabricated stories to provoke outrage, urging fact-checking or kindness over falling for manufactured anger.

### Audience

Social media users

### On-the-ground actions from transcript

- Fact-check before sharing information (suggested)

### Whats missing in summary

Deeper exploration of the impact of fabricated stories on manipulating public outrage and the importance of critical thinking and kindness in response.

### Tags

#InformationConsumption #ManufacturedOutrage #FactChecking #Kindness #SocialMedia


## Transcript
Well howdy there internet people, it's Beau again. So today we're going to talk
about information consumption, a little bit, and we're going to go over a message that I
got and this message could lead to a whole bunch of different points, all of
which deserve to be made. But I want to focus on one in particular. It's about
the new four star.
Ok, so, Beau, are you or any of your friends angry about... I don't know who
this person is. I know who Rachel Levine is. If you can't call her Rachel, you
could just call her Admiral. Being the first woman four star, it's outrageous.
The title of first female four star gets taken by a man. Do you have an issue
with this? I got a bunch of issues with this. I got an issue with you using the
wrong name. I got an issue with you putting woman in quotation marks. I have
a whole bunch of issues with this. But I think my biggest issue is the same issue
that Ann Dunwoody has. You may not know who she is. Ann Dunwoody, by all accounts,
brilliant officer, brilliant officer, the first woman battalion commander in the
82nd Airborne, and when it happened, everybody doubted her. There were a whole
lot of people who wondered if she was up to the task. Turns out she was. She then became
the first woman general officer to command Fort Bragg. And then she became
the first woman four star back in 2008. It's not like she was stuffed away in
some tiny command somewhere. She was over Army material command out of Redstone.
That's almost 70,000 troops. It seems like if this is something that could
outrage you, you would know that the first woman four star occurred more than
a decade ago. If it was something that could make you that angry. And I know, maybe you're
just thinking, well, they didn't really lie to me about this. They just didn't think
about the generals, you know. Levine, she's the first four star admiral. No, no.
The first woman four star admiral is Admiral Michelle Howard. Admiral Michelle
Howard, who also had just an amazing career. I want to say she was the first
black woman to command a US ship. I think it was the Rushmore. Then she eventually
became vice chief of naval operations. And when she was sworn in, she became the
first, I want to say, the first black four star in Navy history and the first woman
four star in Navy history at the same time. And that happened back in 2014. The
whole thing, it's just not true. It's made up. Am I outraged by it? Does it bother me?
Do I have an issue with something that you just made up? No, of course not. But see,
the thing is, I also know you didn't make it up. Representative Jim Banks did.
Because that last line, the title of first female four star, that's a direct
quote, right? So what he got in trouble for on Twitter. It's made up. It's not
true. You could get this information off of Wikipedia. It's not true. It's just
simply manufactured so you will get angry. You can point at that other group.
Look at this horrible thing they did. Oh no. And be mad. Kick down at people.
It's not even true. They made it up to manipulate you. To get you angry. To get
you outraged. So outraged, in fact, that you are messaging people on YouTube. It's
not true. Do I have a problem with Admiral Levine getting a fourth star? I
don't care. And I'm fairly certain, I haven't asked, but I'm fairly certain
that none of my friends care either. Generally speaking, my friends wore scars
not stars. I don't think they really care that much. The fact that you do shows how
easily they can manipulate you with something that isn't true. Completely
manufactured. Completely made up. And this is a trend. They get you outraged about
something by just saying it. Putting it out over Twitter and you're quoting it
as fact. You're outraged by it. It never even happened. Is it that hard just to
fact-check? And if you can't fact-check, just be nice. Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}