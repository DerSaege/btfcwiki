---
title: Let's talk about a Sputnik moment and it not being one....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=loXcr5cZaZU) |
| Published | 2021/10/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- General Milley testified about Chinese developments and hypersonic missiles, potentially signaling a new arms race.
- Milley mentioned the Department of Defense needing to make strategic shifts, reprioritize, and spend money in response.
- Dr. Cameron Tricy, an expert on nuclear arms control, will provide insights in an upcoming interview.
- The new missiles aren't significantly faster, stealthier, or undetectable, challenging the narrative of a game-changing technology.
- Despite lacking funding for infrastructure, the defense budget will likely support these new weapons.
- Beau warns against another arms race and advocates for preparing to face the evolving threats.
- He criticizes the defense industry's potential exaggeration of new threats and the swift funding approval they receive.
- Beau questions the focus on defense spending while domestic issues remain unaddressed.
- The shift towards near-peer adversaries resembles the dynamics of the Cold War, with propaganda and arms race implications.
- Beau suggests a reevaluation of national priorities towards more productive endeavors.

### Quotes

- "We don't need another arms race. We need to inoculate ourselves."
- "If the United States is crumbling from within, what good is defense spending?"
- "Maybe we need to focus on things that are more productive and less destructive."

### Oneliner

General Milley's testimony on Chinese developments and hypersonic missiles may trigger an arms race, demanding strategic shifts and significant spending, while Beau calls for a reevaluation of national priorities.

### Audience

Policy makers, activists

### On-the-ground actions from transcript

- Pay attention to the potential exaggeration of threats by the defense industry (implied)
- Advocate for a reevaluation of national priorities towards less destructive endeavors (implied)

### Whats missing in summary

Dr. Cameron Tricy's insights on nuclear arms control and the nexus between science and security policy.

### Tags

#NationalSecurity #ArmsRace #DefenseSpending #ChineseDevelopments #HypersonicMissiles


## Transcript
Well howdy there internet people, it's Beau again.
So today we're going to talk about General Milley's testimony
and Sputnik moments and missiles, again,
hypersonic missiles.
I did a video about them recently
and laid out some basic facts.
Milley, well, he testified.
And he said that he didn't know if the recent Chinese
developments constituted a Sputnik moment.
But it was really close.
And in one way he's right.
In one way he is right.
But it's not that there's this giant gap.
There's not this giant gap in capability.
There's not this giant gap in technology.
There is not a mine shaft gap.
Something else.
It's going to trigger a race, just like Sputnik did.
A race fueled on unsubstantiated worry.
Later on in that testimony he says
the DOD is going to have to make some moves,
shift things around, reprioritize, spend some money.
That's what they're going to have to do.
It's going to become an arms race.
Down below I'm going to have an interview.
I'm going to have a LinkedIn interview with Dr. Cameron
Tricy.
He's an expert on nuclear arms control.
Most of his work centers on the nexus between science
and security policy.
It's what he knows.
He's very direct about some things
that probably need to be made very clear.
Are these missiles faster?
No.
Are they stealthier?
No.
Are they undetectable?
No.
Not really the game changer it's being made out to be.
But to develop this, to get it fielded,
to replace all weapons, billions upon billions
upon billions of dollars.
When that time comes, when the money
needs to be approved to do this, rest assured it will be.
We can't get money for infrastructure,
but we can get money to defend it.
If the United States is crumbling from within,
what good is defense spending?
What exactly are you defending?
We don't need another arms race.
We need to inoculate ourselves.
We need to get ready for this because it's coming.
The shift to near peers is going to look like the Cold War.
We've been talking about it coming.
We said it's going to come.
It's here.
We're going to start seeing the arms race.
We're going to start seeing the propaganda.
It's going to shift.
The defense industry that has made so much money
over the last 20 years countering a different threat,
they need a new boogeyman bad.
And it appears as though the think tanks,
they seem willing to feed DOD brass information that
many scientists are skeptical of.
They seem willing to upplay and exaggerate
the differences in capability.
It's something that we, as we struggle
to get basic infrastructure needs met in the United States,
need to pay attention to.
Because I guarantee you, Manchin and the other ones
aren't going to hold up the funding for this.
They're going to let that go through right away.
Maybe the United States needs to shift its priorities.
Maybe we need to focus on things that are more productive and less
destructive.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}