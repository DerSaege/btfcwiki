---
title: 'Let''s talk about water, taxes, and #TeamSeas....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=z58RmpkqTmU) |
| Published | 2021/10/29|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Taxes are paid without choice, but politicians dictate where the money should not be spent based on contributors' interests.
- Politicians may not accurately represent the desires of the country when it comes to environmental spending.
- Americans may actually be enthusiastic about funding initiatives for clean rivers, beaches, and oceans.
- Team Seas, led by Mr. Beast on YouTube, aims to raise $30 million to remove trash from rivers, beaches, and oceans.
- Each dollar donated will remove one pound of trash to prevent it from reaching the ocean.
- The funds raised will be split between the Ocean Conservancy and Project Ocean Cleanup with the UN.
- To donate to Team Seas' cause, visit teamseas.org.
- The website for Team Seas was not published at the time of filming but should go live when the videos are released.
- Joining this initiative can send a message to Washington that Americans want their money spent on environmental conservation.
- Supporting Team Seas shows a commitment beyond tax dollars, indicating public interest in environmental causes.

### Quotes

- "Americans may actually be enthusiastic about funding initiatives for clean rivers, beaches, and oceans."
- "We can send a pretty clear message to DC that this is something that we want our money spent on."
- "When they waste our tax dollars on something else, we want it so much we'll pick up the slack."

### Oneliner

Americans want their money spent on environmental conservation, supporting Team Seas' initiative to clean rivers, beaches, and oceans.

### Audience

YouTube viewers

### On-the-ground actions from transcript

- Donate to Team Seas at teamseas.org (suggested)

### Whats missing in summary

The full transcript provides detailed insights into the disconnect between politicians' spending priorities and the public's desire for environmental conservation funding. Viewing the full transcript can offer a deeper understanding of the narrative.

### Tags

#CleanWater #EnvironmentalConservation #TeamSeas #MrBeast #CommunityAction


## Transcript
Well, howdy there, Internet people. It's Beau again. So today, we're going to talk about
clean water and taxes and Team C's. You know, one of the things that's always kind of funny
to me is that, you know, we all pay taxes, right? We don't really have a choice. We definitely
have to do that. But once they're paid, this weird thing occurs. Politicians come out and
tell us what we don't want the money spent on. I mean, what really happens is they talk
to their contributors and their contributors give them a policy outline and then they convince
their constituents that that's what the constituents always wanted. They tell us what we don't want
our money spent on. The thing about that is that I don't really consider politicians as
a demographic that really has their finger on the pulse of the country. They probably
don't know. We hear it a lot when we're talking about the environment. Americans don't want
to pay for clean rivers, clean beaches, clean oceans. They don't want to pay to plant trees.
They don't want to pay to deal with climate change. It's too expensive. We'll leave our
children with a bill. I mean, they don't care about this when it's, you know, for bombs.
But we hear it a lot. I don't believe that's true. I don't think that's true. I think Americans
are enthusiastic about keeping the country and the planet clean. I think Americans want
clean rivers, clean beaches, clean oceans. I think that's something that they want. Now,
what does Team Seas? There's a small channel on YouTube. You've probably never heard of
it. It's called Mr. Beast. His team reached out to, from my understanding, hundreds of
people on YouTube. And the idea was to get everybody to release a video right now, when
this one got released, pushing a single project. And that single project is to raise $30 million.
Thirty million dollars. Each dollar takes one pound of trash out of rivers, off of beaches,
and out of the ocean. Rivers and beaches, the trash ends up in the ocean eventually.
So this is kind of stopping the problem before it gets there. The money will be split between
the Ocean Conservancy and, I want to say it's Project Ocean Cleanup with the UN. That's
where the money's going to go. If you want to be a part of this, it's teamseas.org. That's
how you can donate. What's on the website? I don't know, because this whole thing's supposed
to be like super secret and everything. The website wasn't published at time of filming.
It should go live at the exact time these videos go up. If you want to be a part of
what is probably going to be the largest collaboration crossover in YouTube history, head over to
teamseas.org and drop them a few dollars. We can send a pretty clear message to DC that
this is something that we want our money spent on. Not just do we want our tax dollars to
go to it. When they waste our tax dollars on something else, we want it so much we'll
pick up the slack. Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}