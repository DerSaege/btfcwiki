---
title: Let's talk about the message from Southwest pilots....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=Y6G7DQ68aGY) |
| Published | 2021/10/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Southwest pilots are sending a message by standing up against misinformation and opposition to mandates, which has been debunked.
- The sick rate for pilots at Southwest remains the same, indicating there is no widespread coordinated action.
- Some pilots may be using sick time before leaving Southwest, but there is no evidence of a coordinated effort.
- Aviation experts explain how Southwest's system is susceptible to cascade failure, leading to the recent flight cancellations.
- Despite information debunking the narrative, certain individuals continue pushing it because they know it will work on a segment of the population.
- Those perpetuating misinformation rely on a segment of the population trained not to question but to believe blindly.
- Leaders like Trump, Boebert, Cruz, and media personalities exploit patriotism and lack of critical thinking in their followers.
- Individuals pushing false narratives have a history of misleading the public on various issues for their benefit.
- True leaders empower others to lead from their communities rather than manipulate them with false information.
- People who continue to push debunked narratives are exploiting and monetizing those who believe them, rather than educating them.
- Blindly following without evidence or critical thought is detrimental to society and plays into the hands of those spreading misinformation.
- The incident at Southwest is a minor example of a larger trend within the right wing that relies on anger and manipulation.
- Individuals should demand evidence from those making claims to avoid being emotionally manipulated and misled.
- Emotional manipulation and lack of critical thinking enable certain individuals to profit off spreading misinformation.

### Quotes

- "They have found a way to monetize you."
- "Smart people want to empower you to lead from your own community."
- "Ignore evidence. And listen to people who never provide the evidence they say they have."
- "They have gathered a group of middle-aged, middle-income, middle-America."
- "Start asking these people for evidence of their claims. You'll find out they don't have it."

### Oneliner

Southwest pilots debunk coordinated action narrative; misinformation benefits from blind belief and emotional manipulation.

### Audience

Community members, critical thinkers.

### On-the-ground actions from transcript

- Demand evidence from individuals spreading misinformation (implied).
- Empower others in your community to lead and think critically (implied).
- Stop listening to those who continue pushing debunked narratives (implied).

### Whats missing in summary

The importance of critical thinking and demanding evidence in combating misinformation and manipulation.

### Tags

#Southwest #Misinformation #Leadership #CriticalThinking #CommunityLeadership


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we're going to talk about Southwest again.
And the message that is being sent,
and the message that everybody should latch onto
that's coming from those pilots, that's
coming from those people who are standing up.
OK.
So if you don't know, the whole idea
of some widespread action designed
to voice opposition to mandates has been debunked.
It has been flatly and roundly debunked.
It isn't happening.
The on top of the statement from the FAA, the airlines,
the pilots union themselves, you now
have the fact that the sick rate for this period
is the exact same as it was last summer.
That alone indicates that there is no widespread action.
That alone does it.
Now, I'm actually still open to the idea
that there are some pilots burning
through their sick time before they end up leaving Southwest.
I'm still open to that.
But the idea that there's some widespread coordinated action,
it's been debunked.
You have had aviation experts and consultants
go to great lengths to explain how Southwest uses
a different system than most airlines
and how it is more susceptible to cascade failure, which
is what occurred.
You had pilots out of place.
Flights got canceled.
Then more flights got canceled.
And it caused a domino effect.
This is all information that's out there.
So is there something we can learn from this?
Absolutely there is.
There are still people pushing this narrative now.
Even though this information is out there,
even though it's been flatly debunked by everybody involved,
there's still people pushing it.
Why?
Because it'll work.
Because they know a large segment of the population
has been trained to not believe their eyes and ears,
but to believe the party, Winston.
If they tell you that 2 plus 2 equals 5,
you'll believe it without evidence.
That's a pretty significant portion of the country.
And if you look at it, it's the usual suspects.
It's Trump.
It's Boebert, Cruz, your favorite talking heads, right?
And they're going to continue doing it
because they'll get away with it.
Because their base, they've been trained
not to ask questions, to do what they're told,
to not look for evidence.
They'll continue doing it because it works.
It worked before.
The same people that had you supporting an imaginary strike
are the same people who told you there
was an issue with the election, are the same people who
told you to ignore the advice of public health experts
and to listen to them.
It's the same crew of people.
And because it continues to work,
they will continue doing it.
All they have to do is say they love America
and play on a distorted sense of patriotism.
And they'll get a whole lot of people saying that 2 plus 2
equals 5.
That's it.
When the reality is, if these people really
loved this country, they'd get out of the public light.
If you're wrong this often about things that
have consequences this severe, if you loved the country,
you would try to stop influencing events
because you're not very good at it.
If your actions lead to the loss of 600,000,
700,000 people lead to the events of the 6th,
you would think at some point you would know when to sit down.
But that's assuming they want to be leaders.
They don't.
They never did.
Leaders don't want followers.
They want more leaders.
If there were people who were truly invested in this country,
they wouldn't be telling you, well,
you have to come to me to the evidence
that I'm never going to show you.
They wouldn't be saying that.
They would be empowering you to lead from your own community.
Smart people don't like to exploit the uneducated.
Smart people want to educate them.
If you are ill-informed and you find somebody playing on that
and continuing to push a narrative
after it's been debunked, you should probably
disregard what they're saying.
You should probably stop listening to them
because they are playing you.
They have found a way to monetize you.
But if they stand in front of an American flag,
well, that's how you make America great again.
Just do what you're told.
Ignore evidence.
And listen to people who never provide the evidence
they say they have.
There's a really important message.
This is a minor thing.
This is a minor thing.
This particular incident is small.
It doesn't have a lot of major impacts.
But the reality is it is symptomatic of something
that is going on within the right wing of this country,
where they just believe whatever they're told.
Because all it takes is to get them angry.
That's what this crew of people have found.
They have gathered a group of middle-aged, middle-income,
middle-America.
They have told them who to hate, who to blame.
And they found a way to make money off of it.
They don't care about the country.
Start asking these people for evidence of their claims.
You'll find out they don't have it.
They're using emotional manipulation
to keep you in line, Winston.
Anyway, it's just a thought.
You have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}