---
title: Let's talk about the new Superman and the supermad....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=6lx2txSsPw4) |
| Published | 2021/10/12|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Addressing the backlash surrounding Superman's son, John Kent, being portrayed as bisexual.
- Pointing out the hypocrisy of people claiming they don't care about superheroes' love lives when it has always been a central aspect of comic book characters.
- Listing various comic book characters who have had love interests throughout the comics.
- Calling out the underlying bigotry in objecting to John Kent's bisexuality.
- Emphasizing the importance of representation for individuals who may identify as bisexual.
- Questioning the lack of understanding and acceptance from those who object to LGBTQ+ representation in comic books.
- Reminding readers that comics often convey messages of overcoming biases and supporting each other.

### Quotes

- "It isn't that. It's that you don't like this particular kind. That's what it is. Yes, it's bigotry."
- "What happens if your kid is bi, and they don't see any representation for how they are, who they are?"
- "Most comics have that overriding theme that everybody's a hero, and that all we have to do is support each other."

### Oneliner

Beau addresses the backlash against Superman's son being portrayed as bisexual, calling out the bigotry and hypocrisy surrounding objections to LGBTQ+ representation in comics.

### Audience

Comic book fans, LGBTQ+ advocates

### On-the-ground actions from transcript

- Support LGBTQ+ representation in comic books (suggested)
- Provide positive support to individuals in your life who identify as LGBTQ+ (implied)
- Overcome biases, prejudices, and bigotry in your interactions (exemplified)

### Whats missing in summary

Importance of accepting and celebrating diverse representations in media and storytelling.

### Tags

#Superman #LGBTQ+ #Representation #Comics #Acceptance


## Transcript
Well howdy there internet people, Edsbo again.
So today we're going to talk about Superman,
because there's a whole lot of people out there
who need to go buy some capes because they're super mad.
This is volume 783,612 in the series of videos responding
to when did this become so woke, and the answer was always.
If you don't know, John Kent, who is Clark Kent and Lois Lane's
son, well he's Vi.
And there are a whole lot of people who are mad about this.
And the talking point that is being pushed out
is that, well, we don't care about the superhero's
love life.
That's not what we're interested in.
What we're interested in is them out there doing superhero stuff
and fighting the bad guy, which is a hilarious take coming
from people who are talking about a character that only
exists because of a superhero's love life.
I mean, that seems odd.
You know, Superman and Lois Lane, that whole thing.
I mean, but I get it because it's not
like other characters also had love interests
throughout all of the comics ever,
and it's incredibly common.
It's not like Batman had Catwoman, Natalia, Vicky Vale,
Julie Madison.
OK, bad example because he did have a whole lot because he,
you know, rich, single guy.
Punisher never had anybody except for Katherine O'Brien,
Glenn Michaels, Vicky White.
OK, maybe not a good thing from the Fantastic Four,
the giant rock guy.
Yeah, I mean, it's not like he ever, oh, he got married.
He did.
He got married.
Alicia Masters.
OK, bad example.
Wonder Woman.
Wonder Woman.
Nothing gets more wholesome than Wonder Woman.
OK, so Tom Tressor, Trevor Barnes, Aquaman, Batman,
Superman.
Oh, and she was bi.
With all of this information, you
have to acknowledge that it's not,
that you don't care about the superheroes' love life,
because you do.
It's a part of every single one of them.
They all have one.
It's integral.
It isn't that.
It's that you don't like this particular kind.
That's what it is.
Yes, it's bigotry.
It is.
I would point out that the reality
is that most people who are objecting to this
don't actually care about the love
life of a comic book character being inserted into comic books,
because it's been that way since the 30s or 40s.
They care about John Kent somehow influencing
their child, and then their child
will turn out that way, right?
That's the idea, and that's silly.
Doesn't make any sense.
I have a reverse question here.
What happens if your kid is bi, and they
don't see any representation for how they are, who they are?
Are they going to get positive support from you
if you're opposed to this?
Probably not.
If you can't stand for a comic book character to be bi,
probably not going to be real supportive of somebody
in your life that way.
And maybe that's the problem.
You know, most comics, they have that overriding theme
that everybody's a hero, and that all we have to do
is support each other.
For people who read comics, doesn't
seem like you really got any of the messages.
Most of them are about overcoming biases, prejudices,
and bigotry.
In one way or another, those themes
are pretty much always present.
When did it become awoke?
Always has been.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}