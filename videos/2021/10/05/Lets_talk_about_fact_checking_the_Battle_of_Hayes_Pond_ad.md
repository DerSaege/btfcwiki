---
title: Let's talk about fact checking the Battle of Hayes Pond ad.....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=0njIHlsQWCQ) |
| Published | 2021/10/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- A candidate in North Carolina, Charles Graham, put out an ad referencing an event from the 1950s, leading to viral attention and questions about its accuracy.
- The event in question, the Battle of Hayes Pond, involved a confrontation between James "Catfish" Cole, a Klan leader, and the Lumbee tribe in 1958.
- Cole, a WWII veteran and Klan leader, organized motorcades with the police, causing fear and chaos in neighborhoods.
- On the opposing side was Robert Williams, supported by the NAACP, who formed the Black Armed Guard for community defense.
- In October 1957, Cole's motorcade targeted Dr. Perry's house but was met with armed resistance from the Black Armed Guard.
- Cole then shifted his focus to the Lumbee tribe, despite warnings from local police about potential consequences.
- The Lumbee tribe planned a strategic ambush near Hayes Pond, surprising and defeating Cole's group.
- The defeat at Hayes Pond drew significant attention, leading to the denouncement of the Klan by the governor and Cole's arrest.
- The events at Hayes Pond and the defeat of the Klan resulted in a decline in Klan activities in the area.
- Despite some minor disputes about details, the accuracy of the ad regarding the Battle of Hayes Pond remains largely undisputed.

### Quotes

- "What does it say that an account of the Klan being defeated is still a good political ad today."
- "The defeat at Hayes Pond drew significant attention."
- "It led to the governor denouncing the clan."
- "The accuracy of the ad regarding the Battle of Hayes Pond remains largely undisputed."
- "I don't see that as being very material to anything."

### Oneliner

A political ad referencing the Battle of Hayes Pond in the 1950s sheds light on Klan defeat, prompting questions about its relevance in modern American politics.

### Audience

History buffs, activists

### On-the-ground actions from transcript

- Support community defense groups (implied)
- Advocate for historical events that showcase resistance against hate groups (implied)

### What's missing in summary

The full transcript provides a detailed historical account of the Battle of Hayes Pond, shedding light on a significant event in the fight against the Klan in 1958.

### Tags

#Klan #History #Resistance #CommunityDefense #NorthCarolina


## Transcript
Well, howdy there, internet people.
Lidsbo again.
So today, we're going to talk about Hayes Pond.
If you are unaware, if you've missed it, a candidate in North
Carolina by the name of Charles Graham, he put out an ad
referencing an event that occurred in the 1950s.
And it's gone pretty viral.
Have a whole bunch of questions that came in.
Basically, is it true, fact check it, that kind of thing.
If that's all you want to know, yeah, that's accurate.
The only thing that is disputed in that, that I'm aware of,
is whether or not one of the acts
was intentional or it happened by accident.
But as far as it being factually accurate, everything in that
is historical record.
If anything, that ad plays down how cool that event really
was.
So today, we're going to do a little Paul Harvey thing.
And you're going to get the rest of the story when it comes
to the Battle of Hays Pond.
OK, so it starts with a guy named James Catfish Cole.
Catfish being the nickname.
Well, he served during World War II.
And he was a minister and a taxi driver.
He was also the grand dragon over the Klan
in North and South Carolina.
He had motorcades, is what they called them,
where they would drive through neighborhoods,
scare people, yell at them, fire out the windows,
stuff like that.
They did this accompanied by the cops.
cop said it was to maintain order.
Cole was pretty popular.
His rallies drew thousands.
Now, on the other side, you have Robert Williams
with the NAACP.
He was funded by a doctor, Dr. Perry.
Now Williams, he's like, we got some World War II vets
our own, you know. And he reached out to the NRA, National Rifle Association, and
he got a charter for a group. There's some discussion over whether or not the
NRA knew exactly what they were chartering when they did it, but either
way that occurred. And he called it the Black Armed Guard. And these are World
War II vets, mostly, and they trained for community defense, and they protected important
leaders in North Carolina. Now, in October of 1957, Cole held a rally in Monroe, and
then one of those motorcades took off going to Perry's house. Same thing, yelling out
the windows, firing wildly, stuff like that, scaring people. Campaign of terror. Then they
got to Perry's house and they found out it's not so fun when the rabbits got the gun and
prepared fighting positions.
They took off.
There was an exchange of gunfire and Catfish's crew, well, they left.
And then this weird thing happened.
It seemed like Catfish just wasn't quite as interested in messing with the black community
anymore. So he turned his attention to the natives. And he targeted the Lumbeys. Now
the local cops at the time were kind of like, you may not want to do this. To put it in
modern terminology, the local cops worried he was about to find out. And that's exactly
what happened. He was doing his rallies, and they were going to do one at this field that
they rented near Hayes Pond, near a little town called Maxton. And they announced it
way in advance, and the natives were kind of like, y'all are all going to be in one
place in the middle of the night, surrounded by dense forest, away from any kind of reinforcement.
great idea. Y'all should do that, you know. Now, some of the members of Catfish's
group, well they were a little bit smarter, and they realized that this may
not be a great idea. So Catfish was expecting 5,000 people to show up, 50
dead, and then out of the woods come a bunch of natives, and they were armed
with everything from traditional native implements to like M1s.
And Catfish's crew goes on the run again.
They get defeated again.
There is some dispute.
It seems as though both the black community and white progressives in the area offered to help.
And it's hard to figure out.
It seems like at some points they were turned away
because the Lumbee had the plan and they didn't want to disrupt the plan and in
other cases some of them joined in. The effects from this, it drew a lot of
attention. Now going after Perry and getting defeated there, that was kind of
got kind of got ignored, but the defeat at Hayes Pond, well that got a lot of
attention. It led to the governor denouncing the clan. It led to Catfish
getting arrested. Turned into a big deal. And after that, I guess Catfish left the
state for a while. After he did some time, he came back, tried to reassert his
position within the group, and got ousted. These two events, this chain of
events right here really shut down widespread operations from the Klan.
It led to a lot.
Now as far as a fact check of what's said in that ad, there's nothing that's wrong
with it.
The light that gets shot out, I've read some things that said that that was an accident.
it wasn't that he intentionally shot out the light when it started, it just occurred.
But that's the only thing that's in dispute.
And I don't see that as being very material to anything.
So at the end of the day, I think that while the story is definitely cool, I would question
and why a description of a historical event from the 1950s is still relevant when it comes
to American politics.
What does that say that an account of the Klan being defeated is still a good political
add today. I don't know what it says, but I'm pretty sure it's not good. Anyway, it's
It's just a thought, y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}