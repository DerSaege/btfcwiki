---
title: Let's talk about a possible Hollywood strike....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=iahQ0JHD2Qw) |
| Published | 2021/10/05|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Hollywood unions, with 60,000 members, voted 98% in favor of authorizing a strike for better working conditions and benefits.
- This strike vote is a warning to the industry to negotiate in good faith rather than an immediate action.
- The demands include improved working conditions, larger contributions to pensions and health plans, better rest periods, mill breaks, and a larger share from streaming productions.
- Despite common misconceptions about unions always striking, this specific union has not had a nationwide strike in 128 years, indicating serious grievances.
- The resolution of the members and the high voter turnout suggest skepticism that the industry will negotiate fairly.
- Workers behind entertainment projects seek a fair share of the profits, not just crumbs.
- Delays in entertainment could occur due to the strike, impacting viewers but aiming to secure healthcare and retirement for workers.
- Collective bargaining with industries holding substantial resources can be challenging as they can withstand strikes by hiring replacements.
- Support for the Union from the public could be vital in ensuring a fair negotiation process.
- The strike vote represents a fundamental desire for workers to receive their fair share rather than being overlooked.

### Quotes

- "Delays in entertainment could occur due to the strike, impacting viewers but aiming to secure healthcare and retirement for workers."
- "Workers behind entertainment projects seek a fair share of the profits, not just crumbs."

### Oneliner

Hollywood unions vote overwhelmingly to authorize a strike for better conditions and benefits, signaling a warning to the industry to negotiate fairly and provide workers with their fair share.

### Audience
Entertainment industry workers

### On-the-ground actions from transcript
- Support the Union during negotiations (implied)
- Stay informed about the labor rights issues in the entertainment industry (implied)

### Whats missing in summary
The full transcript provides more context on the Hollywood unions' historical stance on strikes and the challenges faced by workers in negotiating fair treatment and benefits in the entertainment industry.


## Transcript
Well, howdy there, internet people, it's Bo again.
So today we are going to talk about
a possible strike in Hollywood.
There was a vote taken by pretty big unions,
got 60,000 members.
So the International Association
of Theatrical Stage Employees.
60,000 members, about 90% voter turnout
and 98% voted in favor
of authorizing a strike.
Now, this isn't the vote to actually strike.
This is kind of a warning shot.
This is like, this is coming if you don't come to the table
and operate in good faith.
Now, what do they want?
You know, a bunch of liberal stuff.
Better working conditions, larger contributions
to pensions and health plans, better rest periods,
mill breaks, a larger cut from the streaming production,
stuff like that, you know?
Now, when people talk about unions,
they often say that it's a,
that they just go on strike any time they want to raise.
It's worth noting that this union
hasn't had a nationwide strike in 128 years.
They probably have some legitimate grievances
if they're going to this level.
Now, there still is a chance that the industry meets them
and things move forward.
But the resolve of people who are voting in favor of it
and the volume suggests that they probably
don't think the industry is going
to come to the table fairly.
They're not going to get the cut.
And that's basically what this boils down to,
is that the people who work behind the scenes
on a lot of your favorite entertainment projects,
that they want an actual piece of the pie
instead of just the crumbs.
So if your favorite show gets delayed, just understand.
You're putting up with a delay in the new season of your show
so somebody can get health care.
or somebody can have a retirement, something like that.
Collective bargaining of this sort,
when you're talking about unions
and you're talking about industries
that have tons of money,
it's hard because of the amount of money they have,
they can ride out strikes pretty well.
The industries can hire people to come in.
So the Union probably could use your support.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}