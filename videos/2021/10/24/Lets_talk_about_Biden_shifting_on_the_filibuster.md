---
title: Let's talk about Biden shifting on the filibuster....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=7KiLdt_Bd78) |
| Published | 2021/10/24|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:
- Explains the filibuster in the Senate, requiring 60 votes to pass legislation, a rule not part of the Constitution.
- Biden administration is considering changing the filibuster rule to push through legislation with just 51 votes.
- Warns that if Democrats remove the filibuster, they must follow through aggressively on progressive legislation without hesitation.
- Emphasizes the importance of Democrats staying committed to pushing through significant policies once the filibuster is gone.
- Points out the risk that if Democrats fail to publicize the impacts of their policies post-filibuster removal, Republicans could exploit it to pass regressive laws.
- Stresses that Democrats need to be prepared to face the consequences of removing the filibuster and push forward on various key issues like climate change, education, police reform, and social justice.
- Argues that once the filibuster is removed, there's no turning back, and voters will expect substantial progress before the next election.
- Urges for a clear message of moving the country forward and addressing critical societal issues to be conveyed post-filibuster removal.

### Quotes

- "If the Democratic Party amends or does away with the filibuster, they have to push through every piece of progressive legislation they can."
- "If they take the gloves off, they have to stay off."
- "Once that's gone, there's no excuse."
- "And the voters will expect a lot done in a very short period of time."
- "They have to be able to experience the effects of these policies at their kitchen table before the next election."

### Oneliner

Beau warns Democrats to follow through on progressive legislation if they remove the filibuster, stressing the importance of taking decisive action for lasting change in the country.

### Audience

Politically active individuals

### On-the-ground actions from transcript

- Mobilize to support progressive legislation and policies post-filibuster removal (implied)
- Advocate for comprehensive climate change initiatives, education reforms, police reform, and social justice measures in your community (implied)
- Ensure voters are informed about the impacts of policy changes and legislation at a local level (implied)

### Whats missing in summary

The full transcript provides a detailed analysis of the potential consequences of removing the filibuster and the need for Democrats to effectively push through progressive policies in such a scenario.

### Tags

#Filibuster #DemocraticParty #ProgressiveLegislation #PoliticalChange #VoterExpectations


## Transcript
Well, howdy there, internet people. It's Beau again.
So today we're going to talk about Biden's shifting attitude towards the filibuster.
Now, for those that don't know, the filibuster is a rule in the Senate,
and the effect of the rule is that it ends up requiring 60 votes to get something passed.
It's a rule. It's not part of the Constitution. It's a rule.
The purpose of it, theoretically, was to make sure that there was proper debate about things.
That was the goal, in theory.
In reality, the filibuster has been an anchor on progress in this country for a very, very long time.
The Biden administration is indicating that it's kind of ready to change this rule.
And fine. Fine.
If the rule is changed, it only takes 51 votes. They'll be able to push a lot of stuff through.
If they do this, they're taking the gloves off.
The thing is, they can't put them back on.
You can't do this and then get cold feet.
If the Democratic Party amends or does away with the filibuster,
they have to push through every piece of progressive legislation they can.
They have to tout it at every turn. They have to play hardball from here on out.
No just getting rid of it to get the spending package through.
Once it's gone, the Democratic Party has to go all the way.
You are through the door and you are now in a really big fight.
See, the risk is that the Democratic Party gets rid of it, pushes through a little bit,
but the impacts of that policy, well, they're not felt right away.
They're not felt and they don't do a good job publicizing it.
They don't get on message.
And then when that next election rolls around, that majority shifts.
And you've handed Republicans the tool they need to turn this country
into the most regressive, backward state in the world.
If the Democratic Party is up for it and they are actually ready to play hardball
and to view politics in the sense of moving the country forward,
rather than trying to just constantly see compromise with people who do not want to compromise,
it's a good idea.
If they're not willing to do that and they get rid of this filibuster,
expect Republicans to push through every single curtailment of civil liberties,
anti-progress, anti-everything piece of legislation that they can at the first opportunity they have.
It is a huge gamble.
That being said, there have been enough Democrats talk about getting rid of the filibuster publicly
that it may not matter because if that majority shifts, Republicans may do it anyway.
There's enough ammunition for ads to sit there and say, oh, well, you supported it here.
So there may not be that much to lose.
But there's no half-seize with this.
It's got to be all or nothing.
If they take the gloves off, they have to stay off.
And they have to push through everything they can as quickly as they can.
And they need to get on message that we're moving the country forward.
We're doing something about climate change.
We're doing something about education.
We're doing something about police reform.
We're doing something about the environment in general.
We are doing something about the different demographics in this country that have been wronged.
Because once that's gone, there's no excuse.
There's no way to take it back.
And the voters will expect a lot done in a very short period of time.
And they have to be able to fill it.
They have to see it.
They have to be able to experience the effects of these policies at their kitchen table before the next election.
Anything short of that is failure.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}