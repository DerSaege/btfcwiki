---
title: Let's talk about the costs of not passing the infrastructure bill....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=pl1tiy3GYqw) |
| Published | 2021/10/07|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Comparative analysis on the infrastructure bill versus the DOD budget over 10 years.
- Infrastructure bill aims to accomplish infrastructure development and address climate change impacts.
- DOD spending a significant amount on preparing for resource-based conflicts.
- The bill will provide necessary infrastructure to help the US cope with climate change impacts.
- UN study shows a significant increase in flood and drought-related disasters.
- Projection of inadequate water access for 5 billion people by 2050.
- Mitigating these effects could save lives and resources.
- Questioning opposition to the bill, suggesting it’s vital for national resilience.
- Implication that opposition may have ulterior motives benefiting from war machine profits.
- Stressing the importance of current actions shaping the future.
- Infrastructure bill seen as a means to reduce severity of climate-related impacts.
- Urging reevaluation of opposition stance towards investing in infrastructure.
- Choice between investing in infrastructure or future military casualties.
- Encouraging reflection and reconsideration of positions on the bill.

### Quotes

- "If you are in opposition to this, you might want to really rethink your position."
- "Not sure why people would be in opposition to this unless they want the United States to fail."
- "The future is determined by what happens now, by what we do today."
- "You might want to spend it on roads and bridges and cleaner energy or you want to spend it on flag-covered coffins in the future."
- "It is worth noting that DOD is spending a whole lot of that 8 trillion to gear up to fight over resources."

### Oneliner

Beau compares infrastructure bill costs to DOD's budget, urging reevaluation of opposition for a resilient future mitigating climate impacts.

### Audience

US citizens

### On-the-ground actions from transcript

- Support infrastructure bill to invest in sustainable infrastructure and combat climate change (implied).

### Whats missing in summary

The full transcript provides detailed analysis and statistics supporting the importance of investing in infrastructure for climate resilience and national security.


## Transcript
Well, howdy there, internet people.
It's Beau again.
So today, we are going to talk
about the infrastructure bill again.
But we're going to do some comparative analysis.
You know, people keep saying 3.5 trillion,
that's like super expensive.
Sure, it's over 10 years though.
DOD's budget over 10 years, it's like 8 trillion.
That doesn't sound like such a big deal
when you start thinking about it like that.
Especially when you consider
what the infrastructure bill will accomplish.
You know, some of it is exactly what it says,
is infrastructure.
Some of it is infrastructure that is designed
to mitigate the effects of climate change.
It is worth noting that DOD is spending a whole lot
of that 8 trillion to gear up to fight over resources.
We've talked about it before on the channel.
We've talked about the studies.
We've talked about the fact that DOD is researching ways
to suck water out of the air
so soldiers in remote locations can get fresh water.
The infrastructure bill will help mitigate that
and provide the infrastructure necessary
to continue to give the United States an edge
so it can better deal with the impacts from climate change
that are already going to occur.
Some of it is inevitable.
It is going to happen.
A study coming out of the UN points out
there has been 134% increase in flood-related disasters
over the last 20 years
when compared with the 20 preceding years.
More than doubled.
29% increase in drought-related disasters.
25% of cities already experience regular water shortages.
And by 2050, 5 billion people
will have inadequate access to water.
If we could mitigate some of that,
reduce the effects because we change our footprint,
well, I mean, that seems like it would be a really good thing
if it's 3.5 trillion now
or 8 trillion plus the inevitable loss of life.
I mean, this seems really simple.
Not sure why people would be in opposition to this
unless they want the United States to fail
or are personally profiting
from the war machine of the United States.
And certainly nobody in the US Senate
would be like that, right?
The future is determined by what happens now
but what we do today.
This infrastructure bill will help mitigate things.
It will make them less severe.
It will also help the United States
survive the impacts that are already inevitable.
If you are in opposition to this,
you might want to really rethink your position
and consider whether or not you want to spend the money
on roads and bridges and cleaner energy
or you want to spend it on flag-covered coffins
in the future.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}