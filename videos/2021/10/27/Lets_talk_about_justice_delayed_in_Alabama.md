---
title: Let's talk about justice delayed in Alabama....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=FSECaOWeldY) |
| Published | 2021/10/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Talking about buses and justice in Montgomery, Alabama during a specific historical event.
- Recalling the scenario where buses were divided and individuals were required to move if an area got too crowded.
- Describing the incident where a bus driver asked someone to move, leading to police involvement.
- Mentioning Claudette Colvin, a teenager who also resisted giving up her seat before Rosa Parks, but faced media scrutiny due to being unmarried and pregnant.
- Updating on Claudette Colvin's current efforts to have her record expunged at the age of 80.
- Questioning why Claudette Colvin hasn't been pardoned yet despite the historical significance of her actions.
- Expressing support for Claudette Colvin's endeavors while also advocating for a pardon to acknowledge the wrong that was done.
- Pointing out the lack of termination of Claudette Colvin's probation from the state of Alabama officially.
- Emphasizing the importance of recognizing and atoning for the past events that occurred during the civil rights movement in Montgomery.
- Advocating for a more significant gesture beyond record expungement to address the injustices faced by Claudette Colvin.

### Quotes

- "It seems like Alabama owes her."
- "When somebody is penalized for doing the right thing, at some point, there should be some kind of atonement."
- "It seems like there should be that moment where the state says, 'Yeah, we were wrong. We're sorry. We beg your pardon.'"

### Oneliner

Beau talks about buses, Rosa Parks, and the overlooked Claudette Colvin, questioning why Alabama hasn't pardoned her despite her historical significance and the need for atonement.

### Audience

Alabama residents, Civil Rights advocates.

### On-the-ground actions from transcript

- Support Claudette Colvin's petition to get her record expunged (implied).
- Advocate for a pardon for Claudette Colvin in Alabama (implied).

### Whats missing in summary

The emotional weight of Claudette Colvin's story and the importance of acknowledging and rectifying past wrongs in history.

### Tags

#CivilRights #HistoricalInjustice #Alabama #Montgomery #RosaParks #ClaudetteColvin


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we're going to talk about buses and justice
in Montgomery, Alabama way back then.
I mean, I don't think we need to go over all of it.
I'm pretty sure most of us are pretty familiar
with the scenario back then.
We know the way the buses were divided up.
We know that if one area got too crowded,
well, you got to move.
And we know that one fateful day, the bus driver was like,
hey, you need to move.
And somebody was like, nah, not going to.
If you don't move them, call the police.
Call them.
I don't care.
Paraphrasing the conversation, of course.
And police showed up.
And she was drug off the bus, faced a bunch of charges.
Nine months later, Rosa Parks did it.
Prior to Rosa Parks, if you don't know,
a teenager did pretty much the same thing.
However, this teenager wasn't as media friendly
as Rosa Parks was.
Wasn't the best case to use.
There are a whole bunch of reasons
that are assigned to this.
The key one to me was the whole unmarried pregnant thing.
It would give reporters a way to attack her.
A way to say, this isn't a good kid.
She's always in trouble.
Discredit the whole thing.
Now, Claudette Colvin today, she's in her 80s.
And she is currently petitioning to get her record expunged.
Now, it's worth noting she was judged delinquent way back then
and placed on probation.
Probation that the state of Alabama
has apparently never seen fit to terminate,
at least not officially.
There has been no letter.
I would like to believe that that was just an oversight.
However, it was Alabama way back then.
Maybe it wasn't.
But she's 80 something years old now.
I feel fairly certain that she's no longer delinquent.
And I feel that she definitely deserves
to have her record expunged.
And while I fully support that endeavor,
I have just a nagging question.
Can anybody explain to me why she hasn't been pardoned yet?
I mean, that seems like something that would just
come along with the due course of things,
with the way everything's progressed,
with the fact that these events in Montgomery
helped undo a fundamental wrong of this country.
It seems like there should be some kind of recognition
for that.
It seems like something that somebody
at some point in the executive office there in Alabama
might want to address.
And I'm not saying that to overlook the drive
to get a record expunged, but I don't believe that's enough.
That doesn't seem like that's enough to me.
The fact that this occurred, that she was arrested,
and some of the events that occurred,
say during the ride to the police station,
if you're not familiar with the story, definitely look into it.
It seems like Alabama owes her.
It seems like Alabama should try to set that straight.
And I don't think expungement cuts it, plain and simple.
With everything going on, this is probably
something that'll get swept under the rug,
not going to get a lot of coverage.
But to me, when somebody is penalized for standing up
for what's right, for doing the right thing,
at some point in time, especially if they're already
80 now, it seems like there should
be some kind of atonement.
It seems like there should be that moment where the state
says, yeah, we were wrong.
It was wrong about everything.
We're sorry.
We beg your pardon.
That's what I think should happen.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}