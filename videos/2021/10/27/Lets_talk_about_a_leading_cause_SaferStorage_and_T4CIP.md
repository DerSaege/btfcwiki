---
title: 'Let''s talk about a leading cause, #SaferStorage, and T4CIP....'
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=hBsuqOnb97M) |
| Published | 2021/10/27|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Today is a big day for a group called trainees for child injury prevention, aiming to draw attention to the leading cause of childhood injury related deaths.
- Firearms, not cars, account for the leading cause of childhood injury-related deaths in the USA.
- 4.6 million kids live in homes where weapons are stored loaded and unsecured, increasing the risk of unintentional shootings.
- Safer storage of firearms is the focus of the group's day of action to encourage responsible gun ownership.
- The importance of securing firearms is stressed, as hidden weapons can be dangerous to curious children.
- Beau compares securing firearms to a vaccine mandate, stating that people should do it willingly without the need for a law.
- He mentions a webinar and chat session organized by the group, mainly comprising doctors, to raise awareness about firearm safety protocols.

### Quotes

- "Hidden doesn't mean safe. Hidden doesn't mean secure. Hidden means, wow, look what I found."
- "Lock up your firearms. 3,500 a year lost to guns."
- "Either you're going to do it because it's the right thing or it's going to become a law."

### Oneliner

Today is a critical day for child injury prevention advocacy, focusing on the dangers of unsecured firearms and the need for responsible storage.

### Audience

Parents, gun owners, community members

### On-the-ground actions from transcript

- Secure firearms in your home to prevent accidental shootings (exemplified)
- Educate yourself and others about responsible firearm storage (exemplified)
- Join online webinars and chats about child injury prevention (exemplified)

### Whats missing in summary

The emotional impact of accidental shootings on children and the urgency of addressing firearm safety proactively.

### Tags

#ChildInjuryPrevention #FirearmSafety #ResponsibleGunOwnership #CommunityAdvocacy #Awareness-Raising


## Transcript
Well, howdy there internet people, it's Beau again.
So, today we're gonna try to hold a megaphone to something.
We're gonna give somebody else the mic, signal boost, whatever it is you want to call it.
We're going to try to help get a message out because there is a group and
today is their big day.
Today's the day everybody's gonna try to get active,
everybody's gonna try to get a message out because there was a change in something recently.
So they're trying to draw attention to it.
The name of the group is trainees for child injury prevention.
As the name suggests, their goal is to mitigate childhood injury.
All the stats that I'm about to give you, they're coming from them.
I'm trying to help get this message out because it's an important one.
In 2019, the leading cause of childhood injury related deaths changed.
Something new took over.
It is so significant that a child is lost to it every two hours and 34 minutes.
That's 3,500 a year when you're talking about numbers that big.
Again, hard to really picture, so we'll do it this way.
It's a school bus full of kids every nine days.
That's pretty significant.
It accounts for 25.3% of injury related childhood deaths.
You probably think it's cars, right?
I would have assumed it was.
It's not. It's firearms.
Firearms. USA. USA.
Right now, there's a whole lot of people talking about basic safety protocols
because of what happened out there with Alec Baldwin, right?
Whole bunch of people in the gun community saying that if, you know,
if basic safety protocols were followed, that wouldn't have happened.
The thing is, many of the people saying that and millions of Americans
are ignoring the basic safety protocols.
4.6 million kids live in a home where a weapon is stored, loaded, and unsecured.
That is wild to me.
Your kids are curious. Your kids are curious.
They are curious people.
That's one of the joys of being a child.
Hidden doesn't mean safe. Hidden doesn't mean secure.
Hidden means, wow, look what I found.
If firearms are secured, unintentional shootings drop by 85%.
Today, their day of action is all about trying to encourage safer storage.
And I love the fact that they're calling it safer storage
because there's no safe storage, not really.
At the end of the day, the important call to action here is,
one, make sure that you secure your firearms if you own one.
I mean, that seems like it should be common sense.
To me, this is a lot like a vaccine mandate, right?
In my ideal world, you don't need it
because people are going to go get vaccinated.
You don't need a law to tell you to be a good person.
Let me tell you something.
The reason motor vehicles got safer over the years is because of the injuries
and they had regulations imposed on them
because they didn't really do it themselves, right?
That's what will happen here.
Understand, people will secure their firearms safely.
Either you're going to do it because it's the right thing
or it's going to become a law.
That's where you're at with that.
I don't think people need a law to tell them to be a good person,
to protect their kids or their neighbor's kids.
You know, that's something that I've heard a lot from people.
Well, my kids, they know that they shouldn't mess with a firearm.
So I don't need to lock mine up.
No, because your kid has friends.
Your kid has friends.
That friend comes over and they find it.
What happens if they pick it up and pull the trigger?
Who did they hit?
Lock up your firearms.
3,500 a year lost to guns.
Now, this group, they have a webinar going on,
I want to say at noon Eastern.
This video may not get out in time for that.
And then they have a chat going on on Twitter at 2 p.m.
It looks like most of this group is made up of doctors,
is how it appears to me.
I could be wrong about that.
But it does seem with all of the talk about basic safety protocols,
maybe we should take a look at this.
When you have all of these people saying,
well, this wouldn't have happened if they had followed these minor rules.
We should probably not let a crisis go to waste
and use that time to educate.
Anyway, it's just a thought.
Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}