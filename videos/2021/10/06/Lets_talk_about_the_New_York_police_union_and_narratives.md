---
title: Let's talk about the New York police union and narratives....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=gkTTnUU_lMQ) |
| Published | 2021/10/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Explains how the right-wing machine shapes public perception before facts are known.
- Mentions FBI serving warrants on the New York Police's Sergeants Benevolent Association and union president's home.
- Notes the limited information available, with search warrants being served and involvement of Office of Public Corruption.
- Quotes the union's statement indicating the union president may be the target of the federal investigation.
- Observes the trending of the story on Twitter and the generation of a narrative by talking heads.
- Points out that the focus shifted to the union president's support for Trump and past inflammatory tweets.
- Criticizes the creation of a narrative without evidence to suggest political payback.
- Comments on mainstream outlets picking up the narrative created by right-wing media.
- Expresses skepticism towards the speculation around the search warrants without official statements.
- Concludes by reflecting on the narrative already established without concrete evidence.

### Quotes

- "This is how these wild narratives that the right-wing media in the U.S. likes to latch onto, it's how they start, it's how they're created, and it's how they distract from actual events."
- "We don't know what it's about yet. There haven't been any real official statements on it at time of filming."
- "It's the narrative that's already taking shape. And it's all without evidence."

### Oneliner

Beau explains how the right-wing machine shapes narratives before facts are known, using the example of search warrants served in New York, criticizing the premature generation of stories without concrete evidence.

### Audience

Activists, Media Consumers

### On-the-ground actions from transcript

- Fact-check narratives and wait for official statements (implied)

### Whats missing in summary

Full context and Beau's detailed analysis on the creation of narratives before facts are known. 

### Tags

#RightWingMedia #NarrativeShaping #SearchWarrants #PublicPerception #FactChecking


## Transcript
Well, howdy there, Internet people. It's Beau again.
So today, we are going to talk about how the right-wing machine
gets out in front of stories and begins to shape public perception of an event
and establish a narrative before any real facts are even known.
And we're going to talk about how it occurs,
and we're going to use what happened in New York today as an example.
If you don't know, the FBI served some warrants on the New York Police's
Sergeants Benevolent Association, police union up there,
and the union president's home.
Now, that's kind of all we really know, is that the feds served some search warrants.
We know that is true, and there is the general idea that the Office of Public Corruption is involved.
We know that's true.
And then we have this statement from the union itself.
The nature and scope of this criminal investigation has yet to be determined.
However, it is clear that President Mullins is apparently the target of the federal investigation.
We have no reason to believe that any other member of the SBA is involved or targeted in this manner.
Yeah, I mean, that's kind of it.
That's what we know.
There's not a whole lot of a story here just yet, but it trended on Twitter.
The talking heads started generating the narrative.
Now, the reason they do this is because the union president is a very vocal supporter
of former President Trump.
He has made a lot of tweets in the past that could be described as inflammatory.
So that's where the right-wing machine took it.
It's about these tweets.
It's about these tweets.
See, this is political payback.
Is there any evidence to suggest that at all?
No, none.
As far as I know, the Office of Public Corruption doesn't investigate Twitter.
But that was put out there.
It was put out there first, right?
So it starts to shape the perception and it creates that narrative.
This is how these wild narratives that the right-wing media in the U.S. likes to latch onto,
it's how they start, it's how they're created, and it's how they distract from actual events.
We have no clue what these search warrants are about.
The feds are being pretty tight-lipped on this.
You can speculate all you want, but it's not going to do any good.
Now, the sad part is that once the right-wing pundits start talking about it,
then it enters the political sphere.
It enters the normal conversation.
And then mainstream outlets begin to talk about these same things.
They begin to talk about the tweets that he made.
So it reinforces that idea and it shows the people who follow these right-wing pundits.
It says, oh, look, they said this and now it's in the reporting.
That must be what it's about.
It's breaking news.
We don't know what it's about yet.
There haven't been any real official statements on it at time of filming.
Now, y'all won't see this until tomorrow, so maybe by then there'll be some clarification.
I find it unlikely that it has to do with mean tweets, with the exception of one that,
I mean, maybe has something to do with the one where he tweeted out somebody's criminal record.
But even that seems pretty unlikely.
But this is the perception that has already been generated.
It's the narrative that's already taking shape.
And it's all without evidence.
This is how these things start.
So we'll see how it develops from here.
Anyway, it's just a thought. Y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}