---
title: Let's talk about Idaho and republican divisions....
---

| Input     | Output |
| --------- | ------ |
| Link      | [YouTube](https://www.youtube.com/watch?v=yIDdLmQfCgs) |
| Published | 2021/10/06|
| Theme     |  |
| Status    | article incomplete |

# AI Summary (verify and correct me, please!)

### Bits

Beau says:

- Idaho's Republican Party is facing disunity between mainstream conservatives and the far right with authoritarian tendencies.
- Governor Little and Lieutenant Governor are not political allies in Idaho.
- Lieutenant Governor issued an executive order banning vaccine mandates in Governor Little's absence.
- The Lieutenant Governor also tried to mobilize the National Guard without proper authority.
- Major General Garshak clarified the National Guard's role and denied the request.
- Governor Little plans to undo the orders issued by the Lieutenant Governor upon his return.
- This isn't the first time the Lieutenant Governor has acted independently; she previously issued a ban on mask mandates.
- Beau questions the Lieutenant Governor's fitness for office if she continues to make power grabs.
- The discord within the Republican Party in Idaho is becoming more evident.
- Beau urges Republicans to stand against authoritarianism within their party to protect democracy.

### Quotes

- "If somebody is so overcome with the power granted by the absence of the governor that they begin issuing executive orders, this might not be somebody fit for the governor's office."
- "This display of a power grab should weigh pretty heavily on you."
- "They're taking shots at each other pretty regularly."
- "If there's a time for those people who are just conservatives to take a stand and say, we don't want to have anything to do with this, it's this moment."
- "Their party will be overrun with those who are into political grandstanding, those who are in for attaining power for power's sake, and then using it."

### Oneliner

Idaho's Republican Party grapples with disunity as power struggles between Governor and Lieutenant Governor play out, prompting questions on authoritarian tendencies and the need for party reform.

### Audience

Republicans, Idaho citizens

### On-the-ground actions from transcript

- Support candidates who prioritize democracy over power grabs (implied)
- Advocate for unity within the Republican Party to combat authoritarian influences (implied)

### Whats missing in summary

Full context and nuances of Beau's analysis and call for action.

### Tags

#Idaho #RepublicanParty #Governor #LieutenantGovernor #Authoritarianism #Democracy


## Transcript
Well, howdy there, internet people, it's Beau again.
So today, we are going to talk about Idaho
and disunity within the Republican Party
that has become very, very public.
We've talked about how the Republican Party is
factionalizing between what could
be considered mainstream regular conservatives that
are semi-normal people and the far right that displays
lot of authoritarian characteristics that are generally very unhealthy for a semi-functioning
democracy like the United States.
This break has become very apparent and very public in Idaho.
The first thing you need to know about Idaho is that the governor and lieutenant governor,
they're not necessarily political allies.
They don't run on a ticket together.
Okay, so the elected governor, the people or the person who the people of Idaho elected
to be governor is Governor Little.
Governor Little is on a state trip.
He's down in Texas at a meeting with other governors.
The lieutenant governor has taken it upon herself to act as governor in his absence
and issued an executive order banning employers from requiring vaccines of their employees.
That is entertaining in some ways, because Governor Little has made it clear
that he will rescind that as soon as he gets back.
She also attempted to mobilize the National Guard and send them down to the border.
Now, luckily, Major General Garshak is, I don't know, has read the Constitution, has read the laws governing this,
and basically told the Lieutenant Governor that the Idaho National Guard is not a law enforcement agency,
and that he was unaware of any EMAC, which is an emergency request that one state issues saying they want help from
other national guards,  from other states and national guards. And basically just kind of told her to go away.
Now this is all entertaining because nothing was permanently damaged. You know, Governor Little
is saying that he's going to undo all of this as soon as he gets back and that the lieutenant
governor is not authorized to act on his behalf. The thing is, this is the second time this has
happened, happened back in May as well when the lieutenant governor issued a
mask mandate, a ban on mask mandates. This is something that the people of
Idaho need to remember. If somebody is, let's just say, so overcome with the
power granted by the absence of the governor that they begin issuing
executive orders, this might not be somebody fit for the governor's office.
From what I understand, the lieutenant governor plans on running for governor.
Whether you agree with the actions that the lieutenant governor was going to take here or
not, this display of a power grab should weigh pretty heavily on you.
you. If she's willing to do this, knowing that it's going to be Andan and knowing
that it's not going to make her look good, that it's just the raw exercise of
power, I can only imagine what that says about how she might act if she was
actually given power. For the rest of the country, this is something to take note
of because it highlights the discord that exists within the Republican Party.
We've talked about the 11th commandment and how you're not supposed to criticize
another Republican. That commandment is slipping away. They aren't operating by
anymore. They're taking shots at each other pretty regularly. And if there is
a moment for the Republican Party to weed out the overt authoritarians in
their midst, it's now. If there's a time for those people who are just conservatives
to take a stand and say, we don't want to have anything to do with this, we
actually kind of like having a semi-functioning democracy. It's this moment. Because if they
wait too long, their party will be overrun with those who are into political grandstanding,
those who are in for attaining power for power's sake, and then using it.
Anyway, it's just a thought.
y'all have a good day.

## Beau's Shirt 
{{Shirt}}
## Easter Eggs on Shelf
{{EasterEgg}}