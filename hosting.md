---
title: Hosting the Wiki
description: 
published: true
date: 2024-04-15T18:26:06.404Z
tags: 
editor: markdown
dateCreated: 2024-04-15T18:24:53.763Z
---

# transfer

In order to set this up *not* for the first time, you'll need to get the domain name transferred, and ideally also the server itself.  You'll need a digitalocean account and to drop in on discord and ask about it.

# wiki:
- currently hosted on a digitalocean droplet from the [wiki.js template](https://docs.requarks.io/install/digitalocean)
- I'm using droplet size "Basic / 2 GB / 1 vCPU", the wiki was sometimes crashing on the smaller size
- it's possible to [transfer a droplet to another user](https://docs.digitalocean.com/support/how-do-i-transfer-a-droplet-to-another-user/) so if possible we should probably just do that, would need someone to set up a digitalocean account

#### to setup from scratch, you would need:
- the changed wiki.js code I use: https://gitlab.com/ds9fan/wikijs-fork
- the command to launch the changed wiki.js code, after setting up with the template:
    1. connect using either `access -> droplet console` (screenshot), or ssh (advanced, requires setup, only do this if you've used ssh before)
    	![image3.png](/image3.png)
    2. `git clone https://gitlab.com/ds9fan/wikijs-fork.git`
    3. `cd wikijs-fork; docker stop wiki; docker rm wiki; docker create --name=wiki -e LETSENCRYPT_DOMAIN=justathought.wiki -e LETSENCRYPT_EMAIL=`your_email@goes.here` -e SSL_ACTIVE=1 -e DB_TYPE=postgres -e DB_HOST=db -e DB_PORT=5432 -e DB_PASS_FILE=/etc/wiki/.db-secret -v /etc/wiki/.db-secret:/etc/wiki/.db-secret:ro -e DB_USER=wiki -e DB_NAME=wiki -e UPGRADE_COMPANION=1 --restart=unless-stopped -h wiki --network=wikinet -p 80:3000 -p 443:3443 requarks/wiki:2; docker start wiki`
- import the settings and permissions; I can't find an automated way to do this import so I guess you have to go through and manually apply these json files I exported using the automated exporter that for some reason has no matching importer? sigh that's pretty annoying.
- to set up git sync, you'd need to create a [gitlab](https://gitlab.com/) account and create an ssh private key, then paste that private key into the git target on the appropriate tab on the storage settings page, eg if you keep using justathought.wiki as the domain it would be https://justathought.wiki/a/storage
- to set up algolia search you'd do the [instructions from the wikijs docs](https://docs.requarks.io/search/algolia), without this there will be no search and the wiki is kind of useless

# transcription
you'll need to run `update.py` in [this repo](https://gitlab.com/ds9fan/beau-subtitles-wiki-base/) to use my transcription setup and pipeline. if you want it to run regularly, you'll need to run it from cron or something like that, eg I have this script set up to run in cron:

```
#!/bin/bash -i

cd ~/projects/beau-subtitles/

exec > cronjob.log
exec 2>&1
source $HOME/bin/fix_cron_env.sh
echo "hi! I ran! $(date)"

python ./update.py
```
fix_cron_env has most of my cron env setup. it's just a dump of my `env` from a normal shell, notably this includes my openai api key.

folks attempted to dockerize this script before. what I'd suggest doing is downloading that repo, then pasting the files `update.py` and `transcribe.py` into gpt4 or claude opus, and ask for a dockerfile that'll run it. the first try should work.
